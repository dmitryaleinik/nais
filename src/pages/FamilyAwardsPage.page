<apex:page controller="FamilyTemplateController" extensions="FamilyAwardsController" sideBar="false" showHeader="false" applyHtmlTag="false"
           title="Family Portal">
	<apex:stylesheet value="{!$Resource.FamilyAwards}"/>
	<!-- TODO SFP-1750: Can we remove "Page" from the page name here and for the FamilyAward page? -->
	<!--Done-->
	<script>
		function acceptAward(folderId)
		{
			accept(folderId);
		}

		function denyAward(folderId)
		{
			deny(folderId);
		}
	</script>
	<!-- SFP-1750: Some styles are added to counter some things that are done by SLDS-->

	<!-- TODO SFP-1750 can we just use the SLDS styles for this? -->
	<apex:actionStatus id="disableButtons" onstart="disableButtons();" onstop="enableButtons();"/>

	<apex:composition template="{!template}">
		<apex:define name="header">
			<!--<apex:stylesheet value="{!URLFOR($Resource.SchoolPortalStyles, '/css/newStyles.css')}"/>-->
			<!--<apex:includeScript value="{!$Resource.Jquery1113min}" />-->
			<meta charset="utf-8"/>
			<meta http-equiv="x-ua-compatible" content="ie=edge"/>
			<meta name="viewport" content="width=device-width, initial-scale=1"/>
			<!-- Import the Design System style sheet -->
			<apex:slds/>
		</apex:define>
		<apex:define name="body">
			<apex:form>
				<apex:actionFunction name="accept" action="{!acceptAction}" reRender="awardsForm">
					<apex:param value="" name="folderId"/>
				</apex:actionFunction>
				<apex:actionFunction name="deny" action="{!denyAction}" reRender="awardsForm">
					<apex:param value="" name="folderId"/>
				</apex:actionFunction>

				<!-- TODO SFP-1750: This is the new styling to use  -->
				<div class="slds-scope">
					<div class="myApp">
						<!-- TODO SFP-1750: use page header for description of the year/pfs awards that are being viewed. -->
						<div class="awardsMessage">
		                                        <span>
		                                        <apex:image value="{!URLFOR($Resource.GlobalMessagesStyles,'icons/info.svg')}"
		                                                    width="27" styleClass="gmIcon"/>
		                                        </span>
							{!JSINHTMLENCODE(SUBSTITUTE($Label.FamilyAwards_Help_Message,'{0}',controller.academicYear.Name))}
						</div>

						<div class="slds-grid slds-gutters slds-m-top--large">
							<div class="slds-col slds-p-horizontal_large slds-size--12-of-12">
								<apex:repeat var="applicant" value="{!applicantsInfo}">
									<div class="slds-card">
										<div class="slds-card__header slds-grid">
											<header class="slds-media slds-media_center slds-has-flexi-truncate">
												<div class="slds-media__body">
													<h2>
														<a href="javascript:void(0);" class="slds-card__header-link slds-truncate"
														   title="[object Object]">
															<span class="slds-text-heading_small">{!applicant.applicantName}</span>
														</a>
													</h2>
												</div>
											</header>
										</div>
										<div class="slds-card__body slds-card__body_inner">
											<apex:repeat var="award" value="{!applicant.awards}">
												<apex:outputPanel id="awardsForm">
													<div class="slds-card slds-card_boundary">
														<div class="slds-card__header slds-grid">
															<header class="slds-media slds-media_center slds-has-flexi-truncate">
																<div class="slds-media__body">
																	<h2>
																		<a href="javascript:void(0);"
																		   class="slds-card__header-link slds-truncate"
																		   title="[object Object]">
																			<span class="slds-text-heading_small">{!award.SchoolName}
																				Award</span>
																		</a>
																	</h2>
																</div>
															</header>
															<div class="slds-no-flex">
																<span class="slds-badge">{!award.status}</span>
															</div>
														</div>
														<apex:outputPanel rendered="{!NOT(ISNULL(award.infoMessage))}">
															<div class="awardsMessage">
		                                                <span>
		                                                    <apex:image value="{!URLFOR($Resource.GlobalMessagesStyles,'icons/info.svg')}"
		                                                                styleClass="gmIcon" width="23"/>
		                                                 </span>
																{!award.infoMessage}
															</div>
														</apex:outputPanel>
														<div class="slds-card__body">
															<div class="slds-card__body--inner">
																<div class="slds-tile">
																	<div class="slds-tile__detail slds-text-body--small">
																		<dl class="slds-list--horizontal slds-wrap">
																			<dt class="slds-item--label awardField" title="Amount Awarded">
																				Amount Awarded:
																			</dt>
																			<dd class="slds-item--detail awardValue"
																			    title="achoi@burlingtion.com">
																				<apex:outputText value="{0, number, $###,##0}">
																					<apex:param value="{!award.amountAwarded}"/>
																				</apex:outputText>
																			</dd>
																			<dt class="slds-item--label awardField" title="Total Expenses">
																				Unmet Financial Need:
																			</dt>
																			<dd class="slds-item--detail awardValue"
																			    title="achoi@burlingtion.com">
																				<apex:outputText value="{0, number, $###,##0}">
																					<apex:param value="{!award.unmetFinancialNeed}"/>
																				</apex:outputText>
																			</dd>
																		</dl>

																	</div>
																</div>
															</div>
														</div>
														<div class="slds-card__footer">
															<apex:outputPanel rendered="{!award.showAcceptButton}">
																<button onclick="acceptAward('{!award.folderId}'); return false;"
																        class="slds-button slds-button_success">
																	Accept
																</button>
															</apex:outputPanel>
															<apex:outputPanel rendered="{!award.showDenyButton}">
																<button onclick="denyAward('{!award.folderId}'); return false;"
																        class="slds-button slds-button_destructive">
																	Deny
																</button>
															</apex:outputPanel>
															<apex:outputPanel>
																<button class="slds-button slds-button_neutral detailButton">Detail</button>
															</apex:outputPanel>
														</div>
													</div>
												</apex:outputPanel>
											</apex:repeat>
										</div>
									</div>
								</apex:repeat>
							</div>
						</div>
					</div>
				</div>
			</apex:form>

		</apex:define>
	</apex:composition>

</apex:page>