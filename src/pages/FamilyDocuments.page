<apex:page controller="FamilyTemplateController" extensions="FamilyDocumentsController" sideBar="false" showHeader="false" title="Family Portal">
    <apex:composition template="{!template}">
        <apex:define name="header">
            <apex:stylesheet value="{!URLFOR($Resource.SchoolPortalStyles, '/css/newStyles.css')}"/>
            <apex:stylesheet value="{!URLFOR($Resource.FamilyDocumentsCSS)}" />
            <script src="{!URLFOR($Resource.FamilyDocumentsJS)}" ></script>
            <script src="https://cdn.rawgit.com/vast-engineering/jquery-popup-overlay/1.7.13/jquery.popupoverlay.js"></script>
            <apex:includeScript value="{!$Resource.Jquery1113min}" />

            <style>
                #simplemodal-container{
                    width:auto;
                    height:auto;
                }

            </style>
        </apex:define>
        <apex:define name="body">
            <apex:form id="thisform">
                <apex:pageBlock >

                    <div id="pageBackground">
                        <div class="block pageTitle fp">
                            <div class="blockheader">
                                <h2>My Documents</h2>
                            </div>
                            <div class="process_helpers_container">
                                 <div class="process_helpers--flex-left"  >
                                    <div class="process_helpers--flex-top"> <apex:image url="{!URLFOR($Resource.FamilyDocumentsImages, 'FamilyDocumentsImages/mydocuments_01.png')}" /> </div>
                                    <div class="process_helpers--flex-bottom">
                                         <!-- Family Documents Title 1 Hover, is a custom label for the left most image title. named for it's place in the order as to keep it generic enough -->
                                       <apex:outputText escape="false" value="{!$Label.Family_Documents_Image_1_Title}" />
                                    </div>
                                    <!-- Family Documents Image 1 Hover, is a custom label for the left most image popover. named for it's place in the order as to keep it generic enough -->
                                    <div class="process_helpers--popup"> <apex:outputText styleClass="popupOutputText" escape="false" value="{!$Label.Family_Documents_Image_1_Hover}" />   </div>
                                </div>
                                <div class="process_helpers--flex-left">
                                    <div class="process_helpers--flex-top"><apex:image url="{!URLFOR($Resource.FamilyDocumentsImages, 'FamilyDocumentsImages/mydocuments_02.png')}" />  </div>
                                    <div class="process_helpers--flex-bottom">
                                        <!-- Family Documents Title 2 Hover, is a custom label for the left most image title. named for it's place in the order as to keep it generic enough -->
                                        <apex:outputText escape="false" value="{!$Label.Family_Documents_Image_2_Title}" />
                                    </div>
                                    <!-- Family Documents Image 2, is a custom label for the second from the left image popover. named for it's place in the order as to keep it generic enough -->
                                     <div class="process_helpers--popup"><apex:outputText styleClass="popupOutputText" escape="false" value="{!$Label.Family_Documents_Image_2_Hover}" />   </div>
                                </div>
                                <div class="process_helpers--flex-left">
                                    <div class="process_helpers--flex-top"><apex:image url="{!URLFOR($Resource.FamilyDocumentsImages, 'FamilyDocumentsImages/mydocuments_03.png')}" />  </div>
                                    <div class="process_helpers--flex-bottom">
                                        <!-- Family Documents Title 3 Hover, is a custom label for the left most image title. named for it's place in the order as to keep it generic enough -->
                                        <apex:outputText escape="false" value="{!$Label.Family_Documents_Image_3_Title}" />
                                    </div>
                                    <!-- Family Documents Image 3, is a custom label for the third from the left image popover. named for it's place in the order as to keep it generic enough -->
                                     <div class="process_helpers--popup"><apex:outputText styleClass="popupOutputText" escape="false" value="{!$Label.Family_Documents_Image_3_Hover}" />   </div>
                                </div>
                                <div class="process_helpers--flex-left process_helpers--last">
                                    <div class="process_helpers--flex-top"><apex:image url="{!URLFOR($Resource.FamilyDocumentsImages, 'FamilyDocumentsImages/mydocuments_04.png')}" />  </div>
                                    <div class="process_helpers--flex-bottom">
                                        <!-- Family Documents Title 4 Hover, is a custom label for the left most image title. named for it's place in the order as to keep it generic enough -->
                                        <apex:outputText escape="false" value="{!$Label.Family_Documents_Image_4_Title}" />
                                    </div>
                                     <!-- Family Documents Image 4, is a custom label for the right most image popover. named for it's place in the order as to keep it generic enough -->
                                     <div class="process_helpers--popup"><apex:outputText styleClass="popupOutputText" escape="false" value="{!$Label.Family_Documents_Image_4_Hover}" />   </div>
                                </div>
                            </div>
                        </div>
                        <div class="collapsible-section">
                            <div class="family-documents-collapsible">
                                <h2>
                                    <span class="family-documents_heading__flex-item-first"><div style="display: inline-block; width: 2em"><span class="arrow arrow-right"></span></div> Document Due Dates</span>
                                </h2>
                                <div class="family-documents-body closed">
                                    <c:DocumentDueDates pfsRecord="{!pfsRecord}" displayHeader="false" />
                                </div>
                            </div>

                            <div class="family-documents-collapsible">
                                <h2>
                                    <span class="family-documents_heading__flex-item-first"><div style="display: inline-block; width: 2em"><span class="arrow arrow-right"> </span></div> FAQs</span>
                                </h2>
                                 <div class="family-documents-body closed">
                                    <apex:repeat value="{!faqs}" var="faq">
                                        <div class="faq">
                                            <div class="faq__header">
                                                <apex:outputLink target="_blank" value="{!URLFOR($Page.FamilyHelpResultsDetail)}?articleId={!faq.knowledgeArticle.KnowledgeArticleId}&retUrl=/familydocuments">{!faq.knowledgeArticle.title}</apex:outputLink>
                                            </div>
                                            <div class="faq__body">
                                                <apex:outputText escape="false" value="{!faq.abbreviatedBody}" />
                                                <apex:outputPanel styleClass="faq__more" rendered="{!faq.hasAbbreviatedBody}">
                                                    <apex:outputLink styleClass="faq__more--link" style="padding-left: .5em" target="_blank" value="{!URLFOR($Page.FamilyHelpResultsDetail)}?articleId={!faq.knowledgeArticle.KnowledgeArticleId}&retUrl=/">More</apex:outputLink>
                                                </apex:outputPanel>
                                            </div>
                                        </div>
                                    </apex:repeat>
                                </div>
                            </div>

                            <div class="family-documents-collapsible">
                                <h2>
                                    <span class="family-documents_heading__flex-item-first">
                                        <div style="display: inline-block; width: 2em">
                                            <span class="arrow arrow-right"></span>
                                        </div>
                                        {!$Label.Family_Documents_Additional_Resources_Section_Title}                                        
                                    </span>
                                </h2>
                                 <div class="family-documents-body closed">
                                    <p style="padding: 0 0 .5em 0;">
                                        <div class="family-documents_cover-sheet">
                                            Submitting Required Documents:
                                            <apex:outputLink target="_blank" value="http://solutionsbysss.com/parents/apply/required-documents"> <strong> http://solutionsbysss.com/parents/apply/required-documents </strong> </apex:outputLink>
                                        </div>
                                    </p>
                                    <p>
                                        <div class="family-documents_cover-sheet">
                                            International families:
                                            <apex:outputLink target="_blank" value="http://solutionsbysss.com/parents/apply/international-applicants"> <strong> http://solutionsbysss.com/parents/apply/international-applicants </strong> </apex:outputLink>
                                        </div>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="family-documents_cover-sheet">
                            Mailing in your documents? <apex:outputLink target="_blank" value="{!$Page.FamilyAppCoverSheet}?id={!pfsid}"><strong> Print a Cover Sheet </strong></apex:outputLink>
                        </div>

                       <c:FamilyDocumentList pfsRecord="{!pfsRecord}" selectedAcademicYearId="{!academicYearId}"/>

                    </div>
                </apex:pageBlock>
            </apex:form>
            <c:FamilyDocumentSpringUpload pfsRecord="{!pfsRecord}" academicYearId="{!academicYearId}" documentSource="Uploaded by Parent" />
        </apex:define>
    </apex:composition>

</apex:page>