<apex:page controller="SchoolDashboardController" sidebar="false" action="{!init}">
    <link class="user" href="/schoolportal/apexpages/chart/vf-chart.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        
        .basicversion {color:grey;}
        .basicversion a {color:grey;}
        
        .adminCollapsible {
            padding-left: 20px;
            background: transparent url({!URLFOR($Resource.SchoolPortalStyles, '/images/new/collapsableClosedSmall.png')}) no-repeat 7px 13px;
            cursor: pointer;    
        }        
        
        .adminCollapsible.adminExpanded {
            padding-left: 20px;
            background: transparent url({!URLFOR($Resource.SchoolPortalStyles, '/images/new/collapsableSmall.png')}) no-repeat 7px 13px;
            cursor: pointer;
        }
        
        #expandLink {text-decoration: none;}            
        #expandLink A:hover {text-decoration: none;}        

        #renewal-link{
            font-size: 20px;
            font-weight: bold;
            color: #527f44;
        }
        #leftmaindiv{
            min-width: 900px !important;
        }
    </style>
    
    <script type="text/javascript">
    
        function toggle_visibility(id, collapseId) {
           var e = document.getElementById(id);
           var v = document.getElementById(collapseId);

           if(e.style.display == 'none'){
              e.style.display = 'table';
              v.className = 'blockheader adminCollapsible adminExpanded';
              //[SG] 8/26 SFP-14
              document.cookie= "apex__Dashboard_Admin_Setup=true";
           }
           else {
              e.style.display = 'none';
              v.className = 'blockheader adminCollapsible';
              //[SG] 8/26 SFP-14
              document.cookie= "apex__Dashboard_Admin_Setup=false";
           }
        }
    </script>    
        
    <apex:pageMessages />
    <apex:form id="mainid">
        <div class="pageHeaderBar">
            <c:SchoolHeaderBar />
            <c:SchoolAcademicYearSelector host="{!Me}" value="{!academicYearId}" />
            <c:SchoolGlobalSearch />
        </div>
    </apex:form>

    <c:SchoolPickerLink />

    <div class="tablediv maindiv">
    <apex:form >
        <c:SchoolPageInstructions label_name="School_Instructions_Dashboard" field_name="SYSTEM_Hide_Instructions_Dashboard__c" />
    </apex:form>

    <table>
        <tr>
            <td class="maindiv" id="leftmaindiv" style="vertical-align: top;">
                <!-- Only render this section if there are actually announcements to display. -->
                <apex:outputPanel rendered="{!HasGlobalMessages}" layout="block" styleClass="block notifications">
                    <div class="blockheader">
                        School Portal News and Announcements
                    </div>
                    <div class="blockbody" style="padding-top: 10px; padding-bottom: 5px;">
                        <apex:outputPanel rendered="{!HasGlobalMessages}" layout="none">
                            <apex:repeat value="{!GlobalMessages}" var="gm">
                                <apex:outputField value="{!gm.Message__c}" />
                            </apex:repeat>
                        </apex:outputPanel>
                    </div>
                </apex:outputPanel>
                <!-- Do not render this out if basic version -->
                <apex:outputPanel rendered="{!NOT(basicVersion)}">
                    <div class="block aidallocation">
                        <div class="blockbody nopadding">
                            <div>
                                <div class="db-aidtext {!basicVersionClass}">Aid Allocation {!year}</div>
                                <div class="db-aiddata db-aidtext {!basicVersionClass}">
                                    <span class="big" id="stat_awardsGiven"></span>Awards Given
                                </div>
                                <div class="db-aiddata db-aidtext {!basicVersionClass}">
                                    <span class="big" id="stat_avgAward"></span>Average Award Amount
                                </div>
                            </div>
                        </div>
                    </div>
                </apex:outputPanel>
                <div class="block">
                    <c:SchoolDashboardFolderBars acadYearName="{!acadYear.Name}" />
                </div>
                <apex:outputPanel rendered="{!AND(hasBudgets, NOT(basicVersion), NOT(nonAdminUser))}" layout="none">
                    <div class="block admin-setup">
                        <div class="blockheader">
                            Budget Information {!year}
                        </div>
                        <div class="blockbody">
                            <c:SchoolDashboardBudgetPies schId="{!schoolId}" acadYearId="{!acadYear.Id}" />
                        </div>
                    </div>
                </apex:outputPanel>
            </td>
            <td class="rightdiv" style="vertical-align: top;">
                <!-- NAIS-2375 Subscription Info -->
                <div class="block numberhighlight">
                    <div class="blockheader">
                        <div>SSS Subscription Status</div>
                    </div>
                    <div class="blockbody nopadding">
                        <div>
                            <div></div>
                        </div>
                        <div>
                            <div>{!subscriptionType} Subscription current through &nbsp;
                                <apex:outputText value="{0,date,MM'/'dd'/'yyyy}">
                                    <apex:param value="{!subscriptionEndDate}" />
                                </apex:outputText>
                                <apex:outputPanel rendered="{!renderRenewalLink}">
                                    <br /><a id="renewal-link" href="{!$Page.SchoolRenewal}">Renew Now</a>
                                </apex:outputPanel>
                            </div>
                        </div>
                    </div>
                </div>
                <apex:outputPanel styleClass="block admin-setup" rendered="{!NOT(nonAdminUser)}" layout="block">
                    <a href="#" onclick="toggle_visibility('adminpanel', 'adminCollapsibleDiv');" id="expandLink">
                        <div class="blockheader adminCollapsible {!IF(isAdminExpanded,'adminExpanded','')}" id="adminCollapsibleDiv">
                            Admin Setup Progress
                            <apex:outputText value=" - {!adminStatus}" rendered="{!NOT(basicVersion)}" />
                        </div>
                    </a>
                    <div class="blockbody nopadding" id="adminpanel" style="display:{!IF(isAdminExpanded,'','none')}">
                        <!-- [SG] 8/26 SFP-14 -->
                        <!-- START FULL VERSION FOR ADMIN PANEL -->
                        <apex:outputPanel layout="none" rendered="{!NOT(basicVersion)}">
                            <div>
                                <div>
                                    <apex:image url="{!URLFOR($Resource.SchoolPortalStyles, '/images/new/confirm16.png')}" rendered="{!schoolProfileUpdated}" title="Update Made" />
                                    <apex:image url="{!URLFOR($Resource.SchoolPortalStyles, '/images/new/error16.png')}" rendered="{!NOT(schoolProfileUpdated)}" title="Update Not Made" />
                                </div>
                                <div>
                                    <apex:outputLink value="{!URLFOR($Page.SchoolSchoolProfile)}?">School Profile</apex:outputLink>
                                    <apex:outputPanel rendered="{!schoolProfileUpdated}">
                                        <br />Updated:
                                        <apex:outputText value="{0,date,MM'/'dd'/'yyyy}">
                                            <apex:param value="{!schoolProfileDate}" /></apex:outputText>
                                    </apex:outputPanel>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <apex:image url="{!URLFOR($Resource.SchoolPortalStyles, '/images/new/confirm16.png')}" rendered="{!genConfigUpdated}" title="Update Made" />
                                    <apex:image url="{!URLFOR($Resource.SchoolPortalStyles, '/images/new/error16.png')}" rendered="{!NOT(genConfigUpdated)}" title="Update Not Made" />
                                </div>
                                <div>
                                    <apex:outputLink value="{!URLFOR($Page.SchoolAnnualSettings)}?academicyearid={!acadYear.Id}">Annual Settings</apex:outputLink>
                                    <apex:outputPanel rendered="{!genConfigUpdated}">
                                        <br />Updated:
                                        <apex:outputText value="{0,date,MM'/'dd'/'yyyy}">
                                            <apex:param value="{!genConfigDate}" /></apex:outputText>
                                    </apex:outputPanel>
                                </div>
                            </div>
                            <apex:outputPanel rendered="{!displayReqDocs}" layout="none">
                                <div>
                                    <div>
                                        <apex:image url="{!URLFOR($Resource.SchoolPortalStyles, '/images/new/confirm16.png')}" rendered="{!reqDocsUpdated}" title="Update Made" />
                                        <apex:image url="{!URLFOR($Resource.SchoolPortalStyles, '/images/new/error16.png')}" rendered="{!NOT(reqDocsUpdated)}" title="Update Not Made" />
                                    </div>
                                    <div>
                                        <apex:outputLink value="{!URLFOR($Page.SchoolRequiredDocument)}?academicyearid={!acadYear.Id}">Required Documents</apex:outputLink>
                                        <apex:outputPanel rendered="{!reqDocsUpdated}">
                                            <br />Updated:
                                            <apex:outputText value="{0,date,MM'/'dd'/'yyyy}">
                                                <apex:param value="{!reqDocsDate}" /></apex:outputText>
                                        </apex:outputPanel>
                                    </div>
                                </div>
                            </apex:outputPanel>
                            <div>
                                <div>
                                    <apex:image url="{!URLFOR($Resource.SchoolPortalStyles, '/images/new/confirm16.png')}" rendered="{!tuitionFeesUpdated}" title="Update Made" />
                                    <apex:image url="{!URLFOR($Resource.SchoolPortalStyles, '/images/new/error16.png')}" rendered="{!NOT(tuitionFeesUpdated)}" title="Update Not Made" />
                                </div>
                                <div>
                                    <apex:outputLink value="{!URLFOR($Page.SchoolTuitionAndFees)}?cademicyearid={!acadYear.Id}">Tuition &amp; Fees</apex:outputLink>
                                    <apex:outputPanel rendered="{!tuitionFeesUpdated}">
                                        <br />Updated:
                                        <apex:outputText value="{0,date,MM'/'dd'/'yyyy}">
                                            <apex:param value="{!tuitionFeesDate}" /></apex:outputText>
                                    </apex:outputPanel>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <apex:image url="{!URLFOR($Resource.SchoolPortalStyles, '/images/new/confirm16.png')}" rendered="{!profJudgmentUpdated}" title="Update Made" />
                                    <apex:image url="{!URLFOR($Resource.SchoolPortalStyles, '/images/new/error16.png')}" rendered="{!NOT(profJudgmentUpdated)}" title="Update Not Made" />
                                </div>
                                <div>
                                    <apex:outputLink value="{!URLFOR($Page.SchoolGlobalProJudgment)}?academicyearid={!acadYear.Id}">Professional Judgment</apex:outputLink>
                                    <apex:outputPanel rendered="{!profJudgmentUpdated}">
                                        <br />Updated:
                                        <apex:outputText value="{0,date,MM'/'dd'/'yyyy}">
                                            <apex:param value="{!profJudgmentDate}" /></apex:outputText>
                                    </apex:outputPanel>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <apex:image url="{!URLFOR($Resource.SchoolPortalStyles, '/images/new/confirm16.png')}" rendered="{!budgetGroupsUpdated}" title="Update Made" />
                                    <apex:image url="{!URLFOR($Resource.SchoolPortalStyles, '/images/new/error16.png')}" rendered="{!NOT(budgetGroupsUpdated)}" title="Update Not Made" />
                                </div>
                                <div>
                                    <apex:outputLink value="{!URLFOR($Page.SchoolSchoolBudgeting)}?academicyearid={!acadYear.Id}">Budget Management</apex:outputLink>
                                    <apex:outputPanel rendered="{!budgetGroupsUpdated}">
                                        <br />Updated:
                                        <apex:outputText value="{0,date,MM'/'dd'/'yyyy}">
                                            <apex:param value="{!budgetGroupsDate}" /></apex:outputText>
                                    </apex:outputPanel>
                                </div>
                            </div>
                        </apex:outputPanel>
                        <!-- END FULL VERSION FOR ADMIN PANEL -->
                        <!-- START BASIC VERSION FOR ADMIN PANEL -->
                        <apex:outputPanel layout="none" rendered="{!basicVersion}">
                            <div>
                                <div>
                                </div>
                                <div>
                                    <apex:outputText >General Configuration</apex:outputText>
                                    <apex:outputPanel rendered="{!genConfigUpdated}">
                                        <br />
                                        <!--Updated: <apex:outputText value="{0,date,MM'/'dd'/'yyyy}"><apex:param value="{!genConfigDate}" /></apex:outputText>-->
                                    </apex:outputPanel>
                                </div>
                            </div>
                            <div>
                                <div>
                                </div>
                                <div>
                                    <apex:outputText >Tuition and Fees</apex:outputText>
                                    <apex:outputPanel rendered="{!tuitionFeesUpdated}">
                                        <br />
                                        <!--Updated: <apex:outputText value="{0,date,MM'/'dd'/'yyyy}"><apex:param value="{!tuitionFeesDate}" /></apex:outputText>-->
                                    </apex:outputPanel>
                                </div>
                            </div>
                            <div>
                                <div>
                                </div>
                                <div>
                                    <apex:outputText >Budget Management</apex:outputText>
                                    <apex:outputPanel rendered="{!budgetGroupsUpdated}">
                                        <br />
                                        <!--Updated: <apex:outputText value="{0,date,MM'/'dd'/'yyyy}"><apex:param value="{!budgetGroupsDate}" /></apex:outputText>-->
                                    </apex:outputPanel>
                                </div>
                            </div>
                            <div>
                                <div>
                                </div>
                                <div>
                                    <apex:outputText >Professional Judgment</apex:outputText>
                                    <apex:outputPanel rendered="{!profJudgmentUpdated}">
                                        <br />
                                        <!--Updated: <apex:outputText value="{0,date,MM'/'dd'/'yyyy}"><apex:param value="{!profJudgmentDate}" /></apex:outputText>-->
                                    </apex:outputPanel>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <apex:image url="{!URLFOR($Resource.SchoolPortalStyles, '/images/new/confirm16.png')}" rendered="{!schoolProfileUpdated}" title="Update Made" />
                                    <apex:image url="{!URLFOR($Resource.SchoolPortalStyles, '/images/new/error16.png')}" rendered="{!NOT(schoolProfileUpdated)}" title="Update Not Made" />
                                </div>
                                <div>
                                    <apex:outputLink value="{!URLFOR($Page.SchoolSchoolProfile)}">School Profile</apex:outputLink>
                                    <apex:outputPanel rendered="{!schoolProfileUpdated}">
                                        <br />Updated:
                                        <apex:outputText value="{0,date,MM'/'dd'/'yyyy}">
                                            <apex:param value="{!schoolProfileDate}" /></apex:outputText>
                                    </apex:outputPanel>
                                </div>
                            </div>
                        </apex:outputPanel>
                        <!-- END BASIC VERSION FOR ADMIN PANEL -->
                    </div>
                </apex:outputPanel>
                <!-- PFS Applications -->
                <div class="block numberhighlight three">
                    <div class="blockheader">
                        <div>PFS Applications {!year}</div>
                    </div>
                    <div class="blockbody nopadding">
                        <div>
                            <div id="stat_pfsPastWeek"></div>
                            <div id="stat_pfsPastDay"></div>
                            <div id="stat_pfsUpdatedPastDay"></div>
                        </div>
                        <div>
                            <!-- NAIS-1833 Start --> <!-- PFS-36 modified links -->
                            <div><a href="{!$Site.BaseUrl}/{!PFSRepReceivedWeek}?pv0={!schoolRepId}&pv1={!year}" target="_blank">Received within Past Week </a></div>
                            <div><a href="{!$Site.BaseUrl}/{!PFSRepReceivedDay}?pv0={!schoolRepId}&pv1={!year}" target="_blank">Received within Past Day</a></div>
                            <div><a href="{!$Site.BaseUrl}/{!PFSRepUpdatedDay}?pv0={!schoolRepId}&pv1={!year}" target="_blank">Updated in Last 24 Hours</a></div>
                            <!-- NAIS-1833 End -->
                        </div>
                    </div>
                </div>
                <!--NAIS-1738 Grey this out if basic version -->
                <apex:outputPanel rendered="{!NOT(basicVersion)}">
                    <!-- <apex:outputPanel > -->
                    <div class="block numberhighlight three">
                        <div class="blockheader {!basicVersionClass}">
                            <div>Messages</div>
                        </div>
                        <div class="blockbody nopadding {!basicVersionClass}">
                            <div>
                                <div>{!msgsToday}</div>
                                <div>{!msgsThisWeek}</div>
                                <div>{!openCases}</div>
                            </div>
                            <div>
                                <!-- NAIS-1960 Start -->
                                <div>New/Updated within Past Day</div>
                                <div>New/Updated within Past Week</div>
                                <!-- SFP-36 -->                                
                                <div><a href="{!$Page.SchoolListMessage}" target="_blank">All Open</a></div>
                                <!-- END SFP-36 -->                                
                                <!-- NAIS-1960 End -->
                            </div>
                        </div>
                    </div>
                    <!-- NAIS-1973 -->
                    <div class="block numberhighlight three">
                        <div class="blockheader {!basicVersionClass}">
                            <div>Support Tickets</div>
                        </div>
                        <div class="blockbody nopadding {!basicVersionClass}">
                            <div>
                                <div>{!ticketsToday}</div>
                                <div>{!ticketsThisWeek}</div>
                                <div>{!openTickets}</div>
                            </div>
                            <div>
                                <!-- NAIS-1960 Start -->
                                <div>New/Updated within Past Day</div>
                                <div>New/Updated within Past Week</div>
                                <!-- SFP-36 -->
                                <div><a href="{!$Page.SchoolSupportTicket}" target="_blank">All Open</a></div>
                                <!-- END SFP-36 -->
                                <!-- NAIS-1960 End -->
                            </div>
                        </div>
                    </div>
                    <!-- /NAIS-1738 Grey this out if basic version -->
                    <div class="block">
                        <c:SchoolFeeWaiverStats annualSetting="{!annSet}" renderlink="{!NOT(nonAdminUser)}" renderDashboardView="true" basicversionclass="{!basicVersionClass}" />
                    </div>
                </apex:outputPanel>
            </td>
        </tr>
    </table>
</div>

</apex:page>