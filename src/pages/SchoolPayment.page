<apex:page controller="SchoolPaymentController" sidebar="false" showheader="false" action="{!initMethod}">
    <head>

        <apex:stylesheet value="{!URLFOR($Resource.SchoolPortalStyles, '/css/newStyles.css')}"/>

        <style>
            #mainCol {margin-left:40px}
            #paymentTotal {font-size: 24px; text-align:center;}
            #waiverInfo {margin-left:80px; margin-top:16px; margin-bottom:16px; display:block;}
            #processingPayment {font-weight:bold; text-align:center;}
            .paymentTable {width: 100%; margin-bottom:16px}
            .paymentTableLabelCol {width: 18%; text-align:right; vertical-align:top; font-weight:bold; padding-right:15px;}
            .paymentTableHeading {font-weight:bold}
            .paymentTableDataCol {width: 32%; vertical-align:top;}
            .paymentButtonTable {width:40%;margin-left:30%; margin-right:30%}
            .paymentButtonTableLeft {text-align:left;}
            .paymentButtonTableRight {text-align:right; }
            .paymentButtonMessage {text-align:center; margin-top:10px;}
            .paymentFieldError {display:block; color: rgb(204, 0, 0); margin-bottom:3px}
            .paymentFieldRequired {background-color: rgb(255, 221, 221);}
        </style>
        <script>
            function disableSubmit() {
                var buttons = document.querySelectorAll('.processPaymentButton');
                for (var i = 0, l = buttons.length; i < l; i++) {
                    buttons[i].setAttribute('disabled', 'true');
                    buttons[i].style.color = 'grey';
                }
            }
        </script>

        <apex:includeScript value="{!$Resource.Jquery191min}" />
        <apex:includeScript value="{!$Resource.DetectCard}" />
        <apex:includeScript value="{!$Resource.UpdateCardType}" />
        <apex:includeScript value="{!$Resource.inputValidationsForPaymentInputs}" />
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery(".continueSelect").one('click', function (event) {
                    event.preventDefault();
                    continueAction();
                    jQuery(this).prop('disabled', true);
                });
            });
            
            
        </script>

    </head>
    <body class="portalPage withPageBackground">

        <c:SSSSiteLogo />

        <apex:form >

            <apex:pageBlock >
                <div id="pageBackground">
                    <div id="mainCol">
                                <apex:outputLabel value="TEST MODE" rendered="{!isTestMode}" style="color:red"/>
                        <div id="mainColContent">

                            <apex:outputPanel rendered="{!pageError}">
                                 <apex:pageMessage summary="{!errorMessage}" severity="error" strength="3" />
                            </apex:outputPanel>
                            <br/>

                            <apex:outputPanel rendered="{!(paymentStep=='PAYMENT_SELECT')}">
                                <c:SchoolPaymentSelect mainController="{!Me}"/>
                                <table class="paymentButtonTable">
                                    <tr>
                                        <td class="paymentButtonTableLeft">
                                            <apex:commandButton value="Cancel" action="{!actionCancel}"/>
                                        </td>
                                        <td class="paymentButtonTableRight">
                                            <apex:commandButton value="Continue >" action="{!actionPaymentForm}"/>
                                        </td>
                                    </tr>
                                </table>

                            </apex:outputPanel>

                            <apex:outputPanel rendered="{!(paymentStep=='PAYMENT_FORM')}" id="paymentform">
                                <apex:outputPanel rendered="{!paymentData.isCreditCardPaymentType}">
                                    <c:SchoolPaymentCreditCardForm mainController="{!Me}"/>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!paymentData.isECheckPaymentType}">
                                    <c:SchoolPaymentECheckForm mainController="{!Me}" id="echeckcomp"/>
                                </apex:outputPanel>
                                <table class="paymentButtonTable">
                                    <tr>
                                        <td class="paymentButtonTableLeft">
                                            <apex:commandButton value="< Back" action="{!actionPaymentSelect}"/>
                                            <apex:commandButton value="Cancel" action="{!actionCancel}"/>
                                        </td>
                                        <td class="paymentButtonTableRight">
                                            <apex:commandButton value="Continue >" rerender="" action="{!actionPaymentConfirm}"/>
                                        </td>
                                    </tr>
                                </table>

                                <div class="paymentButtonMessage">
                                    <b>You will be able to review and confirm on the next page before submitting your payment. </b>
                                </div>
                                <br /><br /><br />
                            </apex:outputPanel>

                            <apex:outputPanel rendered="{!(paymentStep=='PAYMENT_CONFIRM')}">

                                <apex:outputPanel rendered="{!paymentData.isCreditCardPaymentType}">
                                    <c:SchoolPaymentCreditCardForm mainController="{!Me}" readOnly="true"/>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!paymentData.isECheckPaymentType}">
                                    <c:SchoolPaymentECheckForm mainController="{!Me}" readOnly="true"/>
                                </apex:outputPanel>

                                <div id="submitButtons">
                                    <table class="paymentButtonTable">
                                        <tr>
                                            <td class="paymentButtonTableLeft">
                                                 <apex:commandButton value="< Back" action="{!actionPaymentForm}"/>
                                                 <apex:commandButton value="Cancel" action="{!actionCancel}"/>
                                            </td>
                                            <td class="paymentButtonTableRight">
                                                <apex:commandButton value="Process Payment" action="{!actionPaymentSubmit}" onclick="document.getElementById('submitButtons').style.display='none';document.getElementById('processingPayment').style.display='block';return true;"/>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="processingPayment" style="display:none">
                                    Processing Payment...
                                </div>

                            </apex:outputPanel>

                            <apex:outputPanel rendered="{!(paymentStep=='PAYMENT_INVOICE')}">
                                <c:SchoolPaymentInvoice mainController="{!Me}"/>
                                <table class="paymentButtonTable">
                                    <tr>
                                        <td class="paymentButtonTableLeft">
                                            <apex:commandButton value="< Back" action="{!actionPaymentSelect}"/>
                                            <apex:commandButton value="Cancel" action="{!actionCancel}"/>
                                        </td>
                                        <td class="paymentButtonTableRight">
                                            <apex:commandButton value="Done" action="{!actionInvoiceDone}"/>
                                        </td>
                                    </tr>
                                </table>

                            </apex:outputPanel>

                        </div>
                    </div>
                </div>
            </apex:pageBlock>
        </apex:form>
    </body>
</apex:page>