<apex:page controller="FamilyTemplateController" extensions="FamilyHelpSearchResultsController"  sideBar="false" showHeader="false">
    <apex:composition template="{!template}">
        <apex:define name="header">
            <apex:stylesheet value="{!URLFOR($Resource.SchoolPortalStyles_Community, '/css/dashboard-inline.css')}"/>
            <apex:stylesheet value="{!URLFOR($Resource.SchoolPortalStyles_Community, '/css/newStyles.css')}"/>
            <apex:stylesheet value="{!URLFOR($Resource.FamilyDocumentsCSS)}" />
            <apex:stylesheet value="{!URLFOR($Resource.FamilyHelpCenterCSS)}" />
            <apex:includeScript value="{!$Resource.Jquery1113min}" />
            <script src="{!URLFOR($Resource.FamilyDocumentsJS)}" ></script>
           
        </apex:define>
        <apex:define name="body">
            <apex:form id="resultsForm">
                <div class="searchResults__container" >

                    <apex:outputPanel rendered="{!IF(totalRecords == 0, true, false)}">
                        <div class="searchResults__noResults"> 
                            <div>
                                <p>Your search - <strong> {!searchString} </strong> - did not match any documents. </p>
                                <p class="searchResults__noResults--margin">Suggestions:</p>
                                <ul>
                                    <li>Make sure all words are spelled correctly.</li>
                                    <li>Try different keywords.</li>
                                    <li>Try more general keywords</li>

                                </ul>
                            </div>
                        </div>
                    </apex:outputPanel>

                    <apex:outputPanel rendered="{!IF(totalRecords > 0, true, false)}">
                        <div class="searchResults__header">
                            
                            <div class="searchResults__total">
                                Results <strong>{!displayResults}</strong> of <strong>{!totalRecords}</strong>
                            </div>

                            <div class="searchResults__pagination">
                                <apex:commandLink action="{!previous}" value="< Previous " styleClass="{!IF(prevRequired = false,'searchResults__pagination--inactive','')}" />
                                <span>|</span>
                                <apex:repeat value="{!pageList}" var="pageNum" first="0" id="theRepeat">
                                    <apex:commandLink action="{!skipToPage}" value="{!pageNum}" styleClass="searchResults__pageLinks" style="{!IF(pageNum == TEXT(currentPageNumber),'font-weight: bold; pointer-events: none','font-weight: 100')}">
                                        <apex:param name="jumpToPage" value="{!pageNum}" assignTo="{!jumpToPage}"/>
                                    </apex:commandLink>
                                </apex:repeat>

                                <apex:outputText rendered="{!IF(totalPages > 5, true, false)}">
                                ...
                                    <apex:commandLink action="{!skipToPage}" value="{!totalPages}" styleClass="searchResults__pageLinks" style="{!IF(totalPages == currentPageNumber,'font-weight: bold','font-weight: 100')}">
                                        <apex:param name="jumpToPage" value="{!totalPages}" assignTo="{!jumpToPage}"/>
                                    </apex:commandLink>
                                </apex:outputText>
                                <span>|</span>
                                <apex:commandLink action="{!next}" value=" Next >" styleClass="{!IF(nextRequired = false,'searchResults__pagination--inactive','')}"/>
                            </div>
                        </div>
                        <div class="searchResults__body">
                            <apex:repeat value="{!articles}" var="article" id="theRepeat1">
                                <div class="searchResults__item">
                                    <div class="searchResults__item--title">
                                        <apex:outputLink value="/FamilyHelpResultsDetail?articleId={!article.Id}&searchq={!searchString}&retUrl={!returnUrl}"> {!article.Title} </apex:outputLink>
                                    </div>

                                    <div class="searchResults__item--description">
                                        <apex:outputText value="{!article.Summary}" escape="false"/>
                                    </div>

                                    <div class="searchResults__item--footer">
                                        Created 
                                        <apex:outputText value="{0, date, MM'/'d'/'yyyy}">
                                            <apex:param value="{!article.LastPublishedDate}" /> 
                                        </apex:outputText>
                                    </div>
                                </div>
                            </apex:repeat>
                        </div>
                    </apex:outputPanel>
                </div>           
            
            </apex:form>
            <c:FamilyPortalContactPanel pfsId="{!pfsId}"/>
        </apex:define>
    </apex:composition>
</apex:page>