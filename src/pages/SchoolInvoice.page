<apex:page title="School Invoice"
           renderAs="pdf"
           showheader="false"
           standardStylesheets="false"
           Controller="SchoolInvoiceController"
           cache="false"
           sideBar="false"
           applyBodyTag="false"
           applyHtmlTag="false"
           action="{!applyInvoiceDateTime}">

<html>
    <head>
        <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE" />
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE" />
        <META HTTP-EQUIV="EXPIRES" CONTENT="0" />
        <style>
            @page{
                margin: 0.5in;
                size: letter;

                @bottom-left {
                    content: "Page " counter(page);
                }

                @top-right {
                    content: "Invoice"
                }
            }
            @page :first {
                @bottom-left { content: normal; }
                @top-right { content: normal; }
                }
            }
        </style>
        <title>School Invoice</title>
    </head>

    <body style="font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size:.8em">

        <div> <!-- Header Row -->
            <div style="display:inline-block">
                <h1 title="School & Student Services"><apex:image id="Logo" value="{!$Resource.SSSLogoMain}" /></h1>
                <h2 style="font-size: 1em; font-style: italic;">Helping schools help families for over 40 years</h2>
            </div>
            <div style="display:inline-block; float:right">
                <h1 style="color: #7259CE; text-align: right;">INVOICE</h1>
            </div>
        </div> <!-- /Header Row -->

        <div> <!-- Address Row -->
            <div style="display:inline-block">
                <p><apex:outputText escape="false" style="white-space:pre;" value="{!SUBSTITUTE($Label.SSS_Physical_Address,'\\\n','<br/>')}" /></p>
                <p>
                    <span style="font-weight:bold; color: #7259CE;">TO:</span><br />
                    <!-- NAIS-2376 remove contact name
                    <apex:outputText value="{!toFullName}" /><br />
                    -->
                    <apex:outputText value="{!toSchoolName}" /><br />
                    <apex:outputText value="{!toStreet}" /><br />
                    <apex:outputText value="{!toCity}" />,&nbsp;<apex:outputText value="{!toState}" />&nbsp;<apex:outputText value="{!toPostalCode}" /><br />
                    <apex:outputText value="{!toPhone}" />&nbsp;
                    <!-- NAIS-2376 remove contact email
                    |&nbsp;<apex:outputText value="{!toEmail}" />
                    -->
                </p>
            </div>
            <div style="display:inline-block; float:right;">
                <p style="text-align:right">
                    <span style="font-weight:bold; color: #7259CE;">DATE:</span>&nbsp;
                    <apex:outputText value="{!currentDate}" /><br />
                    <span style="font-weight:bold; color: #7259CE;">FOR:</span>&nbsp;
                    Subscription Fee&nbsp;<apex:outputText value="{!academicYear}" /><br />
                    <span style="font-weight:bold; color: #7259CE;display:{!IF(NOT(ISNULL(subscriptionDates)),'inline','none')};">SUB DATES:</span>
                    &nbsp;<apex:outputText rendered="{!NOT(ISNULL(subscriptionDates))}" value="{!subscriptionDates}" />
                </p>
            </div>
        </div> <!-- /Address Row -->
        <div>
            SSS Code # {!schoolCode}
        </div>
        <div style="min-height:28em; margin-top:2em"> <!-- Table Row -->
            <table style="width:100%; border-top: 1px solid #9CC2E5;">
                <tr style="color: #7259CE; text-align:left;">
                    <th style="width:10%; border-bottom: 1px double #9CC2E5; padding: 3px 0px;">DATE</th>
                    <th style="width:45%; border-bottom: 1px double #9CC2E5; padding: 3px 0px;">DESCRIPTION</th>
                    <th style="width:15%; border-bottom: 1px double #9CC2E5; padding: 3px 0px; text-align:right;">AMOUNT</th>
                    <th style="width:15%; border-bottom: 1px double #9CC2E5; padding: 3px 0px; text-align:right;">DISCOUNT</th>
                    <th style="width:15%; border-bottom: 1px double #9CC2E5; padding: 3px 0px; text-align:right;">TOTAL</th>
                </tr>
                <tr>
                    <td style="padding: 5px 0px; border-bottom: 1px solid #9CC2E5;">
                        <apex:outputText value="{!oppDate}" />
                    </td>
                    <td style="padding: 5px 0px; border-bottom: 1px solid #9CC2E5;">
                        <apex:outputText value="{!description}" />
                    </td>
                    <td style="text-align:right; padding: 5px 0px; border-bottom: 1px solid #9CC2E5; ">
                        <apex:outputText value="${!totalAmount}" />
                    </td>
                    <td style="text-align:right; padding: 5px 0px; border-bottom: 1px solid #9CC2E5; ">
                        <apex:outputText value="${!subDiscount}" />
                    </td>
                    <td style="text-align:right; padding: 5px 0px; border-bottom: 1px solid #9CC2E5; ">
                        <apex:outputText value="${!netDueInvoice}" />
                    </td>
                </tr>
            </table>

            <div style="width:100%; padding: 5px 0px; border-top: 1px solid #9CC2E5; border-bottom:1px double #9CC2E5"> <!-- Summary -->
                <div style="width: 70%; display:inline-block; font-weight:bold">
                    Net due&nbsp;<apex:outputText value="(credit balance)" rendered="{!credit == true}" style="font-weight:bold"/>
                </div>
                <div style="width: 29%; display:inline-block; text-align:right">
                    <apex:outputText value="${!netDueInvoice}" style="font-weight:bold"/>
                </div>
            </div> <!-- /Summary -->
        </div> <!-- /Table Row -->

        <div> <!-- Footer Row -->
            <p>
                Pay by Phone: {!$Label.SSS_Phone_Number}<br />
                Make checks payable to SSS and mail to:<br />
                <div style="margin-left:1em; margin-bottom:1.1em;">
                   <apex:outputText escape="false" style="white-space:pre;" value="{!SUBSTITUTE($Label.SSS_Check_Mailing_Address,'\\\n','<br/>')}" />
                </div>
                Payment is required to access the SSS School Portal.<br />
                If you have any questions concerning this invoice, contact SSS Client Success Team at {!$Label.SSS_Phone_Number}.
            </p>
            <p style="color: #7259CE; text-align:center;    font-size:1.1em; margin-bottom:0px">THANK YOU FOR USING SSS!</p>
        </div> <!-- /Footer Row -->
    </body>
</html>

</apex:page>