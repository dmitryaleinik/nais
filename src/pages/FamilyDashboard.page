<apex:page controller="FamilyTemplateController" extensions="FamilyDashboardController" sideBar="false" showHeader="false" action="{!pageLoad}">
	<apex:composition template="{!template}">
		<apex:define name="header">
			<apex:includeScript value="{!$Resource.Jquery1113min}" />
			<apex:stylesheet value="{!URLFOR($Resource.SchoolPortalStyles, '/css/familyPortalDashboard.css')}"/>
			<apex:includeScript value="{!$Resource.FamilyDocumentsJS}"/>
			<apex:stylesheet value="{!$Resource.FamilyDocumentsCSS}" />
			<apex:stylesheet value="{!$Resource.FamilyPortalDashboardCSS}" />
			<apex:includeScript value="{!$Resource.FamilyPortalDashboardJS}"/>
            <apex:includeScript value="{!$Resource.JQModal}"/>
			
			<script type="text/javascript">
				var $j = jQuery.noConflict();

				/* init on document ready */
				$j( document).ready( function() {
					familyDashboardJS.initDashboard();
                    
                    $j('a').each(function(){
                        var thisUrl = $j(this).prop('href');
                        if( thisUrl!=null && thisUrl.includes('isdtp') && thisUrl.includes('javascript:srcUp'))
                        {
                            var decodeURL = decodeURIComponent(decodeURIComponent(thisUrl));
                            decodeURL = decodeURL.replace('javascript:srcUp(\'','');
                            decodeURL = decodeURL.replace(/\?/g,'&');
                            decodeURL = decodeURL.replace('&','?');
                            decodeURL = decodeURL.replace('\');','');
                            $j(this).prop('href', decodeURL);
                         }
                    });
                    
                    if ({!questionAreAnswered} === false && {!RenderSecurityQuestions} === true) {
                        showSecurityQuestionsPopup();
                    }
				});
			</script>
			<meta http-equiv="X-UA-Compatible" content="IE=10" />
		</apex:define>

		<apex:define name="body">
			<c:FamilyDocumentSpringUpload pfsRecord="{!pfsRecord}" academicYearId="{!academicYearId}" documentSource="Uploaded by Parent" />
			<apex:form id="mainForm">
			    <apex:pageBlock >
			    	<div id="pageBackground">

				        <apex:outputPanel id="main" layout="block" rendered="{!IF(pfsId != null, 'true', 'false')}">

				            <div id="sidebar">
				                <div class="verticalTab open"><img src="{!URLFOR($Resource.SchoolPortalStyles, '/images/new/vTab.png')}"/></div>
				                <div id="pfsChecklist">
				                    <div>
				                        <apex:outputPanel layout="block" styleClass="inner" id="family-nav-bar-container">
				                            <c:FamilyNavigationBar hiddenFields="{!hiddenFields}" currentPFS="{!pfsRecord}" selectedScreen="null" applicationUtility="{!appUtils}"/>
				                        </apex:outputPanel>
				                        <apex:outputPanel layout="block" styleClass="submitpfs">
				                            <apex:commandButton styleClass="btn secondary {!IF(OR(NOT(appUtils.applicationIsComplete), appUtils.isSubmittedAndPaid), 'disabled', '')}"
				                                value="PAY AND SUBMIT THIS PFS"
				                                action="{!submitPFS}"
				                                disabled="{!OR(NOT(appUtils.applicationIsComplete), appUtils.isSubmittedAndPaid)}"
				                                title="{!IF(appUtils.applicationIsComplete, IF(appUtils.isSubmittedAndPaid, 'PFS is already Submitted and Paid', ''),'All application sections must be complete.')}"/>
				                        </apex:outputPanel>
				                    </div>
				                </div>
				                <div class="block pfsHistory">
				                    <div class="blockheader">
				                        Your PFS History
				                    </div>
				                    <div class="blockbody">
				                        <apex:repeat value="{!pfsHistories}" var="pfsH">
				                            <span class="pfsHistoryName">
				                                <apex:outputLink target="_printPfs" value="{!pfsH.PFSPrintURL}">{!pfsH.PFSName}</apex:outputLink>
				                            </span>
				                            <span class="pfsHistoryURL">
				                                <apex:outputLink target="_printPfs" value="{!pfsH.PFSPrintURL}">Print</apex:outputLink>
				                            </span><br/>
				                        </apex:repeat>
				                    </div>
				                </div>

				                <apex:outputPanel layout="none" rendered="{!renderFamilyReport}">
				                    <div class="block pfsHistory">
				                        <div class="blockheader" style="text-align:center;">
				                            <apex:outputlink target="_blank" value="{!URLFOR($Page.FamilyReport)}?id={!pfsId}">View Family Report</apex:outputlink>
				                        </div>
				                    </div>
				                </apex:outputPanel>
				            </div>

				            <div id="mainCol">
				                <apex:outputPanel id="integrationPanel">
				                	<c:IntegrationSource pfsRecord="{!pfsRecord}" academicYearId="{!currentAcademicYear.Id}"/>
				                </apex:outputPanel>

				                <div id="mainColumnHeader" >
				                    <h2>Welcome to Your {!currentAcademicYear.Name} Family Portal Dashboard</h2>
				                </div>
				                <div id="mainColContent">
				                    <ul class="processStates">

				                        <li>
				                            <a href="{!currentAcademicYear.PFS_Workbook_Url__c}" target="_blank" class="stateIcon prepare" style="text-decoration: none;">Download the PFS Workbook</a>
				                        </li>

				                        <li class="{!IF(AND(NOT(AllSectionsDone), PFSSTAGE = 'COMPLETING'), 'active', '')}">
				                            <a class="stateIcon complete">Complete your pfs</a>
				                        </li>

				                        <li class="{!IF(OR(PFSSTAGE = 'PAY', AND(AllSectionsDone, PFSSTAGE = 'COMPLETING')), 'active', '')}">
				                        	<apex:outputPanel rendered="{!OR(PFSSTAGE = 'PAY', AND(AllSectionsDone, PFSSTAGE = 'COMPLETING'))}">
				                        		<a class="stateIcon submit submitpfs"><apex:commandButton styleClass="btn secondary stage" value="PAY & SUBMIT" action="{!submitPFS}" rendered="{!NOT(feeIsWaived)}"/></a>
				                        	</apex:outputPanel>
				                        	<apex:outputPanel rendered="{!NOT(OR(PFSSTAGE = 'PAY', AND(AllSectionsDone, PFSSTAGE = 'COMPLETING')))}">
				                        		 <a class="stateIcon submit">Pay and submit</a>
				                        	</apex:outputPanel>
				                        </li>

				                        <li class="{!IF(PFSSTAGE = 'DOCS', 'active', '')}" >
                                            <apex:outputPanel rendered="{!IF(PFSSTAGE = 'DOCS', true, false)}">
				                                <a href="{!URLFOR($Page.FamilyDocuments)}?id={!pfsId}&academicyearid={!academicYearId}" class="stateIcon manage">Manage documents<br />or update your pfs</a>
                                            </apex:outputPanel>
                                            <apex:outputPanel rendered="{!IF(PFSSTAGE != 'DOCS', true, false)}">
                                                <a class="stateIcon manage">Manage documents<br />or update your pfs</a>
                                            </apex:outputPanel>
				                        </li>

				                        <li class="instructions dashboard__instructions">
					                            <apex:outputText rendered="{!AND(NOT(AllSectionsDone), PFSSTAGE = 'COMPLETING')}">
					                            	<div class="dashboard__instructions__detail">
				                                		Last section completed:&nbsp;<apex:outputLink value="{!LastSectionURL}" >{!LastSectionName}</apex:outputLink>
				                              		</div>
					                            </apex:outputText>
				
				                            <apex:outputText rendered="{!OR(PFSSTAGE = 'PAY', AND(AllSectionsDone, PFSSTAGE = 'COMPLETING'))}" >
				                            	<div class="dashboard__instructions__detail">
				                                	<apex:outputLink value="{!URLFOR($Page.FamilyApplicationMain)}?id={!pfsId}&academicyearid={!academicYearId}&screen=PFSSubmit" >Pay &amp; Submit</apex:outputLink> your PFS.
				                                </div>
				                                <div id="feeBar" class="{!IF(WaiverRequestedButNotYetWaived, 'feewaiverrequested', (IF(OR(FeeIsWaived, PFSSTAGE = 'DOCS'), 'feepaid', 'feeunpaid')))}">
							                        <span class="{!IF(AND(PFSSTAGE = 'PAY', NOT(IsPaid), NOT(WaiverRequestedButNotYetWaived)), '', '')}">
							                            Application Fee:
							                            <span id="feeAmount" class="{!IF(FeeIsWaived,'strikefee','')}" >
							                                <strong> 
                                                                <apex:outputText value="{0, number, $##,##0.00}">
							                                         <apex:param value="{!fee}"/>
							                                	</apex:outputText>
                                                            </strong>
							                            </span>
							                            <apex:outputText rendered="{!WaiverRequestedButNotYetWaived}">
							                                (waiver requested)
							                            </apex:outputText>
							                        </span>
							                        <apex:outputText rendered="{!FeeIsWaived}" value="WAIVED" styleclass="paidOrWaived"/>
							                        <apex:outputText rendered="{!AND(NOT(FeeIsWaived), PFSSTAGE = 'DOCS')}" value="PAID" styleclass="paidOrWaived"/>

							                        <apex:outputPanel rendered="{!NOT(ISNULL(lastPaymentTransactionLineItemId))}">
							                            <a href="#" onclick="printReceipt('{!lastPaymentTransactionLineItemId}'); return false;" style="margin-left:20px;color:#FFFFFF; ">Print Payment Receipt</a>
							                        </apex:outputPanel>

							                    </div>
				                            </apex:outputText>

				                            <apex:outputText styleClass="dashboard__instructions" rendered="{!PFSSTAGE = 'DOCS'}">
				                            	<div class="dashboard__instructions__detail">
					                                You can now &nbsp;
					                                <apex:outputLink value="{!$Page.FamilyDocuments}?academicyearid={!academicYearId}&id={!pfsId}" >Manage and Upload Required Documents</apex:outputLink> 
					                                for your application or &nbsp;
					                                <apex:outputLink value="{!URLFOR($Page.FamilyApplicationMain)}?id={!pfsId}&academicyearid={!academicYearId}&screen=ParentsGuardians" >Update and Make Changes to your PFS</apex:outputLink>.
				                                </div>
				                                <div id="feeBar" class="{!IF(WaiverRequestedButNotYetWaived, 'feewaiverrequested', (IF(OR(FeeIsWaived, PFSSTAGE = 'DOCS'), 'feepaid', 'feeunpaid')))}">
							                        <span class="{!IF(AND(PFSSTAGE = 'PAY', NOT(IsPaid), NOT(WaiverRequestedButNotYetWaived)), '', '')}">
							                            Application Fee:
							                            <span id="feeAmount" class="{!IF(FeeIsWaived,'strikefee','')}" >
							                                 <strong> 
                                                                <apex:outputText value="{0, number, $##,##0.00}">
                                                                     <apex:param value="{!fee}"/>
                                                                </apex:outputText>
                                                            </strong>
							                            </span>
							                            <apex:outputText rendered="{!WaiverRequestedButNotYetWaived}">
							                                (waiver requested)
							                            </apex:outputText>
							                        </span>
							                        <apex:outputText rendered="{!FeeIsWaived}" value="WAIVED" styleclass="paidOrWaived"/>
							                        <apex:outputText rendered="{!AND(NOT(FeeIsWaived), PFSSTAGE = 'DOCS')}" value="PAID" styleclass="paidOrWaived"/>

							                        <apex:outputPanel rendered="{!NOT(ISNULL(lastPaymentTransactionLineItemId))}">
							                            <a href="#" onclick="printReceipt('{!lastPaymentTransactionLineItemId}'); return false;" style="margin-left:20px;color:#FFFFFF; ">Print Payment Receipt</a>
							                        </apex:outputPanel>

							                    </div>
				                            </apex:outputText>
				                        </li>
				                    </ul>

				                    <apex:outputPanel rendered="{!AND(NOT(appUtils.applicationIsComplete), appUtils.isSubmittedAndPaid)}">
				                    	<div id="additionalInformationneeded" class="block">
				                    		<div class="blockheader blockheader--dark withByline" > You've made updates to your application that require additional information </div>
				                    		<div class="list detailList fpTable updatelist--flex">
				                    			<apex:repeat var="item" value="{!nextOutStandingItems}">
					                    			<div class="updatelist__item updatelist--flex ">
					                    				<apex:image url="{!URLFOR($Resource.SchoolPortalStyles, '/images/attention.png')}" />
					                    				<div class="updatelist__item--outstanding"> {!item} </div>
					                    			</div>
				                    			</apex:repeat>
				                    		</div>
				                    	</div>
				                    </apex:outputPanel>
				                    

				                    <div id="dashSelectedSchools" class="block">
				                        <div class="blockheader blockheader--dark withByline" > Selected Schools </div>
				                        <apex:outputPanel layout="block" rendered="{!schPFSWrappers.size != 0}" styleClass="blockbody nopadding">
				                            <table class="list detailList fpTable">
				                                <thead>
				                                    <tr>
				                                        <th class="statColumn"> Status </th>
				                                        <th> SSS Code </th>
				                                        <th> School Name </th>
				                                        <th> Applicant </th>
				                                        <th> PFS Deadline </th>
				                                    </tr>
				                                </thead>
				                                <tbody>
				                                    <apex:repeat value="{!schPFSWrappers}" var="spfs">
				                                        <tr class="dashSelectRow {!IF(NOT(spfs.isSubscribed), 'unsubscribedbgcolor ', '')} {!IF(AND(spfs.isSubscribed, spfs.deadlinePassed), ' deadlinepassedbgcolor ', '')}">
				                                            <td class="dashSelectCell dashSelectCellStatus selectedSchools--item">
				                                            	<apex:outputPanel rendered="{!spfs.isLocked}">
				                                            		<div class="selectedSchools--popup"> <strong>This PFS is locked. </strong> You will not be able to submit updates. </div>
				                                            	</apex:outputPanel>
				                                                <span class="icon-upload {!IF(NOT(spfs.isSubscribed), 'unsubscribed', IF(spfs.isLocked, 'locked', 'unlocked'))}"
				                                                      title="{!IF(NOT(spfs.isSubscribed), 'unsubscribed', IF(spfs.isLocked, 'locked', 'unlocked'))}"></span>
				                                            </td>
				                                            <td class="dashSelectCell dashSelectCellCode">
				                                                {!spfs.sssCode}
				                                            </td>
				                                            <td class="dashSelectCell dashSelectCellSchool">
				                                                {!spfs.schoolName}
				                                                <apex:outputPanel layout="none" rendered="{!NOT(spfs.isSubscribed)}">
				                                                    <span class="unsubscribedwarning">
				                                                        <br />
				                                                        No longer subscribed
				                                                    </span>
				                                                    <br />

				                                                </apex:outputPanel>
				                                            </td>
				                                            <td class="dashSelectCell dashSelectCellApplicant">
				                                                {!spfs.applicantName}
				                                            </td>
				                                            <td class="dashSelectCell dashSelectCellDeadline">
				                                                <apex:outputText value="{0,date,MM'/'dd'/'yyyy}" >
				                                                	<apex:param value="{!IF(spfs.isReturning, spfs.returningDeadline, spfs.newDeadline)}" />
				                                                </apex:outputText>
				                                            </td>
				                                        </tr>

				                                    </apex:repeat>
				                                </tbody>
				                            </table>
				                        </apex:outputPanel>
				                        <div class="selectedSchools__description">
				                        	<apex:outputPanel rendered="{!schPFSWrappers.size != 0}" > Once a school has locked your PFS you will not be able to submit updates to the school for that PFS. </apex:outputPanel>
				                            <apex:outputPanel rendered="{!schPFSWrappers.size = 0}" > You have not yet selected any subscriber schools </apex:outputPanel>
				                        </div>
				                    </div>

				                    <c:DocumentDueDates pfsRecord="{!pfsRecord}" displayHeader="true" headerStyle="blockheader--dark"/>

				                    <div class="collapsible-section">
				                    	<div class="family-documents-collapsible">
			                                <h2>
			                                    <span class="family-documents_heading__flex-item-first"><div style="display: inline-block; width: 2em"><span class="arrow arrow-right"> </span></div> FAQs</span>
			                                </h2>
			                                <div class="family-documents-body closed">
			                                    <apex:repeat value="{!faqs}" var="faq">
			                                        <div class="faq">
			                                            <div class="faq__header">
                                                            <apex:outputLink target="_blank" value="{!URLFOR($Page.FamilyHelpResultsDetail)}?articleId={!faq.knowledgeArticle.KnowledgeArticleId}&retUrl=/">{!faq.knowledgeArticle.title}</apex:outputLink>
			                                            </div>
			                                            <div class="faq__body">
			                                                <apex:outputText escape="false" value="{!faq.abbreviatedBody}" />
                                                            <apex:outputPanel styleClass="faq__more" rendered="{!faq.hasAbbreviatedBody}">
                                                                <apex:outputLink styleClass="faq__more--link" style="padding-left: .5em" target="_blank" value="{!URLFOR($Page.FamilyHelpResultsDetail)}?articleId={!faq.knowledgeArticle.KnowledgeArticleId}&retUrl=/">More</apex:outputLink>
                                                            </apex:outputPanel>
			                                            </div>
			                                        </div>
			                                    </apex:repeat>
			                                </div>
			                            </div>
			                            <div class="family-documents-collapsible">
			                                <h2>
			                                    <span class="family-documents_heading__flex-item-first">
			                                        <div style="display: inline-block; width: 2em">
			                                            <span class="arrow arrow-right"></span>
			                                        </div>
			                                        Outstanding Documents
			                                    </span>
			                                </h2>
			                                <div class="family-documents-body closed">
			                                    <c:FamilyDocumentList pfsInSubmittedStatus="{!IF(PFSSTAGE = 'DOCS', 'true', 'false')}"
			                                                          useSimplifiedView="true"
			                                                          pfsRecord="{!pfsRecord}"
			                                                          actionsHidden="true"
			                                                          selectedAcademicYearId="{!academicYearId}"/>
			                                </div>
			                            </div>
			                        </div>

				                    
				                </div>
				            </div>

				        </apex:outputPanel>

			        </div>
			    </apex:pageBlock>
                <apex:outputPanel id="securityQuestionsPopup">
                    <div id="modal-complete-security-questions" style="display: none;">
                        <div class="modal-overlay" style="display:block;"></div>

                        <div class="questionsModal" Id="questionModal" style="display:block;">
                            <div class="questionsModalTitle">{!$Label.FD_Security_Questions_Absent_Popup_Message}</div>
                            <apex:outputPanel id="showQuestionsModalError" layout="block" styleClass="errorMessage"/>
                            <div>
                                <apex:selectList id="secQuestion1" value="{!pfsContact.Security_Question_1__c}" multiselect="false" size="1" styleClass="questionsModalField">
                                    <apex:selectOptions value="{!secQuestionPicklist1}"/>
                                </apex:selectList>
                            </div>
                            <div>
                                <apex:inputField id="secQuestionAnswer1" value="{!pfsContact.Security_Answer_1__c}" styleClass="requiredField questionsModalField answerFieldPadding"/>
                            </div>

                            <div>
                                <apex:selectList id="secQuestion2" value="{!pfsContact.Security_Question_2__c}" multiselect="false" size="1" styleClass="questionsModalField nextQuestionPadding">
                                    <apex:selectOptions value="{!secQuestionPicklist2}"/>
                                </apex:selectList>
                            </div>
                            <div>
                                <apex:inputField id="secQuestionAnswer2" value="{!pfsContact.Security_Answer_2__c}" styleClass="requiredField questionsModalField answerFieldPadding"/>
                            </div>

                            <div>
                                <apex:selectList id="secQuestion3" value="{!pfsContact.Security_Question_3__c}" multiselect="false" size="1" styleClass="questionsModalField nextQuestionPadding">
                                    <apex:selectOptions value="{!secQuestionPicklist3}"/>
                                </apex:selectList>
                            </div>
                            <div>
                                <apex:inputField id="secQuestionAnswer3" value="{!pfsContact.Security_Answer_3__c}" styleClass="requiredField questionsModalField answerFieldPadding"/>
                            </div>

                            <div align="center" class="nextQuestionPadding">
                                <apex:commandButton value="Save" onclick="checkAnswersExist('{!$Label.FD_Security_Questions_Absent_Error_Message}', '{!pfsContact.Id}')" reRender="false" styleClass="primary"/>
                            </div>
                        </div>
                    </div>
                </apex:outputPanel>
			</apex:form>
		</apex:define>
	</apex:composition>

	<!-- START JAVASCRIPT -->
    <!-- All of this javascript will be in a static resource file -->
    <script type="text/javascript">
        function navigateAway(navTarget){
            window.location = navTarget;
        }

        function printReceipt(id) {
            popupWindow = window.open('{!$Page.FamilyPaymentReceipt}' + '?id=' + id,
                                        'FamilyPaymentReceipt',
                                        'width=600,left=2,top=2,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
            popupWindow.focus();
        }
    </script>
<!-- END JAVASCRIPT -->

</apex:page>