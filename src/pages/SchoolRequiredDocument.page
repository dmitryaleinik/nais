<!--
/*
* SPEC-098
* Schools should be able to create custom labels for the non-standard documents they require which currently have generic names
*
* SPEC-102
* Schools should be able to see the required documents they had chosen for the prior application cycle
*
* Nathan, Exponent Partners, 2013
*/
-->
<apex:page controller="SchoolRequiredDocumentController" sideBar="false" tabStyle="Setup2__tab" >
    <apex:includeScript value="{!$Resource.Jquery191min}" /> <!-- NAIS-2345 -->
    <apex:includeScript value="{!$Resource.JQueryBlockUI}" />  <!-- NAIS-2345 --> 
    
    <apex:outputPanel id="pageJsCode">
        <script>
            var pageMessages = '{!errorMessageStr}';
            var j$ = jQuery.noConflict();  /*NAIS-2345*/

            function confirmDelete() {
                var isDelete = confirm("Are you sure you wish to Delete?");
                if (isDelete) return true;
                return false;
            }        

            j$(document).ready(function() {         
                if("{!editRendered}"=="true"){
                    showHideWhenIsOtherDocument();
                }
            });
            
            function onChangeDocumentTypeJS(){ 
                /* NAIS-2345*/
                var isTaxDocument = document.getElementById('{!$Component.id_form.theBlock.isTaxHidden}').value;   
                var docTypeElement= document.getElementById('{!$Component.id_form.theBlock.inputPageSection.DocTypeSection.DT}');
                var docType = docTypeElement.options[docTypeElement.selectedIndex].text;        
                
                if(docType!=null && (docType=='School/Organization Other Document' || docType=='Expense and Resources Statement')){
                    showHideWhenIsOtherDocument(docType);
                }else{
                    if(docType!=null && (docType=='State Tax Return' || isTaxDocument=="true")){
                        j$('label[id*="istaxdoc_label"]').show();
                        j$('span[id*="istaxdoc_panel"]').show();
                    }else{
                        j$('label[id*="istaxdoc_label"]').hide();
                        j$('span[id*="istaxdoc_panel"]').hide();
                    }
                    j$('label[id*="sample_block_section_label"]').hide();
                    j$('span[id*="sample_block_section_data"]').hide();
                    j$('label[id*="school_document_label"]').hide();
                    j$('span[id*="school_document_data"]').hide();
                    j$('span[id*="school_document_instructions"]').hide();
                }
                /* End NAIS-2345*/
            }
            
            function showHideWhenIsOtherDocument(docType)
            {
                /* NAIS-2345*/
                j$('label[id*="sample_block_section_label"]').show();
                j$('span[id*="sample_block_section_data"]').show();
                if(docType=='School/Organization Other Document')
                {
                    j$('label[id*="school_document_label"]').show();
                    j$('span[id*="school_document_data"]').show();
                }
                j$('span[id*="school_document_instructions"]').show();
                j$('label[id*="istaxdoc_label"]').hide();
                j$('span[id*="istaxdoc_panel"]').hide();
                /* End NAIS-2345*/
            }
        </script>
    </apex:outputPanel>

    <style>
        .editRow {background-color:#CEF6F5}
        a.sample_link{color:red;text-decoration:underline;}
        .hidden{display:none;}
        .visible{display:block;}
        .errorM3{margin-bottom:20px;}
    </style>
    
    <apex:form enctype="multipart/form-data" id="id_form">   
        <apex:actionStatus id="pleaseWaitStatus" onstart="j$.blockUI({'message':j$('.please-wait-message')});" onstop="j$.unblockUI(); window.scrollTo(0,0);"/> <!-- NAIS-2345 -->
        <div class="please-wait-message" style="display:none">
            <div style="font-size:4em;margin:5px">  
                <apex:image value="/img/loading32.gif" width="32px" height="32px" />&nbsp;      
                <apex:outputText escape="false" value="{!$Label.Please_wait}" />
            </div>
        </div><!-- End NAIS-2345 -->
        <apex:outputPanel id="wholePage" layout="block" styleClass="maindiv">
            <div class="pageHeaderBar">
                <c:SchoolHeaderBar />
                <apex:actionregion >
                    <c:SchoolAcademicYearSelector host="{!thisController}" value="{!selectedAcademicYear}"/>
                </apex:actionregion>
                <c:SchoolGlobalSearch />
                <br/><c:SchoolAcademicYearWarning />
            </div>
            
            <c:SchoolSubtabs host="{!thisController}" selectedTab="SchoolRequiredDocument"/>
            
            <apex:messages id="id_page_messages"/>
            
            <c:SchoolPageInstructions label_name="School_Instructions_Req_Documents" field_name="SYSTEM_Hide_Instructions_Req_Documents__c" />
            
            <apex:outputpanel id="customPageMessages" layout="block" styleClass="message errorM3" rendered="{!AND(errorMessageStr!=null,errorMessageStr!='')}">
                <table>
                    <tr valign="top">
                        <td><img alt="ERROR" class="msgIcon" src="/s.gif" title="ERROR" /></td>
                        <td><div class="messageText"><h4  style="color:#cc0000">Errors</h4></div></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>{!errorMessageStr}</td>
                    </tr>
                </table>
            </apex:outputpanel>
            
            <div class="block">
                <div class="blockheader">Required Documents</div>
                <div class="nopadding">
                    <apex:pageBlock id="theBlock">
                        <apex:inputHidden value="{!isTaxDocument}" id="isTaxHidden"/>  <!-- NAIS-2345 -->
                        <apex:pageBlockSection id="inputPageSection" columns="2" collapsible="false">
                            <apex:pageBlockSectionItem >
                                <apex:outputLabel >School</apex:outputLabel>
                                <apex:outputField value="{!school.Name}"/>
                            </apex:pageBlockSectionItem>
                            <apex:pageBlockSectionItem >
                                <apex:outputLabel >Academic Year</apex:outputLabel>
                                <apex:outputField value="{!academicYear.Name}"/>
                            </apex:pageBlockSectionItem>
                            <apex:pageBlockSectionItem id="DocTypeSection">
                                <apex:outputLabel >Document Type</apex:outputLabel>
                                <apex:outputPanel >                                 
                                    <apex:outputPanel rendered="{!newRendered}">
                                        <div class="requiredInput">
                                            <div class="requiredBlock"></div>   
                                            <apex:actionRegion > <!-- NAIS-2345 -->
                                                <apex:inputField value="{!requiredDocument.Document_Type__c}" id="DT">
                                                    <apex:actionSupport event="onchange" rerender="theBlock,documentYearSection" status="pleaseWaitStatus" action="{!onChangeDocumentType}" onComplete="onChangeDocumentTypeJS();"/>
                                                </apex:inputField>
                                            </apex:actionRegion> <!-- NAIS-2345 -->                   
                                        </div>
                                    </apex:outputPanel>
                                    <apex:outputField value="{!requiredDocument.Document_Type__c}" rendered="{!editRendered}"/>
                                    <div></div>
                                    <apex:outputText id="school_document_instructions" value="{!$Label.School_Instructions_SchoolOrg_Doc}"  styleClass="hidden" />                       
                                </apex:outputPanel>
                            </apex:pageBlockSectionItem>                             
                            <apex:pageBlockSectionItem id="documentYearSection" >
                                <apex:outputLabel id="istaxdoc_label" styleClass="hidden">Document Year </apex:outputLabel>
                                <apex:outputPanel id="istaxdoc_panel" styleClass="hidden">
                                    <apex:outputPanel >
                                        <div class="requiredInput">
                                            <div class="requiredBlock"></div>
                                            <apex:selectList value="{!selectedDocumentYear}" size="1">
                                                <apex:selectOptions value="{!documentYearOptions}">
                                                </apex:selectOptions>
                                            </apex:selectList>
                                         </div>
                                    </apex:outputPanel>
                                    <apex:outputField value="{!requiredDocument.Document_Year__c}" rendered="{!AND(editRendered,NOT(isTaxDocument))}"/> 
                                </apex:outputPanel>
                            </apex:pageBlockSectionItem>
                            <apex:pageBlockSectionItem >
                                <apex:outputLabel id="school_document_label" styleClass="hidden">
                                    Label for School/Organization Document
                                </apex:outputLabel>
                                <apex:outputPanel id="school_document_data" styleClass="hidden">
                                    <div class="requiredInput"><div class="requiredBlock"></div>
                                        <apex:inputField value="{!requiredDocument.Label_for_School_Specific_Document__c}"/>
                                    </div>
                                </apex:outputPanel>
                            </apex:pageBlockSectionItem>
                            
                             <apex:pageBlockSectionItem >
                                <apex:outputLabel id="sample_block_section_label" styleClass="hidden">
                                    Provide Document Sample
                                </apex:outputLabel>
                                <apex:outputPanel id="sample_block_section_data" styleClass="hidden">
                                    <div>
                                        <apex:inputFile value="{!attachments.inputFileBody}" filename="{!attachments.inputFileName}"/> 
                                        <apex:outputText escape="false" value="{!attachments.outputFileLink}" />
                                    </div>
                                    <div>
                                        The sample will appear in the Family Portal. Be sure your SSS code is prominently displayed on the document.
                                    </div>
                                </apex:outputPanel>
                            </apex:pageBlockSectionItem>
                        </apex:pageBlockSection>
                        <div class="pbHeader">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td class="pbTitle">&nbsp;</td>
                                        <td class="pbButton ">
                                            <apex:commandButton value="Save" action="{!saveDocument}"/>
                                            <apex:actionRegion >
                                                <apex:commandButton value="Cancel" action="{!cancelDocument}" reRender="pageJsCode, id_form"/>
                                            </apex:actionRegion>
                                        </td>
                                     </tr>
                                </tbody>
                            </table>
                        </div>
                    </apex:pageBlock>
                </div>
            </div>
            
            <div class="block">      
                <div class="blockheader">Required Documents - {!academicYear.Name}</div>
                <apex:pageBlock id="id_page_block">
                    <apex:pageBlockSection columns="1" collapsible="false">
                        <apex:pageBlockTable id="rdTable" value="{!docRowList}" var="RD">
                            <apex:column style="width:50px; white-space:nowrap; background-color:{!IF(RD.document.Id = requiredDocument.Id, '#CEF6F5', '#FFFFFF')};">
                                <apex:outputPanel rendered="{!IF(OR(RD.document.Document_Type__c==schoolSpecificDocumentType,RD.document.Document_Type__c=='Expense and Resources Statement'),true,false)}">
                                    <apex:commandLink value="Edit" 
                                        action="{!editDocument}" >
                                        <apex:param name="requiredDocumentId" value="{!RD.document.Id}"/>
                                    </apex:commandLink>&nbsp;|&nbsp;
                                </apex:outputPanel>
                             <apex:commandLink value="Delete" 
                                 onclick="return confirmDelete()" 
                                 action="{!deleteDocument}">
                                 <apex:param name="requiredDocumentId" value="{!RD.document.Id}"/>
                             </apex:commandLink>
                         </apex:column>
                         <apex:column headerValue="Name"  style="width:300px"
                             styleClass="{!IF(RD.document.Id = requiredDocument.Id, 'editRow', '')}">
                             <apex:outputText value="{!IF(RD.document.Document_Type__c = schoolSpecificDocumentType,RD.document.Label_for_School_Specific_Document__c, RD.document.Document_Type__c)}" />
                            &nbsp;<apex:outputText escape="false" value="{!RD.attachmentLink}" />
                         </apex:column>
                         <apex:column headerValue="Type" style="width:300px" 
                             styleClass="{!IF(RD.document.Id = requiredDocument.Id, 'editRow', '')}">
                             {!IF(RD.document.Document_Type__c = schoolSpecificDocumentType, 'Custom', RD.document.Document_Type__c)}
                             {!IF(RD.document.Document_Year__c != null, ' - ' + RD.document.Document_Year__c, '')}
                         </apex:column>
                        </apex:pageBlockTable>
                    </apex:pageBlockSection>
                </apex:pageBlock>
            </div>
        </apex:outputPanel>
    </apex:form>
</apex:page>