<!-- Spec-109 School Portal - Tuition and Fees;
	 BR, Exponent Partners, 2013 -->

<apex:page controller="SchoolAnnualSettingsController" action="{!LoadAnnualSetting}" sidebar="false" tabStyle="Setup2__tab">
	<!-- NAIS-2452 -->
	<apex:includeScript value="{!$Resource.Jquery191min}" />  
    <apex:includeScript value="{!$Resource.JQueryBlockUI}" />   
	<script type="text/javascript">
      var j$ = jQuery.noConflict();
    </script>
    <!-- NAIS-2452 -->
	<!-- <apex:stylesheet value="{!URLFOR($Resource.SchoolPortalStyles, '/css/newStyles.css')}"/>  -->	
    <apex:form id="schooltuitionfees"><!-- NAIS-2452 -->
    	<!-- NAIS-2452 -->
    	<apex:actionStatus id="pleaseWaitStatus" onstart="j$.blockUI({'message':j$('.please-wait-message')});" onstop="j$.unblockUI(); window.scrollTo(0,0);"/>
	    <div class="please-wait-message" style="display:none">
	        <div style="font-size:4em;margin:5px">  
				<apex:image value="/img/loading32.gif" width="32px" height="32px" />&nbsp;   	
	            <apex:outputText escape="false" value="{!$Label.Please_wait}" />
	        </div>
	    </div>
	   	<!-- End NAIS-2452 -->
		<apex:outputPanel id="allSections" layout="block" styleClass="maindiv">
			<div class="pageHeaderBar">
				<c:SchoolHeaderBar />
				<c:SchoolAcademicYearSelector host="{!Me}" value="{!Model.Academic_Year__c}"/>
				<c:SchoolGlobalSearch />
				<br/><c:SchoolAcademicYearWarning />
			</div>
			
			<c:SchoolSubtabs host="{!Me}" selectedTab="SchoolTuitionAndFees"/>

			<apex:pageMessages />
			
			<!-- SCHOOL INSTRUCTIONS -->
			<c:SchoolPageInstructions label_name="School_Instructions_Tuition_Fees" field_name="SYSTEM_Hide_Instructions_Tuition_Fees__c" />

			<div class="block">			
			
				<div class="blockheader">
					Tuition and Fees
				</div>
				
				<div class="blockbody">
				
		            <p>Enter tuition and other expenses for each grade level to populate individual applicant records and make
		            the financial aid awarding process and budget tracking easier.</p>
		            <p>Any updated tuition and fees can be applied to the students in your account who are applying to the grades
		            for which you've included tuition and fee information. If you do not wish to override the tuition expense values
		            previously entered for any students, save using the first option below. If you want all the students in your
		            school to have the updated tuition and fees for the grade they're applying to, select the second button.</p>
		            <p>Changes will apply to applicants in grade(s) where Tuition and Fees are defined.</p>
				
				</div>	
			</div>
	        
	        <div class="block">
	        

		        <apex:pageBlock id="pageBlockTuition">
		        	<apex:pageBlockButtons location="both">
						<apex:commandButton action="{!applyWNoTuition}" 
							value="Save & Apply to Students without any Tuition Entered" status="pleaseWaitStatus" reRender="schooltuitionfees"/><!-- NAIS-2452 -->
						<apex:commandButton action="{!applyAll}" 
							value="Save & Apply to All Students" status="pleaseWaitStatus" reRender="schooltuitionfees"/> <!-- NAIS-2452 -->
						<apex:commandButton action="{!cancel}" 
							value="Cancel"/>
						<apex:commandButton action="{!copyTuitionFees}" rendered="{!isAvailableCopyTuitionCost}"
							value="Copy Tuition & Fees from Last Year" reRender="pageSections" status="pleaseWaitStatus"/><!-- NAIS-2452 -->
					</apex:pageBlockButtons>
					<div class="blockbody">
			        	<apex:pageBlockSection >
			        		<apex:inputField value="{!Model.Tuition_Schedule_Type__c}" label="Tuition Schedule" required="true">
			        			<apex:actionSupport event="onchange" action="{!tuitionSchedule_onchange}" reRender="pageBlockTuition"/>
			        		</apex:inputField>	
			       		</apex:pageBlockSection>
			
						<apex:outputPanel id="pageSections">
							<apex:outputPanel id="oneGrade" rendered="{!Model.Tuition_Schedule_Type__c == 'One Schedule for Entire School'}">
								<apex:pageBlockTable value="{!Grades}" var="grade" rows="1">
									<apex:column headerValue="Day Tuition" rendered="{!bDay}">
										<apex:inputText value="{!grade.Day_Tuition}" size="12"/>
									</apex:column>
									<apex:column headerValue="Boarding Tuition" rendered="{!bBoarding}">
										<apex:inputText value="{!grade.Boarding_Tuition}" size="12"/>
									</apex:column>
									<apex:column headerValue="Day Fees" rendered="{!bDay}">
										<apex:inputText value="{!grade.Day_Fees}" size="12"/>
									</apex:column>
									<apex:column headerValue="Boarding Fees" rendered="{!bBoarding}">
										<apex:inputText value="{!grade.Boarding_Fees}" size="12"/>
									</apex:column>
									<apex:column headerValue="Day Travel" rendered="{!bDay}">
										<apex:inputText value="{!grade.Day_Travel_Expense}" size="12"/>
									</apex:column>
									<apex:column headerValue="Boarding Travel" rendered="{!bBoarding}">
										<apex:inputText value="{!grade.Boarding_Travel_Expense}" size="12"/>
									</apex:column>
									<apex:column rendered="{!bDay}">
									    <apex:facet name="header">
									        Day New Student <br/> Add'l Expenses
									    </apex:facet>
										<apex:inputText value="{!grade.Day_New_Student_Expense}" size="12"/>
									</apex:column>
									<apex:column rendered="{!bBoarding}">
									    <apex:facet name="header">
									        Board New Student <br/> Add'l Expenses
									    </apex:facet>
										<apex:inputText value="{!grade.Boarding_New_Student_Expense}" size="12"/>
									</apex:column>
									<apex:column rendered="{!bDay}">
									    <apex:facet name="header">
									        Day Returning Student <br/> Add'l Expenses
									    </apex:facet>
										<apex:inputText value="{!grade.Day_Returning_Student_Expense}" size="12"/>
									</apex:column>
									<apex:column rendered="{!bBoarding}">
									    <apex:facet name="header">
									        Board Returning Student <br/> Add'l Expenses
									    </apex:facet>
										<apex:inputText value="{!grade.Boarding_Returning_Student_Expense}" size="12"/>
									</apex:column>
								</apex:pageBlockTable>
							</apex:outputPanel>
				 			
				        	<apex:outputPanel id="allGrades" rendered="{!Model.Tuition_Schedule_Type__c == 'One Schedule per Grade'}">   		
								<apex:pageBlockTable value="{!Grades}" var="grade">
									<apex:column headerValue="Name" value="{!grade.Name}"/>
									<apex:column headerValue="Day Tuition" rendered="{!bDay}">
										<apex:inputText value="{!grade.Day_Tuition}" size="12"/>
									</apex:column>
									<apex:column headerValue="Boarding Tuition" rendered="{!bBoarding}">
										<apex:inputText value="{!grade.Boarding_Tuition}" size="12"/>
									</apex:column>
									<apex:column headerValue="Day Fees" rendered="{!bDay}">
										<apex:inputText value="{!grade.Day_Fees}" size="12"/>
									</apex:column>
									<apex:column headerValue="Boarding Fees" rendered="{!bBoarding}">
										<apex:inputText value="{!grade.Boarding_Fees}" size="12"/>
									</apex:column>
									<apex:column headerValue="Day Travel" rendered="{!bDay}">
										<apex:inputText value="{!grade.Day_Travel_Expense}" size="12"/>
									</apex:column>
									<apex:column headerValue="Boarding Travel" rendered="{!bBoarding}">
										<apex:inputText value="{!grade.Boarding_Travel_Expense}" size="12"/>
									</apex:column>
									<apex:column rendered="{!bDay}">
									    <apex:facet name="header">
									        Day New Student <br/> Add'l Expenses
									    </apex:facet>
										<apex:inputText value="{!grade.Day_New_Student_Expense}" size="12"/>
									</apex:column>
									<apex:column rendered="{!bBoarding}">
									    <apex:facet name="header">
									        Board New Student <br/> Add'l Expenses
									    </apex:facet>
										<apex:inputText value="{!grade.Boarding_New_Student_Expense}" size="12"/>
									</apex:column>
									<apex:column rendered="{!bDay}">
									    <apex:facet name="header">
									        Day Returning Student <br/> Add'l Expenses
									    </apex:facet>
										<apex:inputText value="{!grade.Day_Returning_Student_Expense}" size="12"/>
									</apex:column>
									<apex:column rendered="{!bBoarding}">
									    <apex:facet name="header">
									        Board Returning Student <br/> Add'l Expenses
									    </apex:facet>
										<apex:inputText value="{!grade.Boarding_Returning_Student_Expense}" size="12"/>
									</apex:column>
								</apex:pageBlockTable>
							</apex:outputPanel>
						</apex:outputPanel>
					</div>
				</apex:pageBlock>
			</div>
		</apex:outputPanel>
	</apex:form>
</apex:page>