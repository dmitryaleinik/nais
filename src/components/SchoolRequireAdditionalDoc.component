<apex:component controller="SchoolRequireAdditionalDocController" allowDML="true" >
    <apex:attribute name="showSaveAndNew" description="Flag to show 'Save And New' button." type="Boolean" required="true" />
    <apex:attribute name="showSaveAndUpload" description="Flag to show 'Save And Upload' button." type="Boolean" required="true" />
    <apex:attribute name="showCancel" description="Flag to show 'Cancel' button." type="Boolean" required="true" />
    <apex:attribute name="academicYearNameVar" assignTo="{!academicYearName}" description="SPA's academic year name." type="String" required="true" />
    <apex:attribute name="handlerVar" assignTo="{!handler}" description="Controller that contains the action implementations." type="ISchoolRequireAdditionalDoc" required="true" />
    <apex:attribute name="buttonsLocation" description="Location of buttons block." type="String" />
    <apex:attribute name="statusactionEnabled" description="If the status action logic needs to be executed." type="Boolean" default="false" />
    <apex:attribute name="renderMessageInside" description="If the errors must be rendered inside the table." type="Boolean" default="false" />
    <apex:attribute name="cancelLink" description="Link for cancel button." type="String" required="false"/>
    
<apex:outputPanel rendered="{!NOT(renderMessageInside)}">
<script>
    function hidePageBlock() {
    
        document.getElementById('j_id0:mainform:errorMessages').style.display = "none";
        document.getElementById('{!$Component.thePanel.SchoolRequireAdditionalDoc_Panel}').style.display = "none";
        document.getElementById('{!$Component.thePanel.subtitle}').style.display = "none";
        document.getElementById('{!$Component.thePanel.buttons_block.savebtn}').style.display =  "none";
        hideParentComponents("hidden");
        document.getElementById('processing-modal').style.display = "block";
    }
    function showPageBlock() {
        document.getElementById('j_id0:mainform:errorMessages').style.display = "block";
        document.getElementById('{!$Component.thePanel.SchoolRequireAdditionalDoc_Panel}').style.display = "block";
        document.getElementById('{!$Component.thePanel.subtitle}').style.display = "block";
        document.getElementById('{!$Component.thePanel.buttons_block.savebtn}').style.display = "block";
        hideParentComponents("visible");
        document.getElementById('processing-modal').style.display = "none";
    }
    function hideParentComponents(display) {
    
        if (window.parent !== null) {
        
            if( window.parent.document.getElementsByClassName('ui-dialog-title') !== null) {
                window.parent.document.getElementsByClassName('ui-dialog-title')[0].style.visibility =  display;
            }
            
            if( window.parent.document.getElementsByClassName('ui-button-icon-primary ui-icon ui-icon-closethick') !== null ) {
                window.parent.document.getElementsByClassName('ui-button-icon-primary ui-icon ui-icon-closethick')[0].style.visibility =  display;
            }
        }
    }
</script>
</apex:outputPanel>

<style>
.modal-static { 
    position: fixed;
    top: 10px !important; 
    left: 40% !important; 
    overflow: hidden !important;
}
.modal-static,
.modal-static .modal-dialog,
.modal-static .modal-content {
    width: 480px; 
    height: 180px; 
}
.modal-static .modal-dialog,
.modal-static .modal-content {
    padding: 0 !important; 
    margin: 0 !important;
}
.modal-static .modal-content .icon {
    margin-bottom: 20px;
}
.modal-static .modal-content h4 {
    width: 480px !important;
    font-size: 18px;
    color: #646664;
    margin-left: -113px;
    font-weight: normal;
}
</style>
<apex:actionStatus id="loading" onstart="hidePageBlock();" onstop="showPageBlock();" />
<apex:actionFunction name="rerenderSchoolRequireAdditionalDoc" action="{!loadDocumentYears}" status="typeStatus" reRender="SchoolRequireAdditionalDoc_Panel"/>
<apex:actionFunction name="loadingSaveNewWithActionStatus" status="loading" action="{!saveNewDocument}" rerender="mainform"/>
<apex:actionFunction name="loadingSaveNewWithoutActionStatus" action="{!saveNewDocument}" />

<apex:pageBlock id="thePanel">
    <apex:pageBlockButtons id="buttons_block" location="{!IF(ISNULL(buttonsLocation),'both',buttonsLocation)}">
        <apex:commandButton value="Save" rerender="" rendered="{!statusactionEnabled}" id="savebtn" onclick="loadingSaveNewWithActionStatus();return false;" />
        <apex:commandButton value="Save" rerender="" rendered="{!NOT(statusactionEnabled)}" onclick="loadingSaveNewWithoutActionStatus();return false;" />
        <apex:commandButton value="Save & New" action="{!saveAndNewAdditionalDocument}" rendered="{!showSaveAndNew}" />
        <apex:commandButton value="Save & Upload this File"  action="{!saveAndUploadRequiredDocuments}" rerender="bodyForm, otherDocumentsPopup, errorMessages" rendered="{!showSaveAndUpload}"/>
        <apex:commandButton value="Cancel" action="{!cancelAdditionalDocument}" immediate="true" rendered="{!AND(showCancel, ISNULL(cancelLink))}"/>
        <apex:outputLink value="{!cancelLink}" rendered="{!AND(showCancel, NOT(ISNULL(cancelLink)))}" StyleClass="btn CancelLink">Cancel</apex:outputLink>
    </apex:pageBlockButtons>
    
    <!-- START: Static Modal -->
    <apex:outputPanel rendered="{!NOT(renderMessageInside)}">
        <div style="display:none;" id="processing-modal">
            <div class="modal modal-static fade" role="dialog" aria-hidden="true" >
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="text-center">
                                <img src="{!$Resource.processing_spinner}" class="icon" />
                                <h4>Processing...</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </apex:outputPanel>
    <!-- END: Static Modal -->
    
    <apex:outputPanel id="subtitle" StyleClass="subTitle" layout="block" rendered="{!NOT(ISNULL(subtitle))}">
        <apex:outputLabel >{!subtitle}</apex:outputLabel>
    </apex:outputPanel>
    
    <apex:outputPanel id="SchoolRequireAdditionalDoc_Panel" layout="block" StyleClass="blockbody nopadding">
        <apex:pageBlockSection columns="1" collapsible="false">
            <apex:pageBlockSectionItem rendered="{!renderMessageInside}">
                <apex:pagemessages id="errorMessages" />
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem rendered="{!renderMessageInside}">
                <div id="processing-modal2" style="display:none;">Loading...</div>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel >Document Type</apex:outputLabel>
                <apex:outputPanel >
                    <div class="requiredInput"><div class="requiredBlock"></div>
                        <apex:selectList value="{!additionalSchoolDocAssign.Document_Type__c}" size="1" onchange="rerenderSchoolRequireAdditionalDoc();">
                            <apex:selectOptions value="{!nonSchoolSpecificDocumentTypes}"></apex:selectOptions>
                        </apex:selectList>
                        <apex:actionStatus startText=" (loading...)" id="typeStatus"/>
                    </div>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem rendered="{!isTaxDocument}">
                <apex:outputPanel id="documentYearLabel">
                    <apex:outputLabel >Document Year</apex:outputLabel>
                </apex:outputPanel>
                <apex:outputPanel id="documentYearSelect">
                    <apex:outputPanel layout="block" styleClass="requiredInput">
                        <div class="requiredBlock"></div>
                        <apex:selectList value="{!additionalSchoolDocAssign.Document_Year__c}" multiSelect="false" size="1">
                            <apex:selectOptions value="{!validDocumentYears}" />
                        </apex:selectList>
                    </apex:outputPanel>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>

        </apex:pageBlockSection>
    </apex:outputPanel>
</apex:pageBlock>
</apex:component>