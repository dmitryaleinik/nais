<apex:component id="StudentFolderAttachments" controller="AttachmentsController" allowDML="true">
    <apex:attribute name="studentFolderId" description="Id of the Student Folder" type="Id"
                    required="false" assignTo="{!RecordId}" />

    <apex:stylesheet value="{!URLFOR($Resource.SchoolPortalStyles, '/css/familyTableStyles.css')}"/>

    <apex:includeScript value="{!$Resource.JQModal}" />

    <apex:outputPanel styleClass="pageBlock block" layout="block" id="studentFolderAttachmentsPanel">
        <div class="blockheader" style="padding-right:2px">
            Internal School Documents
            <div style="float: right; padding-top: 5px; padding-bottom: 5px; text-align: center;">
                <a href="#"
                    class="btn primary"
                    style="width: 80px;"
                    onclick="showUploadModal(); return false;">
                    UPLOAD
                </a>
            </div>
        </div>
        <apex:form encType="multipart/form-data">
            <script type="text/javascript">
                function showUploadModal() {
                    jQuery("#main-school-upload-modal").modal({escClose: false});
                    jQuery("#main-school-upload-modal").show();
                }

                function showEditModal(idToEdit) {
                    jQuery("[id$='main-school-edit-modal']").modal({escClose: false});
                    jQuery("[id$='main-school-edit-modal']").show();
                }

                function closeModal(){
                    jQuery.modal.close();
                }

                function populateFileNameInput(uploadInput) {
                    var fileNameInput = jQuery("[id$='fileName']");
                    if (fileNameInput.val() === '') {
                        var fileName = uploadInput.value.split('\\').pop();
                        fileNameInput.val(fileName);
                    }
                }
            </script>

            <div class="blockbody">
                <apex:outputText value="{!$Label.Internal_Docs_School_Only}" escape="false" />
            </div>
            <div class="blockbody nopadding">
                <apex:outputPanel styleClass="pbSubsection" rendered="{!HasAttachments}">
                    <table class="list detailList fpTable" style="width: 100%">
                        <thead>
                        <tr>
                            <th class="editDelColumn" style="width:5%"></th>
                            <th class="fileColumn" style="width:25%">File Name</th>
                            <th class="dateColumn">Date Uploaded</th>
                        </tr>
                        </thead>
                        <tbody>
                            <apex:repeat value="{!Attachments}" var="attch">
                                <tr>
                                    <td class="editDelCoumn">
                                        <apex:commandLink value="Del" onclick="return confirm('Are you sure?');" action="{!deleteAttachment}">
                                            <apex:param name="idToDelete" value="{!attch.Id}" />
                                        </apex:commandLink>
                                        <span> | </span>
                                        <apex:commandLink value="Edit" action="{!renderEditAttachment}" onComplete="showEditModal();"
                                                          rerender="main-school-edit-modal">
                                            <apex:param name="idToEdit" value="{!attch.Id}" />
                                        </apex:commandLink>
                                    </td>
                                    <td>
                                        <apex:outputLink value="{!URLFOR($Action.Attachment.Download, attch.Id)}" target="_blank">{!attch.Description}</apex:outputLink>
                                    </td>
                                    <td>
                                        <apex:outputText value="{0,date,MM-dd-yyyy}">
                                            <apex:param value="{!attch.CreatedDate}" />
                                        </apex:outputText>
                                    </td>
                                </tr>
                            </apex:repeat>
                        </tbody>
                    </table>
                </apex:outputPanel>
            </div>
        </apex:form>
        <div id="main-school-upload-modal" class="block" style="display:none;">
            <apex:form encType="multipart/form-data">
                <apex:outputPanel id="main-school-upload-modal-content">
                    <div class="blockheader">
                        <h1>Upload Internal School Document
                            <span style="float:right;"><a href="#" onClick="javascript:var resp = ('{!$Label.Message_Close_Upload_Document_Window}'!=''?confirm('{!$Label.Message_Close_Upload_Document_Window}'):true);if(resp){closeModal();}else{return false;}">x</a></span>
                        </h1>
                    </div>
                    <div class="blockbody">
                        <p>Please select a file to upload.</p>
                        <div class="row">
                            <apex:outputLabel value="File" for="file"/>
                            <apex:inputFile value="{!NewAttachment.body}" filename="{!NewAttachment.Name}" id="file" onChange="populateFileNameInput(this);"/><br />
                        </div>
                        <div class="row">
                            <apex:outputLabel value="File Name" for="fileName"/>
                            <apex:inputText value="{!NewAttachment.Description}" id="fileName" />
                        </div>
                    </div>
                    <div class="blockfooter">
                        <div class="buttonsWrap">
                            <div class="centerButtons">
                                <apex:commandButton value="Cancel" onClick="closeModal(); return false;" styleClass="cancel" />
                                <apex:commandButton action="{!upload}" value="Upload" styleClass="primary" />
                            </div>
                        </div>
                    </div>
                </apex:outputPanel>
            </apex:form>
        </div>
        <apex:outputPanel layout="block" id="main-school-edit-modal" styleClass="block" style="display:none;">
            <apex:form >
                <apex:outputPanel id="main-school-edit-modal-content" >
                    <div class="blockheader">
                        <h1>Edit Internal School Document
                            <span style="float:right;"><a href="#" onClick="closeModal();">x</a></span>
                        </h1>
                    </div>
                    <div class="blockbody">
                        <div class="row">
                            <apex:outputLabel value="File Name" for="fileNameEdit"/>
                            <apex:inputText value="{!AttachmentForEdit.Description}" id="fileNameEdit" />
                        </div>
                    </div>
                    <div class="blockfooter">
                        <div class="buttonsWrap">
                            <div class="centerButtons">
                                <apex:commandButton value="Cancel" onClick="closeModal(); return false;" styleClass="cancel" />
                                <apex:commandButton action="{!updateAttachment}" value="Save" styleClass="primary" />
                            </div>
                        </div>
                    </div>
                </apex:outputPanel>
            </apex:form>
        </apex:outputPanel>
    </apex:outputPanel>
</apex:component>