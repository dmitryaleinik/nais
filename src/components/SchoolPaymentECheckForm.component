<apex:component >
    <apex:attribute name="mainController" description="The main school payment controller" type="SchoolPaymentController" required="true"/>
    <apex:attribute name="readOnly" description="read-only flag" type="boolean"/>

    <div class="block fp">
        <div class="blockheader">
            Enter Account Information
        </div>

        <div class="blockbody detaillist">
            <table class="paymentTable">
                <tbody>
                    <tr>
                        <td class="paymentTableLabelCol paymentTableHeading">BILLING CONTACT</td>
                        <td class="paymentTableDataCol">&nbsp;</td>
                        <td class="paymentTableLabelCol">&nbsp;</td>
                        <td class="paymentTableDataCol">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="paymentTableLabelCol">First Name</td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.paymentData.billingFirstName}" styleClass="paymentFieldRequired" tabindex="1" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.paymentData.billingFirstName}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.billingFirstName}" rendered="{!NOT(ISNULL(mainController.fieldErrors.billingFirstName))}" styleClass="paymentFieldError"/>
                        </td>
                        <td class="paymentTableLabelCol">Email</td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.paymentData.billingEmail}" styleClass="paymentFieldRequired" tabindex="3" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.paymentData.billingEmail}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.billingEmail}" rendered="{!NOT(ISNULL(mainController.fieldErrors.billingEmail))}" styleClass="paymentFieldError"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="paymentTableLabelCol">Last Name</td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.paymentData.billingLastName}" styleClass="paymentFieldRequired" tabindex="2" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.paymentData.billingLastName}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.billingLastName}" rendered="{!NOT(ISNULL(mainController.fieldErrors.billingLastName))}" styleClass="paymentFieldError"/>
                        </td>
                        <td class="paymentTableLabelCol">
                            <apex:outputPanel rendered="{!NOT(readOnly)}">
                                Confirm Email
                            </apex:outputPanel>
                            <apex:outputPanel rendered="{!readOnly}">Confirm Email</apex:outputPanel>
                        </td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.billingEmailConfirm}" styleClass="paymentFieldRequired" tabindex="4" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.billingEmailConfirm}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.billingEmailConfirm}" rendered="{!NOT(ISNULL(mainController.fieldErrors.billingEmailConfirm))}" styleClass="paymentFieldError"/>
                        </td>
                    </tr>
                </tbody>
            </table>

            <apex:outputPanel id="checkContent">
                <table class="paymentTable">
                    <tbody>
                    <tr>
                        <td class="paymentTableLabelCol paymentTableHeading">BILLING ADDRESS</td>
                        <td class="paymentTableDataCol">&nbsp;</td>
                        <td class="paymentTableLabelCol paymentTableHeading">ACCOUNT INFORMATION</td>
                        <td class="paymentTableDataCol">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="paymentTableLabelCol">Country</td>
                        <td class="paymentTableDataCol">
                            <apex:selectlist value="{!mainController.paymentData.billingCountry}" size="1"  styleClass="paymentFieldRequired" tabindex="5" rendered="{!NOT(readOnly)}">
                                <apex:actionSupport event="onchange" rerender="billing_state_input"/>
                                <apex:selectOption itemValue="NONE" itemLabel="" />
                                <apex:selectOptions value="{!mainController.billingCountrySelectOptions}"/>
                            </apex:selectlist>
                            <apex:outputText value="{!mainController.selectedCountryName}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.billingCountry}" rendered="{!NOT(ISNULL(mainController.fieldErrors.billingCountry))}" styleClass="paymentFieldError"/>
                        </td>
                        <td class="paymentTableLabelCol">Account Type</td>
                        <td class="paymentTableDataCol">
                            <apex:selectlist value="{!mainController.paymentData.checkAccountType}" size="1" styleClass="paymentFieldRequired" tabindex="12" rendered="{!NOT(readOnly)}">
                                <apex:selectOption itemValue="NONE" itemLabel="" />
                                <apex:selectOptions value="{!mainController.accountTypeSelectOptions}"/>
                                <apex:actionSupport event="onchange" reRender="checkContent"/>
                            </apex:selectlist>
                            <apex:outputText value="{!mainController.selectedAccountType}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.checkAccountType}" rendered="{!NOT(ISNULL(mainController.fieldErrors.checkAccountType))}" styleClass="paymentFieldError"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="paymentTableLabelCol">Address Line 1</td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.paymentData.billingStreet1}" styleClass="paymentFieldRequired" tabindex="6" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.paymentData.billingStreet1}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.billingStreet1}" rendered="{!NOT(ISNULL(mainController.fieldErrors.billingStreet1))}" styleClass="paymentFieldError"/>
                        </td>
                        <apex:outputPanel layout="none" rendered="{!mainController.paymentData.checkAccountType == 'Business Checking'}">
                            <td class="paymentTableLabelCol">Business Name</td>
                            <td class="paymentTableDataCol">
                                <apex:inputText value="{!mainController.paymentData.checkNameOnCheck}" styleClass="paymentFieldRequired" tabindex="11" rendered="{!NOT(readOnly)}"/>
                                <apex:outputText value="{!mainController.paymentData.checkNameOnCheck}" rendered="{!readOnly}"/>
                                <apex:outputText value="{!mainController.fieldErrors.checkNameOnCheck}" rendered="{!NOT(ISNULL(mainController.fieldErrors.checkNameOnCheck))}" styleClass="paymentFieldError"/>
                            </td>
                        </apex:outputPanel>
                        <apex:outputPanel layout="none" rendered="{!mainController.paymentData.checkAccountType != 'Business Checking'}">
                            <td class="paymentTableLabelCol"></td>
                            <td class="paymentTableDataCol"></td>
                        </apex:outputPanel>
                    </tr>
                    <tr>
                        <td class="paymentTableLabelCol">Address Line 2</td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.paymentData.billingStreet2}" tabindex="7" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.paymentData.billingStreet2}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.billingStreet2}" rendered="{!NOT(ISNULL(mainController.fieldErrors.billingStreet2))}" styleClass="paymentFieldError"/>
                        </td><td class="paymentTableLabelCol">Bank Name</td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.paymentData.checkBankName}" styleClass="paymentFieldRequired" tabindex="11" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.paymentData.checkBankName}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.checkBankName}" rendered="{!NOT(ISNULL(mainController.fieldErrors.checkBankName))}" styleClass="paymentFieldError"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="paymentTableLabelCol">City</td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.paymentData.billingCity}" styleClass="paymentFieldRequired" tabindex="8" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.paymentData.billingCity}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.billingCity}" rendered="{!NOT(ISNULL(mainController.fieldErrors.billingCity))}" styleClass="paymentFieldError"/>
                        </td>
                        <td class="paymentTableLabelCol">Bank ABA Routing</td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.paymentData.checkRoutingNumber}" styleClass="paymentFieldRequired" tabindex="13" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.paymentData.checkRoutingNumber}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.checkRoutingNumber}" rendered="{!NOT(ISNULL(mainController.fieldErrors.checkRoutingNumber))}" styleClass="paymentFieldError"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="paymentTableLabelCol">State/Province</td>
                        <td class="paymentTableDataCol">
                            <apex:outputPanel id="billing_state_input">
                                <apex:outputPanel rendered="{!NOT(readOnly)}">
                                    <apex:selectlist value="{!mainController.paymentData.billingState}" size="1" styleClass="paymentFieldRequired" rendered="{!mainController.isUnitedStatesSelected}" tabindex="9" >
                                        <apex:selectOption itemValue="NONE" itemLabel="" />
                                        <apex:selectOptions value="{!mainController.billingStateUSSelectOptions}"/>
                                    </apex:selectlist>
                                    <!-- NAIS-2317 [DP] 03.05.2015 removed required style on non-US State -->
                                    <apex:inputText value="{!mainController.paymentData.billingState}" rendered="{!NOT(mainController.isUnitedStatesSelected)}" tabindex="9" />
                                </apex:outputPanel>
                                <apex:outputText value="{!mainController.paymentData.billingState}" rendered="{!readOnly}"/>
                                <apex:outputText value="{!mainController.fieldErrors.billingState}" rendered="{!NOT(ISNULL(mainController.fieldErrors.billingState))}" styleClass="paymentFieldError"/>
                            </apex:outputPanel>
                        </td>
                        <td class="paymentTableLabelCol">Bank Account Number</td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.paymentData.checkAccountNumber}" styleClass="paymentFieldRequired" tabindex="14" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.paymentData.checkAccountNumber}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.checkAccountNumber}" rendered="{!NOT(ISNULL(mainController.fieldErrors.checkAccountNumber))}" styleClass="paymentFieldError"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="paymentTableLabelCol">Zip/Postal Code</td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.paymentData.billingPostalCode}" styleClass="paymentFieldRequired" tabindex="10" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.paymentData.billingPostalCode}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.billingPostalCode}" rendered="{!NOT(ISNULL(mainController.fieldErrors.billingPostalCode))}" styleClass="paymentFieldError"/>
                        </td>
                        <td class="paymentTableLabelCol">
                            <apex:outputPanel rendered="{!NOT(readOnly)}">
                                Confirm Bank Account Number
                            </apex:outputPanel>
                            <apex:outputPanel rendered="{!readOnly}">Confirm Bank Account Number</apex:outputPanel>
                        </td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.checkAccountNumberConfirm}" styleClass="paymentFieldRequired" tabindex="16" rendered="{!NOT(readOnly)}" id="checkacctnumconfirm"/>
                            <apex:outputText value="{!mainController.checkAccountNumberConfirm}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.checkAccountNumberConfirm}" rendered="{!NOT(ISNULL(mainController.fieldErrors.checkAccountNumberConfirm))}" styleClass="paymentFieldError"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="paymentTableDataCol" style="text-align:center;" colspan="4">
                            <apex:outputPanel rendered="{!NOT(readOnly)}">
                                <img src="{!$Resource.CheckSample}"/>
                            </apex:outputPanel>
                            <apex:outputPanel rendered="{!readOnly}">&nbsp;</apex:outputPanel>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </apex:outputPanel>

            <apex:outputPanel rendered="{!mainController.isFeeWaiver}">
            <div id="paymentTotal">Quantity:&nbsp;<apex:outputText value="{0, number,###,###,##0}"><apex:param value="{!mainController.feeWaiverQuantity}" /></apex:outputText></div>
            </apex:outputPanel>
            <div id="paymentTotal">Total:&nbsp;<apex:outputText value="{0, number,$###,###,##0.00}"><apex:param value="{!mainController.paymentData.paymentAmount}" /></apex:outputText></div>

        </div>
    </div>
    <script type="text/javascript" >
        window.onload = function() {
            var checkacctconfirm = document.getElementById('{!$Component.checkacctnumconfirm}');
            checkacctconfirm.onpaste = function(e) {
                e.preventDefault();
            }

            var routeconfirm = document.getElementById('{!$Component.checkroutingnumconfirm}');
            routeconfirm.onpaste = function(e) {
                e.preventDefault();
            }
        }
    </script>
</apex:component>