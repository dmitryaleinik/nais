<apex:component controller="FamilyAppPrintPFSPDFController" layout="none">
    <apex:attribute name="pfsRecord" description="The PFS record that the component should work with" type="PFS__c" assignTo="{!pfs}" />
    
    <div style="width: 70%; border-top: 0px; margin-top: 0px; border: 1px solid; padding-left: 5px; padding-right: 5px; font-size: 12px;">
        <!--  Start Header -->
        <table style="width: 100%;">
            <tr>
                <td style="width: 55%;">
                    <apex:image url="{!URLFOR($Resource.FamilyPortalStyles, '/FamilyPortalStyles/images/SSS_Hrz_RGB.png')}" height="47px"/><br/>
                    <label style="padding-left: 52px; padding-top: 0px;">{!$Label.Contact_Address}</label><br/>                    
                    <label style="padding-left: 52px;">{!$Label.Contact_Email}</label>
                </td>    
                <td style="width: 45%; vertical-align: top;">
                    <label style="color: #610169; float:right; font-size:16px; width: 100%; text-align: right;"><strong>{!academicYear} Academic Year</strong></label><br/>
                    <label style="float: right; width: 100%; text-align: right;">Helpline: {!$Label.Helpline}</label><br/> 
                    <label style="float: right; width: 100%; text-align: right;">From outside of the US or Canada dial {!$Label.HelplineOutside}</label>
                </td>
            </tr>
            <tr><td style="font-size: 16px; font-weight: bold; text-align: center;" colspan="2">PFS Required Document Cover Sheet</td></tr>  
            <tr><td style="font-size: 12px; font-weight: bold; text-align: center;" colspan="2">***Always include this cover sheet when sending any documents.***</td></tr>
            <tr><td style="color: #610169; font-size: 12px; text-align: center;" colspan="2">Complete this cover sheet and send with all documentation supporting your Parents' Financial Statement (PFS).</td></tr>
            <tr><td style="font-size: 16px; font-weight: bold; text-align: left; padding-left: 40px; padding-top: 5px;" colspan="2">YOUR PFS ID NUMBER: {!pfs.PFS_Number__c}</td></tr>
        </table>
        <!-- End Header -->
    
        <!--  Start Household Information -->
        <div style="width: 100%; font-weight: bolder; font-size: 16px; border-top: 1px solid; border-bottom: 1px solid; padding-top: 5px; padding-bottom: 5px;">Household Information</div> 
        <table style="width: 100%;">
            <tr >
                <td style="padding-left: 5px;"><apex:image value="{!URLFOR($Resource.FamilyPortalStyles, 'FamilyPortalStyles/images/ParentA.PNG')}"/></td>
                <td style="width: 97%;">    
                    <table style="width: 100%;">
                        <tr>
                            <td class="col1">First Name:</td><td class="col2"><apex:outputField value="{!pfs.Parent_A_First_Name__c}"/></td>
                            <td class="col1">Last Name:</td><td class="col2"><apex:outputField value="{!pfs.Parent_A_Last_Name__c}"/></td>
                        </tr>
                        <tr>
                            <td class="col1">Address:</td><td class="col2"><apex:outputField value="{!pfs.Parent_A_Address__c}"/></td>
                            <td class="col1">City:</td><td class="col2"><apex:outputField value="{!pfs.Parent_A_City__c}"/></td>
                        </tr>     
                        <tr>
                            <td class="col1">State/Province:</td><td class="col2"><apex:outputField value="{!pfs.Parent_A_State__c}"/></td>
                            <td class="col1">Zip/Postal Code:</td><td class="col2"><apex:outputField value="{!pfs.Parent_A_ZIP_Postal_Code__c}"/></td>
                        </tr>
                        <tr>
                            <td class="col1">Country:</td><td class="col2"><apex:outputField value="{!pfs.Parent_A_Country__c}"/></td>
                            <td class="col1">Date of Birth:</td><td class="col2"><apex:outputField value="{!pfs.Parent_A_Birthdate__c}"/></td>
                        </tr>  
                        <tr>
                            <td class="col1">Email:</td><td class="col2" colspan="3"><apex:outputField value="{!pfs.Parent_A_Email__c}"/></td>
                        </tr>                                                                   
                    </table>  
                </td>
            </tr>  
            <tr><td colspan="2" style="border-bottom: 1px dashed;"></td></tr>      
            <tr>
                <td style="padding-left: 5px;"><apex:image value="{!URLFOR($Resource.FamilyPortalStyles, 'FamilyPortalStyles/images/ParentB.PNG')}"/></td>
                <td style="width: 97%;">    
                    <table style="width: 100%;">
                        <tr>
                            <td class="col1">First Name:</td><td class="col2"><apex:outputField rendered="{!pfs.Student_Has_Parent_B__c == 'Yes'}" value="{!pfs.Parent_B_First_Name__c}"/></td>
                            <td class="col1">Last Name:</td><td class="col2"><apex:outputField rendered="{!pfs.Student_Has_Parent_B__c == 'Yes'}" value="{!pfs.Parent_B_Last_Name__c}"/></td>
                        </tr>
                        <tr>
                            <td class="col1">Address:</td><td class="col2"><apex:outputField rendered="{!pfs.Student_Has_Parent_B__c == 'Yes'}" value="{!pfs.Parent_B_Address__c}"/></td>
                            <td class="col1">City:</td><td class="col2"><apex:outputField rendered="{!pfs.Student_Has_Parent_B__c == 'Yes'}" value="{!pfs.Parent_B_City__c}"/></td>
                        </tr>     
                        <tr>
                            <td class="col1">State/Province:</td><td class="col2"><apex:outputField rendered="{!pfs.Student_Has_Parent_B__c == 'Yes'}" value="{!pfs.Parent_B_State__c}"/></td>
                            <td class="col1">Zip/Postal Code:</td><td class="col2"><apex:outputField rendered="{!pfs.Student_Has_Parent_B__c == 'Yes'}" value="{!pfs.Parent_B_ZIP_Postal_Code__c}"/></td>
                        </tr>
                        <tr>
                            <td class="col1">Country:</td><td class="col2"><apex:outputField rendered="{!pfs.Student_Has_Parent_B__c == 'Yes'}" value="{!pfs.Parent_B_Country__c}"/></td>
                            <td class="col1">Date of Birth:</td><td class="col2"><apex:outputField rendered="{!pfs.Student_Has_Parent_B__c == 'Yes'}" value="{!pfs.Parent_B_Birthdate__c}"/></td>
                        </tr>  
                        <tr>
                            <td class="col1">Email:</td><td class="col2"><apex:outputField rendered="{!pfs.Student_Has_Parent_B__c == 'Yes'}" value="{!pfs.Parent_B_Email__c}"/></td>
                            <td class="col1">&nbsp;</td><td class="col2">&nbsp;</td>
                        </tr>                                                                   
                    </table>  
                </td>
            </tr>    
        </table>            
        <!--  End Household Information -->

        <!--  Start Enclosed Documents -->    
        <div style="width: 100%; font-weight: bolder; font-size: 16px; border-top: 1px solid; border-bottom: 1px solid; padding-top: 5px; padding-bottom: 5px;">Your Enclosed Documents</div>            
        <apex:outputText style="color: #610169; font-weight: bold;" value="Indicate and enclose the documents you have been instructed by schools to send to SSS."/>
        <table style="width: 100%">
            <tr >
                <td style="width: 50%;">
                    <table style="width: 100%;">
                        <tr>
                            <td><div class="noselCheckbox"/></td>
                            <td style="padding-left: 5px; padding-right: 30px;">{!priorTaxYear}</td>
                            <td><div class="noselCheckbox"/></td>
                            <td style="padding-left: 5px; padding-right: 20px;">{!currentTaxYear}</td>
                            <td style="font-weight: bold; font-size: 11pt;">1040, 1040A or 1040EZ</td>
                        </tr> 
                        <tr>
                            <td><div class="noselCheckbox"/></td>
                            <td style="padding-left: 5px; padding-right: 30px;">{!priorTaxYear}</td>
                            <td><div class="noselCheckbox"/></td>
                            <td style="padding-left: 5px; padding-right: 20px;">{!currentTaxYear}</td>
                            <td style="font-weight: bold; font-size: 11pt;">W2 Form</td>
                        </tr> 
                        <tr>
                            <td><div class="noselCheckbox"/></td>
                            <td style="padding-left: 5px; padding-right: 30px;">{!priorTaxYear}</td>
                            <td><div class="noselCheckbox"/></td>
                            <td style="padding-left: 5px; padding-right: 20px;">{!currentTaxYear}</td>
                            <td style="font-weight: bold; font-size: 11pt;">State Tax Form</td>
                        </tr>    
                        <tr>
                            <td><div class="noselCheckbox"/></td>
                            <td style="padding-left: 5px; padding-right: 30px;">{!priorTaxYear}</td>
                            <td><div class="noselCheckbox"/></td>
                            <td style="padding-left: 5px; padding-right: 20px;">{!currentTaxYear}</td>
                            <td style="font-weight: bold; font-size: 11pt;">Schedule C</td>
                        </tr>                                      
                        <tr>
                            <td><div class="noselCheckbox"/></td>
                            <td style="padding-left: 5px; padding-right: 30px;">{!priorTaxYear}</td>
                            <td><div class="noselCheckbox"/></td>
                            <td style="padding-left: 5px; padding-right: 20px;">{!currentTaxYear}</td>
                            <td style="font-weight: bold; font-size: 11pt;">1099 Form</td>
                        </tr>                          
                    </table>
                </td>
                <td style="width: 50%;">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 5%; vertical-align: top; padding-top: 3px;"><div class="noselCheckbox"/></td>
                            <td style="padding-left: 5px; width: 45%;"><label style="font-weight: bold;">School/Organization Supplemental Form</label> <label style="font-style: italic;">(include full school name or SS code number)</label></td>
                            <td style="width: 50%;"><div style="border: 1px solid; height: 65px; margin-right: 25px;"/></td>
                        </tr>
                        <tr>
                            <td style="width: 5%; vertical-align: top; padding-top: 2px;"><div class="noselCheckbox"/></td>
                            <td style="padding-left: 5px; width: 40%; font-weight: bold; vertical-align: top;">Other Form</td>
                            <td style="width: 55%;"><div style="border: 1px solid; height: 65px; margin-right: 25px;"/></td>
                        </tr>            
                    </table>
                </td>
            </tr>
        </table>
        <!--  End Enclosed Documents -->

        <!--  Start Mailing Instructions --> 
        <div style="width: 100%; font-weight: bolder; font-size: 16px; border-top: 1px solid; border-bottom: 1px solid; padding-top: 5px; padding-bottom: 5px;">Mailing Instructions</div>           
        <apex:outputText style="color: #610169; font-weight: bold;" value="Gather all tax and required documentation and bundle with this cover sheet.  Do not staple or put checks or forms on top of this cover sheet."/>
        <table style="width: 100%;">
            <tr>
                <td style="width: 50%;">
                    <apex:outputText style="color: #610169; font-weight: bold;" value="Mail cover sheet and documentation to:"/><br/>
                    <div style="padding-left: 10px; font-weight: bold;">
                       {!$Label.Company_Name_Full}<br />
                       PO Box 449<br />
                       Randolph, MA 02368-0449
                    </div>
                </td>
                <td style="vertical-align: top; width: 50%;  font-weight: bold;"> 
                    For Office Use Only<br/>
                    <div style="margin-right: 25px; border: 1px solid; height: 75px;"/>
                </td>
            </tr>
        </table>
        <table style="width: 100%;">
            <tr>
                <td>
                    <apex:outputText style="color: #610169; font-weight: bold;" value="For overnight Federal Express, UPS, and Postal Services Express Mail:"/><br/>
                    <div style="padding-left: 10px; font-weight: bold;">
                       {!$Label.Company_Name_Full}<br />
                       15 Dan Road<br />
                       Suite 102<br />
                       Canton, MA 02021
                    </div>
                </td>
            </tr>
        </table>       
        <table style="width: 100%;">
            <tr>
                <td style="width: 70%; font-size: 10px; vertical-align: bottom;">
                    {!$Label.Copyright_Info}
                </td>
            </tr>
        </table>
        <!--  End Mailing Instructions -->

    </div>              
</apex:component>