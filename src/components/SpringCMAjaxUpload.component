<apex:component controller="SpringCMAjaxUploadController" allowDML="true">
    <apex:attribute name="thePFSNumber" description="PFS Number (e.g., 16-002958841)" type="String" required="true" assignTo="{!pfsNumber}" />
    <apex:attribute name="theFamilyDoc" description="Family Doc Record" type="Family_Document__c" required="true" assignTo="{!fDoc}" />
    <apex:attribute name="theHHId" description="Household Id" type="String" required="true" assignTo="{!hhId}" />
    <apex:attribute name="theHHName" description="Household Name" type="String" required="true" assignTo="{!hhName}" />
    <apex:attribute name="theVerifyFlag" description="DB Verify Flag" type="String" required="true" assignTo="{!verifyFlag}" />     <!-- SFP-29 -->

    <apex:actionRegion >

    <script src="/soap/ajax/35.0/connection.js" type="text/javascript"></script>
    <apex:includeScript value="{!URLFOR($Resource.Jqgrid, 'js/jquery-1.11.0.min.js')}" />
    <apex:stylesheet value="{!URLFOR($Resource.JQueryUI, 'jquery-ui-1.11.4.custom/jquery-ui.min.css')}" />
    <apex:includeScript value="{!URLFOR($Resource.AjaxUploaderAndJQuery1110)}" />
    <script src="/soap/ajax/28.0/apex.js" type="text/javascript"></script>
    <apex:stylesheet value="{!URLFOR($Resource.SpringCMLogoCSS, 'springcm/springcmlogocss.css')}" />
    
<!-- SFP-71, SFP-138 START -->
    <apex:outputPanel rendered="{!token == 'ERROR'}" layout="none">
        <span style="color:red">{!scmAuthenticationError}</span>
    </apex:outputPanel>
<!-- SFP-71, SFP-138 END -->
    
    <input id="fileupload" type="file" name="files" style="display: none"></input><!--<input type='button' value='Upload' onclick="$('#fileupload').click()"></input>-->

    <ul class="toolbar" id="uploadToolbar">
        <!-- SFP-71 START -->
        <li style="{!If(isNull(scmAuthenticationError), '', 'display: none')}" onclick="if (supportAjaxUpload()) { $('#fileupload').click();} else {$('#specialupload\\:fallback\\:fallbackFile').click();}" id="upload" class="menuItem purplebutton beforeUpload" title="Upload your document" alt="Upload your document"><span class="alignLabelBottom"><span class="menuIconWrapper"><img src="{!URLFOR($Resource.SpringCMLogoCSS, 'springcm/blank.gif')}" class="sprite sprite-upload-large" /></span><span class="menuLabel">{!helpConfig.SpringCM_Upload_Label__c}</span></span></li>
        <!-- SFP-71 END -->
        <!-- BELOW BUTTON was added to show up after initial upload, with differen styling and language -->
        <li onclick="if (supportAjaxUpload()) { $('#fileupload').click();} else {$('#specialupload\\:fallback\\:fallbackFile').click();}" style="display: none" id="uploadreplace" class="menuItem afterUploadAjaxOnly" title="Replace this document with a new one" alt="Replace this document with a new one"><span class="alignLabelBottom"><span class="menuIconWrapper"><img src="{!URLFOR($Resource.SpringCMLogoCSS, 'springcm/blank.gif')}" class="sprite sprite-upload-large" /></span><span class="menuLabel">{!helpConfig.SpringCM_Replace_Label__c}</span></span></li>
        <li id="de51d93c-4c24-4f29-8f71-58d170633cf5" class="menuItemSeparator afterUploadAjaxOnly" style="display: none"></li>
        <li onclick="processCancelDelete();" id="cancel" class="menuItem" title="Cancel Upload" alt="Cancel Upload"><span class="alignLabelBottom"><span class="menuIconWrapper"><img src="{!URLFOR($Resource.SpringCMLogoCSS, 'springcm/blank.gif')}" class="sprite sprite-error-large" /></span><span class="menuLabel">{!helpConfig.SpringCM_Cancel_Label__c}</span></span></li>
        <li id="de51d93c-4c24-4f29-8f71-58d170633cf5" class="menuItemSeparator afterUpload" style="display: none"></li>
        <li onclick="currentPageNumber -= 1;GetPreview(); " id="e567ac55-47c0-47c4-8dc6-99235bb3c48e" style="display: none" class="menuItem afterUpload" title="" alt=""><span class="alignLabelBottom"><span class="menuIconWrapper"><img src="{!URLFOR($Resource.SpringCMLogoCSS, 'springcm/blank.gif')}" class="sprite sprite-arrow_up-large" /></span><span class="menuLabel">{!helpConfig.SpringCM_Previous_Label__c}</span></span></li>
        <li onclick="currentPageNumber += 1;GetPreview(); " id="dd7b9414-876e-45b8-a14d-10162a9b7e71" style="display: none" class="menuItem afterUpload" title="" alt=""><span class="alignLabelBottom"><span class="menuIconWrapper"><img src="{!URLFOR($Resource.SpringCMLogoCSS, 'springcm/blank.gif')}" class="sprite sprite-arrow_down-large" /></span><span class="menuLabel">{!helpConfig.SpringCM_Next_Label__c}</span></span></li>
        <li class="menuItemWidget afterUpload" style="display: none"><span class="alignLabelBottom"><span class="menuWidget"><input type="text" onchange="currentPageNumber = $(this).val();GetPreview();;" size="2" value="1" class="menuItemTextBox alignRight" id="pagenumber" /> / <span id="totalpagenumber" style="display: inline">1</span></span><span class="menuLabel">{!helpConfig.SpringCM_Page_Label__c}</span></span></li>
        <li id="c5ed5895-854c-46a1-a056-52471a886a60" style="display: none" class="menuItemSeparator afterUpload"></li>
        <li onclick="startWorkflow()" id="upload" class="menuItem afterUpload purplebutton" style="display: none"><span class="alignLabelBottom"><span class="menuIconWrapper"><img src="{!URLFOR($Resource.SpringCMLogoCSS, 'springcm/blank.gif')}" class="sprite sprite-success-large" /></span><span class="menuLabel">{!helpConfig.SpringCM_Submit_Label__c}</span></span></li>
        <li id="de51d93c-4c24-4f29-8f71-58d170633cf5" class="menuItemSeparator afterUpload" style="display: none"></li>
    </ul>

    <div id="browserbad" style="display:none;" class="browserbad">
        <h2 style="color:red">{!$Label.SpringCM_Unsupported_Browser}</h2>
        <br /><div style="color:red">{!$Label.SpringCM_Supported_Browsers}</div>
    </div>    

    <div id="progress">
        <div class="bar" style="width: 0%; display: none"></div>
    </div>
     <div id="fallbackUploadSection" style="display: none">
        <!--<apex:form id="fallback">-->
            Choose a File to Upload:<apex:inputFile id="fallbackFile" value="{!Document}" onchange="showSpinner(); $('#sendfile').click();" filename="{!DocumentName}" />
            <input id='sendfile' type="submit" value="Go" style="visibility:hidden"></input>
        <!--</apex:form>-->
        <input type='hidden' id='docurl' value='{!DocumentURL}'></input>
    </div>
    <input type='hidden' id='token' value='{!Token}' />
    <div class="spinner" style="display: none">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div>
        <img id="spinningGif" style="display:none"  src="{!URLFOR($Resource.Spinner)}"/>
    <div class="paddingContainer" style="display:none; overflow:scroll; max-height:400px;">
        <img class="pageContainer selected" id='preview' src='' />
    </div>

    <style>
        .bar {
            background: green none repeat scroll 0 0;
            height: 18px;
        }

        .spinner {
            margin: 100px auto;
            width: 50px;
            height: 30px;
            text-align: center;
            font-size: 10px;
        }

        .spinner > div {
            background-color: #333;
            height: 100%;
            width: 6px;
            display: inline-block;
            -webkit-animation: stretchdelay 1.2s infinite ease-in-out;
            animation: stretchdelay 1.2s infinite ease-in-out;
        }

        .spinner .rect2 {
            -webkit-animation-delay: -1.1s;
            animation-delay: -1.1s;
        }

        .spinner .rect3 {
            -webkit-animation-delay: -1.0s;
            animation-delay: -1.0s;
        }

        .spinner .rect4 {
            -webkit-animation-delay: -0.9s;
            animation-delay: -0.9s;
        }

        .spinner .rect5 {
            -webkit-animation-delay: -0.8s;
            animation-delay: -0.8s;
        }

        @-webkit-keyframes stretchdelay {
            0%, 40%, 100% {
                -webkit-transform: scaleY(0.4);
            }

            20% {
                -webkit-transform: scaleY(1.0);
            }
        }

        @keyframes stretchdelay {
            0%, 40%, 100% {
                transform: scaleY(0.4);
                -webkit-transform: scaleY(0.4);
            }

            20% {
                transform: scaleY(1.0);
                -webkit-transform: scaleY(1.0);
            }
        }

        .purplebutton {
            background-color: #6d5c89;
        }
    </style>

    <script type="text/javascript">
            var remoteAction_HasDuplicatedSpringCMDocument = '{!$RemoteAction.SpringCMAjaxUploadController.hasDuplicatedSpringCMDocument}';//Otherwise, We'll get error in the js file
            var remoteAction_PageCounterSpringCMDocument = '{!$RemoteAction.SpringCMAjaxUploadController.pageCounterSpringCMDocument}';//Otherwise, We'll get error in the js file
            var token = '{!Token}'; //Token
            var numberPageLimit = "{!numberPageLimit}";
            var isEnforcePageLimitActive = "{!isEnforcePageLimitActive}";
            var isTaxableDocument = "{!isTaxableDocument}";
            var sessionId = '{!$Api.Session_ID}'; //$Api.Session_ID
            var documentUrl = '{!DocumentURL}';
            var fallBackUrl = '{!fallBackURL}';
            var pageFallbackUrl = '{!$Page.SpringCMNewUploadTesting}';
            var sendDocEndpoint = '{!sendDocEndpoint}';
            var forceFallback = '{!ForceFallback}'
            var userId = '{!$User.Id}'
            var sitePrefix = '{!$Site.Prefix}';
            var checkExistingURL = '{!checkExistingURL}';
            var enableFileHashRestriction = '{!enableFileHashRestriction}';
            var hhName = '{!hhName}';
            var hhId = '{!hhId}';
            var documentYear = '{!fdoc.Document_Year__c}';
            var fdocName = '{!fDoc.Name}';
            var fdocId = '{!fDoc.Id}';
            var setEOSURL = '{!setEOSURL}';
            var moveDocumentURL = '{!moveDocumentURL}';
            var workflowURL = '{!workflowURL}';
            var databankFriendlyDocType = "{!DatabankFriendlyDocType}";
            var documentSource = "{!fDoc.Document_Source__c}";
            var pfsNumber = "{!pfsNumber}";
            var schoolCode = "{!fDoc.School_Code__c}";
            var verifyFlag = "{!verifyFlag}";

            var uploadSuccessMsg = "{!$Label.SpringCM_Upload_Success}";
            var uploadDuplicateMsg = "{!$Label.SpringCM_Upload_Duplicate}";
            var uploadInvalidTypeMsg = "{!$Label.SpringCM_Upload_Invalid_Type}";
            var uploadSubmitFailure = "{!$Label.SpringCM_Upload_Submit_Failure}";
            var uploadContentAlreadyExistsMsg = "{!$Label.File_Content_Already_Uploaded}";
            var uploadContentPageLimitMsg = "{!$Label.File_Content_Page_Limit}";
 
            var animation = false;
        var b = document.body.style;
        if(b.MozTransition=='' || b.WebkitTransition=='' || b.OTransition=='' || b.transition=='') {
            animation =true;
        } else {
            animation =false;
        }
          initUpload();
    </script>
    
   
    <apex:actionFunction name="processSubmitSuccess" action="{!processSubmitSuccess}" >
    </apex:actionFunction>

    <apex:actionFunction name="processCancelDelete" action="{!processCancelDelete}" >
    </apex:actionFunction>

    </apex:actionRegion>
    <script type="text/javascript">
        var __sfdcSessionId = '{!GETSESSIONID()}';
    </script>

</apex:component>