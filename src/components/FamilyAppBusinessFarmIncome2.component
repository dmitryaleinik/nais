<!-- NAIS-2494 Updated to support multiple businesses/farms -->
<apex:component controller="FamilyAppCompController">

    <apex:attribute name="pfsRecord" description="The PFS record that the component should work with" type="PFS__c" assignTo="{!pfs}" />
    <apex:attribute name="bfRecord" description="The Business Farm record that the component should work with" type="Business_Farm__c" assignTo="{!bf}" />
    <apex:attribute name="applicationUtility" assignTo="{!appUtils}" description="An instance of ApplicationUtils passed in from the main page" type="ApplicationUtils" required="true"/>

    <script type="text/javascript">
        jQuery(document).ready
        (
            function()
            {
                jQuery('.current').change
                (
                    function()
                    {
                        var values = 
                        [
                            !isNaN(ToNumber(jQuery('.business-gross-sales-current').val())) ? ToNumber(jQuery('.business-gross-sales-current').val()) : 0,
                            !isNaN(ToNumber(jQuery('.business-cost-current').val())) ? ToNumber(jQuery('.business-cost-current').val()) : 0,
                            !isNaN(ToNumber(jQuery('.business-other-income-current').val())) ? ToNumber(jQuery('.business-other-income-current').val()) : 0
                        ];
                        
                        var grossProfit = values[0] - values[1];
                        var totalIncome = grossProfit + values[2];
                        
                        jQuery('.business-gross-profit-current span').html('$' + grossProfit.formatMoney().replace('.00', ''));
                        jQuery('.business-gross-profit-current input').val(grossProfit);
                        
                        jQuery('.business-total-income-current span').html('$' + totalIncome.formatMoney().replace('.00', ''));
                        jQuery('.business-total-income-current input').val(totalIncome);
                    }
                );
                
                jQuery('.estimated').change
                (
                    function()
                    {
                        var values = 
                        [
                            !isNaN(ToNumber(jQuery('.business-gross-sales-estimated').val())) ? ToNumber(jQuery('.business-gross-sales-estimated').val()) : 0,
                            !isNaN(ToNumber(jQuery('.business-cost-estimated').val())) ? ToNumber(jQuery('.business-cost-estimated').val()) : 0,
                            !isNaN(ToNumber(jQuery('.business-other-income-estimated').val())) ? ToNumber(jQuery('.business-other-income-estimated').val()) : 0
                        ];

                        var grossProfit = values[0] - values[1];
                        var totalIncome = grossProfit + values[2];
                        
                        jQuery('.business-gross-profit-estimated span').html('$' + grossProfit.formatMoney().replace('.00', ''));
                        jQuery('.business-gross-profit-estimated input').val(grossProfit);
                        
                        jQuery('.business-total-income-estimated span').html('$' + totalIncome.formatMoney().replace('.00', ''));
                        jQuery('.business-total-income-estimated input').val(totalIncome);
                    }
                );
                
                jQuery('.current:first, .estimated:first').change();
            }
        );
        
        function ToNumber(s)
        {   // [DP] 02.18.2016 SFP-315 remove dollar signs, too!!!!!
            return parseFloat(s.replace(/,/g, '').replace('$', ''));
        }
    </script>

    <div class="block fp">
        <div class="blockbody nopadding">
            <apex:outputPanel styleClass="helpmessage" layout="block">
                <apex:outputText escape="false" value="{!appUtils.helpRecord.BI_Business_Income_Instruction__c}"/>
            </apex:outputPanel>
        
            <table cellpadding="3" cellspacing="0" class="datatable familyincometable taxable-income-worksheet detailList" border="0">
                <tr>
                    <th class="labelCol">&nbsp;</th>
                    <!-- NAIS-2494
                    <apex:outputPanel layout="none" rendered="{!showPriorColumn}">
                        <th class="labelCol">{!currentTaxYearLabel} (Estimated)</th>
                    </apex:outputPanel>
                    -->
                    <th class="labelCol">{!currentTaxYearLabel} (Actual)</th>
                    <th class="labelCol">{!futureTaxYearLabel} (Estimated)</th>
                </tr>
                <tr>
                    <td>
                        <apex:outputLabel value="{!appUtils.labels.Business_Gross_Sales_Current__c}" styleClass="questionLabel "/>
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Gross_Sales_Current__c.inlineHelpText != ''}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Gross_Sales_Current__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </td>
                    <!-- NAIS-2494
                    <apex:outputPanel layout="none" rendered="{!showPriorColumn}">
                        <td><apex:outputField value="{!priorYearPFS.Business_Gross_Sales_Est__c}"/></td>
                    </apex:outputPanel>
                    -->
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required business-gross-sales-current current" value="{!bf.Business_Gross_Sales_Current__c}" /></td>
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required business-gross-sales-estimated estimated" value="{!bf.Business_Gross_Sales_Est__c}" /></td>
                </tr>
                
                <tr>
                    <td>
                        <apex:outputLabel value="{!appUtils.labels.Business_Cost_of_Goods_Current__c}" styleClass="questionLabel "/>
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Cost_of_Goods_Current__c.inlineHelpText != ''}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Cost_of_Goods_Current__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </td>
                    <!-- NAIS-2494
                    <apex:outputPanel layout="none" rendered="{!showPriorColumn}">
                        <td><apex:outputField value="{!priorYearPFS.Business_Cost_of_Goods_Est__c}"/></td>
                    </apex:outputPanel>
                    -->
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required business-cost-current current" value="{!bf.Business_Cost_of_Goods_Current__c}" /></td>
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required business-cost-estimated estimated" value="{!bf.Business_Cost_of_Goods_Est__c}" /></td>
                </tr>
                
                <tr>
                    <td>
                        <apex:outputLabel value="{!appUtils.labels.Business_Gross_Profit_Current__c}" styleClass="questionLabel "/>
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Gross_Profit_Current__c.inlineHelpText != ''}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Gross_Profit_Current__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </td>
                    <!-- NAIS-2494
                    <apex:outputPanel layout="none" rendered="{!showPriorColumn}">
                        <td>
                            $<apex:outputText value="{0, number,#,###.##}"><apex:param value="{!priorYearPFS.Business_Gross_Profit_Est__c}" /></apex:outputText>
                        </td>
                    </apex:outputPanel>
                    -->
                    <td class="business-gross-profit-current">
                        <span><apex:outputField value="{!bf.Business_Gross_Profit_Current__c}" /></span>
                        <apex:inputHidden value="{!bf.Business_Gross_Profit_Current__c}" />
                    </td>
                    <td class="business-gross-profit-estimated">
                        <span><apex:outputField value="{!bf.Business_Gross_Profit_Est__c}" /></span>
                        <apex:inputHidden value="{!bf.Business_Gross_Profit_Est__c}" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <apex:outputLabel value="{!appUtils.labels.Business_Other_Income_Current__c}" styleClass="questionLabel "/>
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Other_Income_Current__c.inlineHelpText != ''}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Other_Income_Current__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </td>
                    <!-- NAIS-2494
                    <apex:outputPanel layout="none" rendered="{!showPriorColumn}">
                        <td><apex:outputField value="{!priorYearPFS.Business_Other_Income_Est__c}"/></td>
                    </apex:outputPanel>
                    -->
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required business-other-income-current current" value="{!bf.Business_Other_Income_Current__c}" /></td>
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required business-other-income-estimated estimated" value="{!bf.Business_Other_Income_Est__c}" /></td>
                </tr>
                
                <tr>
                    <td>
                        <apex:outputLabel value="{!appUtils.labels.Business_Total_Income_Current__c}" styleClass="questionLabel "/>
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Total_Income_Current__c.inlineHelpText != ''}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Total_Income_Current__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </td>
                    <!-- NAIS-2494
                    <apex:outputPanel layout="none" rendered="{!showPriorColumn}">
                        <td><apex:outputField value="{!priorYearPFS.Business_Total_Income_Est__c}"/></td>
                    </apex:outputPanel>
                    -->
                    <td class="business-total-income-current">
                        <span><apex:outputField value="{!bf.Business_Total_Income_Current__c}" /></span>
                        <apex:inputHidden value="{!bf.Business_Total_Income_Current__c}" />
                    </td>
                    <td class="business-total-income-estimated">
                        <span><apex:outputField value="{!bf.Business_Total_Income_Est__c}" /></span>
                        <apex:inputHidden value="{!bf.Business_Total_Income_Est__c}" />
                    </td>
                </tr>
            </table>
        </div>
    </div>

</apex:component>