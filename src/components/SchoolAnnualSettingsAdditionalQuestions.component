<apex:component controller="SchoolAnnualSettingsAddQuestionsCntrl">
    <apex:attribute name="annualSettings" description="Annual Settings Record for editing custom questions" type="Annual_Setting__c" assignTo="{!annualSettingsModel}" />
    <apex:attribute name="academicYear" description="Current academic year" type="Id" assignTo="{!academicYearId}" />
    <apex:stylesheet value="{!URLFOR($Resource.SchoolAnnualSettingsAdditionalQuestionsStyling)}" />

    <!-- SFP-688 -->
    <apex:outputPanel rendered="{!NOT(NoQuestionLabels)}">
        <apex:outputPanel rendered="{!customQuestionBankEditable}">
            <!-- SFP-604 [jB]-->
            <div class="marginTop15">
                <h2 class="mainTitle pageblocksection-header">
                    {!$Label.SchoolSettings_AddCustomQuestionBankTitle}

                    <font color="red">
                        <apex:outputText value="{!$Label.SchoolSettings_CustomQuestionBankNote}">
                            <apex:param value="{!annualSettingsModel.Academic_Year__r.Family_Portal_Start_Date__c}" />
                        </apex:outputText>
                    </font> &nbsp;
                </h2>
                <script type="text/javascript">
                    var checkCustomLimit = function() {
                        var limit = j$('[id$=customquestionlimitvalue]').text()
                        var used = j$('[id$=customquestionlimitusedvalue]').text()
                        if (parseInt(used) > parseInt(limit)) {
                            j$('[id$=customquestionlimitusedlabel]').css('color', 'red');
                        } else {
                            j$('[id$=customquestionlimitusedlabel]').css('color', 'black');
                        }
                    }
                </script>
                <apex:actionRegion >
                    <apex:pageBlockSection id="usecustomquestions" columns="2">
                        <apex:pageBlockSectionItem >
                            <apex:outputLabel value="{!labels.Use_Additional_Questions__c}" />
                            <apex:inputField styleClass="uaq-active" value="{!annualSettingsModel.Use_Additional_Questions__c}" required="true">
                                <apex:actionSupport event="onchange" reRender="customquestionbank,usecustomquestions,questionsValidationJS" />
                            </apex:inputField>
                        </apex:pageBlockSectionItem>
                        <apex:pageBlockSectionItem id="customquestionslabel" rendered="{!annualSettingsModel.Use_Additional_Questions__c == 'Yes'}">
                            <apex:outputLabel value="{!customQuestionLimitText}" />
                            <apex:outputLabel id="customquestionlimitusedlabel" value="{!customQuestionsLimitUsed}" />
                        </apex:pageBlockSectionItem>
                        <apex:outputText style="display:none;" id="customquestionlimitvalue" value="{!customQuestionLimit}" />
                        <apex:outputText style="display:none;" id="customquestionlimitusedvalue" value="{!countCustomQuestionsUsed}" />
                    </apex:pageBlockSection>
                    
                    <apex:outputPanel layout="block" styleClass="descriptiveText">
                        <apex:outputText value="{!$Label.SchoolSettings_SelectQuestionsBankTitle}" />
                    </apex:outputPanel>
                    
                    <apex:outputPanel id="customquestionbank">
                        <apex:pageBlockSection columns="1" collapsible="false" rendered="{!annualSettingsModel.Use_Additional_Questions__c == 'Yes'}">
                            <!-- Custom Question 1 -->
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_1__c']}">
                                <apex:inputField styleClass="cq-checkbox" value="{!annualSettingsModel.Custom_Question_1__c}">
                                    <apex:actionSupport event="onchange" reRender="usecustomquestions" oncomplete="checkCustomLimit();" />
                                </apex:inputField>
                                <apex:outputLabel value="{!labels.Custom_Question_1__c}" />
                            </apex:pageBlockSectionItem>
                            <!-- /Custom Question 1 -->
                            <!-- Custom Question 2 -->
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_2__c']}">
                                <apex:inputField styleClass="cq-checkbox" value="{!annualSettingsModel.Custom_Question_2__c}">
                                    <apex:actionSupport event="onchange" reRender="usecustomquestions" oncomplete="checkCustomLimit();" />
                                </apex:inputField>
                                <apex:outputLabel value="{!labels.Custom_Question_2__c}" />
                            </apex:pageBlockSectionItem>
                            <!-- /Custom Question 2 -->
                            <!-- Custom Question 3 -->
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_3__c']}">
                                <apex:inputField styleClass="cq-checkbox" value="{!annualSettingsModel.Custom_Question_3__c}">
                                    <apex:actionSupport event="onchange" reRender="usecustomquestions" oncomplete="checkCustomLimit();" />
                                </apex:inputField>
                                <apex:outputLabel value="{!labels.Custom_Question_3__c}" />
                            </apex:pageBlockSectionItem>
                            <!-- /Custom Question 3 -->
                            <!-- Custom Question 4 -->
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_4__c']}">
                                <apex:inputField styleClass="cq-checkbox" value="{!annualSettingsModel.Custom_Question_4__c}">
                                    <apex:actionSupport event="onchange" reRender="usecustomquestions" oncomplete="checkCustomLimit();" />
                                </apex:inputField>
                                <apex:outputLabel value="{!labels.Custom_Question_4__c}" />
                            </apex:pageBlockSectionItem>
                            <!-- /Custom Question 4 -->
                            <!-- Custom Question 5 -->
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_5__c']}">
                                <apex:inputField styleClass="cq-checkbox" value="{!annualSettingsModel.Custom_Question_5__c}">
                                    <apex:actionSupport event="onchange" reRender="usecustomquestions" oncomplete="checkCustomLimit();" />
                                </apex:inputField>
                                <apex:outputLabel value="{!labels.Custom_Question_5__c}" />
                            </apex:pageBlockSectionItem>
                            <!-- /Custom Question 5 -->
                            <!-- Custom Question 6 -->
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_6__c']}">
                                <apex:inputField styleClass="cq-checkbox" value="{!annualSettingsModel.Custom_Question_6__c}">
                                    <apex:actionSupport event="onchange" reRender="usecustomquestions" oncomplete="checkCustomLimit();" />
                                </apex:inputField>
                                <apex:outputLabel value="{!labels.Custom_Question_6__c}" />
                            </apex:pageBlockSectionItem>
                            <!-- /Custom Question 6 -->
                            <!-- Custom Question 7 -->
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_7__c']}">
                                <apex:inputField styleClass="cq-checkbox" value="{!annualSettingsModel.Custom_Question_7__c}">
                                    <apex:actionSupport event="onchange" reRender="usecustomquestions" oncomplete="checkCustomLimit();" />
                                </apex:inputField>
                                <apex:outputLabel value="{!labels.Custom_Question_7__c}" />
                            </apex:pageBlockSectionItem>
                            <!-- /Custom Question 7 -->
                            <!-- Custom Question 8 -->
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_8__c']}">
                                <apex:inputField styleClass="cq-checkbox" value="{!annualSettingsModel.Custom_Question_8__c}">
                                    <apex:actionSupport event="onchange" reRender="usecustomquestions" oncomplete="checkCustomLimit();" />
                                </apex:inputField>
                                <apex:outputLabel value="{!labels.Custom_Question_8__c}" />
                            </apex:pageBlockSectionItem>
                            <!-- /Custom Question 8 -->
                            <!-- Custom Question 9 -->
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_9__c']}">
                                <apex:inputField styleClass="cq-checkbox" value="{!annualSettingsModel.Custom_Question_9__c}">
                                    <apex:actionSupport event="onchange" reRender="usecustomquestions" oncomplete="checkCustomLimit();" />
                                </apex:inputField>
                                <apex:outputLabel value="{!labels.Custom_Question_9__c}" />
                            </apex:pageBlockSectionItem>
                            <!-- /Custom Question 9 -->
                            <!-- Custom Question 10 -->
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_10__c']}">
                                <apex:inputField styleClass="cq-checkbox" value="{!annualSettingsModel.Custom_Question_10__c}">
                                    <apex:actionSupport event="onchange" reRender="usecustomquestions" oncomplete="checkCustomLimit();" />
                                </apex:inputField>
                                <apex:outputLabel value="{!labels.Custom_Question_10__c}" />
                            </apex:pageBlockSectionItem>
                            <!-- /Custom Question 10 -->
                            <!-- Custom Question 11 -->
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_11__c']}">
                                <apex:inputField styleClass="cq-checkbox" value="{!annualSettingsModel.Custom_Question_11__c}">
                                    <apex:actionSupport event="onchange" reRender="usecustomquestions" oncomplete="checkCustomLimit();" />
                                </apex:inputField>
                                <apex:outputLabel value="{!labels.Custom_Question_11__c}" />
                            </apex:pageBlockSectionItem>
                            <!-- /Custom Question 11 -->
                            <!-- Custom Question 12 -->
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_12__c']}">
                                <apex:inputField styleClass="cq-checkbox" value="{!annualSettingsModel.Custom_Question_12__c}">
                                    <apex:actionSupport event="onchange" reRender="usecustomquestions" oncomplete="checkCustomLimit();" />
                                </apex:inputField>
                                <apex:outputLabel value="{!labels.Custom_Question_12__c}" />
                            </apex:pageBlockSectionItem>
                            <!-- /Custom Question 12 -->
                            <!-- Custom Question 13 -->
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_13__c']}">
                                <apex:inputField styleClass="cq-checkbox" value="{!annualSettingsModel.Custom_Question_13__c}">
                                    <apex:actionSupport event="onchange" reRender="usecustomquestions" oncomplete="checkCustomLimit();" />
                                </apex:inputField>
                                <apex:outputLabel value="{!labels.Custom_Question_13__c}" />
                            </apex:pageBlockSectionItem>
                            <!-- /Custom Question 13 -->
                            <!-- Custom Question 14 -->
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_14__c']}">
                                <apex:inputField styleClass="cq-checkbox" value="{!annualSettingsModel.Custom_Question_14__c}">
                                    <apex:actionSupport event="onchange" reRender="usecustomquestions" oncomplete="checkCustomLimit();" />
                                </apex:inputField>
                                <apex:outputLabel value="{!labels.Custom_Question_14__c}" />
                            </apex:pageBlockSectionItem>
                            <!-- /Custom Question 14 -->
                            <!-- Custom Question 15 -->
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_15__c']}">
                                <apex:inputField styleClass="cq-checkbox" value="{!annualSettingsModel.Custom_Question_15__c}">
                                    <apex:actionSupport event="onchange" reRender="usecustomquestions" oncomplete="checkCustomLimit();" />
                                </apex:inputField>
                                <apex:outputLabel value="{!labels.Custom_Question_15__c}" />
                            </apex:pageBlockSectionItem>
                            <!-- /Custom Question 15 -->
                        </apex:pageBlockSection>
                    </apex:outputPanel>
                </apex:actionRegion>
            </div>
        </apex:outputPanel>
        <apex:outputPanel rendered="{!NOT(customQuestionBankEditable)}">
            <!-- SFP-604 [jB]-->
            <div class="cqBank-not-editable marginTop15">
                <h2 class="mainTitle pageblocksection-header">{!$Label.SchoolSettings_CustomQuestionBankTitle}&nbsp;</h2>
                <apex:actionRegion >
                    <apex:pageBlockSection columns="2">
                        <apex:pageBlockSectionItem >
                            <apex:outputLabel value="{!labels.Use_Additional_Questions__c}" />
                            <span class="uaq-inactive">{!annualSettingsModel.Use_Additional_Questions__c}</span>
                        </apex:pageBlockSectionItem>
                    </apex:pageBlockSection>

                    <apex:outputPanel >
                        <apex:pageBlockSection columns="1" collapsible="false" rendered="{!annualSettingsModel.Use_Additional_Questions__c == 'Yes'}">
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_1__c']}">
                                <apex:outputField value="{!annualSettingsModel.Custom_Question_1__c}" />
                                <apex:outputLabel value="{!labels.Custom_Question_1__c}" />
                            </apex:pageBlockSectionItem>
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_2__c']}">
                                <apex:outputField value="{!annualSettingsModel.Custom_Question_2__c}" />
                                <apex:outputLabel value="{!labels.Custom_Question_2__c}" />
                            </apex:pageBlockSectionItem>
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_3__c']}">
                                <apex:outputField value="{!annualSettingsModel.Custom_Question_3__c}" />
                                <apex:outputLabel value="{!labels.Custom_Question_3__c}" />
                            </apex:pageBlockSectionItem>
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_4__c']}">
                                <apex:outputField value="{!annualSettingsModel.Custom_Question_4__c}" />
                                <apex:outputLabel value="{!labels.Custom_Question_4__c}" />
                            </apex:pageBlockSectionItem>
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_5__c']}">
                                <apex:outputField value="{!annualSettingsModel.Custom_Question_5__c}" />
                                <apex:outputLabel value="{!labels.Custom_Question_5__c}" />
                            </apex:pageBlockSectionItem>
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_6__c']}">
                                <apex:outputField value="{!annualSettingsModel.Custom_Question_6__c}" />
                                <apex:outputLabel value="{!labels.Custom_Question_6__c}" />
                            </apex:pageBlockSectionItem>
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_7__c']}">
                                <apex:outputField value="{!annualSettingsModel.Custom_Question_7__c}" />
                                <apex:outputLabel value="{!labels.Custom_Question_7__c}" />
                            </apex:pageBlockSectionItem>
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_8__c']}">
                                <apex:outputField value="{!annualSettingsModel.Custom_Question_8__c}" />
                                <apex:outputLabel value="{!labels.Custom_Question_8__c}" />
                            </apex:pageBlockSectionItem>
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_9__c']}">
                                <apex:outputField value="{!annualSettingsModel.Custom_Question_9__c}" />
                                <apex:outputLabel value="{!labels.Custom_Question_9__c}" />
                            </apex:pageBlockSectionItem>
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_10__c']}">
                                <apex:outputField value="{!annualSettingsModel.Custom_Question_10__c}" />
                                <apex:outputLabel value="{!labels.Custom_Question_10__c}" />
                            </apex:pageBlockSectionItem>
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_11__c']}">
                                <apex:outputField value="{!annualSettingsModel.Custom_Question_11__c}" />
                                <apex:outputLabel value="{!labels.Custom_Question_11__c}" />
                            </apex:pageBlockSectionItem>
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_12__c']}">
                                <apex:outputField value="{!annualSettingsModel.Custom_Question_12__c}" />
                                <apex:outputLabel value="{!labels.Custom_Question_12__c}" />
                            </apex:pageBlockSectionItem>
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_13__c']}">
                                <apex:outputField value="{!annualSettingsModel.Custom_Question_13__c}" />
                                <apex:outputLabel value="{!labels.Custom_Question_13__c}" />
                            </apex:pageBlockSectionItem>
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_14__c']}">
                                <apex:outputField value="{!annualSettingsModel.Custom_Question_14__c}" />
                                <apex:outputLabel value="{!labels.Custom_Question_14__c}" />
                            </apex:pageBlockSectionItem>
                            <apex:pageblockSectionItem rendered="{!customQuestionHaslabel['Custom_Question_15__c']}">
                                <apex:outputField value="{!annualSettingsModel.Custom_Question_15__c}" />
                                <apex:outputLabel value="{!labels.Custom_Question_15__c}" />
                            </apex:pageBlockSectionItem>
                        </apex:pageBlockSection>
                    </apex:outputPanel>
                </apex:actionRegion>
            </div>
            <!-- END SFP-604 -->
        </apex:outputPanel>
    </apex:outputPanel>
    <!-- /SFP-688 -->
</apex:component>