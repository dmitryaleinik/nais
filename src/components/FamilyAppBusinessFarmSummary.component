<apex:component controller="FamilyAppBusinessFarmSummaryController" allowDML="true">
    <apex:attribute name="pfsRecord" description="The PFS record that the component should work with" type="PFS__c" assignTo="{!pfs}" />
    <apex:attribute name="applicationUtility" assignTo="{!appUtils}" description="An instance of ApplicationUtils passed in from the main page" type="ApplicationUtils" required="true"/>

    <apex:outputPanel id="bizFarmSummaryContainer">
        <div class="block fp">
            <div class="blockbody nopadding">
                <!-- Link to switch grouping-->
<!--    NO LONGER SWITCHING GROUPING [DP] 09.14.2015             
                <apex:commandButton value="Group by Business/Farm and Type" status="theactionstatus" reRender="bizFarmSummaryContainer" rendered="{!theGrouping='biz'}" oncomplete="preparechangemethods();">
                	<apex:param value="entity" name="grp"/>
                </apex:commandButton>
                <apex:commandButton value="Group by Business/Farm" status="theactionstatus" reRender="bizFarmSummaryContainer" rendered="{!theGrouping!='biz'}" oncomplete="preparechangemethods();">
                	<apex:param value="biz" name="grp"/>
                </apex:commandButton>
    	        <apex:actionStatus id="theactionstatus" startText="loading..." />

 -->                <apex:outputPanel id="business-farm-summary">
                	<br /><br />
                 	<apex:repeat value="{!BizOrFarmGroupWrappers}" var="group">
                		<apex:outputPanel layout="none" rendered="{!group.bizFarmList.size > 0}">
    			            <table cellpadding="3" cellspacing="0" class="detailList datatable familyincometable" border="0">
    			                <tr>
    			                	<th>
    			                		{!group.header1}
    			                	</th>
    			                	<apex:outputPanel layout="none" rendered="{!group.header2 != null}">
    			                		<th>
    			                			{!group.header2}
    			                		</th>
    			                	</apex:outputPanel>
    			                	<th>
    			                		Total Income
    			                	</th>
    			                	<th>
    			                		Total Expenses
    			                	</th>
    			                	<th>
    			                		Net Profit/Loss
    			                	</th>
    			                	<th>
    			                		Your Share
    			                	</th>
    			                </tr>
    			            	<apex:repeat value="{!group.bizFarmList}" var="bizFarm">

    				                <tr indexclass="{!group.indexString}">
    				                	<td>
    				                		<apex:outputText value="{!bizFarm.Name}" /><apex:outputText value=" - {!bizFarm.Business_Entity_Type__c}" rendered="{!group.header2 == null}"/>
    				                	</td>
    				                	<apex:outputPanel layout="none" rendered="{!group.header2 != null}">
    					                	<td>
    					                		<apex:outputText value="{!bizFarm.Business_Entity_Type__c}" />
    					                	</td>
    									</apex:outputPanel>
    				                	<td>
    				                		<apex:outputField styleClass="bizincome bizincome{!group.indexString}" value="{!bizFarm.Business_Total_Income_Current__c}" />
    				                	</td>
    				                	<td>
    				                		<apex:outputField styleClass="bizexpenses bizexpenses{!group.indexString}" value="{!bizFarm.Business_Total_Expenses_Current__c}" />
    				                	</td>
    				                	<td>
    				                		<apex:outputField styleClass="biznet biznet{!group.indexString}" value="{!bizFarm.Net_Profit_Loss_Business_Farm_Current__c}" />
    				                	</td>
    				                	<td>
                                        <!-- SFP-319 -->
    				                		<apex:inputField onChange="setChanged()" required="true" styleClass="required bizshare bizshare{!group.indexString}" value="{!bizFarm.Business_Net_Profit_Share_Current__c}" rendered="{!bizFarm.Business_Entity_Type__c != 'Sole Proprietorship'}"/>
                                            <apex:outputField styleClass="bizshare" value="{!bizFarm.Business_Net_Profit_Share_Current__c}" rendered="{!bizFarm.Business_Entity_Type__c == 'Sole Proprietorship'}" />
    				                	<!-- /SFP-319 -->
                                        </td>

    				                </tr>
    			                </apex:repeat>

    			                <tr>
    			                	<td>
    			                		<apex:outputText value="Totals" />
    			                	</td>
    			                	<apex:outputPanel layout="none" rendered="{!group.header2 != null}">
    			                		<td>
    			                		</td>
    			                	</apex:outputPanel>
    			                	<td class="bizincometotal{!group.indexString}">
    			                		<apex:outputField value="{!group.dummyBizFarm.Business_Total_Income_Current__c}" />
    			                	</td>
    			                	<td class="bizexpensestotal{!group.indexString}">
    			                		<apex:outputField value="{!group.dummyBizFarm.Business_Total_Expenses_Current__c}" />
    			                	</td>
    			                	<td class="biznettotal{!group.indexString}">
    			                		<apex:outputField value="{!group.dummyBizFarm.Net_Profit_Loss_Business_Farm_Current__c}" />
    			                	</td>
    			                	<td class="bizsharetotal{!group.indexString}"> 
    			                		<apex:outputField value="{!group.dummyBizFarm.Business_Net_Profit_Share_Current__c}" />
    			                	</td>
    			                </tr>
    			                <br />
    		                </table>
    	                </apex:outputPanel>
    	            </apex:repeat>
                </apex:outputPanel> 
            </div>
        </div>
	</apex:outputPanel>

    <script type="text/javascript">
      function esc(myid) {
         return '#' + myid.replace(/(:|\.)/g,'\\\\$1'); 
      }        
    </script>
    <script> 
        // jquery to sum up totals on the fly
        jQuery(document).ready
        (
            function()
            {
                preparechangemethods();
            }
        );
        
        function preparechangemethods()
        {
            jQuery('.bizincome').change
            (
                function()
                {
                    var indexclass = jQuery(this).closest('tr').attr('indexclass');
                    console.log('drewtesting!: ' + indexclass);
                    SumFields('.bizincome' + indexclass, 'bizincometotal' + indexclass);
                }
            );
            
            jQuery('.bizexpenses').change
            (
                function()
                {
                    var indexclass = jQuery(this).closest('tr').attr('indexclass');
                    SumFields('.bizexpenses' + indexclass, 'bizexpensestotal' + indexclass);
                }
            );
            
            jQuery('.biznet').change
            (
                function()
                {
                    var indexclass = jQuery(this).closest('tr').attr('indexclass');
                    SumFields('.biznet' + indexclass, 'biznettotal' + indexclass);
                }
            );
            
            jQuery('.bizshare').change
            (
                function()
                {
                    var indexclass = jQuery(this).closest('tr').attr('indexclass');
                    SumFields('.bizshare' + indexclass, 'bizsharetotal' + indexclass);
                }
            );
            
            jQuery('.bizincome, .bizexpenses, .biznet, .bizshare').change(); // call of these to get negative numbers in there
        }

        function SumFields(query, fieldClass)
        {
            var currentValues = jQuery(query).map
            (
                function(i, el)
                {
                    var value = 0;
                    if(!isNaN(ToNumber(jQuery(el).val()))) value = ToNumber(jQuery(el).val());
                    
                    return value; 
                }
            ).get();
            
            var total = 0;
            for(var i = 0; i < currentValues.length; i++) total = total + currentValues[i];
            
            jQuery('.' + fieldClass + ' span').html('$'+total.formatMoney().replace('.00', ''));
        }
        
        function ToNumber(s)
        {   // [DP] 02.18.2016 SFP-315 remove dollar signs, too!!!!!
            return parseFloat(s.replace(/,/g, '').replace('$', ''));
        }
    </script>

</apex:component>