<apex:component controller="FamilySelectSchools" allowDML="true">

    <apex:attribute name="pfsRecord" description="The PFS record that the component should work with" type="PFS__c" assignTo="{!pfs}" />
    <apex:attribute name="applicationUtility" assignTo="{!appUtils}" description="An instance of ApplicationUtils passed in from the main page" type="ApplicationUtils" required="true"/>

    <style>
        .schoolSelected{
            background-color: #AACCAA;
        }
        .wideColumn{
            min-width: 250px;
        }
        tr.greyed-out td{
            color:#969A9E;
        }
        .custPopup{
            background-color: white;
            border-width: 2px;
            border-style: solid;
            z-index: 9999;
            left: 50%;
            padding:10px;
            position: absolute;
            /* These are the 3 css properties you will need to change so the popup
            displays in the center of the screen. First set the width. Then set
            margin-left to negative half of what the width is. You can add
            the height property for a fixed size pop up if you want.*/
            width: 500px;
            margin-left: -250px;
            top:400px;
        }
        .popupBackground{
            background-color:black;
            opacity: 0.20;
            filter: alpha(opacity = 20);
            position: absolute;
            width: 100%;
            height: 300%;
            top: 0;
            left: 0;
            z-index: 9998;
        }
        .noPaddingOverwrite
        {
            padding: 0px !important;
        }
    </style>

    <script type="text/javascript">
        function search(event) {
            if (event.keyCode == 13) {
                document.getElementById('{!$Component.findSubscribers}').click();

                return false;
            }

            return true;
        }
        
        function disableSubmitButton() {
            disableButtonsWithStyle('.disableSubmitButton', 'disabled','Continue');
            disableButtonsWithStyle('.disableCancelButton', 'disabled','Cancel');
        }
        
        function enableSubmitButton() {
            enableButtonsWithStyle('.disableSubmitButton', 'disabled', 'Continue');
            enableButtonsWithStyle('.disableCancelButton', 'disabled', 'Cancel');
        }
    </script>

    <apex:stylesheet value="{!URLFOR($Resource.SchoolPortalStyles, '/css/familyTableStyles.css')}"/>
    <apex:includeScript value="{!$Resource.ButtonEnablement}" />
    
    <apex:outputPanel id="everything" rendered="{!InitialRecordLoad}">

        <div id="mainColumnHeader" >
            <h2>
                <span>
                    <apex:outputLabel value="{!appUtils.mainLabel}"/>
                </span>
                <apex:outputLabel value="{!appUtils.subLabel}"/>
            </h2>
            <div style="margin: 10px;">

                <apex:commandButton value="{!$Label.Main_Save_and_Next_Button}" styleClass="primary" onclick="saveAction('saveAndContinue');return false;" disabled="{!NOT(SaveIsActive)}" rerender="mainForm"/>
                <apex:commandButton value="{!$Label.Main_Save_and_Exit_Button}" onclick="saveAction('saveAndExit');return false;" disabled="{!NOT(SaveIsActive)}" rerender="mainForm"/>
                <apex:commandButton value="{!$Label.Main_Cancel_Button}" action="{!cancel}" />
            </div>
        </div>

        <apex:pageMessages rendered="{!OR(searchError, saveSuccessful)}"/>

        <div class="block fp">
            <apex:outputPanel layout="block" id="lastYearsSchools"
                              rendered="{!AND(returningSchools != NULL, isDisplayPriorSchoolsToFamilies)}">
                <c:FamilySchools isSearchResults="false" familySelectSchoolsController="{!controllerInstance}"/>
            </apex:outputPanel>

            <div class="blockheader noPaddingOverwrite" >
                Look Up SSS Subscriber Schools or Organizations &nbsp;
                <apex:actionStatus startText=" (loading...)" stopText="" id="everythingStatus"/>
                <apex:actionStatus startText=" (loading..)" stopText="" id="searchStatus"/>
            </div>
            <div class="blockbody nopadding" > <!-- //SFP-15 [G.S], adding city and zipcode -->
                <table>
                    <tr width="100%">
                        <td width="100%">
                            <table>
                                <tr>
                                    <th>Enter SSS Code</th>
                                    <th></th>
                                    <th>Enter School Name</th>
                                </tr>
 
                                <tr>
                                    <td>
                                        <apex:inputText value="{!sssCode}" onkeypress="return search(event);"/>
                                    </td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td colspan="2">
                                        <apex:inputText value="{!schoolName}" size="80" onkeypress="return search(event);"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr width="100%">
                        <td width="100%">
                            <table>
                                <tr>
                                    <th>Select Country</th>
                                    <th></th>
                                    <th>State/Province</th>
                                    <th></th>
                                    <th>Enter City</th>
                                    <th></th>
                                    <th>Enter Zip Code</th>
                                </tr>

                                <tr>
                                    <td><apex:inputField value="{!dummyContact.MailingCountryCode}" /></td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td><apex:inputField id="statespicklist" style="width:150px; margin-right:5px;" value="{!dummyContact.MailingStateCode}"/></td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td><apex:inputField value="{!dummyContact.MailingCity}" style="width: 170px;"/></td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td><apex:inputField value="{!dummyContact.MailingPostalCode}"/></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr><td align="center">
                        <apex:commandButton action="{!findSubscribers}" value="Find SSS Subscribers" rerender="everything" style="margin-top:10px" styleClass="primary" status="RequestStatus" id="findSubscribers"/>
                    </td></tr>

                </table>
                <br />

                <apex:outputPanel id="results" >
                    <apex:outputPanel styleClass="fpBodyTextBold">
                        <apex:outputText value="Schools and Organization Found: {!resultSchools.size}" rendered="{!searchHasOccurred}" /> &nbsp;
                    </apex:outputPanel>
                    <apex:outputPanel rendered="{!AND(searchHasOccurred, resultSchools.size > 0)}">
                        (<apex:commandLink styleClass="fpLinkNormal" value="Clear Search Criteria" action="{!clearResults}" rerender="everything" status="RequestStatus"/>)
                    </apex:outputPanel>
                    <apex:outputPanel layout="block" id="toppageblock" rendered="{!AND(searchHasOccurred, resultSchools.size > 0)}">
                        Click the "select" button next to each School or Organization you are applying to in order to add them to your Selected Schools/Organizations
                        list below. Once you have selected one or more schools/organizations, you can perform another search.
                        <br/><br/>
                        <c:FamilySchools isSearchResults="true" familySelectSchoolsController="{!controllerInstance}"/>
                    </apex:outputPanel>

                    <apex:outputText rendered="{!AND(searchHasOccurred, resultSchools.size = 0)}"
                        styleclass="loginOrange"
                        value="We could not find any SSS Subscribers for your selected location or SSS code. Please verify the search criteria you entered." >
                    </apex:outputText>
                </apex:outputPanel>
            </div>
        </div>

        <!-- render this if schools have already been selected (either in this page or from previously saved records) OR there are search results -->
        <apex:outputPanel layout="none" rendered="{!OR(resultSchools.size > 0, SchoolSelectList.size > 1)}">
            <div class="block fp">

                <div class="blockheader noPaddingOverwrite"  >
                    Assign Applicants to Schools or Organizations &nbsp;<apex:actionStatus startText=" (loading...)" stopText="" id="applicantStatus"/>
                </div>
                <div class="blockbody nopadding" >
                    Once you have selected Schools or Organizations above, they will appear in your selection list below. You can now assign one or more schools for each applicant.
                    <br />

                    <apex:outputText rendered="{!NOT(SchoolSelectList.size > 1)}" value="Find and select one or more Subscriber Schools/Organizations above to activate this section." />
                    <apex:pageMessages rendered="{!schoolError}"/>

                    <apex:outputPanel layout="none" rendered="{!SchoolSelectList.size > 1}" >
                        <div class="selectTable">
                             <div class="selectRow schoolListHeaderRow">
                                 <div class="selectCell">
                                    Selected Schools:
                                    <apex:repeat value="{!SchoolSelectList}" var="school">
                                        <apex:outputPanel layout="none" rendered="{!NOT(school.Value = SchoolSelectList[0].Value)}">
                                            <span style="font-style: normal; font-weight: normal;">
                                                <apex:outputPanel layout="none" rendered="{!NOT(school.Value = SchoolSelectList[1].Value)}">, </apex:outputPanel>
                                                {!school.Label}
                                            </span>
                                        </apex:outputPanel>
                                    </apex:repeat>
                                </div>
                               </div>
                           </div>
                       </apex:outputPanel>
                </div>
            </div>
            <apex:repeat value="{!ApplicantWrappers}" var="app">
                <div class="block">
                    <div class="blockheader">
                        <span class="collapsable"></span>Applicant: {!app.name}
                    </div>
                    <div class="blockbody nopadding">
                        <table class="list detailList fpTable">
                            <thead>
                                <tr>
                                    <th class="wideColumn">School Name</th>
                                    <th>Address</th>
                                    <th>SSS Code</th>
                                    <th>Day/Boarding</th>
                                    <th>
                                        <span class="hasToolTip">
                                             Currently Enrolled?&nbsp;<apex:image value="/img/msg_icons/info16.png" />
                                             <ul class="toolTip">
                                                <li>Is the applicant currently enrolled at this school?</li>
                                            </ul>
                                         </span>
                                     </th>
                                    <th>Submission Deadline</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                                 <apex:repeat value="{!app.activeSPFSWrappers}" var="spfs">
                                     <tr style="display: {!IF(ksSchoolID == spfs.spfs.School__c, '','none')}">
                                         <td colspan="7">
                                             <font color="#FF0000"><b>There is additional information for you to complete for this school.</b></font>
                                         </td>
                                     </tr>
                                     <tr class="applicantTableRow">
                                         <td width="20%">
                                             <apex:outputText value="{!spfs.schoolName}" rendered="{!NOT(spfs.spfs.Id = null)}"/>
	                                             <apex:selectList value="{!spfs.spfs.School__c}" multiSelect="false" size="1" rendered="{!spfs.spfs.Id = null}" disabled="{!NOT(SchoolSelectList.size > 1)}" styleclass="required">
	                                             <apex:actionSupport event="onchange" onComplete="setChanged()" action="{!spfs.schoolChange}" rerender="everything" status="RequestStatus"/>
	                                             <apex:selectOptions value="{!SchoolSelectList}" >
	                                             </apex:selectOptions>
                                             </apex:selectList>
                                             <apex:outputText value="***" styleclass="reqAst" rendered="{!spfs.hasErrors}" />
                                         </td>
                                         <td width="30%">
                                             {!spfs.schoolAddress}
                                         </td>
                                         <td width="11%">
                                             {!spfs.sssCode}
                                         </td>
                                         <td width="11%">
                                             <!-- NAIS-2252 [DP] 02.05.2015 -->
                                             <apex:inputField onChange="setChanged()" value="{!spfs.folder.Day_Boarding__c}" rendered="{!spfs.bothDayAndBoarding}" styleclass="required" />
                                             <apex:outputField value="{!spfs.folder.Day_Boarding__c}" rendered="{!AND(NOT(spfs.bothDayAndBoarding), NOT(spfs.isAccessOrg))}"/>
                                         </td>
                                         <td width="13%">
                                             <apex:selectList value="{!spfs.folder.New_Returning__c}" multiSelect="false" size="1" rendered="{!AND(NOT(spfs.isAccessOrg), spfs.spfs.School__c != null)}" styleclass="required">
                                                <apex:actionSupport event="onchange" onComplete="setChanged()" rerender="everything" status="RequestStatus"/>
                                                <apex:selectOptions value="{!newReturningOptions}" />
                                            </apex:selectList>
                                         </td>
                                         <td width="13%">
                                             <apex:outputText value="Deadline Passed " rendered="{!spfs.deadlinePassed}" styleclass="deadlinepassed"/>
                                             <!-- render the new deadline for new students and all access orgs -->
                                            <apex:outputText value="{0,date,MM'/'dd'/'yyyy}"  rendered="{!AND((spfs.hardDeadline || spfs.softDeadline), OR(spfs.folder.New_Returning__c = 'New', spfs.isAccessOrg))}">
                                                <apex:param value="{!spfs.newDeadline}" />
                                            </apex:outputText>
                                             <!-- render the returning deadline returning students not in access orgs -->
                                            <apex:outputText value="{0,date,MM'/'dd'/'yyyy}"  rendered="{!AND((spfs.hardDeadline || spfs.softDeadline), spfs.folder.New_Returning__c = 'Returning', NOT(spfs.isAccessOrg))}">
                                                <apex:param value="{!spfs.returningDeadline}" />
                                            </apex:outputText>
                                         </td>
                                         <td>
                                            <apex:commandLink onclick="setChanged()" value="X" action="{!removeApplicantRow}" rerender="everything" status="RequestStatus" title="Withdraw" style="color: red;">
                                                <apex:param name="indexId" value="{!spfs.indexId}"/>
                                            </apex:commandLink>
                                         </td>
                                     </tr>
                                 </apex:repeat>
                             </tbody>
                         </table>
                         <div style="padding: 5px; font-size: 12px;">
                            <apex:commandLink onclick="setChanged()" value="Add another school" action="{!addApplicantRow}"
                                rerender="everything" status="RequestStatus" rendered="{!SchoolSelectList.size > 1}">
                                <apex:param name="appIdParam" value="{!app.appId}"/>
                            </apex:commandLink>
                        </div>
                     </div>
                 </div>
            </apex:repeat>
        </apex:outputPanel>

        <div id="mainColumnFooter" style="margin: 10px;">
            <apex:commandButton value="{!$Label.Main_Save_and_Next_Button}" styleClass="primary" onclick="saveAction('saveAndContinue');return false;" disabled="{!NOT(SaveIsActive)}" rerender="mainForm"/>
            <apex:commandButton value="{!$Label.Main_Save_and_Exit_Button}" onclick="saveAction('saveAndExit');return false;" disabled="{!NOT(SaveIsActive)}" rerender="mainForm"/>
            <apex:commandButton value="{!$Label.Main_Cancel_Button}" action="{!cancel}" />
        </div>
    </apex:outputPanel>
    <apex:actionFunction name="saveAction" action="{!saveAction}"  status="RequestStatus"  rerender="mainForm,familySelectSchoolsPopup_EZ" oncomplete="renderPopup();">
        <apex:param name="var1" value="" assignTo="{!saveOption}"/>
    </apex:actionFunction>
    <apex:actionFunction name="renderPopup" action="{!renderPopup}"  status="{!IF(NOT(showPopupFlag),'RequestStatus','')}"  rerender="{!IF(showPopupFlag,'familySelectSchoolsPopup,sidebar','sidebar')}" />
    <apex:actionFunction name="saveAndContinue" action="{!saveAndContinue}" />
    <apex:actionFunction name="saveAndExit" action="{!saveAndExit}" />
    <apex:actionFunction name="notifySchoolParentSearch" action="{!notifySchoolParentSearch}" rerender="">
        <apex:param name="selectedSchoolId" value="" />
    </apex:actionFunction>

    <!-- This page has its own save method so we need a javascript save action function in case they navigate away wihtout saving-->
    <apex:actionFunction name="saveTheRecordSelectSchools" action="{!SaveAction}" rerender="mainColContent, pagemessages, familyportaltopbarform, everything, expJavascript, expFamilySelectJavascript, familySelectSchoolsPopup_EZ, familySelectSchoolsPopup" status="RequestStatus" onComplete="setUnchangedFamilySelectLocal();">
        <apex:param name="param1" assignTo="{!saveOption}" value="saveAndContinue" />
    </apex:actionFunction>
    <apex:outputPanel id="expFamilySelectJavascript">
        <script type="text/javascript" >
            function setUnchangedFamilySelectLocal(){
                if({!saveSuccessful}){
                    // set the flag that tracks if unchanged has run to 'yes'
                    setUnchanged();
                }
            }
        </script>
    </apex:outputPanel>


    <apex:outputPanel id="familySelectSchoolsPopup">
        <apex:outputPanel rendered="{!showPopupFlag}">
        <apex:outputPanel styleClass="popupBackground"/>
        <apex:outputPanel id="searchBox" styleClass="custPopup" layout="block">
            <apex:PageBlock title="Alert">
                <apex:pageBlockSection columns="1">
                    <apex:pageBlockSectionItem >
                        <apex:outputText value="{!$Label.MIE_Monthly_Income_Schools_Selected}" escape="false"/>
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputPanel >
                            <apex:commandButton value="OK" title="OK" action="{!yesModal}" styleClass="closeButton" status="RequestStatus">
                                <apex:param name="param1" assignTo="{!showPopupFlag}" value="false" />
                            </apex:commandButton>
                        </apex:outputPanel>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
             </apex:PageBlock>
        </apex:outputPanel>
        </apex:outputPanel>
    </apex:outputPanel>
    
    <apex:actionStatus id="disableSubmitButton" onstart="disableSubmitButton();" onstop="enableSubmitButton();" />
    
    <!-- START: EZ Popup -->
    <apex:outputPanel id="familySelectSchoolsPopup_EZ">
        <apex:outputPanel rendered="{!showPopupFlagEZ}">
        <apex:outputPanel styleClass="popupBackground"/>
        <apex:outputPanel id="searchBox2" styleClass="custPopup" layout="block">
            <apex:PageBlock title="Alert">
                <apex:pageBlockSection columns="1">
                    <apex:pageBlockSectionItem >
                        <apex:outputText value="{!$Label.SelectedSchools_SubmissionProcessHasChanged}" escape="false"/>
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputPanel >
                            <apex:commandButton value="Continue" title="Continue" action="{!saveAction2}" rerender="mainForm,familySelectSchoolsPopup_EZ" oncomplete="renderPopup();" styleClass="btn primary disableSubmitButton" status="disableSubmitButton" style="height:22px;">
                                <apex:param name="param1" assignTo="{!showPopupFlagEZ}" value="false" />
                                <apex:param name="param2" assignTo="{!userAcceptRecalcSubmissionProcess}" value="true" />
                            </apex:commandButton>
                            <apex:commandButton value="Cancel" title="Cancel" action="{!cancelModal}" rerender="familySelectSchoolsPopup_EZ" styleClass="btn disableCancelButton" status="RequestStatus" style="height:22px;"/>
                        </apex:outputPanel>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
             </apex:PageBlock>
        </apex:outputPanel>
        </apex:outputPanel>
    </apex:outputPanel>
    <!-- END: EZ Popup -->
</apex:component>