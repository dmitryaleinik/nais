<apex:component controller="FamilyAppCompController" extensions="FamilyMonthlyIncomeAndExpensesController">
    <apex:attribute name="pfsRecord" description="The PFS record that the component should work with" type="PFS__c" assignTo="{!pfs}" />
    <apex:attribute name="applicationUtility" assignTo="{!appUtils}" description="An instance of ApplicationUtils passed in from the main page" type="ApplicationUtils" required="true"/>
    <apex:attribute name="showInputAsLabelFlag" assignTo="{!showInputAsLabel}" description="Should the values be rendered as input?" type="Boolean" required="false"/>
    
    <!-- IMPORTANT: We MUST do the required field validations in the controller. Since, we can not use "immediate"
         for actionFunction:recalcMIETotals. And that actionFunction is the one that  triggers the recalculation
         for summary section. Please, make sure to avoid use "required=true" for the inputs in this component. 
         And move the required validations to FamilyAppMainController.validateMonthlyIncomeExpenses 
     -->
    <div class="blockbody nopadding">
        <!-- Sarts : Monthly Miscellaneous Expenses Section-->
        <apex:pageBlockSection columns="1" collapsible="false">
            <apex:pageBlockSectionItem >
                <apex:outputPanel >
                    <apex:outputLabel value="{!appUtils.labels.Monthly_Expenses_Clothing__c}" styleClass="requiredText"  escape="false" />
                    <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!AND(NOT(showInputAsLabel),$ObjectType.PFS__c.Fields.Monthly_Expenses_Clothing__c.inlineHelpText != '')}">
                        <span class="helpicon"></span>
                        <ul class="toolTip">
                            <li><apex:outputText value="{!$ObjectType.PFS__c.Fields.Monthly_Expenses_Clothing__c.inlineHelpText}"/></li>
                        </ul>
                    </apex:outputPanel>
                </apex:outputPanel>
                <apex:actionRegion >
                    <apex:inputField rendered="{!NOT(showInputAsLabel)}" id="row39" value="{!pfs.Monthly_Expenses_Clothing__c}" styleClass="required" >
                        <apex:actionSupport event="onchange" rerender="total_8" oncomplete="setChanged();"/>
                    </apex:inputField>
                    <apex:outputPanel rendered="{!showInputAsLabel}">{!IF(ISNULL(pfs.Monthly_Expenses_Clothing__c),'$0','$'+TEXT(pfs.Monthly_Expenses_Clothing__c))}</apex:outputPanel>
                </apex:actionRegion>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputPanel >
                    <apex:outputLabel value="{!appUtils.labels.Monthly_Expenses_Pet_Care__c}" styleClass="requiredText"  escape="false" />
                    <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!AND(NOT(showInputAsLabel),$ObjectType.PFS__c.Fields.Monthly_Expenses_Pet_Care__c.inlineHelpText != '')}">
                        <span class="helpicon"></span>
                        <ul class="toolTip">
                            <li><apex:outputText value="{!$ObjectType.PFS__c.Fields.Monthly_Expenses_Pet_Care__c.inlineHelpText}"/></li>
                        </ul>
                    </apex:outputPanel>
                </apex:outputPanel>
                <apex:actionRegion >
                    <apex:inputField rendered="{!NOT(showInputAsLabel)}" id="row40" value="{!pfs.Monthly_Expenses_Pet_Care__c}" styleClass="required" >
                        <apex:actionSupport event="onchange" rerender="total_8" oncomplete="setChanged();"/>
                    </apex:inputField>
                    <apex:outputPanel rendered="{!showInputAsLabel}">{!IF(ISNULL(pfs.Monthly_Expenses_Pet_Care__c),'$0','$'+TEXT(pfs.Monthly_Expenses_Pet_Care__c))}</apex:outputPanel>
                </apex:actionRegion>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputPanel >
                    <apex:outputLabel value="{!appUtils.labels.Monthly_Expenses_Bus_Metro_Rail__c}" styleClass="requiredText"  escape="false" />
                    <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!AND(NOT(showInputAsLabel),$ObjectType.PFS__c.Fields.Monthly_Expenses_Bus_Metro_Rail__c.inlineHelpText != '')}">
                        <span class="helpicon"></span>
                        <ul class="toolTip">
                            <li><apex:outputText value="{!$ObjectType.PFS__c.Fields.Monthly_Expenses_Bus_Metro_Rail__c.inlineHelpText}"/></li>
                        </ul>
                    </apex:outputPanel>
                </apex:outputPanel>
                <apex:actionRegion >
                    <apex:inputField rendered="{!NOT(showInputAsLabel)}" id="row41" value="{!pfs.Monthly_Expenses_Bus_Metro_Rail__c}" styleClass="required" >
                        <apex:actionSupport event="onchange" rerender="total_8" oncomplete="setChanged();"/>
                    </apex:inputField>
                    <apex:outputPanel rendered="{!showInputAsLabel}">{!IF(ISNULL(pfs.Monthly_Expenses_Bus_Metro_Rail__c),'$0','$'+TEXT(pfs.Monthly_Expenses_Bus_Metro_Rail__c))}</apex:outputPanel>
                </apex:actionRegion>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputPanel >
                    <apex:outputLabel value="{!appUtils.labels.Monthly_Expenses_Savings__c}" styleClass="requiredText"  escape="false" />
                    <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!AND(NOT(showInputAsLabel),$ObjectType.PFS__c.Fields.Monthly_Expenses_Savings__c.inlineHelpText != '')}">
                        <span class="helpicon"></span>
                        <ul class="toolTip">
                            <li><apex:outputText value="{!$ObjectType.PFS__c.Fields.Monthly_Expenses_Savings__c.inlineHelpText}"/></li>
                        </ul>
                    </apex:outputPanel>
                </apex:outputPanel>
                <apex:actionRegion >
                    <apex:inputField rendered="{!NOT(showInputAsLabel)}" id="row42" value="{!pfs.Monthly_Expenses_Savings__c}" styleClass="required" >
                        <apex:actionSupport event="onchange" rerender="total_8" oncomplete="setChanged();"/>
                    </apex:inputField>
                    <apex:outputPanel rendered="{!showInputAsLabel}">{!IF(ISNULL(pfs.Monthly_Expenses_Savings__c),'$0','$'+TEXT(pfs.Monthly_Expenses_Savings__c))}</apex:outputPanel>
                </apex:actionRegion>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputPanel >
                    <apex:outputLabel value="{!appUtils.labels.Monthly_Expenses_Entertainment__c}" styleClass="requiredText"  escape="false" />
                    <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!AND(NOT(showInputAsLabel),$ObjectType.PFS__c.Fields.Monthly_Expenses_Entertainment__c.inlineHelpText != '')}">
                        <span class="helpicon"></span>
                        <ul class="toolTip">
                            <li><apex:outputText value="{!$ObjectType.PFS__c.Fields.Monthly_Expenses_Entertainment__c.inlineHelpText}"/></li>
                        </ul>
                    </apex:outputPanel>
                </apex:outputPanel>
                <apex:actionRegion >
                    <apex:inputField rendered="{!NOT(showInputAsLabel)}" id="row43" value="{!pfs.Monthly_Expenses_Entertainment__c}" styleClass="required" >
                        <apex:actionSupport event="onchange" rerender="total_8" oncomplete="setChanged();"/>
                    </apex:inputField>
                    <apex:outputPanel rendered="{!showInputAsLabel}">{!IF(ISNULL(pfs.Monthly_Expenses_Entertainment__c),'$0','$'+TEXT(pfs.Monthly_Expenses_Entertainment__c))}</apex:outputPanel>
                </apex:actionRegion>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputPanel >
                    <apex:outputLabel value="{!appUtils.labels.MIE_Travel_Expenses__c}" styleClass="requiredText"  escape="false" />
                    <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!AND(NOT(showInputAsLabel),$ObjectType.PFS__c.Fields.MIE_Travel_Expenses__c.inlineHelpText != '')}">
                        <span class="helpicon"></span>
                        <ul class="toolTip">
                            <li><apex:outputText value="{!$ObjectType.PFS__c.Fields.MIE_Travel_Expenses__c.inlineHelpText}"/></li>
                        </ul>
                    </apex:outputPanel>
                </apex:outputPanel>
                <apex:actionRegion >
                    <apex:inputField rendered="{!NOT(showInputAsLabel)}" id="row44" value="{!pfs.MIE_Travel_Expenses__c}" styleClass="required" >
                        <apex:actionSupport event="onchange" rerender="total_8" oncomplete="setChanged();"/>
                    </apex:inputField>
                    <apex:outputPanel rendered="{!showInputAsLabel}">{!IF(ISNULL(pfs.MIE_Travel_Expenses__c),'$0','$'+TEXT(pfs.MIE_Travel_Expenses__c))}</apex:outputPanel>
                </apex:actionRegion>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputPanel >
                    <apex:outputLabel value="{!appUtils.labels.MIE_Student_Loan_Payments__c}" styleClass="requiredText"  escape="false" />
                    <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!AND(NOT(showInputAsLabel),$ObjectType.PFS__c.Fields.MIE_Student_Loan_Payments__c.inlineHelpText != '')}">
                        <span class="helpicon"></span>
                        <ul class="toolTip">
                            <li><apex:outputText value="{!$ObjectType.PFS__c.Fields.MIE_Student_Loan_Payments__c.inlineHelpText}"/></li>
                        </ul>
                    </apex:outputPanel>
                </apex:outputPanel>
                <apex:actionRegion >
                    <apex:inputField rendered="{!NOT(showInputAsLabel)}" id="row45" value="{!pfs.MIE_Student_Loan_Payments__c}" styleClass="required" >
                        <apex:actionSupport event="onchange" rerender="total_8" oncomplete="setChanged();"/>
                    </apex:inputField>
                    <apex:outputPanel rendered="{!showInputAsLabel}">{!IF(ISNULL(pfs.MIE_Student_Loan_Payments__c),'$0','$'+TEXT(pfs.MIE_Student_Loan_Payments__c))}</apex:outputPanel>
                </apex:actionRegion>
            </apex:pageBlockSectionItem>
        </apex:pageBlockSection>
        <!-- Ends : Monthly Miscellaneous Expenses Section-->
		
        <!-- Start: Other_Monthly_Entertainment_Expenses_Section -->
        <apex:outputPanel id="Other_Monthly_Entertainment_Expenses_Section" layout="block">
            <apex:pageBlockSection columns="1" collapsible="false">
                <apex:pageBlockSectionItem >
                    <apex:outputPanel >
                        <apex:outputLabel value="{!appUtils.labels.Other_Monthly_Miscellaneous_Expenses__c}" escape="false" />
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!AND(NOT(showInputAsLabel),$ObjectType.PFS__c.Fields.Other_Monthly_Miscellaneous_Expenses__c.inlineHelpText != '')}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.PFS__c.Fields.Other_Monthly_Miscellaneous_Expenses__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </apex:outputPanel>
                    <apex:actionRegion >
                        <apex:inputField StyleClass="otherMIE" rendered="{!NOT(showInputAsLabel)}" id="row46" value="{!pfs.Other_Monthly_Miscellaneous_Expenses__c}" required="false" onChange="setChanged()" />
                       <apex:outputPanel rendered="{!showInputAsLabel}">{!IF(ISNULL(pfs.Other_Monthly_Miscellaneous_Expenses__c),'$0','$'+TEXT(pfs.Other_Monthly_Miscellaneous_Expenses__c))}</apex:outputPanel>
                    </apex:actionRegion>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:outputPanel>
        <apex:outputPanel id="Other_Monthly_Entertainment_Expenses_Description" layout="block">
            <apex:pageBlockSection columns="1" collapsible="false" rendered="{!AND(NOT(ISNULL(pfs.Other_Monthly_Miscellaneous_Expenses__c)), pfs.Other_Monthly_Miscellaneous_Expenses__c>0)}">
                <apex:pageBlockSectionItem >
                    <apex:outputPanel >
                        <apex:outputLabel value="{!appUtils.labels.Other_Monthly_Miscellaneous_Explain__c}" styleClass="requiredText"  escape="false" />
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!AND(NOT(showInputAsLabel),$ObjectType.PFS__c.Fields.Other_Monthly_Miscellaneous_Explain__c.inlineHelpText != '')}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.PFS__c.Fields.Other_Monthly_Miscellaneous_Explain__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </apex:outputPanel>
                    <apex:actionRegion >
                        <apex:inputField id="row47" rendered="{!AND(NOT(showInputAsLabel),NOT(ISNULL(pfs.Other_Monthly_Miscellaneous_Expenses__c)))}" onChange="setChanged()" value="{!pfs.Other_Monthly_Miscellaneous_Explain__c}"  styleClass="required" />
                        <apex:outputPanel rendered="{!showInputAsLabel}">{!pfs.Other_Monthly_Miscellaneous_Explain__c}</apex:outputPanel>
                   </apex:actionRegion>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:outputPanel>
        <!-- End: Other_Monthly_Entertainment_Expenses_Section -->
        <apex:outputPanel id="total_8">
            <apex:pageBlockSection columns="1" collapsible="false">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Total"  escape="false" />
                    <apex:outputPanel >
                        <input value="{!totalMonthlyMiscellaneousExpenses}" style="{!IF(NOT(showInputAsLabel),'','display:none;')}background-color: lightgrey;" disabled="disabled" />
                        <apex:outputPanel rendered="{!showInputAsLabel}">{!IF(ISNULL(pfs.Monthly_Miscellaneous_Total__c),'$1','$'+TEXT(pfs.Monthly_Miscellaneous_Total__c))}</apex:outputPanel>
                   </apex:outputPanel>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:outputPanel>
    </div>
    </apex:component>