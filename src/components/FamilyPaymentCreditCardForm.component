<apex:component >
    <apex:attribute name="mainController" description="The main family payment controller" type="FamilyPaymentController" required="true"/>
    <apex:attribute name="readOnly" description="read-only flag" type="boolean"/>

    <div class="block fp">
        <div class="blockheader">
            Enter Credit / Debit Card Payment Information
        </div>

        <div class="blockbody detaillist">
            <p>If you are paying with a credit or debit card, the charge on your statement will appear as <b> "{!mainController.creditCardStatementDescriptor}."</b>
            <BR />Billing address should match the billing address on the credit/debit card. <br /><br /></p>

            <table class="paymentTable">
                <tbody>
                    <tr>
                        <td class="paymentTableLabelCol paymentTableHeading">BILLING CONTACT</td>
                        <td class="paymentTableDataCol">&nbsp;</td>
                        <td class="paymentTableLabelCol">&nbsp;</td>
                        <td class="paymentTableDataCol">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="paymentTableLabelCol">First Name</td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.paymentData.billingFirstName}" styleClass="paymentFieldRequired" tabindex="1" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.paymentData.billingFirstName}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.billingFirstName}" rendered="{!NOT(ISNULL(mainController.fieldErrors.billingFirstName))}" styleClass="paymentFieldError"/>
                        </td>
                        <td class="paymentTableLabelCol">&nbsp;</td>
                        <td class="paymentTableDataCol">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="paymentTableLabelCol">Last Name</td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.paymentData.billingLastName}" styleClass="paymentFieldRequired" tabindex="2" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.paymentData.billingLastName}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.billingLastName}" rendered="{!NOT(ISNULL(mainController.fieldErrors.billingLastName))}" styleClass="paymentFieldError"/>
                        </td>
                        <td class="paymentTableLabelCol">&nbsp;</td>
                        <td class="paymentTableDataCol">&nbsp;</td>

                    </tr>
                </tbody>
            </table>

            <table class="paymentTable">
                <tbody>
                    <tr>
                        <td class="paymentTableLabelCol paymentTableHeading">BILLING ADDRESS</td>
                        <td class="paymentTableDataCol">&nbsp;</td>
                        <td class="paymentTableLabelCol paymentTableHeading">CARD INFORMATION</td>
                        <td class="paymentTableDataCol">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="paymentTableLabelCol">Country</td>
                        <td class="paymentTableDataCol">
                            <apex:selectlist value="{!mainController.paymentData.billingCountry}" size="1"  styleClass="paymentFieldRequired" tabindex="5" rendered="{!NOT(readOnly)}">
                                <apex:actionSupport event="onchange" rerender="billing_state_input"/>
                                <apex:selectOption itemValue="NONE" itemLabel="" />
                                <apex:selectOptions value="{!mainController.billingCountrySelectOptions}"/>
                            </apex:selectlist>
                            <apex:outputText value="{!mainController.selectedCountryName}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.billingCountry}" rendered="{!NOT(ISNULL(mainController.fieldErrors.billingCountry))}" styleClass="paymentFieldError"/>
                        </td>
                        <td class="paymentTableLabelCol">Name on Card</td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.paymentData.ccNameOnCard}" styleClass="paymentFieldRequired" tabindex="11" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.paymentData.ccNameOnCard}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.ccNameOnCard}" rendered="{!NOT(ISNULL(mainController.fieldErrors.ccNameOnCard))}" styleClass="paymentFieldError"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="paymentTableLabelCol">Address Line 1</td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.paymentData.billingStreet1}" styleClass="paymentFieldRequired"  tabindex="6" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.paymentData.billingStreet1}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.billingStreet1}" rendered="{!NOT(ISNULL(mainController.fieldErrors.billingStreet1))}" styleClass="paymentFieldError"/>
                        </td>
                        <td class="paymentTableLabelCol">Credit Card Number</td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.paymentData.ccCardNumber}" styleClass="paymentFieldRequired ccCardNumber" tabindex="12" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.paymentData.ccCardNumber}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.ccCardNumber}" rendered="{!NOT(ISNULL(mainController.fieldErrors.ccCardNumber))}" styleClass="paymentFieldError"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="paymentTableLabelCol">Address Line 2</td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.paymentData.billingStreet2}" tabindex="7" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.paymentData.billingStreet2}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.billingStreet2}" rendered="{!NOT(ISNULL(mainController.fieldErrors.billingStreet2))}" styleClass="paymentFieldError"/>
                        </td>
                        <td class="paymentTableLabelCol">Card Type</td>
                        <td class="paymentTableDataCol">
                            <apex:selectList size="1" value="{!mainController.paymentData.ccCardType}" styleClass="paymentFieldRequired ccCardType" tabindex="13" rendered="{!NOT(readOnly)}">
                                <apex:selectOption itemValue="NONE" itemLabel="" />
                                <apex:selectOptions value="{!mainController.creditCardTypeSelectOptions}"/>
                            </apex:selectList>
                            <apex:outputText value="{!mainController.paymentData.ccCardType}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.ccCardType}" rendered="{!NOT(ISNULL(mainController.fieldErrors.ccCardType))}" styleClass="paymentFieldError"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="paymentTableLabelCol">City</td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.paymentData.billingCity}" styleClass="paymentFieldRequired" tabindex="8" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.paymentData.billingCity}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.billingCity}" rendered="{!NOT(ISNULL(mainController.fieldErrors.billingCity))}" styleClass="paymentFieldError"/>
                        </td>
                        <td class="paymentTableLabelCol">Expiration Date</td>
                        <td class="paymentTableDataCol">
                            <apex:selectlist value="{!mainController.paymentData.ccExpirationMM}" size="1"  styleClass="paymentFieldRequired" tabindex="14" rendered="{!NOT(readOnly)}">
                                <apex:selectOptions value="{!mainController.expireMonthSelectOptions}"/>
                            </apex:selectlist>
                            <apex:selectlist value="{!mainController.paymentData.ccExpirationYY}" size="1"  styleClass="paymentFieldRequired" tabindex="15" rendered="{!NOT(readOnly)}">
                                <apex:selectOption itemValue="NONE" itemLabel="" />
                                <apex:selectOptions value="{!mainController.expireYearSelectOptions}"/>
                            </apex:selectlist>
                            <apex:outputPanel rendered="{!readOnly}">
                                <apex:outputText value="{!mainController.paymentData.ccExpirationMM}" />/<apex:outputText value="{!mainController.paymentData.ccExpirationYY}" />
                            </apex:outputPanel>
                            <apex:outputText value="{!mainController.fieldErrors.ccExpiration}" rendered="{!NOT(ISNULL(mainController.fieldErrors.ccExpiration))}" styleClass="paymentFieldError"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="paymentTableLabelCol">State/Province</td>
                        <td class="paymentTableDataCol">
                            <apex:outputPanel id="billing_state_input">
                                <apex:outputPanel rendered="{!NOT(readOnly)}">
                                    <apex:selectlist value="{!mainController.paymentData.billingState}" size="1" styleClass="paymentFieldRequired" rendered="{!mainController.isUnitedStatesSelected}" tabindex="9" >
                                        <apex:selectOption itemValue="NONE" itemLabel="" />
                                        <apex:selectOptions value="{!mainController.billingStateUSSelectOptions}"/>
                                    </apex:selectlist>
                                    <!-- NAIS-2317 [DP] 03.05.2015 removed required style on non-US State -->
                                    <apex:inputText value="{!mainController.paymentData.billingState}" rendered="{!NOT(mainController.isUnitedStatesSelected)}" tabindex="9" />
                                </apex:outputPanel>
                                <apex:outputText value="{!mainController.paymentData.billingState}" rendered="{!readOnly}"/>
                                <apex:outputText value="{!mainController.fieldErrors.billingState}" rendered="{!NOT(ISNULL(mainController.fieldErrors.billingState))}" styleClass="paymentFieldError"/>
                            </apex:outputPanel>
                        </td>
                        <td class="paymentTableLabelCol">Security Code<br/>(also called a CVV2)</td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.paymentData.ccSecurityCode}" styleClass="paymentFieldRequired" tabindex="16" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.paymentData.ccSecurityCode}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.ccSecurityCode}" rendered="{!NOT(ISNULL(mainController.fieldErrors.ccSecurityCode))}" styleClass="paymentFieldError"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="paymentTableLabelCol">Zip/Postal Code</td>
                        <td class="paymentTableDataCol">
                            <apex:inputText value="{!mainController.paymentData.billingPostalCode}" styleClass="paymentFieldRequired" tabindex="10" rendered="{!NOT(readOnly)}"/>
                            <apex:outputText value="{!mainController.paymentData.billingPostalCode}" rendered="{!readOnly}"/>
                            <apex:outputText value="{!mainController.fieldErrors.billingPostalCode}" rendered="{!NOT(ISNULL(mainController.fieldErrors.billingPostalCode))}" styleClass="paymentFieldError"/>
                        </td>
                        <td class="paymentTableLabelCol">&nbsp;</td>
                        <td class="paymentTableDataCol">
                            <apex:outputPanel rendered="{!NOT(readOnly)}">
                                <img src="{!$Resource.CreditCardSecurityCode}"/>
                            </apex:outputPanel>
                            <apex:outputPanel rendered="{!readOnly}">
                                &nbsp;
                            </apex:outputPanel>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div id="paymentTotal">Total:&nbsp;<apex:outputText value="{0, number,$###,###,##0.00}"><apex:param value="{!mainController.paymentData.paymentAmount}" /></apex:outputText></div>


        </div>
    </div>
</apex:component>