<apex:component controller="FamilySelectSchools" allowDML="true">
    <apex:attribute name="familySelectSchoolsController" assignTo="{!controllerInstance}" type="FamilySelectSchools" description="Sets if it's a component with search results or with returning schools"/>
    <apex:attribute name="isSearchResults" assignTo="{!isSearchResultsComponent}" type="Boolean" description="Sets if it's a component with search results or with returning schools"/>

    <apex:outputPanel rendered="{!NOT(isSearchResults)}">
        <div class="blockheader noPaddingOverwrite"  >
            {!$Label.Last_Year_Schools_Table_Header}
        </div>
        <div class="blockbody nopadding" >
            {!$Label.Last_Year_Schools_Table_Description}
            <br />
        </div>
    </apex:outputPanel>

    <div class="resultsListContainer block">
        <div class="tableScroll">
            <table class="resultsList">
                <thead>
                    <tr>
                        <th></th>
                        <th><span class="headerLabel">School Name</span></th>
                        <th><span class="headerLabel">Address</span></th>
                        <th><span class="headerLabel">SSS Code</span></th>
                    </tr>
                </thead>
                <tbody>
                    <apex:repeat value="{!IF(isSearchResults, controllerInstance.resultSchools, controllerInstance.returningSchools)}" var="sch">
                        <tr class="{!IF(NOT(sch.IsAnnualSettingUpdated),'greyed-out',IF(sch.currentlySelected,'schoolSelected',''))}">
                            <td>
                                <apex:outputPanel layout="none" rendered="{!NOT(AND(controllerInstance.IsSubmitted, sch.currentlyExisting))}" >
                                    <apex:commandButton action="{!controllerInstance.selectSchool}"
                                        value="Select"
                                        rerender="everything"
                                        status="RequestStatus"
                                        rendered="{!AND(sch.IsAnnualSettingUpdated, NOT(sch.currentlySelected))}"
                                        styleClass="secondary">
                                        <apex:param name="selectedSchoolId" value="{!sch.schoolId}"/>
                                    </apex:commandButton>
                                    <apex:commandButton action="{!controllerInstance.removeSchool}"
                                        value="Remove"
                                        rerender="everything"
                                        status="RequestStatus"
                                        rendered="{!AND(sch.IsAnnualSettingUpdated,sch.currentlySelected)}">
                                        <apex:param name="selectedSchoolId" value="{!sch.schoolId}"/>
                                    </apex:commandButton>
                                    <apex:commandButton value="More Information" immediate="true"
                                        onclick="notifySchoolParentSearch('{!sch.schoolId}');alert('{!$Label.Message_for_School_Without_Annual_Setting}');return false;"
                                        rendered="{!NOT(sch.IsAnnualSettingUpdated)}"/>
                                </apex:outputPanel>
                            </td>
                            <td>{!sch.schoolName}</td>
                            <td>{!sch.schoolAddress}</td>
                            <td>{!sch.sssCode}</td>
                        </tr>
                    </apex:repeat>
                </tbody>
            </table>
        </div>
    </div>
</apex:component>