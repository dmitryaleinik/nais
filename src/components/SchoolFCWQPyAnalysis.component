<apex:component controller="SchoolFCWQPyAnalysis" >
    <apex:attribute name="name" type="string"  description="name of row"/>
    <apex:attribute name="valueType" type="string"  description="type (number or currency)"/>
    <apex:attribute name="lineNumbers" type="string"  description="lineNumbers on FCW"/>
    <apex:attribute name="status" type="string"  description="status of field"/>
    <apex:attribute name="rowData" type="PFSRowData" description="row" assignTo="{!row}"/>
    <apex:attribute name="label" type="string"  description="row label"/>
    <apex:attribute name="hasReviewed" type="boolean" description="row has been reviewed in this session"/>

    <apex:variable value="{!IF(row.currentYearValue > row.previousYearValue,'higher','lower')}" var="highlow" rendered="{!row.comparisonResult != null }"/>
    <apex:variable value="{!ABS(row.currentYearValue - row.previousYearValue)}" var="difference"/>
    <apex:variable value="{!ABS(row.comparisonResult)}" var="percent"/>
    <apex:variable value="{!IF(row.currentYearValue > row.previousYearValue,row.PositiveCommentary,row.NegativeCommentary)}" var="commentary" rendered="{!row.comparisonResult != null }"/>
    <apex:variable value="{!IF(name='totalTaxableIncome' || name ='totalNonTaxableIncome' || name ='TotalIncome' || name ='unusualExpense' || name ='TotalAllowances' || name='totalAssets' || name ='totalDebts' || name='netWorth' || name ='effectiveIncome' || name ='incomeSupplement' || name='adjustedEffectiveIncome' || name='revisedEffectiveIncome','Down','Up')}" var="upOrDown"/>

    <apex:outputPanel id="componentPanel">
        <apex:outputPanel rendered="{!row.isOverThreshold}" style="height:100%;width:100%;">

            <!-- the power tip text for the red warning icon -->
            <div class="powerTipContent" style="display:none;">
                This years' {!label} is&nbsp;
                <apex:outputText value="{!percent}%"/>
                     {!highlow}
                with a difference of
                <strong>
                    <apex:outputText value="{0, number, $#,###,##0}" rendered="{!valueType =='currency'}">
                                <apex:param value="{!difference}"/>
                    </apex:outputText>
                    <apex:outputText value="{0, number, #,###,##0}" rendered="{!valueType =='number'}">
                        <apex:param value="{!difference}"/>
                    </apex:outputText>.
                </strong>

                <!-- the warning icon should be grey or red depending on whether it has been marked as Not An Issue -->
                <apex:outputPanel >
                    <br />
                    <br />
                    <span>
                        <img class="thresholdAlert" src="{!IF(status == 'Not An Issue', URLFOR($Resource.failmsggrey),URLFOR($Resource.SchoolPortalStyles_Community, 'images/failMsgAlert.png'))}" />
                        {!IF(highlow == 'higher',row.PositiveCommentary,row.NegativeCommentary)}&nbsp; <apex:outputText escape="false" value="{!row.lineNumber}"/>
                    </span>
                </apex:outputPanel>
            </div>


           <!-- value that is shown on the lite page -->
            <span class="comp" style="text-align:center;">
                <!-- img that shows on the lite page -->
                <img class="thresholdAlert" src="{!IF(status == 'Not An Issue', URLFOR($Resource.failmsggrey),URLFOR($Resource.SchoolPortalStyles_Community, 'images/failMsgAlert.png'))}" style="height:15x;width:15px;"/>
                <apex:outputText styleClass="schoolRevised" value="{0, number, $#,###,##0}" rendered="{!valueType =='currency'}">
                    <apex:param value="{!row.currentYearValue}"/>
                </apex:outputText>
                <apex:outputText styleClass="schoolRevised" value="{0, number, #,###,##0}" rendered="{!valueType =='number'}">
                    <apex:param value="{!row.currentYearValue}"/>
                </apex:outputText>
            </span>

            <!-- the powertip text for the blue hover icon -->
            <span class="info">
                <apex:outputPanel rendered="{!!ISBLANK(rowData.comments)}">
                    <img id="{!name}-blueIcon" src="{!URLFOR($Resource.SchoolPortalStyles_Community, 'images/info.png')}"
                    onmouseover="showInfoModal{!name}();" onmouseleave="checkPosition{!name}()" />
                </apex:outputPanel>

                <!-- existing comments get injected here, protection for the page if their html is broken -->
                <div id="{!name}comments" style="display:none;" class="commentsDiv"></div>


            </span>
        </apex:outputPanel>

        <apex:outputPanel rendered="{!NOT(row.isOverThreshold)}">
<script type="text/javascript">
console.log('{!row.fieldName}'+'|cur|'+'{!row.currentYearValue}');
console.log('{!row.fieldName}'+'|prev|'+'{!row.previousYearValue}');
</script>
            <span style="text-align:center;">
                <apex:outputText styleClass="schoolRevised" value="{0, number, $#,###,##0}" rendered="{!valueType =='currency'}">
                                    <apex:param value="{!row.currentYearValue}"/>
                </apex:outputText>
                <apex:outputText styleClass="schoolRevised" value="{0, number, #,###,##0}" rendered="{!valueType =='number'}">
                                    <apex:param value="{!row.currentYearValue}"/>
                </apex:outputText>
            </span>
        </apex:outputPanel>

       <!-- FULL MODAL CONTENT -->
        <div style="position:relative">

            <!-- depending on row position on page, orient the modals up or down -->
            <span id="{!name}-powerTipFull" class="powerTipFull{!upOrDown}" onmouseenter="onModal{!name}();" onmouseleave="offModal{!name}();">
              <a onclick="document.getElementById('startReviewButton').style.display ='inline';stopReview()" style="right:10px;top:2px;" class="xLink">x</a>
                            <div class="spinner" id="spinner-{!name}" style="display:none;">
                            <img style="width:5%;height:5%;display:block;left:50%;bottom:75px;position:absolute;" src="{!URLFOR($Resource.loadingwheel)}"/>
                            </div>
                            <!-- this overlay covers the modal functionality while an action is being processed -->
                            <div style="position:absolute;margin-top:0px;margin-left:0px;height:100%;width:100%;z-index:60;background-opacity:5%;display:none;" class="spinner" id="spinner-{!name}2"/>
                <apex:outputPanel rendered="{!row.isOverThreshold}" styleClass="fullTip">
                    <div id="{!name}-powertip">
                     Prior year verification was
                        <strong>
                            <apex:outputText value="{0, number, $#,###,##0}" rendered="{!valueType =='currency'}">
                                <apex:param value="{!row.previousYearValue}"/>
                            </apex:outputText>
                            <apex:outputText value="{0, number, #,###,##0}" rendered="{!valueType =='number'}">
                                <apex:param value="{!row.previousYearValue}"/>
                            </apex:outputText>.
                        </strong>
                        The current year revision is
                        <strong>
                            <apex:outputText value="{!percent}%"/>
                        </strong>
                            {!highlow} than the prior year with a difference of
                    <strong>
                        <apex:outputText value="{0, number, $#,###,##0}" rendered="{!valueType =='currency'}">
                            <apex:param value="{!ABS(row.currentYearValue - row.previousYearValue)}"/>
                        </apex:outputText>
                        <apex:outputText value="{0, number, #,###,##0}" rendered="{!valueType =='number'}">
                            <apex:param value="{!ABS(row.currentYearValue - row.previousYearValue)}"/>
                        </apex:outputText>.
                    </strong>


                            <br />
                            <br />
                            <span>
                                {!IF(highlow == 'higher',row.PositiveCommentary,row.NegativeCommentary)}&nbsp;
                                    <apex:outputText escape="false" value="{!row.lineNumber}"/>
                                <!--<br/> For more detail please check the <a href="" onclick="">full FCW line {!lineNumbers}</a>-->
                            </span>
                        <p/>

                        <!-- existing comments get injected here, protection for the page if their html is broken -->
                        <div style="max-height:200px;overflow-y:scroll;" id="{!name}comments-box"></div>

                        <p/>
                        <textArea id="comment-{!name}" placeholder="Add a comment here." style="width:100%" cols="40" rows="3"/>
                        <p/>


                        <apex:form >
                          <div id="threeButtons{!name}">
                              <apex:commandlink id="RevisitLater" styleClass="roundedButton grey" onclick="showSpinner{!name}();disableButtons();markStatus('Revisit Later','{!name}',document.getElementById('comment-{!name}').value);" value="Revisit Later" status="marking" reRender="blank"/>
                              &nbsp;<apex:commandlink id="NotAnIssue" styleClass="roundedButton {!IF(hasReviewed,'green','grey')}" onclick="showSpinner{!name}();markStatus('Not An Issue','{!name}',document.getElementById('comment-{!name}').value);" value="Not An Issue" status="marking" reRender="blank"/>
                              &nbsp;<apex:commandlink id="ReviewFCW" styleClass="roundedButton {!IF(hasReviewed,'grey','green')}" value="Review FCW" status="marking" reRender="blank" onclick="if(hasIframeLoaded)loadFCW{!name}()"/>
                          </div>
                           <div id="reopenButton{!name}">
                              <apex:commandlink styleClass="roundedButton grey" onclick="disableButtons();markStatus('Reopened','{!name}',document.getElementById('comment-{!name}').value);" value="Reopen Issue" status="marking" rendered="{!status=='Not An Issue'}" oncomplete="document.getElementById('startReviewButton').style.display ='inline'" reRender="blank"/>
                          </div>

                        </apex:form>
                    </div>
                </apex:outputPanel>
            </span>
        </div>
    </apex:outputPanel>
   
    <script>
        var onmodal{!name} = false;

        function showInfoModal{!name}(){
            if(isReview == false){
                document.getElementById('{!name}-powerTipFull').style.display = 'block';
                document.getElementById('threeButtons{!name}').style.display = 'none';
                document.getElementById('reopenButton{!name}').style.display = 'inline';
                document.getElementById('comment-{!name}').style.display = 'none';
            }
        }

        function onModal{!name}(){
            if(isReview == false){
                console.log('detected on modal {!name}, onmodal:' + onmodal{!name});
                onmodal{!name} = true;
            }
        }

        function offModal{!name}(){
            if(isReview == false){
                console.log('detected on modal {!name}, onmodal:' + onmodal{!name});
                document.getElementById('{!name}-powerTipFull').style.display = 'none';
                onmodal{!name} = false;
                document.getElementById('threeButtons{!name}').style.display = 'inline';
                document.getElementById('reopenButton{!name}').style.display = 'none';
                document.getElementById('comment-{!name}').style.display = 'inline';
            }
        }

        function checkPosition{!name}(){
            console.log('checking position {!name}, onmodal:' + onmodal{!name});
            if(isReview == false)
              setTimeout( hideModal{!name} ,300);
        }

        function hideModal{!name}(){
            console.log('called hide modal {!name}');
            if(onmodal{!name} == false){
            console.log('hidemodal and onmodal is false {!name}, onmodal:' + onmodal{!name});
            document.getElementById('{!name}-powerTipFull').style.display = 'none';
            document.getElementById('threeButtons{!name}').style.display = 'inline';
             document.getElementById('reopenButton{!name}').style.display = 'none';
             document.getElementById('comment-{!name}').style.display = 'inline';
            onmodal{!name} = false;
            }

        }

        setFCWLinks{!name}();

        function setFCWLinks{!name}(){
            //if('{!name}' == 'totalTaxableIncome'){
            console.log('***SETTING LINK***');
            console.log('{!name}');
                var a = document.getElementsByClassName('{!name}Link');
                console.log('A: ');
                console.dir(a);
                for(var x=0;x<a.length;x++){
                    a[x].setAttribute('onclick','isFCWLink = true; loadFCW{!name}();');
                    if(x == 0)
                      a[x].style.color = '#CCE';
                }
            //}
        }

        appendComments('comments');
        appendComments('comments-box');
        // this function injects the comments into the respective divs and prevents breaking the page if the comment html itself is broken
        
        function appendComments(which){
            var target{!name} = document.getElementById('{!name}'+which);
            if(target{!name} != null){
                var div{!name} = document.createElement('div');
                var tmpComment = '{!JSENCODE(rowData.comments)}';
                tmpComment = tmpComment != null ? tmpComment.replace(/\n/g , '<br />') : '';
                div{!name}.innerHTML = tmpComment;
                target{!name}.appendChild(div{!name});
            }
        }

        // function to open the FCW and passes in the appropriate values of this row to populate the yellow bar accordingly
        function loadFCW{!name}(){
            markStatus('Review FCW','{!name}',document.getElementById('comment-{!name}').value);

            console.log('loadFCW in component name,value : {!name} {!difference}');
            showFCW('{!rowNameToProperName[name]}','{!percent}','{!highlow}','{!difference}',"{!commentary}",'{!row.lineNumber}', '{!valueType}', '{!name}', true);
        }

        function reloadFCWFromSave{!name}(){
            console.log('loadFCW in component name,value : {!name} {!difference}');
            showFCW('{!rowNameToProperName[name]}','{!percent}','{!highlow}','{!difference}',"{!commentary}",'{!row.lineNumber}', '{!valueType}', '{!name}',false);
        }

        function showSpinner{!name}(){
            document.getElementById('spinner-{!name}').style.display = 'inline';
            document.getElementById('spinner-{!name}2').style.display = 'inline';
        }
    </script>

    <style>
        .powerTipFullUp {
            position: absolute;
            bottom:30px;
            left:-215px;
            display: none;
            background-color: white;
            min-height:200px;
            width:440px;
            opacity:1;
            z-index: 40;
            border: solid #333;
            border-width: 1px;
            border-radius: 5px;
            padding:20px;
        }

        .powerTipFullUp:after, .powerTipFullUp:before {
            top: 100%;
            left: 50%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
        }

        .powerTipFullUp:after {
            border-color: rgba(255, 255, 255, 0);
            border-top-color: #ffffff;
            border-width: 10px;
            margin-left: -10px;
        }
        .powerTipFullUp:before {
            border-color: rgba(51, 51, 51, 0);
            border-top-color: #333;
            border-width: 11px;
            margin-left: -11px;
        }


        .powerTipFullDown {
            position: absolute;
            top:10px;
            left:-215px;
            display: none;
            background-color: white;
            min-height:200px;
            width:440px;
            opacity:1;
            z-index: 40;
            border: solid #333;
            border-width: 1px;
            border-radius: 5px;
            padding:20px;
        }

        .powerTipFullDown:after, .powerTipFullDown:before {
            bottom: 100%;
            left: 50%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
        }

        .powerTipFullDown:after {
            border-color: rgba(255, 255, 255, 0);
            border-bottom-color: #ffffff;
            border-width: 10px;
            margin-left: -10px;
        }
        .powerTipFullDown:before {
            border-color: rgba(51, 51, 51, 0);
            border-bottom-color: #333;
            border-width: 11px;
            margin-left: -11px;
        }
   </style>

</apex:component>