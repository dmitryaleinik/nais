<apex:component controller="FamilyAppCompController" extensions="FamilyMonthlyIncomeAndExpensesController">
    <apex:attribute name="pfsRecord" description="The PFS record that the component should work with" type="PFS__c" assignTo="{!pfs}" />
    <apex:attribute name="applicationUtility" assignTo="{!appUtils}" description="An instance of ApplicationUtils passed in from the main page" type="ApplicationUtils" required="true"/>
    <apex:attribute name="showInputAsLabelFlag" assignTo="{!showInputAsLabel}" description="Should the values be rendered as input?" type="Boolean" required="false"/>
    
    <!-- IMPORTANT: We MUST do the required field validations in the controller. Since, we can not use "immediate"
         for actionFunction:recalcMIETotals. And that actionFunction is the one that  triggers the recalculation
         for summary section. Please, make sure to avoid use "required=true" for the inputs in this component. 
         And move the required validations to FamilyAppMainController.validateMonthlyIncomeExpenses 
     -->
    
    <div class="blockbody nopadding">
        <apex:pageBlockSection columns="1" collapsible="false">
            <apex:pageBlockSectionItem >
                <apex:outputPanel >
                    <apex:outputLabel value="{!appUtils.labels.Monthly_Expenses_Vehicle_Payments__c}" styleClass="requiredText"  escape="false" />
                    <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!AND(NOT(showInputAsLabel),$ObjectType.PFS__c.Fields.Monthly_Expenses_Vehicle_Payments__c.inlineHelpText != '')}">
                        <span class="helpicon"></span>
                        <ul class="toolTip">
                            <li><apex:outputText value="{!$ObjectType.PFS__c.Fields.Monthly_Expenses_Vehicle_Payments__c.inlineHelpText}"/></li>
                        </ul>
                    </apex:outputPanel>
                </apex:outputPanel>
                <apex:actionRegion >
                    <apex:inputField rendered="{!NOT(showInputAsLabel)}" id="row24" value="{!pfs.Monthly_Expenses_Vehicle_Payments__c}" styleClass="required" >
                        <apex:actionSupport event="onchange" rerender="total_5" oncomplete="setChanged();"/>
                    </apex:inputField>
                    <apex:outputPanel rendered="{!showInputAsLabel}">{!IF(ISNULL(pfs.Monthly_Expenses_Vehicle_Payments__c),'$0','$'+TEXT(pfs.Monthly_Expenses_Vehicle_Payments__c))}</apex:outputPanel>
                </apex:actionRegion>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputPanel >
                    <apex:outputLabel value="{!appUtils.labels.Monthly_Expenses_Vehicle_Insurance__c}" styleClass="requiredText"  escape="false" />
                    <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!AND(NOT(showInputAsLabel),$ObjectType.PFS__c.Fields.Monthly_Expenses_Vehicle_Insurance__c.inlineHelpText != '')}">
                        <span class="helpicon"></span>
                        <ul class="toolTip">
                            <li><apex:outputText value="{!$ObjectType.PFS__c.Fields.Monthly_Expenses_Vehicle_Insurance__c.inlineHelpText}"/></li>
                        </ul>
                    </apex:outputPanel>
                </apex:outputPanel>
                <apex:actionRegion >
                    <apex:inputField rendered="{!NOT(showInputAsLabel)}" id="row25" value="{!pfs.Monthly_Expenses_Vehicle_Insurance__c}" styleClass="required" >
                        <apex:actionSupport event="onchange" rerender="total_5" oncomplete="setChanged();"/>
                    </apex:inputField>
                    <apex:outputPanel rendered="{!showInputAsLabel}">{!IF(ISNULL(pfs.Monthly_Expenses_Vehicle_Insurance__c),'$0','$'+TEXT(pfs.Monthly_Expenses_Vehicle_Insurance__c))}</apex:outputPanel>
                </apex:actionRegion>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputPanel >
                    <apex:outputLabel value="{!appUtils.labels.Monthly_Expenses_Vehicle_Maintenance__c}" styleClass="requiredText"  escape="false" />
                    <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!AND(NOT(showInputAsLabel),$ObjectType.PFS__c.Fields.Monthly_Expenses_Vehicle_Maintenance__c.inlineHelpText != '')}">
                        <span class="helpicon"></span>
                        <ul class="toolTip">
                            <li><apex:outputText value="{!$ObjectType.PFS__c.Fields.Monthly_Expenses_Vehicle_Maintenance__c.inlineHelpText}"/></li>
                        </ul>
                    </apex:outputPanel>
                </apex:outputPanel>
                <apex:actionRegion >
                    <apex:inputField rendered="{!NOT(showInputAsLabel)}" id="row26" value="{!pfs.Monthly_Expenses_Vehicle_Maintenance__c}" styleClass="required" >
                        <apex:actionSupport event="onchange" rerender="total_5" oncomplete="setChanged();"/>
                    </apex:inputField>
                    <apex:outputPanel rendered="{!showInputAsLabel}">{!IF(ISNULL(pfs.Monthly_Expenses_Vehicle_Maintenance__c),'$0','$'+TEXT(pfs.Monthly_Expenses_Vehicle_Maintenance__c))}</apex:outputPanel>
                </apex:actionRegion>
            </apex:pageBlockSectionItem>
        </apex:pageBlockSection>
        <!-- Start: Other_Monthly_Vehicle_Expenses_Section -->
        <apex:outputPanel id="Other_Monthly_Vehicle_Expenses_Section" layout="block">
            <apex:pageBlockSection columns="1" collapsible="false">
                <apex:pageBlockSectionItem >
                    <apex:outputPanel >
                        <apex:outputLabel value="{!appUtils.labels.Other_Monthly_Expenses_Vehicle__c}" escape="false" />
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!AND(NOT(showInputAsLabel),$ObjectType.PFS__c.Fields.Other_Monthly_Expenses_Vehicle__c.inlineHelpText != '')}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.PFS__c.Fields.Other_Monthly_Expenses_Vehicle__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </apex:outputPanel>
                    <apex:actionRegion >
                        <apex:inputField StyleClass="otherMIE" rendered="{!NOT(showInputAsLabel)}" id="row27" value="{!pfs.Other_Monthly_Expenses_Vehicle__c}" required="false" onChange="setChanged()" />
                        <apex:outputPanel rendered="{!showInputAsLabel}">{!IF(ISNULL(pfs.Other_Monthly_Expenses_Vehicle__c),'$0','$'+TEXT(pfs.Other_Monthly_Expenses_Vehicle__c))}</apex:outputPanel>
                    </apex:actionRegion>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:outputPanel>
        <apex:outputPanel id="Other_Monthly_Vehicle_Expenses_Description" layout="block">
            <apex:pageBlockSection columns="1" collapsible="false" rendered="{!AND(NOT(ISNULL(pfs.Other_Monthly_Expenses_Vehicle__c)), pfs.Other_Monthly_Expenses_Vehicle__c>0)}">
                <apex:pageBlockSectionItem >
                    <apex:outputPanel >
                        <apex:outputLabel value="{!appUtils.labels.Other_Monthly_Expenses_Vehicle_Explain__c}" styleClass="requiredText"  escape="false" />
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!AND(NOT(showInputAsLabel),$ObjectType.PFS__c.Fields.Other_Monthly_Expenses_Vehicle_Explain__c.inlineHelpText != '')}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.PFS__c.Fields.Other_Monthly_Expenses_Vehicle_Explain__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </apex:outputPanel>
                    <apex:actionRegion >
                        <apex:inputField id="row28" rendered="{!AND(NOT(showInputAsLabel),NOT(ISNULL(pfs.Other_Monthly_Expenses_Vehicle__c)))}" onChange="setChanged()" value="{!pfs.Other_Monthly_Expenses_Vehicle_Explain__c}"  styleClass="required" />
                        <apex:outputPanel rendered="{!showInputAsLabel}">{!pfs.Other_Monthly_Expenses_Vehicle_Explain__c}</apex:outputPanel>                   
                    </apex:actionRegion>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:outputPanel>
        <!-- End: Other_Monthly_Vehicle_Expenses_Section -->
        <apex:outputPanel id="total_5">
            <apex:pageBlockSection columns="1" collapsible="false">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Total"  escape="false" />
                    <apex:outputPanel >
                        <input value="{!totalMonthlyVehicleExpenses}" style="{!IF(NOT(showInputAsLabel),'','display:none;')}background-color: lightgrey;" disabled="disabled" />
                        <apex:outputPanel rendered="{!showInputAsLabel}">{!IF(ISNULL(pfs.Monthly_Vehicle_Total__c),'$0','$'+TEXT(pfs.Monthly_Vehicle_Total__c))}</apex:outputPanel>
                   </apex:outputPanel>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:outputPanel>
    </div>
</apex:component>