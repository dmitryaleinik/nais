<apex:component controller="FamilyAppCompController">

    <apex:attribute name="pfsRecord" description="The PFS record that the component should work with" type="PFS__c" assignTo="{!pfs}" />
    <apex:attribute name="bfRecord" description="The Business Farm record that the component should work with" type="Business_Farm__c" assignTo="{!bf}" />
    <apex:attribute name="applicationUtility" assignTo="{!appUtils}" description="An instance of ApplicationUtils passed in from the main page" type="ApplicationUtils" required="true"/>

    <apex:includeScript value="{!$Resource.JQueryFormatCurrency}" />

    <script>
        jQuery(document).ready(function()
            {
                jQuery('.estimated').change
                (
                    function()
                    {
                        var estimatedValues = jQuery('.estimated').map
                        (
                            function(i, el)
                            {
                                var value = 0;
                                if(!isNaN(ToNumber(jQuery(el).val()))) value = ToNumber(jQuery(el).val());
                                
                                return value; 
                            }
                        ).get();
                        
                        var total = 0;
                        for(var i = 0; i < estimatedValues.length; i++) total = total + estimatedValues[i];
                        // [DP] 02.17.2016 SFP-315 need to check for null otherwise we get a syntax error
                        var net = {!IF(bf.Business_Total_Income_Est__c == null, 0, bf.Business_Total_Income_Est__c)};
                        net = net - total;

                        // [dp] NAIS-1643 using jquery method instead of the format money one so it can handle amounts >= million 4.28.14
                        // jQuery('.business-total-expenses-estimated span').html('$'+total.formatMoney().replace('.00', ''));
                        // jQuery('.business-total-expenses-estimated input').val(total);
                        // [jB] SFP-296 auto adjust values for output fields if 'Sole Proprietorship', these "spans" shouldn't be found
                        // if not rendered (inputs will be instead)
                        if({!bf.Business_Entity_Type__c == 'Sole Proprietorship'}){
                        jQuery('.business-net-profit-loss-share-estimated span').html(net);
                        jQuery('.business-net-profit-loss-share-estimated input').val(net);
                        jQuery('.business-net-profit-loss-share-estimated span').formatCurrency({roundToDecimalPlace:-2});
}

                        jQuery('.business-total-expenses-estimated span').html(total);
                        jQuery('.business-total-expenses-estimated input').val(total);
                        jQuery('.business-total-expenses-estimated span').formatCurrency({roundToDecimalPlace:-2});
                        jQuery('.business-net-profit-loss-estimated span').html(net);
                        jQuery('.business-net-profit-loss-estimated input').val(net);
                        jQuery('.business-net-profit-loss-estimated span').formatCurrency({roundToDecimalPlace:-2});
                    }
                );
                
                jQuery('.current').change
                (
                    function()
                    {
                        var currentValues = jQuery('.current').map
                        (
                            function(i, el)
                            {
                                var value = 0;
                                if(!isNaN(ToNumber(jQuery(el).val()))) value = ToNumber(jQuery(el).val());
                                
                                return value; 
                            }
                        ).get();
                        
                        var total = 0;
                        for(var i = 0; i < currentValues.length; i++){
                            total = total + currentValues[i];
                        }
                        // [DP] 02.17.2016 SFP-315 need to check for null otherwise we get a syntax error                        
                        var net = {!IF(bf.Business_Total_Income_Current__c == null, 0, bf.Business_Total_Income_Current__c)};
                        net = net - total;
                        
                        // [dp] NAIS-1643 using jquery method instead of the format money one so it can handle amounts >= million 4.28.14
                        //jQuery('.business-total-expenses-current span').html('$'+total.formatMoney().replace('.00', ''));
                        // jQuery('.business-total-expenses-current input').val(total);
                        // [jB] SFP-296 auto adjust values for output fields if 'Sole Proprietorship'
                        if({!bf.Business_Entity_Type__c == 'Sole Proprietorship'}){
                            jQuery('.business-net-profit-loss-share-current span').html(net);
                            jQuery('.business-net-profit-loss-share-current input').val(net)
                            jQuery('.business-net-profit-loss-share-current span').formatCurrency({roundToDecimalPlace:-2});
                        }

                        jQuery('.business-total-expenses-current span').html(total);
                        jQuery('.business-total-expenses-current input').val(total);
                        jQuery('.business-total-expenses-current span').formatCurrency({roundToDecimalPlace:-2});
                        jQuery('.business-net-profit-loss-current span').html(net);
                        jQuery('.business-net-profit-loss-current input').val(net);
                        jQuery('.business-net-profit-loss-current span').formatCurrency({roundToDecimalPlace:-2});
                    }
                );
                
                jQuery('.current:first,.estimated:first').change();
            }
        );
        
        function ToNumber(s)
        {   // [DP] 02.18.2016 SFP-315 remove dollar signs, too!!!!!
            return parseFloat(s.replace(/,/g, '').replace('$', ''));
        }
    </script>


    <div class="block fp">
        <div class="blockbody nopadding">
            <apex:outputPanel styleClass="helpmessage" layout="block">
                <apex:outputText escape="false" value="{!appUtils.helpRecord.BE_Business_Expense_Instruction__c}"/>
            </apex:outputPanel>
        
            <table cellpadding="3" cellspacing="0" class="datatable familyincometable taxable-income-worksheet detailList" border="0">
                <tr>
                    <th class="labelCol">&nbsp;</th>
                    <!-- NAIS-2494
                    <apex:outputPanel layout="none" rendered="{!showPriorColumn}">
                        <th class="labelCol">{!currentTaxYearLabel} (Estimated)</th>
                    </apex:outputPanel>
                    -->
                    <th class="labelCol">{!currentTaxYearLabel} (Actual)</th>
                    <th class="labelCol">{!futureTaxYearLabel} (Estimated)</th>
                </tr>

                <tr>
                    <td>
                        <apex:outputLabel value="{!appUtils.labels.Business_Salary_Paid_Current__c}" styleClass="questionLabel "/>
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Salary_Paid_Current__c.inlineHelpText != ''}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Salary_Paid_Current__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </td>
                    <!-- NAIS-2494
                    <apex:outputPanel layout="none" rendered="{!showPriorColumn}">
                        <td><apex:outputField value="{!priorYearPFS.Business_Salary_Paid_Est__c}"/></td>
                    </apex:outputPanel>
                    -->
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required current" value="{!bf.Business_Salary_Paid_Current__c}" /></td>
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required estimated" value="{!bf.Business_Salary_Paid_Est__c}" /></td>
                </tr>
                
                <tr>
                    <td>
                        <apex:outputLabel value="{!appUtils.labels.Business_Other_Wages_Current__c}" styleClass="questionLabel "/>
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Other_Wages_Current__c.inlineHelpText != ''}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Other_Wages_Current__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </td>
                    <!-- NAIS-2494
                    <apex:outputPanel layout="none" rendered="{!showPriorColumn}">
                        <td><apex:outputField value="{!priorYearPFS.Business_Other_Wages_Est__c}"/></td>
                    </apex:outputPanel>
                    -->
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required current" value="{!bf.Business_Other_Wages_Current__c}" /></td>
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required estimated" value="{!bf.Business_Other_Wages_Est__c}" /></td>
                </tr>
                
                <tr>
                    <td>
                        <apex:outputLabel value="{!appUtils.labels.Business_Addtl_Comp_Current__c}" styleClass="questionLabel "/>
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Addtl_Comp_Current__c.inlineHelpText != ''}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Addtl_Comp_Current__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </td>
                    <!-- NAIS-2494
                    <apex:outputPanel layout="none" rendered="{!showPriorColumn}">
                        <td><apex:outputField value="{!priorYearPFS.Business_Addtl_Comp_Est__c}"/></td>
                    </apex:outputPanel>
                    -->
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required current" value="{!bf.Business_Addtl_Comp_Current__c}" /></td>
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required estimated" value="{!bf.Business_Addtl_Comp_Est__c}" /></td>
                </tr>
                
                <tr>
                    <td>
                        <apex:outputLabel value="{!appUtils.labels.Business_Rent_Cost_Current__c}" styleClass="questionLabel "/>
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Rent_Cost_Current__c.inlineHelpText != ''}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Rent_Cost_Current__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </td>
                    <!-- NAIS-2494
                    <apex:outputPanel layout="none" rendered="{!showPriorColumn}">
                        <td><apex:outputField value="{!priorYearPFS.Business_Rent_Cost_Est__c}"/></td>
                    </apex:outputPanel>
                    -->
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required current" value="{!bf.Business_Rent_Cost_Current__c}" /></td>
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required estimated" value="{!bf.Business_Rent_Cost_Est__c}" /></td>
                </tr>
                
                <tr>
                    <td>
                        <apex:outputLabel value="{!appUtils.labels.Business_Mortgage_Current__c}" styleClass="questionLabel "/>
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Mortgage_Current__c.inlineHelpText != ''}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Mortgage_Current__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </td>
                    <!-- NAIS-2494
                    <apex:outputPanel layout="none" rendered="{!showPriorColumn}">
                        <td><apex:outputField value="{!priorYearPFS.Business_Mortgage_Est__c}"/></td>
                    </apex:outputPanel>
                    -->
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required current" value="{!bf.Business_Mortgage_Current__c}" /></td>
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required estimated" value="{!bf.Business_Mortgage_Est__c}" /></td>
                </tr>
                
                <tr>
                    <td>
                        <apex:outputLabel value="{!appUtils.labels.Business_Farm_Asset_Depreciation__c}" styleClass="questionLabel "/>
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Farm_Asset_Depreciation__c.inlineHelpText != ''}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Farm_Asset_Depreciation__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </td>
                    <!-- NAIS-2494
                    <apex:outputPanel layout="none" rendered="{!showPriorColumn}">
                        <td><apex:outputField value="{!priorYearPFS.Business_Farm_Asset_Depreciation_Est__c}"/></td>
                    </apex:outputPanel>
                    -->
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required current" value="{!bf.Business_Farm_Asset_Depreciation__c}" /></td>
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required estimated" value="{!bf.Business_Farm_Asset_Depreciation_Est__c}" /></td>
                </tr>
                
                <tr>
                    <td>
                        <apex:outputLabel value="{!appUtils.labels.Business_Other_Expenses_Current__c}" styleClass="questionLabel "/>
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Other_Expenses_Current__c.inlineHelpText != ''}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Other_Expenses_Current__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </td>
                    <!-- NAIS-2494
                    <apex:outputPanel layout="none" rendered="{!showPriorColumn}">
                        <td><apex:outputField value="{!priorYearPFS.Business_Other_Expenses_Est__c}"/></td>
                    </apex:outputPanel>
                    -->
                    <td>
                        <apex:actionRegion >
                            <apex:inputField required="true" styleClass="required current" value="{!bf.Business_Other_Expenses_Current__c}" onfocus="this.alt = this.value">
                                <apex:actionSupport status="RequestStatus" event="onchange" onsubmit="setChanged(); if ( (this.alt == '' && this.value == '0') || (this.alt == '0' && this.value == '') ) { return false; }" rerender="business-other-expenses" />
                            </apex:inputField>
                        </apex:actionRegion>
                    </td>
                    <td>
                        <apex:actionRegion >
                            <apex:inputField required="true" styleClass="required estimated" value="{!bf.Business_Other_Expenses_Est__c}" onfocus="this.alt = this.value">
                                <apex:actionSupport status="RequestStatus" event="onchange" onsubmit="setChanged(); if ( (this.alt == '' && this.value == '0') || (this.alt == '0' && this.value == '') ) { return false; }" rerender="business-other-expenses" />
                            </apex:inputField>
                        </apex:actionRegion>                        
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <apex:outputLabel value="{!appUtils.labels.Business_Other_Expenses_Descrip__c}" styleClass="questionLabel "/>
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Other_Expenses_Descrip__c.inlineHelpText != ''}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Other_Expenses_Descrip__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </td>
                    <td colspan="{!IF(priorYearPFS != null, 3, 2)}">
                        <apex:outputPanel id="business-other-expenses">
                            <apex:inputField onChange="setChanged()" rendered="{!NULLVALUE(bf.Business_Other_Expenses_Current__c, 0) > 0 || NULLVALUE(bf.Business_Other_Expenses_Est__c, 0) > 0}" required="true" styleClass="required" value="{!bf.Business_Other_Expenses_Descrip__c}" style="width:100%"/>
                            <apex:inputField onChange="setChanged()" rendered="{!NULLVALUE(bf.Business_Other_Expenses_Current__c, 0) <= 0 && NULLVALUE(bf.Business_Other_Expenses_Est__c, 0) <= 0}" required="true" styleClass="required" value="{!bf.Business_Other_Expenses_Descrip__c}" style="width:100%"/>
                        </apex:outputPanel>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <apex:outputLabel value="{!appUtils.labels.Business_Total_Expenses_Current__c}" styleClass="questionLabel "/>
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Total_Expenses_Current__c.inlineHelpText != ''}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Total_Expenses_Current__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </td>
                    <!-- NAIS-2494
                    <apex:outputPanel layout="none" rendered="{!showPriorColumn}">
                        <td><apex:outputField value="{!priorYearPFS.Business_Total_Expenses_Est__c}"/></td>
                    </apex:outputPanel>
                    -->
                    <td class="business-total-expenses-current">
                        <span><apex:outputField value="{!bf.Business_Total_Expenses_Current__c}" /></span>
                        <apex:inputHidden value="{!bf.Business_Total_Expenses_Current__c}" />
                    </td>
                    <td class="business-total-expenses-estimated">
                        <span><apex:outputField value="{!bf.Business_Total_Expenses_Est__c}" /></span>
                        <apex:inputHidden value="{!bf.Business_Total_Expenses_Est__c}" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <apex:outputLabel value="{!appUtils.labels.Self_Employed_Tax_Paid__c}" styleClass="questionLabel "/>
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Self_Employed_Tax_Paid__c.inlineHelpText != ''}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Self_Employed_Tax_Paid__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </td>
                    <!-- NAIS-2494
                    <apex:outputPanel layout="none" rendered="{!showPriorColumn}">
                        <td><apex:outputField value="{!priorYearPFS.Self_Employed_Tax_Paid_Est__c}"/></td>
                    </apex:outputPanel>
                    -->
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required" value="{!bf.Self_Employed_Tax_Paid__c}" /></td>
                    <td><apex:inputField onChange="setChanged()" required="true" styleClass="required" value="{!bf.Self_Employed_Tax_Paid_Est__c}" /></td>
                </tr>
                
                <tr>
                    <td>
                        <apex:outputLabel value="{!appUtils.labels.Net_Profit_Loss_Business_Farm_Current__c}" styleClass="questionLabel "/>
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Net_Profit_Loss_Business_Farm_Current__c.inlineHelpText != ''}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Net_Profit_Loss_Business_Farm_Current__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </td>
                    <!-- NAIS-2494
                    <apex:outputPanel layout="none" rendered="{!showPriorColumn}">
                        <td></td>
                    </apex:outputPanel>
                    -->
                    <td class="business-net-profit-loss-current">
                        <span><apex:outputField value="{!bf.Net_Profit_Loss_Business_Farm_Current__c}" /></span>
                        <apex:inputHidden value="{!bf.Net_Profit_Loss_Business_Farm_Current__c}" />
                    </td>
                    <td class="business-net-profit-loss-estimated">
                        <span><apex:outputField value="{!bf.Net_Profit_Loss_Business_Farm_Est__c}" /></span>
                        <apex:inputHidden value="{!bf.Net_Profit_Loss_Business_Farm_Est__c}" />
                    </td>
                </tr>
                <!-- SFP - 350 : conditional render of message text to provide info for user -->
                 <apex:outputPanel rendered="{!bf.Business_Entity_Type__c != 'Sole Proprietorship'}">
                       
                        <tr>
                            <td colspan="2" class="messageCell">
                                <apex:outputText value="{!$Label.Family_Application_17L_1}" style="font-style:italic" rendered="{!PFS.Business_Farms__r.size = 1}"/>
                                <apex:outputText value="{!$Label.Family_Application_17L_2}" style="font-style:italic" rendered="{!PFS.Business_Farms__r.size > 1}"/>
                            </td>
                            <td></td>
                        </tr>
                    
                    </apex:outputPanel>
               
                <tr>

                    <td>
                    
                        <apex:outputLabel value="{!appUtils.labels.Business_Net_Profit_Share_Current__c}" styleClass="questionLabel "/>
                        <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Net_Profit_Share_Current__c.inlineHelpText != ''}">
                            <span class="helpicon"></span>
                            <ul class="toolTip">
                                <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Net_Profit_Share_Current__c.inlineHelpText}"/></li>
                            </ul>
                        </apex:outputPanel>
                    </td>
                    <!-- NAIS-2494
                    <apex:outputPanel layout="none" rendered="{!showPriorColumn}">
                        <td></td>
                    </apex:outputPanel>
                    -->
                        <!-- SFP-296 -->
                    <td class="business-net-profit-loss-share-current">
                        <apex:inputField rendered="{!bf.Business_Entity_Type__c != 'Sole Proprietorship'}" onChange="setChanged()" required="true" styleClass="required" value="{!bf.Business_Net_Profit_Share_Current__c}" />
                        <apex:outputPanel rendered="{!bf.Business_Entity_Type__c == 'Sole Proprietorship'}">
                            <span><apex:outputField value="{!bf.Business_Net_Profit_Share_Current__c}" /></span>
                            <apex:inputHidden rendered="{!bf.Business_Entity_Type__c == 'Sole Proprietorship'}" value="{!bf.Business_Net_Profit_Share_Current__c}" />
                        </apex:outputPanel>
                        
                    </td>
                    <td class="business-net-profit-loss-share-estimated">
                        <apex:inputField rendered="{!bf.Business_Entity_Type__c != 'Sole Proprietorship'}" onChange="setChanged()" required="true" styleClass="required" value="{!bf.Business_Net_Profit_Share_Est__c}" />
                        <apex:outputPanel rendered="{!bf.Business_Entity_Type__c == 'Sole Proprietorship'}">
                            <span><apex:outputField value="{!bf.Business_Net_Profit_Share_Est__c}" /></span>
                            <apex:inputHidden rendered="{!bf.Business_Entity_Type__c == 'Sole Proprietorship'}" value="{!bf.Business_Net_Profit_Share_Est__c}" />
                        </apex:outputPanel>
                    </td>
                        <!-- /SFP-296 -->                    

                </tr>
            </table>
        </div>
    </div>

</apex:component>