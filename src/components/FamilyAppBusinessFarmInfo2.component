<!-- NAIS-2494 Updated to support multiple businesses/farms -->
<apex:component controller="FamilyAppBusinessFarmInfoController">

    <apex:attribute name="pfsRecord" description="The PFS record that the component should work with" type="PFS__c" assignTo="{!pfs}" />
    <apex:attribute name="bfRecord" description="The Business Farm record that the component should work with" type="Business_Farm__c" assignTo="{!bf}" />
    <apex:attribute name="applicationUtility" assignTo="{!appUtils}" description="An instance of ApplicationUtils passed in from the main page" type="ApplicationUtils" required="true"/>

    <script type="text/javascript">
      function esc(myid) {
         return '#' + myid.replace(/(:|\.)/g,'\\\\$1'); 
      }         

// NAIS-2494
//
//    function validateMoreThanOneBusiness(selected) {
//       if (selected == 'Yes') {
//           j$(esc('{!$Component.multipleBusinessInstr}')).show();
//           j$('#addtionBusinessFarmSection').show();
//       } else {
//           j$(esc('{!$Component.multipleBusinessInstr}')).hide();
//           j$('#addtionBusinessFarmSection').hide();
//       }      
//    }
    </script>

    <div class="block fp">
        <div class="blockbody nopadding">
            
            <apex:outputPanel styleClass="helpmessage" layout="block">
                <apex:outputField value="{!appUtils.helpRecord.BFI_Own_Business_Instruction__c}"/>
            </apex:outputPanel>
            
            <apex:outputPanel id="business-farm-info">
                <!-- NAIS-2494
                <apex:pageBlockSection columns="1" collapsible="false" rendered="{!pfs.Own_Business_or_Farm__c == 'Yes'}">
                    <apex:pageBlockSectionItem >
                        <apex:outputPanel layout="inline">
                            <apex:outputLabel value="{!appUtils.labels.Own_More_than_One_Business_or_Farm__c}" styleClass="questionLabel requiredText"/>
                            <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.PFS__c.Fields.Own_More_than_One_Business_or_Farm__c.inlineHelpText != ''}">
                                <span class="helpicon"></span>
                                <ul class="toolTip">
                                    <li><apex:outputText value="{!$ObjectType.PFS__c.Fields.Own_More_than_One_Business_or_Farm__c.inlineHelpText}"/></li>
                                </ul>
                            </apex:outputPanel>
                        </apex:outputPanel>
                        <apex:inputField value="{!pfs.Own_More_than_One_Business_or_Farm__c}"  required="true" styleClass="required" onchange="setChanged(); validateMoreThanOneBusiness(this.value); "/>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                
                <apex:outputPanel id="ownmultiplebusinessinstr" styleClass="helpmessage" layout="block"  rendered="{!pfs.Own_Business_or_Farm__c == 'Yes'}">
                    <apex:outputText id="multipleBusinessInstr" value="{!appUtils.helpRecord.BFI_Own_Multiple_Businesses_Instruction__c}" rendered="{!pfs.Own_More_than_One_Business_or_Farm__c == 'Yes'}" escape="false"/>
                </apex:outputPanel>
                NAIS-2494 -->
                
                <apex:pageBlockSection columns="1" collapsible="false" rendered="{!pfs.Own_Business_or_Farm__c == 'Yes'}">
                    <apex:pageBlockSectionItem >
                        <apex:outputPanel layout="inline">
                            <apex:outputLabel value="{!appUtils.labels.Business_Name__c}" styleClass="questionLabel "/>
                            <!-- Commenting out help text since this is a standard name field [DP] 09.02.2015
                            <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Name__c.inlineHelpText != ''}">
                                <span class="helpicon"></span>
                                <ul class="toolTip">
                                    <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Name__c.inlineHelpText}"/></li>
                                </ul>
                            </apex:outputPanel>-->
                        </apex:outputPanel>
                        <apex:inputField onChange="setChanged()" required="true" styleClass="required" value="{!bf.Name}" />
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                
                <apex:pageBlockSection columns="1" collapsible="false" rendered="{!pfs.Own_Business_or_Farm__c == 'Yes'}">
                    <apex:pageBlockSectionItem >
                        <apex:outputPanel layout="inline">
                            <apex:outputLabel value="{!appUtils.labels.Business_or_Farm__c}" styleClass="questionLabel "/>
                            <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_or_Farm__c.inlineHelpText != ''}">
                                <span class="helpicon"></span>
                                <ul class="toolTip">
                                    <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_or_Farm__c.inlineHelpText}"/></li>
                                </ul>
                            </apex:outputPanel>
                        </apex:outputPanel>
                        <apex:inputField onChange="setChanged()" required="true" styleClass="required" value="{!bf.Business_or_Farm__c}" />
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                
                <apex:pageBlockSection columns="1" collapsible="false" rendered="{!pfs.Own_Business_or_Farm__c == 'Yes'}">
                    <apex:pageBlockSectionItem >
                        <apex:outputPanel layout="inline">
                            <apex:outputLabel value="{!appUtils.labels.Business_Entity_Type__c}" styleClass="questionLabel "/>
                            <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Entity_Type__c.inlineHelpText != ''}">
                                <span class="helpicon"></span>
                                <ul class="toolTip">
                                    <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Entity_Type__c.inlineHelpText}"/></li>
                                </ul>
                            </apex:outputPanel>
                        </apex:outputPanel>
                        <apex:actionRegion >
                            <apex:inputField value="{!bf.Business_Entity_Type__c}" required="true" styleClass="required" >
                                <apex:actionSupport status="RequestStatus" event="onchange" onsubmit="setChanged(); if(((this.value == 'Partnership' || this.value == 'Corporation') && j$(esc('{!$Component.businessfarmpercent}')).has('input').length > 0) || ((this.value != 'Partnership' && this.value != 'Corporation') && j$(esc('{!$Component.businessfarmpercent}')).has('input').length == 0)) { return false; }" rerender="businessfarmpercent" />
                            </apex:inputField>
                        </apex:actionRegion>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                
                <apex:pageBlockSection id="businessfarmpercent" columns="1" collapsible="false" rendered="{!pfs.Own_Business_or_Farm__c == 'Yes'}">
                    <apex:pageBlockSectionItem rendered="{!bf.Business_Entity_Type__c != 'Sole Proprietorship' && bf.Business_Entity_Type__c != null}">
                        <apex:outputPanel layout="inline">
                            <apex:outputLabel value="{!appUtils.labels.Business_Farm_Ownership_Percent__c}" styleClass="questionLabel requiredText"/>
                            <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Farm_Ownership_Percent__c.inlineHelpText != ''}">
                                <span class="helpicon"></span>
                                <ul class="toolTip">
                                    <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Farm_Ownership_Percent__c.inlineHelpText}"/></li>
                                </ul>
                            </apex:outputPanel>
                        </apex:outputPanel>
                        <apex:inputField onChange="setChanged()" required="true" styleClass="required" value="{!bf.Business_Farm_Ownership_Percent__c}" />
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                
                <apex:pageBlockSection columns="1" collapsible="false" rendered="{!pfs.Own_Business_or_Farm__c == 'Yes'}">
                    <apex:pageBlockSectionItem >
                        <apex:outputPanel layout="inline">
                            <apex:outputLabel value="{!appUtils.labels.Business_Farm_Owner__c}" styleClass="questionLabel requiredText"/>
                            <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Farm_Owner__c.inlineHelpText != ''}">
                                <span class="helpicon"></span>
                                <ul class="toolTip">
                                    <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Farm_Owner__c.inlineHelpText}"/></li>
                                </ul>
                            </apex:outputPanel>
                        </apex:outputPanel>
                        <apex:selectList onChange="setChanged()" value="{!bf.Business_Farm_Owner__c}"  required="true" styleClass="required" size="1" multiselect="false">
                            <apex:selectOptions value="{!ParentOptions}" />
                        </apex:selectList>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                
                <apex:pageBlockSection columns="1" collapsible="false" rendered="{!pfs.Own_Business_or_Farm__c == 'Yes'}">
                    <apex:pageBlockSectionItem >
                        <apex:outputPanel layout="inline">
                            <apex:outputLabel value="{!appUtils.labels.Business_Start_Year__c}" styleClass="questionLabel "/>
                            <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Start_Year__c.inlineHelpText != ''}">
                                <span class="helpicon"></span>
                                <ul class="toolTip">
                                    <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Start_Year__c.inlineHelpText}"/></li>
                                </ul>
                            </apex:outputPanel>
                        </apex:outputPanel>
                        <apex:inputField onChange="setChanged()" required="true" styleClass="required" value="{!bf.Business_Start_Year__c}" />
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                
                <apex:pageBlockSection columns="1" collapsible="false" rendered="{!pfs.Own_Business_or_Farm__c == 'Yes'}">
                    <apex:pageBlockSectionItem >
                        <apex:outputPanel layout="inline">
                            <apex:outputLabel value="{!appUtils.labels.Business_Street__c}" styleClass="questionLabel "/>
                            <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Street__c.inlineHelpText != ''}">
                                <span class="helpicon"></span>
                                <ul class="toolTip">
                                    <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Street__c.inlineHelpText}"/></li>
                                </ul>
                            </apex:outputPanel>
                        </apex:outputPanel>
                        <apex:inputField onChange="setChanged()" required="true" styleClass="required" value="{!bf.Business_Street__c}" />
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                
                <apex:pageBlockSection columns="1" collapsible="false" rendered="{!pfs.Own_Business_or_Farm__c == 'Yes'}">
                    <apex:pageBlockSectionItem >
                        <apex:outputPanel layout="inline">
                            <apex:outputLabel value="{!appUtils.labels.Business_City__c}" styleClass="questionLabel "/>
                            <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_City__c.inlineHelpText != ''}">
                                <span class="helpicon"></span>
                                <ul class="toolTip">
                                    <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_City__c.inlineHelpText}"/></li>
                                </ul>
                            </apex:outputPanel>
                        </apex:outputPanel>
                        <apex:inputField onChange="setChanged()" required="true" styleClass="required" value="{!bf.Business_City__c}" />
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                
                <apex:pageBlockSection columns="1" collapsible="false" rendered="{!pfs.Own_Business_or_Farm__c == 'Yes'}">
                    <apex:pageBlockSectionItem >
                        <apex:outputPanel layout="inline">
                            <apex:outputLabel value="{!appUtils.labels.Business_State_Province__c}" styleClass="questionLabel "/>
                            <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_State_Province__c.inlineHelpText != ''}">
                                <span class="helpicon"></span>
                                <ul class="toolTip">
                                    <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_State_Province__c.inlineHelpText}"/></li>
                                </ul>
                            </apex:outputPanel>
                        </apex:outputPanel>
                        <apex:inputField onChange="setChanged()" required="true" styleClass="required" value="{!bf.Business_State_Province__c}" />
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                
                <apex:pageBlockSection columns="1" collapsible="false" rendered="{!pfs.Own_Business_or_Farm__c == 'Yes'}">
                    <apex:pageBlockSectionItem >
                        <apex:outputPanel layout="inline">
                            <apex:outputLabel value="{!appUtils.labels.Business_Zip_Postal_Code__c}" styleClass="questionLabel "/>
                            <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Zip_Postal_Code__c.inlineHelpText != ''}">
                                <span class="helpicon"></span>
                                <ul class="toolTip">
                                    <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Zip_Postal_Code__c.inlineHelpText}"/></li>
                                </ul>
                            </apex:outputPanel>
                        </apex:outputPanel>
                        <apex:inputField onChange="setChanged()" required="true" styleClass="required" value="{!bf.Business_Zip_Postal_Code__c}" />
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                
                <apex:pageBlockSection columns="1" collapsible="false" rendered="{!pfs.Own_Business_or_Farm__c == 'Yes'}">
                    <apex:pageBlockSectionItem >
                        <apex:outputPanel layout="inline">
                            <apex:outputLabel value="{!appUtils.labels.Business_Product_Service__c}" styleClass="questionLabel "/>
                            <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.Business_Farm__c.Fields.Business_Product_Service__c.inlineHelpText != ''}">
                                <span class="helpicon"></span>
                                <ul class="toolTip">
                                    <li><apex:outputText value="{!$ObjectType.Business_Farm__c.Fields.Business_Product_Service__c.inlineHelpText}"/></li>
                                </ul>
                            </apex:outputPanel>
                        </apex:outputPanel>
                        <apex:inputField onChange="setChanged()" required="true" styleClass="required" value="{!bf.Business_Product_Service__c}" style="width:100%"/>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                
                <!-- NAIS-2494
                <div id="addtionBusinessFarmSection" style="{!IF(pfs.Own_More_than_One_Business_or_Farm__c == 'Yes', '', 'display: none;')}">
                <apex:pageBlockSection id="additionalbusinessesfarm" columns="1" collapsible="false" rendered="{!pfs.Own_Business_or_Farm__c == 'Yes'}" >
                    <apex:pageBlockSectionItem >
                        <apex:outputPanel layout="inline">
                            <apex:outputLabel value="{!appUtils.labels.Additional_Businesses_Farms__c}" styleClass="questionLabel "/>
                            <apex:outputPanel layout="inline" styleClass="hasToolTip" rendered="{!$ObjectType.PFS__c.Fields.Additional_Businesses_Farms__c.inlineHelpText != ''}">
                                <span class="helpicon"></span>
                                <ul class="toolTip">
                                    <li><apex:outputText value="{!$ObjectType.PFS__c.Fields.Additional_Businesses_Farms__c.inlineHelpText}"/></li>
                                </ul>
                            </apex:outputPanel>
                        </apex:outputPanel>
                        <apex:inputField onChange="setChanged()"  value="{!pfs.Additional_Businesses_Farms__c}" style="width:100%"/>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                </div>
                NAIS-2494 -->
                
            </apex:outputPanel>
        
        </div>
    </div>


</apex:component>