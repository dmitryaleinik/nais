/**
 * FamilyDocumentsCSS.resource
 *
 * @description: CSS for FamilyDocuments and popup hover and collapsible 
 *
 * @author: Mike havrilla @ Presence PG
 */

.closed { 
    display: none;
}

.family-documents-collapsible {
    border-top: 1px solid #dad9d9;
}

.family-documents-collapsible:last-child{
    border-bottom: 1px solid #dad9d9;
    margin-bottom: 2em;
}

.family-documents-collapsible > h2 {
    display: flex;
    cursor: pointer;
    color: #3E5385;
    font-size: 1.5em;
    padding: 10px;
}

.family-documents-collapsible--helpcenter > h2 {
    color: #4C6794;
    font-weight: 100;
}

.family-documents-body {
    padding-left: 3.8em;
    padding-right: 3.5em;
    padding-bottom: 10px;
}

.faq {
    padding: .5em 1em;
}

.faq__header {
    font-weight: bold;
    padding-bottom: .25em;
    font-size: 1.25em;
}
.faq__more a,
.faq__header > a {
    color: #354F82;
}
.faq__more--link {
    font-weight: bold;
}


.faq__body {

}


.family-documents_cover-sheet {
    padding-bottom: 1em;
}

.family-documents_cover-sheet strong {
    color: #3E5385;
}

.arrow-down {
  width: 0; 
  height: 0; 
  border-left: 7px solid transparent;
  border-right: 7px solid transparent;
  border-top: 7px solid #3E5385;
  cursor: pointer;
    vertical-align: middle;
    display:inline-block;
}

.arrow-right {
    width: 0; 
    height: 0; 
    border-left: 7px solid #3E5385;
    border-top: 7px solid transparent;
    border-bottom: 7px solid transparent;
    cursor: pointer;
    vertical-align: middle;
    display:inline-block;
}

.process_helpers_container {
    display: -webkit-flex;
    display: flex;

    -webkit-flex-direction: row;
    flex-direction: row;

    justify-content: center;
    margin-bottom: 2em;
    cursor: pointer;
}

.process_helpers--flex-bottom {
    display: flex;
    flex-grow: 1;

}

.process_helpers--flex-top{
    margin-bottom: 1em;
}

.process_helpers--flex-left {
    display: flex;
    flex-direction: column;
    width: 25%;
    padding: 0 5%;
    border-right: 1px solid #dad9d9;
    margin: 0;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    text-align: center;
}

.process_helpers--popup {
    display: none;
    text-align: start;
    position: absolute;
    font-family: Arial,Helvetica,sans-serif;
    color: #2A5283;
    font-size: 1.1em;
    background-color: #FFFFFF;
    box-shadow: 1px 6px 15px 1px darkgray;
    width: 20em;
    padding: 1em;
}

.process_helpers--flex-left:last-child {
    border-right: none;
}


/* Family Help Center CSS, will need to be moved */
.familyHelp__Container {
    margin: 4em;
}

.familyHelp__Title {
    font-size: 2em;
    margin-bottom: .75em;
}

.familyHelp__FAQ {

}

.familyHelp__LearnMore {
    display: flex;
    flex-direction: column;
}

.familyHelp__LearnMore--row {
    display: flex;
    flex-direction: row;
    justify-content: space-between;

}

.familyHelp__LearnMore--item {
    display: flex;
    flex-direction: column;
    flex: 1;
    border-radius: 6px;
    align-items: center;
    margin: 1em;
    padding: 3em;
    border: 1px solid #C6C5CA;
    box-shadow: 1px 3px 4px #C6C5CA;
}

.familyHelp__LearnMore--item img {
    max-width: 50%;
}

.familyHelp--width {
    width: 50%;
    cursor: pointer;
}

.familyHelp--text {
    font-size: 1.3em;
    font-weight: bold;
    margin: 1em;
}

.familyHelp__Button {
    height: 28px;
    display: flex;
    justify-content: center;
    align-items: center;
    background: linear-gradient(to bottom, #75996b 7%,#58834b 63%,#58834b 63%);
    border-radius: 6px;
    padding: .5em;
    flex-grow: 1;
    color: white;
    font-size: 1em
}
