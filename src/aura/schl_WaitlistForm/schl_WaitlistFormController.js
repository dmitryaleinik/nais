({
    doInit: function(cmp)
    {
		cmp.find("recordEditor").getNewRecord(
            "SCHL_Waitlist__c", // sObject type (entityAPIName)
            null, // recordTypeId
            false, // skip cache?
            $A.getCallback(function()
            {
				var newRecord = cmp.get("v.newWaitlist");
                newRecord.Queue_Time_Stamp__c = new Date().getTime();
                cmp.set("v.newWaitlist", newRecord);
            })
        );
    },

    handleSaveWaitlist: function(component, event, helper)
    {
        var isValid = helper.validateWaitlistForm(component);

        if (isValid)
        {
            component.find("recordEditor").saveRecord($A.getCallback(function (saveResult) {
                var resultsToast = $A.get("e.force:showToast");
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    resultsToast.setParams(
                        {
                            "message": "Your contacts were successfully sent to SSS.",
                            "type": "success"
                        });

                    $A.util.toggleClass(component.find("form"),"slds-hide");
                }
                else if (saveResult.state === "INCOMPLETE")
                {
                    resultsToast.setParams(
                        {
                            "message": "We faced some problems with connection. Please try to submit your contacts again",
                            "type": "error"
                        });
                }
                else
                {
                    resultsToast.setParams(
                        {
                            "message": 'We faced some problems with sending yor contacts to SSS. Please check the data you entered and submit it once again',
                            "type": "error"
                        });
                    console.log('Error: ' + JSON.stringify(saveResult.error));
                }
                resultsToast.fire();
            }));
        }
    }
})