({
    doInit: function (component)
    {
        component.find("recordEditor").getNewRecord(
            "SCHL_Waitlist__c", // sObject type (entityAPIName)
            null, // recordTypeId
            false, // skip cache?,
            $A.getCallback(function () {
                var newRecord = component.get("v.newWaitlist");
                newRecord['Queue_Time_Stamp__c'] = new Date().getTime();
				cmp.set("v.newWaitlist", newRecord);
            })
        );
    },

    startApplicationProcess: function (component)
    {
        var isHuman = component.get("v.isHuman");

        if (isHuman)
        {
            var spinner = component.find("processingModal");
            $A.util.toggleClass(spinner, "slds-hide");


            var captchaComp = component.find("captcha");
            $A.util.toggleClass(captchaComp, "slds-hide");

            var formComp = component.find("waitlistForm");
            $A.util.toggleClass(formComp, "slds-hide");

            $A.util.toggleClass(spinner, "slds-hide");
        }
        else
        {
            var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams(
                {
                    "message": "Please check the box below to prove that you are not a robot.",
                    "type": "error"
                });
            resultsToast.fire();
        }
    },

    handleSaveWaitlist: function (component, event, helper)
    {
        var isValid = helper.validateContactForm(component);

        if (isValid)
        {
            component.find("recordEditor").saveRecord($A.getCallback(function (saveResult) {
                var resultsToast = $A.get("e.force:showToast");

                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT")
                {
                    resultsToast.setParams(
                        {
                            "message": "You are now on our waitlist. You should receive an email confirmation shortly.",
                            "type": "success"
                        });
                    $A.util.addClass(component.find("waitlistForm"), "slds-hide");
                }
                else if (saveResult.state === "INCOMPLETE")
                {
                    resultsToast.setParams(
                        {
                            "message": "We faced some problems with connection. Please try to submit your contacts again",
                            "type": "error"
                        });
                }
                else
                {
                    resultsToast.setParams(
                        {
                            "message": 'We faced some problems with sending yor contacts to SSS. Please check the data you entered and submit it once again',
                            "type": "error"
                        });
                }
                resultsToast.fire();
            }));
        }
    }
})