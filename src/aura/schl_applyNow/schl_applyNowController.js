({
    startApplicationProcess: function(component)
    {
        var isHuman = component.get("v.isHuman");

        if (isHuman)
        {
            var spinner = component.find("processingModal");
            $A.util.toggleClass(spinner, "slds-hide");
            var urlEvent = $A.get("e.force:navigateToURL");
            var destination = "/apply";
            urlEvent.setParams({"url": destination});
            $A.util.toggleClass(spinner, "slds-hide");
            urlEvent.fire();
        }
        else
        {
            var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams(
                {
                    "message": "Please, fill the checkbox below the message",
                    "type": "info"
                });
            resultsToast.fire();
        }
    },
})