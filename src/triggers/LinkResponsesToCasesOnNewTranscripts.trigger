trigger LinkResponsesToCasesOnNewTranscripts on LiveChatTranscript (before insert) {
    
    if (FeatureToggles.isEnabled('Link_Survey_Responses_To_Cases__c')) {
        Set<String> chatKeys = new Set<String>();
        
        for (LiveChatTranscript transcript : Trigger.new) {
            if (String.isNotBlank(transcript.ChatKey)) {
                chatKeys.add(transcript.ChatKey);
            }
        }
        
        List<Feedback_Survey_Response__c> surveyResponses = [SELECT Id, ChatKey__c FROM Feedback_Survey_Response__c WHERE ChatKey__c IN :chatKeys];
        Map<String, Feedback_Survey_Response__c> responsesByChatKey = new Map<String, Feedback_Survey_Response__c>();
        
        for (Feedback_Survey_Response__c response : surveyResponses) {
            responsesByChatKey.put(response.ChatKey__c, response);
        }
        
        Map<Id, Feedback_Survey_Response__c> surveyResponsesToUpdateById = new Map<Id, Feedback_Survey_Response__c>();
        for (LiveChatTranscript transcript : Trigger.new) {
            Feedback_Survey_Response__c response = responsesByChatKey.get(transcript.ChatKey);
            
            // If we don't have a response for this chat key, move on to the next one.
            if (response == null) {
                continue;
            }
            
            response.Related_Case__c = transcript.CaseID;
            surveyResponsesToUpdateById.put(response.Id, response);
        }
        
        // Only do the update DML if we actually have records to update.
        if (!surveyResponsesToUpdateById.isEmpty()) {
            update surveyResponsesToUpdateById.values();
        }
    }
}