trigger VerificationAfter on Verification__c (after insert, after update) {

	/**
     * [SL] NAIS-1652: recalc EFC if depreciation fields change 
     */
	Set<Id> verificationIdsToRecalcEFC = new Set<Id>();
	if (Trigger.isUpdate) {
		for (Verification__c newVerification : Trigger.new) {
			Verification__c oldVerification = Trigger.oldMap.get(newVerification.Id);

			if ((newVerification.Depreciation_Sec_179_Exp_4562__c != oldVerification.Depreciation_Sec_179_Exp_4562__c) 
						|| (newVerification.Depreciation_Sec_179_Exp_4562_B__c != oldVerification.Depreciation_Sec_179_Exp_4562_B__c)) {

				verificationIdsToRecalcEFC.add(newVerification.Id);
			}
		}
	}
	if (!verificationIdsToRecalcEFC.isEmpty()) {
		VerificationAction.recalculateEFCs(verificationIdsToRecalcEFC);
	}
}