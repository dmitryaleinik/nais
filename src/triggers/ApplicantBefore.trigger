trigger ApplicantBefore on Applicant__c (before update, before insert) {
	/**
	*	NAIS-503: Any changes made to the application data should be captured in an audit trail
	*   [bc] NAIS-1068: Query PFS relation to Parent B as used in ApplicationUtils.cleanKSApplicantInfo() call... otherwise it is null 
	*	SL, Exponent Partners, 2013
	*/

	// ability to deactivate trigger from hierarchical custom setting
	public static Boolean doTrigger = true;
	try {
		TriggerDeactiviation__c trigDeactivate = TriggerDeactiviation__c.getInstance();
		if (trigDeactivate != null && trigDeactivate.Disable_Triggers__c){
			doTrigger = false;
			System.debug('TRIGGER DEACTIVATED via custom settings');
		}
	} catch (Exception e) {
		System.debug('Error getting custom setting: ' + e);
	}
	//dotrigger=false;
	if(doTrigger && !ApplicationUtils.updatingSiblingCount){
		
		List<Applicant__c> applicantsToProcess = new List<Applicant__c>{};
		Set<Id> pfsIds = new Set<Id>();
		Map<Id, PFS__c> pfsMap = new Map<Id, PFS__c>();
		 
		for(Applicant__c applicant : Trigger.new){
			if(Trigger.isUpdate || (Trigger.isInsert && applicant.Import_ID__c == null)){
				applicantsToProcess.add(applicant);
				
				if (applicant.PFS__c != null) {
					pfsIds.add(applicant.PFS__c);
				}
			}
		}
		
		if (!pfsIds.isEmpty()) {
			pfsMap = new Map<Id, PFS__c> ([ select parent_b__c from pfs__c where id in :pfsIds ]) ;
		}
		 
		for(Applicant__c applicant : applicantsToProcess) {
			if (applicant.PFS__c != null) {
				applicant.PFS__r = pfsMap.get(applicant.PFS__c);
			}
			
			ApplicationUtils.clearApplicantIncome(applicant);
			ApplicationUtils.cleanKSApplicantInfo(applicant);
			ApplicationUtils.cleanKSProgramInfo(applicant);				
		}
			 	
		// Create and/or update Contact records for Applicants being saved
		// ********* COMMENTED OUT BELOW LINE FOR DATA MIGRATION [dp] 11.18.2013 *********
		ApplicationUtils.generateApplicantContacts(applicantsToProcess);
		// ********* COMMENTED OUT ABOVE LINE FOR DATA MIGRATION [dp] 11.18.2013 *********
		
		// [CH] NAIS-1618 Logic for setting history fields for KS integration
		for(Integer i=0; i<Trigger.new.size(); i++){
			Applicant__c newApplicant = Trigger.new[i];
			
			if(newApplicant.Apply_for_KS_K_12_Financial_Aid__c) newApplicant.Apply_for_KS_K_12_Financial_Aid_Hist__c = 'Applied';
			if(newApplicant.Apply_for_KS_Preschool_Financial_Aid__c) newApplicant.Apply_for_KS_Preschool_Fin_Aid_Hist__c = 'Applied';
			if(newApplicant.Apply_for_Pauahi_Keiki_Scholars_Cycle_1__c) newApplicant.Apply_for_PK_Scholars_Cycle_1_Hist__c = 'Applied';
			if(newApplicant.Apply_for_Pauahi_Keiki_Scholars_Cycle_2__c) newApplicant.Apply_for_PK_Scholars_Cycle_2_Hist__c = 'Applied';
			if(newApplicant.Apply_for_Kipona_Scholars_Program__c) newApplicant.Apply_for_Kipona_Scholars_Program_Hist__c = 'Applied';
			
			if(trigger.isInsert){
				if(!newApplicant.Apply_for_KS_K_12_Financial_Aid__c) newApplicant.Apply_for_KS_K_12_Financial_Aid_Hist__c = 'Not Applied';
				if(!newApplicant.Apply_for_KS_Preschool_Financial_Aid__c) newApplicant.Apply_for_KS_Preschool_Fin_Aid_Hist__c = 'Not Applied';
				if(!newApplicant.Apply_for_Pauahi_Keiki_Scholars_Cycle_1__c) newApplicant.Apply_for_PK_Scholars_Cycle_1_Hist__c = 'Not Applied';
				if(!newApplicant.Apply_for_Pauahi_Keiki_Scholars_Cycle_2__c) newApplicant.Apply_for_PK_Scholars_Cycle_2_Hist__c = 'Not Applied';
				if(!newApplicant.Apply_for_Kipona_Scholars_Program__c) newApplicant.Apply_for_Kipona_Scholars_Program_Hist__c = 'Not Applied';
			}
			else if (Trigger.isUpdate) {
				Applicant__c oldApplicant = Trigger.old[i];
				
				if(!newApplicant.Apply_for_KS_K_12_Financial_Aid__c){ 
					if(oldApplicant.Apply_for_KS_K_12_Financial_Aid__c)
						newApplicant.Apply_for_KS_K_12_Financial_Aid_Hist__c = 'Withdrawn';
					else if(newApplicant.Apply_for_KS_K_12_Financial_Aid_Hist__c != 'Withdrawn')
						newApplicant.Apply_for_KS_K_12_Financial_Aid_Hist__c = 'Not Applied';
				}
				
				if(!newApplicant.Apply_for_KS_Preschool_Financial_Aid__c){
					if(oldApplicant.Apply_for_KS_Preschool_Financial_Aid__c)
						newApplicant.Apply_for_KS_Preschool_Fin_Aid_Hist__c = 'Withdrawn';	
					else if(newApplicant.Apply_for_KS_Preschool_Fin_Aid_Hist__c != 'Withdrawn')
						newApplicant.Apply_for_KS_Preschool_Fin_Aid_Hist__c = 'Not Applied';
				}
				
				if(!newApplicant.Apply_for_Pauahi_Keiki_Scholars_Cycle_1__c){
					if(oldApplicant.Apply_for_Pauahi_Keiki_Scholars_Cycle_1__c)
						newApplicant.Apply_for_PK_Scholars_Cycle_1_Hist__c = 'Withdrawn';
					else if(newApplicant.Apply_for_PK_Scholars_Cycle_1_Hist__c != 'Withdrawn')
						newApplicant.Apply_for_PK_Scholars_Cycle_1_Hist__c = 'Not Applied';
				}
				
				if(!newApplicant.Apply_for_Pauahi_Keiki_Scholars_Cycle_2__c){
					if(oldApplicant.Apply_for_Pauahi_Keiki_Scholars_Cycle_2__c)
						newApplicant.Apply_for_PK_Scholars_Cycle_2_Hist__c = 'Withdrawn';	
					else if(newApplicant.Apply_for_PK_Scholars_Cycle_2_Hist__c != 'Withdrawn')
						newApplicant.Apply_for_PK_Scholars_Cycle_2_Hist__c = 'Not Applied';
				}
				
				if(!newApplicant.Apply_for_Kipona_Scholars_Program__c){
					if(oldApplicant.Apply_for_Kipona_Scholars_Program__c)
						newApplicant.Apply_for_Kipona_Scholars_Program_Hist__c = 'Withdrawn';	
					else if(newApplicant.Apply_for_Kipona_Scholars_Program_Hist__c != 'Withdrawn')
						newApplicant.Apply_for_Kipona_Scholars_Program_Hist__c = 'Not Applied';
				}
			}
		}
		
		if (Trigger.isUpdate){
			if (!applicantsToProcess.isEmpty()) {
				AuditLogAction.doAudit(applicantsToProcess, Trigger.oldMap);
			}
		}
		
		/** [CH] NAIS-1620 Update the sibling counts on Applicant records */
		if(trigger.isInsert){
			// Set flag to prevent recursion
			ApplicationUtils.updatingSiblingCount = true;
			// Make updates to Applicant records related to the relevant PFSs
			PFSAction.updateSiblingCounts(trigger.new, false);
			// Turn the flag back off
			ApplicationUtils.updatingSiblingCount = false;
		} 
	}
}