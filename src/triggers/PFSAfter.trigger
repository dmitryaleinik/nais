/*
 *  Calculate Unusual Conditions for any PFS that is saved in a submitted or post-submitted state
 *  DP, Exponent Partners, 2013
*/
trigger PFSAfter on PFS__c (after insert, after update) {
    // ability to deactivate trigger from hierarchical custom setting
    public static Boolean doTrigger = true;
    try {
        TriggerDeactiviation__c trigDeactivate = TriggerDeactiviation__c.getInstance();
        if (trigDeactivate != null && trigDeactivate.Disable_Triggers__c){
            doTrigger = false;
            System.debug('TRIGGER DEACTIVATED via custom settings');
        }
    } catch (Exception e) {
        System.debug('Error getting custom setting: ' + e);
    }

    if(!doTrigger) {
        return;
    }

    /* [SL] Beginning of trigger execution */
    PFSAction.PFSAfterTriggerControl.beginTrigger();

    Set<Id> pfsIdsToProcess = new Set<Id>();
    Schema.DescribeSObjectResult pfsDescribe = PFS__c.sObjectType.getDescribe();
    Set<String> pfsFieldNames = pfsDescribe.fields.getMap().keySet();
    List<String> UCFieldNames = UnusualConditionsCalculation.getUCFieldNames();
    Set<Id> pfsRecordsIdsForBusFarm = new Set<Id>(); // [CH] NAIS-2557

    // only going to process submitted or revised PFSs
    for (PFS__c p : Trigger.new){
        // NAIS-2557 Moving call to remove Business Farm records above UC call to avoid losing trigger context because of update call in UC
        if((Trigger.isInsert || Trigger.isUpdate) && (EfcDataManager.isUpdatingSssEfcCalc != true) && (EfcCalculatorAction.getIsDisabledSssAutoCalc() != true)){
            // [CH] NAIS-2495 Check to see if any PFS records need Business/Farm records managed
            if((Trigger.isInsert && p.Number_of_Businesses__c != null) || (Trigger.isUpdate && p.Number_of_Businesses__c != Trigger.oldMap.get(p.Id).Number_of_Businesses__c)){
                pfsRecordsIdsForBusFarm.add(p.Id);
            }
        }

        if (p.PFS_Status__c == 'Application Submitted'){
            Boolean recalcUC;
            // on update we're only going to recalc if one of the relevant fields is updated
            if (Trigger.isUpdate){
                recalcUC = false;
                PFS__c oldP = trigger.oldMap.get(p.Id);

                for (String fieldname : UCFieldNames){
                    if (pfsFieldNames.contains(fieldname.toLowerCase())){
                        if (p.get(fieldname) != oldP.get(fieldname)){
                            recalcUC = true;
                        }
                    }
                }
                // always calc on insert
            } else if (Trigger.isInsert){
                recalcUC = true;
            }

            // add id
            if (recalcUC){
                pfsIdsToProcess.add(p.Id);
            }
        }
    }

    // [CH] NAIS-2495 Add or remove business farm records when necessary prior to doing EFC calculations
    // [CH] NAIS-2557 Moved above the call to UC code
    if(!pfsRecordsIdsForBusFarm.isEmpty()){
        PFSAction.manageBusinessFarmRecords(pfsRecordsIdsForBusFarm);
    }

    if (!pfsIdsToProcess.isEmpty() && UnusualConditionsCalculation.isFirstRun){
        UnusualConditionsCalculation.isFirstRun = false;
        UnusualConditionsCalculation.calculateConditions(pfsIdsToProcess, true);
    }

   /*
    * NAIS-243, 397
     *
     * Info on the parent new/edit page should pull from the PFS record, not the contact record,
     * for both parent A and parent B.
     *
     * Use the Parent A and Parent B fields on the PFS object for all info on the page
     * (name, address, birthdate, gender, etc). All fields should be created in config on the PFS object;
     *
     * When updated on this page, the corresponding field on the contact record should also be updated.
     *
     * Nathan, Exponent Partners, 2013
     */
    if (Trigger.isUpdate) {
        if (SynchronizeObjects.isSyncPFSParentsToContactExecuting != true) {
            SynchronizeObjects syncObj = new SynchronizeObjects();
            syncObj.pfsOldMap = Trigger.OldMap;
            syncObj.pfsNewMap = Trigger.NewMap;
            syncObj.SyncPFSParentsToContact();
        }
    }

    /**
     *    SFP-640 Determine if the filing status has changed for expected number of 1040s
     */
    if (Trigger.isUpdate){
        VerificationAction.setVerificationExpected1040BasedOnFilingStatus(Trigger.newMap, Trigger.oldMap);
    }
        /* END SFP-650 */

    /*
     * NAIS-499, SPEC-047 Finance - PFS Sale, R-246
     *    When an application is sufbmitted for the first time, if an opportunity of type Application Fee does not already exist on the PFS record,
     *    it needs to be created, and a transaction line item for the cost of the Application needs to be created as well. This would be a
     *    transaction line item of type PFS Sale.
     *
     * WH, Exponent Partners, 2013
     */
    List<PFS__c> newlySubmittedPFSs = new List<PFS__c>();

    if (Trigger.isInsert) {
        for (PFS__c pfs : Trigger.new) {
            if (pfs.PFS_Status__c == 'Application Submitted') {
                newlySubmittedPFSs.add(pfs);
            }
        }
    } else if (Trigger.isUpdate) {
        for (PFS__c pfs : Trigger.new) {
            if (pfs.PFS_Status__c == 'Application Submitted' && Trigger.oldMap.get(pfs.Id).PFS_Status__c != 'Application Submitted') {
                newlySubmittedPFSs.add(pfs);
            }
        }
    }

    if (!newlySubmittedPFSs.isEmpty()) {
        FinanceAction.createOffAndTransactionOnlyIfWaived(newlySubmittedPFSs);
    }

        /*
        * SPEC-017
        * [SL 5/4/13] Recalculate EFC when SSS EFC Calc Status = Recalculate
        */
    if (Trigger.isInsert || Trigger.isUpdate) {
        if ((EfcDataManager.isUpdatingSssEfcCalc != true) && (EfcCalculatorAction.getIsDisabledSssAutoCalc() != true)) {
            Set<Id> pfsIdsToRecalc = new Set<Id>();
            for (PFS__c pfs : Trigger.new) {
                if (pfs.SSS_EFC_Calc_Status__c == EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE) {
                    pfsIdsToRecalc.add(pfs.Id);
                }
            }

            if (!pfsIdsToRecalc.isEmpty()) {
                try {
                    EfcCalculatorAction.calculateSssEfc(pfsIdsToRecalc, true);
                } catch (EfcException.EfcMissingDataException e) {
                    GlobalVariables.sendExceptionEmail('Efc Data Missing', e);
                }

                PFSAction.pfsIdsWithRecalculatedEfcs.addAll(pfsIdsToRecalc);
            }
        }
    }

    /*
     * NAIS-545: Maintain a copy of the SSS/parent/PFS fields on the School PFS Assignment record
     * Note: this should be run after all other trigger logic that updates the PFS, to make sure all updated values get copied as well
     * SL, Exponent Partners, 2013
     */
    // Since this is a potentially recursive trigger, we want to make sure this logic is only run at the outermost level (i.e. not called from a
    // recursive context), so that all other updates are complete before values are copied to the school pfs assignment
    if (PFSAction.PFSAfterTriggerControl.isRecursiveContext() == false) {
        if (Trigger.isUpdate) {

            Set<Id> pfsIdsToSyncPfsStatus = new Set<Id>();
            Set<Id> pfsIdsToCopy = new Set<Id>();
            Set<Id> pfsIdsToCopyPJ = new Set<Id>(); // [SL] NAIS-1021
            Map<Id, PFS__c> pfsRecordsUpdatedByFamily = new Map<Id, PFS__c>(); // [CH] NAIS-1785
            for (PFS__c pfs : Trigger.new) {
                PFS__c oldPfs = Trigger.oldMap.get(pfs.Id);

                // only copy SSS fields for submitted applications
                if (pfs.PFS_Status__c == 'Application Submitted') {
                    if ((oldPfs.PFS_Status__c != 'Application Submitted')
                            || (PFSAction.isUpdatedSssFields(pfs, oldPfs))
                            || PFSAction.pfsIdsWithRecalculatedEfcs.contains(pfs.Id)) {
                        pfsIdsToCopy.add(pfs.Id);
                    }
                }

                // for all changed PFS statuses, copy the value to the School PFS Assignment
                if (pfs.PFS_Status__c != oldPfs.PFS_Status__c) {
                    // only need to copy the status if it's not already being copied with the rest of the SSS fields
                    if (!pfsIdsToCopy.contains(pfs.Id)) {
                        pfsIdsToSyncPfsStatus.add(pfs.Id);
                    }
                }

                // if the PFS changes to submitted/paid, then copy the default PJ settings from annual settings
                if ((pfs.PFS_Status__c == EfcPicklistValues.PFS_STATUS_SUBMITTED) && (pfs.Payment_Status__c == EfcPicklistValues.PAYMENT_STATUS_PAID_IN_FULL)) {
                    if ((oldPfs.PFS_Status__c != EfcPicklistValues.PFS_STATUS_SUBMITTED) || (oldPfs.Payment_Status__c != EfcPicklistValues.PAYMENT_STATUS_PAID_IN_FULL)) {
                        pfsIdsToCopyPJ.add(pfs.Id);
                    }
                }

                // [CH] NAIS-1785 Combining this update with other updates
                if (pfs.Application_Last_Modified_by_Family__c != oldPfs.Application_Last_Modified_by_Family__c) {
                    pfsRecordsUpdatedByFamily.put(pfs.Id, pfs);
                }

            }

            // [CH] NAIS-1785 Updating to combine SPA updates
            if ((!pfsIdsToCopy.isEmpty()) || (!pfsIdsToCopyPJ.isEmpty()) || (!pfsRecordsUpdatedByFamily.values().isEmpty())) {
                // Update the school pfs assignments
                // Note: we are passing in the PFS Ids to reload the PFS records, since they may have changed from the original trigger context
                PFSAction.updateFieldsOnSchoolPfsAssignmentsForPfsIds(pfsIdsToCopy, pfsIdsToCopyPJ, pfsRecordsUpdatedByFamily);
            }

            if (!pfsIdsToSyncPfsStatus.isEmpty()) {
                PFSAction.updatePfsStatusOnSchoolPfsAssignmentsForPfsIds(pfsIdsToSyncPfsStatus);
            }
        }
    }

    /* [CH] NAIS-1525 Updating Student Folder statuses to submitted if PFS is paid and submitted */
    Set<Id> pfsIdsToQuery = new Set<Id>();
    for (PFS__c pfsRecord : trigger.new) {
        if(pfsRecord.PFS_Status__c == EfcPicklistValues.PFS_STATUS_SUBMITTED && pfsRecord.Payment_Status__c == EfcPicklistValues.PAYMENT_STATUS_PAID_IN_FULL){
            pfsIdsToQuery.add(pfsRecord.Id);
        }
    }

    Student_Folder_Statuses__c unsubmittedStatus = Student_Folder_Statuses__c.getValues('Unsubmitted');
    Student_Folder_Statuses__c submittedStatus = Student_Folder_Statuses__c.getValues('Submitted');

    String unsubmittedValue = unsubmittedStatus != null ? unsubmittedStatus.Status_Value__c : 'Unsubmitted';
    String submittedValue = submittedStatus != null ? submittedStatus.Status_Value__c : 'Submitted';

    // If there are any PFS records that might need Student Folder updates
    if (pfsIdsToQuery.size() > 0) {
        List<Student_Folder__c> studentFoldersToUpdate = new List<Student_Folder__c>{};
        Set<Id> studentFoldersToUpdateIds = new Set<Id>();

        for(School_PFS_Assignment__c spaRecord : [SELECT Student_Folder__c, Student_Folder__r.Folder_Status__c
                                                    FROM School_PFS_Assignment__c
                                                   WHERE Student_Folder__r.Folder_Status__c = :unsubmittedValue
                                                     AND Applicant__r.PFS__c IN :pfsIdsToQuery]) {
            // [DP] NAIS-1570 to avoid duplicate id errors, check to make sure we are only updating the folder status once
            if (!studentFoldersToUpdateIds.contains(spaRecord.Student_Folder__c)){
                studentFoldersToUpdateIds.add(spaRecord.Student_Folder__c);
                studentFoldersToUpdate.add(new Student_Folder__c(Id=spaRecord.Student_Folder__c, Folder_Status__c = submittedValue));
            }
        }

        if(studentFoldersToUpdate.size() > 0){
            Database.update(studentFoldersToUpdate);
        }
    }

    // NAIS-2068 [DP] 12.18.2014 Refactor to trigger Total PFS Count and Weighted PFS Count from PFS Update
    Set<Id> pfsIdsForWPFSCountRecalc= new Set<Id>();
    List<PFS__c> pfssToUpdate = new List<PFS__c>();
    if (Trigger.isUpdate){
        for (PFS__c pfs : trigger.new){
            PFS__c oldPFS = trigger.oldMap.get(pfs.ID);
            if (pfs.PFS_Status__c != oldPFS.PFS_Status__c || pfs.Payment_Status__c != oldPFS.Payment_Status__c || pfs.Fee_Waived__c != oldPFS.Fee_Waived__c
                    || (pfs.SYSTEM_WPFS_Recalc__c == true && pfs.SYSTEM_WPFS_Recalc__c != oldPFS.SYSTEM_WPFS_Recalc__c)    // NAIS-2402 [DP] 04.28.2015
                    ){
                pfsIdsForWPFSCountRecalc.add(pfs.ID);
                // We want to uncheck the recalc flag
                if (pfs.SYSTEM_WPFS_Recalc__c == true) {
                    pfssToUpdate.add(new PFS__c(Id = pfs.Id, SYSTEM_WPFS_Recalc__c = false));
                }
            }
        }
    }

    if (!pfsIdsForWPFSCountRecalc.isEmpty() && SchoolSetWeightedPFS.isExecutingFromPFS == false) {
        PfsService.Instance.queueSetWeightedPfs(pfsIdsForWPFSCountRecalc, pfssToUpdate);
    }

    /* [SL] End of trigger execution */
    PFSAction.PFSAfterTriggerControl.endTrigger();
}