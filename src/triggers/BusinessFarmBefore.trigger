trigger BusinessFarmBefore on Business_Farm__c (before insert, before update) {

	for (Business_Farm__c bf :trigger.new){
		// NAIS-2500 [DP] 09.21.2015 need to default to null if not set/sole Proprietorship
        if(bf.Business_Entity_Type__c == null || bf.Business_Entity_Type__c == 'Sole Proprietorship') {
            bf.Business_Farm_Ownership_Percent__c = 100;         
        }	

        // [DP] 02.22.2016 SFP-318 set totals on biz farm
        BusinessFarmAction.setTotalsOnBizFarm(bf);
	}

}