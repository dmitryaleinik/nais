/**
* @SchoolDocumentAssignmentBefore.trigger
* @date 2/28/2014
* @author Charles Howard for Exponent Partners 
* @description Initially created for NAIS-1590
*/

trigger SchoolDocumentAssignmentBefore on School_Document_Assignment__c (before insert, before update) {

	// [CH] NAIS-1590
	// Look up the School-Specific Document type
	String schoolSpecificDocTypeVal = ApplicationUtils.schoolSpecificDocType();

	// Collect the list of Required Document records related to School-Specific 
	//	SDA records that need the label field filled in
	Set<Id> requiredDocumentIds = new Set<Id>{};
	for(School_Document_Assignment__c sdaRecord : Trigger.new){
		
		if(sdaRecord.Required_Document__c != null && sdaRecord.Label_for_School_Specific_Document__c == null && sdaRecord.Document_Type__c == schoolSpecificDocTypeVal){
			requiredDocumentIds.add(sdaRecord.Required_Document__c);
		}
	}

	// Query for related Required Document records
	Map<Id, Required_Document__c> requiredDocumentMap;
	if(requiredDocumentIds.size() > 0){
		requiredDocumentMap = new Map<Id, Required_Document__c>([select Id, Label_for_School_Specific_Document__c from Required_Document__c where Id in :requiredDocumentIds]); 
	}

	// Fill in the School-Specific Document Label for appropriate SDA records
	if(requiredDocumentMap != null && requiredDocumentMap.values().size() > 0){
		for(School_Document_Assignment__c sdaRecord : Trigger.new){
			Required_Document__c matchingDocument = requiredDocumentMap.get(sdaRecord.Required_Document__c);
			if(matchingDocument != null){
				sdaRecord.Label_for_School_Specific_Document__c = matchingDocument.Label_for_School_Specific_Document__c;
			}
		}
	}

	/**
	*	SFP-42 SDA Trigger updates SDA Flag to 'Verify_Data__c' for each school document
	*/
	if (Trigger.isInsert) {
		SchoolDocumentAssignments documentAssignments = SchoolDocumentAssignments.newInstance(Trigger.new);
		documentAssignments.handleBeforeInsert();
	}

	// SFP-1150: Flag any records that need to have their document resent for verification in case the document assigned
	// to an SDA has changed as a result of a type/year mismatch. The mismatch scenarios need to be handled in case a
	// family uploads a tax doc to the wrong year slot and when it comes back from springCM we discover that it needs to
	// be verified as a current year doc.
	if (Trigger.isUpdate) {
		SchoolDocumentAssignments documentAssignments = SchoolDocumentAssignments.newInstance(Trigger.new);
		documentAssignments.handleBeforeUpdate(Trigger.oldMap);
	}
}