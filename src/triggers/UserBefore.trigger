/*
 * Spec-074 School Portal - Delegated User Administration; Req# R-109, R-343
 *  Allow school Admins to utilize the delegated user administration feature, but cap the number of licenses they can delegate based on the
 *  Max_Number_of_Users__c field for the license count on the Account, and a custom setting. The custom setting has the default that can be 
 *  overwritten on the Account.
 *
 * WH, Exponent Partners, 2013
 */
trigger UserBefore on User (before insert, before update) {
	
	List<User> users = new List<User>();
	Set<Id> contactIds = new Set<Id>();
	
	if (Trigger.isInsert) {
		for (User u : Trigger.new) {
			if (u.IsActive && u.ContactId != null) {		// can't use IsPortalEnabled bcoz it is not set before insert
				
				// Added by Doug Friedman for ticket 765
				if (GlobalVariables.isSchoolPortalProfile(u.ProfileId) || GlobalVariables.isFamilyPortalProfile(u.ProfileId)) {
					u.SpringCMEos__SpringCM_User__c = true;
					u.SpringCMEos__SpringCM_Role__c = 'Full Subscriber';
					u.SpringCM_PortalUser__c = true; // [DP] NAIS-1786 -- mark as Portal User
				}
				
				// inserting active portal user
				if (!GlobalVariables.isFamilyPortalProfile(u.ProfileId)) {	// family portal users not subject to per-account user limit
					users.add(u);
					contactIds.add(u.ContactId);
				}
				
			}
		}
	} else if (Trigger.isUpdate) {
		for (User u : Trigger.new) {
            // If we are updating the current user's record, clear the current user session cache.
			if (u.Id == UserInfo.getUserId()) {
                CurrentUser.clearCache();
            }

			if (u.IsActive && u.ContactId != null && !Trigger.oldMap.get(u.Id).IsActive) {
				// activating inactive portal user
				if (!GlobalVariables.isFamilyPortalProfile(u.ProfileId)) {	// family portal users not subject to per-account user limit
					users.add(u);
					contactIds.add(u.ContactId);
				}
			}
		}
	}

	// [DP] NAIS-2025 - sync fields for all users
	for (User u : trigger.new){
		if (u.SpringCMEos__Portal_Only__c != u.SpringCM_PortalUser__c){
			u.SpringCMEos__Portal_Only__c = u.SpringCM_PortalUser__c;
		}
	}
	
	/*
	 * Spec-074 School Portal - Delegated User Administration; Req# R-109, R-343
	 */
	if (!users.isEmpty()) {
		UserAction.checkSchoolPortalUserLimit(users, contactIds);
	}
}