trigger ApplicantAfter on Applicant__c (after insert, after update, after delete) {
    // ability to deactivate trigger from hierarchical custom setting
    public static Boolean doTrigger = true;
    try {
        TriggerDeactiviation__c trigDeactivate = TriggerDeactiviation__c.getInstance();
        if (trigDeactivate != null && trigDeactivate.Disable_Triggers__c){
            doTrigger = false;
            System.debug('TRIGGER DEACTIVATED via custom settings');
        }
    } catch (Exception e) {
        System.debug('Error getting custom setting: ' + e);
    }

    if (doTrigger){
        /* [SL] Beginning of trigger execution */
        PFSAction.ApplicantAfterTriggerControl.beginTrigger(); 
        
        // If the trigger is firing due to an update to sibling count then
        //   don't run anything
        
        if(!ApplicationUtils.updatingSiblingCount){
        
            // [CH] NAIS-1620 Have to make this delete-safe
            Set<Id> pfsIds = new Set<Id>();
            Map<Id, PFS__c> pfsMap = new Map<Id, PFS__c>{}; 
            Map<Id, PFS__c> pfsMapForUpdate = new Map<Id, PFS__c>{}; 
            
            if(!trigger.isDelete){
                /* get PFS records to filter for applicants that have submitted status */
                
                for (Applicant__c applicant : Trigger.new) {
                    if (applicant.PFS__c != null) {
                        pfsIds.add(applicant.PFS__c);
                    }
                }
                pfsMap = new Map<Id, PFS__c> ([SELECT Id, PFS_Status__c, Payment_Status__c, Visible_to_Schools_Until__c FROM PFS__c WHERE Id IN :pfsIds]);
            }
            
            /*
            * NAIS-564: Trigger to calculate SSS EFC
            * SL, Exponent Partners, 2013
            */ 
         
            if (Trigger.isInsert || Trigger.isUpdate) {
                // Set<Id> pfsIdsToRecalculate = new Set<Id>(); [CH] NAIS-1785 No longer needed
                
                // List<PFS__c> pfsRecordsToUpdate = new List<PFS__c>();
                // Map<Id, PFS__c> pfsRecordUpdateMap = new Map<Id, PFS__c>();
                
                // [CH] NAIS-1785 Moved up from below
                // if ( Trigger.isInsert || Trigger.isUpdate) {
                    if ( PFSAction.profileCanUpdatePFSApplicationModifiedDate() ) {
                        
                        // [SL] BEGIN NAIS-966 - pass in the trigger.old records as well to test if EFC fields have changed 
                        // [CH] NAIS-1785 Updated to pass in and get back a Map, to consolidate DML statements 
                        if (Trigger.isUpdate) {
                            pfsMapForUpdate = PFSAction.updatePFSLastModifiedByFamilyDate(Trigger.new, Trigger.oldMap, pfsMap);
                        }
                        else {
                            pfsMapForUpdate = PFSAction.updatePFSLastModifiedByFamilyDate(Trigger.new, null, pfsMap);
                        }
                        // [SL] END NAIS-966
                    }
                // }
                
                for (Applicant__c applicant : Trigger.new) {
                    // added the following to this line - DP 6.4.13:
                    //                         "Trigger.oldMap == null ? null : " 
                    Applicant__c oldApplicant = Trigger.oldMap == null ? null : Trigger.oldMap.get(applicant.Id);
                    if (EfcCalculatorAction.isEfcRecalcNeeded(applicant, oldApplicant)) {
                        if (applicant.PFS__c != null) {
                            // Get the PFS record from the map
                            PFS__c pfs = pfsMap.get(applicant.PFS__c);
                            
                            if(pfs != null){ // [CH] NAIS-1785 Moved up to avoid isStatusRecalculable() with a null PFS
                                // NAIS-1065
                                // Note:  In the case that the Map does not already contain a matching PFS, do not recalculate EFC
                                Boolean recalculable = EfcCalculatorAction.isStatusRecalculable(pfs.PFS_Status__c, pfs.Payment_Status__c, pfs.Visible_to_Schools_Until__c);
                                if (recalculable) {
                                    // [CH] NAIS-1785 Refactored
                                    // pfsIdsToRecalculate.add(applicant.PFS__c);
                                    pfs.SSS_EFC_Calc_Status__c = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE;
                                    pfsMapForUpdate.put(pfs.Id, pfs);
                                }
                            }
                        }
                    }
                }
                
                // [CH] NAIS-1785 If there are records in the PFS map to be updated
                if(!pfsMapForUpdate.values().isEmpty()){
                    Database.update(pfsMapForUpdate.values());
                }
                /* [CH] NAIS-1785 Simplified and refactored above
                if (!pfsIdsToRecalculate.isEmpty()) {
                    //EfcCalculatorAction.calculateSssEfc(pfsIdsToRecalculate, true);
                    List<PFS__c> pfsRecordsToUpdateEfcStatus = new List<PFS__c>();
                    for (Id pfsId : pfsIdsToRecalculate) {
                        PFS__c pfs = pfsMap.get(pfsId);
                        pfs.SSS_EFC_Calc_Status__c = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE;
                        pfsRecordsToUpdateEfcStatus.add(pfs);
                    }
                    if (!pfsRecordsToUpdateEfcStatus.isEmpty()) {           
                        update pfsRecordsToUpdateEfcStatus;
                    }
                }
                */
                
                ApplicationUtils.setupKSRequiredDocuments(Trigger.New);
            }
            
            
            /*
            * NAIS-545: Maintain a copy of the SSS/parent/PFS fields on the School PFS Assignment record
            * SL, Exponent Partners, 2013
            */
        
            // Since this is a potentially recursive trigger, we want to make sure this logic is only run at the outermost level (i.e. not called from a  
            // recursive context), so that all other updates are complete before values are copied to the school pfs assignment
            System.debug(LoggingLevel.ERROR, 'TESTING**Drew120 ' + PFSAction.ApplicantAfterTriggerControl.isRecursiveContext());
            if (PFSAction.ApplicantAfterTriggerControl.isRecursiveContext() == false) {
                if (Trigger.isUpdate) {
                    Set<Id> applicantIdsToCopy = new Set<Id>();
                        for (Applicant__c applicant : Trigger.new) {
                            Applicant__c oldApplicant = Trigger.oldMap.get(applicant.Id);
                            System.debug(LoggingLevel.ERROR, 'TESTING***DREW126 ' + PFSAction.isUpdatedApplicantSssFields(applicant, oldApplicant));
                            if ((PFSAction.isUpdatedApplicantSssFields(applicant, oldApplicant)) 
                                && (applicant.PFS__c != null)
                                && (pfsMap.get(applicant.PFS__c).PFS_Status__c == EfcPicklistValues.PFS_STATUS_SUBMITTED)) {
                                applicantIdsToCopy.add(applicant.Id);
                                System.debug(LoggingLevel.ERROR, 'TESTING***131 made it through IF' );
                            }
                        }
                        if (!applicantIdsToCopy.isEmpty()) { 
                            // Update the school pfs assignments
                            PFSAction.updateSssFieldsOnSchoolPfsAssignmentsForApplicantIds(applicantIdsToCopy, true);
                        }
                }
            }
            
            /** NAIS-965: [JIMI] Populate the Application_Last_Modified_by_Family__c field on the school pfs where Family_May_Submit_Updates__c = Yes.  */
            /* [CH] NAIS-1785 Moved to combine with PFS updates above
            if ( Trigger.isInsert || Trigger.isUpdate) {
                if ( PFSAction.profileCanUpdatePFSApplicationModifiedDate() ) {
                    
                    // [SL] BEGIN NAIS-966 - pass in the trigger.old records as well to test if EFC fields have changed
                    // PFSAction.updatePFSLastModifiedByFamilyDate(Trigger.new);    
                    if (Trigger.isUpdate) {
                        PFSAction.updatePFSLastModifiedByFamilyDate(Trigger.new, Trigger.oldMap);
                    }
                    else {
                        PFSAction.updatePFSLastModifiedByFamilyDate(Trigger.new, null);
                    }
                    // [SL] END NAIS-966
                }
            }
            */
            
            /** [CH] NAIS-1620 */ 
            if(trigger.isDelete){
                // Set flag to prevent recursion
                ApplicationUtils.updatingSiblingCount = true;
                // Make updates to Applicant records related to the relevant PFSs
                PFSAction.updateSiblingCounts(trigger.old, true);
                // Turn the flag back off
                ApplicationUtils.updatingSiblingCount = false;
            }
        }
        
        /* [SL] End of trigger execution */
        PFSAction.ApplicantAfterTriggerControl.endTrigger();
    }
}