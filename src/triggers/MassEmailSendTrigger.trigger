/**
 * MassEmailSendTrigger.trigger
 *
 * @description Single trigger for the Mass_Email_Send__c object, context is passed via 
 * MassEmailSendTriggerHandler, which accomplishes work via service classes specific 
 * to trigger scope and use case following the Factory pattern.
 *
 * @author Chase Logan @ Presence PG
 */
trigger MassEmailSendTrigger on Mass_Email_Send__c ( before insert, before update, after insert, after update,
													 before delete, after delete, after undelete) {

	// create and execute the TriggerHandler instance for this trigger
	TriggerFactory.createAndExecuteHandler( MassEmailSendTriggerHandler.class);
	
}