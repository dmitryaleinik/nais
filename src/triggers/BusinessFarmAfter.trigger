trigger BusinessFarmAfter on Business_Farm__c (after insert, after update, after delete, after undelete) {

	BusinessFarmAction.handleAfterInsertAndAfterUpdate(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap);

}