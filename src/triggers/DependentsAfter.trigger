trigger DependentsAfter on Dependents__c (after insert, after update) {
	
	/** NAIS-965: [JIMI] Populate the Application_Last_Modified_by_Family__c field on the school pfs where Family_May_Submit_Updates__c = Yes.  */
	
	// [CH] NAIS-2276 Refactoring to make all updates to PFS in one DML statement
	// PFSAction.updatePFSLastModifiedByFamilyDate(Trigger.new);
	List<PFS__c> pfsToUpdate = new List<PFS__c>();

	for (Dependents__c dep : trigger.new) {
		
		PFS__c pfs = new PFS__c(id = dep.PFS__c);
		
		if ( PFSAction.profileCanUpdatePFSApplicationModifiedDate() ) {
			pfs.Application_Last_Modified_by_Family__c = PFSAction.LastModifiedTime;
		}
		
		pfs.Number_of_Dependent_Children__c = dep.Number_of_Dependents__c;
		pfsToUpdate.add(pfs);
	}

	update pfsToUpdate;
}