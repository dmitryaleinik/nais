trigger FamilyDocumentAfter on Family_Document__c (after insert, after update, after delete)
{

    /*
    * SPEC-099
    * Once all of the required documents have been provided and verified, the (document) stage of the folder should change
    *
    * Nathan, Exponent Partners, 2013
    */

    // ability to deactivate trigger from hierarchical custom setting
    public static Boolean doTrigger = true;

    try {
        TriggerDeactiviation__c trigDeactivate = TriggerDeactiviation__c.getInstance();
        if (trigDeactivate != null && trigDeactivate.Disable_Triggers__c) {
            doTrigger = false;
            System.debug('TRIGGER DEACTIVATED via custom settings');
        }
    } catch (Exception e) {
        System.debug('Error getting custom setting: ' + e);
    }

    if (doTrigger) {
        System.debug(LoggingLevel.WARN, '###family document after trigger');
        // [SL] NAIS-282 BEGIN code refactor
        /* [CH] NAIS-2442 REMOVED OLD CODE */

        Set<Id> deletedFamilyDocumentIds = new Set<Id>();
        Set<Id> familyDocumentIds = new Set<Id>();
        Set<Id> familyDocIdsToRecalcEFC = new Set<Id>();
        // SFP-640 rerun document status calculation when deleting processed documents
        Set<Id> householdIdsWithDeletedDocs = new Set<Id>();
        // SFP-640
        Set<Family_Document__c> familyDocsToUpdateDocumentStatus = new Set<Family_Document__c>();
        // SFP-811
        Set<Family_Document__c> familyDocumentsToRunSPABatch = new Set<Family_Document__c>();
        if (Trigger.isUpdate) {
            for (Family_Document__c newFamilyDoc : Trigger.new) {
                Family_Document__c oldFamilyDoc = Trigger.oldMap.get(newFamilyDoc.Id);

                // only need to recalc overall status if the Document Status, or Document Type changes, or the document is marked/unmarked for deletion
                if ((newFamilyDoc.Document_Status__c != oldFamilyDoc.Document_Status__c)
                        || (newFamilyDoc.Document_Type__c != oldFamilyDoc.Document_Type__c)
                        || (newFamilyDoc.Deleted__c != oldFamilyDoc.Deleted__c)) {
                    
                    familyDocumentIds.add(newFamilyDoc.Id);
                    
                    //SFP-1118
                    if(newFamilyDoc.Deleted__c && newFamilyDoc.Deleted__c != oldFamilyDoc.Deleted__c) {
                        deletedFamilyDocumentIds.add(newFamilyDoc.Id);
                    }
                }
                
                /**
                 * [SL] NAIS-1652: recalc EFC if depreciation fields change
                 */
                // [CH] NAIS-2442 combined For loop bodies to eliminate a loop
                if ((newFamilyDoc.Sch_C_Depreciation_Sect179__c != oldFamilyDoc.Sch_C_Depreciation_Sect179__c)
                        || (newFamilyDoc.Sch_F_Depreciation_Sec179_Exp__c != oldFamilyDoc.Sch_F_Depreciation_Sec179_Exp__c)
                        || (newFamilyDoc.Sch_E_Depreciation_Total__c != oldFamilyDoc.Sch_E_Depreciation_Total__c)
                        || (newFamilyDoc.Sch_C_Exp_Bus_Uses_Home__c != oldFamilyDoc.Sch_C_Exp_Bus_Uses_Home__c)
                        || ((newFamilyDoc.Deleted__c != oldFamilyDoc.Deleted__c) && FamilyDocumentAction.isPopulatedDepreciationFields(oldFamilyDoc))) {
                    familyDocIdsToRecalcEFC.add(newFamilyDoc.Id);
                }

                // SFP-640 [9.18.16] Add check just these documents [jB]
                // SFP-813 [10.17.16] Run status checks when 1040s are set to processed as well
                if (newFamilyDoc.Document_Status__c == VerificationAction.PROCESSED &&
                        ((newFamilyDoc.W2_Wages__c != oldFamilyDoc.W2_Wages__c)
                        || (newFamilyDoc.Sch_C_Business_Income__c != oldFamilyDoc.Sch_C_Business_Income__c)
                        || (newFamilyDoc.Sch_E_Total_Rent_Restate_Royalty_Income__c != oldFamilyDoc.Sch_E_Total_Rent_Restate_Royalty_Income__c)
                        || (newFamilyDoc.Sch_F_Farm_Income__c != oldFamilyDoc.Sch_F_Farm_Income__c)
                        || new Set<String>(VerificationAction.X1040_DOCUMENT_TYPES).contains(newFamilyDoc.Document_Type__c)
                        || (newFamilyDoc.Duplicate__c != oldFamilyDoc.Duplicate__c)))
                {
                    familyDocsToUpdateDocumentStatus.add(newFamilyDoc);
                }

                // SFP-787 & SFP-811
                if(newFamilyDoc.Document_Status__c == VerificationAction.PROCESSED && oldFamilyDoc.Document_Status__c
                    != VerificationAction.PROCESSED && VerificationAction.isDocTypeVerifiable(newFamilyDoc.Document_Type__c))
                {
                    familyDocumentsToRunSPABatch.add(newFamilyDoc);
                }
            }
        }

        /**
         * SFP-841 [10.19.16] SpringCM Processor can insert new Family Documents in the "PROCESSED" state
         *                      as a result verification values and sums are not being set as they would
         *                      when SpringCM updates an existing FD. For new FD run the document status
         *                      and sum / auto-revision batches [jB]
         */
        if(Trigger.isInsert){

            for (Family_Document__c newFamilyDoc : Trigger.new) {
                if(newFamilyDoc.Document_Status__c == VerificationAction.PROCESSED &&
                     (newFamilyDoc.W2_Wages__c != null ||
                    newFamilyDoc.Sch_C_Business_Income__c != null ||
                    newFamilyDoc.Sch_E_Total_Rent_Restate_Royalty_Income__c != null ||
                    newFamilyDoc.Sch_F_Farm_Income__c != null ||
                    VerificationAction.isDocTypeVerifiable(newFamilyDoc.Document_Type__c))){
                        familyDocsToUpdateDocumentStatus.add(newFamilyDoc);
                }

                // After insert, just run batch. Doc type was only checked after update.
                if(newFamilyDoc.Document_Status__c == VerificationAction.PROCESSED &&
                    VerificationAction.isDocTypeVerifiable(newFamilyDoc.Document_Type__c))
                {
                    familyDocumentsToRunSPABatch.add(newFamilyDoc);
                }
            }
        }
        /** END SFP-841 **/

        if (!familyDocumentIds.isEmpty())
        {
            SchoolSetSPFSDocumentStatus.updateDocumentStatusFromFamilyDocuments(familyDocumentIds, false, deletedFamilyDocumentIds);
        }

        // SFP-640 [9.3.16] add verification completeness check [jB]
        if (!familyDocsToUpdateDocumentStatus.isEmpty())
        {
            VerificationAction.setVerficationStatusValues(familyDocsToUpdateDocumentStatus, null);
        }
        // END SFP-640

        // SFP-811 - run this copy AFTER setting other values on SPA, Verification, etc.
        if (!familyDocumentsToRunSPABatch.isEmpty())
        {
            FamilyDocumentAction.runAutoVerifyCopy(familyDocumentsToRunSPABatch);
        }
        // END SFP-811

        // [CH] NAIS-2442 combined For loop bodies to eliminate a loop
        if (!familyDocIdsToRecalcEFC.isEmpty())
        {
            FamilyDocumentAction.recalculateEFCsFromFamilyDocs(familyDocIdsToRecalcEFC);
        }

        // [SL] NAIS-282 END code refactor

        /*
        * SPEC-146, NAIS-85
        * Check to make sure a W2 is not a duplicate
        * Nathan 04/26/2013
        */
        if (W2DuplicateCheck.isExecuting == false) {
            Set <string> householdIdSet = new Set <string>();

            if (Trigger.isInsert || Trigger.isUpdate) {
                for (Family_Document__c fd : Trigger.New) {
                    // only run the duplicate check if this is a w2, the HH is not blank, the doc has been processed, and some of the W2 fields have been filled out
                    if (fd.Document_Type__c != null &&
                            fd.Document_Type__c.toLowerCase().contains('w2') &&
                            fd.Household__c != null &&
                            fd.Document_Status__c == 'Processed' &&
                            (fd.W2_Wages__c != null ||
                                    fd.W2_Employer_ID__c != null)) {
                        householdIdSet.add(fd.Household__c);
                    }
                }
            }
            if (householdIdSet.size() > 0) {
                //W2DuplicateCheck.DuplicateCheck(householdIdSet);
                if (!System.isFuture()) { // NAIS-1971 the asynchronous SpringCM method may update Fam Docs, in which case we can't fire off another future method here
                    W2DuplicateCheck.DuplicateCheckFuture(householdIdSet);
                }
            }
        }

        // SFP-640 after delete
        if (Trigger.isDelete)
        {
            for (Family_Document__c oldFamilyDoc : Trigger.old) {
                Set<String> documentTypesToProcess =
                    new Set<String>(VerificationAction.DOCUMENT_TYPES_TO_PROCESS);
                if (oldFamilyDoc.Document_Status__c == 'Processed' &&
                        documentTypesToProcess.contains(oldFamilyDoc.Document_Type__c)) {
                    householdIdsWithDeletedDocs.add(oldFamilyDoc.HouseHold__c);
                }
            }
            if (!householdIdsWithDeletedDocs.isEmpty()) {
                VerificationAction.setVerficationStatusValues(null, householdIdsWithDeletedDocs);
            }
        }
        // END SFP-640

    }
}