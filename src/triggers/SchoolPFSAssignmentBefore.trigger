trigger SchoolPFSAssignmentBefore on School_PFS_Assignment__c (before update, before insert, before delete) 
{

    if (Trigger.isUpdate) 
    {
        System.debug(LoggingLevel.WARN,' SPA before update');
    }

    // ability to deactivate trigger from hierarchical custom setting
    public static Boolean doTrigger = true;
    try 
    {
        TriggerDeactiviation__c trigDeactivate = TriggerDeactiviation__c.getInstance();
        if (trigDeactivate != null && trigDeactivate.Disable_Triggers__c)
        {
            doTrigger = false;
            System.debug('TRIGGER DEACTIVATED via custom settings');
        }
    } catch (Exception e) 
    {
        System.debug('Error getting custom setting: ' + e);
    }

    if (doTrigger)
    {
       /*
        * SPEC-099
        * Once all of the required documents have been provided and verified, the (document) stage of the folder should change
        *
        * Nathan, Exponent Partners, 2013
        */
        
        /* [SL] NAIS-282: refactored status calculation code, this trigger logic no longer needed     
            [CH] NAIS-2442 REMOVED OLD CODE
        */   
        SchoolPFSAction.PreviousYearOptionalInfo PreviousYearOptionalInfoHandler = new SchoolPFSAction.PreviousYearOptionalInfo();
        
        // NAIS-2114 [DP] 12.18.2014 auto-set the pfs doc status if it is overridden
        if (Trigger.isInsert || Trigger.isUpdate)
        {
            for (School_PFS_Assignment__c newSPA : trigger.new) 
            {
                if(newSPA.Academic_Year_Picklist__c == null && String.isNotBlank(newSPA.PFS_Academic_Year__c))
                {
                    newSPA.Academic_Year_Picklist__c = newSPA.PFS_Academic_Year__c;
                }
                
                if(newSPA.School__c == null)
                {
                    newSPA.School__c.addError('The School lookup field is required.');
                    continue;  
                }

                if (newSPA.PFS_Document_Status_Override__c && newSPA.PFS_Document_Status__c != SchoolSetSPFSDocumentStatus.requiredDocsCompleteStatus)
                {
                    newSPA.PFS_Document_Status__c = SchoolSetSPFSDocumentStatus.requiredDocsCompleteStatus;
                }

                // NAIS-2252 [DP] 02.13.2015 adding new fields for manual override
                if (newSPA.PFS_Document_Status_Override__c && newSPA.PFS_Current_Year_Tax_Document_Status__c != SchoolSetSPFSDocumentStatus.requiredDocsCompleteStatus)
                {
                    newSPA.PFS_Current_Year_Tax_Document_Status__c = SchoolSetSPFSDocumentStatus.requiredDocsCompleteStatus;
                }

                if (newSPA.PFS_Document_Status_Override__c && newSPA.PFS_Prior_Year_Tax_Document_Status__c != SchoolSetSPFSDocumentStatus.requiredDocsCompleteStatus)
                {
                    newSPA.PFS_Prior_Year_Tax_Document_Status__c = SchoolSetSPFSDocumentStatus.requiredDocsCompleteStatus;
                }
                
                if(Trigger.isInsert)
                {
                    PreviousYearOptionalInfoHandler.add(newSPA.School__c, newSPA.Academic_Year_Picklist__c, newSPA.Academic_Year_Picklist__c, newSPA.Applicant__c);
                } else
                {
                    //SFP-352:Clear the Parent_B_Filing_Status__c if Student_Has_Parent_B__c changes from Yes to No
                    if((Trigger.isBefore && newSPA.Parent_B__c==null && newSPA.Parent_B__c!=(Trigger.oldmap.get(newSPA.Id)).Parent_B__c)
                    ||(Trigger.isBefore && newSPA.Parent_B_Filing_Status__c!=null && newSPA.Filing_Status__c!='Single' && newSPA.Filing_Status__c!='Head of Household'))
                    {
                        newSPA.Parent_B_Filing_Status__c=null;
                    }
                }
            }
        }
        
        /*
        * NAIS-882 - The revision fields should be kept in synch with the SSS/Orig fields both upon intial submission of the PFS 
        * as well as subsequent updates. Each field will need to be checked to see if it differs from the Orig counterpart, and 
        * if it does then leave it as is. If it is the same (meaning no revision has been made) then update it along with the Orig field.
        * 
        * SL 8/4/2013
        */
        if (Trigger.isInsert)
        {
            for (School_PFS_Assignment__c newSPA : trigger.new)
            {
                 SchoolSSSToRevisionSyncAction.syncOrigToRevisionFields(null, newSPA);
            }
        }
        else if (Trigger.isUpdate) {
            // NAIS-2114 [DP] 12.18.2014 re-calc the pfs doc status if it is newly un-overridden
            List<School_PFS_Assignment__c> spasForDocStatusRecalc = new List<School_PFS_Assignment__c>();
            
            for (School_PFS_Assignment__c newSPA : trigger.new) {
                SchoolSSSToRevisionSyncAction.syncOrigToRevisionFields(Trigger.oldMap.get(newSPA.Id), newSPA);
                
                /* [CH] NAIS-2442 Moved to eliminate a For loop */  
                School_PFS_Assignment__c oldSPA = trigger.oldMap.get(newSPA.ID);
                if (newSPA.PFS_Document_Status_Override__c == false && oldSPA.PFS_Document_Status_Override__c == true){
                    spasForDocStatusRecalc.add(newSPA);
                }
            }

            if (!spasForDocStatusRecalc.isEmpty()){
                SchoolSetSPFSDocumentStatus.updateDocumentStatusFromSPABefore(spasForDocStatusRecalc);
            }
        }
        
        
        /*
        * SPEC-017
        * Update the EFC Calc Status to "Recalculate" if any of the EFC input fields have changed 
        *
        * SL 5/6/2013
        */
        
        if (Trigger.isUpdate || Trigger.isInsert) {   
            if (EfcDataManager.isUpdatingRevisionEfcCalc != true) {
                for (School_PFS_Assignment__c schoolPfsAssignment : Trigger.new) {
                    if (schoolPfsAssignment.Revision_EFC_Calc_Status__c != EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE) {
                        School_PFS_Assignment__c oldSchoolPfsAssignment = null;
                        if (Trigger.isUpdate) {
                            oldSchoolPfsAssignment = Trigger.oldMap.get(schoolPfsAssignment.Id);
                        }        
                        if (EfcCalculatorAction.isEfcRecalcNeeded(schoolPfsAssignment, oldSchoolPfsAssignment)) { 
//                            System.debug(LoggingLevel.WARN, '** SPA Recalc needed ');

                            EFC_Settings__c efcSettings = EFCUtil.getEfcSettings();
                            String newPFSEFCSetting;
                            if (efcSettings != null){
                                newPFSEFCSetting = efcSettings.Initial_EFC_Calc_Method__c; 
                            }

                            // [DP] 08.24.2016 SFP-475 If we have "batch" set, this is an update, 
                            // and this is EITHER the first EFC Calc (i.e., the EFC Calc Status field is null) OR the PFS EFC Calc Status is set to Recalc Batch, then set this SPA's status to Recalc Batch
                            // we need to do this because the PFS EFC now does not run synchronously, and so not all of the fields necessary for the SPA EFC are set (e.g., Family Size)
                            if (newPFSEFCSetting == 'Batch' && Trigger.isUpdate && (schoolPfsAssignment.Revision_EFC_Calc_Status__c == null || schoolPfsAssignment.PFS_EFC_Calc_Status__c == EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE_BATCH)){
                                schoolPfsAssignment.Revision_EFC_Calc_Status__c = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE_BATCH;                                
                            } else {
                                schoolPfsAssignment.Revision_EFC_Calc_Status__c = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE;
                            }
                        }
                    }
                }
            }
        }
        
         /**
         *  NAIS-1570: Prevent duplicate SPAs
         *
         *  DP, Exponent Partners, 2014
         */
         if (Trigger.isInsert){
            
            /** NAIS-965: [JIMI] Populate the Application_Last_Modified_by_Family__c field on the school pfs where Family_May_Submit_Updates__c = Yes.  */  
            /* [CH] NAIS-2442 Moved to consolidate if statements */
            SchoolPFSAction action = new SchoolPFSAction(Trigger.NewMap);
            action.setLastModifiedByFamilyDate(Trigger.new);
            Set<Id> applicantIds = new Set<Id>();
            Set<Id> schoolIds = new Set<Id>();

            for (School_PFS_Assignment__c spa : Trigger.new){
                if (spa.Applicant__c != null){applicantIds.add(spa.Applicant__c);}
                if (spa.School__c != null){schoolIds.add(spa.School__c);}
            }

            // set of keys (appId + '-' + schoolId) for preexisting SPAs
            Set<String> preexistingKeySet = new Set<String>();
            for (School_PFS_Assignment__c spa : [Select Id, School__c, Applicant__c FROM School_PFS_Assignment__c 
                                                                WHERE Applicant__c in :applicantIds 
                                                                AND School__c in :schoolIds
                                                                AND Applicant__c != null
                                                                AND School__c != null]){
                String key = spa.Applicant__c + '-' + spa.School__c;
                preexistingKeySet.add(key);
            }

            for (School_PFS_Assignment__c spa : Trigger.new){
                String key = spa.Applicant__c + '-' + spa.School__c;
                
                if (preexistingKeySet.contains(key)){
                    spa.addError('Duplicate School PFS Assignment already exists.  Please contact your administrator.');
                } else {
                    preexistingKeySet.add(key);
                }
            }
            
            //SFP-20, [G.S]
            SchoolPFSAction.updateOptionalFieldsFromAnnualSettings(Trigger.new);
        }
         
         /**
         * SFP-423: Optional Field Data Roll Forward for Returning Students
         */
         if(PreviousYearOptionalInfoHandler.shouldCopyInfo()){
            SchoolPFSAction.copyOptionalInfoFromPreviousYear(PreviousYearOptionalInfoHandler, Trigger.new);
         }
        
        /**
         *  NAIS-503: Any changes made to the application data should be captured in an audit trail
         *
         *  SL, Exponent Partners, 2013
         */
         if (Trigger.isUpdate) {
            AuditLogAction.doAudit(Trigger.new, Trigger.oldMap);
         }       
    }
}