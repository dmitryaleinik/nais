trigger StudentFolderAfter on Student_Folder__c (before insert, before update, after insert, after update, before delete) {

    // ability to deactivate trigger from hierarchical custom setting
    public static Boolean doTrigger = true;
    try {
        TriggerDeactiviation__c trigDeactivate = TriggerDeactiviation__c.getInstance();
        if (trigDeactivate != null && trigDeactivate.Disable_Triggers__c){
            doTrigger = false;
            System.debug('TRIGGER DEACTIVATED via custom settings');
        }
    } catch (Exception e) {
        System.debug('Error getting custom setting: ' + e);
    }

    if(doTrigger){     
        
        if(trigger.isAfter)
        {
            // SPEC-051b School Portal - Budget Allocations
            // BR, Exponent Partners, 2013
            
            // NAIS-2360 [CH]
            Set<String> notAttendingStatuses = new Set<String>{
                'No Action',
                'Denied Admission',
                'Family Declined',
                'Family Withdrawn',
                'Re-enrollment Not Offered'
            };
            // NAIS-2360 [CH]
            Set<String> noDocumentsStatues = new Set<String>{
                'No Documents Received',
                'Required Documents Outstanding'
            };
            
            Set<Id> budgetGroupIds = new Set<Id>();
            Set<Id> sfIds = new Set<Id>();
            Set<Id> sfIdsForBudgetRecalc = new Set<Id>();
            map<Id, String> foldersWithChangedEthnicity = new map<Id, String>();            
            List<Student_Folder__c> foldersWithDayBoardingChange = new List<Student_Folder__c>();
            
            if (Trigger.isUpdate) {
                Set<Id> sfIdsForSPAPing = new Set<Id>{};
                for (Student_Folder__c sf : Trigger.new) {
                    // NAIS-2360 [CH] Updated to be able to re-use the old record
                    Student_Folder__c oldSf = Trigger.oldMap.get(sf.Id);
                    if (sf.Folder_Status__c != oldSf.Folder_Status__c) {
                        sfIds.add(sf.Id);
                    }
                    
                    // NAIS-2360 [CH] Collect the Ids of Folders that have had their Admission_Enrollment_Status__c changed
                    //  Note:  We need to re-fire for folders going into withdrawn states as well as the other direction
                    if(sf.Admission_Enrollment_Status__c != oldSf.Admission_Enrollment_Status__c){
                        sfIdsForSPAPing.add(sf.Id);
                    }
    
                    // NAIS-2036 [DP] 11.19.2014
                    // if (sf.Use_Budget_Groups__c != Trigger.oldMap.get(sf.Id).Use_Budget_Groups__c){
                        sfIdsForBudgetRecalc.add(sf.Id);
                    // }
    
    
                    // NAIS-2252 [DP] 02.05.2015 pushing non-null changes to Day/Boarding down to SPAs
                    if (sf.Day_Boarding__c != null && sf.Day_Boarding__c != '' && sf.Day_Boarding__c != Trigger.oldMap.get(sf.Id).Day_Boarding__c) {
                        foldersWithDayBoardingChange.add(sf);
                    }
                    
                    //SFP-87: Allow schools to mass edit ethnicity column
                    if(trigger.IsUpdate && (Trigger.oldMap.get(sf.Id)).Ethnicity__c!=sf.Ethnicity__c)
                    {
                        foldersWithChangedEthnicity.put(sf.Student__c, sf.Ethnicity__c);
                    }
                }
                
                if(foldersWithChangedEthnicity.size()>0)
                {
                    SchoolPFSAction.updateEthnicity(foldersWithChangedEthnicity);
                }
                
                // NAIS-2360 [CH] Get the list of SPA records that might be queued for email reminder, related to identified Student Folders
                if(sfIdsForSPAPing!=null && sfIdsForSPAPing.size()>0){
                    List<School_PFS_Assignment__c> spaRecordsToPing = [select Id from School_PFS_Assignment__c 
                                                                        where Student_Folder__c in :sfIdsForSPAPing 
                                                                        and (PFS_Current_Year_Tax_Document_Status__c in :noDocumentsStatues 
                                                                        or PFS_Prior_Year_Tax_Document_Status__c in :noDocumentsStatues)];                    
                    if(spaRecordsToPing != null){
                        update spaRecordsToPing;
                    }
                }
                
                if (!sfIds.isEmpty()){
                    for (Budget_Allocation__c ba : [Select Id, Budget_Group__c, Student_Folder__c from Budget_Allocation__c WHERE Student_Folder__c in :sfIds]){
                        budgetGroupIds.add(ba.Budget_Group__c);                                                    
                    }
                }
    
                // NAIS-2252 [DP] 02.05.2015 -- pushing Student_Folder.Day_Boarding__c down to SPA.Day_Boarding__c
                if (!foldersWithDayBoardingChange.isEmpty()) {
                    SchoolPFSAction.updateChildSPAsOnDayBoardingChange(foldersWithDayBoardingChange);
                }
            }

            if (!budgetGroupIds.isEmpty()){
                SchoolBudgetGroupAction.recalcBudgetBalance(budgetGroupIds, null);
            }
            
            // NAIS-2036 [DP] 11.19.2014
            if (!sfIdsForBudgetRecalc.isEmpty()){
                SchoolBudgetGroupAction.recalcStudentFolderGrantAwarded(sfIdsForBudgetRecalc);//If this logic changes, It may affect SchoolEditAllocationsController.saveWhenBudgetGroupIsNo
            }
        } else if(trigger.isBefore) {
            
            if (Trigger.isDelete) {
                
                // SFP-972 removing of Budget Allocation records that causes recalculating of Budget_Group__c.Total_Allocated__c field
                // on Student Folder deleting 
                List<Budget_Allocation__c> budgetAllocationsToRemove = [
                    SELECT Budget_Group__c 
                    FROM Budget_Allocation__c 
                    WHERE Student_Folder__c IN :Trigger.oldMap.keySet()
                      LIMIT 50000];

                if (budgetAllocationsToRemove.isEmpty()) {
                    return;
                }

                Set<Id> budgetGroupIdsForRecalculation = new Set<Id>();
                for (Budget_Allocation__c budgetAllocation : budgetAllocationsToRemove) {
                    budgetGroupIdsForRecalculation.add(budgetAllocation.Budget_Group__c);
                }

                if (!budgetGroupIdsForRecalculation.isEmpty()){
                    SchoolBudgetGroupAction.recalcBudgetBalance(
                        budgetGroupIdsForRecalculation, new Map<Id, Budget_Allocation__c>(budgetAllocationsToRemove).keySet());
                }
            }

            if (Trigger.isInsert || Trigger.isUpdate)
            {
                StudentFolderAction.PreviousYearFoldersWrapper returningFolders = new StudentFolderAction.PreviousYearFoldersWrapper();
                
                for (Student_Folder__c sf : Trigger.new)
                {
                    if (Trigger.isInsert && String.isBlank(sf.Ethnicity__c))
                    {
                        sf.Ethnicity__c = sf.Student_Ethnicity__c;
                    }

                    if(sf.New_Returning__c=='Returning' &&  sf.Student_ID__c==null 
                        && (trigger.isInsert || (trigger.isUpdate && trigger.oldMap.get(sf.Id).New_Returning__c!=sf.New_Returning__c)) )
                    {
                        returningFolders.put(sf.Student__c, sf.School__c, sf);
                    }
                }
                    
                //SFP-968: For a returning student the Student_Folder__c.Student_ID__c 
                //field should be copied from the previous year.
                if(!returningFolders.foldersByStudent.isEmpty())
                {
                    StudentFolderAction.CopyDataFromPreviousYear(returningFolders);
                }
            }
        }

        if (Trigger.IsAfter && Trigger.isUpdate) {
            // Pass values to Domain class
            StudentFolders folders = StudentFolders.newInstance(Trigger.new);
            folders.onAfterUpdate(Trigger.oldMap);
        }

    }
}