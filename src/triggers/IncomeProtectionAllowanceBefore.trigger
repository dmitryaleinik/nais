trigger IncomeProtectionAllowanceBefore on Income_Protection_Allowance__c (before insert, before update) {

	// [CH] NAIS-1885 Make the Allowance field the total of two components
	for(Income_Protection_Allowance__c ipa : trigger.new){
		if(ipa.Housing_Allowance__c != null && 
			ipa.Other_Allowance__c != null && 
			ipa.Allowance__c != ipa.Housing_Allowance__c + ipa.Other_Allowance__c)	
		{
			ipa.Allowance__c = ipa.Housing_Allowance__c + ipa.Other_Allowance__c;
		}
	}
}