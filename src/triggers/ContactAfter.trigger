trigger ContactAfter on Contact (after update, after insert, after delete) {

    /*
    * SPEC-026, NAIS-35
    *
    * The email address (preferred email) of Parent A needs to be copied onto:
    * School PFS Assignment
    * Family Document
    * Opportunity
    * Application Fee Waiver
    * School Document Assignment
    * Transaction Line Item
    *
    * Nathan, Exponent Partners, 2013
    */
    /*
    * SPEC-133, NAIS-36
    *
    * We need a trigger to set the username and user email address when the family user email address changes on the PFS
    *
    * Nathan, Exponent Partners, 2013
    */
    if (Trigger.isDelete) {
    	List<Id> accountIdsToDelete = new List<Id>();
    	
    	for (Contact c : Trigger.old) {
    		if(contactAction.shouldDeleteRelatedContactAccount(c)){
    			accountIdsToDelete.add(c.AccountId);
    		}
    	}
    	
    	if(!accountIdsToDelete.isEmpty()){
    		contactAction.deleteRelatedAccountForFamilyContact(accountIdsToDelete);
    	}
    	
    }else{    	
		if (Trigger.isUpdate) {        
	        // NAIS-35
	        // [CH] NAIS-2442 Refactored to use a map and consolidate For loops
	        // List <Contact> contactNewList = new List <Contact>();
	        Map<Id, Contact> contactNewMap = new Map<Id, Contact>();
	        set<Id> contactWithChangedNames = new set<Id>();
	        set<Id> contactWithChangedEthnicity = new set<Id>();
	                
	        for (Contact con : Trigger.New) {
	            if (con.Email != Trigger.oldMap.get(con.Id).Email) {
	                // contactNewList.add(con);
	                contactNewMap.put(con.Id, con);
	            }
	            if(con.RecordTypeId==RecordTypes.studentContactTypeId 
	            && (con.FirstName!=Trigger.oldMap.get(con.Id).FirstName || con.LastName!=Trigger.oldMap.get(con.Id).LastName))
	            {
	                contactWithChangedNames.add(con.Id);
	            }
	            if(con.Ethnicity__c!=Trigger.oldMap.get(con.Id).Ethnicity__c)
	            {
	                contactWithChangedEthnicity.add(con.Id);
	            }
	        }
	        
	        if(contactWithChangedNames.size()>0 || contactWithChangedEthnicity.size()>0)//SFP-404
	        {
	            ContactActionWithoutSharing.studentFieldChanged(contactWithChangedNames, contactWithChangedEthnicity);
	        }
	        
	        if (contactNewMap.values().size() > 0) {
	            // [CH] NAIS-2442 Updated to use consolidated static method
	            if (ContactAction.isCopyingParentA == false) {
	                ContactAction.CopyParentA(contactNewMap.values());
	            }           
	            
	            /* OLD CODE
	            if (CopyParentA.isExecuting == false) {
	                CopyParentA cpa = new CopyParentA();
	                cpa.contactNewList = contactNewList;
	                cpa.Copy();
	            }
	            */
	        }
	        
	        // NAIS-36
	        // I think you should only call the CopyEmailToUserName method if the email address has changed.
	        // AND you should only send Contacts who have Users associated with them
	        // Set <String> contactIdSet = new Set <String>();
	        Map <Id, Contact> newContactMap = new Map <Id, Contact>();
	
	        /* [CH] NAIS-2442 This is now being done in 1 For loop above
	        for (Contact con : Trigger.New) {
	            if (con.Email != Trigger.oldMap.get(con.Id).Email) {
	                contactIdSet.add(con.Id);
	            }
	        }
	        */
	        
	        Set<Id> contactIdSetOfUsers = new Set<Id>();
	        // for (User u : [Select Id, ContactId from User where ContactId in :contactIdSet]){
	        for (User u : [Select Id, ContactId from User where ContactId in :contactNewMap.keySet()]){
	            contactIdSetOfUsers.add(u.ContactId);
	        }
	
	        // only put contacts in the map if they have associated users, otherwise we don't care
	        if (!contactIdSetOfUsers.isEmpty()){
	            for (Contact con : Trigger.New) {
	                if (contactIdSetOfUsers.contains(con.Id)) {
	                    newContactMap.put(con.Id, con);
	                }
	            }
	        }
	
	        // [CH] NAIS-2442 Updated to use map created above
	        if (newContactMap.keySet().size() > 0) {
	            if (ContactAction.isCopyingEmailToUserName == false) {
	                ContactAction.newContactMap = newContactMap;
	                ContactAction.validateEmailToUsername();
	                
	                if (ContactAction.uniqueEmailContactIdSet.size() > 0) {
	                    ContactAction.CopyFuture(ContactAction.uniqueEmailContactIdSet, UserInfo.getSessionId());
	                }
	            }
	            
	            /* OLD CODE
	            if (CopyEmailToUserName.isExecuting == false) {
	                CopyEmailToUserName.newContactMap = newContactMap;
	                CopyEmailToUserName.validateEmailToUsername();
	                
	                if (CopyEmailToUserName.uniqueEmailContactIdSet.size() > 0) {
	                    CopyEmailToUserName.CopyFuture(CopyEmailToUserName.uniqueEmailContactIdSet, UserInfo.getSessionId());
	                    //CopyEmailToUserName.Copy(CopyEmailToUserName.uniqueEmailContactIdSet);
	                }
	            }
	            */
	        }
	       
	    }
	     //SFP#19, [G.S]
	     ContactAction.updateSSSMainContact(Trigger.newMap, Trigger.oldMap);
    }
    /*
     * NAIS-339 Multiple User Security - Account Change
     *  Share PFS's and Student Folders with staff changing to a new school
     *
     * WH, Exponent Partners, 2013
     */
     /*
    if (Trigger.isUpdate) {
        
        // [CH] NAIS-1766 Refactoring to look up the list of related users and use the 
        // Set<Id> accountIds = new Set<Id>();
        // List<Id> contactIds = new List<Id>();
        Map<Id, Contact> newContactsMap = new Map<Id, Contact>();
        Map<Id, Contact> oldContactsMap = new Map<Id, Contact>(); 
        
        // Need to narrow the list down to just School staff who had their Account changed
        for (Contact c : Trigger.new) {
            Contact oldContact = Trigger.oldMap.get(c.Id);
            if (c.RecordTypeId == RecordTypes.schoolStaffContactTypeId && c.AccountId != null && c.AccountId != oldContact.AccountId) {
                newContactsMap.put(c.Id, c);
                oldContactsMap.put(oldContact.Id, oldContact);
            }
        }
        
        // [CH] NAIS-1766 Remove group memberships first to avoid accidentally 
        //      removing newly inserted memberships
        if(oldContactsMap.size() > 0){
            ContactAction.removeUsersFromGroups(oldContactsMap);
        }

        
        // [CH] NAIS-1766 If there are new users, or users that have had their account changed
        //      then add them to the appropriate account-specific sharing groups
        if(newContactsMap.size() > 0){
            ContactAction.addUsersToGroups(newContactsMap);
        }

        if (!accountIds.isEmpty() && !contactIds.isEmpty()) {
            SchoolStaffShareAction.shareRecordsToUpdatedSchoolUsers(accountIds, contactIds);
        }
    }
    */
    
}