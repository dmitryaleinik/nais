/**
* @AnnualSettingAfter.trigger
* @date 3/21/2014
* @author  CH for Exponent Partners
* @description Trigger for operations on Annual Settings after save
*/
trigger AnnualSettingAfter on Annual_Setting__c (before insert, before update, after insert, after update) {

    if (trigger.isInsert && Trigger.isBefore) {
        
        AnnualSettings.newInstance(Trigger.new).handleBeforeInsert();
        
    } else {
        
        // Update Account fields *** This method runs a DML statement
        AnnualSettingHelper.updateAccountWaiverAmounts(trigger.new, trigger.isInsert ? null : trigger.oldMap, trigger.isInsert);
    
        if(trigger.isUpdate && Trigger.isAfter){//NAIS-2305
            // SFP-1209: Updating SPA reminder dates is now only handled after update since SFP-596 prevents families from
            // applying to a school until after the annual settings have been inserted. If families can't apply, there aren't any PFS Assignments to update.
            AnnualSettingHelper.updateSPAReminderDates(Trigger.new, Trigger.oldMap);
    
            AnnualSettingHelper.updateAccountPFSCounters(trigger.new, trigger.old);
            //SFP-20, [G.S]
            AnnualSettingHelper.updateOptionFieldsonSPFSA(trigger.newMap, trigger.oldMap);
            Boolean premiumSubscription;
            Boolean priorYearCase;
            Subscription__c subscription = GlobalVariables.getMostRecentSubscription();   
                if ( subscription.Subscription_Type__c == 'Premium' && subscription.Status__c == 'Current' ) {
                    premiumSubscription = true;
                } else {
                    premiumSubscription = false;
                }
            //SFP-649
            Set<Id> annualSettingIdsToUpdateSPAs = new Set<Id>();
            for(Annual_Setting__c annualSetting : Trigger.new){
                Annual_Setting__c oldSetting = Trigger.oldMap.get(annualSetting.Id);
    
                String newCopyVerifcationValue = annualSetting.Copy_Verification_Values_to_Revisions__c;
                String verifcationValueToCopy = annualSetting.Verification_Values_To_Copy__c;
                String oldCopyVerifcationValue = oldSetting.Copy_Verification_Values_to_Revisions__c;
                // Only schedule batch for annual settings that are newly updated to copy verification values.
                if(newCopyVerifcationValue == 'Yes' && !newCopyVerifcationValue.equalsIgnoreCase(oldCopyVerifcationValue) && premiumSubscription == false){
                    annualSettingIdsToUpdateSPAs.add(annualSetting.Id);
                    priorYearCase = false;
                }
                
                //SFP-1818 : Automatically Copy PY/CY Verification Values based on revision fields .
                if(newCopyVerifcationValue == 'Yes' &&  premiumSubscription == true && verifcationValueToCopy == 'Prior Year') {
                    priorYearCase = true;
                } else if(newCopyVerifcationValue == 'Yes' &&  premiumSubscription == true && verifcationValueToCopy == 'Current Year') {
                    priorYearCase = false;
                }
            }
    
            if(!annualSettingIdsToUpdateSPAs.isEmpty()){
                AutoSPAValueAssignBatch batch = new AutoSPAValueAssignBatch(annualSettingIdsToUpdateSPAs, priorYearCase);
                Database.executeBatch(batch);
            }
        } else if (Trigger.isAfter && trigger.isInsert) {
            // We need to call onAfterInsert() instead of handleAfterInsert() because handleAfterInsert() enforces object permissions.
            // For the SAO/Ravenna Family Integrations, we are expecting families to be able to insert annual settings for a school if they do not already exist.            
            AnnualSettings.newInstance( Trigger.newMap.values()).onAfterInsert();
        }
    }
}