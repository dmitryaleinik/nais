/*
 * Spec-134 School Portal - Multiple User Security; Req# R-493
 *  Trigger on User to share all PFS and Student Folder records relevant to the school with the School users 
 *  for each school they are affiliated with, or users of Access Orgs the parent has opted-in with school pfs assignments
 *
 * WH, Exponent Partners, 2013
 */
trigger UserAfter on User (after insert, after update) {

    // ability to deactivate trigger from hierarchical custom setting
    public static Boolean doTrigger = true;
    try {
        TriggerDeactiviation__c trigDeactivate = TriggerDeactiviation__c.getInstance();
        if (trigDeactivate != null && trigDeactivate.Disable_Triggers__c){
            doTrigger = false;
            System.debug('TRIGGER DEACTIVATED via custom settings');
        }
    } catch (Exception e) {
        System.debug('Error getting custom setting: ' + e);
    }

    if(doTrigger){
    	   
        List<Id> newUserIds = new List<Id>();
        Set<Id> contactIds = new Set<Id>();
        List<Id> usersToGetSharing = new List<Id>();
        List<User> usersToRemoveSharing = new List<User>();
        
        Map<String, String> newEmailToOldemailMap = new Map<String, String>();
        
        // [DP] NAIS-1786 used to create User in SpringCM via asynchronous method
        List<Id> userIdListForSpringCMXML = new List<Id>();

        // [YJ] [SFP-366]
        //List<CollaborationGroupMember> chatterMembersToInsert = new List<CollaborationGroupMember>();
        list<id> usersToAutoAssignChatterGroups = new list<id>();

        if (Trigger.isInsert) {
            for (User u : Trigger.new) {
                if (u.IsActive && u.ContactId != null) {
                    // new active school or access org user
                    if (GlobalVariables.isSchoolPortalProfile(u.ProfileId) || GlobalVariables.isAccessOrgPortalProfile(u.ProfileId)) {
                        newUserIds.add(u.Id);
                        contactIds.add(u.ContactId);
                        usersToGetSharing.add(u.Id);
                    }

                    // [SFP-366] YJ add new users to chatter groups specified by custom settings
                     if (GlobalVariables.isSchoolPortalProfile(u.ProfileId)) 
                        usersToAutoAssignChatterGroups.add(u.id);
                     
                }

                // [DP] NAIS-1786
                if (u.IsActive && u.SpringCMEos__SpringCM_User__c && SchoolUserAdminProfileController.fromFutureMethod!=true){
                    userIdListForSpringCMXML.add(u.Id);
                }
            }
        } else if (Trigger.isUpdate) {
            for (User u : Trigger.new) {
            	// [CH] NAIS-1766 Refactoring to consolidate if blocks 
            	if ((GlobalVariables.isSchoolPortalProfile(u.ProfileId) ||
            			GlobalVariables.isSchoolPortalAdminProfile(u) ||
            			GlobalVariables.isLimitedSchoolView(u) || 
            			GlobalVariables.isAccessOrgPortalProfile(u.ProfileId)) && 
            			u.ContactId != null) {
            		// If a user is activated
	                if (u.IsActive && !Trigger.oldMap.get(u.Id).IsActive) {
	                    // school or access org user activated
                        newUserIds.add(u.Id);
                        contactIds.add(u.ContactId);
                        usersToGetSharing.add(u.Id);
	                }
	                
	                // If a user is deactivated
	                if (!u.IsActive && Trigger.oldMap.get(u.Id).IsActive) {
	                	// Add the user to the list of users to have groupmembers removed
	                	usersToRemoveSharing.add(u);
	                }
	                
	                // If a user has their account id value changed
	                if(Trigger.oldMap.get(u.Id).AccountId != u.AccountId){
                       	usersToGetSharing.add(u.Id);
                       	usersToRemoveSharing.add(Trigger.oldMap.get(u.Id));
                    }
            	}
                
                // NAIS-1944 [DP] always use email, not username, and only for non-portal users (email is updated on Contact for portal users)
                // see CopyEmailToUserName.cls line 187
                // [CH] NAIS-1766 Consolidating from below
                if (u.ContactId == null && u.Email != Trigger.oldMap.get(u.Id).Email) {
                    // school or access org user activated
                    newEmailToOldemailMap.put(u.Email, Trigger.oldMap.get(u.Id).Email);
                }
                // [CH] NAIS-1766 Consolidating from below
                //if (u.Username != Trigger.oldMap.get(u.Id).Username) {
                //    // school or access org user activated
                //    newEmailToOldemailMap.put(u.Username, Trigger.oldMap.get(u.Id).Username);
                //}


                // [DP] NAIS-1786 -- send info if User is active AND newly marked as SpringCM or newly marked as SpringCMPortal
                User oldUser = Trigger.oldMap.get(u.Id);
                if (u.IsActive && (u.SpringCMEos__SpringCM_User__c != oldUser.SpringCMEos__SpringCM_User__c || u.SpringCM_PortalUser__c != oldUser.SpringCM_PortalUser__c)){
                    userIdListForSpringCMXML.add(u.Id);
                }
            }
        }
        
        // [SFP-366] YJ
        if(!usersToAutoAssignChatterGroups.isEmpty()){
            System.enqueueJob(new AutoAssignChatterGroupQueueable(usersToAutoAssignChatterGroups));
        }
        
        // [CH] NAIS-1766 Remove group memberships first to avoid accidentally 
        //  removing newly inserted memberships
        if(usersToRemoveSharing.size() > 0){
        	UserAction.removeUsersFromGroups(usersToRemoveSharing);
        }
        
        // [CH] NAIS-1766 If there are new users, or users that have had their account changed
        // 	then add them to the appropriate account-specific sharing groups
        if(usersToGetSharing.size() > 0){
        	UserAction.addUsersToGroups(usersToGetSharing);
        }

        /* [CH] NAIS-1766 This should not be needed any more now that Users are added as members to Public Groups synchronously
        if (!newUserIds.isEmpty() && !SchoolStaffShareAction.suppressUserTrigger) {
            SchoolStaffShareAction.shareRecordsToNewSchoolUsers(newUserIds, contactIds);
        }
        */
        
    	/* Consolidating with isUpdate block above [CH] NAIS-1766
    	Map<String, String> newEmailToOldemailMap = new Map<String, String>();
    	
    	if (Trigger.isUpdate) {
            
            for (User u : Trigger.new) {
                if (u.Username != Trigger.oldMap.get(u.Id).Username) {
                    // school or access org user activated
                    newEmailToOldemailMap.put(u.Username, Trigger.oldMap.get(u.Id).Username);
                }
            }
        }
        */
        
        // send new emails to SpringCM
        if (newEmailToOldemailMap != null && !newEmailToOldemailMap.isEmpty()){
        	if (System.isFuture()){
        		// if this is happening from a future call, we are updating the Username from the CopyEmailToUserName class, and
        		// that class will call the sendEmailUpdateXMLToSpringSYNCHRONOUS directly 
        		//UserAction.sendEmailUpdateXMLToSpringSYNCHRONOUS(newEmailToOldemailMap, UserInfo.getSessionId());
        	} else {
        		// if this is not happening from a future call we are probably dealing with an update directly to the User record, and should call this
        		// in a future method
        		UserAction.sendEmailUpdateXMLToSpring(newEmailToOldemailMap, UserInfo.getSessionId());
        	}
        }

        // [DP] NAIS-1786
        if (!userIdListForSpringCMXML.isEmpty()){
            SpringCMAction.sendNewUserInfoToSpringCMAsynchronous(userIdListForSpringCMXML);
        }
        SchoolUserAdminProfileController.fromFutureMethod=false;
    }
}