/** 
* @description  This trigger and its associated field are used for 
* labeling the actual creation date of migrated legacy records 
*/

trigger CreatedDate on Note__c (after insert) {
    List<Note__c> schoolComs = [SELECT ID, CreatedDate, Created_Date__c FROM Note__c WHERE ID =: trigger.new];
    for(Note__c note: schoolComs){
        if(note.Created_Date__c == null){
        	note.Created_Date__c = note.createdDate;
        }
    }
    update schoolComs;
}