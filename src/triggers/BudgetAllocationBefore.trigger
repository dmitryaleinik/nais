/**
* @BudgetAllocationBefore
* @date 05.06.2015
* @author  DP for Exponent Partners
* @description Trigger (before insert) that prevents Budget Allocations from being inserted
* if they are not attached to a Budget Group.  No error is thrown, they are just not inserted.
*/
trigger BudgetAllocationBefore on Budget_Allocation__c (before insert) {
	for (Integer i = 0; i < Trigger.New.size(); i++){
		Budget_Allocation__c ba = Trigger.New[i];
		if (ba.Budget_Group__c == null){
			//System.assertEquals(1,2, 'drew testing no budget group');
			Trigger.New.remove(i);
		}
	}
}