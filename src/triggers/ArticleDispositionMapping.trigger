trigger ArticleDispositionMapping on Article_Disposition_Mapping__c (before insert, before update)
{
	fflib_SObjectDomain.triggerHandler(ArticleDispositionMappings.class);
}