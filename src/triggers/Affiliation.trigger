/*
 * Spec-134 School Portal - Multiple User Security; Req# R-493
 *  Trigger on Affiliation to share all PFS and Student Folder records relevant to the school with the School users
 *  for each school they are affiliated with.
 *
 * WH, Exponent Partners, 2013
 */
trigger Affiliation on Affiliation__c (after insert, after update, before delete) {

    List<Id> newSchoolAffiliationsIds = new List<Id>();
    List<Id> removedAffiliationsIds = new List<Id>();
    Map<Id, Id> removedAffiliationsMap = new Map<Id, Id>();

    if (Trigger.isInsert) {
        // [CH] NAIS-1766 Updated to put record type filters in queries
        for (Affiliation__c aff : Trigger.new
                /*[select Contact__c, Organization__c, Contact__r.RecordTypeId, Organization__r.RecordTypeId, Status__c, Type__c
                    from Affiliation__c
                    where Id in :Trigger.new
                    and Contact__r.RecordTypeId = :RecordTypes.schoolStaffContactTypeId
                    and Organization__r.RecordTypeId = :RecordTypes.schoolAccountTypeId]*/
        )
        {

            if (aff.Status__c == 'Current' && aff.Type__c != 'Relationship') {
                //if (aff.Type__c == 'Additional School User' &&
                //  aff.Contact__r.RecordTypeId == RecordTypes.schoolStaffContactTypeId &&
                //  aff.Organization__r.RecordTypeId == RecordTypes.schoolAccountTypeId) {
                newSchoolAffiliationsIds.add(aff.Id);
                //}
            }
        }
    } else if (Trigger.isUpdate) {
        // [CH] NAIS-1766 Updated to put record type filters in queries
        for (Affiliation__c aff : Trigger.new)
                /*[select Contact__c, Organization__c, Contact__r.RecordTypeId, Organization__r.RecordTypeId, Status__c, Type__c
                    from Affiliation__c
                    where Id in :Trigger.new
                    and Contact__r.RecordTypeId = :RecordTypes.schoolStaffContactTypeId
                    and Organization__r.RecordTypeId = :RecordTypes.schoolAccountTypeId])*/
        {
            if (aff.Status__c == 'Current' && Trigger.oldMap.get(aff.Id).Status__c != 'Current' && aff.Type__c != 'Relationship') {
                // if (aff.Type__c == 'Additional School User' &&
                //  aff.Contact__r.RecordTypeId == RecordTypes.schoolStaffContactTypeId &&
                //  aff.Organization__r.RecordTypeId == RecordTypes.schoolAccountTypeId) {
                    newSchoolAffiliationsIds.add(aff.Id);
                //}
            }
            else if (aff.Status__c == 'Former' && Trigger.oldMap.get(aff.Id).Status__c != 'Former') {
                removedAffiliationsIds.add(aff.Id);
            }
        }
    }
    // [CH] NAIS-1766 Adding support for deletion of Affiliations
    else if (Trigger.isDelete) {
        for (Affiliation__c aff : Trigger.old){
            removedAffiliationsMap.put(aff.Contact__c, aff.Organization__c);
        }
    }


    if (!newSchoolAffiliationsIds.isEmpty()) {
        SchoolStaffShareAction.addAffiliatedUsersToGroups(newSchoolAffiliationsIds);
        // SchoolStaffShareAction.shareRecordsToNewAffiliatedSchoolUsers(newSchoolAffiliations);
    }

    if (!removedAffiliationsIds.isEmpty()) {
        SchoolStaffShareAction.removeAffiliatedUsersFromGroups(removedAffiliationsIds);
    }

    if (!removedAffiliationsMap.isEmpty()) {
        SchoolStaffShareAction.removeDeletedAffiliationsFromGroups(removedAffiliationsMap);
    }
}