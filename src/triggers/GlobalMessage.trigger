trigger GlobalMessage on Global_Message__c (before insert, before update)
{
    fflib_SObjectDomain.triggerHandler(GlobalMessages.class);
}