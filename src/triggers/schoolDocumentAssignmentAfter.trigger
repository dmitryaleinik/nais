trigger schoolDocumentAssignmentAfter on School_Document_Assignment__c (after insert, after update, after delete)
{

    // ability to deactivate trigger from hierarchical custom setting
    public static Boolean doTrigger = true;

    try {
        TriggerDeactiviation__c trigDeactivate = TriggerDeactiviation__c.getInstance();
        if (trigDeactivate != null && trigDeactivate.Disable_Triggers__c){
            doTrigger = false;
            System.debug('TRIGGER DEACTIVATED via custom settings');
        }
    } catch (Exception e) {
        System.debug('Error getting custom setting: ' + e);
    }

    // Temporary Trigger for updating SPA Current/Prior Tax Document Years
    System.debug('### UserInfo.getName(): ' + UserInfo.getName());
    if (trigger.isUpdate && trigger.isAfter && !doTrigger && UserInfo.getName() == 'Import Processor')
    {
        System.debug('### Processing Import Processor Updates');
        List<School_Document_Assignment__c> sdaToUpdate = new List<School_Document_Assignment__c>();
        Set<Id> spfIds = new Set<Id>();
        for (School_Document_Assignment__c sda : trigger.new)
        {
            if (sda.Force_SPA_Status_Update__c && !trigger.oldMap.get(sda.Id).Force_SPA_Status_Update__c) {
                sdaToUpdate.add(sda);
                spfIds.add(sda.School_PFS_Assignment__c);
            }
        }

        if (!spfIds.isEmpty())
        {
            System.debug('### Processing SPFIds: ' + spfIds);
            SchoolSetSPFSDocumentStatus.updateDocumentStatus(spfIds, null);
        }

        if (!sdaToUpdate.isEmpty())
        {
            List<School_Document_Assignment__c> sdaToReset = new List<School_Document_Assignment__c>();
            for (School_Document_Assignment__c sda : sdaToUpdate)
            {
                sdaToReset.add(new School_Document_Assignment__c(Id = sda.Id, Force_SPA_Status_Update__c = false));
            }
            System.debug('### Resetting flag: ' + sdaToReset);
            update sdaToReset;
        }
    }

    if (doTrigger)
    {
        Set<Id> spaIdsToUpdate = new Set<Id>();
        if (Trigger.isInsert) {
            for (School_Document_Assignment__c sda : Trigger.new) {
                if (sda.School_PFS_Assignment__c != null) {
                    spaIdsToUpdate.add(sda.School_PFS_Assignment__c);
                }
            }
        }
        else if (Trigger.isUpdate) {
            for (School_Document_Assignment__c newSDA : Trigger.new) {
                School_Document_Assignment__c oldSDA = Trigger.oldMap.get(newSDA.Id);

                // if the school pfs assignment changed, then need to recalc both the old and new SPA
                if (newSDA.School_PFS_Assignment__c != oldSDA.School_PFS_Assignment__c) {
                    if (oldSDA.School_PFS_Assignment__c != null) {
                        spaIdsToUpdate.add(oldSDA.School_PFS_Assignment__c);
                    }
                    if (newSDA.School_PFS_Assignment__c != null) {
                        spaIdsToUpdate.add(newSDA.School_PFS_Assignment__c);
                    }
                }

                // otherwise, only need to recalc overall status if the Document changes, the Waiver changes, or the SDA is marked/unmarked for deletion
                else if ((newSDA.Document__c != oldSDA.Document__c)
                            || (newSDA.Requirement_Waiver_Status__c != oldSDA.Requirement_Waiver_Status__c)
                            || (newSDA.Deleted__c != oldSDA.Deleted__c)) {
                    if (newSDA.School_PFS_Assignment__c != null) {
                        spaIdsToUpdate.add(newSDA.School_PFS_Assignment__c);
                    }
                }
            }
        }
        if (Trigger.isDelete) {
            for (School_Document_Assignment__c oldSDA : Trigger.old) {
                if (oldSDA.School_PFS_Assignment__c != null) {
                    spaIdsToUpdate.add(oldSDA.School_PFS_Assignment__c);
                }
            }
        }
        if (!spaIdsToUpdate.isEmpty()) {
            SchoolSetSPFSDocumentStatus.updateDocumentStatus(spaIdsToUpdate);
        }
        // [SL] NAIS-282 END code refactor
    }

    /**
     * [SL] NAIS-1652: recalc EFC if family docs are added/removed with depreciation fields set
     */
     if (Trigger.isInsert) {
        Set<Id> sdaIdsToRecalcEFC = new Set<Id>();
        for (School_Document_Assignment__c sda : Trigger.new) {
            if (sda.Document__c != null) {
                sdaIdsToRecalcEFC.add(sda.Id);
            }
        }

        /**
         *  SFP-876 [10.25.16] Order of operations for creating SDAs for FDs inserted by SpringCM processor means that we have
         *          to re-run SPA calculation for SPAs in question after SDA is inserted for newly created Family Docs [jB]
         */
        Set<Id> spaIdsToRerunSPASum = new Set<Id>();
        for(School_Document_Assignment__c sda : Trigger.new)
        {
            if(sda.School_PFS_Assignment__c != null && VerificationAction.isDocTypeVerifiable(sda.Document_Type__c))
            {
                spaIdsToRerunSPASum.add(sda.School_PFS_Assignment__c);
            }
        }

         // Flag SPAs for processing and queue job if necessary.
        DocumentVerificationService.Instance.queueVerificationUpdates(spaIdsToRerunSPASum);
        /** END SFP-876 **/

        if (!sdaIdsToRecalcEFC.isEmpty()) {
            FamilyDocumentAction.recalculateEFCsFromSDAs(sdaIdsToRecalcEFC, null);
        }

         // Queue job to send document verification requests to third party for records that were flagged in before trigger.
         SchoolDocumentAssignments documentAssignments = SchoolDocumentAssignments.newInstance(Trigger.new);
         documentAssignments.onAfterInsert();
     }

     if (Trigger.isUpdate) {
        Set<Id> sdaIdsToRecalcEFC = new Set<Id>();
        Set<Id> spaIdsToRerunSPASum = new Set<Id>();
        Map<Id, Id> spasWithDeletedDocuments = new Map<Id, Id>();
        
        for (School_Document_Assignment__c newSDA : Trigger.new) {
            
            School_Document_Assignment__c oldSDA = Trigger.oldMap.get(newSDA.Id);
            
            if (newSDA.Document__c != oldSDA.Document__c) {
                
                sdaIdsToRecalcEFC.add(newSDA.Id);
                    
                if(oldSDA.Document__c != null && newSDA.Document__c == null) {
                    //SFP-118 If the FD was deleted. Then, at this point the newSDA.Document__c is null. So, we use the oldSDA to know what document was recently removed.
                    spasWithDeletedDocuments.put(newSDA.Id, oldSDA.Document__c);
                }
            }

            if(newSDA.School_PFS_Assignment__c != null && VerificationAction.isDocTypeVerifiable(newSDA.Document_Type__c))
            {
                spaIdsToRerunSPASum.add(newSDA.School_PFS_Assignment__c);
            }
        }
        if (!sdaIdsToRecalcEFC.isEmpty()) {
            FamilyDocumentAction.recalculateEFCsFromSDAs(sdaIdsToRecalcEFC, spasWithDeletedDocuments);
        }

         // Flag SPAs for processing and queue job if necessary.
         DocumentVerificationService.Instance.queueVerificationUpdates(spaIdsToRerunSPASum);

         // Queue job to send document verification requests to third party for records that were updated with the
         // verification request flag.
         SchoolDocumentAssignments documentAssignments = SchoolDocumentAssignments.newInstance(Trigger.new);
         documentAssignments.onAfterUpdate(Trigger.oldMap);
     }
}