/**
* @BudgetAllocationAfter
* @date 05.06.2015
* @author  DP for Exponent Partners
* @description Trigger (after delete, after undelete, after insert, after update) that
*       1. recalculates the budget balance for a budget group. All records for inserts, deletes, undelets are included. 
*           On Update, only Budget Allocation records with changes to Amount Allocated or Student Folder are included.
*       2. recalculates the grant awarded amount on the student folder. All records for inserts, deletes, undelets are included. 
*           On Update, only Budget Allocation records with changes to Amount Allocated or Student Folder are included.
*/


trigger BudgetAllocationAfter on Budget_Allocation__c (after delete, after undelete, after insert, after update) {
    // SPEC-051b School Portal - Budget Allocations
    // BR, Exponent Partners, 2013
    
    Set<Id> orpahnedBGIds = new Set<Id>();
    Set<Id> sfIds = new Set<Id>();
    Set<Id> budgetGroupIds = new Set<Id>();
    
    if (Trigger.isInsert || Trigger.isUndelete) {
        for (Budget_Allocation__c ba : Trigger.new) {
            budgetGroupIds.add(ba.Budget_Group__c);
            sfIds.add(ba.Student_Folder__c); // NAIS-2036 [DP]
        }
    } else if (Trigger.isDelete) {
        for (Budget_Allocation__c ba : Trigger.old) {
            budgetGroupIds.add(ba.Budget_Group__c);
            sfIds.add(ba.Student_Folder__c);
        }
    } else if (Trigger.isUpdate){
        for (Budget_Allocation__c ba : Trigger.new) {
            // NAIS-2036 [DP]
            Budget_Allocation__c baOld = Trigger.oldMap.get(ba.Id);
            if(ba.Amount_Allocated__c != baOld.Amount_Allocated__c || ba.Student_Folder__c != baOld.Student_Folder__c){
                sfIds.add(ba.Student_Folder__c);
                budgetGroupIds.add(ba.Budget_Group__c);
            }
        }
    }
    
    // update budget balance
    if (!budgetGroupIds.isEmpty()){
        SchoolBudgetGroupAction.recalcBudgetBalance(budgetGroupIds, null);
    }

    // NAIS-2036 [DP] 11.19.2014
    if (!sfIds.isEmpty()){
        SchoolBudgetGroupAction.recalcStudentFolderGrantAwarded(sfIds);
    }
}