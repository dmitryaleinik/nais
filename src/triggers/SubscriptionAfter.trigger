/*
 * NAIS-967 Roll Up Subscription Status and Type to Account
 *
 * WH, Exponent Partners, 2013
 */
trigger SubscriptionAfter on Subscription__c (after delete, after insert, after update, after undelete) {
	
	Set<Id> acountIdSet = new Set<Id>();
	
	for (Subscription__c s : (Trigger.isDelete ? Trigger.old : Trigger.new)) {
		acountIdSet.add(s.Account__c);
	}
	
	List<Id> accountIds = new List<Id>();
	accountIds.addAll(acountIdSet);
	
	if (!accountIds.isEmpty()) {
		AccountAction.rollupSubscriptionStatus(accountIds);
	}

    if (Trigger.isUpdate)
    {
        SubscriptionService.updateAnnualSettingDisplayEstimatesField(Trigger.new, Trigger.oldMap);
    }
}