/*
    * SPEC-099
    * Once all of the required documents have been provided and verified, the (document) stage of the folder should change
    *
    *
    */
trigger FamilyDocumentBefore on Family_Document__c (before delete, before insert) {
    
    // ability to deactivate trigger from hierarchical custom setting
    public static Boolean doTrigger = true;
    try {
        TriggerDeactiviation__c trigDeactivate = TriggerDeactiviation__c.getInstance();
        if (trigDeactivate != null && trigDeactivate.Disable_Triggers__c) {
            doTrigger = false;
            System.debug('TRIGGER DEACTIVATED via custom settings');
        }
    } catch (Exception e) {
        System.debug('Error getting custom setting: ' + e);
    }

    if (doTrigger) {
        if (Trigger.isDelete) {
            // [SL] NAIS-282 - update the doc status on the school pfs assignment if a family document record is deleted
            Set<Id> familyDocumentIds = new Set<Id>();
            for (Family_Document__c oldFamilyDoc : Trigger.old) {
                familyDocumentIds.add(oldFamilyDoc.Id);
            }
            if (!familyDocumentIds.isEmpty()) {
                SchoolSetSPFSDocumentStatus.updateDocumentStatusFromFamilyDocuments(familyDocumentIds, true, null);
            }
        } else if (Trigger.isInsert) {
            for (Family_Document__c familyDoc : Trigger.new) {
                // SFP-1506: Make sure that we record the original document pertains to value so we can identify documents that come back from SpringCM and we identify a different document pertains to value.
                // We only do this if the original pertains to value is blank so that we don't overwrite a value set when documents are split.
                // For document splits, we inherit the doc pertains to orig value from the parent document so we wouldn't want to overwrite it.
                if (String.isBlank(familyDoc.Document_Pertains_To_Original__c)) {
                    familyDoc.Document_Pertains_To_Original__c = familyDoc.Document_Pertains_to__c;
                }
            }
        }
    }
}