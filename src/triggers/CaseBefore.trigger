trigger CaseBefore on Case(before insert, before delete, before update) {
    
    if (trigger.isBefore && trigger.isUpdate) {
    
        Cases casesHandler = Cases.newInstance(Trigger.new);
        casesHandler.onBeforeUpdate(Trigger.oldMap);
    }
}