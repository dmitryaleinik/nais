/**
 * SPEC-108: School Portal - Feature Version, R-160, R-159, 60 hours [Drew]
 * For schools which have the full feature view, it is as speced. For schools which have the limited feature view,
 * they will need to have a different nav so they won't be able to see most of the admin functions.
 * The NAIS Administrator should be able to set at a Global level whether their school sees the full or basic version.
 *
 * DP Exponent Partners, 2013
 **/
trigger AccountAfter on Account (after insert, after update) {
    Set<Id> accountIdsNewlyBasic = new Set<Id>();
    Set<Id> accountIdsNewlyFull = new Set<Id>();
    // NAIS-2476 [jB]
    Set<Id> accountIdsUpdateAlertFrequency = new Set<Id>();
    Map<String, Account> accountGroupValuesChanged = new Map<String, Account>();

    if (trigger.isUpdate) {
        for (Account a : trigger.new) {
            // only run this for access org or school record types and only if portal version has changed
            if (a.RecordTypeId == RecordTypes.accessOrgAccountTypeId || a.RecordTypeId == RecordTypes.schoolAccountTypeId) {
                // [CH] NAIS-2063 Moved if statement to make room for SSS Code change logic
                if(a.Portal_Version__c != null && a.Portal_Version__c != trigger.oldMap.get(a.Id).Portal_Version__c) {
                    if (a.Portal_Version__c == 'Basic') {
                        accountIdsNewlyBasic.add(a.Id);
                    } else if (a.Portal_Version__c == 'Full-featured') {
                        accountIdsNewlyFull.add(a.Id);
                    }
                }
                // [CH] NAIS-2063 If an Account has had its SSS Code changed
                if((a.SSS_School_Code__c != null && a.SSS_School_Code__c != trigger.oldMap.get(a.Id).SSS_School_Code__c) || (a.Name != null && a.Name != trigger.oldMap.get(a.Id).Name)) {
                    accountGroupValuesChanged.put('X' + a.Id, a);
                }
            }
            // NAIS-2476 - Update the SSS Main Contact for the account if it's changed [jB]
            if(a.Alert_Frequency__c != trigger.oldMap.get(a.Id).Alert_Frequency__c) {
                accountIdsUpdateAlertFrequency.add(a.Id);
            }
        }
        
        /* SFP-928: We skip update user profiles from batch, because we can not run it @future methodos from there.
        And we need to avoid MIXED_DML_OPERATION: DML operation on setup object is not permitted 
        after you have updated a non-setup object (or vice versa): User, original object: Account: []*/
        if (!System.isBatch() && (!accountIdsNewlyFull.isEmpty() || !accountIdsNewlyBasic.isEmpty())) {
            userAction.updateUserProfiles(accountIdsNewlyBasic, accountIdsNewlyFull);
        }

        // [CH] NAIS-2063 If an Account has had its SSS Code or Name changed
        if(accountGroupValuesChanged.size() > 0) {
            AccountAction.updatePublicGroups(accountGroupValuesChanged);
        }

        // NAIS-2476
        if(accountIdsUpdateAlertFrequency.size() > 0) {
            userAction.setPfsNotificationFrequencyMainContact(accountIdsUpdateAlertFrequency);
        }
    }

    if(trigger.isInsert) {
        // [CH] NAIS-1766 Create public groups for sharing new Accounts
        AccountAction.generatePublicGroups(trigger.newMap);
    }
}