trigger PFSBefore on PFS__c (before update, before insert) {
    
    // [CH] NAIS-2258 Adding value copying logic
    for(PFS__c pfsRecord : trigger.new){
        if(pfsRecord.KS_Mail_Household__c){
            pfsRecord.KS_Mailing_Address__c         = pfsRecord.KS_Household_Address__c;
            pfsRecord.KS_Mailing_City__c             = pfsRecord.KS_Household_City__c;
            pfsRecord.KS_Mailing_State_Province__c     = pfsRecord.KS_Household_State_Province__c;
            pfsRecord.KS_Mailing_ZIP_Postal_Code__c = pfsRecord.KS_Household_ZIP_Postal_Code__c;
            pfsRecord.KS_Mailing_Country__c         = pfsRecord.KS_Household_Country__c;
        }
    }
    
    /*
    * SPEC-017
    * Update the EFC Calc Status to "Recalculate" if any of the EFC input fields have changed 
    *
    * SL 5/6/2013
    */

    Boolean profileCanUpdatePFSModifiedFields = PFSAction.profileCanUpdatePFSApplicationModifiedDate();
    List<Pfs__c> pfssToClearParentBIncomeInformation = new List<Pfs__c>();
    
    for (PFS__c pfs : Trigger.new) {
        //clear any unnecessary dependent values
        ApplicationUtils.clearUnnecessaryVehicles(pfs);
        ApplicationUtils.cleanRealEstateInfo(pfs);
        ApplicationUtils.clearTaxableWorksheet(pfs);
        ApplicationUtils.clearNonTaxableWorksheet(pfs);
        ApplicationUtils.cleanBasicTaxInfo(pfs);
        ApplicationUtils.cleanBusinessFarmInfo(pfs);

        //JIMI NAIS-965 update the application last modified date everytime the PFS is updated.
        if ( profileCanUpdatePFSModifiedFields )
        {
            pfs.Application_Last_Modified_by_Family__c = PFSAction.LastModifiedTime;
            
            // [SL] NAIS-966 update the EFC Fields Last Modified By Family if a EFC field is updated by a family portal user
            if (Trigger.isInsert) 
            {
                if (EfcCalculatorAction.pfsHasUpdatedEfcFields(pfs, null)) 
                {
                    pfs.EFC_Fields_Last_Modified_by_Family__c = PFSAction.LastModifiedTime;
                }
            }
            else if (Trigger.isUpdate) 
            { 
                if (EfcCalculatorAction.pfsHasUpdatedEfcFields(pfs, Trigger.oldMap.get(pfs.Id))) 
                {
                    pfs.EFC_Fields_Last_Modified_by_Family__c = PFSAction.LastModifiedTime;
                }
                //SFP-352:Clear the Parent_B_Filing_Status__c if Student_Has_Parent_B__c changes from Yes to No
                if((Trigger.isBefore && pfs.Student_Has_Parent_B__c=='No' && pfs.Student_Has_Parent_B__c!=(Trigger.oldmap.get(pfs.Id)).Student_Has_Parent_B__c)
                ||(Trigger.isBefore && pfs.Parent_B_Filing_Status__c!=null && pfs.Filing_Status__c!='Single' && pfs.Filing_Status__c!='Head of Household'))
                {
                    pfs.Parent_B_Filing_Status__c=null;
                }
            }
        }

        if (Trigger.isUpdate && pfs.Student_Has_Parent_B__c == 'No' && Trigger.OldMap.get(pfs.Id).Student_Has_Parent_B__c == 'Yes')
        {
            pfssToClearParentBIncomeInformation.add(pfs);
        }
    }

    if (!pfssToClearParentBIncomeInformation.isEmpty())
    {
        for (Pfs__c pfs : pfssToClearParentBIncomeInformation)
        {
            ApplicationUtils.clearParentBIncomeInformation(pfs);
        }
    }
    
    if (Trigger.isUpdate)
    {
        if (EfcDataManager.isUpdatingSssEfcCalc != true) {
            for (PFS__c pfs : Trigger.new) {
                PFS__c oldPfs = null;
                oldPfs = Trigger.oldMap.get(pfs.Id);            

                if (pfs.SSS_EFC_Calc_Status__c != EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE) {    
                    // SFP-273 [DP] 04.18.2016 this boolean is true if the PFS is newly submitted/paid 
                    Boolean newlySetToEFCRecalculable = EfcCalculatorAction.isStatusRecalculable(pfs.PFS_Status__c, pfs.Payment_Status__c, pfs.Visible_to_Schools_Until__c) && !EfcCalculatorAction.isStatusRecalculable(oldPfs.PFS_Status__c, oldPfs.Payment_Status__c, oldPfs.Visible_to_Schools_Until__c);

                    // SFP-273 [DP] 04.18.2016 if the PFS is newly submitted/paid, we'll kick off the EFC  
                    if (newlySetToEFCRecalculable){
                        EFC_Settings__c efcSettings = EFCUtil.getEfcSettings();
                        String newPFSEFCSetting;
                        if (efcSettings != null){
                            newPFSEFCSetting = efcSettings.Initial_EFC_Calc_Method__c; 
                        }

                        // [DP] 04.18.2016 future logic not yet implemented -- could eventually have a setting of "future" th
                        if (newPFSEFCSetting == 'Batch'){
                            pfs.SSS_EFC_Calc_Status__c = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE_BATCH;
                        } else {
                            pfs.SSS_EFC_Calc_Status__c = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE;
                        }
                    // SFP-273 [DP] 04.18.2016 If the PFS is NOT newly submitted/paid, then we do the normal check to see if an EFC recalc is needed
                    } else if (EfcCalculatorAction.isEfcRecalcNeeded(pfs, oldPfs)) {               
                        pfs.SSS_EFC_Calc_Status__c = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE;
                    }
                }
            }
        } else { // this else loop runs if EfcDataManager.isUpdatingSssEfcCalc == true
            // [DP] 04.04.2016 SFP-405 if the EFC has already run once (i.e., isUpdateingSssEFCCalc is true), then we need to set this recalc to batch, 
            // since the EFC can't run twice in a row
            for (PFS__c pfs : Trigger.new) {
                if (pfs.SSS_EFC_Calc_Status__c == EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE){
                    pfs.SSS_EFC_Calc_Status__c = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE_BATCH;
                }
            }
        }

        // [DP] NAIS-1600 -- prevent Parent A from updating status fields if they have the record in edit mode in multiple tabs 3.7.14
        // [CH] NAIS-2014 - Updating to use GlobalVariables method to reduce SOQL queries
        // User currentUser = [Select Id, ContactId from User where Id = :UserInfo.getUserID()];
        User currentUser = GlobalVariables.getCurrentUser();
        
        for (PFS__c pfs : Trigger.new) {
            PFS__c oldPfs = null;
            oldPfs = Trigger.oldMap.get(pfs.Id);
            // not using GlobalVariables method here because doing so broke our unit tests
//            User currentUser = [Select Id, ContactId from User where Id = :UserInfo.getUserID()];
            if (pfs.Parent_A__c == currentUser.ContactId){
                // Parent A cannot un-submit a PFS
                pfs.PFS_Status__c = oldPFS.PFS_Status__c == 'Application Submitted' ? oldPFS.PFS_Status__c : pfs.PFS_Status__c;
                // Parent A cannot set PFS back to "Unpaid"
                pfs.Payment_Status__c = pfs.Payment_Status__c == 'Unpaid' && oldPFS.Payment_Status__c != 'Unpaid' ? oldPFS.Payment_Status__c : pfs.Payment_Status__c;
                // Parent A cannot un-waive a fee
                pfs.Fee_Waived__c = oldPFS.Fee_Waived__c == 'Yes' ? oldPFS.Fee_Waived__c : pfs.Fee_Waived__c;
            }
        }
    }
    /**
     *    NAIS-503: Any changes made to the application data should be captured in an audit trail
     *
     *    SL, Exponent Partners, 2013
     */
     if (Trigger.isUpdate) {
         AuditLogAction.doAudit(Trigger.new, Trigger.oldMap);
     }
}