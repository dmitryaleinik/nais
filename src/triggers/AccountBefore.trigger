/**
 * @description Spec-096 School Portal - School PFS - Notification, Req# R-425, R-429
 *              An option Account.Alert_Frequency__c sets the frequency of PFS Submission
 *              notification: Once per Day, Once per Week.
 *
 *              Whenever a PFS is submitted, each school on the School PFS Assignments is
 *              checked to see what their frequency is, and when the last send was. If the
 *              last email send (read-only field Account.Last_PFS_Notification_Date__c) is
 *              more than the day or week depending on the frequency, all of the PFS records
 *              for that time period are grabbed (based on Submission Date) and listed in
 *              the email. Once the email is sent, Account.Last_PFS_Notification_Date__c
 *              is updated.
 *
 *              Recipients are all staff contacts at that school who are set to receive email:
 *              Contact.PFS_Alert_Preferences__c.
 *
 *              Trigger to reset Account.Last_PFS_Notification_Date__c when Account.Alert_Frequency__c
 *              is changed so that existing submissions are not sent in the first batch after alert is
 *              (re)activated or alert frequency is altered.
 *
 *              WH, Exponent Partners, 2013
 **/
trigger AccountBefore on Account (before insert, before delete, before update)
{

    if (Trigger.isUpdate)
    {
        for (Account a : Trigger.new) {
            String newValue = a.Alert_Frequency__c;
            String oldValue = Trigger.oldMap.get(a.Id).Alert_Frequency__c;
            if (newValue != oldValue && !(oldValue == 'Daily digest of new applications' && newValue == 'Weekly digest of new applications')) {
                // no need to exclude existing submissions when alert frequency is reduced from daily to weekly
                a.Last_PFS_Notification_Date__c = null;
            }

            a.Portal_Version__c = SubscriptionTypes.Instance
                    .getPortalVersionSubscriptionType(a.SSS_Subscription_Type_Current__c);

        }
    }

    if (Trigger.isInsert)
    {
        SchoolCreationService.Instance.setSSSCodes(Trigger.new);
    }
}