/**
 * Trigger: SchoolBizFarmAssignmentAfter
 *
 * Copyright (C) 2015 Exponent Partners
 *
 * Purpose: This trigger handles School Biz Farm Assignment updates, and summarizes the information onto the folder.  For examples
 * if the family has two businesses, each with 10,000 in assets, the folder.PFS1_Business_Farm_Assets__c would be set to
 * 20,000
 * 
 * // NAIS-2528
 * // Sum up BF totals and write to Student Folder
 * // The totals from the BF (or SBFAs) need to be summed up and written to the Student Folder. 
 * // This will replace the code in SchoolPFSAction.setPFS1Info and setPFS2Info (and that code needs to be removed).
 *
 * Where Referenced:
 *   
 *
 * Change History:
 *
 * Developer         Date                          Description
 * ---------------------------------------------------------------------------------------
 * Drew Piston      2015.08.17   		 Initial Development
 *
 * 
 */

// NAIS-2528 [DP] 08.17.2015
// Sum up BF totals and write to Student Folder
// The totals from the BF (or SBFAs) need to be summed up and written to the Student Folder. 
// This will replace the code in SchoolPFSAction.setPFS1Info and setPFS2Info (and that code needs to be removed).

trigger SchoolBizFarmAssignmentAfter on School_Biz_Farm_Assignment__c (after insert, after update, after undelete, after delete) {

	if (trigger.isAfter){
		if(trigger.isInsert){
			SchoolPFSAction action = new SchoolPFSAction();
			if (action.buildIDMapFromInsertBizFarm(trigger.new)){
				action.updateFolder();
			}
		} else if (trigger.isUpdate){
			SchoolPFSAction action = new SchoolPFSAction();
			if (action.buildIDMapFromUpdateBizFarm(trigger.new, trigger.oldMap)){
				action.updateFolder();
			}
			//SFP-55, [G.S]
			boolean isPortalUser = (GlobalVariables.isSchoolPortalProfile(userinfo.getProfileId()));
		    if (isPortalUser) {
			   	SchoolBizFarmAssignmentAction sbfa = new SchoolBizFarmAssignmentAction();

				sbfa.SetRevisionMetrics(trigger.newMap, trigger.oldMap);
		    }
		}

		List<School_Biz_Farm_Assignment__c> sbfaList = trigger.isDelete ? trigger.old : trigger.new;
		Map<Id, School_Biz_Farm_Assignment__c> oldSbfaMap = trigger.isDelete ? null : trigger.oldMap;
		SchoolBizFarmAssignmentAction.recalcEfcIfNeeded(sbfaList, oldSbfaMap);
	}
}