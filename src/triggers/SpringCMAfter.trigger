trigger SpringCMAfter on SpringCM_Document__c (after insert) {


	if (trigger.isAfter){
		List<SpringCM_Document__c> springs = new List<SpringCM_Document__c>();
		for (SpringCM_Document__c s : trigger.new){
			springs.add(s);
		}
		
		if (!springs.isEmpty()){
			SpringCMAction.processSpringCMRecords(springs);
		}
	}
	

}