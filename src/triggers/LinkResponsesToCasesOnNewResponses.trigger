trigger LinkResponsesToCasesOnNewResponses on Feedback_Survey_Response__c (before insert) {
    
    if (FeatureToggles.isEnabled('Link_Survey_Responses_To_Cases__c')) {
        Set<String> chatKeys = new Set<String>();

        for (Feedback_Survey_Response__c response : Trigger.new) {
            if (String.isNotBlank(response.ChatKey__c)) {
                chatKeys.add(response.ChatKey__c);
            }
        }
        
        // Query all the chat transcripts by chat key.
        List<LiveChatTranscript> chatTranscripts = [SELECT Id, CaseID, ChatKey FROM LiveChatTranscript WHERE ChatKey IN :chatKeys];
        
        // Group the transcripts by chat key so we can grab the right one for each survey response.
        Map<String, LiveChatTranscript> chatTranscriptsByChatKey = new Map<String, LiveChatTranscript>();
        for (LiveChatTranscript transcript : chatTranscripts) {
            chatTranscriptsByChatKey.put(transcript.ChatKey, transcript);
        }
        
        for (Feedback_Survey_Response__c response : Trigger.new) {
            if (response.Related_Case__c == null) {
                LiveChatTranscript transcript = chatTranscriptsByChatKey.get(response.ChatKey__c);
                
                // If we don't have a transcript for this chat key, skip it.
                if (transcript == null) {
                    continue;
                }
                
                response.Related_Case__c = transcript.CaseID;
            }
        }   
    }
}