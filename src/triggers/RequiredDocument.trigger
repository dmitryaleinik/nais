/*
 * Spec-044 Document Management - Required Document Processing; Req# R-312
 *	Trigger on Required Document to keep School Document Assignment in synch with required documents for the school.
 *	When a Required Document record is inserted or deleted, a corresponding School Document Assignment record is added or removed.
 *
 * WH, Exponent Partners, 2013
 */
trigger RequiredDocument on Required_Document__c (after insert, after update, before delete) {
	
	if (Trigger.isAfter && Trigger.isInsert) {
		
		List<Required_Document__c> rdocList = new List<Required_Document__c>();
		Set<Id> rDocSet = new Set<Id>();
		
		for (Required_Document__c rd : Trigger.new) {
			if (rd.Academic_Year__c != null && rd.Deleted__c == false) {
				rdocList.add(rd);
				rDocSet.add(rd.Id);
			}
		}
		
		if (!rdocList.isEmpty()) {
			// [dp] 6.4.14 dozens and dozens of unit tests assumed this would be synchronous, rather than re-write we will use this logic
			if (Test.isRunningTest()){
				SchoolDocumentAssignmentAction.createSchoolDocAssignments(rdocList);
			} else {
				SchoolDocumentAssignmentAction.createSchoolDocAssignments(rdocList);
				//SchoolDocumentAssignmentAction.createSchoolDocAssignmentsFromReqDocIdsFuture(rDocSet);
			}
		}
		
	} else if (Trigger.isBefore && Trigger.isDelete) {
		
		SchoolDocumentAssignmentAction.removeSchoolDocAssignments(Trigger.old);
		
	}
	// [CH] NAIS-1590 Adding support for School-Specific Document label updates
	else if(Trigger.isAfter && Trigger.isUpdate){
		
		Set<Id> requiredDocumentIds = new Set<Id>{};
		String schoolSpecificDocType = ApplicationUtils.schoolSpecificDocType();
		
		// Figure out if there are Required Documents with a label that changed
		for(Integer i=0; i<trigger.new.size(); i++){
			// If the school-specific label has changed
			if(trigger.new[i].Document_Type__c == schoolSpecificDocType 
				&& trigger.new[i].Label_for_School_Specific_Document__c != trigger.old[i].Label_for_School_Specific_Document__c)
			{
				requiredDocumentIds.add(trigger.new[i].Id);	
			}
		}
		
		// Get the group of SDA records to update
		List<School_Document_Assignment__c> sdaRecordsToUpdate = [select Id, Required_Document__c from School_Document_Assignment__c where Required_Document__c in :requiredDocumentIds];
		for(School_Document_Assignment__c sdaRecord : sdaRecordsToUpdate){
			sdaRecord.Label_for_School_Specific_Document__c = trigger.newMap.get(sdaRecord.Required_Document__c).Label_for_School_Specific_Document__c;
		}
		
		// Update any SDA records that need it
		if(sdaRecordsToUpdate.size() > 0){
			update sdaRecordsToUpdate;
		}
	}
}