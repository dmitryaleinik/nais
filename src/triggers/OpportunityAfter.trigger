trigger OpportunityAfter on Opportunity (after insert, after update) {
    // [dp] NAIS-1628
    List<Opportunity> oppsToGetSubscriptions = new List<Opportunity>();
    List<Opportunity> oppsToDeleteSubscriptions = new List<Opportunity>();//[LR] SFP-571

    // [CH] NAIS-1669 Adding logic to keep from creating subscription records
    public static Boolean doTrigger = true;

    try {
        TriggerDeactiviation__c trigDeactivate = TriggerDeactiviation__c.getInstance();
        if (trigDeactivate != null && trigDeactivate.Disable_Triggers__c){
            doTrigger = false;
            System.debug('TRIGGER DEACTIVATED via custom settings');
        }
    } catch (Exception e) {
        System.debug('Error getting custom setting: ' + e);
    }

    // NAIS-2042
    List<Opportunity> oppsWithUpdatedPaidStatus = new List<Opportunity>();
    //  Transaction Line Items created
    List<Opportunity> opportunitesToGetTLIRecords = new List<Opportunity>{};
    /*NAIS-2469*/
    Id profileId=userinfo.getProfileId();
    boolean isPortalUser = (GlobalVariables.isSchoolPortalProfile(profileId) || GlobalVariables.isFamilyPortalProfile(profileId));
    /*End NAIS-2469*/
    if (Trigger.isInsert) {
        // Create a couple of lists to collect Opportunities that need
        // Accumulate lists of Opportunities
        for(Opportunity oppRecord : Trigger.new){

            // NAIS-2270: suppress auto insert of Sale TLI in subscription renewal flow
            if (!oppRecord.SYSTEM_Suppress_Auto_Sale_TLI_Insert__c &&
                ((oppRecord.RecordTypeId == RecordTypes.opportunityFeeWaiverPurchaseTypeId && oppRecord.Product_Quantity__c != null) ||
                   oppRecord.RecordTypeId == RecordTypes.opportunitySubscriptionFeeTypeId)) {
                       // only create TLIs if it was not manually created
                       if (isPortalUser) {
                           opportunitesToGetTLIRecords.add(oppRecord);
                       }
            }
            // [dp] NAIS-1628
            if (oppRecord.RecordTypeId == RecordTypes.opportunitySubscriptionFeeTypeId
            && OpportunityAction.closedoppStageNames.contains(oppRecord.StageName)
            && oppRecord.Paid_Status__c == 'Paid'){
                oppsToGetSubscriptions.add(oppRecord);
            }
        }
    } else if (Trigger.isUpdate) {
        
        List<Opportunity> subscriptionFeeOpportunities = new List<Opportunity>();
        
        for (Opportunity oppRecord : Trigger.new) {
            
            Opportunity oldOpp = Trigger.oldMap.get(oppRecord.Id);
            
            if (oppRecord.RecordTypeId == RecordTypes.pfsApplicationFeeOpportunityTypeId || oppRecord.RecordTypeId == RecordTypes.opportunityFeeWaiverPurchaseTypeId) {
                if ((oppRecord.Paid_Status__c != oldOpp.Paid_Status__c) || (oppRecord.Total_Write_Offs__c != oldOpp.Total_Write_Offs__c)) {
                    oppsWithUpdatedPaidStatus.add(oppRecord);
                }
            } else if (oppRecord.RecordTypeId == RecordTypes.opportunitySubscriptionFeeTypeId) {
                subscriptionFeeOpportunities.add(oppRecord);
            }
        }
        
        SubscriptionService.Response response = SubscriptionService.Instance.getOppsThatNeedSubAdjustments(subscriptionFeeOpportunities, Trigger.oldMap);
        if(!response.OppIdsToDeleteSubs.isEmpty()) {
            oppsToDeleteSubscriptions.addAll(response.OppIdsToDeleteSubs.Values());
        }
        
        if(!response.OppIdsToCreateSubs.isEmpty()) {
            oppsToGetSubscriptions.addAll(response.OppIdsToCreateSubs.Values());
            
        }
    }

    // Create Transaction Line Item records
    if (opportunitesToGetTLIRecords.size() > 0){
        // Call the version of the TLI insertion method that handles subscriptions and 5-packs
        PaymentUtils.insertTransactionLineItemsFromOpportunities(opportunitesToGetTLIRecords);
    }
    if (!oppsToGetSubscriptions.isEmpty() && doTrigger){
        OpportunityAction.createSubscriptionRecordsFromPaidOpps(oppsToGetSubscriptions);
    }
    if (!oppsToDeleteSubscriptions.isEmpty() && doTrigger){
        OpportunityAction.deleteSubscriptionRecordsFromPaidOpps(oppsToDeleteSubscriptions);
    }

    // [CH] NAIS-1556 Logic to update the Total_Waivers_Override_Default__c field on Annual Settings
    Set<String> academicYearNameSet = new Set<String>(); // NAIS-2417 [DP] 05.13.2015

    Set<Id> accountIdSet = new Set<Id>{};
    Map<String, Opportunity> opportunityMap = new Map<String, Opportunity>{};

    Set<Id> oppsToQuery = new Set<Id>{};
    Set<Id> pfsIdsToUpdate = new Set<Id>{};

    // Create a list of academic year and account combos to have their related Annual Settings updated with a total fee waiver count
    for (Integer i=0; i<trigger.new.size(); i++) {
        Opportunity oppRecord = trigger.new[i];

        if ((trigger.isInsert || oppRecord.Net_Amount_Due__c != trigger.oldMap.get(oppRecord.Id).Net_Amount_Due__c) && oppRecord.PFS__c != null) {
            pfsIdsToUpdate.add(oppRecord.PFS__c);
        }

        // If the Opportunity is the Fee Waiver record type, and stage is paid in full, and trigger is an insert
        // or the trigger is an update and the stage was not previously Paid in Full
        if (oppRecord.RecordTypeId == RecordTypes.opportunityFeeWaiverPurchaseTypeId &&
            oppRecord.StageName == 'Paid in Full' &&
            (trigger.isInsert || (trigger.isUpdate && trigger.old[i].StageName != 'Paid in Full'))) {
            accountIdSet.add(oppRecord.AccountId);
            academicYearNameSet.add(oppRecord.Academic_Year_Picklist__c);
            oppsToQuery.add(oppRecord.Id);
        }
    }


    //// NAIS-2042 Start -- combining update calls to PFS
    if (!oppsWithUpdatedPaidStatus.isEmpty() || !pfsIdsToUpdate.isEmpty()) {
        OpportunityAction.updatePFSsFromOppTrigger(oppsWithUpdatedPaidStatus, pfsIdsToUpdate);
    }
    //// NAIS-2042 End

    // Need to re-query Opportunity records to get rollup summary values after inserting TLI records
    for (Opportunity fullOppRecord : [select Id, AccountId, Academic_Year_Picklist__c, Total_Purchased_Waivers__c
                                        from Opportunity
                                        where id in: oppsToQuery]) {
        opportunityMap.put(fullOppRecord.AccountId + '|' + fullOppRecord.Academic_Year_Picklist__c, fullOppRecord);    // NAIS-2417 [DP] 05.13.2015
    }

    // If there are any Annual Settings records that need to be updated
    if (academicYearNameSet != null && accountIdSet != null) {

        List<Annual_Setting__c> annualSettingsToUpdate = new List<Annual_Setting__c>{};
        // Query for Annual Setting records and build the list to update
        for(Annual_Setting__c annualSettingRecord : [select Id, Total_Waivers_Override_Default__c, School__c, Academic_Year__c, Academic_Year__r.Name
                                                        from Annual_Setting__c
                                                        where School__c in :accountIdSet
                                                        and Academic_Year__r.Name in :academicYearNameSet]) {
            // Look up the corresponding Opportunity
            Opportunity matchingOpp = opportunityMap.get(annualSettingRecord.School__c + '|' + annualSettingRecord.Academic_Year__r.Name); // NAIS-2417 [DP] 05.13.2015

            if (matchingOpp != null) {
                annualSettingRecord.Total_Waivers_Override_Default__c += matchingOpp.Total_Purchased_Waivers__c;
                annualSettingsToUpdate.add(annualSettingRecord);
            }
        }

        if (annualSettingsToUpdate.size() > 0) {
            Database.update(annualSettingsToUpdate);
        }
    }
}