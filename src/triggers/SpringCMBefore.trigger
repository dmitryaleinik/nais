trigger SpringCMBefore on SpringCM_Document__c (before insert) {
    
    if (trigger.isBefore){
        
        SpringCmDocuments.newInstance(Trigger.new).handleBeforeInsert();
    }
}