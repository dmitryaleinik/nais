// Spec-139 School Portal - Fee Waivers Rollups
// BR, Exponent Partners, 2013
trigger AFWAfter on Application_Fee_Waiver__c (after insert, after update) {
    // Check Trigger.new for School and Year.
    Map<Id, Set<Id>> schoolIdsByYear = new Map<Id, Set<Id>>();

    // [SL] NAIS-1032: keep ruinning list of fee waivers to create TLIs
    List<Application_Fee_Waiver__c> feeWaiversToCreateTLIs = new List<Application_Fee_Waiver__c>();

    // [dp] NAIS-1527: keep ruinning list of pending/declined waivers to set "Waiver Requested" fee waived status on PFS
    List<Application_Fee_Waiver__c> feeWaiversToProcess = new List<Application_Fee_Waiver__c>();

    if (Trigger.isInsert) {
        for (Application_Fee_Waiver__c afw : Trigger.new) {
            // SFP-190 add 'Pending Qualified' counts applied
            if ((afw.Account__c != null) && (afw.Academic_Year__c != null) && (afw.Status__c == 'Assigned' || afw.Status__c == 'Pending Qualification')) {
                // inserting new Assigned AFW.
                if (schoolIdsByYear.get(afw.Academic_Year__c) == null) {
                    schoolIdsByYear.put(afw.Academic_Year__c, new Set<Id>());
                }
                schoolIdsByYear.get(afw.Academic_Year__c).add(afw.Account__c);

                // only create / process if 'Assigned'
                if (afw.Status__c == 'Assigned') {
                    feeWaiversToCreateTLIs.add(afw); // [SL] NAIS-1032: need to create a TLI for this waiver
                }
            }

            // acad year and parent must be filled out to set status on PFS
            if (afw.Academic_Year__c != null && afw.Contact__c != null) {
                if (afw.Status__c == null || afw.Status__c == ApplicationFeeWaiverAction.WAIVER_PENDING_STATUS || afw.Status__c == ApplicationFeeWaiverAction.WAIVER_DECLINED_STATUS) {
                    feeWaiversToProcess.add(afw);
                }
            }
        }
    } else if (Trigger.isUpdate) {
        for (Application_Fee_Waiver__c afw : Trigger.new) {
            if ((afw.Account__c != null) && (afw.Academic_Year__c != null)
                    && afw.Status__c == 'Assigned' && Trigger.oldMap.get(afw.Id).Status__c != 'Assigned') {

                // updating AFW.Status to Assigned.
                if (schoolIdsByYear.get(afw.Academic_Year__c) == null) {
                    schoolIdsByYear.put(afw.Academic_Year__c, new Set<Id>());
                }
                schoolIdsByYear.get(afw.Academic_Year__c).add(afw.Account__c);
                feeWaiversToCreateTLIs.add(afw); // [SL] NAIS-1032: need to create a TLI for this waiver
            }

            // acad year and parent must be filled out to set status on PFS
            if (afw.Academic_Year__c != null && afw.Contact__c != null) {
                if ((afw.Status__c == ApplicationFeeWaiverAction.WAIVER_PENDING_STATUS && Trigger.oldMap.get(afw.Id).Status__c != ApplicationFeeWaiverAction.WAIVER_PENDING_STATUS)
                        ||
                        (afw.Status__c == ApplicationFeeWaiverAction.WAIVER_DECLINED_STATUS && Trigger.oldMap.get(afw.Id).Status__c != ApplicationFeeWaiverAction.WAIVER_DECLINED_STATUS)) {
                    feeWaiversToProcess.add(afw);
                }
            }

            // SFP-190 reset the schools balances after waiver is shifted to MBFW
            if ((afw.Account__c != null) && (afw.Academic_Year__c != null)
                    && afw.Status__c == 'Declined' && Trigger.oldMap.get(afw.Id).Status__c == 'Pending Qualification') {
                if (schoolIdsByYear.get(afw.Academic_Year__c) == null) {
                    schoolIdsByYear.put(afw.Academic_Year__c, new Set<Id>());
                }
                schoolIdsByYear.get(afw.Academic_Year__c).add(afw.Account__c);
            }
        }
    }

    // [SL] NAIS-1032: create TLIs for assigned waivers
    if (!feeWaiversToCreateTLIs.isEmpty()) {
        ApplicationFeeWaiverAction.createFeeWaiverTransactionLineItems(feeWaiversToCreateTLIs);
    }

    // [dp] NAIS-1527: keep ruinning list of pending waivers to set "Waiver Requested" fee waived status on PFS
    // [dp] NAIS-1527: keep ruinning list of declined waivers to set "No" fee waived status on PFS
    if (!feeWaiversToProcess.isEmpty())    {
        ApplicationFeeWaiverAction.updateFeeWaiverStatus(feeWaiversToProcess);
    }

    // Update queues.
    List<Account> uqAccounts = new List<Account>();
    List<Annual_Setting__c> uqAnnualSettings = new List<Annual_Setting__c>();


    // Count the Assigned Waivers per school and year and update Annual Settings.
    Academic_Year__c currYear = GlobalVariables.getCurrentAcademicYear();
    for (Id ayId : schoolIdsByYear.KeySet()) {
        Map<Id, Integer> countBySchoolId = new Map<Id, Integer>();
        Set<Id> schoolIds = schoolIdsByYear.get(ayId);
        // SFP-190 count 'Pending Qualification' fee waivers until they are checked for qualification
        for (AggregateResult ar : [select Account__c, count_distinct(Contact__c) numAssigned from Application_Fee_Waiver__c
                                   where Account__c in :schoolIds and (Status__c = 'Assigned' OR Status__c = 'Pending Qualification') and Academic_Year__c = :ayId
                                           group by Account__c]) {

            countBySchoolId.put((Id)ar.get('Account__c'), (Integer)ar.get('numAssigned'));
        }

        List<Annual_Setting__c> anSettings = [select Academic_Year__c, School__c, School__r.Current_Year_Waiver_Balance__c, School__r.Current_Year_Waivers_Assigned__c,
                                              Total_Waivers_Override_Default__c, Waivers_Assigned__c from Annual_Setting__c
                                              where School__c IN :schoolIds and Academic_Year__c = :ayId];
        for (Annual_Setting__c an : anSettings) {
            an.Waivers_Assigned__c = countBySchoolId.get(an.School__c);
            System.debug('new balance: ' + an.Waivers_Assigned__c);
            // Update School(Account) with current Year values.
            if (an.Academic_Year__c == currYear.Id) {
                an.School__r.Current_Year_Waiver_Balance__c = ((an.Total_Waivers_Override_Default__c != null) ? an.Total_Waivers_Override_Default__c : 0) - (an.Waivers_Assigned__c != null ? an.Waivers_Assigned__c : 0);
                an.School__r.Current_Year_Waivers_Assigned__c = an.Waivers_Assigned__c;
                uqAccounts.add(an.School__r);
            }
            uqAnnualSettings.add(an);
        }
    }

    update uqAccounts;
    update uqAnnualSettings;
}