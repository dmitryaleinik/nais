/**
 * @description Trigger for Transaction Line Items.
 */
trigger TransactionLineItem on Transaction_Line_Item__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerFactory.createAndExecuteHandler(TransactionLineItemHandler.class);
}