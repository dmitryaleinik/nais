trigger DependentsBefore on Dependents__c (before insert, before update) {

	// [CH] NAIS-2276 Fill any gaps in incoming records and set the Number of Dependents field
	if(trigger.isInsert || trigger.isUpdate)
	for(Dependents__c depRecord : Trigger.new){
		// Move Data around to fill in gaps
		ApplicationUtils.moveDependents(depRecord, true);
	}

}