trigger OpportunityBefore on Opportunity (before update) {
    /* 
     * [SL] Keep the Opportunity Amount in sync with the Revenue rollup 
     *      (replaces the Update Opportunity Amount workflow)
     */
     
    /*
    if (Trigger.isUpdate) {        
        [CH] NAIS-2442 OLD CODE REMOVED
    }
    */
    
    /*
     * NAIS-943: When the Opportunity of type Application Fee is paid then the Payment status on the PFS 
     * needs to be updated - Partially Paid, Paid in Full and Written Off are the possible values that will need 
     * to be set when the opp is updated.
     * 
     * SL, Exponent Partners, 2013    
     */
     
    if (Trigger.isUpdate) {
        List<Opportunity> oppsWithUpdatedPaidStatus = new List<Opportunity>();
        Opportunity oldOpp;
        for (Opportunity opp : Trigger.new) {
            oldOpp = Trigger.oldMap.get(opp.Id);
            
            // recalculate PFS Payment status if the Paid Status or the Write Off value on the Opportunity changes
            if (opp.RecordTypeId == RecordTypes.pfsApplicationFeeOpportunityTypeId || opp.RecordTypeId == RecordTypes.opportunityFeeWaiverPurchaseTypeId) {
                if ((opp.Paid_Status__c != oldOpp.Paid_Status__c)
                        || (opp.Total_Write_Offs__c != oldOpp.Total_Write_Offs__c)) {
                    oppsWithUpdatedPaidStatus.add(opp);
                }
            }
        }
        if (!oppsWithUpdatedPaidStatus.isEmpty()) {
            //OpportunityAction.updatePfsPaymentStatusFromOpportunity(oppsWithUpdatedPaidStatus);
            OpportunityAction.updateOppFieldsFromOppBefore(oppsWithUpdatedPaidStatus); // [DP] 13.11.2014 NAIS-2042
        }
    }
    
    
}