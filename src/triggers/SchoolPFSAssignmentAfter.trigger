/*
 *
 *  SPEC-068 School Portal - PFS Lock, R-070, 6-8 Hours [Drew]
 *  The school should be able to individually lock and unlock a given PFS once they are done evaluating it, which would prevent the family from submitting updates to it
 *  This just means the EFC should not recalculate if the PFS is locked, and it will only recalculate to begin with if EFC fields are changed.
 *  This lock should also be applied to the other students in the same PFS. This lock should not retroactively be applied to new applicants added after the PFS was locked.
 *
 */

trigger SchoolPFSAssignmentAfter on School_PFS_Assignment__c (after insert, after update, after delete) {
    if(trigger.isUpdate){   System.debug(LoggingLevel.WARN, '** Begin SPA After Trigger'); }
    // ability to deactivate trigger from hierarchical custom setting
    public static Boolean doTrigger = true;
    try {
        TriggerDeactiviation__c trigDeactivate = TriggerDeactiviation__c.getInstance();
        if (trigDeactivate != null && trigDeactivate.Disable_Triggers__c){
            doTrigger = false;
            System.debug('TRIGGER DEACTIVATED via custom settings');
        }
    } catch (Exception e) {
        System.debug('Error getting custom setting: ' + e);
    }
    if(doTrigger){ 
        
        /* [DP] Beginning of trigger execution */
        SchoolSetWeightedPFS.schoolPFSActionTriggerControl.beginTrigger(); 
        
        // Since this is a potentially recursive trigger, we want to make sure this logic is only run at the outermost level (i.e. not called from a  
        // recursive context), so that all other updates are complete before values are copied to the school pfs assignment
        // temp add of "true" for testing   
        // [dp] REMOVED the "true" in line below -- it was causing SOQL limit exceptions
        if (SchoolSetWeightedPFS.schoolPFSActionTriggerControl.isRecursiveContext() == false) {             
            List<School_PFS_Assignment__c> lockedOrUnlockedSPFSs = new List<School_PFS_Assignment__c>();
            Set<Id> unlockedApplicantIds = new Set<Id>(); // [SL] NAIS-598: Copy SSS fields when unlocked
            
            if(trigger.isUpdate){

                // create and execute the SchoolPFSAssignments domain class
                SchoolPFSAssignments.newInstance( Trigger.newMap.values()).handleAfterUpdate( Trigger.oldMap);
                
                // [CH] NAIS-2442 Moved
                Map <Id, School_PFS_Assignment__c> oldSpfsaMap = new Map <Id, School_PFS_Assignment__c>();
                Map <Id, School_PFS_Assignment__c> newSpfsaMap = new Map <Id, School_PFS_Assignment__c>();  
                Set<Id> spfsaIdsUnWithdrawn = new Set<Id>();
                set<Id> spfsaIdsWithdrawn = new Set<Id>();
                Set<Id> studentFolders = new Set<Id>();

                List<School_PFS_Assignment__c> spasWithNewFilingSeparatelyStatus = new List<School_PFS_Assignment__c>(); // [DP] 08.09.2016 SFP-555
                
                for (School_PFS_Assignment__c newSPFS : trigger.new){
                    if (newSPFS.Family_May_Submit_Updates__c != null && newSPFS.Family_May_Submit_Updates__c != trigger.oldMap.get(newSPFS.Id).Family_May_Submit_Updates__c){
                        lockedOrUnlockedSPFSs.add(newSPFS);
                        
                        // [SL] NAIS-598: Copy SSS fields when unlocked
                        if (newSPFS.Family_May_Submit_Updates__c == 'Yes') {
                            unlockedApplicantIds.add(newSPFS.Applicant__c);
                        }
                    }
                    
                    // [CH] NAIS-2442 Moved
                    if ((newSPFS.PFS_Status__c == 'Application Submitted') && (newSPFS.Payment_Status__c == 'Paid in Full')){
                        newSpfsaMap.put(newSPFS.Id, newSPFS);
                        oldSpfsaMap.put(newSPFS.Id, trigger.OldMap.get(newSPFS.Id));
                    }
                    
                    studentFolders.add(newSPFS.Student_Folder__c);
                    if (newSPFS.Academic_Year_Picklist__c != null) {
                        // only going to re-create SDAs if this SPFSA is newly un-withdrawn
                        if (newSPFS.Withdrawn__c != 'Yes' && Trigger.oldMap.get(newSPFS.Id).Withdrawn__c == 'Yes'){
                            //spfsaList.add(spfsa);
                            spfsaIdsUnWithdrawn.add(newSPFS.Id);
                        }
                        
                        // Get School PFS Assignments that are newly withdrawn
                        if (newSPFS.Withdrawn__c == 'Yes' && Trigger.oldMap.get(newSPFS.Id).Withdrawn__c != 'Yes' ){
                            spfsaIdsWithdrawn.add(newSPFS.Id);
                        }
                    }

                    // [DP] 08.04.2016 SFP-555 if filing married/separately has changed
                    if (EfcConstants.IsFilingMarriedSeparated(newSPFS.Filing_Status__c, newSPFS.Parent_B_Filing_Status__c) != EfcConstants.IsFilingMarriedSeparated(Trigger.oldMap.get(newSPFS.Id).Filing_Status__c, Trigger.oldMap.get(newSPFS.Id).Parent_B_Filing_Status__c)){
                        spasWithNewFilingSeparatelyStatus.add(newSPFS);
                    }
                }

                if (!spasWithNewFilingSeparatelyStatus.isEmpty()){
                    // handle filing status change
                    SchoolDocumentAssignmentAction.handleSPAFilingStatusChange(spasWithNewFilingSeparatelyStatus);
                }
                
                // [CH] NAIS-2442 Moved code to eliminate a For loop
        
                // [Nathan] SPEC-108, NAIS-40 Maintain a per school count of revised fields for audit reporting requirement 
                //[G.S], SFP-8, count only if a portal user
                boolean isPortalUser = (GlobalVariables.isSchoolPortalProfile(userinfo.getProfileId()));
                if (SchoolRevisionMetrics.isExecuting == false && isPortalUser) { //[G.S], SFP-8, count only if a portal user
                    if (newSpfsaMap != null && !newSpfsaMap.isEmpty()){
                            
                        SchoolRevisionMetrics srm = new SchoolRevisionMetrics();
                        //srm.oldSpfsaMap = Trigger.OldMap;
                        //srm.newSpfsaMap = Trigger.NewMap;
                        srm.oldSpfsaMap = oldSpfsaMap;
                        srm.newSpfsaMap = newSpfsaMap;
                        srm.SetRevisionMetrics();
                    }
                }  
                
                // [DP] NAIS-2017 -- adding logic here to prevent calls when there is nothing to update
                if (!spfsaIdsWithdrawn.isEmpty()) {
                    SchoolBudgetGroupAction.deleteBudgetBalance(studentFolders, spfsaIdsWithdrawn);
                    
                    // [CH] NAIS-2291 Adding to remove sharing for newly withdrawn SPA records
                    /* Commenting out for the moment, but leaving in case we want it in the future
                    SchoolStaffShareAction.removeSharingForWithdrawnFolders(spfsaIdsWithdrawn);
                    */
                }
                
                // need to delete existing SDAs and create new ones if this SPFSA is newly not withdrawn, as document requirements may have changed
                // NAIS-1854 Start
                //if (!spfsaList.isEmpty()) {
                if (!spfsaIdsUnWithdrawn.isEmpty()) {
                    // future method in case we need it
                    //SchoolDocumentAssignmentAction.deleteExistingAndCreateSchoolDocAssignmentsFromSPAIdsFuture(spfsaIdSet);
                    SchoolDocumentAssignmentAction.deleteExistingAndCreateSchoolDocAssignments(spfsaIdsUnWithdrawn);
                }
                
            }
            // END Trigger.IsUpdate loop
            
            if(!lockedOrUnlockedSPFSs.isEmpty() && SchoolPFSLockSiblings.isFirstRun){
                SchoolPFSLockSiblings.isFirstRun = false;
                SchoolPFSLockSiblings.lockOrUnlockSiblingSPFSs(lockedOrUnlockedSPFSs);
            }
            
            // [SL] NAIS-598: Copy SSS fields when unlocked
            if (!unlockedApplicantIds.isEmpty()) {
                PFSAction.updateSssFieldsOnSchoolPfsAssignmentsForApplicantIds(unlockedApplicantIds, false);
            }
           
            // [Nathan] SPEC - 060 
            // NAIS-2403 [DP] 05.11.2015 always run on insert, only run on delete if it's not in a recursive state
            if ((SchoolSetWeightedPFS.isExecuting == false && Trigger.isDelete) || Trigger.isInsert) {
                SchoolSetWeightedPFS sswPFS = new SchoolSetWeightedPFS();
                if (Trigger.isInsert) {
                    sswPFS.newSpfsaMap = Trigger.NewMap;
                }
                if (Trigger.isDelete) {
                    sswPFS.oldSpfsaMap = Trigger.OldMap;
                }
                sswPFS.setWeightedPFS();
            }  
            
           /*
            [SL] NAIS-598: When a PFS is unlocked, copy the SSS values to the School PFS Assignment
            [CH 04/30/2013] Call re-calculation of EFC when SPA record are changed
            [SL 5/4/13] SPEC-017 Recalculate EFC when Revision EFC Calc Status = Recalculate
            */
            if (Trigger.isInsert || Trigger.isUpdate) {
                if ((EfcDataManager.isUpdatingRevisionEfcCalc != true) && (EfcCalculatorAction.getIsDisabledRevisionAutoCalc() != true)) {
                    Set<Id> schoolPfsAssignmentIdsToRecalc = new Set<Id>();
                    for (School_PFS_Assignment__c schoolPfsAssignment : Trigger.new) {
                        if (schoolPfsAssignment.Revision_EFC_Calc_Status__c == EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE) {
                            schoolPfsAssignmentIdsToRecalc.add(schoolPfsAssignment.Id);
                        }
                    }
                     
                    if (!schoolPfsAssignmentIdsToRecalc.isEmpty()) {
                        EfcCalculatorAction.calculateRevisionEfc(schoolPfsAssignmentIdsToRecalc, true);
                    }
                }
            }

            // SFP-1024 Zach Field made it so that this code only executes if we have not enabled the "Allow Recursive Folder Updates" toggle.
            // This section just preserves the original functionality allowing us to revert our changes without having to deploy anything.
            if (Trigger.isUpdate && !FeatureToggles.isEnabled('Allow_Recursive_Folder_Updates__c')) {
                SchoolPFSAction action = new SchoolPFSAction(Trigger.NewMap, Trigger.OldMap);
                if (action.buildIDMapFromUpdate()) {
                    action.updateFolder();
                }
            }

            /*
             * Spec-044 Document Management - Required Document Processing; Req# R-312
             *  Trigger on School PFS Assignment to keep School Document Assignment in synch with required documents for the school.
             *  When a School PFS Assignment record is inserted, a School Document Assignment record is created for each Required Document of the school.
             *
             * WH, Exponent Partners, 2013
             */
            if (Trigger.isInsert) {
                
                //List<School_PFS_Assignment__c> spfsaList = new List<School_PFS_Assignment__c>();
                Set<Id> spfsaIdSet = new Set<Id>();
                
                for (School_PFS_Assignment__c spfsa : Trigger.new) {
                    if (spfsa.Academic_Year_Picklist__c != null) {
                        //spfsaList.add(spfsa);
                        spfsaIdSet.add(spfsa.Id);
                    }
                }
                
                //if (!spfsaList.isEmpty()) {
                if (!spfsaIdSet.isEmpty()) {
                    // future method in case we need it
                    //SchoolDocumentAssignmentAction.createSchoolDocAssignmentsFromSPAIdsFuture(spfsaIdSet);
                    // return a queried list of these SPAs that has the Applicant__r.PFS__c field for use in SchoolBizFarmAssignmentAction.createChildSbizFarmAs
                    List<School_PFS_Assignment__c> queriedSPAList = SchoolDocumentAssignmentAction.createSchoolDocAssignments(spfsaIdSet);
                    // NAIS-2501 [DP] 08.17.2015 create School Biz Farm Assignment records
                    SchoolBizFarmAssignmentAction.createChildSbizFarmAs(queriedSPAList);
                }
                
                /*
                 * NAIS-786 Roll-up fields from School PFS Assignment to Folder; Trigger on School PFS Assignment Write the folder
                 * LL, Exponent Partners, 2013
                 */
                // [CH] NAIS-2442 Moved to consolidate
                SchoolPFSAction action = new SchoolPFSAction(Trigger.NewMap);
            
                if (action.buildIDMapFromInsert()) {
                    action.updateFolder();
                }
                //action.updatePFSNumberOnStudentFolder(); //NAIS-2098
            }
        }
        
        /* [SL] NAIS-1031: Re-evaluate application fee when schools are added/removed */
        Set<Id> applicantIdsToEvalApplicationFee = new Set<Id>();
        if (Trigger.isInsert) {
            for (School_PFS_Assignment__c spa : Trigger.new) {
                if ((spa.PFS_Status__c == 'Application Submitted') 
                        && (spa.Payment_Status__c != 'Paid in Full')
                        && (spa.Withdrawn__c != 'Yes')
                        && (spa.Applicant__c != null)) {
                    applicantIdsToEvalApplicationFee.add(spa.Applicant__c);
                }
            }
        }
        else if (Trigger.isUpdate) {            
            for (School_PFS_Assignment__c spa : Trigger.new) {
                if ((spa.PFS_Status__c == 'Application Submitted') 
                        && (spa.Payment_Status__c != 'Paid in Full')
                        && (spa.Withdrawn__c != Trigger.oldMap.get(spa.Id).Withdrawn__c)
                        && (spa.Applicant__c != null)) {        
                    applicantIdsToEvalApplicationFee.add(spa.Applicant__c);
                }
            }

            // SFP-1024 Zach Field put this here.
            // We only want this to execute if the toggle is checked. If it is not checked, then we will handle folder updates the way we did before SFP-1024 on L
            if (FeatureToggles.isEnabled('Allow_Recursive_Folder_Updates__c')) {
                // This was originally on L181 but it was only executing after the first pass which prevented the
                // SchoolPFSAction from seeing the updates to the SPA from the EFC calcs.
                SchoolPFSAction action = new SchoolPFSAction(Trigger.NewMap, Trigger.OldMap);
                if (action.buildIDMapFromUpdate()) {
                    action.updateFolder();
                }
            }
        }
        else if (Trigger.isDelete) {
            
            SchoolPFSAssignments.newInstance( Trigger.oldMap.values() ).handleAfterDelete();
        }
        if (!applicantIdsToEvalApplicationFee.isEmpty()) {
            FinanceAction.updateSaleTransactionForApplicantIds(applicantIdsToEvalApplicationFee, false);
        }
        
        /* [SL] End of trigger execution */
        SchoolSetWeightedPFS.schoolPFSActionTriggerControl.endTrigger();
    }
}