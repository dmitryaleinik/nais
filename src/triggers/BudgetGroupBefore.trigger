/**
* @BudgetGroupBefore.Trigger
* @date 05.06.2015
* @author  DP for Exponent Partners
* @description This trigger (before delete) prevents deletion of Budget Groups from which Awards have already been allocated. 
*   It looks at the "Total_Allocated__c" field to determine if awards have been allocated.
*/


/*
 * Prevent budget groups that have award amounts from being deleted
 */
trigger BudgetGroupBefore on Budget_Group__c (before delete) {
	for (Budget_Group__c bg : Trigger.old) {
		if (bg.Total_Allocated__c != null && bg.Total_Allocated__c != 0) {
			bg.addError('Awards have been made from this Budget Group, so it can not be deleted.');
		}
	}
}