trigger SSSConstantsBefore on SSS_Constants__c (before insert, before update) {

	// [CH] NAIS-1885 Make the Each Additional field at total of two components
	for(SSS_Constants__c sss : trigger.new){
		if(sss.IPA_Housing_For_Each_Additional__c != null && 
			sss.IPA_Other_For_Each_Additional__c != null && 
			sss.IPA_For_Each_Additional__c != sss.IPA_Housing_For_Each_Additional__c + sss.IPA_Other_For_Each_Additional__c)	
		{
			sss.IPA_For_Each_Additional__c = sss.IPA_Housing_For_Each_Additional__c + sss.IPA_Other_For_Each_Additional__c;
		}
	}

}