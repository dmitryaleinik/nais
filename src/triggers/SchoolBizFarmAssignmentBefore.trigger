/**
 * Trigger: SchoolBizFarmAssignmentBefore
 *
 * Copyright (C) 2015 Exponent Partners
 *
 * Purpose: This before trigger kicks off the SchoolSSSToRevisionSyncAction, which writes all of the "orig" fields to their
 * Revision counterparts if values haven't already been revised.
 *
 * NAIS-2529
 * Need to sync orig fields to revision in SBFA before trigger
 * Need a trigger that pushes values from "Orig" fields to un-revised revision fields on SBFA (before trigger?)
 * Previous code around SPA fields needs to be removed (SchoolSSSToRevisionSyncAction.syncOrigToRevisionFields). 
 *
 * Where Referenced:
 *   
 *
 * Change History:
 *
 * Developer         Date                          Description
 * ---------------------------------------------------------------------------------------
 * Drew Piston      2015.08.17           Initial Development
 *
 * 
 */


trigger SchoolBizFarmAssignmentBefore on School_Biz_Farm_Assignment__c (before update, before insert) {


    // ability to deactivate trigger from hierarchical custom setting
    public static Boolean doTrigger = true;
    try {
        TriggerDeactiviation__c trigDeactivate = TriggerDeactiviation__c.getInstance();
        if (trigDeactivate != null && trigDeactivate.Disable_Triggers__c){
            doTrigger = false;
            System.debug('TRIGGER DEACTIVATED via custom settings');
        }
    } catch (Exception e) {
        System.debug('Error getting custom setting: ' + e);
    }

    if(doTrigger){

        // NAIS-2529 [DP] 08.17.2015
        // Need to sync orig fields to revision in SBFA before trigger
        // Need a trigger that pushes values from "Orig" fields to un-revised revision fields on SBFA (before trigger?)
        // Previous code around SPA fields needs to be removed (SchoolSSSToRevisionSyncAction.syncOrigToRevisionFields).
        if (Trigger.isInsert) {
            for (School_Biz_Farm_Assignment__c sbfa : trigger.new) {
                SchoolSSSToRevisionSyncAction.syncOrigToRevisionFields(null, sbfa);
            }
        }
        else if (Trigger.isUpdate) {
            for (School_Biz_Farm_Assignment__c sbfa : trigger.new) {
                SchoolSSSToRevisionSyncAction.syncOrigToRevisionFields(Trigger.oldMap.get(sbfa.Id), sbfa);
            }
        }
    }
}