@isTest
private class PFSOverrideExtTest {

    private static PFS__c pfs1;
    private static User schoolPortalUser;

    private static void createTestData() {
        Account family = AccountTestData.Instance.asFamily().create();
        Account school = AccountTestData.Instance.asSchool().create();
        insert new List<Account>{family, school};

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;

        Contact parentA = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(school.Id).create();
        Contact student1 = ContactTestData.Instance
            .asStudent()
            .forAccount(family.Id).create();
        insert new List<Contact> {schoolStaff, parentA, student1};

        User thisUser = UserSelector.newInstance().selectById(new Set<Id>{UserInfo.getUserId()})[0];
        System.runAs(thisUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);
        String pfsStatusUnpaid = 'Unpaid';
        String folderSource = 'PFS';

        System.runAs(schoolPortalUser){
            pfs1 = PfsTestData.Instance
                .asSubmitted()
                .forPaymentStatus(pfsStatusUnpaid)
                .forParentA(parentA.Id)
                .forOriginalSubmissionDate(System.today().addDays(-3))
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<PFS__c> {pfs1});

            Applicant__c applicant1A = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student1.Id).create();
            Dml.WithoutSharing.insertObjects(new List<Applicant__c> {applicant1A});

            Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
                .forFolderSource(folderSource)
                .forStudentId(student1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<Student_Folder__c> {studentFolder1});

            String fifthGrade = '5';
            School_PFS_Assignment__c spfsa1 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1A.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> {spfsa1});
        }

        // Share records to school staff
        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);
    }

    @isTest
    private static void testNonPortal() {
        createTestData();
        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(pfs1);

        PageReference testPR = Page.PFSOverride;
        testPR.getParameters().put('id', pfs1.Id);
        Test.setCurrentPage(testPR);

        PFSOverrideExt controller = new PFSOverrideExt(stdController);
        PageReference redirectPR = controller.redirect();

        System.assert(redirectPR.getUrl().contains('nooverride'), 'Standard user should be redirected to no override page with PFS Id');
        System.assert(redirectPR.getUrl().contains(pfs1.Id), 'Standard user should be redirected to no override page with PFS Id');
    }
    
    @isTest
    private static void testSchoolPortal() {
        createTestData();
        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(pfs1);

        PageReference testPR = Page.PFSOverride;
        testPR.getParameters().put('id', pfs1.Id);
        Test.setCurrentPage(testPR);

        System.runAs(schoolPortalUser){
            PFSOverrideExt controller = new PFSOverrideExt(stdController);
            PageReference redirectPR = controller.redirect();

            System.assert(!redirectPR.getUrl().contains('nooverride'), 'Standard user should NOT Beredirected to no override page with PFS Id');
            System.assert(!redirectPR.getUrl().contains(pfs1.Id), 'Standard user should NOT be redirected to no override page with PFS Id');
            System.assert(redirectPR.getUrl().contains('summary'), 'Portal user should be redirected PFS Summary');
        }
    }
}