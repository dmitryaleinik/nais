/**
 * StudentFolderDataAccessServiceTest.cls
 *
 * @description: Test class for StudentFolderDataAccessService using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
private class StudentFolderDataAccessServiceTest {
    
    /* test data setup */
    @testSetup 
    static void setupTestData() {
        MassEmailSendTestDataFactory.createEmailTemplate();
        
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
        }
    }
    
    /* negative test cases */

    // Test getStudentFolderById with null param
    @isTest 
    static void getStudentFolderById_nullParam_nullResult() {
        
        // Arrange
        Student_Folder__c expectedResult;

        // Act
        Test.startTest();
            expectedResult = new StudentFolderDataAccessService().getStudentFolderById( null);
        Test.stopTest();

        // Assert
        System.assertEquals( null, expectedResult);
    }

    // Test getStudentFolderEmailEventByIdSet with null params
    @isTest 
    static void getStudentFolderEmailEventByIdSet_nullParams_nullResult() {
        
        // Arrange
        List<Student_Folder__c> expectedResultList;

        // Act
        Test.startTest();
            expectedResultList = new StudentFolderDataAccessService().getStudentFolderEmailEventByIdSet( null, null);
        Test.stopTest();

        // Assert
        System.assertEquals( null, expectedResultList);
    }
    
    
    /* positive test cases */
    
    // Test getStudentFolderById with valid param
    @isTest 
    static void getStudentFolderById_validParam_validResult() {
        
        // Arrange
        Student_Folder__c expectedResult = [select Id from Student_Folder__c limit 1];

        // Act
        Test.startTest();
            expectedResult = new StudentFolderDataAccessService().getStudentFolderById( expectedResult.Id);
        Test.stopTest();

        // Assert
        System.assertNotEquals( null, expectedResult);
    }

    // Test getStudentFolderEmailEventByIdSet with valid params
    @isTest 
    static void getStudentFolderEmailEventByIdSet_validParams_validResult() {
        
        // Arrange
        List<Student_Folder__c> expectedResultList = [select Id from Student_Folder__c];
        Set<String> idSet = new Set<String>();
        for ( Student_Folder__c sF : expectedResultList) {

            idSet.add( sF.Id);
        }
        expectedResultList = null;

        // Act
        Test.startTest();
            expectedResultList = new StudentFolderDataAccessService().getStudentFolderEmailEventByIdSet( idSet, true);
        Test.stopTest();

        // Assert
        System.assertNotEquals( null, expectedResultList);
    }

}