@isTest
private class ApplicationFeeSettingsServiceTest {
    
    @isTest
    private static void testGetMeansBasedFeeWaiverAccountId() {
        Account mbfwSchool = 
            AccountTestData.Instance.forRecordTypeId(RecordTypes.meansBasedFeeWaiverAccountTypeId).DefaultAccount;
        Account standardSchool = 
            AccountTestData.Instance.forRecordTypeId(RecordTypes.schoolAccountTypeId).insertAccount();

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        Application_Fee_Settings__c applicationFeeSettings = ApplicationFeeSettingsTestData.Instance
            .asCurrentYear().insertApplicationFeeSettings();

        Test.startTest();
            Id returnedFromServiceMbfwSchoolId = ApplicationFeeSettingsService.Instance.getMeansBasedFeeWaiverAccountId();
            System.assertEquals(mbfwSchool.Id, returnedFromServiceMbfwSchoolId);
        Test.stopTest();
    }
}