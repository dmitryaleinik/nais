/*
 * Spec-134 School Portal - Multiple User Security; Req# R-493
 *  Scheduled class to process School PFS Assignment records to share all PFS and Student Folder records relevant to the school with the School users 
 *  for each school they are affiliated with. 
 *
 * WH, Exponent Partners, 2013
 */
global class SharingScheduler implements Schedulable {
    
    global void execute(SchedulableContext sc) {
        /*
         * NAIS-1617 New Process for BulkSharing
         */
        
         /* [CH] NAIS-1766 This should not be needed any more now that Users are added as members to Public Groups synchronously
        // batch process all related school pfs assignments to share records with new school staff
        Database.executeBatch(new SchoolStaffNewUserShareBatch());
        */

        /*
         * Spec-134 School Portal - Multiple User Security; Req# R-493
         */
        
        // batch process new school pfs assignments to share records with all related school staff
        Database.executeBatch(new SchoolStaffShareBatch(), 20);
    }
    
}