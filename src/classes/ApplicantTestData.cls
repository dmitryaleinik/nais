/**
 * @description This class is used to create Applicant records for unit tests.
 */
@isTest
public class ApplicantTestData extends SObjectTestData {
    @testVisible private static final String LAST_NAME = 'Applicant Last Name';

    /**
     * @description Get the default values for the Applicant__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Applicant__c.Contact__c => ContactTestData.Instance.DefaultContact.Id,
                Applicant__c.PFS__c => PfsTestData.Instance.DefaultPfs.Id,
                Applicant__c.Last_Name__c => LAST_NAME
        };
    }

    /**
     * @description Set the Contact__c field on the current Applicant record.
     * @param contactId The Contact Id to set to the Contact field of the
     *             current Pfs record.
     * @return The current instance of ApplicantTestData.
     */
    public ApplicantTestData forContactId(Id contactId) {
        return (ApplicantTestData) with(Applicant__c.Contact__c, contactId);
    }

    /**
     * @description Set the Pfs__c field on the current Applicant record.
     * @param pfsId The Id of the PFS record to set on the Applicant.
     * @return The current working instance of ApplicantTestData.
     */
    public ApplicantTestData forPfsId(Id pfsId) {
        return (ApplicantTestData) with(Applicant__c.PFS__c, pfsId);
    }

    /**
     * @description Set the First_Name__c field on the current Applicant record.
     * @param studentFirstName The First Name of the Student record to set on the Applicant.
     * @return The current working instance of ApplicantTestData.
     */
    public ApplicantTestData forStudentFirstName(String studentFirstName) {
        return (ApplicantTestData) with(Applicant__c.First_Name__c, studentFirstName);
    }

    /**
     * @description Set the Last_Name__c field on the current Applicant record.
     * @param studentLastName The Last Name of the Student record to set on the Applicant.
     * @return The current working instance of ApplicantTestData.
     */
    public ApplicantTestData forStudentLastName(String studentLastName) {
        return (ApplicantTestData) with(Applicant__c.Last_Name__c, studentLastName);
    }

    /**
     * @description Set the Birthdate_new__c field on the current Applicant record.
     * @param studentBirthdate The Birthdate of the Student record to set on the Applicant.
     * @return The current working instance of ApplicantTestData.
     */
    public ApplicantTestData forStudentBirthdate(Date studentBirthdate) {
        return (ApplicantTestData) with(Applicant__c.Birthdate_new__c, studentBirthdate);
    }

    /**
     * @description Set the Grade_in_Entry_Year__c field on the current Applicant record.
     * @param gradeInEntryYear The Grade to set on the Applicant.
     * @return The current working instance of ApplicantTestData.
     */
    public ApplicantTestData forGradeInEntryYear(String gradeInEntryYear) {
        return (ApplicantTestData) with(Applicant__c.Grade_in_Entry_Year__c, gradeInEntryYear);
    }

    /**
     * @description Set the Does_Applicant_Own_Assets__c field on the current Applicant record.
     * @param doesApplicantOwnAssets The value of the picklist of Does Applicant Own Assets? to set on the Applicant.
     * @return The current working instance of ApplicantTestData.
     */
    public ApplicantTestData forDoesApplicantOwnAssets(String doesApplicantOwnAssets)
    {
        return (ApplicantTestData) with(Applicant__c.Does_Applicant_Own_Assets__c, doesApplicantOwnAssets);
    }

    /**
     * @description Set the Assets_Total_Value__c field on the current Applicant record.
     * @param assetsTotalValue The Assets Total Value to set on the Applicant.
     * @return The current working instance of ApplicantTestData.
     */
    public ApplicantTestData forAssetsTotalValue(Integer assetsTotalValue)
    {
        return (ApplicantTestData) with(Applicant__c.Assets_Total_Value__c, assetsTotalValue);
    }

    /**
     * @description Set the RavennaId__c field on the current Applicant record.
     * @param ravennaId The ravenna Id to set on the Applicant.
     * @return The current working instance of ApplicantTestData.
     */
    public ApplicantTestData withRavennaId(String ravennaId) {
        return (ApplicantTestData) with(Applicant__c.RavennaId__c, ravennaId);
    }

    /**
     * @description Insert the current working Applicant__c record.
     * @return The currently operated upon Applicant__c record.
     */
    public Applicant__c insertApplicant() {
        return (Applicant__c)insertRecord();
    }

    /**
     * @description Create the current working Applicant record without resetting
     *             the stored values in this instance of ApplicantTestData.
     * @return A non-inserted Applicant__c record using the currently stored field
     *             values.
     */
    public Applicant__c createApplicantWithoutReset() {
        return (Applicant__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Applicant__c record.
     * @return The currently operated upon Applicant__c record.
     */
    public Applicant__c create() {
        return (Applicant__c)super.buildWithReset();
    }

    /**
     * @description The default Applicant__c record.
     */
    public Applicant__c DefaultApplicant {
        get {
            if (DefaultApplicant == null) {
                DefaultApplicant = createApplicantWithoutReset();
                insert DefaultApplicant;
            }
            return DefaultApplicant;
        }
        private set;
    }

    /**
     * @description Get the Applicant__c SObjectType.
     * @return The Applicant__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Applicant__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static ApplicantTestData Instance {
        get {
            if (Instance == null) {
                Instance = new ApplicantTestData();
            }
            return Instance;
        }
        private set;
    }

    private ApplicantTestData() { }
}