public class FamilyMonthlyIncomeAndExpensesController
{
    private FamilyAppCompController controller;
    public Boolean showInputAsLabel{get;set;}
    
    public FamilyMonthlyIncomeAndExpensesController(FamilyAppCompController c)
    {
        this.controller = c;
    }
    
    private Decimal parse(Decimal value)
    {
        return value==null?0:value;
    }//End:parse
    
    public String getAlimonyCurrent()
    {
        if(this.controller.pfs!=null && this.controller.pfs.Alimony_Current__c>0){
            return this.formatCurrency(
                parse(this.controller.pfs.Alimony_Current__c/12));
        }
        return '0';
    }//End:getAlimonyCurrent
    
    public String getChildSupportReceivedCurrent()
    {
        if(this.controller.pfs!=null && this.controller.pfs.Child_Support_Received_Current__c>0){
            return this.formatCurrency(
                parse(this.controller.pfs.Child_Support_Received_Current__c/12));
        }
        return '0';
    }//End:getChildSupportReceivedCurrent
    
    public String getTotalMonthlyIncome()
    {
		String s = this.formatCurrency(MonthlyIncomeAndExpensesService.Instance
		.getMonthlyIncomeTotal(this.controller.pfs));
		s = s.substring(0, s.indexOf('.') );	
		return s;
    }//End:getTotalMonthlyIncome
    
    public String getTotalMonthlySupportExpenses()
    {
        String s = this.formatCurrency(MonthlyIncomeAndExpensesService.Instance
		.getMonthlySupportTotal(this.controller.pfs));
		s = s.substring(0, s.indexOf('.') );	
		return s;
    }//End:getTotalMonthlySupportExpenses
    
    public String getTotalMonthlyHousingExpenses()
    {
        String s = this.formatCurrency(MonthlyIncomeAndExpensesService.Instance
		.getMonthlyHousingTotal(this.controller.pfs));
		s = s.substring(0, s.indexOf('.') );	
		return s;
    }//End:getTotalMonthlySupportExpenses
    
    public String getTotalMonthlyTuitionExpenses()
    {
        String s = this.formatCurrency(MonthlyIncomeAndExpensesService.Instance
		.getMonthlyTuitionTotal(this.controller.pfs));
		s = s.substring(0, s.indexOf('.') );	
		return s;            
    }//End:getTotalMonthlyTuitionExpenses
    
    public String getTotalMonthlyVehicleExpenses()
    {
        String s = this.formatCurrency(MonthlyIncomeAndExpensesService.Instance
		.getMonthlyVehicleTotal(this.controller.pfs));
		s = s.substring(0, s.indexOf('.') );	
		return s; 
    }//End:getTotalMonthlyVehicleExpenses
    
    public String getTotalMonthlyHealthcareExpenses()
    {
        String s = this.formatCurrency(MonthlyIncomeAndExpensesService.Instance
		.getMonthlyHealthcareTotal(this.controller.pfs));
		s = s.substring(0, s.indexOf('.') );	
		return s; 	
    }//End:getTotalMonthlyHealthcareExpenses
    
    public String getTotalMonthlyFoodExpenses()
    {
        String s = this.formatCurrency(MonthlyIncomeAndExpensesService.Instance
		.getMonthlyFoodTotal(this.controller.pfs));
		s = s.substring(0, s.indexOf('.') );	
		return s; 
    }//End:getTotalMonthlyFoodExpenses
    
    public String getTotalMonthlyMiscellaneousExpenses()
    { 	
		String s = this.formatCurrency(MonthlyIncomeAndExpensesService.Instance
		.getMonthlyMiscellaneousTotal(this.controller.pfs));
		s = s.substring(0, s.indexOf('.') );	
		return s;
		
    }//End:getTotalMonthlyMiscellaneousExpenses
    
    public String getTotalMonthlyDebtAndPayments()
    {
		String s = this.formatCurrency(MonthlyIncomeAndExpensesService.Instance
		.getMonthlyDebtPaymentsTotal(this.controller.pfs));
		s = s.substring(0, s.indexOf('.') );	
		return s;
    }//End:getMonthlyDebtPaymentsTotal
    
    public Decimal getMonthlyNetIncome() {
        
        return MonthlyIncomeAndExpensesService.Instance
            .getMonthlyNetIncome(this.controller.pfs);
            
    }//End:getMonthlyNetIncome
    
    private String formatCurrency(Decimal value)
    {
        return (value!=null && value>0?String.ValueOf(value.setScale(2)):'0');
    }//End:formatCurrency
}