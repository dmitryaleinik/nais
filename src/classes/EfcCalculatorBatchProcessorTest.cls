@isTest
private class EfcCalculatorBatchProcessorTest {
    
    private static User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    private static Account school;
    private static Academic_Year__c currentAcademicYear, previousAcademicYear;
    private static List<Academic_Year__c> academicYears;
    private static PFS__c pfs1;
    private static School_PFS_Assignment__c spfsa2;
    private static String filingStatus = 'Single';
    private static Integer parentASalaryWages = 160000;
    private static Integer parentBSalaryWages = 40000;
    private static Integer numChildreInTuitionSchools = 2;

    private static void createTestData() {
        Account family = AccountTestData.Instance.asFamily().create();
        school = AccountTestData.Instance.asSchool()
            .forSSSSubscriberStatus(GlobalVariables.getActiveSSSStatusForTest())
            .forSSSSchoolCode('11111')
            .forPortalVersion(SubscriptionTypes.BASIC_TYPE).create();
        insert new List<Account>{family, school};
        
        Subscription__c sub1 = TestUtils.createSubscription(school.Id, system.today().addDays(-14), system.today().addYears(1), 'no', 'Basic', false);
        insert new List<Subscription__c> {sub1};

        Contact parentA = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact parentB = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(school.Id).create();
        Contact student1 = ContactTestData.Instance
            .asStudent()
            .forAccount(family.Id).create();
        Contact student2 = ContactTestData.Instance
            .asStudent()
            .forAccount(family.Id).create();
        insert new List<Contact> {schoolStaff, parentA, parentB, student1, student2};

        User schoolPortalUser;
        System.runAs(currentUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }
        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);

        academicYears = GlobalVariables.getAllAcademicYears();
        currentAcademicYear = GlobalVariables.getCurrentAcademicYear();
        previousAcademicYear = GlobalVariables.getPreviousAcademicYear();

        createTables();

        // Create SSSConstants records for the Academic Years
        SSS_Constants__c sssConst1 = TestUtils.createSSSConstants(currentAcademicYear.Id, false);
        SSS_Constants__c sssConst2 = TestUtils.createSSSConstants(previousAcademicYear.Id, false);
        Database.insert(new List<SSS_Constants__c>{sssConst1, sssConst2});

        System.runAs(schoolPortalUser){
            pfs1 = createPfs(parentA.Id, currentAcademicYear.Name);
            PFS__c pfs2 = createPfs(parentB.Id, currentAcademicYear.Name);
            PFS__c pfs1Past = createPfs(parentA.Id, previousAcademicYear.Name);
                pfs1Past.Original_Submission_Date__c = System.today().addDays(-3).addYears(-1);
                pfs1Past.Payment_Status__c = 'Paid';
            Dml.WithoutSharing.insertObjects(new List<PFS__c> {pfs1, pfs2, pfs1Past});

            Applicant__c applicant1A = createApplicant(pfs1.Id, student1.Id);
            Applicant__c applicant1B = createApplicant(pfs2.Id, student1.Id);
            Applicant__c applicant2A = createApplicant(pfs1.Id, student2.Id);
            Applicant__c applicant1Past = createApplicant(pfs1Past.Id, student1.Id);
            Dml.WithoutSharing.insertObjects(new List<Applicant__c> {applicant1A, applicant1B, applicant2A, applicant1PAST});

            Student_Folder__c studentFolder1 = createStudentFolder(student1.Id, currentAcademicYear.Name);
            Student_Folder__c studentFolder2 = createStudentFolder(student2.Id, currentAcademicYear.Name);
            Student_Folder__c studentFolder1Past = createStudentFolder(student1.Id, previousAcademicYear.Name);
            Dml.WithoutSharing.insertObjects(new List<Student_Folder__c> {studentFolder1, studentFolder2, studentFolder1Past});

            School_PFS_Assignment__c spfsa1 = createSpfsa(applicant1A.Id, studentFolder1.Id, currentAcademicYear.Name);
            spfsa2 = createSpfsa(applicant1B.Id, studentFolder1.Id, currentAcademicYear.Name);
            School_PFS_Assignment__c spfsa3 = createSpfsa(applicant2A.Id, studentFolder2.Id, currentAcademicYear.Name);
            School_PFS_Assignment__c spfsa1Past = createSpfsa(applicant1Past.Id, studentFolder1Past.Id, previousAcademicYear.Name);
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> {spfsa1, spfsa2 ,spfsa3, spfsa1Past});
        }

        // Share records to school staff
        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);
    }

    private static Pfs__c createPfs(Id parentId, String academicYearName) {
        String pfsStatusUnpaid = 'Unpaid';

        Pfs__c pfs = PfsTestData.Instance
            .asSubmitted()
            .forPaymentStatus(pfsStatusUnpaid)
            .forParentA(parentId)
            .forOriginalSubmissionDate(System.today().addDays(-3))
            .forFillingStatus(filingStatus)
            .forSalaryWagesParentA(parentASalaryWages)
            .forSalaryWagesParentB(parentBSalaryWages)
            .forNumChildreInTuitionSchools(numChildreInTuitionSchools)
            .forAcademicYearPicklist(academicYearName).create();

        return pfs;
    }

    private static Applicant__c createApplicant(Id pfsId, Id studentId) {
        String applicantGradeInENtryYear = '6';

        Applicant__c applicant = ApplicantTestData.Instance
            .forPfsId(pfsId)
            .forGradeInEntryYear(applicantGradeInEntryYear)
            .forContactId(studentId).create();

        return applicant;
    }

    private static Student_Folder__c createStudentFolder(Id studentId, String academicYearName) {
        String folderSource = 'PFS';

        Student_Folder__c studentFolder = StudentFolderTestData.Instance
            .forFolderSource(folderSource)
            .forStudentId(studentId)
            .forAcademicYearPicklist(academicYearName).create();

        return studentFolder;
    }

    private static School_PFS_Assignment__c createSpfsa(Id applicantId, Id studentFolderId, String academicYearName) {
        String fifthGrade = '5';
        String origFamilyStatus = '1 Parent';
        Integer familySize = 4;

        School_PFS_Assignment__c spfsa1 = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicantId)
            .forStudentFolderId(studentFolderId)
            .forAcademicYearPicklist(academicYearName)
            .forGradeApplying(fifthGrade)
            .forFillingStatus(filingStatus)
            .forSalaryWagesParentA(parentASalaryWages)
            .forSalaryWagesParentB(parentBSalaryWages)
            .forNumChildreInTuitionSchools(numChildreInTuitionSchools)
            .forOrigFamilyStatus(origFamilyStatus)
            .forFamilySize(familySize)
            .forSchoolId(school.Id).create();

        return spfsa1;
    }

    private static void createTables() {
        List<Federal_Tax_Table__c> fedTaxTables = new List<Federal_Tax_Table__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            fedTaxTables.add(TestUtils.createFederalTaxTable(academicYears[i].Id, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_SINGLE, false));
        }
        Database.insert(fedTaxTables);

        List<State_Tax_Table__c> stateTaxTables = new List<State_Tax_Table__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            stateTaxTables.add(TestUtils.createStateTaxTable(academicYears[i].Id, false));
        }
        Database.insert(stateTaxTables);

        List<Employment_Allowance__c> emplAllowances = new List<Employment_Allowance__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            emplAllowances.add(TestUtils.createEmploymentAllowance(academicYears[i].Id, false));
        }
        Database.insert(emplAllowances);

        List<Net_Worth_of_Business_Farm__c> busFarm = new List<Net_Worth_of_Business_Farm__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            busFarm.add(TestUtils.createBusinessFarm(academicYears[i].Id, false));
        }
        Database.insert(busFarm);

        List<Retirement_Allowance__c> retAllowances = new List<Retirement_Allowance__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            retAllowances.add(TestUtils.createRetirementAllowance(academicYears[i].Id, false));
        }
        Database.insert(retAllowances);

        List<Asset_Progressivity__c> assetProgs = new List<Asset_Progressivity__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            assetProgs.add(TestUtils.createAssetProgressivity(academicYears[i].Id, false));
        }
        Database.insert(assetProgs);

        List<Income_Protection_Allowance__c> incProAllowances = new List<Income_Protection_Allowance__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            incProAllowances.add(TestUtils.createIncomeProtectionAllowance(academicYears[i].Id, false));
        }
        Database.insert(incProAllowances);

        List<Expected_Contribution_Rate__c> expContRates = new List<Expected_Contribution_Rate__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            expContRates.add(TestUtils.createExpectedContributionRate(academicYears[i].Id, false));
        }
        Database.insert(expContRates);

        List<Housing_Index_Multiplier__c> houseIndexes = new List<Housing_Index_Multiplier__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            houseIndexes.add(TestUtils.createHousingIndexMultiplier(academicYears[i].Id, false));
        }
        Database.insert(houseIndexes);
    }


    @isTest
    private static void testEfcCalculatorBatchProcessor() {
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
        
        // set up test data
        createTestData();
        
        // set pfs and school pfs assignment calc status to Recalculate
        pfs1.SSS_EFC_Calc_Status__c = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE_BATCH;
        spfsa2.Revision_EFC_Calc_Status__c = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE_BATCH;
        update pfs1;
        update spfsa2;

        List<Family_Document__c> fdList = [Select Id, Academic_Year_Picklist__c from Family_Document__c];

        // execute the Batch
        Test.startTest();
            EfcCalculatorBatchProcessor.doExecuteBatch();
        Test.stopTest();
        
        // reload the PFS
        PFS__c pfs = [SELECT SSS_EFC_Calc_Status__c FROM PFS__c WHERE Id = :pfs1.Id];
        
        // verify the EFC was recalculated
        System.assertEquals(EfcPicklistValues.EFC_CALC_STATUS_CURRENT, pfs.SSS_EFC_Calc_Status__c);
        
        // reload the School PFS Assignment
        School_PFS_Assignment__c spa = [SELECT Revision_EFC_Calc_Status__c FROM School_PFS_Assignment__c WHERE Id = :spfsa2.Id];
        
        // verify the EFC was recalculated
        System.assertEquals(EfcPicklistValues.EFC_CALC_STATUS_CURRENT, spa.Revision_EFC_Calc_Status__c);
    }

    @isTest
    private static void testEfcCalculatorBatchProcessorWithType() {
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
        
        // set up test data
        createTestData();
        
        // set pfs and school pfs assignment calc status to Recalculate
        pfs1.SSS_EFC_Calc_Status__c = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE_BATCH;
        spfsa2.Revision_EFC_Calc_Status__c = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE_BATCH;
        update pfs1;
        update spfsa2;
        
        // execute the Batch
        Test.startTest();
            EfcCalculatorBatchProcessor.doExecuteBatch(EfcCalculatorBatchProcessor.CalcType.SSS);
            EfcCalculatorBatchProcessor.doExecuteBatch(EfcCalculatorBatchProcessor.CalcType.Revision);
        Test.stopTest();
        
        // reload the PFS
        PFS__c pfs = [SELECT SSS_EFC_Calc_Status__c FROM PFS__c WHERE Id = :pfs1.Id];
        
        // verify the EFC was recalculated
        System.assertEquals(EfcPicklistValues.EFC_CALC_STATUS_CURRENT, pfs.SSS_EFC_Calc_Status__c);
        
        // reload the School PFS Assignment
        School_PFS_Assignment__c spa = [SELECT Revision_EFC_Calc_Status__c FROM School_PFS_Assignment__c WHERE Id = :spfsa2.Id];
        
        // verify the EFC was recalculated
        System.assertEquals(EfcPicklistValues.EFC_CALC_STATUS_CURRENT, spa.Revision_EFC_Calc_Status__c);
    }
    
    @isTest
    private static void testEfcCalculatorBatchProcessorWithCustomQuery() {
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
        
        // set up test data
        createTestData();
        
        // reload the PFS
        PFS__c prerunpfs = [SELECT SSS_EFC_Calc_Status__c FROM PFS__c WHERE Id = :pfs1.Id];
        
        // verify the EFC was recalculated
        System.assertNotEquals(EfcPicklistValues.EFC_CALC_STATUS_ERROR, prerunpfs.SSS_EFC_Calc_Status__c);

        //System.assertEquals(null, [Select Id, Academic_Year_Picklist__c FROM Family_Document__c]);

        // set pfs and school pfs assignment calc status to Recalculate
        pfs1.SSS_EFC_Calc_Status__c = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE_BATCH;
        spfsa2.Revision_EFC_Calc_Status__c = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE_BATCH;
        update pfs1;
        update spfsa2;
        
        // execute the Batch
        Test.startTest();
            EfcCalculatorBatchProcessor.doExecuteBatch(
                EfcCalculatorBatchProcessor.CalcType.SSS, 'SELECT Id FROM PFS__c WHERE Id=\'' + pfs1.Id + '\'');
            EfcCalculatorBatchProcessor.doExecuteBatch(
                EfcCalculatorBatchProcessor.CalcType.Revision, 'SELECT Id FROM School_PFS_Assignment__c WHERE Id=\'' + spfsa2.Id + '\'');
        Test.stopTest();
        
        // reload the PFS
        PFS__c pfs = [SELECT SSS_EFC_Calc_Status__c FROM PFS__c WHERE Id = :pfs1.Id];
        
        // verify the EFC was recalculated
        System.assertEquals(EfcPicklistValues.EFC_CALC_STATUS_CURRENT, pfs.SSS_EFC_Calc_Status__c);
        
        // reload the School PFS Assignment
        School_PFS_Assignment__c spa = [SELECT Revision_EFC_Calc_Status__c FROM School_PFS_Assignment__c WHERE Id = :spfsa2.Id];
        
        // verify the EFC was recalculated
        System.assertEquals(EfcPicklistValues.EFC_CALC_STATUS_CURRENT, spa.Revision_EFC_Calc_Status__c);
    }
}