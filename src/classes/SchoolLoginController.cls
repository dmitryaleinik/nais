/**
 * @description Controls the School Login page.
 **/
global class SchoolLoginController {
    global String username {get; set;}
    global String password {get; set;}
    global Boolean passwordReset {get; set;}
    global String errorMessage {get; set;}
    
    global SchoolLoginController() {
        passwordReset = false;
    }

    global PageReference login() {
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        return Site.login(username, password, startUrl);
    }

    global PageReference forgotPassword()
    {
        if( usernameIsActive(username) ){
            passwordReset = Site.forgotPassword(username);
            if(!passwordReset) 
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.LOGIN_GENERIC_ERROR));
        }
        return null;
    }
    
    private Boolean usernameIsActive(String username)
    {
        List<User> users = new List<User>([Select Id, isActive from User WHERE username=:username limit 1]);
        if( !users.isEmpty() && !users[0].isActive ){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.USERNAME_INACTIVE));
            return false;
        }
        return true;//Even if the username does not exists
    }
}