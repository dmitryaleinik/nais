@isTest
private class FamilyAppBizFarmSummaryControllerTest {
    
    private static List<Business_Farm__c> bizFarmList = new List<Business_Farm__c>();

    private static FamilyAppBusinessFarmSummaryController testRoller;
    private static PageReference testPR;
    private static FamilyAppMainController mainRoller;
    private static TestDataFamily tdf;

    private static void setupData(){
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);

        FamilyAppSettings__c screenSettings    = new FamilyAppSettings__c();
        screenSettings.Name = 'SelectSchools';
        screenSettings.Main_Label_Field__c = 'SchoolSelection__c';
        screenSettings.Next_Page__c = 'FamilyIncome';
        screenSettings.Status_Field__c = 'statSelectSchools__c';
        screenSettings.Sub_Label_Field__c = 'SelectSchools__c';
        insert screenSettings;
        
        tdf = new TestDataFamily();
                
        testPR = Page.FamilyApplicationMain;
        testPR.getParameters().put('Id', tdf.pfsA.Id);
        testPR.getParameters().put('screen', 'BusinessSummary');
        Test.setCurrentPage(testPR);

        bizFarmList.add(TestUtils.createBusinessFarm(tdf.pfsA.Id, 'MyBiz1', 'Business', 'Sole Proprietorship', false));
        bizFarmList.add(TestUtils.createBusinessFarm(tdf.pfsA.Id, 'MyBiz2', 'Business', 'Sole Proprietorship', false));
        bizFarmList.add(TestUtils.createBusinessFarm(tdf.pfsA.Id, 'MyBiz3', 'Farm', 'Sole Proprietorship', false));

        for (Business_Farm__c bizFarm : bizFarmList){
            bizFarm.Business_Assets_Current__c = 1; // for Business_Farm_Assets__c
            bizFarm.Business_Bldg_Mortgage_Current__c = 2; // for Business_Farm_Debts__c
            bizFarm.Business_Farm_Owner__c = 'Parent A';
            bizFarm.Business_Farm_Ownership_Percent__c = 4;
            bizFarm.Business_Net_Profit_Share_Current__c = 5;
        }
    }

    // run through
    @isTest
    private static void testInsert(){
        setupData();
        Test.startTest();
        insert bizFarmList;
        FamilyTemplateController template = new FamilyTemplateController();
        mainRoller = new FamilyAppMainController(template);

        System.runAs(tdf.familyPortalUser){
            testRoller = new FamilyAppBusinessFarmSummaryController();
            testRoller.pfs = template.pfsRecord;
            
            System.assertEquals(3, testRoller.bizFarmList.size());
            // no longer grouping
            //System.assertEquals(2, testRoller.getBizOrFarmGroupWrappers().size());
            //System.assertEquals(2, testRoller.getBizOrFarmGroupWrappers()[0].bizFarmList.size()); // businesses
            //System.assertEquals(1, testRoller.getBizOrFarmGroupWrappers()[1].bizFarmList.size()); // farms
            testRoller.theGrouping = 'entity';
            System.assertEquals(6, testRoller.getBizOrFarmGroupWrappers().size());
            System.assertEquals(2, testRoller.getBizOrFarmGroupWrappers()[0].bizFarmList.size()); // businesses - Sole Proprietorship
            System.assertEquals(0, testRoller.getBizOrFarmGroupWrappers()[1].bizFarmList.size()); // businesses - - Partnership
            System.assertEquals(0, testRoller.getBizOrFarmGroupWrappers()[2].bizFarmList.size()); // businesses - Corporation
            System.assertEquals(1, testRoller.getBizOrFarmGroupWrappers()[3].bizFarmList.size()); // farms - Sole Proprietorship
            System.assertEquals(0, testRoller.getBizOrFarmGroupWrappers()[4].bizFarmList.size()); // farms - Partnership
            System.assertEquals(0, testRoller.getBizOrFarmGroupWrappers()[5].bizFarmList.size()); // farms - Corporation
        }

        Test.stopTest();
    }    
}