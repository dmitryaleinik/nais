@isTest
private class FamilyMessagesControllerTest {

    private static User CreateData() {
        // create test data
        TestUtils.createAcademicYears();
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        
        Id portalProfileId = GlobalVariables.familyPortalProfileId;

        Account account1 = TestUtils.createAccount('individual', RecordTypes.individualAccountTypeId, 3, true);
        Contact parentA = TestUtils.createContact('Parent A', account1.Id, RecordTypes.parentContactTypeId, true);
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User u = TestUtils.createPortalUser('User 1', 'u1@test.org', 'u1', parentA.Id, portalProfileId, true, true);
        
        System.runAs(u){
            PFS__c pfs1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
            Dml.WithoutSharing.insertObjects(new List<PFS__c>{ pfs1});
        }
        TestUtils.createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_SINGLE, true);

        TestUtils.createStateTaxTable(academicYearId, true);
        
        TestUtils.createEmploymentAllowance(academicYearId, true);
        
        TestUtils.createBusinessFarm(academicYearId, true);

        TestUtils.createRetirementAllowance(academicYearId, true);
        
        TestUtils.createAssetProgressivity(academicYearId, true);
        
        TestUtils.createIncomeProtectionAllowance(academicYearId, true);

        TestUtils.createExpectedContributionRate(academicYearId, true);

        TestUtils.createHousingIndexMultiplier(academicYearId, true);
        
        return u;
    }
    
    @isTest
    private static void TestLoad()
    {
        User u = FamilyMessagesControllerTest.CreateData();
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        PFS__c pfs = [SELECT Id, Name FROM PFS__c LIMIT 1];

        Test.startTest();

        System.Runas(u) {
            PageReference pageRef = Page.FamilyMessages;
            pageRef.getParameters().put('id',pfs.Id);
            pageRef.getParameters().put('AcademicYearId',academicYearId);
            Test.setCurrentPage(pageRef);
            FamilyTemplateController template = new FamilyTemplateController();
            FamilyMessagesController controller = new FamilyMessagesController(template);
            
            System.assertNotEquals(template.Me, null);
            
            System.assert(controller.Cases.isEmpty());
        }
        
        Test.stopTest();
    }
    
    @isTest
    private static void TestCases()
    {
        User u = FamilyMessagesControllerTest.CreateData();
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        PFS__c pfs = [SELECT Id, Name FROM PFS__c LIMIT 1];

        Test.startTest();

        System.Runas(u) {
            
            Case c = new Case(subject = 'Test', description = 'Test', contactId = u.ContactId, RecordTypeId = RecordTypes.schoolParentCommunicationCaseTypeId);
            insert c;
            
            List<Case> cases = [SELECT Id, Subject, Description FROM CASE];
            System.debug('FamilyMessagesControllerTest.TestCases.cases: ' + cases);
            
            PageReference pageRef = Page.FamilyMessages;
            pageRef.getParameters().put('id',pfs.Id);
            pageRef.getParameters().put('AcademicYearId',academicYearId);
            Test.setCurrentPage(pageRef);
            FamilyTemplateController template = new FamilyTemplateController();
            system.assertEquals(true, template.pfsRecord!=null);
            FamilyMessagesController controller = new FamilyMessagesController(template);
            
            System.assertEquals(1, controller.Cases.size());
            System.assertEquals('Test', controller.Cases[0].subject);
        }
        
        Test.stopTest();
    }
    
    @isTest
    private static void TestNewSupportTicket()
    {
        User u = FamilyMessagesControllerTest.CreateData();
        PFS__c pfs = [SELECT Id, Name FROM PFS__c LIMIT 1];
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        Case c = new Case(subject = 'Test', description = 'Test', contactId = u.ContactId);
        insert c;

        Test.startTest();

        System.Runas(u) {
            PageReference pageRef = Page.FamilyMessages;
            pageRef.getParameters().put('id',pfs.Id);
            pageRef.getParameters().put('AcademicYearId',academicYearId);
            Test.setCurrentPage(pageRef);
            FamilyTemplateController template = new FamilyTemplateController();
            FamilyMessagesController controller = new FamilyMessagesController(template);
            system.assertEquals(true, pfs!=null);
            system.assertEquals(true, template!=null);
            system.assertEquals(true, template.pfsRecord!=null);
            PageReference p = controller.NewSupportTicket();
            
            System.assertNotEquals(p, null);
        }
        
        Test.stopTest();
    }
}