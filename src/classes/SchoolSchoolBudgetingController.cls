/*
 * Spec-105 School Portal - School Budgeting; R-114, R-115, R-116, R-118, R-119
 * BR, Exponent Partners, 2013
 */

public with sharing class SchoolSchoolBudgetingController implements SchoolAcademicYearSelectorInterface {

    /* Initialization */
    public SchoolSchoolBudgetingController() {

        // Due to the selector_onchange not refreshing properly, we redirect to the same page with academicyearid parameter.
        Map<String, String> parameters = ApexPages.currentPage().getParameters();
        String academicYearId = null;
        if (parameters.containsKey('academicyearid'))
             setAcademicYearId(parameters.get('academicyearid'));
           else
            myAcademicYear = GlobalVariables.getCurrentAcademicYear();

        //mySchoolId = GlobalVariables.getCurrentUser().Contact.AccountId;
        mySchoolId = GlobalVariables.getCurrentSchoolId();

        newBudgetGroup();

        if (parameters.containsKey('budgetgrpid')){
            selectedBudGrp = parameters.get('budgetgrpid');
            editLink();
            setAcademicYearId(myBudgetGroup.Academic_Year__c);
        }

        myBudgetGroups= getBudgetGroups(myAcademicYear.Id); //NAIS-2351

        reportURL = SchoolPortalSettings.Budget_Allocation_Report_ID;

        if ( reportURL == null || reportURL == '') {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Budget Allocation Report Link not defined'));
        }
    }

    /* Properties */

    public Academic_Year__c myAcademicYear { get; set; }
    public List<Budget_Group__c> myBudgetGroups { get; private set; }
    public Budget_Group__c myBudgetGroup { get; set; }

    //NAIS-2351
    public boolean isCopyPriorYear{
            get{
                isCopyPriorYear =validateDisplayCopyButton(GlobalVariables.getPreviousAcademicYear().Id);
                return isCopyPriorYear;
            }
            set;
       }
       private List<Budget_Group__c> previousBudgetGroups{get;set;}
       //End NAIS-2351
    public String recordToDelete {get; set;}

    public Id mySchoolId { get; private set; }
    public Id selectedBudGrp { get; set; }

    public Boolean bEdit { get; set; }    // Could use Id instead.
    public Boolean bOnChange { get; set; }

    public String reportURL { get; set; }

    public SchoolSchoolBudgetingController Me { get { return this; } }

    public List<BudgetGroupWrapper> getBudgetGroupWrappers(){
        List<BudgetGroupWrapper> bgwList = new List<BudgetGroupWrapper>();
        for (Budget_Group__c bg : myBudgetGroups){
            bgwList.add(new BudgetGroupWrapper(bg));
        }

        return bgwList;
    }

    /* Action Methods */
    //NAIS-2351
    public PageReference copyPreviousYear()
    {
        try{
            list<Budget_Group__c> newBG = new list<Budget_Group__c>();
            System.debug('****** previousBudgetGroups -- '+previousBudgetGroups);
            for(Budget_Group__c oldBG: previousBudgetGroups)
            {
                Budget_Group__c bg = new Budget_Group__c();
                bg.Name = oldBG.Name;
                bg.Academic_Year__c = myAcademicYear.Id;
                bg.Description__c = oldBG.Description__c;
                bg.School__c= mySchoolId;
                bg.Total_Funds__c = oldBG.Total_Funds__c;
                newBG.add(bg);
            }
            insert newBG;
            myBudgetGroups= getBudgetGroups(myAcademicYear.Id);
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Budget groups have been copied from the previous year.'));
        }
        catch(Exception e){
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'And error occurred copy the budget group to previous year.'+e));
            return null;
        }
        return null;
    }
    //End NAIS-2351
    public PageReference cancel() {

        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Cancelled.'));

        // NAIS-1186 - return to New Budget state when cancelled out of New or Edit Budget
        //if (bEdit) {
            //return editlink();
        //} else {
            newBudgetGroup();
        //}

        return null;
    }

    public PageReference savebutton() {

        if (myBudgetGroup.Total_Funds__c == null || myBudgetGroup.Total_Funds__c <= 0){
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Total Funds must be greater than zero.'));
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Saved.'));

            upsert myBudgetGroup;
            myBudgetGroups= getBudgetGroups(myAcademicYear.Id);//NAIS-2351
            newBudgetGroup();
        }
        return null;
    }

    public PageReference newbutton() {
        Budget_Group__c bg = newBudgetGroup();
        insert bg;

        myBudgetGroups.add(bg);
        return null;
    }

    public PageReference deletelink() {

        try {
            Database.Deleteresult delResult = Database.delete(recordToDelete);
        } catch (Exception e) {
            return null;
        }

        myBudgetGroups= getBudgetGroups(myAcademicYear.Id);//NAIS-2351

        if (myBudgetGroup.Id == selectedBudGrp) {
            newBudgetGroup();
        }

        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Deleted.'));

        return null;
    }

    public PageReference editlink() {

        bEdit = true;
        myBudgetGroup = [SELECT Id, Name, Academic_Year__c, Academic_Year__r.Name, Description__c, Total_Allocated__c, Total_Funds__c, Total_Remaining__c
                            FROM Budget_Group__c
                            WHERE Id = :selectedBudGrp];
        return null;
    }

    public PageReference reportlink() {

        return null;
    }

    /* Helper Methods */
    //NAIS-2351
    public boolean validateDisplayCopyButton(Id previousAcademicYearId)
    {
        boolean isPriorYearValid;
        if(previousBudgetGroups== null || previousBudgetGroups.size()==0){
            previousBudgetGroups = getBudgetGroups(previousAcademicYearId);
        }
        isPriorYearValid = (previousBudgetGroups.size()>0 && myBudgetGroups.size()==0)?true: false;
        return isPriorYearValid;
    }
    //End NAIS-2351
    public List<Budget_Group__c> getBudgetGroups(Id academicYearId) { //NAIS-2351
        List<Budget_Group__c> auxBudgetGroups; //NAIS-2351
        auxBudgetGroups = [SELECT Name, Academic_Year__c, Academic_Year__r.Name, Description__c, Total_Allocated__c, Total_Funds__c, Total_Remaining__c
                            FROM Budget_Group__c
                            WHERE Academic_Year__c = :academicYearId AND School__c = :mySchoolId]; //NAIS-2351
        return auxBudgetGroups;
    }

    public Budget_Group__c newBudgetGroup() {

        bEdit = false;
        myBudgetGroup = new Budget_Group__c(School__c = mySchoolId, Academic_Year__r = myAcademicYear, Academic_Year__c = myAcademicYear.Id);
        return myBudgetGroup;
    }

    /* SchoolAcademicYearSelectorInterface Implementation */

    public String getAcademicYearId() {

        return myAcademicYear.Id;
    }

    public void setAcademicYearId(String academicYearId) {

        myAcademicYear = [SELECT Id, Name FROM Academic_Year__c WHERE Id = :academicYearId];
    }

    public PageReference SchoolAcademicYearSelector_OnChange(Boolean saveRecord) {

        /*
         if(saveRecord){
             saveButton();
         }
         */

         PageReference pageRef = Page.SchoolSchoolBudgeting;
        pageRef.getParameters().put('academicyearid', myAcademicYear.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }

    public Class BudgetGroupWrapper{
        public Budget_Group__c budgrp {get; set;}
        public String fifteenId {get; set;}

        public budgetGroupWrapper(Budget_Group__c bgParam){
            budgrp = bgParam;
            fifteenId = String.valueOf(budgrp.Id);
            fifteenId = fifteenId.left(15);
        }

    }

}