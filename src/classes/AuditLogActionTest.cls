@isTest
private class AuditLogActionTest
{

    @isTest
    private static void testAuditFields()
    {
        // create test data
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        TestUtils.createSSSConstants(academicYear.Id, true);
        TableTestData.populateAllEfcTables(academicYear.Id);
        Account school = TestUtils.createAccount('School1', RecordTypes.schoolAccountTypeId, 5, true);
        Account family = TestUtils.createAccount('The individual', RecordTypes.individualAccountTypeId, 5, true);
        Contact student = TestUtils.createContact('Student', family.Id, RecordTypes.studentContactTypeId, true);
        Contact parent = TestUtils.createContact('Parent', family.Id, RecordTypes.parentContactTypeId, true);
        Student_Folder__c folder = TestUtils.createStudentFolder('Folder', academicYear.Id, student.Id, true);
        
        PFS__c pfs = TestUtils.createPFS('pfs', academicYear.Id, parent.Id, false);
        pfs.Salary_Wages_Parent_A__c = 10000;
        pfs.Salary_Wages_Parent_B__c = 20000;
        pfs.Additional_Comments__c = 'ABC';
        pfs.PFS_Status__c = EfcPicklistValues.PFS_STATUS_SUBMITTED;
        pfs.Payment_Status__c = EfcPicklistValues.PAYMENT_STATUS_PAID_IN_FULL;
        insert pfs;
        
        Applicant__c applicant = TestUtils.createApplicant(student.Id, pfs.Id, false);
        applicant.Assets_Total_Value__c = 10000;
        applicant.Grade_in_Entry_Year__c = '1';
        applicant.Assets_Description__c = 'ABC';
        applicant.Does_Applicant_Own_Assets__c = 'Yes';
        applicant.PFS__c = pfs.Id;
        insert applicant;
        
        School_PFS_Assignment__c pfsAssign = EfcTestData.createTestSchoolPfsAssignment(academicYear.Name, folder.Id, school.Id, applicant.Id, false);
        pfsAssign.Salary_Wages_Parent_A__c = 10000;
        pfsAssign.Salary_Wages_Parent_B__c = 20000;
        pfsAssign.Parent_A_Email__c = 'ABC@email.com';
        pfsAssign.Apply_Minimum_Income__c = 'No';
        insert pfsAssign;
        
        Test.startTest();
        
        // update PFS fields
        pfs.Salary_Wages_Parent_A__c = 30000; // audited field
        pfs.Salary_Wages_Parent_B__c = 40000; // audited field
        pfs.Additional_Comments__c = 'DEF'; // not audited field
        update pfs;
        
        // verify only the audited fields show up in the log
        String changeLog = [SELECT Change_Log__c FROM PFS__c WHERE Id=:pfs.Id].Change_Log__c;
        System.debug('PFS Change Log: ' + changeLog);
        System.assertEquals(true, changeLog.contains('Salary/Wages Parent A old:10000 new:30000'));
        System.assertEquals(true, changeLog.contains('Salary/Wages Parent B old:20000 new:40000'));
        System.assertEquals(false, changeLog.contains('Additional Comments old:ABC new:DEF'));
        
        // update Applicant fields                
        applicant.Grade_in_Entry_Year__c = '2'; // audited field
        applicant.Assets_Total_Value__c = 20000; // audited field
        applicant.Assets_Description__c = 'DEF'; // not audited field
        update applicant;        
        
        // verify only the audited fields show up in the log                
        changeLog = [SELECT Change_Log__c FROM Applicant__c WHERE Id=:applicant.Id].Change_Log__c;
        System.debug('Applicant Change Log: ' + changeLog);
        System.assertEquals(true, changeLog.contains('Grade in Entry Year old:1 new:2'));
        System.assertEquals(true, changeLog.contains('Assets Total Value old:10000 new:20000'));
        System.assertEquals(false, changeLog.contains('Assets Description old:ABC new:DEF'));        
        
        // update School PFS Assignment Fields
        pfsAssign.Salary_Wages_Parent_A__c = 30000; // audited field
        pfsAssign.Salary_Wages_Parent_B__c = 40000; // audited field
        pfsAssign.Parent_A_Email__c = 'DEF@email.com'; // not audited field
        pfsAssign.Apply_Minimum_Income__c = 'Yes'; // audited PJ field
        update pfsAssign;
        
        // verify only the audited fields show up in the log
        changeLog = [SELECT Change_Log__c FROM School_PFS_Assignment__c WHERE Id=:pfsAssign.Id].Change_Log__c;
        System.debug('School PFS Assignment Change Log: ' + changeLog);
        System.assertEquals(true, changeLog.contains('Salary/Wages Parent A old:10000 new:30000'));
        System.assertEquals(true, changeLog.contains('Salary/Wages Parent B old:20000 new:40000'));
        System.assertEquals(false, changeLog.contains('Parent A Email old:ABC@email.com new:DEF@email.com'));        
        
        // verify the PJ audit log
        System.assertEquals(true, changeLog.contains(AuditLogAction.PJ_CHANGE_ALERT));
        String pjChangeLog = [SELECT Professional_Judgment_Log__c FROM School_PFS_Assignment__c WHERE Id=:pfsAssign.Id].Professional_Judgment_Log__c;
        System.assertEquals(true, pjChangeLog.contains('Apply Minimum Income=Yes'));
                
        Test.stopTest();
    }
}