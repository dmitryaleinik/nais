/**
 * @description Query selector for Account records.
 */
public class AccountSelector extends fflib_SObjectSelector
{

    @testVisible private static final String RECORDTYPE_ID  = 'recordTypeId';
    @testVisible private static final String RECORDTYPE_NAME_PARAM  = 'recordTypeName';
    @testVisible private static final String RECORDTYPE_NAME_SCHOOL = 'School';
    @testVisible private static final String SCHOOL_IDS = 'schoolIds';
    @testVisible private static final String SCHOOL_FIELDS = 'schoolFields';
    @testVisible private static final String ANNUAL_SETTING_FIELDS = 'annualSettingFields';
    @testVisible private static final String ACADEMIC_YEAR_ID = 'academicYearId';
    @testVisible private static final String SSS_CODE_NUMBER = 'sssCodeNumber';

    public List<Account> selectWithLatestAnnualSetting(Set<Id> schoolIds, Set<String> annualSettingFields) {
        
        ArgumentNullException.throwIfNull(schoolIds, SCHOOL_IDS);
        ArgumentNullException.throwIfNull(annualSettingFields, 'annualSettingFields');

        return selectWithLatestAnnualSetting(schoolIds, annualSettingFields, 1);
    }

    public List<Account> selectWithAllAnnualSettings(Set<Id> schoolIds, Set<String> annualSettingFields)
    {
        ArgumentNullException.throwIfNull(schoolIds, SCHOOL_IDS);
        ArgumentNullException.throwIfNull(annualSettingFields, 'annualSettingFields');

        return selectWithLatestAnnualSetting(schoolIds, annualSettingFields, 50000);
    }
    
    /**
    * @description Query the schools that has opted out EZ for the given schoolIds and academic year.
    * @param The set of school Ids that will be queried.
    * @param academicYearName The name of the academic year for which we will query the annual setting records for the given schools.
    * @return A List with the accounts and its related annual setting records that meets the condition Opt_Out_Of_PFS_EZ__c = true.
    */
    public List<Account> selectHasOptOutEZ(Set<Id> schoolIds, String academicYearName) {
        
        assertIsAccessible();
        
        fflib_QueryFactory accountQueryFactory = newQueryFactory().setCondition('Id IN :schoolIds');
        
        fflib_QueryFactory AnnualSettingsQueryFactory = AnnualSettingsSelector.newInstance()
            .addQueryFactorySubselect(accountQueryFactory)
            .selectFields(new Set<String>{'Id', 'School__c', 'Academic_Year_Name__c', 'Opt_Out_Of_PFS_EZ__c'})
            .setCondition('Academic_Year_Name__c =: academicYearName AND Opt_Out_Of_PFS_EZ__c = true')
            .setLimit(1);
        
        return Database.query(accountQueryFactory.toSOQL());
    }
    
    public List<Account> selectWithLatestAnnualSetting(Set<Id> schoolIds, Set<String> annualSettingFields, Integer limitQuery) {

        assertIsAccessible();
        
        fflib_QueryFactory accountQueryFactory = newQueryFactory().setCondition('Id IN :schoolIds');
        
        AnnualSettingsSelector selectorAS = AnnualSettingsSelector.newInstance();
        selectorAS.setOrderBy('Academic_Year__r.Start_Date__c DESC');
        
        fflib_QueryFactory AnnualSettingsQueryFactory = 
            selectorAS
            .addQueryFactorySubselect(accountQueryFactory)
            .setLimit(limitQuery);
        
        if (!annualSettingFields.isEmpty()) {
            AnnualSettingsQueryFactory.selectFields(annualSettingFields);
        }
        
        return Database.query(accountQueryFactory.toSOQL());
    }

    /**
    * @description Query the schools that are active subscribers and visible for selection by their Ids with linked AnnualSetting for specified academic year.
    * @param schoolIds The set of school Ids that will be queried.
    * @param schoolFields school fileds to query
    * @param annualSettingFields fileds to query
    * @param academicYearId The academic year for which we will query the annual setting records for the given schools.
    * @throws An ArgumentNullException if schoolIds is null.
    * @throws An ArgumentNullException if schoolFields is null.
    * @throws An ArgumentNullException if annualSettingFields is null.
    * @throws An ArgumentNullException if academicYearId is null.
    * @return A List of accounts with related annual setting records that meets all conditions.
    */
    public List<Account> selectVisibleActiveSchoolsWithAnnualSetting(Set<Id> schoolIds, List<String> schoolFields, List<String> annualSettingFields, Id academicYearId)
    {
        ArgumentNullException.throwIfNull(schoolIds, SCHOOL_IDS);
        ArgumentNullException.throwIfNull(schoolFields, SCHOOL_FIELDS);
        ArgumentNullException.throwIfNull(annualSettingFields, ANNUAL_SETTING_FIELDS);
        ArgumentNullException.throwIfNull(academicYearId, ACADEMIC_YEAR_ID);
        
        assertIsAccessible();

        Set<String> activeSSSStatuses = GlobalVariables.activeSSSStatuses;
        
        fflib_QueryFactory accountQueryFactory = newQueryFactory()
            .selectFields(schoolFields)
            .setCondition('Id IN :schoolIds'
                + ' AND Hide_from_School_Selection__c = false'
                + ' AND SSS_Subscriber_Status__c IN :activeSSSStatuses');
        
        AnnualSettingsSelector asSelector = AnnualSettingsSelector.newInstance();
        asSelector.setOrderBy('Academic_Year__r.Start_Date__c DESC');
        
        fflib_QueryFactory AnnualSettingsQueryFactory = asSelector
            .addQueryFactorySubselect(accountQueryFactory)
            .selectFields(annualSettingFields)
            .setCondition('Academic_Year__c = :academicYearId');
        
        return Database.query(accountQueryFactory.toSOQL());
    }

    /**
     * @description Selects Account records by RecordType Name.
     * @param recordTypeName The Name of the RecordType to query for Account records.
     * @return A list of Account records.
     * @throws An ArgumentNullException if recordTypeName is null.
     */
    public List<Account> selectByRecordTypeName(String recordTypeName) {
        ArgumentNullException.throwIfNull(recordTypeName, RECORDTYPE_NAME_PARAM);

        assertIsAccessible();

        String accountQuery = newQueryFactory(true)
                .setCondition('RecordType.Name = :recordTypeName')
                .toSOQL();

        return Database.query(accountQuery);
    }

    /**
     * @description Selects Account records by RecordType Id.
     * @param recordTypeId The Id of the RecordType to query for Account records.
     * @return A list of Account records.
     * @throws An ArgumentNullException if recordTypeId is null.
     */
    public List<Account> selectByRecordTypeId(Id recordTypeId) {
        ArgumentNullException.throwIfNull(recordTypeId, RECORDTYPE_ID);

        assertIsAccessible();

        String accountQuery = newQueryFactory(true)
                .setCondition('RecordTypeId = :recordTypeId')
                .toSOQL();

        return Database.query(accountQuery);
    }

    /**
     * @description Selects Account records by Id.
     * @param schoolIds The Ids of the Account records to query.
     * @param fieldsToQuery The list of account fields to query.
     * @return A list of Account records.
     * @throws An ArgumentNullException if schoolIds is null.
     */
    public List<Account> selectById(Set<Id> schoolIds, Set<String> fieldsToQuery) {
        ArgumentNullException.throwIfNull(schoolIds, SCHOOL_IDS);
        
        assertIsAccessible();
        
        String accountQuery = newQueryFactory()
            .setCondition('Id IN :schoolIds')
            .selectFields(fieldsToQuery)
            .toSOQL();
        
        return Database.query(accountQuery);
    }

    /**
     * @description Selects Account with School and Access Org recordtypes and sssCodeNumbers that are higher that one passed in as param.
     * @param sssCodeNumbers The sss code number of the School or Access Org.
     * @return A list of Account records.
     * @throws An ArgumentNullException if schoolIds is null.
     */
    public List<Account> selectSchoolsAndAccessOrgsWithHigherSSSCodes(Integer sssCodeNumbers)
    {
        ArgumentNullException.throwIfNull(sssCodeNumbers, SSS_CODE_NUMBER);
        assertIsAccessible();

        List<Id> schoolAndAccessOrgRecordTypeIds = new List<Id>{RecordTypes.schoolAccountTypeId, RecordTypes.accessOrgAccountTypeId};

        String accountQuery = newQueryFactory()
            .setCondition('RecordTypeId in :schoolAndAccessOrgRecordTypeIds AND SSS_School_Code_Number__c >= :sssCodeNumbers')
            .selectFields(new Set<String>{'SSS_School_Code_Number__c'})
            .addOrdering('SSS_School_Code_Number__c', fflib_QueryFactory.SortOrder.ASCENDING)
            .setLimit(50000)
            .toSOQL();

        return Database.query(accountQuery);
    }

    private Schema.SObjectType getSObjectType()
    {
        return Account.getSObjectType();
    }

    private List<Schema.SObjectField> getSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
            Account.Name,
            Account.Id
        };
    }

    /**
     * @description Creates a new instance of the AccountSelector.
     * @return An instance of AccountSelector.
     */
    public static AccountSelector newInstance() {
        return new AccountSelector();
    }
    
    /**
     * @description Singleton property ensuring external sources do not
     *              instantiate this directly.
     */
    public static AccountSelector Instance {
        get {
            if (Instance == null) {
                Instance = new AccountSelector();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to enforce singleton access
     *              via the Instance property.
     */
    private AccountSelector() {}
}