/**
 * @description This class is used to create Family App Settings records for unit tests.
 */
@isTest
public class FamilyAppSettingsTestData extends SObjectTestData {
    @testVisible private static final String FAS_NAME = 'FamilyAppSettings Name';

    /**
     * @description Get the default values for the FamilyAppSettings__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                FamilyAppSettings__c.Name => FAS_NAME
        };
    }

    /**
     * @description Set the Name field on the current Family App Settings record.
     * @param name The Name to set on the Family App Settings.
     * @return The current working instance of FamilyAppSettingsTestData.
     */
    public FamilyAppSettingsTestData forName(String name) {
        return (FamilyAppSettingsTestData) with(FamilyAppSettings__c.Name, name);
    }

    /**
     * @description Set the Main_Label_Field__c field on the current Family App Settings record.
     * @param mainLabelField The Main Label Field to set on the Family App Settings.
     * @return The current working instance of FamilyAppSettingsTestData.
     */
    public FamilyAppSettingsTestData forMainLabelField(String mainLabelField) {
        return (FamilyAppSettingsTestData) with(FamilyAppSettings__c.Main_Label_Field__c, mainLabelField);
    }

    /**
     * @description Set the Next_Page__c field on the current Family App Settings record.
     * @param nextPage The Next Page to set on the Family App Settings.
     * @return The current working instance of FamilyAppSettingsTestData.
     */
    public FamilyAppSettingsTestData forNextPage(String nextPage) {
        return (FamilyAppSettingsTestData) with(FamilyAppSettings__c.Next_Page__c, nextPage);
    }

    /**
     * @description Set the Status_Field__c field on the current Family App Settings record.
     * @param statusField The Status Field to set on the Family App Settings.
     * @return The current working instance of FamilyAppSettingsTestData.
     */
    public FamilyAppSettingsTestData forStatusField(String statusField) {
        return (FamilyAppSettingsTestData) with(FamilyAppSettings__c.Status_Field__c, statusField);
    }

    /**
     * @description Set the Sub_Label_Field__c field on the current Family App Settings record.
     * @param subLabelField The Sub Label Field to set on the Family App Settings.
     * @return The current working instance of FamilyAppSettingsTestData.
     */
    public FamilyAppSettingsTestData forSubLabelField(String subLabelField) {
        return (FamilyAppSettingsTestData) with(FamilyAppSettings__c.Sub_Label_Field__c, subLabelField);
    }

    /**
     * @description Set the Is_Speedbump_Page__c field on the current Family App Settings record.
     * @param isSpeedbumpPage The Is Speedbump Page to set on the Family App Settings.
     * @return The current working instance of FamilyAppSettingsTestData.
     */
    public FamilyAppSettingsTestData forIsSpeedbumpPage(Boolean isSpeedbumpPage) {
        return (FamilyAppSettingsTestData) with(FamilyAppSettings__c.Is_Speedbump_Page__c, isSpeedbumpPage);
    }

    /**
     * @description Set the Loop_Back_Page__c field on the current Family App Settings record.
     * @param loopBackPage The Loop Back Page to set on the Family App Settings.
     * @return The current working instance of FamilyAppSettingsTestData.
     */
    public FamilyAppSettingsTestData forLoopBackPage(String loopBackPage) {
        return (FamilyAppSettingsTestData) with(FamilyAppSettings__c.Loop_Back_Page__c, loopBackPage);
    }

    /**
     * @description Insert the current working FamilyAppSettings__c record.
     * @return The currently operated upon FamilyAppSettings__c record.
     */
    public FamilyAppSettings__c insertFamilyAppSettings() {
        return (FamilyAppSettings__c)insertRecord();
    }

    /**
     * @description Create the current working Family App Settings record without resetting
     *             the stored values in this instance of FamilyAppSettingsTestData.
     * @return A non-inserted FamilyAppSettings__c record using the currently stored field
     *             values.
     */
    public FamilyAppSettings__c createFamilyAppSettingsWithoutReset() {
        return (FamilyAppSettings__c)buildWithoutReset();
    }

    /**
     * @description Create the current working FamilyAppSettings__c record.
     * @return The currently operated upon FamilyAppSettings__c record.
     */
    public FamilyAppSettings__c create() {
        return (FamilyAppSettings__c)super.buildWithReset();
    }

    /**
     * @description The default FamilyAppSettings__c record.
     */
    public FamilyAppSettings__c DefaultFamilyAppSettings {
        get {
            if (DefaultFamilyAppSettings == null) {
                DefaultFamilyAppSettings = createFamilyAppSettingsWithoutReset();
                insert DefaultFamilyAppSettings;
            }
            return DefaultFamilyAppSettings;
        }
        private set;
    }

    /**
     * @description Get the FamilyAppSettings__c SObjectType.
     * @return The FamilyAppSettings__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return FamilyAppSettings__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static FamilyAppSettingsTestData Instance {
        get {
            if (Instance == null) {
                Instance = new FamilyAppSettingsTestData();
            }
            return Instance;
        }
        private set;
    }

    private FamilyAppSettingsTestData() { }
}