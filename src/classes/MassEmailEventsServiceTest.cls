/**
 * MassEmailEventsServiceTest.cls
 *
 * @description: Test class for MassEmailEventsService using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
public class MassEmailEventsServiceTest {
    
    
    /* positive test cases */

    // Test after update with valid Open event data
    @isTest static void onAfterUpdate_withOpenEventData_validOpenCount() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();

                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();

                Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                                     Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                                     Html_Body__c, Footer__c, Reply_To_Address__c
                                               from Mass_Email_Send__c
                                              where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];

                Contact c = [select Id, Name
                               from Contact
                              where Email = :MassEmailSendTestDataFactory.TEST_PARENT2_CONTACT_EMAIL];

                Contact contactStudent = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_STUDENT1_CONTACT_EMAIL];
                Applicant__c app = [select Id from Applicant__c where Contact__c = :contactStudent.Id];
                School_PFS_Assignment__c sPFS = [select Id, Name, Mass_Email_Events__c
                                                   from School_PFS_Assignment__c
                                                  where Applicant__c = :app.Id]; 
                sPFS.Mass_Email_Events__c = MassEmailSendTestDataFactory.createMassEmailJSONEvent( 
                                            sPFS.Id, massES.School__c, c.Id, massES.Id, SchoolMassEmailSendModel.STATUS_OPENED); 
                update sPFS;
            Test.stopTest();

            // Assert
            massES = [select Id, Name, Bounced__c, Opened__c, Unsubscribed__c
                        from Mass_Email_Send__c
                       where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
            
            System.assertEquals( 1, massES.Opened__c);
            System.assertEquals( 0, massES.Bounced__c);
            System.assertEquals( 0, massES.Unsubscribed__c);
        }
    }

    // Test after update with valid Bounced event data
    @isTest static void onAfterUpdate_withBouncedEventData_validBouncedCount() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();

                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();

                Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                                     Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                                     Html_Body__c, Footer__c, Reply_To_Address__c
                                               from Mass_Email_Send__c
                                              where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];

                Contact c = [select Id, Name
                               from Contact
                              where Email = :MassEmailSendTestDataFactory.TEST_PARENT2_CONTACT_EMAIL];

                Contact contactStudent = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_STUDENT1_CONTACT_EMAIL];
                Applicant__c app = [select Id from Applicant__c where Contact__c = :contactStudent.Id];
                School_PFS_Assignment__c sPFS = [select Id, Name, Mass_Email_Events__c
                                                   from School_PFS_Assignment__c
                                                  where Applicant__c = :app.Id]; 
                sPFS.Mass_Email_Events__c = MassEmailSendTestDataFactory.createMassEmailJSONEvent( 
                                            sPFS.Id, massES.School__c, c.Id, massES.Id, SchoolMassEmailSendModel.STATUS_BOUNCED); 
                update sPFS;
            Test.stopTest();

            // Assert
            massES = [select Id, Name, Bounced__c, Opened__c, Unsubscribed__c
                        from Mass_Email_Send__c
                       where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
            
            System.assertEquals( 0, massES.Opened__c);
            System.assertEquals( 1, massES.Bounced__c);
            System.assertEquals( 0, massES.Unsubscribed__c);
        }
    }

    // Test after update with valid Unsub event data
    @isTest static void onAfterUpdate_withUnsubEventData_validUnsubCount() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();

                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();

                Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                                     Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                                     Html_Body__c, Footer__c, Reply_To_Address__c
                                               from Mass_Email_Send__c
                                              where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];

                Contact c = [select Id, Name
                               from Contact
                              where Email = :MassEmailSendTestDataFactory.TEST_PARENT2_CONTACT_EMAIL];

                Contact contactStudent = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_STUDENT1_CONTACT_EMAIL];
                Applicant__c app = [select Id from Applicant__c where Contact__c = :contactStudent.Id];
                School_PFS_Assignment__c sPFS = [select Id, Name, Mass_Email_Events__c
                                                   from School_PFS_Assignment__c
                                                  where Applicant__c = :app.Id]; 
                sPFS.Mass_Email_Events__c = MassEmailSendTestDataFactory.createMassEmailJSONEvent( 
                                            sPFS.Id, massES.School__c, c.Id, massES.Id, SchoolMassEmailSendModel.STATUS_UNSUB); 
                update sPFS;
            Test.stopTest();

            // Assert
            massES = [select Id, Name, Bounced__c, Opened__c, Unsubscribed__c
                        from Mass_Email_Send__c
                       where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
            
            System.assertEquals( 0, massES.Opened__c);
            System.assertEquals( 0, massES.Bounced__c);
            System.assertEquals( 1, massES.Unsubscribed__c);
        }
    }

    // Test after update with valid (but duplicated) Open event data, ensure de-duped count
    @isTest static void onAfterUpdate_withDuplicateOpenEventData_validOpenCount() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();

                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();

                Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                                     Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                                     Html_Body__c, Footer__c, Reply_To_Address__c
                                               from Mass_Email_Send__c
                                              where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];

                Contact c = [select Id, Name
                               from Contact
                              where Email = :MassEmailSendTestDataFactory.TEST_PARENT2_CONTACT_EMAIL];

                Contact contactStudent = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_STUDENT1_CONTACT_EMAIL];
                Applicant__c app = [select Id from Applicant__c where Contact__c = :contactStudent.Id];
                School_PFS_Assignment__c sPFS = [select Id, Name, Mass_Email_Events__c
                                                   from School_PFS_Assignment__c
                                                  where Applicant__c = :app.Id]; 
                sPFS.Mass_Email_Events__c = MassEmailSendTestDataFactory.createMassEmailJSONEvent( 
                                            sPFS.Id, massES.School__c, c.Id, massES.Id, SchoolMassEmailSendModel.STATUS_OPENED); 
                sPFS.Mass_Email_Events__c += ',' + MassEmailSendTestDataFactory.createMassEmailJSONEvent( 
                                            sPFS.Id, massES.School__c, c.Id, massES.Id, SchoolMassEmailSendModel.STATUS_OPENED); 
                update sPFS;
            Test.stopTest();

            // Assert
            massES = [select Id, Name, Bounced__c, Opened__c, Unsubscribed__c
                        from Mass_Email_Send__c
                       where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
            
            System.assertEquals( 1, massES.Opened__c);
            System.assertEquals( 0, massES.Bounced__c);
            System.assertEquals( 0, massES.Unsubscribed__c);
        }
    }

}