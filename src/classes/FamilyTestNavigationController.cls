public with sharing class FamilyTestNavigationController {

    public PFS__c pfsRecord {get; set;}
    public String currentScreen {get; set;}
    public ApplicationUtils appUtils {get; set;}

    public FamilyTestNavigationController() {
        String pfsId = String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('Id'));
        currentScreen = ApexPages.currentPage().getParameters().get('screen');
        
        List<String> pfsFieldNames = new List<String>(Schema.SObjectType.PFS__c.fields.getMap().keySet());
        List<String> applicantFieldNames = new List<String>(Schema.SObjectType.Applicant__c.fields.getMap().keySet());
        
        String pfsQueryString = 'SELECT ' + String.Join(pfsFieldNames, ',');
        pfsQueryString += ', (select ' + String.Join(applicantFieldNames, ',') + ' from Applicants__r)';
        pfsQueryString += ' from PFS__c where Id = :pfsId';
        
        pfsRecord = Database.query(pfsQueryString);
        
        appUtils = new ApplicationUtils(UserInfo.getLanguage(), pfsRecord, currentScreen);
    }
    
    public PageReference Refresh() {
        return null;
    }
}