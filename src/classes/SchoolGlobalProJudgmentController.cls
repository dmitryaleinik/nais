/* 
 * Spec-140, NAIS-66 School Portal - Global Professional Judgment
 * BR, Exponent Partners, 2013
 */
    
public with sharing class SchoolGlobalProJudgmentController implements SchoolAcademicYearSelectorInterface {

    @testvisible private static final String SG_PRO_JUDG_TELL_ME_MORE_LINK_DOCINFO_NAME = 'SG_Pro_Judg_Tell_Me_More_Link';
    
    public SchoolGlobalProJudgmentController() {}
    
    // Called by the action attribute of the page
    public void pageLoad(){
        //mySchoolId = GlobalVariables.getCurrentUser().Contact.AccountId;
        mySchoolId = GlobalVariables.getCurrentSchoolId();

        loadAcademicYear();
        loadAnnualSettings(myAcademicYear.Id);      
        loadPreviousAnnualSetting(); //NAIS-2357                
        // loadSPAs(); // [SL] NAIS-1006 - load the SPAs at time of update
    }
    /*NAIS-2357*/
    public void loadPreviousAnnualSetting()
    {
        existsPreviousValues=false;
        Academic_Year__c previousAY= GlobalVariables.getPreviousAcademicYear(myAcademicYear.Id);
        if(previousAY!= null)
        {
            list<Annual_Setting__c> lstAnnualSetting = GlobalVariables.getCurrentAnnualSettings(false,previousAY.Id);
            if(!GlobalVariables.isNewAnnualSetting && lstAnnualSetting.size()> 0)
            {
                existsPreviousValues =true;
                previousAnnualSetting = lstAnnualSetting.get(0);
            }           
        }               
    }   
    /*NAIS-2357*/
    public void loadAcademicYear() {
        
        // Due to the selector_onchange not refreshing properly, we redirect to the same page with academicyearid parameter.
        isCurrYear = false;
        Map<String, String> parameters = ApexPages.currentPage().getParameters();
        String academicYearId = null;
        if (parameters.containsKey('academicyearid')){
             setAcademicYearId(parameters.get('academicyearid'));
        }
        else{
            myAcademicYear = GlobalVariables.getCurrentAcademicYear();
        }
        //SFP-91, [G.S]
        isCurrYear = (myAcademicYear.Id == GlobalVariables.getCurrentAcademicYear().Id);
    }
    
    public void loadAnnualSettings(Id academicYearId) {
        
        List<Annual_Setting__c> annualSettings = GlobalVariables.getCurrentAnnualSettings(true, academicYearId);
        for (Annual_Setting__c setting : annualSettings) {
            if(setting.Academic_Year__c == academicYearId) {
                myAnnualSettings = setting;
                break;
            }
        }
    }
    
    public PageReference clearInputValue() {
        
        String mainField = ApexPages.currentPage().getParameters().get('mainField');
        String dependentField = ApexPages.currentPage().getParameters().get('dependentField');
        
        if (myAnnualSettings.get(mainField) != null) {
           myAnnualSettings.put(dependentField, null);
        }
        return null;
    }

    /* Properties */
    public Annual_Setting__c previousAnnualSetting {get;set;} //NAIS-2357
    public boolean isCopyPJSetting {get;set;} //NAIS-2357
    public boolean existsPreviousValues {get;set;}//NAIS-2357
    public string buttonMode {get;set;} //NAIS-2357
    public SchoolGlobalProJudgmentController Me { get { return this; } }
    public boolean isCurrYear {get;set;}//SFP-91, [G.S]
    //NAIS-2357
    public boolean prevUsesCOLARecord { 
        get{
            if(previousAnnualSetting != null && previousAnnualSetting.Use_Cost_of_Living_Adjustment__c == 'Yes' && previousAnnualSetting.COLA__c != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        } 
        set; 
    } 
    //NAIS-2357
    public string prevCOLAClass { 
        get{
            return prevUsesCOLARecord ? 'inactive' : '';
        } 
        set; 
    }
    
    public String lastButton { get; set; }
    
    public Academic_Year__c myAcademicYear { get; set; }
    public Annual_Setting__c myAnnualSettings { get; set; }
    
    // [CH] NAIS-1885 Added to support turning off Adjustable IPA fields for certain years
    public Boolean displayAdjustableIPA { 
        get{
            if(myAcademicYear != null && myAcademicYear.Include_Adjustable_IPA_Setting__c == 'Yes'){
                return true;
            }   
            else{
                return false;
            }
        }
        set;
    }
    
    public Id mySchoolId { get; private set; }
    
    public String bUnlocked { get; set; } // Param for actions.
    public Id tellMeMoreDocumentId {
        get {
            if (tellMeMoreDocumentId == null) {
                if (!Test.isRunningTest()) {
                    tellMeMoreDocumentId = 
                        DocumentLinkMappingService.getDocumentRecordId(SG_PRO_JUDG_TELL_ME_MORE_LINK_DOCINFO_NAME);
                } else {
                    tellMeMoreDocumentId = DocumentLinkMappingService.getDocumentRecordId(
                        DocumentLinkMappingService.TEST_DOCUMENT_LINK_MAPPING_RECORD_NAME);                 
                }
            }
            return tellMeMoreDocumentId;
        } 
        private set;
    }
    // public List<School_PFS_Assignment__c> SPAs { get; set; } // [SL] NAIS-1006 - load the SPAs at time of update
    
    /* End Properties */
    
    /* Action Methods */
    public PageReference saveSettings() {
        // If they didn't cancel at the confirmation and the 
        //  page doesn't have any errors
        if(lastButton != 'cancel' && validatePage()){ 
            // Copy cola.SSS_COLA__c to value per meeting. We end up saving COLA__c, should we not save it?
            if ((myAnnualSettings.Use_Cost_of_Living_Adjustment__c == 'Yes') && (myAnnualSettings.COLA__c != null)) {
                COLA__c cola = [select SSS_COLA__c from COLA__c where Id = :myAnnualSettings.COLA__c];
                if ((cola != null) && (cola.SSS_COLA__c != null)) {
                    myAnnualSettings.Override_Default_COLA_Value__c = cola.SSS_COLA__c;
                }
            }
            
            // [SL] NAIS-886 - Get parameters from hidden input field to determine mode
            String buttonString = null;
            String buttonMode = null;
            List<String> clickStrings = lastButton.split(':');
            if (clickStrings.size() == 2) {
                buttonString = clickStrings[0];
                buttonMode = clickStrings[1];
            }
            
            // [SL] NAIS-886 - handle the update in a separate method, which updates just the relevant field
            if (buttonString != null) {
                
                // Update the annual setting record
                updateAnnualSetting(buttonString);

                // [SL] NAIS-886 - explicitly set "Future" mode instead of relying on blank value
                if (buttonMode != 'Future') {
                    // Update related SPA records
                    updateSPAs(buttonString, buttonMode);
                }
                
            }
            lastButton='';
            return null;
        }
        else{
            lastButton=''; 
            return null;
        }
    }
    
    public PageReference cancel() {
        PageReference clearPage = new PageReference('/apex/SchoolGlobalProJudgment');
        clearPage.setRedirect(true);
        return clearPage;
    }
    
    public PageReference updateImputeRate() {
        
        if(this.myAnnualSettings.Use_Dividend_Interest_to_Impute_Assets__c == 'Yes' && this.myAnnualSettings.Impute_Rate__c == null) {
            SSS_Constants__c constants = [SELECT Id, Percentage_for_Imputing_Assets__c FROM SSS_Constants__c WHERE Academic_Year__c = :this.myAcademicYear.Id LIMIT 1];
            
            this.myAnnualSettings.Impute_Rate__c = constants.Percentage_for_Imputing_Assets__c;
        }
        
        if(this.myAnnualSettings.Use_Dividend_Interest_to_Impute_Assets__c == 'No') {
            this.myAnnualSettings.Impute_Rate__c = null;
        }
        
        return null;
    }
    //NAIS-2357         
    public pageReference copyPJSetting()
    {    
        if(lastButton=='cancel')
            return null;
        if(previousAnnualSetting != null)  
        {                       
            myAnnualSettings.Apply_Minimum_Income__c = previousAnnualSetting.Apply_Minimum_Income__c;
            myAnnualSettings.Minimum_Income_Amount__c = previousAnnualSetting.Minimum_Income_Amount__c;
            myAnnualSettings.Add_Depreciation_Home_Business_Expense__c = previousAnnualSetting.Add_Depreciation_Home_Business_Expense__c;
            myAnnualSettings.Use_Home_Equity__c = previousAnnualSetting.Use_Home_Equity__c;
            myAnnualSettings.Use_Home_Equity_Cap__c = previousAnnualSetting.Use_Home_Equity_Cap__c;
            myAnnualSettings.Use_Housing_Index_Multiplier__c = previousAnnualSetting.Use_Housing_Index_Multiplier__c;
            myAnnualSettings.Use_Dividend_Interest_to_Impute_Assets__c = previousAnnualSetting.Use_Dividend_Interest_to_Impute_Assets__c;
            myAnnualSettings.Impute_Rate__c = previousAnnualSetting.Impute_Rate__c;
            if(previousAnnualSetting.Use_Cost_of_Living_Adjustment__c == 'Yes' && previousAnnualSetting.Override_Default_COLA_Value__c != null){
                myAnnualSettings.Use_Cost_of_Living_Adjustment__c = previousAnnualSetting.Use_Cost_of_Living_Adjustment__c;
                myAnnualSettings.Override_Default_COLA_Value__c = previousAnnualSetting.Override_Default_COLA_Value__c; 
            }
            else{
                myAnnualSettings.Use_Cost_of_Living_Adjustment__c = 'No';
                myAnnualSettings.Override_Default_COLA_Value__c = null;
                myAnnualSettings.COLA__c = null;
            }
            myAnnualSettings.Adjust_Housing_Portion_of_IPA__c = previousAnnualSetting.Adjust_Housing_Portion_of_IPA__c;
            myAnnualSettings.IPA_Housing_Family_of_4__c = previousAnnualSetting.IPA_Housing_Family_of_4__c; 
            myAnnualSettings.Professional_Judgment_Updated__c =system.today();
            
            update myAnnualSettings;
        }
        
        if(lastButton!='Future') 
            updateSPAs('Copy',lastButton);
        return null;
    }
    //End NAIS-2357
    /* End Action Methods */
    

    /* Helper Methods */
    private Boolean validatePage(){
        // Minimum Income
        if ((myAnnualSettings.Apply_Minimum_Income__c == 'Yes') &&
            ((myAnnualSettings.Minimum_Income_Amount__c == null) || (myAnnualSettings.Minimum_Income_Amount__c == 0))) {
                myAnnualSettings.Minimum_Income_Amount__c.addError('If Apply Minimum Income is Yes, Minimum Income Amount must have a value.'); 
        }
        else if(myAnnualSettings.Apply_Minimum_Income__c == 'No'){
            myAnnualSettings.Minimum_Income_Amount__c = null;
        }
        
        // Impute Rate
        if ((myAnnualSettings.Use_Dividend_Interest_to_Impute_Assets__c == 'Yes') &&
            ((myAnnualSettings.Impute_Rate__c == null) || (myAnnualSettings.Impute_Rate__c == 0))) {
                myAnnualSettings.Impute_Rate__c.addError('If Use Dividend/Interest Income to Impute Assets? is Yes, Impute Rate must have a value.');                           
        }
        else if(myAnnualSettings.Use_Dividend_Interest_to_Impute_Assets__c == 'No'){
            myAnnualSettings.Impute_Rate__c = null;
        }
        
        // COLA, only allow lookup or value.
        if (myAnnualSettings.Use_Cost_of_Living_Adjustment__c == 'Yes') {
            // Either Override_Default_COLA_Value__c or COLA__c must be set.
            if ((myAnnualSettings.COLA__c == null) &&
                ((myAnnualSettings.Override_Default_COLA_Value__c == null) || (myAnnualSettings.Override_Default_COLA_Value__c == 0))) {
                myAnnualSettings.Use_Cost_of_Living_Adjustment__c.addError('If Cost of Living Adjustment is Yes, COLA Value or Own Value must have a value.');          
            }
            // But not both.
            String isColaAction = ApexPages.currentPage().getParameters().get('isColaAction');          
            if ((myAnnualSettings.COLA__c != null) && (myAnnualSettings.Override_Default_COLA_Value__c != null) && isColaAction == 'Yes') {
                myAnnualSettings.Use_Cost_of_Living_Adjustment__c.addError('If Use Cost of Living Adjustment is Yes, COLA Value and Enter Your Own Value cannot both have a value.');
            }
        }
        else if (myAnnualSettings.Use_Cost_of_Living_Adjustment__c == 'No'){
            myAnnualSettings.COLA__c = null;
            myAnnualSettings.Override_Default_COLA_Value__c = null;
        }
        
        // [CH] NAIS-1885 Adding validation to require family of 4 value
        if(displayAdjustableIPA && myAnnualSettings.Adjust_Housing_Portion_of_IPA__c == 'Yes') {
            if(myAnnualSettings.IPA_Housing_Family_of_4__c == null || myAnnualSettings.IPA_Housing_Family_of_4__c == 0){
                myAnnualSettings.IPA_Housing_Family_of_4__c.addError('If Adjust Housing Portion of IPA is Yes, you must enter the value that would apply to a family of 4.');   
            }
        }
        else if (myAnnualSettings.Adjust_Housing_Portion_of_IPA__c == 'No') {
            myAnnualSettings.IPA_Housing_Family_of_4__c = null;
        }
        
        return !ApexPages.hasMessages(ApexPages.Severity.ERROR);
    }
    
    
    // [CH] NAIS-2357 Defining a wrapper for specifying if an SPA should be updated while
    //                  being processed by field updating code.  This allows us to avoid 
    //                  duplicating field copying logic between a single-row case and a copy from previous case
    class SPAWrapper{
        public boolean updateRecord {get; set;}
        public School_PFS_Assignment__c spa {get; set;}
        public SPAWrapper(School_PFS_Assignment__c spaRecord){
            spa = spaRecord;
            updateRecord = false;
        }
    }
    
    private void updateSPAs(String buttonString, String buttonMode){
        
        // [SL] NAIS-1006 - load the SPAs at time of update
        List<School_PFS_Assignment__c> SPAs = [select Add_Deprec_Home_Bus_Expense__c, Use_Home_Equity__c,
                                                    Use_Home_Equity_Cap__c, Use_Housing_Index_Multiplier__c, 
                                                    Use_Div_Int_to_Impute_Assets__c, 
                                                    //ImputeRate,
                                                    Impute__c, Adjust_Housing_Portion_of_IPA__c, IPA_Housing_Family_of_4__c,
                                                    Apply_Minimum_Income__c, Minimum_Income_Amount__c, 
                                                    Use_COLA__c, Override_Default_COLA_Value__c, Lock_Professional_Judgment__c,
                                                    Academic_Year_Picklist__c
                                                    from School_PFS_Assignment__c
                                                    where School__c = :mySchoolId 
                                                    and Academic_Year_Picklist__c = :myAcademicYear.Name];
        
        List<School_PFS_Assignment__c> spasToUpdate = new List<School_PFS_Assignment__c>{};
        
        // Get parameters from hidden input field
        /*List<String> clickStrings = lastButton.split(':');
        String buttonString = clickStrings[0];
        String buttonMode = clickStrings[1];
        */
        
        // [CH] NAIS-2357 Refactoring to handle copy from previous use case
        // boolean updateSPA =false;
        
        // Iterate through the list of related SPA records
        for (School_PFS_Assignment__c spa : SPAs) {
            // updateSPA =false;
            // If we should try to update the current record
            if(buttonMode == 'All' || (buttonMode == 'Unlocked' && spa.Lock_Professional_Judgment__c == 'No')){
                SPAWrapper spaWrap = new SPAWrapper(spa);
                
                if(buttonString == 'Copy'){
                    spaWrap = copyDepreciationFields(spaWrap);
                    spaWrap = copyHomeEquityFields(spaWrap);
                    spaWrap = copyHomeEquityCapFields(spaWrap);
                    spaWrap = copyHousingIndexFields(spaWrap);
                    spaWrap = copyImputeFields(spaWrap);
                    spaWrap = copyMinIncomefields(spaWrap);
                    spaWrap = copyCOLAFields(spaWrap);
                    spaWrap = copyIPAFields(spaWrap);
                }
                else if(buttonString == 'depreciation' || buttonString=='All'){
                    spaWrap = copyDepreciationFields(spaWrap);
                }
                else if(buttonString == 'homeequity' || buttonString=='All'){
                    spaWrap = copyHomeEquityFields(spaWrap);
                }
                else if(buttonString == 'homeequitycap' || buttonString=='All'){
                    spaWrap = copyHomeEquityCapFields(spaWrap);
                }
                else if(buttonString == 'housingindex' || buttonString=='All'){
                    spaWrap = copyHousingIndexFields(spaWrap);
                }
                else if(buttonString == 'impute' || buttonString=='All'){
                    spaWrap = copyImputeFields(spaWrap);
                }
                else if(buttonString == 'minincome' || buttonString=='All'){
                    spaWrap = copyMinIncomefields(spaWrap);
                }
                else if(buttonString == 'cola' || buttonString=='All'){
                    spaWrap = copyCOLAFields(spaWrap);
                }
                else if(buttonString == 'ipa' || buttonString=='All'){
                    spaWrap = copyIPAFields(spaWrap);
                }
                if(spaWrap.updateRecord) //NAIS-2357 - Add to the list once time. 
                    spasToUpdate.add(spaWrap.spa);      
            }
        }
        
        if(spasToUpdate.size()>0)// NAIS-2357 - Execute batch only if exists SPA. 
        {
            try{            
                BatchGenericUpsert batchUpsert = new BatchGenericUpsert();
                batchUpsert.recordsToUpsert = spasToUpdate;
                Database.executeBatch(batchUpsert);
            
                // update spasToUpdate;
            }
            catch(Exception e){
                System.debug('Error Updating SPA: ' + e.getCause() + '\n' + e.getLineNumber() + '\n' + e.getMessage() + '\n' + e.getStackTraceString());
                throw e;
            }
        }
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Applied to ' + SPAs.size() + ' PFSs. (' + spasToUpdate.size() + ' changed.)'));
    }
    
    // [CH] NAIS-2357  New methods for copying field values in both single line and Copy From Previous cases
    private SPAWrapper copyDepreciationFields(SPAWrapper spaWrap){
        if (spaWrap.spa.Add_Deprec_Home_Bus_Expense__c != myAnnualSettings.Add_Depreciation_Home_Business_Expense__c){
            spaWrap.spa.Add_Deprec_Home_Bus_Expense__c = myAnnualSettings.Add_Depreciation_Home_Business_Expense__c;
            spaWrap.updateRecord =true;             
        }
        return spaWrap;
    }
    private SPAWrapper copyHomeEquityFields(SPAWrapper spaWrap){
        if (spaWrap.spa.Use_Home_Equity__c != myAnnualSettings.Use_Home_Equity__c){
            spaWrap.spa.Use_Home_Equity__c = myAnnualSettings.Use_Home_Equity__c;
            spaWrap.updateRecord =true; 
        }
        return spaWrap;
    }
    private SPAWrapper copyHomeEquityCapFields(SPAWrapper spaWrap){
        if (spaWrap.spa.Use_Home_Equity_Cap__c != myAnnualSettings.Use_Home_Equity_Cap__c){
            spaWrap.spa.Use_Home_Equity_Cap__c = myAnnualSettings.Use_Home_Equity_Cap__c;
            spaWrap.updateRecord =true; 
        }
        return spaWrap;
    }
    private SPAWrapper copyHousingIndexFields(SPAWrapper spaWrap){
        if (spaWrap.spa.Use_Housing_Index_Multiplier__c != myAnnualSettings.Use_Housing_Index_Multiplier__c){
            spaWrap.spa.Use_Housing_Index_Multiplier__c = myAnnualSettings.Use_Housing_Index_Multiplier__c;
            spaWrap.updateRecord =true; 
        }
        return spaWrap;
    }
    private SPAWrapper copyImputeFields(SPAWrapper spaWrap){
        if ((spaWrap.spa.Use_Div_Int_to_Impute_Assets__c != myAnnualSettings.Use_Dividend_Interest_to_Impute_Assets__c)
                    || (spaWrap.spa.Impute__c != myAnnualSettings.Impute_Rate__c)) { // [SL] NAIS-998
            spaWrap.spa.Use_Div_Int_to_Impute_Assets__c = myAnnualSettings.Use_Dividend_Interest_to_Impute_Assets__c;
            spaWrap.spa.Impute__c = myAnnualSettings.Impute_Rate__c;
            // Impute Rate
            spaWrap.updateRecord =true; 
        }
        return spaWrap;
    }
    private SPAWrapper copyMinIncomeFields(SPAWrapper spaWrap){
        if ((spaWrap.spa.Apply_Minimum_Income__c != myAnnualSettings.Apply_Minimum_Income__c)
                    || (spaWrap.spa.Minimum_Income_Amount__c != myAnnualSettings.Minimum_Income_Amount__c)) { // [SL] NAIS-998
            spaWrap.spa.Apply_Minimum_Income__c = myAnnualSettings.Apply_Minimum_Income__c;
            spaWrap.spa.Minimum_Income_Amount__c = myAnnualSettings.Minimum_Income_Amount__c;
            spaWrap.updateRecord =true; 
        }
        return spaWrap;
    }
    private SPAWrapper copyCOLAFields(SPAWrapper spaWrap){
        if ((spaWrap.spa.Use_COLA__c != myAnnualSettings.Use_Cost_of_Living_Adjustment__c) 
                    || (spaWrap.spa.Override_Default_COLA_Value__c != myAnnualSettings.Override_Default_COLA_Value__c)){ // [SL] NAIS-998
            spaWrap.spa.Use_COLA__c = myAnnualSettings.Use_Cost_of_Living_Adjustment__c;
            // Lookup Field 
            // or...
            spaWrap.spa.Override_Default_COLA_Value__c = myAnnualSettings.Override_Default_COLA_Value__c;
            spaWrap.updateRecord =true; 
        }
        return spaWrap;
    }
    private SPAWrapper copyIPAFields(SPAWrapper spaWrap){
        if (
            (spaWrap.spa.Adjust_Housing_Portion_of_IPA__c != myAnnualSettings.Adjust_Housing_Portion_of_IPA__c) || 
            (spaWrap.spa.IPA_Housing_Family_of_4__c != myAnnualSettings.IPA_Housing_Family_of_4__c ))
        { // [SL] NAIS-998
            spaWrap.spa.Adjust_Housing_Portion_of_IPA__c = myAnnualSettings.Adjust_Housing_Portion_of_IPA__c;
            spaWrap.spa.IPA_Housing_Family_of_4__c = myAnnualSettings.IPA_Housing_Family_of_4__c ;
            spaWrap.updateRecord =true; 
        }
        return spaWrap;
    }
    
    class DebugException extends Exception{}
    
    // [SL] NAIS-886: Allow saving individual PJ settings
    private void updateAnnualSetting(String buttonString) {
        if (myAnnualSettings != null) {
            Annual_Setting__c updatedAnnualSetting = new Annual_Setting__c(Id = myAnnualSettings.Id);
            
            if(buttonString == 'depreciation') {
                updatedAnnualSetting.Add_Depreciation_Home_Business_Expense__c = myAnnualSettings.Add_Depreciation_Home_Business_Expense__c;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Professional Judgment setting updated: Add Depreciation/Home Business Expenses from Tax Forms'));
            }
            else if(buttonString == 'homeequity') {
                updatedAnnualSetting.Use_Home_Equity__c = myAnnualSettings.Use_Home_Equity__c;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Professional Judgment setting updated: Home Equity'));
            } 
            else if(buttonString == 'homeequitycap') {
                updatedAnnualSetting.Use_Home_Equity_Cap__c = myAnnualSettings.Use_Home_Equity_Cap__c;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Professional Judgment setting updated: Home Equity Cap'));
            } 
            else if(buttonString == 'housingindex') {
                updatedAnnualSetting.Use_Housing_Index_Multiplier__c = myAnnualSettings.Use_Housing_Index_Multiplier__c;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Professional Judgment setting updated: Housing Index Multiplier'));
            } 
            else if(buttonString == 'impute') {
                updatedAnnualSetting.Use_Dividend_Interest_to_Impute_Assets__c = myAnnualSettings.Use_Dividend_Interest_to_Impute_Assets__c;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Professional Judgment setting updated: Impute Assets'));
                updatedAnnualSetting.Impute_Rate__c = myAnnualSettings.Impute_Rate__c;
            } 
            else if(buttonString == 'minincome') {
                updatedAnnualSetting.Apply_Minimum_Income__c = myAnnualSettings.Apply_Minimum_Income__c;
                updatedAnnualSetting.Minimum_Income_Amount__c = myAnnualSettings.Minimum_Income_Amount__c;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Professional Judgment setting updated: Apply Minimum Income Amount'));
            } 
            else if(buttonString == 'cola') {
                updatedAnnualSetting.Use_Cost_of_Living_Adjustment__c = myAnnualSettings.Use_Cost_of_Living_Adjustment__c;
                updatedAnnualSetting.COLA__c = myAnnualSettings.COLA__c;
                updatedAnnualSetting.Override_Default_COLA_Value__c = myAnnualSettings.Override_Default_COLA_Value__c;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Professional Judgment setting updated: COLA'));
            }
            // [CH] NAIS-1885 Adding handling for saving IPA values
            else if(buttonString == 'ipa') {
                updatedAnnualSetting.Adjust_Housing_Portion_of_IPA__c = myAnnualSettings.Adjust_Housing_Portion_of_IPA__c;
                updatedAnnualSetting.IPA_Housing_Family_of_4__c = myAnnualSettings.IPA_Housing_Family_of_4__c;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Professional Judgment setting updated: Income Protection Allowance Settings'));
            }  
            
            updatedAnnualSetting.Professional_Judgment_Updated__c = System.today();
            update updatedAnnualSetting;
            
            loadAnnualSettings(myAcademicYear.Id); //NAIS-2357
        }
    }
    /* End Helper Methods */
    
    /* SchoolAcademicYearSelectorInterface Implementation */
    
    public String getAcademicYearId() {
        return myAcademicYear.Id;
    }
    
    public void setAcademicYearId(String academicYearId) {
        myAcademicYear = [SELECT Id, Name, Include_Adjustable_IPA_Setting__c FROM Academic_Year__c WHERE Id = :academicYearId];
    }
    
    public PageReference SchoolAcademicYearSelector_OnChange(Boolean saveRecord) {
        
        if(saveRecord){  
            saveSettings();
        }
        
        PageReference newPage = new PageReference(ApexPages.currentPage().getUrl());
        newPage.getParameters().put('academicyearid', myAcademicYear.Id);       
        newPage.setRedirect(true);
        return newPage;
    }
    /* END: SchoolAcademicYearSelectorInterface Implementation */
}