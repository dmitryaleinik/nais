public class FamilyAppKSApplicantInfoCont extends FamilyAppCompController {

    public String currentApplicantId {get; set;}
    
    public String birthParentsOnApplication {get; set;}
    
    public String parentsMaritalStatus {get; set;}
    
    /* [CH] NAIS-2258 This is not used anywhere so commenting out
    public Boolean mailCheckBox {
        get {
            if(mailCheckBox == null){
                mailCheckBox = pfs.KS_Mail_Household__c;
            }
            return mailCheckBox;
        }
         set;
    }
    */
    
    public PageReference UpdateBirthParentsOnApplication() {
        
        if(currentApplicantId != null && birthParentsOnApplication != null) {
            
            Map<Id, Applicant__c> applicantMap = new Map<Id, Applicant__c>(pfs.Applicants__r);
            
            if(applicantMap.containsKey(currentApplicantId)) {
                Applicant__c applicant = applicantMap.get(currentApplicantId);
                
                applicant.Birth_Parents_Included_in_Application__c = birthParentsOnApplication;
            }
            
        }
        
        return null;
        
    }
    
    public PageReference UpdateParentsMaritalStatus() {
        
        if(currentApplicantId != null && parentsMaritalStatus != null) {
            
            Map<Id, Applicant__c> applicantMap = new Map<Id, Applicant__c>(pfs.Applicants__r);
            
            if(applicantMap.containsKey(currentApplicantId)) {
                Applicant__c applicant = applicantMap.get(currentApplicantId);
                
                applicant.Birth_Parents_Marital_Status__c = parentsMaritalStatus;
            }
        }
        
        return null;
        
    }
    
    public List<Applicant__c> ksApplicants { 
        get {
            if (ksApplicants == null) {
                ksApplicants = new List<Applicant__c>();
                
                if ( pfs != null && pfs.applicants__r != null && pfs.applicants__r.size() > 0 &&
                     SchoolPortalSettings.KS_School_Account_Id != null ) {
                    Map<Id, Applicant__c> applicants = new Map<Id, Applicant__c>(pfs.Applicants__r);                 
                    
                    List<Applicant__c> applicantList = [SELECT id, ( select id 
                                                                        from School_PFS_Assignments__r 
                                                                        where School__c = :SchoolPortalSettings.KS_School_Account_Id 
                                                                        and School__r.SSS_Subscriber_Status__c in :GlobalVariables.activeSSSStatuses 
                                                                        and Withdrawn__c != 'Yes' ) 
                                                                    FROM Applicant__c  where id in :applicants.keySet()];
                                                                    
                    for (Applicant__c applicant : applicantList) {
                        if (applicant.School_PFS_Assignments__r != null && applicant.School_PFS_Assignments__r.size() > 0) {
                            ksApplicants.add(applicants.get(applicant.id));
                        }
                    }
                }
            }
            
            return ksApplicants;
        }
        
        set;
    }
    // NAIS-1822 Start
    public PageReference populateMail() {
        // mailCheckBox = !mailCheckBox; [CH] NAIS-2258 This is not used anywhere so commenting out
        if(pfs.KS_Mail_Household__c){
            pfs.KS_Mailing_Address__c = pfs.KS_Household_Address__c;
            pfs.KS_Mailing_City__c = pfs.KS_Household_City__c;
            pfs.KS_Mailing_State_Province__c = pfs.KS_Household_State_Province__c;
            pfs.KS_Mailing_ZIP_Postal_Code__c = pfs.KS_Household_ZIP_Postal_Code__c;
            pfs.KS_Mailing_Country__c = pfs.KS_Household_Country__c;
        }
        return null;
    }
    //NAIS-1822 End
}