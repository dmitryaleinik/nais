@isTest
private class FamilyAwardsServiceTest {
    
    private static Account school_1, school_2;
    private static Contact parent, student_1, student_2;
    private static PFS__c pfs;
    private static Applicant__c applicant_1, applicant_2;
    private static Student_Folder__c folder_1, folder_2, folder_3, folder_4;
    private static School_PFS_Assignment__c spa_1, spa_2, spa_3, spa_4;
    private static Contact staff_1, staff_2;
    private static User staffUser_1, staffUser_2, parentUser;
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    
    private static void createParentUser() {
        
        System.runAs(thisUser){
            
            parentUser = UserTestData.createFamilyPortalUser();
            insert parentUser;
        }
        
        Id contactId = [SELECT Id, ContactId FROM User WHERE Id =: parentUser.Id].ContactId;
        parent = [SELECT Id, FirstName, LastName FROM Contact WHERE Id =: contactId];
        parent.FirstName = 'Steven';
        parent.LastName = 'Tyler';
        update parent;
        
    }//End:createParentUser    
    
    private static void setupData() {
        
        setupData(true);        
    }//End:setupData
    
    private static void setupData(Boolean runSharingForSchools) {
        
        createParentUser();
        
        Academic_Year__c academicYear = AcademicYearTestData.Instance.insertAcademicYear();
        
        //Create Schools
        school_1 = AccountTestData.Instance
            .forName('School_1')
            .asSchool()
            .create();
        school_2 = AccountTestData.Instance
            .forName('School_2')
            .asSchool()
            .create();
        insert new List<Account>{school_1, school_2};
        
        //Create contacts for parents and students.
		student_1 = ContactTestData.Instance
		  .asStudent()
		  .forFirstName('Liv')
		  .forLastName('Tyler')
		  .create();
        student_2 = ContactTestData.Instance
          .asStudent()
          .forFirstName('Mia')
          .forLastName('Tyler')
          .create();
        staff_1 = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(school_1.Id)
            .create();
        staff_2 = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(school_2.Id)
            .create();
		insert new List<Contact> { student_1, student_2, staff_1, staff_2 };
		
        //Create PFS records.
		pfs = PfsTestData.Instance
		    .forParentA(parent.Id)
		    .forAcademicYearPicklist(academicYear.Name)
		    .asPaid()
		    .insertPfs();
		    
	    //Create applicants.
		applicant_1 = ApplicantTestData.Instance
		    .forContactId(student_1.Id)
		    .forPfsId(pfs.Id)
		    .create();
		applicant_2 = ApplicantTestData.Instance
		    .forContactId(student_2.Id)
		    .forPfsId(pfs.Id)
		    .create();
		insert new List<Applicant__c>{ applicant_1, applicant_2 };
		
		//Create Folders
		folder_1 = StudentFolderTestData.Instance.forName('Folder: Applicant_1-School_1')
		    .forAcademicYearPicklist(academicYear.Name)
		    .forStudentId(student_1.Id)
		    .forSchoolId(school_1.Id)
		    .create();
	    folder_2 = StudentFolderTestData.Instance.forName('Folder: Applicant_1-School_2')
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student_1.Id)
            .forSchoolId(school_2.Id)
            .create();
        folder_3 = StudentFolderTestData.Instance.forName('Folder: Applicant_1-School_1')
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student_2.Id)
            .forSchoolId(school_1.Id)
            .create();
        folder_4 = StudentFolderTestData.Instance.forName('Folder: Applicant_1-School_2')
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student_2.Id)
            .forSchoolId(school_2.Id)
            .create();
        insert new List<Student_Folder__c>{ folder_1, folder_2, folder_3, folder_4 };
        
        //Create SPA records
		spa_1 = SchoolPfsAssignmentTestData.Instance
		    .forAcademicYearPicklist(academicYear.Name)
		    .forApplicantId(applicant_1.Id)
		    .forSchoolId(school_1.Id)
		    .forStudentFolderId(folder_1.Id)
		    .create();
	    spa_2 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forApplicantId(applicant_1.Id)
            .forSchoolId(school_2.Id)
            .forStudentFolderId(folder_2.Id)
            .create();
        spa_3 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forApplicantId(applicant_2.Id)
            .forSchoolId(school_1.Id)
            .forStudentFolderId(folder_3.Id)
            .create();
        spa_4 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forApplicantId(applicant_2.Id)
            .forSchoolId(school_2.Id)
            .forStudentFolderId(folder_4.Id)
            .create();
        insert new List<School_PFS_Assignment__c>{ spa_1, spa_2, spa_3, spa_4 };
        
        
        System.runAs(thisUser){
            
            staffUser_1 = UserTestData.Instance
                .forUsername('Staff_1@test.com')
                .forContactId(staff_1.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId)
                .create();
            staffUser_2 = UserTestData.Instance
                .forUsername('Staff_2@test.com')
                .forContactId(staff_2.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId)
                .create();
            insert new List<User>{ staffUser_1, staffUser_2 };
            
            if (runSharingForSchools) {
                
	            Test.startTest();                
	                // call scheduled batch Apex to process new access pfs and folders.
	                Database.executeBatch(new SchoolStaffShareBatch());
	            Test.stopTest();
            }
        }
        
    }//End:setupData
    
    @isTest 
    private static void isAwardLinkEnabledForCurrentSchool_currentSchoolHasNotPremiumFeature_expectAwardLinkDisabled() {
        
        setupData();
        
        FeatureToggles.setToggle('Allow_Mass_Email_Per_Applicant__c', true);
        
        School_1.Family_Award_Acknowledgement_Enabled__c = false;
        update School_1;
        
        System.runAs(staffUser_1){
            
            System.AssertEquals(true, FeatureToggles.isEnabled('Allow_Mass_Email_Per_Applicant__c'));
            
            System.AssertEquals(false, GlobalVariables.getCurrentSchool().Family_Award_Acknowledgement_Enabled__c);
            
            System.AssertEquals(false, FamilyAwardsService.Instance.isAwardLinkEnabledForCurrentSchool());
            
        }
    }//End:isAwardLinkEnabledForCurrentSchool_currentSchoolHasNotPremiumFeature_expectAwardLinkDisabled
    
    @isTest 
    private static void isAwardLinkEnabledForCurrentSchool_currentSchoolHasPremiumFeature_expectAwardLinkEnabled() {
        
        setupData();
        
        FeatureToggles.setToggle('Allow_Mass_Email_Per_Applicant__c', true);
        
        School_1.Family_Award_Acknowledgement_Enabled__c = true;
        update School_1;
        
        System.runAs(staffUser_1){
            
            System.AssertEquals(true, FeatureToggles.isEnabled('Allow_Mass_Email_Per_Applicant__c'));
            
            System.AssertEquals(true, GlobalVariables.getCurrentSchool().Family_Award_Acknowledgement_Enabled__c);
            
            System.AssertEquals(true, FamilyAwardsService.Instance.isAwardLinkEnabledForCurrentSchool());
            
        }
    }//End:isAwardLinkEnabledForCurrentSchool_currentSchoolHasPremiumFeature_expectAwardLinkEnabled
    
    @isTest 
    private static void showAwardsTabForPFS_toggleEnabledAndSchoolFeatureDisabled_expectAwardsTabHidden() {
        
        setupData();
        
        FeatureToggles.setToggle('Allow_Mass_Email_Per_Applicant__c', true);
        
        School_1.Family_Award_Acknowledgement_Enabled__c = false;
        update School_1;
        
        folder_1.Folder_Status__c = 'Award Approved';
        folder_1.Award_Letter_Sent__c = 'Yes';
        update folder_1;
        
        System.AssertEquals(false, FamilyAwardsService.Instance.showAwardsTabForPFS(pfs.Id));
        
    }//End:showAwardsTabForPFS_toggleEnabledAndSchoolFeatureDisabled_expectAwardsTabHidden
    
    @isTest 
    private static void showAwardsTabForPFS_toggleAndSchoolFeatureEnabled_expectAwardsTabVisible() {
        
        setupData(false);
        
        FeatureToggles.setToggle('Allow_Mass_Email_Per_Applicant__c', true);
        
        School_1.Family_Award_Acknowledgement_Enabled__c = true;
        update School_1;
        
        folder_1.Folder_Status__c = 'Award Approved';
        folder_1.Award_Letter_Sent__c = 'Yes';
        update folder_1;
        
        System.AssertEquals(true, FamilyAwardsService.Instance.showAwardsTabForPFS(pfs.Id));
        
    }//End:showAwardsTabForPFS_toggleAndSchoolFeatureEnabled_expectAwardsTabVisible
    
    @isTest
    private static void requestResponse_applicantsWithAwards_expectAcceptAwards() {
        
        setupData(false);
        
        FeatureToggles.setToggle('Allow_Mass_Email_Per_Applicant__c', true);
        
        School_1.Family_Award_Acknowledgement_Enabled__c = true;
        School_2.Family_Award_Acknowledgement_Enabled__c = true;
        update new List<Account>{ School_1, School_2 };
        
        folder_1.Folder_Status__c = 'Award Approved';
        folder_1.Award_Letter_Sent__c = 'Yes';
        folder_2.Folder_Status__c = 'Complete, Award Denied';
        folder_2.Award_Letter_Sent__c = 'Yes';
        folder_3.Folder_Status__c = 'Award Approved';
        folder_3.Award_Letter_Sent__c = 'Yes';
        folder_4.Folder_Status__c = 'Award Approved';
        folder_4.Award_Letter_Sent__c = 'Yes';
        update new List<Student_Folder__c>{ folder_1, folder_2, folder_3, folder_4 };
        
        Test.StartTest();


            List<FamilyAwardsService.ApplicantsWithAwardsWrapper> applicantsInfo = FamilyAwardsService.Instance.getApplicantsWithAwards(pfs.Id, null);
            Map<Id, Student_Folder__c> folders = FamilyAwardsService.Instance.foldersMap;


	        
	        System.AssertEquals(4, folders.size());
	        
	        System.AssertEquals(2, applicantsInfo.size());
	        
	        System.AssertEquals(2, applicantsInfo[0].awards.size());
	        
	        System.AssertEquals(2, applicantsInfo[1].awards.size());
	        
	        System.AssertEquals(true, applicantsInfo[0].awards[0].getShowAcceptAndDenyButton());
	        
	        System.AssertEquals(false, applicantsInfo[0].awards[1].getShowAcceptAndDenyButton());
	        
        Test.StopTest();
        
    }//End:requestResponse_applicantsWithAwards_expectAcceptAwards
}