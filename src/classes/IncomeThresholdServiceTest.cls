@isTest
private class IncomeThresholdServiceTest {
    
    @isTest private static void getIncomeThreshold_for_DefaultAcademicYear() {
        List<Household_Income_Threshold__c> result = HouseholdIncomeThresholdTestData.Instance.insertIncomeThresholds();
        Id academicYearId = result[0].Academic_Year__c;
        
        system.assertEquals(45000, IncomeThresholdService.Instance.getIncomeThreshold(1, academicYearId));
        system.assertEquals(55000, IncomeThresholdService.Instance.getIncomeThreshold(2, academicYearId));
        system.assertEquals(60000, IncomeThresholdService.Instance.getIncomeThreshold(3, academicYearId));
        system.assertEquals(70000, IncomeThresholdService.Instance.getIncomeThreshold(4, academicYearId));
        system.assertEquals(80000, IncomeThresholdService.Instance.getIncomeThreshold(5, academicYearId));
        system.assertEquals(90000, IncomeThresholdService.Instance.getIncomeThreshold(6, academicYearId));
        system.assertEquals(100000, IncomeThresholdService.Instance.getIncomeThreshold(7, academicYearId));
        system.assertEquals(110000, IncomeThresholdService.Instance.getIncomeThreshold(8, academicYearId));
        system.assertEquals(120000, IncomeThresholdService.Instance.getIncomeThreshold(9, academicYearId));
        system.assertEquals(130000, IncomeThresholdService.Instance.getIncomeThreshold(10, academicYearId));
    }
}