/**
 * SchoolCreateMessageControllerTest.cls
 *
 * @description: Test class for SchoolCreateMessageController using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
private class SchoolCreateMessageControllerTest {
    
    /* test data setup */
    @testSetup 
    static void setupTestData() {
        
        MassEmailSendTestDataFactory.createEmailTemplate();
    }

    /* postive test cases */

    // test valid init, valid Mass ES, return to dashboard
    @isTest 
    static void init_validUserEditExisting_validInitAndNavBack() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();

                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();
                Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                                    Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                                    Html_Body__c, Footer__c, Reply_To_Address__c
                                               from Mass_Email_Send__c
                                              where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
                
                PageReference createMsgPRef = Page.SchoolCreateMessage;
                createMsgPRef.getParameters().put( SchoolCreateMessageController.QUERY_STRING_MESSAGE_DETAIL, massES.Id);
                Test.setCurrentPage( createMsgPRef);
                SchoolCreateMessageController createMsgController = new SchoolCreateMessageController();
                Boolean enabled = createMsgController.massEmailEnabled;
                createMsgController.navigateBackToDashboard();
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, createMsgController.massESModel);
        }
    }

    // test valid init, valid Mass ES, save as draft, nav back to recips
    @isTest 
    static void saveAsDraft_validUserEditExisting_validInitAndNavigate() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();

                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();
                Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                                    Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                                    Html_Body__c, Footer__c, Reply_To_Address__c
                                               from Mass_Email_Send__c
                                              where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
                
                PageReference createMsgPRef = Page.SchoolCreateMessage;
                createMsgPRef.getParameters().put( SchoolCreateMessageController.QUERY_STRING_MESSAGE_DETAIL, massES.Id);
                Test.setCurrentPage( createMsgPRef);
                SchoolCreateMessageController createMsgController = new SchoolCreateMessageController();
                createMsgController.saveAsDraft();
                createMsgController.navigateBackToRecips();
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, createMsgController.massESModel);
        }
    }

    // test valid init, valid Mass ES and execute/hide preview
    @isTest 
    static void executeAndHidePreview_validUserExecutePreview_validInitAndPreview() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();

                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();
                Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                                    Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                                    Html_Body__c, Footer__c, Reply_To_Address__c
                                               from Mass_Email_Send__c
                                              where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
                
                PageReference createMsgPRef = Page.SchoolCreateMessage;
                createMsgPRef.getParameters().put( SchoolCreateMessageController.QUERY_STRING_MESSAGE_DETAIL, massES.Id);
                Test.setCurrentPage( createMsgPRef);
                SchoolCreateMessageController createMsgController = new SchoolCreateMessageController();
                createMsgController.executePreview();
                createMsgController.hidePreview();
                createMsgController.sendEmail();
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, createMsgController.massESModel);
            System.assertEquals( false, createMsgController.renderPreview);
        }
    }

    // test valid init, valid Mass ES and execute/hide preview
    @isTest 
    static void sendTestEmail_validUser_validInitAndTestSend() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            MassEmailSendTestDataFactory.createMassEmailMergeSettings();
            Test.startTest();

                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();
                Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                                    Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                                    Html_Body__c, Footer__c, Reply_To_Address__c
                                               from Mass_Email_Send__c
                                              where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
                
                PageReference createMsgPRef = Page.SchoolCreateMessage;
                createMsgPRef.getParameters().put( SchoolCreateMessageController.QUERY_STRING_MESSAGE_DETAIL, massES.Id);
                Test.setCurrentPage( createMsgPRef);
                SchoolCreateMessageController createMsgController = new SchoolCreateMessageController();
                SchoolCreateMessageController.RemoteResponseModel remoteResponse = SchoolCreateMessageController.getAdvancedEditorDetails();
                createMsgController.sendTestEmail();
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, createMsgController.massESModel);
            System.assertNotEquals( null, remoteResponse);
            System.assertEquals( false, createMsgController.renderPreview);
            System.assertEquals( true, createMsgController.mergeSuccess);
        }
    }

}