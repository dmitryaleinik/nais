public with sharing class ContactAction {
    public static Boolean isFirstRun = true;
    
    // [CH] NAIS-2442 Moved to consolidate classes
    public static Boolean isCopyingParentA = false; // to prevent trigger recursive calls
    public static Boolean isCopyingEmailToUserName = false;
    
    public static Map <Id, Contact> newContactMap;
    
    public static Set <String> uniqueEmailContactIdSet = new Set <String>();
    public static String errorMessage;

    public class UsernameException extends Exception {}
    
    /**
    * @description Method implemented to determine if the account related to a deleted 
    * contact should also be deleted.
    * @return true, if the contact's account should be deteled. Otherwise, false.
    */
    public static boolean shouldDeleteRelatedContactAccount(Contact record)
    {
        return (record.AccountId!=null && record.MasterRecordId == null && 
                (record.RecordTypeId == RecordTypes.studentContactTypeId 
                    || record.RecordTypeId == RecordTypes.parentContactTypeId)
                ?true:false);
    }//End:shouldDeleteRelatedContactAccount
    
     /**
    * @description Method implemented to deleted the account related to a deleted contact.
    */
    public static void deleteRelatedAccountForFamilyContact(List<Id> accountIds)
    {
        List<Account> accountsToDelete = new List<Account>([Select Id from Account where 
                                                Id IN: accountIds    
                                                and RecordTypeId=:RecordTypes.individualAccountTypeId
                                                and Name<>'Individual']);
        if(!accountsToDelete.isEmpty()){
            delete accountsToDelete;
        }
    }//End:deleteRelatedAccountForFamilyContact
    
    public static void createBucketAccountForFamilyContact(List<Contact> allContactsInInsert)
    {
        createBucketAccountForFamilyContact(allContactsInInsert,false);
    }
    /**
    * @description Method implemented to create an individual account every time a FP contact is created.
    * @param allContactsInInsert The list of new contacts to be created.
    */
    public static void createBucketAccountForFamilyContact(List<Contact> allContactsInInsert, Boolean isBatch)
    {
        if (isFirstRun){
            isFirstRun = false;    
            Map<Contact, Account> accountByContact = new Map<Contact, Account>();
            Map<Contact, Household__c> householdByContact = new Map<Contact, Household__c>();
            Id ownerId = GlobalVariables.getIndyOwner();
            
            // STEP 01: Identify the FP contacts and create its Account object.
            for(Contact c:allContactsInInsert){
                if ((isBatch || c.AccountId == null) && (c.RecordTypeId == RecordTypes.studentContactTypeId || c.RecordTypeId == RecordTypes.parentContactTypeId)) {
                    accountByContact.put(c, getBucketAccountForFamilyContact(c, ownerId));
                    
                    if (c.Household__c == null && c.RecordTypeId == RecordTypes.parentContactTypeId) {
                        householdByContact.put(c, new Household__c(Name = c.FirstName + ' ' + c.LastName + ' Household'));
                    }
                }
            }
            
            // STEP 02: Insert the new accounts for each FP contact. And link the Contact with its new Account (through Contact.AccountId)
            if(!accountByContact.isEmpty()){
                insert accountByContact.values();
            
                for(Contact c:accountByContact.keyset()){
                    if((accountByContact.get(c)).Id!=null)
                        c.AccountId = (accountByContact.get(c)).Id;
                }
            }
            
            // STEP 03: Insert the missing households for the new Contacts. This is needed when the contact is created through standard Salesforce.
            if(!householdByContact.isEmpty()) {
                
                insert householdByContact.values();
                
                for(Contact cc : householdByContact.keySet()) {
                    
                    cc.Household__c = householdByContact.get(cc).Id;
                }
            }
            
        }
    }//End:createBucketAccountForFamilyContact
    
    /**
    * @description Method implemented to initialize the Account record that will
    * be linked to a new Family Portal contact.
    * @param c The contact record that will be related to the new Account record.
    * @param ownerId The Id of the owner of the new Account record
    *
    * @return The new Account record.
    */
    public static Account getBucketAccountForFamilyContact(Contact c, Id ownerId)
    {
        return createIndividualAccountForContact(c, ownerId);
    }//End:getBucketAccountForFamilyContact
    
    public static Account createIndividualAccountForContact(Contact c, Id ownerId) {
        
        ownerId = (ownerId == null ? GlobalVariables.getIndyOwner() : ownerId);
        return new Account(Name = c.FirstName+' '+c.LastName+' Account',
                                                        RecordTypeId = RecordTypes.individualAccountTypeId,
                                                        OwnerId = ownerId);
    }//End:createIndividualAccountForContact
    
    /*
    * SPEC-026, NAIS-35
    *
    * The email address (preferred email) of Parent A needs to be copied onto:
    * School PFS Assignment
    * Family Document
    * Opportunity
    * Application Fee Waiver
    * School Document Assignment
    * Transaction Line Item
    *
    * Nathan, Exponent Partners, 2013
    */
    
    // [CH] NAIS-2442 Moved to consolidate classes
    public static void CopyParentA(List<Contact> contactNewList){
        // start trigger execution
        isCopyingParentA = true;
    
        Map <String, List <PFS__c>> contactId_pfsList_map = new Map <String, List <PFS__c>>();
        Map <String, List <Opportunity>> pfsId_opptyList_map = new Map <String, List <Opportunity>>();
        Map <String, List <Transaction_Line_Item__c>> opptyId_tliList_map = new Map <String, List <Transaction_Line_Item__c>>();
        Map <String, List <Application_Fee_Waiver__c>> contactId_afwList_map = new Map <String, List <Application_Fee_Waiver__c>>();
        Map <String, List <Applicant__c>> pfsId_applicantList_map = new Map <String, List <Applicant__c>>();
        Map <String, List <School_PFS_Assignment__c>> applicantId_spfsaList_map = new Map <String, List <School_PFS_Assignment__c>>();
        Map <String, List <School_Document_Assignment__c>> spfsaId_sdaList_map = new Map <String, List <School_Document_Assignment__c>>();
        Map <String, Family_Document__c> sdaId_familyDocument_map = new Map <String, Family_Document__c>();
        
        Set <String> parentAContactIdSet = new Set <String>();
        Set <String> opptyIdSet = new Set <String>();
        Set <String> pfsIdSet = new Set <String>();
        Set <String> applicantIdSet = new Set <String>();
        Set <String> spfsaIdSet = new Set <String>();
        Set <String> familyDocumentIdSet = new Set <String>();    // to prevent addind dupes in list
        
        List <PFS__c> pfsToUpdateList = new List<PFS__c>();
        List <Opportunity> opptyUpdateList = new List <Opportunity>();
        List <Transaction_Line_Item__c> tliUpdateList = new List <Transaction_Line_Item__c>();
        List <Application_Fee_Waiver__c> afwUpdateList = new List <Application_Fee_Waiver__c>();
        List <School_PFS_Assignment__c> spfsaUpdateList = new List <School_PFS_Assignment__c>();
        List <School_Document_Assignment__c> sdaUpdateList = new List <School_Document_Assignment__c>();
        List <Family_Document__c> fdUpdateList = new List <Family_Document__c>();
        
        if (contactNewList == null || contactNewList.size() == 0) {
            return;
        }

        // collect all parent A pfs and opportinities
        List <Contact> conList = [select Id, Name, Email,
            (select Id, Name, Parent_A__c, Parent_A_Email__c from PFSs_Parent_A__r),
            (select Id, Name, Parent_A_Email__c from Application_Fee_Waivers__r)
            from Contact where Id in :contactNewList];
            
        for (Contact con : conList) {
            contactId_pfsList_map.put(con.Id, con.PFSs_Parent_A__r);
            contactId_afwList_map.put(con.Id, con.Application_Fee_Waivers__r);
            
            if (con.PFSs_Parent_A__r.size() > 0) {
                parentAContactIdSet.add(con.Id);
            }
            for (PFS__c pfs : con.PFSs_Parent_A__r) {
                pfsIdSet.add(pfs.Id);
            }
        }
        
        
        // no incoming parent contacts. return
        if (parentAContactIdSet.size() == 0) {
            return;
        }
        
        // collect all applicants
        List <PFS__c> pfsList = [select Id, Name, PFS__c.Parent_A_Email__c,
            (select Id, Name, PFS__c from Applicants__r),
            (select Id, Name, Parent_A_Email__c from Opportunities__r)
            from PFS__c where Id in :pfsIdSet]; 
        
        for (PFS__c pfs : pfsList) {
            pfsId_applicantList_map.put(pfs.Id, pfs.Applicants__r);
            pfsId_opptyList_map.put(pfs.Id, pfs.Opportunities__r);
            for (Applicant__c app : pfs.Applicants__r) {
                applicantIdSet.add(app.Id);
            }
            for (Opportunity oppty : pfs.Opportunities__r) {
                opptyIdSet.add(oppty.Id);
            }
        }
                
        // collect all transaction line items
        List <Opportunity> opptyList = [select Id, Name,
            (select Id, Name, Parent_A_Email__c from Transaction_Line_Items__r)
            from Opportunity where Id in :opptyIdSet];

        for (Opportunity oppty : opptyList) {
            opptyId_tliList_map.put(oppty.Id, oppty.Transaction_Line_Items__r);
        }            
        
        // collect all school pfs assignments
        List <Applicant__c> appList = [select Id, Name,
            (select Id, Name, Applicant__c, Parent_A_Email__c from School_PFS_Assignments__r)
            from Applicant__c where Id in :applicantIdSet];

        for (Applicant__c app : appList) {
            applicantId_spfsaList_map.put(app.Id, app.School_PFS_Assignments__r);
            for (School_PFS_Assignment__c spfsa : app.School_PFS_Assignments__r) {
                spfsaIdSet.add(spfsa.Id);
            }
        } 

        // collect all school document assignment
        List <School_PFS_Assignment__c> spfsaList = [select Id, Name,
            (select Id, Name, Document__c, Parent_A_Email__c, Document__r.Parent_A_Email__c  
                from School_Document_Assignments__r)
            from School_PFS_Assignment__c where Id in :spfsaIdSet];
        
        for (School_PFS_Assignment__c spfsa : spfsaList) {
            spfsaId_sdaList_map.put(spfsa.Id, spfsa.School_Document_Assignments__r);
            for (School_Document_Assignment__c sda : spfsa.School_Document_Assignments__r) {
                if (sda.Document__c != null) {
                    sdaId_familyDocument_map.put(sda.Id, sda.Document__r);
                }
            }
        } 
           
        for (Contact con : contactNewList) {
            
            // if contact is not parent A then dont copy
            if (parentAContactIdSet.contains(con.Id) == false) {
                continue;
            }
            
            // set Parent_A_Email__c for Application_Fee_Waiver__c 
            if (contactId_afwList_map.containsKey(con.Id)) {
                for (Application_Fee_Waiver__c afw : contactId_afwList_map.get(con.Id)) {
                    if (afw.Parent_A_Email__c != con.Email) {
                        afw.Parent_A_Email__c = con.Email;
                        afwUpdateList.add(afw);
                    }
                }
            }
            
            if (contactId_pfsList_map.containsKey(con.Id)) {
                for (PFS__c pfs : contactId_pfsList_map.get(con.Id)) {
                    if (pfs.Parent_A_Email__c != con.Email){
                        pfs.Parent_A_Email__c = con.Email;
                        pfsToUpdateList.add(pfs);
                    }
                    
                    // set Parent_A_Email__c for opportunity
                    if (pfsId_opptyList_map.containsKey(pfs.Id)) {
                        for (Opportunity oppty : pfsId_opptyList_map.get(pfs.Id)) {
                            if (oppty.Parent_A_Email__c != con.Email) {
                                oppty.Parent_A_Email__c = con.Email;
                                opptyUpdateList.add(oppty);
                            }

                            // set Parent_A_Email__c  from Transaction_Line_Item__c
                            if (opptyId_tliList_map.containsKey(oppty.Id)) {
                                for (Transaction_Line_Item__c tli : opptyId_tliList_map.get(oppty.Id)) {
                                    if (tli.Parent_A_Email__c != con.Email) {
                                        tli.Parent_A_Email__c = con.Email;
                                        tliUpdateList.add(tli);
                                    }
                                }
                            }
                        }
                    }
                    
                    if (pfsId_applicantList_map.containsKey(pfs.Id)) {
                        for (Applicant__c app : pfsId_applicantList_map.get(pfs.Id)) {
                            if (applicantId_spfsaList_map.containsKey(app.Id)) {
                                
                                // set Parent_A_Email__c for School_PFS_Assignment__c
                                for (School_PFS_Assignment__c spfsa : applicantId_spfsaList_map.get(app.Id)) {
                                    if (spfsa.Parent_A_Email__c != con.Email) {
                                        spfsa.Parent_A_Email__c = con.Email;
                                        spfsaUpdateList.add(spfsa);
                                    }
                                    
                                    // set Parent_A_Email__c for School_Document_Assignment__c
                                    if (spfsaId_sdaList_map.containsKey(spfsa.Id)) {
                                        for (School_Document_Assignment__c sda : spfsaId_sdaList_map.get(spfsa.Id)) {
                                            if (sda.Parent_A_Email__c != con.Email) {
                                                sda.Parent_A_Email__c = con.Email;
                                                sdaUpdateList.add(sda);
                                            }
                                            
                                            // set Parent_A_Email__c for Family_Document__c 
                                            if (sdaId_familyDocument_map.containsKey(sda.Id)) {
                                                Family_Document__c fd = sdaId_familyDocument_map.get(sda.Id);
                                                if (fd.Parent_A_Email__c != con.Email) {
                                                    fd.Parent_A_Email__c = con.Email;
                                                    // add only once to update list
                                                    if (familyDocumentIdSet.add(fd.Id)) {
                                                        fdUpdateList.add(fd);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } 
            }
        } 
        
        //System.assertEquals(null, opptyUpdateList);
        
        // all updates
        if (pfsToUpdateList.size() > 0) {
            update pfsToUpdateList;
        } 
        if (opptyUpdateList.size() > 0) {
            update opptyUpdateList;
        } 
        if (tliUpdateList.size() > 0) {
            update tliUpdateList;
        }
        if (afwUpdateList.size() > 0) {
            update afwUpdateList;
        }   
        if (spfsaUpdateList.size() > 0) {
            update spfsaUpdateList;
        } 
        if (sdaUpdateList.size() > 0) {
            update sdaUpdateList;
        }  
        if (fdUpdateList.size() > 0) {
            update fdUpdateList;
        }    

        // end trigger execution
        isCopyingParentA = false;
    }

    /*
    * SPEC-133, NAIS-36
    * We need a trigger to set the username and user email address when the family user email address changes on the PFS
    * Nathan, Exponent Partners, 2013
    */
    // [CH] NAIS-2442 Moved to consolidate classes
    public static void validateEmailToUsername() {

        Map <String, List <Contact>> email_contactList_map = new Map <String, List <Contact>>();
        Map <String, List <Contact>> username_contactList_map = new Map <String, List <Contact>>();
        Set <String> usernameSet = new Set <String>(); 
        String usernameSuffix;
        
        // get username suffix from custom setting
        List <Portal_Username_Suffix_Setting__c> csList = Portal_Username_Suffix_Setting__c.getAll().values();
        for (Portal_Username_Suffix_Setting__c cs : csList) {
            if (cs.Value__c != null && cs.Active__c == true) {
                if (cs.Name == 'Username_Suffix') {
                    usernameSuffix = cs.Value__c;
                }
            }
        }
        
        // load email to contact list map
        if (newContactMap != null) {    
            for (Contact con : newContactMap.values()) {
                if (con.Email != null) {
                    List <Contact> conList;
                    if (email_contactList_map.containsKey(con.Email)) {
                        conList = email_contactList_map.get(con.Email);
                    } else {
                        conList = new List <Contact>();
                    }
                    conList.add(con);
                    email_contactList_map.put(con.Email, conList);
                    
                    String username = con.Email + (usernameSuffix != null ? usernameSuffix : '');
                    username_contactList_map.put(username, conList);
                }
            }
        }
        
        // filter input for duplicate emails
        for (String email : email_contactList_map.keySet()) {
            List <Contact> contactList = email_contactList_map.get(email);
            if (contactList.size() > 1) {
                for (Contact con : contactList) {
                    con.addError('Duplicate email: ' + con.Email);
                }
            } else {
                uniqueEmailContactIdSet.add(contactList[0].Id);
                usernameSet.add(contactList[0].Email + (usernameSuffix != null ? usernameSuffix : ''));
            }
        }
        
        // nothing to process
        if (uniqueEmailContactIdSet.size() == 0) {
            return;
        }

        // verify if username already exists
        List <User> userList = [select Id, Name, ContactId, Contact.Email, Contact.Name, UserName, SpringCMEos__SpringCM_User__c
            from User
            where IsActive = true and
                IsPortalEnabled = true and
                UserName in :usernameSet and
                ContactId != null];

        // throw error if username already exists
        for (User u : userList) {
            if (username_contactList_map.containsKey(u.Username)) {
                List <Contact> conList = username_contactList_map.get(u.Username);
                for (Contact con : conList) {
                    // [dp] 6.25.14 NAIS-1702 don't throw the error if this user belongs to this contact
                    if (con.Id != u.ContactId){
                        String errMsg = 'A user with this email ' + con.Email + ' already exists. Please contact support to update your email address.'; 
                        errorMessage = errMsg;
                        con.addError(errMsg);
                        throw new UsernameException(errMsg);
                    }
                }                
            }
        }
    }
    
    @future (callout=true)
    public static void CopyFuture(Set <String> contactIdSet, String sessionId) {
        Copy(contactIdSet, sessionId);
    }
        
    public static void Copy(Set <String> contactIdSet, String sessionId) {

        if (contactIdSet == null || contactIdSet.size() == 0) {
            return;
        }
        
        // start trigger execution
        isCopyingEmailToUserName = true;

        Map <String, User> contactId_user_map = new Map <String, User>();
        Map <String, Contact> userId_contact_map = new Map <String, Contact>();
        List <User> userUpdateList = new List <User>();
        String usernameSuffix;
        
        // get username suffix from custom setting
        List <Portal_Username_Suffix_Setting__c> csList = Portal_Username_Suffix_Setting__c.getAll().values();
        for (Portal_Username_Suffix_Setting__c cs : csList) {
            if (cs.Value__c != null && cs.Active__c == true) {
                if (cs.Name == 'Username_Suffix') {
                    usernameSuffix = cs.Value__c;
                }
            }
        }
        
        // get portal users
        List <User> userList = [select Id, Name, ContactId, Contact.Name, Email, UserName, SpringCMEos__SpringCM_User__c
            from User
            where IsActive = true and
                IsPortalEnabled = true and
                ContactId in :contactIdSet];

        // no portal users affected
        if (userList.size() == 0) {
            isCopyingEmailToUserName = false;
            return;
        }

        for (User u : userList) {
            contactId_user_map.put(u.ContactId, u);
            userId_contact_map.put(u.Id, u.Contact);
        }                

        // for calling the UserAction.sendEmailUpdateXMLToSpring method
        Map<String, String> newEmailToOldemailMap = new Map<String, String>();

        // set user name
        for (Contact con : [select Id, Email from Contact where Id in :contactIdSet]) {
            if (con.Email == null) {
                continue;
            }
            
            if (contactId_user_map.containsKey(con.Id)) {
                User usr = contactId_user_map.get(con.Id);
                
                String username = con.Email + (usernameSuffix != null ? usernameSuffix : '');
                if (usr.UserName != username || usr.Email != con.Email) {
                    if (usr.Email != con.Email && usr.SpringCMEos__SpringCM_User__c){
                        // [DP} 6.27.14 Changed to send SpringCM the user.email as opposed to the user.username
                        newEmailToOldemailMap.put(con.Email, usr.Email);
                    }
                    usr.UserName = username;
                    usr.Email = con.Email;
                    userUpdateList.add(usr);
                }
            }
        }

        // update users
        if (userUpdateList.size() > 0) {
            // The optional opt_allOrNone parameter specifies whether the operation allows partial success. If you specify 
            // false for this parameter and a record fails, the remainder of the DML operation can still succeed.
            try{
                // send new emails to SpringCM
                if (newEmailToOldemailMap.size() > 0){
                    //UserAction.sendEmailUpdateXMLToSpringSYNCHRONOUS(newEmailToOldemailMap, UserInfo.getSessionId());
                    // try catch loop here? [DP]
                    UserAction.sendEmailUpdateXMLToSpringSYNCHRONOUS(newEmailToOldemailMap, sessionId);
                } 
            } catch (Exception e){
                System.debug(LoggingLevel.ERROR, 'ERROR SENDING EMAIL UPDATE TO SPRINGCM: ' + e.getMessage());
            }

            // continue with update even if workflow failed
            if (Test.isRunningTest() == false) {
                Database.update(userUpdateList, false); 
            } else {
                // avoid mixed dml operation error
                System.runAs(new User(Id = Userinfo.getUserId())) {
                    Database.update(userUpdateList, false); 
                }
            }
        }
        
        // end trigger execution
        isCopyingEmailToUserName = false;
    }
    
    //SFP#19, [G.S]
    public static void updateSSSMainContact(Map<Id,Contact> newMap, Map<Id,Contact> oldMap){
        Set<Id> schoolIdSet = new Set<Id>();
        if(oldMap != null){
            for(Contact cntct : newMap.values()){
                Contact oldCntct = oldMap.get(cntct.Id);
                if(cntct.SSS_Main_Contact__c && oldCntct.SSS_Main_Contact__c != cntct.SSS_Main_Contact__c && cntct.RecordTypeId == RecordTypes.schoolStaffContactTypeId && string.isNotBlank(cntct.AccountId)){
                    schoolIdSet.add(cntct.AccountId);
                }
            }
        }
        else{
            for(Contact cntct : newMap.values()){
                if(cntct.SSS_Main_Contact__c && cntct.RecordTypeId == RecordTypes.schoolStaffContactTypeId  && string.isNotBlank(cntct.AccountId)){
                    schoolIdSet.add(cntct.AccountId);
                }
            }
        }
        
        if(!schoolIdSet.isEmpty()){
            Map<Id, List<Contact>> accountContactMap = new Map<Id, List<Contact>>();
            for(Contact cntct :[Select Id,SSS_Main_Contact__c,AccountId from Contact where RecordTypeId=:RecordTypes.schoolStaffContactTypeId and 
                                       AccountId != null and AccountId in :schoolIdSet and SSS_Main_Contact__c = true and Id not in:newMap.values()]){
                if(accountContactMap.containsKey(cntct.AccountId)){
                    cntct.SSS_Main_Contact__c = false;
                    accountContactMap.get(cntct.AccountId).add(cntct);
                }
                else{
                    cntct.SSS_Main_Contact__c = false;
                    accountContactMap.put(cntct.AccountId, new List<Contact>{cntct});
                }                           
            }
            List<Contact> updateContacts = new List<Contact>();
            if(!accountContactMap.isEmpty()){
                for(List<Contact> cntList : accountContactMap.values()){
                    updateContacts.addAll(cntList);
                }
                update updateContacts;
            }
        }
    }
    
    /*
    // [CH] NAIS-1766 Action for adding Users to Groups
    public static void addUsersToGroups(Map<Id, Contact> contactsToAdd){
        List<String> groupNameList = new List<String>();
        
        // Collect the list of AccountIds to use for querying Groups
        for(Contact contact : contactsToAdd.values()){
            groupNameList.add('X' + contact.AccountId);
        }
        
        // Query groups based on DeveloperName and add to a Map
        Map<String, Id> groupsMap = new Map<String, Id>();
        for(Group groupRecord : [select Id, Name, DeveloperName from Group where DeveloperName in :groupNameList]){
            groupsMap.put(groupRecord.DeveloperName, groupRecord.Id);
        }
        
        // Query related User records
        Map<Id, Id> usersMap = new Map<Id, Id>();
        for(User userRecord : [select Id, ContactId from User where ContactId in :contactsToAdd.keySet()]){
            usersMap.put(userRecord.ContactId, userRecord.Id);
        }
        
        // Match users up to groups and create new GroupMembers to be inserted
        List<GroupMember> groupMembersToInsert = new List<GroupMember>();
        for(Contact contact : contactsToAdd.values()){
            Id matchingGroupId = groupsMap.get('X' + contact.AccountId);
            Id matchingUserId = usersMap.get(contact.Id);
            if(matchingGroupId != null){
                GroupMember newMember = new GroupMember();
                newMember.GroupId = matchingGroupId;
                newMember.UserOrGroupId = matchingUserId;
                
                groupMembersToInsert.add(newMember);
            }
        }
        
        // If there are GroupMembers to insert
        Database.insert(groupMembersToInsert);
    }
    */
    
    /*
    // [CH] NAIS-1766 Action for adding Users to Groups
    public static void removeUsersFromGroups(Map<Id, Contact> contactsToRemove){
        
        
            
        List<String> groupNameList = new List<String>();
        
        // Collect the list of AccountIds to use for querying Groups
        for(Contact contact : contactsToRemove.values()){
            groupNameList.add('X' + contact.AccountId);
        }
        
        // Query groups based on DeveloperName and add to a Map
        Map<String, Id> groupsMap = new Map<String, Id>();
        for(Group groupRecord : [select Id, Name, DeveloperName from Group where DeveloperName in :groupNameList]){
            groupsMap.put(groupRecord.DeveloperName, groupRecord.Id);
        }
        
        // Query related User records
        Map<Id, User> usersMap = new Map<Id, User>([select Id from User where ContactId in :contactsToRemove.keySet()]);
        
        // Query the set of GroupMembers to remove
        Map<Id, GroupMember> groupMembersToRemove = new Map<Id, GroupMember>([select Id from GroupMember where Group.DeveloperName in :groupNameList and UserOrGroupId in :usersMap.keySet()]);
        // Deleting records
        Database.delete(new List<Id>(groupMembersToRemove.keySet())); 
    }
    */
}