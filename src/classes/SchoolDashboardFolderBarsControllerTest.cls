@isTest
private class SchoolDashboardFolderBarsControllerTest {
    private without sharing class LocalTestData {
        public Account school;
        public User schoolPortalUser;
        public String academicYearName;
        private List<Contact> studentList;

        public LocalTestData() {
            school = AccountTestData.Instance
                    .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                    .forSSSSubscriberStatus(GlobalVariables.getActiveSSSStatusForTest())
                    .forMaxNumberOfUsers(100)
                    .DefaultAccount;
            Contact schoolStuff = ContactTestData.Instance
                    .forAccount(school.Id)
                    .forRecordTypeId(RecordTypes.schoolStaffContactTypeId)
                    .forEmail('testschoolstaff@school.test' + String.valueOf(Math.random()))
                    .forMailingCountry('United States')
                    .DefaultContact;

            SchoolPortalSettings.Folder_Status_Report_Id = '00Od0000003kvHEEAY';

            Profile_Type_Grouping__c profileTypeGroupingSettings = ProfileTypeGroupingTestData.Instance
                    .forIsSchoolProfile(true)
                    .forProfileName(ProfileSettings.SchoolAdminProfileName)
                    .DefaultProfileTypeGrouping;

            AcademicYearTestData.Instance.insertAcademicYears(5);
            academicYearName = GlobalVariables.getCurrentAcademicYear().Name;

            studentList = new List<contact> {
                ContactTestData.Instance
                        .forFirstName('John')
                        .forLastName('Doe')
                        .forRecordTypeId(RecordTypes.studentContactTypeId)
                        .forEthnicity('Latino/Hispanic American')
                        .createContactWithoutReset(),

                ContactTestData.Instance
                        .forFirstName('Jane')
                        .forLastName('Doe')
                        .forRecordTypeId(RecordTypes.studentContactTypeId)
                        .forEthnicity('Latino/Hispanic American')
                        .createContactWithoutReset()
            };
            insert studentList;
        }

        public void initStudentFolders() {
            insert new List<Student_Folder__c> {
                StudentFolderTestData.Instance
                        .forName('Student Folder 1')
                        .forAcademicYearPicklist(academicYearName)
                        .forStudentId(studentList[0].Id)
                        .forDayBoarding('Day')
                        .forFolderSource('PFS')
                        .forSchoolId(school.Id)
                        .forEthnicity('Caucasian')
                        .createStudentFolderWithoutReset(),

                StudentFolderTestData.Instance
                        .forName('Student Folder 2')
                        .forAcademicYearPicklist(academicYearName)
                        .forStudentId(studentList[1].Id)
                        .forDayBoarding('Day')
                        .forFolderSource('PFS')
                        .forSchoolId(school.Id)
                        .forFolderStatus('Award Approved')
                        .forUseBudgetGroups('No')
                        .forGrantAwarded(5000)
                        .createStudentFolderWithoutReset()
            };
        }
    }

    @isTest private static void testController_validData_resultSize() {
        LocalTestData testData = new LocalTestData();

        System.runAs(CurrentUser.getCurrentUser()) {
            testData.schoolPortalUser = UserTestData.createSchoolPortalUser();
            testData.schoolPortalUser.In_Focus_School__c = testData.school.Id;
            insert testData.schoolPortalUser;
        }

        System.runAs(testData.schoolPortalUser) {
            testData.initStudentFolders();

            List<Student_Folder__c> folders = new List<Student_Folder__c>([
                    SELECT Id, Academic_Year_Picklist__c, School__c
                    FROM Student_Folder__c
                    WHERE School__c = :GlobalVariables.getCurrentSchoolId()
                    AND Academic_Year_Picklist__c = :testData.academicYearName]);
            System.assertEquals(folders[0].Academic_Year_Picklist__c, testData.academicYearName);
            System.assertEquals(folders[0].School__c, testData.school.Id);
            System.assertEquals(GlobalVariables.getCurrentSchoolId(), testData.school.Id);
            System.assertEquals(2, folders.size());

            SchoolDashboardFolderBarsController.FolderBarsWrapper result = new SchoolDashboardFolderBarsController.FolderBarsWrapper();

            Test.startTest();
                SchoolDashboardFolderBarsController controller = new SchoolDashboardFolderBarsController();
                controller.acadYearName = testData.academicYearName;
                System.assertEquals('00Od0000003kvHEEAY', controller.getReportId());
                result = SchoolDashboardFolderBarsController.getSchoolDashboardChartData(testData.academicYearName);
                System.assertEquals(testData.school.Id, GlobalVariables.getCurrentSchoolId());
            Test.stopTest();

            Map<Id, Student_Folder__c> foldersByAcademicYearName = SchoolDashboardFolderBarsController.getFolderIds(testData.academicYearName);
            Map<String, Integer> dataResult = new Map<String, Integer>();
            Integer counter = SchoolDashboardFolderBarsController.mappingFolders(dataResult, foldersByAcademicYearName.keyset());
            List<VFChartData> chartResult = SchoolDashboardFolderBarsController.setChartValues(dataResult);

            System.assertEquals(folders.size(), foldersByAcademicYearName.size());
            System.assertEquals(folders.size(), dataResult.size());
            System.assertEquals(Student_Folder__c.Folder_Status__c.getDescribe().getPicklistValues().size(), result.result.size());
            System.assertEquals(Student_Folder__c.Folder_Status__c.getDescribe().getPicklistValues().size(), chartResult.size());
        }
    }
}