/**
 * SchoolSelectedRecipientsExtensionTest.cls
 *
 * @description: Test class for SchoolSelectedRecipientsExtension using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
private class SchoolSelectedRecipientsExtensionTest {
    
    /* test data setup */
    @testSetup 
    static void setupTestData() {
        
        MassEmailSendTestDataFactory.createEmailTemplate();
    }

    /* positive test cases */

    // test constructor w/ init and no StandardSetController data, no mass ES Id, will display select recips message
    @isTest 
    static void initModelAndView_emptySetController_validInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            Test.startTest();
                
                PageReference selectedRecipPRef = Page.SchoolSelectedRecipients;
                Test.setCurrentPage( selectedRecipPRef);
                SchoolSelectedRecipientsExtension selectedRecipExt = 
                    new SchoolSelectedRecipientsExtension( new ApexPages.StandardSetController( new List<School_PFS_Assignment__c>()));
                selectedRecipExt.initModelAndView();
                Boolean enabled = selectedRecipExt.massEmailEnabled;
            Test.stopTest();

            // Assert
            System.assertEquals( 0, selectedRecipExt.selectedRecipCount);
            System.assertEquals( null, selectedRecipExt.massESModel);
            System.assertEquals( true, selectedRecipExt.initHasRun);
        }
    }

    // test constructor w/ init and valid StandardSetController data, no mass ES Id, will init based on set controller data
    @isTest 
    static void initModelAndView_validSetController_validInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();

                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();
                List<School_PFS_Assignment__c> sPFSList = [select Id from School_PFS_Assignment__c];

                PageReference selectedRecipPRef = Page.SchoolSelectedRecipients;
                ApexPages.StandardSetController setController = new ApexPages.StandardSetController(  new List<School_PFS_Assignment__c>());
                setController.setSelected( sPFSList);
                SchoolSelectedRecipientsExtension selectedRecipExt = 
                    new SchoolSelectedRecipientsExtension( setController);
                selectedRecipExt.initModelAndView();
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, selectedRecipExt.massESModel);
            System.assertEquals( true, selectedRecipExt.initHasRun);
        }
    }

    // test constructor w/ init and no StandardSetController data, valid mass ES Id, will init based on mass ES Id
    @isTest 
    static void initModelAndView_validExistingMassES_validInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();

                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();

                // re-query massES record and instantiate page
                Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                                    Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                                    Html_Body__c, Footer__c, Reply_To_Address__c
                                               from Mass_Email_Send__c
                                              where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];

                PageReference selectedRecipPRef = Page.SchoolSelectedRecipients;
                selectedRecipPRef.getParameters().put( SchoolSelectedRecipientsExtension.QUERY_STRING_MESSAGE_DETAIL, massES.Id);
                Test.setCurrentPage( selectedRecipPRef);
                ApexPages.StandardSetController setController = new ApexPages.StandardSetController(  new List<School_PFS_Assignment__c>());
                SchoolSelectedRecipientsExtension selectedRecipExt = 
                    new SchoolSelectedRecipientsExtension( setController);
                selectedRecipExt.initModelAndView();
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, selectedRecipExt.massESModel);
            System.assertEquals( true, selectedRecipExt.initHasRun);
        }
    }

    // test constructor w/ init and valid StandardSetController data, no mass ES Id, will init based on set controller data
    // remove recipient, save as draft, navigate back to dashboard
    @isTest 
    static void removeRecipAndSave_validSetController_validInitAndSave() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();

                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();
                List<School_PFS_Assignment__c> sPFSList = [select Id from School_PFS_Assignment__c];

                PageReference selectedRecipPRef = Page.SchoolSelectedRecipients;
                ApexPages.StandardSetController setController = new ApexPages.StandardSetController(  new List<School_PFS_Assignment__c>());
                setController.setSelected( sPFSList);
                SchoolSelectedRecipientsExtension selectedRecipExt = 
                    new SchoolSelectedRecipientsExtension( setController);
                selectedRecipExt.initModelAndView();
                selectedRecipExt.removeRecipient();
                PageReference pRef = selectedRecipExt.navigateBackToDashboard();
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, selectedRecipExt.massESModel);
            System.assertEquals( true, selectedRecipExt.initHasRun);
            System.assertNotEquals( null, pRef);
        }
    }

    // test constructor w/ init and valid StandardSetController data, no mass ES Id, will init based on set controller data
    // navigate to next page
    @isTest 
    static void navigateNext_validSetController_validInitAndNavigate() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();

                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();
                List<School_PFS_Assignment__c> sPFSList = [select Id from School_PFS_Assignment__c];

                PageReference selectedRecipPRef = Page.SchoolSelectedRecipients;
                ApexPages.StandardSetController setController = new ApexPages.StandardSetController(  new List<School_PFS_Assignment__c>());
                setController.setSelected( sPFSList);
                SchoolSelectedRecipientsExtension selectedRecipExt = 
                    new SchoolSelectedRecipientsExtension( setController);
                selectedRecipExt.initModelAndView();
                PageReference pRef = selectedRecipExt.navigateNext();
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, selectedRecipExt.massESModel);
            System.assertEquals( true, selectedRecipExt.initHasRun);
            System.assertNotEquals( null, pRef);
        }
    }

    // test constructor w/ init and valid StandardSetController data, no mass ES Id, will init based on set controller data
    // navigate back to dashboard via Back link, fires handleUnload()
    @isTest 
    static void navigateNext_validSetController_validInitAndBackLink() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();

                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();
                List<School_PFS_Assignment__c> sPFSList = [select Id from School_PFS_Assignment__c];

                PageReference selectedRecipPRef = Page.SchoolSelectedRecipients;
                ApexPages.StandardSetController setController = new ApexPages.StandardSetController(  new List<School_PFS_Assignment__c>());
                setController.setSelected( sPFSList);
                SchoolSelectedRecipientsExtension selectedRecipExt = 
                    new SchoolSelectedRecipientsExtension( setController);
                selectedRecipExt.initModelAndView();
                PageReference pRef = selectedRecipExt.handleUnload();
            Test.stopTest();

            // Assert
            System.assertEquals( true, selectedRecipExt.initHasRun);
            System.assertNotEquals( null, pRef);
        }
    }    

}