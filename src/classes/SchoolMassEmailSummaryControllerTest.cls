/**
 * SchoolMassEmailSummaryControllerTest.cls
 *
 * @description: Test class for SchoolMassEmailSummaryController using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
public class SchoolMassEmailSummaryControllerTest {
    
    /* negative test cases */

    // test constructor w/o Student Folder ID
    @isTest 
    static void constructor_noStudentFolderId_noInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            Test.startTest();
                
                SchoolMassEmailSummaryController massEmailSummaryCtrl = new SchoolMassEmailSummaryController();
            Test.stopTest();

            // Assert
            System.assertEquals( null, massEmailSummaryCtrl.massEmailEMList);
        }
    }

    
    /* positive test cases */

    // test init w/ valid Student_Folder__c Id
    @isTest 
    static void init_withStudentFolderId_validInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();
                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();

                Id studentFolderId = [select Id from Student_Folder__c where Name = :MassEmailSendTestDataFactory.TEST_STUDENT_FOLDER1_NAME].Id;
                SchoolMassEmailSummaryController massEmailSummaryCtrl = new SchoolMassEmailSummaryController();
                massEmailSummaryCtrl.studentFolderId = studentFolderId;
            Test.stopTest();

            // Assert
            System.assert( massEmailSummaryCtrl.massEmailEMList.isEmpty());
        }
    }

    // test init w/ valid Student_Folder__c Id w/ valid Mass Email Events
    @isTest 
    static void init_withStudentFolderIdWithEvents_validInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();
                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();

                Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                                     Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                                     Html_Body__c, Footer__c, Reply_To_Address__c
                                               from Mass_Email_Send__c
                                              where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];

                Contact c = [select Id, Name
                               from Contact
                              where Email = :MassEmailSendTestDataFactory.TEST_PARENT1_CONTACT_EMAIL];

                Contact contactStudent = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_STUDENT1_CONTACT_EMAIL];
                Applicant__c app = [select Id from Applicant__c where Contact__c = :contactStudent.Id];
                School_PFS_Assignment__c sPFS = [select Id, Name, Mass_Email_Events__c
                                                   from School_PFS_Assignment__c
                                                  where Applicant__c = :app.Id]; 

                // Create STATUS_DELIVERED and STATUS_OPENED events
                String massEmailEventString = MassEmailSendTestDataFactory.createMassEmailJSONEvent( 
                                                        sPFS.Id, massES.School__c, c.Id, massES.Id, SchoolMassEmailSendModel.STATUS_DELIVERED);
                massEmailEventString += ',' + MassEmailSendTestDataFactory.createMassEmailJSONEvent( 
                                                        sPFS.Id, massES.School__c, c.Id, massES.Id, SchoolMassEmailSendModel.STATUS_OPENED);

                sPFS.Mass_Email_Events__c = massEmailEventString;
                update sPFS;

                Student_Folder__c studentFolder = [select Id from Student_Folder__c where Name = :MassEmailSendTestDataFactory.TEST_STUDENT_FOLDER1_NAME];
                SchoolMassEmailSummaryController massEmailSummaryCtrl = new SchoolMassEmailSummaryController();
                massEmailSummaryCtrl.studentFolderId = studentFolder.Id;
            Test.stopTest();

            // Assert
            System.assertEquals( 1, massEmailSummaryCtrl.massEmailEMList.size());
        }
    }

    // test init w/ valid Student_Folder__c Id w/ valid bounced Mass Email Event
    @isTest 
    static void init_withStudentFolderIdWithBounceEvent_validInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();
                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();

                Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                                     Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                                     Html_Body__c, Footer__c, Reply_To_Address__c
                                               from Mass_Email_Send__c
                                              where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];

                Contact c = [select Id, Name
                               from Contact
                              where Email = :MassEmailSendTestDataFactory.TEST_PARENT1_CONTACT_EMAIL];

                Contact contactStudent = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_STUDENT1_CONTACT_EMAIL];
                Applicant__c app = [select Id from Applicant__c where Contact__c = :contactStudent.Id];
                School_PFS_Assignment__c sPFS = [select Id, Name, Mass_Email_Events__c
                                                   from School_PFS_Assignment__c
                                                  where Applicant__c = :app.Id]; 

                // Create STATUS_BOUNCED event
                String massEmailEventString = MassEmailSendTestDataFactory.createMassEmailJSONEvent( 
                                                        sPFS.Id, massES.School__c, c.Id, massES.Id, SchoolMassEmailSendModel.STATUS_BOUNCED);
                sPFS.Mass_Email_Events__c = massEmailEventString;
                update sPFS;

                Student_Folder__c studentFolder = [select Id from Student_Folder__c where Name = :MassEmailSendTestDataFactory.TEST_STUDENT_FOLDER1_NAME];
                SchoolMassEmailSummaryController massEmailSummaryCtrl = new SchoolMassEmailSummaryController();
                massEmailSummaryCtrl.studentFolderId = studentFolder.Id;
            Test.stopTest();

            // Assert
            System.assertEquals( 1, massEmailSummaryCtrl.massEmailEMList.size());
        }
    }

    // test init w/ valid Student_Folder__c Id w/ valid unsub Mass Email Event
    @isTest 
    static void init_withStudentFolderIdWithUnsubEvent_validInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();
                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();

                Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                                     Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                                     Html_Body__c, Footer__c, Reply_To_Address__c
                                               from Mass_Email_Send__c
                                              where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];

                Contact c = [select Id, Name
                               from Contact
                              where Email = :MassEmailSendTestDataFactory.TEST_PARENT1_CONTACT_EMAIL];

                Contact contactStudent = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_STUDENT1_CONTACT_EMAIL];
                Applicant__c app = [select Id from Applicant__c where Contact__c = :contactStudent.Id];
                School_PFS_Assignment__c sPFS = [select Id, Name, Mass_Email_Events__c
                                                   from School_PFS_Assignment__c
                                                  where Applicant__c = :app.Id]; 

                // Create STATUS_UNSUB event
                String massEmailEventString = MassEmailSendTestDataFactory.createMassEmailJSONEvent( 
                                                        sPFS.Id, massES.School__c, c.Id, massES.Id, SchoolMassEmailSendModel.STATUS_UNSUB);
                sPFS.Mass_Email_Events__c = massEmailEventString;
                update sPFS;

                Student_Folder__c studentFolder = [select Id from Student_Folder__c where Name = :MassEmailSendTestDataFactory.TEST_STUDENT_FOLDER1_NAME];
                SchoolMassEmailSummaryController massEmailSummaryCtrl = new SchoolMassEmailSummaryController();
                massEmailSummaryCtrl.studentFolderId = studentFolder.Id;
            Test.stopTest();

            // Assert
            System.assertEquals( 1, massEmailSummaryCtrl.massEmailEMList.size());
        }
    }

    // test init w/ valid Student_Folder__c Id and then valid nav to Mass Email View
    @isTest 
    static void init_navMassEmailView_validInitAndNav() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();
                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();

                Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                                     Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                                     Html_Body__c, Footer__c, Reply_To_Address__c
                                               from Mass_Email_Send__c
                                              where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];

                Contact c = [select Id, Name
                               from Contact
                              where Email = :MassEmailSendTestDataFactory.TEST_PARENT1_CONTACT_EMAIL];

                Contact contactStudent = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_STUDENT1_CONTACT_EMAIL];
                Applicant__c app = [select Id from Applicant__c where Contact__c = :contactStudent.Id];
                School_PFS_Assignment__c sPFS = [select Id, Name, Mass_Email_Events__c
                                                   from School_PFS_Assignment__c
                                                  where Applicant__c = :app.Id]; 

                // Create STATUS_OPENED event
                String massEmailEventString = MassEmailSendTestDataFactory.createMassEmailJSONEvent( 
                                                        sPFS.Id, massES.School__c, c.Id, massES.Id, SchoolMassEmailSendModel.STATUS_OPENED);
                sPFS.Mass_Email_Events__c = massEmailEventString;
                update sPFS;

                Student_Folder__c studentFolder = [select Id from Student_Folder__c where Name = :MassEmailSendTestDataFactory.TEST_STUDENT_FOLDER1_NAME];
                SchoolMassEmailSummaryController massEmailSummaryCtrl = new SchoolMassEmailSummaryController();
                massEmailSummaryCtrl.studentFolderId = studentFolder.Id;
                PageReference pRef = massEmailSummaryCtrl.navMassEmailView();
            Test.stopTest();

            // Assert
            System.assertEquals( 1, massEmailSummaryCtrl.massEmailEMList.size());
            System.assertNotEquals( null, pRef);
        }
    }

}