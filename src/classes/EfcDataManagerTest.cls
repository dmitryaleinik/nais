@isTest
private class EfcDataManagerTest {
    @isTest
    private static void testCreateWorksheetDataForPfs() {
        // create the academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        // create a test PFS record
        PFS__c pfs = EfcTestData.createTestPfs(academicYear.Id, true);

        // NAIS-2500 [DP] 09.18.2015 adding biz farm for efc calcs
        List<Business_Farm__c> bizFarmList = new List<Business_Farm__c>();
        bizFarmList.add(TestUtils.createBusinessFarm(pfs.Id, 'biz1', 'Business', 'Partnership', false));
        bizFarmList.add(TestUtils.createBusinessFarm(pfs.Id, 'biz2', 'Business', 'Partnership', false));
        for (Business_Farm__c bizFarm : bizFarmList){
            bizFarm.Net_Profit_Loss_Business_Farm_Current__c = 300;
            bizFarm.Business_Farm_Ownership_Percent__c = 10;
            bizFarm.Business_Net_Profit_Share_Current__c = 100;
            bizFarm.Business_Farm_Owner__c = 'Parent A';
        }
        insert bizFarmList;

        // test creating a worksheet
        EfcWorksheetData worksheet = EfcDataManager.createEfcWorksheetDataForPFS(pfs.Id);

        // make sure the fields are mapped properly
        System.assertEquals(pfs.Id, worksheet.sourcePfsId);

        System.assertEquals(pfs.Salary_Wages_Parent_A__c, worksheet.salaryWagesParentA);
        System.assertEquals(pfs.Salary_Wages_Parent_B__c, worksheet.salaryWagesParentB);
        System.assertEquals(pfs.Dividend_Income_Current__c, worksheet.dividendIncome);
        System.assertEquals(pfs.Interest_Income_Current__c, worksheet.interestIncome);
        System.assertEquals(pfs.Alimony_Current__c, worksheet.alimonyReceived);
        System.assertEquals(100 * bizFarmList.size(), worksheet.netProfitLossBusinessFarm);
        System.assertEquals(pfs.Taxable_Refunds_Current__c, worksheet.taxableRefunds);
        System.assertEquals(pfs.Capital_Gain_Loss_Current__c, worksheet.capitalGainLoss);
        System.assertEquals(pfs.Other_Gain_Loss_Current__c, worksheet.otherGainsLosses);
        System.assertEquals(pfs.IRA_Distribution_Current__c, worksheet.iraDistributions);
        System.assertEquals(pfs.Pensions_and_Annuities_Current__c, worksheet.pensionsAndAnnuities);
        System.assertEquals(pfs.Rental_Real_Estate_Trusts_etc_Current__c, worksheet.rentalRealEstateTrustsEtc);
        System.assertEquals(pfs.Unemployment_Compensation_Current__c, worksheet.unemploymentCompensation);
        System.assertEquals(pfs.Social_Security_Benefits_Taxable_Current__c, worksheet.socialSecurityBenefitsTaxable);
        System.assertEquals(pfs.Other_Income_Current__c, worksheet.otherIncome);
        System.assertEquals(pfs.Keogh_Plan_Payment_Current__c, worksheet.sepSimpleQualifiedPlans);
        System.assertEquals(pfs.Untaxed_IRA_Plan_Payment_Current__c, worksheet.untaxedIraPlanPayment);
        System.assertEquals(pfs.Tax_Defer_Pension_Saving_Current__c, worksheet.preTaxPaymentsToRetirementPlans);
        System.assertEquals(pfs.Child_Support_Received_Current__c, worksheet.childSupportReceived);
        System.assertEquals(pfs.Social_Security_Benefits_Current__c, worksheet.socialSecurityBenefits);
        System.assertEquals(pfs.Fringe_Benefit_Plan_Untaxed_Current__c, worksheet.preTaxFringeBenefitContribution);
        System.assertEquals(pfs.Cash_Support_Gift_Income_Current__c, worksheet.cashSupportGiftIncome);
        System.assertEquals(pfs.Tax_Exempt_Investments_Current__c, worksheet.taxExemptInvestmentIncome);
        System.assertEquals(pfs.Income_Abroad_Current__c, worksheet.incomeEarnedAbroad);
        System.assertEquals(pfs.Business_Farm_Owner__c, worksheet.businessFarmOwner);
        System.assertEquals(pfs.Other_Untaxed_Income_Current__c, worksheet.otherUntaxedIncome);
        System.assertEquals(pfs.Filing_Status__c, worksheet.filingStatus);
        System.assertEquals(pfs.Income_Tax_Exemptions__c, worksheet.incomeTaxExemptions);
        System.assertEquals(pfs.Itemized_Deductions__c, worksheet.itemizedDeductions);
        System.assertEquals(pfs.Federal_Income_Tax_Calculated__c, worksheet.federalIncomeTaxCalculated);
        System.assertEquals(pfs.Social_Security_Tax_Allowance_Calc__c, worksheet.socialSecurityTaxAllowanceCalculated);
        System.assertEquals(pfs.Medicare_Tax_Allowance_Calculated__c, worksheet.medicareTaxAllowanceCalculated);
        System.assertEquals(pfs.Parent_A_State__c, worksheet.parentState);
        System.assertEquals(pfs.State_Other_Tax_Allowance_Calculated__c, worksheet.stateOtherTaxAllowanceCalculated);
        System.assertEquals(pfs.Employment_Allowance__c, worksheet.employmentAllowance);
        System.assertEquals(pfs.Medical_Dental_Exp_Current__c, worksheet.medicalDentalExpenses);
        System.assertEquals(pfs.Medical_Allowance__c, worksheet.medicalAllowance);
        System.assertEquals(pfs.Unusual_Expenses_Current__c, worksheet.unusualExpenses);
        System.assertEquals(pfs.Self_Employed_Tax_Paid__c, worksheet.selfEmploymentTaxPaid);
        System.assertEquals(pfs.Unpaid_Principal_1st_Mortgage__c, worksheet.unpaidPrincipal1stMortgage);
        System.assertEquals(pfs.Unpaid_Principal_2nd_Mortgage__c, worksheet.unpaidPrincipal2ndMortgage);
        System.assertEquals(pfs.Home_Purchase_Year__c, worksheet.homePurchaseYear);
        System.assertEquals(pfs.Home_Purchase_Price__c, worksheet.homePurchasePrice);
        System.assertEquals(pfs.Other_Real_Estate_Unpaid_Principal__c, worksheet.otherRealEstateUnpaidMortgagePrincipal);
        System.assertEquals(pfs.Other_Real_Estate_Market_Value_Total__c, worksheet.otherRealEstateMarketValue);
        List<Decimal> expectedOwnerShipList = new List<Decimal>();
        for (Business_Farm__c bf : bizFarmList){
            Decimal d = 10;
            expectedOwnerShipList.add(d);
        }
        System.assertEquals(expectedOwnerShipList, worksheet.businessFarmPercentOwnershipList);
        System.assertEquals(pfs.Bank_Account_Value__c, worksheet.bankAccounts);
        System.assertEquals(pfs.Investments_Net_Value__c, worksheet.otherInvestments);
        System.assertEquals(pfs.Total_Debts__c, worksheet.totalDebts);
        System.assertEquals(pfs.Net_Worth__c, worksheet.netWorth);
        System.assertEquals(EfcUtil.ageFromBirthdate(pfs.Parent_A_Birthdate__c, pfs.Original_Submission_Date__c), worksheet.ageParentA);
        System.assertEquals(EfcUtil.ageFromBirthdate(pfs.Parent_B_Birthdate__c, pfs.Original_Submission_Date__c), worksheet.ageParentB);
        System.assertEquals(pfs.Discretionary_Net_Worth__c, worksheet.discretionaryNetWorth);
        System.assertEquals(pfs.Income_Supplement__c, worksheet.incomeSupplement);
        System.assertEquals(pfs.Revised_Adjusted_Effective_Income__c, worksheet.revisedAdjustedEffectiveIncome);
        System.assertEquals(pfs.Family_Size__c, worksheet.familySize);
        System.assertEquals(pfs.Income_Protection_Allowance__c, worksheet.incomeProtectionAllowance);
        System.assertEquals(pfs.Est_Parental_Contribution__c, worksheet.estimatedParentalContribution);
        System.assertEquals(pfs.Num_Children_in_Tuition_Schools__c, worksheet.numChildrenInTuitionSchools);
        System.assertEquals(pfs.Est_Parental_Contribution_per_Child__c, worksheet.estimatedParentalContributionPerChild);
    }

    @isTest
    private static void testCreateWorksheetDataForSchoolPfsAssignment() {

        // create a test School PFS Assignment record
        School_PFS_Assignment__c pfsAssign = EfcTestData.createTestSchoolPfsAssignment(true);

        PFS__c pfs = [Select Id FROM PFS__c limit 1];

        // NAIS-2500 [DP] 09.18.2015 adding biz farm for efc calcs
        List<Business_Farm__c> bizFarmList = new List<Business_Farm__c>();
        bizFarmList.add(TestUtils.createBusinessFarm(pfs.Id, 'biz1', 'Business', 'Partnership', false));
        bizFarmList.add(TestUtils.createBusinessFarm(pfs.Id, 'biz2', 'Business', 'Partnership', false));
        for (Business_Farm__c bizFarm : bizFarmList){
            bizFarm.Net_Profit_Loss_Business_Farm_Current__c = 300;
            bizFarm.Business_Farm_Ownership_Percent__c = 10;
            bizFarm.Business_Net_Profit_Share_Current__c = 100;
        }
        insert bizFarmList;

        List<School_Biz_Farm_Assignment__c> sbfaList = [Select Id, School_PFS_Assignment__c, Net_Profit_Loss_Business_Farm__c, Orig_Net_Profit_Loss_Business_Farm__c, Business_Farm__r.Net_Profit_Loss_Business_Farm_Current__c FROM School_Biz_Farm_Assignment__c];
        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){
            sbfa.Business_Farm_Owner__c = 'Parent A';
            sbfa.Business_Farm_Share__c = 101;
        }
        update sbfaList;

        // test creating a worksheet
        EfcWorksheetData worksheet = EfcDataManager.createEfcWorksheetDataForSchoolPfsAssignment(pfsAssign.Id);

        // make sure the fields are mapped properly
        System.assertEquals(pfsAssign.Id, worksheet.sourceSchoolPfsAssignmentId);

        System.assertEquals(pfsAssign.Salary_Wages_Parent_A__c, worksheet.salaryWagesParentA);
        System.assertEquals(pfsAssign.Salary_Wages_Parent_B__c, worksheet.salaryWagesParentB);
        System.assertEquals(pfsAssign.Dividend_Income__c, worksheet.dividendIncome);
        System.assertEquals(pfsAssign.Interest_Income__c, worksheet.interestIncome);
        System.assertEquals(pfsAssign.Alimony_Received__c, worksheet.alimonyReceived);
        System.assertEquals(100 * bizFarmList.size(), worksheet.netProfitLossBusinessFarm);
        System.assertEquals(pfsAssign.Taxable_Refunds__c, worksheet.taxableRefunds);
        System.assertEquals(pfsAssign.Capital_Gain_Loss__c, worksheet.capitalGainLoss);
        System.assertEquals(pfsAssign.Other_Gain_Loss__c, worksheet.otherGainsLosses);
        System.assertEquals(pfsAssign.IRA_Distribution__c, worksheet.iraDistributions);
        System.assertEquals(pfsAssign.Pensions_and_Annuities__c, worksheet.pensionsAndAnnuities);
        System.assertEquals(pfsAssign.Rental_Real_Estate_Trusts_etc__c, worksheet.rentalRealEstateTrustsEtc);
        System.assertEquals(pfsAssign.Unemployment_Compensation__c, worksheet.unemploymentCompensation);
        System.assertEquals(pfsAssign.Social_Security_Benefits_Taxable__c, worksheet.socialSecurityBenefitsTaxable);
        System.assertEquals(pfsAssign.Other_Income__c, worksheet.otherIncome);
        System.assertEquals(pfsAssign.Keogh_Plan_Payment__c, worksheet.sepSimpleQualifiedPlans);
        System.assertEquals(pfsAssign.Untaxed_IRA_Plan_Payment__c, worksheet.untaxedIraPlanPayment);
        System.assertEquals(pfsAssign.Child_Support_Received__c, worksheet.childSupportReceived);
        System.assertEquals(pfsAssign.Social_Security_Benefits__c, worksheet.socialSecurityBenefits);
        System.assertEquals(pfsAssign.Pre_Tax_Fringe_Benefit_Contribution__c, worksheet.preTaxFringeBenefitContribution);
        System.assertEquals(pfsAssign.Cash_Support_Gift_Income__c, worksheet.cashSupportGiftIncome);
        System.assertEquals(pfsAssign.Tax_Exempt_Investment_Income__c, worksheet.taxExemptInvestmentIncome);
        System.assertEquals(pfsAssign.Income_Earned_Abroad__c, worksheet.incomeEarnedAbroad);
        System.assertEquals(pfsAssign.Other_Untaxed_Income__c, worksheet.otherUntaxedIncome);
        System.assertEquals(pfsAssign.Business_Farm_Owner__c, worksheet.businessFarmOwner);
        System.assertEquals(pfsAssign.Depreciation_Sec_179_Exp_Sch_C__c, worksheet.depreciationSec179ExpSchC);
        System.assertEquals(pfsAssign.Depreciation_Schedule_E__c, worksheet.depreciationScheduleE);
        System.assertEquals(pfsAssign.Depreciation_Sec_179_Exp_Sch_F__c, worksheet.depreciationSec179ExpSchF);
        System.assertEquals(pfsAssign.Depreciation_Sec_179_Exp_4562__c, worksheet.depreciationSec179Exp4562);
        System.assertEquals(pfsAssign.Home_Bus_Exp_Sch_C__c, worksheet.homeBusinessExpenseScheduleC);
        System.assertEquals(pfsAssign.Filing_Status__c, worksheet.filingStatus);
        System.assertEquals(pfsAssign.Income_Tax_Exemptions__c, worksheet.incomeTaxExemptions);
        System.assertEquals(pfsAssign.Itemized_Deductions__c, worksheet.itemizedDeductions);
        System.assertEquals(pfsAssign.Federal_Income_Tax_Calculated__c, worksheet.federalIncomeTaxCalculated);
        System.assertEquals(pfsAssign.Social_Security_Tax_Allowance_Calc__c, worksheet.socialSecurityTaxAllowanceCalculated);
        System.assertEquals(pfsAssign.Medicare_Tax_Allowance_Calc__c, worksheet.medicareTaxAllowanceCalculated);
        System.assertEquals(pfsAssign.Total_SE_Tax_Paid__c, worksheet.selfEmploymentTaxPaid);
        System.assertEquals(pfsAssign.Parent_State__c, worksheet.parentState);
        System.assertEquals(pfsAssign.State_Other_Tax_Allowance_Calculated__c, worksheet.stateOtherTaxAllowanceCalculated);
        System.assertEquals(pfsAssign.Employment_Allowance__c, worksheet.employmentAllowance);
        System.assertEquals(pfsAssign.Medical_Dental_Expense__c, worksheet.medicalDentalExpenses);
        System.assertEquals(pfsAssign.Medical_Allowance__c, worksheet.medicalAllowance);
        System.assertEquals(pfsAssign.Unusual_Expense__c, worksheet.unusualExpenses);
        System.assertEquals(pfsAssign.Unpaid_Principal_1st_Mortgage__c, worksheet.unpaidPrincipal1stMortgage);
        System.assertEquals(pfsAssign.Unpaid_Principal_2nd_Mortgage__c, worksheet.unpaidPrincipal2ndMortgage);
        System.assertEquals(pfsAssign.Home_Purchase_Year__c, worksheet.homePurchaseYear);
        System.assertEquals(pfsAssign.Home_Purchase_Price__c, worksheet.homePurchasePrice);
        System.assertEquals(pfsAssign.Other_Real_Estate_Unpaid_Principal__c, worksheet.otherRealEstateUnpaidMortgagePrincipal);
        System.assertEquals(pfsAssign.Other_Real_Estate_Market_Value__c, worksheet.otherRealEstateMarketValue);
        List<Decimal> expectedOwnerShipList = new List<Decimal>();
        for (Business_Farm__c bf : bizFarmList){
            Decimal d = 10;
            expectedOwnerShipList.add(d);
        }
        System.assertEquals(expectedOwnerShipList, worksheet.businessFarmPercentOwnershipList);
        System.assertEquals(pfsAssign.Bank_Accounts__c, worksheet.bankAccounts);
        System.assertEquals(pfsAssign.Investments__c, worksheet.otherInvestments);
        System.assertEquals(pfsAssign.Total_Debts__c, worksheet.totalDebts);
        System.assertEquals(pfsAssign.Net_Worth__c, worksheet.netWorth);
        System.assertEquals(pfsAssign.Discretionary_Net_Worth__c, worksheet.discretionaryNetWorth);
        System.assertEquals(pfsAssign.Income_Supplement__c, worksheet.incomeSupplement);
        System.assertEquals(pfsAssign.Revised_Adjusted_Effective_Income__c, worksheet.revisedAdjustedEffectiveIncome);
        System.assertEquals(pfsAssign.Family_Size__c, worksheet.familySize);
        System.assertEquals(pfsAssign.Income_Protection_Allowance__c, worksheet.incomeProtectionAllowance);
        System.assertEquals(pfsAssign.Est_Parental_Contribution_All_Students__c, worksheet.estimatedParentalContribution);
        System.assertEquals(pfsAssign.Num_Children_in_Tuition_Schools__c, worksheet.numChildrenInTuitionSchools);
        System.assertEquals(pfsAssign.Est_Parental_Contribution_per_Child__c, worksheet.estimatedParentalContributionPerChild);
    }

    @isTest
    private static void testCreateWorksheetDataForSchoolPfsAssignmentOrigFields() {
        // create a test School PFS Assignment record, with a blank revision value
        School_PFS_Assignment__c pfsAssign = EfcTestData.createTestSchoolPfsAssignment(false);
        pfsAssign.Salary_Wages_Parent_A__c = null;
        pfsAssign.Orig_Salary_Wages_Parent_A__c = 99999;
        insert pfsAssign;

        // test creating a worksheet
        EfcWorksheetData worksheet = EfcDataManager.createEfcWorksheetDataForSchoolPfsAssignment(pfsAssign.Id);

        // make sure the orig field is read properly
        System.assertEquals(pfsAssign.Orig_Salary_Wages_Parent_A__c, worksheet.salaryWagesParentA);
    }


    @isTest
    private static void testCommitEfcResultsToPfs() {
        // create the academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        // create a PFS
        PFS__c pfs = EfcTestData.createTestPfs(academicYear.Id, true);

        // create a worksheet
        EfcWorksheetData worksheet = new EfcWorksheetData();
        worksheet.estimatedParentalContribution = 1234;
        worksheet.sourcePfsId = pfs.Id;

        // do the commit
        EfcDataManager.commitEfcResults(new List<EfcWorksheetData> {worksheet});

        // verify the PFS was updated
        List<PFS__c> pfsList = [SELECT Id, Est_Parental_Contribution__c FROM PFS__c WHERE Id=:pfs.Id LIMIT 1];
        System.assertEquals(worksheet.estimatedParentalContribution, pfsList[0].Est_Parental_Contribution__c);
    }

    @isTest
    private static void testErrorOnCommitToPfs() {
        // create the academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        // create a PFS
        PFS__c pfs = EfcTestData.createTestPfs(academicYear.Id, true);

        // create a worksheet
        EfcWorksheetData worksheet = new EfcWorksheetData();
        worksheet.estimatedParentalContribution = 12345;
        worksheet.sourcePfsId = pfs.Id;
        worksheet.contribPercentTotalIncome = 12645789;

        // do the commit
        EfcDataManager.commitEfcResults(new List<EfcWorksheetData> {worksheet});

        // verify the PFS was updated
        List<PFS__c> pfsList = [SELECT Id, SSS_EFC_Calc_Status__c, SSS_EFC_Calc_Error__c FROM PFS__c WHERE Id=:pfs.Id LIMIT 1];
        System.assertEquals( pfsList[0].SSS_EFC_Calc_Status__c, EfcPicklistValues.EFC_CALC_STATUS_ERROR);
        System.assertNotEquals( pfsList[0].SSS_EFC_Calc_Error__c, null);
        System.assertNotEquals( pfsList[0].SSS_EFC_Calc_Error__c, '');
    }

    @isTest
    private static void testCommitEfcResultsToSchoolPfsAssignment() {
        // create a test School PFS Assignment record
        School_PFS_Assignment__c pfsAssign = EfcTestData.createTestSchoolPfsAssignment(true);

        // create a worksheet
        EfcWorksheetData worksheet = new EfcWorksheetData();
        worksheet.estimatedParentalContribution = 12345;
        worksheet.sourceSchoolPfsAssignmentId = pfsAssign.Id;

        // do the commit
        EfcDataManager.commitEfcResults(new List<EfcWorksheetData> {worksheet});

        // verify the PFS was updated
        List<School_PFS_Assignment__c> pfsAssignList = [SELECT Id, Est_Parental_Contribution_All_Students__c FROM School_PFS_Assignment__c WHERE Id=:pfsAssign.Id LIMIT 1];
        System.assertEquals(worksheet.estimatedParentalContribution, pfsAssignList[0].Est_Parental_Contribution_All_Students__c );
    }

    @isTest
    private static void testErrorOnCommitToSchoolPfsAssignment() {
        // create a test School PFS Assignment record
        School_PFS_Assignment__c pfsAssign = EfcTestData.createTestSchoolPfsAssignment(true);

        // create a worksheet
        EfcWorksheetData worksheet = new EfcWorksheetData();
        worksheet.estimatedParentalContribution = 12345;
        worksheet.sourceSchoolPfsAssignmentId = pfsAssign.Id;
        worksheet.housingIndexMultiplier = 22.12;

        // do the commit
        EfcDataManager.commitEfcResults(new List<EfcWorksheetData> {worksheet});

        // verify the PFS was updated
        List<School_PFS_Assignment__c> pfsAssignList = [SELECT Id, Est_Parental_Contribution_All_Students__c, Revision_EFC_Calc_Status__c, Revision_EFC_Calc_Error__c FROM School_PFS_Assignment__c WHERE Id=:pfsAssign.Id LIMIT 1];
        System.assertEquals( pfsAssignList[0].Revision_EFC_Calc_Status__c, EfcPicklistValues.EFC_CALC_STATUS_ERROR );
        System.assertNotEquals( pfsAssignList[0].Revision_EFC_Calc_Error__c, null );
        System.assertNotEquals( pfsAssignList[0].Revision_EFC_Calc_Error__c, '' );
    }

    private static EFC_Profile__c initEFCProfile() {
        Account account = new Account(
            Name = 'Account #1'
        );
        insert Account;

        EFC_Profile__c efcProfile = new EFC_Profile__c(
            Name = 'EFC_Profile #1',
            Account__c = account.Id,

            /* Family Information */
            Parent_A_Country__c = 'United States',
            Parent_A_State__c = 'Alabama',
            Family_Status__c = '2 Parents',
            Parent_A_Birthdate__c = 1,
            Parent_B_Birthdate__c = 2,
            Family_Size__c = 3,
            Num_Children_in_Tuition_Schools__c = 4,
            Grade_Applying__c = 'Preschool',
            Day_or_Boarding__c = 'Day',
            Filing_Status__c = 'Single',
            Parent_B_Filing_Status__c = 'Single',
            Income_Tax_Exemptions__c = 5,

            /* Income/Allowances */
            Salary_Wages_Parent_A__c = 6,
            Salary_Wages_Parent_B__c = 7,
            Interest_Income_Current__c = 8,
            Other_Income_Current__c = 9,
            Other_Untaxed_Income_Current__c = 10,
            Medical_Dental_Exp_Current__c = 11,
            Unusual_Expenses_Current__c = 12,

            /* Assets/Debts */
            Home_Purchase_Year__c = '2016',
            Home_Purchase_Price__c = 13,
            Home_Market_Value__c = 14,
            Unpaid_Principal_1st_Mortgage__c = 15,
            Other_Real_Estate_Market_Value_Total__c = 16,
            Other_Real_Estate_Unpaid_Principal__c = 17,
            Bank_Account_Value__c = 18,
            Total_Debts__c = 19,
            Student_Assets__c = 20,

            /* Professional Judgment Options */
            Override_Default_COLA_Value__c = 21,
            Use_Div_Int_to_Impute_Assets__c = true,
            Impute__c = 22,
            Use_Home_Equity__c = true,
            Use_Home_Equity_Cap__c = true,
            Use_Housing_Index_Multiplier__c = true,
            Adjust_Housing_Portion_of_IPA__c = 'No',
            IPA_Housing_Family_of_4__c = 23
        );
        insert efcProfile;

        return efcProfile;
    }

    @isTest
    private static void testUpdateEFCDataFromEFCProfile_updateEFCProfileFromEFCData() {
        /* Test updateEFCDataFromEFCProfile method */
        EFC_Profile__c efcProfile = initEFCProfile();
        EfcWorksheetData efcData = new EfcWorksheetData();

        EfcDataManager.updateEFCDataFromEFCProfile(efcData, efcProfile);

        System.assertEquals(efcProfile.Parent_A_Country__c, efcData.parentCountry);
        System.assertEquals(efcProfile.Parent_A_State__c, efcData.parentState);
        System.assertEquals(efcProfile.Family_Status__c, efcData.familyStatus);
        System.assertEquals(efcProfile.Parent_A_Birthdate__c, efcData.ageParentA);
        System.assertEquals(efcProfile.Parent_B_Birthdate__c, efcData.ageParentB);
        System.assertEquals(efcProfile.Family_Size__c, efcData.familySize);
        System.assertEquals(efcProfile.Num_Children_in_Tuition_Schools__c, efcData.numChildrenInTuitionSchools);
        System.assertEquals(efcProfile.Grade_Applying__c, efcData.gradeApplying);
        System.assertEquals(efcProfile.Day_or_Boarding__c, efcData.dayOrBoarding);
        System.assertEquals(efcProfile.Filing_Status__c, efcData.filingStatus);
        System.assertEquals(efcProfile.Parent_B_Filing_Status__c, efcData.filingStatusParentB);
        System.assertEquals(efcProfile.Income_Tax_Exemptions__c, efcData.incomeTaxExemptions);

        System.assertEquals(efcProfile.Salary_Wages_Parent_A__c, efcData.salaryWagesParentA);
        System.assertEquals(efcProfile.Salary_Wages_Parent_B__c, efcData.salaryWagesParentB);
        System.assertEquals(efcProfile.Interest_Income_Current__c, efcData.interestIncome);
        System.assertEquals(efcProfile.Other_Income_Current__c, efcData.otherIncome);
        System.assertEquals(efcProfile.Other_Untaxed_Income_Current__c, efcData.otherUntaxedIncome);
        System.assertEquals(efcProfile.Medical_Dental_Exp_Current__c, efcData.medicalDentalExpenses);
        System.assertEquals(efcProfile.Unusual_Expenses_Current__c, efcData.unusualExpenses);

        System.assertEquals(efcProfile.Home_Purchase_Year__c, efcData.homePurchaseYear);
        System.assertEquals(efcProfile.Home_Purchase_Price__c, efcData.homePurchasePrice);
        System.assertEquals(efcProfile.Home_Market_Value__c, efcData.homeMarketValue);
        System.assertEquals(efcProfile.Unpaid_Principal_1st_Mortgage__c, efcData.unpaidPrincipal1stMortgage);
        System.assertEquals(efcProfile.Other_Real_Estate_Market_Value_Total__c, efcData.otherRealEstateMarketValue);
        System.assertEquals(efcProfile.Other_Real_Estate_Unpaid_Principal__c, efcData.otherRealEstateUnpaidMortgagePrincipal);
        System.assertEquals(efcProfile.Bank_Account_Value__c, efcData.bankAccounts);
        System.assertEquals(efcProfile.Total_Debts__c, efcData.totalDebts);
        System.assertEquals(efcProfile.Student_Assets__c, efcData.studentAssets);

        System.assertEquals(efcProfile.Override_Default_COLA_Value__c, efcData.pjOverrideDefaultColaValue);
        System.assertEquals(efcProfile.Use_Div_Int_to_Impute_Assets__c, efcData.pjUseDividendInterestIncomeToImputeAssets);
        System.assertEquals(efcProfile.Impute__c, efcData.pjPercentageForImputingAssets);
        System.assertEquals(efcProfile.Use_Home_Equity__c, efcData.pjUseHomeEquity);
        System.assertEquals(efcProfile.Use_Home_Equity_Cap__c, efcData.pjUseHomeEquityCap);
        System.assertEquals(efcProfile.Use_Housing_Index_Multiplier__c, efcData.pjUseHousingIndexMultiplier);
        System.assertEquals(efcProfile.Adjust_Housing_Portion_of_IPA__c, efcData.adjustHousingPortion);
        System.assertEquals(efcProfile.IPA_Housing_Family_of_4__c, efcData.ipaHousingFamilyOf4);

        /* Test updateEFCProfileFromEFCData method */
        efcProfile = new EFC_Profile__c();
        EfcDataManager.updateEFCProfileFromEFCData(efcProfile, efcData);

        System.assertEquals(efcData.parentCountry, efcProfile.Parent_A_Country__c);
        System.assertEquals(efcData.parentState, efcProfile.Parent_A_State__c);
        System.assertEquals(efcData.familyStatus, efcProfile.Family_Status__c);
        System.assertEquals(efcData.ageParentA, efcProfile.Parent_A_Birthdate__c);
        System.assertEquals(efcData.ageParentB, efcProfile.Parent_B_Birthdate__c);
        System.assertEquals(efcData.familySize, efcProfile.Family_Size__c);
        System.assertEquals(efcData.numChildrenInTuitionSchools, efcProfile.Num_Children_in_Tuition_Schools__c);
        System.assertEquals(efcData.gradeApplying, efcProfile.Grade_Applying__c);
        System.assertEquals(efcData.dayOrBoarding, efcProfile.Day_or_Boarding__c);
        System.assertEquals(efcData.filingStatus, efcProfile.Filing_Status__c);
        System.assertEquals(efcData.filingStatusParentB, efcProfile.Parent_B_Filing_Status__c);
        System.assertEquals(efcData.incomeTaxExemptions, efcProfile.Income_Tax_Exemptions__c);

        System.assertEquals(efcData.salaryWagesParentA, efcProfile.Salary_Wages_Parent_A__c);
        System.assertEquals(efcData.salaryWagesParentB, efcProfile.Salary_Wages_Parent_B__c);
        System.assertEquals(efcData.interestIncome, efcProfile.Interest_Income_Current__c);
        System.assertEquals(efcData.otherIncome, efcProfile.Other_Income_Current__c);
        System.assertEquals(efcData.otherUntaxedIncome, efcProfile.Other_Untaxed_Income_Current__c);
        System.assertEquals(efcData.medicalDentalExpenses, efcProfile.Medical_Dental_Exp_Current__c);
        System.assertEquals(efcData.unusualExpenses, efcProfile.Unusual_Expenses_Current__c);

        System.assertEquals(efcData.homePurchaseYear, efcProfile.Home_Purchase_Year__c);
        System.assertEquals(efcData.homePurchasePrice, efcProfile.Home_Purchase_Price__c);
        System.assertEquals(efcData.homeMarketValue, efcProfile.Home_Market_Value__c);
        System.assertEquals(efcData.unpaidPrincipal1stMortgage, efcProfile.Unpaid_Principal_1st_Mortgage__c);
        System.assertEquals(efcData.otherRealEstateMarketValue, efcProfile.Other_Real_Estate_Market_Value_Total__c);
        System.assertEquals(efcData.otherRealEstateUnpaidMortgagePrincipal, efcProfile.Other_Real_Estate_Unpaid_Principal__c);
        System.assertEquals(efcData.bankAccounts, efcProfile.Bank_Account_Value__c);
        System.assertEquals(efcData.totalDebts, efcProfile.Total_Debts__c);
        System.assertEquals(efcData.studentAssets, efcProfile.Student_Assets__c);

        System.assertEquals(efcData.pjOverrideDefaultColaValue, efcProfile.Override_Default_COLA_Value__c);
        System.assertEquals(efcData.pjUseDividendInterestIncomeToImputeAssets, efcProfile.Use_Div_Int_to_Impute_Assets__c);
        System.assertEquals(efcData.pjPercentageForImputingAssets, efcProfile.Impute__c);
        System.assertEquals(efcData.pjUseHomeEquity, efcProfile.Use_Home_Equity__c);
        System.assertEquals(efcData.pjUseHomeEquityCap, efcProfile.Use_Home_Equity_Cap__c);
        System.assertEquals(efcData.pjUseHousingIndexMultiplier, efcProfile.Use_Housing_Index_Multiplier__c);
        System.assertEquals(efcData.adjustHousingPortion, efcProfile.Adjust_Housing_Portion_of_IPA__c);
        System.assertEquals(efcData.ipaHousingFamilyOf4, efcProfile.IPA_Housing_Family_of_4__c);
    }

    @isTest
    private static void getSumFromSBFAsExcludingScorpPartnerships_toggleEnabled_1819academicYear_netProfitLoss()
    {
        FeatureToggles.setToggle('Exclude_S_Corps_Partnerships_From_Sums__c', true);
        createDataForGetSumFromSBFAsTest();

        List<School_Biz_Farm_Assignment__c> sbfaList = SchoolBizFarmAssignmentSelector.newInstance().selectAllWithCustomFieldList(
            new List<String>{'Business_Entity_Type__c', 'Net_Profit_Loss_Business_Farm__c', 'Orig_Net_Profit_Loss_Business_Farm__c',
                'Business_Farm__r.Academic_Year__c', 'Business_Farm__r.Business_Entity_Type__c'});

        Decimal netProfitLossBusinessFarm;

        Test.startTest();
            netProfitLossBusinessFarm = EfcDataManager.getSumFromSBFAsExcludingScorpPartnerships(
                sbfaList, 'Net_Profit_Loss_Business_Farm__c', 'Orig_Net_Profit_Loss_Business_Farm__c');

            System.assertEquals(30, netProfitLossBusinessFarm);
        Test.stopTest();
    }

    private static void createDataForGetSumFromSBFAsTest()
    {
        Integer netProfitLossValue = 10;
        Integer shareValue = 10;
        String acadmeicYear2017_2018 = '2017-2018';
        String BUSINESS_ENTITY_SOLE_PROPRIETORSHIP = 'Sole Proprietorship';

        PFS__c pfs2017_2018 = PfsTestData.Instance
            .forAcademicYearPicklist(acadmeicYear2017_2018).create();
        PFS__c pfs2018_2019 = PfsTestData.Instance
            .forAcademicYearPicklist(EfcDataManager.ACADEMIC_YEAR_2018_2019).create();
        insert new List<PFS__c>{pfs2017_2018, pfs2018_2019};

        Business_Farm__c businessFarm1 = BusinessFarmTestData.Instance
            .forPfsId(pfs2017_2018.Id)
            .forBusinessEntityType(BUSINESS_ENTITY_SOLE_PROPRIETORSHIP).create();
        Business_Farm__c businessFarm2 = BusinessFarmTestData.Instance
            .forPfsId(pfs2017_2018.Id)
            .forBusinessEntityType(BUSINESS_ENTITY_SOLE_PROPRIETORSHIP).create();
        Business_Farm__c businessFarm3 = BusinessFarmTestData.Instance
            .forPfsId(pfs2018_2019.Id)
            .forBusinessEntityType(BUSINESS_ENTITY_SOLE_PROPRIETORSHIP).create();
        Business_Farm__c businessFarm4 = BusinessFarmTestData.Instance
            .forPfsId(pfs2018_2019.Id)
            .forBusinessEntityType(EfcDataManager.BUSINESS_ENTITY_TYPE_PARTNERSHIP).create();
        Business_Farm__c businessFarm5 = BusinessFarmTestData.Instance
            .forPfsId(pfs2018_2019.Id)
            .forBusinessEntityType(EfcDataManager.BUSINESS_ENTITY_TYPE_CORPORATION).create();
        insert new List<Business_Farm__c>{businessFarm1, businessFarm2, businessFarm3, businessFarm4, businessFarm5};

        School_Biz_Farm_Assignment__c sbfa1 = SchoolBusinessFarmAssignmentTestData.Instance
            .forNetProfitLossBusinessFarm(netProfitLossValue)
            .forOrigBusinessFarmShare(shareValue)
            .forBusinessFarm(businessFarm1.Id)
            .withBusinessType(BUSINESS_ENTITY_SOLE_PROPRIETORSHIP)
            .create();
        School_Biz_Farm_Assignment__c sbfa2 = SchoolBusinessFarmAssignmentTestData.Instance
            .forNetProfitLossBusinessFarm(netProfitLossValue)
            .forOrigBusinessFarmShare(shareValue)
            .withBusinessType(BUSINESS_ENTITY_SOLE_PROPRIETORSHIP)
            .forBusinessFarm(businessFarm2.Id).create();
        School_Biz_Farm_Assignment__c sbfa3 = SchoolBusinessFarmAssignmentTestData.Instance
            .forNetProfitLossBusinessFarm(netProfitLossValue)
            .forOrigBusinessFarmShare(shareValue)
            .withBusinessType(BUSINESS_ENTITY_SOLE_PROPRIETORSHIP)
            .forBusinessFarm(businessFarm3.Id).create();
        School_Biz_Farm_Assignment__c sbfa4 = SchoolBusinessFarmAssignmentTestData.Instance
            .forNetProfitLossBusinessFarm(netProfitLossValue)
            .forOrigBusinessFarmShare(shareValue)
            .withBusinessType(EfcDataManager.BUSINESS_ENTITY_TYPE_PARTNERSHIP)
            .forBusinessFarm(businessFarm4.Id).create();
        School_Biz_Farm_Assignment__c sbfa5 = SchoolBusinessFarmAssignmentTestData.Instance
            .forNetProfitLossBusinessFarm(netProfitLossValue)
            .forOrigBusinessFarmShare(shareValue)
            .withBusinessType(EfcDataManager.BUSINESS_ENTITY_TYPE_CORPORATION)
            .forBusinessFarm(businessFarm5.Id).create();
        insert new List<School_Biz_Farm_Assignment__c>{sbfa1, sbfa2, sbfa3, sbfa4, sbfa5};
    }
}