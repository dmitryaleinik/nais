@isTest
private class FamilyAppViewPFSPDFControllerTest
{

    @isTest
    private static void TestPFSPDFController()
    {
        // create a new PFS
        List<Academic_Year__c> academicYears = TestUtils.createAcademicYears();
        Account testAccount = TestUtils.createAccount('Test Account', RecordTypes.individualAccountTypeId, 5, true);
        Contact theParent = TestUtils.createContact('Parent A', testAccount.Id, RecordTypes.parentContactTypeId, true);
        User parentAUser = TestUtils.createPortalUser('Test User', 'test@parentAUser.com', 'TestP', theParent.Id, GlobalVariables.familyPortalProfileId, true, true);                 
        PFS__c thePFS = TestUtils.createPFS('Test PFS', academicYears[0].id, theParent.id, true);
        Contact theStudent = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, true);
        
        // create applicant 
        Applicant__c theApplicant = TestUtils.createApplicant(theStudent.Id, thePFS.Id, true);
        Student_Folder__c theStudentFolder = TestUtils.createStudentFolder('Student Folder 1', academicYears[0].id, theStudent.Id, true);
        Account theSchool = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, null, true);
        
        // Create School assignment 
        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYears[0].id, theApplicant.Id, theSchool.Id, theStudentFolder.Id, true);
        
        ApexPages.currentPage().getParameters().put('Id', thePFS.id);
        FamilyAppViewPFSPDFController controller = new FamilyAppViewPFSPDFController();
        System.assert(controller.pfs != null);
        System.assertEquals(thePFS.id, controller.pfs.id);   
    }
}