@IsTest
private class PhoneHelperTest
{

    @isTest
    private static void testHelper()
    {
        //Valid area codes cannot start with a 1
        String p1='232-123-1234';
        String p2='1(232) 123-1234';
        String p3='+1(232) 123-1234';
        String p4='9@#$8123(232) 123-1234';

        System.Assert(PhoneHelper.isValidPhone(p1));
        System.Assert(PhoneHelper.isValidPhone(p2));
        System.Assert(PhoneHelper.isValidPhone(p3));
        System.Assert(!PhoneHelper.isValidPhone(p4));

        System.Assertequals(PhoneHelper.stripLeading1(p2),'(232) 123-1234' );
        System.Assertequals(PhoneHelper.stripLeading1(p3),'(232) 123-1234' );
        System.Assertequals(PhoneHelper.getSFPhone(p1),'(232) 123-1234' );
        System.Assertequals(PhoneHelper.getSFSearchablePhone(p1),'%232%123%1234' );
    }
}