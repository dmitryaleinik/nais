@isTest
private class ApplicantCustomButtonTest {
    
    private static Account school;
    private static Applicant__c applicant;
    private static PFS__c pfs;
    private static Student_Folder__c studentFolder;
    private static Academic_Year__c academicYear;
    private static School_PFS_Assignment__c schoolPfsAssignment;
    private static School_Document_Assignment__c schoolDocumentAssignment;
    private static Required_Document__c requiredDocument;
    private static String documentYear;
    
    private static void setup(Boolean createTestData){
        
        if(createTestData) {
            // 1. Setup Databank custom settings.
            SpringCMActionTest.setupCustomSettings();
            SpringCMActionTest.setupMappings();
            
            //2. Create test data records.
            academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
            
            documentYear = academicYear.Name.left(4);
            
            school = AccountTestData.Instance.forName('School1')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccount();
                
            Contact parentA = ContactTestData.Instance.asParent().create();
            Contact parentB = ContactTestData.Instance.asParent().create();
            Contact student = ContactTestData.Instance.asStudent().create();
            insert new List<Contact> { parentA, parentB, student };
                
            pfs = PfsTestData.Instance
                .forParentA(parentA.Id)
                .forParentB(parentB.Id)
                .forAcademicYearPicklist(academicYear.Name)
                .asPaid()
                .insertPfs();
            
            applicant = ApplicantTestData.Instance
                .forContactId(student.Id)
                .forPfsId(pfs.Id)
                .insertApplicant();
                
            studentFolder = StudentFolderTestData.Instance
                .forAcademicYearPicklist(academicYear.Name)
                .forStudentId(student.Id)
                .insertStudentFolder();
                
            schoolPfsAssignment = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant.Id)
                .forStudentFolderId(studentFolder.Id)
                .forSchoolId(school.Id)
                .forAcademicYearPicklist(academicYear.Name)
                .createSchoolPfsAssignmentWithoutReset();
            insert schoolPfsAssignment;
            
            requiredDocument = RequiredDocumentTestData.Instance
                .forSchoolId(school.Id)
                .forDocType('W2')
                .forYear(documentYear)
                .insertRequiredDocument();
                
            Family_Document__c document = FamilyDocumentTestData.Instance
                .forDocumentType('W2')
                .insertFamilyDocument();
            
	        schoolDocumentAssignment = SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE)
                .asType('W2')
                .forDocumentYear(documentYear)
                .forPfsAssignment(schoolPfsAssignment.Id)
                .forRequirement(requiredDocument.Id)
                .insertSchoolDocumentAssignment();
        }
    }//End:setup
    
    //SFP-890
    @isTest static void DeleteApplicantFromCustomButton_validApplicantId_ApplicantAndRelatedRecordsDeleted() {
        
        //0. Create test data.
        setup(true);
        
        //1. Retrieve the test pfs record.
        PFS__c pfsRecord = [
            SELECT Id, (
            SELECT Id FROM Applicants__r) 
            FROM PFS__c 
            WHERE Id =: pfs.Id 
            LIMIT 1];
        
        //2. Define the applicant to be deleted.  
        Id applicantIdToDelete = pfsRecord.Applicants__r[0].Id;
        
        //3. Retrieve the Applicant and SPA records that will be deleted.
        List<Applicant__c> applicantToDelete = new List<Applicant__c>([
            SELECT Id,  
            (SELECT Id, Student_Folder__c 
                FROM School_PFS_Assignments__r) 
            FROM Applicant__c 
            WHERE Id = :applicantIdToDelete 
            LIMIT 1]);
        
        //4. Retrieve the StudentFolder records that will be deleted.
        Set<Id> folderIds = new Set<Id>();
        Set<Id> spaIds = new Set<Id>();
        for(School_PFS_Assignment__c spa : applicantToDelete[0].School_PFS_Assignments__r) {
            folderIds.add(spa.Student_Folder__c);
            spaIds.add(spa.Id);
        }
        
        List<Student_Folder__c> folders = new List<Student_Folder__c>([
                SELECT Id 
                FROM Student_Folder__c 
                WHERE Id =: folderIds 
                LIMIT 1]);
                
        List<School_Document_Assignment__c> sdas = new List<School_Document_Assignment__c>([
                SELECT Id 
                FROM School_Document_Assignment__c 
                WHERE School_PFS_Assignment__c IN: spaIds]);
                
        System.assertEquals(true, sdas.size() > 0);
        
        String resultMessage;
        
        Test.startTest();
            //5. Execute the method that needs to be tested.
            ApplicantCustomButton controller = new ApplicantCustomButton(new ApexPages.StandardController(applicantToDelete[0]));
            controller.DeleteApplicantFromCustomButton();
            System.assertEquals(true, controller.getSuccessURL() != null);
        Test.stopTest();
        
        //6. Verify that the success message is returned by the excuted method
        System.assertEquals(controller.resultMessage, 
                            String.format(Label.Delete_Applicant_Process_Completed, 
                            new List<String>{
                            String.ValueOf(applicantToDelete[0].School_PFS_Assignments__r.size()),
                            String.ValueOf(folders.size()),
                            String.ValueOf(sdas.size())}));
        
        //7. Verify that the records related to the applicant were successfully deleted.
        System.assertEquals(0, (new List<Student_Folder__c>([
                SELECT Id 
                FROM Student_Folder__c 
                WHERE Id =: folderIds]).size()),
                'Related StudentFolders were not successfully deleted.');
        
        System.assertEquals(0, (new List<Applicant__c>([
                SELECT Id 
                FROM Applicant__c 
                WHERE Id =: applicantIdToDelete]).size()),
                'Selected Applicant was not deleted.');
        
        System.assertEquals(0, (new List<School_PFS_Assignment__c>([
                SELECT Id 
                FROM School_PFS_Assignment__c 
                WHERE Id IN: spaIds]).size()),
                'Related SPAs were not successfully deleted.');
                
        System.assertEquals(0, (new List<School_Document_Assignment__c>([
                SELECT Id 
                FROM School_Document_Assignment__c 
                WHERE School_PFS_Assignment__c IN: spaIds]).size()),
                'Related SDAs were not successfully deleted.');
        
    }//End:DeleteApplicantFromCustomButton_validApplicantId_ApplicantAndRelatedRecordsDeleted
}//End-Class