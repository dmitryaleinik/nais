/*
 * SPEC-146, NAIS-85
 *
 * Check to make sure a W2 is not a duplicate
 *
 * Nathan, Exponent Partners, 2013
 */
@isTest
private class W2DuplicateCheckTest
{

    private class TestData {
        String academicYearId;
        String academicYearName;
        Household__c household1, household2, household3;
        
        private TestData() {
            // academic year
            TestUtils.createAcademicYears();
            Academic_Year__c academicYear = GlobalVariables.getCurrentAcademicYear();
            academicYearId = academicYear.Id;
            academicYearName = academicYear.Name;
            
            // household
            household1 = TestUtils.createHousehold('Household 1', false);
            insert household1;
            household2 = TestUtils.createHousehold('Household 2', false);
            insert household2;
            household3 = TestUtils.createHousehold('Household 3', false);
            insert household3;
        }
    }

    private static final String DOC_TYPE_W2 = 'W2';
    private static final String DOC_TYPE_1099 = '1099';
    private static final String FAMILY_DOC_NAME_PREFIX = 'FD ';
    // This will be used to insert family docs with unique names.
    private static Integer familyDocCounter = 1;

    private static String generateDocName() {
        String docName = FAMILY_DOC_NAME_PREFIX + familyDocCounter;

        familyDocCounter++;

        return docName;
    }

    private static final Integer DEFAULT_WAGES = 10000;
    private static final Integer DEFAULT_DOC_YEAR = 2010;
    private static final String DUPLICATE_EMPLOYEE_ID = '1234567890';
    private static final String DUPLICATE_EMPLOYER_ID = '01234567890';

    private static final String PROCESSED_DOCUMENT_STATUS = 'Processed';

    private static Integer w2InfoCounter = 1;
    private static final String EMPLOYEE_ID_PREFIX = '95489874';
    private static final String EMPLOYER_ID_PREFIX = '3214568164';

    private static void setDuplicateW2Fields(Family_Document__c docToUpdate) {
        docToUpdate.W2_Wages__c = DEFAULT_WAGES;
        docToUpdate.W2_Employee_ID__c = DUPLICATE_EMPLOYEE_ID;
        docToUpdate.W2_Employer_ID__c = DUPLICATE_EMPLOYER_ID;
        docToUpdate.Document_Year__c = String.valueOf(DEFAULT_DOC_YEAR);
    }

    private static void setUniqueW2Fields(Family_Document__c docToUpdate) {
        docToUpdate.W2_Wages__c = DEFAULT_WAGES + w2InfoCounter;
        docToUpdate.W2_Employee_ID__c = EMPLOYEE_ID_PREFIX + w2InfoCounter;
        docToUpdate.W2_Employer_ID__c = EMPLOYER_ID_PREFIX + w2InfoCounter;
        docToUpdate.Document_Year__c = String.valueOf(DEFAULT_DOC_YEAR + w2InfoCounter);

        w2InfoCounter++;
    }

    private static void clearW2DuplicateKeyFields(Family_Document__c docToUpdate) {
        docToUpdate.W2_Wages__c = null;
        docToUpdate.W2_Employee_ID__c = null;
        docToUpdate.W2_Employer_ID__c = null;
    }

    private static Family_Document__c createDocument(TestData currentTestData, Id houseHoldId, Boolean duplicateW2Fields, String docType) {
        Family_Document__c newFamilyDoc = TestUtils.createFamilyDocument(generateDocName(), currentTestData.academicYearName, docType, houseHoldId, false);

        if (duplicateW2Fields) {
            setDuplicateW2Fields(newFamilyDoc);
        } else {
            setUniqueW2Fields(newFamilyDoc);
        }

        newFamilyDoc.Document_Status__c = PROCESSED_DOCUMENT_STATUS;

        return newFamilyDoc;
    }

    private static Family_Document__c createW2(TestData currentTestData, Id houseHoldId, Boolean duplicateW2Fields) {
        return createDocument(currentTestData, houseHoldId, duplicateW2Fields, DOC_TYPE_W2);
    }

    private static Family_Document__c create1099(TestData currentTestData, Id houseHoldId) {
        Family_Document__c newFamilyDoc = TestUtils.createFamilyDocument(generateDocName(), currentTestData.academicYearName, DOC_TYPE_1099, houseHoldId, false);

        return newFamilyDoc;
    }

    private static void assertDuplicates(Set<Id> expectedDupeIds, Set<Id> unexpectedDupeIds) {
        // Create a set of all the family document Ids.
        Set<Id> allFamilyDocIds = new Set<Id>(expectedDupeIds);
        allFamilyDocIds.addAll(unexpectedDupeIds);

        System.assert(!(allFamilyDocIds.contains(null)), 'The family doc Ids should not be null for this assertion.');

        List<Family_Document__c> familyDocs = FamilyDocumentsSelector.newInstance().selectById(allFamilyDocIds);

        for (Family_Document__c doc : familyDocs) {
            if (expectedDupeIds.contains(doc.Id)) {
                System.assertEquals('Yes', doc.Duplicate__c, 'Expected the family doc to be marked as a duplicate: ' + doc);
            } else {
                System.assertNotEquals('Yes', doc.Duplicate__c, 'Expected the family doc to not be marked as a duplicate: ' + doc);
            }
        }
    }

    @isTest
    private static void duplicateCheck_triggerDisabledForInserts_familyDocsForSameHouseholdWithNullW2EmployeeFields_expectNoDuplicates() {
        TestData currentTestData = new TestData();

        Boolean duplicateW2Fields = true;
        Family_Document__c familyDoc1 = createW2(currentTestData, currentTestData.houseHold1.Id, duplicateW2Fields);
        clearW2DuplicateKeyFields(familyDoc1);
        Family_Document__c familyDoc2 = createW2(currentTestData, currentTestData.houseHold1.Id, duplicateW2Fields);
        clearW2DuplicateKeyFields(familyDoc2);

        W2DuplicateCheck.isExecuting = true;
        List<Family_Document__c> familyDocs = new List<Family_Document__c> { familyDoc1, familyDoc2 };
        insert familyDocs;
        W2DuplicateCheck.isExecuting = false;

        Set<String> householdIds = new Set<String> { (String)currentTestData.houseHold1.Id };
        W2DuplicateCheck.DuplicateCheck(householdIds);

        Set<Id> expectedDuplicateIds = new Set<Id>();
        Set<Id> unexpectedDuplicateIds = new Set<Id> { familyDoc1.Id, familyDoc2.Id };

        assertDuplicates(expectedDuplicateIds, unexpectedDuplicateIds);
    }

    // To test methods defined with the future annotation, call the class containing the method in a 
    // startTest, stopTest code block. All asynchronous calls made after the startTest method are collected 
    // by the system. When stopTest is executed, all asynchronous processes are run synchronously 
     
    @isTest
    private static void testNewW2()
    {
        TestData td = new TestData();
        
        Test.startTest();

        Boolean duplicateW2Fields = false;
        Family_Document__c familyDoc1 = createW2(td, td.houseHold1.Id, duplicateW2Fields);
        insert familyDoc1;

        Test.stopTest();

        Set<Id> expectedDupeIds = new Set<Id>();
        Set<Id> unexpectedDupeIds = new Set<Id> { familyDoc1.Id };

        assertDuplicates(expectedDupeIds, unexpectedDupeIds);
    }

    @isTest
    private static void testDuplicateW2()
    {
        TestData td = new TestData();
        
        Test.startTest();
        
        // same household. fd2 and fd3 are duplicates
        // Set employee Ids to null to verify that not all of the fields used for the duplicate keys are required.
        Boolean duplicateW2Fields = true;
        Family_Document__c fd1 = createW2(td, td.household1.Id, duplicateW2Fields);
        fd1.W2_Employee_ID__c = null;
        insert fd1;

        Family_Document__c fd2 = createW2(td, td.household1.Id, duplicateW2Fields);
        fd2.W2_Employee_ID__c = null;
        insert fd2;

        Family_Document__c fd3 = createW2(td, td.household1.Id, duplicateW2Fields);
        fd3.W2_Employee_ID__c = null;
        insert fd3;

        Test.stopTest();

        Set<Id> expectedDupeIds = new Set<Id> { fd2.Id, fd3.Id };
        Set<Id> unexpectedDupeIds = new Set<Id> { fd1.Id };

        assertDuplicates(expectedDupeIds, unexpectedDupeIds);
    }

    @isTest
    private static void testNonDuplicateW2()
    {
        TestData td = new TestData();
        
        Test.startTest();
        
        // different households should not yield duplicate W2s.
        Boolean duplicateW2Fields = true;
        Family_Document__c fd1 = createW2(td, td.household1.Id, duplicateW2Fields);
        insert fd1;

        Family_Document__c fd2 = createW2(td, td.household2.Id, duplicateW2Fields);
        insert fd2;

        Family_Document__c fd3 = createW2(td, td.household3.Id, duplicateW2Fields);
        insert fd3;

        Test.stopTest();

        Set<Id> expectedDupeIds = new Set<Id>();
        Set<Id> unexpectedDupeIds = new Set<Id> { fd1.Id, fd2.Id, fd3.Id };

        assertDuplicates(expectedDupeIds, unexpectedDupeIds);
    }

    @isTest
    private static void testDuplicateToNonDuplicateW2()
    {
        TestData td = new TestData();
        
        Test.startTest();
        
        // same household. fd2 and fd3 are duplicates
        Boolean duplicateW2Fields = true;
        Family_Document__c fd1 = createW2(td, td.household1.Id, duplicateW2Fields);
        insert fd1;

        Family_Document__c fd2 = createW2(td, td.household1.Id, duplicateW2Fields);
        insert fd2;

        Family_Document__c fd3 = createW2(td, td.household1.Id, duplicateW2Fields);
        insert fd3;

        fd2.Household__c = td.household2.Id;
        update fd2;

        Test.stopTest();

        Set<Id> expectedDupeIds = new Set<Id> { fd3.Id };
        Set<Id> unexpectedDupeIds = new Set<Id> { fd1.Id, fd2.Id };

        assertDuplicates(expectedDupeIds, unexpectedDupeIds);
    }

    @isTest
    private static void testNonDuplicateToDuplicateW2()
    {
        TestData td = new TestData();
        
        Test.startTest();
        
        // different household. it is not duplicate
        Boolean duplicateW2Fields = true;
        Family_Document__c fd1 = createW2(td, td.household1.Id, duplicateW2Fields);
        insert fd1;
        Family_Document__c fd2 = createW2(td, td.household2.Id, duplicateW2Fields);
        insert fd2;
        Family_Document__c fd3 = createW2(td, td.household3.Id, duplicateW2Fields);
        insert fd3;
        fd1.Household__c = td.household2.Id;
        update fd1;

        Test.stopTest();

        Set<Id> expectedDupeIds = new Set<Id> { fd2.Id };
        Set<Id> unexpectedDupeIds = new Set<Id> { fd1.Id, fd3.Id };

        assertDuplicates(expectedDupeIds, unexpectedDupeIds);
    }

    @isTest
    private static void testW2Fields()
    {
        TestData td = new TestData();
        
        Test.startTest();
        
        // same household but w2 fields are different
        Boolean duplicateW2Fields = false;
        Family_Document__c fd1 = createW2(td, td.household1.Id, duplicateW2Fields);
        insert fd1;

        Family_Document__c fd2 = createW2(td, td.household1.Id, duplicateW2Fields);
        insert fd2;

        Family_Document__c fd3 = createW2(td, td.household1.Id, duplicateW2Fields);
        insert fd3;

        Test.stopTest();

        Set<Id> expectedDupeIds = new Set<Id>();
        Set<Id> unexpectedDupeIds = new Set<Id> { fd1.Id, fd2.Id, fd3.Id };

        assertDuplicates(expectedDupeIds, unexpectedDupeIds);
    }

    @isTest
    private static void testW2DocType()
    {
        TestData td = new TestData();
        
        Test.startTest();
        
        // same household but one doc is a 1099
        Boolean duplicateW2Fields = true;
        Family_Document__c fd1 = createW2(td, td.household1.Id, duplicateW2Fields);
        insert fd1;
        Family_Document__c fd2 = create1099(td, td.household1.Id);
        insert fd2;
        Family_Document__c fd3 = createW2(td, td.household1.Id, duplicateW2Fields);
        insert fd3;

        Test.stopTest();

        Set<Id> expectedDupeIds = new Set<Id> { fd3.Id };
        Set<Id> unexpectedDupeIds = new Set<Id> { fd1.Id, fd2.Id};

        assertDuplicates(expectedDupeIds, unexpectedDupeIds);
    }

    @isTest
    private static void testBulk()
    {
        Integer batchSize = 200;
        TestData td = new TestData();
        
        Test.startTest();

        Boolean duplicateW2Fields = true;

        List <Family_Document__c> fdList = new List <Family_Document__c>();
        for (Integer i=0; i<batchSize; i++) {
        	Family_Document__c fd = createW2(td, td.household1.Id, duplicateW2Fields);
            fdList.add(fd);
        }
        insert fdList;

        Test.stopTest();

        Set<Id> expectedDupeIds = new Map<Id, Family_Document__c>(fdList).keySet();
        expectedDupeIds.remove(fdList[0].Id);
        Set<Id> unexpectedDupeIds = new Set<Id> { fdList[0].Id };

        assertDuplicates(expectedDupeIds, unexpectedDupeIds);
    }

    @isTest
    private static void testW2DocYear()
    {
        TestData td = new TestData();
        
        Test.startTest();
        
        // same household but w2 fields are different
        Boolean duplicateW2Fields = false;

        Family_Document__c fd1 = createW2(td, td.household1.Id, duplicateW2Fields);
        insert fd1;

        duplicateW2Fields = true;
        Family_Document__c fd2 = createW2(td, td.household1.Id, duplicateW2Fields);
        insert fd2;

        Family_Document__c fd3 = createW2(td, td.household1.Id, duplicateW2Fields);
        insert fd3;

        Test.stopTest();

        Set<Id> expectedDupeIds = new Set<Id> { fd3.Id };
        Set<Id> unexpectedDupeIds = new Set<Id> { fd1.Id, fd2.Id};

        assertDuplicates(expectedDupeIds, unexpectedDupeIds);
    }
}