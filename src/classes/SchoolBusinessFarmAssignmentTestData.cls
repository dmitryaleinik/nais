/**
 * @description This class is used to create School Business Farm Assignment records for unit tests.
 */
@isTest
public class SchoolBusinessFarmAssignmentTestData extends SObjectTestData 
{

    /**
     * @description Get the default values for the School_Biz_Farm_Assignment__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
            School_Biz_Farm_Assignment__c.Business_Farm__c => BusinessFarmTestData.Instance.DefaultBusinessFarm.Id,
            School_Biz_Farm_Assignment__c.School_PFS_Assignment__c => SchoolPfsAssignmentTestData.Instance.DefaultSchoolPfsAssignment.Id
        };
    }

    /**
     * @description Sets the Business Farm field on the current School Business Farm Assignment record.
     * @param businessFarmId The Business or Farm Id to set Business_Farm__c field.
     * @return The current working instance of SchoolBusinessFarmAssignmentTestData.
     */
    public SchoolBusinessFarmAssignmentTestData forBusinessFarm(Id businessFarmId) {
        return (SchoolBusinessFarmAssignmentTestData) with(School_Biz_Farm_Assignment__c.Business_Farm__c, businessFarmId);
    }

    /**
     * @description Sets the School PFS Assignment field on the current School Business Farm Assignment record.
     * @param schoolPfsAssignmentId The School PFS Assignment Id to set School_PFS_Assignment__c field.
     * @return The current working instance of SchoolBusinessFarmAssignmentTestData.
     */
    public SchoolBusinessFarmAssignmentTestData forSchoolPfsAssignment(Id schoolPfsAssignmentId) {
        return (SchoolBusinessFarmAssignmentTestData) with(School_Biz_Farm_Assignment__c.School_PFS_Assignment__c, schoolPfsAssignmentId);
    }

    /**
     * @description Sets the Business/Farm Assets field on the current School Business Farm Assignment record.
     * @param businessFarmAssets The Business Farm Assets to set Business_Farm_Assets__c field.
     * @return The current working instance of SchoolBusinessFarmAssignmentTestData.
     */
    public SchoolBusinessFarmAssignmentTestData forBusinessFarmAssets(Integer businessFarmAssets) {
        return (SchoolBusinessFarmAssignmentTestData) with(School_Biz_Farm_Assignment__c.Business_Farm_Assets__c, businessFarmAssets);
    }

    /**
     * @description Sets the Business/Farm Debts field on the current School Business Farm Assignment record.
     * @param businessFarmDebts The Business Farm Debts to set Business_Farm_Debts__c field.
     * @return The current working instance of SchoolBusinessFarmAssignmentTestData.
     */
    public SchoolBusinessFarmAssignmentTestData forBusinessFarmDebts(Integer businessFarmDebts) {
        return (SchoolBusinessFarmAssignmentTestData) with(School_Biz_Farm_Assignment__c.Business_Farm_Debts__c, businessFarmDebts);
    }

    /**
     * @description Sets the Business/Farm Equity Share field on the current School Business Farm Assignment record.
     * @param businessFarmEquityShare The Business Farm Equity Share to set Business_Farm_Share__c field.
     * @return The current working instance of SchoolBusinessFarmAssignmentTestData.
     */
    public SchoolBusinessFarmAssignmentTestData forBusinessFarmEquityShare(Integer businessFarmEquityShare) {
        return (SchoolBusinessFarmAssignmentTestData) with(School_Biz_Farm_Assignment__c.Business_Farm_Share__c, businessFarmEquityShare);
    }

    /**
     * @description Sets the Business/Farm Owner field on the current School Business Farm Assignment record.
     * @param businessFarmOwner The Business Farm Owner to set Business_Farm_Owner__c field.
     * @return The current working instance of SchoolBusinessFarmAssignmentTestData.
     */
    public SchoolBusinessFarmAssignmentTestData forBusinessFarmOwner(String businessFarmOwner) {
        return (SchoolBusinessFarmAssignmentTestData) with(School_Biz_Farm_Assignment__c.Business_Farm_Owner__c, businessFarmOwner);
    }

    /**
     * @description Sets the Business/Farm Ownership Percent field on the current School Business Farm Assignment record.
     * @param businessFarmOwnershipPercent The Business Farm Ownership Percent to set Business_Farm_Ownership_Percent__c field.
     * @return The current working instance of SchoolBusinessFarmAssignmentTestData.
     */
    public SchoolBusinessFarmAssignmentTestData forBusinessFarmOwnershipPercent(Integer businessFarmOwnershipPercent) {
        return (SchoolBusinessFarmAssignmentTestData) with(School_Biz_Farm_Assignment__c.Business_Farm_Ownership_Percent__c, businessFarmOwnershipPercent);
    }

    /**
     * @description Sets the Net Profit/Loss Business/Farm field on the current School Business Farm Assignment record.
     * @param netProfitLossBusinessFarm The Net Profit/Loss Business/Farm to set Net_Profit_Loss_Business_Farm__c field.
     * @return The current working instance of SchoolBusinessFarmAssignmentTestData.
     */
    public SchoolBusinessFarmAssignmentTestData forNetProfitLossBusinessFarm(Integer netProfitLossBusinessFarm) {
        return (SchoolBusinessFarmAssignmentTestData) with(School_Biz_Farm_Assignment__c.Net_Profit_Loss_Business_Farm__c, netProfitLossBusinessFarm);
    }

    /**
     * @description Sets the Orig-Business/Farm Equity Share field on the current School Business Farm Assignment record.
     * @param origBusinessFarmShare The Orig-Business/Farm Equity Share to set Orig_Business_Farm_Share__c field.
     * @return The current working instance of SchoolBusinessFarmAssignmentTestData.
     */
    public SchoolBusinessFarmAssignmentTestData forOrigBusinessFarmShare(Integer origBusinessFarmShare) {
        return (SchoolBusinessFarmAssignmentTestData) with(School_Biz_Farm_Assignment__c.Orig_Business_Farm_Share__c, origBusinessFarmShare);
    }

    /**
     * @description Sets the Orig-Business/Farm Assets field on the current School Business Farm Assignment record.
     * @param origBusinessFarmAssets The Orig-Business/Farm Assets to set Orig_Business_Farm_Assets__c field.
     * @return The current working instance of SchoolBusinessFarmAssignmentTestData.
     */
    public SchoolBusinessFarmAssignmentTestData forOrigBusinessFarmAssets(Integer origBusinessFarmAssets)
    {
        return (SchoolBusinessFarmAssignmentTestData) with(School_Biz_Farm_Assignment__c.Orig_Business_Farm_Assets__c, origBusinessFarmAssets);
    }

    /**
     * @description Sets the Business Entity Type orig and revision fields.
     * @param businessType The type of business. (e.g. Partnership, Corporation)
     * @return The current instance.
     */
    public SchoolBusinessFarmAssignmentTestData withBusinessType(String businessType) {
        return (SchoolBusinessFarmAssignmentTestData)with(School_Biz_Farm_Assignment__c.Business_Entity_Type__c, businessType)
                .with(School_Biz_Farm_Assignment__c.Orig_Business_Entity_Type__c, businessType);
    }

    /**
     * @description Insert the current working School_Biz_Farm_Assignment__c record.
     * @return The currently operated upon School_Biz_Farm_Assignment__c record.
     */
    public School_Biz_Farm_Assignment__c insertSchoolBusinessFarmAssignment() {
        return (School_Biz_Farm_Assignment__c)insertRecord();
    }

    /**
     * @description Create the current working School Business Farm Assignment record without resetting
     *             the stored values in this instance of SchoolBusinessFarmAssignmentTestData.
     * @return A non-inserted School_Biz_Farm_Assignment__c record using the currently stored field
     *             values.
     */
    public School_Biz_Farm_Assignment__c createSchoolBusinessFarmAssignmentWithoutReset() {
        return (School_Biz_Farm_Assignment__c)buildWithoutReset();
    }

    /**
     * @description Create the current working School_Biz_Farm_Assignment__c record.
     * @return The currently operated upon School_Biz_Farm_Assignment__c record.
     */
    public School_Biz_Farm_Assignment__c create() {
        return (School_Biz_Farm_Assignment__c)super.buildWithReset();
    }

    /**
     * @description The default School_Biz_Farm_Assignment__c record.
     */
    public School_Biz_Farm_Assignment__c DefaultSchoolBusinessFarmAssignment {
        get {
            if (DefaultSchoolBusinessFarmAssignment == null) {
                DefaultSchoolBusinessFarmAssignment = createSchoolBusinessFarmAssignmentWithoutReset();
                insert DefaultSchoolBusinessFarmAssignment;
            }
            return DefaultSchoolBusinessFarmAssignment;
        }
        private set;
    }

    /**
     * @description Get the School_Biz_Farm_Assignment__c SObjectType.
     * @return The School_Biz_Farm_Assignment__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return School_Biz_Farm_Assignment__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static SchoolBusinessFarmAssignmentTestData Instance {
        get {
            if (Instance == null) {
                Instance = new SchoolBusinessFarmAssignmentTestData();
            }
            return Instance;
        }
        private set;
    }

    private SchoolBusinessFarmAssignmentTestData() {}
}