/**
 * @description Unit test helper methods.
 */
@isTest
public class TestHelpers {
    private static final String EXCEPTION_PARAM = 'e';
    private static final String EXCEPTION_TYPE_PARAM = 'exceptionType';
    private static final String PARAM_NAME_PARAM = 'paramName';
    private static final String MESSAGE_PARAM = 'message';
    private static final String EXPECTED_MESSAGE_PARAM = 'expectedMessage';
    private static final String EXPECTED_TYPE_PARAM = 'expectedType';

    private static final String EXPECTED_EXCEPTION_ASSERTION = 'Expected {0} to be thrown.';
    private static final String EXCEPTION_MESSAGE_ASSERTION = 'Expected the exception message to match.';

    /**
     * @description Validate whether a caught exception is an ArgumentNullException
     *              or not.
     * @param e The exception to check.
     * @param paramName The parameter name to validate the exception against to ensure
     *        it's the expected ArgumentNullException.
     * @throws An ArgumentNullException if either parameter is null.
     */
    public static void assertArgumentNullException(Exception e, String paramName) {
        ArgumentNullException.throwIfNull(e, EXCEPTION_PARAM);
        ArgumentNullException.throwIfNull(paramName, PARAM_NAME_PARAM);

        System.assertEquals(ArgumentNullException.class.getName(), e.getTypeName(),
                'Expected an ArgumentNullException.');

        String expectedMessage = String.format(ArgumentNullException.MESSAGE, new List<String> { paramName });
        System.assertEquals(expectedMessage, e.getMessage(), 'Expected the expectedMessage returned.');
    }

    /**
     * @description Fail the unit test for failure to catch an ArgumentNullException.
     */
    public static void expectedArgumentNullException() {
        System.assert(false, 'Expected an ArgumentNullException to be thrown.');
    }

    /**
     * @description Validate whether a caught exception is an ArgumentException or not,
     *              failing the test if not.
     * @param e The exception to check.
     * @param message The expected message thrown with the Argument Exception.
     * @throws An ArgumentNullException if either of the parameters are null.
     */
    public static void assertArgumentException(Exception e, String message) {
        ArgumentNullException.throwIfNull(e, EXCEPTION_PARAM);
        ArgumentNullException.throwIfNull(message, MESSAGE_PARAM);

        System.assertEquals(ArgumentException.class.getName(), e.getTypeName(), 'Expected an ArgumentException.');
        System.assertEquals(message, e.getMessage(), 'Expected the message thrown by the exception to match.');
    }

    /**
     * @description Fail the unit test for failure to catch an ArgumentException.
     */
    public static void expectedArgumentException() {
        System.assert(false, 'Expected an ArgumentException to be thrown.');
    }

    /**
     * @description Validate whether a caught exception is a NavigationException or not,
     *              failing the test if not.
     * @param e The exception to check
     * @param message The expected message thrown with the NavigationException.
     * @throws An ArgumentNullException if either of the parameters are null.
     */
    public static void assertNavigationException(Exception e, String message) {
        ArgumentNullException.throwIfNull(e, EXCEPTION_PARAM);
        ArgumentNullException.throwIfNull(message, MESSAGE_PARAM);

        System.assertEquals(NavigationException.class.getName(), e.getTypeName(), 'Expected a NavigationException.');
        System.assertEquals(message, e.getMessage(), 'Expected the message thrown by the exception to match.');
    }

    /**
     * @description Fail the unit test for failure to catch a NavigationException.
     */
    public static void expectedNavigationException() {
        expectedException(NavigationException.class);
    }

    /**
     * @description Fail a unit test because the specified exception type has not been thrown.
     * @param The expected exception type.
     * @throw ArgumentNullException if exceptionType is null.
     */
    public static void expectedException(System.Type exceptionType) {
        ArgumentNullException.throwIfNull(exceptionType, EXCEPTION_TYPE_PARAM);
        System.assert(false, formatExpectedExceptionAssertion(exceptionType));
    }

    /**
     * @description Fails a unit test if the specified exception does not match the expected type.
     * @param expectedType The expected exception type.
     * @param e The actual exception that was thrown.
     * @throw ArgumentNullException if any parameter is null.
     */
    public static void assertException(System.Type expectedType, Exception e) {
        ArgumentNullException.throwIfNull(expectedType, EXPECTED_TYPE_PARAM);
        ArgumentNullException.throwIfNull(e, EXCEPTION_PARAM);

        System.assertEquals(expectedType.toString(), e.getTypeName(), 'Expected the correct exception type.');
    }

    /**
     * @description Fails a unit test if the specified exception does not match the expected type and does not contain the specified message.
     * @param expectedType The expected exception type.
     * @param expectedMessage The expected exception message.
     * @param e The actual exception that was thrown.
     * @throw ArgumentNullException if any parameter is null.
     */
    public static void assertException(System.Type expectedType, String expectedMessage, Exception e) {
        ArgumentNullException.throwIfNull(expectedType, EXPECTED_TYPE_PARAM);
        ArgumentNullException.throwIfNull(expectedMessage, EXPECTED_MESSAGE_PARAM);
        ArgumentNullException.throwIfNull(e, EXCEPTION_PARAM);

        System.assertEquals(expectedType.toString(), e.getTypeName(), 'Expected the correct exception type.');
        System.assertEquals(expectedMessage, e.getMessage(), EXCEPTION_MESSAGE_ASSERTION);
    }

    private static String formatExpectedExceptionAssertion(System.Type exceptionType) {
        return String.format(EXPECTED_EXCEPTION_ASSERTION, new List<String> { exceptionType.toString() });
    }
}