public with sharing class UnauthorizedCustomController {

    public PageReference currentPR {get; set;}

    public UnauthorizedCustomController() {
        currentPR = new PageReference(URL.getCurrentRequestUrl().toExternalForm());
    }

    public PageReference smartRedirect(){
        PageReference pr;
        if (currentPR.getUrl().contains('schoolportal')){
            pr = Page.SchoolLogin;
        } else {
            pr = Page.FamilyLogin;
            String startURL = ApexPages.currentPage().getParameters().get('startURL');
            if(CurrentUser.isGuest() && startURL!=null && startURL!='') {
                if( Site.getSiteType()=='Visualforce') {//Is not a community user
                    pr = new PageReference(FamilyRedirectTemplateController.calculateCommunityURL(startURL));
                }else{
                    pr.getParameters().put('startURL',startURL);
                }
            }
        }

        return pr;
    }
}