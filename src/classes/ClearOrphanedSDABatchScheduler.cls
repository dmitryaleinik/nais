/**
* Scheduler for the batch that daily clears SDA records with empty School_Document_Assignment field. 
*/
global class ClearOrphanedSDABatchScheduler implements Schedulable {

    global void execute(SchedulableContext sc) {
        Database.executebatch(new ClearOrphanedSDABatch());
    }
}