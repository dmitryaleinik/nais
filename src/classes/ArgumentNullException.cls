/**
  * @description A Custom Exception for handling Null Arguments.
  **/
public class ArgumentNullException extends Exception {
    /**
     * @description Argument Null Exception message to be used when
     *              an argument or set of arguments are found to be
     *              unexpectedly null.
     */
    public static final String MESSAGE = 'Expected {0} to not be null.';

    @testVisible private static final String NO_PARAMETER_MESSAGE = 'Expected there to be a parameter name provided.';

    /**
     * @description Throw an ArgumentNullException if a provided parameter is null.
     * @param parameter The parameter to null check.
     * @param parameterName The name of the parameter to check.
     */
    public static void throwIfNull(Object parameter, String parameterName) {
        if (parameterName == null) {
            throw new ArgumentNullException(NO_PARAMETER_MESSAGE);
        }
        if (parameter == null) {
            throw new ArgumentNullException(String.format(MESSAGE, new List<String> { parameterName }));
        }
    }
}