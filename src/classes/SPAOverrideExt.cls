public with sharing class SPAOverrideExt {
    
    public final School_PFS_Assignment__c spaRecord { get; set; }
    private User currentUser {get; set;}
    
    public SPAOverrideExt(ApexPages.StandardController stdController){
        spaRecord = [Select Id, Student_Folder__c from School_PFS_Assignment__c where Id = :((School_PFS_Assignment__c)stdController.getRecord()).Id];
        
        currentUser = [Select Id, ContactId from User where Id = :UserInfo.getUserId()];
    }
    
    public PageReference redirect(){
        if(currentUser.ContactId == null){
            return new PageReference('/' + spaRecord.Id + '?nooverride=true');
        }
        else{
            PageReference redirectPage = Page.SchoolFolderSummary;
            redirectPage.getParameters().put('id', spaRecord.Student_Folder__c);
            return redirectPage;
        }
    }
}