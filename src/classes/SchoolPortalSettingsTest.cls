@isTest
private class SchoolPortalSettingsTest {
    @isTest
    private static void testFields() {
        system.assertEquals(true, SchoolPortalSettings.Record != null);
        system.assertEquals(true, SchoolPortalSettings.Additional_Questions_Limit != null);
        system.assertEquals(true, SchoolPortalSettings.Budget_Allocation_Report_ID == null);
        system.assertEquals(true, SchoolPortalSettings.Budget_Allocation_Report_Name != null);
        system.assertEquals(true, SchoolPortalSettings.Default_PY_Verification_Threshold != null);
        system.assertEquals(true, SchoolPortalSettings.Email_Alert_Submission_Days != null);
        system.assertEquals(true, SchoolPortalSettings.Fee_Waivers_Table_Page_Size != null);
        system.assertEquals(true, SchoolPortalSettings.Folder_Status_Report_ID == null);
        system.assertEquals(true, SchoolPortalSettings.Folder_Status_Report_Name != null);
        system.assertEquals(true, SchoolPortalSettings.Import_User_Id_18 != null);
        system.assertEquals(true, SchoolPortalSettings.KS_School_Account_Id != null);
        system.assertEquals(true, SchoolPortalSettings.Max_Number_of_Users != null);
        system.assertEquals(true, SchoolPortalSettings.MIE_Link_on_Annual_Settings_page != null);
        system.assertEquals(true, SchoolPortalSettings.MIE_Pilot != null);
        system.assertEquals(true, SchoolPortalSettings.Overlap_End_Date != null);
        system.assertEquals(true, SchoolPortalSettings.Overlap_Start_Date != null);
        system.assertEquals(true, SchoolPortalSettings.Req_Doc_Synchronous_Threshold != null);
        system.assertEquals(true, SchoolPortalSettings.SSS_Org_Wide_Email_Address != null);
        system.assertEquals(true, SchoolPortalSettings.Override_Today == null);
        system.assertEquals(true, SchoolPortalSettings.Verification_Match_Tolerance != null);
        system.assertEquals(true, SchoolPortalSettings.Require_Additional_Documents_Bulk_Limit != null);
        system.assertEquals(true, SchoolPortalSettings.Require_MIE_Bulk_Limit != null);
    }
}