/**
 * @description Query for Payment_Provider__mdt records.
 **/
public class PaymentProviderSelector extends fflib_SObjectSelector {
    @testVisible private static final String PAYMENT_PROVIDER_NAMES = 'paymentProviderNames';
    @testVisible private static final String PAYMENT_PROVIDER_IDS = 'paymentProviderIds';

    /**
     * @description Select Payment_Provider__mdt configuration records by
     *              their developer names.
     * @param paymentProviderNames The Developer names to select the Payment
     *        Provider records by.
     * @return A list of Payment_Provider__mdt records.
     * @throws An ArgumentNullException if the param is null.
     */
    public List<Payment_Provider__mdt> selectByName(Set<String> paymentProviderNames) {
        ArgumentNullException.throwIfNull(paymentProviderNames, PAYMENT_PROVIDER_NAMES);

        assertIsAccessible();

        String query = 'SELECT {0} FROM {1} WHERE DeveloperName IN :paymentProviderNames ORDER BY {2}';
        return Database.query(String.format(query, new List<String> {
                getFieldListString(),
                getSObjectName(),
                getOrderBy()
        }));
    }

    /**
     * @description Select Payment_Provider__mdt configuration records by
     *              their Ids.
     * @param paymentProviderIds The Ids to select Payment Provider records
     *        by.
     * @return A list of Payment_Provider__mdt records.
     * @throws An ArgumentNullException if the param is null.
     */
    public List<Payment_Provider__mdt> selectById(Set<Id> paymentProviderIds) {
        ArgumentNullException.throwIfNull(paymentProviderIds, PAYMENT_PROVIDER_IDS);

        assertIsAccessible();

        String query = 'SELECT {0} FROM {1} WHERE Id IN :paymentProviderIds ORDER BY {2}';
        return Database.query(String.format(query, new List<String> {
                getFieldListString(),
                getSObjectName(),
                getOrderBy()
        }));
    }

    private SObjectType getSObjectType() {
        return Payment_Provider__mdt.getSObjectType();
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                Payment_Provider__mdt.Account_GUID__c,
                Payment_Provider__mdt.Account_ID__c,
                Payment_Provider__mdt.Custom_Debug__c,
                Payment_Provider__mdt.Custom_Debug_2__c,
                Payment_Provider__mdt.DeveloperName,
                Payment_Provider__mdt.Enable_Fraud_Detection__c,
                Payment_Provider__mdt.Enable_Live_Transactions__c,
                Payment_Provider__mdt.MasterLabel,
                Payment_Provider__mdt.Payment_Provider_Class__c
        };
    }

    /**
     * @description The singleton instance of the Payment
     *              Provider Selector.
     */
    public static PaymentProviderSelector Instance {
        get {
            if (Instance == null) {
                Instance = new PaymentProviderSelector();
            }
            return Instance;
        }
        private set;
    }

    private PaymentProviderSelector() {}
}