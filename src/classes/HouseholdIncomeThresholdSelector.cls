/**
 * @description This class is responsible for querying Household Income Threshold records.
 */
public class HouseholdIncomeThresholdSelector extends fflib_SObjectSelector {
    
    @testVisible private static final String ID_SET_PARAM = 'idSet';
    
    public HouseholdIncomeThresholdSelector() {
        super(false, false, false, false);
    }    
    
    /**
     * @description Specifies the default ordering of Household Income Threshold which 
     *              is by Family Size in descending order.
     * @return The Order By criteria.
     */
    public override String getOrderBy() {
        return 'Family_Size__c DESC';
    }
    
    public List<Household_Income_Threshold__c> selectById(Set<Id> idSet) {
        ArgumentNullException.throwIfNull(idSet, ID_SET_PARAM);

        return (List<Household_Income_Threshold__c>)super.selectSObjectsById(idSet);
    }
    
    public List<Household_Income_Threshold__c> selectByAcademicYear(Id academicYearId) {
        ArgumentNullException.throwIfNull(academicYearId, 'academicYearId');

        String condition = 'Academic_Year__c =: academicYearId';

        String formattedQuery = newQueryFactory().setCondition(condition).toSOQL();

        return Database.query(formattedQuery);
    }

    private Schema.SObjectType getSObjectType() {
        return Household_Income_Threshold__c.SObjectType;
    }
    
    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>{
                Household_Income_Threshold__c.Name,
                Household_Income_Threshold__c.Academic_Year__c,
                Household_Income_Threshold__c.Family_Size__c,
                Household_Income_Threshold__c.Income_Threshold__c
        };
    }
    
    /**
     * @description Creates a new instance of the HouseholdIncomeThresholdSelector.
     * @return An instance of HouseholdIncomeThresholdSelector.
     */
    public static HouseholdIncomeThresholdSelector newInstance() {
        return new HouseholdIncomeThresholdSelector();
    }
    
    /**
     * @description Singleton instance property.
     */
    public static HouseholdIncomeThresholdSelector Instance {
        get {
            if (Instance == null) {
                Instance = newInstance();
            }
            return Instance;
        }
        private set;
    }
}