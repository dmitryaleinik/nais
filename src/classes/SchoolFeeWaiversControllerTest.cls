@isTest
private class SchoolFeeWaiversControllerTest {

    // [CH] NAIS-1766 Adding to help avoid Mixed DML Errors
    public static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    private static User schoolPortalUser;
    private static Account school;
    private static Academic_Year__c currentAcademicYear, previousAcademicYear;
    private static Contact parentA, parentB, parentC;

    private static Account setupMeansBasedWaiverSettings(User portalUser) {
        Account meansBasedFeeWaiverAccount = TestUtils.createAccount('MBFW Account', RecordTypes.meansBasedFeeWaiverAccountTypeId, 5, false);
        insert new List<Account> { meansBasedFeeWaiverAccount };

        //Share means based waiver account with school user
        // SFP-672 Create Share between portalUser and the means based waiver account because criteria based
        // sharing rules do not run in unit tests.
        insert new AccountShare(AccountId=meansBasedFeeWaiverAccount.Id, CaseAccessLevel='None',
                OpportunityAccessLevel='None', AccountAccessLevel='Edit', UserOrGroupId=portalUser.Id);

        // Add MBFW settings
        List<Application_Fee_Settings__c> afsList = new List<Application_Fee_Settings__c>();
        Application_Fee_Settings__c afs = new Application_Fee_Settings__c(
                Name = GlobalVariables.getCurrentYearString(),
                Amount__c = 42,
                Discounted_Amount__c = 42,
                Discounted_Product_Code__c = '131-200 PFS Fee - KS - 2015',
                Product_Code__c = '131-100 PFS Fee - 2015',
                Means_Based_Fee_Waiver_Record_Type_Name__c 
                    = ApplicationFeeSettingsService.MEANS_BASED_FEE_WAIVER_RECORDTYPE,
                Maximum_Assets_Waiver__c = 100000);
        afsList.add(afs);
        Application_Fee_Settings__c afs1 = new Application_Fee_Settings__c(
                Name = GlobalVariables.getPreviousYearString(),
                Amount__c = 40,
                Discounted_Amount__c = 40,
                Discounted_Product_Code__c = '131-200 PFS Fee - KS - 2015',
                Product_Code__c = '131-100 PFS Fee - 2015',
                Means_Based_Fee_Waiver_Record_Type_Name__c 
                    = ApplicationFeeSettingsService.MEANS_BASED_FEE_WAIVER_RECORDTYPE,
                Maximum_Assets_Waiver__c = 100000);
        afsList.add(afs1);
        insert afsList;
        return meansBasedFeeWaiverAccount;
    }

    private static User createPortalUser(Contact staff) {
        User portalUser;
        System.runAs(thisUser) {
            portalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(staff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        TestUtils.insertAccountShare(staff.AccountId, portalUser.Id);

        return portalUser;
    }

    private static User setupUnassignedWaivers() {
        SchoolPortalSettings.Fee_Waivers_Table_Page_Size = 10;

        Account family = TestUtils.createAccount('Individual Account', RecordTypes.individualAccountTypeId, 5, false);
        Account school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, 5, false);
        Account school2 = TestUtils.createAccount('school2', RecordTypes.schoolAccountTypeId, 5, false);
        insert new List<Account> { family, school1, school2 };

        Contact staff1 = TestUtils.createContact('Staff 1', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
        staff1.Email = 'staff1@test.org';
        Contact staff2 = TestUtils.createContact('Staff 2', school2.Id, RecordTypes.schoolStaffContactTypeId, false);

        Contact parentA1 = TestUtils.createContact('Parent A', family.Id, RecordTypes.parentContactTypeId, false);
        Contact parentB1 = TestUtils.createContact('Parent B', family.Id, RecordTypes.parentContactTypeId, false);

        Contact student1 = TestUtils.createContact('Student 1', family.Id, RecordTypes.studentContactTypeId, false);
        Contact student2 = TestUtils.createContact('Student 2', family.Id, RecordTypes.studentContactTypeId, false);

        insert new List<Contact> { staff1, staff2, parentA1, parentB1, student1, student2 };

        User portalUser = createPortalUser(staff1);

        Account meansBasedFeeWaiverAccount = setupMeansBasedWaiverSettings(portalUser);

        TestUtils.createAcademicYears();
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        //create annual settings
        Annual_Setting__c schoolSetting = TestUtils.createAnnualSetting(school1.Id, academicYearId, false);
        schoolSetting.Total_Waivers_Override_Default__c = 1;
        insert schoolSetting;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfs1 = TestUtils.createPFS('PFS A', academicYearId, parentA1.Id, false);
        pfs1.Original_Submission_Date__c = (Datetime)System.today().addDays(-3);
        pfs1.PFS_Status__c = 'Application Submitted';
        pfs1.Fee_Waived__c = 'No';
        pfs1.Payment_Status__c = 'Unpaid';

        PFS__c pfs2 = TestUtils.createPFS('PFS B', academicYearId, parentB1.Id, false);
        pfs2.Original_Submission_Date__c = (Datetime)System.today().addDays(-5);
        pfs2.PFS_Status__c = 'Application Submitted';
        pfs2.Payment_Status__c = 'Unpaid';

        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
        insert new List<PFS__c> { pfs1, pfs2 };

        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfs1.Id, false);
        applicant1A.First_Name__c = student1.FirstName;
        applicant1A.Last_Name__c = student1.LastName;
        Applicant__c applicant1B = TestUtils.createApplicant(student1.Id, pfs2.Id, false);
        applicant1B.First_Name__c = student1.FirstName;
        applicant1B.Last_Name__c = student1.LastName;
        Applicant__c applicant2A = TestUtils.createApplicant(student2.Id, pfs1.Id, false);
        applicant2A.First_Name__c = student2.FirstName;
        applicant2A.Last_Name__c = student2.LastName;
        insert new List<Applicant__c> { applicant1A, applicant1B, applicant2A };

        Student_Folder__c studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
        Student_Folder__c studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', academicYearId, student2.Id, false);
        insert new List<Student_Folder__c> { studentFolder1, studentFolder2 };

        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder1.Id, false);
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1B.Id, school1.Id, studentFolder1.Id, false);
        School_PFS_Assignment__c spfsa3 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.Id, school2.Id, studentFolder2.Id, false);
        List<School_PFS_Assignment__c> allSPFSAs = new List<School_PFS_Assignment__c> { spfsa1, spfsa2, spfsa3 };
        insert allSPFSAs;

        List<School_PFS_Assignment__c> spfsaList = [SELECT Id, Applicant__r.PFS__c,
                Applicant__r.PFS__r.Parent_A__r.AccountId, School__c, Student_Folder__c
                FROM School_PFS_Assignment__c
                WHERE Id = :spfsa1.Id OR Id = :spfsa2.Id OR Id = :spfsa3.Id];

        // share records to school staff
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spfsaList , null);
        return portalUser;
    }

    private static void createTestData() {
        Account family = AccountTestData.Instance.asFamily().create();
        school = AccountTestData.Instance.asSchool().create();
        insert new List<Account>{family, school};

        currentAcademicYear = AcademicYearTestData.Instance.create();
        previousAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, previousAcademicYear};

        parentA = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        parentB = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        parentC = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .forAccount(school.Id)
            .asSchoolStaff().create();
        Contact student1 = ContactTestData.Instance.asStudent().create();
        Contact student2 = ContactTestData.Instance.asStudent().create();
        Contact student3 = ContactTestData.Instance.asStudent().create();
        insert new List<Contact>{parentA, parentB, parentC, schoolStaff, student1, student2, student3};

        System.runAs(thisUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);

        String pfsStatusUnpaid = 'Unpaid';
        String isPfsFeeWaived = 'Yes';
        PFS__c pfs1, pfs2, pfs1Past, pfs2Past;
        System.runAs(schoolPortalUser) {
            pfs1 = PfsTestData.Instance
                .asSubmitted()
                .forPaymentStatus(pfsStatusUnpaid)
                .forParentA(parentA.Id)
                .forOriginalSubmissionDate(System.today().addDays(-3))
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            pfs2 = PfsTestData.Instance
                .asSubmitted()
                .forPaymentStatus(pfsStatusUnpaid)
                .forParentA(parentB.Id)
                .forOriginalSubmissionDate(System.today().addDays(-5))
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            pfs1Past = PfsTestData.Instance
                .asSubmitted()
                .forParentA(parentA.Id)
                .forOriginalSubmissionDate(Date.newInstance(System.today().addYears(-5).year(), 12, 12))
                .forAcademicYearPicklist(previousAcademicYear.Name).create();
            pfs2Past = PfsTestData.Instance
                .asSubmitted()
                .forParentA(parentC.Id)
                .forOriginalSubmissionDate(Date.newInstance(System.today().addYears(-5).year(), 12, 12))
                .forFeeWaived(isPfsFeeWaived)
                .forAcademicYearPicklist(previousAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<PFS__c> {pfs1, pfs2, pfs1Past, pfs2Past});

            Applicant__c applicant1A = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student1.Id).create();
            Applicant__c applicant1B = ApplicantTestData.Instance
                .forPfsId(pfs2.Id)
                .forContactId(student1.Id).create();
            Applicant__c applicant2A = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student2.Id).create();
            Applicant__c applicant1Past = ApplicantTestData.Instance
                .forPfsId(pfs1Past.Id)
                .forContactId(student1.Id).create();
            Applicant__c applicant2Past  = ApplicantTestData.Instance
                .forPfsId(pfs2Past.Id)
                .forContactId(student3.Id).create();
            Dml.WithoutSharing.insertObjects(
                new List<Applicant__c> {applicant1A, applicant1B, applicant2A, applicant1Past, applicant2Past});

            Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
                .forStudentId(student1.Id)
                .forSchoolId(school.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Student_Folder__c studentFolder2 = StudentFolderTestData.Instance
                .forStudentId(student2.Id)
                .forSchoolId(school.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Student_Folder__c studentFolder1Past = StudentFolderTestData.Instance
                .forStudentId(student1.Id)
                .forSchoolId(school.Id)
                .forAcademicYearPicklist(previousAcademicYear.Name).create();
            Student_Folder__c studentFolder2Past = StudentFolderTestData.Instance
                .forStudentId(student3.Id)
                .forSchoolId(school.Id)
                .forAcademicYearPicklist(previousAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(
                new List<Student_Folder__c> {studentFolder1, studentFolder2, studentFolder1Past, studentFolder2Past});

            String fifthGrade = '5';
            School_PFS_Assignment__c spfsa1A = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1A.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            School_PFS_Assignment__c spfsa1B = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1B.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            School_PFS_Assignment__c spfsa2A = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant2A.Id)
                .forStudentFolderId(studentFolder2.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            School_PFS_Assignment__c spfsa1APast = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1Past.Id)
                .forStudentFolderId(studentFolder1Past.Id)
                .forAcademicYearPicklist(previousAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> {spfsa1A, spfsa1B, spfsa2A, spfsa1APast});
        }

        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);
    }

    @isTest
    private static void testFeeWaiversInit() {
        createTestData();

        Test.startTest();
            System.runAs(schoolPortalUser) {
                Test.setCurrentPage(Page.SchoolFeeWaivers);

                Application_Fee_Waiver__c afw1 = new Application_Fee_Waiver__c();
                afw1.Academic_Year__c = currentAcademicYear.Id;
                afw1.Account__c = school.Id;
                afw1.Contact__c = parentA.Id;
                afw1.Status__c = 'Pending Approval';

                Application_Fee_Waiver__c afw2 = new Application_Fee_Waiver__c();
                afw2.Academic_Year__c = currentAcademicYear.Id;
                afw2.Account__c = school.Id;
                afw2.Contact__c = parentB.Id;
                afw2.Status__c = 'Assigned';
                Dml.WithoutSharing.insertObjects(new List<Application_Fee_Waiver__c>{afw1,afw2});

                List<PFS__c> priorYearPFS = [
                    SELECT Id
                    FROM PFS__c
                    WHERE Academic_Year_Picklist__c = :previousAcademicYear.Name   // Previous Year
                        AND Fee_Waived__c = 'Yes'                                  // Waiver Assigned
                      ORDER BY Parent_A__r.LastName];

                List<PFS__c> requestingPFS = [
                    SELECT Id
                    FROM PFS__c
                    WHERE Academic_Year_Picklist__c = :currentAcademicYear.Name
                        AND Payment_Status__c != 'Paid in Full'
                        AND Parent_A__c IN (
                            SELECT Contact__c                        // In AFW Requesting list.
                            FROM Application_Fee_Waiver__c
                            WHERE Academic_Year__c = :currentAcademicYear.Id
                                AND Account__c = :school.Id
                                AND Status__c = 'Pending Approval')
                      ORDER BY Parent_A__r.LastName];                // Sort by Parents Last Name.

                System.assertEquals(1,priorYearPFS.size());
                System.assertEquals(1,requestingPFS.size());
            }
        Test.stopTest();
    }

    @isTest
    private static void testFeeWaiversAssignedInit() {
        createTestData();
        
        List<PFS__c> pfss = PfsSelector.newInstance().selectByContactIdAndAcademicYearId(parentB.Id, currentAcademicYear.Id);
        System.assertEquals(1, pfss.size());
        PFS__c pfs2 = pfss[0];

        Account meansBasedFeeWaiverAccount = setupMeansBasedWaiverSettings(schoolPortalUser);
        
        Test.startTest();
            System.runAs(schoolPortalUser) {
                Test.setCurrentPage(Page.SchoolFeeWaiversAssigned);
                ApexPages.currentPage().getParameters().put('academicyearid', GlobalVariables.getCurrentAcademicYear().Id);

                // Setup some AFWs. 1 Assigned and 1 Pending.
                Application_Fee_Waiver__c afw1 = new Application_Fee_Waiver__c();
                afw1.Academic_Year__c = currentAcademicYear.Id;
                afw1.Account__c = school.Id;
                afw1.Contact__c = parentA.Id;
                afw1.Status__c = 'Pending Approval';
                Application_Fee_Waiver__c afw2 = new Application_Fee_Waiver__c();
                afw2.Academic_Year__c = currentAcademicYear.Id;
                afw2.Account__c = school.Id;
                afw2.Contact__c = parentB.Id;
                afw2.Status__c = 'Assigned';
                Application_Fee_Waiver__c afw3 = new Application_Fee_Waiver__c();
                afw3.Academic_Year__c = currentAcademicYear.Id;
                afw3.Account__c = meansBasedFeeWaiverAccount.Id;
                afw3.Contact__c = parentB.Id;
                afw3.Status__c = 'Assigned';
                insert new List<Application_Fee_Waiver__c> {afw1, afw2, afw3};

                update pfs2;

                SchoolFeeWaiversController testCtrlr = new SchoolFeeWaiversController();

                testCtrlr.initFeeWaiversAssignedTabPage();

                // Should only pick up the Assigned one.
                System.assertEquals(1, testCtrlr.assignedList.size());

            }
        Test.stopTest();
    }

    @isTest
    private static void testFeeWaiversAssignedInitPastYear() {
        createTestData();

        List<PFS__c> pfss = PfsSelector.newInstance().selectByContactIdAndAcademicYearId(parentB.Id, currentAcademicYear.Id);
        System.assertEquals(1, pfss.size());
        PFS__c pfs2 = pfss[0];

        Account meansBasedFeeWaiverAccount = setupMeansBasedWaiverSettings(schoolPortalUser);

        System.runAs(schoolPortalUser) {
            Test.startTest();
                Test.setCurrentPage(Page.SchoolFeeWaiversAssigned);
                ApexPages.currentPage().getParameters().put('academicyearid', currentAcademicYear.Id);

                // Setup some assigned AFWs. 1 with a current year parent and 1 with a past year parent.
                Application_Fee_Waiver__c afw1 = new Application_Fee_Waiver__c();
                afw1.Academic_Year__c = currentAcademicYear.Id;
                afw1.Account__c = school.Id;
                afw1.Contact__c = parentA.Id;
                afw1.Status__c = 'Assigned';
                Application_Fee_Waiver__c afw2 = new Application_Fee_Waiver__c();
                afw2.Academic_Year__c = currentAcademicYear.Id;
                afw2.Account__c = school.Id;
                afw2.Contact__c = parentC.Id;
                afw2.Status__c = 'Assigned';
                insert new List<Application_Fee_Waiver__c> {afw1, afw2};

                // Have to set Fee_Waived__c. That field is lame.
                pfs2.Fee_Waived__c = 'Yes';
                update pfs2;

                SchoolFeeWaiversController testCtrlr = new SchoolFeeWaiversController();
                testCtrlr.initFeeWaiversAssignedTabPage();
                testCtrlr.doInitFeeWaiverConfirm();
                testCtrlr.purchaseAdditionalWaivers();

                // Should pick up the both current and past parents.
                System.assertEquals(1, testCtrlr.assignedList.size());

                testCtrlr.loadAnnualSettings(currentAcademicYear.Id);
                testCtrlr.SchoolAcademicYearSelector_OnChange(false);
            Test.stopTest();
        }
    }

    @isTest
    private static void testFeeWaiversDeclinedInit() {
        createTestData();

        Account meansBasedFeeWaiverAccount = setupMeansBasedWaiverSettings(schoolPortalUser);

        System.runAs(schoolPortalUser) {
            Test.startTest();
                Application_Fee_Waiver__c afw1 = new Application_Fee_Waiver__c();
                afw1.Academic_Year__c = currentAcademicYear.Id;
                afw1.Account__c = school.Id;
                afw1.Contact__c = parentA.Id;
                afw1.Status__c = 'Pending Approval';
                insert afw1; // To get Id.

                Test.setCurrentPage(Page.SchoolFeeWaiverDecline);
                ApexPages.currentPage().getParameters().put('retURL', 'return_url');
                ApexPages.currentPage().getParameters().put('id', afw1.Id);

                SchoolFeeWaiverDeclineController testCtrlr = new SchoolFeeWaiverDeclineController();
                testCtrlr.afw.Decline_Message_to_Parent__c = '';

                PageReference pRef = testCtrlr.declinewithmsg();

                // missing decline message
                System.assertEquals(null, pRef);
                List<Application_Fee_Waiver__c> afws = [select Status__c from Application_Fee_Waiver__c where Id = :afw1.Id];
                System.assertEquals('Pending Approval', afws[0].Status__c);

                testCtrlr.afw.Decline_Message_to_Parent__c = 'So sorry!';
                pRef = testCtrlr.declinewithmsg();

                System.assertNotEquals(null, pRef);
                afws = [select Status__c from Application_Fee_Waiver__c where Id = :afw1.Id];
            Test.stopTest();
            
            System.assertEquals('Declined', afws[0].Status__c);
            System.assertNotEquals(null, testCtrlr.cancel());
        }
    }

    @isTest
    private static void initFeeWaiversAssignedTabPage_expectIsAssignedWaiverPageToBeTrue() {
        school = AccountTestData.Instance.asSchool().insertAccount();
        currentAcademicYear = AcademicYearTestData.Instance.create();
        previousAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, previousAcademicYear};
        Contact schoolStaff = ContactTestData.Instance
            .forAccount(school.Id)
            .asSchoolStaff().insertContact();

        System.runAs(thisUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        setupMeansBasedWaiverSettings(schoolPortalUser);
        System.runAs(schoolPortalUser) {
            SchoolFeeWaiversController controller = new SchoolFeeWaiversController();
            controller.initFeeWaiversAssignedTabPage();

            System.assert(controller.isAssignedWaiverPage == true,
                    'Expected initFeeWaiversAssignedTabPage() to set isAssignedWaiverPage to true.');
        }
    }

    @isTest
    private static void paging_unassignedWaivers_expectUpdatedSelectedList() {
        User portalUser = setupUnassignedWaivers();

        System.runAs(portalUser) {
            SchoolFeeWaiversController testCtrlr = new SchoolFeeWaiversController();

            testCtrlr.initFeeWaiversPage();
            System.assertEquals(2, testCtrlr.assignedList.size());
            testCtrlr.assignedList[0].isChecked = true;

            testCtrlr.next();
            System.assertEquals(1, testCtrlr.selectedApplicantsByPfsId.keySet().size(),
                    'Expected one selection in map of selected waivers');
            testCtrlr.previous();
            System.assertEquals(1, testCtrlr.selectedApplicantsByPfsId.keySet().size(),
                    'Expected one selection in map of selected waivers');
        }
    }

    @isTest
    private static void pfsLink_withSpa_expectLinkToSPA() {
        Account family = AccountTestData.Instance.asFamily().create();
        school = AccountTestData.Instance.asSchool().create();
        insert new List<Account>{family, school};

        currentAcademicYear = AcademicYearTestData.Instance.create();
        previousAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, previousAcademicYear};

        parentA = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .forAccount(school.Id)
            .asSchoolStaff().create();
        Contact student1 = ContactTestData.Instance.asStudent().create();
        insert new List<Contact>{parentA, schoolStaff, student1};

        System.runAs(thisUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);

        String pfsStatusUnpaid = 'Unpaid';
        School_PFS_Assignment__c spfsa1A;
        System.runAs(schoolPortalUser) {
            PFS__c pfs1 = PfsTestData.Instance
                .asSubmitted()
                .forPaymentStatus(pfsStatusUnpaid)
                .forParentA(parentA.Id)
                .forOriginalSubmissionDate(System.today().addDays(-3))
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<PFS__c> {pfs1});

            Applicant__c applicant1A = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student1.Id).create();
            Dml.WithoutSharing.insertObjects(
                new List<Applicant__c> {applicant1A});

            Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
                .forStudentId(student1.Id)
                .forSchoolId(school.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(
                new List<Student_Folder__c> {studentFolder1});

            String fifthGrade = '5';
            spfsa1A = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1A.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> {spfsa1A});
        }

        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);

        setupMeansBasedWaiverSettings(schoolPortalUser);

        System.runAs(schoolPortalUser) {
            SchoolFeeWaiversController controller = new SchoolFeeWaiversController();
            controller.initFeeWaiversPage();
            String pfsNumber = [SELECT Id, PFS_Number__c FROM School_PFS_Assignment__c WHERE Id = :spfsa1A.Id].PFS_Number__c;
            controller.selectedPFSNumber = pfsNumber;

            PageReference summary = controller.pfslink();
            System.assert(summary.getUrl().containsIgnoreCase('SchoolPFSSummary'), 'Expected page to be SchoolPFSSummary');
            System.assertNotEquals(null, summary, 'Expected Page Reference to be returned.');
        }
    }

    @isTest
    private static void assignWaivers_expectWaiverToBeAssigned() {
        User portalUser = setupUnassignedWaivers();

        Test.startTest();
            System.runAs(portalUser) {

                Test.setCurrentPage(Page.SchoolFeeWaivers);
                SchoolFeeWaiversController testCtrlr = new SchoolFeeWaiversController();
                testCtrlr.initFeeWaiversPage();

                System.assertEquals(2, testCtrlr.assignedList.size());
                testCtrlr.assignedList[0].isChecked = true;

                testCtrlr.assignWaivers();

                List<Application_Fee_Waiver__c> waivers = [SELECT Id FROM Application_Fee_Waiver__c];

                System.assertEquals(1, waivers.size(), 'Expected a fee waiver to be inserted.');
            }
        Test.stopTest();
    }

    @isTest
    private static void assignWaivers_moreThanBalance_expectNoWaiversAssigned() {
        User portalUser = setupUnassignedWaivers();

        System.runAs(portalUser) {
            Test.startTest();
                Test.setCurrentPage(Page.SchoolFeeWaivers);
                SchoolFeeWaiversController testCtrlr = new SchoolFeeWaiversController();
                testCtrlr.initFeeWaiversPage();

                System.assertEquals(2, testCtrlr.assignedList.size());
                testCtrlr.assignedList[0].isChecked = true;
                testCtrlr.assignedList[1].isChecked = true;

                testCtrlr.assignWaivers();

                List<Application_Fee_Waiver__c> waivers = [SELECT Id FROM Application_Fee_Waiver__c];

            System.assertEquals(0, waivers.size(),
                    'Expected no waivers to be assigned when assigning more than the fee waiver balance.');
        }
    }

    @isTest
    private static void getNumOfNewWaivers_selectOne_assignWaivers_expectOneIsReturned() {
        User portalUser = setupUnassignedWaivers();

        System.runAs(portalUser) {
            Test.startTest();
                Test.setCurrentPage(Page.SchoolFeeWaivers);
                SchoolFeeWaiversController testCtrlr = new SchoolFeeWaiversController();
                testCtrlr.initFeeWaiversPage();

                System.assertEquals(2, testCtrlr.assignedList.size());
                testCtrlr.assignedList[0].isChecked = true;

                // Simulate event where user decides to assign waivers which should update the selected waivers.
                testCtrlr.doInitFeeWaiverConfirm();
            
            System.assertEquals(1, testCtrlr.getNumOfNewWaivers(), 'Expected 1 when only 1 new waiver was selected.');
        }
    }

    @isTest
    private static void testSssFeeWaiverGuidelinesId() {
        SchoolFeeWaiversController controller = new SchoolFeeWaiversController();
        Id sssFeeWaiverGuidelinesId = controller.sssFeeWaiverGuidelinesId;

        System.assert(String.isBlank(controller.sssFeeWaiverGuidelinesId));

        Test.startTest();
            Document doc = DocumentTestData.Instance
                .forDocumentDeveloperName(DocumentLinkMappingService.documentInfo.Document_Developer_Name__c).insertDocument();

            controller = new SchoolFeeWaiversController();
            sssFeeWaiverGuidelinesId = controller.sssFeeWaiverGuidelinesId;

            System.assertEquals(DocumentLinkMappingService.documentId, sssFeeWaiverGuidelinesId);
        Test.stopTest();
    }

    /***Test generateReceipt***/
    @isTest   
    private static void testGenerateReceipt() {
        User portalUser = UserTestData.insertSchoolPortalUser();
        Opportunity opportunity = OpportunityTestData.Instance
            .asFeeWaiverPurchasing().DefaultOpportunity;
        PageReference schoolFeeWaiversPage = Page.SchoolFeeWaivers;
        Test.setCurrentPage(schoolFeeWaiversPage);
        SchoolFeeWaiversController controller = new SchoolFeeWaiversController();
        PageReference generateReceiptReference;
        
        Test.StartTest();
            //Run as the portal user who created the Opp
            system.runAs(portalUser) {
                
                system.assertEquals(null, controller.showThanksForFWPurchasingMessage);
                
                Apexpages.currentPage().getParameters().put('Id', opportunity.Id);
                controller = new SchoolFeeWaiversController();
                generateReceiptReference = controller.generateReceipt();
            }
        Test.stopTest();

        system.assertEquals(true, controller.showThanksForFWPurchasingMessage);
        system.assertEquals(true, String.valueOf(generateReceiptReference).contains(String.valueOf(opportunity.Id)));
    }
}