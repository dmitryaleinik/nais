@isTest
private class StudentFolderSelectorTest
{
    
    @isTest
    private static void selectWithCustomFieldListById_expectRecordReturned() {
        Contact student = ContactTestData.Instance.forRecordTypeId(RecordTypes.studentContactTypeId).DefaultContact;
        Student_Folder__c studentFolder = StudentFolderTestData.Instance
            .forStudentId(student.Id).DefaultStudentFolder;

        Test.startTest();
            List<Student_Folder__c> studentFolderRecords = StudentFolderSelector.newInstance()
                .selectByIdWithCustomFieldList(new Set<Id>{studentFolder.Id}, new List<String>{'Name'});
        Test.stopTest();

        System.assert(!studentFolderRecords.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(1, studentFolderRecords.size(), 'Expected there to be one record returned.');
        System.assertEquals(studentFolder.Id, studentFolderRecords[0].Id, 'Expected the Ids of the records to match.');
    }

    @isTest
    private static void selectWithCustomFieldListById_expectArgumentNullException() {
        Contact student = ContactTestData.Instance.forRecordTypeId(RecordTypes.studentContactTypeId).DefaultContact;
        Student_Folder__c studentFolder = StudentFolderTestData.Instance
            .forStudentId(student.Id).DefaultStudentFolder;

        Test.startTest();
            try {
                StudentFolderSelector.newInstance().selectByIdWithCustomFieldList(new Set<Id>{studentFolder.Id}, null);

                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, StudentFolderSelector.STUDENT_FOLDER_FIELDS_TO_SELECT_PARAM);
            }

            try {
                StudentFolderSelector.newInstance().selectByIdWithCustomFieldList(null, new List<String>{'Name'});

                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, StudentFolderSelector.STUDENT_FOLDER_IDS_PARAM);
            }
        Test.stopTest();
    }

    @isTest
    private static void selectAll_expectRecordReturned() {
        Contact student1 = ContactTestData.Instance.asStudent().create();
        Contact student2 = ContactTestData.Instance.asStudent().create();
        insert new List<Contact>{student1, student2};

        Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
            .forStudentId(student1.Id).create();
        Student_Folder__c studentFolder2 = StudentFolderTestData.Instance
            .forStudentId(student2.Id).create();
        insert new List<Student_Folder__c>{studentFolder1, studentFolder2};

        Test.startTest();
            List<Student_Folder__c> studentFolderRecords = StudentFolderSelector.newInstance().selectAll();
        Test.stopTest();

        System.assert(!studentFolderRecords.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(2, studentFolderRecords.size(), 'Expected there to be 2 records returned.');
    }

    @isTest
    private static void selectByStudentId_schoolId_academicYearId_expectRecordReturned() {
        Contact student1 = ContactTestData.Instance.asStudent().create();
        Contact student2 = ContactTestData.Instance.asStudent().create();
        Contact student3 = ContactTestData.Instance.asStudent().create();
        insert new List<Contact>{student1, student2, student3};

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
        Academic_Year__c previousAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, previousAcademicYear};

        Account school = AccountTestData.Instance.asSchool().DefaultAccount;

        Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forStudentId(student1.Id).create();
        Student_Folder__c studentFolder2 = StudentFolderTestData.Instance
            .forSchoolId(school.Id)
            .forStudentId(student2.Id).create();
        Student_Folder__c studentFolder3 = StudentFolderTestData.Instance
            .forAcademicYearPicklist(previousAcademicYear.Name)
            .forStudentId(student3.Id).create();
        insert new List<Student_Folder__c>{studentFolder1, studentFolder2, studentFolder3};

        Test.startTest();
            List<Student_Folder__c> studentFolderRecords = 
                StudentFolderSelector.newInstance().selectByStudentId_schoolId_academicYearName(
                    new Set<Id>{student1.Id, student2.Id, student3.Id}, null, null);
            System.assertEquals(3, studentFolderRecords.size());
            
            studentFolderRecords = StudentFolderSelector.newInstance().selectByStudentId_schoolId_academicYearName(
                new Set<Id>{student1.Id, student2.Id, student3.Id}, new Set<Id>{school.Id}, null);
            System.assertEquals(2, studentFolderRecords.size());

            studentFolderRecords = StudentFolderSelector.newInstance().selectByStudentId_schoolId_academicYearName(
                new Set<Id>{student1.Id, student2.Id, student3.Id}, new Set<Id>{school.Id}, new Set<String>{currentAcademicYear.Name});
            System.assertEquals(1, studentFolderRecords.size());

            studentFolderRecords = StudentFolderSelector.newInstance().selectByStudentId_schoolId_academicYearName(
                new Set<Id>{student1.Id, student2.Id, student3.Id}, null, new Set<String>{currentAcademicYear.Name});
            System.assertEquals(1, studentFolderRecords.size());
        Test.stopTest();
    }

    @isTest
    private static void selectByStudentId_schoolId_academicYearId_expectException() {
        Contact student1 = ContactTestData.Instance.asStudent().insertContact();
        Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
            .forStudentId(student1.Id).insertStudentFolder();

        Test.startTest();
            try {
                StudentFolderSelector.newInstance().selectByStudentId_schoolId_academicYearName(null, null, null);

                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, StudentFolderSelector.STUDENT_IDS);
            }
        Test.stopTest();
    }
    
    //SFP-1243
    @isTest
    private static void selectByStudentSchoolAndYearWithBudgetAllocations_studentWithPreviousBudgetAllocations_folderWithBudgetAllocationsForPreiousAcademicYear() {
        
        //1. Create current and previous academic year.
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
        Academic_Year__c previousAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, previousAcademicYear};
        
        //2. Create 2 students.
        Contact student1 = ContactTestData.Instance.asStudent().create();
        Contact student2 = ContactTestData.Instance.asStudent().create();
        insert new List<Contact>{student1, student2};
        
        //3. Create one School.
        Account school = AccountTestData.Instance.asSchool().DefaultAccount;
        
        //4. Create 2 Budget Groups for the School for Previous academic year.
        //   And one budget group for current academic year.
        Budget_Group__c budgetPreviousAcademicYear = BudgetGroupTestData.Instance
            .forAcademicYearId(previousAcademicYear.Id)
            .forSchoolId(school.Id)
            .forTotalFunds(50000).create();
            
        Budget_Group__c budgetPreviousAcademicYear1 = BudgetGroupTestData.Instance
            .forAcademicYearId(previousAcademicYear.Id)
            .forSchoolId(school.Id)
            .forTotalFunds(50000).create();
        budgetPreviousAcademicYear1.Name = 'Test Group 1';
        Budget_Group__c budgetPreviousAcademicYear2 = BudgetGroupTestData.Instance
            .forAcademicYearId(previousAcademicYear.Id)
            .forSchoolId(school.Id)
            .forTotalFunds(10000).create();
        budgetPreviousAcademicYear2.Name = 'Test Group 2';
        Budget_Group__c budgetCurrentAcademicYear1 = BudgetGroupTestData.Instance
            .forAcademicYearId(currentAcademicYear.Id)
            .forSchoolId(school.Id)
            .forTotalFunds(60000).create();  
        budgetCurrentAcademicYear1.Name = 'Test Group 3'; 
        insert new List<Budget_Group__c>{budgetPreviousAcademicYear1, budgetPreviousAcademicYear2, budgetCurrentAcademicYear1};
        
        //5. Create 2 folders for each Student, one for previous Academic Year.
        //   And another current Academic Year.
        Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(previousAcademicYear.Name)
            .forStudentId(student1.Id).create();
        Student_Folder__c studentFolder2 = StudentFolderTestData.Instance
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forStudentId(student1.Id).create();
        Student_Folder__c studentFolder3 = StudentFolderTestData.Instance
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(previousAcademicYear.Name)
            .forStudentId(student2.Id).create();
        Student_Folder__c studentFolder4 = StudentFolderTestData.Instance
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forStudentId(student2.Id).create();
        insert new List<Student_Folder__c>{studentFolder1, studentFolder2, studentFolder3, studentFolder4};
        
        //6. Award StudentFolders for Student1:
        //   Two allocations for previous Academic Year.
        //   One allocation for current Academic Year.
        Budget_Allocation__c allocationPreviousYear1 = BudgetAllocationTestData.Instance
            .forBudgetGroupId(budgetPreviousAcademicYear1.Id)
            .forAmountAllocated(7000)
            .forStudentFolder(studentFolder1.Id).create();
        Budget_Allocation__c allocationPreviousYear2 = BudgetAllocationTestData.Instance
            .forBudgetGroupId(budgetPreviousAcademicYear2.Id)
            .forAmountAllocated(3000)
            .forStudentFolder(studentFolder1.Id).create();
        Budget_Allocation__c allocationPreviousYear3 = BudgetAllocationTestData.Instance
            .forBudgetGroupId(budgetCurrentAcademicYear1.Id)
            .forAmountAllocated(2000)
            .forStudentFolder(studentFolder2.Id).create();
        insert new List<Budget_Allocation__c>{allocationPreviousYear1, allocationPreviousYear2, allocationPreviousYear3};
        
        Test.startTest();
            List<Student_Folder__c> studentFolderRecords1 = 
                StudentFolderSelector.newInstance().selectByStudentSchoolAndYearWithBudgetAllocations(
                    student1.Id, school.Id, previousAcademicYear.Name);
            System.assertEquals(1, studentFolderRecords1.size());
            System.assertEquals(2, studentFolderRecords1[0].Budget_Allocations__r.size());
            
            List<Student_Folder__c> studentFolderRecords2 = 
                StudentFolderSelector.newInstance().selectByStudentSchoolAndYearWithBudgetAllocations(
                    new Set<Id>{student1.Id, student2.Id}, new Set<Id>{school.Id}, new Set<String>{currentAcademicYear.Name});
            System.assertEquals(2, studentFolderRecords2.size());
            System.assertEquals(1, studentFolderRecords2[0].Budget_Allocations__r.size());
            System.assertEquals(0, studentFolderRecords2[1].Budget_Allocations__r.size());
        Test.stopTest();
    }
}