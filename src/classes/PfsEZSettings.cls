public class PfsEZSettings {
    
    public Map<String, EZHiddenField> hiddenFieldsByScreen { get; set; }
    public string hiddenFieldsByScreenMapKey{get;set;}
    
    public PfsEZSettings(String SubmissionProcess) {
        
        retrieveHiddenFields(SubmissionProcess);
    }
    
    public void retrieveHiddenFields(String SubmissionProcess) {
        hiddenFieldsByScreen = new Map<String, EZHiddenField>();
        List<PFS_EZ_Hidden_Fields__c> hiddenFieldsForEZ = PFS_EZ_Hidden_Fields__c.getall().values();
        EZHiddenField tmp;
        
        if( hiddenFieldsForEZ != null ) {
            for(PFS_EZ_Hidden_Fields__c field : hiddenFieldsForEZ ) {
                tmp = hiddenFieldsByScreen.containsKey(field.Name)
                    ? hiddenFieldsByScreen.get(field.Name)
                    : new EZHiddenField();
                tmp.add(field.Field_Name__c);
                hiddenFieldsByScreen.put( field.Name, tmp );
            }
        }
        
        hiddenFieldsByScreenMapKey =  string.valueof(hiddenFieldsByScreen.keyset()).replace('{', '').replace('{', '');
    }//End:retrieveHiddenFields
    
    public class EZHiddenField { 
        public Boolean allInSection { get; set; }
        public Set<String> hiddenFields { get; set; }
        
        public EZHiddenField() {
            allInSection = false;
            hiddenFields = new Set<String>();
        }
        
        /**
        * @description Creates a set of fields hidden for a given section, determined by current "screen". 
        *              If there's no fields defined and the screen has fieldName='all', it  means that the 
        *              entired section is hidden. 
        * @param fieldName 'All' if the entired section should be hidden. Otherwise, the name of the 
        *        field to be hide.
        */
        public void add(String fieldName) {
            
            if( fieldName != 'All' ) {
                
                if( !hiddenFields.contains(fieldName) ) {
                    hiddenFields.add(fieldName);
                }
            } else {
                allInSection = !allInSection && hiddenFields.isEmpty() ? true : allInSection;
            }
        }//End:add
        
    }//End-Class:EZHiddenField
    
}