/**
 * @description This class is used to create Profile Type Grouping records for unit tests.
 */
@isTest
public class ProfileTypeGroupingTestData extends SObjectTestData
{

    @testVisible private static final String PTG_NAME = ProfileSettings.SchoolAdminProfileName;
    @testVisible private static final String PTG_NAME_DATABANK_USER = 'Databank Data Entry User';
    @testVisible private static final String PTG_NAME_SCHOOL_PORTAL_ADMIN = ProfileSettings.SchoolAdminProfileName;
    @testVisible private static final String PTG_NAME_SCHOOL_PORTAL_USER = ProfileSettings.SchoolUserProfileName;

    /**
     * @description Get the default values for the Profile_Type_Grouping__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Profile_Type_Grouping__c.Name => PTG_NAME
        };
    }

    /**
     * @description Set all necessary fileds of the current Profile_Type_Grouping__c record to create 
     *              relevant DatabankUser record
     * @return The current instance of ProfileTypeGroupingTestData.
     */
    public ProfileTypeGroupingTestData asDatabankUser() 
    {
        return (ProfileTypeGroupingTestData) with(Profile_Type_Grouping__c.Name, PTG_NAME_DATABANK_USER)
                                            .with(Profile_Type_Grouping__c.Profile_Name__c, PTG_NAME_DATABANK_USER)
                                            .with(Profile_Type_Grouping__c.Is_Databank__c, true);
    }

    /**
     * @description Set all necessary fileds of the current Profile_Type_Grouping__c record to create 
     *              relevant School Portal Administrator record
     * @return The current instance of ProfileTypeGroupingTestData.
     */
    public ProfileTypeGroupingTestData asSchoolPortalAdministrator()
    {
        return (ProfileTypeGroupingTestData) with(Profile_Type_Grouping__c.Name, PTG_NAME_SCHOOL_PORTAL_ADMIN)
                                            .with(Profile_Type_Grouping__c.Profile_Name__c, ProfileSettings.SchoolAdminProfileName)
                                            .with(Profile_Type_Grouping__c.Is_School_Profile__c , true);
    }

    /**
     * @description Set all necessary fileds of the current Profile_Type_Grouping__c record to create
     *              relevant School Portal User record
     * @return The current instance of ProfileTypeGroupingTestData.
     */
    public ProfileTypeGroupingTestData asSchoolPortalUser()
    {
        return (ProfileTypeGroupingTestData) with(Profile_Type_Grouping__c.Name, PTG_NAME_SCHOOL_PORTAL_USER)
                                            .with(Profile_Type_Grouping__c.Profile_Name__c, ProfileSettings.SchoolUserProfileName)
                                            .with(Profile_Type_Grouping__c.Is_School_Profile__c , true);
    }

    /**
     * @description Set the Name field on the current Profile Type Grouping record.
     * @param name The Name to set on the Profile Type Grouping.
     * @return The current working instance of ProfileTypeGroupingTestData.
     */
    public ProfileTypeGroupingTestData forName(String name) {
        return (ProfileTypeGroupingTestData) with(Profile_Type_Grouping__c.Name, name);
    }

    /**
     * @description Set the Is_School_Profile__c field on the current Profile Type Grouping record.
     * @param isSchoolProfile The Is School Profile to set on the Profile Type Grouping.
     * @return The current working instance of ProfileTypeGroupingTestData.
     */
    public ProfileTypeGroupingTestData forIsSchoolProfile(Boolean isSchoolProfile) {
        return (ProfileTypeGroupingTestData) with(Profile_Type_Grouping__c.Is_School_Profile__c, isSchoolProfile);
    }

    /**
     * @description Set the Is_Databank__c field on the current Profile Type Grouping record.
     * @param isDatabank The Is Databank to set on the Profile Type Grouping.
     * @return The current working instance of ProfileTypeGroupingTestData.
     */
    public ProfileTypeGroupingTestData forIsDatabank(Boolean isDatabank) {
        return (ProfileTypeGroupingTestData) with(Profile_Type_Grouping__c.Is_Databank__c, isDatabank);
    }

    /**
     * @description Set the Profile_Name__c field on the current Profile Type Grouping record.
     * @param profileName The Profile Name to set on the Profile Type Grouping.
     * @return The current working instance of ProfileTypeGroupingTestData.
     */
    public ProfileTypeGroupingTestData forProfileName(String profileName) {
        return (ProfileTypeGroupingTestData) with(Profile_Type_Grouping__c.Profile_Name__c, profileName);
    }

    /**
     * @description Insert the current working Profile_Type_Grouping__c record.
     * @return The currently operated upon Profile_Type_Grouping__c record.
     */
    public Profile_Type_Grouping__c insertProfileTypeGrouping() {
        return (Profile_Type_Grouping__c)insertRecord();
    }

    /**
     * @description Create the current working Profile Type Grouping record without resetting
     *             the stored values in this instance of ProfileTypeGroupingTestData.
     * @return A non-inserted Profile_Type_Grouping__c record using the currently stored field
     *             values.
     */
    public Profile_Type_Grouping__c createProfileTypeGroupingWithoutReset() {
        return (Profile_Type_Grouping__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Profile_Type_Grouping__c record.
     * @return The currently operated upon Profile_Type_Grouping__c record.
     */
    public Profile_Type_Grouping__c create() {
        return (Profile_Type_Grouping__c)super.buildWithReset();
    }

    /**
     * @description The default Profile_Type_Grouping__c record.
     */
    public Profile_Type_Grouping__c DefaultProfileTypeGrouping {
        get {
            if (DefaultProfileTypeGrouping == null) {
                DefaultProfileTypeGrouping = createProfileTypeGroupingWithoutReset();
                insert DefaultProfileTypeGrouping;
            }
            return DefaultProfileTypeGrouping;
        }
        private set;
    }

    /**
     * @description Get the Profile_Type_Grouping__c SObjectType.
     * @return The Profile_Type_Grouping__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Profile_Type_Grouping__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static ProfileTypeGroupingTestData Instance {
        get {
            if (Instance == null) {
                Instance = new ProfileTypeGroupingTestData();
            }
            return Instance;
        }
        private set;
    }

    private ProfileTypeGroupingTestData() { }
}