/**
 * @description A request to make a payment using the PaymentProcessor.
 **/
public class PaymentRequest extends PaymentProcessorRequest {
    public Id OpportunityId { get; set; }

    public Opportunity Opportunity {
        get {
            if (Opportunity == null && OpportunityId != null) {
                List<Opportunity> opportunities = OpportunitySelector.Instance
                        .selectByIdWithPfs(new Set<Id> { OpportunityId });

                if (!opportunities.isEmpty()) {
                    Opportunity = opportunities[0];
                }
            }
            return Opportunity;
        }
        set;
    }

    public PFS__c PFS { get; set; }

    /**
     * @description The generic PaymentInformation. This will be populated with
     *              either an instance of CreditCardInformation or ECheckInformation.
     */
    public PaymentInformation PaymentInformation { get; set; }

    /**
     * @description Identifies this PaymentRequest as a Credit Card transaction or not.
     *              Returns true if PaymentInformation is a populated instance of
     *              CreditCardInformation.
     */
    public Boolean IsCreditCard {
        get {
            return (PaymentInformation != null && PaymentInformation instanceof CreditCardInformation);
        }
    }

    /**
     * @description Identifies this PaymentRequest as an eCheck transaction or not.
     *              Returns true if PaymentInformation is a populated instance of
     *              ECheckInformation.
     */
    public Boolean IsECheck {
        get {
            return (PaymentInformation != null && PaymentInformation instanceof ECheckInformation);
        }
    }
}