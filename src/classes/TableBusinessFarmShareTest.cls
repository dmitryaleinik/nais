@IsTest
private class TableBusinessFarmShareTest
{

    @isTest
    private static void testTableBusinessFarmShare()
    {
        // create the academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        // create the table
        TableTestData.populateBusinessFarmShareData(academicYear.Id);

        // test low range
        System.assertEquals(4000, TableBusinessFarmShare.calculateBusinessFarmShare(academicYear.Id, 10000));

        // test middle range
        System.assertEquals(45351, TableBusinessFarmShare.calculateBusinessFarmShare(academicYear.Id, 100000));

        // test high range
        System.assertEquals(658147, TableBusinessFarmShare.calculateBusinessFarmShare(academicYear.Id, 1000000));
    }
}