/**
* Batch that every hour recalculates sharing for merged contacts.
*/
global class RecalcSharingForMergedContactsBatch implements Database.Batchable<sObject> {

    global RecalcSharingForMergedContactsBatch() { }

    /**
     * @description Gets a list of records that have to be handled from the Database.
     * @param bc System Batchable Context.
     */
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        Id parentContactRecordTypeId = RecordTypes.parentContactTypeId;

        String recalcSharingBatchQueryString = 'SELECT Sharing_Processed__c, Applicant__r.PFS__r.Parent_A__c FROM School_PFS_Assignment__c'
        + ' WHERE Applicant__r.PFS__r.Parent_A__r.Recalc_Sharing_For_Contact_Merge__c = true AND Sharing_Processed__c = true ORDER BY CreatedDate';

        return Database.getQueryLocator(recalcSharingBatchQueryString);
    }

    /**
     * @description Gets a chunk of records that were selected and handles them.
     * @param bc System Batchable Context.
     * @param scope Chunk of the records to handle.
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope)
    {
        if (scope == null || scope.isEmpty())
        {
            return;
        }

        Map<Id, School_PFS_Assignment__c> pfsAssignmentsToUpdateById = new Map<Id, School_PFS_Assignment__c>();
        Map<Id, Contact> contactsToUpdateById = new Map<Id, Contact>();

        for (School_PFS_Assignment__c pfsAssignment : (List<School_PFS_Assignment__c>)scope) {
            School_PFS_Assignment__c pfsAssignmentToUpdate = new School_PFS_Assignment__c(Id = pfsAssignment.Id);
            pfsAssignmentToUpdate.Sharing_Processed__c = false;
            pfsAssignmentsToUpdateById.put(pfsAssignment.Id, pfsAssignmentToUpdate);

            Contact contactToUpdate = new Contact(Id = pfsAssignment.Applicant__r.PFS__r.Parent_A__c);
            contactToUpdate.Recalc_Sharing_For_Contact_Merge__c = false;
            contactsToUpdateById.put(contactToUpdate.Id, contactToUpdate);
        }

        if (!pfsAssignmentsToUpdateById.isEmpty()) {
            update pfsAssignmentsToUpdateById.values();
        }

        if (!contactsToUpdateById.isEmpty()) {
            update contactsToUpdateById.values();
        }
    }

    /**
     * @description Finish method to perform some operations after batch was processed.
     * @param bc System Batchable Context.
     */
    global void finish(Database.BatchableContext bc) {}
}