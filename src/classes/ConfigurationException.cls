/**
 * @description An exception that is thrown when there is a configuration error.
 **/
public class ConfigurationException extends Exception { }