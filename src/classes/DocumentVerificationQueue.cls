/**
 * @description This class is used to send information to a document processing vendor to notify them that a family document needs to be verified.
 */
public with sharing class DocumentVerificationQueue implements Queueable, Database.AllowsCallouts {

    private static final String RECORD_IDS_PARAM = 'recordIds';

    private Set<Id> docAssignmentIds;

    private DocumentVerificationQueue(Set<Id> recordIds) {
        ArgumentNullException.throwIfNull(recordIds, RECORD_IDS_PARAM);
        this.docAssignmentIds = recordIds;
    }

    /**
     * @description Resends document verification requests to a third party using the DocumentVerificationService. If
     *              we are unable to process all of the records, we queue another job with the remaining records.
     */
    public void execute(QueueableContext context) {
        DocumentVerificationService.Instance.requestVerificationForProcessedDocs(this.docAssignmentIds);

        // Queue next job if we still have records to process.
        // This should not be done during unit tests since salesforce does not support job chaining in unit tests.
        Map<Id, School_Document_Assignment__c> remainingRecordsToProcessById =
                new Map<Id, School_Document_Assignment__c>(queryDocumentAssignments(this.docAssignmentIds));

        if (!remainingRecordsToProcessById.isEmpty() && !Test.isRunningTest()) {
            chainJob(remainingRecordsToProcessById.keySet());
        }
    }

    private List<School_Document_Assignment__c> queryDocumentAssignments(Set<Id> recordIds) {
        // TODO Selector School_Document_Assignment__c
        return [SELECT Id FROM School_Document_Assignment__c
        WHERE Id IN :recordIds AND Verification_Request_Status__c = :SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE ORDER BY CreatedDate];
    }

    /**
     * @description Schedules this queueable job for the specified record Ids. If the set is empty, we return null.
     * @param recordIds The Ids of the records that should be processed.
     * @return The Id of the queued job. Null if no job was added to the queue.
     */
    public static Id queueVerificationRequests(Set<Id> recordIds) {
        if (recordIds == null || recordIds.isEmpty()) {
            return null;
        }

        return System.enqueueJob(new DocumentVerificationQueue(recordIds));
    }

    /**
     * @description This future method is a temporary solution to allow us to chain queueable jobs that do callouts.
     *              This is not currently supported by Salesforce. The soonest we can expect a resolution is with the
     *              Spring '17 release. Even then we may have to opt-in to some sort of pilot.
     * @param recordIds The Ids of the records to queue the job for.
     */
    @future(callout=true)
    private static void chainJob(Set<Id> recordIds) {
        queueVerificationRequests(recordIds);
    }
}