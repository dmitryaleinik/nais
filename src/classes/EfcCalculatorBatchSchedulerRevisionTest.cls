@IsTest
private class EfcCalculatorBatchSchedulerRevisionTest
{

    @isTest
    private static void testSchedule()
    {
        Test.startTest();
            // first delete any jobs
            ScheduleSharingJobsController.deleteJobsRemotely();

            Id job0Id = System.schedule('Test Schedule', '0  0 * * * ?', new EfcCalculatorBatchSchedulerRevision());

            CronTrigger ct = [select Id, CronExpression from CronTrigger where Id = :job0Id];
            System.assertEquals('0  0 * * * ?', ct.CronExpression);
        Test.stopTest();
    }
}