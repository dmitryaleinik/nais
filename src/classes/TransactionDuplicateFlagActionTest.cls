/**
* @ TransactionDuplicateFlagActionTest.cls
* @date 4/17/2014
* @author  CH for Exponent Partners
* @description Unit tests from TransactionDuplicateFlagActionTest
*/

@isTest
private class TransactionDuplicateFlagActionTest {

    @isTest
    private static void testInsert() {
        Subscription_Settings__c sSettings = new Subscription_Settings__c();
        sSettings.Name = 'Subscription Settings';
        sSettings.Start_Month__c = 8;
        sSettings.Start_Day__c = 1;
        sSettings.Number_of_Months__c = 12;
        sSettings.Cutoff_Month__c = 3;
        insert sSettings;
        
        Account testAccount = TestUtils.createAccount('Test Account', RecordTypes.schoolAccountTypeId, 1, true);
            
        Opportunity opp = new Opportunity(AccountId = testAccount.Id, name = 'op1', Stagename = 'closed',closedate = System.Today().adddays(5));
        insert opp;
        // transaction_type__C 'Other School Fees' resolves Debit_Credit__c  to credit
        // transaction_type__C 'ACH' resolves Debit_Credit__c to debit
        Transaction_Line_Item__c t1 = new Transaction_Line_Item__c (Opportunity__c = opp.id,transaction_type__C = 'Other School Fees', Amount__c = 100);
        TransactionDuplicateFlagAction.canRun=true;
        insert t1;
        t1 = [select Duplicate_Payment__c, Opportunity__r.Net_Amount_Due__c , Opportunity__r.Count_of_Payments__c from Transaction_Line_Item__c where id = :t1.id];
        System.assertEquals(t1.Opportunity__r.Net_Amount_Due__c,100);
        System.assertEquals(t1.Opportunity__r.Count_of_Payments__c,0);
        System.assertEquals(t1.Duplicate_Payment__c,false);
        
        Transaction_Line_Item__c t2 = new Transaction_Line_Item__c (Opportunity__c = opp.id,Transaction_Status__c ='Posted',transaction_type__C = 'ACH', Amount__c = 100);
        TransactionDuplicateFlagAction.canRun=true;
        
        insert t2;
        
        t2 = [select Duplicate_Payment__c, Opportunity__r.Net_Amount_Due__c , Opportunity__r.Count_of_Payments__c from Transaction_Line_Item__c where id = :t2.id];
        System.assertEquals(t2.Opportunity__r.Net_Amount_Due__c,0);
        System.assertEquals(t2.Opportunity__r.Count_of_Payments__c,1);
        System.assertEquals(t2.Duplicate_Payment__c,false);
        Transaction_Line_Item__c t3 = new Transaction_Line_Item__c (Opportunity__c = opp.id,Transaction_Status__c ='Posted',transaction_type__C = 'ACH', Amount__c = 100);
        TransactionDuplicateFlagAction.canRun=true;
        
        insert t3;
        t3 = [select Duplicate_Payment__c, Transaction_Category__c, Opportunity__r.Net_Amount_Due__c , Opportunity__r.Count_of_Payments__c from Transaction_Line_Item__c where id = :t3.id];
        System.debug(t3.Transaction_Category__c + ' ' + t3.Duplicate_Payment__c);
        System.assertEquals(t3.Duplicate_Payment__c,true);
        System.assertEquals(t3.Opportunity__r.Count_of_Payments__c,2);
    }
    
    @isTest
    private static void testMultiple() {
        Subscription_Settings__c sSettings = new Subscription_Settings__c();
        sSettings.Name = 'Subscription Settings';
        sSettings.Start_Month__c = 8;
        sSettings.Start_Day__c = 1;
        sSettings.Number_of_Months__c = 12;
        sSettings.Cutoff_Month__c = 3;
        insert sSettings;
        
        Account testAccount = TestUtils.createAccount('Test Account', RecordTypes.schoolAccountTypeId, 1, true);
        
        Opportunity opp = new Opportunity(AccountId = testAccount.Id, name = 'op1', Stagename = 'closed',closedate = System.Today().adddays(5));
        insert opp;
        // transaction_type__C 'Other School Fees' resolves Debit_Credit__c  to credit
        // transaction_type__C 'ACH' resolves Debit_Credit__c to debit
        Transaction_Line_Item__c t1 = new Transaction_Line_Item__c (Opportunity__c = opp.id,transaction_type__C = 'Other School Fees', Amount__c = 100);
        TransactionDuplicateFlagAction.canRun=true;
        insert t1;
        t1 = [select Duplicate_Payment__c, Opportunity__r.Net_Amount_Due__c , Opportunity__r.Count_of_Payments__c from Transaction_Line_Item__c where id = :t1.id];
        System.assertEquals(t1.Opportunity__r.Net_Amount_Due__c,100);
        System.assertEquals(t1.Opportunity__r.Count_of_Payments__c,0);
        System.assertEquals(t1.Duplicate_Payment__c,false);
        
        Transaction_Line_Item__c t2 = new Transaction_Line_Item__c (Opportunity__c = opp.id,Transaction_Status__c ='Posted',transaction_type__C = 'ACH', Amount__c = 100);
        TransactionDuplicateFlagAction.canRun=true;
        
        insert t2;
        
        t2 = [select Duplicate_Payment__c, Opportunity__r.Net_Amount_Due__c , Opportunity__r.Count_of_Payments__c from Transaction_Line_Item__c where id = :t2.id];
        System.assertEquals(t2.Opportunity__r.Net_Amount_Due__c,0);
        System.assertEquals(t2.Opportunity__r.Count_of_Payments__c,1);
        System.assertEquals(t2.Duplicate_Payment__c,false);
        Transaction_Line_Item__c t3 = new Transaction_Line_Item__c (Opportunity__c = opp.id,Transaction_Status__c ='Posted',transaction_type__C = 'ACH', Amount__c = 100);
        Transaction_Line_Item__c t4 = new Transaction_Line_Item__c (Opportunity__c = opp.id,Transaction_Status__c ='Posted',transaction_type__C = 'ACH', Amount__c = 100);
        
        TransactionDuplicateFlagAction.canRun=true;
        
        insert new List<Transaction_Line_Item__c>{t4,t3};
        
        Map<ID,Transaction_Line_Item__c> transactions = new Map<ID,Transaction_Line_Item__c> ([select Duplicate_Payment__c, Transaction_Category__c, Opportunity__r.Net_Amount_Due__c , Opportunity__r.Count_of_Payments__c from Transaction_Line_Item__c ]);
        System.assertEquals(transactions.get(t4.id).Duplicate_Payment__c,true);
        System.assertEquals(transactions.get(t3.id).Duplicate_Payment__c,true);
        System.assertEquals(transactions.get(t4.id).Opportunity__r.Count_of_Payments__c,3);
    }
}