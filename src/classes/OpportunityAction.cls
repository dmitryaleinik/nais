/**
 * @description When the Opportunity of type Application Fee is paid
 *              then the Payment status on the PFS needs to be updated
 *              - Partially Paid, Paid in Full and Written Off are the
 *              possible values that will need to be set when the opp
 *              is updated.
 **/
public class OpportunityAction {
    public static Set<String> closedoppStageNames = new Set<String>{'Closed Won', 'Closed', 'Paid in Full'};
    public static Set<String> readyForSaleTLIStageNames = new Set<String>{'Ready for Sale TLI'};
    public static Map<Id, PFS__c> pfsIdToPFSForTriggerForUpdate = new Map<Id, PFS__c>();

    public static void updatePFSsFromOppTrigger(List<Opportunity> opportunitiesForPFSPaymentStatus, Set<Id> referencedPFSIdsForPFSAmounts){
        for (PFS__c p : updatePfsPaymentStatusFromOpportunity(opportunitiesForPFSPaymentStatus)){
            pfsIdToPFSForTriggerForUpdate.put(p.Id, p);
        }
        for (PFS__c p : updatePFSAmountsFromOpportunities(referencedPFSIdsForPFSAmounts)){
            pfsIdToPFSForTriggerForUpdate.put(p.Id, p);
        }


        // save the updated pfs records
        if (!pfsIdToPFSForTriggerForUpdate.values().isEmpty()) {
            // [CH] NAIS-1606 Adding try-catch to prevent EFC errors from preventing
            //     the insertion of an Opportunity and Transaction Line Item
            try{
                update pfsIdToPFSForTriggerForUpdate.values();
            }
            catch(EfcException e){
                System.debug('Error processing EFC after PFS status update: ' + e.getMessage());
            }
        }
    }

    // [DP] 11.13.2014 new method just for the Opp field logic in the before trigger
    public static void updateOppFieldsFromOppBefore(List<Opportunity> opportunities) {
        for (Opportunity opp : opportunities) {
            if (opp.RecordTypeId == RecordTypes.pfsApplicationFeeOpportunityTypeId) {

                // check if fee is waived
                if ((opp.Total_Waivers__c != null)
                        && (opp.Total_Waivers__c != 0)
                        && (opp.Total_Waivers__c >= opp.Amount)) {
                    opp.Fee_Waived__c = 'Yes';
                }
                else {
                    opp.Fee_Waived__c = 'No';
                }

                // check if opportunity is written off
                if ((opp.Total_Write_Offs__c != null)
                        && (opp.Total_Write_Offs__c != 0)
                        && (opp.Total_Write_Offs__c >= opp.Amount)) {

                    opp.StageName = 'Written Off';
                }

                // check if opportunity is closed
                else if ((opp.Paid_Status__c == 'Paid') || (opp.Paid_Status__c == 'Overpaid')){
                    opp.StageName = 'Closed';
                }

                // otherwise the opportunity is open
                else {
                    opp.StageName = 'Open';
                }

            }
            // [CH] NAIS-1508 Adding support for auto-updating StageName on Fee Waiver opps
            else if(opp.RecordTypeId == RecordTypes.opportunityFeeWaiverPurchaseTypeId && opp.Paid_Status__c != 'Cancelled'){
                // check if opportunity is paid
                if ((opp.Paid_Status__c == 'Paid') || (opp.Paid_Status__c == 'Overpaid')){
                    opp.StageName = 'Paid in Full';
                }
                else {
                    opp.StageName = 'Negotiation/Review';
                }
            }
        }
    }

    public static List<PFS__c> updatePfsPaymentStatusFromOpportunity(List<Opportunity> opportunities) {
        Map<Id, Opportunity> pfsIdToOpportunityMap = new Map<Id, Opportunity>();
        for (Opportunity opp : opportunities) {
            if (opp.RecordTypeId == RecordTypes.pfsApplicationFeeOpportunityTypeId) {
                // create a map of PFS ids to Opportunities
                if (opp.PFS__c != null) {
                    pfsIdToOpportunityMap.put(opp.PFS__c, opp);
                }
            }
        }

        // loop through the list of PFS records and update the status is necessary
        List<PFS__c> pfsRecordsToUpdate = new List<PFS__c>();
        Opportunity pfsOpportunity;
        String newPaymentStatus;
        Decimal newWaiverTotal;
        for (PFS__c pfs : [SELECT Id, Payment_Status__c, Fee_Waived__c FROM PFS__c WHERE Id IN :pfsIdToOpportunityMap.keySet()]) {
            pfsOpportunity = pfsIdToOpportunityMap.get(pfs.Id);
            if (pfsOpportunity != null) {

                // first check the stage name for written-off status
                if (pfsOpportunity.StageName == 'Written Off') {
                    newPaymentStatus = 'Written Off';
                }

                // if not a write-off, then set the PFS payment status based on the Opp Paid Status
                else {
                    newPaymentStatus = mapOpportunityPaidStatusToPfsPaymentStatus(pfsOpportunity.Paid_Status__c);

                }

                boolean isUpdated = false;
                // update the payment status field on the PFS if necessary
                if ((newPaymentStatus != null) && (pfs.Payment_Status__c != newPaymentStatus)) {
                    pfs.Payment_Status__c = newPaymentStatus;
                    isUpdated = true;
                }

                // update the fee waived status field on the PFS if necessary
                if (pfs.Fee_Waived__c != pfsOpportunity.Fee_Waived__c) {
                    if (pfs.Fee_Waived__c == 'Waiver Requested' && pfsOpportunity.Fee_Waived__c == 'No'){
                        // [DP] do nothing in this case, since waiver is still requested
                    } else {
                        pfs.Fee_Waived__c = pfsOpportunity.Fee_Waived__c;
                        isUpdated = true;
                    }
                }
                if (isUpdated == true) {
                    pfsRecordsToUpdate.add(pfs);
                }
            }
        }

        return pfsRecordsToUpdate;
    }

    private static String mapOpportunityPaidStatusToPfsPaymentStatus(String oppPaidStatus) {
        String pfsPaymentStatus = null;
        if (oppPaidStatus == 'Paid') {
            pfsPaymentStatus = 'Paid in Full';
        } else if (oppPaidStatus == 'Unpaid') {
            pfsPaymentStatus = 'Unpaid';
        } else if (oppPaidStatus == 'Underpaid') {
            pfsPaymentStatus = 'Partially Paid';
        } else if (oppPaidStatus == 'Overpaid') {
            pfsPaymentStatus = 'Paid in Full';
        }

        return pfsPaymentStatus;
    }

    // [CH] NAIS-1619
    public static List<PFS__c> updatePFSAmountsFromOpportunities(Set<Id> referencedPFSIds) {
        List<PFS__c> pfsRecordsToUpdate = new List<PFS__c>{};

        // Query for opportunity
        List<AggregateResult> opportunityResults = [select PFS__c, SUM(Amount) opportunitySum from Opportunity where PFS__c in :referencedPFSIds group by PFS__c ];
        for(AggregateResult result : opportunityResults){
            PFS__c pfsToUpdate;
            Id pfsId = (Id)result.get('PFS__c');
            if (pfsIdToPFSForTriggerForUpdate.get(pfsId) != null){
                pfsToUpdate = pfsIdToPFSForTriggerForUpdate.get(pfsId);
                pfsToUpdate.Amount_Paid__c = (Decimal)result.get('opportunitySum');
            } else {
                pfsToUpdate = new PFS__c(Id = (Id)result.get('PFS__c'), Amount_Paid__c = (Decimal)result.get('opportunitySum'));
            }
            pfsRecordsToUpdate.add(pfsToUpdate);
        }

        //if(pfsRecordsToUpdate.size() > 0){
        //    update pfsRecordsToUpdate;
        //}
        return pfsRecordsToUpdate;
    }

    // NAIS-2488 [WH] 2015-07-17 START
    private static Set<Id> oppIdsProcessedForSubscriptionCreation = new Set<Id>();
    // NAIS-2488 [WH] 2015-07-17 END

    // [dp] NAIS-1628 - create subscriptions from paid opps with next years dates
    public static void createSubscriptionRecordsFromPaidOpps(List<Opportunity> theOpps) {
        // NAIS-2488 [WH] 2015-07-17 START
        //    prevent duplicate trigger firing, e.g. update trigger -> WF rule -> update trigger
        //    more general mechanism to handle same trigger firing in tandem but possibly with different set of Opps
        List<Opportunity> oppsToProcess = new List<Opportunity>();
        for (Opportunity o : theOpps) {
            if (!oppIdsProcessedForSubscriptionCreation.contains(o.Id)) {
                // have not been processed before
                oppIdsProcessedForSubscriptionCreation.add(o.Id);
                oppsToProcess.add(o);
            } // else do nothing because this Opp has already been processed for new subscription record creation
        }
        if (oppsToProcess.isEmpty()) {
            return;
        }
        // NAIS-2488 [WH] 2015-07-17 END

        List<Subscription__c> theSubscriptionsToCreate = new List<Subscription__c>();
        // NAIS-2417 [DP] 05.13.2015 use picklist field, no longer need to query for academic year
        //Map<Id, Academic_Year__c> acadYearsById = new Map<Id, Academic_Year__c>([Select Id, Name from Academic_Year__c]);

        for (Opportunity o : oppsToProcess) {
            Subscription__c s = new Subscription__c();
            s.Account__c = o.AccountId;
            s.Subscription_Type__c = o.Subscription_Type__c != null ? o.Subscription_Type__c : 'Full';
            s.Paid_Date__c = o.CloseDate;

            // get values from custom settings, handling null values
            //Academic_Year__c acad = acadYearsById.get(o.Academic_Year__c);
            // [WH] NAIS-2270 Take Suscription start year from Subscription_Period_Start_Year__c field if set explicitly in the Opp
            //Integer year = acad == null ? o.CloseDate.Year() : Integer.valueOf(acad.Name.Left(4));
            /*Integer year = String.isNotBlank(o.Subscription_Period_Start_Year__c) ? Integer.valueOf(o.Subscription_Period_Start_Year__c) :
                            // NAIS-2417 [DP] 05.13.2015 use picklist field
                            // NAIS-2474 [WH] 06.24.2015 Subscription period is one year in advance of Academic Year on the Opp
                            (o.Academic_Year_Picklist__c == null ? o.CloseDate.Year() : Integer.valueOf(o.Academic_Year_Picklist__c.Left(4)) - 1);*/
            Map<string, Date> subscriptionDates = OpportunityAction.subscriptionDatesCalculator(o);
            s.Start_Date__c = subscriptionDates.get('startDate');
            s.End_Date__c = subscriptionDates.get('endDate');

            theSubscriptionsToCreate.add(s);
        }

        insert theSubscriptionsToCreate;
    }

    /**
     * @description Method implemented to calculate the subscription__c.Start_Date__c and
     * subscription__c.End_Date__c according to a given opportunity values.
     * @param o The record of the related opportunity.
     * @return A map with a map of the dates.
     */
    private static Map<string, Date> subscriptionDatesCalculator(Opportunity o) {
        try {
            Subscription_Settings__c sSettings = Subscription_Settings__c.getInstance('Subscription Settings');
            Integer year = String.isBlank(o.Academic_Year_Picklist__c) ? (Integer.ValueOf(GlobalVariables.getCurrentAcademicYear().name.left(4))-1):(Integer.valueOf(o.Academic_Year_Picklist__c.Left(4)) - 1);
            Integer startMonth = sSettings.Start_Month__c != null ? Integer.valueOf(sSettings.Start_Month__c) : 9;
            Integer startDay = sSettings.Start_Day__c != null ? Integer.valueOf(sSettings.Start_Day__c) : 30;
            Integer endMonth = sSettings.End_Month__c != null ? Integer.valueOf(sSettings.End_Month__c) : 9;
            Integer endDay = sSettings.End_Day__c != null ? Integer.valueOf(sSettings.End_Day__c) : 30;
            Date startDate = Date.newInstance(year, startMonth, startDay);
            startDate = startDate > o.CloseDate ? startDate : o.CloseDate;
            Integer numberOfMonths = sSettings.Number_of_Months__c != null ? Integer.valueOf(sSettings.Number_of_Months__c) : 12;
            Integer numberOfExtMonths = sSettings.Number_of_Months_Extended__c != null ? Integer.valueOf(sSettings.Number_of_Months_Extended__c) : 36;
            Integer monthsToAdd = o.Subscription_Type__c == 'Extended' ? numberOfExtMonths : numberOfMonths;
            Date endDate = Date.newInstance(year, endMonth, endDay).addMonths(monthsToAdd);
            return new Map<string, Date>{'startDate' => startDate, 'endDate' => endDate};
        } catch (Exception e) {
            return new Map<string, Date>{'startDate' => null, 'endDate' => null};
        }
    }//End:subscriptionDatesCalculator

    /**
     * @description Method implemented to delete subscriptions related to given opportunities
     *              in the following scenario: If a subscription opportunity changes from Closed/Won
     *              to any other stage the subscription record should be deleted.
     * @param theOpps List of opportunities which related subscriptions must be deleted.
     */
    public static void deleteSubscriptionRecordsFromPaidOpps(List<Opportunity> theOpps) {
        List<Date> startDates = new List<Date>();
        List<Date> endDates = new List<Date>();
        Map<string, Date> subscriptionDates;
        Map<Id, Map<string, Date>> subscriptionDatesByOpp = new Map<Id, Map<string, Date>>();
        Map<Id, List<Opportunity>> opportunitiesByAccount = new Map<Id, List<Opportunity>>();
        List<Opportunity> tmpOppList;

        // Logic to retrieve the list of startDates, endDates, AcountIds to query from
        // Subscriptions records.
        for (Opportunity o:theOpps) {
            if (o.AccountId != null) {
                subscriptionDates = OpportunityAction.subscriptionDatesCalculator(o);
                startDates.add(subscriptionDates.get('startDate'));
                endDates.add(subscriptionDates.get('endDate'));

                subscriptionDatesByOpp.put(o.Id, subscriptionDates);
                tmpOppList = (opportunitiesByAccount.containsKey(o.AccountId)
                        ? opportunitiesByAccount.get(o.AccountId)
                        : new List<Opportunity>());
                tmpOppList.add(o);
                opportunitiesByAccount.put(o.AccountId, tmpOppList);
            }
        }

        List<Subscription__c> subscriptionsToDelete = new List<Subscription__c>();
        Map<Id, List<Subscription__c>> subscriptionsByOpportunity = new Map<Id, List<Subscription__c>>();
        for(Subscription__c s:[Select Id, Subscription_Type__c, Account__c, Start_Date__c, End_Date__c
                                        from Subscription__c
                                        where Account__c IN: opportunitiesByAccount.keyset()
                                        and Start_Date__c IN: startDates
                                        and End_Date__c IN: endDates
                                        and Start_Date__c!=null
                                        and End_Date__c!=null ]) {
            for(Opportunity o:opportunitiesByAccount.get(s.Account__c)) {
                if(s.Subscription_Type__c == o.Subscription_Type__c
                && s.Account__c == o.AccountId
                && (subscriptionDatesByOpp.get(o.Id)).get('startDate') == s.Start_Date__c
                && (subscriptionDatesByOpp.get(o.Id)).get('endDate') == s.End_Date__c) {
                    subscriptionsToDelete.add(s);
                }
            }
        }//End-For

        if(!subscriptionsToDelete.isEmpty()) {
            delete subscriptionsToDelete;
        }
    }
}