/**
 * An apex page controller that exposes the change password functionality
 */
public with sharing class ChangePasswordController {
    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}        

    /**
     * @description Determines which portal we are resetting a password from then defers to the SiteService to handle
     *              changing the password.
     * @return The page to navigate to after changing passwords.
     */
    public PageReference changePassword() {
        if( String.isBlank(newPassword) || String.isBlank(verifyNewPassword) || (!Site.IsPasswordExpired() && String.isBlank(oldPassword)) ){
            if( !Site.IsPasswordExpired() && String.isBlank(oldPassword) )ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Old Password: '+Label.CHANGEPASS_FIELD_IS_REQUIRED));
            else if( String.isBlank(newPassword) )ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'New Password: '+Label.CHANGEPASS_FIELD_IS_REQUIRED));
            else if( String.isBlank(verifyNewPassword) )ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Verify New Password: '+Label.CHANGEPASS_FIELD_IS_REQUIRED));
            
            return null;
        }
        
        // Determine the type of portal we are in.
        SiteService.PortalType currentPortal = Site.getName()=='Family_Portal' 
                                                ? SiteService.PortalType.FAMILY 
                                                : SiteService.PortalType.SCHOOL;

        SiteService.Request changePasswordRequest = new SiteService.Request(currentPortal, newPassword, verifyNewPassword, oldPassword);

        PageReference postPasswordChangePage = SiteService.Instance.changePassword(changePasswordRequest);

        return postPasswordChangePage;
    }
    
       public ChangePasswordController() {}
}