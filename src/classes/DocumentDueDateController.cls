/**
 * DocumentDueDateController.cls
 *
 * @description: Controller for DocumentDueDate.component. Gets all documnet due dates for a given PFS.
 * some of this code logic was pulled from FamilyListController.cls
 *
 * @author: Mike Havrilla @ Presence PG
 */

public class DocumentDueDateController {
    public PFS__c pfs { get; set; }
    public List<String> yearValueList { get; set; }
    public Boolean showHeader { get; set; }
    public String headerClass { get; set; }

    public DocumentDueDateController() {}

    public void loadDocumentDueDates() {
        Academic_Year__c currentYearRecord;
        documentDueDates = new List<DocDueDateRow>();
        
        // Query for the Academic Year record related to the currently selected PFS
        if(pfs != null){
            currentYearRecord = GlobalVariables.getAcademicYearByName( pfs.Academic_Year_Picklist__c);
        }

        List<String> yearsList = currentYearRecord.Name.split('-');

        // Put the current, previous year and previous previous year values into a Set for querying
        yearValueList = new List<String>{};
        if(yearsList != null){
            Integer yearInt = Integer.valueOf(yearsList[0]);
            yearValueList.add(String.valueOf(yearInt-1));
            yearValueList.add(String.valueOf(yearInt-2));
            yearValueList.add(String.valueOf(yearInt-3));
        }

        Map<Id, School_PFS_Assignment__c> spaDedupeMap = new Map<Id, School_PFS_Assignment__c>{};
        for(School_PFS_Assignment__c spaRecord : [select Id, School__c, School__r.Name 
                                                    from School_PFS_Assignment__c 
                                                    where Applicant__r.PFS__c = :pfs.Id 
                                                    and Withdrawn__c<>'Yes']){
           spaDedupeMap.put(spaRecord.School__c, spaRecord);
        }
        Map<Id, Annual_Setting__c> annualSettingsMap = new Map<Id, Annual_Setting__c>{};
        for(Annual_Setting__c settingRecord : [select Id, School__c, School__r.Name, Current_Year_Tax_Documents_New__c, 
                                                    Current_Year_Tax_Documents_Returning__c, 
                                                    PP_Year_Tax_Documents_New__c, PP_Year_Tax_Documents_Returning__c, 
                                                    Prior_Year_Tax_Documents_New__c, Prior_Year_Tax_Documents_Returning__c
                                                    from Annual_Setting__c 
                                                    where School__c in :spaDedupeMap.keySet()
                                                    and Academic_Year__c = :currentYearRecord.Id
                                                    order by School__r.Name asc]) { 
            // Add results to a map to deduplicate
            annualSettingsMap.put(settingRecord.School__c, settingRecord);
        }



        for(School_PFS_Assignment__c spaRecord : spaDedupeMap.values()){
            DocDueDateRow docRow = new DocDueDateRow();
            docRow.schoolName = spaRecord.School__r.Name;
            
            Annual_Setting__c settingRecord = annualSettingsMap.get(spaRecord.School__c);
            
            docRow.newCurrent = (settingRecord != null && settingRecord.Current_Year_Tax_Documents_New__c != null) ? settingRecord.Current_Year_Tax_Documents_New__c.format() : 'N/A';
            docRow.newPrevious = (settingRecord != null && settingRecord.Prior_Year_Tax_Documents_New__c != null) ? settingRecord.Prior_Year_Tax_Documents_New__c.format() : 'N/A';
            docRow.returningCurrent = (settingRecord != null && settingRecord.Current_Year_Tax_Documents_Returning__c != null) ? settingRecord.Current_Year_Tax_Documents_Returning__c.format() : 'N/A';
            docRow.returningPrevious = (settingRecord != null && settingRecord.Prior_Year_Tax_Documents_Returning__c != null) ? settingRecord.Prior_Year_Tax_Documents_Returning__c.format() : 'N/A'; 
            // SFP-1812 : Added to display Prior Prior Year Docs/Requirements to Families
            docRow.newPrePrevious = (settingRecord != null && settingRecord.PP_Year_Tax_Documents_New__c != null) ? settingRecord.PP_Year_Tax_Documents_New__c.format() : 'N/A';			
            docRow.returningPrePrevious = (settingRecord != null && settingRecord.PP_Year_Tax_Documents_Returning__c != null) ? settingRecord.PP_Year_Tax_Documents_Returning__c.format() : 'N/A'; 
            documentDueDates.add(docRow); 
        }
    }

    public List<DocDueDateRow> documentDueDates {
        get{
            if(documentDueDates == null){
                loadDocumentDueDates();
            } 
            return documentDueDates;
        }
        set;
    }

    // inner class to hold DocumentDueDates
    public class DocDueDateRow {

        public String schoolName { get; set; }
        public String newPrevious { get; set; }
        public String newCurrent { get; set; }
        public String returningPrevious { get; set; }
        public String returningCurrent { get; set; }
        // SFP-1812 : Added to displaying Prior Prior Year Docs/Requirements to Families		
        public String newPrePrevious { get; set; } 
        public String returningPrePrevious { get; set; }
         
        public DocDueDateRow() {}
    }
}