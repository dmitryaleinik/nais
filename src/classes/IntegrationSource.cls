public without sharing class IntegrationSource {

    public final String RAVENNA_TOGGLE = 'UseLegacyRavennaIntegration__c';

    /* props */
    public PFS__c pfs { get; set; }
    public Id ayId { get; set; }
    public String integrationSourceFromCookie { get; set; }
    public String selectedIntegrationSource { get; set; }

    private User userRecord;
    private String homePageMessage;
    private IntegrationSource__c integrationSource;
    private Map<String, String> authSourceToStatus;
    private String dashboardURL;
    private List<IntegrationSource__c> integrationSources;

    // ctor
    public IntegrationSource() {}

    /* accessor methods */
    /* Help define the difference of legacy integrations and current integrations */
    public User getUserRecord() {

        if ( userRecord == null) {

            if ( UserInfo.getUserId() != null) {

                userRecord = [
                        select Id, Name, IntegrationSource__c, IntegrationRavennaStatus__c, IntegrationStatus__c,
                                ProcessingIntegration__c
                        from User
                        where Id = :UserInfo.getUserId()
                ];
            }
        }
        return userRecord;
    }

    public Boolean getAuthSourceIsEmpty() {

        return authSourceToStatus != null && authSourceToStatus.size() > 0;
    }

    public Boolean getIsIntegrationInitialized() {

        Boolean showIntializedModal = false;
        if( FeatureToggles.isEnabled( this.RAVENNA_TOGGLE)) {

            showIntializedModal = getUserRecord().IntegrationStatus__c == 'Initialized';
        } else {

            showIntializedModal = this.pfs.IntegrationStatus__c == 'Initialized';
        }

        return showIntializedModal;
    }

    public String getCurrentProcessingIntegration() {

        String currentIntegration = '';
        if( FeatureToggles.isEnabled( this.RAVENNA_TOGGLE)) {

            currentIntegration = getUserRecord().ProcessingIntegration__c;
        } else {

            currentIntegration = this.pfs.ProcessingIntegration__c;
        }

        return currentIntegration;
    }

    public Map<String, String> getAuthSourceToStatus() {

        if ( !FeatureToggles.isEnabled( this.RAVENNA_TOGGLE)) {

            if ( authSourceToStatus == null) {
                authSourceToStatus = new Map<String, String>();
            }

            String tempSource = '';
            if( this.pfs.IntegrationSource__c != null) {

                for ( String integrationSource : this.pfs.IntegrationSource__c.split(';')) {
                    authSourceToStatus.put( integrationSource, 'Not Started');
                }
            }

            if( this.pfs.IntegrationErrors__c != null) {
                for ( String integrationSource : this.pfs.IntegrationErrors__c.split(';')) {
                    authSourceToStatus.put( integrationSource, 'Error');
                }
            }

        } else {

            // Used in legacy ravenns integration
            if (authSourceToStatus == null) {

                authSourceToStatus = new Map<String, String> { 'ravenna' => 'Not Started' };
                if ( getUserRecord().IntegrationRavennaStatus__c != null) {

                    authSourceToStatus.put('ravenna', getUserRecord().IntegrationRavennaStatus__c);
                }
            }
        }
        return authSourceToStatus;
    }

    // Returns Integration sources if they are not past the statparentsguardians__c step.
    public List<IntegrationSource__c> getIntegrationSources() {

        if (integrationSources == null) {

            integrationSources = new list<IntegrationSource__c>();

            if ( FeatureToggles.isEnabled( this.RAVENNA_TOGGLE)) {

                if ( getuserRecord().IntegrationSource__c != null) {

                    for ( String each : getuserRecord().IntegrationSource__c.split(';')) {

                        if( each == 'ravenna' && pfs != null && !pfs.StatParentsGuardians__c) {

                            integrationSources.add( IntegrationSource__c.getInstance( each.toLowerCase()));
                        }
                    }
                }
            } else {

                if ( this.pfs.IntegrationSource__c != null) {

                    for ( String each : this.pfs.IntegrationSource__c.split(';')) {

                        if( pfs != null && !pfs.StatParentsGuardians__c) {

                            integrationSources.add( IntegrationSource__c.getInstance( each.toLowerCase()));
                        }
                    }
                }
            }

        }
        return integrationSources;
    }

    /* Returns home page message for the login screen */
    public String getHomePageMessage() {

        if ( homePageMessage == null) {

            if ( getIntegrationSource() != null) {

                if ( getIntegrationSource().homePageMessage__c.startsWith('Label.')) {

                    homePageMessage = getIntegrationSource().homePageMessage__c.split('.')[1];
                } else {

                    homePageMessage = getIntegrationSource().homePageMessage__c;
                }
            }

        }
        return homePageMessage;
    }

    public String getDashboardURL() {

        if (dashboardURL == null) {

            dashboardURL = 'https://' + ApexPages.currentPage().getHeaders().get('Host') + '/familyportal/FamilyDashboard?source=' + ApexPages.currentPage().getParameters().get('source');

        }
        return dashboardURL;
    }

    public IntegrationSource__c getIntegrationSource() {

        if (integrationSource == null) {

            String source = ApexPages.currentPage().getParameters().get( 'source');
            if (!String.isEmpty(source)) {

                integrationSource = IntegrationSource__c.getInstance( source.toLowerCase());
            }
        }
        return integrationSource;
    }

    // Entry method, applies integration source to PFS Or to user Record.
    public void applyIntegrationSource() {

        if( FeatureToggles.isEnabled( this.RAVENNA_TOGGLE)) {

            //IntegrationSource__c is a multipicklist on the user record that indicates the user has an external system available for integration with
            String integrationSourceFromCookie = ApexPages.currentPage().getParameters().get('integrationSource');
            if (!String.isEmpty(integrationSourceFromCookie)) {

                if (getuserRecord().IntegrationSource__c != null) {

                    if (!getuserRecord().IntegrationSource__c.contains(integrationSourceFromCookie)) {

                        getuserRecord().IntegrationSource__c = getuserRecord().IntegrationSource__c + integrationSourceFromCookie;
                    }
                } else {

                    getuserRecord().IntegrationSource__c = integrationSourceFromCookie;
                }

                update getuserRecord();
                integrationSources = null;
            }
        } else {

            this.applyIntegrationSourceOnPFS();
        }
    }

    // Registration method, Executes the OAUTH PROCESS and sets
    // default values on the user reocrd or pfs based on legacy or new process
    public PageReference kickoffRegistration() {

        //selectedIntegrationSource = ApexPages.currentPage().getParameters().get('selectedIntegrationSource');
        if( FeatureToggles.isEnabled( this.RAVENNA_TOGGLE))  {
            // set the status and current integration for the user
            getUserRecord().processingIntegration__c = selectedIntegrationSource;
            getUserRecord().integrationStatus__c = 'Initialized';

            update getUserRecord();
        } else {

            this.pfs.ProcessingIntegration__c = selectedIntegrationSource;
            this.pfs.IntegrationStatus__c = 'Initialized';

            update this.pfs;
        }

        if (!Test.isRunningTest()) {

            // redirect to the correct oauth client.
            String ssoURL = Auth.AuthConfiguration.getAuthProviderSsoUrl( Site.getbasesecureurl(), '/FamilyDashboard?academicyearid=' + ayid + '&id=' + pfs.id, selectedIntegrationSource);
            String regUrl = ssoUrl.replace( 'sso', 'oauth');
            return new PageReference(regUrl);
        } else {

            return null;
        }
    }

    // Kicks off the integration, calls downstream methods.
    public void kickoffIntegration() {

        final String RAVENNA_INTEGRATION_NAME = 'ravenna';

        String processing = getUserRecord().processingIntegration__c;
        if ( FeatureToggles.isEnabled( this.RAVENNA_TOGGLE) && processing.toLowerCase() == RAVENNA_INTEGRATION_NAME) {

            // ***this is not generic : for each integration we have a specific flag to mark as integration complete on the user record
            // conditions to start integration:
            // IntegrationSource__c value,  getToken on the source returns a token, and any integration specific conditions (E.g. ravenna->fresh pfs, no parent fields yet)
            // You should not be using this anymore.
            if ( token( RAVENNA_INTEGRATION_NAME) != null) {

                //now check ravenna specific conditions (has a pfs but only just started)
                if ( !pfs.statparentsguardians__c) {

                    processRavenna();
                }
            }
            this.resetIntegration();
        } else if( !FeatureToggles.isEnabled( this.RAVENNA_TOGGLE)) {

            processing = this.pfs.processingIntegration__c;
            System.debug('DEBUG:::processing' + processing);
            IntegrationApiService.Request request = new IntegrationApiService.Request( processing, this.pfs);
            IntegrationApiService.Response response = IntegrationApiService.Instance.executeIntegration( request);

            if( response.success) {

                this.setProcessedIntegrationSourceOnPfs( processing);
                ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Confirm, response.messageToDisplay));
            } else {

                this.setErrorIntegrationSourceOnPfs( processing);
                ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.error, response.messageToDisplay));
            }
            this.resetIntegrationPFS();
            this.resetIntegration();
        }
    }

    /* The following methods are all NEW Integration methods
        with the integration status being persisted on the PFS Record
     */

    // This method updates the PFS INtegration source
    // and removes it from the User record.
    // This is important when the user is redirected
    // from the integration site, the only way we know to
    // process the integration on a PFS is from the user record.
    // So this will be kept as a "placeholder"
    public void applyIntegrationSourceOnPFS() {

        //IntegrationSource__c is a multipicklist on the user record that indicates the user has an external system available for integration with
        String integrationSourceFromCookie = ApexPages.currentPage().getParameters().get('integrationSource');
        System.debug('DEBUG:::integrationSourceFromCookie' + integrationSourceFromCookie);
        if ( !String.isEmpty(integrationSourceFromCookie)) {

            if ( this.pfs.IntegrationSource__c != null) {

                if ( !this.pfs.IntegrationSource__c.contains( integrationSourceFromCookie)) {

                    this.pfs.IntegrationSource__c = pfs.IntegrationSource__c + ';' + integrationSourceFromCookie;
                }
            } else {

                this.pfs.IntegrationSource__c = integrationSourceFromCookie;
            }

            update this.pfs;

            getuserRecord().IntegrationSource__c = null;
            update getUserRecord();

            integrationSources = null;
        }
    }

    private void resetIntegrationPFS() {

        // after an integration, reinitialize to allow next integrations
        this.pfs.IntegrationStatus__c = 'Not Started';
        this.pfs.ProcessingIntegration__c = null;
        update this.pfs;
    }

    private void setErrorIntegrationSourceOnPfs( String source) {

        Boolean containsError = false;
        if( this.pfs.IntegrationErrors__c != null) {

            for ( String integrationSource : this.pfs.IntegrationErrors__c.split(';')) {

                if( integrationSource.equalsIgnoreCase( source)) {

                    containsError = true;
                }
            }

            if ( !containsError) {

                if ( this.pfs.IntegrationErrors__c != null) {

                    this.pfs.IntegrationErrors__c += ';'+ source;
                } else {

                    this.pfs.IntegrationErrors__c = source;
                }
            }
        } else {

            pfs.IntegrationErrors__c = source;
        }

        this.pfs.ProcessingIntegration__c = null;
        this.pfs.IntegrationStatus__c = 'Error';
        update this.pfs;

        this.setIntegrationsProcesedOnPfs( source);
    }

    private void setProcessedIntegrationSourceOnPfs( String source) {

        // If it's done processing then remove it from teh multiselect picklist.
        String tempSource = '';
        for ( String integrationSource : this.pfs.IntegrationSource__c.split(';')) {

            if ( !integrationSource.equalsIgnoreCase( source)) {

                tempSource += integrationSource + ';';
            }
        }
        this.pfs.IntegrationSource__c = tempSource;

        // If it's done processing but and there was an error it from teh multiselect piclist.
        String errorSource = '';
        if( this.pfs.IntegrationErrors__c != null) {

            for (String integrationSource : this.pfs.IntegrationErrors__c.split(';')) {

                if (!integrationSource.equalsIgnoreCase( source)) {

                    errorSource += integrationSource + ';';
                }
            }
            this.pfs.IntegrationErrors__c = errorSource;
        }

        this.setIntegrationsProcesedOnPfs( source);
    }

    // sets integration processed multi select. This field is only for successful integrations
    private void setIntegrationsProcesedOnPfs( String source) {

        // If it's done processing but and there was an error it from teh multiselect piclist.
        if( this.pfs.IntegrationProcessed__c != null) {

            if ( !this.pfs.IntegrationProcessed__c.contains( source)) {

                this.pfs.IntegrationProcessed__c = pfs.IntegrationProcessed__c + ';' + source;
            }
        } else {

            this.pfs.IntegrationProcessed__c = source;
        }


    }

    // The following is for Legacy Ravenna Integration.

    public String token( String source) {

        List<AuthProvider> auths = [ select Id from AuthPRovider where DeveloperName = :source];
        String tokenResponse = null;
        // not fully generic, specific auth type would need to be passed in for each
        if ( !auths.isEmpty()) {

            tokenResponse = Auth.AuthToken.getAccessToken(auths[0].id, getauthSourceToType().get( source));
        }

        if ( Test.isRunningTest()) {

            tokenResponse = 'TestToken';
        }

        return tokenResponse;
    }

    private void setProcessedIntegrationSource( String source) {

        // If it's done processing then remove it from teh multiselect piclist.
        String tempSource = '';
        for ( String integrationSource : getUserRecord().IntegrationSource__c.split(';')) {

            if ( !integrationSource.equalsIgnoreCase( source)) {

                tempSource += integrationSource + ';';
            }
        }
        getUserRecord().IntegrationSource__c = tempSource;

    }

    private void resetIntegration() {
        // after an integration, reinitialize to allow next integrations
        getUserRecord().ProcessingIntegration__c = null;
        getUserRecord().IntegrationStatus__c = 'Not Started';
        update getUserRecord();

        userRecord = null;
        integrationSources = null;
        authSourceToStatus = null;
    }

    public Map<String, String> getauthSourceToType() {

        return new Map<String, String> { 'ravenna' => 'Open ID Connect' };
    }

    public void processRavenna() {

        IntegrationPFS processor = new IntegrationPFS( pfs, 'ravenna');
        try {

            // when successfully completed, mark the checkbox on user record
            // add a pagemessage indicating success
            processor.processRavenna();
            getUserRecord().IntegrationRavennaStatus__c = 'Complete';
            this.setProcessedIntegrationSource( 'ravenna');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, System.Label.Ravenna_Integration_Complete));

        } catch (Exception e) {

            // expose a custom message that they should ask for support
            getUserRecord().integrationRavennaStatus__c = 'Error';
            // update the user at the end if it suceeded or failed
            if (e.getMessage() == 'Unable to find applicant data for the selected year.')
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Unable to find applicant data for the selected year.')); else
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.Ravenna_Integration_error));
            processor.logException(e);
        }
    }
}