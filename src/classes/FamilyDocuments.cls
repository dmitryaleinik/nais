/**
 * @description This is the domain class for Family_Document__c records.
 */
public class FamilyDocuments {

    // Document_Status__c picklist values.
    public static final String DOC_STATUS_PROCESSED = 'Processed';
    public static final String DOC_STATUS_RECEIVED = 'Received/In Progress';
    public static final String DOC_STATUS_NA = 'Not Applicable/Waive Requested';
}