@isTest
private class CallCenterPFSFamilyViewTest {
    @isTest
    private static void constructor_noPfsId_expectPageMessage() {
        Test.setCurrentPage(Page.CallCenterPFSFamilyView);
        CallCenterPFSFamilyView controller = new CallCenterPFSFamilyView();

        List<ApexPages.Message> pageMessages = ApexPages.getMessages();
        System.assertNotEquals(null, pageMessages, 'Expected the page messages to not be null.');
        System.assertEquals(1, pageMessages.size(), 'Expected there to be exactly one page message.');
        System.assertEquals(CallCenterPFSFamilyView.FAMILY_VIEW_MESSAGE, pageMessages[0].getDetail(),
                'Expected the page message to match.');
    }

    @isTest
    private static void constructor_pfsId_expectNoPageMessage() {
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        PageReference familyView = Page.CallCenterPFSFamilyView;
        familyView.getParameters().put('id', pfs.Id);
        Test.setCurrentPage(familyView);
        CallCenterPFSFamilyView controller = new CallCenterPFSFamilyView();

        List<ApexPages.Message> pageMessages = ApexPages.getMessages();
        System.assertNotEquals(null, pageMessages, 'Expected the page messages to not be null.');
        System.assert(pageMessages.isEmpty(), 'Expected there to be no page messages.');
    }

    @isTest
    private static void init_noPfsId_expectNullPageReference() {
        Test.setCurrentPage(Page.CallCenterPFSFamilyView);
        CallCenterPFSFamilyView controller = new CallCenterPFSFamilyView();

        System.assertEquals(null, controller.init(), 'Expected the returned page reference to be null.');
    }

    @isTest
    private static void init_pfsId_expectPageReference() {
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        PageReference familyView = Page.CallCenterPFSFamilyView;
        familyView.getParameters().put('id', pfs.Id);
        Test.setCurrentPage(familyView);
        CallCenterPFSFamilyView controller = new CallCenterPFSFamilyView();

        String expectedUrl = 'familydashboard?id=' + pfs.Id;

        PageReference familyDashboard = controller.init();
        System.assertNotEquals(null, familyDashboard, 'Expected the dashboard to be returned.');
        System.assert(familyDashboard.getUrl().contains(expectedUrl), 'Expected the dashboard to be returned.');
    }
}