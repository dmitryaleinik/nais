/**
 * @description This class is used as the Connected App Plugin for the Connected Apps that handle authentication for the
 *              Financial Aid API. This class activates the integration for first time users and activates a session
 *              based permission set that corresponds to the current connected app. This class is generic enough to be
 *              used for all connected apps for the Financial Aid API.
 */
global class FinancialAidApplications_ConnAppPlugin extends Auth.ConnectedAppPlugin {

    global override Boolean authorize(Id userId, Id connectedAppId, Boolean isAdminApproved, Auth.InvocationContext context) {
        // Create a request for the Fin Aid API Service which will handle the authorization process for our connected app plugins.
        FinancialAidApiService.AuthorizationRequest authRequest = new FinancialAidApiService.AuthorizationRequest(userId, connectedAppId);

        FinancialAidApiService.Response authResponse = FinancialAidApiService.Instance.authorizeUserForApi(authRequest);

        return authResponse.isSuccessful();
    }
}