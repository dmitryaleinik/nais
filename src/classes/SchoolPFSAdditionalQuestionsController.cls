/**
 *    7.21.2016  SFP-605 Adapted from SchoolPFSAdditionalInfoController [jb]
 */
public with sharing class SchoolPFSAdditionalQuestionsController  implements SchoolAcademicYearSelectorStudInterface {

    // input
    private String schoolPFSAssignmentId;

    /*Properties*/

    // to pass to component academic year selector
    public Student_Folder__c folder {get; set;}
    public SchoolPFSAdditionalQuestionsController thisController {get {return this;}}
    public Map<String,Boolean> customQuestionsEnabledForAcademicYear {get; set; }
    // output
    public School_PFS_Assignment__c schoolPFSAssignment {get; private set;}
    public PFS__c pfs {get; private set;}
    public Question_Labels__c questionLabels {get; set;}    

    public String errorMessage {get; private set;}    // to verify in test classes
    
    public Boolean isEditableYear {
        get{
            return GlobalVariables.isEditableYear(schoolPFSAssignment.Academic_Year_Picklist__c);
        }
        set;
    }

    private Id academicYearId;
    private Id schoolId; 

    public SchoolPFSAdditionalQuestionsController() {
        try{
            schoolPFSAssignmentId = ApexPages.currentPage().getParameters().get('schoolPFSAssignmentId');
            loadSchoolPFSAssignment();
            loadPFS();
            // load the academic year from the name on the PFS assignment
            academicYearId = GlobalVariables.getAcademicYearByName(schoolPFSAssignment.Academic_Year_Picklist__c).Id;
            schoolId = schoolPFSAssignment.School__c;

            customQuestionsEnabledForAcademicYear = AnnualSettingHelper.getCustomQuestionsEnabledForYear(new Set<String>{ schoolId }, academicYearId);
            questionLabels = AnnualSettingHelper.getCustomQuestionLabelsForYear(academicYearId, 'en_US');
        } catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }

/*Helper Methods*/
     
    private void loadSchoolPFSAssignment() {
        
        // init
        
        if (schoolPFSAssignmentId != null) {    
            List <School_PFS_Assignment__c> spfsaList = [select Id, Name,
                Academic_Year_Picklist__c,
                Applicant__r.PFS__c, 
                Student_Folder__c,
                School__c
            from School_PFS_Assignment__c
            where Id = :schoolPFSAssignmentId];
            
            if (spfsaList.size() > 0) {                
                schoolPFSAssignment = spfsaList[0];
            }
        }
    }

    private void loadLeftNav() {
        
        if (schoolPFSAssignment != null && schoolPFSAssignment.Student_Folder__c != null) {
            folder = GlobalVariables.getFolderAndSchoolPFSAssignments(schoolPFSAssignment.Student_Folder__c);
        }
    }

    private void loadPFS() {
    
        if (schoolPFSAssignment != null && schoolPFSAssignment.Applicant__r.PFS__c != null) {    
            Id tempPFSId = schoolPFSAssignment.Applicant__r.PFS__c;
            
            // SFP-605 Reduce number of fields selected [8.8.16 jB]                        
            String pfsQueryString = 'SELECT ' + AnnualSettingHelper.customQuestionsResponseQueryString + ' from PFS__c where Id = :tempPFSId';

            List <PFS__c> pfsList = Database.query(pfsQueryString);
            
            if (pfsList.size() > 0) {
                pfs = pfsList[0];
            }
        }
    }



        /*SchoolAcademicYearSelectorStudInterface methods*/
    
    public PageReference SchoolAcademicYearSelectorStudent_OnChange(PageReference newPR) {

        return newPR;
    }
    
    /*End SchoolAcademicYearSelectorStudInterface methods*/
}