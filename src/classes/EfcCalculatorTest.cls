@isTest
private class EfcCalculatorTest {
    // NOTE: these methods are called from testMethods inside the EfcCalculationSteps class

    /* Test Calculation Steps */

    @isTest
    private static void testCalcStepTaxableIncome() {
        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        worksheet.familyStatus = EfcPicklistValues.FAMILY_STATUS_2_PARENT;
        Decimal a = worksheet.salaryWagesParentA = 100000;
        Decimal b = worksheet.salaryWagesParentB = 50000;
        Decimal c = worksheet.dividendIncome = 1000;
        Decimal d = worksheet.interestIncome = 2000;
        Decimal e = worksheet.alimonyReceived = 10000;

        // NAIS-1011
        // NAIS-2500 [DP] 09.29.2015 net profit share is now set explicitly (f), not calculated (f1 - f2)
        Decimal f = worksheet.netProfitLossBusinessFarm = 20000;
        //Decimal f1 = worksheet.businessTotalIncome = 30000;
        //Decimal f2 = worksheet.businessTotalExpenses = 10000;

        Decimal g = worksheet.taxableRefunds = 100;
        Decimal h = worksheet.capitalGainLoss = 200;
        Decimal i = worksheet.otherGainsLosses = 300;
        Decimal j = worksheet.iraDistributions = 400;
        Decimal k = worksheet.pensionsAndAnnuities = 500;
        Decimal l = worksheet.rentalRealEstateTrustsEtc = 600;
        Decimal m = worksheet.unemploymentCompensation = 700;
        Decimal n = worksheet.socialSecurityBenefitsTaxable = 800;
        Decimal o = worksheet.otherIncome = 900;
        Decimal p = worksheet.untaxedIraPlanPayment = 2000;
        Decimal q = worksheet.sepSimpleQualifiedPlans = 3000;
        Decimal s = worksheet.deductiblePartOfSelfEmploymentTax = 6000;
        Decimal t = worksheet.adjustmentsToIncome = 12000;

        // do the calculation
        EfcCalculationSteps.TaxableIncome calcStep = new EfcCalculationSteps.TaxableIncome();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        System.assertEquals(c+d, worksheet.dividendInterestIncome);
        System.assertEquals(g+h+i+j+k+l+m+n+o, worksheet.otherTaxableIncome);
        System.assertEquals(t-(q+p+s), worksheet.otherAdjustmentsToIncome);

        System.assertEquals(
                a+b+worksheet.dividendInterestIncome+e
                        + f // NAIS-1011
                        +worksheet.otherTaxableIncome-worksheet.adjustmentsToIncome,
                worksheet.totalTaxableIncome
        );
    }

    @isTest
    private static void testCalcStepNonTaxableIncome() {
        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        Decimal a = worksheet.childSupportReceived = 100;
        Decimal b = worksheet.socialSecurityBenefits = 200;
        Decimal d = worksheet.untaxedIraPlanPayment = 400;
        Decimal e = worksheet.sepSimpleQualifiedPlans = 500;
        Decimal f = worksheet.pjMinimumIncomeAmount = 600;
        Decimal g = worksheet.depreciationSec179ExpSchC = 700;
        Decimal h = worksheet.depreciationScheduleE = 800;
        Decimal i = worksheet.depreciationSec179ExpSchF = 900;
        Decimal j = worksheet.depreciationSec179Exp4562 = 1000;
        Decimal k = worksheet.homeBusinessExpenseScheduleC = 1100;
        Decimal l = worksheet.preTaxPaymentsToRetirementPlans = 100;
        Decimal m = worksheet.preTaxFringeBenefitContribution = 200;
        Decimal n = worksheet.cashSupportGiftIncome = 300;
        Decimal o = worksheet.otherSupportFromDivorcedSpouse = 400;
        Decimal p = worksheet.foodHousingOtherAllowance = 500;
        Decimal q = worksheet.earnedIncomeCredits = 600;
        Decimal r = worksheet.welfareVeteransAndWorkersComp = 700;
        Decimal s = worksheet.taxExemptInvestmentIncome = 800;
        Decimal t = worksheet.incomeEarnedAbroad = 900;
        worksheet.familyStatus = EfcPicklistValues.FAMILY_STATUS_2_PARENT;
        worksheet.pjApplyMinIncomeToNonTaxableIncome = true;
        Decimal u = worksheet.otherUntaxedIncome = 1000;
        Decimal v = worksheet.salaryWagesParentA = 1000;
        Decimal w = worksheet.salaryWagesParentB = 0;
        Decimal x = worksheet.pjMinimumIncomeAmount = 123;
        worksheet.pjAddDeprecHomeBusExpense = false;

        // do the calculation
        EfcCalculationSteps.NonTaxableIncome calcStep = new EfcCalculationSteps.NonTaxableIncome();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        System.assertEquals(l+m+n+o+p+q+r+s+t+u, worksheet.otherNonTaxableIncome);
        System.assertEquals(worksheet.pjMinimumIncomeAmount, worksheet.pjMinimumIncomeImputed);
        System.assertEquals(k+g+h+i-j, worksheet.depreciationHomeBusinessExpense);
        System.assertEquals(
                a+b+worksheet.otherNonTaxableIncome+d+e+worksheet.pjMinimumIncomeImputed+worksheet.depreciationHomeBusinessExpense,
                worksheet.totalNonTaxableIncome);
    }

    @isTest
    private static void testCalcStepTotalIncome() {
        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        Decimal a = worksheet.totalTaxableIncome = 100;
        Decimal b = worksheet.totalNonTaxableIncome = 200;

        // do the calculation
        EfcCalculationSteps.TotalIncome calcStep = new EfcCalculationSteps.TotalIncome();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        System.assertEquals(a+b, worksheet.totalIncome);
    }

    @isTest
    private static void testCalcStepTotalAllowances() {
        // create a test academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        worksheet.academicYearId = academicYear.Id;
        worksheet.familyStatus = EfcPicklistValues.FAMILY_STATUS_2_PARENT;
        worksheet.filingStatus = EfcPicklistValues.FILING_STATUS_MARRIED_JOINT;
        // worksheet.businessFarmOwner = EfcPicklistValues.BUSINESS_FARM_OWNER_BOTH; NAIS-2390 [CH] Updating for new logic
        worksheet.parentState = 'Alabama';
        worksheet.parentCountry = 'United States';
        Decimal a = worksheet.totalTaxableIncome = 100000;
        Decimal b = worksheet.incomeTaxExemptions = 2;
        Decimal c = worksheet.itemizedDeductions = 20000; // greater than std deduction
        Decimal d = worksheet.totalIncome = 200000;
        Decimal e = worksheet.salaryWagesParentA = 100000;
        Decimal f = worksheet.salaryWagesParentB = 50000;
        Decimal g = worksheet.selfEmploymentTaxPaid = 10000;
        Decimal h = worksheet.deductiblePartOfSelfEmploymentTax = 5000;
        Decimal i = worksheet.netProfitLossBusinessFarm = 50000;
        Decimal j = worksheet.medicalDentalExpenses = 15000;
        Decimal k = worksheet.unusualExpenses = 10000;

        // set up the test tables
        TestUtils.createSSSConstants(academicYear.Id, true);
        TableTestData.populateAllEfcTables(academicYear.Id);

        List<State_Tax_Table__c> testTaxTables = [Select Id, State__c, Percent_of_Total_Income__c from State_Tax_Table__c where State__c = 'Mexico'];
        for (State_Tax_Table__c stt : testTaxTables){
            stt.Percent_of_Total_Income__c += 0.5;
        }
        update testTaxTables;

        // do the calculation
        EfcCalculationSteps.TotalAllowances calcStep = new EfcCalculationSteps.TotalAllowances();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the federal tax
        Decimal federalTax = TableFederalIncomeTax.calculateTax(
                worksheet.academicYearId,
                worksheet.filingStatus,
                a - b*5000 - c, // total taxable income - exemptions - deductions
                worksheet.filingStatusParentB
        );
        System.assertEquals(federalTax, worksheet.federalIncomeTaxCalculated);

        // verify social security
        Decimal socialSecurityAllowance = e*0.042 + f*0.042;
        System.assertEquals(socialSecurityAllowance, worksheet.socialSecurityTaxAllowanceCalculated);

        // verify medicare
        Decimal medicareAllowance = e*0.10 + f*0.10;
        System.assertEquals(medicareAllowance, worksheet.medicareTaxAllowanceCalculated);

        // verify self employment tax
        Decimal selfEmploymentAllowance = g-h;
        System.assertEquals(selfEmploymentAllowance, worksheet.selfEmploymentTaxAllowanceRemainder);

        // verify the state tax
        Decimal stateTax = TableStateIncomeTax.calculateTax(worksheet.academicYearId,worksheet.parentState,null, d);

        // System.assertEquals(stateTax, worksheet.stateOtherTaxAllowanceCalculated);
        System.assertEquals(stateTax, 12000);

        stateTax = 0;
        stateTax = TableStateIncomeTax.calculateTax(worksheet.academicYearId,null,'Canada', d);
        System.assertEquals(stateTax, 12000);

        stateTax = 0;
        stateTax = TableStateIncomeTax.calculateTax(worksheet.academicYearId,null,'Mexico', d);
        System.assertNotEquals(stateTax, 12000);

        stateTax = 0;
        stateTax = TableStateIncomeTax.calculateTax(worksheet.academicYearId,null,'Zimbabwe', d);
        System.assertEquals(stateTax, 12000);

        // verify employment allowance
        Decimal employmentAllowance = TableEmploymentAllowance.calculateAllowance(worksheet.academicYearId, f+i);
        System.assertEquals(employmentAllowance, worksheet.employmentAllowance);

        // NAIS-2390 [CH] Adding check for new two parent logic
        worksheet.businessFarmOwner = EfcPicklistValues.BUSINESS_FARM_OWNER_BOTH;

        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});
        employmentAllowance = TableEmploymentAllowance.calculateAllowance(worksheet.academicYearId, f+i);
        System.assertEquals(EfcConstants.getEmploymentAllowanceMaximum(worksheet.academicYearId), worksheet.employmentAllowance);

        // verify medical allowance
        Decimal medicalAllowance = j - d*0.05;
        System.assertEquals(medicalAllowance, worksheet.medicalAllowance);

        // verify total allowances
        Decimal totalAllowances = federalTax + socialSecurityAllowance + medicareAllowance
                                    + selfEmploymentAllowance + stateTax + employmentAllowance
                                    + medicalAllowance + k;
        System.assertEquals(totalAllowances, worksheet.totalAllowances);

        SSS_Constants__c sssConstants = EfcConstants.getSssConstantsForYear(worksheet.academicYearId);
        sssConstants.Medicare_Tax_Rate__c = 1;
        sssConstants.Default_COLA_Value__c = 2;
        sssConstants.IPA_For_Each_Additional__c = 3;
        sssConstants.Percentage_for_Imputing_Assets__c = 4;
        sssConstants.Medicare_Threshold_Rate_Married_Joint__c = 5;
        update sssConstants;

        System.assertEquals(1, EfcConstants.getMedicareTaxRate(worksheet.academicYearId));
        System.assertEquals(2, EfcConstants.getDefaultColaValue(worksheet.academicYearId));
        System.assertEquals(3, EfcConstants.getIpaForEachAdditional(worksheet.academicYearId));
        System.assertEquals(4, EfcConstants.getPercentageForImputingAssets(worksheet.academicYearId));
        System.assertEquals(5, EfcConstants.getMedicareThresholdRateForFilingStatus(
                                                                                    worksheet.academicYearId,
                                                                                    EfcPicklistValues.FILING_STATUS_MARRIED_JOINT,
                                                                                    null));
    }

    @isTest
    private static void testCalcStepEffectiveIncome() {
        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        Decimal a = worksheet.totalIncome = 200000;
        Decimal b = worksheet.totalAllowances = 15000;

        // do the calculation
        EfcCalculationSteps.EffectiveIncome calcStep = new EfcCalculationSteps.EffectiveIncome();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        System.assertEquals(a-b, worksheet.effectiveIncome);
    }

    @isTest
    private static void testCalcStepHomeEquity() {
        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        Decimal a = worksheet.homeMarketValue = 100000;
        Decimal b = worksheet.unpaidPrincipal1stMortgage = 20000;
        Decimal c = worksheet.unpaidPrincipal2ndMortgage = 10000;
        Decimal d = worksheet.totalIncome = 15000;
        worksheet.ownHome = true;

        // do the calculation
        EfcCalculationSteps.HomeEquity calcStep = new EfcCalculationSteps.HomeEquity();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        System.assertEquals(a-(b+c), worksheet.homeEquityUncapped);
        System.assertEquals(3*d, worksheet.homeEquityCap);
        System.assertEquals(worksheet.homeEquityCap, worksheet.homeEquity);
    }

    @isTest
    private static void testCalcStepNetWorth() {
        // create a test academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        // create table data
        TableTestData.populateBusinessFarmShareData(academicYear.Id);

        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        worksheet.academicYearId = academicYear.Id;
        worksheet.businessFarmOwner = 'Parent A';
        Decimal a = worksheet.homeEquity = 200000;
        Decimal b = worksheet.otherRealEstateMarketValue = 100000;
        Decimal c = worksheet.otherRealEstateUnpaidMortgagePrincipal = 10000;
        List<Decimal> d = worksheet.businessFarmAssetsList = new List<Decimal>{100000};
        List<Decimal> e = worksheet.businessFarmDebtsList = new List<Decimal>{20000};
        List<Decimal> f = worksheet.businessFarmPercentOwnershipList = new List<Decimal>{50};
        Decimal g = worksheet.bankAccounts = 30000;
        Decimal h = worksheet.otherInvestments = 40000;
        Decimal i = worksheet.totalDebts = 50000;
        Decimal j = worksheet.interestIncome = 20000;
        Decimal k = worksheet.dividendIncome = 10000;
        Decimal l = worksheet.dividendInterestIncome = 30000;

        // do the calculation
        EfcCalculationSteps.NetWorth calcStep = new EfcCalculationSteps.NetWorth();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        System.assertEquals(b-c, worksheet.otherRealEstateEquity);
        System.assertEquals((d[0]-e[0])*f[0]/100, worksheet.valueOfBusinessFarm);
        System.assertEquals(TableBusinessFarmShare.calculateBusinessFarmShare(academicYear.Id, worksheet.valueOfBusinessFarm), worksheet.businessFarmShare);
        System.assertEquals(a+worksheet.otherRealEstateEquity+worksheet.businessFarmShare+g+h, worksheet.totalAssets);
        System.assertEquals(worksheet.totalAssets - i, worksheet.netWorth);

        // test imputing assets
        worksheet.pjUseDividendInterestIncomeToImputeAssets = true;
        worksheet.pjPercentageForImputingAssets = 5;

        // do the calculation
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        System.assertEquals(((j+k)/(worksheet.pjPercentageForImputingAssets/100)).Round(RoundingMode.HALF_UP), worksheet.imputedAssets);
    }

    @isTest
    private static void testCalcStepDiscretionaryNetWorth() {
        // create a test academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        // create table data
        TableTestData.populateRetirementAllowanceData(academicYear.Id);

        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        worksheet.academicYearId = academicYear.Id;
        worksheet.familyStatus = EfcPicklistValues.FAMILY_STATUS_2_PARENT;
        Decimal a = worksheet.netWorth = 200000;
        Integer b = worksheet.ageParentA = 30;
        Integer c = worksheet.ageParentB = 35;

        // do the calculation
        EfcCalculationSteps.DiscretionaryNetWorth calcStep = new EfcCalculationSteps.DiscretionaryNetWorth();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        TableRetirementAllowance.AllowanceData allowanceData = TableRetirementAllowance.getRetirementAllowance(academicYear.Id, EfcPicklistValues.FAMILY_STATUS_2_PARENT, c);
        System.assertEquals(allowanceData.allowance, worksheet.retirementAllowance);
        System.assertEquals(allowanceData.conversionCoefficient, worksheet.conversionCoefficient);
        System.assertEquals(a-worksheet.retirementAllowance, worksheet.discretionaryNetWorth);
    }

    @isTest
    private static void testCalcStepIncomeSupplement() {
        // create a test academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        // create table data
        TableTestData.populateAssetProgressivityData(academicYear.Id);

        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        worksheet.academicYearId = academicYear.Id;
        Decimal a = worksheet.discretionaryNetWorth = 200000;
        Decimal b = worksheet.conversionCoefficient = .5;

        // do the calculation
        EfcCalculationSteps.IncomeSupplement calcStep = new EfcCalculationSteps.IncomeSupplement();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        System.assertEquals(TableAssetProgressivity.calculateIncomeSupplement(academicYear.Id, a, b), worksheet.incomeSupplement);
    }

    @isTest
    private static void testCalcStepAdjustedEffectiveIncome() {
        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        Decimal a = worksheet.effectiveIncome = 100000;
        Decimal b = worksheet.incomeSupplement = 20000;

        // do the calculation
        EfcCalculationSteps.AdjustedEffectiveIncome calcStep = new EfcCalculationSteps.AdjustedEffectiveIncome();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        System.assertEquals(a+b, worksheet.adjustedEffectiveIncome);
    }

    @isTest
    private static void testCalcStepRevisedAdjustedEffectiveIncome() {
        // create a test academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        // create SSS constants
        TestUtils.createSSSConstants(academicYear.Id, true);

        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        worksheet.academicYearId = academicYear.Id;
        worksheet.pjUseCostOfLivingAdjustment = true;
        Decimal a = worksheet.adjustedEffectiveIncome = 200000;
        Decimal b = worksheet.effectiveIncome = 400000;
        Decimal c = worksheet.pjOverrideDefaultColaValue = 1.25;


        // do the calculation
        EfcCalculationSteps.RevisedAdjustedEffectiveIncome calcStep = new EfcCalculationSteps.RevisedAdjustedEffectiveIncome();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        System.assertEquals(worksheet.pjOverrideDefaultColaValue, worksheet.pjColaValue);
        System.assertEquals(a/c, worksheet.revisedAdjustedEffectiveIncome);
        System.assertEquals(b/c, worksheet.revisedEffectiveIncome);
    }

    @isTest
    private static void testCalcStepIncomeProtectionAllowance() {
        // create a test academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        // create table data
        TableTestData.populateIncomeProtectionAllowanceData(academicYear.Id);

        // create SSS Constants
        TestUtils.createSSSConstants(academicYear.Id, true);

        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        worksheet.calculateFamilySize = true; // [SL] NAIS-1007
        worksheet.numDependentChildren = 2; // [SL] NAIS-1007
        worksheet.familyStatus = EfcPicklistValues.FAMILY_STATUS_2_PARENT; // [SL] NAIS-1007
        worksheet.academicYearId = academicYear.Id;
        // Decimal a = worksheet.familySize = 5; // [SL] NAIS-1007

        // do the calculation
        EfcCalculationSteps.IncomeProtectionAllowance calcStep = new EfcCalculationSteps.IncomeProtectionAllowance();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        System.assertEquals(5, worksheet.familySize);
        System.assertEquals(TableIncomeProtectionAllowance.getIncomeProtectionAllowance(worksheet)[2], worksheet.incomeProtectionAllowance);
        // System.assertEquals(TableIncomeProtectionAllowance.getIncomeProtectionAllowance(academicYear.Id, worksheet.familySize), worksheet.incomeProtectionAllowance);
    }

    // [CH] NAIS-1885 Test Adjusting the housing portion
    @isTest
    private static void testCalcStepIPAAdjustHousing() {
        // create a test academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        // create table data
        TableTestData.populateIncomeProtectionAllowanceData(academicYear.Id);
        // create SSS Constants
        TestUtils.createSSSConstants(academicYear.Id, true);

        // Create an SPA record
        School_PFS_Assignment__c spaRecord = EfcTestData.createTestSchoolPfsAssignment(false, academicYear);
        spaRecord.Academic_Year_Picklist__c = academicYear.Name;
        spaRecord.Adjust_Housing_Portion_of_IPA__c = 'Yes';
        spaRecord.IPA_Housing_Family_of_4__c  = 4000;
        insert spaRecord;

        // set up the data
        EfcWorksheetData worksheet = EfcDataManager.createEfcWorksheetDataForSchoolPFSAssignment(spaRecord.Id, academicYear);

        // do the calculation for a Family Size of 4
        EfcCalculationSteps.IncomeProtectionAllowance calcStep = new EfcCalculationSteps.IncomeProtectionAllowance();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        EfcDataManager.mapEfcDataToSchoolPfsAssignment(worksheet, spaRecord);

        System.assertEquals(4800, spaRecord.Orig_Income_Protection_Allowance_Housing__c );
        System.assertEquals(28751, spaRecord.Income_Protection_Allowance_Other__c );
        System.assertEquals(33551, spaRecord.Income_Protection_Allowance__c );

        // Run the test for a family size of 8
        worksheet.familySize = 8;
        EfcCalculationSteps.IncomeProtectionAllowance calcStepSize8 = new EfcCalculationSteps.IncomeProtectionAllowance();
        calcStepSize8.doCalculation(new List<EfcWorksheetData> {worksheet});

        EfcDataManager.mapEfcDataToSchoolPfsAssignment(worksheet, spaRecord);

        System.assertEquals(7000, spaRecord.Orig_Income_Protection_Allowance_Housing__c );
        System.assertEquals(41927, spaRecord.Income_Protection_Allowance_Other__c );
        System.assertEquals(48927, spaRecord.Income_Protection_Allowance__c );
    }

    @isTest
    private static void testCalcStepDiscretionaryIncome() {
        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        Decimal a = worksheet.incomeProtectionAllowance = 1000;
        Decimal b = worksheet.revisedAdjustedEffectiveIncome = 100000;
        Decimal c = worksheet.revisedEffectiveIncome = 120000;

        // do the calculation
        EfcCalculationSteps.DiscretionaryIncome calcStep = new EfcCalculationSteps.DiscretionaryIncome();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        System.assertEquals(b-a, worksheet.discretionaryIncome);
        System.assertEquals(c-a, worksheet.discretionaryIncomeNoIncomeSupplement);
    }

    @isTest
    private static void calcStepEstimatedParentalContribution_academicYear20162017_daySchool_expectFoodAllowanceSubtracted() {
        // create a test academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear('2016-2017', true);

        // create SSS constants
        TestUtils.createSSSConstants(academicYear.Id, true);

        // create table data
        TableTestData.populateEstimatedContributionRateData(academicYear.Id);

        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        worksheet.academicYearId = academicYear.Id;
        Decimal discretionaryIncome = worksheet.discretionaryIncome = 200000;
        Decimal incomeWithoutSupplement = worksheet.discretionaryIncomeNoIncomeSupplement = 100000;
        Decimal totalIncome = worksheet.totalIncome = 1000000;

        // We are going to mock the source applicant and main applicant Ids by just using an academic year Id.
        // This is just to avoid inserting several other records unneccessarily.
        Id mockSourceApplicantId = academicYear.Id;
        worksheet.MainApplicantId = mockSourceApplicantId;
        worksheet.applicants[0].sourceApplicantId = mockSourceApplicantId;
        worksheet.applicants[0].dayOrBoarding = EfcPicklistValues.DAYBOARDING_DAY;

        // do the calculation
        EfcCalculationSteps.EstimatedParentalContribution calcStep = new EfcCalculationSteps.EstimatedParentalContribution();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        Decimal boardingFoodAllowance = EfcConstants.getBoardingSchoolFoodAllowance(academicYear.Id);
        System.assertEquals(
                TableEstimatedContributionRate.getEstimatedContributionRate(academicYear.Id, discretionaryIncome) - boardingFoodAllowance,
                worksheet.estimatedParentalContribution,
                'Expected the parentl contribution to not include the food allowance.'
        );
        System.assertEquals(
                TableEstimatedContributionRate.getEstimatedContributionRate(academicYear.Id, incomeWithoutSupplement) - boardingFoodAllowance,
                worksheet.contribFromIncome,
                'Expected the parentl contribution to not include the food allowance.'
        );
        System.assertEquals((worksheet.estimatedParentalContribution/totalIncome*100).Round(RoundingMode.HALF_UP), worksheet.contribPercentTotalIncome);
        System.assertEquals((worksheet.estimatedParentalContribution/discretionaryIncome*100).Round(RoundingMode.HALF_UP), worksheet.contribPercentDiscretionaryIncome);
        System.assertEquals(worksheet.estimatedParentalContribution-worksheet.contribFromIncome, worksheet.contribFromAssets);
    }

    @isTest
    private static void calcStepEstimatedParentalContribution_academicYear20162017_boardingSchool_expectFoodAllowanceSubtracted() {
        // create a test academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear('2016-2017', true);

        // create SSS constants
        TestUtils.createSSSConstants(academicYear.Id, true);

        // create table data
        TableTestData.populateEstimatedContributionRateData(academicYear.Id);

        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        worksheet.academicYearId = academicYear.Id;
        Decimal discretionaryIncome = worksheet.discretionaryIncome = 200000;
        Decimal incomeWithoutSupplement = worksheet.discretionaryIncomeNoIncomeSupplement = 100000;
        Decimal totalIncome = worksheet.totalIncome = 1000000;

        // We are going to mock the source applicant and main applicant Ids by just using an academic year Id.
        // This is just to avoid inserting several other records unneccessarily.
        Id mockSourceApplicantId = academicYear.Id;
        worksheet.MainApplicantId = mockSourceApplicantId;
        worksheet.applicants[0].sourceApplicantId = mockSourceApplicantId;
        worksheet.applicants[0].dayOrBoarding = EfcPicklistValues.DAYBOARDING_BOARDING;

        // do the calculation
        EfcCalculationSteps.EstimatedParentalContribution calcStep = new EfcCalculationSteps.EstimatedParentalContribution();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        Decimal boardingFoodAllowance = EfcConstants.getBoardingSchoolFoodAllowance(academicYear.Id);
        System.assertEquals(
                TableEstimatedContributionRate.getEstimatedContributionRate(academicYear.Id, discretionaryIncome) - boardingFoodAllowance,
                worksheet.estimatedParentalContribution,
                'Expected the parental contribution to not include the boarding school food allowance.'
        );
        System.assertEquals(
                TableEstimatedContributionRate.getEstimatedContributionRate(academicYear.Id, incomeWithoutSupplement) - boardingFoodAllowance,
                worksheet.contribFromIncome,
                'Expected the parental contribution to not include the boarding school food allowance.'
        );
        System.assertEquals((worksheet.estimatedParentalContribution/totalIncome*100).Round(RoundingMode.HALF_UP), worksheet.contribPercentTotalIncome);
        System.assertEquals((worksheet.estimatedParentalContribution/discretionaryIncome*100).Round(RoundingMode.HALF_UP), worksheet.contribPercentDiscretionaryIncome);
        System.assertEquals(worksheet.estimatedParentalContribution-worksheet.contribFromIncome, worksheet.contribFromAssets);
    }

    @isTest
    private static void calcStepEstimatedParentalContribution_academicYear20172018_daySchool_expectFoodAllowanceNotIncluded() {
        // create a test academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear('2017-2018', true);

        // create SSS constants
        TestUtils.createSSSConstants(academicYear.Id, true);

        // create table data
        TableTestData.populateEstimatedContributionRateData(academicYear.Id);

        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        worksheet.academicYearId = academicYear.Id;
        Decimal discretionaryIncome = worksheet.discretionaryIncome = 200000;
        Decimal incomeWithoutSupplement = worksheet.discretionaryIncomeNoIncomeSupplement = 100000;
        Decimal totalIncome = worksheet.totalIncome = 1000000;

        // We are going to mock the source applicant and main applicant Ids by just using an academic year Id.
        // This is just to avoid inserting several other records unneccessarily.
        Id mockSourceApplicantId = academicYear.Id;
        worksheet.MainApplicantId = mockSourceApplicantId;
        worksheet.applicants[0].sourceApplicantId = mockSourceApplicantId;
        worksheet.applicants[0].dayOrBoarding = EfcPicklistValues.DAYBOARDING_DAY;

        // do the calculation
        EfcCalculationSteps.EstimatedParentalContribution calcStep = new EfcCalculationSteps.EstimatedParentalContribution();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        Decimal boardingFoodAllowance = EfcConstants.getBoardingSchoolFoodAllowance(academicYear.Id);
        System.assertEquals(
                TableEstimatedContributionRate.getEstimatedContributionRate(academicYear.Id, discretionaryIncome),
                worksheet.estimatedParentalContribution,
                'Expected the parentl contribution to not include the food allowance.'
        );
        System.assertEquals(
                TableEstimatedContributionRate.getEstimatedContributionRate(academicYear.Id, incomeWithoutSupplement),
                worksheet.contribFromIncome,
                'Expected the parentl contribution to not include the food allowance.'
        );
        System.assertEquals((worksheet.estimatedParentalContribution/totalIncome*100).Round(RoundingMode.HALF_UP), worksheet.contribPercentTotalIncome);
        System.assertEquals((worksheet.estimatedParentalContribution/discretionaryIncome*100).Round(RoundingMode.HALF_UP), worksheet.contribPercentDiscretionaryIncome);
        System.assertEquals(worksheet.estimatedParentalContribution-worksheet.contribFromIncome, worksheet.contribFromAssets);
    }

    @isTest
    private static void calcStepEstimatedParentalContribution_academicYear20172018_boardingSchool_expectFoodAllowanceIncluded() {
        // create a test academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear('2017-2018', true);

        // create SSS constants
        TestUtils.createSSSConstants(academicYear.Id, true);

        // create table data
        TableTestData.populateEstimatedContributionRateData(academicYear.Id);

        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        worksheet.academicYearId = academicYear.Id;
        Decimal discretionaryIncome = worksheet.discretionaryIncome = 200000;
        Decimal incomeWithoutSupplement = worksheet.discretionaryIncomeNoIncomeSupplement = 100000;
        Decimal totalIncome = worksheet.totalIncome = 1000000;

        // We are going to mock the source applicant and main applicant Ids by just using an academic year Id.
        // This is just to avoid inserting several other records unneccessarily.
        Id mockSourceApplicantId = academicYear.Id;
        worksheet.MainApplicantId = mockSourceApplicantId;
        worksheet.applicants[0].sourceApplicantId = mockSourceApplicantId;
        worksheet.applicants[0].dayOrBoarding = EfcPicklistValues.DAYBOARDING_BOARDING;

        // do the calculation
        EfcCalculationSteps.EstimatedParentalContribution calcStep = new EfcCalculationSteps.EstimatedParentalContribution();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        Decimal boardingFoodAllowance = EfcConstants.getBoardingSchoolFoodAllowance(academicYear.Id);
        System.assertEquals(
                TableEstimatedContributionRate.getEstimatedContributionRate(academicYear.Id, discretionaryIncome) + boardingFoodAllowance,
                worksheet.estimatedParentalContribution,
                'Expected the parental contribution to include the boarding school food allowance.'
        );
        System.assertEquals(
                TableEstimatedContributionRate.getEstimatedContributionRate(academicYear.Id, incomeWithoutSupplement) + boardingFoodAllowance,
                worksheet.contribFromIncome,
                'Expected the parental contribution to include the boarding school food allowance.'
        );
        System.assertEquals((worksheet.estimatedParentalContribution/totalIncome*100).Round(RoundingMode.HALF_UP), worksheet.contribPercentTotalIncome);
        System.assertEquals((worksheet.estimatedParentalContribution/discretionaryIncome*100).Round(RoundingMode.HALF_UP), worksheet.contribPercentDiscretionaryIncome);
        System.assertEquals(worksheet.estimatedParentalContribution-worksheet.contribFromIncome, worksheet.contribFromAssets);
    }

    @isTest
    private static void testCalcStepEstimatedParentalContributionPerChild() {
        // create a test academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        // create SSS constants
        TestUtils.createSSSConstants(academicYear.Id, true);

        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        worksheet.academicYearId = academicYear.Id;
        Decimal a = worksheet.estimatedParentalContribution = 100000;
        worksheet.estimatedParentalContributionBoarding = a;
        Decimal b = worksheet.numChildrenInTuitionSchools = 5;

        // do the calculation
        EfcCalculationSteps.EstimatedParentalContributionPerChild calcStep = new EfcCalculationSteps.EstimatedParentalContributionPerChild();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        System.assertEquals(a/b, worksheet.estimatedParentalContributionPerChild);
    }

    @isTest
    private static void testCalcStepStudentAssetContribution() {
        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        EfcWorksheetData.ApplicantData applicant = new EfcWorksheetData.ApplicantData();
        String a = applicant.gradeInEntryYear = '12';
        Decimal b = applicant.assetsTotalValue = 10000;
        worksheet.applicants = new List<EfcWorksheetData.ApplicantData>{applicant};

        // do the calculation
        EfcCalculationSteps.StudentAssetContribution calcStep = new EfcCalculationSteps.StudentAssetContribution();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        System.assertEquals((b/5).Round(RoundingMode.HALF_UP), applicant.studentAssetContribution);
    }

    @isTest
    private static void testCalcStepEstimatedFamilyContribution() {
        // set up the data
        EfcWorksheetData worksheet = new EfcWorksheetData();
        Decimal a = worksheet.estimatedParentalContributionPerChild = 10000;
        Decimal b = worksheet.estimatedParentalContributionPerChildBoarding = 11000;

        EfcWorksheetData.ApplicantData applicant = new EfcWorksheetData.ApplicantData();
        applicant.dayOrBoarding = EfcPicklistValues.DAYBOARDING_DAY;
        Decimal c = applicant.studentAssetContribution = 2000;
        worksheet.applicants = new List<EfcWorksheetData.ApplicantData>{applicant};

        // do the calculation
        EfcCalculationSteps.EstimatedFamilyContribution calcStep = new EfcCalculationSteps.EstimatedFamilyContribution();
        calcStep.doCalculation(new List<EfcWorksheetData> {worksheet});

        // verify the results
        System.assertEquals(a+c, worksheet.applicants[0].efcDay);
        System.assertEquals(b+c, worksheet.applicants[0].efcBoarding);
    }
}