/**
 *    NAIS-69: Document Preview
 *
 *    SL, Exponent Partners, 2013
 */
@isTest
private class PreviewPagePDFControllerTest {

    @isTest static void getPdf_enableSOAPVersion_expectIntanceOfSpringCMDocPreviewerSOAP() {
        
        FeatureToggles.setToggle('SpringCM_REST_Preview_Enabled__c', false);
        
        PreviewPagePDFController controller = new PreviewPagePDFController();
        
        System.assertEquals(true, controller.handler instanceof SpringCMDocPreviewer_SOAP);
    }

    @isTest static void getPdf_enableSOAPVersion_expectIntanceOfSpringCMDocPreviewerREST() {
        
        FeatureToggles.setToggle('SpringCM_REST_Preview_Enabled__c', true);
        
        PreviewPagePDFController controller = new PreviewPagePDFController();
        
        System.assertEquals(true, controller.handler instanceof SpringCMDocPreviewer_REST);
    }
}