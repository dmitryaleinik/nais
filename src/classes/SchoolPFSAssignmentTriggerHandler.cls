/**
 * SchoolPFSAssignmentTriggerHandler.cls
 *
 * @description School_PFS_Assignment__c handler class for divying work to service classes depending on trigger scope, 
 * implements TriggerHandler interface to solve for all trigger scopes.
 *
 * @author Chase Logan @ Presence PG
 */
public class SchoolPFSAssignmentTriggerHandler implements TriggerHandler {
    
    /* Insert Operations */

    /**
     * @description: handles beforeInsert trigger context
     *
     * @param newSObjects - List of new sObjects
     */
    public void beforeInsert( List<sObject> newSObjects) { /* implement future operations */ }
    /**
     * @description: handles afterInsert trigger context
     *
     * @param newSObjects - List of new sObjects
     */
    public void afterInsert( Map<Id,sObject> newSObjects) { /* implement future operations */ }

    
    /* Update Operations */

    /**
     * @description: handles beforeUpdate trigger context
     *
     * @param newSObjects - Map of new sObjects (about to be updated)
     * @param oldSObjects - Map of old sObjects (values about to be replaced)
     */
    public void beforeUpdate( Map<Id,sObject> newSObjects, Map<Id,sObject> oldSObjects) { /* implement future operations */ }

    /**
     * @description: handles afterUpdate trigger context
     *
     * @param newSObjects - Map of new sObjects (about to be updated)
     * @param oldSObjects - Map of old sObjects (values about to be replaced)
     */
    public void afterUpdate( Map<Id,sObject> newSObjects, Map<Id,sObject> oldSObjects) { 

        // ensure single trigger execution per transaction
        if ( !TriggerHelper.getSchoolPFSAssignmentTriggerHasRun()) {

            // generate agg webhook event counts
            TriggerHelper.setSchoolPFSAssignmentTriggerHasRun( true);
            this.calculateAggregateEventCounts( newSObjects);
        }
    }
    
    
    /* Delete Operations */

    /**
     * @description: handles beforeDelete trigger context
     *
     * @param newSObjects - Map of new sObjects (about to be deleted)
     * @param oldSObjects - Map of old sObjects (values about to be replaced)
     */
    public void beforeDelete( Map<Id,sObject> newSObjects, Map<Id,sObject> oldSObjects) { /* implement future operations */ }
    /**
     * @description: handles afterDelete trigger context
     *
     * @param newSObjects - Map of new sObjects (about to be deleted)
     * @param oldSObjects - Map of old sObjects (values about to be replaced)
     */
    public void afterDelete( Map<Id,sObject> newSObjects, Map<Id,sObject> oldSObjects) { /* implement future operations */ }

    
    /* Undelete Operations */

    /**
     * @description: handles afterUnDelete trigger context
     *
     * @param newSObjects - Map of new sObjects (about to be restored)
     * @param oldSObjects - Map of old sObjects (values about to be replaced)
     */
    public void afterUnDelete( Map<Id,sObject> newSObjects, Map<Id,sObject> oldSObjects) { /* implement future operations */ }


    /* private instance methods */

    // calculates aggregate counts for webhook events stored in Mass_Email_Events__c field, 
    // counts are stored in aggregate total fields on Mass_Email_Send__c object
    private void calculateAggregateEventCounts( Map<Id,sObject> newSObjects) {
        
        // early return on invalid data
        if ( newSObjects == null || newSObjects.isEmpty()) return;

        Set<String> massEmailIdSet = new Set<String>();
        Set<String> schoolIdSet = new Set<String>();
        Set<String> academicYearSet = new Set<String>();
        for ( School_PFS_Assignment__c spa : ( List<School_PFS_Assignment__c>)newSObjects.values()) {

            if ( String.isNotBlank( spa.Mass_Email_Events__c)) {

                // Event's are stored as comma separted JSON objects, wrap events in leading and trailing braces
                // to make valid JSON array before deserializing
                String jsonArrayString = '[' + spa.Mass_Email_Events__c + ']';
                List<SendgridEvent> eventList = 
                    ( List<SendgridEvent>)JSON.deserialize( jsonArrayString, List<SendgridEvent>.class);

                for ( SendgridEvent evt : eventList) {

                    if ( !massEmailIdSet.contains( evt.mass_email_send_id)) massEmailIdSet.add( evt.mass_email_send_id);
                    if ( !schoolIdSet.contains( evt.school_id)) schoolIdSet.add( evt.school_id);
                    if ( !academicYearSet.contains( evt.academic_year)) academicYearSet.add( evt.academic_year);
                }
            }
        }

        // must query all related Mass_Email_Send__c records and retrieve comma separated list of original recips
        // from Recipients__c field, then must query all recips SPA's by school and school year to get
        // all applicable event data for each MES record.
        if ( massEmailIdSet.size() > 0) {

            List<Mass_Email_Send__c> massESList = 
                new MassEmailSendDataAccessService().getMassEmailSendByIdSet( massEmailIdSet);

            if ( massESList != null) {

                Set<String> recipIdSet = new Set<String>();
                for ( Mass_Email_Send__c massES : massESList) {

                    if ( String.isNotBlank( massES.Recipients__c)) {

                        List<String> splitList = massES.Recipients__c.split( SchoolMassEmailSendModel.SPLIT_CHAR);
                        for ( String s : splitList) {

                            if ( !recipIdSet.contains( s)) recipIdSet.add( s);
                        }
                    }
                }

                if ( recipIdSet.size() > 0) {

                    List<School_PFS_Assignment__c> spaList = 
                        new SchoolPFSAssignmentDataAccessService().getSchoolPFSByParentAIdSet( 
                            recipIdSet, schoolIdSet, academicYearSet, false);


                    // must iterate back over returned SPA's and grab JSON stored in Mass_Email_Events__c field,
                    // de-dupe by recipient, store counts by MES record, pass counts off for update of MES record(s)
                    // any time one SPA with event data is updated, counts are recalculated for every MES record stored on that SPA
                    if ( spaList != null) {

                        this.updateMassEmailAggCountFields( this.deDupeEventData( spaList));
                    }
                }
            }
        }
    }

    // given a list of SPA's, aggregate and de-dupe all email event data stored in each SPA's Mass_Email_Events__c
    // field by recipient and event, each recipient can only have one event type per MES record
    private Map<String,Map<String,EventDataModel>> deDupeEventData( List<School_PFS_Assignment__c> spaList) {
        Map<String,Map<String,EventDataModel>> returnMap = new Map<String,Map<String,EventDataModel>>();

        // early return on invalid data
        if ( spaList == null || spaList.isEmpty()) return returnMap;

        for ( School_PFS_Assignment__c spa : spaList) {

            if ( String.isNotBlank( spa.Mass_Email_Events__c)) {

                String jsonArrayString = '[' + spa.Mass_Email_Events__c + ']';
                List<SendgridEvent> eventList = 
                    ( List<SendgridEvent>)JSON.deserialize( jsonArrayString, List<SendgridEvent>.class);

                for ( SendgridEvent evt : eventList) {

                    if ( evt.event.equalsIgnoreCase( SchoolMassEmailSendModel.STATUS_OPENED)) {

                        returnMap = this.populateEventDataMap( evt.mass_email_send_id,
                            SchoolMassEmailSendModel.STATUS_OPENED, spa, returnMap);

                    } else if ( evt.event.equalsIgnoreCase( SchoolMassEmailSendModel.STATUS_BOUNCED)) {

                        returnMap = this.populateEventDataMap( evt.mass_email_send_id,
                            SchoolMassEmailSendModel.STATUS_BOUNCED, spa, returnMap);
                    } else if ( evt.event.equalsIgnoreCase( SchoolMassEmailSendModel.STATUS_UNSUB)) {

                        returnMap = this.populateEventDataMap( evt.mass_email_send_id,
                            SchoolMassEmailSendModel.STATUS_UNSUB, spa, returnMap);
                    }
                }
            }
        }

        return returnMap;
    }

    // handles summing and actual updating of applicable MES records agg count fields
    private void updateMassEmailAggCountFields( Map<String,Map<String,EventDataModel>> eventDataByMESIdMap) {

        // early return on invalid data
        if ( eventDataByMESIdMap == null || eventDataByMESIdMap.isEmpty()) return;

        List<Mass_Email_Send__c> massESList = 
            new MassEmailSendDataAccessService().getMassEmailSendByIdSet( eventDataByMESIdMap.keySet());

        if ( massESList == null) return;

        Boolean needsUpdate = false;
        for ( Mass_Email_Send__c massES : massESList) {

            Integer openedCount = 0;
            Integer bouncedCount = 0;
            Integer unsubCount = 0;
            if ( eventDataByMESIdMap.containsKey( massES.Id)) {

                needsUpdate = true;
                Map<String,EventDataModel> tempMap = eventDataByMESIdMap.get( massES.Id);
                for ( EventDataModel eDModel : tempMap.values()) {

                    if ( eDModel.massESId == massES.Id &&
                            eDModel.eventMap.containsKey( SchoolMassEmailSendModel.STATUS_OPENED) &&
                                eDModel.eventMap.get( SchoolMassEmailSendModel.STATUS_OPENED)) {

                        openedCount++;
                    } else if ( eDModel.massESId == massES.Id &&
                                       eDModel.eventMap.containsKey( SchoolMassEmailSendModel.STATUS_BOUNCED) &&
                                        eDModel.eventMap.get( SchoolMassEmailSendModel.STATUS_BOUNCED)) {

                        bouncedCount++;
                    } else if ( eDModel.massESId == massES.Id &&
                                    eDModel.eventMap.containsKey( SchoolMassEmailSendModel.STATUS_UNSUB) &&
                                        eDModel.eventMap.get( SchoolMassEmailSendModel.STATUS_UNSUB)) {

                        unsubCount++;
                    }
                }

                massES.Opened__c = openedCount;
                massES.Bounced__c = bouncedCount;
                massES.Unsubscribed__c = unsubCount;
            }
        }

        // update MES records with new agg counts
        if ( needsUpdate) {

            try {

                update massESList;
            } catch ( Exception e) {

                System.debug( 'Exception occurred in SchoolPFSAssignmentTriggerHandler.updateMassEmailAggCountFields()' + 
                    ' message: ' + e.getMessage() + ' - ' + e.getStackTraceString());
            }
        }
    }

    // helper method for resuable map logic by status
    private Map<String,Map<String,EventDataModel>> populateEventDataMap( String massESId,
                                                        String status, School_PFS_Assignment__c spa, 
                                                            Map<String,Map<String,EventDataModel>> workingMap) {
        // early return on invalid data
        if ( String.isBlank( massESId) || String.isBlank( status) || spa == null || workingMap == null) return null;

        if ( workingMap.containsKey( massESId)) {

            Map<String,EventDataModel> tempMap = workingMap.get( massESId);
            if ( ( !tempMap.containsKey( spa.Applicant__r.PFS__r.Parent_A__c)) || 
                    ( tempMap.containsKey( spa.Applicant__r.PFS__r.Parent_A__c) &&
                        !tempMap.get( spa.Applicant__r.PFS__r.Parent_A__c).eventMap.containsKey( status))) {

                EventDataModel eDModel = new EventDataModel( spa.Applicant__r.PFS__r.Parent_A__c, massESId);
                eDModel.eventMap.put( status, true);
                tempMap.put( spa.Applicant__r.PFS__r.Parent_A__c, eDModel);
            }
        } else {

            Map<String,EventDataModel> tempMap = new Map<String,EventDataModel>();
            EventDataModel eDModel = new EventDataModel( spa.Applicant__r.PFS__r.Parent_A__c, massESId);
            eDModel.eventMap.put( status, true);
            tempMap.put( spa.Applicant__r.PFS__r.Parent_A__c, eDModel);
            workingMap.put( massESId, tempMap);
        }

        return workingMap;
    }

    // Wrapper model class to combine Event and Contact data
    private class EventDataModel {

        public String recipId { get; set; }
        public String massESId { get; set; }
        public Map<String,Boolean> eventMap { get; set; }

        public EventDataModel( String recipId, String massESId) {

            this.recipId = recipId;
            this.massESId = massESId;
            this.eventMap = new Map<String,Boolean>();
        }
    }

}