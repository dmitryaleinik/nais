/**
 * MassEmailSendTriggerHandler.cls
 *
 * @description Mass_Email_Send__c handler class for divying work to service classes depending on trigger scope, 
 * implements TriggerHandler interface to solve for all trigger scopes.
 *
 * @author Chase Logan @ Presence PG
 */
public class MassEmailSendTriggerHandler implements TriggerHandler {
    
    /* Insert Operations */

    /**
     * @description: handles beforeInsert trigger context
     *
     * @param newSObjects - List of new sObjects
     */
    public void beforeInsert( List<sObject> newSObjects) { /* implement future operations */ }
    /**
     * @description: handles afterInsert trigger context
     *
     * @param newSObjects - List of new sObjects
     */
    public void afterInsert( Map<Id,sObject> newSObjects) { 

        // ensure single trigger execution per transaction
        if ( !TriggerHelper.getMassEmailSendTriggerHasRun()) {

            // send emails for valid records, invalid records will be moved to Error status
            TriggerHelper.setMassEmailSendTriggerHasRun( true);
            List<Mass_Email_Send__c> massESResultList = this.prepAndSendEmails( newSObjects, null);

            // update records
            if ( massESResultList != null && massESResultList.size() > 0) {
                update massESResultList;
            }
        }
    }

    /* Update Operations */

    /**
     * @description: handles beforeUpdate trigger context
     *
     * @param newSObjects - Map of new sObjects (about to be updated)
     * @param oldSObjects - Map of old sObjects (values about to be replaced)
     */
    public void beforeUpdate( Map<Id,sObject> newSObjects, Map<Id,sObject> oldSObjects) { /* implement future operations */ }

    /**
     * @description: handles afterUpdate trigger context
     *
     * @param newSObjects - Map of new sObjects (about to be updated)
     * @param oldSObjects - Map of old sObjects (values about to be replaced)
     */
    public void afterUpdate( Map<Id,sObject> newSObjects, Map<Id,sObject> oldSObjects) { 

        // ensure single trigger execution per transaction
        if ( !TriggerHelper.getMassEmailSendTriggerHasRun()) {

            // send emails for valid records, invalid records will be moved to Error status
            TriggerHelper.setMassEmailSendTriggerHasRun( true);
            List<Mass_Email_Send__c> massESResultList = this.prepAndSendEmails( newSObjects, oldSObjects);

            // update records
            if ( massESResultList != null && massESResultList.size() > 0) {
                update massESResultList;
            }
        }
    }
    
    /* Delete Operations */

    /**
     * @description: handles beforeDelete trigger context
     *
     * @param newSObjects - Map of new sObjects (about to be deleted)
     * @param oldSObjects - Map of old sObjects (values about to be replaced)
     */
    public void beforeDelete( Map<Id,sObject> newSObjects, Map<Id,sObject> oldSObjects) { /* implement future operations */ }
    /**
     * @description: handles afterDelete trigger context
     *
     * @param newSObjects - Map of new sObjects (about to be deleted)
     * @param oldSObjects - Map of old sObjects (values about to be replaced)
     */
    public void afterDelete( Map<Id,sObject> newSObjects, Map<Id,sObject> oldSObjects) { /* implement future operations */ }

    /* Undelete Operations */

    /**
     * @description: handles afterUnDelete trigger context
     *
     * @param newSObjects - Map of new sObjects (about to be restored)
     * @param oldSObjects - Map of old sObjects (values about to be replaced)
     */
    public void afterUnDelete( Map<Id,sObject> newSObjects, Map<Id,sObject> oldSObjects) { /* implement future operations */ }


    /* private instance methods specific to Mass_Email_Send__c */

    // validate a Mass_Email_Send__c record has necessary fields populated to generate and send email
    private Boolean validateMSEForSend( Mass_Email_Send__c massES) {
        Boolean returnVal = false;

        if ( massES != null) {
            Boolean valid = true;
            
            // validate recipients
            valid = ( valid && String.isEmpty( massES.Recipients__c) ? false : valid);

            // validate Reply To
            valid = ( valid && String.isEmpty( massES.Reply_To_Address__c) ? false : valid);

            // validate Subject
            valid = ( valid && String.isEmpty( massES.Subject__c) ? false : valid);

            // validate text or HTML body
            valid = ( valid && String.isEmpty( massES.Text_Body__c) && String.isEmpty( massES.Html_Body__c) ? false : valid);

            returnVal = valid;
        }

        return returnVal;
    }

    // validate Mass_Email_Send__c records, trigger email send on valid records, return list of invalid records to be updated immediately
    private List<Mass_Email_Send__c> prepAndSendEmails( Map<Id,sObject> newSObjects, Map<Id,sObject> oldSObjects) {
        List<Mass_Email_Send__c> invalidMassEmailList = new List<Mass_Email_Send__c>();
        List<Id> validIdList = new List<Id>();

        // trigger email send on EMAIL_SEND_STATUS
        for ( Id i : newSObjects.keySet()) {
            
            Mass_Email_Send__c newMassES = ( Mass_Email_Send__c)newSObjects.get( i).clone( true, false, false, false);
            Mass_Email_Send__c oldMassES = ( Trigger.isUpdate ? ( Mass_Email_Send__c)oldSObjects.get( i) : new Mass_Email_Send__c());

            if ( Trigger.isInsert && newMassES.Status__c == EmailService.EMAIL_SEND_STATUS && this.validateMSEForSend( newMassES)) {

                validIdList.add( i);
            } else if ( Trigger.isUpdate && newMassES.Status__c == EmailService.EMAIL_SEND_STATUS && newMassES.Status__c != oldMassES.Status__c &&
                         this.validateMSEForSend( newMassES)) {
                
                validIdList.add( i);
            } else if ( newMassES.Status__c != EmailService.EMAIL_DRAFT_STATUS && 
                            newMassES.Status__c != EmailService.EMAIL_NEW_STATUS && !this.validateMSEForSend( newMassES)) {

                // records are invalid to even attempt send, update them to error status now
                newMassES.Status__c = EmailService.EMAIL_ERR_STATUS;
                newMassES.Error_Message__c = EmailService.EMAIL_INVALID_ERR_MSG;
                invalidMassEmailList.add( newMassES);
            }
        }

        // pass callout of valid records to EmailService
        if ( validIdList.size() > 0) {

            EmailService.getInstance().sendEmails( validIdList);
        }
        
        // list of invalid records to be updated immediately
        return invalidMassEmailList;
    }

}