@isTest
private class SchoolPickerLinkControllerTest
{
    
    @isTest 
    private static void testController()
    {
        Account schoolA = AccountTestData.Instance.asSchool().forName('SchoolA').create();
        Account schoolB = AccountTestData.Instance.asSchool().forName('SchoolB').create();
        insert new List<Account>{schoolA, schoolB};

        Contact schoolUserContact = ContactTestData.Instance.forAccount(schoolA.Id).DefaultContact;
        User schoolUser;
        System.runAs(GlobalVariables.getCurrentUser())
        {
            schoolUser = UserTestData.createSchoolPortalUser();
            schoolUser.In_Focus_School__c = schoolB.Id;
            insert schoolUser;
        }

        Affiliation__c aff = AffiliationTestData.Instance
            .forOrganizationId(schoolB.Id)
            .forStatus('Current')
            .forType('Additional School User').DefaultAffiliation;

        Test.startTest();
            System.runAs(schoolUser)
            {
                SchoolPickerLinkController controller = new SchoolPickerLinkController();
                System.assertEquals(schoolB.Name, controller.currentSchoolName);
                System.assert(controller.displaySchoolPickerLink);
            }
        Test.stopTest();
    }
}