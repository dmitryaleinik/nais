@isTest
private class AccountSelectorTest {
    
    @isTest
    private static void selectByRecordTypeName_accountExists_expectRecordReturned() {
        Account account = AccountTestData.Instance
            .forRecordTypeId(RecordTypes.meansBasedFeeWaiverAccountTypeId).DefaultAccount;

        Test.startTest();
            List<Account> accountRecords = AccountSelector.Instance
                .selectByRecordTypeName(ApplicationFeeSettingsService.MEANS_BASED_FEE_WAIVER_RECORDTYPE);
        Test.stopTest();

        System.assert(!accountRecords.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(1, accountRecords.size(), 'Expected there to be one account record returned.');
        System.assertEquals(account.Id, accountRecords[0].Id, 'Expected the Ids of the account records to match.');
    }

    @isTest
    private static void selectByRecordTypeName_nullSet_expectArgumentNullException() {
        Account account = AccountTestData.Instance
            .forRecordTypeId(RecordTypes.meansBasedFeeWaiverAccountTypeId).DefaultAccount;

        Test.startTest();
            try {
                AccountSelector.Instance.selectByRecordTypeName(null);

                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, AccountSelector.RECORDTYPE_NAME_PARAM);
            }
        Test.stopTest();
    }

    @isTest
    private static void selectByRecordTypeId_accountExists_expectRecordReturned() {
        Account account = AccountTestData.Instance
            .forRecordTypeId(RecordTypes.schoolAccountTypeId).DefaultAccount;

        Test.startTest();
            List<Account> accountRecords = AccountSelector.Instance
                .selectByRecordTypeId(RecordTypes.schoolAccountTypeId);
        Test.stopTest();

        System.assert(!accountRecords.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(1, accountRecords.size(), 'Expected there to be one account record returned.');
        System.assertEquals(account.Id, accountRecords[0].Id, 'Expected the Ids of the account records to match.');
    }

    @isTest
    private static void selectByRecordTypeId_nullSet_expectArgumentNullException() {
        Account account = AccountTestData.Instance
            .forRecordTypeId(RecordTypes.schoolAccountTypeId).DefaultAccount;

        Test.startTest();
            try {
                AccountSelector.Instance.selectByRecordTypeId(null);

                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, AccountSelector.RECORDTYPE_ID);
            }
        Test.stopTest();
    }
    
    @isTest
    private static void selectWithLatestAnnualSetting_annualSettingForPreviousAY_expectLastAcademicYearRecord() {
        
        List<Academic_Year__c> academicYears = AcademicYearTestData.Instance.createAcademicYears(5);
        insert academicYears;
        
        Account school = AccountTestData.Instance
            .forRecordTypeId(RecordTypes.schoolAccountTypeId).DefaultAccount;
            
        List<Annual_Setting__c> annualSettings = new List<Annual_Setting__c>();
        
        annualSettings.add( 
	        AnnualSettingsTestData.Instance
	            .forSchoolId(school.Id)
	            .forAcademicYearId(academicYears[4].Id)
	            .create());
	            
        annualSettings.add( 
            AnnualSettingsTestData.Instance
                .forSchoolId(school.Id)
                .forAcademicYearId(academicYears[3].Id)
                .create());
                
        annualSettings.add( 
            AnnualSettingsTestData.Instance
                .forSchoolId(school.Id)
                .forAcademicYearId(academicYears[2].Id)
                .create());
                
        annualSettings.add( 
            AnnualSettingsTestData.Instance
                .forSchoolId(school.Id)
                .forAcademicYearId(academicYears[1].Id)
                .create());
                
        annualSettings.add( 
            AnnualSettingsTestData.Instance
                .forSchoolId(school.Id)
                .forAcademicYearId(academicYears[0].Id)
                .create());
                
        insert annualSettings;
        
        Test.startTest();
            List<Account> result = AccountSelector.Instance
                .selectWithLatestAnnualSetting(new Set<Id>{school.Id}, new Set<String>{'id', 'Academic_Year_Name__c'});
            
            System.AssertEquals(academicYears[0].Name, result[0].Annual_Settings__r[0].Academic_Year_Name__c);
        Test.stopTest();
    }

    @isTest
    private static void selectVisibleActiveSchoolsWithAnnualSetting_expectExceptions()
    {
        Academic_Year__c academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        Account school = AccountTestData.Instance.asSchool().DefaultAccount;

        Test.startTest();
            try 
            {
                AccountSelector.Instance.selectVisibleActiveSchoolsWithAnnualSetting(null, new List<String>(), new List<String>(), academicYear.Id);

                TestHelpers.expectedArgumentNullException();
            } 
            catch (Exception e)
            {
                TestHelpers.assertArgumentNullException(e, AccountSelector.SCHOOL_IDS);
            }

            try 
            {
                AccountSelector.Instance.selectVisibleActiveSchoolsWithAnnualSetting(new Set<Id>{school.Id}, null, new List<String>(), academicYear.Id);

                TestHelpers.expectedArgumentNullException();
            } 
            catch (Exception e)
            {
                TestHelpers.assertArgumentNullException(e, AccountSelector.SCHOOL_FIELDS);
            }

            try 
            {
                AccountSelector.Instance.selectVisibleActiveSchoolsWithAnnualSetting(new Set<Id>{school.Id}, new List<String>(), null, academicYear.Id);

                TestHelpers.expectedArgumentNullException();
            } 
            catch (Exception e)
            {
                TestHelpers.assertArgumentNullException(e, AccountSelector.ANNUAL_SETTING_FIELDS);
            }

            try 
            {
                AccountSelector.Instance.selectVisibleActiveSchoolsWithAnnualSetting(new Set<Id>{school.Id}, new List<String>(), new List<String>(), null);

                TestHelpers.expectedArgumentNullException();
            } 
            catch (Exception e)
            {
                TestHelpers.assertArgumentNullException(e, AccountSelector.ACADEMIC_YEAR_ID);
            }
        Test.stopTest();
    }

    @isTest
    private static void selectVisibleActiveSchoolsWithAnnualSetting_expectRecordsReturned()
    {
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        Academic_Year__c priorAcademicYear = AcademicYearTestData.Instance.asPreviousYear().insertAcademicYear();

        Account schoolA = AccountTestData.Instance
            .asSchool()
            .forHideFromSchoolSelection(false)
            .forSSSSubscriberStatus(GlobalVariables.getActiveSSSStatusForTest()).create();
        Account schoolB = AccountTestData.Instance
            .asSchool()
            .forHideFromSchoolSelection(false)
            .forSSSSubscriberStatus(GlobalVariables.getActiveSSSStatusForTest()).create();
        Account schoolC = AccountTestData.Instance
            .asSchool()
            .forHideFromSchoolSelection(false)
            .forSSSSubscriberStatus(GlobalVariables.getActiveSSSStatusForTest()).create();
        Account notActiveSubscriberSchool = AccountTestData.Instance
            .asSchool()
            .forHideFromSchoolSelection(false)
            .forSSSSubscriberStatus('Test Status').create();
        Account hiddenFromSelectionSchool = AccountTestData.Instance
            .asSchool()
            .forSSSSubscriberStatus(GlobalVariables.getActiveSSSStatusForTest())
            .forHideFromSchoolSelection(true).create();
        List<Account> schools = new List<Account>{schoolA, schoolB, schoolC, notActiveSubscriberSchool, hiddenFromSelectionSchool};
        insert schools;

        Annual_Setting__c asAforCurrentYear = AnnualSettingsTestData.Instance
            .forSchoolId(schoolA.Id)
            .forAcademicYearId(currentAcademicYear.Id)
            .create();
        Annual_Setting__c asAforPriorYear = AnnualSettingsTestData.Instance
            .forSchoolId(schoolA.Id)
            .forAcademicYearId(priorAcademicYear.Id)
            .create();
        Annual_Setting__c asBforCurrentYear = AnnualSettingsTestData.Instance
            .forSchoolId(schoolB.Id)
            .forAcademicYearId(currentAcademicYear.Id)
            .create();
        Annual_Setting__c asBforPriorYear = AnnualSettingsTestData.Instance
            .forSchoolId(schoolB.Id)
            .forAcademicYearId(priorAcademicYear.Id)
            .create();
        Annual_Setting__c asCforCurrentYear = AnnualSettingsTestData.Instance
            .forSchoolId(schoolC.Id)
            .forAcademicYearId(currentAcademicYear.Id)
            .create();
        Annual_Setting__c asCforPriorYear = AnnualSettingsTestData.Instance
            .forSchoolId(schoolC.Id)
            .forAcademicYearId(priorAcademicYear.Id)
            .create();
        insert new List<Annual_Setting__c>{asAforCurrentYear, asAforPriorYear, asBforCurrentYear, asBforPriorYear, 
            asCforCurrentYear, asCforPriorYear};

        Test.startTest();
            List<Account> accounts = AccountSelector.Instance.selectVisibleActiveSchoolsWithAnnualSetting(
                new Map<Id, Account>(schools).keySet(), new List<String>(), new List<String>(), priorAcademicYear.Id);

            System.assertEquals(3, accounts.size());
            System.assert(new Set<Id>{schoolA.Id, schoolB.Id, schoolC.Id}.containsAll(new Map<Id, Account>(accounts).keySet()));
            for (Integer i=0;i<accounts.size();i++)
            {
                System.assertEquals(1, accounts[i].Annual_Settings__r.size());
                System.assert(
                    new Set<Id>{asAforPriorYear.Id, asBforPriorYear.Id, asCforPriorYear.Id}.contains(accounts[i].Annual_Settings__r[0].Id));
            }
        Test.stopTest();
    }

    @isTest
    private static void testSelectSchoolsAndAccessOrgsWithHigherSSSCodes_expectException()
    {
        Test.startTest();
            try
            {
                AccountSelector.Instance.selectSchoolsAndAccessOrgsWithHigherSSSCodes(null);

                TestHelpers.expectedArgumentNullException();
            }
            catch (Exception e)
            {
                TestHelpers.assertArgumentNullException(e, AccountSelector.SSS_CODE_NUMBER);
            }
        Test.stopTest();
    }

    @isTest
    private static void testSelectSchoolsAndAccessOrgsWithHigherSSSCodes_expectRecordsReturned()
    {
        Integer lastGeneratedSSSCode = 200002;
        List<String> lowerSSSCodes = new List<String>{'200000', '200001'};
        List<String> higherSSSCodes = new List<String>{'200003', '200004', '200005','200006'};

        Account schoolAwithLowerCode = AccountTestData.Instance
            .asSchool()
            .forSSSSchoolCode(lowerSSSCodes[0]).create();
        Account schoolBwithLowerCode = AccountTestData.Instance
            .asSchool()
            .forSSSSchoolCode(lowerSSSCodes[1]).create();
        Account schoolWithSameCode = AccountTestData.Instance
            .asSchool()
            .forSSSSchoolCode(String.valueOf(lastGeneratedSSSCode)).create();
        Account schoolAwithHigherCode = AccountTestData.Instance
            .asSchool()
            .forSSSSchoolCode(higherSSSCodes[0]).create();
        Account schoolBwithHigherCode = AccountTestData.Instance
            .asSchool()
            .forSSSSchoolCode(higherSSSCodes[1]).create();
        Account accountWithHigherCode = AccountTestData.Instance
            .asFamily()
            .forSSSSchoolCode(higherSSSCodes[2]).create();
        Account accessOrgwithHigherCode = AccountTestData.Instance
            .asAccessOrganisation()
            .forSSSSchoolCode(higherSSSCodes[3]).create();

        insert new List<Account>{schoolAwithLowerCode, schoolBwithLowerCode, schoolWithSameCode, schoolAwithHigherCode,
                schoolBwithHigherCode, accountWithHigherCode,accessOrgwithHigherCode};

        Test.startTest();
            List<Account> accountsWithHigherCodes = AccountSelector.Instance.selectSchoolsAndAccessOrgsWithHigherSSSCodes(lastGeneratedSSSCode);

            System.assertEquals(4, accountsWithHigherCodes.size());
            System.assert((new Map<Id, Account>(accountsWithHigherCodes).keySet()).containsAll(
                new Set<Id>{schoolAwithHigherCode.Id, schoolBwithHigherCode.Id,
                        schoolWithSameCode.Id, accessOrgwithHigherCode.Id}));
        Test.stopTest();
    }
}