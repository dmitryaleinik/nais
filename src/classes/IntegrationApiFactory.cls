/**
 * IntegrationApiFactory.cls
 *
 * @description: Factory class to validate Integration Settings are correct before starting the process.
 *
 * @author: Mike Havrilla @ Presence PG
 */

public class IntegrationApiFactory {

    private final String BASE_URL_CALLOUT_NULL_ERROR =  'IntegrationSource__c.BaseCalloutUrl__c is null';
    private final String API_ENDPOINT_NULL_ERROR =  'Integration_Process.apiEndpoint is is null for: ';
    private final String NEXT_API_EMPTY_ERROR = 'Integration_Process.nextApiName is populated but there is no active integrations with that name.';
    private final String NEXT_API_PARAMS_INVALID_ERROR = 'Error: Integration_Process.NextApiParameter is populated but the Integration_Process.nextApiName is empty';
    private final String PRE_PROCESS_CLASS_ERROR = 'Error: preProcess class is populated but not of the correct type (IntegrationApiModel.IAction)';
    private final String POST_PROCESS_CLASS_ERROR = 'Error: postProcess class is populated but not of the correct type (IntegrationApiModel.IAction)';
    private final String INVALID_SOURCE_ERROR = 'No Integration Source record for: ';
    private final String INVALID_API_SETTING_ERROR = 'No Integration API Setting records found for: ';

    /**
     * @description Singleton instance of the IntegrationApiFactory.
     */
    public static IntegrationApiFactory Instance {
        get {
            if (Instance == null) {
                Instance = new IntegrationApiFactory();
            }
            return Instance;
        }
        private set;
    }

    private IntegrationApiFactory() {}

    /**
     * @description: Accepts a request which contains the source, and a pfs instance. Gets all integration
     * mappings based on source, and validates the model.
     *
     * @param: IntegrationApiFactory.Request request:  - Innstance of IntegrationApiFactory.Request.
     *
     * @return: IntegrationApiFactory.Response: - response instance
     *
     */
    public IntegrationApiFactory.Response getAPIModels( IntegrationApiFactory.Request request) {
        IntegrationApiFactory.Response response = this.getIntegrationsApis( request.source);

        for ( IntegrationApiModel integrationModel : response.integrationModels.values()) {

            this.validateModel( integrationModel, response.integrationModels);
        }

        return response;
    }

    /**
     * @description: Validates the integration Model for a specific integration.
     *
     * @param: IntegrationApiModel integrationModel:  - Innstance of IntegrationModel.
     *
     * @param: Map<String, IntegrationApiModel> integrationModelMap: - Map by source, of Integration Models
     *
     */
    private void validateModel( IntegrationApiModel integrationModel, Map<String, IntegrationApiModel> integrationModelMap) {

        if ( String.isEmpty( integrationModel.apiBaseUrl)) {

            throw new IntegrationApiService.IntegrationConfigurationException( this.BASE_URL_CALLOUT_NULL_ERROR);
        }

        if ( String.isEmpty( integrationModel.apiEndpoint)) {

            throw new IntegrationApiService.IntegrationConfigurationException( this.API_ENDPOINT_NULL_ERROR +  integrationModel.apiName);
        }


        if ( integrationModel.nextApiName != null && integrationModelMap.get( integrationModel.nextApiName) == null) {

            throw new IntegrationApiService.IntegrationConfigurationException( this.NEXT_API_EMPTY_ERROR +  integrationModel.apiName);
        }

        if ( integrationModel.nextApiParameters != null && integrationModel.nextApiName == null) {

            throw new IntegrationApiService.IntegrationConfigurationException( this.NEXT_API_PARAMS_INVALID_ERROR +  integrationModel.apiName);
        }

        if( integrationModel.preProcessClasses.size() > 0) {

            for( String preProcessClass : integrationModel.preProcessClasses) {
                Type t = Type.forName( preProcessClass.trim());
                if ( (t.newInstance() instanceof IntegrationApiModel.IAction) == false) {

                    throw new IntegrationApiService.IntegrationConfigurationException( this.PRE_PROCESS_CLASS_ERROR);
                }
            }
        }


        if ( integrationModel.postProcessClasses.size() > 0) {

            for( String postProcessClass : integrationModel.postProcessClasses) {
                Type t = Type.forName( postProcessClass.trim());
                if ( (t.newInstance() instanceof IntegrationApiModel.IAction) == false) {

                    throw new IntegrationApiService.IntegrationConfigurationException( this.POST_PROCESS_CLASS_ERROR);
                }
            }

        }
    }

    /**
     * @description: Inflates IntegrationApiModelMap based on Values from IntegrationProcess_Mdt.
     *
     * @param: String source:  - API source name (i.e. ravenna, sao);
     *
     * @return:IntegrationApiFactory.Response: - Instance of Response class.
     *
     */
    private IntegrationApiFactory.Response getIntegrationsApis( String source) {

        IntegrationApiFactory.Response response = new IntegrationApiFactory.Response();
        IntegrationSource__c apiSource = IntegrationSource__c.getInstance( source.toLowerCase());
        if ( apiSource == null) {

            throw new IntegrationApiService.IntegrationConfigurationException( this.INVALID_SOURCE_ERROR + source);
        }
        response.apiDisplayName = apiSource.DisplayName__c != null ? apiSource.DisplayName__c : source;

        if( Test.isRunningTest()) {

            source = source + '_Sandbox';
        }
        List<Integration_API_Setting__mdt> integrationProcess = [ select Api_Endpoint__c, ApiName__c, DeveloperName, Id, IntegrationSource__c,
                                                                         IsActive__c, IsFirstApi__c, Iterate_Over_Next_Call__c, Iterate_Over_With_Key__c,
                                                                         Next_Api_Name__c, Next_Api_Parameters__c, PreProcessClass__c, PostProcessClass__c
                                                                    from Integration_API_Setting__mdt
                                                                    where IsActive__c = true
                                                                     and IntegrationSource__c = :source];

        if( integrationProcess.size() == 0) {

            throw new IntegrationApiService.IntegrationConfigurationException( this.INVALID_API_SETTING_ERROR + source);
        }
        for ( Integration_API_Setting__mdt process : integrationProcess) {

            IntegrationApiModel integrationModel = new IntegrationApiModel( process, apiSource.BaseCalloutURL__c);
            response.addIntegrationModel( integrationModel);
        }


        return response;
    }

    // wrapper class for request/response
    public class Request {
        public String source;
        public PFS__c pfs;

        public Request( String source, PFS__c pfs) {
            this.source = source;
            this.pfs = pfs;
        }
    }

    public class Response {

        public Map<String, IntegrationApiModel> integrationModels { get; private set; }
        public String apiDisplayName { get; set; }

        public Response() {

            this.integrationModels = new Map<String, IntegrationApiModel>();
            this.apiDisplayName = '';
        }

        public void addIntegrationModel( IntegrationApiModel model) {

            this.integrationModels.put( model.apiName, model);
        }
    }

}