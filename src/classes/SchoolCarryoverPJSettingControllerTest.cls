@isTest
private class SchoolCarryoverPJSettingControllerTest {
    
    private static final String STRING_YES = 'Yes';

    private static Annual_Setting__c CreateData(Id schoolId) {
        Id academicYearPastId = GlobalVariables.getPreviousAcademicYear().Id;
        Annual_Setting__c pastAnnualSetting = AnnualSettingsTestData.Instance
            .forSchoolId(schoolId)
            .forAcademicYearId(academicYearPastId)
            .forApplyMinimumIncome(STRING_YES)
            .forAddDeprecationHomeBusinessExpense(STRING_YES).insertAnnualSettings();

        return pastAnnualSetting;
    }

    @isTest
    private static void SchoolCarryoverPJSettingTest() {
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
        Academic_Year__c previousAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, previousAcademicYear};

        Account school = AccountTestData.Instance.asSchool().DefaultAccount;
        Contact schoolStaff = ContactTestData.Instance
            .forAccount(school.Id)
            .asSchoolStaff().insertContact();
        
        User thisUser = UserSelector.newInstance().selectById(new Set<Id>{UserInfo.getUserId()})[0];
        User schoolPortalUser;
        System.runAs(thisUser) {
            schoolPortalUser = UserTestData.Instance
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }
        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);

        Test.startTest();
            System.runAs(schoolPortalUser) {
                Annual_Setting__c currentAnnual = AnnualSettingsTestData.Instance
                    .forSchoolId(school.Id)
                    .forAcademicYearId(currentAcademicYear.Id).DefaultAnnualSettings;
                Annual_Setting__c annualS= SchoolCarryoverPJSettingControllerTest.CreateData(school.Id);
                PageReference pageRef = Page.SchoolCarryoverPJSettings;  
                 
                ApexPages.StandardController sc = new ApexPages.StandardController(annualS);
                SchoolCarryoverPJSettingController controller = new SchoolCarryoverPJSettingController(sc);                 
                pageRef.getParameters().put('currentAcademicyearId', currentAcademicYear.Id); 
                pageRef.getParameters().put('Id', annualS.Id); 
                Test.setCurrentPage(pageRef);     
                controller.buttonMode='All';            
                controller.copyPJSetting();                 
                
                Annual_Setting__c annualSetting = 
                    GlobalVariables.getCurrentAnnualSettings(false, currentAcademicYear.Id).get(0); 
                system.assertEquals(annualSetting.Apply_Minimum_Income__c, STRING_YES);
            }
        Test.stopTest();
    }
}