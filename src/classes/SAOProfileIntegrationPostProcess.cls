/**
* SAOProfileIntegrationPostProcess.cls
*
* @description: Handles postprocessing of SAO integration api calls.
* Currently called after the all API calls and maps student info into the requried objects.
*
*/
public class SAOProfileIntegrationPostProcess implements IntegrationApiModel.IAction {

    private final String PROFILES_API = 'Profiles';
    private final String VALID_PARENT_PROFILE = 'Parent';
    private final String STUDENT_ID_FIELD = 'folioId';

    //Constructor
    public SAOProfileIntegrationPostProcess() {}

    /**
    * @description: Handles the post processing of SAO integration. Maps Profiles,
    * processes applicants and populates any additional fields before allowing IntegrationFinalize
    * to insert the records
    *
    * @param: apiState - A Current Instance of IntegrationApiModel.ApiState
    */
    public void invoke( IntegrationApiModel.ApiState apiState) {

        if( apiState.currentApi == PROFILES_API) {

            // First, make sure we actually got a response from SAO.
            List<Object> profilesResponseList = apiState.httpResponseByApi.get( PROFILES_API );

            if (profilesResponseList.isEmpty()) {
                throw new IntegrationApiService.IntegrationException('No information was received from SAO.');
            }

            Map<String, Object> profilesResponse = (Map<String, Object>)profilesResponseList[0];

            // We also make sure that the profile response is for an SAO parent since previously, the parents using this
            // integration would login to SAO with each student's credentials.
            String authenticationError = getAuthenticationErrorMessage(profilesResponse);
            if (!String.isBlank(authenticationError)) {
                throw new IntegrationApiService.IntegrationException(authenticationError);
            }

            // Get the student profiles and the applications from the parent profile returned by the SAO API.
            processProfilesResponse(apiState, profilesResponse);

            IntegrationUtils.processApplicants(apiState, STUDENT_ID_FIELD, PROFILES_API);

            this.populateFields(apiState);
        }
    }

    /**
    * @description Verify if the user that was authenticated against SAO has "Parent" profile.
    *              If the user has a "profileType" different than "Parent" an error message is
    *              returned. Otherwise, it returns null.
    * @param apiState The current Instance of IntegrationApiModel.ApiState.
    * @return The error message for SAO users that has "profileType" different than "Parent". It
    *         return NULL, if the logged SAO user is a "Parent".
    */
    private String getAuthenticationErrorMessage(Map<String, Object> profilesResponse) {

        String profileType = String.ValueOf(profilesResponse.get('profileType'));

        if (profileType  != VALID_PARENT_PROFILE) {

            Map<String, Object> parentNames = (Map<String, Object>)profilesResponse.get('name');
            String parentFirstName = String.ValueOf(parentNames.get('firstName'));
            String parentLastName = String.ValueOf(parentNames.get('lastName'));

            return String.format('The user "{0} {1}" that you are trying to sign in is not an SAO parent account.',
                    new List<String>{parentFirstName, parentLastName});
        }

        return null;
    }

    /**
    * @description: Use this method to set fields on Applicant__c object that are specific to this integration
    *
    * @param: apiState - A Current Instance of IntegrationApiModel.ApiState
    * @param: apiState - A Current Instance of IntegrationApiModel.ApiState
    */
    private void populateFields(IntegrationApiModel.ApiState apiState) {

        for ( sObject applicant : apiState.familyModel.studentApplicants) {
            String grade = (String)applicant.get('Current_Grade__c');

            if ( grade.containsIgnoreCase( 'Grade')) {

                grade = grade.remove( 'Grade');
            }
            applicant = apiState.familyModel.populateFieldByType( applicant, 'Current_Grade__c', grade);
        }
    }

    private void processProfilesResponse(IntegrationApiModel.ApiState apiState, Map<String, Object> saoProfilesResponse) {
        // Make sure we have a place to store the school codes for each student
        apiState.familyModel.studentIdToSchoolsMap = new Map<String, List<String>>();

        // Using the PFS Academic Year, get the student profiles for that year from the sao profiles response.
        SAO_StudentProfileModel studentProfiles = SAO_StudentProfileModel.createModelForYear(saoProfilesResponse, apiState.pfs.Academic_Year_Picklist__c);

        // For each student, we need to get the school codes for the schools that they have applications for and add them to the API state.
        // This will be used to determine which schools to create SPAs and Folders for.
        // We also need the raw student data to override the response from SAO with the results filtered by academic year.
        List<Object> rawStudentResults = new List<Object>();
        for (SAO_StudentProfileModel.Student student : studentProfiles.getStudents()) {
            apiState.familyModel.studentIdToSchoolsMap.put(student.getExternalId(), student.getSchoolCodes());
            rawStudentResults.add(student.getRawData());
            // SFP-1686: Get the students applications from the student profiles and store them in the ApplicationsByResponsesByStudentId map.
            // This map will then be used when inserting Student Folders/PFS Assignments.
            apiState.familyModel.ApplicationResponsesByStudentId.put(student.getExternalId(), student.getApplicationData());
        }

        apiState.httpResponseByApi.put(PROFILES_API, rawStudentResults);
    }
}