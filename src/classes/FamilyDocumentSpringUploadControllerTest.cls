@isTest
private class FamilyDocumentSpringUploadControllerTest {
    
    private class TestDataLocal {
        Id academicYearId;
        String academicYearName;
        Account school1;
        Contact staff1, parentA, parentB, student1, student2;
        Applicant__c applicant1A;
        PFS__c pfs1;
        Student_Folder__c studentFolder1;
        School_PFS_Assignment__c spfsa1;
        Required_Document__c rd1, rd2, rd3;
        Family_Document__c fd1, fd2;
        School_Document_Assignment__c sda1, sda2, sda3, sda4;
        Household__c hh;

        private TestDataLocal() {
            // academic year
            TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
            academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
            academicYearName = GlobalVariables.getCurrentAcademicYear().Name;

            // school
            school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, 1, false);
            school1.SSS_School_Code__c = '1234';
            insert school1;

            // household
            hh = TestUtils.createHousehold('Test Household', true);

            // parents and students
            parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
            parentA.Household__c = hh.Id;
            parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
            student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
            student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
            insert new List<Contact> {parentA, parentB, student1, student2};

            // psf
            pfs1 = TestUtils.createPFS('PFS A', academicYearName, parentA.Id, false);
            insert new List<PFS__c> {pfs1};

            // applicant
            applicant1A = TestUtils.createApplicant(student1.Id, pfs1.Id, false);
            insert new List<Applicant__c> {applicant1A};

            // student folder
            studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearName, student1.Id, false);
            insert studentFolder1;

            // school pfs assignment
            spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearName, applicant1A.Id, school1.Id, studentFolder1.Id, false);
            insert spfsa1;

            // required documents
            rd1 = TestUtils.createRequiredDocument(academicYearId, school1.Id, false);
            rd2 = TestUtils.createRequiredDocument(academicYearId, school1.Id, false);
            rd3 = TestUtils.createRequiredDocument(academicYearId, school1.Id, false);
              rd3.Label_for_School_Specific_Document__c = 'Test School Specific Doc';
              rd3.Document_Year__c = '2013';
              rd3.Document_Type__c = ApplicationUtils.schoolSpecificDocType();
            insert new List<Required_Document__c> {rd1, rd2, rd3};

            // family documents
            fd1 = TestUtils.createFamilyDocument('FamilyDoc1', academicYearName, false);
            fd2 = TestUtils.createFamilyDocument('FamilyDoc2', academicYearName, false);
            insert new List<Family_Document__c> {fd1, fd2};

            System.debug(LoggingLevel.ERROR, 'TESTINGdrew71 ' +  academicYearName  + '; ' + academicYearId);
            System.debug(LoggingLevel.ERROR, 'TESTINGdrew72 ' + [select id, Required_Document__c, Document_Type__c, Document_Year__c FROM School_Document_Assignment__c]);

            // update family document lookup of school document assignments automatically created by Required Document trigger
            for (School_Document_Assignment__c sd : [select Id, Required_Document__c, Document_Type__c, Document_Year__c from School_Document_Assignment__c where School_PFS_Assignment__c = :spfsa1.Id]) {
                if (sd.Required_Document__c == rd1.Id) {
                    sda1 = sd;
                    sda1.Document__c = fd1.Id;
                } else if (sd.Required_Document__c == rd2.Id) {
                    sda2 = sd;
                    sda2.Document__c = fd2.Id;
                }
                else if (sd.Required_Document__c == rd3.Id) {
                    sda3 = sd;
                }

            }
            update new List<School_Document_Assignment__c> {sda1, sda2};

            // create pertains to mapping
            SpringCMActionTest.setupMappings();

            insert new Global_Message__c(Portal__c='School Portal; School Login; Spring CM - Maintenance Window', Display_End_Date_Time__c = system.now().addDays(1), Display_Start_Date_Time__c = system.now().addDays(-1));
        }
    }

    @isTest
    private static void testSaveAndReplaceNewDocument() {
        TestDataLocal td = new TestDataLocal();
        Test.startTest();

        td.sda1.Document_Type__c = 'W2';
        update td.sda1;

        FamilyDocumentSpringUploadController controller = new FamilyDocumentSpringUploadController();
        controller.pfsRecordParam = td.pfs1;
        controller.docType = td.sda1.Document_Type__c;
        controller.docYear = td.sda1.Document_Year__c;
        controller.pertainsTo = 'Parent A';
        controller.documentSourceParam = 'Uploaded by Parent';
        controller.schoolCode = null;
        controller.schoolSpecificLabel = null;

        controller.doSpringUploadStep1();
        controller.doSpringUploadStep2();

        // verify a familyDoc was created and attached to the SDA
        System.assertNotEquals(null, controller.docId);
        System.assertEquals(1, [SELECT count() FROM School_Document_Assignment__c WHERE Id=:td.sda1.Id AND Document__c = :controller.docId AND Document_Status__c='Not Received']);

        // test replace
        Family_Document__c fd = new Family_Document__c(Id=controller.docId, Document_Status__c = 'Error/Invalid Document', W2_Employee_ID__c = 'fordel');
        update fd;

        controller.doSpringUploadStep1();
        controller.doSpringUploadStep2();

        // verify the status was reset
        System.assertEquals(1, [SELECT count() FROM School_Document_Assignment__c WHERE Id=:td.sda1.Id AND Document__c = :controller.docId AND Document_Status__c='Not Received']);
        System.assertEquals(null, [Select W2_Employee_ID__c FROM Family_Document__c WHERE Id = :controller.docId][0].W2_Employee_ID__c); // testing nullify on replace

        // SFP 171 : test maintenance window
        system.assertEquals(true,controller.getIsMaintenanceWindow());


        Test.stopTest();
    }

    @isTest
    private static void testSaveSchoolSpecificDocument() {
        TestDataLocal td = new TestDataLocal();
        Test.startTest();

        FamilyDocumentSpringUploadController controller = new FamilyDocumentSpringUploadController();
        controller.pfsRecordParam = td.pfs1;
        controller.docType = td.sda3.Document_Type__c;
        controller.docYear = td.sda3.Document_Year__c;
        controller.pertainsTo = 'Parent B';
        controller.documentSourceParam = 'Uploaded by Parent';
        controller.schoolCode = '1234';
        controller.schoolSpecificLabel = 'Test School Specific Doc';

        controller.doSpringUploadStep1();
        controller.doSpringUploadStep2();

        // verify a familyDoc was created and attached to the SDA
        System.assertNotEquals(null, controller.docId);
        System.assertEquals(1, [SELECT count() FROM School_Document_Assignment__c WHERE Id=:td.sda3.Id AND Document__c = :controller.docId AND Document_Status__c='Not Received']);
        //[CH] NAIS-1568 Adding verification that Other_Document_Label__c is being filled in
        List<Family_Document__c> famDocResults = [select Id, Other_Document_Label__c from Family_Document__c where Id = :controller.docId];
        System.assertEquals(1, famDocResults.size());
        System.assertEquals(controller.schoolSpecificLabel, famDocResults[0].Other_Document_Label__c);

        Test.stopTest();
    }

    @isTest
    private static void testSaveAdditionalDocument() {
        TestDataLocal td = new TestDataLocal();
        td.sda1.Document_Type__c = 'Other/Misc Tax Form';
        update td.sda1;

        Test.startTest();

        FamilyDocumentSpringUploadController controller = new FamilyDocumentSpringUploadController();
        controller.pfsRecordParam = td.pfs1;
        controller.docType = td.sda1.Document_Type__c;
        controller.docYear = td.sda1.Document_Year__c;
        controller.pertainsTo = 'Parent A';
        controller.documentSourceParam = 'Uploaded by Parent';
        controller.schoolCode = null;
        controller.schoolSpecificLabel = null;
        controller.createSchoolAsmnt = true;

        controller.doSpringUploadStep1();
        controller.doSpringUploadStep2();

        // verify a familyDoc was created and attached to a new SDA
        System.assertNotEquals(null, controller.docId);
        System.assertEquals(1, [SELECT count() FROM School_Document_Assignment__c WHERE Id != :td.sda1.Id AND Document__c = :controller.docId AND Document_Status__c='Not Received']);

        //NAIS-2313 [DP] 03.05.2015
        Family_Document__c testFD = [SELECT Id, Document_Type__c, Document_Year__c, Document_Year_Original__c, Document_Type_Original__c FROM Family_Document__c WHERE Id = :controller.docId ];
        System.assertEquals(td.sda1.Document_Type__c, testFD.Document_Type__c);
        System.assertEquals(td.sda1.Document_Type__c, testFD.Document_Type_Original__c);
        System.assertEquals(td.sda1.Document_Year__c, testFD.Document_Year__c);
        System.assertEquals(td.sda1.Document_Year__c, testFD.Document_Year_Original__c);

        Test.stopTest();
    }

    @isTest
    private static void testAddAndSaveNewDocument() {
        TestDataLocal td = new TestDataLocal();
        update td.sda1;

        Test.startTest();

        FamilyDocumentSpringUploadController controller = new FamilyDocumentSpringUploadController();
        controller.pfsRecordParam = td.pfs1;
        controller.docType = td.sda1.Document_Type__c;
        controller.docYear = td.sda1.Document_Year__c;
        controller.pertainsTo = 'Parent A';
        controller.documentSourceParam = 'Uploaded by Parent';
        controller.schoolCode = null;
        controller.schoolSpecificLabel = null;
        controller.createSchoolAsmnt = true;

        controller.doSpringUploadStep1();
        controller.doSpringUploadStep2();

        // verify a familyDoc was created and attached to a new SDA
        System.assertNotEquals(null, controller.docId);
        System.assertEquals(1, [SELECT count() FROM School_Document_Assignment__c WHERE Id != :td.sda1.Id AND Document__c = :controller.docId AND Document_Status__c='Not Received']);

        Test.stopTest();
    }

    @isTest
    private static void testSaveAdditionalDocumentSchoolRequired() {
        TestDataLocal td = new TestDataLocal();
        td.sda1.Document_Type__c = 'Other/Misc Tax Form';
        update td.sda1;

        Test.startTest();

        FamilyDocumentSpringUploadController controller = new FamilyDocumentSpringUploadController();
        controller.pfsRecordParam = td.pfs1;
        controller.docType = td.sda1.Document_Type__c;
        controller.docYear = td.sda1.Document_Year__c;
        controller.pertainsTo = 'Parent A';
        controller.documentSourceParam = 'Uploaded by Parent';
        controller.schoolCode = null;
        controller.schoolSpecificLabel = null;
        controller.createSchoolAsmnt = true;

        System.assertEquals(null, controller.doSpringUploadAndRedirect());

        controller.selectedSchools = td.school1.Id;

        System.assertNotEquals(null, controller.doSpringUploadAndRedirect());

        Test.stopTest();
    }

    // test to make sure that when a school user uploads a document that the resulting Fam Doc has the SSS Code
    @isTest
    private static void docSprindUploadSteps_asSchoolUser_expectSssUploaderCodePopulated() {

        User currentUser = GlobalVariables.getCurrentUser();
        User schoolUser;
        TestDataLocal td;
        System.runAs(currentUser){
            // need to make the PTG so code will know schoolUser is a school user
            Profile_Type_Grouping__c ptg = new Profile_Type_Grouping__c();
            ptg.Name = 'School Portal Administrator';
            ptg.Is_School_Profile__c = true;
            ptg.Profile_Name__c = ProfileSettings.SchoolAdminProfileName;
            insert ptg;

            td = new TestDataLocal();
            td.staff1 = TestUtils.createContact('School Staff', td.school1.Id, RecordTypes.schoolStaffContactTypeId, true);
        }

        schoolUser = TestUtils.createPortalUser('Org User 1', 'ou1@test.org', 'ou1', td.staff1.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);    // active user

        Test.startTest();
        System.runAs(schoolUser){
            GlobalVariables.u = null; // fake new context
            GlobalVariables.allSchoolsForUser = null; // fake new context
            FamilyDocumentSpringUploadController controller = new FamilyDocumentSpringUploadController();
            controller.pfsRecordParam = td.pfs1;
            controller.docType = td.sda1.Document_Type__c;
            controller.docYear = td.sda1.Document_Year__c;
            controller.pertainsTo = 'Parent A';
            controller.documentSourceParam = 'Uploaded by Parent';
            controller.schoolCode = null;
            controller.schoolSpecificLabel = null;
            controller.createSchoolAsmnt = true;

            controller.doSpringUploadStep1();
            controller.doSpringUploadStep2();

            // verify a familyDoc was created and attached to a new SDA and has the sss school code (since it was uploaded by a School User)
            System.assertNotEquals(null, controller.docId);
            System.assertEquals(td.school1.SSS_School_Code__c, [SELECT Id, Uploader_SSS_Code__c FROM Family_Document__c WHERE Id = :controller.docId][0].Uploader_SSS_Code__c);
        }
        Test.stopTest();
    }

    // [SL] NAIS-767
    @isTest
    private static void testActionMethods() {
        TestDataLocal td = new TestDataLocal();

        Test.startTest();

        td.pfs1.Filing_Status__c = 'Married, Filing Separately';
        update td.pfs1;

        td.sda1.Document_Type__c =  '1040';
        update td.sda1;

        FamilyDocumentSpringUploadController controller = new FamilyDocumentSpringUploadController();
        controller.pfsRecordParam = [SELECT Id, Academic_Year_Picklist__c, Parent_A__c, Parent_A__r.Household__c, Parent_A__r.Household__r.Name,
                                            Parent_A__r.Name, Parent_A_Email__c, Parent_A_First_Name__c, PFS_Number__c
                                        FROM PFS__c WHERE Id = :td.pfs1.Id];
        controller.academicYearIdParam = td.academicYearId;
        controller.documentSourceParam = 'Uploaded by Parent';
        controller.returnUrlParam = 'http://test.com';
        controller.docYear = td.sda1.Document_Year__c;
        controller.docType = td.sda1.Document_Type__c;
        controller.docId = null;
        controller.schoolCode = null;
        controller.createSchoolAsmnt = false;
        controller.pertainsTo = 'Parent A';
        controller.springCMDocType = td.sda1.Document_Type__c;
        controller.naReason = 'Test Reason';


        controller.doInitNADialog();
        System.assertEquals('NA_DOC', controller.dialogPage);

        controller.doInitUploadDialog();
        System.assertEquals('SELECT_DOC', controller.dialogPage);

        controller.doSpringUploadStep1();
        controller.doSpringUploadStep2();
        System.assertNotEquals(null, controller.docId);

        controller.doSetDocNA();
        Family_Document__c fd = [SELECT Id,Request_to_Waive_Requirement__c FROM Family_Document__c WHERE Id=:controller.docId];
        System.assertEquals('Yes', fd.Request_to_Waive_Requirement__c);

        System.assertNotEquals(null, controller.getSessionId());

        // test getters
        System.assertNotEquals(null, controller.getHouseholdId());
        System.assertNotEquals(null, controller.getHouseholdName());
        System.assertNotEquals(null, controller.getCurrentRequestUrl());
        System.assertNotEquals(null, controller.getTimeNow());
        System.assertNotEquals(null, controller.getScmBaseUrl());
        System.assertNotEquals(null, controller.getScmParamAID());
        System.assertNotEquals(null, controller.getScmParamFormUID());
        System.assertNotEquals(null, controller.getScmParamSuppressNavigation());
        System.assertNotEquals(null, controller.getScmParamRUBase());
        System.assertNotEquals(null, controller.getRetUrl(true));
        System.assertNotEquals(null, controller.getScmParamRURetUrl());
        System.assertNotEquals(null, controller.getScmParamCancelUrl());

    }

    @isTest
    private static void testUserMethods() {
        TestDataLocal td = new TestDataLocal();

        FamilyDocumentSpringUploadController controller = new FamilyDocumentSpringUploadController();
        System.assertEquals(false, controller.userAlreadyOptedOutOfSafariText);
        controller.optOutOfSafariText();

        controller = new FamilyDocumentSpringUploadController();
        System.assertEquals(true, controller.userAlreadyOptedOutOfSafariText);
        controller.optIntoSafariText();

        controller = new FamilyDocumentSpringUploadController();
        System.assertEquals(false, controller.userAlreadyOptedOutOfSafariText);
    }

    @isTest
    private static void testSafariTutorialController(){
        TestDataLocal td = new TestDataLocal();
        SafariCookieTutorialController controller = new SafariCookieTutorialController();
        System.assertNotEquals(null, controller.theHelp);
    }

    @isTest
    private static void testNARequest(){
        TestDataLocal td = new TestDataLocal();

        Test.startTest();

        td.pfs1.Filing_Status__c = 'Married, Filing Separately';
        update td.pfs1;

        td.sda1.Document_Type__c =  '1040';
        update td.sda1;

        FamilyDocumentSpringUploadController controller = new FamilyDocumentSpringUploadController();
        controller.pfsRecordParam = [SELECT Id, Academic_Year_Picklist__c, Parent_A__c, Parent_A__r.Household__c, Parent_A__r.Household__r.Name,
                                            Parent_A__r.Name, Parent_A_Email__c, Parent_A_First_Name__c, PFS_Number__c
                                        FROM PFS__c WHERE Id = :td.pfs1.Id];
        controller.academicYearIdParam = td.academicYearId;
        controller.documentSourceParam = 'Uploaded by Parent';
        controller.returnUrlParam = 'http://test.com';
        controller.docYear = td.sda1.Document_Year__c;
        controller.docType = td.sda1.Document_Type__c;
        controller.docId = null;
        controller.schoolCode = null;
        controller.createSchoolAsmnt = false;
        controller.pertainsTo = 'Parent A';
        controller.springCMDocType = td.sda1.Document_Type__c;
        controller.naReason = 'Test Reason';


        controller.doInitNADialog();
        System.assertEquals('NA_DOC', controller.dialogPage);

        controller.doInitUploadDialog();
        System.assertEquals('SELECT_DOC', controller.dialogPage);

        controller.doSpringUploadStep1();
        controller.doSpringUploadStep2();
        System.assertNotEquals(null, controller.docId);

        controller.doSetDocNA();
        Family_Document__c fd = [SELECT Id,Request_to_Waive_Requirement__c FROM Family_Document__c WHERE Id=:controller.docId];
        System.assertEquals('Yes', fd.Request_to_Waive_Requirement__c);
    }

    // NAIS-2194 [DP] 01.29.2015 allow parent to unrequest an NA
    @isTest
    private static void testNARequestRemove(){
        TestDataLocal td = new TestDataLocal();

        td.pfs1.Filing_Status__c = 'Married, Filing Separately';
        update td.pfs1;

        td.sda1.Document_Type__c =  '1040';
        update td.sda1;

        FamilyDocumentSpringUploadController controller = new FamilyDocumentSpringUploadController();
        controller.pfsRecordParam = [SELECT Id, Academic_Year_Picklist__c, Parent_A__c, Parent_A__r.Household__c, Parent_A__r.Household__r.Name,
                                            Parent_A__r.Name, Parent_A_Email__c, Parent_A_First_Name__c, PFS_Number__c
                                        FROM PFS__c WHERE Id = :td.pfs1.Id];
        controller.academicYearIdParam = td.academicYearId;
        controller.documentSourceParam = 'Uploaded by Parent';
        controller.returnUrlParam = 'http://test.com';
        controller.docYear = td.sda1.Document_Year__c;
        controller.docType = td.sda1.Document_Type__c;
        controller.docId = null;
        controller.schoolCode = null;
        controller.createSchoolAsmnt = false;
        controller.pertainsTo = 'Parent A';
        controller.springCMDocType = td.sda1.Document_Type__c;
        controller.naReason = 'Test Reason';


        controller.doInitNADialog();
        System.assertEquals('NA_DOC', controller.dialogPage);

        controller.doInitUploadDialog();
        System.assertEquals('SELECT_DOC', controller.dialogPage);

        controller.doSpringUploadStep1();
        controller.doSpringUploadStep2();
        System.assertNotEquals(null, controller.docId);

        controller.doSetDocNA();
        Family_Document__c fd = [SELECT Id,Request_to_Waive_Requirement__c, (Select Id, Requirement_Waiver_Status__c FROM School_Document_Assignments__r) FROM Family_Document__c WHERE Id=:controller.docId];
        System.assertEquals('Yes', fd.Request_to_Waive_Requirement__c);

        System.assertNotEquals(0, fd.School_Document_Assignments__r.size());

        fd.School_Document_Assignments__r[0].Requirement_Waiver_Status__c = 'Approved';
        update fd.School_Document_Assignments__r[0];

        System.assertEquals('Approved', [Select Id, Requirement_Waiver_Status__c FROM School_Document_Assignment__c WHERE ID = :fd.School_Document_Assignments__r][0].Requirement_Waiver_Status__c);

        System.assertNotEquals(null, controller.docId);

        Test.startTest();
        controller.removeNADoc();
        Test.stopTest();

        fd = [SELECT Id,Request_to_Waive_Requirement__c, (Select Id, Requirement_Waiver_Status__c FROM School_Document_Assignments__r) FROM Family_Document__c WHERE Id=:controller.docId];
        System.assertNotEquals('Yes', fd.Request_to_Waive_Requirement__c);

        System.assertNotEquals('Approved', [Select Id, Requirement_Waiver_Status__c FROM School_Document_Assignment__c WHERE ID = :fd.School_Document_Assignments__r][0].Requirement_Waiver_Status__c);


    }


    // NAIS-2256 [DP] 02.12.2015 - test school approving request
    @isTest
    private static void testNARequestApprove(){
        TestDataLocal td = new TestDataLocal();

        td.pfs1.Filing_Status__c = 'Married, Filing Separately';
        update td.pfs1;

        td.sda1.Document_Type__c =  '1040';
        update td.sda1;

        FamilyDocumentSpringUploadController controller = new FamilyDocumentSpringUploadController();
        controller.pfsRecordParam = [SELECT Id, Academic_Year_Picklist__c, Parent_A__c, Parent_A__r.Household__c, Parent_A__r.Household__r.Name,
                                            Parent_A__r.Name, Parent_A_Email__c, Parent_A_First_Name__c, PFS_Number__c
                                        FROM PFS__c WHERE Id = :td.pfs1.Id];
        controller.academicYearIdParam = td.academicYearId;
        controller.documentSourceParam = 'Uploaded by Parent';
        controller.returnUrlParam = 'http://test.com';
        controller.docYear = td.sda1.Document_Year__c;
        controller.docType = td.sda1.Document_Type__c;
        controller.docId = null;
        controller.schoolCode = null;
        controller.createSchoolAsmnt = false;
        controller.pertainsTo = 'Parent A';
        controller.springCMDocType = td.sda1.Document_Type__c;
        controller.naReason = 'Test Reason';


        controller.doInitNADialog();
        System.assertEquals('NA_DOC', controller.dialogPage);

        controller.doSetDocNA();
        Family_Document__c fd = [SELECT Id,Request_to_Waive_Requirement__c, (Select Id, Requirement_Waiver_Status__c FROM School_Document_Assignments__r) FROM Family_Document__c WHERE Id=:controller.docId];
        System.assertEquals('Yes', fd.Request_to_Waive_Requirement__c);

        System.assertNotEquals(0, fd.School_Document_Assignments__r.size());
        System.assertNotEquals('Approved', fd.School_Document_Assignments__r[0].Requirement_Waiver_Status__c);

        //System.assertNotEquals(null, controller.docId);
        controller.sdaId = fd.School_Document_Assignments__r[0].Id;

        Test.startTest();
        controller.doApproveNADoc();
        Test.stopTest();

        School_Document_Assignment__c updatedSDA = [SELECT Id, Requirement_Waiver_Status__c FROM School_Document_Assignment__c WHERE Id=:controller.sdaId];

        System.assertEquals('Approved', updatedSDA.Requirement_Waiver_Status__c);

    }

    // NAIS-2256 [DP] 02.12.2015 - test school denying request
    @isTest
    private static void testNARequestDeny(){
        TestDataLocal td = new TestDataLocal();

        td.pfs1.Filing_Status__c = 'Married, Filing Separately';
        update td.pfs1;

        td.sda1.Document_Type__c =  '1040';
        update td.sda1;

        FamilyDocumentSpringUploadController controller = new FamilyDocumentSpringUploadController();
        controller.pfsRecordParam = [SELECT Id, Academic_Year_Picklist__c, Parent_A__c, Parent_A__r.Household__c, Parent_A__r.Household__r.Name,
                                            Parent_A__r.Name, Parent_A_Email__c, Parent_A_First_Name__c, PFS_Number__c
                                        FROM PFS__c WHERE Id = :td.pfs1.Id];
        controller.academicYearIdParam = td.academicYearId;
        controller.documentSourceParam = 'Uploaded by Parent';
        controller.returnUrlParam = 'http://test.com';
        controller.docYear = td.sda1.Document_Year__c;
        controller.docType = td.sda1.Document_Type__c;
        controller.docId = null;
        controller.schoolCode = null;
        controller.createSchoolAsmnt = false;
        controller.pertainsTo = 'Parent A';
        controller.springCMDocType = td.sda1.Document_Type__c;
        controller.naReason = 'Test Reason';


        controller.doInitNADialog();
        System.assertEquals('NA_DOC', controller.dialogPage);

        controller.doSetDocNA();
        Family_Document__c fd = [SELECT Id,Request_to_Waive_Requirement__c, (Select Id, Requirement_Waiver_Status__c FROM School_Document_Assignments__r) FROM Family_Document__c WHERE Id=:controller.docId];
        School_Document_Assignment__c sda4 = TestUtils.createSchoolDocumentAssignment('TestSDA4', fd.Id, td.rd1.Id, td.spfsa1.Id, true);
        System.assertEquals('Yes', fd.Request_to_Waive_Requirement__c);

        System.assertNotEquals(0, fd.School_Document_Assignments__r.size());
        System.assertNotEquals('Approved', fd.School_Document_Assignments__r[0].Requirement_Waiver_Status__c);

        //System.assertNotEquals(null, controller.docId);
        controller.sdaId = fd.School_Document_Assignments__r[0].Id;

        //[G.S], SFP-21
        List<School_Document_Assignment__c> sdaList = [select Id from School_Document_Assignment__c where Document__c = :controller.docId and Requirement_Waiver_Status__c = 'Not Approved'];
        System.assertEquals(0,sdaList.size());

        Test.startTest();
        controller.doDenyNADoc();
        Test.stopTest();

        School_Document_Assignment__c updatedSDA = [SELECT Id, Requirement_Waiver_Status__c FROM School_Document_Assignment__c WHERE Id=:controller.sdaId];
        System.assertEquals('Not Approved', updatedSDA.Requirement_Waiver_Status__c);

        sdaList = [select Id from School_Document_Assignment__c where Document__c = :controller.docId and Requirement_Waiver_Status__c = 'Not Approved'];
        System.assertEquals(2,sdaList.size());

    }

    // NAIS-2256 [DP] 02.12.2015 - test school denying request
    @isTest 
    private static void testIssueReport(){
        TestDataLocal td = new TestDataLocal();

        td.pfs1.Filing_Status__c = 'Married, Filing Separately';
        update td.pfs1;

        td.sda1.Document_Type__c =  '1040';
        update td.sda1;

        FamilyDocumentSpringUploadController controller = new FamilyDocumentSpringUploadController();
        controller.pfsRecordParam = [SELECT Id, Academic_Year_Picklist__c, Parent_A__c, Parent_A__r.Household__c, Parent_A__r.Household__r.Name,
                                            Parent_A__r.Name, Parent_A_Email__c, Parent_A_First_Name__c, PFS_Number__c
                                        FROM PFS__c WHERE Id = :td.pfs1.Id];
        controller.academicYearIdParam = td.academicYearId;
        controller.documentSourceParam = 'Uploaded by Parent';
        controller.returnUrlParam = 'http://test.com';
        controller.docYear = td.sda1.Document_Year__c;
        controller.docType = td.sda1.Document_Type__c;
        controller.docId = null;
        controller.schoolCode = null;
        controller.createSchoolAsmnt = false;
        controller.pertainsTo = 'Parent A';
        controller.springCMDocType = td.sda1.Document_Type__c;
        controller.naReason = 'Test Reason';


        controller.doInitIssueSetDialog();
        System.assertEquals('ISSUE_DOC_SET', controller.dialogPage);

        controller.famDocIllegible = true;

        controller.doReportDocIssue();
        Family_Document__c fd = [SELECT Id, Issue_Categories__c FROM Family_Document__c WHERE Id = :td.fd1.Id];
        System.assert(String.isBlank(fd.Issue_Categories__c));

        controller.docId = td.fd1.Id;
        controller.famDocIllegible = false;

        controller.doReportDocIssue();
        fd = [SELECT Id, Issue_Categories__c FROM Family_Document__c WHERE Id = :td.fd1.Id];
        System.assert(String.isBlank(fd.Issue_Categories__c));

        controller.famDocIllegible = true;

        Test.startTest();
        controller.doReportDocIssue();
        Test.stopTest();

        fd = [SELECT Id, Issue_Categories__c FROM Family_Document__c WHERE Id = :td.fd1.Id];
        System.assert(!String.isBlank(fd.Issue_Categories__c));
        System.assertEquals(SpringCMAction.illegibleValue, fd.Issue_Categories__c);

    }

    // SFP-29 START
    @isTest 
    private static void testDBVerifyFlag() {
        TestDataLocal td = new TestDataLocal();

        td.sda1.Verify_Data__c = true;
        td.sda2.Verify_Data__c = false;
        td.sda3.Verify_Data__c = false;
        td.sda3.Document__c = td.fd1.Id;
        update new List<School_Document_Assignment__c> { td.sda1, td.sda2, td.sda3 };

        Test.startTest();

        String flag1, flag2;

        ApexPages.currentPage().getParameters().put('schoolPFSAssignmentId', td.spfsa1.Id);

        FamilyDocumentSpringUploadController cont = new FamilyDocumentSpringUploadController();
        cont.academicYearIdParam = td.academicYearId;
        cont.docId = td.fd1.Id;
        flag1 = cont.DBVerifyFlag;

        cont = new FamilyDocumentSpringUploadController();
        cont.academicYearIdParam = td.academicYearId;
        cont.docId = td.fd2.Id;
        flag2 = cont.DBVerifyFlag;

        Test.stopTest();

        System.assertEquals('T', flag1);
        System.assertEquals('F', flag2);
    }
    // SFP-29 END

    @isTest
    private static void testRemoveDoc() {
        School_PFS_Assignment__c spfsa = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(GlobalVariables.getCurrentAcademicYear().Name).insertSchoolPfsAssignment();
        Family_Document__c familyDocument = FamilyDocumentTestData.Instance.DefaultFamilyDocument;
        School_Document_Assignment__c sda = SchoolDocumentAssignmentTestData.Instance
            .forPfsAssignment(spfsa.Id)
            .forDocument(familyDocument.Id)
            .forDocumentYear(GlobalVariables.getCurrentAcademicYear().Name)
            .verifyData()
            .insertSchoolDocumentAssignment();
        
        List<Family_Document__c> familyDocuments = FamilyDocumentsSelector.newInstance()
            .selectByIdWithSchoolDocumentAssignmentsWithCustomFieldLists(
                new Set<Id>{sda.Document__c}, new List<String>{'Id', 'Deleted__c'}, new List<String>{'Id'}, false);
        System.assertEquals(1, familyDocuments.size());
        System.assertEquals(false, familyDocuments[0].Deleted__c);
        System.assertEquals(1, familyDocuments[0].School_Document_Assignments__r.size());

        FamilyDocumentSpringUploadController ctrl = new FamilyDocumentSpringUploadController();
        ctrl.docId = familyDocument.Id;

        Test.startTest();
            ctrl.removeDoc();

            familyDocuments = FamilyDocumentsSelector.newInstance()
                .selectByIdWithSchoolDocumentAssignmentsWithCustomFieldLists(
                    new Set<Id>{sda.Document__c}, new List<String>{'Id', 'Deleted__c'}, new List<String>{'Id'}, false);
        Test.stopTest();

        System.assertEquals(1, familyDocuments.size());
        System.assertEquals(true, familyDocuments[0].Deleted__c);
        System.assertEquals(0, familyDocuments[0].School_Document_Assignments__r.size());
    }

    @isTest
    private static void testInFamilyPortal() {
        User newFamilyPortalUser = UserTestData.insertFamilyPortalUser();

        Contact newContact = ContactTestData.Instance.insertContact();
        User newSchoolPortalUser = UserTestData.createSchoolPortalUser();
        newSchoolPortalUser.ContactId = newContact.Id;
        insert newSchoolPortalUser;

        Test.startTest();
            System.runAs(newFamilyPortalUser) {
                FamilyDocumentSpringUploadController ctrl = new FamilyDocumentSpringUploadController();
                System.assertEquals(true, ctrl.isFamilyPortal);
            }

            System.runAs(newSchoolPortalUser) {
                FamilyDocumentSpringUploadController ctrl = new FamilyDocumentSpringUploadController();
                System.assertEquals(false, ctrl.isFamilyPortal);
            }
        Test.stopTest();
    }
}