/**
 * MassEmailUtilTest.cls
 *
 * @description: Test class for MassEmailUtil using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 * NOTE: this is just to get coverage for a targeted deploy, custom metadata types are still not createable during test coverage and so you must
 * assume real values are present, to avoid future problems no asserts are performed.
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
public class MassEmailUtilTest {

    /* test data setup */
    @testSetup 
    static void setupTestData() {
        
        MassEmailSendTestDataFactory.createEmailTemplate();
    }

    /* negative test cases */

    // test verifyMergeFields w/ valid merge settings but invalid merge fields
	@isTest 
	static void verifyMergeFields_withInvalidMergeInBody_invalidMergeFields() {
		
		// Arrange
		MassEmailSendTestDataFactory tData;
		User testUser = [select Id from User where Id = :UserInfo.getUserId()];
		System.runAs( testUser) {
		 	
		 	tData = new MassEmailSendTestDataFactory( false);
		}

		// Act
		System.runAs( tData.portalUser) {
			
			MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
			MassEmailSendTestDataFactory.createMassEmailMergeSettings();
			Test.startTest();
				
				// Must create revelant test data records as portal user so that portal user owns records and they
				// are visible during test execution
				String messageBody = 'Hello {{PareFN}} {{ParensALN}} this is an invalid merge';
				Set<String> mergeErrorSet = MassEmailUtil.verifyMergeFields( messageBody);
			Test.stopTest();

			// Assert
			System.assert( !mergeErrorSet.isEmpty());
		}
	}

    
    /* positive test cases */

    // Attempt isMassEmailGloballyEnabledByAccount with valid param
    @isTest 
    static void isMassEmailGloballyEnabledByAccount_withId_validExecution() {
        
        // Arrange
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
        }
        Account acct = [select Id from Account where Name = :MassEmailSendTestDataFactory.TEST_SCHOOL_NAME];

        // Act
        MassEmailUtil.isMassEmailGloballyEnabledByAccount( acct.Id);

        // Assert
        // no work
    }


    // Attempt isMassEmailGloballyEnabledByAccount with valid params
    @isTest 
    static void isMassEmailGloballyEnabledByAccount_withIds_validExecution() {
        
        // Arrange
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
        }
        Account acct = [select Id from Account where Name = :MassEmailSendTestDataFactory.TEST_SCHOOL_NAME];
        acct.Mass_Email_Enabled__c = true;
        update acct;
        
        List<Profile> pList = new List<Profile>();
        pList = [select Id from Profile where Name = :ProfileSettings.SchoolAdminProfileName];

        // Act
        if ( pList != null && pList.size() > 0) {

            MassEmailUtil.isMassEmailGloballyEnabledByAccount( acct.Id, pList[0].Id);
        }
    }

    // Attempt isWebhookDebugEnabled
    @isTest 
    static void isWebhookDebugEnabled_noParam_validExecution() {
        
        // Arrange
        // no work

        // Act
        MassEmailUtil.isWebhookDebugEnabled();

        // Assert
        // no work
    }

    // Attempt getImageUploadURL
    @isTest 
    static void getImageUploadURL_noParam_validExecution() {
        
        // Arrange
        // no work

        // Act
        MassEmailUtil.getImageUploadURL();

        // Assert
        // no work
    }

    // Attempt writeToIntegrationLog with valid params
    @isTest 
    static void writeToIntegrationLog_withParams_validExecution() {
        
        // Arrange
        // no work

        // Act
        MassEmailUtil.writeToIntegrationLog( 'TEST', 'TEST', 'TEST');

        // Assert
        // no work
    }

    // test createMergeMap w/ valid merge settings
	@isTest 
	static void createMergeMap_withMergeSettingData_validMergeMap() {
		
		// Arrange
		MassEmailSendTestDataFactory tData;
		User testUser = [select Id from User where Id = :UserInfo.getUserId()];
		System.runAs( testUser) {
		 	
		 	tData = new MassEmailSendTestDataFactory( false);
		}

		// Act
		System.runAs( tData.portalUser) {
			
			MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
			MassEmailSendTestDataFactory.createMassEmailMergeSettings();
			Test.startTest();
				
				// Must create revelant test data records as portal user so that portal user owns records and they
				// are visible during test execution
				MassEmailSendTestDataFactory.createRequiredPFSData();

				Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
											     	Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
											     	Html_Body__c, Footer__c, Reply_To_Address__c
										       from Mass_Email_Send__c
										      where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];

				Contact c = [select Id, Name
							   from Contact
							  where Email = :MassEmailSendTestDataFactory.TEST_PARENT1_CONTACT_EMAIL];

				Contact contactStudent = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_STUDENT1_CONTACT_EMAIL];
				Applicant__c app = [select Id from Applicant__c where Contact__c = :contactStudent.Id];
				School_PFS_Assignment__c spa = [select Id, Name, Parent_A_First_Name__c, Parent_A_Last_Name__c
												  from School_PFS_Assignment__c
												 where Applicant__c = :app.Id]; 
				Map<String,String> mergeDataMap = MassEmailUtil.createMergeMap( spa);
			Test.stopTest();

			// Assert
			System.assert( !mergeDataMap.isEmpty());
		}
	}

	// test verifyMergeFields w/ valid merge settings
	@isTest 
	static void verifyMergeFields_withMergeInBody_validMergeFields() {
		
		// Arrange
		MassEmailSendTestDataFactory tData;
		User testUser = [select Id from User where Id = :UserInfo.getUserId()];
		System.runAs( testUser) {
		 	
		 	tData = new MassEmailSendTestDataFactory( false);
		}

		// Act
		System.runAs( tData.portalUser) {
			
			MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
			MassEmailSendTestDataFactory.createMassEmailMergeSettings();
			Test.startTest();
				
				// Must create revelant test data records as portal user so that portal user owns records and they
				// are visible during test execution
				String messageBody = 'Hello {{ParentAFN}} {{ParentALN}} this is a valid merge';
				Set<String> mergeErrorSet = MassEmailUtil.verifyMergeFields( messageBody);
			Test.stopTest();

			// Assert
			System.assert( mergeErrorSet.isEmpty());
		}
	}

	// test replaceMergeFields w/ valid merge settings
	@isTest 
	static void replaceMergeFields_withMergeFields_validMerge() {
		
		// Arrange
		MassEmailSendTestDataFactory tData;
		User testUser = [select Id from User where Id = :UserInfo.getUserId()];
		System.runAs( testUser) {
		 	
		 	tData = new MassEmailSendTestDataFactory( false);
		}

		// Act
		System.runAs( tData.portalUser) {
			
			MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
			MassEmailSendTestDataFactory.createMassEmailMergeSettings();
			Test.startTest();
				
				// Must create revelant test data records as portal user so that portal user owns records and they
				// are visible during test execution
				MassEmailSendTestDataFactory.createRequiredPFSData();

				Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
											     	Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
											     	Html_Body__c, Footer__c, Reply_To_Address__c
										       from Mass_Email_Send__c
										      where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];

				Contact c = [select Id, Name
							   from Contact
							  where Email = :MassEmailSendTestDataFactory.TEST_PARENT1_CONTACT_EMAIL];

				Contact contactStudent = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_STUDENT1_CONTACT_EMAIL];
				Applicant__c app = [select Id from Applicant__c where Contact__c = :contactStudent.Id];
				School_PFS_Assignment__c spa = [select Id, Name, Parent_A_First_Name__c, Parent_A_Last_Name__c
												  from School_PFS_Assignment__c
												 where Applicant__c = :app.Id]; 
				spa.Parent_A_First_Name__c = 'Ted';
				spa.Parent_A_Last_Name__c = 'Testerson';

				Map<String,String> mergeDataMap = MassEmailUtil.createMergeMap( spa);
				String messageBody = 'Hello {{ParentAFN}} {{ParentALN}} this is a valid merge';
				messageBody = MassEmailUtil.replaceMergeFields( messageBody, mergeDataMap);
			Test.stopTest();

			// Assert
			System.assertEquals( 'Hello Ted Testerson this is a valid merge', messageBody);
		}
	}

}