/*
* SPEC-140, NAIS-66
* Contains handler methods and static variables for processing related
*  to updates of Professional Judgement settings
*
* CH, Exponent Partners 04/30/2013
*
*/
public with sharing class SchoolProfJudgement {
    /* Static Variables */
    public static Boolean isCalculatingEFC {
        get {
            if (isCalculatingEFC == null) {
                return false;
            } else {
                return isCalculatingEFC;
            }
        }
        set;
    }
    /* End Static Variables */

    /* Trigger Handlers */
    // Runs EFC re-calculations in chunks of a size specified by
    //  the Batch_Size_Settings__c custom settings
    public static List<String> runEFCRecalculations(List<School_PFS_Assignment__c> listToProcess) {
        Integer mainIterator = 0;
        List<String> processReport = new List<String>{};

        // Loop until we hit the end of the list
        while (mainIterator < listToProcess.size()) {
            Set<Id> idsToProcess = new Set<Id>{};
            Integer batchSize = getEFCBatchSize();

            // Iterate to create one batch of id values
            for(Integer i=0; i<batchSize; i++){
                idsToProcess.add(listToProcess[mainIterator].Id);

                mainIterator += 1;
                // If this iteration is incomplete but we've hit
                //  the end of the record list
                if(mainIterator == listToProcess.size()){
                    break;
                }
            }

            runEFCCalculationBatch(idsToProcess);
            processReport.add('Processing request for chunk of ' + idsToProcess.size() + ' at ' + String.valueOf(DateTime.now()));
        }

        return processReport;
    }
    /* End Trigger Handlers */

    /* Helper Methods */
    // Helper method for getting the current EFC batch size, with a built-in default
    @testVisible
    private static Integer getEFCBatchSize() {
        if (Batch_Size_Settings__c.getValues('EFC') != null) {
            return Integer.valueOf(Batch_Size_Settings__c.getValues('EFC').Batch_Size__c);
        } else {
            return 1000;
        }
    }

    @future
    private static void runEFCCalculationBatch(Set<Id> idsToProcess) {
        SchoolProfJudgement.isCalculatingEFC = true;

           // Call EfcCalculator methods to re-calculate for the current batch
        EfcCalculator efcCalc = new EfcCalculator();
        try {
            efcCalc.calculateEfc(
                EfcDataManager.createEfcWorksheetDataForSchoolPFSAssignments(idsToProcess)
            );
        } catch (EfcException e) {}

        SchoolProfJudgement.isCalculatingEFC = false;
    }
    /* End Helper Methods */
}