public with sharing class TableEmploymentAllowance
{
    
    private static Map<Id, List<Employment_Allowance__c>> employmentAllowanceDataByYear = new Map<Id, List<Employment_Allowance__c>>();
    
    public static List<Employment_Allowance__c> getEmploymentAllowanceData(Id academicYearId) {
        List<Employment_Allowance__c> employmentAllowanceData = employmentAllowanceDataByYear.get(academicYearId);
        if (employmentAllowanceData == null) {
            employmentAllowanceData = [SELECT Id, Income_Low__c, Income_High__c, Allowance_Base_Amount__c, Allowance_Rate__c,
                                                Allowance_Threshold__c, Has_Maximum_Allowance__c
                                        FROM Employment_Allowance__c
                                        WHERE Academic_Year__c = :academicYearId
                                        ORDER BY Income_Low__c ASC];
            employmentAllowanceDataByYear.put(academicYearId, employmentAllowanceData);
        }
        
        return employmentAllowanceData;
    }
    
    public static Decimal calculateAllowance(Id academicYearId, Decimal income) {
        Decimal maxAllowance = EfcConstants.getEmploymentAllowanceMaximum(academicYearId);
        Decimal calculatedAllowance = null;
        
        // make sure the income is rounded to the nearest whole number
        Decimal roundedIncome = income.round(RoundingMode.HALF_UP);
        List<Employment_Allowance__c> employmentAllowanceData = getEmploymentAllowanceData(academicYearId);
        if (employmentAllowanceData != null) {
            Employment_Allowance__c matchingEmploymentAllowance = null;
            // assume the table is sorted in ascending order by the Income_Low__c field
            for (Employment_Allowance__c employmentAllowance : employmentAllowanceData) {
                if (roundedIncome >= employmentAllowance.Income_Low__c) {
                    if ((employmentAllowance.Income_High__c == null) || (roundedIncome <= employmentAllowance.Income_High__c)) {
                        matchingEmploymentAllowance = employmentAllowance;
                        break;
                    }
                }
            }
            
            // do the calculation
            if (matchingEmploymentAllowance != null) {
                // get the amount above the threshold                
                Decimal incomeAboveThreshold = (roundedIncome > matchingEmploymentAllowance.Allowance_Threshold__c) 
                                                    ? (roundedIncome - matchingEmploymentAllowance.Allowance_Threshold__c) : 0;
                
                // apply the base and rate
                calculatedAllowance = (matchingEmploymentAllowance.Allowance_Base_Amount__c
                                        + (matchingEmploymentAllowance.Allowance_Rate__c * incomeAboveThreshold)).round(RoundingMode.HALF_UP);
                // make sure it's not above the max allowance
                if (calculatedAllowance > maxAllowance) {
                    calculatedAllowance = maxAllowance;
                }
            }
        }
        
        if (calculatedAllowance == null) {
            throw new EfcException('No Employment Allowance table record found for academic year \'' + EfcUtil.getAcademicYearName(academicYearId) + '\' for income = ' + income);
        }
        
        return calculatedAllowance;
    }
}