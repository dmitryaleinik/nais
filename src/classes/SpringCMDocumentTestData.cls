/**
 * @description 
 */
@isTest
public class SpringCMDocumentTestData extends SObjectTestData {

    private static final String DEFAULT_DOC_YEAR = '2016';
    private static final String DEFAULT_PFS_NUMBER = '18-asfdqwer';
    private static final String DEFAULT_IMPORT_ID = 'xxxxxx-zzzz-dddd-bbbaaa';
    private static final String DOC_TYPE_W2 = 'W2';
    private static final String DOC_STATUS_PROCESSED = 'Processed';

    private SpringCMDocumentTestData() { }

    /**
     * @description Get the default values for the SpringCM_Document__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                SpringCM_Document__c.Document_Year__c => DEFAULT_DOC_YEAR,
                SpringCM_Document__c.PFS_Number__c => DEFAULT_PFS_NUMBER,
                SpringCM_Document__c.Import_ID__c => DEFAULT_IMPORT_ID,
                SpringCM_Document__c.Document_Type__c => DOC_TYPE_W2
        };
    }

    /**
     * @description Sets the Import Id field.
     * @param importId The import Id to use.
     * @return The current instance of the class.
     */
    public SpringCMDocumentTestData withImportId(String importId) {
        return (SpringCMDocumentTestData)with(SpringCM_Document__c.Import_ID__c, importId);
    }

    /**
     * @description Sets the Document Source field.
     * @param source The source to use.
     * @return The current instance of the class.
     */
    public SpringCMDocumentTestData withSource(String source) {
        return (SpringCMDocumentTestData)with(SpringCM_Document__c.Document_Source__c, source);
    }

    /**
     * @description Sets the Date Verified field.
     * @param verifiedDate The date to use.
     * @return The current instance of the class.
     */
    public SpringCMDocumentTestData verifiedOn(Date verifiedDate) {
        return (SpringCMDocumentTestData)with(SpringCM_Document__c.Date_Verified__c, verifiedDate);
    }

    /**
     * @description Sets the PFS Number field.
     * @param pfsNumber The pfs number to use.
     * @return The current instance of the class.
     */
    public SpringCMDocumentTestData forPfs(String pfsNumber) {
        return (SpringCMDocumentTestData)with(SpringCM_Document__c.PFS_Number__c, pfsNumber);
    }

    /**
     * @description Sets the Document Year field.
     * @param docYear The document year to use.
     * @return The current instance of the class.
     */
    public SpringCMDocumentTestData forDocumentYear(String docYear) {
        return (SpringCMDocumentTestData)with(SpringCM_Document__c.Document_Year__c, docYear);
    }

    /**
     * @description Sets the Document Type field.
     * @param docType The document type to use.
     * @return The current instance of the class.
     */
    public SpringCMDocumentTestData forDocumentType(String docType) {
        return (SpringCMDocumentTestData)with(SpringCM_Document__c.Document_Type__c, docType);
    }

    /**
     * @description Sets the family document Id field.
     * @param docType The family document Id to use.
     * @return The current instance of the class.
     */
    public SpringCMDocumentTestData forFamilyDocumentId(String fdId) {
        return (SpringCMDocumentTestData)with(SpringCM_Document__c.Family_Document_Id__c, fdId);
    }
    
    /**
     * @description Sets the W2 Wages field.
     * @param wages The wages to use.
     * @return The current instance.
     */
    public SpringCMDocumentTestData withW2Wages(Decimal wages) {
        return (SpringCMDocumentTestData)with(SpringCM_Document__c.W2_Wages__c, wages);
    }

    /**
     * @description Sets the Document Status field.
     * @param status The status to use.
     * @return The current instance of the class.
     */
    public SpringCMDocumentTestData withDocStatus(String status) {
        return (SpringCMDocumentTestData)with(SpringCM_Document__c.Document_Status__c, status);
    }

    /**
     * @description Sets the Document Status field to Processed.
     * @return The current instance of the class.
     */
    public SpringCMDocumentTestData isProcessed() {
        return withDocStatus(DOC_STATUS_PROCESSED);
    }

    /**
     * @description Sets the doc type to W2 and fills in some mock verification data for the W2.
     * @return The current instance of the class.
     */
    public SpringCMDocumentTestData asVerifiedW2() {
        return forDocumentType(DOC_TYPE_W2).withW2Wages(120000).verifiedOn(System.today()).isProcessed();
    }

    /**
     * @description Sets the ISSUE_Mislabeled Document field to Yes.
     * @return The current instance of the class.
     */
    public SpringCMDocumentTestData isMislabeled() {
        return (SpringCMDocumentTestData)with(SpringCM_Document__c.ISSUE_Mis_labeled_Document__c, 'Yes');
    }

    /**
     * @description Sets the First_Name__c field.
     * @return The current instance of the class.
     */
    public SpringCMDocumentTestData withFirstName(String firstName) {
        return (SpringCMDocumentTestData)with(SpringCM_Document__c.First_Name__c, firstName);
    }

    /**
     * @description Sets the Last_Name__c field.
     * @return The current instance of the class.
     */
    public SpringCMDocumentTestData withLastName(String lastName) {
        return (SpringCMDocumentTestData)with(SpringCM_Document__c.Last_Name__c, lastName);
    }

    /**
     * @description Sets the Name__c field which is used to store the full name of the person the document is for.
     * @return The current instance of the class.
     */
    public SpringCMDocumentTestData withFullName(String fullName) {
        return (SpringCMDocumentTestData)with(SpringCM_Document__c.Name__c, fullName);
    }

    /**
     * @description Sets the Document_Pertains_to__c field. This field is used to indicate who the doc is for such as
     *              Parent A, Parent B, Student, or Dependent.
     * @return The current instance of the class.
     */
    public SpringCMDocumentTestData pertainsTo(String docPertainsTo) {
        return (SpringCMDocumentTestData)with(SpringCM_Document__c.Document_Pertains_to__c, docPertainsTo);
    }

    /**
     * @description Creates a SpringCM Document.
     * @return A SpringCM Document.
     */
    public SpringCM_Document__c create() {
        return (SpringCM_Document__c)buildWithoutReset();
    }

    /**
     * @description Inserts a SpringCM Document.
     * @return A SpringCM Document record.
     */
    public SpringCM_Document__c insertScmDocument() {
        return (SpringCM_Document__c)insertRecord();
    }

    /**
     * @description Gets the SpringCM_Document__c SObjectType.
     * @return The SpringCM_Document__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return SpringCM_Document__c.SObjectType;
    }

    /**
     * @description Singleton used to generate test data.
     */
    public static SpringCMDocumentTestData Instance {
        get {
            if (Instance == null) {
                Instance = new SpringCMDocumentTestData();
            }
            return Instance;
        }
        private set;
    }
}