/**
 * TriggerHelper.cls
 *
 * @description Helper class for limiting trigger executions per transaction
 *
 * @author Chase Logan @ Presence PG
 */
global class TriggerHelper {

    /* private statics, defaulted to false indicating trigger has not yet run */
    private static Boolean massEmailSendTriggerHasRun = false;
    private static Boolean schoolPFSAssignmentTriggerHasRun = false;
    
    
    /* mutators for getting/setting private statics */
    public static Boolean getMassEmailSendTriggerHasRun() {

        return massEmailSendTriggerHasRun;
    }

    public static void setMassEmailSendTriggerHasRun( Boolean value) {

        massEmailSendTriggerHasRun = value;
    }

    public static Boolean getSchoolPFSAssignmentTriggerHasRun() {

        return schoolPFSAssignmentTriggerHasRun;
    }

    public static void setSchoolPFSAssignmentTriggerHasRun( Boolean value) {

        schoolPFSAssignmentTriggerHasRun = value;
    }

}