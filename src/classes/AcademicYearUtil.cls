/**
 * @description This class has several utility methods related to academic year records. It is not meant to do any DML
 *              or SOQL so don't do those things in this class.
 */
public class AcademicYearUtil {

    @testVisible private static final String ACADEMIC_YEAR_PARAM = 'academicYear';
    @testVisible private static final String ACADEMIC_YEAR_NAME_PARAM = 'academicYearName';

    /**
     * @description Determines whether or not an academic year is for the 2017-2018 academic term or later. This is
     *              done by looking at the name of the academic year record, grabbing the first four characters, then
     *              converting them to an integer. If they are greater than or equal to 2017, then the method returns
     *              true.
     * @param academicYear The record to evaluate.
     * @return True if the academic year is 2017-2018 or later.
     * @throws ArgumentNullException if academicYear is null.
     */
    public static Boolean is20172018OrLater(Academic_Year__c academicYear) {
        ArgumentNullException.throwIfNull(academicYear, ACADEMIC_YEAR_PARAM);
        // Get the academic year record and determine what the start year is by using the first four characters of its name.
        // Academic Year record names follow the format {Start Year}-{End Year}. For example, 2017-2018.
        Integer academicStartYear = getAcademicTermStartYear(academicYear);

        return academicStartYear >= 2017;
    }

    /**
     * @description Determines whether or not an academic year is for the 2018-2019 academic term or later. This is
     *              done by looking at the name of a academic year record, grabbing the first four characters, then
     *              converting them to an integer. If they are greater than or equal to 2018, then the method returns
     *              true.
     * @param academicYearName The academic year name to evaluate.
     * @return True if the academic year name is 2018-2019 or later.
     */
    public static Boolean is20182019OrLater(String academicYearName) {
        if (String.isBlank(academicYearName)) {
            return false;
        }
        // Get the academic year record and determine what the start year is by using the first four characters of its name.
        // Academic Year record names follow the format {Start Year}-{End Year}. For example, 2017-2018.
        Integer academicStartYear = Integer.valueOf(academicYearName.left(4));

        return academicStartYear >= 2018;
    }

    private static Integer getAcademicTermStartYear(Academic_Year__c academicYear) {
        String academicYearName = academicYear.Name;

        return Integer.valueOf(academicYearName.left(4));
    }
}