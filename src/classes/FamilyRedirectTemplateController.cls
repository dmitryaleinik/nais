public class FamilyRedirectTemplateController
{
    
    public FamilyRedirectTemplateController(FamilyTemplateController c)
    {
    }//End-Constructor
    
    public String getCommunityURL()
    {
        return calculateCommunityURL(SchoolPagesUtils.getPageName(ApexPages.currentPage())
                                            +getPageParametersAsString(ApexPages.currentPage()));
    }//End:getCommunityURL
    
    public static String getPageParametersAsString(PageReference thisPage)
    {
        String params = '';
        Map<String,String> allParams = thisPage.getParameters();
        for(String s:allParams.keyset()){
            if(s!='redirectToCommunity')
                params += (params!=''?'&':'?')+s+'='+allParams.get(s);
        }
        return params;
    }//End:getPageParametersAsString
    
    public static String calculateCommunityURL(String thisPageName)//thisPageName in lowercase
    {
        String url;
        if(String.isBlank(thisPageName)) {
            url = getFamilyCommunityUrl() + getPageParametersAsString(ApexPages.currentPage());
        } else {
            url = getFamilyCommunityURL()+'/'+thisPageName;
        }
        url = url.replace('/familyportal//familyportal/','/familyportal/');
        return 'https://'+url;
    }//End:calculateCommunityURL
    
    public static String getFamilyCommunityURL()
    {
        List<Domain> d = new List<Domain>([SELECT Domain, DomainType FROM Domain limit 1]);
        List<Network> n = new List<Network>([SELECT Name, UrlPathPrefix FROM Network where name like '%family%' limit 1]);
        return (!d.isEmpty() && !n.isEmpty()? 
                d[0].Domain+'/'+n[0].UrlPathPrefix:'');
    }//End:getFamilyCommunityURL
}//End-Class