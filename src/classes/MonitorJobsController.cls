/**
 * @description A controller for the MonitorJobs.page that will be used to poll the status of an async job.
 */
public class MonitorJobsController {

    private static final Set<String> COMPLETE_STATUSES = new Set<String> { 'Aborted', 'Completed', 'Failed' };

    /**
     * @description This property stores the Id of the job we are trying to monitor.
     */
    public String JobIdInput { get; set; }

    public JobStatus JobToMonitor { get; private set; }

    public Boolean getIsMonitoringJob() {
        System.debug('&&& Check if we should display job details: ' + (JobToMonitor != null));
        return JobToMonitor != null;
    }

    public PageReference loadApexJob() {
        System.debug('&&& Begin loadApexJob');
        if (String.isBlank(JobIdInput)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You must specify a job Id.'));
            return null;
        }

        Id jobId;
        try {
            jobId = Id.valueOf(JobIdInput);
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The value you input is not a valid Id.'));
            return null;
        }

        AsyncApexJob apexJob = selectAsyncApexJobById(jobId);

        if (apexJob == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'The apex job specified could not be found.'));
            return null;
        }

        JobToMonitor = new JobStatus(apexJob);

        return null;
    }

    public PageReference refreshJobStatus() {
        if (getIsMonitoringJob()) {
            JobToMonitor.refresh();
        }

        return null;
    }

    private static AsyncApexJob selectAsyncApexJobById(Id apexJobId) {
        return [SELECT Id, TotalJobItems, JobItemsProcessed, NumberOfErrors, MethodName, JobType, Status, ExtendedStatus FROM AsyncApexJob WHERE Id = :apexJobId];
    }

    public class JobStatus {

        private AsyncApexJob apexJob;

        public JobStatus(AsyncApexJob jobToMonitor) {
            apexJob = jobToMonitor;
        }

        public Integer getNumberOfBatches() {
            return apexJob.TotalJobItems;
        }

        public Integer getNumberOfProcessedBatches() {
            return apexJob.JobItemsProcessed;
        }

        public Integer getNumberOfErrors() {
            return apexJob.NumberOfErrors;
        }

        public String getApexMethodName() {
            return apexJob.MethodName;
        }

        public String getJobType() {
            return apexJob.JobType;
        }

        public String getStatus() {
            return apexJob.Status;
        }

        public String getErrorDetails() {
            return apexJob.ExtendedStatus;
        }

        public Boolean getHasErrors() {
            return getNumberOfErrors() > 0;
        }

        public Boolean getIsComplete() {
            return COMPLETE_STATUSES.contains(apexJob.Status);
        }

        public void refresh() {
            this.apexJob = selectAsyncApexJobById(apexJob.Id);
        }

        public Integer getPercentComplete() {
            if (getNumberOfBatches() == null || getNumberOfProcessedBatches() == null || getNumberOfBatches() == 0) {
                return 0;
            }

            Double percentComplete = getNumberOfProcessedBatches() / getNumberOfBatches();

            return Integer.valueOf(Math.floor(percentComplete) * 100);
        }
    }
}