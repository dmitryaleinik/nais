/**
 * @description Handle navigation for the Family PFS process.
 **/
public class FamilyNavigationService
{

    private static final String BUSINESS_ASSETS = 'BusinessAssets';
    private static final String BUSINESS_EXPENSES = 'BusinessExpenses';
    private static final String BUSINESS_FARM = 'BusinessFarm';
    private static final String BUSINESS_INCOME = 'BusinessIncome';
    private static final String BUSINESS_INFORMATION = 'BusinessInformation';
    private static final String BUSINESS_SUMMARY = 'BusinessSummary';
    private static final String PFS_SUBMIT_PAGE = 'PFSSubmit';
    private static final String MIE = 'MonthlyIncomeExpensesMain';

    private static final String YES = 'Yes';
    private static final String COMPLETE = 'Complete';
    private static final String BF_ID_PARAM = 'bfId';

    @testVisible private static final String SETTING_PARAM = 'setting';
    @testVisible private static final String APP_UTILS_PARAM = 'appUtils';
    @testVisible private static final String NEXT_PAGE_PARAM = 'nextPage';
    @testVisible private static final String NO_BUSINESS_FARMS_ERROR = 'There were no business farms to navigate to.';

    /**
     * @description Retrieve the next section based on the current
     *              provided setting.
     * @param setting The current FamilyAppSettings__c record that
     *        will be evaluated.
     * @param appUtils The current instance of ApplicationUtils to
     *        operate with.
     * @returns A FamilyNavigationService.Response object that will
     *          contain the state of the next destination section.
     * @throws An ArgumentNullException when the setting or appUtils
     *         parameters are null.
     */
    public Response getNextSection(FamilyAppSettings__c setting, ApplicationUtils appUtils)
    {
        ArgumentNullException.throwIfNull(setting, SETTING_PARAM);
        ArgumentNullException.throwIfNull(appUtils, APP_UTILS_PARAM);

        return new Response(setting, appUtils);
    }

    /**
     * @description Generate a placeholder FamilyNavigationService.Response
     *              with a given next page.
     * @param nextPage The page to navigate to next.
     * @returns A FamilyNavigationService.Response object populated with the
     *          Next page to navigate to.
     * @throws An ArgumentNullException when nextPage is null.
     */
    public Response getNextSection(String nextPage)
    {
        ArgumentNullException.throwIfNull(nextPage, NEXT_PAGE_PARAM);

        return new Response(nextPage);
    }

    /**
     * @description The response that is returned when interacting with
     *              the FamilyNavigationService.
     **/
    public class Response
    {
        private ApplicationUtils appUtils;
        private PFS__c pfs;
        private FamilyAppSettings__c currentFamilyAppSetting;
        private Id currentBusinessFarmId; // The Current Business Farm Id on the page entering this service.
        private Id activeBusinessFarmId; // The Business Farm Id actively being interrogated in the service.
        private FamilyAppSettings__c monthlyIncomeExpensesbump;

        /**
         * @description The next outstanding page if there is an outstanding
         *              page left for the user to fill out, otherwise return
         *              the next linear page.
         */
        public String NextPage
        {
            get {
                if (NextPage == null)
                {
                    NextPage = getNextOutstandingScreen(currentFamilyAppSetting);

                    if (NextPage == PFS_SUBMIT_PAGE && currentFamilyAppSetting.Next_Page__c != PFS_SUBMIT_PAGE)
                    {
                        NextPage = getNextAbsoluteScreen(currentFamilyAppSetting);
                    }
                }
                return NextPage;
            }
            private set;
        }

        /**
         * @description If the NextPage is a part of the Business Farm section
         *              then this will be set to the BusinessFarm that is next
         *              to be interacted with.
         */
        public Business_Farm__c BusinessFarm {get; private set;}

        /**
         * @description A generic response constructor allowing this object to
         *              be populated with a desired next page and returned for
         *              consistent operation.
         * @param nextPage The Next Page to navigate to.
         * @throws An ArgumentNullException if nextPage is null.
         */
        public Response(String nextPage)
        {
            ArgumentNullException.throwIfNull(nextPage, NEXT_PAGE_PARAM);

            this.NextPage = nextPage;
        }

        /**
         * @description Accept in the current FamilyAppSettings__c record that
         *              the user is operating against, and the current instance
         *              of ApplicationUtils, giving access to the current PFS and
         *              Business Farms.
         * @param setting The FamilyAppSettings__c record that the current page has
         *        been generated from.
         * @param appUtils The current page's instantiation of ApplicationUtils that
         *        provides the current PFS and Business Farms to operate against.
         * @throws An ArgumentNullException when either setting or appUtils is null.
         */
        public Response(FamilyAppSettings__c setting, ApplicationUtils appUtils)
        {
            ArgumentNullException.throwIfNull(setting, SETTING_PARAM);
            ArgumentNullException.throwIfNull(appUtils, APP_UTILS_PARAM);

            this.currentFamilyAppSetting = setting;
            this.currentBusinessFarmId = getBusinessFarmId();
            this.appUtils = appUtils;
            this.pfs = ApplicationUtils.queryPFSRecord(appUtils.pfs.Id);
        }

        private String getNextOutstandingScreen(FamilyAppSettings__c setting)
        {
            // If the next page setting that has been passed in is null,
            // return early.
            if (setting == null || setting.Next_Page__c == null || isDashboard())
            {
                return null;
            }

            // Explicitly extract out the conditions to get the next
            // settings next page.
            Boolean shouldSkip = shouldSkipBusinessOrFarm(setting.Name);
            Boolean status = isSectionCompleted(setting);
            //Boolean isBusinessSummary = setting.Name == BUSINESS_SUMMARY;
            Boolean hasLoopback = hasBusinessFarmLoopback(setting);

            // If the current setting is completed and the next page is the submit
            // page, let's exit early since we know that this setting is the end
            // of the line.
            if (status && setting.Next_Page__c == PFS_SUBMIT_PAGE)
            {
                return PFS_SUBMIT_PAGE;
            }

            if (status && hasLoopback) 
            {
                setting = FamilyAppSettings__c.getInstance(setting.Loop_Back_Page__c);
            
                if (setting == null) 
                {
                     return null;
                }

                // Since we've looped back, we want to update the currentBusinessFarm
                // We need to place the business farm Id in the page parameters or we will
                // not have it in scope when we navigate.
                List<Business_Farm__c> businessFarms = this.pfs.Business_Farms__r;
                Id currentbfId = getBusinessFarmId();
                currentbfId = (currentbfId == null) ? businessFarms[0].Id : currentbfId;
                Integer nextSequenceNumber = (new Map<Id, Business_Farm__c>(businessFarms).get(currentbfId)).Sequence_Number__c.intValue();
                Business_Farm__c nextBusinessFarm = businessFarms.get(nextSequenceNumber);
                activeBusinessFarmId = nextBusinessFarm.Id;

                status = isSectionCompleted(setting);
            }

            if (appUtils.isMIERequired != null && appUtils.isMIERequired
                && (status || (!status && setting.Name == BUSINESS_SUMMARY && this.pfs.Business_Farms__r.isEmpty()))
                && setting.Name != 'MonthlyIncomeExpenses' && setting.Name != 'MonthlyIncomeExpensesMain'
                && pfs.StatMonthlyIncomeExpensesMain__c == 'Incomplete'
                && (setting.Next_Page__c == 'OtherConsiderations' || setting.Next_Page__c == 'OtherInformation'))
            {
                return getMonthlyIncomeExpensesbump().Name;
            }

            // If we should get the next settings next page, do so. We are going to
            // null guard the returned next setting, returning the current setting's
            // next page if we don't get anything back.
            if (shouldSkip || status)
            {
                String nextScreen = getNextOutstandingScreen(FamilyAppSettings__c.getInstance(setting.Next_Page__c));

                if (nextScreen == null)
                {
                    return setting.Next_Page__c;
                }

                return nextScreen;
            }

            // In the instance that for some reason we shouldn't get the next setting,
            // let's return the current setting's screen as a safe degradation step.
            // If this occurs, however, it's likely that there is a misconfiguration
            // of FamilyAppSettings__c records.
            String potentialNext = FamilyAppSettings__c.getInstance(setting.Name).Section_Info_page__c;

            if (potentialNext != currentFamilyAppSetting.Name
                && potentialNext != currentFamilyAppSetting.Section_Info_page__c)
            {
                return potentialNext;
            }

            return setting.Name;
        }

        private Id getBusinessFarmId()
        {
            if (activeBusinessFarmId != null) {
                return activeBusinessFarmId;
            }

            Id businessFarmId = currentBusinessFarmId;
            businessFarmId = (businessFarmId == null) ?
                UrlService.Instance.getIdByType(BF_ID_PARAM, Business_Farm__c.getSObjectType()) : businessFarmId;

            return businessFarmId;
        }

        private String getNextAbsoluteScreen(FamilyAppSettings__c setting)
        {
            // If the next page setting that has been passed in is null, return early.
            if (setting == null || setting.Next_Page__c == null || isDashboard())
            {
                return null;
            }
            
            String nextAbsolutePage = setting.Next_Page__c;
            Boolean loopedBack = false;
            if (hasBusinessFarmLoopback(setting, currentBusinessFarmId)) {
                nextAbsolutePage = setting.Loop_Back_Page__c;
                loopedBack = true;
            }

            if (isBusinessFarm(nextAbsolutePage))
            {
                List<Business_Farm__c> businessFarms = this.pfs.Business_Farms__r;
                Map<Id, Business_Farm__c> businessFarmsById = new Map<Id, Business_Farm__c>(businessFarms);

                if (currentBusinessFarmId == null)
                {
                    // We got here by mistake if there are no business farms.
                    // Throw an error and dump out.
                    if (businessFarms.isEmpty())
                    {
                        throw new NavigationException(NO_BUSINESS_FARMS_ERROR);
                    }

                    currentBusinessFarmId = businessFarms[0].Id;
                    BusinessFarm = businessFarms[0];

                    return nextAbsolutePage;
                }

                BusinessFarm = businessFarmsById.get(this.currentBusinessFarmId);

                // hasBusinessFarmLoopback will determine if there are any
                // additional business farms, ensuring we don't attempt to
                // go out of bounds on the businessFarms list here.
                if (loopedBack && businessFarms.size() > 1)
                {
                    Integer nextSequenceNumber = BusinessFarm.Sequence_Number__c.intValue();
                    BusinessFarm = businessFarms.get(nextSequenceNumber);
                }
            }
            
            if( PfsProcessService.Instance.isSectionHidden(pfs.Submission_Process__c,nextAbsolutePage) )
            {
                return getNextAbsoluteScreen(FamilyAppSettings__c.getInstance(nextAbsolutePage));
            }
            
            return nextAbsolutePage;
        }

        private Boolean isDashboard()
        {
            return this.pfs == null;
        }

        public Boolean isBusinessFarm(string currentScreen)
        {
            return (currentScreen == BUSINESS_INFORMATION ||
                    currentScreen == BUSINESS_INCOME ||
                    currentScreen == BUSINESS_FARM ||
                    currentScreen == BUSINESS_EXPENSES ||
                    currentScreen == BUSINESS_ASSETS);
        }

        private Boolean shouldSkipBusinessOrFarm(String currentScreen) {
            // currentScreen == BUSINESS_SUMMARY cannot go inside the isBusinessFarm(currentScreen) method because that
            // method is called elsewhere. With BUSINESS_SUMMARY in the isBusinessFarm method Save and Next for
            // Business Assets throws an exception.
            return (!(familyReportedBusinessFarm()) && (isBusinessFarm(currentScreen) || currentScreen == BUSINESS_SUMMARY));
        }

        // Need to make sure the pfs has business farms that the family has added. Schools also can add biz/farms
        // through the FCW worksheet but that will not set the Own Business or Farm field to Yes.
        private Boolean familyReportedBusinessFarm() {
            return this.pfs.Own_Business_or_Farm__c == YES;
        }

        private FamilyAppSettings__c getMonthlyIncomeExpensesbump() {
            if (monthlyIncomeExpensesbump == null) {
                monthlyIncomeExpensesbump = FamilyAppSettings__c.getInstance(MIE);
            }
            return monthlyIncomeExpensesbump;
        }

        private Boolean isSectionCompleted(FamilyAppSettings__c setting)
        {
            if (isDashboard() || setting.Status_Field__c == null)
            {
                return false;
            }

            Boolean status = false;

            // Validate the current record's status field on the PFS__c record, parsing for
            // string and boolean in the process.
            status = isRecordComplete(this.pfs, setting.Status_Field__c);

            // Determine if we are in a business farm. If so check each individual business
            // for the status.
            if (isBusinessFarm(setting.Name))
            {
                List<Business_Farm__c> businessFarms = this.pfs.Business_Farms__r;

                if (businessFarms.isEmpty())
                {
                    return status;
                }

                Map<Id, Business_Farm__c> businessFarmsById = new Map<Id, Business_Farm__c>(businessFarms);
                Id currentBusinessFarmId = getBusinessFarmId();
                currentBusinessFarmId = (currentBusinessFarmId == null) ? businessFarms[0].Id : currentBusinessFarmId;

                if (!setting.Is_Speedbump_Page__c && businessFarmsById.containsKey(currentBusinessFarmId)) {
                    status = isRecordComplete(businessFarmsById.get(currentBusinessFarmId), setting.Status_Field__c);

                    if (!status) {
                        BusinessFarm = businessFarmsById.get(currentBusinessFarmId);
                    }
                }
            }
            return status;
        }

        private Boolean isRecordComplete(SObject record, String statusField)
        {
            if (record.get(statusField) instanceof Boolean)
            {
                return (Boolean)record.get(statusField);
            } else if (record.get(statusField) instanceOf String) {
                return ((String)record.get(statusField)) == COMPLETE ? true : false;
            }

            return false;
        }

        private Boolean hasBusinessFarmLoopback(FamilyAppSettings__c setting)
        {
            return hasBusinessFarmLoopback(setting, null);
        }

        private Boolean hasBusinessFarmLoopback(FamilyAppSettings__c setting, Id bfId)
        {
            List<Business_Farm__c> businessFarms = this.pfs.Business_Farms__r;
            if (!familyReportedBusinessFarm() || !isBusinessFarm(setting.Name) ||
                    businessFarms.isEmpty() || String.isEmpty(setting.Loop_Back_Page__c)) {
                return false;
            }

            if (bfId == null) {
                bfId = getBusinessFarmId();
            }
            bfId = (bfId == null) ? businessFarms[0].Id : bfId;
            Business_Farm__c currentBusinessFarm = new Map<Id, Business_Farm__c>(businessFarms).get(bfId);

            return currentBusinessFarm.Sequence_Number__c + 1 <= businessFarms.size();
        }
    }

    /**
     * @description An instance property to store the singleton
     *              instance of the FamilyNavigationService.
     */
    public static FamilyNavigationService Instance {
        get {
            if (Instance == null) {
                Instance = new FamilyNavigationService();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to keep this from being
     *              instantiated externally.
     */
    private FamilyNavigationService() { }
}