public class GlobalMessages extends fflib_SObjectDomain
{

    private Map<String, Global_Message__c> globalMessageNameToRecord;
    private Map<String, List<String>> pageNameToGMnames;

    public GlobalMessages(List<Global_Message__c> records)
    {
        super(records);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable
    {
        public fflib_SObjectDomain construct(List<SObject> sObjectList)
        {
            return new GlobalMessages(sObjectList);
        }
    }

    public override void onBeforeInsert()
    {
        validateNewRecords(records);
    }

    public override void onBeforeUpdate(Map<Id, SObject> oldRecords)
    {
        validateNewRecords(records);
    }

    private  void validateNewRecords(List<Global_Message__c> globalMessages)
    {
        List<String> allPageNames = new List<String>();
        globalMessageNameToRecord = new Map<String, Global_Message__c>();
        pageNameToGMnames = new Map<String, List<String>>();

        for (Global_Message__c singleMessage : globalMessages)
        {
            if (String.isNotBlank(singleMessage.Page_Name__c))
            {
                String pageNameInLowerCase = singleMessage.Page_Name__c.toLowerCase();

                allPageNames.add(pageNameInLowerCase);
                globalMessageNameToRecord.put(singleMessage.Name, singleMessage);

                if (pageNameToGMnames.get(pageNameInLowerCase) == null)
                {
                    pageNameToGMnames.put(pageNameInLowerCase, new List<String>{singleMessage.Name});
                }
                else
                {
                    pageNameToGMnames.get(pageNameInLowerCase).add(singleMessage.Name);
                }
            }
        }

        if(allPageNames.size() > 0)
        {
            Set<String> uniquePageNames = new Set<String>(allPageNames);

            checkApexPageExistence(uniquePageNames);
        }
    }

    private  void checkApexPageExistence(Set<String> pageNames)
    {
        List<String> listPageNames = new List<String>(pageNames);

        List<ApexPage> existingApexPages = Database.query('SELECT Name FROM ApexPage WHERE Name IN :listPageNames');

        Set<String> existingPageNames = new Set<String>{};
        if (!existingApexPages.isEmpty())
        {
            existingPageNames = buildApexPageNamesSet(existingApexPages);
        }

        pageNames.removeAll(existingPageNames);

        if (!pageNames.isEmpty())
        {
            checkScreenExistence(pageNames);
        }
    }

    private  Set<String> buildApexPageNamesSet(List<ApexPage> pages)
    {
        Set<String> pageNamesSet = new Set<String>{};

        for (ApexPage singlePage : pages)
        {
            pageNamesSet.add(singlePage.Name.toLowerCase());
        }

        return pageNamesSet;
    }

    private  void checkScreenExistence(Set<String> screenNames)
    {
        List<FamilyAppSettings__c> familyAppSettings = FamilyAppSettings__c.getAll().values();
        Set<String> allScreenNames = new Set<String>();

        if (!familyAppSettings.isEmpty())
        {
            allScreenNames = buildScreenNameSet(familyAppSettings);
        }

        for (String name : screenNames)
        {
            if (allScreenNames.isEmpty() || !allScreenNames.contains(name))
            {
                // Expected that number of records inserted or updated at once will be not too large
                // to make inner loop the cause of performance issues
                List<String> gmNames = pageNameToGMnames.get(name);
                for (String gmName : gmNames)
                {
                    globalMessageNameToRecord.get(gmName).addError(String.format(Label.GM_Invalid_Page_Name, new List<String>{name}));
                }
            }
        }
    }

    private  Set<String> buildScreenNameSet(List<FamilyAppSettings__c> familyAppSettings)
    {
        Set<String> screenNamesSet = new Set<String>{};

        for (FamilyAppSettings__c singleScreen : familyAppSettings)
        {
            screenNamesSet.add(singleScreen.Name.toLowerCase());
        }

        return screenNamesSet;
    }
}