/**
 * @description This class is used to create School Document Assignment records for unit tests.
 */
@isTest
public class SchoolDocumentAssignmentTestData extends SObjectTestData {
    /**
     * @description Get the default values for the School_Document_Assignment__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object>();
    }

    /**
     * @description Set the Deleted__c field on the current School Document Assignment record to true.
     * @return The current working instance of SchoolDocumentAssignmentTestData.
     */
    public SchoolDocumentAssignmentTestData asDeleted() {
        return (SchoolDocumentAssignmentTestData) with(School_Document_Assignment__c.Deleted__c, true);
    }

    /**
     * @description Set the Document__c field on the current School Document Assignment record.
     * @param documentId The Name to set on the School Document Assignment.
     * @return The current working instance of SchoolDocumentAssignmentTestData.
     */
    public SchoolDocumentAssignmentTestData forDocument(String documentId) {
        return (SchoolDocumentAssignmentTestData) with(School_Document_Assignment__c.Document__c, documentId);
    }

    /**
     * @description Set the Document_Year__c field on the current School Document Assignment record.
     * @param documentYear The Document Year to set on the School Document Assignment.
     * @return The current working instance of SchoolDocumentAssignmentTestData.
     */
    public SchoolDocumentAssignmentTestData forDocumentYear(String documentYear) {
        return (SchoolDocumentAssignmentTestData) with(School_Document_Assignment__c.Document_Year__c, documentYear);
    }

    /**
     * @description Set the Verification_Request_Status__c field on the record.
     * @param requestStatus The status to use.
     * @return The current working instance of SchoolDocumentAssignmentTestData.
     */
    public SchoolDocumentAssignmentTestData withVerificationRequestStatus(String requestStatus) {
        return (SchoolDocumentAssignmentTestData)with(School_Document_Assignment__c.Verification_Request_Status__c, requestStatus);
    }

    /**
     * @description Sets the School_PFS_Assignment__c field on the record.
     * @param pfsAssignmentId The Id of a School_PFS_Assignment__c record.
     * @return The current working instance of SchoolDocumentAssignmentTestData.
     */
    public SchoolDocumentAssignmentTestData forPfsAssignment(Id pfsAssignmentId) {
        return (SchoolDocumentAssignmentTestData)with(School_Document_Assignment__c.School_PFS_Assignment__c, pfsAssignmentId);
    }

    /**
     * @description Sets the Document_Type__c field on the record.
     * @param docType The type of document.
     * @return The current working instance of SchoolDocumentAssignmentTestData.
     */
    public SchoolDocumentAssignmentTestData asType(String docType) {
        return (SchoolDocumentAssignmentTestData)with(School_Document_Assignment__c.Document_Type__c, docType);
    }

    /**
     * @description Sets the Verify_Data__c field on the record to true.
     * @return The current working instance of SchoolDocumentAssignmentTestData.
     */
    public SchoolDocumentAssignmentTestData verifyData() {
        return (SchoolDocumentAssignmentTestData)with(School_Document_Assignment__c.Verify_Data__c, true);
    }

    /**
     * @description Sets the Required_Document__c field on the record.
     * @param requiredDocId The id of a document requirement.
     * @return The current working instance of SchoolDocumentAssignmentTestData.
     */
    public SchoolDocumentAssignmentTestData forRequirement(Id requiredDocId) {
        return (SchoolDocumentAssignmentTestData)with(School_Document_Assignment__c.Required_Document__c, requiredDocId);
    }

    /**
     * @description Sets School PFS Assignment lookup to the default record if no other value was specified. This
     *              prevents duplicate record errors that occurred when using the default record in the default value map.
     */
    protected override void beforeInsert(SObject sObj) {
        School_Document_Assignment__c record = (School_Document_Assignment__c)sObj;
        if (record.School_PFS_Assignment__c == null) {
            record.School_PFS_Assignment__c = SchoolPfsAssignmentTestData.Instance.DefaultSchoolPfsAssignment.Id;
        }
    }

    /**
     * @description Insert the current working School_Document_Assignment__c record.
     * @return The currently operated upon School_Document_Assignment__c record.
     */
    public School_Document_Assignment__c insertSchoolDocumentAssignment() {
        return (School_Document_Assignment__c)insertRecord();
    }

    /**
     * @description Create the current working School Document Assignment record without resetting
     *          the stored values in this instance of SchoolDocumentAssignmentTestData.
     * @return A non-inserted School_Document_Assignment__c record using the currently stored field
     *          values.
     */
    public School_Document_Assignment__c createSchoolDocumentAssignmentWithoutReset() {
        return (School_Document_Assignment__c)buildWithoutReset();
    }

    /**
     * @description Create the current working School_Document_Assignment__c record.
     * @return The currently operated upon School_Document_Assignment__c record.
     */
    public School_Document_Assignment__c create() {
        return (School_Document_Assignment__c)super.buildWithReset();
    }

    /**
     * @description The default School_Document_Assignment__c record.
     */
    public School_Document_Assignment__c DefaultSchoolDocumentAssignment {
        get {
            if (DefaultSchoolDocumentAssignment == null) {
                DefaultSchoolDocumentAssignment = insertSchoolDocumentAssignment();
            }
            return DefaultSchoolDocumentAssignment;
        }
        private set;
    }

    /**
     * @description Get the School_Document_Assignment__c SObjectType.
     * @return The School_Document_Assignment__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return School_Document_Assignment__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static SchoolDocumentAssignmentTestData Instance {
        get {
            if (Instance == null) {
                Instance = new SchoolDocumentAssignmentTestData();
            }
            return Instance;
        }
        private set;
    }

    private SchoolDocumentAssignmentTestData() { }
}