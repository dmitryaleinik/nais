/**
 * SchoolRichTextEditorController.cls
 *
 * @description Controller for SchoolRichTextEditor.component
 *
 * @author Chase Logan @ Presence PG
 */
public class SchoolRichTextEditorController {
    
    /* instance variables */
    private Mass_Email_Send__c massES;

    /* properties */
    public Id massEmailSendId { 
        get; 
        set {
            if ( this.massES == null) {
                this.init( value);
            }
            this.massEmailSendId = value;
        } 
    }

    /* view getters */


    /* private instance methods */

    private void init( String massESId) {

        // implement me
    }
}