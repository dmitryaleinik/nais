public class GlobalMessagesSelector extends fflib_SObjectSelector
{

    /**
     * @description Selects Global Message records by portal, page name, academic year and start and end dates.
     * @param portal The Name of the portal.
     * @param pageAndScreenList Names of pages or screens for the query
     * @param academicYearName The Name of the selected Academic Year to query for records.
     * @return A list of Global_Message__c records.
     */
    public List<Global_Message__c> selectRelevantByPortalPageAcademicYear (String portal, List<String> pageAndScreenList, String academicYearName)
    {
        assertIsAccessible();

        Datetime currentDateAndTime = System.now();
        String filterCondition = 'Display_Start_Date_Time__c <= :currentDateAndTime AND Display_End_Date_Time__c >= :currentDateAndTime ';

        if (pageAndScreenList != null && !pageAndScreenList.isEmpty())
        {
            filterCondition += 'AND (Page_Name__c in :pageAndScreenList ';

            if (String.isNotBlank(portal))
            {
                filterCondition += 'OR Portal__c INCLUDES (:portal) ';
            }

            filterCondition += ') ';
        }
        else
        {
            if (String.isBlank(portal))
            {
                return new List<Global_Message__c>();
            }

            filterCondition += 'AND Portal__c INCLUDES (:portal) ';
        }

        if (String.isNotBlank(academicYearName))
        {
            filterCondition += 'AND (Academic_Year__c = :academicYearName OR Academic_Year__c = NULL) ';
        }

        String globalMessagesQuery = newQueryFactory(true)
            .setCondition(filterCondition)
            .toSOQL();

        return Database.query(globalMessagesQuery);
    }

    private Schema.SObjectType getSObjectType()
    {
        return Global_Message__c.getSObjectType();
    }

    private List<Schema.SObjectField> getSObjectFieldList()
    {
        return new List<Schema.SObjectField>
            {
                Global_Message__c.Display_Start_Date_Time__c,
                Global_Message__c.Display_End_Date_Time__c,
                Global_Message__c.Page_Name__c,
                Global_Message__c.Portal__c,
                Global_Message__c.Academic_Year__c,
                Global_Message__c.Message__c,
                Global_Message__c.Name,
                Global_Message__c.Type__c,
                Global_Message__c.Id
            };
    }

    /**
     * @description Creates a new instance of the GlobalMessagesSelector.
     * @return An instance of GlobalMessagesSelector.
     */
    public static GlobalMessagesSelector newInstance()
    {
        return new GlobalMessagesSelector();
    }

    /**
     * @description Singleton property ensuring external sources do not
     *              instantiate this directly.
     */
    public static GlobalMessagesSelector Instance
    {
        get
        {
            if (Instance == null)
            {
                Instance = new GlobalMessagesSelector();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to enforce singleton access
     *              via the Instance property.
     */
    private GlobalMessagesSelector() {}
}