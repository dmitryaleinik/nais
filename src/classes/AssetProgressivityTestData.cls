/**
 * @description This class is used to create Asset Progressivity records for unit tests.
 */
@isTest
public class AssetProgressivityTestData extends SObjectTestData {
    @testVisible private static final Decimal DISCRETIONARY_NET_WORTH_LOW = 20;
    @testVisible private static final Decimal INDEX = 1;

    /**
     * @description Get the default values for the Asset_Progressivity__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Asset_Progressivity__c.Academic_Year__c => AcademicYearTestData.Instance.DefaultAcademicYear.Id,
                Asset_Progressivity__c.Discretionary_Net_Worth_Low__c => DISCRETIONARY_NET_WORTH_LOW,
                Asset_Progressivity__c.Index__c => INDEX
        };
    }

    /**
     * @description Set the Academic Year on the current Asset Progressivity record.
     * @param academicyearId The Academic Year to set on the current Asset Progressivity
     *             record.
     * @return The current working instance of AssetProgressivityTestData.
     */
    public AssetProgressivityTestData forAcademicYearId(Id academicYearId) {
        return (AssetProgressivityTestData) with(Asset_Progressivity__c.Academic_Year__c, academicYearId);
    }

    /**
     * @description Insert the current working Asset_Progressivity__c record.
     * @return The currently operated upon Asset_Progressivity__c record.
     */
    public Asset_Progressivity__c insertAssetProgressivity() {
        return (Asset_Progressivity__c)insertRecord();
    }

    /**
     * @description Create the current working Asset Progressivity record without resetting
     *             the stored values in this instance of AssetProgressivityTestData.
     * @return A non-inserted Asset_Progressivity__c record using the currently stored field
     *             values.
     */
    public Asset_Progressivity__c createAssetProgressivityWithoutReset() {
        return (Asset_Progressivity__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Asset_Progressivity__c record.
     * @return The currently operated upon Asset_Progressivity__c record.
     */
    public Asset_Progressivity__c create() {
        return (Asset_Progressivity__c)super.buildWithReset();
    }

    /**
     * @description The default Asset_Progressivity__c record.
     */
    public Asset_Progressivity__c DefaultAssetProgressivity {
        get {
            if (DefaultAssetProgressivity == null) {
                DefaultAssetProgressivity = createAssetProgressivityWithoutReset();
                insert DefaultAssetProgressivity;
            }
            return DefaultAssetProgressivity;
        }
        private set;
    }

    /**
     * @description Get the Asset_Progressivity__c SObjectType.
     * @return The Asset_Progressivity__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Asset_Progressivity__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static AssetProgressivityTestData Instance {
        get {
            if (Instance == null) {
                Instance = new AssetProgressivityTestData();
            }
            return Instance;
        }
        private set;
    }

    private AssetProgressivityTestData() { }
}