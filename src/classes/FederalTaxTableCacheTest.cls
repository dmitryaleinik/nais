@isTest
private class FederalTaxTableCacheTest 
{
    
    @isTest
    private static void testGetTaxTables_exception() {
        Academic_Year__c currentYear = AcademicYearTestData.Instance.DefaultAcademicYear;

        try
        {
            List<Federal_Tax_Table__c> federalTaxTables = FederalTaxTableCache.getTaxTables(currentYear.Id);
        } catch (Exception ex) 
        {
            system.assertEquals(FederalTaxTableCache.queryTaxTablesException.replace('{0}', currentYear.Id), ex.getMessage());
        }
    }

    @isTest
    private static void testGetTaxTables_getRecords() {
        Id currentYearId = AcademicYearTestData.Instance.DefaultAcademicYear.Id;
        Academic_Year__c priorAcademicYear = AcademicYearTestData.Instance.asPreviousYear().insertAcademicYear();

        Federal_Tax_Table__c currentYearFederalTaxTable = FederalTaxTableTestData.Instance.create();
        Federal_Tax_Table__c priorYearFederalTaxTable = FederalTaxTableTestData.Instance
            .forAcademicYearId(priorAcademicYear.Id).create();
        insert new List<Federal_Tax_Table__c>{currentYearFederalTaxTable, priorYearFederalTaxTable};

        Test.startTest();
            System.assert(!Cache.Org.contains(FederalTaxTableCache.CACHE_KEY_PREFIX + String.valueOf(currentYearId)));
            List<Federal_Tax_Table__c> federalTaxTables = FederalTaxTableCache.getTaxTables(currentYearId);
            System.assertEquals(1, federalTaxTables.size());
            System.assertEquals(currentYearFederalTaxTable.Id, federalTaxTables[0].Id);

            //// Clearing of the federalTaxTables Map
            federalTaxTables = new List<Federal_Tax_Table__c>();

            Integer currentNumberOfSOQLs = Limits.getQueries();
            
            federalTaxTables = FederalTaxTableCache.getTaxTables(currentYearId);
            System.assertEquals(currentNumberOfSOQLs, Limits.getQueries(), 
                'Number of soqls didn\'t increase - the tax table was received from the Org Cache but not from the database');

            //// We check that federalTaxTables list still contains the same values as at the previous step
            System.assertEquals(1, federalTaxTables.size());
            System.assertEquals(currentYearFederalTaxTable.Id, federalTaxTables[0].Id);
        Test.stopTest();
    }
}