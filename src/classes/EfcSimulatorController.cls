public without sharing class EfcSimulatorController {
    public EfcCalculator calculator = new EfcCalculator();
    public EfcWorksheetData worksheet { get; set; }
    
    
    public Boolean isRunInternal;
    
    public void setIsRunInternal(Boolean isRunInternal) {
        if (this.isRunInternal == null) {
            this.isRunInternal = isRunInternal;
            init();
        }
    }
    
    public Boolean getIsRunInternal() {
        return isRunInternal;
    }
    
    
    public Academic_Year__c academicYear { get; set; }
    public String message { get; set; }

    public List<SelectOption> stateSelectOptions { get; set; }
    public List<SelectOption> countrySelectOptions { get; set; }
    
    
    private Id activeAccountId;
    public Boolean isProfileCanWork {get; set;}
        
    public List<SelectOption> profileSelectOptions {get; set;}
    public String profileId {get; set;}
    
    public Boolean isShowAcademicYearList {get; set;}
    public List<SelectOption> academicYearSelectOptions {get; set;}
    public String academicYearId {get; set;}
    
    public String saveAsName {get; set;}
    
    
    public EfcSimulatorController() { }
    

    private void init() {
        // retrieve the State picklist options
        stateSelectOptions = new List<SelectOption>();
        stateSelectOptions.add(new SelectOption('', ''));
        List<Schema.PicklistEntry> statePicklistEntries = PFS__c.Parent_A_State__c.getDescribe().getPicklistValues();
        stateSelectOptions.add(new SelectOption('N/A', 'N/A'));
        for (Schema.PicklistEntry statePicklistEntry: statePicklistEntries) {
            stateSelectOptions.add(new SelectOption(statePicklistEntry.getLabel(), statePicklistEntry.getValue()));
        }

        // retrieve the Country picklist options
        countrySelectOptions = new List<SelectOption>();
        countrySelectOptions.add(new SelectOption('', ''));
        countrySelectOptions.add(new SelectOption('United States', 'United States'));
        countrySelectOptions.add(new SelectOption('Canada', 'Canada'));
        countrySelectOptions.add(new SelectOption('Mexico', 'Mexico'));
        countrySelectOptions.add(new SelectOption('Other', 'Other'));
        
        
        /* Init active account id */
        isProfileCanWork = true;
        
        if (isRunInternal) {
            List<Account> accounts = [
                    SELECT Id
                    FROM Account
                    WHERE Name = 'SSS Training School'];
            
            if (accounts.size() == 0) {
                isProfileCanWork = false;
                
                ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.WARNING, 
                        '"SSS Training School" account needs to be created. ' + 
                                '<a href="/setup/ui/recordtypeselect.jsp?ent=Account&retURL=%2F001%2Fo&save_new_url=%2F001%2Fe%3Facc2=SSS%20Training%20School&retURL%3D%252F001%252Fo" title="Create a new Account (New Tab)" target="_blank">[Create "SSS Training School"]</a>'));
            } else {
                activeAccountId = accounts[0].Id;
            }
        } else {
            User currentUser = [
                    SELECT In_Focus_School__c, Contact.AccountId
                    FROM User
                    WHERE Id = :UserInfo.getUserId()
                    LIMIT 1];
                    
            if ((currentUser.In_Focus_School__c == null) && (currentUser.Contact.AccountId == null)) {
                /* We can't work with EFC Profiles if no Account Id */
                isProfileCanWork = false;
                
                ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.WARNING, 
                        'User has no related Account. System can not continue working with EFC Profiles.'));
            } else {
                activeAccountId = currentUser.In_Focus_School__c != null ? currentUser.In_Focus_School__c : currentUser.Contact.AccountId;
            }
        }
        
        
        /* Init Profile select options */
        initProfileSelectOptions();
        
        
        /* Init Academic Year */
        academicYear = GlobalVariables.getCurrentAcademicYear();
        
        if (isRunInternal) {
            academicYearSelectOptions = new List<SelectOption>();
            
            for (Academic_Year__c acYear : GlobalVariables.getAllAcademicYears()) {
                academicYearSelectOptions.add(new SelectOption(acYear.Id, acYear.Name));
            }
            
            academicYearId = academicYear.Id;
            
            
            isShowAcademicYearList = true;
        } else {
            isShowAcademicYearList = false;
        }
        
        
        /* Init Worksheet */
        worksheet = new EfcWorksheetData();
        initWorksheet(worksheet, academicYear);
    }
    
    
    public void initWorksheet(EfcWorksheetData worksheet, Academic_Year__c academicYear) {
        worksheet.calculateFamilySize = false;

        // set the academic year
        worksheet.academicYearId = academicYear.Id;

        // set defaults
        worksheet.pjApplyMinIncomeToNonTaxableIncome = false;
        worksheet.pjAddDeprecHomeBusExpense = false;
        worksheet.pjUseCostOfLivingAdjustment = true;
        worksheet.pjOverrideDefaultColaValue = 1;
        worksheet.pjPercentageForImputingAssets = 2;
        worksheet.familyStatus = EfcPicklistValues.FAMILY_STATUS_2_PARENT;
        worksheet.dayOrBoarding = EfcPicklistValues.DAYBOARDING_DAY;
    }
    
    
    public void initProfileSelectOptions() {
        profileSelectOptions = new List<SelectOption> {
            new SelectOption('', '-- Create New --')
        };
                
        for (EFC_Profile__c profile : [
                SELECT Name 
                FROM EFC_Profile__c 
                WHERE Account__c = :activeAccountId
                ORDER BY Name]) {
                    
            profileSelectOptions.add(new SelectOption(profile.Id, profile.Name));
        }
    }
    

    public boolean getHasMessage() {
        boolean hasMessage = false;
        if ((message != null) && (message.length() > 0)) {
            hasMessage = true;
        }
        return hasMessage;
    }


    public boolean validateWorksheet() {
        boolean isValid = true;

        // check for required fields
        if (worksheet.parentState == null) {
            isValid = false;
            message = 'ERROR: Parent State is a required field.';
        } else if (worksheet.familyStatus == null) {
            isValid = false;
            message = 'ERROR: Family Status is a required field.';
        } else if ((worksheet.ageParentA == null) || (worksheet.ageParentA == 0)) {
            isValid = false;
            message = 'ERROR: Parent A Age is a required field.';
        } else if ((worksheet.familySize == null) || (worksheet.familySize == 0)) {
            isValid = false;
            message = 'ERROR: Family Size is a required field.';
        } else if ((worksheet.numChildrenInTuitionSchools == null) || (worksheet.numChildrenInTuitionSchools == 0)) {
            isValid = false;
            message = 'ERROR: Number of Children in Tuition-Charging Schools is a required field.';
        } else if (worksheet.gradeApplying == null) {
            isValid = false;
            message = 'ERROR: Grade Applying is a required field.';
        } else if (worksheet.dayOrBoarding == null) {
            isValid = false;
            message = 'ERROR: Day/Boarding is a required field.';
        } else if (worksheet.filingStatus == null) {
            isValid = false;
            message = 'ERROR: Filing Status is a required field.';
        } else if (worksheet.familyStatus=='2 Parents'
        && (worksheet.filingStatus=='Single' || worksheet.filingStatus=='Head of Household')
        && (worksheet.filingStatusParentB==null || worksheet.filingStatusParentB=='')) {
            isValid = false;
            message = 'ERROR: You must select a value for \'Parent B Filing Status\'.';
        } else if ((worksheet.incomeTaxExemptions == null) || (worksheet.incomeTaxExemptions == 0) && worksheet.filingStatus!='Did not File') {
            isValid = false;
            message = 'ERROR: Income Tax Exemptions is a required field.';
        } else if ((worksheet.salaryWagesParentA == null) || (worksheet.salaryWagesParentA == 0)) {
            isValid = false;
            message = 'ERROR: Salary Parent A is a required field.';
        } else if (worksheet.adjustHousingPortion == 'Yes' && (worksheet.ipaHousingFamilyOf4 == null || worksheet.ipaHousingFamilyOf4 == 0)) {
            isValid = false;
            message = 'ERROR: Annual Housing Amount for Family of Four required when Adjust Housing Portion of Income Protection Allowance is set to yes.';
        }

        if (worksheet.pjUseHomeEquity == true) { // [SL] NAIS-1016
            if (worksheet.pjUseHousingIndexMultiplier == true) {
                if ((worksheet.homePurchaseYear == null) || (worksheet.homePurchaseYear == '')) {
                    isValid = false;
                    message = 'ERROR: Home Year Purchased is required if Use Housing Index Multiplier is selected.';
                } else if ((worksheet.homePurchasePrice == null) || (worksheet.homePurchasePrice == 0)) {
                    isValid = false;
                    message = 'ERROR: Home Purchase Price is required if Use Housing Index Multiplier is selected.';
                }
            }
        }

        return isValid;
    }

    public PageReference doCalculate() {
        message = null;

        // [SL] NAIS-1016: set own home flag based on PJ
        if (worksheet.pjUseHomeEquity == true) {
            worksheet.ownHome = true;
        } else {
            worksheet.ownHome = false;
        }


        if (validateWorksheet()) {
            try {
                calculator.calculateEfc(worksheet);
            } catch(EfcException e) {
                message = 'ERROR: ' + e.getMessage();
            }
        }
        return null;
    }
    
    
    public PageReference profileSave() {
        String query = 'SELECT ' + getProfileFields() + ' FROM EFC_Profile__c WHERE Id = \'' + profileId + '\' LIMIT 1';
        EFC_Profile__c efcProfile = Database.query(query);
        
        EfcDataManager.updateEFCProfileFromEFCData(efcProfile, worksheet);
        
        try {
            update efcProfile;
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.CONFIRM, 'Save Successful'));
        } catch (DmlException ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, ex.getMessage()));
        }
        
        return null;
    }
    
    
    public PageReference profileSaveAs() {
        EFC_Profile__c efcProfile = new EFC_Profile__c(
                Name = saveAsName, 
                Account__c = activeAccountId
        );
        
        EfcDataManager.updateEFCProfileFromEFCData(efcProfile, worksheet);
        
        
        Database.SaveResult sr = Database.insert(efcProfile, false);
        
        if (sr.isSuccess()) {
            initProfileSelectOptions();
            profileId = sr.getId();
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.CONFIRM, 'Save Successful'));
        } else {
            for (Database.Error error : sr.getErrors()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, error.getMessage()));
            }
        }
        
        
        return null;
    }
    
    
    public PageReference profileDelete() {
        try {
            Database.delete(profileId);
            
            worksheet = new EfcWorksheetData();
            initWorksheet(worksheet, academicYear);
            
            initProfileSelectOptions();
            profileId = null;
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.CONFIRM, 'Delete Successful'));
        } catch(DmlException ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, ex.getMessage()));
        }
        
        
        return null;
    }
    
    
    public void changeProfile() {
        worksheet = new EfcWorksheetData();
        initWorksheet(worksheet, academicYear);
            
        if (profileId != null) {
            String query = 'SELECT ' + getProfileFields() + ' FROM EFC_Profile__c WHERE Id = \'' + profileId + '\' LIMIT 1';
            EFC_Profile__c efcProfile = Database.query(query);
        
            EfcDataManager.updateEFCDataFromEFCProfile(worksheet, efcProfile);
        }
    }
    
    
    public void changeAcademicYear() {
        academicYear = GlobalVariables.getAcademicYear(academicYearId);
        worksheet.academicYearId  = academicYear.Id;
    }
    
    
    private String getProfileFields() {
        Map<String, Schema.sObjectField> efcProfileFieldMap = Schema.sObjectType.EFC_Profile__c.fields.getMap();
        
        return String.join(new List<String>(efcProfileFieldMap.keySet()), ', ');
    }
    

    public list<SelectOption> getFilingStatusOptions(){
        list<SelectOption> options = new list<SelectOption>{new SelectOption('','')};
        options.addAll(applicationUtils.getPicklistValues(Schema.sObjectType.School_PFS_Assignment__c.fields.Filing_Status__c));
        return options;
    }

    public list<SelectOption> getParentBFilingStatusOptions(){
         list<SelectOption> options = new list<SelectOption>{new SelectOption('','')};
         options.addAll(applicationUtils.getPicklistValues(Schema.sObjectType.School_PFS_Assignment__c.fields.Parent_B_Filing_Status__c));
         return options;
    }
}