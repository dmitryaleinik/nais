/**
 * @description This class is responsible for querying user records.
 */
public class UsersSelector extends fflib_SObjectSelector {
    @testVisible private static final String ID_SET_PARAM = 'idSet';
    private static final String NAMES_PARAM = 'names';
    private static final String SYSTEM_USER_NAME = 'System User';

    /**
     * @description Default constructor for an instance of the UsersSelector that will not include field sets while
     *              enforcing FLS and CRUD.
     */
    public UsersSelector() { }

    /**
     * @description Queries user records by Id.
     * @param idSet The Ids of the user records to query.
     * @return A list of user records.
     * @throws ArgumentNullException if idSet is null.
     */
    public List<User> selectById(Set<Id> idSet) {
        ArgumentNullException.throwIfNull(idSet, ID_SET_PARAM);

        return (List<User>)super.selectSObjectsById(idSet);
    }

    private Schema.SObjectType getSObjectType() {
        return User.SObjectType;
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                User.Can_Apply_Courtesy_Waivers__c,
                User.Contact.AccountId,
                User.Contact.Account.Name,
                User.Contact.Name,
                User.ContactId,
                User.Early_Access__c,
                User.Email,
                User.In_Focus_School__c,
                User.Name,
                User.ProfileId,
                User.Persist_School_Focus__c,
                User.Profile.Name,
                User.SYSTEM_Terms_and_Conditions_Accepted__c,
                User.Username
        };
    }

    /**
     * @description Creates a new instance of the UsersSelector.
     * @return A UsersSelector.
     */
    public static UsersSelector newInstance() {
        return new UsersSelector();
    }
}