/**
 * @description This class is used to create Household Income Threshold records for unit tests.
 */
@isTest
public with sharing class HouseholdIncomeThresholdTestData extends SObjectTestData {
    @testVisible private static final Decimal INCOME_THRESHOLD_DEFAULT = 45000;
    @testVisible private static final Map<Integer, Decimal> INCOME_THRESHOLD_DEFAULT_MAP = new  Map<Integer, Decimal>{
                                                                                        1 => 45000,
                                                                                        2 => 55000,
                                                                                        3 => 60000,
                                                                                        4 => 70000,
                                                                                        5 => 80000,
                                                                                        6 => 90000,
                                                                                        7 => 100000,
                                                                                        8 => 110000
                                                                                    };
    
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Household_Income_Threshold__c.Academic_Year__c => AcademicYearTestData.Instance.DefaultAcademicYear.Id,
                Household_Income_Threshold__c.Family_Size__c => 1,
                Household_Income_Threshold__c.Income_Threshold__c => INCOME_THRESHOLD_DEFAULT
        };
    }    
    
    
    public List<Household_Income_Threshold__c> insertIncomeThresholds() {
        
        List<Academic_Year__c> academicYears = academicYearTestData.Instance.insertAcademicYears(5);
              
        return insertIncomeThresholds(academicYears);
    }
    
    public List<Household_Income_Threshold__c> insertIncomeThresholds(List<Academic_Year__c> academicYears) {
        
        List<Household_Income_Threshold__c> householdIncomeThreshold = new List<Household_Income_Threshold__c>();
        
        
        for(Academic_Year__c a : academicYears) {

            for(Integer familySize : INCOME_THRESHOLD_DEFAULT_MAP.keySet()) {
                householdIncomeThreshold.add(forIncome(INCOME_THRESHOLD_DEFAULT_MAP.get(familySize)).forAcademicYear(a.Id).forFamilySize(familySize).create());
            }
        }
        
        insert householdIncomeThreshold;
        return householdIncomeThreshold;
    }
    
    /**
     * @description Set the Family Size field on the current Household Income Threshold record.
     * @param familySize The Value to set on the Family_Size__c field.
     * @return The current working instance of HouseholdIncomeThresholdTestData.
     */
    public HouseholdIncomeThresholdTestData forFamilySize(Decimal familySize) {
        return (HouseholdIncomeThresholdTestData) with(Household_Income_Threshold__c.Family_Size__c, familySize);
    }
    
    /**
     * @description Set the  Academic Year field on the current Household Income Threshold record.
     * @param academicYearId The Value to set on the Academic_Year__c field.
     * @return The current working instance of HouseholdIncomeThresholdTestData.
     */
    public HouseholdIncomeThresholdTestData forAcademicYear(Id academicYearId) {
        return (HouseholdIncomeThresholdTestData) with(Household_Income_Threshold__c.Academic_Year__c, academicYearId);
    }
    
    /**
     * @description Set the Income_Threshold__c field on the current Household Income Threshold record.
     * @param incomeValue The Value to set on the Income_Threshold__c field.
     * @return The current working instance of HouseholdIncomeThresholdTestData.
     */
    public HouseholdIncomeThresholdTestData forIncome(Decimal incomeValue) {
        return (HouseholdIncomeThresholdTestData) with(Household_Income_Threshold__c.Income_Threshold__c, incomeValue);
    }
    
    /**
     * @description Insert the current working Household_Income_Threshold__c record.
     * @return The currently operated upon Household_Income_Threshold__c record.
     */
    public Household_Income_Threshold__c insertHouseholdIncomeThreshold() {
        return (Household_Income_Threshold__c)insertRecord();
    }

    /**
     * @description Create the current working Household Income Threshold record without resetting
     *              the stored values in this instance of HouseholdIncomeThresholdTestData.
     * @return A non-inserted Household_Income_Threshold__c record using the currently stored field
     *         values.
     */
    public Household_Income_Threshold__c createHouseholdIncomeThresholdWithoutReset() {
        return (Household_Income_Threshold__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Household_Income_Threshold__c record.
     * @return The currently operated upon Household_Income_Threshold__c record.
     */
    public Household_Income_Threshold__c create() {
        return (Household_Income_Threshold__c)super.buildWithReset();
    }
    
    /**
     * @description Get the Household_Income_Threshold__c SObjectType.
     * @return The Household_Income_Threshold__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Household_Income_Threshold__c.SObjectType;
    }
    
    /**
    * @description Static singleton instance property.
    */
    public static HouseholdIncomeThresholdTestData Instance {
        get {
            if (Instance == null) {
                Instance = new HouseholdIncomeThresholdTestData();
            }
            return Instance;
        }
        private set;
    }

    private HouseholdIncomeThresholdTestData() { }
}