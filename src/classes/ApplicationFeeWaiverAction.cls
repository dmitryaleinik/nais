/*
 * NAIS-1032: Create a Fee Waiver transacation line item if an ApplicationFeeWaiver is approved.
 * SL, Exponent Partners, 2013
 */
public class ApplicationFeeWaiverAction 
{

    @testVisible private static final String PENDING_QUALIFICATION = 'Pending Qualification';
    @testVisible private static final String ASSIGNED = 'Assigned';
    @testVisible private static final String WAIVER_REQUESTED_STATUS = 'Waiver Requested';
    @testVisible private static final String WAIVER_DENIED_STATUS = 'No';

    /**
     * @description The Application Fee Waiver status of Declined.
     */
    public static final String WAIVER_DECLINED_STATUS = 'Declined';

    /**
     * @description The Application Fee Waiver status of Pending Approval.
     */
    public static final String WAIVER_PENDING_STATUS = 'Pending Approval';

    /**
     * @description Create Fee Waiver Transaction Line Items based on the
     *              provided Application Fee Waiver records.
     * @param feeWaivers The Application Fee Waivers to generate the new
     *        Transaction_Line_Item records from.
     */
    public static void createFeeWaiverTransactionLineItems(List<Application_Fee_Waiver__c> feeWaivers) {
        // get the list of academic years and parent ids
        Set<String> academicYearNames = new Set<String>();
        Set<Id> parentContactIds = new Set<Id>();
        for (Application_Fee_Waiver__c feeWaiver : feeWaivers) {
            if (feeWaiver.Status__c == ASSIGNED) {
                if (feeWaiver.Academic_Year__c != null) {
                    academicYearNames.add(GlobalVariables.getAcademicYear(feeWaiver.Academic_Year__c).Name);
                }
                if (feeWaiver.Contact__c != null) {
                    parentContactIds.add(feeWaiver.Contact__c);
                }
            }
        }

        // query all the opportunities for the set of parents and academic years, and map by academic year / contact
        Map<String, Opportunity> parentAcademicYearToOpportunityMap = new Map<String, Opportunity>();
        for (Opportunity opp : [SELECT Id, Fee_Waived__c, Amount, PFS__c, PFS__r.Academic_Year_Picklist__c, PFS__r.Parent_A__c
                                FROM Opportunity
                                WHERE RecordTypeId = :RecordTypes.pfsApplicationFeeOpportunityTypeId
                                        AND PFS__r.Academic_Year_Picklist__c IN :academicYearNames
                                        AND PFS__r.Parent_A__c IN :parentContactIds
                                        ORDER BY CreatedDate]) {
            if ((opp.PFS__c != null) && (opp.PFS__r.Academic_Year_Picklist__c != null) && (opp.PFS__r.Parent_A__c != null)) {
                parentAcademicYearToOpportunityMap.put(opp.PFS__r.Parent_A__c + ':' + opp.PFS__r.Academic_Year_Picklist__c, opp);
            }
        }

        // [DP] 08.07.2015 NAIS-2525 need gather these so we can create opps for existing PFSs that were submitted and have requested waiver
        Set<Id> parentContactIdsWithWaivedPFSButNoOpp = new Set<Id>();

        // create the TLI records
        List<Transaction_Line_Item__c> transactionLineItemsToInsert = new List<Transaction_Line_Item__c>();
        for (Application_Fee_Waiver__c feeWaiver : feeWaivers) {
            if (feeWaiver.Status__c == ASSIGNED) {
                if ((feeWaiver.Contact__c != null) && (feeWaiver.Academic_Year__c != null)) {

                    Opportunity opp = parentAcademicYearToOpportunityMap.get(feeWaiver.Contact__c + ':' + GlobalVariables.getAcademicYear(feeWaiver.Academic_Year__c).Name);
                    System.debug('The Opportunity: ' + opp);
                    if (opp != null) {
                        if (opp.Fee_Waived__c != 'Yes') {
                            Transaction_Line_Item__c feeWaiverTLI = new Transaction_Line_Item__c();
                            feeWaiverTLI.RecordTypeId = RecordTypes.pfsFeeWaiverTransactionTypeId;
                            feeWaiverTLI.Opportunity__c = opp.Id;
                            feeWaiverTLI.Amount__c = opp.Amount;
                            feeWaiverTLI.Waiver_Provider__c = feeWaiver.Account__c;
                            feeWaiverTLI.Transaction_Type__c = 'PFS Fee Waiver';
    
                            // [CH] NAIS-1489 Adding auto-population of codes from custom setting
                            Transaction_Settings__c settingRecord = Transaction_Settings__c.getValues(feeWaiverTLI.Transaction_Type__c);

                            if (settingRecord != null) {
                                feeWaiverTLI.Transaction_Code__c = settingRecord.Transaction_Code__c;
                                feeWaiverTLI.Account_Code__c = settingRecord.Account_Code__c;
                                feeWaiverTLI.Account_Label__c = settingRecord.Account_Label__c;
                            }

                            feeWaiverTLI.Transaction_Status__c = 'Posted';
                            feeWaiverTLI.Transaction_Date__c = System.today();
                            transactionLineItemsToInsert.add(feeWaiverTLI);
                        }
                        // [DP] 08.07.2015 NAIS-2525 need gather these so we can create opps for existing PFSs that were submitted and have requested waiver
                    } else {
                        parentContactIdsWithWaivedPFSButNoOpp.add(feeWaiver.Contact__c);
                    }
                }
            }
        }

        // insert the TLIs
        if (!transactionLineItemsToInsert.isEmpty()) {
            insert transactionLineItemsToInsert;
        }

        // [DP] 08.07.2015 NAIS-2525 need to create opps for existing PFSs that were submitted and have requested waiver
        if (!parentContactIdsWithWaivedPFSButNoOpp.isEmpty()) {
            List<PFS__c> pfsList = [Select Id, Parent_A__c, Academic_Year_Picklist__c FROM PFS__c
                                    WHERE Parent_A__c in :parentContactIdsWithWaivedPFSButNoOpp
                                    AND PFS_Status__c = 'Application Submitted'
                                            AND Payment_Status__c != 'Paid in Full'];
            System.debug('Pfs list: ' + pfsList);
            FinanceAction.createOffAndTransactionOnlyIfWaived(pfsList);
        }
    }

    // this method updates a field on the PFS to capture when a fee waiver has been requested and is currently pending.
    // It will also revert out of this state once the fee waiver is declined.
    public static void updateFeeWaiverStatus(List<Application_Fee_Waiver__c> feeWaiversToProcess) {
        // gather parent Ids for query
        Set<Id> parentIds = new Set<Id>();
        for (Application_Fee_Waiver__c afw : feeWaiversToProcess) {
            if (afw.Contact__c != null) {
                parentIds.add(afw.Contact__c);
            }
        }

        // map of key (parent Id + '-' + acad year Id) to PFS
        Map<String, PFS__c> parentYearKeyToPFSMap = new Map<String, PFS__c>();
        if (!parentIds.isEmpty()) {
            List<PFS__c> allPFSs = [Select Id, Fee_Waived__c, Academic_Year_Picklist__c, Parent_A__c
                                    FROM PFS__c where Parent_A__c in :parentIds];
            for (PFS__c pfs : allPFSs) {
                if (pfs.Academic_Year_Picklist__c != null) {
                    String key = pfs.Parent_A__c + '-' + pfs.Academic_Year_Picklist__c;
                    parentYearKeyToPFSMap.put(key, pfs);
                }
            }
        }

        // map of PFSs to update (to avoid dupes)
        Map<Id, PFS__c> pfsToUpdateMap = new Map<Id, PFS__c>();

        // set fee waiver status to requested/denied on PFS if it is not correct
        for (Application_Fee_Waiver__c afw : feeWaiversToProcess) {
            String key = afw.Contact__c + '-' + GlobalVariables.getAcademicYear(afw.Academic_Year__c).Name;
            PFS__c pfs = parentYearKeyToPFSMap.get(key);

            // [DP] don't continue if pfs is null 2.3.14
            if (pfs != null) {
                if (afw.Status__c == 'Pending Approval' || afw.Status__c == null) {
                    if (pfs.Fee_Waived__c != WAIVER_REQUESTED_STATUS) {
                        pfs.Fee_Waived__c = WAIVER_REQUESTED_STATUS;
                        pfsToUpdateMap.put(pfs.Id, pfs);
                    }
                } else if (afw.Status__c == WAIVER_DECLINED_STATUS) {
                    if (pfs.Fee_Waived__c != WAIVER_DENIED_STATUS) {
                        pfs.Fee_Waived__c = WAIVER_DENIED_STATUS;
                        pfsToUpdateMap.put(pfs.Id, pfs);
                    }
                }
            }
        }

        // update PFSs
        if (!pfsToUpdateMap.isEmpty()) {
            // one failure won't prevent the entire batch
            Database.SaveResult[] sr = Database.update(pfsToUpdateMap.values(), false);
        }
    }

    /**
     *    SFP-1 If applicant qualifies for a AFW based on Means Based Fee Waiver test
     *            apply AFWs to applicant's PFS [jB]
     */
    public static void autoApplyAFW(PFS__c pfs) {
        List<Application_Fee_Waiver__c> feeWaiversToApply = new List<Application_Fee_Waiver__c>();

        // SFP-139, check for existing fee waivers for parent, this academic year (could be from any school)
        // SFP-190 Change check for existing fee waivers to 'Pending Qualification' and change status to 'declined'
        //         to return waiver credit to school as they have qualified for a MBFW
        List<Application_Fee_Waiver__c> existingAFWs = getExistingApplicationFeeWaivers(
                pfs, new Set<String> { PENDING_QUALIFICATION, ASSIGNED });

        Boolean hasExistingAssignedWaiver = false;

        if (!existingAFWs.isEmpty()) {
            for (Application_Fee_Waiver__c existingAfw : existingAFWs) {
                hasExistingAssignedWaiver = (hasExistingAssignedWaiver || existingAfw.Status__c == ASSIGNED);

                if (existingAfw.Status__c == PENDING_QUALIFICATION) {
                    existingAfw.Status__c = WAIVER_DECLINED_STATUS;
                }
            }
        }

        // add a FW from the MBFW school - existing AFWs are limited - declined waivers aren't counted
        if (!hasExistingAssignedWaiver) {
            Id academicYear = GlobalVariables.getAcademicYearByName(pfs.Academic_Year_Picklist__c).Id;
            Id accountId = ApplicationFeeSettingsService.Instance.getMeansBasedFeeWaiverAccountId();
            feeWaiversToApply.add(new Application_Fee_Waiver__c(
                    Academic_Year__c = academicYear,
                    Contact__c = pfs.Parent_A__c,
                    Status__c = ASSIGNED,
                    Account__c = accountId));
        }
        
        if (!existingAFWs.isEmpty()) {
            update existingAFWs;
        }

        if (!feeWaiversToApply.isEmpty()) {
            insert feeWaiversToApply;
        }
    }

    /**
     * @description Query for any existing Application Fee Waivers associated
     *              with the provided pfs and statuses.
     * @param pfs The PFS that the Application Fee Waivers should be associated
     *        with.
     * @param statuses The Statuses that the Fee Waivers being queried for.
     * @returns A list of Application Fee Waiver records.
     */
    @testVisible
    private static List<Application_Fee_Waiver__c> getExistingApplicationFeeWaivers(PFS__c pfs, Set<String> statuses) {
        Id academicYear = GlobalVariables.getAcademicYearByName(pfs.Academic_Year_Picklist__c).id;

        return [SELECT
                       Id,
                       Account__c,
                       Status__c,
                       Academic_Year__c,
                       Contact__c
                  FROM Application_Fee_Waiver__c
                 WHERE Contact__c = :pfs.Parent_A__c
                   AND Academic_Year__c = :academicYear
                   AND Status__c in :statuses
              ORDER BY Status__c ASC];
    }
}