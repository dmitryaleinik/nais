/**
 * @description This class is responsible for querying Budget Group records.
 */
public class BudgetGroupSelector extends fflib_SObjectSelector {
    
    @testVisible private static final String ID_SET_PARAM = 'idSet';
    
    /**
     * @description Default constructor for an instance of the BudgetGroupSelector that will not include 
     *              field sets while enforcing FLS and CRUD.
     */
    public BudgetGroupSelector() { }

    /**
     * @description Queries Budget Group records by Id.
     * @param idSet The Ids of the Budget Group records to query.
     * @return A list of Budget Group records.
     * @throws ArgumentNullException if idSet is null.
     */
    public List<Budget_Group__c> selectById(Set<Id> idSet) {
        ArgumentNullException.throwIfNull(idSet, ID_SET_PARAM);

        return (List<Budget_Group__c>)super.selectSObjectsById(idSet);
    }

    private Schema.SObjectType getSObjectType() {
        return Budget_Group__c.SObjectType;
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                Budget_Group__c.Name,
                Budget_Group__c.Academic_Year__c,
                Budget_Group__c.Description__c,
                Budget_Group__c.School__c,
                Budget_Group__c.Total_Allocated__c,
                Budget_Group__c.Total_Funds__c,
                Budget_Group__c.Total_Remaining__c
        };
    }

    /**
     * @description Creates a new instance of the BudgetGroupSelector.
     * @return A BudgetGroupSelector.
     */
    public static BudgetGroupSelector newInstance() {
        return new BudgetGroupSelector();
    }
}