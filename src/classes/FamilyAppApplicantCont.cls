public class FamilyAppApplicantCont extends FamilyAppCompController {
    /*Initialization*/
    public FamilyAppApplicantCont(){
        blankApplicant = new Applicant__c();
    }
    /*End Initialization*/

    /*Properties*/
    public Applicant__c blankApplicant { get; set; }
    public String createApplicant { get; set; }
    public Integer rowNum { get; set; }
    public Integer applicantIndex { get; set; }
    public String applicantId { get; set; } //NAIS-1964
    public Boolean areApplicants {
        get {
            // [DP] NAIS-1806 now 4 digit SSN.  Need to do this here so that PFS attribute is set in VF
            if (pfs != null && pfs.Applicants__r != null) {
                for (Applicant__c app : pfs.Applicants__r) {
                    if (app.SSN__c != null){
                        String ssn4 = String.valueOf(app.SSN__c);
                        if (ssn4.length() > 4){
                            app.SSN__c = ssn4.right(4);
                        }
                    }
                }
            }

            return pfs.Applicants__r.size() > 0;
        }
        set;
    }
    /*End Properties*/

    /*Action Methods*/
    //NAIS-1964 Start
    public void deleteApplicant() {
        deleteApplicantAndUpdateRelatedPFS(applicantId);
        // [DP] 03.25.2016 SFP-201 method was refactored into virtual method so it could also be used by FamilyAppHouseSumCont (see FamilyAppCompController)
    }

    //NAIS-1964 End
}