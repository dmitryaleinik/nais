/*
 * NAIS-499, SPEC-047 Finance - PFS Sale, R-246
 *    When an application is submitted for the first time, if an opportunity of type Application Fee does not already exist on the PFS record,
 *    it needs to be created, and a transaction line item for the cost of the Application needs to be created as well. This would be a
 *    transaction line item of type PFS Sale.
 *
 * WH, Exponent Partners, 2013
 */
@isTest
private class FinanceActionTest
{

    /*
     * NAIS-499, SPEC-047 Finance - PFS Sale, R-246
     */

    @isTest
    private static void oppAndTransactionCreatedForPFSSubmitOnInsert() {

        // current year fees - these should be used
        Application_Fee_Settings__c feeSettings = new Application_Fee_Settings__c();
        feeSettings.Name = GlobalVariables.getCurrentYearString();
        feeSettings.Amount__c = 50;
        feeSettings.Discounted_Amount__c = 40;
        feeSettings.Product_Code__c = 'Test Product Code'; // [SL] NAIS-1031: new required field
        feeSettings.Discounted_Product_Code__c = 'KS Test Product Code'; // [SL] NAIS-1031: new required field
        insert feeSettings;

        Contact parent1 = TestUtils.createContact('Parent 1', null, RecordTypes.parentContactTypeId, false);
        Contact parent2 = TestUtils.createContact('Parent 2', null, RecordTypes.parentContactTypeId, false);
        insert new List<Contact> { parent1, parent2 };

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfs1 = TestUtils.createPFS('PFS 1', academicYearId, parent1.Id, false);
        pfs1.PFS_Status__c = 'Application Submitted';
        //PFS__c pfs2 = TestUtils.createPFS('PFS 2', academicYearId, parent2.Id, false);
        //pfs2.PFS_Status__c = 'Application In Progress';
        List<PFS__c> allPFSs = new List<PFS__c> { pfs1 };

        Test.startTest();
        insert allPFSs;

        // [DP] 07.07.2015 NAIS-1933 now need to call this explicitly
        Set<Id> pfsIds = new Set<Id>();
        for (PFS__c pfs : allPFSs){
            pfsIds.add(pfs.Id);
        }
        FinanceAction.createOppAndTransaction(pfsIds);

        List<Opportunity> opps = [select Id, Name, StageName, CloseDate, PFS__c, Academic_Year_Picklist__c, Parent_A__c, RecordTypeId from Opportunity];
        System.assertEquals(1, opps.size());
        //System.assertEquals('PFS 1 - Application Fee', opps[0].Name); [DP] 07.07.2015 set by workflow, no need to test
        System.assertEquals('Open', opps[0].StageName);
        System.assertEquals(Date.today(), opps[0].CloseDate);
        System.assertEquals(pfs1.Id, opps[0].PFS__c);
        System.assertEquals(GlobalVariables.getCurrentAcademicYear().Name, opps[0].Academic_Year_Picklist__c);
        System.assertEquals(parent1.Id, opps[0].Parent_A__c);
        System.assertEquals(RecordTypes.pfsApplicationFeeOpportunityTypeId, opps[0].RecordTypeId);

        List<Transaction_Line_Item__c> trans = [select Id, Amount__c, Opportunity__c, Transaction_Type__c, Transaction_Status__c, RecordTypeId from Transaction_Line_Item__c];
        System.assertEquals(1, trans.size());
        System.assertEquals(feeSettings.Amount__c, trans[0].Amount__c);
        System.assertEquals(opps[0].Id, trans[0].Opportunity__c);
        System.assertEquals('PFS Sale', trans[0].Transaction_Type__c);
        System.assertEquals('Posted', trans[0].Transaction_Status__c);
        System.assertEquals(RecordTypes.saleTransactionTypeId, trans[0].RecordTypeId);
        Test.stopTest();

    }

    @isTest
    private static void oppAndTransactionCreatedForPFSSubmitOnUpdate() {

        // current year fees - these should be used
        Application_Fee_Settings__c feeSettings = new Application_Fee_Settings__c();
        feeSettings.Name = GlobalVariables.getCurrentYearString();
        feeSettings.Amount__c = 50;
        feeSettings.Discounted_Amount__c = 40;
        feeSettings.Product_Code__c = 'Test Product Code'; // [SL] NAIS-1031: new required field
        feeSettings.Discounted_Product_Code__c = 'KS Test Product Code'; // [SL] NAIS-1031: new required field
        insert feeSettings;

        Contact parent1 = TestUtils.createContact('Parent 1', null, RecordTypes.parentContactTypeId, false);
        Contact parent2 = TestUtils.createContact('Parent 2', null, RecordTypes.parentContactTypeId, false);
        Contact parent3 = TestUtils.createContact('Parent 3', null, RecordTypes.parentContactTypeId, false);
        insert new List<Contact> { parent1, parent2, parent3 };

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfs1 = TestUtils.createPFS('PFS 1', academicYearId, parent1.Id, false);
        PFS__c pfs2 = TestUtils.createPFS('PFS 2', academicYearId, parent2.Id, false);
        //PFS__c pfs3 = TestUtils.createPFS('PFS 3', academicYearId, parent3.Id, false);
        List<PFS__c> allPFSs = new List<PFS__c> { pfs1, pfs2/*, pfs3 */};
        insert allPFSs;

        Opportunity oldOpp = TestUtils.createOpportunity('Old Opp', RecordTypes.pfsApplicationFeeOpportunityTypeId , pfs1.Id, GlobalVariables.getCurrentYearString(), false);
        oldOpp.StageName = 'Open';
        oldOpp.CloseDate = Date.today().addDays(-30);
        insert oldOpp;

        // not inserted, pending bug fix in TransactionLineItemAfter trigger; but no material impact to this test
        // Transaction_Line_Item__c oldTrans = TestUtils.createTransactionLineItem(oldOpp.Id, RecordTypes.saleTransactionTypeId, false);
        Transaction_Line_Item__c oldTrans = TestUtils.createTransactionLineItem(oldOpp.Id, RecordTypes.saleTransactionTypeId, true); // [SL] NAIS-1034: refactored code, should be able to insert now

        Test.startTest();
        pfs1.PFS_Status__c = 'Application Submitted';    // already has pfs app fee opp and sale transaction => no new opp and transaction created
        pfs2.PFS_Status__c = 'Application Submitted';    // new pfs app fee opp and sale transaction should be created for pfs2
        //pfs3.PFS_Status__c = 'Application In Progress';    // not yet applied => no new opp or transaction created
        update allPFSs;

        // [DP] 07.07.2015 NAIS-1933 now need to call this explicitly
        Set<Id> pfsIds = new Set<Id>();
        for (PFS__c pfs : allPFSs){
            pfsIds.add(pfs.Id);
        }
        FinanceAction.createOppAndTransaction(pfsIds);


        List<Opportunity> opps = [select Id, Name, StageName, CloseDate, PFS__c, Academic_Year_Picklist__c, Parent_A__c, RecordTypeId from Opportunity where Id != :oldOpp.Id];
        System.assertEquals(1, opps.size());
        System.assertEquals('PFS 2 - Application Fee', opps[0].Name);
        System.assertEquals('Open', opps[0].StageName);
        System.assertEquals(Date.today(), opps[0].CloseDate);
        System.assertEquals(pfs2.Id, opps[0].PFS__c);
        System.assertEquals(GlobalVariables.getCurrentAcademicYear().Name, opps[0].Academic_Year_Picklist__c);
        System.assertEquals(parent2.Id, opps[0].Parent_A__c);
        System.assertEquals(RecordTypes.pfsApplicationFeeOpportunityTypeId, opps[0].RecordTypeId);

        List<Transaction_Line_Item__c> trans = [select Id, Amount__c, Opportunity__c, Transaction_Type__c, Transaction_Status__c, RecordTypeId from Transaction_Line_Item__c where Id != :oldTrans.Id];
        System.assertEquals(1, trans.size());
        System.assertEquals(feeSettings.Amount__c, trans[0].Amount__c);
        System.assertEquals(opps[0].Id, trans[0].Opportunity__c);
        System.assertEquals('PFS Sale', trans[0].Transaction_Type__c);
        System.assertEquals('Posted', trans[0].Transaction_Status__c);
        System.assertEquals(RecordTypes.saleTransactionTypeId, trans[0].RecordTypeId);
        Test.stopTest();

    }

    @isTest
    private static void discountedAppFeeForPFSAppliedToKSSchool() {

        Account ksSchool = TestUtils.createAccount('KS School', RecordTypes.schoolAccountTypeId, 3, true);

        SchoolPortalSettings.KS_School_Account_Id = ksSchool.Id;

        // current year fees - these should be used
        Application_Fee_Settings__c feeSettings = new Application_Fee_Settings__c();
        feeSettings.Name = GlobalVariables.getCurrentYearString();
        feeSettings.Amount__c = 50;
        feeSettings.Discounted_Amount__c = 40;
        feeSettings.Product_Code__c = 'Test Product Code'; // [SL] NAIS-1031: new required field
        feeSettings.Discounted_Product_Code__c = 'KS Test Product Code'; // [SL] NAIS-1031: new required field
        insert feeSettings;

        // past year fees - these should not be used
        Application_Fee_Settings__c oldFeeSettings = new Application_Fee_Settings__c();
        oldFeeSettings.Name = '2010-2011';
        oldFeeSettings.Amount__c = 100;
        oldFeeSettings.Discounted_Amount__c = 80;
        oldFeeSettings.Product_Code__c = 'Test Product Code'; // [SL] NAIS-1031: new required field
        oldFeeSettings.Discounted_Product_Code__c = 'KS Test Product Code'; // [SL] NAIS-1031: new required field
        insert oldFeeSettings;

        Contact parent1 = TestUtils.createContact('Parent 1', null, RecordTypes.parentContactTypeId, false);
        Contact parent2 = TestUtils.createContact('Parent 2', null, RecordTypes.parentContactTypeId, false);

        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);

        insert new List<Contact> { parent1, parent2, student1 };

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfs1 = TestUtils.createPFS('PFS 1', academicYearId, parent1.Id, false);
        PFS__c pfs2 = TestUtils.createPFS('PFS 2', academicYearId, parent2.Id, false);
        List<PFS__c> allPFSs = new List<PFS__c> { pfs1, pfs2 };
        insert allPFSs;

        Applicant__c applicant1 = TestUtils.createApplicant(student1.Id, pfs1.Id, true);

        Student_Folder__c studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, true);

        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1.Id, ksSchool.Id, studentFolder1.Id, true);

        Test.startTest();
        pfs1.PFS_Status__c = 'Application Submitted';    // applied to KS School => qualifies for discount
        pfs2.PFS_Status__c = 'Application Submitted';    // does not qualify for discount
        update allPFSs;

        // [DP] 07.07.2015 NAIS-1933 now need to call this explicitly
        Set<Id> pfsIds = new Set<Id>();
        for (PFS__c pfs : allPFSs){
            pfsIds.add(pfs.Id);
        }

        FinanceAction.createOppAndTransaction(pfsIds);
        List<Opportunity> opps = [select Id, PFS__c, Parent_A__c from Opportunity where PFS__c in :allPFSs order by Name];
        System.assertEquals(2, opps.size());
        System.assertEquals(pfs1.Id, opps[0].PFS__c);
        System.assertEquals(parent1.Id, opps[0].Parent_A__c);
        System.assertEquals(pfs2.Id, opps[1].PFS__c);
        System.assertEquals(parent2.Id, opps[1].Parent_A__c);

        List<Transaction_Line_Item__c> trans = [select Id, Amount__c, Opportunity__c from Transaction_Line_Item__c where Opportunity__c in :opps order by Opportunity__r.Name];
        System.assertEquals(2, trans.size());
        System.assertEquals(opps[0].Id, trans[0].Opportunity__c);                    // pfs1
        System.assertEquals(feeSettings.Discounted_Amount__c, trans[0].Amount__c);    // discounted amount for pfs1
        System.assertEquals(opps[1].Id, trans[1].Opportunity__c);                    // pfs2
        System.assertEquals(feeSettings.Amount__c, trans[1].Amount__c);                // full amount for pfs2
        Test.stopTest();

    }

    @isTest
    private static void oppAndTransactionOwnershipForPFSSubmitByStandardUser() {

        // current year fees - these should be used
        Application_Fee_Settings__c feeSettings = new Application_Fee_Settings__c();
        feeSettings.Name = GlobalVariables.getCurrentYearString();
        feeSettings.Amount__c = 50;
        feeSettings.Discounted_Amount__c = 40;
        feeSettings.Product_Code__c = 'Test Product Code'; // [SL] NAIS-1031: new required field
        feeSettings.Discounted_Product_Code__c = 'KS Test Product Code'; // [SL] NAIS-1031: new required field
        insert feeSettings;

        Contact parent = TestUtils.createContact('Parent', null, RecordTypes.parentContactTypeId, true);

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfs = TestUtils.createPFS('PFS', academicYearId, parent.Id, false);
        pfs.PFS_Status__c = 'Application Submitted';

        User u = TestUtils.createPortalUser('Parent', 'parent@test.com', 'ptc', parent.Id, GlobalVariables.familyPortalProfileId, true, true);

        Family_Portal_Settings__c fps = new Family_Portal_Settings__c();
        fps.Name = 'Family';
        fps.Individual_Account_Owner_Id__c = u.Id;
        insert fps;

        Test.startTest();

        insert pfs;

        // [DP] 07.07.2015 NAIS-1933 now need to call this explicitly
        Set<Id> pfsIds = new Set<Id>();
        pfsIds.add(pfs.Id);
        FinanceAction.createOppAndTransaction(pfsIds);

        List<Opportunity> opps = [select Id, OwnerId, (Select Id, Transaction_Type__c from Transaction_Line_Items__r) from Opportunity];
        System.assertEquals(1, opps.size());
        System.assertEquals(1, opps[0].Transaction_Line_Items__r.size());
        System.assertEquals(UserInfo.getUserId(), opps[0].OwnerId);
        System.assertNotEquals(fps.Individual_Account_Owner_Id__c, opps[0].OwnerId);
        System.assert([select count() from OpportunityShare where OpportunityId = :opps[0].Id] > 1);    // there is one or more Opp sharing rule

        Test.stopTest();

    }

    @isTest
    private static void oppAndTransactionOwnershipForPFSSubmitByStandardUserWithExistingWaiver() {

        Account school1 = TestUtils.createAccount('name', RecordTypes.schoolAccountTypeId, 5, true);

        // current year fees - these should be used
        Application_Fee_Settings__c feeSettings = new Application_Fee_Settings__c();
        feeSettings.Name = GlobalVariables.getCurrentYearString();
        feeSettings.Amount__c = 50;
        feeSettings.Discounted_Amount__c = 40;
        feeSettings.Product_Code__c = 'Test Product Code'; // [SL] NAIS-1031: new required field
        feeSettings.Discounted_Product_Code__c = 'KS Test Product Code'; // [SL] NAIS-1031: new required field
        insert feeSettings;

        Contact parent = TestUtils.createContact('Parent', null, RecordTypes.parentContactTypeId, true);

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        Application_Fee_Waiver__c preexistingWaiver = Testutils.createApplicationFeeWaiver(school1.Id, parent.Id, academicYearId, false);
        preexistingWaiver.Status__c = 'Assigned';
        insert preexistingWaiver;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfs = TestUtils.createPFS('PFS', academicYearId, parent.Id, false);
        pfs.PFS_Status__c = 'Application Submitted';

        User u = TestUtils.createPortalUser('Parent', 'parent@test.com', 'ptc', parent.Id, GlobalVariables.familyPortalProfileId, true, true);

        Family_Portal_Settings__c fps = new Family_Portal_Settings__c();
        fps.Name = 'Family';
        fps.Individual_Account_Owner_Id__c = u.Id;
        insert fps;

        Test.startTest();

        insert pfs;

        //// [DP] 07.07.2015 NAIS-1933 now need to call this explicitly
        //Set<Id> pfsIds = new Set<Id>();
        //pfsIds.add(pfs.Id);
        //FinanceAction.createOppAndTransaction(pfsIds);

        List<Opportunity> opps = [select Id, OwnerId, (Select Id, Transaction_Type__c from Transaction_Line_Items__r) from Opportunity];
        System.assertEquals(1, opps.size());
        // expect 2 TLIs -- one for the amount owed and one for the fee waived
        System.assertEquals(2, opps[0].Transaction_Line_Items__r.size());
        Integer waivers = 0;
        Integer pfsSales = 0;
        for (Transaction_Line_Item__c tli : opps[0].Transaction_Line_Items__r){
            if (tli.Transaction_Type__c == 'PFS Sale'){pfsSales++;}
            if (tli.Transaction_Type__c == 'PFS Fee Waiver'){waivers++;}
        }
        System.assertEquals(1, waivers);
        System.assertEquals(1, pfsSales);

        System.assertEquals(UserInfo.getUserId(), opps[0].OwnerId);
        System.assertNotEquals(fps.Individual_Account_Owner_Id__c, opps[0].OwnerId);
        System.assert([select count() from OpportunityShare where OpportunityId = :opps[0].Id] > 1);    // there is one or more Opp sharing rule

        Test.stopTest();

    }

    @isTest
    private static void oppAndTransactionOwnershipForPFSSubmitByFamilyPortalUser() {

        Account school1 = TestUtils.createAccount('name', RecordTypes.schoolAccountTypeId, 5, true);

        // current year fees - these should be used
        Application_Fee_Settings__c feeSettings = new Application_Fee_Settings__c();
        feeSettings.Name = GlobalVariables.getCurrentYearString();
        feeSettings.Amount__c = 50;
        feeSettings.Discounted_Amount__c = 40;
        feeSettings.Product_Code__c = 'Test Product Code'; // [SL] NAIS-1031: new required field
        feeSettings.Discounted_Product_Code__c = 'KS Test Product Code'; // [SL] NAIS-1031: new required field
        insert feeSettings;

        Contact parent = TestUtils.createContact('Parent', null, RecordTypes.parentContactTypeId, true);

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        Application_Fee_Waiver__c preexistingWaiver = Testutils.createApplicationFeeWaiver(school1.Id, parent.Id, academicYearId, false);
        preexistingWaiver.Status__c = 'Assigned';
        insert preexistingWaiver;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfs = PfsTestData.Instance
                .forAcademicYearPicklist(GlobalVariables.getCurrentAcademicYear().Name)
                .forParentA(parent.Id)
                .asSubmitted()
                .create();

        User u = TestUtils.createPortalUser('Parent', 'parent@test.com', 'ptc', parent.Id, GlobalVariables.familyPortalProfileId, true, true);

        Family_Portal_Settings__c fps = new Family_Portal_Settings__c();
        fps.Name = 'Family';
        fps.Individual_Account_Owner_Id__c = UserInfo.getUserId();
        insert fps;

        Test.startTest();

        System.runAs(u) {
            insert pfs;
        }

        // [DP] 07.07.2015 NAIS-1933 now need to call this explicitly
        List<Opportunity> opps = [select Id, OwnerId, (Select Id, Transaction_Type__c from Transaction_Line_Items__r) from Opportunity WHERE PFS__c = :pfs.Id];
        System.assertEquals(1, opps.size(), 'Expected there to be an opportunity generated because there is a pre-existing waiver.');
        opps[0].StageName = 'Open';
        update opps[0];

        FinanceAction.createOppAndTransaction(new Set<Id> { pfs.Id });

        opps = [select Id, OwnerId, (Select Id, Transaction_Type__c from Transaction_Line_Items__r) from Opportunity WHERE PFS__c = :pfs.Id];
        System.assertEquals(1, opps.size(), 'Expected there to still only be one opportunity.');
        // expect 2 TLIs -- one for the amount owed and one for the fee waived
        System.assertEquals(2, opps[0].Transaction_Line_Items__r.size());
        Integer waivers = 0;
        Integer pfsSales = 0;
        for (Transaction_Line_Item__c tli : opps[0].Transaction_Line_Items__r){
            if (tli.Transaction_Type__c == 'PFS Sale'){pfsSales++;}
            if (tli.Transaction_Type__c == 'PFS Fee Waiver'){waivers++;}
        }
        System.assertEquals(1, waivers);
        System.assertEquals(1, pfsSales);

        System.assertNotEquals(u.Id, opps[0].OwnerId);
        System.assertEquals(fps.Individual_Account_Owner_Id__c, opps[0].OwnerId);
        System.assert([select count() from OpportunityShare where OpportunityId = :opps[0].Id] > 1);    // there is one or more Opp sharing rule

        Test.stopTest();

    }

    @isTest
    private static void oppAndTransactionOwnershipForPFSSubmitByFamilyPortalUserWithExistingWaiver() {

        // current year fees - these should be used
        Application_Fee_Settings__c feeSettings = new Application_Fee_Settings__c();
        feeSettings.Name = GlobalVariables.getCurrentYearString();
        feeSettings.Amount__c = 50;
        feeSettings.Discounted_Amount__c = 40;
        feeSettings.Product_Code__c = 'Test Product Code'; // [SL] NAIS-1031: new required field
        feeSettings.Discounted_Product_Code__c = 'KS Test Product Code'; // [SL] NAIS-1031: new required field
        insert feeSettings;

        Contact parent = TestUtils.createContact('Parent', null, RecordTypes.parentContactTypeId, true);

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfs = TestUtils.createPFS('PFS', academicYearId, parent.Id, false);
        pfs.PFS_Status__c = 'Application Submitted';

        User u = TestUtils.createPortalUser('Parent', 'parent@test.com', 'ptc', parent.Id, GlobalVariables.familyPortalProfileId, true, true);

        Family_Portal_Settings__c fps = new Family_Portal_Settings__c();
        fps.Name = 'Family';
        fps.Individual_Account_Owner_Id__c = UserInfo.getUserId();
        insert fps;

        Test.startTest();

        System.runAs(u) {
            insert pfs;
        }

        // [DP] 07.07.2015 NAIS-1933 now need to call this explicitly
        Set<Id> pfsIds = new Set<Id>();
        pfsIds.add(pfs.Id);
        FinanceAction.createOppAndTransaction(pfsIds);

        List<Opportunity> opps = [select Id, OwnerId, (Select Id, Transaction_Type__c from Transaction_Line_Items__r) from Opportunity];
        System.assertEquals(1, opps.size());
        System.assertEquals(1, opps[0].Transaction_Line_Items__r.size());
        System.assertNotEquals(u.Id, opps[0].OwnerId);
        System.assertEquals(fps.Individual_Account_Owner_Id__c, opps[0].OwnerId);
        System.assert([select count() from OpportunityShare where OpportunityId = :opps[0].Id] > 1);    // there is one or more Opp sharing rule

        Test.stopTest();

    }

    // [SL] NAIS-1031
    @isTest
    private static void testAddRemoveSchool() {
        Account ksSchool = TestUtils.createAccount('KS School', RecordTypes.schoolAccountTypeId, 3, true);
        Account nonKSSchool = TestUtils.createAccount('Non-KS School', RecordTypes.schoolAccountTypeId, 3, true);

        SchoolPortalSettings.KS_School_Account_Id = ksSchool.Id;

        // current year fees - these should be used
        Application_Fee_Settings__c feeSettings = new Application_Fee_Settings__c();
        feeSettings.Name = GlobalVariables.getCurrentYearString();
        feeSettings.Amount__c = 50;
        feeSettings.Discounted_Amount__c = 40;
        feeSettings.Product_Code__c = 'Test Product Code'; // [SL] NAIS-1031: new required field
        feeSettings.Discounted_Product_Code__c = 'KS Test Product Code'; // [SL] NAIS-1031: new required field
        insert feeSettings;

        Contact parent1 = TestUtils.createContact('Parent 1', null, RecordTypes.parentContactTypeId, false);
        Contact parent2 = TestUtils.createContact('Parent 2', null, RecordTypes.parentContactTypeId, false);
        insert new List<Contact> { parent1, parent2 };

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfs1 = TestUtils.createPFS('PFS 1', academicYearId, parent1.Id, false);
        pfs1.PFS_Status__c = 'Application In Progress';

        Test.startTest();
        insert pfs1;

        // add a KS school
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, true);
        Applicant__c applicant1 = TestUtils.createApplicant(student1.Id, pfs1.Id, true);
        Student_Folder__c studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, true);
        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1.Id, ksSchool.Id, studentFolder1.Id, false);
        insert spfsa1;

        pfs1.PFS_Status__c = 'Application Submitted';
        update pfs1;

        // [DP] 07.07.2015 NAIS-1933 now need to call this explicitly
        Set<Id> pfsIds = new Set<Id>();
        pfsIds.add(pfs1.Id);
        FinanceAction.createOppAndTransaction(pfsIds);

        // verify discounted amount
        List<Transaction_Line_Item__c> trans = [select Id, Amount__c, Opportunity__c, Product_Amount__c, Transaction_Type__c, Transaction_Status__c, RecordTypeId from Transaction_Line_Item__c];
        System.assertEquals(1, trans.size());
        System.assertEquals(feeSettings.Discounted_Amount__c, trans[0].Amount__c);

        List<Opportunity> opps = [select Id, Amount, Name, StageName, CloseDate, PFS__c, Academic_Year_Picklist__c, Parent_A__c, RecordTypeId from Opportunity];
        System.assertEquals(1, opps.size());
        System.assertEquals(feeSettings.Discounted_Amount__c, opps[0].Amount);

        /* [CH] This is an invalid test because TLI records are created once a PFS is Submitted and Paid, after which school selection cannot be
                changed.  Additionally, TLI should never have their Amount__c changed, but would instead get an Adjustment TLI
                should be entered to adjust or negate the previous TLI

        // add a non KS school
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1.Id, nonKSSchool.Id, studentFolder1.Id, false);
        spfsa2.PFS_Status__c = 'Application Submitted';
        insert spfsa2;

        // verify regular amount
        opps = [select Id, Amount, Name, StageName, CloseDate, PFS__c, Academic_Year__c, Parent_A__c, RecordTypeId from Opportunity];
        System.assertEquals(1, opps.size());
        System.assertEquals(feeSettings.Amount__c, opps[0].Amount);

        trans = [select Id, Amount__c, Opportunity__c, Transaction_Type__c, Transaction_Status__c, RecordTypeId from Transaction_Line_Item__c];
        System.assertEquals(1, trans.size());
        System.assertEquals(feeSettings.Amount__c, trans[0].Amount__c);

        // withdraw from the non-KS school
        spfsa2.Withdrawn__c = 'Yes';
        update spfsa2;

        // verify discounted amount
        opps = [select Id, Amount, Name, StageName, CloseDate, PFS__c, Academic_Year__c, Parent_A__c, RecordTypeId from Opportunity];
        System.assertEquals(1, opps.size());
        System.assertEquals(feeSettings.Discounted_Amount__c, opps[0].Amount);

        trans = [select Id, Amount__c, Opportunity__c, Transaction_Type__c, Transaction_Status__c, RecordTypeId from Transaction_Line_Item__c];
        System.asfsertEquals(1, trans.size());
        System.assertEquals(feeSettings.Discounted_Amount__c, trans[0].Amount__c);
        */

        Test.stopTest();
    }


    @isTest
    private static void getAmountsTesting() {

        // current year fees - these should be used
        Application_Fee_Settings__c feeSettings = new Application_Fee_Settings__c();
        feeSettings.Name = GlobalVariables.getCurrentYearString();
        feeSettings.Amount__c = 50;
        feeSettings.Discounted_Amount__c = 40;
        feeSettings.Product_Code__c = 'Test Product Code'; // [SL] NAIS-1031: new required field
        feeSettings.Discounted_Product_Code__c = 'KS Test Product Code'; // [SL] NAIS-1031: new required field
        insert feeSettings;

        Contact parent1 = TestUtils.createContact('Parent 1', null, RecordTypes.parentContactTypeId, false);
        Contact parent2 = TestUtils.createContact('Parent 2', null, RecordTypes.parentContactTypeId, false);
        insert new List<Contact> { parent1, parent2 };

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfs1 = TestUtils.createPFS('PFS 1', academicYearId, parent1.Id, false);
        pfs1.PFS_Status__c = 'Application Submitted';
        //PFS__c pfs2 = TestUtils.createPFS('PFS 2', academicYearId, parent2.Id, false);
        //pfs2.PFS_Status__c = 'Application In Progress';
        List<PFS__c> allPFSs = new List<PFS__c> { pfs1 };

        Test.startTest();
        insert allPFSs;

        // [DP] 07.07.2015 NAIS-1933 now need to call this explicitly
        Set<Id> pfsIds = new Set<Id>();
        for (PFS__c pfs : allPFSs){
            pfsIds.add(pfs.Id);
        }
        FinanceAction.createOppAndTransaction(pfsIds);

        List<Opportunity> opps = [select Id, Name, StageName, CloseDate, PFS__c, Academic_Year_Picklist__c, Parent_A__c, RecordTypeId, Amount, Net_Amount_Due__c from Opportunity];
        System.assertEquals(1, opps.size());
        //System.assertEquals('PFS 1 - Application Fee', opps[0].Name); [DP] 07.07.2015 set by workflow, no need to test
        System.assertEquals('Open', opps[0].StageName);
        System.assertEquals(Date.today(), opps[0].CloseDate);
        System.assertEquals(pfs1.Id, opps[0].PFS__c);
        System.assertEquals(GlobalVariables.getCurrentAcademicYear().Name, opps[0].Academic_Year_Picklist__c);
        System.assertEquals(parent1.Id, opps[0].Parent_A__c);
        System.assertEquals(RecordTypes.pfsApplicationFeeOpportunityTypeId, opps[0].RecordTypeId);

        List<Transaction_Line_Item__c> trans = [select Id, Amount__c, Opportunity__c, Transaction_Type__c, Transaction_Status__c, RecordTypeId from Transaction_Line_Item__c];
        System.assertEquals(1, trans.size());
        System.assertEquals(feeSettings.Amount__c, trans[0].Amount__c);
        System.assertEquals(opps[0].Id, trans[0].Opportunity__c);
        System.assertEquals('PFS Sale', trans[0].Transaction_Type__c);
        System.assertEquals('Posted', trans[0].Transaction_Status__c);
        System.assertEquals(RecordTypes.saleTransactionTypeId, trans[0].RecordTypeId);

        System.assertEquals(feeSettings.Amount__c, FinanceAction.getAmountTotal(opps[0], opps[0].Academic_Year_Picklist__c, false));
        System.assertEquals(feeSettings.Amount__c, FinanceAction.getAmountTotal(null, opps[0].Academic_Year_Picklist__c, false));
        System.assertEquals(feeSettings.Discounted_Amount__c, FinanceAction.getAmountTotal(null, opps[0].Academic_Year_Picklist__c, true));
        System.assertEquals(feeSettings.Amount__c, FinanceAction.getAmountDue(opps[0], opps[0].Academic_Year_Picklist__c, false));
        System.assertEquals(false, FinanceAction.isKSOnlyPFS(pfs1));

        Test.stopTest();

    }

    @isTest
    private static void testIsKSSchool() {

        Account ksSchool = TestUtils.createAccount('KS School', RecordTypes.schoolAccountTypeId, 3, true);

        SchoolPortalSettings.KS_School_Account_Id = ksSchool.Id;

        // current year fees - these should be used
        Application_Fee_Settings__c feeSettings = new Application_Fee_Settings__c();
        feeSettings.Name = GlobalVariables.getCurrentYearString();
        feeSettings.Amount__c = 50;
        feeSettings.Discounted_Amount__c = 40;
        feeSettings.Product_Code__c = 'Test Product Code'; // [SL] NAIS-1031: new required field
        feeSettings.Discounted_Product_Code__c = 'KS Test Product Code'; // [SL] NAIS-1031: new required field
        insert feeSettings;

        // past year fees - these should not be used
        Application_Fee_Settings__c oldFeeSettings = new Application_Fee_Settings__c();
        oldFeeSettings.Name = '2010-2011';
        oldFeeSettings.Amount__c = 100;
        oldFeeSettings.Discounted_Amount__c = 80;
        oldFeeSettings.Product_Code__c = 'Test Product Code'; // [SL] NAIS-1031: new required field
        oldFeeSettings.Discounted_Product_Code__c = 'KS Test Product Code'; // [SL] NAIS-1031: new required field
        insert oldFeeSettings;

        Contact parent1 = TestUtils.createContact('Parent 1', null, RecordTypes.parentContactTypeId, false);
        Contact parent2 = TestUtils.createContact('Parent 2', null, RecordTypes.parentContactTypeId, false);

        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);

        insert new List<Contact> { parent1, parent2, student1 };

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfs1 = TestUtils.createPFS('PFS 1', academicYearId, parent1.Id, false);
        PFS__c pfs2 = TestUtils.createPFS('PFS 2', academicYearId, parent2.Id, false);
        List<PFS__c> allPFSs = new List<PFS__c> { pfs1, pfs2 };
        insert allPFSs;

        Applicant__c applicant1 = TestUtils.createApplicant(student1.Id, pfs1.Id, true);

        Student_Folder__c studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, true);

        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1.Id, ksSchool.Id, studentFolder1.Id, true);

        Test.startTest();
        pfs1.PFS_Status__c = 'Application Submitted';    // applied to KS School => qualifies for discount
        pfs2.PFS_Status__c = 'Application Submitted';    // does not qualify for discount
        update allPFSs;

        // [DP] 07.07.2015 NAIS-1933 now need to call this explicitly
        Set<Id> pfsIds = new Set<Id>();
        for (PFS__c pfs : allPFSs){
            pfsIds.add(pfs.Id);
        }

        FinanceAction.createOppAndTransaction(pfsIds);
        List<Opportunity> opps = [select Id, Name, StageName, CloseDate, PFS__c, Academic_Year_Picklist__c, Parent_A__c, RecordTypeId, Amount, Net_Amount_Due__c from Opportunity where PFS__c in :allPFSs order by Name];
        System.assertEquals(2, opps.size());
        System.assertEquals(pfs1.Id, opps[0].PFS__c);
        System.assertEquals(parent1.Id, opps[0].Parent_A__c);
        System.assertEquals(pfs2.Id, opps[1].PFS__c);
        System.assertEquals(parent2.Id, opps[1].Parent_A__c);

        List<Transaction_Line_Item__c> trans = [select Id, Amount__c, Opportunity__c from Transaction_Line_Item__c where Opportunity__c in :opps order by Opportunity__r.Name];
        System.assertEquals(2, trans.size());
        System.assertEquals(opps[0].Id, trans[0].Opportunity__c);                    // pfs1
        System.assertEquals(feeSettings.Discounted_Amount__c, trans[0].Amount__c);    // discounted amount for pfs1
        System.assertEquals(opps[1].Id, trans[1].Opportunity__c);                    // pfs2
        System.assertEquals(feeSettings.Amount__c, trans[1].Amount__c);                // full amount for pfs2

        System.assertEquals(feeSettings.Discounted_Amount__c, FinanceAction.getAmountTotal(opps[0], opps[0].Academic_Year_Picklist__c, true));
        System.assertEquals(feeSettings.Discounted_Amount__c, FinanceAction.getAmountTotal(null, opps[0].Academic_Year_Picklist__c, true));
        System.assertEquals(true, FinanceAction.isKSOnlyPFS(pfs1));
        System.assertEquals(true, FinanceAction.isKSOnlyPFS(new Set<Id> {ksSchool.Id}));
        System.assertEquals(true, FinanceAction.isKSOnlyPFS(new List<School_PFS_Assignment__c>{spfsa1}));


        Test.stopTest();

    }
}