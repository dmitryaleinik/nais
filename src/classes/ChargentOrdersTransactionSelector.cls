/**
 * @description Queries for ChargentOrders__Transaction__c records.
 **/
public class ChargentOrdersTransactionSelector extends fflib_SObjectSelector {
    private static final String TRANSACTION_IDS_PARAM = 'transactionIds';

    /**
     * @description Select ChargentOrders__Transaction__c records by their Ids.
     * @param transactionIds The Ids to select Transaction records by.
     * @return A list of ChargentOrders__Transaction__c records.
     * @throws An ArgumentNullException if the transactionIds param is null.
     */
    public List<ChargentOrders__Transaction__c> selectById(Set<Id> transactionIds) {
        ArgumentNullException.throwIfNull(transactionIds, TRANSACTION_IDS_PARAM);

        if (isEnforcingCRUD()) {
            assertIsAccessible();
        }

        return Database.query(String.format('SELECT {0} FROM {1} WHERE Id IN :transactionIds ORDER BY {2}',
                new List<String> {
                        getFieldListString(),
                        getSObjectName(),
                        getOrderBy()
                }));
    }

    private Schema.SObjectType getSObjectType() {
        return ChargentOrders__Transaction__c.getSObjectType();
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                ChargentOrders__Transaction__c.ChargentOrders__Account__c,
                ChargentOrders__Transaction__c.ChargentOrders__Amount__c,
                ChargentOrders__Transaction__c.ChargentOrders__Authorization__c,
                ChargentOrders__Transaction__c.ChargentOrders__AVS_International__c,
                ChargentOrders__Transaction__c.ChargentOrders__AVS_Response_Code__c,
                ChargentOrders__Transaction__c.ChargentOrders__AVS_Zip__c,
                ChargentOrders__Transaction__c.ChargentOrders__Bank_Account_Name__c,
                ChargentOrders__Transaction__c.ChargentOrders__Bank_Account_Number__c,
                ChargentOrders__Transaction__c.ChargentOrders__Bank_Account_Type__c,
                ChargentOrders__Transaction__c.ChargentOrders__Bank_Name__c,
                ChargentOrders__Transaction__c.ChargentOrders__Bank_Routing_Number__c,
                ChargentOrders__Transaction__c.ChargentOrders__Billing_Address__c,
                ChargentOrders__Transaction__c.ChargentOrders__Billing_Address_Line_2__c,
                ChargentOrders__Transaction__c.ChargentOrders__Billing_City__c,
                ChargentOrders__Transaction__c.ChargentOrders__Billing_Company__c,
                ChargentOrders__Transaction__c.ChargentOrders__Billing_Country__c,
                ChargentOrders__Transaction__c.ChargentOrders__Billing_Email__c,
                ChargentOrders__Transaction__c.ChargentOrders__Billing_Fax__c,
                ChargentOrders__Transaction__c.ChargentOrders__Billing_First__c,
                ChargentOrders__Transaction__c.ChargentOrders__Billing_Last__c,
                ChargentOrders__Transaction__c.ChargentOrders__Billing_Phone__c,
                ChargentOrders__Transaction__c.ChargentOrders__Billing_Postal_Code__c,
                ChargentOrders__Transaction__c.ChargentOrders__Billing_Province__c,
                ChargentOrders__Transaction__c.ChargentOrders__Billing_State__c,
                ChargentOrders__Transaction__c.ChargentOrders__Card_Last_4__c,
                ChargentOrders__Transaction__c.ChargentOrders__Cash_Disbursement__c,
                ChargentOrders__Transaction__c.ChargentOrders__Cash_Receipt__c,
                ChargentOrders__Transaction__c.ChargentOrders__Credit_Card_Name__c,
                ChargentOrders__Transaction__c.ChargentOrders__Credit_Card_Number__c,
                ChargentOrders__Transaction__c.ChargentOrders__Credit_Card_Type__c,
                ChargentOrders__Transaction__c.ChargentOrders__Currency__c,
                ChargentOrders__Transaction__c.ChargentOrders__Customer_Token__c,
                ChargentOrders__Transaction__c.ChargentOrders__Description__c,
                ChargentOrders__Transaction__c.ChargentOrders__Details__c,
                ChargentOrders__Transaction__c.ChargentOrders__Details_Payflow__c,
                ChargentOrders__Transaction__c.ChargentOrders__Gateway__c,
                ChargentOrders__Transaction__c.ChargentOrders__Gateway_Date__c,
                ChargentOrders__Transaction__c.ChargentOrders__Gateway_ID__c,
                ChargentOrders__Transaction__c.ChargentOrders__Gateway_Response__c,
                ChargentOrders__Transaction__c.ChargentOrders__Order__c,
                ChargentOrders__Transaction__c.ChargentOrders__Payment_Method__c,
                ChargentOrders__Transaction__c.ChargentOrders__Payment_Request__c,
                ChargentOrders__Transaction__c.ChargentOrders__Reason_Code__c,
                ChargentOrders__Transaction__c.ChargentOrders__Reason_Text__c,
                ChargentOrders__Transaction__c.ChargentOrders__Recurring__c,
                ChargentOrders__Transaction__c.ChargentOrders__Response__c,
                ChargentOrders__Transaction__c.ChargentOrders__Response_Code__c,
                ChargentOrders__Transaction__c.ChargentOrders__Response_Message__c,
                ChargentOrders__Transaction__c.ChargentOrders__Response_Status__c,
                ChargentOrders__Transaction__c.ChargentOrders__Card_Code_Response__c,
                ChargentOrders__Transaction__c.ChargentOrders__Tokenization__c,
                ChargentOrders__Transaction__c.ChargentOrders__Type__c
        };
    }

    /**
     * @description Singleton property ensuring external sources do not
     *              instantiate this directly.
     */
    public static ChargentOrdersTransactionSelector Instance {
        get {
            if (Instance == null) {
                Instance = new ChargentOrdersTransactionSelector();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to enforce singleton access
     *              via the Instance property.
     */
    private ChargentOrdersTransactionSelector() {
        super(false, false, false);
    }
}