/*
 * SPEC-146, NAIS-85
 *
 * Check to make sure a W2 is not a duplicate
 *
 * Nathan, Exponent Partners, 2013
 */
public class W2DuplicateCheck {

    // to prevent trigger recursive calls
    public static Boolean isExecuting = false;

    // constructor
    public W2DuplicateCheck() {
    
    }

    // future method makes logic simple. otherwise we have to process pre event for new or updates of incoming
    // records and update other existing family document records that are connected to households as 2nd step. 
    // this is very complicated. with future you have all records available and it is single step update
    
    @future
    public static void DuplicateCheckFuture(Set <string> householdIdSet) {
        DuplicateCheck(householdIdSet);
    }
    
    public static void DuplicateCheck(Set <string> householdIdSet) {

        // start trigger execution
        isExecuting = true;

        Map <String, List <Family_Document__c>> key_fdList_map = new Map <String, List <Family_Document__c>>();
        List <Family_Document__c> fdUpdateList = new List <Family_Document__c>();
        List <Family_Document__c> fdUpdateDeletedList = new List <Family_Document__c>();
        
        system.debug('householdIdSet.size(): ' + householdIdSet.size());

        // nothing to process
        if (householdIdSet.size() == 0) {
            isExecuting = false;
            return;
        }

        List<Family_Document__c> fdList = FamilyDocumentsSelector.newInstance().selectW2sByHousehold(householdIdSet);

        system.debug('fdList.size(): ' + fdList.size());

        for (Family_Document__c fd : fdList) {
            List <Family_Document__c> fdListLocal;

            if (fd.Deleted__c == false && isDocVerified(fd)) {
                if (key_fdList_map.containsKey(getKey(fd))) {
                    fdListLocal = key_fdList_map.get(getKey(fd));
                } else {
                    fdListLocal = new List <Family_Document__c>();
                }
                fdListLocal.add(fd);
                key_fdList_map.put(getKey(fd), fdListLocal);
            } else {
                if (fd.Duplicate__c != null || fd.Duplicate_Family_Document__c != null) {
                    fd.Duplicate__c = null;
                    fd.Duplicate_Family_Document__c = null;
                    fdUpdateDeletedList.add(fd);
                }
            }
        }

        system.debug('key_fdList_map.size(): ' + key_fdList_map.size());
        system.debug('fdUpdateDeletedList.size(): ' + fdUpdateDeletedList.size());

        // scan list and mark duplicates
        for (String key : key_fdList_map.keySet()) {
            List <Family_Document__c> fdListLocal = key_fdList_map.get(key);
            
            // set each 1st record as non duplicate
            if (fdListLocal.size() > 0) {
                Family_Document__c fdFirst = fdListLocal[0];
                if (fdFirst.Duplicate__c != null || fdFirst.Duplicate_Family_Document__c != null) {
                    fdFirst.Duplicate__c = null;
                    fdFirst.Duplicate_Family_Document__c = null; 
                    fdUpdateList.add(fdFirst);
                }
                
                // determine if other records are duplicates
                if (fdListLocal.size() > 1) {
                    for (Integer i=1; i<fdListLocal.size(); i++) {
                        // reset other records
                        Family_Document__c fdNext = fdListLocal[i];
                        if (fdNext.Duplicate__c != 'Yes' || fdNext.Duplicate_Family_Document__c != fdFirst.Id) {
                            fdNext.Duplicate__c = 'Yes';
                            fdNext.Duplicate_Family_Document__c = fdFirst.Id; 
                            fdUpdateList.add(fdNext);
                        }   
                    }
                } 
            } 
        }            

        if (fdUpdateList.size() > 0) {
           update fdUpdateList;
        } 
        
        if (fdUpdateDeletedList.size() > 0) {
            update fdUpdateDeletedList;
        }
                                   
        // end trigger execution
        isExecuting = false;
    }

    // NAIS-2254 [DP] 02.03.2015 changing W2 matching criteria
    private static String getKey(Family_Document__c fd) {
        String key;
        key = fd.Household__c;
        //key += '_' + fd.Academic_Year__c;
        //key += '_' + fd.W2_Deferred_Untaxed_1_A__c;
        //key += '_' + fd.W2_Deferred_Untaxed_2_B__c;
        //key += '_' + fd.W2_Deferred_Untaxed_3_C__c;
        //key += '_' + fd.W2_Deferred_Untaxed_4_D__c;
        //key += '_' + fd.W2_Deferred_Untaxed_5_E__c;
        //key += '_' + fd.W2_Deferred_Untaxed_6_F__c;
        //key += '_' + fd.W2_Deferred_Untaxed_7_G__c;
        //key += '_' + fd.W2_Deferred_Untaxed_8_H__c;
        key += '_' + fd.W2_Employee_ID__c;
        key += '_' + fd.W2_Employer_ID__c;
        //key += '_' + fd.W2_Medicare__c;
        //key += '_' + fd.W2_SS_Tax__c;
        key += '_' + fd.W2_Wages__c;
        key += '_' + fd.Document_Year__c;

        return key;
    }

    private static Boolean isDocVerified(Family_Document__c docToCheck) {
        return docToCheck.W2_Employee_ID__c != null ||
                docToCheck.W2_Employer_ID__c != null ||
                docToCheck.W2_Wages__c != null;
    }
}