/**
 * DataAccessService.cls
 *
 * @description Data access service base class that all other data access classes may extend to get provided base functionality for free
 *
 * @author Chase Logan @ Presence PG
 */
public abstract class DataAccessService {
    // common exception class
    public class DataAccessServiceException extends Exception {}

    // Overloaded option gets an sObject's field names provided a valid sObject type name
    protected List<String> getSObjectFieldNames( String sObjectName) {
        
        return getSObjectFieldNames( sObjectName, null);
    }

    // Gets an sObject's field names provided a valid sObject type name, excludes any field names in 
    // exclude list
    protected List<String> getSObjectFieldNames( String sObjectName, List<String> fieldExcludeList) {
        List<String> returnList;

        if ( !String.isEmpty( sObjectName)) {

            Schema.SObjectType sObjType = Schema.getGlobalDescribe().get( sObjectName);
            if ( sObjType != null) {

                Map<String, Schema.SObjectField> fieldNameMap = sObjType.getDescribe().fields.getMap();
                Set<String> workingSet = fieldNameMap.keySet();
                // exclude fields if present
                if ( fieldExcludeList != null && fieldExcludeList.size() > 0) {

                    workingSet.removeAll( fieldExcludeList);
                }
                returnList = new List<String>();
                returnList.addAll( workingSet);
            }
        }
         
         return returnList;
    }

}