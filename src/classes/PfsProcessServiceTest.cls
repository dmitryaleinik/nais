@isTest 
private class PfsProcessServiceTest {
    
    public static PFS__c pfsRecord;
    public static ApplicationUtils appUtils;
    public static String currentScreen;
    public static Id academicYearId;
    public static Academic_Year__c academicYear;
    public static Applicant__c applicant;
    public static Contact parentA, student1;
    public static School_PFS_Assignment__c spa_1, spa_2;
    
    static void generateBasicPFSData()
    {
        FeatureToggles.setToggle('PFS_EZ_Enabled__c', true);
        
        insertFamilyAppSettings();
        List<Household_Income_Threshold__c> result = HouseholdIncomeThresholdTestData.Instance.insertIncomeThresholds();
        academicYearId = result[0].Academic_Year__c;
        academicYear = GlobalVariables.getAcademicYear(academicYearId);
        
        //Create parent and student.
        parentA = ContactTestData.Instance.asParent().create();
        student1 = ContactTestData.Instance.asStudent().create();
        insert new List<Contact>{parentA, student1};
        
        
        User userRecord = [select Id, Name, ContactId, Contact.LastName, Contact.Name, Contact.MailingStreet, Contact.MailingCity, Contact.MailingState,  
                        Contact.MailingPostalCode, Contact.MailingCountry, Contact.Gender__c, Contact.Birthdate, 
                        Contact.Email, Contact.HomePhone, Contact.MobilePhone, Contact.Preferred_Phone__c,
                        Contact.FirstName, Contact.Middle_Name__c, Contact.Suffix__c, Contact.Salutation, Contact.Phone,
                        LanguageLocaleKey
                        from User 
                        where Id = :UserInfo.getUserId()];
                        
        pfsRecord = PfsTestData.Instance
		    .forParentA(parentA.Id)
		    .forAcademicYearPicklist(academicYear.Name)
		    .insertPfs();
        
        applicant =  ApplicantTestData.Instance
		    .forContactId(student1.Id)
		    .forPfsId(pfsRecord.Id)
		    .insertApplicant();
    
        pfsRecord = ApplicationUtils.queryPFSRecord(pfsRecord.Id);
        
        if (currentScreen != null){
            appUtils = new ApplicationUtils(UserInfo.getLanguage(), pfsRecord, currentScreen);
        } else {
            appUtils = new ApplicationUtils(UserInfo.getLanguage(), pfsRecord);
        }
    }
    
    static void insertFamilyAppSettings() {
        
        List<FamilyAppSettings__c> familyAppSettings = new List<FamilyAppSettings__c> {
            FamilyAppSettingsTestData.Instance
                    .forName('ParentsGuardians')
                    .forMainLabelField('SchoolSelection__c')
                    .forNextPage('FamilyIncome')
                    .forStatusField('statSelectSchools__c')
                    .forSubLabelField('SelectSchools__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset(),

            FamilyAppSettingsTestData.Instance
                    .forName('SelectSchools')
                    .forMainLabelField('SchoolSelection__c')
                    .forNextPage('FamilyIncome')
                    .forStatusField('statSelectSchools__c')
                    .forSubLabelField('SelectSchools__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset(),

            FamilyAppSettingsTestData.Instance
                    .forName('ApplicantInformation')
                    .forMainLabelField('SchoolSelection__c')
                    .forNextPage('FamilyIncome')
                    .forStatusField('statSelectSchools__c')
                    .forSubLabelField('SelectSchools__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset(),

            FamilyAppSettingsTestData.Instance
                    .forName('BusinessFarm')
                    .forMainLabelField('BusinessFarm__c')
                    .forNextPage('BusinessInformation')
                    .forIsSpeedbumpPage(true)
                    .forStatusField('statBusinessFarmBoolean__c')
                    .createFamilyAppSettingsWithoutReset(),

            FamilyAppSettingsTestData.Instance
                    .forName('BusinessInformation')
                    .forMainLabelField('BusinessFarm__c')
                    .forNextPage('BusinessIncome')
                    .forStatusField('statBusinessInformation__c')
                    .forSubLabelField('BusinessInformation__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset(),

            FamilyAppSettingsTestData.Instance
                    .forName('BusinessAssets')
                    .forMainLabelField('BusinessFarm__c')
                    .forNextPage('OtherInformation')
                    .forLoopBackPage('BusinessInformation')
                    .forStatusField('statBusinessAssets__c')
                    .forSubLabelField('BusinessAssets__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset(),

            FamilyAppSettingsTestData.Instance
                    .forName('ForKamehamehaProgram')
                    .forMainLabelField('ForKamehameha__c')
                    .forNextPage('OtherInformation')
                    .forLoopBackPage('ForKamehamehaApplicant')
                    .forStatusField('statForKamehamehaProgram__c')
                    .forSubLabelField('ForKamehameha__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset(),

            FamilyAppSettingsTestData.Instance
                    .forName('EducationalExpenses')
                    .forMainLabelField('SchoolSelection__c')
                    .forNextPage('OtherInformation')
                    .forStatusField('statSelectSchools__c')
                    .forSubLabelField('SelectSchools__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset(),
                    
            FamilyAppSettingsTestData.Instance
                    .forName('BusinessSummary')
                    .forMainLabelField('OtherInformation')
                    .forNextPage('BusinessFarm')
                    .forStatusField('statBusinessSummary__c')
                    .forSubLabelField('BusinessSummary__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset()
        };

        insert familyAppSettings;
    }
    
    static PFS__c requeryPFS(Id pfsId) {
        
        Set<String> pfsFieldsSet = Schema.SObjectType.PFS__c.fields.getMap().keySet();
        List<String> pfsFieldsList = new List<String>();
        pfsFieldsList.addAll(pfsFieldsSet);
        String pfsFieldsStr = String.join(pfsFieldsList, ',');            
        List<PFS__c> pfsRecords = Database.query('SELECT '+pfsFieldsStr.removeEnd(',')+' FROM PFS__c WHERE Id=\''+pfsId+'\' LIMIT 1');            
            
        return pfsRecords[0];
    }
    
    @isTest private static void checkHiddenItemsUI_MetadataPFSEZEnabledWithFieldsToHide_expectSectionsHidden() {
        
        FeatureToggles.setToggle('PFS_EZ_Enabled__c', true);
        
        Map<String, String> hiddenSectionsForCheck = new Map<String, String>{
            'StudentIncome' => '9. Applicant Income',
            'RealEstate' => '10. Real Estate',
            'Vehicles' => '11. Vehicles',
            'BusinessInformation' => '15. Business - Information',
            'BusinessIncome' => '16. Business - Income',
            'BusinessExpenses' => '17. Business - Expenses',
            'BusinessAssets' => '18. Business - Assets & Debts',
            'BusinessSummary' => '19. Business/Farm Summary'};
       
       FamilyPortalHiddenFields config = PfsProcessService.Instance.checkHiddenItemsUI('EZ');
       Integer verifiedSections = 0;
       String missingSections = '';
       
       system.assertEquals(true, PfsProcessService.Instance.isEZProcess('EZ'));
       system.assertEquals(true, config.hiddenFieldsByScreenMapKey != '');
       
       
       for(String fieldName : hiddenSectionsForCheck.keySet()) {
           if( config.hiddenFieldsByScreenMapKey.contains(fieldName) ) {
               verifiedSections ++;
           } else {
               missingSections +=fieldName + ', ';
           }
       }
       
       system.assertEquals('', missingSections, 'The following sections must be hidden for EZ PFS: '+missingSections);
                        
       system.assertEquals(hiddenSectionsForCheck.size(), verifiedSections,
            'There is some missing configurations for Family_Portal_Hidden_Fields__mdt');
            
    }//End:checkSectionsVisibility
    
    @isTest private static void checkHiddenItemsUI_MetadataPFSEZEnabledWithFieldsToHide_expectFieldsHidden() {
        
        FeatureToggles.setToggle('PFS_EZ_Enabled__c', true);
            
        Map<String, String> hiddenFieldsForCheck = new Map<String, String>{ 
            'Interest_Income_Current__c' => '7c',
            'Dividend_Income_Current__c' => '7d',
            'IRS_Adjustments_to_Income_Current__c' => '7f',
            'Untaxed_IRA_Plan_Payment_Current__c' => '7g',
            'Keogh_Plan_Payment_Current__c' => '7h',
            'Deductible_Part_SE_Tax_Current__c' => '7i',
            'IRS_Adjustments_to_Income_Descrip__c' => '7j',
            'Current_Tax_Return_Status__c' => '6a',
            'Federal_Income_Tax_Reported__c' => '6g',
            'Tax_Defer_Pension_Saving_Current__c' => '8d',
            'Fringe_Benefit_Plan_Untaxed_Current__c' => '8e',
            'Cash_Support_Gift_Income_Current__c' => '8f',
            'HH_Expense_Support_Received_Current__c' => '8g',
            'Outside_Allowances_Received_Current__c' => '8h',
            'Income_Credit_Benefits_Received_Current__c' => '8i',
            'Welfare_Veterans_Workers_Comp_Current__c' => '8j',
            'Tax_Exempt_Investments_Current__c' => '8k',
            'Income_Abroad_Current__c' => '8l',
            'Other_Untaxed_Income_Current__c' => '8m',
            'Other_Untaxed_Income_Descrip__c' => '8n',
            'Other_Non_Taxable_Income_Current__c' => '8o',
            'Investments_Itemized__c' => '12c',
            'Parent_A_Employee_Retirement_Plan__c' => '12d',
            'Parent_B_Employee_Retirement_Plan__c' => '12e',
            'Parents_Retirement_Total_Value__c' => '12f',
            'Outstanding_Debt_Itemized__c' => '12h',
            'Debt_Amount_Pay_Off_Est__c' => '12i',
            'Consumer_Debt_Total__c' => '12j',
            'Consumer_Debt_Itemized__c' => '12k',
            'Medical_Dental_Insurance_Current__c' => '14c',
            'Annual_Club_Fees__c' => '14f',
            'Club_Membership_Description__c' => '14g',
            'Camps_Lessons_Annual_Cost__c' => '14h',
            'Camps_Lessons_Description__c' => '14i',
            'Family_Vacations_Annual_Cost__c' => '14j'
            };
       
       FamilyPortalHiddenFields config = PfsProcessService.Instance.checkHiddenItemsUI('EZ');
       Integer verifiedFields = 0;
       String missingFields = '';
       
       system.assertEquals(true, PfsProcessService.Instance.isEZProcess('EZ'));
       system.assertEquals(true, config.hiddenFieldsByScreenMapKey != '');
       
       for(String fieldName : hiddenFieldsForCheck.keySet()) {
           if( config.hiddenFieldsByScreenMapKey.contains(fieldName) ) {
               verifiedFields ++;
           } else {
               missingFields += fieldName + ', ';
           }
       }
       
       system.assertEquals('', missingFields, 'The following fields must be hidden for EZ PFS: '+missingFields);
       
       system.assertEquals(hiddenFieldsForCheck.size(), verifiedFields,
            'There is some missing configurations for Family_Portal_Hidden_Fields__mdt');
    }//End:checkFieldsVisibility
    
    @isTest private static void toFull_pfsHasEZSubmissionProcess_expectPfsUpdated() {
        
        generateBasicPFSData();
        pfsRecord.Submission_Process__c = 'EZ';
        update pfsRecord;
        
        Test.startTest();
            PfsProcessService.Instance.toFull(pfsRecord);
        Test.stopTest();
        
        system.assertEquals('Full', (requeryPFS(pfsRecord.Id)).Submission_Process__c);
    }
    
    @isTest private static void isAllowedSection_sections1To6_isAllowed() {
        
        System.assertEquals(true, PfsProcessService.Instance.isAllowedSection('householdinformation'));
        System.assertEquals(true, PfsProcessService.Instance.isAllowedSection('parentsguardians'));
        System.assertEquals(true, PfsProcessService.Instance.isAllowedSection('applicantinformation'));
        System.assertEquals(true, PfsProcessService.Instance.isAllowedSection('dependentinformation'));
        System.assertEquals(true, PfsProcessService.Instance.isAllowedSection('householdsummary'));
        System.assertEquals(true, PfsProcessService.Instance.isAllowedSection('schoolselection'));
        System.assertEquals(true, PfsProcessService.Instance.isAllowedSection('selectschools'));
        System.assertEquals(false, PfsProcessService.Instance.isAllowedSection('AdditionalQuestions'));
        System.assertEquals(false, PfsProcessService.Instance.isAllowedSection('ForKamehameha'));
        System.assertEquals(false, PfsProcessService.Instance.isAllowedSection('OtherInformation'));
    }
    
    /**
    * @description The logic to determine PFS FULL or PFS EZ is in this order:
    *              “True” to 3 = PFS EZ (stop)
    *              “True” to 4 OR 5 = PFS Full (stop)
    *              “True” to 1 AND 2 AND “False” to 4 AND 5 (PFS EZ)
    */
    @isTest private static void calculateSubmissionProcess_pfsSubmissionProcess_expectEZ() {
        
        generateBasicPFSData();
        pfsRecord.Family_Size__c = 4;
        pfsRecord.Parent_A_Country__c = 'United States';        
        update pfsRecord;
        
        Test.startTest();
            
            PageReference mainPage = Page.FamilyApplicationMain;
            mainPage.getParameters().put('id', pfsRecord.Id);
            mainPage.getParameters().put('screen', 'selectschools');
            Test.setCurrentPage(mainPage);
            
            pfsRecord.Household_Income_Under_Threshold__c = 'Yes';
            pfsRecord.Household_Assets_Under_Threshold__c = 'Yes';
            pfsRecord.Household_Has_Received_Benefits__c = 'No';
            pfsRecord.Guardian_Owns_Business__c = 'No';
            pfsRecord.Guardian_Self_Employed_Freelancer__c = 'No';
            System.assertEquals('EZ', PfsProcessService.Instance.calculateSubmissionProcess(pfsRecord));
            
            pfsRecord.Household_Income_Under_Threshold__c = 'No';
            pfsRecord.Household_Assets_Under_Threshold__c = 'No';
            pfsRecord.Household_Has_Received_Benefits__c = 'Yes';
            pfsRecord.Guardian_Owns_Business__c = 'No';
            pfsRecord.Guardian_Self_Employed_Freelancer__c = 'No';
            System.assertEquals('EZ', PfsProcessService.Instance.calculateSubmissionProcess(pfsRecord));
            
            //1, 2, 3, 4 to “No" and question 5 to “Yes”
            pfsRecord.Household_Income_Under_Threshold__c = 'No';
            pfsRecord.Household_Assets_Under_Threshold__c = 'No';
            pfsRecord.Household_Has_Received_Benefits__c = 'No';
            pfsRecord.Guardian_Owns_Business__c = 'No';
            pfsRecord.Guardian_Self_Employed_Freelancer__c = 'Yes';
            System.assertEquals('Full', PfsProcessService.Instance.calculateSubmissionProcess(pfsRecord));
            
            //1, 2, 3, 5 to “No” and question 4 to “Yes"
            pfsRecord.Household_Income_Under_Threshold__c = 'No';
            pfsRecord.Household_Assets_Under_Threshold__c = 'No';
            pfsRecord.Household_Has_Received_Benefits__c = 'No';
            pfsRecord.Guardian_Owns_Business__c = 'Yes';
            pfsRecord.Guardian_Self_Employed_Freelancer__c = 'No';
            System.assertEquals('Full', PfsProcessService.Instance.calculateSubmissionProcess(pfsRecord));
        Test.stopTest();
    }
    
    @isTest private static void LockNavigationForNonAllowedSections_pfsRecord_expectTrue() {
        generateBasicPFSData();
        FeatureToggles.setToggle('PFS_EZ_Enabled__c', true);
        System.assertEquals(true, PfsProcessService.Instance.LockNavigationForNonAllowedSections(pfsRecord));
    }
    
    @isTest private static void LockNavigationForNonAllowedSections_pfsRecord_expectFalse() {
        generateBasicPFSData();
        FeatureToggles.setToggle('PFS_EZ_Enabled__c', true);
        
        pfsRecord.Submission_Process__c = 'EZ';
        pfsRecord.Family_Size__c = null;
        pfsRecord.Parent_B__c = null;
        pfsRecord.statDependentInformation__c = true;
        pfsRecord.statSelectSchools__c = true;
        update pfsRecord;
        
        Test.startTest();
            System.assertEquals(true, pfsRecord.Applicants__r.size()>0);
            System.assertEquals(true, pfsRecord.Family_Status__c != null);
            System.assertEquals(true, pfsRecord.statDependentInformation__c);
            System.assertEquals(false, PfsProcessService.Instance.LockNavigationForNonAllowedSections(pfsRecord));
        Test.stopTest();
    }
    
    @isTest private static void checkPFSStatsToAllowNavigation_allBasicSectionsCompleted_expectTrue() {
        generateBasicPFSData();
        FeatureToggles.setToggle('PFS_EZ_Enabled__c', true);
        
        pfsRecord.statParentsGuardians__c  = true;
        pfsRecord.statApplicantInformation__c   = true;
        pfsRecord.statDependentInformation__c  = true;
        pfsRecord.statHouseholdSummary__c   = true;        
        pfsRecord.statSelectSchools__c = true;
        update pfsRecord;
        
        Test.startTest();
        pfsRecord = requeryPFS(pfsRecord.Id);
        System.assertEquals(true, PfsProcessService.Instance.checkPFSStatsToAllowNavigation(pfsRecord));
        Test.stopTest();
    }
    
    @isTest private static void checkPFSStatsToAllowNavigation_oneBasicSectionIncompleted_expectTrue() {
        generateBasicPFSData();
        FeatureToggles.setToggle('PFS_EZ_Enabled__c', true);
        
        pfsRecord.statParentsGuardians__c  = true;
        pfsRecord.statApplicantInformation__c   = true;
        pfsRecord.statDependentInformation__c  = true;
        pfsRecord.statHouseholdSummary__c   = true;        
        pfsRecord.statSelectSchools__c = false;
        update pfsRecord;
        
        Test.startTest();
            pfsRecord = requeryPFS(pfsRecord.Id);
            System.assertEquals(false, PfsProcessService.Instance.checkPFSStatsToAllowNavigation(pfsRecord));
        Test.stopTest();
    }
    
    @isTest private static void getEZQuestionLabelFields_EZEnabled_expectTrue() {
        List<String> labels = new List<String>();
        FeatureToggles.setToggle('PFS_EZ_Enabled__c', true);
        labels = PfsProcessService.Instance.getEZQuestionLabelFields(labels);
        System.assertEquals(true, labels.size() > 0);
    }
    
    @isTest private static void getEZQuestionLabelFields_EZEnabled_expectFalse() {
        List<String> labels = new List<String>();
        FeatureToggles.setToggle('PFS_EZ_Enabled__c', false);
        labels = PfsProcessService.Instance.getEZQuestionLabelFields(labels);
        System.assertEquals(false, labels.size() > 0);
    }
    
    @isTest private static void reCalculateSubmissionProcess_EZPfsWithEZToggleActive_expectedFullPfs() {
        generateBasicPFSData();
        FeatureToggles.setToggle('PFS_EZ_Enabled__c', true);
        
        pfsRecord.Submission_Process__c = 'EZ';
        update pfsRecord;
        
        Test.startTest();
            pfsRecord.Own_Business_or_Farm__c = 'Yes';
            PfsProcessService.RecalculateResult result = PfsProcessService.Instance.reCalculateSubmissionProcess(pfsRecord, 'BasicTax');
            system.assertEquals(true, result.hasChanged);
        Test.stopTest();
    }
    
    @isTest private static void reCalculateSubmissionProcess_FullPfsWithEZToggleActive_expectedFullPfs() {
        generateBasicPFSData();
        FeatureToggles.setToggle('PFS_EZ_Enabled__c', true);
        
        pfsRecord.Submission_Process__c = 'Full';
        update pfsRecord;
        
        Test.startTest();
            pfsRecord.Own_Business_or_Farm__c = 'Yes';
            PfsProcessService.RecalculateResult result = PfsProcessService.Instance.reCalculateSubmissionProcess(pfsRecord, 'BasicTax');
            system.assertEquals(false, result.hasChanged);
        Test.stopTest();
    }
    
    @isTest private static void isSectionHidden_EZPFS_Section19IsHidden()
    {
        generateBasicPFSData();
        FeatureToggles.setToggle('PFS_EZ_Enabled__c', true);
        
        pfsRecord.Submission_Process__c = 'EZ';
        update pfsRecord;
        
        Test.startTest();
            System.assertEquals('EZ', pfsRecord.Submission_Process__c);
            System.assertEquals(true, PfsProcessService.Instance.isSectionHidden(pfsRecord.Submission_Process__c, 'BusinessSummary'));
        Test.stopTest();
    }
    
    private static void setupHasOptOutEZ(Boolean school1HasOptOutEZ, Boolean school2HasOptOutEZ) {
        
        generateBasicPFSData();
        
        //Create 2 schools that has opt out EZ
        Account school_1 = AccountTestData.Instance.asSchool().create();
        
        Account school_2 = AccountTestData.Instance
            .asSchool()
            .create();
        insert new List<Account>{school_1, school_2};
        
        Annual_Setting__c annualSetting_1 = AnnualSettingsTestData.Instance
            .forSchoolId(school_1.Id)
            .forAcademicYearId(academicYear.Id)
            .forOptOutPfsEz(school1HasOptOutEZ)
            .create();
        
        Annual_Setting__c annualSetting_2 = AnnualSettingsTestData.Instance
            .forSchoolId(school_2.Id)
            .forAcademicYearId(academicYear.Id)
            .forOptOutPfsEz(school2HasOptOutEZ)
            .create();
        insert new List<Annual_Setting__c>{annualSetting_1, annualSetting_2};
        
        System.AssertNotEquals(null, student1.Id);
        //Create Folders.
		Student_Folder__c folder_1 = StudentFolderTestData.Instance.forName('Folder1')
		    .forAcademicYearPicklist(academicYear.Name)
		    .forStudentId(student1.Id)
		    .forSchoolId(school_1.Id)
		    .create();
		    
        Student_Folder__c folder_2 = StudentFolderTestData.Instance.forName('Folder2')
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student1.Id)
            .forSchoolId(school_2.Id)
            .create();
        insert new List<Student_Folder__c>{folder_1, folder_2};
        
        //Create 2 spas, one for each school.
        spa_1 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forApplicantId(applicant.Id)
            .forSchoolId(school_1.Id)
            .forStudentFolderId(folder_1.Id)
            .create();
            
        spa_2 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forApplicantId(applicant.Id)
            .forSchoolId(school_2.Id)
            .forStudentFolderId(folder_1.Id)
            .create();
    }
    
    @isTest 
    private static void calculateSubmissionProcess_allSelectedSchoolsHasOptOutEZ_expectedFull() {
        
        FeatureToggles.setToggle('PFS_EZ_Enabled__c', true);
        
        setupHasOptOutEZ(true, true);
        
        insert new List<School_PFS_Assignment__c>{spa_1, spa_2};
        
        pfsRecord.Household_Income_Under_Threshold__c = 'Yes';
        pfsRecord.Household_Assets_Under_Threshold__c = 'Yes';
        pfsRecord.Household_Has_Received_Benefits__c = 'Yes';
        pfsRecord.Guardian_Owns_Business__c = 'Yes';
        pfsRecord.Guardian_Self_Employed_Freelancer__c = 'Yes';
        pfsRecord.PFS_Status__c = 'Application In Progress';
        
        update pfsRecord;
        
        Test.startTest();
            System.assertEquals('Full', PfsProcessService.Instance.calculateSubmissionProcess(pfsRecord));
        Test.stopTest();
    }
    
    @isTest 
    private static void calculateSubmissionProcess_atLeastOneSelectedSchoolsHasOptOutEZ_expectedFull() {
        
        FeatureToggles.setToggle('PFS_EZ_Enabled__c', true);
        
        setupHasOptOutEZ(false, true);
        
        insert new List<School_PFS_Assignment__c>{spa_1, spa_2};
        
        pfsRecord.Household_Income_Under_Threshold__c = 'Yes';
        pfsRecord.Household_Assets_Under_Threshold__c = 'Yes';
        pfsRecord.Household_Has_Received_Benefits__c = 'Yes';
        pfsRecord.Guardian_Owns_Business__c = 'Yes';
        pfsRecord.Guardian_Self_Employed_Freelancer__c = 'Yes';
        pfsRecord.PFS_Status__c = 'Application In Progress';
        
        update pfsRecord;
        
        Test.startTest();
            System.assertEquals('Full', PfsProcessService.Instance.calculateSubmissionProcess(pfsRecord));
        Test.stopTest();
    }
    
    
    
    @isTest 
    private static void calculateSubmissionProcess_insertWithdrawnSpaForASchoolThatHasOptOutEzForAEzPfs_expectedEZ() {
        
        //This case cover the scenario where 2 spas are related to the given PFS:
        // One for a school that has NOT opt out EZ.
        // Another for a school that has opt out EZ. But, it has Withdrawn__c = 'Yes'.
        //And the questions to determine if its an EZ pfs, will return EZ.
        //This means that the pfs should remain with submission Process equals to EZ.
        FeatureToggles.setToggle('PFS_EZ_Enabled__c', true);
        
        setupHasOptOutEZ(false, true);
        
        spa_2.Withdrawn__c = 'Yes';
        insert new List<School_PFS_Assignment__c>{spa_1, spa_2};
        
        pfsRecord.Household_Income_Under_Threshold__c = 'Yes';
        pfsRecord.Household_Assets_Under_Threshold__c = 'Yes';
        pfsRecord.Household_Has_Received_Benefits__c = 'Yes';
        pfsRecord.Guardian_Owns_Business__c = 'Yes';
        pfsRecord.Guardian_Self_Employed_Freelancer__c = 'Yes';
        pfsRecord.PFS_Status__c = 'Application Submitted';
        
        update pfsRecord;
        
        Test.startTest();
        
            System.assertEquals('EZ', PfsProcessService.Instance.calculateSubmissionProcess(pfsRecord));
        Test.stopTest();
    }
}