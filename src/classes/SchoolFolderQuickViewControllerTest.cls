/*
* SPEC-091
*
* The EFC should be visible throughout the folder view and aid allocation processes, because the school users 
* will be changing fields and needing to understand the ramifications on the EFC
* The system should provide a summary area for each folder with the most relevant information that appears on each screen
*
* Nathan, Exponent Partners, 2013
*/
@isTest
private class SchoolFolderQuickViewControllerTest {

    private class TestData {
        Account school1;
        Contact staff1, parentA, parentB, student1, student2;
        Id academicYearId;
        String academicYearName;
        PFS__c pfs1;
        Applicant__c applicant1;
        Student_Folder__c studentFolder1;
        School_PFS_Assignment__c spfsa1;

        private TestData() {
            // school
            school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, null, false); 
            insert school1;       
        
            // school staff
            staff1 = TestUtils.createContact('Staff 1', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
            insert staff1;
            
            // parents and students
            parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
            parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
            student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
            student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
            insert new List<Contact> {parentA, parentB, student1, student2};

            // academic year
            TestUtils.createAcademicYears();
            academicYearId = GlobalVariables.getCurrentAcademicYear().Id;  
            academicYearName = GlobalVariables.getCurrentAcademicYear().Name;  
            
            // psf
            pfs1 = TestUtils.createPFS('PFS A', academicYearName, parentA.Id, false);
            insert new List<PFS__c> {pfs1};
            
            // applicant
            applicant1 = TestUtils.createApplicant(student1.Id, pfs1.Id, false);
            insert new List<Applicant__c> {applicant1};
            
            // student folder
            studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearName, student1.Id, false);
            insert studentFolder1;
            
            // school pfs assignment
            spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearName, applicant1.Id, school1.Id, studentFolder1.Id, false);
            insert spfsa1;
        }
    }

    @isTest
    private static void testCurrentAcademicYear() {
        TestData td = new TestData();

        Test.startTest();
        SchoolFolderQuickViewController controller = new SchoolFolderQuickViewController();
        controller.studentFolderId = td.studentFolder1.Id;
        Student_Folder__c studentFolder = controller.studentFolder;    // this will load loadFolderQuickView()
        system.AssertEquals(td.academicYearId, controller.selectedAcademicYear.Id);
        Test.stopTest();    
    }    

    @isTest
    private static void testSiblingApplicant() {
        TestData td = new TestData();

        Test.startTest();
        SchoolFolderQuickViewController controller = new SchoolFolderQuickViewController();
        controller.studentFolderId = td.studentFolder1.Id;
            
        // applicant 2 - sibling applicant. applying to same school 1
        Applicant__c applicant2 = TestUtils.createApplicant(td.student2.Id, td.pfs1.Id, false);
        insert new List<Applicant__c> {applicant2};
        
        // student folder
        Student_Folder__c studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', td.academicYearId, td.student2.Id, false);
        insert studentFolder2;

        // school pfs assignment
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(td.academicYearId, applicant2.Id, td.school1.Id, studentFolder2.Id, false);
        insert spfsa2;
        
        Student_Folder__c studentFolder = controller.studentFolder;    // this will load loadFolderQuickView()
        system.AssertEquals(td.student2.Id, controller.householdMemberList[0].member.Id);
        system.AssertEquals('Sibling Applicant', controller.householdMemberList[0].relationship);
        Test.stopTest();    
    }    

    @isTest
    private static void testSiblingNonApplicant() {
        TestData td = new TestData();

        Test.startTest();
        SchoolFolderQuickViewController controller = new SchoolFolderQuickViewController();
        controller.studentFolderId = td.studentFolder1.Id;
            
        // school
        Account school2 = TestUtils.createAccount('school2', RecordTypes.schoolAccountTypeId, null, false); 
        insert school2;       

        // applicant 2 - sibling non applicant. applying to different school 2
        Applicant__c applicant2 = TestUtils.createApplicant(td.student2.Id, td.pfs1.Id, false);
        insert new List<Applicant__c> {applicant2};
        
        // student folder
        Student_Folder__c studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', td.academicYearId, td.student2.Id, false);
        insert studentFolder2;

        // school pfs assignment
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(td.academicYearId, applicant2.Id, school2.Id, studentFolder2.Id, false);
        insert spfsa2;
        
        Student_Folder__c studentFolder = controller.studentFolder;    // this will load loadFolderQuickView()
        system.AssertEquals(td.student2.Id, controller.householdMemberList[0].member.Id);
        system.AssertEquals('Sibling', controller.householdMemberList[0].relationship);
        Test.stopTest();    
    }    

    @isTest
    private static void testPreviousAcademicYear() {
        TestData td = new TestData();

        Test.startTest();
        SchoolFolderQuickViewController controller = new SchoolFolderQuickViewController();
        controller.studentFolderId = td.studentFolder1.Id;

        Id prevAcademicYearId = GlobalVariables.getPreviousAcademicYear().Id;
            
        // psf
        PFS__c pfs2 = TestUtils.createPFS('PFS A', prevAcademicYearId, td.parentA.Id, false);
        insert new List<PFS__c> {pfs2};
        
        // applicant
        Applicant__c applicant2 = TestUtils.createApplicant(td.student1.Id, pfs2.Id, false);
        insert new List<Applicant__c> {applicant2};
        
        // student folder
        Student_Folder__c studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', prevAcademicYearId, td.student1.Id, false);
        insert studentFolder2;
        
        // school pfs assignment
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(prevAcademicYearId, applicant2.Id, td.school1.Id, studentFolder2.Id, false);
        insert spfsa2;
        
        Student_Folder__c studentFolder = controller.studentFolder;    // this will load loadFolderQuickView()
        system.AssertEquals(true, controller.pfsTotalPrevious1.existData );

        Test.stopTest();    
    }    
}