/**
* @SchoolPFSTopBarController.cls
* @date 5/21/2013
* @author Charles Howard, Exponent Partners, 2013
* @description SPEC-092, NAIS-20:  Controller for the SchoolPFSTopBar component displayed on the Folder Summary,
*               PFS Summary, and FCW Worksheet pages
*/

public with sharing class SchoolPFSTopBarController {

    /* Initialization */
    public SchoolPFSTopBarController(){
        // Define some default settings
        tabSelected = 'Folder Summary';
        existsPFS1 = false;
        existsPFS2 = false;
        isFullView = !GlobalVariables.isLimitedSchoolView();
        renderAll = true;
    }
    /* End Initialization */

    /* Properties */
    public String studentFolderId {get; set;}
    public String pfsRecordId {get; set;}
    public String tabSelected {get; set;}

    public String pfs1Num {get; set;}
    public String pfs2Num {get; set;}

    public School_PFS_Assignment__c spa1 {get; set;}
    public School_PFS_Assignment__c spa2 {get; set;}

    public Boolean isFullView{get; set;}
    public Boolean renderAll{get; set;}
    public Boolean existsPFS1 {get; set;}
    public Boolean existsPFS2 {get; set;}

    public String pfs1Withdrawn {get; set;} //NAIS-2010
    public String pfs2Withdrawn {get; set;} //NAIS-2010

    public Boolean historyReport {get; set;}

    public String parentA {get; set;}//SFP-125, [G.S]
    public String parent2A {get; set;}

    // [CH] NAIS-1866 Adding to support non need-based folders
    public String gradeApplying {
        get{
            if(folder.School_PFS_Assignments__r != null && folder.School_PFS_Assignments__r.size() > 0){
                return folder.School_PFS_Assignments__r[0].Grade_Applying__c;
            }
            else{
                return folder.Grade_Applying__c;
            }
        }
        set;
    }

    public Boolean pfs1IsSubmitted{
        get{ return GlobalVariables.pfsIsSubmitted(spa1); }
        set;
    }

    public Boolean pfs2IsSubmitted{
        get{ return GlobalVariables.pfsIsSubmitted(spa2); }
        set;
    }

    // NAIS-2020 [DP] 11.19.2014 changing "bothSubmitted" to "atLeastOneSubmitted" and commenting out "bothSubmitted" logic
    //public Boolean bothSubmitted{
    //    get{
    //        return (spa1 == null || GlobalVariables.pfsIsSubmitted(spa1)) && (spa2 == null || GlobalVariables.pfsIsSubmitted(spa2));
    //    }
    //    set;
    //}
    public Boolean atLeastOneSubmitted{
        get{
            return (spa1 != null && GlobalVariables.pfsIsSubmitted(spa1)) || (spa2 != null && GlobalVariables.pfsIsSubmitted(spa2)) || (folder.Folder_Source__c == 'School-Initiated');
        }
        set;
    }

    public Student_Folder__c folder {
        get{
            if(folder == null){
                loadAllData();
            }
            return folder;
        }
        set;
    }

    // This method is first triggered by a call to a hidden input field to load data after the constructor
    //  and after the component parameters are set, but before the rest of the page components need information
    public void loadAllData(){
        // [CH] NAIS-1866 Supporting loading the page for entering new Folder
        if(studentFolderId != null){

            folder = [select Id, Folder_Status__c, Grade_Applying__c, Folder_Source__c, Student__c, First_Name__c, Last_Name__c, BirthDate__c, Gender__c,
                        (select Id, Grade_Applying__c, Grade_Applying_Text__c, Applicant__r.PFS__c, Applicant__r.PFS__r.PFS_Number__c,
                                Applicant__r.Gender__c, Applicant__r.Name, Applicant__r.Birthdate_new__c, Applicant__r.PFS__r.Parent_A__c, Applicant__r.PFS__r.Parent_A_First_Name__c, Applicant__r.PFS__r.Parent_A_Last_Name__c,
                                Applicant__r.PFS__r.PFS_Status__c, Applicant__r.PFS__r.Payment_Status__c, Applicant__r.PFS__r.Visible_to_Schools_Until__c, Birthdate__c, Withdrawn__c
                            from School_PFS_Assignments__r
                            order by Applicant__r.PFS__r.CreatedDate asc, Applicant__r.PFS__r.PFS_Number__c asc)
                        from Student_Folder__c
                        where Id = :studentFolderId];

            if(folder.School_PFS_Assignments__r.size() > 0){
                //SFP-125, [G.S]
                if(String.isNotBlank(folder.School_PFS_Assignments__r[0].Applicant__r.PFS__r.Parent_A__c)){
                    parentA = ((folder.School_PFS_Assignments__r[0].Applicant__r.PFS__r.Parent_A_First_Name__c != null)?folder.School_PFS_Assignments__r[0].Applicant__r.PFS__r.Parent_A_First_Name__c:'')+' '+
                              ((folder.School_PFS_Assignments__r[0].Applicant__r.PFS__r.Parent_A_Last_Name__c != null)?folder.School_PFS_Assignments__r[0].Applicant__r.PFS__r.Parent_A_Last_Name__c:'');
                    parentA = parentA.trim().length()>0?parentA:'';
                }
                spa1 = folder.School_PFS_Assignments__r[0];
                pfs1Num = folder.School_PFS_Assignments__r[0].Applicant__r.PFS__r.PFS_Number__c;
                existsPFS1 = true;
                pfs1Withdrawn = folder.School_PFS_Assignments__r[0].Withdrawn__c; //NAIS-2010

                if(pfsRecordId != null && pfsRecordId == folder.School_PFS_Assignments__r[0].Applicant__r.PFS__c){
                    tabSelected = 'PFS1';

                }
            }
            if(folder.School_PFS_Assignments__r.size() > 1){
                //SFP-125, [G.S]
                if(String.isNotBlank(folder.School_PFS_Assignments__r[1].Applicant__r.PFS__r.Parent_A__c)){
                    parent2A = ((folder.School_PFS_Assignments__r[1].Applicant__r.PFS__r.Parent_A_First_Name__c != null)?folder.School_PFS_Assignments__r[1].Applicant__r.PFS__r.Parent_A_First_Name__c:'')+' '+
                              ((folder.School_PFS_Assignments__r[1].Applicant__r.PFS__r.Parent_A_Last_Name__c != null)?folder.School_PFS_Assignments__r[1].Applicant__r.PFS__r.Parent_A_Last_Name__c:'');
                    parent2A = parent2A.trim().length()>0?parent2A:'';
                }
                spa2 = folder.School_PFS_Assignments__r[1];
                pfs2Num = folder.School_PFS_Assignments__r[1].Applicant__r.PFS__r.PFS_Number__c;
                existsPFS2 = true;
                pfs2Withdrawn = folder.School_PFS_Assignments__r[1].Withdrawn__c; //NAIS-2010

                if(pfsRecordId != null && pfsRecordId == folder.School_PFS_Assignments__r[1].Applicant__r.PFS__c){
                    tabSelected = 'PFS2';
                }
            }

        }
        else{ // [CH] NAIS-1866
            folder = new Student_Folder__c();
        }

        //[JIMI] No logic for Financial Aid or future tabs. Implementing system for passing through URL
        if ( ApexPages.currentPage().getParameters().get('tabName') != null) {
            tabSelected = ApexPages.currentPage().getParameters().get('tabName');
        }

    }

    public Boolean isMIEActive {
        get{
            if(isMIEActive==null)
            {
                MonthlyIncomeAndExpensesService.isMIEActive(studentFolderId);
            }
            return isMIEActive;
        }
        set;
    }
    /* End Properties */
}