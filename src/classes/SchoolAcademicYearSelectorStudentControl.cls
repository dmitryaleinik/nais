public with sharing class SchoolAcademicYearSelectorStudentControl {
    /*Initialization*/

    public SchoolAcademicYearSelectorStudentControl() {
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=edge');

    }

    /*End Initialization*/

    /*Properties*/
    public SchoolAcademicYearSelectorStudInterface Consumer { get; set; }

    public Student_Folder__c folder {get; set;}
    public String NewAcademicYearId { get; set; }
    public Boolean saveRecord { get; set; }

    public Map<String, Student_Folder__c> acadYearNameToFolder;
    public Map<String, Academic_Year__c> acadYearNameToAcadYear;

    public pageReference newFolderSummary {get; set;}

    public Boolean InOverlapPeriodOrMultipleYears {
        get {
            if (acadYearNameToFolder == null){
                loadMaps();
            }
            return (acadYearNameToAcadYear.values().size() > 1);
        }
    }
    
    private Academic_Year__c CurrentAcademicYear{
        get {
            if(CurrentAcademicYear==null) {
                CurrentAcademicYear = GlobalVariables.getAcademicYearByName(folder.Academic_Year_Picklist__c);
            }
            
            return CurrentAcademicYear;
        }
        set;
    }
    public Id CurrentAcademicYearId {
        get {
            return CurrentAcademicYear != null ? CurrentAcademicYear.Id : null;
        }
        set;
    }
    
    public String CurrentAcademicYearName {
        get {
            return CurrentAcademicYear != null ? CurrentAcademicYear.Name : null;
        }
        set;
    }

    private List<SelectOption> academicYearOptions_x = null;
    private List<SelectOption> descAcademicYearOptions_x = null; //NAIS-1874
    public List<SelectOption> AcademicYearOptions {

        get {
            if (acadYearNameToFolder == null){
                loadMaps();
            }
            if(this.academicYearOptions_x == null) {
                this.academicYearOptions_x = new List<SelectOption>();

                for(Academic_Year__c year : acadYearNameToAcadYear.values()) {
                    this.academicYearOptions_x.add(new SelectOption(year.Id, year.Name));
                }
            }
            //NAIS-1874 Start
            this.academicYearOptions_x.sort();
            descAcademicYearOptions_x = new List<SelectOption>();
            for(Integer i = this.academicYearOptions_x.size()-1; i >= 0; i--){
                descAcademicYearOptions_x.add(this.academicYearOptions_x[i]);
            }
            return this.descAcademicYearOptions_x;
            //NAIS-1874 End
        }

    }

    /*End Properties*/

    /*Action Methods*/

    public PageReference SchoolAcademicYearSelectorStudent_OnChange() {

        newFolderSummary = Page.SchoolFolderSummary;
        newFolderSummary.getParameters().put('id', acadYearNameToFolder.get(GlobalVariables.getAcademicYear(NewAcademicYearId).Name).Id);
        newFolderSummary.setRedirect(true);

        PageReference pr;

        if(this.Consumer != null) {
            pr = this.Consumer.SchoolAcademicYearSelectorStudent_OnChange(newFolderSummary);
        }

        return pr;
    }

    /*End Action Methods*/

    /*Helper Methods*/

    public void loadMaps() {
        System.assert(folder != null);

        // make sure the folder attribute passed in has the right fields
        if (folder.Student__c == null || folder.Academic_Year_Picklist__c == null || folder.School_PFS_Assignments__r == null || folder.School_PFS_Assignments__r.size() == 0){
            folder = [Select Id, Academic_Year_Picklist__c, Student__c,
                        (Select Id, Applicant__r.Contact__c, Student_Folder__c, Student_Folder__r.Id,
                        Student_Folder__r.Academic_Year_Picklist__c,
                        School__c
                        from School_PFS_Assignments__r
                        order by Student_Folder__r.Academic_Year_Picklist__c)
                        from Student_Folder__c where Id = :folder.Id limit 1];
        }

        acadYearNameToFolder = new Map<String, Student_Folder__c>();
        acadYearNameToAcadYear = new Map<String, Academic_Year__c>();
        Id SchoolId = folder.School_PFS_Assignments__r.size() > 0 ? folder.School_PFS_Assignments__r[0].School__c : null;

        for (School_PFS_Assignment__c spfs : [Select Id, Applicant__r.Contact__c, Student_Folder__c, Student_Folder__r.Id, Student_Folder__r.Academic_Year_Picklist__c
                                                from School_PFS_Assignment__c
                                                WHERE Student_Folder__r.Student__c = :folder.Student__c
                                                //AND Student_Folder__c != :folder.Id
                                                AND School__c = :SchoolId
                                                order by Student_Folder__r.Academic_Year_Picklist__c desc
                                                ]){
            if(!acadYearNameToFolder.keySet().contains(spfs.Student_Folder__r.Academic_Year_Picklist__c)){
                Academic_Year__c academicYear = GlobalVariables.getAcademicYearByName(spfs.Student_Folder__r.Academic_Year_Picklist__c);
                if( academicYear != null ) {
                    acadYearNameToAcadYear.put(spfs.Student_Folder__r.Academic_Year_Picklist__c, academicYear);
                    acadYearNameToFolder.put(spfs.Student_Folder__r.Academic_Year_Picklist__c, spfs.Student_Folder__r);
                }
            }
        }
    }
}