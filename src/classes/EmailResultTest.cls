/**
 * EmailResultTest.cls
 *
 * @description: Test class for EmailResult using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest
private class EmailResultTest {
    
    /* test data setup */
    @testSetup static void setupTestData() {

        // any necessary data setup
    }
    
    /* negative test cases */

    // Construct an EmailResult object with nulls
    @isTest static void emailResultConstructor_withNulls_emptyEmail() {
        
        // Arrange
        // no work

        // Act
        EmailResult eR = new EmailResult( null, null);

        // Assert
        System.assertEquals( 0, eR.errorList.size());
    }


    /* postive test cases */

    // Construct an EmailResult object with valid values
    @isTest static void emailResultConstructor_withValues_validEmail() {

        // Arrange
        List<String> errList = new List<String>();
        errList.add( 'error');

        // Act
        EmailResult eR = new EmailResult( errList, false);

        // Assert
        System.assertEquals( 1, eR.errorList.size());
    }
    
}