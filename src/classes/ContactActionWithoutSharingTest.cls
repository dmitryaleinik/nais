@isTest
private without sharing class ContactActionWithoutSharingTest
{

    private without sharing class testData
    {
        private User schoolPortalUser;
        private contact student1;
        private Student_Folder__c studentFolder1;
        private list<School_PFS_Assignment__c> spfsaList;
        private list<PFS__c> pfsList;
        private Account school1;
        private Contact staffContact;
        private String  academicYearId;
        private String academicYearName;
        private list<Applicant__c> applicantList;
        
        private testData()
        {
            Profile_Type_Grouping__c ptg = new Profile_Type_Grouping__c();
            ptg.Name = 'School Portal Administrator';
            ptg.Is_School_Profile__c = true;
            ptg.Profile_Name__c = ProfileSettings.SchoolAdminProfileName;
            insert ptg; 
        
            // academic year
            TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
               academicYearId = GlobalVariables.getCurrentAcademicYear().Id;  
            academicYearName = GlobalVariables.getCurrentAcademicYear().Name;
            
             // school
            school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, null, false); 
            school1.SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest();
            school1.Max_Number_of_Users__c = 100;
            insert school1; 
            
            //Staff Contact
            list<Contact> newContactList = new list<Contact>();
            staffContact = TestUtils.createContact('Test School Staff', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
            staffContact.Email = 'testschoolstaff@school.test' + String.valueOf(Math.random());
            staffContact.MailingCountry = 'United States';
            newContactList.add(staffContact);
            
            
            // student
            student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
            student1.FirstName = 'John';
            student1.LastName = 'Doe';
            student1.RecordTypeId=RecordTypes.studentContactTypeId;
            student1.Ethnicity__c = 'Latino/Hispanic American';
            newContactList.add(student1);
            insert newContactList;
        }
        
        private void createFolderData()
        {
            // student folder
            studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
            studentFolder1.School__c = school1.Id;
            studentFolder1.Ethnicity__c = 'Caucasian';
            insert studentFolder1;
    
            // parent A
            list <Contact> parentAList = new list<Contact>();
            for (Integer i=0; i<5; i++) {
                Contact parentA = TestUtils.createContact('Parent A' + i, null, RecordTypes.parentContactTypeId, false);
                parentAList.add(parentA);
            }
            insert parentAList;
            
            // PFS
            pfsList = new List <PFS__c>();
            for (Integer i=0; i<5; i++) {
                PFS__c pfs = TestUtils.createPFS('PFS A' + i, academicYearId, parentAList[i].Id, false);
                pfsList.add(pfs);
            }
            insert pfsList;
            
            // Applicant
            applicantList = new List <Applicant__c>();
            for (Integer i=0; i<5; i++) {
                Applicant__c applicant = TestUtils.createApplicant(student1.Id, pfsList[i].Id, false);
                applicantList.add(applicant);
            }
            insert applicantList;
            
            // School PFS Assignment
            spfsaList = new List <School_PFS_Assignment__c>();
            for (Integer i=0; i<5; i++) {
                School_PFS_Assignment__c spfsa = TestUtils.createSchoolPFSAssignment(academicYearId, applicantList[i].Id, school1.Id, studentFolder1.Id, false);
                spfsa.School__c = school1.Id;
                spfsaList.add(spfsa);
            }
            insert spfsaList;
        }
    }

    @isTest
    private static void testChangeStudentName()
    {
        string expectedNewName;
        testData dummy = new testData();
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        //Insert the Portal User as the current system administrator to avoid Mixed DML Errors
        system.runAs(thisUser) {
            dummy.schoolPortalUser = TestUtils.createPortalUser('Richie', 'Richietest@unittest.com', 'alias', dummy.staffContact.Id, GlobalVariables.schoolPortalAdminProfileId, true, true);
        }
        
        System.runAs(dummy.schoolPortalUser) {
            dummy.createFolderData();
            dummy.student1.FirstName = 'Testing';
            expectedNewName=dummy.student1.FirstName+' '+dummy.student1.LastName+' - '+dummy.school1.Name;
            Test.startTest();
            Dml.WithoutSharing.updateObjects(dummy.student1);
            Test.stopTest();
        }
        
        string spfsName = [Select Name from School_PFS_Assignment__c where Id=:dummy.spfsaList[0].Id limit 1].Name;
        Student_Folder__c tmpFolder = [Select Name, Ethnicity__c from Student_Folder__c where Id=:dummy.studentFolder1.Id limit 1];
        string sfolderName = tmpFolder.Name;
        
        system.assertEquals('Testing', dummy.student1.FirstName);
        system.assertEquals(expectedNewName+' Folder', sfolderName);
        system.assertEquals(expectedNewName, spfsName.left(expectedNewName.length()));
    }//End:testChangeStudentName

    @isTest
    private static void testChangeStudentEthnicity()
    {
        testData dummy = new testData();
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        //Insert the Portal User as the current system administrator to avoid Mixed DML Errors
        system.runAs(thisUser) {
            dummy.schoolPortalUser = TestUtils.createPortalUser('Richie', 'Richietest@unittest.com', 'alias', dummy.staffContact.Id, GlobalVariables.schoolPortalAdminProfileId, true, true);
        }
        
        System.runAs(dummy.schoolPortalUser) {
            dummy.createFolderData();     
            dummy.student1.Ethnicity__c = 'International';
            Test.startTest();
            Dml.WithoutSharing.updateObjects(dummy.student1);
            Test.stopTest();
        }
        
        Student_Folder__c tmpFolder = [Select Name, Ethnicity__c from Student_Folder__c where Id=:dummy.studentFolder1.Id limit 1];
        
        system.assertEquals('International', tmpFolder.Ethnicity__c);
    }//End:testChangeStudentEthnicity

    @isTest
    private static void testChangeFolderEthnicity()
    {
        testData dummy = new testData();
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        //Insert the Portal User as the current system administrator to avoid Mixed DML Errors
        system.runAs(thisUser) {
            dummy.schoolPortalUser = TestUtils.createPortalUser('Richie', 'Richietest@unittest.com', 'alias', dummy.staffContact.Id, GlobalVariables.schoolPortalAdminProfileId, true, true);
        }
        
        System.runAs(dummy.schoolPortalUser) {
            dummy.createFolderData();     
            dummy.studentFolder1.Ethnicity__c = 'Pacific Islander';
            Test.startTest();
            update dummy.studentFolder1;
            Test.stopTest();
        }
        
        Contact tmpContact= [Select Id, Ethnicity__c from Contact where Id=:dummy.student1.Id limit 1];
        
        system.assertEquals('Pacific Islander', tmpContact.Ethnicity__c);
    }//End:testChangeStudentEthnicity
}