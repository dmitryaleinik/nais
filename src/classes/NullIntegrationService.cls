/**
 * NullIntegrationService.cls
 *
 * @description: follows Null Object Pattern for IIntegrationProvider
 *
 * @author: Mike Havrila @ Presence PG
 */

public class NullIntegrationService implements IIntegrationProvider {
    public NullIntegrationService() { /* Do Nothing */ }

    public IIntegrationProvider init( String type) { return this; }

    public IIntegrationProvider preProcess() { return this; }

    public IIntegrationProvider process() { return this; }

    public IIntegrationProvider postProcess() { return this; }
}