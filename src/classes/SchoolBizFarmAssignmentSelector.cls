/**
 * @description Query selector for School_Biz_Farm_Assignment__c records.
 */
public class SchoolBizFarmAssignmentSelector extends fflib_SObjectSelector
{

    /**
     * @description Selects All School Business Farm Assignment records with only Id field.
     * @return A list of School Business Farm Assignment records.
     */
    public List<School_Biz_Farm_Assignment__c> selectAll()
    {
        return Instance.selectAllWithCustomFieldList(new List<String>());
    }

    /**
     * @description Selects All School Business Farm Assignment records with custom fields mentioned in the passed parameter.
     * @param fieldsToSelect The School Business Farm Assignment fields to query.
     * @return A list of School Business Farm Assignment records.
     */
    public List<School_Biz_Farm_Assignment__c> selectAllWithCustomFieldList(List<String> fieldsToSelect)
    {
        assertIsAccessible();
        
        if (fieldsToSelect == null || fieldsToSelect.isEmpty())
        {
            fieldsToSelect = new List<String>{'Id'};
        }

        String queryString = newQueryFactory(false)
                .selectFields(fieldsToSelect)
                .toSOQL();

        return Database.query(queryString);
    }

    private Schema.SObjectType getSObjectType()
    {
        return School_Biz_Farm_Assignment__c.getSObjectType();
    }

    private List<Schema.SObjectField> getSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
                School_Biz_Farm_Assignment__c.Name,
                School_Biz_Farm_Assignment__c.Id,
                School_Biz_Farm_Assignment__c.Business_Farm__c,
                School_Biz_Farm_Assignment__c.School_PFS_Assignment__c
        };
    }

    /**
     * @description Creates a new instance of the SchoolBizFarmAssignmentSelector.
     * @return An instance of SchoolBizFarmAssignmentSelector.
     */
    public static SchoolBizFarmAssignmentSelector newInstance()
    {
        return new SchoolBizFarmAssignmentSelector();
    }
    
    /**
     * @description Singleton property ensuring external sources do not
     *              instantiate this directly.
     */
    public static SchoolBizFarmAssignmentSelector Instance
    {
        get
        {
            if (Instance == null)
            {
                Instance = new SchoolBizFarmAssignmentSelector();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to enforce singleton access
     *              via the Instance property.
     */
    private SchoolBizFarmAssignmentSelector() {}
}