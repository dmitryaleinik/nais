/*
 * @description We take each PAID PFS [meaning we exclude waivers], and divide
 *              it by the number of unique school codes on it. The resulting weight
 *              is then allocated to each of those schools. The school's weight is
 *              the sum of the allocations.
 **/
public class SchoolSetWeightedPFS {
    // static objects to keep track of trigger recursion in the PFS After and Applicant After triggers
    public static TriggerControl schoolPFSActionTriggerControl = new TriggerControl();

    // input
    public Set <String> pfsIdSet = new Set <String>();
    public Set <Id> spfsaIdSet = new Set <Id>();
    public Set <String> applicantIdSet = new Set <String>();

    public Map <Id, School_PFS_Assignment__c> oldSpfsaMap = new Map <Id, School_PFS_Assignment__c>();
    public Map <Id, School_PFS_Assignment__c> newSpfsaMap = new Map <Id, School_PFS_Assignment__c>();
    public Map <Id, School_PFS_Assignment__c> spfsaMapForPFSCountRecalc = new Map <Id, School_PFS_Assignment__c>();

    // to prevent trigger recursive calls
    public static Boolean isExecuting = false;
    public static Boolean isExecutingFromPFS = false;
    public static Boolean preventCalculation = false; // [DP] 10.7.14 set by the sharing job to prevent this setWeightedPFS from being fired from the SchoolPFSAssignmentAfter trigger

    // constructor
    public SchoolSetWeightedPFS() { }

    // NAIS-2068 [DP] 12.18.2014 REFACTOR to call this from PFS Update
    public void setWeightedPFSFromPFSIds(Set<Id> pfsIds) {
        isExecutingFromPFS = true;
        spfsaMapForPFSCountRecalc = new Map <Id, School_PFS_Assignment__c>(
                [SELECT
                        Id,
                        Applicant__c,
                        School__c,
                        Academic_Year_Picklist__c
                   FROM School_PFS_Assignment__c
                  WHERE Applicant__r.PFS__c in :pfsIds]);

        if (!spfsaMapForPFSCountRecalc.isEmpty()) {
            setWeightedPFS();
        }
    }

    /**
     * @description Creates weighted pfss. This calculation can be triggered manually by setting the SYSTEM_WPFS_Recalc__c
     *              flag on the PFS. If you mark a school's System_Exclude__c flag as true the PFS's have to be
     *              recalculated by setting the SYSTEM_WPFS_Recalc__c flag.
     */
    public void setWeightedPFS() {
        Set <String> schIdSet = new Set <String>();
        Set <String> acadYearNameSet = new Set <String>();
        Map <String, Set <String>> pfsId_schoolIdSet_map = new Map <String, Set <String>>();
        Map <String, List <Annual_Setting__c>> schId_annSettList_map = new Map <String, List <Annual_Setting__c>>();
        Map <String, Annual_Setting__c> id_annSett_map = new Map <String, Annual_Setting__c>();
        Map <String, Weighted_PFS__c> key_weightedPFS_map = new Map <String, Weighted_PFS__c>();
        Set <String> wpfsKeySet = new Set <String>();
        List <Weighted_PFS__c> wpfsUpdateList = new List <Weighted_PFS__c>();
        List <Weighted_PFS__c> wpfsInsertList = new List <Weighted_PFS__c>();
        List <Weighted_PFS__c> wpfsDeleteList = new List <Weighted_PFS__c>();
        // [CH] NAIS-1100 List of Annual Setting records to have their Total PFS Count set
        List<Annual_Setting__c> annualSettingsToUpsert = new List<Annual_Setting__c>{}; // NAIS-2403 [DP] 05.01.2015 rename to upsert
        Set <String> wpfsIdSetUpdate = new Set <String>();    // to avoid including dupes in list
        Set <String> wpfsIdSetInsert = new Set <String>();    // to avoid including dupes in list
        Set <String> wpfsIdSetDelete = new Set <String>();    // to avoid including dupes in list

        // NAIS-2068 [DP] 12.18.2014 Refactor to prevent this from running on every single update to SPA
        // Calc now occurs on SPA Insert, SPA Delete
        // AND PFS Update with change to Payment_Status__c, PFS_Status, and/or Fee_Waived__C
        for (School_PFS_Assignment__c spa : newSpfsaMap.values()) {
            School_PFS_Assignment__c oldSpa = oldSpfsaMap.get(spa.Id);

            // insert criteria
            if (oldSpa == null && spa.PFS_Status__c == 'Application Submitted') {
                spfsaMapForPFSCountRecalc.put(spa.Id, spa);
            }
        }

        for (School_PFS_Assignment__c spa : oldSpfsaMap.values()) {
            School_PFS_Assignment__c newSpa = newSpfsaMap.get(spa.Id);

            // delete criteria
            if (newSPA == null) {
                spfsaMapForPFSCountRecalc.put(spa.Id, spa);
            }
        }

        // Return early if this is not for SPA wPFS mapping
        if (spfsaMapForPFSCountRecalc == null || spfsaMapForPFSCountRecalc.isEmpty()) {
            return;
        }

        // Safeguard against trigger recursion
        isExecuting = true;

        // Extract the Applicant Ids, School Ids, and Academic Years from the SPAs
        spfsaIdSet.addAll(spfsaMapForPFSCountRecalc.keySet());
        for (School_PFS_Assignment__c spfsa : spfsaMapForPFSCountRecalc.values()) {
            applicantIdSet.add(spfsa.Applicant__c);
            schIdSet.add(spfsa.School__c);
            acadYearNameSet.add(spfsa.Academic_Year_Picklist__c);
        }

        // Extract out the PFS record Ids from the Applicants
        if (applicantIdSet != null && applicantIdSet.size() > 0) {
            // removed PFS__c != null portion of query since this is large data and added if logic [DP] 3.11.13
            for (Applicant__c app : [SELECT PFS__c FROM Applicant__c WHERE Id IN :applicantIdSet]) {
                if (app.PFS__c != null) {
                    pfsIdSet.add(app.PFS__c);
                }
            }
        }

        // [CH] NAIS-1100 Need to query all of the PFS records that share an Account and Academic Year
        //      with the SPA records in the current trigger lists
        //      [DP] THIS QUERY GETS ALL SPAs in these schools so that it can calculate the Annual_Settings__c.Total_PFS_Count__c at line 189
        Map<String, Integer> pfsCountBySchoolAndYearMap = new Map<String, Integer>{};
        for (AggregateResult ar : [SELECT
                                          Count(Applicant__r.PFS__c) pfsCount,
                                          School__c,
                                          Academic_Year_Picklist__c
                                     FROM School_PFS_Assignment__c
                                    WHERE School__c IN :schIdSet
                                      AND Academic_Year_Picklist__c IN :acadYearNameSet
                                      AND Applicant__r.PFS__r.PFS_Status__c = 'Application Submitted'
                                 GROUP BY School__c, Academic_Year_Picklist__c]) {

            // Allocating to variables just for clarity, these are not referenced anywhere else
            String schoolId = String.valueOf(ar.get('School__c'));
            String acadYearName = String.valueOf(ar.get('Academic_Year_Picklist__c'));
            Integer pfsCount = Integer.valueOf(ar.get('pfsCount'));
            pfsCountBySchoolAndYearMap.put(schoolId + ':' + acadYearName, pfsCount);
        }

        if (pfsIdSet != null && pfsIdSet.size() > 0) {

            // Query for existing wPFS records associated with
            // the given academic years and the PFS records.
            List <Weighted_PFS__c> wpfsList = [SELECT
                                                      Id,
                                                      Name,
                                                      Annual_Setting__c,
                                                      Annual_Setting__r.Academic_Year__r.Name,
                                                      PFS__c,
                                                      School_Count__c,
                                                      Weighted_School_Count__c,
                                                      PFS__r.PFS_Number__c,
                                                      Annual_Setting__r.School__c
                                                 FROM Weighted_PFS__c
                                                WHERE PFS__c IN :pfsIdSet
                                                  AND Annual_Setting__r.Academic_Year__r.Name IN :acadYearNameSet];

            for (Weighted_PFS__c wpfs : wpfsList) {
                String key = wpfs.Annual_Setting__c + '_' + wpfs.PFS__c;
                key_weightedPFS_map.put(key, wpfs);
            }

            // load pfs ->> schools map
            // By excluding spas for schools with the system_exclude flag set here but not in the wpfsList
            // query we correctly update school totals and delete old weighted pfss' for newly excluded schools.
            List <School_PFS_Assignment__c> spfsaList = [SELECT
                                                                Id,
                                                                Name,
                                                                Academic_Year_Picklist__c,
                                                                PFS_Status__c,
                                                                Applicant__r.PFS__c,
                                                                Applicant__r.PFS__r.Academic_Year_Picklist__c,
                                                                Applicant__r.PFS__r.Fee_Waived__c,
                                                                Applicant__r.PFS__r.PFS_Status__c,
                                                                Applicant__r.PFS__r.Payment_Status__c,
                                                                School__c,
                                                                Withdrawn__c
                                                           FROM School_PFS_Assignment__c
                                                          WHERE Applicant__r.PFS__c IN :pfsIdSet
                                                            AND School__r.System_Exclude__c = FALSE];

            for (School_PFS_Assignment__c spfsa : spfsaList) {
                // [CH] NAIS-1100 Refactored
                Set <String> schIdSetLocal = pfsId_schoolIdSet_map.get(spfsa.Applicant__r.PFS__c);
                if (schIdSetLocal == null) {
                    schIdSetLocal = new Set <String>();
                }

                // Add to the list of school ids referenced by all SPA records
                schIdSet.add(spfsa.School__c);

                // Add to the list of school ids referenced by the
                //  PFS that this SPA record belongs to
                schIdSetLocal.add(spfsa.School__c);
                pfsId_schoolIdSet_map.put(spfsa.Applicant__r.PFS__c, schIdSetLocal);
            }

            // load school ->> annual settings map
            if (schIdSet.size() > 0) {

                // [CH] NAIS-1100 Refactored
                // Query all of the schools referenced by any SPA in the current trigger,
                //    along with their Annual Settings records
                // [DP] 12.12.2014 NAIS-2075 - refactored to only get annual settings for the relevant academic year.
                // UPDATE [DP] 05.08.2015 now getting all annual settings in case we need to clone an old one

                // get all non-date fields for clone
                List<String> fieldNames = new List<String>();
                Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Annual_Setting__c.fields.getMap();
                for (String fieldName : fieldMap.keySet()) {
                    if (fieldMap.get(fieldName).getDescribe().getType() != Schema.Displaytype.Date &&
                        fieldMap.get(fieldName).getDescribe().getType() != Schema.Displaytype.DateTime) {

                        fieldNames.add(fieldName);
                    }
                }

                String queryString = 'SELECT Id, Name, (SELECT ' + String.Join(fieldNames, ',') +
                        ', Academic_Year__r.Name from Annual_Settings__r ORDER BY Academic_Year__r.Name asc) ' +
                        'FROM Account where Id in :schIdSet';

                for (Account sch : Database.query(queryString)) {
                    // NAIS-2403 [DP] 05.01.2015 Begin refactor to create new annual settings when they don't already exist

                    // map for this school -- schoolandyear key is SchoolId:AcadYearId, value is Annual Setting
                    Map<String, Annual_Setting__c> acadYearNameToThisSchoolsAnnualSetting = new Map<String, Annual_Setting__c>();
                    // pouplate the map with existing annual settings
                    Annual_Setting__c latestAnnSet;
                    for (Annual_Setting__c annSett : sch.Annual_Settings__r) {
                        // only put in relevant years [DP] 05.08.2015. Otherwise the PFS Counts get overwritten incorrectly
                        if (acadYearNameSet.contains(annSett.Academic_Year__r.Name)) {
                            acadYearNameToThisSchoolsAnnualSetting.put(annSett.Academic_Year__r.Name, annSett);
                        }
                        latestAnnSet = annSett; // this should be the "latest" (or most recent) ann set
                    }

                    // loop through map of ALL schoolandyear keys to pfs counts
                    for (String schoolAndYearKey : pfsCountBySchoolAndYearMap.keySet()) {
                        String schoolId = schoolAndYearKey.split(':')[0];
                        String acadYearName = schoolAndYearKey.split(':')[1];

                        // if the key has THIS schools id, and there is not an existing Annual Setting, create one and add it to our map
                        if (schoolId == sch.Id && acadYearNameToThisSchoolsAnnualSetting.get(acadYearName) == null) {
                            // now we are using the existing GV logic to clone an exsting annsett, clear out some dates fields, and set some defaults
                            Annual_Setting__c newAnnSet = GlobalVariables.newAnnualSettingFromOld(latestAnnSet, schoolId, GlobalVariables.getAcademicYearByName(acadYearName).Id);
                            acadYearNameToThisSchoolsAnnualSetting.put(acadYearName, newAnnSet);
                        }
                    }

                    // add all annual settings (both the existing ones and the new ones yet-to-be-inserted)
                    schId_annSettList_map.put(sch.Id, acadYearNameToThisSchoolsAnnualSetting.values());

                    // loop through all annual settings (both the existing ones and the new ones yet-to-be-inserted)
                    for (Annual_Setting__c annSett : schId_annSettList_map.get(sch.Id)) {
                        // [CH] NAIS-1987 Refactor to use the count retrieved in the aggregate query above
                        String key = sch.Id + ':';
                        key += annSett.Academic_Year__r.Name != null ? annSett.Academic_Year__r.Name : GlobalVariables.getAcademicYear(annSett.Academic_Year__c).Name;
                        Integer relatedPFSCount = pfsCountBySchoolAndYearMap.get(key);
                        if (relatedPFSCount != null) {
                            annSett.Total_PFS_Count__c = relatedPFSCount;
                        } else {
                            annSett.Total_PFS_Count__c = 0;
                        }

                        // Add to the list to UPSERT
                        annualSettingsToUpsert.add(annSett);
                    }
                }
            }

            // Upsert Annual Settings records to update the Total PFS Count,
            // creating new if necessary
            if (annualSettingsToUpsert.size() > 0) {
                upsert annualSettingsToUpsert; // NAIS-2403 [DP] 05.01.2015
            }


            // for each spfsa update, create or delete weighted pfs
            for (School_PFS_Assignment__c spfsa : spfsaList) {
                // Get the list of Annual Settings for the school referenced in the current SPA record
                List <Annual_Setting__c> annSettList = schId_annSettList_map.get(spfsa.School__c);

                // If there's no annual setting to work against, skip this SPA
                if (annSettList == null) {
                    continue;
                }

                // For each Annual Setting attached to the school
                for (Annual_Setting__c annSett : annSettList) {
                    String acadYearNameLocal = annSett.Academic_Year__r.Name != null ?
                            annSett.Academic_Year__r.Name :
                            GlobalVariables.getAcademicYear(annSett.Academic_Year__c).Name;

                    // If:
                    //    - The academic year picklist doesn't match OR
                    //    - No PFS mapped OR
                    //    - The Pfs is not submitted and paid in full THEN
                    // Skip this SPA
                    if ((spfsa.Academic_Year_Picklist__c != acadYearNameLocal) ||
                        (!pfsId_schoolIdSet_map.containsKey(spfsa.Applicant__r.PFS__c)) ||
                        (spfsa.Applicant__r.PFS__r.PFS_Status__c != 'Application Submitted' ||
                         spfsa.Applicant__r.PFS__r.Payment_Status__c != 'Paid in Full')) {
                        continue;
                    }

                    String key = annSett.Id + '_' + spfsa.Applicant__r.PFS__c;
                    Set <String> schIdSetLocal = pfsId_schoolIdSet_map.get(spfsa.Applicant__r.PFS__c);
                    if (key_weightedPFS_map.containsKey(key)) {
                        // update
                        Weighted_PFS__c wpfs = key_weightedPFS_map.get(key);
                        wpfsKeySet.add(key);
                        if (wpfs.School_Count__c != schIdSetLocal.size()) {
                            wpfs.School_Count__c = schIdSetLocal.size();
                            // avoid include of dupes in list
                            if (wpfsIdSetUpdate.add(wpfs.Id)) {
                                wpfsUpdateList.add(wpfs);
                            }
                        }
                    } else {
                        // insert
                        if (spfsa.Applicant__r.PFS__c != null && schIdSetLocal.size() > 0) {
                            Weighted_PFS__c wpfs = new Weighted_PFS__c(
                                    Annual_Setting__c = annSett.Id,
                                    PFS__c = spfsa.Applicant__r.PFS__c,
                                    School_Count__c = schIdSetLocal.size());

                            // avoid include of dupes in list
                            if (wpfsIdSetInsert.add(key)) {
                                wpfsInsertList.add(wpfs);
                                wpfsKeySet.add(key);
                            }
                        }
                    }
                }
            }
        }

        // deletes
        // Primarily used when a weighted pfs is being recalculated and a school.SYSTEM_Exclude__c flag has recently
        // been set to true.
        Weighted_PFS__c dWPFS;
        String wKey;
        for (String key : key_weightedPFS_map.keySet()) {
            dWPFS = key_weightedPFS_map.get(key);
            wKey = dWPFS.PFS__r.PFS_Number__c +'-'+dWPFS.Annual_Setting__r.School__c;
            if (wpfsKeySet.contains(key) == false) {
                wpfsDeleteList.add(dWPFS);
            }
        }

        if (wpfsUpdateList.size() > 0) {
            update wpfsUpdateList;
        }

        if (wpfsInsertList.size() > 0) {
            insert wpfsInsertList;
        }

        if (wpfsDeleteList.size() > 0) {
            delete wpfsDeleteList;
        }
    }
}