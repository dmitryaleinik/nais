@IsTest
private class GlobalMessagesTest
{

    @isTest
    private static void testValidateNewRecords()
    {
        // test for the global message with valid input data
        String existingPageName = 'FamilyDashboard';
        // test of creating more than 1 messages for the same page with different case
        String existingPageNameInChangedCase1 = 'familydashboard';
        // test of creating the page with incorrect case
        String existingPageNameInChangedCase2 = 'familymessages';
        // test for the global message with invalid page name
        String incorrectPageName = 'incorrectPageName';
        // test for the SelectSchools screen in original case
        String existingScreen = 'SelectSchools';
        // test for the SelectSchools screen in changed case
        String existingScreenInChangedCase = 'selectschools';

        String applicantInformationScreenName = 'ApplicantInformation';
        String selectSchoolsScreenName = 'SelectSchools';
        FamilyAppSettings__c fasApplicantInformation = FamilyAppSettingsTestData.Instance.forName(applicantInformationScreenName).create();
        FamilyAppSettings__c fasSelectSchools = FamilyAppSettingsTestData.Instance.forName(selectSchoolsScreenName).create();
        insert new List<FamilyAppSettings__c>{fasApplicantInformation, fasSelectSchools};

        Global_Message__c gmExistingPage = GlobalMessageTestData.Instance
            .forPageName(existingPageName).create();
        Global_Message__c gmExistingPageInChangedCase1 = GlobalMessageTestData.Instance
            .forPageName(existingPageNameInChangedCase1).create();
        Global_Message__c gmExistingPageInChangedCase2 = GlobalMessageTestData.Instance
            .forPageName(existingPageNameInChangedCase2).create();
        Global_Message__c gmExistingScreen = GlobalMessageTestData.Instance
            .forPageName(existingScreen).create();
        Global_Message__c gmExistingScreenInChangedCase = GlobalMessageTestData.Instance
            .forPageName(existingScreenInChangedCase).create();
        Global_Message__c gmEmptyPageField = GlobalMessageTestData.Instance.create();

        Global_Message__c gmIncorrectPageName = GlobalMessageTestData.Instance
            .forPageName(incorrectPageName).create();
        Global_Message__c gmEmptyPageNameAndPortalFields = GlobalMessageTestData.Instance
            .forPortal(null).create();

        Test.startTest();
            insert new List<Global_Message__c>{gmExistingPage, gmExistingPageInChangedCase1, gmExistingPageInChangedCase2, gmEmptyPageField,
                gmExistingScreen, gmExistingScreenInChangedCase};
            System.assertNotEquals(null, gmExistingPage.Id, 'Successful insert expect');
            System.assertNotEquals(null, gmExistingPageInChangedCase1.Id, 'Successful insert expect');
            System.assertNotEquals(null, gmExistingPageInChangedCase2.Id, 'Successful insert expect');
            System.assertNotEquals(null, gmEmptyPageField.Id, 'Successful insert expect');
            System.assertNotEquals(null, gmExistingScreen.Id, 'Successful insert expect');
            System.assertNotEquals(null, gmExistingScreenInChangedCase.Id, 'Successful insert expect');

            try
            {
                insert gmIncorrectPageName;
            }
            catch (Exception ex)
            {
                System.assert(ex.getMessage().containsIgnoreCase(
                    String.format(Label.GM_Invalid_Page_Name, new List<String>{incorrectPageName})));
            }

            String validationRuleError = 'At least one of the Portal or Page Name fields should be populated';
            try
            {
                insert gmEmptyPageNameAndPortalFields;
            }
            catch (Exception ex)
            {
                System.assert(ex.getMessage().containsIgnoreCase(validationRuleError));
            }
        Test.stopTest();
    }
}