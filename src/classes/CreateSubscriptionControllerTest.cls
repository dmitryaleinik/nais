@isTest
private class CreateSubscriptionControllerTest {
    private static final Decimal PRODUCT_AMOUNT = 200.00;
    private static final Decimal EARLY_BIRD_PRODUCT_AMOUNT = 100.00;
    private static final Decimal SCHOOL_DISCOUNT_AMOUNT = 50.00;
    private static final String PRODUCT_CODE = 'Full Product Code';
    private static final String SUBSCRIPTION_TYPE = 'Full';
    private static final String EARLY_BIRD = 'Early Bird';
    private static final String ACADEMIC_YEAR = (System.today().year()+1) + '-' + (System.today().year() + 2);
    private static final String OPP_STAGENAME_PROSPECTING = 'Prospecting';    

    @testSetup
    private static void setup() {
        TestUtils.createAcademicYear(ACADEMIC_YEAR, true);
        insert new List<Transaction_Annual_Settings__c> {
                new Transaction_Annual_Settings__c(
                        Product_Amount__c = PRODUCT_AMOUNT,
                        Product_Code__c = PRODUCT_CODE,
                        Year__c = ACADEMIC_YEAR.left(4),
                        Name = 'School Sub',
                        Subscription_Type__c = SUBSCRIPTION_TYPE),
                new Transaction_Annual_Settings__c(
                        Product_Amount__c = EARLY_BIRD_PRODUCT_AMOUNT,
                        Product_Code__c = 'Another ' + PRODUCT_CODE,
                        Year__c = ACADEMIC_YEAR.left(4),
                        Name = EARLY_BIRD,
                        Discount_Type__c = EARLY_BIRD,
                        Subscription_Type__c = SUBSCRIPTION_TYPE)
        };

        Account schoolAccount = new Account(
                Name = 'Great School',
                SSS_School_Code__c = '123456');

        insert schoolAccount;

        Opportunity opportunityRecord = new Opportunity(
                AccountId = schoolAccount.Id,
                Name = 'Placeholder Name',
                New_Renewal__c = 'New',
                StageName = OPP_STAGENAME_PROSPECTING,
                CloseDate = System.today(),
                Subscription_Period_Start_Year__c = ACADEMIC_YEAR.left(4),
                Academic_Year_Picklist__c = ACADEMIC_YEAR,
                Subscription_Type__c = SUBSCRIPTION_TYPE,
                Amount = PRODUCT_AMOUNT);

        insert opportunityRecord;
    }

    private static ApexPages.StandardController createStandardController() {
        // Query for the available Opportunity record.
        Opportunity opportunityRecord = [SELECT Id, AccountId FROM Opportunity LIMIT 1];

        // Set the current page to the Create Subscription page
        Test.setCurrentPage(Page.CreateSubscription);
        System.currentPageReference().getParameters().put(CreateSubscriptionController.ID_PARAM, opportunityRecord.Id);

        return new ApexPages.StandardController(opportunityRecord);
    }

    private static void deleteAcademicYear() {
        delete [SELECT Id FROM Academic_Year__c];
    }

    private static void setEarlyBird() {
        // Renewal Settings are set for the year before the beginning of the academic year picklist value.
        Integer renewalSettingYear = Integer.valueOf(ACADEMIC_YEAR.left(4)) - 1;
        insert new Renewal_Annual_Settings__c(
                Name = String.valueOf(renewalSettingYear),
                Early_Bird_End_Date__c = System.today().addMonths(1),
                Renewal_Season_Start_Date__c = System.today().addMonths(-2),
                Renewal_Season_End_Date__c = System.today().addMonths(2));

        insert new Transaction_Settings__c(
                Name = 'Early Bird Subscription Discount',
                Account_Code__c = '5100',
                Transaction_Code__c = '3230',
                Account_Label__c = 'Subscription Revenue');
    }

    private static Subscription__c insertSubscriptionForOpportunity(Opportunity opportunity) {
        Subscription__c subscription = new Subscription__c(
                Account__c = opportunity.AccountId,
                Start_Date__c = System.today(),
                End_Date__c = System.today().addYears(Integer.valueOf(ACADEMIC_YEAR.left(4)) - system.today().year()));

        insert subscription;

        // Because the Name field is populated on insert we need to requery for it.
        return [SELECT Name, Start_Date__c FROM Subscription__c WHERE Id = :subscription.Id];
    }

    private static void assertSaleTransactionLineItem(Transaction_Line_Item__c transactionLineItem) {
        System.assertEquals(RecordTypes.saleTransactionTypeId, transactionLineItem.RecordTypeId,
                'Expected the Record Type Id to be ' + RecordTypes.saleTransactionTypeId + '.');
        System.assertEquals('School Subscription', transactionLineItem.Transaction_Type__c,
                'Expected the Transaction Type to be School Subscription.');
        System.assertEquals(PRODUCT_AMOUNT, Decimal.valueOf(transactionLineItem.Product_Amount__c),
                'Expected the product amount to be ' + PRODUCT_AMOUNT + '.');
        System.assertEquals(System.today(), transactionLineItem.Transaction_Date__c,
                'Expected the transaction date to be today.');
    }

    private static void assertEarlyBirdTransactionLineItem(Transaction_Line_Item__c transactionLineItem) {
        System.assertEquals(RecordTypes.schoolDiscountTransactionTypeId, transactionLineItem.RecordTypeId,
                'Expected the Record Type Id to be ' + RecordTypes.schoolDiscountTransactionTypeId + '.');
        System.assertEquals('Early Bird Subscription Discount', transactionLineItem.Transaction_Type__c,
                'Expected the Transaction Type to be Early Bird Subscription Discount.');
        System.assertEquals(EARLY_BIRD_PRODUCT_AMOUNT, Decimal.valueOf(transactionLineItem.Product_Amount__c),
                'Expected the product amount to be ' + EARLY_BIRD_PRODUCT_AMOUNT + '.');
        System.assertEquals(System.today(), transactionLineItem.Transaction_Date__c,
                'Expected the transaction date to be today.');
    }

    private static void assertDiscountTransactionLineItem(Transaction_Line_Item__c transactionLineItem,
            Decimal expectedAmount) {

        System.assertEquals(RecordTypes.schoolDiscountTransactionTypeId, transactionLineItem.RecordTypeId,
                'Expected the Record Type Id to be ' + RecordTypes.schoolDiscountTransactionTypeId + '.');
        System.assertEquals(CreateSubscriptionController.SCHOOL_DISCOUNT, transactionLineItem.Transaction_Type__c,
                'Expected the Transaction Type to be ' + CreateSubscriptionController.SCHOOL_DISCOUNT + '.');
        System.assertEquals(expectedAmount, Decimal.valueOf(transactionLineItem.Product_Amount__c),
                'Expected the product amount to be ' + expectedAmount + '.');
        System.assertEquals(System.today(), transactionLineItem.Transaction_Date__c,
                'Expected the transaction date to be today.');
    }

    private static List<Transaction_Line_Item__c> gatherTransactionLineItems(Id opportunityId) {
        return [SELECT
            Transaction_Date__c,
            Transaction_Type__c,
            Product_Amount__c,
            RecordTypeId
           FROM Transaction_Line_Item__c
          WHERE Opportunity__c = :opportunityId];
    }

    @isTest
    private static void constructor_expectDefaultState() {
        ApexPages.StandardController standardController = createStandardController();

        Test.startTest();
        CreateSubscriptionController controller = new CreateSubscriptionController(standardController);
        Test.stopTest();

        System.assertNotEquals(null, controller.controller, 'Expected the standard controller to be saved.');
        System.assertEquals(standardController.getRecord().Id, controller.getOpportunity().Id,
                'Expected the Opportunity record returned to match the record provided.');

        System.assertEquals(CreateSubscriptionController.REGULAR, controller.SubscriptionType,
                'Expected the Subscription Type to be: ' + CreateSubscriptionController.REGULAR + '.');
        System.assert(controller.HasAcademicYear, 'Expected the Opportunity to have an Academic Year set.');
        System.assert(!controller.HasCurrentSubscription, 'Expected there to not be a current subscription.');
    }

    @isTest
    private static void hasCurrentSubscription_hasSubscriptionAlready_expectSubscriptionError() {
        ApexPages.StandardController standardController = createStandardController();
        Subscription__c subscription = insertSubscriptionForOpportunity((Opportunity)standardController.getRecord());
        CreateSubscriptionController controller = new CreateSubscriptionController(standardController);

        Test.startTest();
        Boolean hasCurrentSubscription = controller.HasCurrentSubscription;
        Test.stopTest();

        System.assert(hasCurrentSubscription, 'Expected there to be a current subscription.');

        String expectedErrorMessage = String.format(Label.SubscriptionAlreadyExists, new List<String>{
                String.valueOf(subscription.Start_Date__c.year()),
                '<a href=\'/' + subscription.Id + '\'>' + subscription.Name + '</a>'
        });

        System.assertEquals(expectedErrorMessage, controller.getSubscriptionExistsErrorMessage(),
                'Expected the error message to match.');

        List<ApexPages.Message> messages = ApexPages.getMessages();
        System.assertEquals(1, messages.size(), 'Expected only one page message.');
        System.assertEquals(expectedErrorMessage, messages[0].getDetail(),
                'Expected the message to match the subscription error message.');
    }

    @isTest
    private static void hasCurrentSubscription_noSubscription_expectNoErrorMessage() {
        CreateSubscriptionController controller = new CreateSubscriptionController(createStandardController());

        Test.startTest();
        Boolean hasCurrentSubscription = controller.HasCurrentSubscription;
        Test.stopTest();

        System.assert(!hasCurrentSubscription, 'Expected there to be a current subscription.');
        System.assertEquals(null, controller.getSubscriptionExistsErrorMessage(),
                'Expected the error message to be null.');
        List<ApexPages.Message> messages = ApexPages.getMessages();
        System.assertEquals(0, messages.size(), 'Expected there to be no page messages.');
    }

    @isTest
    private static void hasAcademicYear_noAcademicYear_expectErrorMessage() {
        ApexPages.StandardController standardController = createStandardController();

        Opportunity opportunity = (Opportunity)standardController.getRecord();
        opportunity.Academic_Year_Picklist__c = null;
        update opportunity;

        CreateSubscriptionController controller = new CreateSubscriptionController(standardController);

        Test.startTest();
        Boolean hasAcademicYear = controller.HasAcademicYear;
        Test.stopTest();

        System.assert(!hasAcademicYear, 'Expected there to be no academic year.');

        List<ApexPages.Message> messages = ApexPages.getMessages();
        System.assertEquals(1, messages.size(), 'Expected only one page message.');
        System.assertEquals(Label.NoAcademicYearDefined, messages[0].getDetail(),
                'Expected the message to match the no academic year defined error message.');
    }

    @isTest
    private static void hasAcademicYear_yearExists_expectNoErrorMessage() {
        CreateSubscriptionController controller = new CreateSubscriptionController(createStandardController());

        Test.startTest();
        Boolean hasAcademicYear = controller.HasAcademicYear;
        Test.stopTest();

        System.assert(hasAcademicYear, 'Expected there to be an academic year defined.');
        List<ApexPages.Message> messages = ApexPages.getMessages();
        System.assertEquals(0, messages.size(), 'Expected there to be no page messages.');
    }

    @isTest
    private static void academicYearRecordMissing_AcademicYearExists_expectFalse() {
        CreateSubscriptionController controller = new CreateSubscriptionController(createStandardController());

        Test.startTest();
        Boolean academicYearRecordMissing = controller.AcademicYearRecordMissing;
        Test.stopTest();

        System.assert(!academicYearRecordMissing, 'Expected the Academic Year to not be missing.');
    }

    @isTest
    private static void academicYearRecordMissing_AcademicYearDoesNotExist_expectTrue() {
        deleteAcademicYear();
        CreateSubscriptionController controller = new CreateSubscriptionController(createStandardController());
        Opportunity opportunity = controller.getOpportunity();

        Test.startTest();
        Boolean academicYearRecordMissing = controller.AcademicYearRecordMissing;
        Test.stopTest();

        System.assert(academicYearRecordMissing, 'Expected the Academic Year to be missing.');

        // Since the academic year is missing, let's make sure there's a page message as well.
        List<ApexPages.Message> messages = ApexPages.getMessages();
        System.assertEquals(1, messages.size(), 'Expected exactly one page message.');
        String expectedMessage = String.format(Label.AcademicYearRecordMissing, new List<String> {
                opportunity.Academic_Year_Picklist__c
        });
        System.assertEquals(expectedMessage, messages[0].getDetail(),
                'Expected the message to match the Academic Year missing message.');
    }

    @isTest
    private static void returnToOpportunity_expectReturnedToOpportunity() {
        CreateSubscriptionController controller = new CreateSubscriptionController(createStandardController());

        String expectedUrl = CreateSubscriptionController.FORWARD_SLASH + controller.getOpportunity().Id;

        Test.startTest();
        PageReference returnPage = controller.returnToOpportunity();
        Test.stopTest();

        System.assertEquals(expectedUrl, returnPage.getUrl(), 'Expected the URL to be ' + expectedUrl + '.');
    }

    @isTest
    private static void getSubscriptionTypeOptions_expectRegularAndDiscounted() {
        CreateSubscriptionController controller = new CreateSubscriptionController(createStandardController());

        Test.startTest();
        List<SelectOption> options = controller.getSubscriptionTypeOptions();
        Test.stopTest();

        System.assertEquals(2, options.size(), 'Expected there to be two options.');
        System.assertEquals(CreateSubscriptionController.REGULAR, options[0].getLabel(),
                'Expected the first option to be ' + CreateSubscriptionController.REGULAR + '.');
        System.assertEquals(CreateSubscriptionController.DISCOUNTED, options[1].getLabel(),
                'Expected the second option to be ' + CreateSubscriptionController.DISCOUNTED + '.');
    }

    @isTest
    private static void schoolDiscount_defaultState() {
        CreateSubscriptionController controller = new CreateSubscriptionController(createStandardController());

        Test.startTest();
        Transaction_Line_Item__c schoolDiscount = controller.SchoolDiscount;
        Test.stopTest();

        System.assertNotEquals(null, schoolDiscount,
                'Expected the School Discount transaction line item to not be null.');
        System.assertEquals(null, schoolDiscount.Id, 'Expected the record to not be inserted yet.');
        System.assertEquals(CreateSubscriptionController.SCHOOL_DISCOUNT, schoolDiscount.Transaction_Type__c,
                'Expected the transaction type to be ' + CreateSubscriptionController.SCHOOL_DISCOUNT + '.');
        System.assertEquals(controller.getOpportunity().Id, schoolDiscount.Opportunity__c,
                'Expected the Opportunity to be set to ' + controller.getOpportunity().Id + '.');
        System.assertEquals(RecordTypes.schoolDiscountTransactionTypeId, schoolDiscount.RecordTypeId,
                'Expected the Record Type Id to be ' + RecordTypes.schoolDiscountTransactionTypeId + '.');
        System.assertEquals(System.today(), schoolDiscount.Transaction_Date__c,
                'Expected the transaction date to be today.');
    }

    @isTest
    private static void generateTransactions_regularSubscriptionNoDiscounts_expectSaleTransactionLineItem() {
        CreateSubscriptionController controller = new CreateSubscriptionController(createStandardController());

        Test.startTest();
        PageReference pageReference = controller.generateTransactions();
        Test.stopTest();

        List<Transaction_Line_Item__c> transactionLineItems = gatherTransactionLineItems(controller.getOpportunity().Id);

        System.assertEquals(1, transactionLineItems.size(), 'Expected there to be one transaction line item.');
        assertSaleTransactionLineItem(transactionLineItems[0]);
    }

    @isTest
    private static void generateTransactions_regularIsEarlyBirdIsNotRenewal_expectOnlySaleTransactionLineItem() {
        CreateSubscriptionController controller = new CreateSubscriptionController(createStandardController());
        setEarlyBird();

        System.assert(GlobalVariables.isEarlyBirdRenewal(), 'Expected Early Bird renewal to be available.');

        Test.startTest();
        PageReference pageReference = controller.generateTransactions();
        Test.stopTest();

        List<Transaction_Line_Item__c> transactionLineItems = gatherTransactionLineItems(controller.getOpportunity().Id);

        System.assertEquals(1, transactionLineItems.size(), 'Expected there to be one transaction line item.');
        assertSaleTransactionLineItem(transactionLineItems[0]);
    }

    @isTest
    private static void generateTransactions_regularIsNotEarlyBirdIsRenewal_expectOnlySaleTransactionLineItem() {
        ApexPages.StandardController standardController = createStandardController();

        Opportunity opportunity = (Opportunity)standardController.getRecord();
        opportunity.New_Renewal__c = 'Renewal';
        update opportunity;

        CreateSubscriptionController controller = new CreateSubscriptionController(standardController);

        Test.startTest();
        PageReference pageReference = controller.generateTransactions();
        Test.stopTest();

        List<Transaction_Line_Item__c> transactionLineItems = gatherTransactionLineItems(controller.getOpportunity().Id);

        System.assertEquals(1, transactionLineItems.size(), 'Expected there to be one transaction line item.');
        assertSaleTransactionLineItem(transactionLineItems[0]);
    }

    @isTest
    private static void generateTransactions_regularIsEarlyBirdIsRenewal_expectSaleAndEarlyBirdTransactionLineItems() {
        ApexPages.StandardController standardController = createStandardController();

        Opportunity opportunity = (Opportunity)standardController.getRecord();
        opportunity.New_Renewal__c = 'Renewal';
        update opportunity;

        CreateSubscriptionController controller = new CreateSubscriptionController(standardController);
        setEarlyBird();

        System.assert(GlobalVariables.isEarlyBirdRenewal(), 'Expected Early Bird renewal to be available.');

        Test.startTest();
        PageReference pageReference = controller.generateTransactions();
        Test.stopTest();

        List<Transaction_Line_Item__c> transactionLineItems = gatherTransactionLineItems(controller.getOpportunity().Id);

        System.assertEquals(2, transactionLineItems.size(), 'Expected there to be two transaction line items.');

        Boolean saleTransactionLineItemFound = false;
        Boolean earlyBirdTransactionLineItemFound = false;

        for (Transaction_Line_Item__c transactionLineItem : transactionLineItems) {
            if (transactionLineItem.RecordTypeId == RecordTypes.saleTransactionTypeId) {
                assertSaleTransactionLineItem(transactionLineItem);
                saleTransactionLineItemFound = true;
            } else if (transactionLineItem.RecordTypeId == RecordTypes.schoolDiscountTransactionTypeId) {
                earlyBirdTransactionLineItemFound = true;
                assertEarlyBirdTransactionLineItem(transactionLineItem);
            }
        }

        System.assert(saleTransactionLineItemFound && earlyBirdTransactionLineItemFound,
                'Expected both Transaction Line Items to be found.');
    }

    @isTest
    private static void generateTransactions_discounted_expectSaleAndDiscountTransactionLineItemsGenerated() {
        CreateSubscriptionController controller = new CreateSubscriptionController(createStandardController());
        controller.SubscriptionType = CreateSubscriptionController.DISCOUNTED;

        controller.SchoolDiscount.Product_Code__c = PRODUCT_CODE;
        controller.SchoolDiscount.Product_Amount__c = '125.00';
        controller.SchoolDiscount.Discount_Reason__c = 'Reason';

        Test.startTest();
        PageReference pageReference = controller.generateTransactions();
        Test.stopTest();

        List<Transaction_Line_Item__c> transactionLineItems = gatherTransactionLineItems(controller.getOpportunity().Id);

        System.assertEquals(2, transactionLineItems.size(), 'Expected there to be two transaction line items.');

        Boolean saleTransactionLineItemFound = false;
        Boolean discountTransactionLineItemFound = false;

        for (Transaction_Line_Item__c transactionLineItem : transactionLineItems) {
            if (transactionLineItem.RecordTypeId == RecordTypes.saleTransactionTypeId) {
                assertSaleTransactionLineItem(transactionLineItem);
                saleTransactionLineItemFound = true;
            } else if (transactionLineItem.RecordTypeId == RecordTypes.schoolDiscountTransactionTypeId) {
                discountTransactionLineItemFound = true;
                assertDiscountTransactionLineItem(transactionLineItem, 125.00);
            }
        }

        System.assert(saleTransactionLineItemFound && discountTransactionLineItemFound,
                'Expected both Transaction Line Items to be found.');
    }

    @isTest
    private static void generateTransactions_discounted_opportunityBecameClosed()
    {
        CreateSubscriptionController controller = new CreateSubscriptionController(createStandardController());
        controller.SubscriptionType = CreateSubscriptionController.DISCOUNTED;

        controller.SchoolDiscount.Product_Code__c = PRODUCT_CODE;
        controller.SchoolDiscount.Product_Amount__c = '200.00';
        controller.SchoolDiscount.Discount_Reason__c = 'Reason';

        Test.startTest();
            PageReference pageReference = controller.generateTransactions();
        Test.stopTest();

        Opportunity opp = controller.getOpportunity(true);
        System.assertEquals(0, opp.Net_Amount_Due__c);
        System.assertEquals(CreateSubscriptionController.CLOSED_WON, opp.StageName);
    }

    @isTest
    private static void generateTransactions_discounted_opportunityRemainedProspecting()
    {
        CreateSubscriptionController controller = new CreateSubscriptionController(createStandardController());
        controller.SubscriptionType = CreateSubscriptionController.DISCOUNTED;

        insert new Transaction_Line_Item__c (
            Transaction_Type__c = CreateSubscriptionController.SCHOOL_SUBSCRIPTION,
            Opportunity__c = controller.getOpportunity().Id,
            Transaction_Date__c = System.today(),
            Product_Amount__c = '300.00');

        controller.SchoolDiscount.Product_Code__c = PRODUCT_CODE;
        controller.SchoolDiscount.Product_Amount__c = '200.00';
        controller.SchoolDiscount.Discount_Reason__c = 'Reason';


        Test.startTest();
            PageReference pageReference = controller.generateTransactions();
        Test.stopTest();

        Opportunity opp = controller.getOpportunity(true);
        System.assertEquals(100, opp.Net_Amount_Due__c);
        System.assertEquals(OPP_STAGENAME_PROSPECTING, opp.StageName);
    }
    
    //SFP-929
    @isTest
    private static void generateTransactions_regularHasExistingDiscount_expectDiscountFoundAndDeleted() {
        
        //1. Create the subscription opportunity that needs to be paid 
        Opportunity opportunityRecord = [SELECT Id, AccountId FROM Opportunity LIMIT 1];
        
        //2. Create a "sale" TLI for the "Full" opportunity along with the "discount" TLI.
        //   In this step we manually emulate what is done through:
        //   - Click "Create Subscription" for a "Full" opportunity without TLI records. And select "Discount" as "Subscription Type".
        //     The sale amount will be greater than the discount. Otherwise, the opp would be already "Paid" and "Closed Won" and a subscription would already exist.
        Transaction_Line_Item__c sale = TransactionLineItemTestData.Instance.asSale()
                .forOpportunity(opportunityRecord.Id).forAmount(SCHOOL_DISCOUNT_AMOUNT + 200).create();
        Transaction_Line_Item__c existingDiscount = TransactionLineItemTestData.Instance.asSchoolDiscount()
                .forOpportunity(opportunityRecord.Id).forAmount(SCHOOL_DISCOUNT_AMOUNT).create();
        insert new List<Transaction_Line_Item__c>{sale, existingDiscount};
        
        //3. Create an instance of CreateSubscriptionController for a Regular subscription.
        CreateSubscriptionController controller = new CreateSubscriptionController(createStandardController());
        controller.SubscriptionType = CreateSubscriptionController.REGULAR;

        System.assertEquals(existingDiscount.Id, controller.SchoolDiscount.Id, 'Expected the same Discount to be used.');

        Test.startTest();
        controller.generateTransactions();
        Test.stopTest();

        // We assert the Id here since the property will automatically generate a new discount in the get method.
        System.assertEquals(null, controller.SchoolDiscount.Id, 'Expected the school discount to be deleted.');
    }

    @isTest
    private static void getBlockSubmission_regularSelected_expectFalse() {
        CreateSubscriptionController controller = new CreateSubscriptionController(createStandardController());

        Test.startTest();
        Boolean shouldBlockSubmission = controller.getBlockSubmission();
        Test.stopTest();

        System.assert(!shouldBlockSubmission, 'Expected the form to allow submission.');
    }

    @isTest
    private static void getBlockSubmission_discountedSelectedNoProductCodeChosen_expectTrue() {
        CreateSubscriptionController controller = new CreateSubscriptionController(createStandardController());

        Test.startTest();
        controller.SubscriptionType = CreateSubscriptionController.DISCOUNTED;

        Boolean shouldBlockSubmission = controller.getBlockSubmission();
        Test.stopTest();

        System.assert(shouldBlockSubmission, 'Expected the form to block submission.');
    }

    @isTest
    private static void getBlockSubmission_discountedSelectedProductCodeChosen_expectFalse() {
        CreateSubscriptionController controller = new CreateSubscriptionController(createStandardController());

        Test.startTest();
        controller.SubscriptionType = CreateSubscriptionController.DISCOUNTED;
        controller.SchoolDiscount.Product_Code__c = PRODUCT_CODE;

        Boolean shouldBlockSubmission = controller.getBlockSubmission();
        Test.stopTest();

        System.assert(!shouldBlockSubmission, 'Expected the form to allow submission.');
    }
}