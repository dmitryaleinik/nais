/*
* SPEC-121
*
* The school should be able to see all answers provided by the families on the main application screens
*
* Nathan, Exponent Partners, 2013
*/
@isTest
private class SchoolPFSAdditionalInfoControllerTest
{

    private class TestData {
        String academicYearId;
        Account school1;
        Contact staff1, parentA, parentB, student1, student2;
        Applicant__c applicant1A, applicant2A;
        PFS__c pfs1;
        Student_Folder__c studentFolder1, studentFolder2;
        School_PFS_Assignment__c spfsa1, spfsa2;

        private TestData() {
            // academic year
            TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
            academicYearId = GlobalVariables.getCurrentAcademicYear().Id;  

            // school
            school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, null, false); 
            insert school1;       

            // parents and students
            parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
            parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
            student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
            student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
            insert new List<Contact> {parentA, parentB, student1, student2};

            // psf
            pfs1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
            insert new List<PFS__c> {pfs1};
            
            // applicant
            applicant1A = TestUtils.createApplicant(student1.Id, pfs1.Id, false);
            applicant2A = TestUtils.createApplicant(student2.Id, pfs1.Id, false);
            insert new List<Applicant__c> {applicant1A, applicant2A};

            // student folder
            studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
            studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', academicYearId, student2.Id, false);
            insert new List <Student_Folder__c> {studentFolder1, studentFolder2};

            // school pfs assignment
            spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder1.Id, false);
            spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.Id, school1.Id, studentFolder2.Id, false);
            insert new List <School_PFS_Assignment__c> {spfsa1, spfsa2};
        }
    }

    @isTest
    private static void testCarList(){
        TestData td = new TestData();

        td.pfs1.Car_1_Make__c = 'Make 1';
        update td.pfs1;
        
        Test.startTest();
        PageReference pageRef = Page.SchoolPFSAdditionalInformation;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSAdditionalInfoController controller = new SchoolPFSAdditionalInfoController();        

        system.assertEquals('Make 1', controller.vehicleListDisplay[0].pfs.Car_1_Make__c); 
        Test.stopTest();    
    }

    @isTest
    private static void testBoatRVList() {
        TestData td = new TestData();

        //td.pfs1.Boat_Rec_Vehicle_1_Make__c = 'Make 1';
        update td.pfs1;
        
        Test.startTest();
        PageReference pageRef = Page.SchoolPFSAdditionalInformation;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSAdditionalInfoController controller = new SchoolPFSAdditionalInfoController();        

        //system.assertEquals('Make 1', controller.vehicleListDisplay[0].pfs.Boat_Rec_Vehicle_1_Make__c); 
        Test.stopTest();    
    }

    @isTest
    private static void testExpenses() {
        TestData td = new TestData();

        td.applicant1A.Tuition_Cost_Current__c = 10;
        td.applicant1A.Parent_Sources_Current__c = 10;
        td.applicant1A.Student_Sources_Current__c = 10;
        td.applicant1A.Financial_Aid_Sources_Current__c = 10;
        td.applicant1A.Loan_Sources_Current__c = 10;
        td.applicant1A.Other_Source_Sources_Current__c = 10;
        update td.applicant1A;
        
        Test.startTest();
        PageReference pageRef = Page.SchoolPFSAdditionalInformation;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSAdditionalInfoController controller = new SchoolPFSAdditionalInfoController();        

        // nothing to test
        Test.stopTest();    
    }

    @isTest
    private static void testIncome() {
        TestData td = new TestData();

        td.applicant1A.Income_Earned_Current__c = 10;
        update td.applicant1A;
        
        Test.startTest();
        PageReference pageRef = Page.SchoolPFSAdditionalInformation;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSAdditionalInfoController controller = new SchoolPFSAdditionalInfoController();        

        // nothing to test
        Test.stopTest();    
    }
    
    // NAIS-2498 START
    // positive test
    @isTest
    private static void testPFSWithBusinessFarms() {
        TestData td = new TestData();
        
        Business_Farm__c bf1 = TestUtils.createBusinessFarm(td.pfs1.Id, 'Business ABC', 1, 'Business', 'Partnership', false);
        bf1.Business_Street__c = '720 Market Street';
        bf1.Business_City__c = 'San Francisco';
        Business_Farm__c bf2 = TestUtils.createBusinessFarm(td.pfs1.Id, 'Business XYZ', 2, 'Farm', 'Sole Proprietorship', false);
        bf2.Business_State_Province__c = 'CA';
        bf2.Business_Zip_Postal_Code__c = '94103';
        insert new List<Business_Farm__c> { bf1, bf2 };
        
        Test.startTest();
        PageReference pageRef = Page.SchoolPFSAdditionalInformation;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSAdditionalInfoController controller = new SchoolPFSAdditionalInfoController();
        Test.stopTest();
        
        // verify that a list of 2 business farms and the business name map are populated
        System.assertEquals(2, controller.bfList.size());
        System.assertEquals('Business ABC', controller.bfList[0].Name);
        System.assertEquals('Farm', controller.bfList[1].Business_or_Farm__c);
        System.assertEquals('720 Market Street, San Francisco', controller.businessAddressByBFId.get(bf1.Id));
        System.assertEquals('CA 94103', controller.businessAddressByBFId.get(bf2.Id));
    }
    
    // negative test
    @isTest
    private static void testPFSWithNoBusinessFarms() {
        TestData td = new TestData();
        
        Test.startTest();
        PageReference pageRef = Page.SchoolPFSAdditionalInformation;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSAdditionalInfoController controller = new SchoolPFSAdditionalInfoController();
        Test.stopTest();
        
        // verify that list of business farms and business name map are empty
        System.assertNotEquals(null, controller.bfList);
        System.assertEquals(0, controller.bfList.size());
        System.assertNotEquals(null, controller.businessAddressByBFId);
        System.assertEquals(0, controller.businessAddressByBFId.size());
    }
    // NAIS-2498 END
}