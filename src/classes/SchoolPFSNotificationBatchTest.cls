/*
 * Spec-096 School Portal - School PFS - Notification, Req# R-425, R-429
 *  An option Account.Alert_Frequency__c sets the frequency of PFS Submission notification: Once per Day, Once per Week.
 *
 *  Whenever a PFS is submitted, each school on the School PFS Assignments is checked to see what their frequency is, and when the last send was.
 *  If the last email send (read-only field Account.Last_PFS_Notification_Date__c) is more than the day or week depending on the frequency, all of the PFS
 *  records for that time period are grabbed (based on Submission Date) and listed in the email. Once the email is sent, Account.Last_PFS_Notification_Date__c
 *  is updated.
 *
 *  Recipients are all staff contacts at that school who are set to receive email: Contact.PFS_Alert_Preferences__c.
 *
 * WH, Exponent Partners, 2013
 */
@isTest
private class SchoolPFSNotificationBatchTest {
    
    static void insertUsers(List<Contact> contacts){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            List<User> users = new List<User>();
            User u;
            Integer i=1;
            for(Contact c:contacts){
                u = TestUtils.createPortalUser(c.FirstName, c.Email, 'sff'+i, c.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);
                u.LastName = c.LastName;
                u.IsActive = true; 
                users.add(u);
                i++;
            }
            if(!users.isEmpty())insert users;
        }
    }
    
    // variant of SchoolPFSNotificationTest.sendEmailLimitExceeded
    @isTest
    private static void sendEmailLimitNotExceeded() {
        Family_Portal_Settings__c fps = new Family_Portal_Settings__c();
        fps.Name = 'Family';
        fps.Individual_Account_Owner_Id__c = UserInfo.getUserId();
        insert fps;
        
        Integer max = limits.getLimitEmailInvocations();
        Integer cnt = max;                                  // can only execute 1 batch in test
        
        List<Account> schoolList = new List<Account>();
        for (Integer i = 0; i < cnt; i++) {
            Account school = TestUtils.createAccount('School ' + i, RecordTypes.schoolAccountTypeId, null, false);
            school.SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest();
            school.Alert_Frequency__c = 'Daily digest of new applications';
            school.Last_PFS_Notification_Date__c = System.today() - 1;
            school.Max_Number_of_Users__c = 100;
            schoolList.add(school);
        }
        insert schoolList;
        
        List<Contact> staffList = new List<Contact>();
        for (Integer i = 0; i < cnt; i++) {
            Contact staff = TestUtils.createContact('Staff ' + i, schoolList[i].Id, RecordTypes.schoolStaffContactTypeId, false);
            staff.Email = 'staff' + i + '@test.org';
            staff.PFS_Alert_Preferences__c = 'Receive Alerts';
            staffList.add(staff);
        }
        insert staffList;
        insertUsers(staffList);
        
        Contact parent = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        
        Contact student = TestUtils.createContact('Student A1', null, RecordTypes.studentContactTypeId, false);
        
        insert new List<Contact> { parent, student };
        
        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        
        TestUtils.createUnusualConditions(academicYearId, true);
        
        PFS__c pfs = TestUtils.createPFS('PFS A', academicYearId, parent.Id, false);
        pfs.Original_Submission_Date__c = (Datetime)System.today().addDays(-1);
        pfs.PFS_Status__c = SchoolPFSNotification.PFS_STATUS_APPLICATION_SUBMITTED;
        pfs.Payment_Status__c = SchoolPFSNotification.PFS_PAYMENT_STATUS_PAID_IN_FULL;
        insert pfs;
        
        Applicant__c applicant = TestUtils.createApplicant(student.Id, pfs.Id, true);
        
        List<Student_Folder__c> folderList = new List<Student_Folder__c>();
        for (Integer i = 0; i < cnt; i++) {
            Student_Folder__c folder = TestUtils.createStudentFolder('Student Folder ' + i, academicYearId, student.Id, false);
            folderList.add(folder);
        }
        insert folderList;
        
        List<School_PFS_Assignment__c> spfsaList = new List<School_PFS_Assignment__c>();
        for (Integer i = 0; i < cnt; i++) {
            School_PFS_Assignment__c spfsa = TestUtils.createSchoolPFSAssignment(academicYearId, applicant.Id, schoolList[i].Id, folderList[i].Id, false);
            spfsa.Withdrawn__c = 'No';
            spfsa.PFS_Status__c = SchoolPFSNotification.PFS_STATUS_APPLICATION_SUBMITTED;
            spfsa.Sharing_Processed__c = true;
            spfsa.School_Alert_Sent__c = false;
            spfsaList.add(spfsa);
        }
        
        Test.startTest();
        insert spfsaList;
        Database.executeBatch(new SchoolPFSNotificationBatch(), Limits.getLimitEmailInvocations());
        Test.stopTest();
        
        // all accounts should be processed successfully in 1 batch w/o exceeding email invocation limit
        for (Account a : [select Last_PFS_Notification_Date__c from Account where Id in :schoolList]) {
            System.assertEquals(System.today(), a.Last_PFS_Notification_Date__c);
        }
    }

    @isTest
    private static void schoolIdConstructor_sendEmailLimitNotExceeded() {
        Family_Portal_Settings__c fps = new Family_Portal_Settings__c();
        fps.Name = 'Family';
        fps.Individual_Account_Owner_Id__c = UserInfo.getUserId();
        insert fps;

        Integer max = limits.getLimitEmailInvocations();
        Integer cnt = max;                                  // can only execute 1 batch in test

        List<Account> schoolList = new List<Account>();
        for (Integer i = 0; i < cnt; i++) {
            Account school = TestUtils.createAccount('School ' + i, RecordTypes.schoolAccountTypeId, null, false);
            school.SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest();
            school.Alert_Frequency__c = 'Daily digest of new applications';
            school.Last_PFS_Notification_Date__c = System.today() - 1;
            school.Max_Number_of_Users__c = 100;
            schoolList.add(school);
        }
        insert schoolList;

        List<Contact> staffList = new List<Contact>();
        for (Integer i = 0; i < cnt; i++) {
            Contact staff = TestUtils.createContact('Staff ' + i, schoolList[i].Id, RecordTypes.schoolStaffContactTypeId, false);
            staff.Email = 'staff' + i + '@test.org';
            staff.PFS_Alert_Preferences__c = 'Receive Alerts';
            staffList.add(staff);
        }
        insert staffList;
        insertUsers(staffList);

        Contact parent = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);

        Contact student = TestUtils.createContact('Student A1', null, RecordTypes.studentContactTypeId, false);

        insert new List<Contact> { parent, student };

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfs = TestUtils.createPFS('PFS A', academicYearId, parent.Id, false);
        pfs.Original_Submission_Date__c = (Datetime)System.today().addDays(-1);
        pfs.PFS_Status__c = SchoolPFSNotification.PFS_STATUS_APPLICATION_SUBMITTED;
        pfs.Payment_Status__c = SchoolPFSNotification.PFS_PAYMENT_STATUS_PAID_IN_FULL;
        insert pfs;

        Applicant__c applicant = TestUtils.createApplicant(student.Id, pfs.Id, true);

        List<Student_Folder__c> folderList = new List<Student_Folder__c>();
        for (Integer i = 0; i < cnt; i++) {
            Student_Folder__c folder = TestUtils.createStudentFolder('Student Folder ' + i, academicYearId, student.Id, false);
            folderList.add(folder);
        }
        insert folderList;

        List<School_PFS_Assignment__c> spfsaList = new List<School_PFS_Assignment__c>();
        for (Integer i = 0; i < cnt; i++) {
            School_PFS_Assignment__c spfsa = TestUtils.createSchoolPFSAssignment(academicYearId, applicant.Id, schoolList[i].Id, folderList[i].Id, false);
            spfsa.Withdrawn__c = 'No';
            spfsa.Sharing_Processed__c = true;
            spfsa.School_Alert_Sent__c = false;
            spfsa.PFS_Status__c = SchoolPFSNotification.PFS_STATUS_APPLICATION_SUBMITTED;
            spfsaList.add(spfsa);
        }

        Map<Id, Account> schoolsById = new Map<Id, Account>(schoolList);

        Test.startTest();
        insert spfsaList;
        Database.executeBatch(new SchoolPFSNotificationBatch(schoolsById.keySet()), Limits.getLimitEmailInvocations());
        Test.stopTest();

        // all accounts should be processed successfully in 1 batch w/o exceeding email invocation limit
        for (Account a : [select Last_PFS_Notification_Date__c from Account where Id in :schoolList]) {
            System.assertEquals(System.today(), a.Last_PFS_Notification_Date__c);
        }
    }
}