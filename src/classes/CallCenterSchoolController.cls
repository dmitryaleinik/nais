//fill out case on edit
// 2 fill out contact recordtype
//work fone column header-  exp sandbox
public with sharing class CallCenterSchoolController
{

    public class SearchResult
    {
        public String contactId {get;set;}    
        public String contactName {get;set;}    
        public String contactemail {get;set;}    
        public String accountId {get;set;}
        public String accountName {get;set;}
        public String accountSSSCode {get;set;}
        public String accountPhone {get;set;}
        public String accountCity {get;set;}
        public String accountState {get;set;}
        public String accountZip {get;set;}
        public String accountCountry {get;set;}
        
        public void setAccountInfo(Account account) {
            this.accountId =account.id;
            this.accountName =account.name;
            this.accountSSSCode =account.SSS_School_Code__c;
            this.accountPhone =account.Phone;
            this.accountCity =account.BillingCity;
            this.accountState =account.BillingState;
            this.accountZip =account.BillingPostalCode;
            this.accountCountry =account.BillingCountry;
        }
        public void setContactInfo(Contact contact){
            this.contactName = contact.name;
            this.contactId = contact.id;
            this.contactemail = contact.email;
            if (contact.account!=null)
                setAccountInfo(contact.account);
        }
    }
    
    public String ani    {get;set;}
    public String dnis    {get;set;}
    public String callID {get;set;}
    public String username {get;set;}
    public String searchTerm {get;set;}
    public boolean initialMatchFound {get;set;}
    public boolean renderSearchResults {get;set;}

    /*move this to recordtype class when it gets checked into GIT for now leave it here*/
    /*move this to recordtype class when it gets checked into GIT for now leave it here*/
       private static final Map<String, Schema.RecordTypeInfo> caseTypeMap = Case.SObjectType.getDescribe().getRecordTypeInfosByName();
 //    public static final Id schoolSupportCaseTypeId = caseTypeMap.get('School Support Case').RecordTypeId;

    public List <SearchResult> searchResults {get;set;}
    public String accountUrl {get;set;}
    //public String contactUrl {get;set;}
    
    public String caseUrl {get;set;}
    public String url {get;set;} // popup url
    
    private ID contactRecordTypeID = RecordTypes.schoolStaffContactTypeId;
    private List<ID> accountRecordTypeIDs = new List<ID>{ RecordTypes.schoolAccountTypeId,RecordTypes.accessOrgAccountTypeId };

    public CallCenterSchoolController() {
        initialMatchFound = false;
    }
    
    public void doSearch()
    {
        if (this.searchTerm!=null && searchTerm.length()>0)
        {
            this.searchResults = new List<SearchResult>();
            
            searchTerm='%'+searchTerm+'%';
            List<Account> searchList = [select id, name, SSS_School_Code__c, phone, BillingCity, BillingState, BillingCountry, BillingPostalCode from Account where 
                    recordtypeid in :accountRecordTypeIDs and 
                    (phone like :searchTerm  or SSS_School_Code__c like :searchTerm or name like :searchTerm)  ];
            
            for (Account account: searchList) {
                 SearchResult result = new SearchResult();
                 result.setAccountInfo(account);
                 searchResults.add(result);
            }
            
            if (searchResults.size()>20) {
                
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.info,'More than 20 results found. Try searching again with more specific criteria.');
                ApexPages.addMessage(myMsg);
            }
            else if    (searchResults.size() == 0 ) {
                searchResults = null;
            }
            
            renderSearchResults = true;
        }
    }
    
    public PageReference findContacts()
    {
        this.ani = ApexPages.currentPage().getParameters().get('ani');
        this.dnis = ApexPages.currentPage().getParameters().get('dnis');
        this.callID = ApexPages.currentPage().getParameters().get('callID');
        this.username = ApexPages.currentPage().getParameters().get('username');

        List<Contact> theContacts =null;
        
        if (ani != null && ani!='') {
            
            if (PhoneHelper.isValidPhone(ani)) { //if the phone is something we understand
                String sfFormatted = PhoneHelper.getSFPhone(ani);
                String sfSearchable = PhoneHelper.getSFSearchablePhone(ani);
                
                theContacts = [select Account.name, Account.id, Account.phone, Account.SSS_School_Code__c, Account.BillingCountry ,Account.BillingPostalCode, Account.BillingState, Account.BillingCity ,id, name,HomePhone, MobilePhone, Phone, email from contact
                    where recordtypeid = :contactRecordTypeID and (
                HomePhone=:ani or MobilePhone=:ani or Phone=:ani or Account.phone=:ani or 
                homePhone like :sfSearchable or MobilePhone like :sfSearchable or Phone like:sfSearchable  or Account.phone like:sfSearchable
                )];
                
            } else {
        
                theContacts = [select Account.name, Account.id, Account.phone, Account.SSS_School_Code__c, Account.BillingCountry ,Account.BillingPostalCode, Account.BillingState, Account.BillingCity ,id, name,HomePhone, MobilePhone, Phone,email from contact 
                    where  recordtypeid = :contactRecordTypeID and (
                        HomePhone=:ani or MobilePhone=:ani or Phone=:ani  or Account.phone=:ani
                )];
            }
        }
        else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.info,'Phone Number Unavailable');
            ApexPages.addMessage(myMsg);
        }
        this.accountURL = '/001/e?RecordType='+RecordTypes.schoolAccountTypeId;

        if ( theContacts==null || theContacts.size() == 0 ) {
            renderSearchResults = false;
            
            if (ani!=null && ani.length()>0) {
                accountURL+= '&acc10='+ani;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.info,'No School Found for ' + PhoneHelper.getSFPhone(ani));
                ApexPages.addMessage(myMsg);
            }
            
            // NAIS-1392 set retURL
            accountURL += '&retURL=' + EncodingUtil.urlEncode(ApexPages.currentPage().getUrl(), 'UTF-8');
            
            return null;
        }
        else {
            renderSearchResults = true;
            initialMatchFound = true;
        }
        
        // NAIS-1392 set retURL
        accountURL += '&retURL=' + EncodingUtil.urlEncode(ApexPages.currentPage().getUrl(), 'UTF-8');

        this.searchResults = new List<SearchResult>();
        
        Set<ID> accountIDs = new Set<ID>();
        
        for (Contact contact: theContacts)
        {
             SearchResult result = new SearchResult();
             result.setContactInfo(contact);
             searchResults.add(result);
             if (contact.account!=null)
                 accountIDs.add(contact.account.id);
        }
        
        if (accountIDs.size()==1) {
            
            this.url = '/' + theContacts[0].account.id;
            
            return null;
        }
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.info,'Multiple Contacts Found for ' +PhoneHelper.getSFPhone(ani));
        ApexPages.addMessage(myMsg);
        return null;
    }
}