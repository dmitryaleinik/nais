public with sharing class SchoolPFSComments {

    public list<School_Biz_Farm_Assignment__c> businesses {get;set;}
    public list<School_Biz_Farm_Assignment__c> farms {get;set;}
    public School_PFS_Assignment__c theSPA {get;set;}
    public string studentName {get;set;}
    public SchoolPFSComments() {
    
    string spaId = ApexPages.currentPage().getParameters().get('spaId');
    businesses = new list<School_Biz_Farm_Assignment__c>();
    farms = new list<School_Biz_Farm_Assignment__c>();

    list<School_PFS_Assignment__c> spa = [SELECT PFS_Number__c, Academic_Year_Name__c,Comment_Bank_Accounts__c, Comment_Capital_Gain_Loss__c, Comment_Cash_Support_Gift_Income__c, Comment_Child_Support_Received__c, 
            Comment_COLA_Value__c, Comment_Current_Tax_Return_Status__c, Comment_Deductible_Part_Self_Employ_Tax__c, Comment_Dependent_Children__c, 
            Comment_Depreciation_Schedule_E__c, Comment_Depreciation_Sec_179_Exp_4562__c, Comment_Depreciation_Sec_179_Exp_Sch_C__c, 
            Comment_Depreciation_Sec_179_Exp_Sch_F__c, Comment_Dividend_Income__c, Comment_Earned_Income_Credits__c, Comment_Family_Size__c, 
            Comment_Federal_Income_Tax_Revision__c, Comment_Filing_Status__c, Comment_Food_Housing_Other_Allowance__c, Comment_Home_Bus_Expense_Sch_C__c, 
            Comment_Home_Market_Value__c, Comment_Home_Purchase_Price__c, Comment_Home_Purchase_Year__c, Comment_Impute_Rate__c, Comment_Income_Earned_Abroad__c,
            Comment_Income_Tax_Exemptions__c, Comment_Interest_Income__c, Comment_Investments__c, Comment_IRA_Deduction__c, Comment_IRA_Distributions__c, 
            Comment_Itemized_Deductions__c, Comment_Medical_Dental_Expense__c, Comment_Med_Tax_Allowance_Calc_Revision__c, Comment_Num_Children_in_Tuition_Schools__c,
            Comment_Other_Adjustments_to_Income__c, Comment_Other_Gains_Losses__c, Comment_Other_Income__c, Comment_Other_Real_Estate_Market_Value__c, 
            Comment_Other_Real_Estate_Unpaid_Princip__c, Comment_Other_Support_fr_Divorced_Spouse__c, Comment_Other_Taxable_Income__c, Comment_Other_Untaxed_Income__c,
            Comment_Parent_State__c, Comment_Pensions_and_Annuities__c, Comment_Pre_Tax_Fringe_Benefit_Contr__c, Comment_Pre_Tax_Paymnts_to_Retirement__c, 
            Comment_Prof_Judgment_Minimum_Income_Amt__c, Comment_Rental_Real_Estate_Etc__c, Comment_Salary_Wages_Parent_A__c, Comment_Salary_Wages_Parent_B__c,
            Comment_Self_Employed_Plans__c, Comment_SEP_and_SIMPLE_Plans__c, Comment_Social_Security_Benefits__c, Comment_Social_Security_Taxable__c,
            Comment_Soc_Sec_Tax_Allowance_Calc_Rev__c, Comment_State_Other_Tax_Allow_Revision__c, Comment_Student_Assets__c, Comment_Taxable_Refunds__c,
            Comment_Tax_Exempt_Investment_Income__c, Comment_Total_Debts__c, Comment_Total_SE_Tax_Paid__c, Comment_Unemployment_Comp__c, 
            Comment_Unpaid_Principal_1st_Mortgage__c, Comment_Unpaid_Principal_2nd_Mortgage__c, Comment_Untaxed_IRA_Plan_Payment__c, Comment_Unusual_Expense__c,
            Comment_Welfare_Veterans_Workers_Comp__c, Comment_Alimony_Received__c, Student_Folder__r.Student_Name__c, (SELECT Business_Farm__r.name, Business_Farm__r.Business_or_Farm__c, Comment_Business_Farm_Assets__c, Comment_Business_Farm_Debts__c, Comment_Business_Farm_Share__c, Comment_Business_Farm_Owner__c, Comment_Business_Farm_Ownership_Perc__c, Comment_Net_Profit_Loss_Business_Farm__c FROM School_Biz_Farm_Assignments__r)
            FROM School_PFS_Assignment__c 
            WHERE id =:spaId];

        if(!spa.isEmpty()){
            theSPA = spa[0];
            list<School_Biz_Farm_Assignment__c> sbas = new list<School_Biz_Farm_Assignment__c>();
            for (School_Biz_Farm_Assignment__c sbfa : spa[0].getSObjects('School_Biz_Farm_Assignments__r')) 
                        if (sbfa.Business_Farm__r.Business_or_Farm__c != 'Farm')
                            businesses.add(sbfa);
                        else
                            farms.add(sbfa);
                    
            processSPAComments(spa[0]);
        }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'SPA could not be found.'));
        }
        
    }

    private void processSPAComments(School_PFS_Assignment__c spa){
        commentRows = new list<RowWrapper>();
        bizFarmRows = new list<RowWrapper>();

        if(!String.isEmpty(spa.Comment_Salary_Wages_Parent_A__c))
            commentRows.add(new RowWrapper('1', 'Salary/Wages - Income from Parent A',spa.Comment_Salary_Wages_Parent_A__c));
        if(!String.isEmpty(spa.Comment_Salary_Wages_Parent_B__c))
            commentRows.add(new RowWrapper('2', 'Salary/Wages - Income from Parent B',spa.Comment_Salary_Wages_Parent_B__c));
        if(!String.isEmpty(spa.Comment_Interest_Income__c))
            commentRows.add(new RowWrapper('3a', 'Interest Income',spa.Comment_Interest_Income__c));
        if(!String.isEmpty(spa.Comment_Dividend_Income__c))
            commentRows.add(new RowWrapper('3b', 'Dividend Income',spa.Comment_Dividend_Income__c));
        if(!String.isEmpty(spa.Comment_Alimony_Received__c))
            commentRows.add(new RowWrapper('4', 'Alimony Received',spa.Comment_Alimony_Received__c));

        

        if(!String.isEmpty(spa.Comment_Other_Taxable_Income__c))
            commentRows.add(new RowWrapper('7', 'Other Taxable Income',spa.Comment_Other_Taxable_Income__c));
        if(!String.isEmpty(spa.Comment_Taxable_Refunds__c))
            commentRows.add(new RowWrapper('7a', 'Taxable Refunds',spa.Comment_Taxable_Refunds__c));
        if(!String.isEmpty(spa.Comment_Capital_Gain_Loss__c))
            commentRows.add(new RowWrapper('7b', 'Capital Gain/Loss',spa.Comment_Capital_Gain_Loss__c));
        if(!String.isEmpty(spa.Comment_Other_Gains_Losses__c))
            commentRows.add(new RowWrapper('7c', 'Other Gain/Loss',spa.Comment_Other_Gains_Losses__c));
        if(!String.isEmpty(spa.Comment_IRA_Distributions__c))
            commentRows.add(new RowWrapper('7d', 'IRA Distributions',spa.Comment_IRA_Distributions__c));
        if(!String.isEmpty(spa.Comment_Pensions_and_Annuities__c))
            commentRows.add(new RowWrapper('7e', 'Pensions and Annuities',spa.Comment_Pensions_and_Annuities__c));
        if(!String.isEmpty(spa.Comment_Rental_Real_Estate_Etc__c))
            commentRows.add(new RowWrapper('7f', 'Rental Real Estate, Royalties, Partnerships, S Corps, Trusts, etc.',spa.Comment_Rental_Real_Estate_Etc__c));
        if(!String.isEmpty(spa.Comment_Unemployment_Comp__c))
            commentRows.add(new RowWrapper('7g', 'Unemployment Compensation',spa.Comment_Unemployment_Comp__c));
        if(!String.isEmpty(spa.Comment_Social_Security_Taxable__c))
            commentRows.add(new RowWrapper('7h', 'Social Security Benefits, taxable',spa.Comment_Social_Security_Taxable__c));
        if(!String.isEmpty(spa.Comment_Other_Income__c))
            commentRows.add(new RowWrapper('7i', 'Other Income',spa.Comment_Other_Income__c));
        if(!String.isEmpty(spa.Comment_Deductible_Part_Self_Employ_Tax__c))
            commentRows.add(new RowWrapper('8a', 'Deductible Part of Self-Employment Tax',spa.Comment_Deductible_Part_Self_Employ_Tax__c));
        if(!String.isEmpty(spa.Comment_Self_Employed_Plans__c))
            commentRows.add(new RowWrapper('8b', 'Self-Employed SEP, SIMPLE, and Qualified Plan Payments',spa.Comment_Self_Employed_Plans__c));
        if(!String.isEmpty(spa.Comment_IRA_Deduction__c))
            commentRows.add(new RowWrapper('8c', 'IRA Deduction',spa.Comment_IRA_Deduction__c));
        if(!String.isEmpty(spa.Comment_Other_Adjustments_to_Income__c))
            commentRows.add(new RowWrapper('8d', 'Other Adjustments to Income',spa.Comment_Other_Adjustments_to_Income__c));
        if(!String.isEmpty(spa.Comment_Child_Support_Received__c))
            commentRows.add(new RowWrapper('10', 'Child Support Received',spa.Comment_Child_Support_Received__c));
        if(!String.isEmpty(spa.Comment_Social_Security_Benefits__c))
            commentRows.add(new RowWrapper('11', 'Social Security Benefits',spa.Comment_Social_Security_Benefits__c));
        if(!String.isEmpty(spa.Comment_Pre_Tax_Paymnts_to_Retirement__c))
            commentRows.add(new RowWrapper('12a', 'Pre-Tax Payments to Retirement Plans',spa.Comment_Pre_Tax_Paymnts_to_Retirement__c));
        if(!String.isEmpty(spa.Comment_Pre_Tax_Fringe_Benefit_Contr__c))
            commentRows.add(new RowWrapper('12b', 'Pre-Tax Contributions to Fringe Benefits',spa.Comment_Pre_Tax_Fringe_Benefit_Contr__c));
        if(!String.isEmpty(spa.Comment_Cash_Support_Gift_Income__c))
            commentRows.add(new RowWrapper('12c', 'Cash Support, Gift Income',spa.Comment_Cash_Support_Gift_Income__c));
        if(!String.isEmpty(spa.Comment_Other_Support_fr_Divorced_Spouse__c))
            commentRows.add(new RowWrapper('12d', 'Other Support Received from Separated/Divorced Spouse',spa.Comment_Other_Support_fr_Divorced_Spouse__c));
        if(!String.isEmpty(spa.Comment_Food_Housing_Other_Allowance__c))
            commentRows.add(new RowWrapper('12e', 'Allowances for Food, Housing, and Other Expenses',spa.Comment_Food_Housing_Other_Allowance__c));
        if(!String.isEmpty(spa.Comment_Earned_Income_Credits__c))
            commentRows.add(new RowWrapper('12f', 'Earned Income Credits',spa.Comment_Earned_Income_Credits__c));
        if(!String.isEmpty(spa.Comment_Welfare_Veterans_Workers_Comp__c))
            commentRows.add(new RowWrapper('12g', 'Welfare, Veterans, and Workers Compensation Benefits',spa.Comment_Welfare_Veterans_Workers_Comp__c));
        if(!String.isEmpty(spa.Comment_Tax_Exempt_Investment_Income__c))
            commentRows.add(new RowWrapper('12h', 'Income from Tax-Exempt Investments',spa.Comment_Tax_Exempt_Investment_Income__c));
        if(!String.isEmpty(spa.Comment_Income_Earned_Abroad__c))
            commentRows.add(new RowWrapper('12i', 'Income Earned Abroad',spa.Comment_Income_Earned_Abroad__c));
        if(!String.isEmpty(spa.Comment_Other_Untaxed_Income__c))
            commentRows.add(new RowWrapper('12j', 'Other Untaxed Income and Benefits',spa.Comment_Other_Untaxed_Income__c));
        if(!String.isEmpty(spa.Comment_Untaxed_IRA_Plan_Payment__c))
            commentRows.add(new RowWrapper('13a', 'Untaxed IRA Plan Payment',spa.Comment_Untaxed_IRA_Plan_Payment__c));
        if(!String.isEmpty(spa.Comment_SEP_and_SIMPLE_Plans__c))
            commentRows.add(new RowWrapper('13b', 'Self-employed SEP, SIMPLE, and Qualified Plan Payments',spa.Comment_SEP_and_SIMPLE_Plans__c));    
        if(!String.isEmpty(spa.Comment_Prof_Judgment_Minimum_Income_Amt__c))
            commentRows.add(new RowWrapper('PJ', ' Minimum Income Amount',spa.Comment_Prof_Judgment_Minimum_Income_Amt__c));
        if(!String.isEmpty(spa.Comment_Depreciation_Sec_179_Exp_Sch_C__c))
            commentRows.add(new RowWrapper('15a', 'Depreciation and Sec 179 Exp Sch C',spa.Comment_Depreciation_Sec_179_Exp_Sch_C__c));
        if(!String.isEmpty(spa.Comment_Depreciation_Sec_179_Exp_Sch_F__c))
            commentRows.add(new RowWrapper('15b', 'Depreciation and Sec 179 Exp Sch F',spa.Comment_Depreciation_Sec_179_Exp_Sch_F__c));
        if(!String.isEmpty(spa.Comment_Depreciation_Sec_179_Exp_4562__c))
            commentRows.add(new RowWrapper('15c', 'Sec 179 Exp IRS 4562',spa.Comment_Depreciation_Sec_179_Exp_4562__c));
        if(!String.isEmpty(spa.Comment_Depreciation_Schedule_E__c))
            commentRows.add(new RowWrapper('15e', 'Rental Property Depreciation Exp Sch E',spa.Comment_Depreciation_Schedule_E__c));
        if(!String.isEmpty(spa.Comment_Home_Bus_Expense_Sch_C__c))
            commentRows.add(new RowWrapper('15g', 'Home Bus Exp Sch C',spa.Comment_Home_Bus_Expense_Sch_C__c));
        if(!String.isEmpty(spa.Comment_Current_Tax_Return_Status__c))
            commentRows.add(new RowWrapper('18', 'Current Tax Return Status',spa.Comment_Current_Tax_Return_Status__c));
        if(!String.isEmpty(spa.Comment_Filing_Status__c))
            commentRows.add(new RowWrapper('19', 'Filing Status',spa.Comment_Filing_Status__c));
        if(!String.isEmpty(spa.Comment_Income_Tax_Exemptions__c))
            commentRows.add(new RowWrapper('20', 'Income Tax Exemptions',spa.Comment_Income_Tax_Exemptions__c));
        if(!String.isEmpty(spa.Comment_Itemized_Deductions__c))
            commentRows.add(new RowWrapper('21', 'Itemized Deductions',spa.Comment_Itemized_Deductions__c));
        if(!String.isEmpty(spa.Comment_Federal_Income_Tax_Revision__c))
            commentRows.add(new RowWrapper('22b', 'Federal Income Tax Reported/Verified',spa.Comment_Federal_Income_Tax_Revision__c));
        if(!String.isEmpty(spa.Comment_Soc_Sec_Tax_Allowance_Calc_Rev__c))
            commentRows.add(new RowWrapper('23b', 'Social Security Tax Allowance Revision/Verified',spa.Comment_Soc_Sec_Tax_Allowance_Calc_Rev__c));
        if(!String.isEmpty(spa.Comment_Med_Tax_Allowance_Calc_Revision__c))
            commentRows.add(new RowWrapper('24b', 'Medicare Tax Allowance Revision/Verified',spa.Comment_Med_Tax_Allowance_Calc_Revision__c));
        if(!String.isEmpty(spa.Comment_Total_SE_Tax_Paid__c))
            commentRows.add(new RowWrapper('25b', 'Self-Employment Tax Revision/Verified',spa.Comment_Total_SE_Tax_Paid__c));
        if(!String.isEmpty(spa.Comment_Parent_State__c))
            commentRows.add(new RowWrapper('27', 'Parent State',spa.Comment_Parent_State__c));
        if(!String.isEmpty(spa.Comment_State_Other_Tax_Allow_Revision__c))
            commentRows.add(new RowWrapper('28b', 'State and Other Tax Allowance Calculation Revision',spa.Comment_State_Other_Tax_Allow_Revision__c));
        if(!String.isEmpty(spa.Comment_Medical_Dental_Expense__c))
            commentRows.add(new RowWrapper('30', 'Medical/ Dental Expenses',spa.Comment_Medical_Dental_Expense__c));
        if(!String.isEmpty(spa.Comment_Unusual_Expense__c))
            commentRows.add(new RowWrapper('32', 'Unusual Expenses',spa.Comment_Unusual_Expense__c));
        if(!String.isEmpty(spa.Comment_Home_Purchase_Year__c))
            commentRows.add(new RowWrapper('39', 'Home Purchase Year',spa.Comment_Home_Purchase_Year__c));
        if(!String.isEmpty(spa.Comment_Home_Purchase_Price__c))
            commentRows.add(new RowWrapper('40', 'Home Purchase Price',spa.Comment_Home_Purchase_Price__c));
        if(!String.isEmpty(spa.Comment_Home_Market_Value__c))
            commentRows.add(new RowWrapper('42a', 'Home Market Value or HIM Home Value',spa.Comment_Home_Market_Value__c));
        if(!String.isEmpty(spa.Comment_Unpaid_Principal_1st_Mortgage__c))
            commentRows.add(new RowWrapper('42b', 'Unpaid Principal 1st Mortgage',spa.Comment_Unpaid_Principal_1st_Mortgage__c));
        if(!String.isEmpty(spa.Comment_Unpaid_Principal_2nd_Mortgage__c))
            commentRows.add(new RowWrapper('42c', 'Unpaid Principal 2nd Mortgage',spa.Comment_Unpaid_Principal_2nd_Mortgage__c));
        if(!String.isEmpty(spa.Comment_Other_Real_Estate_Market_Value__c))
            commentRows.add(new RowWrapper('44a', 'Total Market Value',spa.Comment_Other_Real_Estate_Market_Value__c));
        if(!String.isEmpty(spa.Comment_Other_Real_Estate_Unpaid_Princip__c))
            commentRows.add(new RowWrapper('44b', 'Total Unpaid Principal',spa.Comment_Other_Real_Estate_Unpaid_Princip__c));

        

        if(!String.isEmpty(spa.Comment_Impute_Rate__c))
            commentRows.add(new RowWrapper('PJ', 'Impute %',spa.Comment_Impute_Rate__c));
        if(!String.isEmpty(spa.Comment_Bank_Accounts__c))
            commentRows.add(new RowWrapper('49', 'Bank Accounts',spa.Comment_Bank_Accounts__c));
        if(!String.isEmpty(spa.Comment_Investments__c))
            commentRows.add(new RowWrapper('50', 'Other Investments',spa.Comment_Investments__c));
        if(!String.isEmpty(spa.Comment_Total_Debts__c))
            commentRows.add(new RowWrapper('52', 'Total Debts',spa.Comment_Total_Debts__c));
        if(!String.isEmpty(spa.Comment_COLA_Value__c))
            commentRows.add(new RowWrapper('58', 'COLA Value',spa.Comment_COLA_Value__c));
        if(!String.isEmpty(spa.Comment_Family_Size__c))
            commentRows.add(new RowWrapper('60', 'Family Size',spa.Comment_Family_Size__c));
        if(!String.isEmpty(spa.Comment_Dependent_Children__c))
            commentRows.add(new RowWrapper('60b', 'Number of Non-Applicant Dependents',spa.Comment_Dependent_Children__c));
        if(!String.isEmpty(spa.Comment_Num_Children_in_Tuition_Schools__c))
            commentRows.add(new RowWrapper('65', 'Number of Children in Tuition - Charging Schools',spa.Comment_Num_Children_in_Tuition_Schools__c));
        if(!String.isEmpty(spa.Comment_Student_Assets__c))
            commentRows.add(new RowWrapper('67', 'Student Assets',spa.Comment_Student_Assets__c));
        

        // BIZ FARM LOGIC : rows are from subqueries to business_farm__c, determine if its biz/farm by type
        
        for(School_Biz_Farm_Assignment__c each : businesses){
            if(!String.isEmpty(each.Comment_Net_Profit_Loss_Business_Farm__c))
            bizFarmRows.add(new RowWrapper('5', 'Net Profit/Loss - Business : '+each.Business_Farm__r.name,each.Comment_Net_Profit_Loss_Business_Farm__c));
            
            if(!String.isEmpty(each.Comment_Business_Farm_Owner__c))
            bizFarmRows.add(new RowWrapper('45', 'Business Owner: '+each.Business_Farm__r.name,each.Comment_Business_Farm_Owner__c));
            
            if(!String.isEmpty(each.Comment_Business_Farm_Ownership_Perc__c))
            bizFarmRows.add(new RowWrapper('46a', 'Business Ownership Percent: '+each.Business_Farm__r.name,each.Comment_Business_Farm_Ownership_Perc__c));
        
            
            if(!String.isEmpty(each.Comment_Business_Farm_Assets__c))
            bizFarmRows.add(new RowWrapper('46b', 'Business Assets: '+each.Business_Farm__r.name,each.Comment_Business_Farm_Assets__c));
        

            if(!String.isEmpty(each.Comment_Business_Farm_Debts__c))
            bizFarmRows.add(new RowWrapper('46c', 'Business Debts: '+each.Business_Farm__r.name,each.Comment_Business_Farm_Debts__c));
        
        }
        for(School_Biz_Farm_Assignment__c each : farms){
            if(!String.isEmpty(each.Comment_Net_Profit_Loss_Business_Farm__c))
            bizFarmRows.add(new RowWrapper('6', 'Net Profit/Loss - Farm : '+each.Business_Farm__r.name,each.Comment_Net_Profit_Loss_Business_Farm__c));
        
            if(!String.isEmpty(each.Comment_Business_Farm_Owner__c))
            bizFarmRows.add(new RowWrapper('45', 'Farm Owner Farm: '+each.Business_Farm__r.name,each.Comment_Business_Farm_Owner__c));
    
            if(!String.isEmpty(each.Comment_Business_Farm_Ownership_Perc__c))
            bizFarmRows.add(new RowWrapper('46a', 'Farm Ownership Percent: '+each.Business_Farm__r.name,each.Comment_Business_Farm_Ownership_Perc__c));
        
            if(!String.isEmpty(each.Comment_Business_Farm_Assets__c))
            bizFarmRows.add(new RowWrapper('46b', 'Farm Assets: '+each.Business_Farm__r.name,each.Comment_Business_Farm_Assets__c));

            if(!String.isEmpty(each.Comment_Business_Farm_Debts__c))
            bizFarmRows.add(new RowWrapper('46c', 'Farm Debts: '+each.Business_Farm__r.name,each.Comment_Business_Farm_Debts__c));
        
        }
        

    }

    public list<RowWrapper> commentRows {get;set;}
    public list<RowWrapper> bizFarmRows {get;set;}
    public class RowWrapper{
        public string rowNumber {get;set;}
        public string description {get;set;}
        public string comment {get;set;}
        public rowWrapper(string r, string d, string c){
            rowNumber = r;
            description = d;
            comment = c;
        }
    }
}