public with sharing class SchoolNonNeedBasedController  implements SchoolAcademicYearSelectorInterface{
    
    public Contact searchContact {get; set;}
    public List<selectableSObject> searchResults {get; set;}
    
    public SchoolNonNeedBasedController Me {
        get { return this; }
    }
    
    // Constructor
    public SchoolNonNeedBasedController(){
        searchContact = new Contact();
        searchResults = new List<selectableSObject>();
    }
    
    // Action Methods
    public PageReference searchContacts(){
        
        Map<Id, Student_Folder__c> queryResultsMap = new Map<Id, Student_Folder__c>();

        // Create the initial list of SelectableSObject records for the results table
        for(Student_Folder__c folderRecord : [select Id, Student__c, Folder_Source__c, First_Name__c, Last_Name__c, 
                                                    Birthdate__c, Academic_Year_Picklist__c, PFS1_Parent_A__c, 
                                                    Primary_Parent__r.Name, Primary_Parent__r.MailingStreet, Primary_Parent__r.MailingCity, Primary_Parent__r.MailingState  
                                                    from Student_Folder__c 
                                                    where First_Name__c = :searchContact.FirstName 
												        and Last_Name__c = :searchContact.LastName
												        and Birthdate__c = :searchContact.Birthdate 
                                                    order by First_Name__c asc, Academic_Year_Picklist__c desc])
        {
            // Only want to take the most recent Academic Year's record
            if(queryResultsMap != null && !queryResultsMap.keySet().contains(folderRecord.Student__c)){
                queryResultsMap.put(folderRecord.Student__c, folderRecord);
            }    
        }
                                                                                                
        searchResults = SelectableSObject.createList(queryResultsMap.values());
        
        // Collect the list of referenced Contact records
        Set<Id> referencedContacts = new Set<Id>{};
        for(SelectableSObject resultRow : searchResults){
            referencedContacts.add(resultRow.folder.Student__c);
            referencedContacts.add(resultRow.folder.Primary_Parent__c);
        }
        
        // Query Contact records with associated Applicants and PFS records 
        Map<Id, Contact> contactMap = new Map<Id, Contact>([select Id, Name, MailingStreet, MailingCity, MailingState, 
                                                                (select Id, PFS__r.Parent_A__r.Name, PFS__r.Parent_A__r.MailingStreet, 
                                                                    PFS__r.Parent_A__r.MailingCity, PFS__r.Parent_A__r.MailingState 
                                                                    from Applicants__r 
                                                                    order by PFS__r.CreatedDate desc)
                                                                from Contact 
                                                                where Id in :referencedContacts]);
        
        // Iterate through the set of Student Folders matching search criteria
        //  and try to fill in Parent-related information
        for(SelectableSObject resultRow : searchResults){
            // Look up the referenced Contact
            if(resultRow.folder.Primary_Parent__c != null){
                resultRow.stringValue1 = resultRow.folder.Primary_Parent__r.Name;
                resultRow.stringValue2 = formatAddress(resultRow.folder.Primary_Parent__r.MailingStreet,resultRow.folder.Primary_Parent__r.MailingCity,resultRow.folder.Primary_Parent__r.MailingState);
            }
            else{
                Contact referencedContact = contactMap.get(resultRow.folder.Student__c);
            
                if(referencedContact != null && referencedContact.Applicants__r != null && referencedContact.Applicants__r.size() > 0){
                    Applicant__c appRecord = referencedContact.Applicants__r[0]; 
                    resultRow.stringValue1 = appRecord.PFS__r.Parent_A__r.Name;
                    resultRow.stringValue2 = formatAddress(appRecord.PFS__r.Parent_A__r.MailingStreet,appRecord.PFS__r.Parent_A__r.MailingCity,appRecord.PFS__r.Parent_A__r.MailingState);
                }
            }
        }
        
        if(searchResults != null && searchResults.size() > 0){
            return null;    
        }
        else{
            return goToNewFolder();
        }
        
    }
    
    public PageReference startAgain(){
        searchContact = new Contact();
        searchResults = null;
        
        return null;
    }
    
    public PageReference goToNewFolder(){
        return new PageReference('/apex/SchoolFinancialAid?firstname=' + searchContact.FirstName + 
                                    '&lastname=' + searchContact.LastName + 
                                    '&birthdate=' + searchContact.Birthdate);
    }
    
    private String formatAddress(String street, String city, String state){
        String returnAddress = '';
        
        if(street != null){
            returnAddress += street;
            
            if(city != null || state != null){
                returnAddress += ', ';
            }
        } 
        
        if(city != null){ 
            returnAddress += city;
            
            if(state != null){
                returnAddress += ', ';
            }
        }
        
        if(state != null){
            returnAddress += state;
        }
        
        return returnAddress;
        
    }
    
    /* Academic Year Selector implementation */
    public String newAcademicYearId;
    public String getAcademicYearId() {return null;}
    public void setAcademicYearId(String academicYearId) {}
    public PageReference SchoolAcademicYearSelector_OnChange(Boolean saveRecord) {return null;}    
    
}