public with sharing class IntegrationPFS {

    private PFS__c pfs;
    private string source;
    private List<CalloutDetails> calloutLogs;

    public IntegrationPFS(PFS__c pfs, string source) {
        this.pfs = pfs;
        this.source = source;
        this.calloutLogs = new List<CalloutDetails>();
    }

    private without sharing class AccountChecker{
        private IntegrationPFS ipfs;
        set<string> schoolIds;
        set<string> schoolSFIds;
        public map<string,string> schoolIdToName;


        private AccountChecker(IntegrationPFS ipfs){
            this.ipfs = ipfs;
            schoolIds = ipfs.schoolIds;
            schoolIdToName = new map<string,string>();
        }

        private set<string> getSchoolIdSet(){
            set<string> schoolSet = new set<string>();
            schoolSFIds = new set<string>();

            string schoolIdMapping;
            for(IntegrationMapping__mdt  each : ipfs.getsourceMappings().get('Student Applications'))
                if(each.DataType__c == 'LOOKUP' && each.TargetObject__c =='Student_Folder__c'){
                    schoolIdMapping = each.TargetField__c.split(':')[1].split(' ')[1];
                    break;
                }

            system.debug(schoolIds);
            List<String> schoolIdList = new List<String>(schoolIds);
            string q = 'SELECT id, name, '+schoolIdMapping +' FROM Account WHERE '+ schoolIdMapping+' IN :schoolIdList AND SSS_Subscriber_Status__C = \'Current\'';
            for(sObject each : Database.query(q)){
                // Check to make sure there are Annual Settings on the school : Creating annual settings if they do not exist
                GlobalVariables.getCurrentAnnualSettings(true, null, GlobalVariables.getAcademicYearByName(ipfs.pfs.Academic_Year_Picklist__c).id, (id)each.get('id')); 
                schoolSet.add((string)each.get('IntegrationId__c'));
                schoolSFIds.add((id)Each.get('id'));
                schoolIdToName.put((string)each.get(schoolIdMapping), (string)each.get('name'));
            }
            return schoolSet;
        }
    }

    public without sharing class LookupHelper{
        public list<sObject> lookupRecords(list<sobject> records, IntegrationMapping__mdt mapping){
            list<string> fields = mapping.TargetField__c.split(':');
            string targetField = fields[1].split(' ')[1];
            string targetObject = fields[1].split(' ')[0];
            

            list<string>externalIds = new list<string>();
            for(sObject each : records){
                externalIds.add((string)each.get(fields[0]));
            }

            // find potential lookup matches by searching on the key
            string q = 'SELECT id,'+targetfield+' FROM '+targetObject+' WHERE '+targetField+' IN:externalIds';
            map<string,SObject> externalIdToRecords = new map<string,SObject>();
            for(sObject each : database.query(q)){
                externalIdToRecords.put((string)each.get(targetField), each);
            }

            // take the temporary value in the lookup field to try to get the id of the record, and use it. if none, null so no lookup value
            for(sObject each : records){
                sObject lookup = externalIdToRecords.get((string)each.get(fields[0]));
                if(lookup !=null){
                    each.put(fields[0],lookup.id);
                }
                else{
                    each.put(fields[0],null);
                }
            }

            return records;
        }
    }

    public without sharing class StudentFinder{

        public list<Student_Folder__c> findStudents(list<string> firstNameList, list<string> lastNameList, list<date> BirthDateList,list<Student_Folder__c> folders, string folderIdMapping, map<string,string> folderNameKeyMap){
            // trying to find a contact to lookup to for folders : can't use generic lookup processing because using compound key
            map<string,string> contactNameKeyMap = new map<string,string>();
            for(contact each : [SELECT id,firstname,lastname,birthdate FROM Contact
                                                 WHERE firstname IN :firstNameList
                                                AND lastname IN :LastNameList
                                                AND birthdate IN :BirthDateList
                                                ORDER BY CreatedDate DESC]
                                            )
                contactNameKeyMap.put(String.format('{0}%{1}%{2}', new list<string>{each.FirstName, each.LastName, String.valueOf(Date.newInstance(each.birthdate.year(),each.birthdate.month(),each.birthdate.day()))}), each.id);
            

            for(sObject each : folders){
                string nameKey = folderNameKeyMap.get((string)each.get(folderIdMapping));
                each.put('Student__c',contactNameKeyMap.get(nameKey));
            }

            return folders;
        }

        public list<Applicant__c> findApplicants(list<string> ravennaIdList, id pfsId){

            return [SELECT id,first_name__c,last_name__c, birthdate_new__c, RavennaId__c 
                                    FROM Applicant__c 
                                    WHERE (RavennaId__c IN :RavennaIdList 
                                    OR RavennaId__c = null)
                                    AND PFS__c =: pfsid];
        }


    }

    // OpenId Authentication
    // get access token based on auth provider
    private string getToken(){

        list<AuthProvider> auths = [SELECT id FROM AuthPRovider WHERE DeveloperName =:source];
        if(!auths.isEmpty())
            return Auth.AuthToken.getAccessToken(auths[0].id,getauthSourceToType().get(source));
        else
            return null;
    }
    private map<string,string> getAuthSourceToType(){
        return new map<string,string>{'ravenna'=>'Open ID Connect'};
    }

    private set<string> schoolIds;

    public void processRavenna(){
        string ravennaBaseURL;
        if(!Test.isRunningTest()){
             ravennaBaseURL = IntegrationSource__c.getInstance('ravenna').baseCalloutURL__c;
        }
        else{
             ravennaBaseURL = 'ravenna';
        }
        
        StudentFinder sf = new StudentFinder();

        // Ravenna handles admission years differently from SSS, we must subtract one
        list<string> admissionYears = PFS.Academic_Year_Picklist__c.split('-');
        integer yearStart = integer.valueof(admissionYears[0])-1;
        integer yearEnd = integer.valueof(admissionYears[1])-1;
        string admissionYear = String.valueOf(yearStart+'-'+yearEnd);


        // GET STUDENT DATA
        string students = callout(getToken(), 'GET', ravennaBaseUrL+ '/api/v1/students/',null);
        list<object> response = (list<object>)JSON.deserializeUntyped(students);

        // GENERIC PROCESSING
        list<sObject> applicants = processRecords(response, 'Applicant__c', 'Students');
        
        // CUSTOM POST-PROCESSING
        //associate applicant with pfs
        for(sObject each : applicants)
            each.put('PFS__c',pfs.id);
            


        map<string,Applicant__c> studentIdToApplicant = new map<string,Applicant__c>();

        // find Id type target field mapped to on student
        // must be unique to the student to associate downstream with spas and folders
        string idMapping;
        for(IntegrationMapping__mdt  each : getsourceMappings().get('Students'))
            if(each.DataType__c == 'ID' && each.TargetObject__c =='Applicant__c'){
                idMapping = each.Targetfield__c;
                break;
            }

        list<string> firstNameList = new list<String>();
        list<string> lastNameList = new list<String>();
        list<date> birthDateList = new list<Date>();
        list<string> ravennaIdList = new list<String>();

        //%map<list<string>,string> compoundKeyToId = new map<list<string>,string>();
        map<string,string> compoundKeyToId = new map<string,string>();
        if(!string.isEmpty(idmapping)){
            for(sObject each : applicants){
                //system.debug(each+(string)each.get(idmapping));
                string t = String.format('{0}%{1}%{2}', new list<string>{(string)each.get('first_name__c'), (string)each.get('last_name__c'), string.valueOf(each.get('birthdate_new__c'))});
                //%list<string> t  = new list<string>{(string)each.get('first_name__c'), (string)each.get('last_name__c'), string.valueOf(each.get('birthdate_new__c'))};
                firstNameList.add((string)each.get('first_name__c'));
                lastNameList.add((string)each.get('last_name__c'));
                birthDateList.add((date)each.get('birthdate_new__c'));
                ravennaIdList.add((string)each.get('ravennaid__c'));
                // the compoundKey will be used to get the ravenna id from first/last/bday later
                compoundKeyToId.put(t, (string)each.get(idMapping));
                // to be used for parenting child records to these applicants
                studentIdToApplicant.put((string)each.get(idMapping), (applicant__c)each);
            }

            for(Applicant__c each : sf.findApplicants(ravennaIdList, pfs.id) ){
                list<string> t  =  new list<string>{(string)each.get('first_name__c'), (string)each.get('last_name__c'), string.valueOf(each.get('birthdate_new__c'))};
                
                if(studentIdToApplicant.get(each.RavennaId__c) != null){
                    // if there are applicants on this years pfs with matching ravenna ids, get those ids
                    studentIdToApplicant.get(each.RavennaId__c).id = each.id;
                }
                else if(compoundKeyToId.containsKey(String.format('{0}%{1}%{2}',t))){
                    // if there are applicants on this years pfs already, get those ids
                    studentIdToApplicant.get(compoundKeyToId.get(String.format('{0}%{1}%{2}',t))).id = each.id;
                }
            
            }
        }
            
            // else, create new applicants on this years pfs (normal upsert)



        map<string,list<object>> studentIdToResponse = new map<string,list<object>>();

        list<Student_Folder__c> folders = new list<Student_Folder__c>();
        list<School_PFS_Assignment__c> spas = new list<School_PFS_Assignment__c>();

        map<string,string> applicantIdToSchoolId = new map<String,string>();
        map<string,list<string>> studentIdToSchoolId = new map<String,list<string>>();
        schoolIds = new set<string>();

        map<string,string> studentIdToGrade = new map<string,string>();

            //gather nces_ids and add them into the response objects
        map<string,string> schoolIdToNCESId = new map<string,string>();


        // matching on school should use nces_id and if null then school id. 
        //this should correspond in sss to targeting the integrationid__c field (set in integration mappings), 
        //a formula representing :
        // the nces_id__c field and if null then ravennaid__c  

        string schoolId;
        Map<String,object> o2;
        Map<String,object> o;

        // Starting Folder/SPA processing
        for(string eachId : studentIdToApplicant.keyset()){
            studentIdToSchoolId.put(eachId,new list<string>());
            // add student_id back in for lookup matching 
            list<object> apps = new list<object>();
            // looping through students and calling out for applicants for each, processing each right away but DML later
            for(object each : (list<object>)JSON.deserializeUntyped(callout(getToken(), 'GET', ravennaBaseUrL+'/api/v1/students/'+eachId+'/applications?admission_year='+admissionYear,null))){
                o = (map<string,object>)each;
                // If the map is empty that is because the applicant has applied to previous years in Ravenna, but has
                // not applied to any schools for this year. For example, a student who graduated last year.
                if(o.isEmpty()) {
                    continue;
                }
                if(!schoolIdToNCESId.containsKey((string)o.get('school_id'))){
                     o2 = (map<string,object>)JSON.deserializeUntyped(callout(getToken(), 'GET', ravennaBaseUrL+'/api/v1/schools/'+(string)o.get('school_id'),null));
                    schoolIdToNCESId.put(string.valueOf(o.get('school_id')),(string)o2.get('nces_id'));
                }
                
                
                schoolId = schoolIdToNCESId.get((string)o.get('school_id')) != null ? schoolIdToNCESId.get((string)o.get('school_id')) : (string)o.get('school_id');
                o.put('school_id',schoolId);
                applicantIdToSchoolId.put((string)o.get('id'),schoolId);
                studentIdToSchoolId.get(eachId).add(schoolId);
                schoolIds.add(schoolId);

                o.put('student_id',eachId);
                
                apps.add((object)o);
                
                studentIdToGrade.put(eachId,mapPicklist('apply_grade',(string)o.get('apply_grade')));
                
            }
            studentIdToResponse.put(eachId, apps);
        }

        system.debug('**schoolidToNCESid'+schoolIdToNCESId.keyset().size()+' '+schoolIdToNCESId);

        // We have not found any applicants in Ravenna who have applied to any school in SSS for the current
        // Academic Year.
        if(applicantIdToSchoolId.isEmpty()) {
            throw new IntegrationException('Unable to find applicant data for the selected year.');
        }

        // callouts complete so now DML is allowed
        
        // check for matching schools :
        // if there is no matching school, do not create applicants/spa/folders

        AccountChecker ac = new AccountChecker(this);
        set<string> schoolSet = ac.getSchoolIdSet();

        system.debug(studentIdToApplicant);

        boolean hasSchool;
        for(string each : studentIdToApplicant.keyset()){
            studentIdToApplicant.get(each).Grade_In_Entry_Year__c = studentIdToGrade.get(each);
            hasSchool = false;
            for(string eachSchool : studentIdToSchoolId.get(each)){
                if(schoolSet.contains(eachSchool)){
                    hasSchool = true;
                    break;
                }
            }
            // if ANY of the schools of an applicant exist in SSS, create the applicant. 
            if(!hasSchool){
                studentIdToApplicant.remove(each);
            }

        }
    
        system.debug(studentIdToApplicant);
        // Upsert Applicants
        Type appType = Type.forName('list<Applicant__c>');
        list<sObject> genApplicants = (list<sObject>) appType.newInstance();

        set<id> appIds = new set<id>();
        for(sObject each : studentIdToApplicant.values()){
            if(!appIds.contains((Id)each.get('Id')))
                    genApplicants.add(each);
            if((Id)each.get('Id') != null)
                appIds.add((Id)each.get('Id'));
        }
            
        

        DML.WithoutSharing.upsertObjects(genApplicants);

        // Post-Processing for Student Folders and School PFS Assignments

        // get the unique id fields for this object, must be present
        string folderIdMapping;
        string spaIdMapping;
        for(IntegrationMapping__mdt  each : getsourceMappings().get('Student Applications'))
            if(each.DataType__c == 'ID' && each.TargetObject__c =='Student_Folder__c'){
                folderidMapping = each.Targetfield__c;
            }
            else if(each.DataType__c == 'ID' && each.TargetObject__c =='School_PFS_Assignment__c'){
                spaIdMapping = each.TargetField__c;
            }

        firstNameList = new list<String>();
        lastNameList = new list<String>();
        birthDateList = new list<Date>();

        // id of the folder to name key
        map<string,string> folderNameKeyMap = new map<string,string>();
        for(string eachId : studentIdToApplicant.keyset()){
            // CUSTOM PROCESSING
            for(sObject each : processRecords(studentIdToResponse.get(eachId), 'Student_Folder__c','Student Applications')){
                // custom name concactenation
                each.put('name', studentIdToApplicant.get(eachId).first_name__c +' '+ studentIdToApplicant.get(eachId).last_name__c+' - '+ ac.schoolIdToName.get(applicantIdToSchoolId.get((string)each.get(folderIdMapping))));
                each.put('Academic_Year_Picklist__c', pfs.Academic_Year_Picklist__c);
                // lookup to contact (contact should be same over many applicants)
                firstNameList.add(studentIdToApplicant.get(eachId).first_name__c);
                lastNameList.add(studentIdToApplicant.get(eachId).last_name__c);
                birthDateList.add(studentIdToApplicant.get(eachId).birthdate_new__c);
                // compound key for matching in lookups
                string nameKey = String.format('{0}%{1}%{2}', new list<string>{studentIdToApplicant.get(eachId).first_name__c, studentIdToApplicant.get(eachId).last_name__c, string.valueOf(studentIdToApplicant.get(eachId).birthdate_new__c) });
                system.debug(nameKey);
                // temporarily assign an invalid id (compound key) to the lookup, which will be used to get the correct id for that compound key
                each.put('Student__c', nameKey);
                folderNameKeyMap.put((string)each.get(folderIdMapping), namekey);
                folders.add((Student_Folder__c)each);
            }
        }

        // CUSTOM POST-PROCESSING
        
        folders = sf.findStudents(firstNameList, lastNameList, BirthDateList, folders, folderIdMapping, folderNameKeyMap);

        Schema.SObjectField folderField = Schema.getGlobalDescribe().get('Student_Folder__c').getDescribe().fields.getMap().get(folderIdMapping);
        
        list<Student_Folder__c> foldersToUpsert = new list<Student_Folder__c>();
        for(Student_Folder__c each : folders){
            if(each.School__c != null && ac.schoolSFIds.contains(each.school__c)){
                each.put('OwnerId', GlobalVariables.getIndyOwner());
                foldersToUpsert.add(each);
            }
        }

        Type folderType = Type.forName('list<Student_Folder__c>');
        list<sObject> genFolders = (list<sObject>) folderType.newInstance();
        system.debug(foldersToUpsert);
        genFolders.addAll((list<sObject>)foldersToUpsert);
        system.debug(genFolders);
        DML.WithoutSharing.upsertObjects(genFolders,folderField);

        for(string eachId : studentIdToApplicant.keyset()){
            for(sObject each : processRecords(studentIdToResponse.get(eachId), 'School_PFS_Assignment__c','Student Applications')){
                each.put('name', studentIdToApplicant.get(eachId).first_name__c +' '+ studentIdToApplicant.get(eachId).last_name__c+' - '+ ac.schoolIdToName.get(applicantIdToSchoolId.get((string)each.get(spaIdMapping))));
                each.put('Applicant__c', studentIdToApplicant.get(eachId).id);
                // specify the school academic year picklist
                each.put('Academic_Year_Picklist__c',pfs.Academic_Year_Picklist__c);
                spas.add((School_PFS_Assignment__c)each);
            }
        }

        list<School_PFS_Assignment__c> spasToUpsert = new list<School_PFS_Assignment__c>();
        for(School_PFS_Assignment__c each : spas){
            if(each.School__c != null && ac.schoolSFIds.contains(each.school__c)){
                spasToUpsert.add(each);
            }
        }


        // DML
        Schema.SObjectField spaField = Schema.getGlobalDescribe().get('School_PFS_Assignment__c').getDescribe().fields.getMap().get(spaIdMapping);
        Type spaType = Type.forName('list<School_PFS_Assignment__c>');
        list<sObject> genSpas = (list<sObject>) spaType.newInstance();
        genSpas.addAll((list<sObject>)spasToUpsert);
        DML.WithoutSharing.upsertObjects(genSpas,spaField);
            
    }

    private map<string,list<IntegrationMapping__mdt>> sourceMappings;
    public map<string,list<IntegrationMapping__mdt>> getsourceMappings(){
        if(sourceMappings == null){
            sourcemappings = new map<string,list<IntegrationMapping__mdt>>();
            for(IntegrationMapping__mdt each : [SELECT developername, SourceField__c, SourceObject__c, TargetField__c, TargetObject__c, DataType__c FROM IntegrationMapping__mdt WHERE Source__c =:source]){
                if(each.targetfield__c != null){
                    if(sourcemappings.get(each.sourceObject__c) == null){
                        sourcemappings.put(each.sourceObject__c, new list<IntegrationMapping__mdt>{each});
                    }
                    else{
                        sourcemappings.get(each.sourceObject__c).add(each);
                    }
                }
            }
        }
        return sourcemappings;
    }

    public string callout(string token, string method, string endpoint, string body){

        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod(method);
        req.setHeader('Authorization','Bearer '+token);
        system.debug(req +' '+ token);

        if(!string.isEmpty(body)){
            req.setBody(body);
        }

        HttpResponse res;
        string response;

       
             IntegrationPFSMock ipfsm = new IntegrationPFSMock();

            if(!Test.isRunningTest()){
                res = h.send(req);
            }
            else{
                res = ipfsm.respond(req);
            }

            registerCallout(req, res);

            if (res.getStatusCode() == 200) {
                system.debug('***'+res.getHeaderKeys());
                for(string each : res.getHeaderKeys())
                    if(each != null)
                        system.debug('*'+res.getHeader(each));

                response = res.getBody();
                if(String.isEmpty(response)){
                       response = '[{}]';
                }
           } else {
                response = '[{}]';
           }
        return response;
    }


    // use this method if the records to process are in form {"test":"value", "responseRecorKey":[{of},{what},{you},{need}]}
    private list<sObject> processRecords(list<object> response, string targetSfObject, string sourceObject){
        map<string,list<IntegrationMapping__mdt>> mappings = getsourceMappings();

        list<sObject> sfRecords = new list<sObject>();
        object tempValue;
        for(object each : response){
            sObject sfRecord = Schema.getGlobalDescribe().get(targetSfObject).newSObject();

            map<String,object> values = (map<string,object>)each;
            for(IntegrationMapping__mdt mapping:mappings.get(sourceObject)){
                // is this a compound source?
                tempValue = null;
                if(mapping.sourceField__c.contains(',')){
                    for(string eachSource : mapping.sourceField__c.split(',')){
                        if(values.get(eachSource) != null){
                            tempValue = values.get(eachSource);
                            break; 
                        }
                    }
                }else
                    tempValue = values.get(mapping.sourceField__c);
                    
                system.debug(mapping);
                // there may be source objects mapped to multiple sf objects, processed separately
                if(mapping.TargetObject__c == targetSfObject)
                    if(tempValue!=null)
                        if(mapping.DataType__c == 'LOOKUP'){
                            sfRecord.put(mapping.targetField__c.split(':')[0], tempValue);
                        }
                        // if this is an id, treat it as an external id from a spefic external system : prepend this source
                        else if(mapping.DataType__c == 'ID'){
                            sfRecord.put(mapping.targetField__c, string.valueOF(tempValue));
                        }
                        else if(mapping.DataType__c =='NUMBER'){

                        }
                        else if(mapping.DataType__c =='BOOLEAN'){

                        }
                        else if(mapping.DataType__c == 'PICKLIST'){
                            string mappedValue = mapPicklist(mapping.sourceField__c, string.valueOf(tempValue) );
                            sfRecord.put(mapping.targetField__c, mappedValue);
                        }
                        else if(mapping.DataType__c == 'DATE'){
                            string v = (string)tempValue;
                            date d = Date.newinstance(Integer.valueOf(v.split('-')[0]),Integer.valueOf(v.split('-')[1]),Integer.valueOf(v.split('-')[2]));
                            sfRecord.put(mapping.targetField__c, d) ;
                        }
                        else{
                            sfRecord.put(mapping.targetField__c, tempValue);
                        }
            
            }
            sfRecords.add(sfRecord);
        }


        // do lookups if necessary
        for(IntegrationMapping__mdt mapping : mappings.get(sourceObject))
            if(mapping.DataType__c == 'LOOKUP' && mapping.TargetObject__c == targetSfObject){
                LookupHelper lh = new LookupHelper();
                sfRecords = lh.lookupRecords(sfRecords, mapping);
            }
        
        system.debug(sfRecords);

        return sfRecords;

    }

    // this method will swap a source picklist value for a target valid picklist value
    // this method must be manually updated for each field
    private string mapPicklist(string fieldName, string value){
        if(fieldName == 'apply_grade' || fieldName == 'current_grade'){
            if(value == 'PS'){
                return 'Preschool';
            }
            else if(value == 'PK'){
                return 'Pre-Kindergarten';
            }
            else if(value == 'K'){
                return 'Kindergarten';
            }
            else if(value == 'PG'){
                return 'Post Graduation';
            }
        }

        // no match, so just return the value
        return value;

    }

    public class integrationException extends Exception{}

    private void registerCallout(HttpRequest latestRequest, HttpResponse latestResponse) {
        CalloutDetails details = new CalloutDetails(latestRequest, latestResponse);
        this.calloutLogs.add(details);
    }

    private CalloutDetails getLatestCallout() {
        if (this.calloutLogs == null || this.calloutLogs.isEmpty()) {
            return null;
        }

        Integer numberOfCallouts = this.calloutLogs.size();

        return this.calloutLogs[numberOfCallouts - 1];
    }

    /**
     * @description Inserts a new IntegrationLog record with the details of the specified exception along with details
     *              from the last callout made by this class if available.
     * @param calloutException The exception to log.
     */
    public void logException(Exception calloutException) {
        if (calloutException == null) {
            return;
        }


        IntegrationLog__c newLog = new IntegrationLog__c();
        newLog.Body__c = 'Line: ' + calloutException.getLineNumber() + ' Error: '+ calloutException.getMessage();
        newLog.Source__c = 'Ravenna';
        CalloutDetails details = getLatestCallout();
        if (details != null) {
            newLog.Endpoint__c = details.Endpoint;
            newLog.Raw_Request__c = details.RawRequest;
            newLog.Raw_Response__c = details.RawResponse;
        }

        insert newLog;
    }

    /**
     * @description Contains the details of a request and a response along with the endpoint that was used.
     */
    public class CalloutDetails {

        private HttpRequest request;
        private HttpResponse response;

        /**
         * @description Constructor for an Http request and response.
         * @param calloutRequest The request.
         * @param calloutResponse The response.
         */
        public CalloutDetails(HttpRequest calloutRequest, HttpResponse calloutResponse) {
            this.request = calloutRequest;
            this.response = calloutResponse;
        }

        /**
         * @description Gets the endpoint from the request.
         */
        public String Endpoint {
            get {
                return this.request == null ? '' : this.request.getEndpoint();
            }
        }

        /**
         * @description The entire raw request.
         */
        public String RawRequest {
            get {
                return this.request == null ? '' : this.request.toString();
            }
        }

        /**
         * @description The entire raw response.
         */
        public String RawResponse {
            get {
                return this.response == null ? '' : this.response.toString();
            }
        }
    }
}