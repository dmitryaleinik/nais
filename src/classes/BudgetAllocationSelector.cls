/**
 * @description This class is responsible for querying Budget Allocation records.
 */
public class BudgetAllocationSelector extends fflib_SObjectSelector {
    
    @testVisible private static final String ID_SET_PARAM = 'idSet';
    
    /**
     * @description Default constructor for an instance of the BudgetAllocationSelector that will not include 
     *              field sets while enforcing FLS and CRUD.
     */
    public BudgetAllocationSelector() { }

    /**
     * @description Queries Budget Allocation records by Id.
     * @param idSet The Ids of the Budget Allocation records to query.
     * @return A list of Budget Allocation records.
     * @throws ArgumentNullException if idSet is null.
     */
    public List<Budget_Allocation__c> selectById(Set<Id> idSet) {
        ArgumentNullException.throwIfNull(idSet, ID_SET_PARAM);

        return (List<Budget_Allocation__c>)super.selectSObjectsById(idSet);
    }

    private Schema.SObjectType getSObjectType() {
        return Budget_Allocation__c.SObjectType;
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                Budget_Allocation__c.Name,
                Budget_Allocation__c.Amount_Allocated__c,
                Budget_Allocation__c.Budget_Group__c,
                Budget_Allocation__c.Status__c,
                Budget_Allocation__c.Student_Folder__c
        };
    }

    /**
     * @description Creates a new instance of the BudgetAllocationSelector.
     * @return A BudgetAllocationSelector.
     */
    public static BudgetAllocationSelector newInstance() {
        return new BudgetAllocationSelector();
    }
}