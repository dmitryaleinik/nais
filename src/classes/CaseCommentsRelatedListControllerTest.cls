@isTest
private class CaseCommentsRelatedListControllerTest {

    private static void CreateData() {
        // create test data
        TestUtils.createAcademicYears();
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        
        Profile portalProfile = [select Id from Profile where Name = :ProfileSettings.FamilyProfileName LIMIT 1];
        Id portalProfileId = portalProfile.Id;

        Account account1 = TestUtils.createAccount('individual', RecordTypes.individualAccountTypeId, 3, true);
        Contact parentA = TestUtils.createContact('Parent A', account1.Id, RecordTypes.parentContactTypeId, true);
        
        TestUtils.createPortalUser('User 1', 'u1@test.org', 'u1', parentA.Id, portalProfileId, true, true);
        
        TestUtils.createPFS('PFS A', academicYearId, parentA.Id, true);
        
        TestUtils.createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_SINGLE, true);

        TestUtils.createStateTaxTable(academicYearId, true);
        
        TestUtils.createEmploymentAllowance(academicYearId, true);
        
        TestUtils.createBusinessFarm(academicYearId, true);

        TestUtils.createRetirementAllowance(academicYearId, true);
        
        TestUtils.createAssetProgressivity(academicYearId, true);
        
        TestUtils.createIncomeProtectionAllowance(academicYearId, true);

        TestUtils.createExpectedContributionRate(academicYearId, true);

        TestUtils.createHousingIndexMultiplier(academicYearId, true);
    }
    
    @isTest
    private static void TestLoad()
    {
        CaseCommentsRelatedListControllerTest.CreateData();
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        User u = [SELECT Id, profileId, username, email, contactId FROM User LIMIT 1];
        PFS__c pfs = [SELECT Id, Name FROM PFS__c LIMIT 1];
        
        Test.startTest();
        
        Case c = new Case(subject = 'Test', description = 'Test', contactId = u.ContactId, RecordTypeId = RecordTypes.familyPortalCaseTypeId);
        insert c;
        
        CaseComment cc = new CaseComment(parentid = c.Id, commentBody = 'test', IsPublished = true);
        insert cc;

        CaseCommentsRelatedListController controller = new CaseCommentsRelatedListController();
        controller.caseId = c.Id;
        
        System.assertEquals(1, controller.comments.size());
        System.assertNotEquals(null, controller.NewComment());
        
        Test.stopTest();
    }
}