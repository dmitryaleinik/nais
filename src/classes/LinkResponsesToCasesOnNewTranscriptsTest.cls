@IsTest
private class  LinkResponsesToCasesOnNewTranscriptsTest
{

    @isTest
    private static void runTests()
    {
    	FeatureToggles.setToggle('Link_Survey_Responses_To_Cases__c', true);
        String chatKey = '00001';

        //Creating the Feedback Survey Response
        User feedbackUser = UserTestData.Instance.DefaultUser;
        Feedback_Survey_Response__c f = new Feedback_Survey_Response__c (Name = 'Test', OwnerID = feedbackUser.Id, ChatKey__c = chatKey);
        insert f;

        Case newCase = CaseTestData.Instance.DefaultCase;
        //Creating the LiveAgentTranscript
        LiveChatVisitor liveChatVisitor = new LiveChatVisitor();
        insert liveChatVisitor;

        LiveChatTranscript l = new LiveChatTranscript (CaseID = newCase.Id, ChatKey = chatKey, LiveChatVisitorID = liveChatVisitor.Id);
        insert l;

        f = [SELECT Id, Name, OwnerID, ChatKey__c, Related_Case__c FROM Feedback_Survey_Response__c  WHERE Id = :f.Id];

        //Updating the Address__c with a new value  
        f.Related_Case__c = l.CaseID;

        update f;
    }
}