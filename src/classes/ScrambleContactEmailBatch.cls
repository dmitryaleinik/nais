/**
 * @description This job is only meant to be run in dev environments and is used to make sure that parent/guardian
 *              contact email addresses are bogus so we don't actually email real people while testing in dev orgs.
 */
public class ScrambleContactEmailBatch implements Database.Batchable<sObject> {

    private static final String PARENT_GUARDIAN_RECORD_TYPE = 'Parent/Guardian';
    @testVisible private static final String BOGUS_EMAIL = 'notanemail-test1234@sample9874domain.com';
    private static final String PARENT_CONTACT_QUERY = 'SELECT Id, Email FROM Contact WHERE RecordType.Name = :recordTypeName AND Email != :bogusEmail';

    public ScrambleContactEmailBatch() { }

    public Database.QueryLocator start(Database.BatchableContext context) {
        String recordTypeName = PARENT_GUARDIAN_RECORD_TYPE;
        String bogusEmail = BOGUS_EMAIL;
        return Database.getQueryLocator(PARENT_CONTACT_QUERY);
    }

    public void execute(Database.BatchableContext context, List<sObject> scope) {
        List<Contact> records = (List<Contact>)scope;

        for (Contact record : records) {
            record.Email = BOGUS_EMAIL;
        }

        update records;
    }

    public void finish(Database.BatchableContext context) {
        // nothing to do here.
    }
}