global class CopyVerifForPfsaBatchScheduler implements Schedulable {
    
    global void execute(SchedulableContext sc) {
        Database.executebatch(new CopyVerificationForPfsAssignmentsBatch());
    }
}