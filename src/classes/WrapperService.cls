/**
 * Class: WrapperService
 *
 * Copyright (C) 2015 NAIS
 *
 * Purpose: Written by SpringCM, this class supports the AJAX upload method.
 *
 * Where Referenced: SpringCMAjaxUpload.component
 *
 *
 * Change History:
 *
 * Developer         Date                          Description
 * ---------------------------------------------------------------------------------------
 * Drew Piston      2015.10.21           Initial Development
 *
 *
 */


//This service is used to wrap spring REST Calls from salesforce for old browsers. This is not needed for modern browsers.
global class WrapperService {

    global static Databank_Settings__c databankSettings = Databank_Settings__c.getInstance('Databank');

    //this will get the image prview as a base64 string
    webService static string GetPreview(String documentId, string Token, string pageNumber) {
        HttpRequest req = new HttpRequest();

        req.setHeader('Accept', 'image');
        req.setHeader('Authorization', 'oauth ' + Token);

        req.setEndpoint(documentId + '?page=' + pageNumber);
        req.setMethod('GET');

        Http http = new Http();

        //Execute web service call here
        if (!Test.isRunningTest()) {
            HTTPResponse res = http.send(req);

            return EncodingUtil.base64Encode(res.getBodyAsBlob());
        } else {
            return 'Im a base64 string';
        }
    }

    //this will start a spring workflow
    webService static string StartWorkflow(string token, String startXML, string workflowName) {

        HttpRequest req = new HttpRequest();

        req.setHeader('Accept', 'application/json');
        req.setHeader('Authorization', 'oauth ' + token);

        //req.setEndpoint('https://apiuatna11.springcm.com/v201411/workflows');
        req.setEndpoint(databankSettings.WorkflowURL__c);
        req.setMethod('POST');
        req.setBody('Name=' + workflowName + '&Params=' + EncodingUtil.urlEncode(startXML, 'UTF-8'));
        Http http = new Http();

        //Execute web service call here
        if (!Test.isRunningTest()) {
            HTTPResponse res = http.send(req);

            return res.getBody();
        } else {
            return 'test passed';
        }
    }

    //this will move a document to a diffrent folder
    webService static string MoveDocument(string token, String documentUrl, string parentURL) {

        HttpRequest req = new HttpRequest();

        req.setHeader('Accept', 'application/json');
        req.setHeader('Authorization', 'oauth ' + token);
        req.setHeader('content-type', 'application/x-www-form-urlencoded');
        req.setEndpoint(documentUrl);
        req.setMethod('PUT');
        req.setBody(EncodingUtil.urlEncode('ParentFolder[Href]', 'UTF-8') + '=' + EncodingUtil.urlEncode(parentURL, 'UTF-8'));
        Http http = new Http();

        //Execute web service call here

        if (!Test.isRunningTest()) {
            HTTPResponse res = http.send(req);

            return res.getBody();
        } else {
            return 'test passed';
        }
    }

    //this will get an EOS Folder
    webService static string GetEOSFolder(string Token, String Name, string ObjectType, string ObjectId, string Path) {
        HttpRequest req = new HttpRequest();

        req.setHeader('Accept', 'application/json');
        req.setHeader('Authorization', 'oauth ' + Token);

        //req.setEndpoint('https://apiuatna11.springcm.com/v201411/folders?expand=Folders' + EncodingUtil.urlEncode('EosName', 'UTF-8') + '=' + EncodingUtil.urlEncode(Name, 'UTF-8') + '&' +
        req.setEndpoint(databankSettings.SetEOSURL__c + '?expand=Folders' + EncodingUtil.urlEncode('EosName', 'UTF-8') + '=' + EncodingUtil.urlEncode(Name, 'UTF-8') + '&' +
                        EncodingUtil.urlEncode('EosObjectId', 'UTF-8') + '=' + EncodingUtil.urlEncode(ObjectId, 'UTF-8') + '&' +
                        EncodingUtil.urlEncode('EosObjectType', 'UTF-8') + '=' + EncodingUtil.urlEncode(ObjectType, 'UTF-8') + '&' +
                        EncodingUtil.urlEncode('EosPath', 'UTF-8') + '=' + EncodingUtil.urlEncode(Path, 'UTF-8'));
        req.setMethod('GET');
        Http http = new Http();

        //Execute web service call here
        if (!Test.isRunningTest()) {
            HTTPResponse res = http.send(req);

            return res.getBody();
        } else {
            return 'test passed';
        }
    }

    webService static string GetFolder(string Token, string Path) {
        HttpRequest req = new HttpRequest();
        req.setHeader('Accept', 'application/json');
        req.setHeader('Authorization', 'oauth ' + Token);

        req.setEndpoint(databankSettings.SetEOSURL__c + '?Path=' + EncodingUtil.urlEncode(Path, 'UTF-8'));
        req.setMethod('GET');
        Http http = new Http();

        //Execute web service call here
        if (!Test.isRunningTest()) {
            HTTPResponse res = http.send(req);
            return res.getBody();
        } else {
            return 'test passed';
        }
    }


    //This will search for a document by name in a given folder
    webService static string DocumentSearch(string Token, String Name, string ParentFolder) {

        HttpRequest req = new HttpRequest();


        req.setHeader('Accept', 'application/json');
        req.setHeader('Authorization', 'oauth ' + Token);

        //req.setEndpoint('https://apiuatna11.springcm.com/v201411/documentsearchtasks');
        req.setEndpoint(databankSettings.CheckExistingURL__c);

        req.setMethod('POST');
        req.setBody(EncodingUtil.urlEncode('InFolder[Href]', 'UTF-8') + '=' + EncodingUtil.urlEncode(ParentFolder, 'UTF-8') + '&' +
                    'IncludeSubFolders=true' + '&' +
                    'Phrase=' + EncodingUtil.urlEncode(Name, 'UTF-8'));
        Http http = new Http();


        //Execute web service call here

        if (!Test.isRunningTest()) {
            HTTPResponse res = http.send(req);

            return res.getBody();
        } else {
            return 'test passed';
        }

    }

    //this will get the document info for a given document.
    webService static string GetDocument(string Token, String DocumentUrl) {

        HttpRequest req = new HttpRequest();
        //req.setTimeout(60000);

        req.setHeader('Accept', 'application/json');
        req.setHeader('Authorization', 'oauth ' + Token);

        req.setEndpoint(DocumentUrl);
        req.setMethod('GET');

        Http http = new Http();

        //Execute web service call here
        if (!Test.isRunningTest()) {
            HTTPResponse res = http.send(req);

            return res.getBody();
        } else {
            return 'test passed';
        }
    }

}