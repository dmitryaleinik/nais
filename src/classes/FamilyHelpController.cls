/**
 * FamilyHelpController.cls
 *
 * @description Controller for FamilyHelp.page
 *
 * @author Mike Havrilla @ Presence PG
 */

public class FamilyHelpController {
    public FamilyTemplateController controller{ get; set; }
    public List<Knowledge__kav> knowledgeArticles { get; set; }
    public String category { get; set; }

    public FamilyHelpController( FamilyTemplateController controller) {
        this.controller = controller;

        knowledgeArticles = new KnowledgeDataAccessService().getTopFamilyKnowledgeArticlesFaq(10);

    }

    public PageReference goToCategory() {
        PageReference nextPage = Page.FamilyHelpSearchResults;
        nextPage.getParameters().put('id', this.controller.pfsRecord.Id);
        nextPage.getParameters().put('category', category);
        nextPage.setRedirect( true);
        return nextPage;
    }

}