@isTest
private class FamilyAwardsControllerTest
{
    
    private static Account school_1, school_2;
    private static Contact parent, student_1, student_2;
    private static PFS__c pfs;
    private static Applicant__c applicant_1, applicant_2;
    private static Student_Folder__c folder_1, folder_2, folder_3, folder_4;
    private static School_PFS_Assignment__c spa_1, spa_2, spa_3, spa_4;
    private static Contact staff_1, staff_2;
    private static User staffUser_1, staffUser_2;
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    
    private static void setupData() {
        
        Academic_Year__c academicYear = AcademicYearTestData.Instance.insertAcademicYear();
        
        //Create Schools
        school_1 = AccountTestData.Instance
            .forName('School_1')
            .asSchool()
            .create();
        school_2 = AccountTestData.Instance
            .forName('School_2')
            .asSchool()
            .create();
        insert new List<Account>{school_1, school_2};
        
        //Create contacts for parents and students.
        parent = ContactTestData.Instance
          .asParent()
          .forFirstName('Steven')
          .forLastName('Tyler')
          .create();
        student_1 = ContactTestData.Instance
          .asStudent()
          .forFirstName('Liv')
          .forLastName('Tyler')
          .create();
        student_2 = ContactTestData.Instance
          .asStudent()
          .forFirstName('Mia')
          .forLastName('Tyler')
          .create();
        staff_1 = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(school_1.Id)
            .create();
        staff_2 = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(school_2.Id)
            .create();
        insert new List<Contact> { parent, student_1, student_2, staff_1, staff_2 };
        
        //Create PFS records.
        pfs = PfsTestData.Instance
            .forParentA(parent.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .asPaid()
            .insertPfs();
            
        //Create applicants.
        applicant_1 = ApplicantTestData.Instance
            .forContactId(student_1.Id)
            .forPfsId(pfs.Id)
            .create();
        applicant_2 = ApplicantTestData.Instance
            .forContactId(student_2.Id)
            .forPfsId(pfs.Id)
            .create();
        insert new List<Applicant__c>{ applicant_1, applicant_2 };
        
        //Create Folders
        folder_1 = StudentFolderTestData.Instance.forName('Folder: Applicant_1-School_1')
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student_1.Id)
            .forSchoolId(school_1.Id)
            .create();
        folder_2 = StudentFolderTestData.Instance.forName('Folder: Applicant_1-School_2')
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student_1.Id)
            .forSchoolId(school_2.Id)
            .create();
        folder_3 = StudentFolderTestData.Instance.forName('Folder: Applicant_1-School_1')
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student_2.Id)
            .forSchoolId(school_1.Id)
            .create();
        folder_4 = StudentFolderTestData.Instance.forName('Folder: Applicant_1-School_2')
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student_2.Id)
            .forSchoolId(school_2.Id)
            .create();
        insert new List<Student_Folder__c>{ folder_1, folder_2, folder_3, folder_4 };
        
        //Create SPA records
        spa_1 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forApplicantId(applicant_1.Id)
            .forSchoolId(school_1.Id)
            .forStudentFolderId(folder_1.Id)
            .create();
        spa_2 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forApplicantId(applicant_1.Id)
            .forSchoolId(school_2.Id)
            .forStudentFolderId(folder_2.Id)
            .create();
        spa_3 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forApplicantId(applicant_2.Id)
            .forSchoolId(school_1.Id)
            .forStudentFolderId(folder_3.Id)
            .create();
        spa_4 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forApplicantId(applicant_2.Id)
            .forSchoolId(school_2.Id)
            .forStudentFolderId(folder_4.Id)
            .create();
        insert new List<School_PFS_Assignment__c>{ spa_1, spa_2, spa_3, spa_4 };

        System.runAs(thisUser)
        {
            staffUser_1 = UserTestData.Instance
                .forUsername('Staff_1@test.com')
                .forContactId(staff_1.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).create();
            staffUser_2 = UserTestData.Instance
                .forUsername('Staff_2@test.com')
                .forContactId(staff_2.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).create();
            insert new List<User>{ staffUser_1, staffUser_2 };
        }
        
    }//End:setupData
    
    @isTest 
    private static void applicantsInfo_AcceptDeclineAwards_expectProperFolderStatusAfterCompleteAction()
    {
        setupData();
        
        FeatureToggles.setToggle('Allow_Mass_Email_Per_Applicant__c', true);
        
        School_1.Family_Award_Acknowledgement_Enabled__c = true;
        School_2.Family_Award_Acknowledgement_Enabled__c = true;
        update new List<Account>{ School_1, School_2 };
        
        folder_1.Folder_Status__c = 'Award Approved';
        folder_1.Award_Letter_Sent__c = 'Yes';
        folder_2.Folder_Status__c = 'Award Approved';
        folder_2.Award_Letter_Sent__c = 'Yes';
        folder_3.Folder_Status__c = 'Award Approved';
        folder_3.Award_Letter_Sent__c = 'Yes';
        folder_4.Folder_Status__c = 'Award Approved';
        folder_4.Award_Letter_Sent__c = 'Yes';
        update new List<Student_Folder__c>{ folder_1, folder_2, folder_3, folder_4 };
        
        Test.StartTest();
            
            PageReference familyAwardsPage = new ApexPages.Pagereference('/apex/FamilyAwards?id=' + pfs.Id);
            Test.setCurrentPage(familyAwardsPage);
            
            FamilyTemplateController template = new FamilyTemplateController();
            FamilyAwardsController controller = new FamilyAwardsController(template);
            
            System.AssertEquals(2, controller.applicantsInfo.size());
            
            //Accept award for Applicant_1-School_1
            familyAwardsPage = new ApexPages.Pagereference('/apex/FamilyAwards?id=' + pfs.Id + '&folderId=' + folder_1.Id);
            Test.setCurrentPage(familyAwardsPage);
            controller.acceptAction();
            
            //Deny award for Applicant_1-School_2
            familyAwardsPage = new ApexPages.Pagereference('/apex/FamilyAwards?id=' + pfs.Id + '&folderId=' + folder_2.Id);
            Test.setCurrentPage(familyAwardsPage);
            controller.denyAction();
            
            //Deny award for Applicant_2-School_1
            familyAwardsPage = new ApexPages.Pagereference('/apex/FamilyAwards?id=' + pfs.Id + '&folderId=' + folder_3.Id);
            Test.setCurrentPage(familyAwardsPage);
            controller.denyAction();
            
            //Accept award for Applicant_2-School_2
            familyAwardsPage = new ApexPages.Pagereference('/apex/FamilyAwards?id=' + pfs.Id + '&folderId=' + folder_4.Id);
            Test.setCurrentPage(familyAwardsPage);
            controller.acceptAction();
            
            //Let's verify the folders statuses
            familyAwardsPage = new ApexPages.Pagereference('/apex/FamilyAwards?id=' + pfs.Id);
            Test.setCurrentPage(familyAwardsPage);
            
            template = new FamilyTemplateController();
            controller = new FamilyAwardsController(template);
            
            //Applicant_1
            System.AssertEquals(Label.Complete_Award_Accepted, controller.applicantsInfo[0].awards[0].status);
            System.AssertEquals(Label.Complete_Award_Denied, controller.applicantsInfo[0].awards[1].status);
            
            //Applicant_2
            System.AssertEquals(Label.Complete_Award_Denied, controller.applicantsInfo[1].awards[0].status);
            System.AssertEquals(Label.Complete_Award_Accepted, controller.applicantsInfo[1].awards[1].status);
            
        Test.StopTest();
        
    }//End:applicantsInfo_AcceptDelineAwards_expectProperFolderStatusAfterCompleteAction
    
    
    @isTest 
    private static void applicantsInfo_AllowMassEmailPerApplicantOff_expectNoAwardsListed() {
        
        setupData();
        
        FeatureToggles.setToggle('Allow_Mass_Email_Per_Applicant__c', false);
        
        School_1.Family_Award_Acknowledgement_Enabled__c = true;
        School_2.Family_Award_Acknowledgement_Enabled__c = true;
        update new List<Account>{ School_1, School_2 };
        
        folder_1.Folder_Status__c = 'Award Approved';
        folder_1.Award_Letter_Sent__c = 'Yes';
        folder_2.Folder_Status__c = 'Award Approved';
        folder_2.Award_Letter_Sent__c = 'Yes';
        folder_3.Folder_Status__c = 'Award Approved';
        folder_3.Award_Letter_Sent__c = 'Yes';
        folder_4.Folder_Status__c = 'Award Approved';
        folder_4.Award_Letter_Sent__c = 'Yes';
        update new List<Student_Folder__c>{ folder_1, folder_2, folder_3, folder_4 };
        
        Test.StartTest();
            
            //Verify when the toggle feature is FALSE. Then, the awards are not listed in FP.
            PageReference familyAwardsPage = new ApexPages.Pagereference('/apex/FamilyAwards?id=' + pfs.Id);
            Test.setCurrentPage(familyAwardsPage);            
            FamilyTemplateController template = new FamilyTemplateController();
            FamilyAwardsController controller = new FamilyAwardsController(template);            
            System.AssertEquals(null, controller.applicantsInfo);
            
            //Verify when the toggle feature is TRUE. Then the awards are not listed in FP.
            FeatureToggles.setToggle('Allow_Mass_Email_Per_Applicant__c', true);            
            controller = new FamilyAwardsController(template);            
            System.AssertEquals(2, controller.applicantsInfo.size());
            System.AssertEquals(2, controller.applicantsInfo[0].awards.size());
            System.AssertEquals(2, controller.applicantsInfo[1].awards.size());
            
            //Verify when the toggle feature is TRUE and the school has not the awards feature enabled.
            //Then the awards are not listed in FP.
            School_1.Family_Award_Acknowledgement_Enabled__c = false;
	        School_2.Family_Award_Acknowledgement_Enabled__c = false;
	        update new List<Account>{ School_1, School_2 };
	        controller = new FamilyAwardsController(template);            
            System.AssertEquals(0, controller.applicantsInfo.size());
	        
	        //Verify when the toggle feature is TRUE and the school has not the awards feature enabled.
            //Then the awards are not listed in FP.
            School_1.Family_Award_Acknowledgement_Enabled__c = false;
            School_2.Family_Award_Acknowledgement_Enabled__c = true;
            update new List<Account>{ School_1, School_2 };
            controller = new FamilyAwardsController(template);            
            System.AssertEquals(2, controller.applicantsInfo.size());
            System.AssertEquals(1, controller.applicantsInfo[0].awards.size());
            System.AssertEquals(1, controller.applicantsInfo[1].awards.size());
            
        Test.StopTest();
        
    }//End:applicantsInfo_AllowMassEmailPerApplicantOff_expectNoAwardsListed
}