public class EfcCalculator extends EfcCalculatorBase {
    /**
     * @description Initialize the steps for the Efc Calculator.
     */
    public override void initializeSteps() {
        addStep(new EfcCalculationSteps.TaxableIncome());
        addStep(new EfcCalculationSteps.NonTaxableIncome());
        addStep(new EfcCalculationSteps.TotalIncome());
        addStep(new EfcCalculationSteps.TotalAllowances());
        addStep(new EfcCalculationSteps.EffectiveIncome());
        addStep(new EfcCalculationSteps.HomeEquity());
        addStep(new EfcCalculationSteps.NetWorth());
        addStep(new EfcCalculationSteps.DiscretionaryNetWorth());
        addStep(new EfcCalculationSteps.IncomeSupplement());
        addStep(new EfcCalculationSteps.AdjustedEffectiveIncome());
        addStep(new EfcCalculationSteps.RevisedAdjustedEffectiveIncome());
        addStep(new EfcCalculationSteps.IncomeProtectionAllowance());
        addStep(new EfcCalculationSteps.DiscretionaryIncome());
        addStep(new EfcCalculationSteps.EstimatedParentalContribution());
        addStep(new EfcCalculationSteps.EstimatedParentalContributionPerChild());
        addStep(new EfcCalculationSteps.StudentAssetContribution());
        addStep(new EfcCalculationSteps.EstimatedFamilyContribution());
    }
}