/**
 * @description Created by Zach to try testing a custom connected app plugin class.
 */
global class SampleConnectedAppPlugin extends Auth.ConnectedAppPlugin {

    global override void refresh(Id userId, Id connectedAppId, Auth.InvocationContext context) {
        System.debug('&&& SampleConnectedAppPlugin.refresh()');

        insertLog();
    }

    global override boolean authorize(Id userId, Id connectedAppId, boolean isAdminApproved, Auth.InvocationContext context) {
        System.debug('&&& SampleConnectedAppPlugin.authorize()');

        insertLog();

        return false;
    }

    global override Map<String,String> customAttributes(Id userId, Id connectedAppId, Map<String,String> formulaDefinedAttributes, Auth.InvocationContext context) {
        System.debug('&&& SampleConnectedAppPlugin.customAttributes()');

        insertLog();

        return new Map<String, String>();
    }

    private static IntegrationLog__c insertLog() {
        IntegrationLog__c newLog = new IntegrationLog__c();
        newLog.Body__c = 'SampleConnectedAppPlugin';

        insert newLog;

        return newLog;
    }
}