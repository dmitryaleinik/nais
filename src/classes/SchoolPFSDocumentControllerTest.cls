/*
* SPEC-100
*
* School users should be able to upload documents to a given PFS
*
* SPEC-101
*
* School users should be able to rename documents already uploaded to a given PFS
* A school user should be able to change document metadata (name, type, etc.) once a family uploads it.
* A school user/TCN agent should be able to mark a document as deleted, which results in it being hidden from their view. 
* This should include an ""Are you sure?"" prompt and possibly making the button red (or otherwise look dangerous)
*
* Nathan, Exponent Partners, 2013
*/
@isTest
private class SchoolPFSDocumentControllerTest
{

    private class TestDataLocal
    {
        private Academic_Year__c academicYear;
        private Id academicYearId;
        private Account school1;
        private Contact staff1, parentA, parentB, student1, student2;
        private Applicant__c applicant1A;
        private Applicant__c applicant2A;
        private PFS__c pfs1;
        private Student_Folder__c studentFolder1;
        private Student_Folder__c studentFolder2;
        private School_PFS_Assignment__c spfsa1;
        private School_PFS_Assignment__c spfsa2;
        private Required_Document__c rd1, rd2;
        private Family_Document__c fd1, fd2;
        private School_Document_Assignment__c sda1, sda2;

        private TestDataLocal()
        {
            // academic year
            academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
            academicYearId = GlobalVariables.getCurrentAcademicYear().Id;  

            // school
            school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, null, false); 
            insert school1;       

            // parents and students
            parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
            parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
            student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
            student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
            insert new List<Contact> {parentA, parentB, student1, student2};

            // psf
            pfs1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
            insert new List<PFS__c> {pfs1};

            // applicant
            applicant1A = TestUtils.createApplicant(student1.Id, pfs1.Id, false);
            applicant2A = TestUtils.createApplicant(student2.Id, pfs1.Id, false);
            insert new List<Applicant__c> {applicant1A, applicant2a};

            // student folder
            studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
            studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', academicYearId, student2.Id, false);
            insert new List<Student_Folder__c>{studentFolder1, studentFolder2};

            // school pfs assignment
            spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder1.Id, false);
            spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.Id, school1.Id, studentFolder2.Id, false);
            insert new List<School_PFS_Assignment__c> {spfsa1, spfsa2};

            // required documents
            rd1 = TestUtils.createRequiredDocument(academicYearId, school1.Id, false);
            rd2 = TestUtils.createRequiredDocument(academicYearId, school1.Id, false);
            insert new List<Required_Document__c> {rd1, rd2};

            // family documents
            fd1 = TestUtils.createFamilyDocument('FamilyDoc1', academicYearId, false);
            fd2 = TestUtils.createFamilyDocument('FamilyDoc2', academicYearId, false);
            insert new List<Family_Document__c> {fd1, fd2};

            // update family document lookup of school document assignments automatically created by Required Document trigger  
            for (School_Document_Assignment__c sd : [select Id, Required_Document__c from School_Document_Assignment__c where School_PFS_Assignment__c = :spfsa1.Id]) {
                if (sd.Required_Document__c == rd1.Id) {
                    sda1 = sd;
                    sda1.Document__c = fd1.Id;
                } else {
                    sda2 = sd;
                    sda2.Document__c = fd2.Id;
                }
            }
            update new List<School_Document_Assignment__c> {sda1, sda2};
        }
    }

    @isTest
    private static void testDocumentWrapperListSize() {
        TestDataLocal td = new TestDataLocal();

        Test.startTest();
        PageReference pageRef = Page.SchoolPFSDocument;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSDocumentController controller = new SchoolPFSDocumentController();
        
        Integer academicYearStart = Integer.valueOf(td.academicYear.name.split('-',2)[0]);
        
        for(SelectOption vdy : controller.getValidDocumentYears()){
            if(vdy.getValue() != null && vdy.getValue() != ''){
                system.assert(Integer.valueOf(vdy.getValue()) < academicYearStart);
            }
        }

        // there should be 2 rows as per above seed test data. one for rd1-fd1 and other for rd2-fd2

        // system.assertEquals(2, controller.documentWrapperList.size()); 
        system.assertEquals(2, controller.sdaWrapperList.size()); 
        
        
        delete td.sda1;
        
        // one required document left
        controller = new SchoolPFSDocumentController();
        //system.assertEquals(1, controller.documentWrapperList.size());
        system.assertEquals(1, controller.sdaWrapperList.size());
       
        Test.stopTest();    
    }

    @isTest
    private static void testAddAnother(){
        TestDataLocal td = new TestDataLocal();

        //Document_Type_Settings__c newSetting = new Document_Type_Settings__c(Name='W2', Category__c = 'Add Another');
        Document_Type_Settings__c newSetting = new Document_Type_Settings__c(Name='W2', Add_Another__c = true);
        insert newSetting;

        Required_Document__c rd3 = TestUtils.createRequiredDocument(td.academicYearId, td.school1.Id, 'W2', GlobalVariables.getPrevDocYearStringFromAcadYearName(td.academicYear.Name), true);

        Test.startTest();
            // Check with a set of non-uploaded docs first
            PageReference pageRef = Page.SchoolPFSDocument;
            pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
            Test.setCurrentPage(pageRef);
            SchoolPFSDocumentController controller = new SchoolPFSDocumentController();

            for(SchoolDocAssignWrapper sdaWrapper : controller.sdaWrapperList){
                if(sdaWrapper.sda.Document_Type__c == 'W2'){
                    System.assert(!sdaWrapper.isAddAnotherDoc);
                }
            }

            // Simulate uploading a document
            Family_Document__c fd3 = TestUtils.createFamilyDocument('FamilyDoc3', td.academicYearId, false);
            fd3.Document_Status__c = 'Complete';
            insert fd3;

            List<School_Document_Assignment__c> sdaUpdateList = [select Id, Document__c
                                                            from School_Document_Assignment__c
                                                            where School_PFS_Assignment__c = :td.spfsa1.Id];

            for(School_Document_Assignment__c sdaRecord : sdaUpdateList){
                sdaRecord.Document__c = fd3.Id;
            }

            Database.update(sdaUpdateList);

            // Check with a set of non-uploaded docs first
            PageReference pageRef2 = Page.SchoolPFSDocument;
            pageRef2.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
            Test.setCurrentPage(pageRef2);
            SchoolPFSDocumentController controller2 = new SchoolPFSDocumentController();

            for(SchoolDocAssignWrapper sdaWrapper : controller2.sdaWrapperList){
                if(sdaWrapper.sda.Document_Type__c == 'W2'){
                    System.assert(sdaWrapper.isAddAnotherDoc);
                }
            }
        Test.stopTest();
    }
     
    @isTest
    private static void testMFS1040DocumentRow(){
        TestDataLocal td = new TestDataLocal();
        
        Required_Document__c rd3 = TestUtils.createRequiredDocument(td.academicYearId, td.school1.Id, '1040 with all filed schedules and attachments', GlobalVariables.getPrevDocYearStringFromAcadYearName(td.academicYear.Name), true);
        Required_Document__c rd4 = TestUtils.createRequiredDocument(td.academicYearId, td.school1.Id, '1040 with all filed schedules and attachments', GlobalVariables.getDocYearStringFromAcadYearName(td.academicYear.Name), true);
        
        Test.startTest();
            // Check a non Married, Filing Separately PFS first
            PageReference pageRef = Page.SchoolPFSDocument;
            pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
            Test.setCurrentPage(pageRef);
            SchoolPFSDocumentController controller = new SchoolPFSDocumentController();        
    
            system.assertEquals(4, controller.sdaWrapperList.size());
            
            // Check a Married, Filing Separately PFS
            td.pfs1.Filing_Status__c = 'Married, Filing Separately';
            update td.pfs1;
            
            PageReference pageRef2 = Page.SchoolPFSDocument;
            pageRef2.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
            Test.setCurrentPage(pageRef2);
            SchoolPFSDocumentController controller2 = new SchoolPFSDocumentController();
            
            system.assertEquals(6, controller2.sdaWrapperList.size());
            
            // Update the SDA records from this SPFSA to be Parent B records
            List<School_Document_Assignment__c> sdaUpdateList = [select Id, Document__c 
                                                                    from School_Document_Assignment__c 
                                                                    where Document_Type__c = '1040 with all filed schedules and attachments'
                                                                    AND School_PFS_Assignment__c = :td.spfsa1.Id];
            System.assertEquals(2, sdaUpdateList.size());
            
            // family documents
            Family_Document__c fd3 = TestUtils.createFamilyDocument('FamilyDoc3', td.academicYearId, false);
            fd3.Document_Pertains_to__c = 'Parent B';
            Family_Document__c fd4 = TestUtils.createFamilyDocument('FamilyDoc4', td.academicYearId, false);
            fd4.Document_Pertains_to__c = 'Parent B';
            insert new List<Family_Document__c> {fd3, fd4};
            
            sdaUpdateList[0].Document__c = fd3.Id;
            sdaUpdateList[1].Document__c = fd4.Id;
            
            Database.update(sdaUpdateList);
            
               PageReference pageRef3 = Page.SchoolPFSDocument;
            pageRef3.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
            Test.setCurrentPage(pageRef3);
            SchoolPFSDocumentController controller3 = new SchoolPFSDocumentController();
            
            system.assertEquals(6, controller3.sdaWrapperList.size());
        Test.stopTest();
        
    }

    @isTest
    private static void testAddNotReceivedDocument() {
        TestDataLocal td = new TestDataLocal();

        Test.startTest();
        // create required document without family document
        Required_Document__c rd = TestUtils.createRequiredDocument(td.academicYearId, td.school1.Id, false);
        insert rd;
        
        // NAIS-937: handled by trigger
        // School_Document_Assignment__c sda = TestUtils.createSchoolDocumentAssignment('sda', null, rd.Id, null, false);
        // sda.School_PFS_Assignment__c = td.spfsa1.Id;    // required field
        //insert sda;            
        
        PageReference pageRef = Page.SchoolPFSDocument;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSDocumentController controller = new SchoolPFSDocumentController();        

        // there should be 3 rows. one for rd1-fd1 and other for rd2-fd2 + not received family document for rd
        // system.assertEquals(3, controller.documentWrapperList.size()); 
        system.assertEquals(3, controller.sdaWrapperList.size()); 
        Test.stopTest();    
    }

    @isTest
    private static void testEditSaveCancelDocument() {
        TestDataLocal td = new TestDataLocal();

        Test.startTest();
        PageReference pageRef = Page.SchoolPFSDocument;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSDocumentController controller = new SchoolPFSDocumentController();        

        //pageRef.getParameters().put('familyDocumentId', td.fd1.Id);
        pageRef.getParameters().put('schoolDocumentAssignmentId', td.sda1.Id);
        
        controller.editDocument();
        
        // change some field values
        controller.familyDocument.Document_Pertains_to__c = 'Parent A';
        
        // save and verify
        controller.saveDocument();
        
        // requery and verify
        system.assertEquals('Parent A', [select Document_Pertains_to__c from Family_Document__c 
            where Id =:td.fd1.Id].Document_Pertains_to__c); 

        // test cancel
        controller.familyDocument.Document_Pertains_to__c = 'Parent B';
        controller.cancelDocument();
        // requery and verify that previous value retained
        system.assertEquals('Parent A', [select Document_Pertains_to__c from Family_Document__c 
            where Id =:td.fd1.Id].Document_Pertains_to__c); 
        
        Test.stopTest();    
    }

    @isTest
    private static void testDeleteDocument() {
        TestDataLocal td = new TestDataLocal();

        Test.startTest();
        PageReference pageRef = Page.SchoolPFSDocument;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSDocumentController controller = new SchoolPFSDocumentController();        

        //pageRef.getParameters().put('familyDocumentId', td.fd1.Id);
        pageRef.getParameters().put('schoolDocumentAssignmentId', td.sda1.Id);
        controller.deleteDocument();
        
        // NAIS-272
        // Per clarification from Sara, the Family Document record flag should NOT be set to Deleted 
        // when a school deletes a document. I have added a Deleted flag to the School Document Assignment, 
        // and this is what should be set when a school removes a document
        system.assertEquals(false, [select Deleted__c from Family_Document__c where Id =:td.fd1.Id].Deleted__c); 
        system.assertEquals(true, [select Deleted__c from School_Document_Assignment__c where Id =:td.sda1.Id].Deleted__c); 
        Test.stopTest();    
    }

    @isTest
    private static void testDeleteDocumentWithSiblings() {
        TestDataLocal td = new TestDataLocal();

        Test.startTest();
        PageReference pageRef = Page.SchoolPFSDocument;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSDocumentController controller = new SchoolPFSDocumentController();        

        system.assertEquals(0, [select count() from School_Document_Assignment__c where Deleted__c = true AND School_PFS_Assignment__r.Applicant__c=:td.applicant1A.Id]); 
        system.assertEquals(0, [select count() from School_Document_Assignment__c where Deleted__c = true AND School_PFS_Assignment__r.Applicant__c=:td.applicant2A.Id]); 


        //pageRef.getParameters().put('familyDocumentId', td.fd1.Id);
        pageRef.getParameters().put('schoolDocumentAssignmentId', td.sda1.Id);
        controller.deleteDocument();
        
        // NAIS-272
        // Per clarification from Sara, the Family Document record flag should NOT be set to Deleted 
        // when a school deletes a document. I have added a Deleted flag to the School Document Assignment, 
        // and this is what should be set when a school removes a document
        system.assertEquals(false, [select Deleted__c from Family_Document__c where Id =:td.fd1.Id].Deleted__c); 
        system.assertEquals(true, [select Deleted__c from School_Document_Assignment__c where Id =:td.sda1.Id].Deleted__c); 
        system.assertEquals(1, [select count() from School_Document_Assignment__c where Deleted__c = true AND School_PFS_Assignment__r.Applicant__c=:td.applicant1A.Id]); 
        system.assertEquals(1, [select count() from School_Document_Assignment__c where Deleted__c = true AND School_PFS_Assignment__r.Applicant__c=:td.applicant2A.Id]); 
        Test.stopTest();    
    }

    @isTest
    private static void testNewSaveCancelAdditionalDocument() {
        TestDataLocal td = new TestDataLocal();

        Test.startTest();
        PageReference pageRef = Page.SchoolPFSDocument;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSDocumentController controller = new SchoolPFSDocumentController();        

        // upload new additional document
        controller.requireAdditionalDocument();
        
        // set some fields and save
//        controller.familyDocument.Document_Pertains_to__c = 'Parent A';
        controller.saveAdditionalDocument();
        
        system.assertEquals(2, [select Id from School_Document_Assignment__c where School_PFS_Assignment__c =:controller.schoolPfsAssignment.Id].size()); 
        
        // cancel. nothing to verify here since it is new/cancel
        controller.requireAdditionalDocument();
        controller.cancelAdditionalDocument();
        
        Test.stopTest();    
    }     

    @isTest
    private static void testuploadDocumentHeader() {
        TestDataLocal td = new TestDataLocal();

        Test.startTest();
         
        PageReference pageRef = Page.SchoolPFSDocument;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSDocumentController controller = new SchoolPFSDocumentController();        

        // upload new family document for rd
        controller.requiredDocumentId = null;
        controller.uploadDocument();
        
        // set some fields and save
        controller.familyDocument.Document_Pertains_to__c = 'Parent A';
        //controller.selectedNonVerifyDocType = dcs.Id;    // select non verifiable type
        //controller.selectedNonVerifyDocType = 'Custom NonVerifiable';    // select non verifiable type
        controller.additionalSchoolDocAssign.Document_Type__c = 'Custom NonVerifiable';    // select non verifiable type
        controller.saveUploadDocument();
        
        // non verifiable document type is required to be selected without required document
        // you should not get error message
        system.assertEquals(null, controller.errorMessage);

        // it creates a family document and school document assignment linked to required document   
        
        controller.cancelUploadDocument();
        
        System.assertEquals(controller.listRendered, true);
        System.assertEquals(controller.editRendered, false);
        System.assertEquals(controller.additionalRendered, false);
        System.assertEquals(controller.uploadRendered, false);        
        
        Test.stopTest();    
    }

    // [SL] NAIS-1365: Check for existing documents when a school requires an additional document
    @isTest
    private static void testRequireAdditionalDocumentWithExistingDocument() {
        
        TestDataLocal td = new TestDataLocal();
        
        Document_Type_Settings__c typeSettings = new Document_Type_Settings__c();
        typeSettings.Name = 'Personal Letter';
        typeSettings.Full_Type_Name__c = 'Personal Letter';
        typeSettings.Tax_Document__c = true;
        
        // [CH] NAIS-1521
        Document_Type_Settings__c typeSettings2 = new Document_Type_Settings__c();
        typeSettings2.Name = 'Non-Tax Document';
        typeSettings2.Full_Type_Name__c = 'Non-Tax Document';
        typeSettings2.Tax_Document__c = false;
        
        Database.insert(new List<Document_Type_Settings__c>{typeSettings, typeSettings2});
        
        Household__c hh = TestUtils.createHousehold('Test Household', true);
        td.parentA.Household__c = hh.Id;
        update td.parentA;

        Test.startTest();
        PageReference pageRef = Page.SchoolPFSDocument;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSDocumentController controller = new SchoolPFSDocumentController();        

        // create a family document
        Family_Document__c familyDoc = new Family_Document__c();
        // NAIS-2383 [DP] 04.02.2015 refactor to get name for picklist value
        familyDoc.Academic_Year_Picklist__c = td.academicYear.Name;
        familyDoc.Document_Status__c = 'Received/In Progress';
        familyDoc.Document_Type__c = 'Personal Letter';
        familyDoc.Document_Year__c = '2013';
        familyDoc.Household__c = td.parentA.Household__c;
        insert familyDoc;

        // upload new additional document
        controller.requireAdditionalDocument();
        
        // set some fields and save
        controller.additionalSchoolDocAssign.Document_Type__c = 'Personal Letter';
        controller.additionalSchoolDocAssign.Document_Year__c = '2013';
        controller.saveAdditionalDocument();
         
        // check that a SDA was created and linked to the family doc
        system.assertEquals(familyDoc.Id, [SELECT Document__c 
                                            FROM School_Document_Assignment__c 
                                            WHERE Document_Type__c = 'Personal Letter' 
                                            AND Document_Year__c = '2013'
                                            AND School_PFS_Assignment__c = :controller.schoolPfsAssignment.Id LIMIT 1].Document__c);
                                            
        // upload new additional document
        controller.requireAdditionalDocument();
        
        // set some fields and save
        controller.additionalSchoolDocAssign.Document_Type__c = 'Non-Tax Document';
        controller.saveAdditionalDocument();
        
        // Check that the correct year was used for the non tax doc
        // NAIS-2191 [DP] 01.28.2015 -- now making non-tax docs the year previous to the acad year (e.g., 2014-2015 non-tax docs get a year of 2013)
        system.assertEquals(String.valueOf(Integer.valueOf(td.academicYear.Name.split('-')[0])-1), [SELECT Document_Year__c 
                                            FROM School_Document_Assignment__c 
                                            WHERE Document_Type__c = 'Non-Tax Document' 
                                            AND School_PFS_Assignment__c = :controller.schoolPfsAssignment.Id LIMIT 1].Document_Year__c); 
        Test.stopTest();    
    }

    // [SL] NAIS-1365: Check for existing documents when a school requires an additional document
    @isTest
    private static void testRequireAdditionalDocumentWithExistingDocumentAndSibling() {
        TestDataLocal td = new TestDataLocal();
        
        Document_Type_Settings__c typeSettings = new Document_Type_Settings__c();
        typeSettings.Name = 'Personal Letter';
        typeSettings.Full_Type_Name__c = 'Personal Letter';
        typeSettings.Tax_Document__c = true;
        insert typeSettings;
        
        Household__c hh = TestUtils.createHousehold('Test Household', true);
        td.parentA.Household__c = hh.Id;
        update td.parentA;

        Test.startTest();
        PageReference pageRef = Page.SchoolPFSDocument;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSDocumentController controller = new SchoolPFSDocumentController();        

        // create a family document
        Family_Document__c familyDoc = new Family_Document__c();
        // NAIS-2383 [DP] 04.02.2015 refactor to get name for picklist value
        familyDoc.Academic_Year_Picklist__c = td.academicYear.Name;
        familyDoc.Document_Status__c = 'Received/In Progress';
        familyDoc.Document_Type__c = 'Personal Letter';
        familyDoc.Document_Year__c = '2013';
        familyDoc.Household__c = td.parentA.Household__c;
        insert familyDoc;

        // upload new additional document
        controller.requireAdditionalDocument();
        
        // set some fields and save
        controller.additionalSchoolDocAssign.Document_Type__c = 'Personal Letter';
        controller.additionalSchoolDocAssign.Document_Year__c = '2013';
        controller.saveAdditionalDocument();

        // expect 2, once for the spfsa we are looking at one for the sibling
        System.assertEquals(2, [Select count() FROM School_Document_Assignment__c 
                                            WHERE Document_Type__c = 'Personal Letter' 
                                            AND Document_Year__c = '2013'
                                            AND Document__c = :familyDoc.Id]);
        Test.stopTest();    
    }

    // [SL] NAIS-1365: Check for existing documents when a school requires an additional document
    @isTest
    private static void testRequireAdditionalDocumentWithOUTExistingDocumentAndSibling() {
        
        TestDataLocal td = new TestDataLocal();
        
        Document_Type_Settings__c typeSettings = new Document_Type_Settings__c();
        typeSettings.Name = 'Personal Letter';
        typeSettings.Full_Type_Name__c = 'Personal Letter';
        typeSettings.Tax_Document__c = true;
        insert typeSettings;
        
        Household__c hh = TestUtils.createHousehold('Test Household', true);
        td.parentA.Household__c = hh.Id;
        update td.parentA;

        td.spfsa2.Withdrawn__c = 'Yes';
        update td.spfsa2;

        Test.startTest();
        PageReference pageRef = Page.SchoolPFSDocument;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSDocumentController controller = new SchoolPFSDocumentController();        

        // create a family document
        Family_Document__c familyDoc = new Family_Document__c();
        // NAIS-2383 [DP] 04.02.2015 refactor to get name for picklist value
        familyDoc.Academic_Year_Picklist__c = td.academicYear.Name;
        familyDoc.Document_Status__c = 'Received/In Progress';
        familyDoc.Document_Type__c = 'Personal Letter';
        familyDoc.Document_Year__c = '2013';
        familyDoc.Household__c = td.parentA.Household__c;
        insert familyDoc;

        // upload new additional document
        controller.requireAdditionalDocument();
        
        // set some fields and save
        controller.additionalSchoolDocAssign.Document_Type__c = 'Personal Letter';
        controller.additionalSchoolDocAssign.Document_Year__c = '2013';
        controller.saveNewDocument();

        // expect 1, since we withdrew the sibling above
        System.assertEquals(1, [Select count() FROM School_Document_Assignment__c 
                                            WHERE Document_Type__c = 'Personal Letter' 
                                            AND Document_Year__c = '2013'
                                            AND Document__c = :familyDoc.Id]);
                                          
        // upload new additional document
        controller.requireAdditionalDocument();
        
        // set some fields and save
        controller.additionalSchoolDocAssign.Document_Type__c = 'Personal Letter';
        controller.additionalSchoolDocAssign.Document_Year__c = '2013';
        controller.saveAndNewAdditionalDocument();
        
        System.assertEquals(controller.listRendered, false);
        System.assertEquals(controller.editRendered, false);
        System.assertEquals(controller.additionalRendered, true);
        
        Test.stopTest();    
    }
    
    @isTest
    private static void testuploadOtherDocuments() {
        TestDataLocal td = new TestDataLocal();

        Test.startTest();
        Family_Document__c familyDoc = new Family_Document__c(
                                            Name='Test',
                                            Academic_Year_Picklist__c=td.academicYear.Id,
                                            Document_Type__c='Other/Misc Tax Form');
        insert familyDoc;
        td.sda1.document__c = familyDoc.Id;
        update td.sda1;
        PageReference pageRef = Page.SchoolPFSDocument;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSDocumentController controller = new SchoolPFSDocumentController();        

        // upload new family document for rd
        controller.requiredDocumentId = null;
        controller.fromOtherDocumentsMenu =true;
        controller.uploadDocumentOther();
        
        // set some fields and save
        controller.familyDocument.Document_Pertains_to__c = 'Parent A';
        //controller.selectedNonVerifyDocType = dcs.Id;    // select non verifiable type
        //controller.selectedNonVerifyDocType = 'Custom NonVerifiable';    // select non verifiable type
        controller.additionalSchoolDocAssign.Document_Type__c = 'Custody Agreement';    // select non verifiable type
        controller.saveAndUploadOtherDocuments();
        
        // non verifiable document type is required to be selected without required document
        // you should not get error message
        system.assertEquals(null, controller.errorMessage);
        // it creates a family document and school document assignment linked to required document   
        
        controller.cancelUploadDocument();
        
        System.assertEquals(controller.ShowUploadPopupForUploadOtherDocs, true);
        
        controller.documentNameForEditId = td.sda1.Id;
        controller.saveMiscellaneousDocumentForm();
        controller.miscellaneousDocumentToEdit  = td.sda1;
        //controller.miscellaneousDocumentToEdit.document__r.Document_Name__c = 'Test 2';
        controller.saveMiscellaneousDocument();
        controller.cancelsaveMiscellaneousDocument();
        
        Test.stopTest();    
    }
    
    // NAIS-2336 [CH] For testing override and unoverride of document statuses
    @isTest
    private static void testOverrideAllDocStatus() {
        
        TestDataLocal td = new TestDataLocal();

        Test.startTest();
            // Check with a set of non-uploaded docs first
            PageReference pageRef = Page.SchoolPFSDocument;
            pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
            Test.setCurrentPage(pageRef);
            SchoolPFSDocumentController controller = new SchoolPFSDocumentController();
            
            controller.overrideAllDocStatus();
            
            List<School_PFS_Assignment__c> spaCheckList = [select Id, PFS_Document_Status_Override__c from School_PFS_Assignment__c];
            System.AssertEquals(true, spaCheckList[0].PFS_Document_Status_Override__c);
            System.AssertEquals(true, spaCheckList[1].PFS_Document_Status_Override__c);
            
            controller.unOverrideAllDocStatus();
            
               spaCheckList = [select Id, PFS_Document_Status_Override__c from School_PFS_Assignment__c];
            System.AssertEquals(false, spaCheckList[0].PFS_Document_Status_Override__c);
            System.AssertEquals(false, spaCheckList[1].PFS_Document_Status_Override__c);
            
        Test.stopTest();
    }
    
    //SFP-38, [G.S]
    @isTest
    private static void testArchiveDocuments() {
        TestDataLocal td = new TestDataLocal();

        Test.startTest();
        PageReference pageRef = Page.SchoolPFSDocument;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSDocumentController controller = new SchoolPFSDocumentController();        
        pageRef.getParameters().put('schoolDocumentAssignmentId', td.sda1.Id);
        pageRef.getParameters().put('archiveDocument', 'true');
        controller.archiveDocument();
        system.assertEquals(true, [select archived__c from School_Document_Assignment__c where Id =:td.sda1.Id].archived__c); 
        
        controller = new SchoolPFSDocumentController();        
        pageRef.getParameters().put('schoolDocumentAssignmentId', td.sda1.Id);
        pageRef.getParameters().put('archiveDocument', 'false');
        controller.archiveDocument();
        system.assertEquals(false, [select archived__c from School_Document_Assignment__c where Id =:td.sda1.Id].archived__c); 
        Set<String> nonSchoolDocs = new Set<String>();
        for(SelectOption s:controller.getNonSchoolSpecificDocumentTypes() ){
            nonSchoolDocs.add( s.getValue()  );
        }
        system.assertEquals(false, nonSchoolDocs.contains('Other/Misc Tax Form') );
        system.assertEquals(false, nonSchoolDocs.contains(ApplicationUtils.schoolSpecificDocType()) );
        Test.stopTest();
    }
}