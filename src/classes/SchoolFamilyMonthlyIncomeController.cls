public class SchoolFamilyMonthlyIncomeController implements SchoolAcademicYearSelectorStudInterface
{
    public String schoolpfsassignmentid{ get; set; }
    public PFS__c pfsRecord{ get; set; }
    private Id pfsId{ get; set; }
    public ApplicationUtils appUtils {get; set;}
    public Student_Folder__c folder{ get; set; }
    
    public SchoolFamilyMonthlyIncomeController()
    {
        this.schoolpfsassignmentid = ApexPages.currentPage().getParameters().get('schoolPFSAssignmentId');
        this.loadPFS();
        this.appUtils = new ApplicationUtils(UserInfo.getLanguage(), this.pfsRecord, 'MonthlyIncomeExpenses');
    }//End-Constructor
    
    private Id getPFSId()
    {
        if( this.schoolpfsassignmentid != null && this.pfsId == null )
        {
            List<School_PFS_Assignment__c> spa = new List<School_PFS_Assignment__c>(
                                                        [Select Id, Applicant__r.PFS__c, Academic_Year_Picklist__c, Student_Folder__c  
                                                            from School_PFS_Assignment__c 
                                                            where Id=:this.schoolpfsassignmentid 
                                                            and Student_Folder__c <> null 
                                                            limit 1]);
            if( !spa.isEmpty() ){
                this.pfsId = spa[0].Applicant__r.PFS__c;
                this.folder = [Select Id, Student__c, Academic_Year_Picklist__c 
                                from Student_Folder__c 
                                where Id=:spa[0].Student_Folder__c 
                                limit 1];
            }
        }
        return this.pfsId;
    }//End:getPFSId
    
    private List<String> getPFSFieldsToQuery()
    {
        List<String> result = new List<String>();
        for(Schema.FieldSetMember f : SObjectType.PFS__c.FieldSets.MonthlyIncomeAndExpenses.getFields()) {
            result.add(f.getFieldPath());
        }
        return result;
    }//End:getPFSFieldsToQuery
    
    private void loadPFS()
    {
        this.getPFSId();
        List<PFS__c> pfsList = Database.query('SELECT ' + String.join(this.getPFSFieldsToQuery(), ',')
                                        + ' ,MIE_Travel_Expenses__c '
                                        + ' ,MIE_Student_Loan_Payments__c '
                                        + ' ,Academic_Year_Picklist__c '
                                        + ' FROM PFS__c ' +
                                        + ' WHERE Id = :pfsId '
                                        + ' LIMIT 1');
        this.pfsRecord = ( !pfsList.isEmpty()?  pfsList[0] : new PFS__c() );
    }//End:loadPFS
    
    /** Implementation of SchoolAcademicYearSelectorStudInterface */
    public SchoolFamilyMonthlyIncomeController Me {
        get { return this; }
    }

    public Id academicyearid;

    public Id getAcademicYearId() {
        return this.academicyearid;
    }

    public void setAcademicYearId(String academicYearIdParam) {
        this.academicyearid = academicYearIdParam;
    }

    // on change of academic year, reload page with new folder
    public PageReference SchoolAcademicYearSelectorStudent_OnChange(PageReference newPage) {
        return newPage;
    }
}//End-Class