/**
 * @description Custom exception class for handling invalid arguments.
 **/
public class ArgumentException extends Exception { }