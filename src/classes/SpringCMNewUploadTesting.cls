public class SpringCMNewUploadTesting {

   // public String helpConfig { get; set; }

   // public Family_Document__c fDoc {get; set;}
   // public String pfsNumber {get; set;}
    //public String hhId {get; set;}
   // public String hhName {get; set;}

    public SpringCMNewUploadTesting() {
        String fdocid = System.currentPagereference().getParameters().get('fdocid');

        fDoc = [Select Id, Name, Document_Year__c, Document_Type__c, Household__c, Household__r.Name, Document_Source__c, School_Code__c FROM Family_Document__c WHERE Id = :fdocid];
        pfsNumber = System.currentPagereference().getParameters().get('pfsnumber');
        hhId = fDoc.Household__c;
        hhName = fDoc.Household__r.Name;
        Databank_Settings__c databankSettings = Databank_Settings__c.getInstance('Databank');

    sendDocEndpoint = databankSettings.sendDocEndpoint__c;
    getTokenEndpoint = databankSettings.getTokenEndpoint__c;
    clientId = databankSettings.clientId__c;
    clientSecret = databankSettings.clientSecret__c;
    moveDocumentURL = databankSettings.moveDocumentURL__c;
    //moveDocumentURL = '/Other Sources/Salesforce/Households/Drew Testfamily HH_a0PJ0000004dZqkMAE/2014/';
    workflowURL = databankSettings.workflowURL__c;
    checkExistingURL = databankSettings.checkExistingURL__c;
    setEOSURL = databankSettings.setEOSURL__c;
    //setEOSURL = 'https://apiuploaduatna11.springcm.com/v201411/folders/Other Sources/Salesforce/Households/Drew Testfamily HH_a0PJ0000004dZqkMAE/2014/';

        String language = UserInfo.getLanguage();
        language = language == null || language == '' ? 'en_US' : language;
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        
        helpConfig = [Select Id, SpringCM_Cancel_Label__c, SpringCM_Next_Label__c, SpringCM_Page_Label__c, SpringCM_Previous_Label__c, SpringCM_Replace_Label__c, SpringCM_Submit_Label__c, SpringCM_Upload_Label__c
                            from Help_Configuration__c 
                            where Academic_Year__c = : academicYearId
                            AND Language__c = :language limit 1];

    if (!Test.isRunningTest()) {
      GetToken();
    }

    urlParams = System.currentPagereference().getParameters();
        
    }
    
     // set from custom settings
  public String sendDocEndpoint {get; set;}
  public String getTokenEndpoint {get; set;}
  public String clientId {get; set;}
  public String clientSecret {get; set;}
  public String moveDocumentURL {get; set;}
  public String workflowURL {get; set;}
  public String checkExistingURL {get; set;}
  public String setEOSURL {get; set;}
  
  public Family_Document__c fDoc {get; set;}
  public String pfsNumber {get; set;}
  public String hhId {get; set;}
  public String hhName {get; set;}
  public String verifyFlag {get; set;}  // SFP-29
  public String scmAuthenticationError {get; set;}   // SFP-71
  public Boolean forceTheFallback {get; set;}

  // for button labels
  public Help_Configuration__c helpConfig {get; set;}

  private string pdocumentURL;
  public string Token {get; set;}
  public Blob Document {get; set;}
  public string DocumentName {get; set;}

  public Map<String, String> urlParams {get; set;}

  public string ForceFallBack {
    get{
      if (forceTheFallback != null && forceTheFallback){
        return 'true';
      } else {
        return ApexPages.currentPage().getParameters().get('fallback');
      }
    }
  }
  public string DocumentURL {
    get{
      if (pdocumentURL == null && Document != null) {
        SendDocument();
        Document = null;
      }
      return pdocumentURL;
    }
    set{
      pdocumentURL = value;
    }
  }

  public Boolean alreadyFellback {
    get {
      return System.currentPagereference().getParameters().get('fellback') == 'true';
    }
    set;
  }  

  

  public void SendDocument() {
    //System.assertEquals(1,2, 'sendingdocument');

    HttpRequest req = new HttpRequest();
    req.setHeader('Accept', 'application/json');
    req.setHeader('Authorization', 'oauth ' + Token);
    //req.setHeader('content-type', 'application/x-www-form-urlencoded');
    if (!Test.isRunningTest()) {
      // BELOW LINE IS DEMO CODE
      //req.setEndpoint('https://apiuploaduatna11.springcm.com/v201411/folders/478ff48b-2ff4-e411-bd70-6c3be5a75f4d/documents?name=' + EncodingUtil.urlEncode(DocumentName, 'UTF-8') );
      req.setEndpoint(sendDocEndpoint + EncodingUtil.urlEncode(DocumentName, 'UTF-8') );
    }
    req.setMethod('POST');
    if (!Test.isRunningTest()) {
      req.setBodyAsBlob(Document);
    }
    //req.setBody('SFSession=' + UserInfo.getSessionId() + '|' + EncodingUtil.urlEncode(url, 'UTF-8') + '&ClientId=f321ec76-7e9c-44a0-a0ac-a5d0384c63fd&ClientSecret=e78dcd4b24614efb8bd312017f624052d2r2mZzg8tibflRfQ1W37PT6wgCyp8zrByAg8e5NW3k8I5UtjqCK5MSOivFGXB1A40Zrkvhv6IgDoVx0LSO95pvzfw7MZz0V');
    Http http = new Http();
    //Execute web service call here
    if (!Test.isRunningTest()) {
      HTTPResponse res = http.send(req);
      //System.assertEquals(null, res.getBody());
      System.debug(LoggingLevel.ERROR, 'TESTINGdrew resgetbody ' + res.getBody());
      Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());

      DocumentURL = string.ValueOf(m.get('Href'));
    }
  }

  public string GetToken() {
    HttpRequest req = new HttpRequest();

    String url;
    if (GlobalVariables.isSchoolPortalProfile(UserInfo.getProfileId())) {
      // in school community
      url = Site.getBaseUrl() + '/services/Soap/u/18.0/' + UserInfo.getOrganizationId();
    } else {
      // in family portal or SF platform
      url = System.URL.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/18.0/' + UserInfo.getOrganizationId();
    }
    req.setHeader('Accept', 'application/json');
    req.setHeader('content-type', 'application/x-www-form-urlencoded');
    req.setEndpoint(getTokenEndpoint);
    req.setMethod('POST');
    // BELOW LINE is demo code, commented out
    //req.setBody('SFSession=' + UserInfo.getSessionId() + '|' + EncodingUtil.urlEncode(url, 'UTF-8') + '&ClientId=f321ec76-7e9c-44a0-a0ac-a5d0384c63fd&ClientSecret=e78dcd4b24614efb8bd312017f624052d2r2mZzg8tibflRfQ1W37PT6wgCyp8zrByAg8e5NW3k8I5UtjqCK5MSOivFGXB1A40Zrkvhv6IgDoVx0LSO95pvzfw7MZz0V');
    req.setBody('SFSession=' + UserInfo.getSessionId() + '|' + EncodingUtil.urlEncode(url, 'UTF-8') + '&ClientId=' + clientId + '&ClientSecret=' + clientSecret);

    Http http = new Http();
    //Execute web service call here
    if (!Test.isRunningTest()) {
      HTTPResponse res = http.send(req);
// SFP-71 START
      if (res.getStatusCode() == 200) {
        Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        Token = string.ValueOf(m.get('AccessTokenValue'));
        scmAuthenticationError = null;
      } else {
        scmAuthenticationError = 'Authentication to SpringCM failed';
      }
// SFP-71 END
    } else {
      return 'test passed';
    }
    return '';
  }

  // updates fam doc in SF after a successful submit
    public PageReference processSubmitSuccess() {
        // if the return page is loaded before the upload is processed by spring CM, then mark the status Upload Pending
        Family_Document__c familyDocument = null;
        
        if (String.isNotBlank(fDoc.Id)) {
            for (Family_Document__c fd : [SELECT Id, Document_Status__c 
                                                FROM Family_Document__c 
                                                WHERE Id = :fDoc.Id 
                                                AND Document_Status__c = 'Not Received'
                                                AND Request_to_Waive_Requirement__c != 'Yes'
                                                LIMIT 1
                                                FOR UPDATE  // Lock the record to avoid a race condition with the Spring CM update logic
                                                ]) {
                                                // Removed these criteria so we can handle docs that are being Replaced
                                                //AND Import_Id__c = null
                                                //AND Filename__c = null

                familyDocument = fd;
            }
            
            if (familyDocument != null) {
                try {
                    if (familyDocument.Document_Status__c == 'Not Received'){
                        familyDocument.Document_Status__c = 'Upload Pending';
                        update familyDocument;
                        //System.assertEquals(null, familyDocument);
                    }
                }
                catch (DmlException e) {
                    String errmsg = 'ERROR: Unable to update family document ' + familyDocument.Id + ': ' + e.getMessage() + ' ' + e.getStackTraceString();
                    System.debug(LoggingLevel.Error, errmsg);
                    insert new Custom_Debug__c(Message__c = errmsg);
                }
            }
        }

        if (alreadyFellback){
          return alreadyFellbackRedirect();
        }
        return ApexPages.currentPage().setRedirect(true);
    }

    // deletes fam doc in SF after a cancel
    public PageReference processCancelDelete() {
        if (String.isNotBlank(fDoc.Id)) {
            Family_Document__c familyDocument = null;
            
            // retrieve and delete the family document, but only if it hasn't been received
            for (Family_Document__c fd : [SELECT Id, (select Id from School_Document_Assignments__r where Required_Document__c = null and PFS_Specific__c != true)
                                            FROM Family_Document__c 
                                            WHERE Id = :fDoc.Id 
                                            AND Document_Status__c = 'Not Received'
                                            AND Import_Id__c = null
                                            AND Filename__c = null
                                            AND Request_to_Waive_Requirement__c != 'Yes'
                                            LIMIT 1]) {
                familyDocument = fd;
            }
                        
            // delete the family document record
            if (familyDocument != null) {
                
                // delete the family document SDA records that are from an Other/Misc Document upload
                if (familyDocument.School_Document_Assignments__r != null) {
                    try {
                        delete familyDocument.School_Document_Assignments__r;
                    }
                    catch (DmlException e) {
                        System.debug(LoggingLevel.Error, 'ERROR: Unable to delete School Document Assignments ' + familyDocument.Id + ': ' + e.getMessage() + ' ' + e.getStackTraceString());
                    }
                }
                
                try {
                    delete familyDocument;
                }
                catch (DmlException e) {
                    System.debug(LoggingLevel.Error, 'ERROR: Unable to delete family document ' + familyDocument.Id + ': ' + e.getMessage() + ' ' + e.getStackTraceString());
                }
            }
        }
        
        if (alreadyFellback){
          return alreadyFellbackRedirect();
        }
        
        return ApexPages.currentPage().setRedirect(true);   
    }
    
    // smart redirect (for either school or family portal) after cancel or submit from fallback method
    public PageReference alreadyFellbackRedirect(){
      PageReference pr = GlobalVariables.isSchoolPortalProfile(UserInfo.getProfileId()) ? Page.SchoolPFSDocument : Page.FamilyDocuments;
      for (String paramKey : urlParams.keySet()){
        if (paramKey.equalsIgnoreCase('id') || paramKey.equalsIgnoreCase('academicyearid') || paramKey.equalsIgnoreCase('schoolPFSAssignmentId') || paramKey.equalsIgnoreCase('requiredDocumentId')){
          pr.getParameters().put(paramKey, urlParams.get(paramKey));
        }
      } 
      return pr;
    }

  // url for "fallback" (when ajax is not supported)
  public PageReference getFallBackURL() {
    PageReference fallBackURL = Page.SpringCMAjaxUploadFallback;
    fallBackURL.getParameters().put('fallback', 'true');
    fallBackURL.getParameters().put('fellback', 'true');
    if (fDoc != null){
      fallBackURL.getParameters().put('fdocid', fdoc.Id);
    }
    if (!String.isBlank(pfsNumber)){
      fallBackURL.getParameters().put('pfsnum', pfsNumber);
    }

    if (!String.isBlank(System.currentPagereference().getParameters().get('id'))){
      fallBackURL.getParameters().put('id', System.currentPagereference().getParameters().get('id'));
    }

    if (!String.isBlank(System.currentPagereference().getParameters().get('academicyearid'))){
      fallBackURL.getParameters().put('academicyearid', System.currentPagereference().getParameters().get('academicyearid'));
    }
    
    return fallBackURL;
  }

  public String getFallBackURLParams(){
    String paramSTring = '';
    PageReference fback = getFallBackURL();
    for (String keyString : fback.getParameters().keyset()){
      paramSTring += keySTring + '=' + fback.getParameters().get(keyString) + '&';
    } 
    return paramString;
  }

  // translates sf doc type into databank friendly doc type
  public String getDatabankFriendlyDocType(){
    return SpringCMAction.getSpringCMDocTypeFromSFDocType(fDoc.Document_Type__c);
  }
}