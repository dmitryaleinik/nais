public with sharing class ProvideLicensesController {
    /*
    public ProvideLicensesController(ApexPages.StandardSetController controller) {
        
        this.BaseController = controller;
    }
    
    private ApexPages.StandardSetController BaseController {get; set;}
    
    private List<SelectOption> profiles_x = null;
    public List<SelectOption> Profiles {
        get {
            if(this.profiles_x == null) {
                this.profiles_x = new List<SelectOption>();
                this.profiles_x.add(new SelectOption('', ''));
                
                for(Profile p : [SELECT Id, Name FROM Profile]) {
                    this.profiles_x.add(new SelectOption(p.Id, p.Name));
                }
            }
            return this.profiles_x;
        }
    }
    
    public String SelectedProfileId {get; set;}
    
    public PageReference CreateUsers() {
        
        if(this.SelectedProfileId == '') throw new ProvideLicensesPageException('Please select a profile.');
        
        List<User> usersToUpsert = new List<User>();
        
        List<Contact> contacts = (List<Contact>)this.BaseController.getRecords();
        Map<Id, Contact> contactMapById = new Map<Id, Contact>(contacts);
        
        List<User> allUsers = [SELECT Id, Name, UserName, Email, Alias, IsActive, ContactId, UserPermissionsSFContentUser FROM User];

        Set<String> userNames = new Set<String>();
        Set<String> aliases = new Set<String>();
        
        for(User u : allUsers) {
            if(u.ContactId != null) {
                if(contactMapById.containsKey(u.ContactId)) {
                    
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Warning ,
                        'User not created for contact: ' + contactMapById.get(u.ContactId).Name + ', username:' + u.UserName + ' -- user already exists');
                    ApexPages.addMessage(myMsg);
                    contactMapById.remove(u.ContactId);
                    
                    if(!u.IsActive || !u.UserPermissionsSFContentUser) {
                        u.IsActive = true;
                        u.UserPermissionsSFContentUser = true;
                        usersToUpsert.add(u);
                    }
                    
                }
            }
            
            userNames.add(u.UserName.toLowerCase());
            aliases.add(u.Alias.toLowerCase());
        }
        
        for(Contact c : contactMapById.values()) {
            String userName = c.Email.toLowerCase();

            if(!userNames.contains(userName)) {
                
                String alias = (c.FirstName.mid(0,1) + c.LastName).toLowerCase().mid(0,6);            
                if(aliases.contains(alias)) {
                    Integer i = 1;
                    String newAlias = alias;
                    while(aliases.contains(newAlias)) {
                        newAlias = alias + String.valueOf(i);
                        i++;
                    }
                    alias = newAlias;
                }
                
                User u = new User (
                    FirstName = c.FirstName,
                    LastName = c.LastName,
                    Alias = alias,
                    Email = c.Email,
                    Username = userName,
                    CommunityNickname = alias,
                    ProfileId = this.SelectedProfileId,
                    ContactId = c.Id,
                    EmailEncodingKey = 'ISO-8859-1',
                    TimeZoneSidKey = 'America/New_York',
                    LocaleSidKey = 'en_US',
                    LanguageLocaleKey = 'en_US',
                    UserPermissionsSFContentUser = true
                );
                
                // [SL] option to disable user email from custom settings
                School_Portal_Settings__c schoolSettings = School_Portal_Settings__c.getInstance('School');
                if ((schoolSettings == null) || (schoolSettings.Disable_ProvideLicenses_User_Email__c != true)) {
                    Database.DMLOptions dmo = new Database.DMLOptions();
                    dmo.EmailHeader.triggerUserEmail = true;
                    u.setOptions(dmo);
                }
                
                usersToUpsert.add(u);
                
                userNames.add(userName);
                
            } else {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Warning ,
                    'User not created for contact: ' + c.Name + ', username:' + userName + ' -- username already in use');
                    
                System.debug('ProvideLicensesController.CreateUsers.myMsg: ' + myMsg);
                ApexPages.addMessage(myMsg);
            }

        }
        
        Database.Upsertresult[] results = Database.upsert(usersToUpsert, false);

        for(Integer rIndex = 0; rIndex < results.size(); rIndex++) {
            Database.Upsertresult r = results[rIndex];
            if(!r.isSuccess()) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error ,
                    'User not created for contact: ' + usersToUpsert[rIndex].FirstName + ' ' + usersToUpsert[rIndex].LastName + ', username:' + usersToUpsert[rIndex].UserName + ' -- error');
                System.debug('ProvideLicensesController.CreateUsers.myMsg.userNotCreated: ' + myMsg);
                ApexPages.addMessage(myMsg);
                
                for(Database.Error e : r.getErrors()) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error ,e.getMessage()));
                }
            }
        }
        
        return null;
    }
    
    public class ProvideLicensesPageException extends Exception {}
    */
}