/**
 * @description This is a batch class specifically for the massive bulk update that needs to be done for SFP-1150 where
 *              we will be populating the new Family Document Verification Requested Date field to clearly identify
 *              family documents that have already been verified.
 */
public without sharing class Sfp1150FamilyDocUpdateBatch implements Database.Batchable<SObject> {

    private static final String DOC_STATUS_PROCESSED = 'Processed';
    private static final String DOC_SOURCE_MAILED = 'Mailed';
    private static final String DOC_SOURCE_PARENT = 'Uploaded By Parent';
    private static final String DOC_SOURCE_SCHOOL = 'Uploaded By School';

    private Set<String> documentTypes;
    private Set<String> academicYears;
    private Set<String> documentSources;
    private Set<String> documentStatuses;

    /**
     * @description Default constructor that allows you to specify the academic years, whether it is for mailed or uploaded docs, and the doc types to include.
     */
    public Sfp1150FamilyDocUpdateBatch(Set<String> academicYearsToInclude, Boolean isForMailedDocs, Set<String> documentTypesToInclude) {
        academicYears = academicYearsToInclude;
        documentTypes = documentTypesToInclude;

        if (isForMailedDocs == true) {
            documentSources = new Set<String> { DOC_SOURCE_MAILED };
        } else {
            documentSources = new Set<String> { DOC_SOURCE_PARENT, DOC_SOURCE_SCHOOL };
        }
    }

    /**
     * @description Default constructor that allows you to specify the academic years, whether it is for mailed or uploaded docs, the doc types to include, and the document statuses to include.
     */
    public Sfp1150FamilyDocUpdateBatch(Set<String> academicYearsToInclude, Boolean isForMailedDocs, Set<String> documentTypesToInclude, Set<String> documentStatusesToInclude){
        this(academicYearsToInclude, isForMailedDocs, documentTypesToInclude);
        documentStatuses = documentStatusesToInclude;
    }

    /**
     * @description Gets the query locator used to identify the family document records that need to be identified.
     */
    public Database.QueryLocator start(Database.BatchableContext context) {
        // All mailed documents are verified so we need all docs with Document_Source__c = 'Mailed'

        // This bulk update will include lots of records and involve querying child records as well so we might want to have different query locators, one for mailed docs, one for uploaded docs.

        // We also want documents with a verifiable document type that have at least one related SDA with Verify Data set to true.

        // Don't include docs where Deleted__c = false

        return getQueryLocator();
    }

    public void execute(Database.BatchableContext context, List<SObject> scope) {
        List<Family_Document__c> familyDocuments = (List<Family_Document__c>)scope;

        Map<Id, Family_Document__c> familyDocumentsToUpdateById = new Map<Id, Family_Document__c>();

        for (Family_Document__c record : familyDocuments) {
            // If the Verification Requested Date is already populated, then we don't need to do anything to this record so skip it.
            if (record.Verification_Requested_Date__c != null) {
                continue;
            }

            Family_Document__c recordToUpdate = createRecordToUpdate(record);

            if (recordToUpdate != null) {
                familyDocumentsToUpdateById.put(recordToUpdate.Id, recordToUpdate);
            }
        }

        try {
            // We don't need to do W2 dupe checking for this update so make sure this won't run.
            // Not only is it unnecessary, but it will also give us errors since the dupe checking is done using a future call and future calls are not allowed from batches.
            W2DuplicateCheck.isExecuting = true;

            if (!familyDocumentsToUpdateById.isEmpty()) {
                update familyDocumentsToUpdateById.values();
            }
        } catch (Exception e) {
            System.debug('&&& Exception: ' + e);
            System.debug('&&& Exception Message: ' + e.getMessage());
            System.debug('&&& Exception Stack Trace String: ' + e.getStackTraceString());
            throw e;
        }
    }

    public void finish(Database.BatchableContext context) {
        BatchNotificationService.Instance.notifyFailures(context.getJobId(), 'SFP-1150 Bulk Family Document Update');
    }

    private Database.QueryLocator getQueryLocator() {
        if (getDocSourcesToInclude().contains(DOC_SOURCE_MAILED)) {
            return Database.getQueryLocator([SELECT Id, Date_Received__c, Date_Verified__c, Document_Source__c, Verification_Requested_Date__c FROM Family_Document__c WHERE Academic_Year_Picklist__c IN :getAcademicYearsToInclude() AND Document_Source__c = :DOC_SOURCE_MAILED AND Document_Status__c IN :getDocStatusesToInclude() AND Deleted__c = FALSE AND Verification_Requested_Date__c = null]);
        }

        return Database.getQueryLocator([SELECT Id, Date_Received__c, Date_Verified__c, Document_Source__c, Verification_Requested_Date__c, (SELECT Id FROM School_Document_Assignments__r WHERE Verify_Data__c = TRUE) FROM Family_Document__c WHERE Academic_Year_Picklist__c IN :getAcademicYearsToInclude() AND Document_Type__c IN :getDocumentTypesToInclude() AND Document_Status__c IN :getDocStatusesToInclude() AND Document_Source__c IN :getDocSourcesToInclude() AND Deleted__c = FALSE and Verification_Requested_Date__c = null]);
    }

    private Set<String> getDocumentTypesToInclude() {
        if (documentTypes != null) {
            return documentTypes;
        }

        documentTypes = VerificationAction.getVerifiableDocTypes();

        return documentTypes;
    }

    private Set<String> getAcademicYearsToInclude() {
        if (academicYears != null) {
            return academicYears;
        }

        academicYears = new Set<String>();

        return academicYears;
    }

    private Set<String> getDocSourcesToInclude() {
        if (documentSources != null) {
            return documentSources;
        }

        documentSources = new Set<String>();

        return documentSources;
    }

    private Set<String> getDocStatusesToInclude() {
        if (documentStatuses != null) {
            return documentStatuses;
        }

        documentStatuses = new Set<String> { DOC_STATUS_PROCESSED };

        return documentStatuses;
    }

    private Family_Document__c createRecordToUpdate(Family_Document__c recordToCheck) {
        // Check if the documents source is Mailed. If so, we should populate the Verification Requested Date field.
        if (String.isNotBlank(recordToCheck.Document_Source__c) && DOC_SOURCE_MAILED.equalsIgnoreCase(recordToCheck.Document_Source__c)) {
            Family_Document__c recordToUpdate = new Family_Document__c(Id = recordToCheck.Id);

            recordToUpdate.Verification_Requested_Date__c = getDateAndTimeToUse(recordToCheck);

            return recordToUpdate;
        }

        // If we make it this far, we assume the document was uploaded by a school or parent so now we need to check that the record has at least one SDA with verify data set to true.
        // Since the Verify Data filter was included in the query, we just have to see if the related list of records is not empty.
        if (!recordToCheck.School_Document_Assignments__r.isEmpty()) {
            Family_Document__c recordToUpdate = new Family_Document__c(Id = recordToCheck.Id);

            recordToUpdate.Verification_Requested_Date__c = getDateAndTimeToUse(recordToCheck);

            return recordToUpdate;
        }

        return null;
    }

    private DateTime getDateAndTimeToUse(Family_Document__c recordToCheck) {
        Date dateToUse = recordToCheck.Date_Verified__c == null ? recordToCheck.Date_Received__c : recordToCheck.Date_Verified__c;

        Time midnight = Time.newInstance(0, 0, 0, 0);

        return DateTime.newInstance(dateToUse, midnight);
    }
}