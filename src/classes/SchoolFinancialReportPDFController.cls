/**
 * Class: SchoolFinancialReportPDFController
 *
 * Copyright (C) 2015 NAIS
 *
 * Purpose: Recreate the Financial History Report as PDF.
 *
 * It’s pulling dynamically (so the years change each year) the data from this year and if it exists,
 * the last 3 years for this student PFS. The Financial History report is PER PFS
 *
 * Where Referenced:
 *   School Portal - SchoolFinancialHistoryReportPDF page
 *
 * Change History:
 *
 * Developer           Date        JIRA Ref      Description
 * ---------------------------------------------------------------------------------------
 * Andrea Useche      05/14/15     NAIS-1597     Initial Development
 *
 *
 */
public with sharing class SchoolFinancialReportPDFController {
    /*Initialization*/
    public SchoolFinancialReportPDFController() {
    }

    /*End Initialization*/
    /*Properties*/
    public String currentYearStr {get;set;}
    public String previousYear1Str{get;set;}
    public String previousYear2Str{get;set;}
    public String previousYear3Str{get;set;}

    public School_PFS_Assignment__c currentYear {get;set;}
    public School_PFS_Assignment__c previousYear1{get;set;}
    public School_PFS_Assignment__c previousYear2{get;set;}
    public School_PFS_Assignment__c previousYear3{get;set;}

    public School_PFS_Assignment__c YR1_PFSAssignment {get;set;}
    public School_PFS_Assignment__c YR2_PFSAssignment {get;set;}
    public School_PFS_Assignment__c YR3_PFSAssignment {get;set;}

    public SchoolPFSAssignmentAux currentYearAux {get;set;}
    public SchoolPFSAssignmentAux YR1_PFSAssignmentAux {get;set;}
    public SchoolPFSAssignmentAux YR2_PFSAssignmentAux {get;set;}
    public SchoolPFSAssignmentAux YR3_PFSAssignmentAux {get;set;}

    public Student_Folder__c studentFolder {
        get {
            if (studentFolder == null) {
                loadStudentFolder();
            }
            return studentFolder;
        }
        private set;
    }
    public String studentFolderId {
        get{
            // If a folder id is not provided yet try getting one from parameters
            if(studentFolderId == null){
                studentFolderId = ApexPages.currentPage().getParameters().get('studentFolderId');
            }
            return studentFolderId;
        }
        set;
    }
    public String studentId {
        get{

            // If student id is not provided yet try getting one from parameters
            if(studentId == null){
                studentId = ApexPages.currentPage().getParameters().get('studentId');
            }
            return studentId;
        }
        set;
    }
    public String schoolId {
        get{
            // If school id is not provided yet try getting one from parameters
            if(schoolId == null){
                schoolId = ApexPages.currentPage().getParameters().get('schoolId');
            }
            return schoolId;
        }
        set;
    }

    //SFP-274, [G.S]
    public String pfsParentId {
        get{
            // If applicant id is not provided yet try getting one from parameters
            if(pfsParentId == null){
                pfsParentId = ApexPages.currentPage().getParameters().get('pfsParentId');
            }
            return pfsParentId;
        }
        set;
    }
    // wrapper class for SchoolPFSAssignment
    public class SchoolPFSAssignmentAux
    {
        public Decimal Federal_State_FICA_Tax_Allowance {get;set;}
        public Decimal Medical_and_Unusual_Expenses {get;set;}
        public Decimal Total_Contribution {get;set;}
        public Decimal StudentFolder_Total_Contributions{get;set;}
        public Decimal PC_TotalIncome {get;set;}
        public Decimal PC_DiscIncome {get;set;}
        public Decimal Total_Tuition_and_Expenses {get;set;}
        public Decimal Aid_of_Financial_Need {get;set;}
        public Decimal Dif_Aid_of_Financial_Need {get;set;}
        public Decimal Grant_Awarded {get;set;}
        public Decimal UnmetFinancialNeed {get;set;}
        public Decimal Dif_UnmetFinancialNeed{get;set;}
        public SchoolPFSAssignmentAux() {}
    }
    /*End Properties*/

    /*Action Methods*/

    /*End Action Methods*/
    /*Helper Methods*/
    private void loadStudentFolder()
    {
        studentFolder = new Student_Folder__c();
        string academicYear;
        string academicYearStr;
        Integer i=0;
        map<String, list<School_PFS_Assignment__c>> mapStudentFolderByYear = new map<String, list<School_PFS_Assignment__c>>();
        if(studentFolderId != null)
        {
            studentFolder = [SELECT Id, Gender__c, Student_Name__c, Student__r.Name, Student__r.Gender__c, Grade_Applying__c,
                                    New_Returning__c, Day_Boarding__c
                            FROM Student_Folder__c WHERE Id=: studentFolderId limit 1];
        }
        if (studentId != null && schoolId!= null) //Only if the parameters exists!
        {
            list <School_PFS_Assignment__c> lstAssignment = [SELECT Family_Size__c, Num_Children_in_Tuition_Schools__c, Total_Taxable_Income__c, Medical_and_Unusual_Expenses__c ,
                                                                Total_Nontaxable_Income__c, Income_Supplement__c,  Adjusted_Effective_Income_Currency__c,
                                                                Discretionary_Income_Currency__c, Federal_State_FICA_Tax_Allowance__c, Academic_Year_Picklist__c,
                                                                Medical_Allowance__c, Unusual_Expense__c, Income_Protection_Allowance__c,
                                                                Total_Assets_Currency__c, Total_Debts__c, Net_Worth__c, Total_Contribution__c,
                                                                Orig_Parent_Can_Afford_to_Pay__c, Orig_Parent_Can_Afford_to_Pay_All_Childr__c, //
                                                                Student_Folder__r.Total_Contributions__c,
                                                                Student_Folder__r.Total_Tuition_and_Expenses__c, Student_Folder__r.Aid_of_Financial_Need__c,
                                                                Student_Folder__r.Grant_Awarded__c, Student_Folder__r.Unmet_Financial_Need__c,
                                                                Student_Folder__r.Academic_Year_Picklist__c,Applicant__r.pfs__c
                                                            FROM School_PFS_Assignment__c WHERE Applicant__r.Contact__c =: studentId
                                                            AND School__c= : schoolId and
                                                                applicant__r.Pfs__r.Parent_A__c = :pfsParentId
                                                            order by Academic_Year_Picklist__c desc ];

            for(School_PFS_Assignment__c sAssignment : lstAssignment)
            {
                if(i==0) academicYear = sAssignment.Academic_Year_Picklist__c;
                academicYearStr=GlobalVariables.getNextDocYearStringFromAcadYearName(sAssignment.Academic_Year_Picklist__c);
                if(!mapStudentFolderByYear.containsKey(academicYearStr))
                {
                    list <School_PFS_Assignment__c> lstAuxAssignment = new list <School_PFS_Assignment__c>();
                    lstAuxAssignment.add(sAssignment);
                    mapStudentFolderByYear.put(academicYearStr, lstAuxAssignment);
                }
                else
                {
                    list <School_PFS_Assignment__c> lstAuxAssignment = new list <School_PFS_Assignment__c>();
                    lstAuxAssignment= mapStudentFolderByYear.get(academicYearStr);
                    lstAuxAssignment.add(sAssignment);
                    mapStudentFolderByYear.put(academicYearStr, lstAuxAssignment);
                }
                i++;
            }
            if(mapStudentFolderByYear!=null)
                loadAcademicYear(mapStudentFolderByYear, academicYear);
        }

    }//End loadStudentFolder
    private void loadAcademicYear(map<String, list<School_PFS_Assignment__c>> mapStudentFolderByYear, string academicYear)
    {
        Datetime currentAcademicYear = Datetime.now();
        if(academicYear == null || academicYear=='') academicYear= currentAcademicYear.year()+'-'+currentAcademicYear.addYears(1).year();

        currentYearStr= GlobalVariables.getNextDocYearStringFromAcadYearName(academicYear);
        currentYear = (mapStudentFolderByYear.containsKey(currentYearStr)? mapStudentFolderByYear.get(currentYearStr).get(0): new School_PFS_Assignment__c());

        previousYear1Str= GlobalVariables.getDocYearStringFromAcadYearName(academicYear);
        previousYear1 = (mapStudentFolderByYear.containsKey(previousYear1Str)? mapStudentFolderByYear.get(previousYear1Str).get(0): new School_PFS_Assignment__c());

        previousYear2Str= GlobalVariables.getPrevDocYearStringFromAcadYearName(academicYear);
        previousYear2 = (mapStudentFolderByYear.containsKey(previousYear2Str)? mapStudentFolderByYear.get(previousYear2Str).get(0): new School_PFS_Assignment__c());

        previousYear3Str= GlobalVariables.getDocYearStringFromAcadYearName(previousYear2Str+'-'+previousYear1Str);
        previousYear3 = (mapStudentFolderByYear.containsKey(previousYear3Str)? mapStudentFolderByYear.get(previousYear3Str).get(0): new School_PFS_Assignment__c());

        loadTotalsByAcademicYear();
    }
    private void loadTotalsByAcademicYear()
    {
        YR1_PFSAssignment = new School_PFS_Assignment__c();
        YR2_PFSAssignment = new School_PFS_Assignment__c();
        YR3_PFSAssignment = new School_PFS_Assignment__c();

        currentYearAux = new SchoolPFSAssignmentAux();
        YR1_PFSAssignmentAux = new SchoolPFSAssignmentAux();
        YR2_PFSAssignmentAux = new SchoolPFSAssignmentAux();
        YR3_PFSAssignmentAux = new SchoolPFSAssignmentAux();

        currentYear.Family_Size__c = (currentYear.Family_Size__c!=null)?currentYear.Family_Size__c: 0;
        YR1_PFSAssignment.Family_Size__c = (previousYear1.Family_Size__c!=null)?(currentYear.Family_Size__c - previousYear1.Family_Size__c):currentYear.Family_Size__c;
        YR2_PFSAssignment.Family_Size__c = (previousYear2.Family_Size__c!=null)?(currentYear.Family_Size__c - previousYear2.Family_Size__c):currentYear.Family_Size__c;
        YR3_PFSAssignment.Family_Size__c = (previousYear3.Family_Size__c!=null)?(currentYear.Family_Size__c - previousYear3.Family_Size__c):currentYear.Family_Size__c;

        currentYear.Num_Children_in_Tuition_Schools__c = (currentYear.Num_Children_in_Tuition_Schools__c !=null)? currentyear.Num_Children_in_Tuition_Schools__c :0;
        YR1_PFSAssignment.Num_Children_in_Tuition_Schools__c = (previousYear1.Num_Children_in_Tuition_Schools__c!=null)?(currentYear.Num_Children_in_Tuition_Schools__c - previousYear1.Num_Children_in_Tuition_Schools__c):currentYear.Num_Children_in_Tuition_Schools__c;
        YR2_PFSAssignment.Num_Children_in_Tuition_Schools__c = (previousYear2.Num_Children_in_Tuition_Schools__c!=null)?(currentYear.Num_Children_in_Tuition_Schools__c - previousYear2.Num_Children_in_Tuition_Schools__c):currentYear.Num_Children_in_Tuition_Schools__c;
        YR3_PFSAssignment.Num_Children_in_Tuition_Schools__c = (previousYear3.Num_Children_in_Tuition_Schools__c!=null)?(currentYear.Num_Children_in_Tuition_Schools__c - previousYear3.Num_Children_in_Tuition_Schools__c):currentYear.Num_Children_in_Tuition_Schools__c;

        currentYear.Total_Taxable_Income__c = (currentYear.Total_Taxable_Income__c != null)?currentYear.Total_Taxable_Income__c :0;
        YR1_PFSAssignment.Total_Taxable_Income__c = (previousYear1.Total_Taxable_Income__c!=null)?(currentYear.Total_Taxable_Income__c - previousYear1.Total_Taxable_Income__c):currentYear.Total_Taxable_Income__c;
        YR2_PFSAssignment.Total_Taxable_Income__c = (previousYear2.Total_Taxable_Income__c!=null)?(currentYear.Total_Taxable_Income__c - previousYear2.Total_Taxable_Income__c):currentYear.Total_Taxable_Income__c;
        YR3_PFSAssignment.Total_Taxable_Income__c = (previousYear3.Total_Taxable_Income__c!=null)?(currentYear.Total_Taxable_Income__c - previousYear3.Total_Taxable_Income__c):currentYear.Total_Taxable_Income__c;

           currentYear.Total_Nontaxable_Income__c = (currentYear.Total_Nontaxable_Income__c != null)?currentYear.Total_Nontaxable_Income__c :0;
        YR1_PFSAssignment.Total_Nontaxable_Income__c = (previousYear1.Total_Nontaxable_Income__c!=null)?(currentYear.Total_Nontaxable_Income__c - previousYear1.Total_Nontaxable_Income__c):currentYear.Total_Nontaxable_Income__c;
        YR2_PFSAssignment.Total_Nontaxable_Income__c = (previousYear2.Total_Nontaxable_Income__c!=null)?(currentYear.Total_Nontaxable_Income__c - previousYear2.Total_Nontaxable_Income__c):currentYear.Total_Nontaxable_Income__c;
        YR3_PFSAssignment.Total_Nontaxable_Income__c = (previousYear3.Total_Nontaxable_Income__c!=null)?(currentYear.Total_Nontaxable_Income__c - previousYear3.Total_Nontaxable_Income__c):currentYear.Total_Nontaxable_Income__c;

        currentYear.Income_Supplement__c = (currentYear.Income_Supplement__c != null)?currentYear.Income_Supplement__c :0;
        YR1_PFSAssignment.Income_Supplement__c = (previousYear1.Income_Supplement__c!=null)?(currentYear.Income_Supplement__c - previousYear1.Income_Supplement__c):currentYear.Income_Supplement__c;
        YR2_PFSAssignment.Income_Supplement__c = (previousYear2.Income_Supplement__c!=null)?(currentYear.Income_Supplement__c - previousYear2.Income_Supplement__c):currentYear.Income_Supplement__c;
        YR3_PFSAssignment.Income_Supplement__c = (previousYear3.Income_Supplement__c!=null)?(currentYear.Income_Supplement__c - previousYear3.Income_Supplement__c):currentYear.Income_Supplement__c;

           currentYear.Adjusted_Effective_Income_Currency__c = (currentYear.Adjusted_Effective_Income_Currency__c != null)?currentYear.Adjusted_Effective_Income_Currency__c :0;
        YR1_PFSAssignment.Adjusted_Effective_Income_Currency__c = (previousYear1.Adjusted_Effective_Income_Currency__c!=null)?(currentYear.Adjusted_Effective_Income_Currency__c - previousYear1.Adjusted_Effective_Income_Currency__c):currentYear.Adjusted_Effective_Income_Currency__c;
        YR2_PFSAssignment.Adjusted_Effective_Income_Currency__c = (previousYear2.Adjusted_Effective_Income_Currency__c!=null)?(currentYear.Adjusted_Effective_Income_Currency__c - previousYear2.Adjusted_Effective_Income_Currency__c):currentYear.Adjusted_Effective_Income_Currency__c;
        YR3_PFSAssignment.Adjusted_Effective_Income_Currency__c = (previousYear3.Adjusted_Effective_Income_Currency__c!=null)?(currentYear.Adjusted_Effective_Income_Currency__c - previousYear3.Adjusted_Effective_Income_Currency__c):currentYear.Adjusted_Effective_Income_Currency__c;

           currentYear.Discretionary_Income_Currency__c = (currentYear.Discretionary_Income_Currency__c != null)?currentYear.Discretionary_Income_Currency__c :0;
        YR1_PFSAssignment.Discretionary_Income_Currency__c = (previousYear1.Discretionary_Income_Currency__c!=null)?(currentYear.Discretionary_Income_Currency__c - previousYear1.Discretionary_Income_Currency__c):currentYear.Discretionary_Income_Currency__c;
        YR2_PFSAssignment.Discretionary_Income_Currency__c = (previousYear2.Discretionary_Income_Currency__c!=null)?(currentYear.Discretionary_Income_Currency__c - previousYear2.Discretionary_Income_Currency__c):currentYear.Discretionary_Income_Currency__c;
        YR3_PFSAssignment.Discretionary_Income_Currency__c = (previousYear3.Discretionary_Income_Currency__c!=null)?(currentYear.Discretionary_Income_Currency__c - previousYear3.Discretionary_Income_Currency__c):currentYear.Discretionary_Income_Currency__c;

           //Use the wrapperClass for Federal_State_FICA_Tax_Allowance__c because this field is not writeable.
           currentYearAux.Federal_State_FICA_Tax_Allowance = (currentYear.Federal_State_FICA_Tax_Allowance__c != null)?currentYear.Federal_State_FICA_Tax_Allowance__c :0;
        YR1_PFSAssignmentAux.Federal_State_FICA_Tax_Allowance = (previousYear1.Federal_State_FICA_Tax_Allowance__c!=null)?(currentYearAux.Federal_State_FICA_Tax_Allowance - previousYear1.Federal_State_FICA_Tax_Allowance__c):currentYearAux.Federal_State_FICA_Tax_Allowance;
        YR2_PFSAssignmentAux.Federal_State_FICA_Tax_Allowance = (previousYear2.Federal_State_FICA_Tax_Allowance__c!=null)?(currentYearAux.Federal_State_FICA_Tax_Allowance - previousYear2.Federal_State_FICA_Tax_Allowance__c):currentYearAux.Federal_State_FICA_Tax_Allowance;
        YR3_PFSAssignmentAux.Federal_State_FICA_Tax_Allowance = (previousYear3.Federal_State_FICA_Tax_Allowance__c!=null)?(currentYearAux.Federal_State_FICA_Tax_Allowance - previousYear3.Federal_State_FICA_Tax_Allowance__c):currentYearAux.Federal_State_FICA_Tax_Allowance;

           //Use the warpperClass for Medical_and_Unusual_Expenses__c because this field is not writeable.
           currentYearAux.Medical_and_Unusual_Expenses = (currentYear.Medical_and_Unusual_Expenses__c != null)?currentYear.Medical_and_Unusual_Expenses__c :0;
        YR1_PFSAssignmentAux.Medical_and_Unusual_Expenses = (previousYear1.Medical_and_Unusual_Expenses__c!=null)?(currentYearAux.Medical_and_Unusual_Expenses - previousYear1.Medical_and_Unusual_Expenses__c):currentYearAux.Medical_and_Unusual_Expenses;
        YR2_PFSAssignmentAux.Medical_and_Unusual_Expenses = (previousYear2.Medical_and_Unusual_Expenses__c!=null)?(currentYearAux.Medical_and_Unusual_Expenses - previousYear2.Medical_and_Unusual_Expenses__c):currentYearAux.Medical_and_Unusual_Expenses;
        YR3_PFSAssignmentAux.Medical_and_Unusual_Expenses = (previousYear3.Medical_and_Unusual_Expenses__c!=null)?(currentYearAux.Medical_and_Unusual_Expenses - previousYear3.Medical_and_Unusual_Expenses__c):currentYearAux.Medical_and_Unusual_Expenses;

        currentYear.Income_Protection_Allowance__c = (currentYear.Income_Protection_Allowance__c != null)?currentYear.Income_Protection_Allowance__c :0;
        YR1_PFSAssignment.Income_Protection_Allowance__c = (previousYear1.Income_Protection_Allowance__c!=null)?(currentYear.Income_Protection_Allowance__c - previousYear1.Income_Protection_Allowance__c):currentYear.Income_Protection_Allowance__c;
        YR2_PFSAssignment.Income_Protection_Allowance__c = (previousYear2.Income_Protection_Allowance__c!=null)?(currentYear.Income_Protection_Allowance__c - previousYear2.Income_Protection_Allowance__c):currentYear.Income_Protection_Allowance__c;
        YR3_PFSAssignment.Income_Protection_Allowance__c = (previousYear3.Income_Protection_Allowance__c!=null)?(currentYear.Income_Protection_Allowance__c - previousYear3.Income_Protection_Allowance__c):currentYear.Income_Protection_Allowance__c;

           currentYear.Total_Assets_Currency__c = (currentYear.Total_Assets_Currency__c != null)?currentYear.Total_Assets_Currency__c :0;
        YR1_PFSAssignment.Total_Assets_Currency__c = (previousYear1.Total_Assets_Currency__c!=null)?(currentYear.Total_Assets_Currency__c - previousYear1.Total_Assets_Currency__c):currentYear.Total_Assets_Currency__c;
        YR2_PFSAssignment.Total_Assets_Currency__c = (previousYear2.Total_Assets_Currency__c!=null)?(currentYear.Total_Assets_Currency__c - previousYear2.Total_Assets_Currency__c):currentYear.Total_Assets_Currency__c;
        YR3_PFSAssignment.Total_Assets_Currency__c = (previousYear3.Total_Assets_Currency__c!=null)?(currentYear.Total_Assets_Currency__c - previousYear3.Total_Assets_Currency__c):currentYear.Total_Assets_Currency__c;

           currentYear.Total_Debts__c = (currentYear.Total_Debts__c != null)?currentYear.Total_Debts__c :0;
        YR1_PFSAssignment.Total_Debts__c = (previousYear1.Total_Debts__c!=null)?(currentYear.Total_Debts__c - previousYear1.Total_Debts__c):currentYear.Total_Debts__c;
        YR2_PFSAssignment.Total_Debts__c = (previousYear2.Total_Debts__c!=null)?(currentYear.Total_Debts__c - previousYear2.Total_Debts__c):currentYear.Total_Debts__c;
        YR3_PFSAssignment.Total_Debts__c = (previousYear3.Total_Debts__c!=null)?(currentYear.Total_Debts__c - previousYear3.Total_Debts__c):currentYear.Total_Debts__c;

        currentYear.Net_Worth__c = (currentYear.Net_Worth__c != null)?currentYear.Net_Worth__c :0;
        YR1_PFSAssignment.Net_Worth__c = (previousYear1.Net_Worth__c!=null)?(currentYear.Net_Worth__c - previousYear1.Net_Worth__c):currentYear.Net_Worth__c;
        YR2_PFSAssignment.Net_Worth__c = (previousYear2.Net_Worth__c!=null)?(currentYear.Net_Worth__c - previousYear2.Net_Worth__c):currentYear.Net_Worth__c;
        YR3_PFSAssignment.Net_Worth__c = (previousYear3.Net_Worth__c!=null)?(currentYear.Net_Worth__c - previousYear3.Net_Worth__c):currentYear.Net_Worth__c;

        //Use the wrapperClass for Total_Contribution__c because this field is not writeable
        currentYearAux.Total_Contribution = (currentYear.Total_Contribution__c != null)?currentYear.Total_Contribution__c :0;
        YR1_PFSAssignmentAux.Total_Contribution = (previousYear1.Total_Contribution__c!=null)?(currentYearAux.Total_Contribution - previousYear1.Total_Contribution__c):currentYearAux.Total_Contribution;
        YR2_PFSAssignmentAux.Total_Contribution = (previousYear2.Total_Contribution__c!=null)?(currentYearAux.Total_Contribution - previousYear2.Total_Contribution__c):currentYearAux.Total_Contribution;
        YR3_PFSAssignmentAux.Total_Contribution = (previousYear3.Total_Contribution__c!=null)?(currentYearAux.Total_Contribution - previousYear3.Total_Contribution__c):currentYearAux.Total_Contribution;

        //Use the wrapperClasss for Student_Folder__r.Total_Contributions__c because this field is not writeable
        currentYearAux.StudentFolder_Total_Contributions = (currentYear.Student_Folder__r.Total_Contributions__c != null)?currentYear.Student_Folder__r.Total_Contributions__c :0;
        YR1_PFSAssignmentAux.StudentFolder_Total_Contributions = (previousYear1.Student_Folder__r.Total_Contributions__c!=null)?(currentYearAux.StudentFolder_Total_Contributions - previousYear1.Student_Folder__r.Total_Contributions__c):currentYearAux.StudentFolder_Total_Contributions;
        YR2_PFSAssignmentAux.StudentFolder_Total_Contributions = (previousYear2.Student_Folder__r.Total_Contributions__c!=null)?(currentYearAux.StudentFolder_Total_Contributions - previousYear2.Student_Folder__r.Total_Contributions__c):currentYearAux.StudentFolder_Total_Contributions;
        YR3_PFSAssignmentAux.StudentFolder_Total_Contributions = (previousYear3.Student_Folder__r.Total_Contributions__c!=null)?(currentYearAux.StudentFolder_Total_Contributions - previousYear3.Student_Folder__r.Total_Contributions__c):currentYearAux.StudentFolder_Total_Contributions;

        currentYear.Orig_Parent_Can_Afford_to_Pay__c = (currentYear.Orig_Parent_Can_Afford_to_Pay__c != null)?currentYear.Orig_Parent_Can_Afford_to_Pay__c :0;
        YR1_PFSAssignment.Orig_Parent_Can_Afford_to_Pay__c = (previousYear1.Orig_Parent_Can_Afford_to_Pay__c!=null)?(currentYear.Orig_Parent_Can_Afford_to_Pay__c - previousYear1.Orig_Parent_Can_Afford_to_Pay__c):currentYear.Orig_Parent_Can_Afford_to_Pay__c;
        YR2_PFSAssignment.Orig_Parent_Can_Afford_to_Pay__c = (previousYear2.Orig_Parent_Can_Afford_to_Pay__c!=null)?(currentYear.Orig_Parent_Can_Afford_to_Pay__c - previousYear2.Orig_Parent_Can_Afford_to_Pay__c):currentYear.Orig_Parent_Can_Afford_to_Pay__c;
        YR3_PFSAssignment.Orig_Parent_Can_Afford_to_Pay__c = (previousYear3.Orig_Parent_Can_Afford_to_Pay__c!=null)?(currentYear.Orig_Parent_Can_Afford_to_Pay__c - previousYear3.Orig_Parent_Can_Afford_to_Pay__c):currentYear.Orig_Parent_Can_Afford_to_Pay__c;

           currentYear.Orig_Parent_Can_Afford_to_Pay_All_Childr__c = (currentYear.Orig_Parent_Can_Afford_to_Pay_All_Childr__c != null)?currentYear.Orig_Parent_Can_Afford_to_Pay_All_Childr__c :0;
        YR1_PFSAssignment.Orig_Parent_Can_Afford_to_Pay_All_Childr__c = (previousYear1.Orig_Parent_Can_Afford_to_Pay_All_Childr__c!=null)?(currentYear.Orig_Parent_Can_Afford_to_Pay_All_Childr__c - previousYear1.Orig_Parent_Can_Afford_to_Pay_All_Childr__c):currentYear.Orig_Parent_Can_Afford_to_Pay_All_Childr__c;
        YR2_PFSAssignment.Orig_Parent_Can_Afford_to_Pay_All_Childr__c = (previousYear2.Orig_Parent_Can_Afford_to_Pay_All_Childr__c!=null)?(currentYear.Orig_Parent_Can_Afford_to_Pay_All_Childr__c - previousYear2.Orig_Parent_Can_Afford_to_Pay_All_Childr__c):currentYear.Orig_Parent_Can_Afford_to_Pay_All_Childr__c;
        YR3_PFSAssignment.Orig_Parent_Can_Afford_to_Pay_All_Childr__c = (previousYear3.Orig_Parent_Can_Afford_to_Pay_All_Childr__c!=null)?(currentYear.Orig_Parent_Can_Afford_to_Pay_All_Childr__c - previousYear3.Orig_Parent_Can_Afford_to_Pay_All_Childr__c):currentYear.Orig_Parent_Can_Afford_to_Pay_All_Childr__c;

           currentYearAux.PC_TotalIncome= (currentYear.Total_Taxable_Income__c!= null && currentYear.Total_Taxable_Income__c !=0)?(currentYearAux.Total_Contribution/currentYear.Total_Taxable_Income__c):0;
           YR1_PFSAssignmentAux.PC_TotalIncome= currentYearAux.PC_TotalIncome - ((previousYear1.Total_Taxable_Income__c !=null && previousYear1.Total_Taxable_Income__c !=0)?(((previousYear1.Total_Contribution__c!=null)?previousYear1.Total_Contribution__c:0)/previousYear1.Total_Taxable_Income__c):0);
           YR2_PFSAssignmentAux.PC_TotalIncome= currentYearAux.PC_TotalIncome - ((previousYear2.Total_Taxable_Income__c !=null && previousYear2.Total_Taxable_Income__c !=0)?(((previousYear2.Total_Contribution__c!=null)?previousYear2.Total_Contribution__c:0)/previousYear2.Total_Taxable_Income__c):0);
           YR3_PFSAssignmentAux.PC_TotalIncome= currentYearAux.PC_TotalIncome - ((previousYear3.Total_Taxable_Income__c !=null && previousYear3.Total_Taxable_Income__c !=0)?(((previousYear3.Total_Contribution__c!=null)?previousYear3.Total_Contribution__c:0)/previousYear3.Total_Taxable_Income__c):0);

           currentYearAux.PC_DiscIncome = (currentYear.Discretionary_Income_Currency__c!= null && currentYear.Discretionary_Income_Currency__c !=0)?(currentYearAux.Total_Contribution/currentYear.Discretionary_Income_Currency__c):0;
           YR1_PFSAssignmentAux.PC_DiscIncome= currentYearAux.PC_DiscIncome - ((previousYear1.Discretionary_Income_Currency__c !=null && previousYear1.Discretionary_Income_Currency__c !=0)?(((previousYear1.Total_Contribution__c!=null)?previousYear1.Total_Contribution__c:0)/previousYear1.Discretionary_Income_Currency__c):0);
           YR2_PFSAssignmentAux.PC_DiscIncome= currentYearAux.PC_DiscIncome - ((previousYear2.Discretionary_Income_Currency__c !=null && previousYear2.Discretionary_Income_Currency__c !=0)?(((previousYear2.Total_Contribution__c!=null)?previousYear2.Total_Contribution__c:0)/previousYear2.Discretionary_Income_Currency__c):0);
           YR3_PFSAssignmentAux.PC_DiscIncome= currentYearAux.PC_DiscIncome - ((previousYear3.Discretionary_Income_Currency__c !=null && previousYear3.Discretionary_Income_Currency__c !=0)?(((previousYear3.Total_Contribution__c!=null)?previousYear3.Total_Contribution__c:0)/previousYear3.Discretionary_Income_Currency__c):0);

           currentYearAux.Total_Tuition_and_Expenses = (currentYear.Student_Folder__r.Total_Tuition_and_Expenses__c != null)? currentYear.Student_Folder__r.Total_Tuition_and_Expenses__c: 0;
           YR1_PFSAssignmentAux.Total_Tuition_and_Expenses = (previousYear1.Student_Folder__r.Total_Tuition_and_Expenses__c!=null)?(currentYearAux.Total_Tuition_and_Expenses - previousYear1.Student_Folder__r.Total_Tuition_and_Expenses__c):currentYearAux.Total_Tuition_and_Expenses;
        YR2_PFSAssignmentAux.Total_Tuition_and_Expenses = (previousYear2.Student_Folder__r.Total_Tuition_and_Expenses__c!=null)?(currentYearAux.Total_Tuition_and_Expenses - previousYear2.Student_Folder__r.Total_Tuition_and_Expenses__c):currentYearAux.Total_Tuition_and_Expenses;
        YR3_PFSAssignmentAux.Total_Tuition_and_Expenses = (previousYear3.Student_Folder__r.Total_Tuition_and_Expenses__c!=null)?(currentYearAux.Total_Tuition_and_Expenses - previousYear3.Student_Folder__r.Total_Tuition_and_Expenses__c):currentYearAux.Total_Tuition_and_Expenses;

        currentYearAux.Aid_of_Financial_Need = (currentYear.Total_Contribution__c != null)?currentYear.Student_Folder__r.Total_Tuition_and_Expenses__c - currentYear.Total_Contribution__c: currentYear.Student_Folder__r.Total_Tuition_and_Expenses__c;
        YR1_PFSAssignmentAux.Aid_of_Financial_Need = (previousYear1.Total_Contribution__c != null)?previousYear1.Student_Folder__r.Total_Tuition_and_Expenses__c - previousYear1.Total_Contribution__c: previousYear1.Student_Folder__r.Total_Tuition_and_Expenses__c;
        YR2_PFSAssignmentAux.Aid_of_Financial_Need = (previousYear2.Total_Contribution__c != null)?previousYear2.Student_Folder__r.Total_Tuition_and_Expenses__c - previousYear2.Total_Contribution__c: previousYear2.Student_Folder__r.Total_Tuition_and_Expenses__c;
        YR3_PFSAssignmentAux.Aid_of_Financial_Need = (previousYear3.Total_Contribution__c != null)?previousYear3.Student_Folder__r.Total_Tuition_and_Expenses__c - previousYear3.Total_Contribution__c: previousYear3.Student_Folder__r.Total_Tuition_and_Expenses__c;

           YR1_PFSAssignmentAux.Dif_Aid_of_Financial_Need =  (YR1_PFSAssignmentAux.Aid_of_Financial_Need != null)? (currentYearAux.Aid_of_Financial_Need - YR1_PFSAssignmentAux.Aid_of_Financial_Need):currentYearAux.Aid_of_Financial_Need;
           YR2_PFSAssignmentAux.Dif_Aid_of_Financial_Need =  (YR2_PFSAssignmentAux.Aid_of_Financial_Need != null)? (currentYearAux.Aid_of_Financial_Need - YR2_PFSAssignmentAux.Aid_of_Financial_Need):currentYearAux.Aid_of_Financial_Need;
           YR3_PFSAssignmentAux.Dif_Aid_of_Financial_Need =  (YR3_PFSAssignmentAux.Aid_of_Financial_Need != null)? (currentYearAux.Aid_of_Financial_Need - YR3_PFSAssignmentAux.Aid_of_Financial_Need):currentYearAux.Aid_of_Financial_Need;

           currentYearAux.Grant_Awarded = (currentYear.Student_Folder__r.Grant_Awarded__c != null)? currentYear.Student_Folder__r.Grant_Awarded__c: 0;
           YR1_PFSAssignmentAux.Grant_Awarded = (previousYear1.Student_Folder__r.Grant_Awarded__c!=null)?(currentYearAux.Grant_Awarded - previousYear1.Student_Folder__r.Grant_Awarded__c):currentYearAux.Grant_Awarded;
        YR2_PFSAssignmentAux.Grant_Awarded = (previousYear2.Student_Folder__r.Grant_Awarded__c!=null)?(currentYearAux.Grant_Awarded - previousYear2.Student_Folder__r.Grant_Awarded__c):currentYearAux.Grant_Awarded;
        YR3_PFSAssignmentAux.Grant_Awarded = (previousYear3.Student_Folder__r.Grant_Awarded__c!=null)?(currentYearAux.Grant_Awarded - previousYear3.Student_Folder__r.Grant_Awarded__c):currentYearAux.Grant_Awarded;

           currentYearAux.UnmetFinancialNeed = (currentYear.Student_Folder__r.Grant_Awarded__c!= null)?(currentYearAux.Aid_of_Financial_Need - currentYear.Student_Folder__r.Grant_Awarded__c):currentYearAux.Aid_of_Financial_Need;
           YR1_PFSAssignmentAux.UnmetFinancialNeed = (previousYear1.Student_Folder__r.Grant_Awarded__c!= null)?(YR1_PFSAssignmentAux.Aid_of_Financial_Need - previousYear1.Student_Folder__r.Grant_Awarded__c):YR1_PFSAssignmentAux.Aid_of_Financial_Need;
           YR2_PFSAssignmentAux.UnmetFinancialNeed = (previousYear2.Student_Folder__r.Grant_Awarded__c!= null)?(YR2_PFSAssignmentAux.Aid_of_Financial_Need - previousYear2.Student_Folder__r.Grant_Awarded__c):YR2_PFSAssignmentAux.Aid_of_Financial_Need;
           YR3_PFSAssignmentAux.UnmetFinancialNeed = (previousYear3.Student_Folder__r.Grant_Awarded__c!= null)?(YR3_PFSAssignmentAux.Aid_of_Financial_Need - previousYear3.Student_Folder__r.Grant_Awarded__c):YR3_PFSAssignmentAux.Aid_of_Financial_Need;

           YR1_PFSAssignmentAux.Dif_UnmetFinancialNeed = (YR1_PFSAssignmentAux.UnmetFinancialNeed!= null)?(currentYearAux.UnmetFinancialNeed - YR1_PFSAssignmentAux.UnmetFinancialNeed):currentYearAux.UnmetFinancialNeed;
           YR2_PFSAssignmentAux.Dif_UnmetFinancialNeed = (YR2_PFSAssignmentAux.UnmetFinancialNeed!= null)?(currentYearAux.UnmetFinancialNeed - YR2_PFSAssignmentAux.UnmetFinancialNeed):currentYearAux.UnmetFinancialNeed;
           YR3_PFSAssignmentAux.Dif_UnmetFinancialNeed = (YR3_PFSAssignmentAux.UnmetFinancialNeed!= null)?(currentYearAux.UnmetFinancialNeed - YR3_PFSAssignmentAux.UnmetFinancialNeed):currentYearAux.UnmetFinancialNeed;

    }
}