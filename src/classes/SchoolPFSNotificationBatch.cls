/*
 * Spec-096 School Portal - School PFS - Notification, Req# R-425, R-429
 *  An option Account.Alert_Frequency__c sets the frequency of PFS Submission notification: Once per Day, Once per Week.
 *
 *  Whenever a PFS is submitted, each school on the School PFS Assignments is checked to see what their frequency is, and when the last send was.
 *  If the last email send (read-only field Account.Last_PFS_Notification_Date__c) is more than the day or week depending on the frequency, all of the PFS
 *  records for that time period are grabbed (based on Submission Date) and listed in the email. Once the email is sent, Account.Last_PFS_Notification_Date__c
 *  is updated.
 *
 *  Recipients are all staff contacts at that school who are set to receive email: Contact.PFS_Alert_Preferences__c.
 *
 * WH, Exponent Partners, 2013
 */
global class SchoolPFSNotificationBatch implements Database.Batchable<sObject>, Schedulable, Database.Stateful {
    global List<String> errorMessages = new List<String>();
    /*
     * Scheduleable
     */
    
    global void execute(SchedulableContext sc) {
        // limit batch size to send email limit
        SchoolPFSNotificationBatch notificationBatch = new SchoolPFSNotificationBatch();
        Database.executeBatch(notificationBatch, 1);
    }
    
    /*
     * Batchable
     */
    
    global final Date thisDay;
    global final Date sevenDaysAgo;
    global final Set<String> activeSubscriberStatuses;
    global final Set<Id> schoolIds;
    global final String query;
    
    global SchoolPFSNotificationBatch(Set<Id> ids) {
        thisDay = System.today();
        sevenDaysAgo = System.today() - 7;
        activeSubscriberStatuses = GlobalVariables.activeSSSStatuses;
        schoolIds = ids;
        // find all school accounts that are due to be alerted based on their alert frequency       
        query = 'select Id, Name, Last_PFS_Notification_Date__c, Alert_Frequency__c ' + 
                    'from Account where RecordTypeId = ' + '\'' + RecordTypes.schoolAccountTypeId + '\' and ' +
                    (schoolIds!=null && !schoolIds.isEmpty() ? ' id IN :schoolIds and ' : '')+
                    'SSS_Subscriber_Status__c in :activeSubscriberStatuses and ' +
                    'Alert_Frequency__c != null and Alert_Frequency__c != \'No Alerts\' and ' +
                    '((Alert_Frequency__c = \'Daily digest of new applications\' and ' +
                    '(Last_PFS_Notification_Date__c = null or Last_PFS_Notification_Date__c < :thisDay)) or ' +
                    '(Alert_Frequency__c = \'Weekly digest of new applications\' and ' +
                    '(Last_PFS_Notification_Date__c = null or Last_PFS_Notification_Date__c < :sevenDaysAgo)))';
    }
    
    global SchoolPFSNotificationBatch() {
        // find all school accounts that are due to be alerted based on their alert frequency
        thisDay = System.today();
        sevenDaysAgo = System.today() - 7;
        activeSubscriberStatuses = GlobalVariables.activeSSSStatuses;
        query = 'select Id, Name, Last_PFS_Notification_Date__c, Alert_Frequency__c ' + 
                    'from Account where RecordTypeId = ' + '\'' + RecordTypes.schoolAccountTypeId + '\' and ' +
                    'SSS_Subscriber_Status__c in :activeSubscriberStatuses and ' +
                    'Alert_Frequency__c != null and Alert_Frequency__c != \'No Alerts\' and ' +
                    '((Alert_Frequency__c = \'Daily digest of new applications\' and ' +
                    '(Last_PFS_Notification_Date__c = null or Last_PFS_Notification_Date__c < :thisDay)) or ' +
                    '(Alert_Frequency__c = \'Weekly digest of new applications\' and ' +
                    '(Last_PFS_Notification_Date__c = null or Last_PFS_Notification_Date__c < :sevenDaysAgo)))';
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        Map<Id, Account> accounts = new Map<Id, Account>();
        for (sObject s : scope) {
            accounts.put(s.Id, (Account)s);
        }
        
        try{
          SchoolPFSNotification.sendNotification(accounts);
        }catch(Exception e) {
            errorMessages.add('\n\t\t - Exception Type: ' + e.getTypeName() + 
                '. Exception Message: ' + e.getMessage() + 
                '. Stack Trace: ' + e.getStackTraceString());
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        
        if( !errorMessages.isEmpty() ) {
            BatchNotificationService.Instance.notifyFailures(bc.getJobId(), 'School PFS Notification', String.Join(errorMessages, '\n') );
        }
    }
    
}