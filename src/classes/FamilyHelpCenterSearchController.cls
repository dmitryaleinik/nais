/**
 * FamilyHelpCenterSearchController.cls
 *
 * @description Controller for FamilyHelpCenterSearch.component
 *
 * @author Mike Havrilla @ Presence PG
 */

public class FamilyHelpCenterSearchController {
    public String searchString { get; set; }
    public String pfsId { get; set; }
    public String yearId { get; set; }

    public FamilyHelpCenterSearchController() {
        String q = ApexPages.currentPage().getParameters().get('searchq');
        if( String.isNotEmpty( q)) {
            searchString = EncodingUtil.urlDecode(q, 'UTF-8'); 
        }
    }

    public PageReference onSubmit() {

        // if search string is empty, don't do anything. 
        if( String.isEmpty( searchString)) {
            return null;
        }

        String encodedString = EncodingUtil.urlEncode(searchString, 'UTF-8');
        PageReference redirectPage = Page.FamilyHelpCenterSearchResults;
        redirectPage.setRedirect(true);
        redirectPage.getParameters().put('id', pfsId);
        redirectPage.getParameters().put('AcademicYearId', yearId);
        redirectPage.getParameters().put('searchq', encodedString);
        return redirectPage;
    }

    @RemoteAction
    public static List<Knowledge__kav> searchKnowledgeArticles( String keyword) {
        List<Knowledge__kav> articles = new List<Knowledge__kav>();

        Search.SearchResults searchResults = new KnowledgeDataAccessService().searchKnowledgeArticlesByKeyword( keyword, 1, 10);
        List<Search.SearchResult> articlelist = searchResults.get('Knowledge__kav');

        for ( Search.SearchResult searchResult : articleList) { 
            Knowledge__kav article = (Knowledge__kav) searchResult.getSObject();
            articles.add( article);
        }

        return articles;

    }
}