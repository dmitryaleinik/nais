/**
 * @description This class is used to create AccountShare records for unit tests.
 */
@isTest
public class AccountShareTestData extends SObjectTestData {
    @testVisible private static final String CASE_ACCESS_LEVEL = 'Edit';
    @testVisible private static final String OPPORTUNITY_ACCESS_LEVEL = 'Edit';
    @testVisible private static final String ACCOUNT_ACCESS_LEVEL = 'Edit';

    /**
     * @description Get the default values for the AccountShare object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                AccountShare.AccountId => AccountTestData.Instance.DefaultAccount.Id,
                AccountShare.CaseAccessLevel => CASE_ACCESS_LEVEL,
                AccountShare.OpportunityAccessLevel => OPPORTUNITY_ACCESS_LEVEL,
                AccountShare.AccountAccessLevel => ACCOUNT_ACCESS_LEVEL,
                AccountShare.UserOrGroupId => CurrentUser.getCurrentUser().Id
        };
    }

    /**
     * @description Set the User or Group Id on the current AccountShare record.
     * @param userOrGroupId The Id of the User or Group.
     * @return The current instance of AccountShareTestData.
     */
    public AccountShareTestData forUserOrGroupId(Id userOrGroupId) {
        return (AccountShareTestData) with(AccountShare.UserOrGroupId, userOrGroupId);
    }

    /**
     * @description Set the Account to share Id on the current AccountShare record.
     * @param accountId The Id of the Account to share.
     * @return The current instance of AccountShareTestData.
     */
    public AccountShareTestData forAccountId(Id accountId)
    {
        return (AccountShareTestData) with(AccountShare.AccountId, accountId);
    }

    /**
     * @description Insert the current working AccountShare record.
     * @return The currently operated upon AccountShare record.
     */
    public AccountShare insertAccountShare() {
        return (AccountShare)insertRecord();
    }

    /**
     * @description Create the current working AccountShare record without resetting
     *             the stored values in this instance of AccountShareTestData.
     * @return A non-inserted AccountShare record using the currently stored field values.
     */
    public AccountShare createAccountShareWithoutReset() {
        return (AccountShare)buildWithoutReset();
    }

    /**
     * @description Create the current working AccountShare record.
     * @return The currently operated upon AccountShare record.
     */
    public AccountShare create() {
        return (AccountShare)super.buildWithReset();
    }

    /**
     * @description The default AccountShare record.
     */
    public AccountShare DefaultAccountShare {
        get {
            if (DefaultAccountShare == null) {
                DefaultAccountShare = createAccountShareWithoutReset();
                insert DefaultAccountShare;
            }
            return DefaultAccountShare;
        }
        private set;
    }

    /**
     * @description Get the AccountShare SObjectType.
     * @return The AccountShare SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return AccountShare.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static AccountShareTestData Instance {
        get {
            if (Instance == null) {
                Instance = new AccountShareTestData();
            }
            return Instance;
        }
        private set;
    }

    private AccountShareTestData() { }
}