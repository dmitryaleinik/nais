/**
 * @description This class is used to create Student Folder records for unit tests.
 */
@isTest
public class StudentFolderTestData extends SObjectTestData {
    
    @testVisible private static final String NAME = 'StudentFolder Name';
    @testVisible private static final String FOLDER_SOURCE = 'School-Initiated';

    /**
     * @description Get the default values for the Student_Folder__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
            Student_Folder__c.Name => NAME,
            Student_Folder__c.Folder_Source__c => FOLDER_SOURCE,
            Student_Folder__c.Student__c => ContactTestData.Instance.asStudent().DefaultContact.Id
        };
    }

    /**
     * @description Set the Name field on the current Student Folder record.
     * @param studentFolderName The Name to set on the Student Folder.
     * @return The current working instance of StudentFolderTestData.
     */
    public StudentFolderTestData forName(String studentFolderName) {
        return (StudentFolderTestData) with(Student_Folder__c.Name, studentFolderName);
    }

    /**
     * @description Set the Academic_Year_Picklist__c field on the current Student Folder record.
     * @param academicYearName The Academic Year Picklist to set on the Student Folder.
     * @return The current working instance of StudentFolderTestData.
     */
    public StudentFolderTestData forAcademicYearPicklist(String academicYearName) {
        return (StudentFolderTestData) with(Student_Folder__c.Academic_Year_Picklist__c, academicYearName);
    }

    /**
     * @description Set the Student__c field on the current Student Folder record.
     * @param contactId The Student Id to set on the Student Folder.
     * @return The current working instance of StudentFolderTestData.
     */
    public StudentFolderTestData forStudentId(Id contactId) {
        return (StudentFolderTestData) with(Student_Folder__c.Student__c, contactId);
    }

    /**
     * @description Set the Student_ID__c field on the current Student Folder record. Not to be confused with the
     *              forStudentId() method which sets the Student__c contact lookup field.
     * @param studentId The Student Id to set on the Student Folder.
     * @return The current working instance of StudentFolderTestData.
     */
    public StudentFolderTestData withStudentIdString(String studentId) {
        return (StudentFolderTestData) with(Student_Folder__c.Student_ID__c, studentId);
    }

    /**
     * @description Set the Faculty_Child__c field on the current Student Folder record.
     * @param yesOrNo A yes or no string indicating whether or not the student is the child of someone who works at the school.
     * @return The current working instance of StudentFolderTestData.
     */
    public StudentFolderTestData withFacultyChild(String yesOrNo) {
        return (StudentFolderTestData) with(Student_Folder__c.Faculty_Child__c, yesOrNo);
    }

    /**
     * @description Set the Day_Boarding__c field on the current Student Folder record.
     * @param dayBoarding The Day or Boarding to set on the Student Folder.
     * @return The current working instance of StudentFolderTestData.
     */
    public StudentFolderTestData forDayBoarding(String dayBoarding) {
        return (StudentFolderTestData) with(Student_Folder__c.Day_Boarding__c, dayBoarding);
    }

    /**
     * @description Set the Folder_Source__c field on the current Student Folder record.
     * @param folderSource The Folder Source or Boarding to set on the Student Folder.
     * @return The current working instance of StudentFolderTestData.
     */
    public StudentFolderTestData forFolderSource(String folderSource) {
        return (StudentFolderTestData) with(Student_Folder__c.Folder_Source__c, folderSource);
    }

    /**
     * @description Set the School__c field on the current Student Folder record.
     * @param accountId The School Id to set on the Student Folder.
     * @return The current working instance of StudentFolderTestData.
     */
    public StudentFolderTestData forSchoolId(Id accountId) {
        return (StudentFolderTestData) with(Student_Folder__c.School__c, accountId);
    }

    /**
     * @description Set the Ethnicity__c field on the current Student Folder record.
     * @param ethnicity The Ethnicity to set on the Student Folder.
     * @return The current working instance of StudentFolderTestData.
     */
    public StudentFolderTestData forEthnicity(String ethnicity) {
        return (StudentFolderTestData) with(Student_Folder__c.Ethnicity__c, ethnicity);
    }

    /**
     * @description Set the Folder_Status__c field on the current Student Folder record.
     * @param folderStatus The Folder Status to set on the Student Folder.
     * @return The current working instance of StudentFolderTestData.
     */
    public StudentFolderTestData forFolderStatus(String folderStatus) {
        return (StudentFolderTestData) with(Student_Folder__c.Folder_Status__c, folderStatus);
    }

    public StudentFolderTestData forLoanAmount(Decimal loanAmount) {
        return (StudentFolderTestData) with(Student_Folder__c.Loan__c, loanAmount);
    }

    public StudentFolderTestData forAPIEnabled(boolean enabled) {
        return (StudentFolderTestData) with(Student_Folder__c.Financial_Aid_API_Enabled__c, enabled);
    }

    /**
     * @description Set the Use_Budget_Groups__c field on the current Student Folder record.
     * @param useBudgetGroups The Use Budget Groups to set on the Student Folder.
     * @return The current working instance of StudentFolderTestData.
     */
    public StudentFolderTestData forUseBudgetGroups(String useBudgetGroups) {
        return (StudentFolderTestData) with(Student_Folder__c.Use_Budget_Groups__c, useBudgetGroups);
    }

    /**
     * @description Set the Grant_Awarded__c field on the current Student Folder record.
     * @param grantAwarded The Grant Awarded to set on the Student Folder.
     * @return The current working instance of StudentFolderTestData.
     */
    public StudentFolderTestData forGrantAwarded(Integer grantAwarded) {
        return (StudentFolderTestData) with(Student_Folder__c.Grant_Awarded__c, grantAwarded);
    }

    /**
     * @description Set the New_Returning__c field on the current Student Folder record.
     * @param newReturning The New / Returning to set on the Student Folder.
     * @return The current working instance of StudentFolderTestData.
     */
    public StudentFolderTestData forNewReturning(String newReturning) {
        return (StudentFolderTestData) with(Student_Folder__c.New_Returning__c, newReturning);
    }

    /**
     * @description Insert the current working Student_Folder__c record.
     * @return The currently operated upon Student_Folder__c record.
     */
    public Student_Folder__c insertStudentFolder() {
        return (Student_Folder__c)insertRecord();
    }

    /**
     * @description Create the current working Student Folder record without resetting
     *          the stored values in this instance of StudentFolderTestData.
     * @return A non-inserted Student_Folder__c record using the currently stored field
     *          values.
     */
    public Student_Folder__c createStudentFolderWithoutReset() {
        return (Student_Folder__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Student_Folder__c record.
     * @return The currently operated upon Student_Folder__c record.
     */
    public Student_Folder__c create() {
        return (Student_Folder__c)super.buildWithReset();
    }

    /**
     * @description The default Student_Folder__c record.
     */
    public Student_Folder__c DefaultStudentFolder {
        get {
            if (DefaultStudentFolder == null) {
                DefaultStudentFolder = createStudentFolderWithoutReset();
                insert DefaultStudentFolder;
            }
            return DefaultStudentFolder;
        }
        private set;
    }

    /**
     * @description Get the Student_Folder__c SObjectType.
     * @return The Student_Folder__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Student_Folder__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static StudentFolderTestData Instance {
        get {
            if (Instance == null) {
                Instance = new StudentFolderTestData();
            }
            return Instance;
        }
        private set;
    }

    private StudentFolderTestData() { }
}