public without sharing class SchoolFolderAidPieChart {

    /*Initialization*/
       public SchoolFolderAidPieChart(){
           
       }
    /*End Initialization*/
   
    /*Properties*/
    public Id folderId {get; set;}
    public Double total {get; set;}
    public List<budgetLink> budgetLinks {get; set;}
    public String display {get; set;}
    /*End Properties*/
       
    /*Action Methods*/
   
    /*End Action Methods*/
   
    /*Helper Methods*/
       public List<VFChartData> getAidData(){
           List<VFChartData> aidData = new List<VFChartData>();
           
           budgetLinks = new List<budgetLink>();
           total = 0;
           
           for (AggregateResult ar : [SELECT Budget_Group__r.Name budget, Budget_Group__r.Id budgetId, Sum(Amount_Allocated__c) amt from Budget_Allocation__c 
                                    where Student_Folder__c = :folderId
                                    group by Budget_Group__r.Name, Budget_Group__r.Id
                                    ]){
            aidData.add(new VFChartData(String.valueOf(ar.get('budget')), Integer.valueOf(ar.get('amt'))));    
            total += Double.valueOf(ar.get('amt'));
            budgetLinks.add(new budgetLink(ar));
        }
        
        display = aidData.size() > 3 ? 'none' : 'outside';
        
        return aidData;
       }
    /*End Helper Methods*/
    
    /*Internal Class*/
    public class BudgetLink{
        public String budgetId {get; set;}
        public Double budgetAmount {get; set;}
        public String budgetName {get; set;}
        
        public budgetLink(AggregateResult ar){
            budgetId = String.valueOf(ar.get('budgetId'));
            budgetAmount = Double.valueOf(ar.get('amt'));
            budgetName = String.valueOf(ar.get('budget'));
        }
    }
    
    
    /*End Internal Class*/
    

}