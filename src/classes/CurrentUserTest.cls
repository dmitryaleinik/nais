@isTest
private class CurrentUserTest
{

    private static void setAccessCookie() {
        Cookie earlyAccessCookie = new Cookie(AccessService.EARLY_ACCESS_COOKIE, 'true', '/', 300, false);
        ApexPages.currentPage().setCookies(new List<Cookie> { earlyAccessCookie });
    }

    private static User setupSchoolUser() {
        User userRecord = UserTestData.insertSchoolPortalUser();

        Profile schoolAdminProfile = [SELECT Name FROM Profile WHERE Id = :userRecord.ProfileId][0];
        insert new Profile_Type_Grouping__c(
            Name = schoolAdminProfile.Name,
            Profile_Name__c = schoolAdminProfile.Name,
            Is_School_Admin_Profile__c = true,
            Is_Call_Center_Profile__c = false,
            Is_Sys_Admin__c = true,
            Is_School_Profile__c = true);
        GlobalVariables.populateProfileIds();

        CurrentUser.clearCache();

        return userRecord;
    }

    private static User getGuestUser() {
        List<User> guestUsers = [Select Id FROM User WHERE UserType = :CurrentUser.GUEST_USER_TYPE];

        return guestUsers[0];
    }

    @isTest
    private static void isGuest_withGuest_expectTrue() {
        System.runAs(getGuestUser()) {
            System.assert(CurrentUser.isGuest(), 'Expected isGuest() to return true for guest user.');
        }
    }

    @isTest
    private static void isGuest_withNonGuest_expectFalse() {
        System.assert(!CurrentUser.isGuest(), 'Expected isGuest() to return false for non guest user.');
    }

    @isTest
    private static void getCurrentUser_asGuestUser_expectNull() {
        System.runAs(getGuestUser()) {
            System.assertEquals(null, CurrentUser.getCurrentUser(),
                    'Expected current user to be null for a guest user.');
        }
    }

    @isTest
    private static void getCurrentUser_asFamilyPortalUser_expectUserReturned() {
        User familyUser = UserTestData.insertFamilyPortalUser();

        System.runAs(familyUser) {
            System.assertNotEquals(null, CurrentUser.getCurrentUser(),
                    'Expected the current user to not be null for a family portal user.');
        }
    }

    @isTest
    private static void hasEarlyAccess_earlyAccessClosed_userDoesNotHaveAccess_expectFalse() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        // Insert a user without granting them early access.
        User userRecord = UserTestData.insertFamilyPortalUser();

        System.runAs(userRecord) {
            System.assert(!CurrentUser.hasEarlyAccess(), 'Expected the current user to not have early access.');
        }
    }

    @isTest
    private static void hasEarlyAccess_earlyAccessClosed_userHasEarlyAccess_expectFalse() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        // Insert a user with early access set to true.
        User userRecord = UserTestData.createFamilyPortalUser();
        userRecord.Early_Access__c = true;
        insert userRecord;

        System.runAs(userRecord) {
            System.assert(!CurrentUser.hasEarlyAccess(), 'Expected the current user to not have early access.');
        }
    }

    @isTest
    private static void hasEarlyAccess_earlyAccessOpen_accessCookieNotSet_runAsGuest_expectFalse() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessOpened().DefaultFamilyPortalSettings;

        System.runAs(getGuestUser()) {
            System.assert(!CurrentUser.hasEarlyAccess(),
                    'Expected the guest user to not have access if the cookie is not set.');
        }
    }

    @isTest
    private static void hasEarlyAccess_earlyAccessOpen_accessCookieNotSet_userWithoutAccess_expectFalse() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessOpened().DefaultFamilyPortalSettings;

        // Insert a user without early access.
        User userRecord = UserTestData.insertFamilyPortalUser();

        System.runAs(userRecord) {
            System.assert(!CurrentUser.hasEarlyAccess(),
                    'Expected a user with early access set to false to not have access without the access cookie.');
        }
    }

    @isTest
    private static void hasEarlyAccess_earlyAccessOpen_accessCookieSet_expectTrue() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessOpened().DefaultFamilyPortalSettings;

        setAccessCookie();

        System.runAs(getGuestUser()) {
            System.assert(CurrentUser.hasEarlyAccess(),
                    'Expected a user to have early access if the cookie is set and early access is open.');
        }
    }

    @isTest
    private static void hasEarlyAccess_earlyAccessOpen_accessCookieNotSet_userHasAccess_expectTrue() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessOpened().DefaultFamilyPortalSettings;

        // Insert a user with early access.
        User userRecord = UserTestData.createFamilyPortalUser();
        userRecord.Early_Access__c = true;
        insert userRecord;

        System.runAs(userRecord) {
            System.assert(CurrentUser.hasEarlyAccess(),
                    'Expected a user with early access set to true to be allowed into the family portal when early access is open.');
        }
    }

    @isTest
    private static void getCurrentSchool_familyUser_expectNull() {
        Account school;

        System.runAs(UserTestData.insertFamilyPortalUser()) {
            school = CurrentUser.getCurrentSchool();
        }

        System.assertEquals(null, school, 'Expected the school to be null.');
    }

    @isTest
    private static void getCurrentSchool_schoolExists_expectSchoolReturned() {
        User userRecord = setupSchoolUser();

        Id expectedSchoolId = [SELECT AccountId FROM Contact WHERE Id = :userRecord.ContactId LIMIT 1].AccountId;

        Account returnedSchool;
        System.runAs(userRecord) {
            returnedSchool = CurrentUser.getCurrentSchool();
        }

        System.assertNotEquals(null, returnedSchool, 'Expected a school returned.');
        System.assertEquals(expectedSchoolId, returnedSchool.Id, 'Expected the same school returned.');
    }

    @isTest
    private static void getCurrentSubscription_noneExists_expectNull() {
        User userRecord = setupSchoolUser();

        Subscription__c returnedSubscription;
        System.runAs(userRecord) {
            returnedSubscription = CurrentUser.getCurrentSubscription();
        }

        System.assertEquals(null, returnedSubscription, 'Expected the subscription to be null.');
    }

    @isTest
    private static void getCurrentSubscription_currentExists_expectSubscription() {
        User userRecord = setupSchoolUser();

        Id schoolId = [SELECT AccountId FROM Contact WHERE Id = :userRecord.ContactId LIMIT 1].AccountId;
        Subscription__c expectedSubscription = SubscriptionTestData.Instance.forAccount(schoolId).insertSubscription();

        Subscription__c returnedSubscription;
        System.runAs(userRecord) {
            returnedSubscription = CurrentUser.getCurrentSubscription();
        }

        System.assertEquals(expectedSubscription.Id, returnedSubscription.Id, 'Expected the subscriptions to match.');
    }

    @isTest
    private static void subscriptions_subscriptionsExist_expectPopulated() {
        User userRecord = setupSchoolUser();

        List<Subscription__c> returnedSubscriptions;
        System.runAs(userRecord) {
            returnedSubscriptions = CurrentUser.Subscriptions;
        }

        System.assert(returnedSubscriptions.isEmpty(), 'Expected returnedSubscriptions to be empty.');
    }

    @isTest
    private static void subscriptions_noneExist_expectEmpty() {
        User userRecord = setupSchoolUser();

        Id schoolId = [SELECT AccountId FROM Contact WHERE Id = :userRecord.ContactId LIMIT 1].AccountId;
        Subscription__c expectedSubscription = SubscriptionTestData.Instance.forAccount(schoolId).insertSubscription();

        List<Subscription__c> returnedSubscriptions;
        System.runAs(userRecord) {
            returnedSubscriptions = CurrentUser.Subscriptions;
        }

        System.assertEquals(1, returnedSubscriptions.size(), 'Expected returnedSubscriptions to have 1 subscription.');
        System.assertEquals(expectedSubscription.Id, returnedSubscriptions[0].Id,
                'Expected the inserted subscription to be returned.');
    }

    @isTest
    private static void hasCurrentSubscription_currentExists_expectTrue() {
        User userRecord = setupSchoolUser();

        Id schoolId = [SELECT AccountId FROM Contact WHERE Id = :userRecord.ContactId LIMIT 1].AccountId;
        SubscriptionTestData.Instance.forAccount(schoolId).insertSubscription();

        Subscription__c returnedSubscription;
        System.runAs(userRecord) {
            System.assert(CurrentUser.hasCurrentSubscription(), 'Expected there to be a current subscription.');
        }
    }

    @isTest
    private static void hasCurrentSubscription_expiredExists_expectFalse() {
        User userRecord = setupSchoolUser();

        Id schoolId = [SELECT AccountId FROM Contact WHERE Id = :userRecord.ContactId LIMIT 1].AccountId;
        SubscriptionTestData.Instance.asExpired().forAccount(schoolId).insertSubscription();

        Subscription__c returnedSubscription;
        System.runAs(userRecord) {
            System.assert(!CurrentUser.hasCurrentSubscription(), 'Expected there to be no current subscription.');
        }
    }

    @isTest
    private static void hasCurrentSubscription_noneExists_expectFalse() {
        User userRecord = setupSchoolUser();

        Subscription__c returnedSubscription;
        System.runAs(userRecord) {
            System.assert(!CurrentUser.hasCurrentSubscription(), 'Expected there not to be a current subscription.');
        }
    }

    @isTest
    private static void hasSubscriptionOpportunityForCurrentAcademicYear_hasOpportunity_expectTrue() {
        User userRecord = setupSchoolUser();

        Id schoolId = [SELECT AccountId FROM Contact WHERE Id = :userRecord.ContactId LIMIT 1].AccountId;

        System.runAs(userRecord) {
            Opportunity opp = OpportunityTestData.Instance.forAccount(schoolId).insertOpportunity();
            TransactionLineItemTestData.Instance.asSale()
                    .forAmount(opp.Amount).forOpportunity(opp.Id).insertTransactionLineItem();
            TransactionLineItemTestData.Instance.asCheck()
                    .forAmount(opp.Amount).forOpportunity(opp.Id).insertTransactionLineItem();
            OpportunityService.Instance.markAsClosedWon(opp);

            System.assert(CurrentUser.hasSubscriptionOpportunityForCurrentAcademicYear(),
                    'Expected there to be an Opportunity.');
        }
    }

    @isTest
    private static void hasSubscriptionOpportunityForCurrentAcademicYear_noOpportunity_expectFalse() {
        User userRecord = setupSchoolUser();

        System.runAs(userRecord) {
            System.assert(!CurrentUser.hasSubscriptionOpportunityForCurrentAcademicYear(),
                    'Expected there not to be an Opportunity.');
        }
    }

    @isTest
    private static void isSchoolUser_familyPortalUser_expectFalse() {
        User userRecord = UserTestData.insertFamilyPortalUser();

        CurrentUser.clearCache();
        System.runAs(userRecord) {
            System.assertEquals(false, CurrentUser.isSchoolUser(),
                    'Expected false for a user with the family portal user profile.');
        }
    }

    @isTest
    private static void isSchoolUser_schoolAdminUser_expectTrue() {
        User userRecord = setupSchoolUser();

        System.runAs(userRecord) {
            System.assertEquals(true, CurrentUser.isSchoolUser(),
                    'Expected true for a user with the school admin profile.');
        }
    }
}