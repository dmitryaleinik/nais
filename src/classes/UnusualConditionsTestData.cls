/**
 * @description This class is used to create Unusual Conditions records for unit tests.
 */
@isTest
public class UnusualConditionsTestData extends SObjectTestData {
    /**
     * @description Get the default values for the Unusual_Conditions__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Unusual_Conditions__c.Academic_Year__c => AcademicYearTestData.Instance.DefaultAcademicYear.Id
        };
    }

    /**
     * @description Set the Academic Year on the current Unusual Conditions record.
     * @param academicYearId The Id of the Academic Year to set on the current
     *             Unusual Conditions record.
     * @return The current working instance of UnusualConditionsTestData.
     */
    public UnusualConditionsTestData forAcademicYearId(Id academicYearId) {
        return (UnusualConditionsTestData) with(Unusual_Conditions__c.Academic_Year__c, academicYearId);
    }

    /**
     * @description Insert the current working Unusual_Conditions__c record.
     * @return The currently operated upon Unusual_Conditions__c record.
     */
    public Unusual_Conditions__c insertUnusualConditions() {
        return (Unusual_Conditions__c)insertRecord();
    }

    /**
     * @description Create the current working Unusual Conditions record without resetting
     *             the stored values in this instance of UnusualConditionsTestData.
     * @return A non-inserted Unusual_Conditions__c record using the currently stored field
     *             values.
     */
    public Unusual_Conditions__c createUnusualConditionsWithoutReset() {
        return (Unusual_Conditions__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Unusual_Conditions__c record.
     * @return The currently operated upon Unusual_Conditions__c record.
     */
    public Unusual_Conditions__c create() {
        return (Unusual_Conditions__c)super.buildWithReset();
    }

    /**
     * @description The default Unusual_Conditions__c record.
     */
    public Unusual_Conditions__c DefaultUnusualConditions {
        get {
            if (DefaultUnusualConditions == null) {
                DefaultUnusualConditions = createUnusualConditionsWithoutReset();
                insert DefaultUnusualConditions;
            }
            return DefaultUnusualConditions;
        }
        private set;
    }

    /**
     * @description Get the Unusual_Conditions__c SObjectType.
     * @return The Unusual_Conditions__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Unusual_Conditions__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static UnusualConditionsTestData Instance {
        get {
            if (Instance == null) {
                Instance = new UnusualConditionsTestData();
            }
            return Instance;
        }
        private set;
    }

    private UnusualConditionsTestData() { }
}