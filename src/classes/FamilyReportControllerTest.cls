@isTest
private class FamilyReportControllerTest 
{

    @isTest
    private static void testConstructor()
    {
        TestDataFamily tdf = new TestDataFamily();

        FamilyReportController testRoller = new FamilyReportController();
        System.assertEquals(true, testRoller.hasErrors);

        PageReference testPR = Page.FamilyReport;
        testPR.getParameters().put('id', tdf.pfsA.Id);
        Test.setCurrentPage(testPR);
        testRoller = new FamilyReportController();
        
        System.assertEquals(false, testRoller.hasErrors);
        System.assertEquals(tdf.pfsA.Id, testRoller.thePFS.Id);
    }

    @isTest
    private static void testBusinessFarmRollups_asFamilyPortalUser()
    {
        User familyPortalUser = UserTestData.insertFamilyPortalUser();

        System.runAs(familyPortalUser)
        {
            Pfs__c pfs = PfsTestData.Instance.DefaultPfs;
            Pfs__c deletedPfs = PfsTestData.Instance.insertPfs();
            Business_Farm__c business = BusinessFarmTestData.Instance
                .forBusinessOrFarm('Business')
                .forBusinessEntityType('Partnership')
                .withNetProfits(500)
                .forBusinessAssetsCurrent(10000)
                .forBusinessBldgMortgageCurrent(1000).create();
            Business_Farm__c farm  = BusinessFarmTestData.Instance
                .forBusinessOrFarm('Farm')
                .forBusinessEntityType('Sole Proprietorship')
                .withGrossSales(1000)
                .forBusinessAssetsCurrent(20000)
                .forBusinessBldgMortgageCurrent(2000).create();
            insert new List<Business_Farm__c> {business, farm};

            Test.startTest();
                PageReference testPR = Page.FamilyReport;
                testPR.getParameters().put('id', deletedPfs.Id);
                Test.setCurrentPage(testPR);
                delete deletedPfs;

                FamilyReportController testRoller = new FamilyReportController();
                System.assertEquals(1, ApexPages.getMessages().size());
                System.assertEquals('Invalid Id or insufficient privileges.', ApexPages.getMessages()[0].getDetail());

                testPR.getParameters().put('id', pfs.Id);
                Test.setCurrentPage(testPR);

                testRoller = new FamilyReportController();

                Decimal assets = testRoller.getBusinessFarmAssets();
                Decimal debts = testRoller.getBusinessFarmDebts();
                Decimal netPL = testRoller.getBusinessFarmNetProfitLoss();
            Test.stopTest();

            System.assertEquals(30000, assets);
            System.assertEquals(3000, debts);
            System.assertEquals(1500, netPL);
        }
    }

    @isTest
    private static void testBusinessFarmRollups_asSchoolPortalAdmin()
    {
        Profile_Type_Grouping__c ptgSchoolUser = ProfileTypeGroupingTestData.Instance
            .asSchoolPortalAdministrator().DefaultProfileTypeGrouping;
        User schoolPortalAdmin = UserTestData.insertSchoolPortalUser();
        
        System.runAs(schoolPortalAdmin)
        {
            Pfs__c pfs = PfsTestData.Instance.DefaultPfs;
            Business_Farm__c business = BusinessFarmTestData.Instance
                .forBusinessOrFarm('Business')
                .forBusinessEntityType('Partnership')
                .withNetProfits(500)
                .forBusinessAssetsCurrent(10000)
                .forBusinessBldgMortgageCurrent(1000).create();
            Business_Farm__c farm  = BusinessFarmTestData.Instance
                .forBusinessOrFarm('Farm')
                .forBusinessEntityType('Sole Proprietorship')
                .withGrossSales(1000)
                .forBusinessAssetsCurrent(20000)
                .forBusinessBldgMortgageCurrent(2000).create();
            insert new List<Business_Farm__c> {business, farm};

            Test.startTest();
                PageReference testPR = Page.FamilyReport;
                testPR.getParameters().put('id', pfs.Id);
                Test.setCurrentPage(testPR);

                FamilyReportController testRoller = new FamilyReportController();
            Test.stopTest();

            Decimal assets = testRoller.getBusinessFarmAssets();
            Decimal debts = testRoller.getBusinessFarmDebts();
            Decimal netPL = testRoller.getBusinessFarmNetProfitLoss();
            
            System.assertEquals(30000, assets);
            System.assertEquals(3000, debts);
            System.assertEquals(1500, netPL);
        }
    }

    @isTest
    private static void testGetDisplayContribution()
    {
        FamilyReportController controller = new FamilyReportController();
        Integer estParentalContribution = 1000;
        controller.thePFS = new Pfs__c(Est_Parental_Contribution__c = estParentalContribution);

        Test.startTest();
            System.assertEquals(estParentalContribution, controller.getDisplayContribution());
        Test.stopTest();
    }

    @isTest
    private static void testGetSchoolCalculationList()
    {
        FamilyReportController controller = new FamilyReportController();

        Test.startTest();
            System.assertEquals(Label.School_Calculation_List.split('<br/>'), controller.getSchoolCalculationList());
        Test.stopTest();
    }

    @isTest
    private static void testGetFinancialAidDecisionList()
    {
        FamilyReportController controller = new FamilyReportController();

        Test.startTest();
            System.assertEquals(Label.Financial_Aid_Decision_List.split('<br/>'), controller.getFinancialAidDecisionList());
        Test.stopTest();
    }
}