/**
 * FamilyPortalContactPanelController.cls
 *
 * @description: Class for getting metadata types FamilyPortalContactPanelController.cls
 * This class is really just needed for the remote action to get the live agent data. 
 *
 * @author: Mike havrilla @ Presence PG
 */
public class FamilyPortalContactPanelController extends DataAccessService {

    @RemoteAction 
    public static LiveAgentDeploymentService.LiveAgentConfigurationWrapper getLiveAgentInformation( String deploymentType) {
        return LiveAgentDeploymentService.getLiveAgentInformation( deploymentType);
    }
}