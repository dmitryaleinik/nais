/**
 * @description This class is used to create Pfs records for unit tests.
 */
@isTest
public class PfsTestData extends SObjectTestData
{

    @testVisible private static final String PFS_INITIAL_NAME = 'PFS A';
    @testVisible private static final String PFS_STATUS = 'Application In Progress';

    /**
     * @description Get the default values for the PFS__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                PFS__c.Parent_A__c => ContactTestData.Instance.DefaultContact.Id,
                PFS__c.PFS_Status__c => PFS_STATUS,
                PFS__c.Name => PFS_INITIAL_NAME
        };
    }

    /**
     * @description Set the PFS_Status__c field to Application Submitted for the
     *              current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData asSubmitted() {
        return (PfsTestData) with(PFS__c.PFS_Status__c, 'Application Submitted');
    }

    /**
     * @description Set the Payment_Status__c field to Paid in Full.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData asPaid() {
        return (PfsTestData) with(PFS__c.Payment_Status__c, 'Paid in Full');
    }

    /**
     * @description Populate the Verification__c field with DefaultVerification Id.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData asVerified() {
        return (PfsTestData) with(PFS__c.Verification__c, VerificationTestData.Instance.DefaultVerification.Id);
    }

    /**
     * @description Set section status fields of the PFS record to complete(true) status.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData asCompleted() {
        return (PfsTestData)  
            // Household Information
             with(PFS__c.statHouseholdInformationBoolean__c, true)
            .with(PFS__c.statParentsGuardians__c, true)
            .with(PFS__c.statApplicantInformation__c, true)
            .with(PFS__c.statDependentInformation__c, true)
            .with(PFS__c.statHouseholdSummary__c, true)
            
            // School Selection
            .with(PFS__c.statSchoolSelectionBoolean__c, true)
            .with(PFS__c.statSelectSchools__c, true)
            
            // Family Income
            .with(PFS__c.statFamilyIncomeBoolean__c, true)
            .with(PFS__c.statBasicTax__c, true)
            .with(PFS__c.statTotalTaxable__c, true)
            .with(PFS__c.statTotalNon__c, true)
            .with(PFS__c.statStudentIncome__c, true)

            // Family Assets & Debts
            .with(PFS__c.statFamilyAssetsBoolean__c, true)
            .with(PFS__c.statRealEstate__c, true)
            .with(PFS__c.statVehicles__c, true)
            .with(PFS__c.statOtherAssets__c, true)

            // Family Expenses
            .with(PFS__c.statFamilyExpensesBoolean__c, true)
            .with(PFS__c.statEducationalExpenses__c, true)
            .with(PFS__c.statOtherExpenses__c, true)

            // Business/Farm
            .with(PFS__c.statBusinessFarmBoolean__c, true)
            .with(PFS__c.statBusinessInformation__c, true)
            .with(PFS__c.statBusinessIncome__c, true)
            .with(PFS__c.statBusinessExpenses__c, true)
            .with(PFS__c.statBusinessAssets__c, true)
            .with(PFS__c.statBusinessSummary__c, true)

            // Monthly Income and Expenses
            .with(PFS__c.statMonthlyIncomeExpensesMainBoolean__c, true)
            .with(PFS__c.statMonthlyIncomeExpenses__c, true)

            // Other Information
            .with(PFS__c.statOtherInformationBoolean__c, true)
            .with(PFS__c.statOtherConsiderations__c, true)
            .with(PFS__c.statAdditionalQuestions__c, true);
    }

    /**
     * @description Set the Name field on the current Pfs record.
     * @param name The Name to set to the Name field of the
     *        current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forName(String name) {
        return (PfsTestData) with(PFS__c.Name, name);
    }

    /**
     * @description Set the Parent_A__c field on the current Pfs record.
     * @param contactId The Contact Id to set to the Parent A field of the
     *        current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forParentA(Id contactId) {
        return (PfsTestData) with(PFS__c.Parent_A__c, contactId);
    }

    /**
     * @description Set the ParentA First Name field on the current Pfs record.
     * @param firstName The First Name to set to the Parent_A_First_Name__c field of the
     *        current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forParentAFirstName(String firstName) {
        return (PfsTestData) with(PFS__c.Parent_A_First_Name__c, firstName);
    }

    /**
     * @description Set the ParentA Last Name field on the current Pfs record.
     * @param lastName The Last Name to set to the Parent_A_Last_Name__c field of the
     *        current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forParentALastName(String lastName) {
        return (PfsTestData) with(PFS__c.Parent_A_Last_Name__c, lastName);
    }
    
    /**
     * @description Set the Parent_B__c field on the current Pfs record.
     * @param contactId The Contact Id to set to the Parent B field of the
     *        current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forParentB(Id contactId) {
        return (PfsTestData) with(PFS__c.Parent_B__c, contactId);
    }

    /**
     * @description Set the Student_Has_Parent_B__c field on the current Pfs record.
     * @param studentHasParentB The Student Has Parent B answer to set to the Student Has Parent B field of the
     *        current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forStudentHasParentB(String studentHasParentB) {
        return (PfsTestData) with(PFS__c.Student_Has_Parent_B__c, studentHasParentB);
    }

    /**
     * @description Set the Academic Year Picklist on the current Pfs record.
     * @param academicYearName The name of the academic year to set the Academic
     *        Year Picklist field to on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forAcademicYearPicklist(String academicYearName) {
        return (PfsTestData) with(PFS__c.Academic_Year_Picklist__c, academicYearName);
    }

    /**
     * @description Set the Pfs Status on the current Pfs record.
     * @param pfsStatus The Pfs Status to set the PFS_Status__c field to on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forPfsStatus(String pfsStatus) {
        return (PfsTestData) with(PFS__c.PFS_Status__c, pfsStatus);
    }

    /**
     * @description Set the Payment Status on the current Pfs record.
     * @param paymentStatus The Payment Status to set the Payment_Status__c field to on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forPaymentStatus(String paymentStatus) {
        return (PfsTestData) with(PFS__c.Payment_Status__c, paymentStatus);
    }

    /**
     * @description Set the Filling Status on the current Pfs record.
     * @param fillingStatus The Filling Status to set the Filing_Status__c field to on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forFillingStatus(String fillingStatus) {
        return (PfsTestData) with(PFS__c.Filing_Status__c, fillingStatus);
    }

    /**
     * @description Set the Salary Wages Parent A on the current Pfs record.
     * @param salaryWagesParentA The Salary Wages Parent A to set the Salary_Wages_Parent_A__c field to 
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forSalaryWagesParentA(Integer salaryWagesParentA) {
        return (PfsTestData) with(PFS__c.Salary_Wages_Parent_A__c, salaryWagesParentA);
    }

    /**
     * @description Set the Salary Wages Parent B on the current Pfs record.
     * @param salaryWagesParentB The Salary Wages Parent B to set the Salary_Wages_Parent_B__c field to 
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forSalaryWagesParentB(Integer salaryWagesParentB) {
        return (PfsTestData) with(PFS__c.Salary_Wages_Parent_B__c, salaryWagesParentB);
    }

    /**
     * @description Set the Salary Wages Parent B Estimated on the current Pfs record.
     * @param salaryWagesParentBEst The estimated Salary Wages Parent B to set the Salary_Wages_Parent_B_Est__c field to 
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forSalaryWagesParentBEst(Integer salaryWagesParentBEst) {
        return (PfsTestData) with(PFS__c.Salary_Wages_Parent_B_Est__c, salaryWagesParentBEst);
    }

    /**
     * @description Set the Parent B Employee Retirement Plan on the current Pfs record.
     * @param parentBEmployeeRetirementPlan The Parent B Employee Retirement Plan to set the Parent_B_Employee_Retirement_Plan__c field to 
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forParentBEmployeeRetirementPlan(String parentBEmployeeRetirementPlan) {
        return (PfsTestData) with(PFS__c.Parent_B_Employee_Retirement_Plan__c, parentBEmployeeRetirementPlan);
    }

    /**
     * @description Set the Number of Children in Tuition Schools on the current Pfs record.
     * @param numChildreInTuitionSchools The Number of Children in Tuition Schools 
     *        to set the Num_Children_in_Tuition_Schools__c field to on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forNumChildreInTuitionSchools(Integer numChildreInTuitionSchools) {
        return (PfsTestData) with(PFS__c.Num_Children_in_Tuition_Schools__c, numChildreInTuitionSchools);
    }

    /**
     * @description Set the Original Submission Date on the current Pfs record.
     * @param originalSubmissionDate The Original Submission Date to set the Original_Submission_Date__c field to 
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forOriginalSubmissionDate(Datetime originalSubmissionDate) {
        return (PfsTestData) with(PFS__c.Original_Submission_Date__c, originalSubmissionDate);
    }

    /**
     * @description Set the Verification on the current Pfs record.
     * @param verificationId The Id of the verification record to use.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forVerification(Id verificationId) {
        return (PfsTestData) with(PFS__c.Verification__c, verificationId);
    }

    /**
     * @description Set the Fee_Waived__c on the current Pfs record.
     * @param feeWaived The string value to set Fee_Waived__c of the PFS record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forFeeWaived(String feeWaived) {
        return (PfsTestData) with(PFS__c.Fee_Waived__c, feeWaived);
    }

    /**
     * @description Set the Parent_A_Email__c on the current Pfs record.
     * @param parentAEmail The email of parentA to set Parent_A_Email__c field.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forParentAEmail(String parentAEmail) {
        return (PfsTestData) with(PFS__c.Parent_A_Email__c, parentAEmail);
    }

    /**
     * @description Set the SSS EFC Calc Status field on the current Pfs record.
     * @param sssEfcCalcStatus The Filling Status to set the SSS_EFC_Calc_Status__c field to on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forSssEfcCalcStatus(String sssEfcCalcStatus) {
        return (PfsTestData) with(PFS__c.SSS_EFC_Calc_Status__c, sssEfcCalcStatus);
    }

    /**
     * @description Set the parent A address field on the current Pfs record.
     * @param parentAAdress The parent address to set the Parent_A_Address__c field to on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forParentAAddress(String parentAAdress) {
        return (PfsTestData) with(PFS__c.Parent_A_Address__c, parentAAdress);
    }

    /**
     * @description Set the Parent A country field on the current Pfs record.
     * @param parentACountry The country to set the Parent_A_Country__c field to on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forParentACountry(String parentACountry) {
        return (PfsTestData) with(PFS__c.Parent_A_Country__c, parentACountry);
    }

    /**
     * @description Set the Application Last Modified on Family on the current Pfs record.
     * @param applicationLastModifiedByFamily The date to set the Application_Last_Modified_by_Family__c field 
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forApplicationLastModifiedByFamily(Date applicationLastModifiedByFamily) {
        return (PfsTestData) with(PFS__c.Application_Last_Modified_by_Family__c, applicationLastModifiedByFamily);
    }

    /**
     * @description Set the Own Business or Farm? field on the current Pfs record.
     * @param ownBusinessOrFarm The picklist value to set the Own_Business_or_Farm__c field 
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forOwnBusinessOrFarm(String ownBusinessOrFarm) {
        return (PfsTestData) with(PFS__c.Own_Business_or_Farm__c, ownBusinessOrFarm);
    }

    /**
     * @description Set the Status Monthly Income Expenses field on the current Pfs record.
     * @param statMonthlyIncomeExpenses The value to set the statMonthlyIncomeExpenses__c field 
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forStatMonthlyIncomeExpenses(Boolean statMonthlyIncomeExpenses) {
        return (PfsTestData) with(PFS__c.statMonthlyIncomeExpenses__c, statMonthlyIncomeExpenses);
    }

    /**
     * @description Set the Medical/Dental Exp Current field on the current Pfs record.
     * @param medicalDentalExpCurrent The value to set the Medical_Dental_Exp_Current__c field 
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forMedicalDentalExpCurrent(Integer medicalDentalExpCurrent) {
        return (PfsTestData) with(PFS__c.Medical_Dental_Exp_Current__c, medicalDentalExpCurrent);
    }

    /**
     * @description Set the Unusual Expenses Current field on the current Pfs record.
     * @param unusualExpensesCurrent The value to set the Unusual_Expenses_Current__c field 
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forUnusualExpensesCurrent(Integer unusualExpensesCurrent) {
        return (PfsTestData) with(PFS__c.Unusual_Expenses_Current__c, unusualExpensesCurrent);
    }

    /**
     * @description Set the Annual Club Fees field on the current Pfs record.
     * @param annualClubFees The value to set the Annual_Club_Fees__c field 
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forAnnualClubFees(Integer annualClubFees) {
        return (PfsTestData) with(PFS__c.Annual_Club_Fees__c, annualClubFees);
    }

    /**
     * @description Set the Camps/Lessons Annual Cost field on the current Pfs record.
     * @param campsLessonsAnnualCost The value to set the Camps_Lessons_Annual_Cost__c field 
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forCampsLessonsAnnualCost(Integer campsLessonsAnnualCost) {
        return (PfsTestData) with(PFS__c.Camps_Lessons_Annual_Cost__c, campsLessonsAnnualCost);
    }

    /**
     * @description Set the Medical/Dental Exp Est field on the current Pfs record.
     * @param medicalDentalExpEst The value to set the Medical_Dental_Exp_Est__c field 
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forMedicalDentalExpEst(Integer medicalDentalExpEst) {
        return (PfsTestData) with(PFS__c.Medical_Dental_Exp_Est__c, medicalDentalExpEst);
    }

    /**
     * @description Set the Unusual Expenses Est field on the current Pfs record.
     * @param unusualExpensesEst The value to set the Unusual_Expenses_Est__c field 
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forUnusualExpensesEst(Integer unusualExpensesEst) {
        return (PfsTestData) with(PFS__c.Unusual_Expenses_Est__c, unusualExpensesEst);
    }

    /**
     * @description Set the Other Income Current field on the current Pfs record.
     * @param otherIncomeCurrent The value to set the Other_Income_Current__c field 
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forOtherIncomeCurrent(Integer otherIncomeCurrent) {
        return (PfsTestData) with(PFS__c.Other_Income_Current__c, otherIncomeCurrent);
    }

    /**
     * @description Set the Other Taxable Income Description field on the current Pfs record.
     * @param otherIncomeDescription The value to set the Other_Taxable_Income_Descrip__c field
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData withOtherIncomeDescription(String otherIncomeDescription) {
        return (PfsTestData)with(PFS__c.Other_Taxable_Income_Descrip__c, otherIncomeDescription);
    }

    /**
     * @description Set the Deductible Part SE Tax Current field on the current Pfs record.
     * @param deductiblePartSETaxCurrent The value to set the Deductible_Part_SE_Tax_Current__c field 
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forDeductiblePartSETaxCurrent(Integer deductiblePartSETaxCurrent) {
        return (PfsTestData) with(PFS__c.Deductible_Part_SE_Tax_Current__c, deductiblePartSETaxCurrent);
    }

    /**
     * @description Set the IRS Adjustments To Income Current field on the current Pfs record.
     * @param iRSAdjustmentsToIncomeCurrent The value to set the IRS_Adjustments_to_Income_Current__c field 
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forIRSAdjustmentsToIncomeCurrent(Integer iRSAdjustmentsToIncomeCurrent) {
        return (PfsTestData) with(PFS__c.IRS_Adjustments_to_Income_Current__c, iRSAdjustmentsToIncomeCurrent);
    }

    /**
     * @description Set Business Not Indicated field on the current Pfs record.
     * @param businessNotIndicated The value to set the Business_Not_Indicated__c field
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forBusinessNotIndicated(String businessNotIndicated) {
        return (PfsTestData) with(PFS__c.Business_Not_Indicated__c, businessNotIndicated);
    }

    /**
     * @description Set Visible To Schools Until field on the current Pfs record.
     * @param visibleToSchoolsUntil The value to set the Visible_to_Schools_Until__c field
     *        on the current Pfs record.
     * @return The current instance of PfsTestData.
     */
    public PfsTestData forVisibleToSchoolsUntil(Date visibleToSchoolsUntil)
    {
        return (PfsTestData) with(PFS__c.Visible_to_Schools_Until__c, visibleToSchoolsUntil);
    }

    /**
     * @description Insert the current working PFS__c record.
     * @return The currently operated upon PFS__c record.
     */
    public PFS__c insertPfs() {
        return (PFS__c)insertRecord();
    }

    /**
     * @description Create the current working Pfs record without resetting
     *              the stored values in this instance of PfsTestData.
     * @return A non-inserted Pfs record using the currently stored field values.
     */
    public PFS__c createPfsWithoutReset() {
        return (PFS__c)buildWithoutReset();
    }

    /**
     * @description Create the current working PFS__c record.
     * @return The currently operated upon PFS__c record.
     */
    public PFS__c create() {
        return (PFS__c)super.buildWithReset();
    }

    /**
     * @description The default PFS__c record.
     */
    public PFS__c DefaultPfs {
        get {
            if (DefaultPfs == null) {
                DefaultPfs = createPfsWithoutReset();
                insert DefaultPfs;
            }
            return DefaultPfs;
        }
        private set;
    }

    /**
     * @description Get the PFS__c SObjectType.
     * @return The PFS__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return PFS__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static PfsTestData Instance {
        get {
            if (Instance == null) {
                Instance = new PfsTestData();
            }
            return Instance;
        }
        private set;
    }

    private PfsTestData() { }
}