@isTest
private class AcademicYearUtilTest {

    @isTest
    private static void is20172018OrLater_recordIs20162017_expectFalse() {
        Academic_Year__c record = TestUtils.createAcademicYear('2016-2017', true);

        System.assertEquals(false, AcademicYearUtil.is20172018OrLater(record),
                'Expected false for the 2016-2017 academic year record.');
    }

    @isTest
    private static void is20172018OrLater_recordIs20172018_expectTrue() {
        Academic_Year__c record = TestUtils.createAcademicYear('2017-2018', true);

        System.assertEquals(true, AcademicYearUtil.is20172018OrLater(record),
                'Expected true for the 2017-2018 academic year record.');
    }

    @isTest
    private static void is20172018OrLater_recordIs20182019_expectFalse() {
        Academic_Year__c record = TestUtils.createAcademicYear('2018-2019', true);

        System.assertEquals(true, AcademicYearUtil.is20172018OrLater(record),
                'Expected true for the 2018-2019 academic year record.');
    }

    @isTest
    private static void is20182019OrLater_recordIs20172018_expectFalse() {
        String academicYearName = '2017-2018';

        System.assertEquals(false, AcademicYearUtil.is20182019OrLater(academicYearName),
                'Expected false for the 2017-2018 academic year.');
    }

    @isTest
    private static void is20182019OrLater_recordIs20182019_expectTrue() {
        String academicYearName = '2018-2019';

        System.assertEquals(true, AcademicYearUtil.is20182019OrLater(academicYearName),
                'Expected true for the 2018-2019 academic year.');
    }

    @isTest
    private static void is20182019OrLater_recordIs20192020_expectFalse() {
        String academicYearName = '2019-2020';

        System.assertEquals(true, AcademicYearUtil.is20182019OrLater(academicYearName),
                'Expected true for the 2019-2020 academic year.');
    }
}