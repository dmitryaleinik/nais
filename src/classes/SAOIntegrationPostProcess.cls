/**
 * SAOIntegrationPostProcess.cls
 *
 * @description: Handles postprocessing of SAO integration api calls.
 * Currently called after the all API calls and maps student info into the requried objects.
 *
 * @author: Mike Havrilla @ Presence PG
 */

public class SAOIntegrationPostProcess implements IntegrationApiModel.IAction {

    private final String STUDENT_ID_FIELD = 'folioId';
    private final String STUDENTS_API = 'Students';

    //ctor
    public SAOIntegrationPostProcess() {}

    /**
	 * @description: Handles the post processing of SAO integration. Maps students,
	 * processes applicants and populates any additional fields before allowing IntegrationFinalize
	 * to insert the records
	 *
	 * @param: apiState - A Current Instance of IntegrationApiModel.ApiState
	 */
    public void invoke( IntegrationApiModel.ApiState apiState) {

        if( apiState.currentApi == STUDENTS_API) {

            // Build out applicants to studentIdToSchoolsMap
            // Set schools.
            apiState.familyModel.studentIdToSchoolsMap = this.getStudentsToNcesId( apiState);

            // Step 2: map response to Sobject
            IntegrationUtils.processApplicants( apiState, this.STUDENT_ID_FIELD, this.STUDENTS_API);
            // check to see if this user has already been added.
            this.checkForExistingApplicant( apiState.familyModel.studentApplicants, apiState.pfs.Id);
            this.populateFields( apiState);
        }
    }

    private void checkForExistingApplicant( List<sObject> applicantsToValidate, Id pfsId) {

        Set<String> saoIds = new Set<String>();
        for( sObject applicant : applicantsToValidate) {

            saoIds.add( (String)applicant.get( 'saoId__c'));
        }
		System.debug('DEBUG::: checkForExistingApplicant:SAOID:' + saoIds);
        List<Applicant__c> applicants = [ select Id, saoId__c, First_Name__c, Last_Name__c
                                            from Applicant__c
                                           where saoId__c in :saoIds
                                             and saoId__c != null
                                           limit 10000];


        Boolean hasApplicant = false;
        String errorMessage = 'Applicants have already been created for the following user(s): ';
        for( Applicant__c existingApplicant : applicants) {

            for( sObject newApplicant : applicantsToValidate) {

                if( (String)newApplicant.get( 'saoId__c') == existingApplicant.saoId__c ) {

                    errorMessage += (String)newApplicant.get( 'first_name__c') + ' ' + (String)newApplicant.get( 'last_name__c');
                    hasApplicant = true;
                }
            }

        }

        if( hasApplicant) {

            throw new IntegrationApiService.ApplicantExistsException( errorMessage);
        }

    }

    /**
	 * @description: Use this method to set fields on Applicant__c object that are specific to this integration
	 *
	 * @param: apiState - A Current Instance of IntegrationApiModel.ApiState
	 * @param: apiState - A Current Instance of IntegrationApiModel.ApiState
	 */
    private void populateFields( IntegrationApiModel.ApiState apiState) {

        for ( sObject applicant : apiState.familyModel.studentApplicants) {

            String grade = (String)applicant.get('Current_Grade__c');
            if ( grade.containsIgnoreCase( 'Grade')) {

                grade = grade.remove( 'Grade');
            }

            applicant = apiState.familyModel.populateFieldByType( applicant, 'Current_Grade__c', grade);
        }
    }

    /**
	 * @description: Loops over the raw student api response and gets the nces code that is nested
	 * and maps that to a student id.
	 *
	 * @param: apiState - A Current Instance of IntegrationApiModel.ApiState
	 *
	 * @return: Map<String, List<String>> - a collection of StudentId's to List of nces Id's
	 */
    private Map<String, List<String>> getStudentsToNcesId( IntegrationApiModel.ApiState apiState) {

        Map<String, List<String>> returnVal = new Map<String, List<String>>();
        List<Object> data = apiState.httpResponseByApi.get( STUDENTS_API);
        for ( Object source : data) {

            List<String> schoolIds = new List<String>();
            Map<String, Object> sourceValues = ( Map<String, Object>)source;
            String studentId = (String)sourceValues.get( this.STUDENT_ID_FIELD);

            if ( returnVal.containsKey( studentId)) schoolIds = returnVal.get( studentId);

            List<Object> myValue = (List<Object>)IntegrationUtils.getValuesFromObject( sourceValues, 'currentSchoolApplications');
            for ( Object o : myValue) {
                String ncesId = (String)IntegrationUtils.getValuesFromObject( ( Map<String, Object>)o, 'school.code');
                schoolIds.add( ncesId);
            }
            returnVal.put( studentId, schoolIds);
        }

        return returnVal;
    }
}