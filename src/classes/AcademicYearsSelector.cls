/**
 * @description This class is responsible for querying Academic Year records.
 */
public class AcademicYearsSelector extends fflib_SObjectSelector {

    @testVisible private static final String ID_SET_PARAM = 'idSet';

    /**
     * @description Default constructor for an instance of the AcademicYearsSelector that will not include field sets while
     *              NOT enforcing FLS or CRUD.
     */
    public AcademicYearsSelector() {
        // We have to avoid enforcing FLS or CRUD so that guest users can access these records as well.
        // This seemed less likely to break the portals than actually relying on guest permissions.
        super(false, false, false, false);
    }

    /**
     * @description Queries Academic Year records by Id.
     * @param idSet The Ids of the Academic Year records to query.
     * @return A list of Academic Year records.
     * @throws ArgumentNullException if idSet is null.
     */
    public List<Academic_Year__c> selectById(Set<Id> idSet) {
        ArgumentNullException.throwIfNull(idSet, ID_SET_PARAM);

        return (List<Academic_Year__c>)super.selectSObjectsById(idSet);
    }
    
    /**
     * @description Queries Academic Year records by Name.
     * @param names The names of the Academic Year records to query.
     * @return A list of Academic Year records.
     * @throws ArgumentNullException if names is null.
     */
    public List<Academic_Year__c> selectByName(Set<String> names) {
        ArgumentNullException.throwIfNull(names, 'names');
        
        assertIsAccessible();

        String query = newQueryFactory()
                .setCondition('Name IN :names')
                .toSOQL();

        return Database.query(query);
    }

    /**
     * @description Queries all Academic Years ordered by start date in descending order.
     * @return A list of academic year records.
     */
    public List<Academic_Year__c> selectAll() {
        return Database.query(newQueryFactory().toSOQL());
    }

    private Schema.SObjectType getSObjectType() {
        return Academic_Year__c.SObjectType;
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>{
                Academic_Year__c.Name,
                Academic_Year__c.Start_Date__c,
                Academic_Year__c.End_Date__c,
                Academic_Year__c.Family_Portal_Start_Date__c,
                Academic_Year__c.Family_Portal_End_Date__c,
                Academic_Year__c.Family_Landing_Page_Start_Date__c,
                Academic_Year__c.Include_Adjustable_IPA_Setting__c,
                Academic_Year__c.Non_PFS_Folder_Open__c,
                Academic_Year__c.Non_PFS_Folder_Close__c
        };
    }

    /**
     * @description Specifies the default ordering of Academic Years which is by Start Date in descending order.
     * @return The Order By criteria.
     */
    public override String getOrderBy() {
        return 'Start_Date__c DESC';
    }

    /**
     * @description Creates a new instance of the AcademicYearsSelector.
     * @return An instance of AcademicYearsSelector.
     */
    public static AcademicYearsSelector newInstance() {
        return new AcademicYearsSelector();
    }

    /**
     * @description Singleton instance property.
     */
    public static AcademicYearsSelector Instance {
        get {
            if (Instance == null) {
                Instance = newInstance();
            }
            return Instance;
        }
        private set;
    }
}