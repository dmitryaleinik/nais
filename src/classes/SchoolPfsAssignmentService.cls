/**
 * @description Handle common tasks for School PFS Assignments
 **/
public class SchoolPfsAssignmentService {
    @testVisible private static final String SPA_PARAM = 'spa';
    @testVisible private static final String ANNUAL_SETTING_PARAM = 'annualSetting';

    /**
     * @description Copy over the Personal Judgement settings from
     *              the annual setting provided.
     * @param spa The School PFS Assignment to copy Personal Judgement
     *        settings into.
     * @param annualSetting The Annual Setting to copy Personal Judgement
     *        settings from.
     * @return The completed School PFS Assignment record.
     */
    public School_PFS_Assignment__c copyPersonalJudgementSettings(
            School_PFS_Assignment__c spa, Annual_Setting__c annualSetting) {
        ArgumentNullException.throwIfNull(spa, SPA_PARAM);
        ArgumentNullException.throwIfNull(annualSetting, ANNUAL_SETTING_PARAM);

        spa.Add_Deprec_Home_Bus_Expense__c = annualSetting.Add_Depreciation_Home_Business_Expense__c;
        spa.Use_Home_Equity__c = annualSetting.Use_Home_Equity__c;
        spa.Use_Home_Equity_Cap__c = annualSetting.Use_Home_Equity_Cap__c;
        spa.Use_Housing_Index_Multiplier__c = annualSetting.Use_Housing_Index_Multiplier__c;
        spa.Use_Div_Int_to_Impute_Assets__c = annualSetting.Use_Dividend_Interest_to_Impute_Assets__c;
        spa.Impute__c = annualSetting.Impute_Rate__c;
        spa.Apply_Minimum_Income__c = annualSetting.Apply_Minimum_Income__c;
        spa.Minimum_Income_Amount__c = annualSetting.Minimum_Income_Amount__c;
        spa.Use_COLA__c = annualSetting.Use_Cost_of_Living_Adjustment__c;
        spa.Override_Default_COLA_Value__c = annualSetting.Override_Default_COLA_Value__c;
        spa.Adjust_Housing_Portion_of_IPA__c = annualSetting.Adjust_Housing_Portion_of_IPA__c;
        spa.IPA_Housing_Family_of_4__c = annualSetting.IPA_Housing_Family_of_4__c;

        return spa;
    }

    /**
     * @description Singleton instance for the SchoolPfsAssignmentService.
     */
    public static SchoolPfsAssignmentService Instance {
        get {
            if (Instance == null) {
                Instance = new SchoolPfsAssignmentService();
            }
            return Instance;
        }
        private set;
    }

    private SchoolPfsAssignmentService() {}
}