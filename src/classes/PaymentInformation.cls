/**
 * @description The base information needed to make a payment using the
 *              payment processor.
 **/
public virtual class PaymentInformation {
    // Billing Information data
    public String BillingFirstName { get; set; }
    public String BillingLastName { get; set; }

    // Billing Address data
    public String BillingStreet1 { get; set; }
    public String BillingStreet2 { get; set; }
    public String BillingCity { get; set; }
    public String BillingState { get; set; }
    public String BillingPostalCode { get; set; }
    public String BillingCountryCode { get; set; }
    public String BillingEmail { get; set; }

    // Payment related data
    public Decimal PaymentAmount { get; set; }
    public String ItemName { get; set; }
    public String Tracker { get; set; }
    public Boolean SendReceipt { get; set; }
}