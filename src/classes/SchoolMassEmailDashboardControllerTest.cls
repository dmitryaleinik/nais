/**
 * SchoolMassEmailDashboardControllerTest.cls
 *
 * @description: Test class for SchoolMassEmailDashboardController using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
private class SchoolMassEmailDashboardControllerTest {
    
    /* test data setup */
    @testSetup 
    static void setupTestData() {
        
        MassEmailSendTestDataFactory.createEmailTemplate();
    }

    /* postive test cases */

    // test controller init w/ header/academic year selector
    @isTest 
    static void constructor_validUser_validInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            Test.startTest();
                
                PageReference massEDashPRef = Page.SchoolMassEmailDashboard;
                Test.setCurrentPage( massEDashPRef);
                SchoolMassEmailDashboardController massEDashController = new SchoolMassEmailDashboardController();
                massEDashController.loadAcademicYear();
                massEDashController.SchoolAcademicYearSelector_OnChange( null);
            Test.stopTest();

            // Assert
            System.assertEquals( massEDashController.Me, massEDashController);
            System.assertEquals( massEDashController.getAcademicYearId(), GlobalVariables.getCurrentAcademicYear().Id);
        }
    }

    // test page action method w/ valid user, valid init
    @isTest 
    static void init_validUser_validInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTData = new MassEmailSendTestDataFactory(); 
            Test.startTest();
                
                MassEmailSendTestDataFactory.createRequiredPFSData();
                Mass_Email_Send__c massES = [select Id, School__c from Mass_Email_Send__c where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
                massES.School__c = tData.school1Acct.Id;
                update massES;

                PageReference massEDashPRef = Page.SchoolMassEmailDashboard;
                Test.setCurrentPage( massEDashPRef);
                SchoolMassEmailDashboardController massEDashController = new SchoolMassEmailDashboardController();
                Boolean enabled = massEDashController.massEmailEnabled;
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, massEDashController.massEmailModelList);
            System.assertEquals( 1, massEDashController.massEmailModelList.size());
        }
    }

    // test change filter method w/ valid user, valid init
    @isTest 
    static void changeFilter_validUser_validFilterChange() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTData = new MassEmailSendTestDataFactory();
            Test.startTest();
                
                MassEmailSendTestDataFactory.createRequiredPFSData();
                Mass_Email_Send__c massES = [select Id, School__c from Mass_Email_Send__c where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
                massES.School__c = tData.school1Acct.Id;
                update massES;
                
                PageReference massEDashPRef = Page.SchoolMassEmailDashboard;
                Test.setCurrentPage( massEDashPRef);
                SchoolMassEmailDashboardController massEDashController = new SchoolMassEmailDashboardController();
                massEDashController.getFilterOptions();
                massEDashController.selectedFilter = MassEmailSendDataAccessService.FILTER_DRAFT;
                massEDashController.changeFilter();
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, massEDashController.massEmailModelList);
            System.assertEquals( 1, massEDashController.massEmailModelList.size());
        }
    }

    // test sort by ascending methods w/ valid user, valid init
    @isTest 
    static void sortByAsc_validUser_validSort() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTData = new MassEmailSendTestDataFactory();
            Test.startTest();
                
                MassEmailSendTestDataFactory.createRequiredPFSData();
                Mass_Email_Send__c massES = [select Id, School__c from Mass_Email_Send__c where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
                massES.School__c = tData.school1Acct.Id;
                update massES;
                
                PageReference massEDashPRef = Page.SchoolMassEmailDashboard;
                Test.setCurrentPage( massEDashPRef);
                SchoolMassEmailDashboardController massEDashController = new SchoolMassEmailDashboardController();
                massEDashController.sortTableByBouncedAsc();
                massEDashController.sortTableByNameAsc();
                massEDashController.sortTableByOpenedAsc();
                massEDashController.sortTableByRecipAsc();
                massEDashController.sortTableByStatusAsc();
                massEDashController.sortTableByUnsubAsc();
                massEDashController.sortTableByDateSentAsc();
                massEDashController.sortTableBySentByAsc();
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, massEDashController.massEmailModelList);
            System.assertEquals( 1, massEDashController.massEmailModelList.size());
        }
    }

    // test sort by descending methods w/ valid user, valid init
    @isTest 
    static void sortByDesc_validUser_validSort() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTData = new MassEmailSendTestDataFactory();
            Test.startTest();
                
                MassEmailSendTestDataFactory.createRequiredPFSData();
                Mass_Email_Send__c massES = [select Id, School__c from Mass_Email_Send__c where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
                massES.School__c = tData.school1Acct.Id;
                update massES;
                
                PageReference massEDashPRef = Page.SchoolMassEmailDashboard;
                Test.setCurrentPage( massEDashPRef);
                SchoolMassEmailDashboardController massEDashController = new SchoolMassEmailDashboardController();
                massEDashController.sortTableByBouncedDesc();
                massEDashController.sortTableByNameDesc();
                massEDashController.sortTableByOpenedDesc();
                massEDashController.sortTableByRecipDesc();
                massEDashController.sortTableByStatusDesc();
                massEDashController.sortTableByUnsubDesc();
                massEDashController.sortTableByDateSentDesc();
                massEDashController.sortTableBySentByDesc();
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, massEDashController.massEmailModelList);
            System.assertEquals( 1, massEDashController.massEmailModelList.size());
        }
    }

    // test resendRecord remoting method w/ valid user, valid init
    @isTest 
    static void resendRecord_validUser_validResend() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTData = new MassEmailSendTestDataFactory();
            Test.startTest();
                
                MassEmailSendTestDataFactory.createRequiredPFSData();
                Mass_Email_Send__c massES = [select Id, School__c from Mass_Email_Send__c where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
                massES.School__c = tData.school1Acct.Id;
                update massES;
                
                PageReference massEDashPRef = Page.SchoolMassEmailDashboard;
                Test.setCurrentPage( massEDashPRef);
                SchoolMassEmailDashboardController massEDashController = new SchoolMassEmailDashboardController();
                String massESId = massEDashController.massEmailModelList[0].id;
                SchoolMassEmailDashboardController.resendRecord( massESId);
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, massEDashController.massEmailModelList);
            System.assertEquals( 1, massEDashController.massEmailModelList.size());
        }
    }

    // test getRecordStatus remoting method w/ valid user, valid init
    @isTest 
    static void getRecordStatus_validUser_validRecord() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTData = new MassEmailSendTestDataFactory();
            Test.startTest();
                
                MassEmailSendTestDataFactory.createRequiredPFSData();
                Mass_Email_Send__c massES = [select Id, School__c from Mass_Email_Send__c where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
                massES.School__c = tData.school1Acct.Id;
                update massES;
                
                PageReference massEDashPRef = Page.SchoolMassEmailDashboard;
                Test.setCurrentPage( massEDashPRef);
                SchoolMassEmailDashboardController massEDashController = new SchoolMassEmailDashboardController();
                String massESId = massEDashController.massEmailModelList[0].id;
                SchoolMassEmailDashboardController.getRecordStatus( massESId);
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, massEDashController.massEmailModelList);
            System.assertEquals( 1, massEDashController.massEmailModelList.size());
        }
    }

}