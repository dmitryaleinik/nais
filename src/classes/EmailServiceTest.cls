/**
 * EmailServiceTest.cls
 *
 * @description: Test class for EmailService using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
private class EmailServiceTest {
    
    /* test data setup */
    @testSetup static void setupTestData() {
        
        MassEmailSendTestDataFactory.createEmailTemplate();
        
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
        }
    }
    
    /* negative test cases */

    // Attempt sendEmails(Id) with null param, expect null object ref returned
    @isTest static void sendEmails_withNull_nullResult() {
        
        // Arrange
        EmailResult result;
        Id testId = null;

        // Act
        EmailService.getInstance().sendEmails( testId); 

        // Assert
    }

    // Attempt sendEmails(List<Id>) with null param, expect null object ref returned
    @isTest static void sendEmails_withNullList_nullResult() {
        
        // Arrange
        List<EmailResult> resultList;
        List<Id> testIdList = null;

        // Act
        EmailService.getInstance().sendEmails( testIdList); 

        // Assert
    }

    // Attempt sendTestEmailAsync() with null params, expect no exception
    @isTest static void sendTestEmailAsync_withNullParams_earlyReturn() {
        
        // Arrange
        // no work

        // Act
        EmailService.sendTestEmailAsync( null, null); 

        // Assert
    }


    /* postive test cases */
    // NOTE: (as of 7/2016)
    // Due to a bug introduced in Spring '15, Test.start/stopTest() commits are not getting cleared when a future method with the callout annotation
    // is called, meaning any DML before future callout is treated as one transaction, resulting in "You have uncommitted work pending" exception
    // see links: 
    // http://www.joe-ferraro.com/2014/04/apex-unit-testing-test-data-and-test-setmock/
    // https://patlatus.wordpress.com/2015/02/12/bugs-in-salesforce-spring-15-causing-system-calloutexception-you-have-uncommitted-work-pending-please-commit-or-rollback-before-calling-out/
    // https://success.salesforce.com/issues_view?id=a1p300000008XHBAA2
    // Because a Mass_Email_Send__c record must exist before we can make callout, there is no way to trigger this and
    // not receive an exception - exception is caught and gracefully handled, these calls are still necessary for test coverage.

    // Attempt sendEmails with valid param, this will trigger send and test callout
    // this call will result in future callout, which will fail due to above bug, result will be null
    @isTest static void sendEmails_withValidParam_validEmailResult() {
        
        // Arrange
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {

            // Create Academic Year, Applicant, PFS && PFS Assignment
            MassEmailSendTestDataFactory.createRequiredPFSData();
            EmailResult result;
            Mass_Email_Send__c massES = [select Id from Mass_Email_Send__c where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];

            // Act
            Test.startTest();
                
                Test.setMock( HttpCalloutMock.class, new SendgridMockHttpResponseGenerator());
                EmailService.getInstance().sendEmails( massES.Id); 
            Test.stopTest();
        }

        // Assert
    }

    // Attempt sendEmails with valid param, this will trigger send and test callout
    // this call will result in future callout, which will fail due to above bug, result will be null
    @isTest static void sendEmails_withValidParamSingleRecip_validEmailResult() {
        
        // Arrange
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {

            // Create Academic Year, Applicant, PFS && PFS Assignment
            MassEmailSendTestDataFactory.createRequiredPFSData();
            EmailResult result;
            Mass_Email_Send__c massES = [select Id from Mass_Email_Send__c where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL2_NAME];

            // Act
            Test.startTest();
                
                Test.setMock( HttpCalloutMock.class, new SendgridMockHttpResponseGenerator());
                EmailService.getInstance().sendEmails( massES.Id); 
            Test.stopTest();
        }

        // Assert
    }

    // Attempt sendTestEmailAsync() with valid params, callout succeeds
    @isTest static void sendTestEmailAsync_withValidParams_validTestSend() {
        
        // Arrange
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {

            // Create Academic Year, Applicant, PFS && PFS Assignment 
            MassEmailSendTestDataFactory.createRequiredPFSData();
            EmailResult result;
            Mass_Email_Send__c massES = [select Id from Mass_Email_Send__c where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL2_NAME];

            // Act
            Test.startTest();
                
                Test.setMock( HttpCalloutMock.class, new SendgridMockHttpResponseGenerator());
                EmailService.sendTestEmailAsync( massES.Id, 'testemail@email.com');
            Test.stopTest();
        }

        // Assert
        // Nothing to assert as test email does not change record status and happens async
    }

}