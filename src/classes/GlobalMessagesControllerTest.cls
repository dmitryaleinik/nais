@IsTest
private class GlobalMessagesControllerTest
{

    @isTest
    private static void testGettersSetters()
    {
        Academic_Year__c academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;

        String portalNameValue = 'Family Portal';
        String addPortalNameValue = 'Spring CM - Maintenance Window';
        Id academicYearIdValue = academicYear.Id;

        Test.startTest();
            GlobalMessagesController controller = new GlobalMessagesController();
            controller.portalName = portalNameValue;
            controller.addPortalName = addPortalNameValue;
            controller.academicYearId = academicYearIdValue;
        Test.stopTest();

        System.assertEquals(portalNameValue, controller.portalName);
        System.assertEquals(addPortalNameValue, controller.addPortalName);
        System.assertEquals(academicYearIdValue, controller.academicYearId);
    }

    @IsTest
    private static void testGetGlobalMessages()
    {
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
        Academic_Year__c nextAcademicYear = AcademicYearTestData.Instance.asNextYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, nextAcademicYear};

        String applicantInformationScreenName = 'ApplicantInformation';
        String selectSchoolsScreenName = 'SelectSchools';
        FamilyAppSettings__c fasApplicantInformation = FamilyAppSettingsTestData.Instance.forName(applicantInformationScreenName).create();
        FamilyAppSettings__c fasSelectSchools = FamilyAppSettingsTestData.Instance.forName(selectSchoolsScreenName).create();
        insert new List<FamilyAppSettings__c>{fasApplicantInformation, fasSelectSchools};

        String familyPortalValue = 'Family Portal';
        String schoolPortalValue = 'School Portal';
        String addPortalNameValue = 'Spring CM - Maintenance Window';
        Id academicYearIdValue = currentAcademicYear.Id;
        String familyApplicationMainPage = 'FamilyApplicationMain';
        String familyMessagesPage = 'FamilyMessages';

        Global_Message__c gm1 = GlobalMessageTestData.Instance
            .forAcademicYear(currentAcademicYear.Name)
            .forPortal(familyPortalValue).create();
        Global_Message__c gm2 = GlobalMessageTestData.Instance
            .forAcademicYear(nextAcademicYear.Name)
            .forPortal(familyPortalValue).create();
        Global_Message__c gm3 = GlobalMessageTestData.Instance
            .forAcademicYear(currentAcademicYear.Name)
            .forPortal(schoolPortalValue).create();
        Global_Message__c gm4 = GlobalMessageTestData.Instance
            .forPageName(familyMessagesPage)
            .forPortal(schoolPortalValue).create();
        Global_Message__c gm5 = GlobalMessageTestData.Instance
            .forPageName(selectSchoolsScreenName)
            .forAcademicYear(currentAcademicYear.Name)
            .forPortal(schoolPortalValue).create();
        Global_Message__c gm6 = GlobalMessageTestData.Instance
            .forPageName(applicantInformationScreenName)
            .forAcademicYear(currentAcademicYear.Name)
            .forPortal(schoolPortalValue).create();
        Global_Message__c gm7 = GlobalMessageTestData.Instance
            .forPageName(familyApplicationMainPage)
            .forPortal(schoolPortalValue).create();

        GlobalMessagesController controller = new GlobalMessagesController();
        controller.portalName = familyPortalValue;
        controller.addPortalName = addPortalNameValue;
        controller.academicYearId = academicYearIdValue;

        PageReference pageRef = Page.FamilyMessages;
        Test.setCurrentPage(pageRef);

        Test.startTest();
            List<Global_Message__c> globalMessages = controller.getGlobalMessages();
            System.assert(globalMessages.isEmpty());

            insert new List<Global_Message__c>{gm1, gm2, gm3, gm4};
            globalMessages = controller.getGlobalMessages();
            System.assertEquals(1, globalMessages.size());
            System.assertEquals(gm4.Id, globalMessages[0].Id);

            gm1.Portal__c = addPortalNameValue + ';' + familyPortalValue;
            gm2.Portal__c = addPortalNameValue + ';' + familyPortalValue;
            gm3.Portal__c = addPortalNameValue + ';' + schoolPortalValue;
            gm4.Portal__c = addPortalNameValue + ';' + schoolPortalValue;
            update new List<Global_Message__c>{gm1, gm2, gm3, gm4};
            globalMessages = controller.getGlobalMessages();
            System.assertEquals(2, globalMessages.size());
            System.assert((new Map<Id, Global_Message__c>(globalMessages).keySet()).containsAll(new Set<Id>{gm1.Id, gm4.Id}));

            pageRef = Page.FamilyApplicationMain;
            pageRef.getParameters().put('screen', selectSchoolsScreenName);
            Test.setCurrentPage(pageRef);
            insert new List<Global_Message__c>{gm5, gm6, gm7};
            globalMessages = controller.getGlobalMessages();
            System.assertEquals(3, globalMessages.size());
            System.assert((new Map<Id, Global_Message__c>(globalMessages).keySet()).containsAll(new Set<Id>{gm1.Id, gm5.Id, gm7.Id}));
        Test.stopTest();
    }
}