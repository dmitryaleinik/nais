/*
* NAIS-2344
*
* Page for create PFS Comments
*
*/
@isTest
private class SchoolCommentControllerTest
{

    private class TestDataLocal
    {
        Account school1;
        Contact staff1, parentA, parentB, student1, student2, student3, student4;
        String academicYearId;
        PFS__c pfs1;
        Applicant__c applicant1, applicant2, applicant3, applicant4;
        Student_Folder__c studentFolder1, studentFolder2, studentFolder3, studentFolder4;
        School_PFS_Assignment__c spfsa1, spfsa2, spfsa3, spfsa4;
        Required_Document__c rd1;
        Family_Document__c fd1;
        School_Document_Assignment__c sda1, sda2, sda3, sda4;
        
        private TestDataLocal() {
            // school
            school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, null, false); 
            insert school1;       
        
            // school staff
            staff1 = TestUtils.createContact('Staff 1', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
            insert staff1;
            
            // parents and students
            parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
            parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
            student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
            student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
            student3 = TestUtils.createContact('Student 3', null, RecordTypes.studentContactTypeId, false);
            student4 = TestUtils.createContact('Student 4', null, RecordTypes.studentContactTypeId, false);
            insert new List<Contact> {parentA, parentB, student1, student2, student3, student4};

            // academic year
            TestUtils.createAcademicYears();
            academicYearId = GlobalVariables.getCurrentAcademicYear().Id;  
            
            // psf
            pfs1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
            pfs1.Parent_B__c = parentB.Id;
            insert new List<PFS__c> {pfs1};
            
            // applicant
            applicant1 = TestUtils.createApplicant(student1.Id, pfs1.Id, false);
            applicant2 = TestUtils.createApplicant(student2.Id, pfs1.Id, false);
            applicant3 = TestUtils.createApplicant(student3.Id, pfs1.Id, false);
            applicant4 = TestUtils.createApplicant(student4.Id, pfs1.Id, false);
            insert new List<Applicant__c> {applicant1, applicant2, applicant3, applicant4};
            
            // student folder
            studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
            studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', academicYearId, student2.Id, false);
            studentFolder3 = TestUtils.createStudentFolder('Student Folder 3', academicYearId, student3.Id, false);
            studentFolder4 = TestUtils.createStudentFolder('Student Folder 4', academicYearId, student4.Id, false);
            insert new List<Student_Folder__c> {studentFolder1, studentFolder2, studentFolder3, studentFolder4};
            
            // school pfs assignment
            spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1.Id, school1.Id, studentFolder1.Id, false);
            spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2.Id, school1.Id, studentFolder2.Id, false);
            spfsa3 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant3.Id, school1.Id, studentFolder3.Id, false);
            spfsa4 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant4.Id, school1.Id, studentFolder4.Id, false);
            insert new List<School_PFS_Assignment__c> {spfsa1, spfsa2, spfsa3, spfsa4};
        }
    }

    @isTest
    private static void testSchoolComment()
    {
        TestDataLocal td = new TestDataLocal();

        Test.startTest();
        PageReference pageRef = Page.SchoolComment;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        pageRef.getParameters().put('isViewMode', 'false');
        pageRef.getParameters().put('yearIsEditable', 'true');
        
        Test.setCurrentPage(pageRef);
        SchoolCommentController controller = new SchoolCommentController();
        
        system.assertEquals(controller.isViewMode, false);
        system.assertEquals(controller.yearIsEditable, true);
        
        //New Comment
        controller.note.Name = 'Test Comment New';
        controller.note.Body__c = 'This is a test comment.';
        controller.savelNote();
        
        //Edit Comment
        SchoolCommentController controller2 = new SchoolCommentController();
        controller2.noteId = controller.noteId;
        controller2.note.Name = 'Test Comment Edited';
        controller2.savelNote();
        
        //Cancel
        controller.cancelNote();
    }

    @isTest
    private static void testSchoolCommentInsertWithSiblings()
    {
        TestDataLocal td = new TestDataLocal();

        Test.startTest();
        PageReference pageRef = Page.SchoolComment;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        pageRef.getParameters().put('isViewMode', 'false');
        pageRef.getParameters().put('yearIsEditable', 'true');
        
        Test.setCurrentPage(pageRef);
        SchoolCommentController controller = new SchoolCommentController();
        
        system.assertEquals(controller.isViewMode, false);
        system.assertEquals(controller.yearIsEditable, true);
        
        //New Comment
        controller.note.Name = 'Test Comment New';
        controller.note.Body__c = 'This is a test comment.';
        controller.copyToSiblings = true;
        controller.savelNote();
        
        List<Note__c> savedNotes = [Select Id, ParentID__c from Note__c];
        System.assertEquals(4, savedNotes.size());
        Set<Id> spaIds = new Set<Id>();
        for (Note__c note : savedNotes){
            spaIds.add(note.ParentId__c);
        }
        System.assertEquals(4, spaIds.size());
        System.assert(spaIds.contains(td.spfsa1.ID));
        System.assert(spaIds.contains(td.spfsa2.ID));
        System.assert(spaIds.contains(td.spfsa3.ID));
        System.assert(spaIds.contains(td.spfsa4.ID));
    }

    @isTest
    private static void testSchoolCommentInsertWithSiblingsEdit()
    {
        TestDataLocal td = new TestDataLocal();

        Test.startTest();
        PageReference pageRef = Page.SchoolComment;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        pageRef.getParameters().put('isViewMode', 'false');
        pageRef.getParameters().put('yearIsEditable', 'true');
        
        Test.setCurrentPage(pageRef);
        SchoolCommentController controller = new SchoolCommentController();
        
        system.assertEquals(controller.isViewMode, false);
        system.assertEquals(controller.yearIsEditable, true);
        
        //New Comment
        controller.note.Name = 'Test Comment New';
        controller.note.Body__c = 'This is a test comment.';
        controller.copyToSiblings = true;
        controller.savelNote();
        
        List<Note__c> savedNotes = [Select Id, ParentID__c from Note__c];
        System.assertEquals(4, savedNotes.size());
        Set<Id> spaIds = new Set<Id>();
        for (Note__c note : savedNotes){
            spaIds.add(note.ParentId__c);
        }
        System.assertEquals(4, spaIds.size());
        System.assert(spaIds.contains(td.spfsa1.ID));
        System.assert(spaIds.contains(td.spfsa2.ID));
        System.assert(spaIds.contains(td.spfsa3.ID));
        System.assert(spaIds.contains(td.spfsa4.ID));

        //Edit Comment
        pageRef.getParameters().put('Id', controller.note.Id);
        Test.setCurrentPage(pageRef);
        SchoolCommentController controller2 = new SchoolCommentController();
        System.assertEquals(controller.note.Id, controller2.note.Id);
        controller2.note.Name = 'Test Comment Edited';
        controller2.copyToSiblings = true;
        controller2.savelNote();

        savedNotes = [Select Id, ParentID__c from Note__c];
        System.assertEquals(7, savedNotes.size()); // original note got updated, other three siblings got new comments, so 7 total
    }
}