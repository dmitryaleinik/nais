/**
 * @description This class is used to retrieve the Household Income Threshold for a specific Academic Year / Family Size
*/
public without sharing class IncomeThresholdService {
    
    @testVisible private static final Decimal INCOME_INCREMENT = 10000;
    @testVisible private static final String FAMILY_SIZE_PARAM = 'familySize';
    @testVisible private static final String ACADEMIC_YEAR_ID_PARAM = 'academicYearId';
    
    public Decimal getIncomeThreshold(Integer familySize, String academicYearId) {
        ArgumentNullException.throwIfNull(familySize, FAMILY_SIZE_PARAM);
        ArgumentNullException.throwIfNull(academicYearId, ACADEMIC_YEAR_ID_PARAM);
        
        if( !incomesByAcademicYear.containsKey(academicYearId) ) {
            incomesByAcademicYear.put(academicYearId, new IncomeThreshold(academicYearId));
        }
        
        return calculateIncomeThresholdValue( incomesByAcademicYear.get(academicYearId), familySize );
    }
    
    private static Map<String, IncomeThreshold> incomesByAcademicYear {
        get {
            if (incomesByAcademicYear == null) {
                incomesByAcademicYear = new Map<String, IncomeThreshold>();
            }
            return incomesByAcademicYear;
        }
        private set;
    }
    
    /**
    * @description Return the Income Threshold for the specified family size.
    *              If the specified family size is greater than the value on any of our records, 
    *              add $10,000 to the largest cutoff for each additional family member.
    * @param record The wrapper that contains the map with the Income Threshold values for an specific
    *               academic year.
    * @param familySize the specified family size for which the Income Threshold needs to be calculated.
    */
    private Decimal calculateIncomeThresholdValue(IncomeThreshold record, Integer familySize) {
        Decimal total = 0;
        
        if( record !=null && record.incomes != null ) {
            
            if( record.incomes.containsKey(familySize) ) {
                total = record.incomes.get(familySize).Income_Threshold__c;
            } else {
                
                if( record.greaterFamilySize == -1 ) {
                    total = 0;
                } else {
                    Decimal cutOff = familySize - record.greaterFamilySize; 
                    total = (record.incomes.get(record.greaterFamilySize)).Income_Threshold__c + (cutOff * INCOME_INCREMENT);
                }
            }
        }
        
        return total;
    }
    
    /**
    * @description Return the list of Household Income Threshold records for a given Academic Year.
    * @param academicYearId the Academic Year Name for which the Income Threshold records needs to be retrived.
    */
    private static List<Household_Income_Threshold__c> getIncomeThresholdByAcademicYear(String academicYearId) {
        
        return HouseholdIncomeThresholdSelector.Instance.selectByAcademicYear(academicYearId);
    }
    
    private IncomeThresholdService() { }
    
    /**
    * @description Singleton instance property.
    */
    public static IncomeThresholdService Instance {
        get {
            if (Instance == null) {
                Instance = new IncomeThresholdService();
            }
            return Instance;
        }
        private set;
    }
    
    class IncomeThreshold{
        public Map<Decimal, Household_Income_Threshold__c>  incomes{ get; set; }
        public Decimal greaterFamilySize{ get; set; }
        
        public IncomeThreshold(String varAcademicYear) {
            List<Household_Income_Threshold__c> listIncomes = getIncomeThresholdByAcademicYear(varAcademicYear);
            incomes = new Map<Decimal, Household_Income_Threshold__c>();
            greaterFamilySize = -1;
            
            for(Household_Income_Threshold__c varIncome : listIncomes) {
                
                if(varIncome.Family_Size__c > greaterFamilySize) {
                    greaterFamilySize = varIncome.Family_Size__c;
                }
                incomes.put(varIncome.Family_Size__c, varIncome);
            }
        }
    }
}