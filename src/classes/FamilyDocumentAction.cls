public class FamilyDocumentAction 
{

    private static final String PARENT_A = 'Parent A';
    private static final String PARENT_B = 'Parent B';
    @testVisible private static final String W2_WAGES_FIELD = 'W2_Wages__c';
    @testVisible private static final String SUM_W2_WAGES_PARENT_A = 'Sum_W2_Wages_Parent_A__c';
    @testVisible private static final String SUM_W2_WAGES_PARENT_B = 'Sum_W2_Wages_Parent_B__c';
    @testVisible private static final String PY_SUM_W2_WAGES_PARENT_A = 'PY_Sum_W2_Wages_Parent_A__c';
    @testVisible private static final String PY_SUM_W2_WAGES_PARENT_B = 'PY_Sum_W2_Wages_Parent_B__c';

    // Set of fields that were previously calculated directly in the FCW Controller [jB]
    public static Final Map<String, String> familyDocumentFieldsToSum = new Map<String, String> {
        'W2_Wages__c' => '',
        'W2_Deferred_Untaxed_1_A__c' => 'Sum_Pre_Tax_Payments_To_Retirement_Plans__c',
        'W2_Deferred_Untaxed_2_B__c' => 'Sum_Pre_Tax_Payments_To_Retirement_Plans__c',
        'W2_Deferred_Untaxed_3_C__c' => 'Sum_Pre_Tax_Payments_To_Retirement_Plans__c',
        'W2_Deferred_Untaxed_4_D__c' => 'Sum_Pre_Tax_Payments_To_Retirement_Plans__c',
        'W2_Deferred_Untaxed_5_E__c' => 'Sum_Pre_Tax_Payments_To_Retirement_Plans__c',
        'W2_Deferred_Untaxed_6_F__c' => 'Sum_Pre_Tax_Payments_To_Retirement_Plans__c',
        'W2_Deferred_Untaxed_7_G__c' => 'Sum_Pre_Tax_Payments_To_Retirement_Plans__c',
        'W2_Deferred_Untaxed_8_H__c' => 'Sum_Pre_Tax_Payments_To_Retirement_Plans__c',
        'W2_SS_Tax__c' => 'Sum_W2_SS_Tax__c',
        'W2_Medicare__c' => 'Sum_W2_Medicare__c',
        // SCH C
        'Sch_C_Depreciation_Sect179__c' => 'Sum_Sch_C_Depreciation_Sect_179__c',
        'Sch_C_Exp_Bus_Uses_Home__c' => 'Sum_Sch_C_Exp_Bus_Uses_Home__c',
        // SCH E
        'Sch_E_Depreciation_Total__c' => 'Sum_Sch_E_Depreciation_Total__c',
        // SCH F
        'Sch_F_Depreciation_Sec179_Exp__c' => 'Sum_Sch_F_Depreciation_Sec_179_Exp__c'
    };

    public static Final Map<String, String> pyFamilyDocumentFieldsToSum = new Map<String, String> {
        'W2_Wages__c' => '',
        'W2_Deferred_Untaxed_1_A__c' => 'PY_Sum_Pre_Tax_Payments_To_Retirement_Pl__c',
        'W2_Deferred_Untaxed_2_B__c' => 'PY_Sum_Pre_Tax_Payments_To_Retirement_Pl__c',
        'W2_Deferred_Untaxed_3_C__c' => 'PY_Sum_Pre_Tax_Payments_To_Retirement_Pl__c',
        'W2_Deferred_Untaxed_4_D__c' => 'PY_Sum_Pre_Tax_Payments_To_Retirement_Pl__c',
        'W2_Deferred_Untaxed_5_E__c' => 'PY_Sum_Pre_Tax_Payments_To_Retirement_Pl__c',
        'W2_Deferred_Untaxed_6_F__c' => 'PY_Sum_Pre_Tax_Payments_To_Retirement_Pl__c',
        'W2_Deferred_Untaxed_7_G__c' => 'PY_Sum_Pre_Tax_Payments_To_Retirement_Pl__c',
        'W2_Deferred_Untaxed_8_H__c' => 'PY_Sum_Pre_Tax_Payments_To_Retirement_Pl__c',
        'W2_SS_Tax__c' => 'PY_Sum_W2_SS_Tax__c',
        'W2_Medicare__c' => 'PY_Sum_W2_Medicare__c',
        // SCH C
        'Sch_C_Depreciation_Sect179__c' => 'PY_Sum_Sch_C_Depreciation_Sect_179__c',
        'Sch_C_Exp_Bus_Uses_Home__c' => 'PY_Sum_Sch_C_Exp_Bus_Uses_Home__c',
        // SCH E
        'Sch_E_Depreciation_Total__c' => 'PY_Sum_Sch_E_Depreciation_Total__c',
        // SCH F
        'Sch_F_Depreciation_Sec179_Exp__c' => 'PY_Sum_Sch_F_Depreciation_Sec_179_Exp__c'
    };

    public static Final Set<String> spaFields = new Set<String> {
        'Sum_Sch_C_Exp_Bus_Uses_Home__c', 'Sum_Pre_Tax_Payments_To_Retirement_Plans__c',
        'Sum_Sch_C_Depreciation_Sect_179__c', 'Sum_Sch_E_Depreciation_Total__c',
        'Sum_Sch_F_Depreciation_Sec_179_Exp__c', 'Sum_W2_Medicare__c',
        'Sum_W2_SS_Tax__c', 'Sum_W2_Wages_Parent_A__c', 'Sum_W2_Wages_Parent_B__c',
        'PY_Sum_Sch_C_Exp_Bus_Uses_Home__c', 'PY_Sum_Pre_Tax_Payments_To_Retirement_Pl__c',
        'PY_Sum_Sch_C_Depreciation_Sect_179__c', 'PY_Sum_Sch_E_Depreciation_Total__c',
        'PY_Sum_Sch_F_Depreciation_Sec_179_Exp__c', 'PY_Sum_W2_Medicare__c',
        'PY_Sum_W2_SS_Tax__c', 'PY_Sum_W2_Wages_Parent_A__c', 'PY_Sum_W2_Wages_Parent_B__c'
    };

    /* [SL] NAIS-1652: recalc EFC if depreciation fields change  */
    public static void recalculateEFCsFromFamilyDocs(Set<Id> familyDocumentIds) {

        // loop through the school document assignments and get a list of school PFS assignments
        Set<Id> spaIds = new Set<Id>();
        for (School_Document_Assignment__c sda : [SELECT Id, School_PFS_Assignment__c
                FROM School_Document_Assignment__c
                WHERE Document__c IN :familyDocumentIds
                AND School_PFS_Assignment__r.Add_Deprec_Home_Bus_Expense__c = 'Yes'
                        AND School_PFS_Assignment__r.Family_May_Submit_Updates__c = 'Yes']) {
            spaIds.add(sda.School_PFS_Assignment__c);
        }

        System.debug(LoggingLevel.ERROR, 'TESTINGdrew16spaIds ' + spaIds);

        // update the school PFS Assignments
        if (!spaIds.isEmpty()) {
            updateSchoolPFSAssignments(spaIds);
        }
    }
    
    public static void recalculateEFCsFromSDAs(Set<Id> sdaIds, Map<Id, Id> spasWithDeletedDocuments) {

        // loop through the school document assignments and get a list of school PFS assignments for docs with depreciation values set
        Set<Id> spaIds = new Set<Id>();
        Map<Id, Family_Document__c> deletedDocuments = new Map<Id, Family_Document__c>();
        
        //1. We verify if there's SPA for which its FamilyDocument was recently deleted. And that deleted FD have depreciated values.
        //   We do a separated query, because the SPA.Document__c relationship do not exist anymore at this point. So, there's no way to
        //   get the FD record though the SPA. For more details: SFP-1118 and LNI-1766.
        if(spasWithDeletedDocuments != null && !spasWithDeletedDocuments.isEmpty() ) {
            
            //1.1 Mapping the deleted FD will allow us to find them easily.
            deletedDocuments = new Map<Id, Family_Document__c>([
                SELECT Id, Sch_C_Depreciation_Sect179__c, Sch_F_Depreciation_Sec179_Exp__c, Sch_E_Depreciation_Total__c, Sch_C_Exp_Bus_Uses_Home__c 
                FROM Family_Document__c 
                WHERE Id IN: spasWithDeletedDocuments.Values()]);
        }
        
        //2. We loop the SPA with Documents that contain depreciated values. and those values have being changed.
        //   Or if the SPA's Document was deleted. And that FD contained depreciated values.
        Family_Document__c tmpDocument;
        Boolean FDWithDepreciatedValuesWasDeleted;
        for (School_Document_Assignment__c sda : [SELECT Id, Document__c, School_PFS_Assignment__c,
                Document__r.Sch_C_Depreciation_Sect179__c,
                Document__r.Sch_F_Depreciation_Sec179_Exp__c,
                Document__r.Sch_E_Depreciation_Total__c,
                Document__r.Sch_C_Exp_Bus_Uses_Home__c
                FROM School_Document_Assignment__c
                WHERE Id IN :sdaIds
                AND School_PFS_Assignment__r.Add_Deprec_Home_Bus_Expense__c = 'Yes'
                AND School_PFS_Assignment__r.Family_May_Submit_Updates__c = 'Yes']) {
            
            FDWithDepreciatedValuesWasDeleted = spasWithDeletedDocuments != null && spasWithDeletedDocuments.containsKey(sda.Id);
            
            if (sda.Document__c != null || FDWithDepreciatedValuesWasDeleted ) {
                
                tmpDocument = sda.Document__c != null ? sda.Document__r : deletedDocuments.get(spasWithDeletedDocuments.get(sda.Id));
                
                if (tmpDocument != null && isPopulatedDepreciationFields(tmpDocument)) {
                    
                    spaIds.add(sda.School_PFS_Assignment__c);
                }
            }
        }

        // update the school PFS Assignments
        if (!spaIds.isEmpty()) {
            updateSchoolPFSAssignments(spaIds);
        }
    }

    private static void updateSchoolPFSAssignments(Set<Id> spaIds) {
        List<School_PFS_Assignment__c> spasToUpdate = new List<School_PFS_Assignment__c>();
        for (School_PFS_Assignment__c spa : [SELECT Id, Revision_EFC_Calc_Status__c, PFS_Status__c, Payment_Status__c, Visible_to_Schools_Until__c
                                             FROM School_PFS_Assignment__c
                                             WHERE Id IN :spaIds
                                             AND Add_Deprec_Home_Bus_Expense__c = 'Yes'
                                                     AND Family_May_Submit_Updates__c = 'Yes']) {
            if (EfcCalculatorAction.isStatusRecalculable(spa.PFS_Status__c, spa.Payment_Status__c, spa.Visible_to_Schools_Until__c)) {
                spa.Revision_EFC_Calc_Status__c = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE;
                spasToUpdate.add(spa);
            }
        }
        if (!spasToUpdate.isEmpty()) {
            update spasToUpdate;
        }
    }

    public static Boolean isPopulatedDepreciationFields(Family_Document__c fd) {
        return (((fd.Sch_F_Depreciation_Sec179_Exp__c != null) && (fd.Sch_F_Depreciation_Sec179_Exp__c != 0))
                || ((fd.Sch_E_Depreciation_Total__c != null) && (fd.Sch_E_Depreciation_Total__c != 0))
                || ((fd.Sch_C_Exp_Bus_Uses_Home__c != null) && (fd.Sch_C_Exp_Bus_Uses_Home__c != 0))
                || ((fd.Sch_C_Depreciation_Sect179__c != null) && (fd.Sch_C_Depreciation_Sect179__c != 0)));

    }

    /**
     *  SFP-787
     *  [10.3.16] Pull set of fields from Family Docs into SPA so they are not recalculated as sums when FCW page
     *            is loaded [jB]
     *  [10.5.16] Process prior year documents as well - School_Document_Assignment__c should return
     *            School_PFS_Assignment__c for current year but values not copied from prior year
     *            SPA, discard documents from other years [jB]
     *  [10.10.16] Process all school documents and reset sums based on totals rather than just adding to the sum
     *             this will reset sums.
     *             This is a similar procedure as implemented in SFP-640, VerificationAction.cls except it sums
     *             the values to SPA and doesn't change the Verification [jB]
     */
    public static void setSpaFieldsFromFamilyDocument(List<School_PFS_Assignment__c> spas) {
        try{
            Map<String, Verification__c> verificationByHouseholdId = new Map<String, Verification__c>();
            Map<String, Set<Family_Document__c>> familyDocumentsByHouseholdId = new Map<String, Set<Family_Document__c>>();
            Map<String, Family_Document__c> documentsByAcademicYear = new Map<String, Family_Document__c>();
            Map<String, Set<Family_Document__c>> familyDocumentsBySPAId = new Map<String, Set<Family_Document__c>>();
            Map<String, School_PFS_Assignment__c> spasById = new Map<String,School_PFS_Assignment__c>();
            // get all family documents
            String familyDocumentQuery = buildFamilyDocumentQuery(spas);

            // sort all family documents by SPA - this could be more efficient but easier to match
            // pulling SPAs directly from a SDA / FD
            for (Family_Document__c fd : Database.query(familyDocumentQuery)) {
                for (School_Document_Assignment__c sda : fd.School_Document_Assignments__r) {
                    // extract SPA
                    School_PFS_Assignment__c spa = (School_PFS_Assignment__c)sda.getSObject('School_PFS_Assignment__r');
                    if (familyDocumentsBySPAId.get(spa.id) == null) {
                        familyDocumentsBySPAId.put(spa.id, new Set<Family_Document__c>());
                    }
                    familyDocumentsBySPAId.get(spa.id).add(fd);
                    spasById.put(spa.Id, spa);
                }
            }

            Set<School_PFS_Assignment__c> spasToUpdate = new Set<School_PFS_Assignment__c>();
            // Loop over all SPAs by their Id and resum their values for all document fields;
            for (String spaId : familyDocumentsBySPAId.keySet()) {
                School_PFS_Assignment__c tempSPA = spasById.get(spaId);
                Boolean updated = false;
                // reset all values to zero
                for(String spaField : spaFields){
                    tempSPA.put(spaField, null);
                }
                tempSPA.put('Sum_W2_Wages_Parent_A__c', null);
                tempSPA.put('Sum_W2_Wages_Parent_B__c', null);
                tempSPA.put('PY_Sum_W2_Wages_Parent_A__c',null);
                tempSPA.put('PY_Sum_W2_Wages_Parent_B__c',null);

                for (Family_Document__c fd : familyDocumentsBySPAId.get(spaId)) {
                    Decimal sumValue = 0.0;
                    Boolean docIsCurrentDocYear = false;
                    Boolean docIsPreviousTaxYear = false; // process previous year documents
                    Boolean familyDocOrSpaAcademicYearIsNull = false;
                    
                    // Documents uploaded to the current year will have Academic_Year_Picklist = SPA Academic Year Name,
                    // their appropriate year will be set by SpringCM Processor if they came from previous year
                    if (fd.Document_Year__c != null && tempSPA.Academic_Year_Picklist__c != null)
                    {
                        if (fd.Document_Year__c ==
                                GlobalVariables.getDocYearStringFromAcadYearName(tempSPA.Academic_Year_Picklist__c)) 
                        {
                            docIsCurrentDocYear = true;
                        }

                        // if doc is not current year, check if it is previous year
                        if (fd.Document_Year__c ==
                                GlobalVariables.getPrevDocYearStringFromAcadYearName(tempSPA.Academic_Year_Picklist__c)) 
                        {
                            docIsPreviousTaxYear = true;
                        }
                    } else {
                        familyDocOrSpaAcademicYearIsNull = true;
                    }

                    // only process current and previous doc years
                    if (!familyDocOrSpaAcademicYearIsNull && (docIsCurrentDocYear || docIsPreviousTaxYear)) 
                    {
                        Map<String, String> familyDocumentToSpaFields = (docIsCurrentDocYear ? familyDocumentFieldsToSum : pyFamilyDocumentFieldsToSum);
                        for (String familyDocumentField : familyDocumentToSpaFields.keySet()) {
                            if (fd.get(familyDocumentField) != null) {
                                // field = 'W2_Wages__c', set based on Family_Document Pertains to
                                if (familyDocumentField == W2_WAGES_FIELD && fd.get(W2_WAGES_FIELD) != null) {
                                    if (fd.Document_Pertains_To__c == PARENT_A) {
                                        sumValue = tempSPA.get((docIsCurrentDocYear ? SUM_W2_WAGES_PARENT_A : PY_SUM_W2_WAGES_PARENT_A)) != null
                                                   ? (Decimal)tempSPA.get((docIsCurrentDocYear ? SUM_W2_WAGES_PARENT_A : PY_SUM_W2_WAGES_PARENT_A)) : 0.0;
                                        sumValue += (Decimal)fd.get(W2_WAGES_FIELD);
                                        
                                        tempSPA.put((docIsCurrentDocYear ? SUM_W2_WAGES_PARENT_A : PY_SUM_W2_WAGES_PARENT_A), sumValue != 0.0 ? sumValue : null);

                                        if (tempSPA.get((docIsCurrentDocYear ? SUM_W2_WAGES_PARENT_A : PY_SUM_W2_WAGES_PARENT_A)) != null) {
                                            updated = true;
                                        }
                                    }
                                    if (fd.Document_Pertains_To__c == PARENT_B) {
                                        sumValue = tempSPA.get((docIsCurrentDocYear ? SUM_W2_WAGES_PARENT_B : PY_SUM_W2_WAGES_PARENT_B)) != null ? (Decimal)tempSPA.get((docIsCurrentDocYear ? SUM_W2_WAGES_PARENT_B : PY_SUM_W2_WAGES_PARENT_B)) : 0.0;
                                        sumValue += (Decimal)fd.get(W2_WAGES_FIELD);
                                        tempSPA.put((docIsCurrentDocYear ? SUM_W2_WAGES_PARENT_B : PY_SUM_W2_WAGES_PARENT_B), sumValue != 0.0 ? sumValue : null);
                                        if (tempSPA.get((docIsCurrentDocYear ? SUM_W2_WAGES_PARENT_B : PY_SUM_W2_WAGES_PARENT_B)) != null) {
                                            updated = true;
                                        }
                                    }
                                } else {
                                    String spaField = familyDocumentToSpaFields.get(familyDocumentField);
                                    sumValue = tempSPA.get(spaField) != null ?
                                               (Decimal)tempSPA.get(spaField) : 0.0;
                                    sumValue += (Decimal)fd.get(familyDocumentField);
                                    tempSPA.put(spaField, sumValue != 0.0 ? sumValue : null);
                                    if (tempSPA.get(spaField) != null) {
                                        updated = true;
                                    }
                                }
                            }
                        }
                    } else {
                        System.debug(LoggingLevel.ERROR, 'Family Doc or SPA without Academic_Year_Picklist__c value, skipped setting SPA fields from FD: '+fd.Id);
                    }
                } // end processing family docs

                if (updated && !spasToUpdate.contains(tempSPA)) 
                {
                    spasToUpdate.add(tempSPA);
                }
            }

            if (!spasToUpdate.isEmpty()) 
            {
                update new List<School_PFS_Assignment__c>(spasToUpdate);
            }
        } catch(Exception e){
            System.debug('setSpaFieldsFromFamilyDocument exception: '+e.getLineNumber()+': '+e.getMessage());
        }
    }

    private static String buildFamilyDocumentQuery(List<School_PFS_Assignment__c> spas){

        // reuse code from VerificationAction
        Set<String> familyDocumentTypes = new Set<String>(VerificationAction.familyDocumentFieldsByDocumentType.keyset());

        String spaIdsQuery = '(';
        for(School_PFS_Assignment__c spa : spas){
            spaIdsQuery += '\''+spa.Id+'\',';
        }
        spaIdsQuery = spaIdsQuery.substring(0, spaIdsQuery.length()-1);
        spaIdsQuery += ')';

        String familyDocumentTypesQuery = '(';
        for(String familyDocType : familyDocumentTypes){
            familyDocumentTypesQuery += '\''+familyDocType+'\',';
        }
        familyDocumentTypesQuery = familyDocumentTypesQuery.substring(0, familyDocumentTypesQuery.length()-1);
        familyDocumentTypesQuery += ')';

        List<String> spaRelationshipFields = new List<String>();
        for (String spaField : spaFields) {
            spaRelationshipFields.add('School_PFS_Assignment__r.' + spaField);
        }

        // select all family documents that have SDAs to incoming SPAs
        return 'SELECT Document_Year__c, Document_Type__c, Academic_Year_Picklist__c,Document_Pertains_to__c,' +
                String.join(new List<String>(familyDocumentFieldsToSum.keySet()), ',') + ',' +
                '(SELECT Id, ' + String.join(spaRelationshipFields, ',') + ', School_PFS_Assignment__r.Academic_Year_Picklist__c, Academic_Year__c, School_PFS_Assignment__r.School__c, School_PFS_Assignment__r.Academic_Year_Name__c ' +
               ' FROM School_Document_Assignments__r) FROM Family_Document__c WHERE Id in (SELECT Document__c FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c IN '+spaIdsQuery+')'+
               ' AND Document_Type__c IN '+familyDocumentTypesQuery+' AND Document_Status__c = \'Processed\' AND Duplicate__c != \'Yes\'';
    }

    /**
     *  Takes a set of SPAs and reduces it to just those that should have revision fields auto-copied per
     *  the school's annual setting
     */
    public static Map<Id, School_PFS_Assignment__c> findSPAIdsThatShouldAutoCopy(Map<String,Set<School_PFS_Assignment__c>> spasBySchoolId, Set<String> spaAcademicYearNames){
        Map<Id, School_PFS_Assignment__c> autoCopySPAs = new Map<Id, School_PFS_Assignment__c>();

        Map<String, Boolean> copyValuesToSpaBySchoolId = buildAcademicYearAutoCopyMap(spasBySchoolId.keyset(), spaAcademicYearNames);
        for (String schoolId : spasBySchoolId.keySet()) {
            for (School_PFS_Assignment__c spa : spasBySchoolId.get(schoolId)) {
                if (copyValuesToSpaBySchoolId.get(schoolId + '-' + spa.Academic_Year_Name__c) != null &&
                    copyValuesToSpaBySchoolId.get(schoolId + '-' + spa.Academic_Year_Name__c)) {
                    if(!autoCopySPAs.containsKey(spa.Id))
                    {
                        autoCopySPAs.put(spa.Id, spa);
                    }
                }
            }
        }
        return autoCopySPAs;
    }

    // SFP-640 / SFP-811 - Auto adjust SPA values after batch update is completed
    private static Map<String, Boolean> buildAcademicYearAutoCopyMap(Set<String> schoolIds, Set<String> spaAcademicYearNames) {
        Map<String,Boolean> copyValuesToSpaBySchoolId = new Map<String,Boolean>();
        for (Annual_Setting__c annualSetting : [SELECT Academic_Year_Name__c, Copy_Verification_Values_to_Revisions__c, School__c
                                                FROM Annual_Setting__c WHERE School__c IN :schoolIds
                                                AND Academic_Year_Name__c IN :spaAcademicYearNames]) {
            copyValuesToSpaBySchoolId.put(annualSetting.School__c + '-' + annualSetting.Academic_Year_Name__c,
                                          annualSetting.Copy_Verification_Values_to_Revisions__c == 'Yes');
        }
        return copyValuesToSpaBySchoolId;
    }

    /**
     *  Run Auto-Verify batch on all family documents as they are set to Processed
     */
    public static void runAutoVerifyCopy(Set<Family_Document__c> familyDocuments) {
        Set<Id> spaIdsToRunCopyBatch = new Set<Id>();
        Set<String> fdIds = new Set<String>();
        for(Family_Document__c fd : familyDocuments){
            fdIds.add(fd.Id);
        }

        // query for SPAs from SDAs on family docs
        for(School_Document_Assignment__c sda : [SELECT School_PFS_Assignment__c FROM School_Document_Assignment__c
                                                    WHERE Document__c IN :fdIds]){
            spaIdsToRunCopyBatch.add(sda.School_PFS_Assignment__c);
        }

        // Flag SPAs for processing and queue job if necessary.
        DocumentVerificationService.Instance.queueVerificationUpdates(spaIdsToRunCopyBatch);
    }

}