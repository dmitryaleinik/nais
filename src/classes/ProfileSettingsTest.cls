@isTest
private class ProfileSettingsTest {

    @isTest
    private static void familyProfileName_notConfigured_expectDefaultValue() {
        // Don't set any values on the Profile_Settings__c.
        // This should cause the class to use the default value configured for Profile_Settings__c.Family_Profile_Name__c field.

        // Start by getting the default values.
        Profile_Settings__c defaultSettings = (Profile_Settings__c)Profile_Settings__c.SObjectType.newSObject(null, true);
        String expectedValue = defaultSettings.Family_Profile_Name__c;

        System.assertEquals(expectedValue, ProfileSettings.FamilyProfileName,
                'Expected the default value for the Family_Profile_Name__c field.');
    }

    @isTest
    private static void familyProfileName_profileNameSetToNullValue_expectLegacyValue() {
        // Set null family profile name to test that we correctly default to the legacy profile name.
        ProfileSettings.setField(Profile_Settings__c.Family_Profile_Name__c, null);

        String expectedValue = ProfileSettings.FAMILY_PROFILE_LEGACY;

        System.assertEquals(expectedValue, ProfileSettings.FamilyProfileName,
                'Expected the name of the legacy family profile.');
    }

    @isTest
    private static void familyProfileName_profileNameConfigured_expectNewValue() {
        // Set the family profile name.
        String expectedValue = 'Brand New Profile Name';
        ProfileSettings.setField(Profile_Settings__c.Family_Profile_Name__c, expectedValue);

        System.assertEquals(expectedValue, ProfileSettings.FamilyProfileName, 'Expected the configured profile name.');
    }

    @isTest
    private static void schoolUserProfileName_notConfigured_expectDefaultValue() {
        // Don't set any values on the Profile_Settings__c.
        // This should cause the class to use the default value configured for Profile_Settings__c.School_User_Profile_Name__c field.

        // Start by getting the default values.
        Profile_Settings__c defaultSettings = (Profile_Settings__c)Profile_Settings__c.SObjectType.newSObject(null, true);
        String expectedValue = defaultSettings.School_User_Profile_Name__c;

        System.assertEquals(expectedValue, ProfileSettings.SchoolUserProfileName,
                'Expected the default value for the School_User_Profile_Name__c field.');
    }

    @isTest
    private static void schoolUserProfileName_profileNameSetToNullValue_expectLegacyValue() {
        // Set null family profile name to test that we correctly default to the legacy profile name.
        ProfileSettings.setField(Profile_Settings__c.School_User_Profile_Name__c, null);

        String expectedValue = ProfileSettings.SCHOOL_USER_PROFILE_LEGACY;

        System.assertEquals(expectedValue, ProfileSettings.SchoolUserProfileName,
                'Expected the name of the legacy school user profile.');
    }

    @isTest
    private static void schoolUserProfileName_profileNameConfigured_expectNewValue() {
        // Set the family profile name.
        String expectedValue = 'Brand New Profile Name';
        ProfileSettings.setField(Profile_Settings__c.School_User_Profile_Name__c, expectedValue);

        System.assertEquals(expectedValue, ProfileSettings.SchoolUserProfileName, 'Expected the configured profile name.');
    }

    @isTest
    private static void schoolAdminProfileName_notConfigured_expectDefaultValue() {
        // Don't set any values on the Profile_Settings__c.
        // This should cause the class to use the default value configured for Profile_Settings__c.School_Admin_Profile_Name__c field.

        // Start by getting the default values.
        Profile_Settings__c defaultSettings = (Profile_Settings__c)Profile_Settings__c.SObjectType.newSObject(null, true);
        String expectedValue = defaultSettings.School_Admin_Profile_Name__c;

        System.assertEquals(expectedValue, ProfileSettings.SchoolAdminProfileName,
                'Expected the default value for the School_Admin_Profile_Name__c field.');
    }

    @isTest
    private static void schoolAdminProfileName_profileNameSetToNullValue_expectLegacyValue() {
        // Set null family profile name to test that we correctly default to the legacy profile name.
        ProfileSettings.setField(Profile_Settings__c.School_Admin_Profile_Name__c, null);

        String expectedValue = ProfileSettings.SCHOOL_ADMIN_PROFILE_LEGACY;

        System.assertEquals(expectedValue, ProfileSettings.SchoolAdminProfileName,
                'Expected the name of the legacy school admin profile.');
    }

    @isTest
    private static void schoolAdminProfileName_profileNameConfigured_expectNewValue() {
        // Set the family profile name.
        String expectedValue = 'Brand New Profile Name';
        ProfileSettings.setField(Profile_Settings__c.School_Admin_Profile_Name__c, expectedValue);

        System.assertEquals(expectedValue, ProfileSettings.SchoolAdminProfileName, 'Expected the configured profile name.');
    }

    @isTest
    private static void schoolBasicAdminProfileName_notConfigured_expectDefaultValue() {
        // Don't set any values on the Profile_Settings__c.
        // This should cause the class to use the default value configured for Profile_Settings__c.School_Basic_Admin_Profile_Name__c field.

        // Start by getting the default values.
        Profile_Settings__c defaultSettings = (Profile_Settings__c)Profile_Settings__c.SObjectType.newSObject(null, true);
        String expectedValue = defaultSettings.School_Basic_Admin_Profile_Name__c;

        System.assertEquals(expectedValue, ProfileSettings.SchoolBasicAdminProfileName,
                'Expected the default value for the School_Basic_Admin_Profile_Name__c field.');
    }

    @isTest
    private static void schoolBasicAdminProfileName_profileNameSetToNullValue_expectLegacyValue() {
        // Set null family profile name to test that we correctly default to the legacy profile name.
        ProfileSettings.setField(Profile_Settings__c.School_Basic_Admin_Profile_Name__c, null);

        String expectedValue = ProfileSettings.SCHOOL_BASIC_ADMIN_PROFILE_LEGACY;

        System.assertEquals(expectedValue, ProfileSettings.SchoolBasicAdminProfileName,
                'Expected the name of the legacy basic school admin profile.');
    }

    @isTest
    private static void schoolBasicAdminProfileName_profileNameConfigured_expectNewValue() {
        // Set the family profile name.
        String expectedValue = 'Brand New Profile Name';
        ProfileSettings.setField(Profile_Settings__c.School_Basic_Admin_Profile_Name__c, expectedValue);

        System.assertEquals(expectedValue, ProfileSettings.SchoolBasicAdminProfileName, 'Expected the configured profile name.');
    }

    @isTest
    private static void schoolPowerUserProfileName_notConfigured_expectDefaultValue() {
        // Don't set any values on the Profile_Settings__c.
        // This should cause the class to use the default value configured for Profile_Settings__c.School_Power_User_Profile_Name__c field.

        // Start by getting the default values.
        Profile_Settings__c defaultSettings = (Profile_Settings__c)Profile_Settings__c.SObjectType.newSObject(null, true);
        String expectedValue = defaultSettings.School_Power_User_Profile_Name__c;

        System.assertEquals(expectedValue, ProfileSettings.SchoolPowerUserProfileName,
                'Expected the default value for the School_Power_User_Profile_Name__c field.');
    }

    @isTest
    private static void schoolPowerUserProfileName_profileNameSetToNullValue_expectLegacyValue() {
        // Set null family profile name to test that we correctly default to the legacy profile name.
        ProfileSettings.setField(Profile_Settings__c.School_Power_User_Profile_Name__c, null);

        String expectedValue = ProfileSettings.SCHOOL_POWER_USER_PROFILE_LEGACY;

        System.assertEquals(expectedValue, ProfileSettings.SchoolPowerUserProfileName,
                'Expected the name of the legacy school power user profile.');
    }

    @isTest
    private static void schoolPowerUserProfileName_profileNameConfigured_expectNewValue() {
        // Set the family profile name.
        String expectedValue = 'Brand New Profile Name';
        ProfileSettings.setField(Profile_Settings__c.School_Power_User_Profile_Name__c, expectedValue);

        System.assertEquals(expectedValue, ProfileSettings.SchoolPowerUserProfileName, 'Expected the configured profile name.');
    }

    @isTest
    private static void partnerProfileName_notConfigured_expectDefaultValue() {
        // Don't set any values on the Profile_Settings__c.
        // This should cause the class to use the default value configured for Profile_Settings__c.Partner_Profile_Name__c field.

        // Start by getting the default values.
        Profile_Settings__c defaultSettings = (Profile_Settings__c)Profile_Settings__c.SObjectType.newSObject(null, true);
        String expectedValue = defaultSettings.Partner_Profile_Name__c;

        System.assertEquals(expectedValue, ProfileSettings.PartnerProfileName,
                'Expected the default value for the Partner_Profile_Name__c field.');
    }

    @isTest
    private static void partnerProfileName_profileNameSetToNullValue_expectLegacyValue() {
        // Set null family profile name to test that we correctly default to the legacy profile name.
        ProfileSettings.setField(Profile_Settings__c.Partner_Profile_Name__c, null);

        String expectedValue = ProfileSettings.PARTNER_PROFILE_LEGACY;

        System.assertEquals(expectedValue, ProfileSettings.PartnerProfileName,
                'Expected the name of the legacy partner user profile.');
    }

    @isTest
    private static void partnerProfileName_profileNameConfigured_expectNewValue() {
        // Set the family profile name.
        String expectedValue = 'Brand New Profile Name';
        ProfileSettings.setField(Profile_Settings__c.Partner_Profile_Name__c, expectedValue);

        System.assertEquals(expectedValue, ProfileSettings.PartnerProfileName, 'Expected the configured profile name.');
    }
}