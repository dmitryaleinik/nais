@isTest
private class RavennaUpdateContactsTest {
    
    private static IntegrationApiModel.ApiState createEmptyApiState() {
        return new IntegrationApiModel.ApiState('Ravenna', new PFS__c(), new Map<String, IntegrationApiModel>());
    }

    private static IntegrationApiModel.ApiState createMockApiState(Applicant__c ravennaApplicant, String ravennaId, String ravennaEthnicity) {
        IntegrationApiModel.ApiState apiState = createEmptyApiState();

        addStudentResponseToApiState(apiState, ravennaId, ravennaEthnicity);

        // Store the applicant we are testing on in the api state to mock what we expect to happen by the time our class is called.
        // When the integration upserts applicants, we expect them to be stored here.
        apiState.familyModel.applicantsToUpsert = new List<Applicant__c> { ravennaApplicant };

        return apiState;
    }
    
    // This method creates a single student response the way we deserialize the results from the ravenna API.
    private static Map<String, Object> createMockStudentResponse(String ravennaId, String ravennaEthnicity) {
        Map<String, Object> studentResponse = new Map<String, Object>();

        studentResponse.put('id', ravennaId);
        studentResponse.put(RavennaIntegrationUtils.RAVENNA_ETHNICITY_FIELD, ravennaEthnicity);

        return studentResponse;
    }
    
    private static void addStudentResponseToApiState(IntegrationApiModel.ApiState apiState, String ravennaId, String ravennaEthnicity) {
        // First create a single student response.
        Map<String, Object> studentResponse = createMockStudentResponse(ravennaId, ravennaEthnicity);

        // Now add that single response to a list. We store multiple responses per student Id (ravenna Id) since we might
        // actually get multiple results when we use the ravenna API.
        List<Object> studentResponses = new List<Object> { studentResponse };

        apiState.familyModel.studentIdToResponseMap.put(ravennaId, studentResponses);
    }
    
    @isTest 
    private static void invoke_contactHasNullEthnicity_responseEthnicityNativeAmerican_expectEthnicityUpdated() {
        
        String originalEthnicity = null;
        String expectedEthnicity = 'American Indian';
        String ravennaId = '123456';
        String ravennaEthnicity = 'Native American';
        
        // Insert the student contact. We make sure the ravenna Id is null just to be safe.
        Contact student = ContactTestData.Instance
                .forEthnicity(originalEthnicity)
                .withRavennaId(null)
                .asStudent()
                .insertContact();

        Applicant__c ravennaApplicant = ApplicantTestData.Instance
                .forContactId(student.Id)
                .withRavennaId(ravennaId)
                .insertApplicant();

        // Create mock API state. Calling it mock because it will only contain what is necessary for our unit tests.
        IntegrationApiModel.ApiState mockApiState = createMockApiState(ravennaApplicant, ravennaId, ravennaEthnicity);

        Test.startTest();
        RavennaUpdateContacts handler = new RavennaUpdateContacts();
        handler.invoke(mockApiState);
        Test.stopTest();
        
        student = [SELECT Id, Ethnicity__c FROM Contact WHERE Id =: student.Id LIMIT 1];
        System.AssertEquals(expectedEthnicity, student.Ethnicity__c);
    }
}