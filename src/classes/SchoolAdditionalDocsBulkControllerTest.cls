@isTest
private class SchoolAdditionalDocsBulkControllerTest {
    
    @isTest
    private static void saveNewDocument_newAdditionalDocumentRequestForApplicantAndSiblings_createNewSDA() {
        
        //1. Create the current academic year.
        Academic_Year__c academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        
        //2. Create a school.
        Account school = AccountTestData.Instance.forName('School1')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccount();
        
        //3. Create one parent with 3 applicants.
        Contact parentA = ContactTestData.Instance.asParent().forFirstName('ParentA').forLastName('ParentA').create();
        Contact student1 = ContactTestData.Instance.asStudent().forFirstName('Student1').forLastName('Student1').create();
        Contact student2 = ContactTestData.Instance.asStudent().forFirstName('Student2').forLastName('Student2').create();
        Contact student3 = ContactTestData.Instance.asStudent().forFirstName('Student3').forLastName('Student3').create();
        insert new List<Contact> { parentA, student1, student2, student3 };
        
        //4. Create one PFS.
        PFS__c pfs1 = PfsTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forParentA(parentA.Id)
            .asPaid()
            .insertPfs();
        
        //5. Create 3 applicants.
        Applicant__c applicant1 = ApplicantTestData.Instance
            .forContactId(student1.Id)
            .forPfsId(pfs1.Id)
            .create();
        Applicant__c applicant2 = ApplicantTestData.Instance
            .forContactId(student2.Id)
            .forPfsId(pfs1.Id)
            .create();
        Applicant__c applicant3 = ApplicantTestData.Instance
            .forContactId(student3.Id)
            .forPfsId(pfs1.Id)
            .create();
        insert new List<Applicant__c>{applicant1, applicant2, applicant3};
        
        //6. Create one student folder for each student.    
        Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student1.Id)
            .create();
        Student_Folder__c studentFolder2 = StudentFolderTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student2.Id) 
            .create();
        Student_Folder__c studentFolder3 = StudentFolderTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student3.Id) 
            .create();
        insert new List<Student_Folder__c>{studentFolder1, studentFolder2, studentFolder3};
        
        //7. Create an SPA for each Student Folder.
        School_PFS_Assignment__c spa1 = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicant1.Id)
            .forStudentFolderId(studentFolder1.Id)
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .createSchoolPfsAssignmentWithoutReset();
        School_PFS_Assignment__c spa2 = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicant2.Id)
            .forStudentFolderId(studentFolder2.Id)
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .createSchoolPfsAssignmentWithoutReset();
        School_PFS_Assignment__c spa3 = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicant3.Id)
            .forStudentFolderId(studentFolder3.Id)
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .createSchoolPfsAssignmentWithoutReset();
        insert new List<School_PFS_Assignment__c>{spa1, spa2, spa3};
        
        
        List<School_Document_Assignment__c> sdas1, sdas2, sdas3;
        Test.startTest();
            
            //8. Verify that none of the 3 applicants has related SDAS.
            sdas1 = new List<School_Document_Assignment__c>([SELECT Id FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: spa1.Id]);
            System.AssertEquals(0, sdas1.size());
        
            sdas2 = new List<School_Document_Assignment__c>([SELECT Id FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: spa2.Id]);
            System.AssertEquals(0, sdas2.size());
        
            sdas3 = new List<School_Document_Assignment__c>([SELECT Id FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: spa3.Id]);
            System.AssertEquals(0, sdas3.size());
            
            //9. Select the folders for which we want to require additional documents.
            List<Student_Folder__c> folders = new List<Student_Folder__c>{studentFolder1};
            
            //10. Create an istancet of the class that we are going to test.
            ApexPages.StandardSetController standardController = new ApexPages.StandardSetController(folders);
            standardController.setSelected(folders);
            
            SchoolAdditionalDocsBulkController controller = new SchoolAdditionalDocsBulkController(standardController);
            controller.loadDocumentYears();
            
            System.AssertEquals(true, controller.getValidDocumentYears().size()>0);
            System.AssertEquals(true, controller.getNonSchoolSpecificDocumentTypes().size()>0);
            System.AssertEquals(controller, controller.getMe());
            System.AssertEquals(true, controller.getAdditionalSchoolDocAssign() != null);
            
            //11. Define the additional document that wer are going to request.
            controller.additionalSchoolDocAssign.Document_Type__c = 'Proof of College Enrollment';
            controller.saveNewDocument();
            System.AssertEquals(null, controller.saveAndNewAdditionalDocument());
            System.AssertEquals(null, controller.saveAndUploadRequiredDocuments());
            System.AssertEquals(null, controller.cancelAdditionalDocument());
            System.AssertEquals(null, controller.saveAndNewAdditionalDocument());
        Test.stopTest();
        
        //12. Verify that the additional documents were required to siblings too.
        sdas1 = new List<School_Document_Assignment__c>([SELECT Id, Document_Type__c FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: spa1.Id]);
        System.AssertEquals(1, sdas1.size());
        System.AssertEquals('Proof of College Enrollment', sdas1[0].Document_Type__c);
    
        sdas2 = new List<School_Document_Assignment__c>([SELECT Id, Document_Type__c FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: spa2.Id]);
        System.AssertEquals(1, sdas2.size());
        System.AssertEquals('Proof of College Enrollment', sdas2[0].Document_Type__c);
    
        sdas3 = new List<School_Document_Assignment__c>([SELECT Id, Document_Type__c FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: spa3.Id]);
        System.AssertEquals(1, sdas3.size());
        System.AssertEquals('Proof of College Enrollment', sdas3[0].Document_Type__c);
    
    }
}