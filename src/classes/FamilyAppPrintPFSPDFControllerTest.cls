/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
/**
 * Class: FamilyAppPrintPFSPDFControllerTest
 *
 * Copyright (C) 2013 NAIS
 *
 * Purpose: Test class for FamilyAppPrintPFSPDFController
 * 
 * Where Referenced:
 *   Family Portal - FamilyAppViewPFSPDF Component
 *
 * Change History:
 *
 * Developer           Date        JIRA Ref     Description
 * ---------------------------------------------------------------------------------------
 * Brian Clift         09/11/13    NAIS-526     Initial development
 *
 * exponent partners
 * 901 Mission Street, Suite 105
 * San Francisco, CA  94103
 * +1 (800) 918-2917
 * 
 */ 
@isTest
private class FamilyAppPrintPFSPDFControllerTest {

    private static final String BASE_EMAIL = 'test@sample{0}user{1}email.com';

    private static String generateRandomEmail() {
        return String.format(BASE_EMAIL, new List<String> { String.valueOf(Math.random()), String.valueOf(Math.random()) });
    }

    @isTest
    private static void TestPFSPDFController() {      
        // create a new PFS
        List<Academic_Year__c> academicYears = TestUtils.createAcademicYears();
        Account testAccount = TestUtils.createAccount('Test Account', RecordTypes.individualAccountTypeId, 5, true);
        Contact theParent = TestUtils.createContact('Parent A', testAccount.Id, RecordTypes.parentContactTypeId, true);
        String parentAUserEmail = generateRandomEmail();
        User parentAUser = TestUtils.createPortalUser('Test User', parentAUserEmail, 'TestP', theParent.Id, GlobalVariables.familyPortalProfileId, true, true);
        PFS__c thePFS = TestUtils.createPFS('Test PFS', academicYears[0].id, theParent.id, true);
        Contact theStudent = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, true);
        
        // create applicant
        Applicant__c theApplicant = TestUtils.createApplicant(theStudent.Id, thePFS.Id, false);
        theApplicant.Birthdate_new__c = System.today();
        insert theApplicant;
        Student_Folder__c theStudentFolder = TestUtils.createStudentFolder('Student Folder 1', academicYears[0].id, theStudent.Id, true);
        Account theSchool = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, 1, false);
        theSchool.SSS_Subscriber_Status__c = 'Current';
        insert theSchool;

        // insert portal user
        Contact portalContact = TestUtils.createContact('PortalUser', theSchool.id, RecordTypes.schoolStaffContactTypeId, true);
        User portalUser = new User();
        System.runAs(new User(Id = UserInfo.getUserId())){
          portalUser = TestUtils.createPortalUser('Test PortalUser', generateRandomEmail(), 'tps', portalContact.Id, GlobalVariables.schoolPortalAdminProfileId, true, true);
        }
        PFS__c queryPFS = ApplicationUtils.queryPFSRecord(thePFS.id, null, null, 'PFSView');
        
        // Create School assignment
        School_PFS_Assignment__c spfsa1 = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(theApplicant.Id)
            .forAcademicYearPicklist(academicYears[0].Name)
            .forSchoolId(theSchool.Id)
            .forStudentFolderId(theStudentFolder.Id).DefaultSchoolPfsAssignment;

        Account school2 = AccountTestData.Instance.asSchool().insertAccount();
        School_PFS_Assignment__c spfsa2 = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(theApplicant.Id)
            .forAcademicYearPicklist(academicYears[0].Name)
            .forSchoolId(school2.Id)
            .forStudentFolderId(theStudentFolder.Id)
            .asWithdrawn().insertSchoolPfsAssignment();
        
        FamilyAppPrintPFSPDFController controller = new FamilyAppPrintPFSPDFController();
        controller.pfs = queryPFS;
        PageReference pageRef = Page.FamilyAppViewPFSPDF;
        pageRef.getParameters().put('id', controller.pfs.Id);
        Test.setCurrentPage(pageRef);
        controller.bulkController = new FamilyAppViewPFSPDFBulkController();
        
        Map<Id, List<School_PFS_Assignment__c>> schoolPFSAssignmentMap = controller.applicationSchoolAssignmentMap;
        System.assertEquals(1, schoolPFSAssignmentMap.size());
        List<School_PFS_Assignment__c> schoolPFSAssignments = schoolPFSAssignmentMap.get(theApplicant.id);
        System.assertEquals(1, schoolPFSAssignments.size());
        System.assertEquals('school1', schoolPFSAssignments[0].School__r.Name);
        
        // create test Annual Settings (no KS School has been identified)
        Date minDate = System.today().addDays(-1);
        Date maxDate = System.today().addDays(1);
        Annual_Setting__c theAnnualSetting =  new Annual_Setting__c( Kipona_Scholarship_Min_Birthdate__c = minDate,
                                                                     Kipona_Scholarship_Max_Birthdate__c = maxDate,
                                                                     Pauahi_Keiki_Min_Birthdate_Cycle_2__c = minDate,
                                                                     Pauahi_Keiki_Max_Birthdate_Cycle_2__c = maxDate,
                                                                     Pauahi_Keiki_Min_Birthdate_Cycle_1__c = minDate,
                                                                     Pauahi_Keiki_Max_Birthdate_Cycle_1__c = maxDate,
                                                                     KS_Preschool_Financial_Min_Birthdate__c = minDate,
                                                                     KS_Preschool_Financial_Max_Birthdate__c = maxDate,
                                                                     KS_K_12_Financial_Min_Birthdate__c = minDate,
                                                                     KS_K_12_Financial_Max_Birthdate__c = maxDate,
                                                                     School__c = theSchool.id,
                                                                     Academic_Year__c = academicYears[0].id,
                                                                     Use_Additional_Questions__c = 'Yes',
                                                                     Custom_Question_1__c = true,
                                                                     Custom_Question_2__c = true,
                                                                     Custom_Question_3__c = true,
                                                                     Custom_Question_4__c = true,
                                                                     Custom_Question_5__c = true );
                                                                     
        insert theAnnualSetting;      
        
        controller = new FamilyAppPrintPFSPDFController();
        controller.pfs = queryPFS;
        pageRef = Page.FamilyAppViewPFSPDF;
        pageRef.getParameters().put('id', controller.pfs.Id);
        Test.setCurrentPage(pageRef);
        controller.bulkController = new FamilyAppViewPFSPDFBulkController();
        
        // code coverage
        System.assertEquals(thePFS.id, controller.pfs.id);
        System.assertEquals(13, controller.sectionIds.size());
        System.assertEquals(academicYears[0].name, controller.academicYear);
        System.assertEquals(academicYears[0].Start_Date__c.addYears(-1).year().format().replaceAll(',',''), controller.priorTaxYear);
        System.assertEquals(academicYears[0].Start_Date__c.year().format().replaceAll(',',''), controller.currentTaxYear); 
        System.assert(controller.appUtils != null);
        System.assertEquals(1, controller.applicationSchoolAssignmentMap.size());

        // SFP-606
        System.runAs(portalUser){
            
            Test.startTest();
            controller = new FamilyAppPrintPFSPDFController();
            controller.pfs = queryPFS;
            pageRef = Page.FamilyAppViewPFSPDF;
            pageRef.getParameters().put('id', controller.pfs.Id);
            Test.setCurrentPage(pageRef);
            controller.bulkController = new FamilyAppViewPFSPDFBulkController();

            Map<String,Boolean> customQuestionsSelected = controller.customQuestionsRequestedBySchools;
            System.assertEquals(true,controller.displayCustomQuestions);
            System.assertEquals(true,customQuestionsSelected.get('Custom_Question_1__c'));
            System.assertEquals(true,customQuestionsSelected.get('Custom_Question_2__c'));
            System.assertEquals(true,customQuestionsSelected.get('Custom_Question_3__c'));
            System.assertEquals(true,customQuestionsSelected.get('Custom_Question_4__c'));
            System.assertEquals(true,customQuestionsSelected.get('Custom_Question_5__c')); 
            System.assertEquals(false,customQuestionsSelected.get('Custom_Question_6__c'));
            Test.stopTest();
        }
    }
    
    @isTest
    private static void TestPFSPDFControllerAsBulk() {     
        // create a new PFS
        List<Academic_Year__c> academicYears = TestUtils.createAcademicYears();
        Account testAccount = TestUtils.createAccount('Test Account', RecordTypes.individualAccountTypeId, 5, true);
        Contact theParent = TestUtils.createContact('Parent A', testAccount.Id, RecordTypes.parentContactTypeId, true);
        User parentAUser = TestUtils.createPortalUser('Test User', generateRandomEmail(), 'TestP', theParent.Id, GlobalVariables.familyPortalProfileId, true, true);
        PFS__c thePFS = TestUtils.createPFS('Test PFS', academicYears[0].id, theParent.id, true);
        Contact theStudent = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, true);
        
        // create applicant
        Applicant__c theApplicant = TestUtils.createApplicant(theStudent.Id, thePFS.Id, true);
        Student_Folder__c theStudentFolder = TestUtils.createStudentFolder('Student Folder 1', academicYears[0].id, theStudent.Id, true);
        Account theSchool = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, null, true);
        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYears[0].id, theApplicant.Id, theSchool.Id, theStudentFolder.Id, true);
        List<Student_Folder__c> studentFolders = new List<Student_Folder__c>();
        studentFolders.add(theStudentFolder);
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(studentFolders);
        ApexPages.currentPage().getParameters().put('ids', theStudentFolder.id);
        ssc.setSelected(studentFolders);
        FamilyAppViewPFSPDFBulkController bulkController = new FamilyAppViewPFSPDFBulkController(ssc);
        System.assertEquals(1, bulkController.pfsList.size());
        
        ApexPages.currentPage().getParameters().put('debug', '1');
        FamilyAppPrintPFSPDFController controller = new FamilyAppPrintPFSPDFController();
        controller.pfs = bulkController.pfsList[0];
        controller.bulkController = bulkController;
        
        Map<Id, List<School_PFS_Assignment__c>> schoolPFSAssignmentMap = controller.applicationSchoolAssignmentMap;
        System.assertEquals(1, schoolPFSAssignmentMap.size());
        List<School_PFS_Assignment__c> schoolPFSAssignments = schoolPFSAssignmentMap.get(theApplicant.id);
        System.assertEquals(1, schoolPFSAssignments.size());
        System.assertEquals('school1', schoolPFSAssignments[0].School__r.Name);
        
        // code coverage
        System.assertEquals(thePFS.id, controller.pfs.id);
        System.assertEquals(academicYears[0].name, controller.academicYear);
        System.assertEquals(academicYears[0].Start_Date__c.addYears(-1).year().format().replaceAll(',',''), controller.priorTaxYear);
        System.assertEquals(academicYears[0].Start_Date__c.year().format().replaceAll(',',''), controller.currentTaxYear);     
    }   
    
    @isTest
    private static void TestPFSPDFBusinessPrint() {      
        // create a new PFS
        List<Academic_Year__c> academicYears = TestUtils.createAcademicYears();
        Account testAccount = TestUtils.createAccount('Test Account', RecordTypes.individualAccountTypeId, 5, true);
        Contact theParent = TestUtils.createContact('Parent A', testAccount.Id, RecordTypes.parentContactTypeId, true);
        User parentAUser = TestUtils.createPortalUser('Test User', generateRandomEmail(), 'TestP', theParent.Id, GlobalVariables.familyPortalProfileId, true, true);
        PFS__c thePFS = TestUtils.createPFS('Test PFS', academicYears[0].id, theParent.id, true);
        Contact theStudent = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, true);
        
        // create applicant
        Applicant__c theApplicant = TestUtils.createApplicant(theStudent.Id, thePFS.Id, false);
        theApplicant.Birthdate_new__c = System.today();
        insert theApplicant;
        Student_Folder__c theStudentFolder = TestUtils.createStudentFolder('Student Folder 1', academicYears[0].id, theStudent.Id, true);
        Account theSchool = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, null, false);
        theSchool.SSS_Subscriber_Status__c = 'Current';
        insert theSchool;
        
        PFS__c queryPFS = ApplicationUtils.queryPFSRecord(thePFS.id, null, null, 'PFSView');
        
        Business_Farm__c bfA1 = TestUtils.createBusinessFarm(thePFS.Id, 'Business A1', 1, 'Business', 'Partnership', false);
        Business_Farm__c bfA2 = TestUtils.createBusinessFarm(thePFS.Id, 'Business A2', 2, 'Farm', 'Sole Proprietorship', false);
        insert new List<Business_Farm__c> { bfA1, bfA2 };
        
         // Create School assignment
        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYears[0].id, theApplicant.Id, theSchool.Id, theStudentFolder.Id, true);
        
        FamilyAppPrintPFSPDFController controller = new FamilyAppPrintPFSPDFController();
        controller.pfs = queryPFS;
        PageReference pageRef = Page.FamilyAppViewPFSPDF;
        pageRef.getParameters().put('id',queryPFS.Id);
        Test.setCurrentPage(pageRef);
        controller.bulkController = new FamilyAppViewPFSPDFBulkController();
        System.assertEquals(thePFS.id, controller.pfs.id);
        System.assertEquals(2, controller.businessFarmMap.get(queryPFS.Id).size());
        System.assertEquals(6, controller.getBizOrFarmGroupWrappersMap().get(queryPFS.Id).size());
    }
}