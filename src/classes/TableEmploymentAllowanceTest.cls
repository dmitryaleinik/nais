@IsTest
private class TableEmploymentAllowanceTest
{

    @isTest
    private static void testTableEmploymentAllowance()
    {
        // create the academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        // set the constants
        TestUtils.createSSSConstants(academicYear.Id, true);

        // create the table
        TableTestData.populateEmploymentAllowanceData(academicYear.Id);

        // test low
        System.assertEquals(500, TableEmploymentAllowance.calculateAllowance(academicYear.Id, 1000));

        // test high
        System.assertEquals(5000, TableEmploymentAllowance.calculateAllowance(academicYear.Id, 10000));

        // test high max
        System.assertEquals(5430, TableEmploymentAllowance.calculateAllowance(academicYear.Id, 1000000));
    }
}