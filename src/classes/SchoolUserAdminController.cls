// NAIS-82 School Portal - User Administration
// BR, Exponent Partners, 2013

public with sharing class SchoolUserAdminController {

    /* Initialization */ 
     
    public SchoolUserAdminController() {
        
        //mySchoolId = GlobalVariables.getCurrentUser().Contact.AccountId;
        mySchoolId = GlobalVariables.getCurrentSchoolId();
        loadSchoolContact();
        this.isNewFutureUser = (System.currentPageReference().getParameters().get('future')!=null
                                ?String.ValueOf(System.currentPageReference().getParameters().get('future'))
                                :null);
    }
     
    public List<Contact> loadSchoolContact() {  
        
        // and Has Access to SSS School Portal?
        schoolContacts = [select Id, Name, Email, Role__c, SSS_Main_Contact__c, 
                            Inactive__c, Username_Exists_In_Other_Org__c 
                            from Contact 
                            where AccountId = :mySchoolId 
                            and RecordTypeId=:RecordTypes.schoolStaffContactTypeId 
                            and Deleted__c = false 
                            order by Name];
        Set<Id> profileIds = GlobalVariables.schoolPortalProfileIds;
        
        List<User> userIdList = [select ContactId, IsActive, Contact.Inactive__c 
                                    from User
                                    where ContactId in :schoolContacts 
                                    and ProfileId in :profileIds];
                                    
        
        //NAIS-2015 End
        Set<Id> userIdSet = new Set<Id>();
        Set<Id> contactWithActiveUser = new Set<Id>();
        for (User u : userIdList) {
            contactWithActiveUser.add(u.ContactId);
            
            if(!(u.IsActive == false && u.Contact.InActive__c == true)){
                userIdSet.add(u.ContactId);
            }
        }
        
        Map<Id, Boolean> contactIdToUserActive = new Map<Id, Boolean>();
        For(User u: userIdList){
            if(userIdSet.contains(u.Contact.Id)){
                contactIdToUserActive.put(u.Contact.Id, u.IsActive);
            }
        }
        
        String errorMessages = '';
        contactWitDuplicatedUsernameAsyncError = new Set<Id>();
        contactList = new List<UAWrapper>();
        Integer totalUsers = 0;
        for (Contact c : schoolContacts) {
            UAWrapper uaw = new UAWrapper(c, contactIdToUserActive.get(c.Id));
            contactList.add(uaw);
            
            if(contactIdToUserActive.get(c.Id) != null && contactIdToUserActive.get(c.Id)){
                totalUsers++;
            }
            
            if (c.Username_Exists_In_Other_Org__c) {
                
                contactWitDuplicatedUsernameAsyncError.add(c.Id);
                errorMessages += '   <a class="error-link item" href="#" onclick="location=\'/schoolportal/schooluseradminprofile?id='+c.Id+'&return=1\';">'+c.Email+'</a><br/>';
            }
        }
        
        if (errorMessages != '') {
            
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR, 
                Label.School_Portal_Duplicated_Username.replace('{0}',errorMessages).replace('\n','<br/>')));
        }
        
        //SFP-204, [G.S]
        usersLeft = 0;
        Integer defaultMax = SchoolPortalSettings.Max_Number_of_Users;
        
        if(schoolContacts != null && schoolContacts.size() > 0){
            Account schoolAccnt = [select Id, Max_Number_of_Users__c from Account where Id = :mySchoolId];
            if(schoolAccnt.Max_Number_of_Users__c != null && schoolAccnt.Max_Number_of_Users__c > 0){
                usersLeft = (Integer.valueof(schoolAccnt.Max_Number_of_Users__c)-totalUsers);
            }
            else{
                usersLeft = defaultMax - totalUsers;
            }
            usersLeft = (usersLeft < 0)?0:usersLeft;
        }
        return schoolContacts;
    }
    
    /* Properties */
    
    public Id mySchoolId { get; private set; }
    public Id selectedContactId { get; set; }
    public String isNewFutureUser { get; set; }
    public List<Contact> schoolContacts { private get; private set; }
    public List<UAWrapper> contactList { get; private set; }
    public Integer usersLeft {get;set;}
    private Set<Id> contactWitDuplicatedUsernameAsyncError{get;set;}
    
    public class UAWrapper {
        
        // Wraps an sObject adding a checkbox.
        public Contact c { get; set; }
        public Boolean portalAccess { get; set; }
        
        public UAWrapper(Contact con, Boolean pa) {
            c = con;
            portalAccess = pa;
        }
    }
    
    /* Action Methods */
    public PageReference refreshPage(){
        PageReference thisPage = Page.SchoolUserAdmin;
        thisPage.setRedirect(true);
        return thisPage;
    }
    public PageReference namelink()  {
    
        PageReference pageRef = Page.SchoolUserAdminProfile;
        pageRef.getParameters().put('id', selectedContactId);
        pageRef.getParameters().put('return', '1');
        return pageRef;
    }
    
    public PageReference newcontact()  {
        return Page.SchoolUserAdminProfile;
    }
    
    public PageReference deleteUserAction(){
        
        if (selectedContactId != null){
            Savepoint sp = Database.setSavepoint();
            try{
                
                list<User> u = [select Id from User where ContactId=:selectedContactId and IsActive=true limit 1];
                
                Contact thisContact = [select Id, AccountId from Contact where Id=:selectedContactId limit 1];
                thisContact.Receive_SSS_Correspondence__c = false;
                thisContact.Deleted__c = true;
                update thisContact;
                
                if(u!=null && u.size()>0){
                    if(u[0].Id == UserInfo.getUserId()){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You may not delete your own user.'));
                        Database.rollback(sp);
                        return null;
                    }else{
                        SchoolUserAdminProfileController.activateUser(u[0].Id, false);
                    }
                } else {
                    //We need to tell the batch that this is a contact that needs its individual account. Since, its portal user was already disabled.
                    individualAccountForScoolStaffContact handler  = new individualAccountForScoolStaffContact();
                    handler.createIndividualAccountForContacts(new Set<Id>{thisContact.Id});
                    handler.insertAccounts();
                    handler.upsertContactWithOwnIndividualAccount(false);
                }
            }catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception: '+e+e.getLineNumber()));
                Database.rollback(sp);
                return null;
            }
        }
        PageReference pageRef = Page.SchoolUserAdmin;
        pageRef.setRedirect(true);
        return pageRef;
    }//End:deleteUserAction
    
    public PageReference removePortalAccess() {
        if (selectedContactId != null) {
            User u = [select Id from User where ContactId = :selectedContactId];
            if (u.Id != UserInfo.getUserId()) {
                u.IsActive = false;
                update u;
            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You may not remove your own access to the portal.'));
                return null;
            }
        }
        PageReference pageRef = Page.SchoolUserAdmin;
        pageRef.setRedirect(true);
        return pageRef;
    }    
    
    public class individualAccountForScoolStaffContact {
        
        private Map<Id, Account> accountsByContact { get; set; }
        private Map<Id, Contact> contactsById { get; set; }
        
        public individualAccountForScoolStaffContact() {
            
            accountsByContact = new Map<Id, Account>();
            contactsById = new Map<Id, Contact>();
        }
        
        
        public Map<Id, Contact> createIndividualAccountForContacts(Set<Id> contactIds) {
            
            Map<Id, Contact> result = new Map<Id, Contact>();
            Id ownerId = GlobalVariables.getIndyOwner();
            
            for (Contact c : [SELECT Id, AccountId, FirstName, LastName, RecordTypeId, Deleted__c 
                FROM Contact 
                WHERE Id IN: contactIds 
                AND  RecordTypeId =: RecordTypes.schoolStaffContactTypeId]) {
                
                //Verify that the user was deleted from SP, to avoid unlink school contacts that were "Disabled" instead of "Deleted"
                //This logic is only for deleted school portal users.
                if (c.Deleted__c == true) {
                    
                    accountsByContact.put(c.Id, ContactAction.createIndividualAccountForContact(c, ownerId));
                    contactsById.put(c.Id, c);
                }
            }
            
            return result;        
        }
        
        public void upsertContactWithOwnIndividualAccount() {
            
            upsertContactWithOwnIndividualAccount(true);
        }
        
        public void upsertContactWithOwnIndividualAccount(Boolean isAsync) {
            
            List<Contact> contactsToUpdate = new List<Contact>();
            Map<Id, Id> accountsBycontactId = new Map<Id, Id>();
            Set<Id> accountIds = new Set<Id>();
            Contact thisContact;
            Account thisAccount;
            
            for (Id cid : contactsById.keySet()) {
                
                //Once the new accounts where created. We can assign its ID to the respective contact.
                thisContact = contactsById.get(cid);
                thisAccount = accountsByContact.get(cid);
                
                if (thisAccount.Id != null && thisContact.RecordTypeId == RecordTypes.schoolStaffContactTypeId) {
                    
                    thisContact.AccountId = thisAccount.Id;
                    contactsToUpdate.add(thisContact);
                    
                    //Create set of ids to be used on future method. Since, we can not send sobjects.
                    if (!isAsync) {
                        accountsBycontactId.put(thisContact.Id, thisAccount.Id);
                    }
                }
            }
            
            if (isAsync) {
                update contactsToUpdate;
            } else {
                SchoolUserAdminProfileController.linkContactWithAccount(accountsBycontactId);
            }
        }
        
        public void insertAccounts() {
            
            if(!accountsByContact.isEmpty()) {
                
                insert accountsByContact.Values();
            }
        }
    }//End-Class:individualAccountForScoolStaffContact
}