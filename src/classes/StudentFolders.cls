/**
 * @description Domain class for Student_Folder__c records. This class is responsible for logic that should be run from
 *              Student Folder triggers. Currently, this class publishes Application Update platform events to notify
 *              subscribers when the Folder Status field changes.
 */
public class StudentFolders extends fflib_SObjectDomain {

    @testVisible private static final String STUDENT_FOLDERS_PARAM = 'studentFolders';
    private static final String NEW_LINE = '\n';
    private static final String EVENT_PUBLISH_ERROR_FORMAT = 'ApplicationUpdate__e Failed To Publish. Error Status Code: {0} Error Message: {1}';
    private static final String FIN_AID_API_EVENT_SOURCE = 'Financial Aid API - Application Update Event';

    private StudentFolders(List<Student_Folder__c> studentFolders) {
        super(studentFolders);
    }

    /**
     * @description Handles any operations that should be done after student folder records are updated. Currently, this
     *              involves publishing Application_Update__e platform events after Student Folder Status changes.
     */
    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        // Check the student folders for changes that Application_Update__e subscribers should be notified about.
        publishApplicationUpdateEvents((Map<Id, Student_Folder__c>)existingRecords);
    }

    private List<Application_Update__e> publishApplicationUpdateEvents(Map<Id, Student_Folder__c> existingFoldersById) {
        // Check the toggle on the Financial Aid API Settings. We will only publish platform events if the
        // Application_Update_Event_Trigger_Enabled__c toggle is true.
        if (applicationUpdateEventsDisabled()) {
            return new List<Application_Update__e>();
        }

        List<Application_Update__e> eventsToPublish = new List<Application_Update__e>();

        // Create a set of Ids for the folders that need to be published. This set will help us provide insight when platform events fail to publish.
        // The set is for strings so we can easily include them in the IntegrationLogs if we need to record those errors
        // and try to pinpoint which folders updates were not published.
        Set<String> folderIdsToPublish = new Set<String>();

        for (Student_Folder__c updatedFolder: (List<Student_Folder__c>)this.Records) {
            Student_Folder__c oldFolder = existingFoldersById.get(updatedFolder.Id);

            // Make sure the folder is for a school that has the Financial Aid API Enabled. Then see if we need to publish these changes.
            if (shouldPublishFolderChanges(updatedFolder, oldFolder)) {
                folderIdsToPublish.add(updatedFolder.Id);
                Application_Update__e appUpdate = createApplicationUpdateEvent(updatedFolder);
                eventsToPublish.add(appUpdate);
            }
        }

        if (!eventsToPublish.isEmpty()) {
            List<Database.SaveResult> results = EventBus.publish(eventsToPublish);

            logFailedEvents(results, folderIdsToPublish);
        }

        return eventsToPublish;
    }

    private Boolean applicationUpdateEventsDisabled() {
        return FinancialAidAPISettings.isEnabled('Application_Update_Event_Trigger_Enabled__c') == false;
    }

    /**
     * @description Compares the updated folder to the original version of the folder to look for any changes that
     *              Application_Update__e subscribers should be notified of. Regardless of the updates made to the
     *              student folder record, subscribers will only be notified if it is for a school that is actively
     *              using the Financial Aid API. This is determined by the Student_Folder__c.Financial_Aid_API_Enabled__c
     *              formula field.
     *
     *              Application_Update__e subscribers are notified when the following fields change:
     *                  - Folder_Status__c
     */
    private Boolean shouldPublishFolderChanges(Student_Folder__c newFolder, Student_Folder__c existingFolder) {
        // If the folder is not for a school actively using the API, don't bother looking for field changes.
        if (newFolder.Financial_Aid_API_Enabled__c == false) {
            return false;
        }

        // Check to see if the folder status has changed.
        return newFolder.Folder_Status__c != existingFolder.Folder_Status__c;
    }

    private Application_Update__e createApplicationUpdateEvent(Student_Folder__c folder) {
        // Perhaps we should make this part of the Application inner class of the REST API or make that a top level class with the version appended so the transition from folder to API result/platform event is handled in the same place.
        Application_Update__e appUpdate = new Application_Update__e();
        appUpdate.Application_Id__c = folder.Id;
        appUpdate.School_Student_Id__c = folder.School_Student_Id__c;
        appUpdate.Admission_Enrollment_Status__c = folder.Admission_Enrollment_Status__c;
        appUpdate.Status__c = folder.Folder_Status__c;
        // SFP-1576: We added this new field to store the Student_Folder__c.EFC__c value since EFC__c is really the EFC for Day school.
        // This was confusing because we originally put the Student_Folder__c.EFC__c value in the Estimated_Family_Contribution__c
        // field on the platform event which seems to indicate that it is the true EFC for this family.
        // We now populate Estimated_Family_Contribution__c with Student_Folder__c.Total_EFC__c which is a formula field
        // that returns the correct EFC calculation based on whether the family selected Day or Boarding school while applying.
        // Application_Update__e.Total_Estimated_Family_Contribution__c is no longer used.
        appUpdate.Estimated_Family_Contribution__c = folder.Total_EFC__c;
        appUpdate.Total_Contributions__c = folder.Total_Contributions__c;
        appUpdate.Grant_Awarded__c = folder.Grant_Awarded__c;
        appUpdate.Loan__c = folder.Loan__c;
        appUpdate.Aid_of_Tuition__c = folder.Aid_of_Tuition__c;
        appUpdate.Aid_of_Financial_Need__c = folder.Aid_of_Financial_Need__c;
        appUpdate.Aid_of_Total_Expenses__c = folder.Aid_of_Total_Expenses__c;
        appUpdate.Total_Aid__c = folder.Total_Aid__c;
        appUpdate.School_NCES_Id__c = folder.School_NCES_Id__c;
        appUpdate.Student_Birthdate__c = folder.Birthdate__c;
        appUpdate.Student_First_Name__c = folder.First_Name__c;
        appUpdate.Student_Last_Name__c = folder.Last_Name__c;
        appUpdate.Submitted_Date__c = folder.First_PFS_Submission_Date__c == null ? null : folder.First_PFS_Submission_Date__c.date();
        appUpdate.Number_of_Households__c = getNumberOfHouseholds(folder);
        appUpdate.Student_Tuition__c = folder.Student_Tuition__c;
        appUpdate.Parent_Offer_To_Pay__c = folder.Parent_Offer_to_Pay_combined__c;
        appUpdate.Proposed_Award__c = folder.Proposed_Award__c;
        return appUpdate;
    }

    private Integer getNumberOfHouseholds(Student_Folder__c folderRecord) {
        // Only set the number of households if none of the numbers are null.
        if (folderRecord.Count_of_PFSs__c != null && folderRecord.Count_of_Withdrawn_PFSs__c != null) {
            return Integer.valueOf(folderRecord.Count_of_PFSs__c) - Integer.valueOf(folderRecord.Count_of_Withdrawn_PFSs__c);
        }

        return null;
    }

    private void logFailedEvents(List<Database.SaveResult> eventPublishResults, Set<String> publishedFolderIds) {
        Integer totalNumberOfResults = eventPublishResults.size();
        Integer numberOfFailures = 0;

        // We will log all the failures with one integration log object since we can't determine which student folders did not have their updates published.
        // we will log the total number of attempts and provide the Ids for all the folders we tried to publish in the
        // IntegrationLog__c.Body__c. The Errors__c field will contain all the errors on separate lines.
        List<String> errors = new List<String>();
        for (Database.SaveResult result : eventPublishResults) {
            if (!result.isSuccess()) {
                errors.addAll(getErrorsFromEventPublishResult(result));
                numberOfFailures++;
            }
        }

        String errorSummary = 'Failed To Publish All Application Update Events. Total Attempts: ' + totalNumberOfResults + ' Number Of Failures: ' + numberOfFailures + ' All Student Folder Ids:';

        List<String> folderIdListBody = new List<String> { errorSummary };
        folderIdListBody.addAll(publishedFolderIds);

        String integrationLogErrors = String.join(errors, NEW_LINE);
        String integrationLogBody = String.join(folderIdListBody, NEW_LINE);

        IntegrationLog__c newLog = new IntegrationLog__c();
        newLog.Errors__c = integrationLogErrors;
        newLog.Body__c = integrationLogBody;
        newLog.Source__c = FIN_AID_API_EVENT_SOURCE;
        newLog.Status__c = IntegrationLogs.Status.FATAL.name();

        try {
            // Wrap DML in try-catch so logging doesn't ruin other important DML.
            if (!errors.isEmpty()) {
                insert newLog;
            }
        } catch (Exception e) { }
    }

    private List<String> getErrorsFromEventPublishResult(Database.SaveResult eventPublishResult) {
        List<Database.Error> databaseErrors = eventPublishResult.getErrors();

        List<String> errors = new List<String>();

        for (Database.Error databaseError : databaseErrors) {
            String error = String.format(EVENT_PUBLISH_ERROR_FORMAT, new List<String> { String.valueOf(databaseError.getStatusCode()), databaseError.getMessage() });
            errors.add(error);
        }

        return errors;
    }

    public static StudentFolders newInstance(List<Student_Folder__c> studentFolders) {
        ArgumentNullException.throwIfNull(studentFolders, STUDENT_FOLDERS_PARAM);
        return new StudentFolders(studentFolders);
    }
}