/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SchoolApplicantsApplyingControllerTest
{

    @isTest
    private static void searchSchoolApplicants()
    {
        Academic_Year__c currentAcademicYear = GlobalVariables.getCurrentAcademicYear();
        Account school1 = AccountTestData.Instance.asSchool().forName('SchoolTest1').forSSSSchoolCode('1234').create();
        Account school2 = AccountTestData.Instance.asSchool().forName('SchoolTest2').forSSSSchoolCode('2345').create();
        insert new List<Account>{school1, school2};

        Annual_Setting__c annualSetting1 = AnnualSettingsTestData.Instance.forSchoolId(school1.Id).forAcademicYearId(currentAcademicYear.Id).create();
        Annual_Setting__c annualSetting2 = AnnualSettingsTestData.Instance.forSchoolId(school2.Id).forAcademicYearId(currentAcademicYear.Id).create();
        insert new List<Annual_Setting__c>{annualSetting1, annualSetting2};

        Contact student1 = ContactTestData.Instance.asStudent().forLastName('Student 1').create();
        Contact student2 = ContactTestData.Instance.asStudent().forLastName('Student 2').create();
        Contact parentA = ContactTestData.Instance.asParent().forLastName('Parent A').create();
        Contact parentA1 = ContactTestData.Instance.asParent().forLastName('Parent A1').create();
        insert new List<Contact>{student1, student2, parentA, parentA1};
        
        PFS__c pfs1 = PfsTestData.Instance
            .forName('PFS 1')
            .forParentA(parentA.Id)
            .forAcademicYearPicklist(currentAcademicYear.Name).create();
        PFS__c pfs2 = PfsTestData.Instance
            .forName('PFS 2')
            .forParentA(parentA1.Id)
            .forAcademicYearPicklist(currentAcademicYear.Name).create();
        insert new List<PFS__c>{pfs1, pfs2};

        Applicant__c applicant1 = ApplicantTestData.Instance.forContactId(student1.Id).forPfsId(pfs1.Id).create();
        Applicant__c applicant2 = ApplicantTestData.Instance.forContactId(student2.Id).forPfsId(pfs2.Id).create();
        insert new List<Applicant__c>{applicant1, applicant2};


        Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
            .forName('Student Folder 1')
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forSchoolId(school1.Id)
            .forStudentId(student1.Id)
            .forNewReturning('New').create();
        Student_Folder__c studentFolder2 = StudentFolderTestData.Instance
            .forName('Student Folder 1')
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forSchoolId(school2.Id)
            .forStudentId(student2.Id)
            .forNewReturning('New').create();
        insert new List<Student_Folder__c>{studentFolder1, studentFolder2};
        
        List<School_PFS_Assignment__c> spfsaList = new List<School_PFS_Assignment__c>();

        School_PFS_Assignment__c spfsa = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1.Id)
                .forSchoolId(school1.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forDayOrBoarding('Day').create();
        School_PFS_Assignment__c spfsa1 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1.Id)
                .forSchoolId(school2.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forDayOrBoarding('Boarding').create();
        School_PFS_Assignment__c spfsa2 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant2.Id)
                .forSchoolId(school1.Id)
                .forStudentFolderId(studentFolder2.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forDayOrBoarding('Day').create();
        School_PFS_Assignment__c spfsa3 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant2.Id)
                .forSchoolId(school2.Id)
                .forStudentFolderId(studentFolder2.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forDayOrBoarding('Boarding').create();
        spfsaList.addAll(new List<School_PFS_Assignment__c>{spfsa1, spfsa2, spfsa3, spfsa});
        insert spfsaList;
        
        PageReference pageRef = Page.SchoolApplicantsApplying;
        Test.setCurrentPage(pageRef);
        
        SchoolApplicantsApplyingController saac = new SchoolApplicantsApplyingController();
        saac.searchSchools();  

        for(ApexPages.Message msg :  ApexPages.getMessages()) {
            System.assert(msg.getSummary() == 'Please enter SSS Code' ||  msg.getSummary() == 'Please select Academic Year');
        }   
        
        saac.sssCode = '1234';
        saac.selectedSPfsa.Academic_Year_Picklist__c = GlobalVariables.getPreviousYearString();
        saac.searchSchools();
        System.assert(ApexPages.getMessages().size() == 3);
        
        saac.sssCode = '1234';
        saac.selectedSPfsa.Academic_Year_Picklist__c = GlobalVariables.getCurrentYearString();
        saac.searchSchools();
        System.assertEquals(2, saac.spfsaList.size(), 'Expected 2 School PFS Assignments that were created to be shown.');

        List<String> dayOrBoarding = new List<String>();
        List<String> newOrReturning = new List<String>();
        for (School_PFS_Assignment__c s : saac.spfsaList)
        {
            dayOrBoarding.add(s.Day_Boarding__c);
            newOrReturning.add(s.New_Returning__c);
        }
        System.assert(dayOrBoarding.equals(new List<String>{'Boarding','Boarding'}));
        System.assert(newOrReturning.equals(new List<String>{'New','New'}));

        System.assertEquals(saac, saac.getThisController());
        System.assertEquals(GlobalVariables.getCurrentAcademicYear().Id, saac.getAcademicYearId());
        System.assertNotEquals(null,saac.SchoolAcademicYearSelector_OnChange(true));
    }

    @isTest
    private static void setAcademicYearId_anyId_savesToProperty()
    {
        String testId = 'yearId';
        SchoolApplicantsApplyingController controller = new SchoolApplicantsApplyingController();
        controller.setAcademicYearId(testId);
        System.assertEquals(testId, controller.getAcademicYearId(), 'Expected academic year id to be the test value that was set.');
    }
}