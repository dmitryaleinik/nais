/**
 * @description This class is used to create Federal Tax Table records for unit tests.
 */
@isTest
public class FederalTaxTableTestData extends SObjectTestData
{

    @testVisible 
    private static final Decimal TAXABLE_INCOME_LOW = 20;
    @testVisible 
    private static final Decimal US_INCOME_TAX_RATE = 1.5;
    @testVisible 
    private static final String FILLING_TYPE = 'Married Joint/Surviving Spouse';

    /**
     * @description Get the default values for the Federal_Tax_Table__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Federal_Tax_Table__c.Academic_Year__c => AcademicYearTestData.Instance.DefaultAcademicYear.Id,
                Federal_Tax_Table__c.Taxable_Income_Low__c => TAXABLE_INCOME_LOW,
                Federal_Tax_Table__c.US_Income_Tax_Rate__c => US_INCOME_TAX_RATE,
                Federal_Tax_Table__c.Filing_Type__c => FILLING_TYPE
        };
    }

    /**
     * @description Set the Academic Year for the Tax Table record.
     * @param academicYearId The Id of the Academic Year record to use.
     * @return The current instance of FederalTaxTableTestData.
     */
    public FederalTaxTableTestData forAcademicYearId(Id academicYearId) {
        return (FederalTaxTableTestData) with(Federal_Tax_Table__c.Academic_Year__c, academicYearId);
    }

    /**
     * @description Insert the current working Federal_Tax_Table__c record.
     * @return The currently operated upon Federal_Tax_Table__c record.
     */
    public Federal_Tax_Table__c insertFederalTaxTable() {
        return (Federal_Tax_Table__c)insertRecord();
    }

    /**
     * @description Create the current working Federal Tax Table record without resetting
     *             the stored values in this instance of FederalTaxTableTestData.
     * @return A non-inserted Federal_Tax_Table__c record using the currently stored field
     *             values.
     */
    public Federal_Tax_Table__c createFederalTaxTableWithoutReset() {
        return (Federal_Tax_Table__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Federal_Tax_Table__c record.
     * @return The currently operated upon Federal_Tax_Table__c record.
     */
    public Federal_Tax_Table__c create() {
        return (Federal_Tax_Table__c)super.buildWithReset();
    }

    /**
     * @description The default Federal_Tax_Table__c record.
     * @return The default Federal Tax Table record.
     */
    public Federal_Tax_Table__c DefaultFederalTaxTable {
        get {
            if (DefaultFederalTaxTable == null) {
                DefaultFederalTaxTable = createFederalTaxTableWithoutReset();
                insert DefaultFederalTaxTable;
            }
            return DefaultFederalTaxTable;
        }
        private set;
    }

    /**
     * @description Get the Federal_Tax_Table__c SObjectType.
     * @return The Federal_Tax_Table__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Federal_Tax_Table__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static FederalTaxTableTestData Instance {
        get {
            if (Instance == null) {
                Instance = new FederalTaxTableTestData();
            }
            return Instance;
        }
        private set;
    }

    private FederalTaxTableTestData() { }
}