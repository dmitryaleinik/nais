/**
 *    NAIS-69: Document Preview
 *
 *    SL, Exponent Partners, 2013
 */
@isTest
private class SpringCMDocPreviewer_RESTTest {

    @isTest static void getPdf() {
        
        Databank_Settings__c databankSettings = new Databank_Settings__c();
        databankSettings.Name = 'Databank';
        databankSettings.Use_UAT__c = true;
        databankSettings.Documents_Endpoint_UAT__c = 'https://apiuatna11.springcm.com/v201411/documents/';
        databankSettings.getTokenEndpoint__c = 'https://authuat.springcm.com/oauth/201309/token/sfaccesstoken';
        databankSettings.clientId__c = 'clientid123';
        databankSettings.clientSecret__c = 'secret123';
        databankSettings.Sharelinks_Endpoint_UAT__c  = 'https://apiuatna11.springcm.com/v201411/sharelinks';
        databankSettings.Document_Versions_Endpoint_UAT__c  = 'https://apiuatna11.springcm.com/v201411/documents/{id}/versions';
        databankSettings.PDF_Publish_Minutes__c = 30;
        insert databankSettings;
        
        Test.setMock(HttpCalloutMock.class, new HttpCalloutMockImplementation());
        
        //Create academic year.
        Academic_Year__c academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        
        //Create test family document record
        String DOCUMENT_ID = '12345';
        Family_Document__c documen1040 = FamilyDocumentTestData.Instance
            .forDocumentType('1040')
            .forAcademicYearPicklist(academicYear.Name)
            .forDocYear(academicYear.Name.left(4))
            .withImportId(DOCUMENT_ID)
            .insertFamilyDocument();
        
        Test.startTest();
            //Create the test the controller
            PageReference documentPreviewPagePDF = Page.DocumentPreviewPagePDF;
            documentPreviewPagePDF.getParameters().put('id', DOCUMENT_ID);
            Test.setCurrentPage(documentPreviewPagePDF);
            SpringCMDocPreviewer_REST controller = new SpringCMDocPreviewer_REST();
            controller.getPdf();
            
            System.assertEquals(HttpCalloutMockImplementation.ACCESS_TOKEN_VALUE, controller.Token,
                'Expected the token to match the response from the HttpCalloutMockImplementation.');
            
            System.assertEquals(false, controller.isValidPage);
        Test.stopTest();
    }
}