@isTest
private class SpringCMAjaxUploadControllerTest {
    private static Family_Document__c familyDocument;

    private static PFS__c setupPfs(String academicYear) {
        academicYear = academicYear == null ? '2016-2017' : academicYear;

        PFS__c newPfs = new PFS__c(Academic_Year_Picklist__c = academicYear);
        insert newPfs;
        return newPfs;
    }

    private static void setupTestData() {
        Databank_Settings__c databankSettings = new Databank_Settings__c();

        databankSettings.Name = 'Databank';
        databankSettings.sendDocEndpoint__c = 'https://apiuploaduatna11.springcm.com/v201411/folders/170ead2f-eb63-e511-9fcc-6c3be5a75f4d/documents?name=';
        databankSettings.getTokenEndpoint__c = 'https://authuat.springcm.com/oauth/201309/token/sfaccesstoken';
        databankSettings.clientId__c = '4bef1522-6190-42e1-89dc-dde0942010e9';
        databankSettings.clientSecret__c = 'efafddd29c85485086edfc89f7594c12PuaLqr5BnePXubOqtDapWmoxDSBiOVoZyTmk6FMHNTPI7eNPSQHTY9mnFmHYpWwSz0jbznto0TcDWg0jEwQ8NVByq9eT7xb0';
        databankSettings.moveDocumentURL__c = 'https://apiuatna11.springcm.com/v201411/folders/ee34a000-2b05-e311-9cc6-6c3be5a75f4c';
        databankSettings.workflowURL__c = 'https://apiuatna11.springcm.com/v201411/workflows';
        databankSettings.checkExistingURL__c = 'https://apiuatna11.springcm.com/v201411/documentsearchtasks';
        databankSettings.setEOSURL__c = 'https://apiuatna11.springcm.com/v201411/folders';

        insert databankSettings;

        // create test family document
        familyDocument = FamilyDocumentTestData.Instance.createFamilyDocumentWithoutReset();
        familyDocument.Document_Status__c = 'Not Received';
        insert familyDocument;

        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        PFS__c pfs = setupPfs(academicYear.Name);

        TestUtils.createHelp(GlobalVariables.getCurrentAcademicYear().Id, true);

        PageReference testPR = Page.SpringCMAjaxUploadPage;
        testPR.getParameters().put('fdocId', familyDocument.Id);
        testPR.getParameters().put('pfsnum', pfs.PFS_Number__c);
        Test.setCurrentPage(testPR);

        Test.setMock(HttpCalloutMock.class, new HttpCalloutMockImplementation());
    }

    @isTest
    private static void authorizeAndSetTheToken_expectToken() {
        setupTestData();
        SpringCMAjaxUploadController svc = new SpringCMAjaxUploadController();

        Test.startTest();
        svc.authorizeAndSetTheToken();
        Test.stopTest();

        System.assertEquals(HttpCalloutMockImplementation.ACCESS_TOKEN_VALUE, svc.Token,
                'Expected the token to match the response from the HttpCalloutMockImplementation.');
    }

    @isTest
    private static void sendDocument_expectDocumentUrl() {
        setupTestData();

        Test.startTest();
        SpringCMAjaxUploadController svc = new SpringCMAjaxUploadController();
        svc.Document = Blob.valueof('This is a silly little blob.');
        svc.DocumentName = 'Generic Doc Name.pdf';
        svc.SendDocument();
        Test.stopTest();

        System.assertEquals(HttpCalloutMockImplementation.HREF_VALUE, svc.DocumentUrl,
                'Expected the DocumentUrl to match the response from the HttpCalloutMockImplementation.');
    }

    @isTest
    private static void TestDocumentUpload() {
        setupTestData();
        SpringCMAjaxUploadController svc = new SpringCMAjaxUploadController();
        string test = '';

        svc.DocumentURL = 'test url';
        System.assertEquals('test url' , svc.DocumentURL);
        svc.Token = '123';
        System.assertEquals('123' , svc.Token);
        svc.DocumentName = '123';
        System.assertEquals('123' , svc.DocumentName);
    }

    @isTest
    private static void testProcessSuccess() {
        setupTestData();
        SpringCMAjaxUploadController svc = new SpringCMAjaxUploadController();
        svc.fDoc = familyDocument;
        svc.processSubmitSuccess();
        Family_Document__c updatedFDoc = [Select Id, Document_Status__c FROM Family_Document__c WHERE Id = :familyDocument.Id];
        System.assertEquals('Upload Pending', updatedFDoc.Document_Status__c);
    }

    @isTest
    private static void testProcessCancel() {
        setupTestData();
        SpringCMAjaxUploadController svc = new SpringCMAjaxUploadController();
        svc.fDoc = familyDocument;
        svc.processCancelDelete();
        System.assertEquals(0, [Select count() FROM Family_Document__c WHERE Id = :familyDocument.Id]);
    }

    @isTest
    private static void testMisc() {
        setupTestData();

        SpringCMAjaxUploadController svc = new SpringCMAjaxUploadController();
        svc.fDoc = familyDocument;

        svc.hhId = '';
        svc.hhName = 'test2';
        String testing = svc.ForceFallBack;
        svc.verifyFlag = 'T';
        svc.getFallBackURL();
        svc.getDatabankFriendlyDocType();
        svc.alreadyFellbackRedirect();
        Boolean fellbackBool = svc.alreadyFellback;
        String s = svc.scmAuthenticationError;
        Blob blo = svc.Document;
    }

    @isTest
    private static void testTestingPage() {
        // avoid creating duplicate Academic Year record for current year
        setupTestData();
        PageReference testPR = Page.SpringCMNewUploadTesting;
        testPR.getParameters().put('fdocid', familyDocument.Id);
        testPR.getParameters().put('pfsnumber', 'testpfsnum');
        Test.setCurrentPage(testPR);

        SpringCMNewUploadTesting controller = new SpringCMNewUploadTesting();
    }
    
    

    @isTest
    private static void hasDuplicatedSpringCMDocument_familyDocument_expectNonDuplicated() {
        // avoid creating duplicate Academic Year record for current year
        setupTestData();
        DocumentUploadSettings.setSetting('Page_Limit__c', 900);
        PageReference testPR = Page.SpringCMNewUploadTesting;
        testPR.getParameters().put('fdocid', familyDocument.Id);
        testPR.getParameters().put('pfsnumber', 'testpfsnum');
        Test.setCurrentPage(testPR);
        SpringCMAjaxUploadController svc = new SpringCMAjaxUploadController();

        Test.startTest();
        SpringCMAjaxUploadController.hasDuplicatedSpringCMDocument('', 'oauth ', null, familyDocument.Id);
        SpringCMAjaxUploadController.pageCounterSpringCMDocument(familyDocument.Id, 'oauth ', familyDocument.Id);
        SpringCMAjaxUploadController.SpringSMDocumentResponseWrapper result1 = SpringCMAjaxUploadController.hasDuplicatedSpringCMDocument('','Om1kRt1qW+vh4XXJpHDqohQgGkscTniL8lxHlhA9oeg=',familyDocument.Household__c, familyDocument.Id);
        System.assertEquals(900, svc.numberPageLimit);
        svc.XSpringCMContentSHA256 = 'Om1kRt1qW+vh4XXJpHDqohQgGkscTniL8lxHlhA9oeg=';
        svc.pageCounter = 10; 
        svc.processSubmitSuccess();
        system.assertEquals(false, result1.isDuplicated);
        Test.stopTest();
    }

    @isTest
    private static void processSubmitSuccess_trackVerificationRequestDataEnabled_expectVerificationRequestDataOnFamilyDoc() {
        setupTestData();

        FeatureToggles.setToggle('Track_Verification_Request_Date__c', true);

        SpringCMAjaxUploadController svc = new SpringCMAjaxUploadController();

        // Set the verify flag to true.
        svc.verifyFlag = 'T';
        svc.fDoc = familyDocument;
        svc.processSubmitSuccess();
        Family_Document__c updatedFDoc = [Select Id, Verification_Requested_Date__c, Document_Status__c FROM Family_Document__c WHERE Id = :familyDocument.Id];
        System.assertEquals('Upload Pending', updatedFDoc.Document_Status__c);
        System.assertNotEquals(null, updatedFDoc.Verification_Requested_Date__c,
                'Expected the verification request date to not be null.');
    }
    
    @isTest
    private static void isValidFileSize_familyDocument_expectMaxSizeExceeded() {
        
        setupTestData();
        DocumentUploadSettings.setSetting('Max_File_Size__c', 50);
        DocumentUploadSettings.setSetting('Restrict_File_Size__c', true);
        SpringCMAjaxUploadController svc = new SpringCMAjaxUploadController();

        Test.startTest();
        
	        SpringCMAjaxUploadController.SpringSMDocumentResponseWrapper response = 
	            SpringCMAjaxUploadController.isValidFileSize('53428800');
	            
            system.assertEquals(false, response.isValidFileSize);
        
        Test.stopTest();
    }
    
    @isTest
    private static void isValidFileSize_familyDocument_expectMaxSizeIsNotExceeded() {
        
        setupTestData();
        DocumentUploadSettings.setSetting('Max_File_Size__c', 50);
        DocumentUploadSettings.setSetting('Restrict_File_Size__c', true);
        SpringCMAjaxUploadController svc = new SpringCMAjaxUploadController();

        Test.startTest();
        
            SpringCMAjaxUploadController.SpringSMDocumentResponseWrapper response = 
                SpringCMAjaxUploadController.isValidFileSize('52428800');
                
            system.assertEquals(true, response.isValidFileSize);
        
        Test.stopTest();
    }
}