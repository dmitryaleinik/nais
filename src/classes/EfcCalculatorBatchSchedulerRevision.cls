public class EfcCalculatorBatchSchedulerRevision implements Schedulable
{

    public void execute(SchedulableContext SC)
    {
        if (EfcUtil.getEfcSettingDisableRevisionAutoEfcCalc() != true)
        {
             EfcCalculatorBatchProcessor.doExecuteBatch(EfcCalculatorBatchProcessor.CalcType.Revision);
        }
    }
}