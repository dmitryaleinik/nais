/**
 * @description This class is responsible for querying user records.
 */
public class UserSelector extends fflib_SObjectSelector {

    @testVisible private static final String ID_SET_PARAM = 'idSet';
    @testVisible private static final String USER_IDS_PARAM  = 'userIds';
    @testVisible private static final String USER_FIELDS_TO_SELECT_PARAM = 'fieldsToSelect';
    private static final String NAMES_PARAM = 'names';
    private static final String SYSTEM_USER_NAME = 'System User';

    /**
     * @description Default constructor for an instance of the UserSelector that will not include field sets while
     *              enforcing FLS and CRUD.
     */
    public UserSelector() { }

    /**
     * @description Queries user records by Id.
     * @param idSet The Ids of the user records to query.
     * @return A list of user records.
     * @throws ArgumentNullException if idSet is null.
     */
    public List<User> selectById(Set<Id> idSet) {
        ArgumentNullException.throwIfNull(idSet, ID_SET_PARAM);

        return (List<User>)super.selectSObjectsById(idSet);
    }

    /**
     * @description Queries user records by ids with custom field list.
     * @param userIds The ids of the user records to query.
     * @param fieldsToSelect The list of fields to query.
     * @return A list of user records.
     * @throws ArgumentNullException if userIds is null.
     * @throws ArgumentNullException if fieldsToSelect is null.
     */
    public List<User> selectByIdWithCustomFieldList(Set<Id> userIds, List<String> fieldsToSelect) {
        ArgumentNullException.throwIfNull(userIds, USER_IDS_PARAM);
        ArgumentNullException.throwIfNull(fieldsToSelect, USER_FIELDS_TO_SELECT_PARAM);

        assertIsAccessible();

        String query = newQueryFactory(false)
                .selectFields(fieldsToSelect)
                .setCondition('Id IN :userIds')
                .toSOQL();

        return Database.query(query);
    }

    private Schema.SObjectType getSObjectType() {
        return User.SObjectType;
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                User.Can_Apply_Courtesy_Waivers__c,
                User.Contact.AccountId,
                User.Contact.Account.Name,
                User.Contact.Name,
                User.ContactId,
                User.Early_Access__c,
                User.Email,
                User.In_Focus_School__c,
                User.Name,
                User.ProfileId,
                User.Persist_School_Focus__c,
                User.Profile.Name,
                User.SYSTEM_Terms_and_Conditions_Accepted__c,
                User.Username
        };
    }

    /**
     * @description Creates a new instance of the UserSelector.
     * @return A UserSelector.
     */
    public static UserSelector newInstance() {
        return new UserSelector();
    }
}