public without sharing class SchoolBudgetGroupAction {

    public static void recalcBudgetBalance(Set<Id> budgetGroupIds, Set<Id> removedBudgetAllocationsIds){
        Set<Id> budgetGroupIdsToQuery = budgetGroupIds == null ? new Set<Id>() : budgetGroupIds;
        Set<Id> budgetAllocationIds = removedBudgetAllocationsIds == null ? new Set<Id>() : removedBudgetAllocationsIds;

        // sum up allocation amounts by budgets
        AggregateResult[] groupedResults = [
                SELECT Budget_Group__c, SUM(Amount_Allocated__c) sumAllocated
                FROM Budget_Allocation__c
                WHERE Budget_Group__c IN :budgetGroupIdsToQuery
                AND Status__c != 'Complete, Award Denied'
                AND Status__c != 'Complete, Award Refused'
                AND Id NOT IN :budgetAllocationIds
                GROUP BY Budget_Group__c];

        Map<Id, Decimal> totalsByBudgetGroup = new Map<Id, Decimal>();

        for (AggregateResult ar : groupedResults) {
            totalsByBudgetGroup.put((Id)ar.get('Budget_Group__c'), (Decimal)ar.get('sumAllocated'));
        }

        List<Budget_Group__c> budgetGroups = [
            SELECT Total_Allocated__c
            FROM Budget_Group__c
            WHERE Id IN :totalsByBudgetGroup.KeySet()];

        for (Budget_Group__c bg : budgetGroups) {
            Decimal d = totalsByBudgetGroup.get(bg.Id) == null ? 0 : totalsByBudgetGroup.get(bg.Id);
            bg.Total_Allocated__c = d;
        }
        if (!budgetGroups.isEmpty()){
            update budgetGroups;
        }


        // handle case where all allocations were deleted
        List<Budget_Group__c> orphanedBGs = [select Total_Allocated__c
                                                from Budget_Group__c
                                                where Id IN :budgetGroupIds
                                                    and Id NOT IN :totalsByBudgetGroup.KeySet()];
        for (Budget_Group__c bg : orphanedBGs) {
            bg.Total_Allocated__c = 0;
        }

        if (!orphanedBGs.isEmpty()){
            update orphanedBGs;
        }

    }

    //NAIS-1854 Start
    public static void deleteBudgetBalance(Set<Id> studentFolders, Set<Id> spfsas){
        set<Id> studentFolderWithdrawn = new Set<Id>();
        Map<Id, School_PFS_Assignment__c> spaIdToSpa = new Map<Id, School_PFS_Assignment__c>();

        List<Student_Folder__c> studentFolds = [Select Id, Grant_Awarded__c, Admission_Enrollment_Status__c, Count_of_PFSs__c
                                                    FROM Student_Folder__c
                                                    WHERE Id IN :studentFolders
                                                ];
        List<School_PFS_Assignment__c> spas = [SELECT Id, Withdrawn__c, Student_Folder__c
                                                    FROM School_PFS_Assignment__c
                                                    WHERE Student_Folder__c IN :studentFolders
                                                ];

        studentFolderWithdrawn = studentFolders;

        for(School_PFS_Assignment__c spa: spas){
            spaIdToSpa.put(spa.Student_Folder__c, spa);
            if(spa.Withdrawn__c != 'Yes'){
                studentFolderWithdrawn.remove(spa.Student_Folder__c);
            }
        }

        If(studentFolderWithdrawn.size() > 0){

            List<Student_Folder__c> updatedStudentFolds = new List<Student_Folder__c>();

            List<Budget_Allocation__c> budgAllos = [SELECT Id, Amount_Allocated__c, Student_Folder__c
                                                        FROM Budget_Allocation__c
                                                        WHERE Student_Folder__c IN :studentFolderWithdrawn
                                                    ];

            List<Budget_Allocation__c> budgAllosToDelete = new List<Budget_Allocation__c>();
            for(School_PFS_Assignment__c spfs: spaIdToSpa.values()){
                for(Budget_Allocation__c ba: budgAllos){
                    if(spfs.Withdrawn__c == 'Yes' && ba.Student_Folder__c == spfs.Student_Folder__c) {
                        budgAllosToDelete.add(ba);
                    }
                }
            }

            for(Student_Folder__c studentFold: studentFolds){
                if(studentFolderWithdrawn.contains(studentFold.Id)){
                    studentFold.Grant_Awarded__c = null;
                    updatedStudentFolds.add(studentFold);
                }
            }

            //update budgAllos;  //For Zeroed out Budget Allocation
            if(!updatedStudentFolds.isEmpty()){
                update updatedStudentFolds;
            }
            if(!budgALlosToDelete.isEmpty()){
                delete budgAllosToDelete;
            }
        }
    }
    //NAIS-1854 END

    //NAIS-2036 [DP] 11.19.2014

    // for added efficiency, these calculations can be prevented if they are taking place in the SchoolFinancialAid page
    public static Boolean preventFolderGrantRecalc = false;

    // 1. On Budget Allocation Insert/Update, set Student Folder.Use Budgets to 'Yes' and rollup amount to Student Folder.Grant_Awarded__c
    // 2. On Student Folder update, with Use Budgets newly set to 'No,' delete any Budget Allocations
    public static void recalcStudentFolderGrantAwarded(Set<Id> folderIds){
        if (!preventFolderGrantRecalc){
            List<Student_Folder__c> foldersToUpdate = new List<Student_Folder__c>();
            List<Budget_Allocation__c> budgetsAlocsToDelete = new List<Budget_Allocation__c>();

            List<Student_Folder__c> allFolders = [Select Id, Grant_Awarded__c, Use_Budget_Groups__c,
                                                    (Select Id, Amount_Allocated__c, Student_Folder__c FROM Budget_Allocations__r)
                                                FROM Student_Folder__c WHERE Id in :folderIds];

            for (Student_Folder__c sf : allFolders){
                if (sf.Use_Budget_Groups__c == 'No'){
                    if (sf.Budget_Allocations__r != null && !sf.Budget_Allocations__r.isEmpty()){
                        budgetsAlocsToDelete.addAll(sf.Budget_Allocations__r);//If this logic changes, It may affect SchoolEditAllocationsController.saveWhenBudgetGroupIsNo
                    }
                } else {
                    Decimal amtFromBudgets = 0;
                    if (sf.Budget_Allocations__r != null && !sf.Budget_Allocations__r.isEmpty()){
                        for (Budget_Allocation__c ba : sf.Budget_Allocations__r){
                            if (ba.Amount_Allocated__c != null){
                                System.debug('TESTINGdrew133 ' + ba.Amount_Allocated__c + '; ' + ba.Id);
                                amtFromBudgets += ba.Amount_Allocated__c;
                            }
                        }
                    }

                    System.debug('TESTINGdrew138 ' + amtFromBudgets);
                    // set to null if it is zero
                    amtFromBudgets = amtFromBudgets == 0 ? null : amtFromBudgets;
                    if (sf.Grant_Awarded__c != amtFromBudgets){
                        sf.Grant_Awarded__c = amtFromBudgets;
                        System.debug('TESTINGdrew144 ' + sf.Grant_Awarded__c);
                        foldersToUpdate.add(sf);
                    }
                }
            }

            if (!foldersToUpdate.isEmpty()){
                update foldersToUpdate;
            }

            if (!budgetsAlocsToDelete.isEmpty()){
                delete budgetsAlocsToDelete;//If this logic changes, It may affect SchoolEditAllocationsController.saveWhenBudgetGroupIsNo
            }
        }
    }
}