/**
 *    [SL] Utility class to calculate the Total Income for a PFS
 * 
 */
public class FamilyTotalIncomeCalculator extends EfcCalculatorBase {
    // only need to calculate Taxable Income, NonTaxable Income, and Total Income
    public override void initializeSteps() {
        addStep(new EfcCalculationSteps.TaxableIncome());
        addStep(new EfcCalculationSteps.NonTaxableIncome());
        addStep(new EfcCalculationSteps.TotalIncome());
    }
    
    // maintain a static reference to the calculator
    public static final FamilyTotalIncomeCalculator familyTotalIncomeCalculator = new FamilyTotalIncomeCalculator();
    
    // returns the Total Income for a PFS
    public static Decimal calculateTotalIncome(Id pfsId) {
        EfcWorksheetData worksheet = EfcDataManager.createEfcWorksheetDataForPFS(pfsId);
        familyTotalIncomeCalculator.calculateEfc(worksheet);
        return worksheet.totalIncome;
    } 
}