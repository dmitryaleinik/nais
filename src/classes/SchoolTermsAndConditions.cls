public without sharing class SchoolTermsAndConditions {

    public User u;
    public Boolean checkbox {get; set;}
    public Boolean hasAcceptedOnce {get; set;}
    public Boolean isKSUser {get; set;}
    public Help_Configuration__c help {get; set;}

    public SchoolTermsAndConditions(){
        try{u = GlobalVariables.getCurrentUser();

            hasAcceptedOnce = u.SYSTEM_Terms_and_Conditions_Accepted__c != null;
            if (hasAcceptedOnce){
                apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The terms and conditions have been updated.'));
            }

            checkbox = false;
            isKSUser = ApplicationUtils.IsKSSchoolUserStatic();

            help = GlobalVariables.getCurrentTermsAndConditionsHelpRecord();}catch(Exception e){system.debug('>>>>>>>>>'+e.getMessage());}
    }

    public PageReference accept(){
        if (!checkbox){
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please check the box to continue.'));
            return null;
        }

        u.SYSTEM_Terms_and_Conditions_Accepted__c = System.Now();
        update u;

        // page parameter is set in each of the two VF pages
        Boolean inSchoolPortal = System.currentPagereference().getParameters().get('inSchoolPortal') == 'true';

        PageReference pr = inSchoolPortal ? Page.SchoolDashboard : Page.FamilyDashboard;
        pr.getParameters().put('login', 'true');
        return pr;
    }

    public PageReference cancel(){
        return new PageReference('/secur/logout.jsp');
    }
}