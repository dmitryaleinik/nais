/**
 * Class: SchoolAppViewLockPFSBulkController
 *
 * Copyright (C) 2015 NAIS
 *
 * Purpose: There are two parts:
 * 1. From the School Portal Skuid page (Status/Demographics), allow the user to check a number 
 * of folders and click "Lock Folders" or "Unlock Folders." These actions should update the "Family 
 * May Submit Updates" fields on all SPAs that belonging to the checked folders for this school. If 
 * the user is a multi-school user, it should only lock the SPAs for the current school that is "In Focus".
 * 2. From the School Portal Annual Settings page (under Setup), have two buttons: "Lock All PFSs" and 
 * "Unlock All PFSs". These buttons should change the "Family May Submit Updates" fields on all SPAs for 
 * this school/academic year. If the user is a multi-school user, it should only lock the SPAs for the 
 * current school that is "In Focus".
 * 
 * Where Referenced:
 *   School Portal - SchoolAppViewLockPFSBulk page
 *
 * Change History:
 *
 * Developer           Date        JIRA Ref     Description
 * ---------------------------------------------------------------------------------------
 * Leydi Rangel         05/19/15    NAIS-2411   Initial Development
 *
 * 
 */
public class SchoolAppViewLockPFSBulkController {
    /*  Initialization  */
    public SchoolAppViewLockPFSBulkController(ApexPages.StandardSetController controller)
    {
        this.totalRecordsProcessed = 0;
        this.getSelectedStudentFolders(controller);
        this.pageMessage = 'Are you sure that you want to lock the selected '
                            +(this.totalRecordsProcessed==1?'PFS record?'
                            :this.totalRecordsProcessed+' PFS records?');
        this.showActionButton = true;
    }//End:Constructor
    /*  End Initialization  */
    
    /*Properties*/
    public list<Id> selectedFolderIds{get; set;}
    public Integer totalRecordsProcessed{get; set;}
    private static final Integer MAX_LIMIT_DML = 100;
    public String pageMessage{get; set;}
    public boolean showActionButton{get; set;}
    /*End Properties*/
    
    /*Action Methods*/
    
    
    /*End Action Methods*/
    
    /* Helper Mehtods */
    public PageReference lockSelectedPFS()
    {
        this.showActionButton = false;
        if(selectedFolderIds!=null && selectedFolderIds.size()>0 && selectedFolderIds.size()<=MAX_LIMIT_DML)
        {
            list<School_PFS_Assignment__c> selectedPFS = [Select Id, Family_May_Submit_Updates__c, Applicant__r.PFS__r.PFS_Status__c, Applicant__r.PFS__r.Payment_Status__c
                                                            from School_PFS_Assignment__c 
                                                            where Student_Folder__c  IN:this.selectedFolderIds 
                                                            and Family_May_Submit_Updates__c!='No'];
            list<School_PFS_Assignment__c> pfsToUpdate = new list<School_PFS_Assignment__c>();
            list<School_PFS_Assignment__c> pfsFailToUpdate = new list<School_PFS_Assignment__c>();

            if(selectedPFS!=null && selectedPFS.size()>0)
            {

                for(School_PFS_Assignment__c iPfs:selectedPFS)
                {
                    if(iPfs.Applicant__r.PFS__r.PFS_Status__c == 'Application Submitted' && iPfs.Applicant__r.PFS__r.Payment_Status__c == 'Paid in Full') {
                        iPfs.Family_May_Submit_Updates__c = 'No';
                        pfsToUpdate.add(iPfs);
                    }
                    else
                        pfsFailToUpdate.add(iPfs);
                }
                
                if(pfsToUpdate.size()>0)
                {
                    try{
                        update pfsToUpdate;
                    }catch(Exception e){
                        this.pageMessage = 'Error while trying to lock PFSs: '+e.getMessage();
                        return null;
                    }
                    this.pageMessage = pfsToUpdate.size()+' PFSs were successfully locked.';
                    if(pfsFailToUpdate.size()>0)
                        this.pageMessage+=' One or more of the folders are unsubmitted and cannot be locked.';

                    pfsToUpdate = new list<School_PFS_Assignment__c>();
                }
                else{
                    this.pageMessage = 'This PFS has not been submitted. The folder cannot be locked until the family submits PFS.';
                }

            }else{
                this.pageMessage = this.totalRecordsProcessed==1
                                ?'The selected PFS record is locked.'
                                :'The selected '+this.totalRecordsProcessed+' PFS records are locked.';
            }
        }else if(selectedFolderIds.size()>MAX_LIMIT_DML){
            this.pageMessage = 'You may only lock ' + MAX_LIMIT_DML + ' PFSs at a time, but selected ' + selectedFolderIds.size() + ' PFSs.';
        }else{
            this.pageMessage = 'You must select at least 1 PFS to lock.';
        }
        return null;
    }//End:lockSelectedPFS
    
    
    private void getSelectedStudentFolders(ApexPages.StandardSetController controller)
    {
        list<Id> tmpFolderIds = new list<Id>();
        Integer counter = 0;
        this.selectedFolderIds = new list<Id>();
        Student_Folder__c[] tmpSelected = (Student_Folder__c[])controller.getSelected();
        this.totalRecordsProcessed = 0;
        
        for(Student_Folder__c folder:tmpSelected)
        {
            if(folder.Id!=null)
            {
                this.selectedFolderIds.add(folder.Id);
                this.totalRecordsProcessed++;
            }
        }
    }//End:getSelectedFolders
    
    /* End Helper Mehtods */
}