/**
 * @description This class is responsible for querying Article Disposition Mapping records.
 */
public class ArticleDispositionMappingSelector extends fflib_SObjectSelector
{
    
    /**
     * @description Default constructor for an instance of the ArticleDispositionMappingSelector
     *               that will not include field sets while NOT enforcing FLS or CRUD.
     */
    public ArticleDispositionMappingSelector()
    {
        // We have to avoid enforcing FLS or CRUD so that guest users can access these records as well.
        // This seemed less likely to break the portals than actually relying on guest permissions.
        super(false, false, false, false);
    }
    
    @testVisible private Map<String, Article_Disposition_Mapping__c> mapByArticleNumber;
    @testVisible private static final String ARTICLE_NUMBERS_PARAM  = 'articleNumbers';
    
    public Map<String, Article_Disposition_Mapping__c> getMapByArticleNumber() {
        
        if (mapByArticleNumber == null) {
        
            mapByArticleNumber = new Map<String, Article_Disposition_Mapping__c>();
            
            
            List<Article_Disposition_Mapping__c> records = Database.query(newQueryFactory().toSOQL());
            
            for (Article_Disposition_Mapping__c adm : records) {
                
                mapByArticleNumber.put(adm.Article_Number__c, adm);
            }
            
            if (test.isRunningTest()) {
                
                Article_Disposition_Mapping__c testArticle = new Article_Disposition_Mapping__c(
                    Title__c ='Uploading Scans or Pictures (JPGs) of 1040 and Other Documents', 
                    Visible_In_Public_Knowledge_Base__c=  true, 
                    Visible_to_Customer__c=   true, 
                    Article_Number__c=    '000001040', 
                    Type__c=  'Documents' , 
                    Disposition__c=   'Document Question' , 
                    Sub_Category__c=  'How to Upload Documents');
            
                mapByArticleNumber.put('000001040', testArticle);
            }
        }
        
        return mapByArticleNumber;
    }

    /**
     * @description Selects ADM records by Article_Number__c.
     * @param articleNumbers The Article Numbers to query for ADM records.
     * @return A list of Article_Disposition_Mapping__c records.
     * @throws An ArgumentNullException if articleNumbers is null.
     */
    public List<Article_Disposition_Mapping__c> selectByArticleNumber(Set<String> articleNumbers)
    {
        ArgumentNullException.throwIfNull(articleNumbers, ARTICLE_NUMBERS_PARAM);

        assertIsAccessible();

        String query = newQueryFactory(true)
            .setCondition('Article_Number__c IN :articleNumbers')
            .toSOQL();

        return Database.query(query);
    }

    private Schema.SObjectType getSObjectType()
    {
        return Article_Disposition_Mapping__c.SObjectType;
    }

    private List<Schema.SObjectField> getSObjectFieldList()
    {
        return new List<Schema.SObjectField>{
                Article_Disposition_Mapping__c.Article_Number__c,
                Article_Disposition_Mapping__c.Title__c,
                Article_Disposition_Mapping__c.Type__c,
                Article_Disposition_Mapping__c.Sub_category__c,
                Article_Disposition_Mapping__c.Disposition__c,
                Article_Disposition_Mapping__c.Visible_In_Public_Knowledge_Base__c,
                Article_Disposition_Mapping__c.Visible_to_Customer__c
        };
    }

    /**
     * @description Creates a new instance of the ArticleDispositionMappingSelector.
     * @return An instance of ArticleDispositionMappingSelector.
     */
    public static ArticleDispositionMappingSelector newInstance()
    {
        return new ArticleDispositionMappingSelector();
    }

    /**
     * @description Singleton instance property.
     */
    public static ArticleDispositionMappingSelector Instance
    {
        get
        {
            if (Instance == null)
            {
                Instance = newInstance();
            }
            return Instance;
        }
        private set;
    }
}