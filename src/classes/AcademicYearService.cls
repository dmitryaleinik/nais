/**
 * @description This class is used to retrieve the academic years that a user has access to as well as determine what
 *              the current academic year is for school users, family users, and family users with early access.
 *              This class is without sharing because Academic Year records aren't created by school or family users
 *              yet are critical for many system functions.
 */
public without sharing class AcademicYearService {

    @testVisible private static final String ACADEMIC_YEAR_ID_PARAM = 'academicYearId';
    @testVisible private static final String ACADEMIC_YEAR_NAME_PARAM = 'academicYearName';

    @testVisible private List<Academic_Year__c> allAcademicYears;
    private Map<String, Academic_Year__c> academicYearsByName;
    private Map<Id, Integer> academicYearPositionsById;
    private Schema.SObjectField startDateField;
    private Schema.SObjectField endDateField;
    @testVisible private Academic_Year__c testCurrentAcademicYear;
    @testVisible private Academic_Year__c testPreviousAcademicYear;

    private AcademicYearService() { }

    /**
     * @description Gets all academic year records by start date in descending order. If within a unit test and no
     *              academic year records exist, the TestUtils.cls will insert them.
     * @return A list of academic years.
     */
    public List<Academic_Year__c> getAllAcademicYears() {
        if (allAcademicYears == null || allAcademicYears.size() == 0) {
            allAcademicYears = AcademicYearsSelector.newInstance().selectAll();

            if (Test.isRunningTest() && (allAcademicYears == null || allAcademicYears.size() == 0)) {
                allAcademicYears = TestUtils.createAcademicYears();
            }
        }
        return allAcademicYears;
    }

    /**
     * @description Gets all academic years that are available to the current user based on the start and end dates of
     *              academic year records. The Academic Year start/end date fields used are relative to the current
     *              user. Family Users rely on the Family Portal Start/End Dates. After getting the records by date
     *              ranges, there is one final check for Family Users to see if the next academic year should be
     *              included based on early access.
     * @return The Academic Year records available to the current user.
     */
    public List<Academic_Year__c> getAvailableAcademicYears() {
        Date currentDate = GlobalVariables.getCurrentDate();

        // Get academic years where the current date is within the start/end dates relevant to the current user.
        // Family users will use Family Portal Start/End Date fields.
        // School users will use Start/End Date fields.
        List<Academic_Year__c> openAcademicYears = getAllOpenYearsByDate(currentDate);

        if (openAcademicYears.isEmpty()) {
            return openAcademicYears;
        }

        // If the current user isn't a family user, don't do any special handling and return early.
        if (!isFamilyPortalUser()) {
            return openAcademicYears;
        }

        // For family portal users, we need to grab the latest open academic year then see if there is another academic year after that which should be accessible due to early access.
        // If so, then that academic year will be the 'current academic year' for the user.
        Academic_Year__c latestAcademicYear = openAcademicYears[0];

        // Determine the latest academic year's position compared to all academic year records.
        // 0 would indicate that there are no academic year records after this one.
        Integer latestAcademicYearPosition = getPositionsById().get(latestAcademicYear.Id);

        // If the position is 0, we know there is nothing else the user could access.
        if (latestAcademicYearPosition == 0) {
            return openAcademicYears;
        }

        if (CurrentUser.hasEarlyAccess()) {
            Integer nextAcademicYearPosition = latestAcademicYearPosition - 1;

            Academic_Year__c nextAcademicYear = getAllAcademicYears()[nextAcademicYearPosition];

            List<Academic_Year__c> academicYearsForFamilyUser = new List<Academic_Year__c> { nextAcademicYear };

            academicYearsForFamilyUser.addAll(openAcademicYears);

            return academicYearsForFamilyUser;
        }

        return openAcademicYears;
    }

    /**
     * @description Gets the current academic year for the current user. The current academic year record is determined
     *              by finding the latest academic year record that the current user has access to.
     * @return The current Academic Year record. Null if there are no records available.
     */
    public Academic_Year__c getCurrentAcademicYear() {
        
        if (Test.isRunningTest() && testCurrentAcademicYear != null) {
            return testCurrentAcademicYear;
        }
        
        // Family users require us to only display academic years where the current date is within the Family Portal Start/End Date ranges.
        // The only exception is for early access users which get access to an AY before the Family Portal Start Date.
        if (isFamilyPortalUser()) {
            List<Academic_Year__c> academicYearsAvailableToFamilyUser = getAvailableAcademicYears();

            return academicYearsAvailableToFamilyUser.isEmpty() ? null : academicYearsAvailableToFamilyUser[0];
        }

        // Begin handling for school users.
        Integer currentYearIndex = getAcademicYearPositionByDate(GlobalVariables.getCurrentDate());

        Academic_Year__c currentAcademicYear = getAcademicYearForUserBasedOnPosition(currentYearIndex);

        return currentAcademicYear;
    }

    /**
     * @description Gets the previous academic year for the current user. The previous academic year is simply the
     *              academic year before the current academic year. If the user is only able to access one academic
     *              year, null is returned.
     * @return The previous Academic Year record or null.
     */
    public Academic_Year__c getPreviousAcademicYear() {
        
        if (Test.isRunningTest() && testPreviousAcademicYear != null) {
            
            return testPreviousAcademicYear;
        }
        
        // Family users require us to only display academic years where the current date is within the Family Portal Start/End Date ranges.
        // We will get all academic years available to the user. If there is at least 2, return the second one.
        if (isFamilyPortalUser()) {
            List<Academic_Year__c> yearsAvailableToFamilyUser = getAvailableAcademicYears();

            // If there is more than one year available to the user, return the second AY otherwise return null.
            return yearsAvailableToFamilyUser.size() >= 2 ? yearsAvailableToFamilyUser[1] : null;
        }

        // Begin handling school users. This won't necessarily return an academic year where the current date is within the start/end dates of an AY record.
        // It will simply return the AY before whichever record is the current AY for school users.
        // School users always have access to two academic years.
        Integer defaultCurrentAcademicYearPosition = getAcademicYearPositionByDate(GlobalVariables.getCurrentDate());

        Academic_Year__c prevAcadYear = getAcademicYearForUserBasedOnPosition(defaultCurrentAcademicYearPosition + 1);

        return prevAcadYear;
    }

    /**
     * @description Gets the start of the overlap period as a string. This is done by finding the academic year after
     *              the current academic year and formatting the Family Portal Start Date.
     * @return The start date of the next academic year as a string.
     */
    public String getOverlapStartDate() {
        Academic_Year__c currentAcademicYear = getCurrentAcademicYear();

        Academic_Year__c nextAcademicYear = getNextAcademicYear(currentAcademicYear);

        if (nextAcademicYear == null || nextAcademicYear.Family_Portal_Start_Date__c == null) {
            return null;
        }

        return nextAcademicYear.Family_Portal_Start_Date__c.format();
    }

    /**
     * @description Determines if users should be taken to the landing page after logging in based on whether or not
     *              the current date/time is within the next academic year's landing page start date and family portal
     *              start date.
     * @return True if the user should be taken to the landing page.
     */
    public Boolean showLandingPage() {
        Academic_Year__c currentAcademicYear = getCurrentAcademicYear();

        Academic_Year__c nextAcademicYear = getNextAcademicYear(currentAcademicYear);

        // Do not show landing page, if there is no academic year set up after the current academic year available to the user.
        if (nextAcademicYear == null) {
            return false;
        }

        Integer nextStartYear = nextAcademicYear.Family_Portal_Start_Date__c.year();
        Integer nextStartMonth = nextAcademicYear.Family_Portal_Start_Date__c.month();
        Integer nextStartDay = nextAcademicYear.Family_Portal_Start_Date__c.day();

        DateTime nextYearStart = DateTime.newInstance(nextStartYear, nextStartMonth, nextStartDay);
        DateTime landingPageStart = nextAcademicYear.Family_Landing_Page_Start_Date__c;
        DateTime rightNow = System.now();

        if (nextYearStart == null || landingPageStart == null) {
            return false;
        }

        // Show landing page if the current date and time is between the
        // landing page start date and start date of the next academic year.
        return rightNow >= landingPageStart && rightNow <= nextYearStart;
    }

    private Academic_Year__c getNextAcademicYear(Academic_Year__c currentAcademicYear) {
        if (currentAcademicYear == null) {
            return null;
        }

        Integer currentYearPosition = getPositionsById().get(currentAcademicYear.Id);

        if (currentYearPosition == 0) {
            return null;
        }

        return getAllAcademicYears()[currentYearPosition - 1];
    }

    public Map<Id, Integer> getPositionsById() {
        if (academicYearPositionsById != null) {
            return academicYearPositionsById;
        }

        academicYearPositionsById = new Map<Id, Integer>();

        List<Academic_Year__c> allAcademicYearRecords = getAllAcademicYears();
        Integer numberOfAcademicYears = allAcademicYearRecords.size();

        for (Integer i = 0; i < numberOfAcademicYears; i++) {
            Academic_Year__c record = allAcademicYearRecords[i];
            academicYearPositionsById.put(record.Id, i);
        }

        return academicYearPositionsById;
    }

    public List<Academic_Year__c> getAllOpenYearsByDate(Date targetDate) {
        List<Academic_Year__c> openAcademicYears = new List<Academic_Year__c>();

        List<Academic_Year__c> allAcademicYearRecords = getAllAcademicYears();

        Integer numberOfYears = allAcademicYearRecords.size();

        for (Integer i = 0; i < numberOfYears; i++) {
            Academic_Year__c academicYear = allAcademicYearRecords[i];
            if (isAcademicYearOpen(academicYear, targetDate)) {
                openAcademicYears.add(academicYear);
            }
        }

        return openAcademicYears;
    }

    public Boolean isAcademicYearOpen(Academic_Year__c academicYear, Date targetDate) {
        Date userSpecificStartDate = getDate(academicYear, getStartDateField());
        Date userSpecificEndDate = getDate(academicYear, getEndDateField());

        return userSpecificStartDate <= targetDate && userSpecificEndDate >= targetDate;
    }

    private Date getDate(Academic_Year__c academicYear, Schema.SObjectField dateField) {
        return (Date)academicYear.get(dateField);
    }

    // SFP-663 [Zach Field] 08.18.2016
    private Integer getAcademicYearPositionByDate(Date targetDate) {
        List<Academic_Year__c> allAcademicYearRecords = getAllAcademicYears();

        Integer numberOfYears = allAcademicYearRecords.size();

        // Loop through all academic years to figure out which academic year has a date range which the target date falls in.
        // Once we find an academic year which satisfies that criteria, return the index representing the records
        // position in the list of all academic years.
        // The index of the desired record is returned because we sometimes need to grab the academic year before the
        // one identified by the current date. This is generally the case for Family Portal users who shouldn't have
        // access to the current academic year until schools open up the current year.
        for (Integer i = 0; i < numberOfYears; i++) {
            Academic_Year__c academicYear = allAcademicYearRecords[i];
            if (isAcademicYearOpen(academicYear, targetDate)) {
                return i;
            }
        }

        return null;
    }

    // SFP-663 [Zach Field] 08.18.2016
    // Gets the academic year from the list of all academic years based on location in the list of all academic years.
    private Academic_Year__c getAcademicYearForUserBasedOnPosition(Integer academicYearPosition) {
        List<Academic_Year__c> allAcademicYearRecords = getAllAcademicYears();

        // If there are no academic years, return null.
        if (allAcademicYearRecords.isEmpty()) {
            return null;
        }

        Integer index = academicYearPosition;
        Integer totalNumberOfAcademicYears = allAcademicYearRecords.size();

        // If the specified position is equal to or greater than the total number of academic years, grab the last academicYear.
        if (index >= totalNumberOfAcademicYears) {
            return allAcademicYearRecords[totalNumberOfAcademicYears - 1];
        }

        return allAcademicYearRecords[index];
    }

    private Schema.SObjectField getStartDateField() {
        if (startDateField != null) {
            return startDateField;
        }

        startDateField = isFamilyPortalUser() ? Academic_Year__c.Family_Portal_Start_Date__c : Academic_Year__c.Start_Date__c;

        return startDateField;
    }

    private Schema.SObjectField getEndDateField() {
        if (endDateField != null) {
            return endDateField;
        }

        endDateField = isFamilyPortalUser() ? Academic_Year__c.Family_Portal_End_Date__c : Academic_Year__c.End_Date__c;

        return endDateField;
    }

    private Boolean isFamilyPortalUser() {
        return GlobalVariables.isFamilyPortalUser() || CurrentUser.isGuest();
    }

    /**
     * @description Determines whether or not the family portal has opened for the specified academic year. An academic
     *              year has opened for the family portal if today's date is after the Family Portal Start
     *              Date. This method will return false if the specified academic year is not one of the 10
     *              latest academic years.
     * @param academicYearId The Id of the academic year to check.
     * @return True if the family portal has opened for the specified academic year.
     * @throws ArgumentNullException if academicYearId is null.
     */
    public Boolean hasFamilyPortalOpened(Id academicYearId) {
        ArgumentNullException.throwIfNull(academicYearId, ACADEMIC_YEAR_ID_PARAM);

        Map<Id, Academic_Year__c> yearsById = new Map<Id, Academic_Year__c>(getAllAcademicYears());

        Academic_Year__c specifiedYear = yearsById.get(academicYearId);

        if (specifiedYear == null) {
            return false;
        }

        Date familyStart = specifiedYear.Family_Portal_Start_Date__c;

        if (familyStart == null) {
            return false;
        }

        Date currentDate = Date.today();

        // Check that the current date is after the family start date.
        return currentDate >= familyStart;
    }
    
    public Boolean isFamilyPortalOpen(Id academicYearId) {
        ArgumentNullException.throwIfNull(academicYearId, ACADEMIC_YEAR_ID_PARAM);
        
        Map<Id, Academic_Year__c> academicYearsMap = new Map<Id, Academic_Year__c>(getAllAcademicYears());

        Academic_Year__c academicYear = academicYearsMap.get(academicYearId);

        return isFamilyPortalOpen(academicYear);
    }

    public Boolean isFamilyPortalOpen(String academicYearName) {
        ArgumentNullException.throwIfNull(academicYearName, ACADEMIC_YEAR_NAME_PARAM);

        Academic_Year__c academicYear = getAcademicYearsByName().get(academicYearName);

        return isFamilyPortalOpen(academicYear);
    }

    private Boolean isFamilyPortalOpen(Academic_Year__c academicYearRecord) {
        if (academicYearRecord == null) {
            return false;
        }

        Date familyPortalStartDate = academicYearRecord.Family_Portal_Start_Date__c;
        Date familyPortalEndDate = academicYearRecord.Family_Portal_End_Date__c;

        if (familyPortalStartDate == null || familyPortalEndDate == null) {
            return false;
        }

        Date todayDate = Date.today();

        // Check that the current date is between the family Portal startDate and endDate.
        return todayDate >= familyPortalStartDate && todayDate <= familyPortalEndDate;
    }

    private Map<String, Academic_Year__c> getAcademicYearsByName() {
        if (academicYearsByName == null || academicYearsByName.isEmpty()) {
            academicYearsByName = new Map<String, Academic_Year__c>();

            for (Academic_Year__c record : getAllAcademicYears()) {
                academicYearsByName.put(record.Name, record);
            }
        }

        return academicYearsByName;
    }

    /**
     * @description Singleton instance of the AcademicYearService. This is testVisible to allow unit tests to clear caches as needed.
     */
    @testVisible
    public static AcademicYearService Instance {
        get {
            if (Instance == null) {
                Instance = new AcademicYearService();
            }
            return Instance;
        }
        private set;
    }
}