/**
 * Created by Nate on 8/15/17.
 */

@isTest
public class FinancialAidApiServiceTest {

    private static User schoolPortalUser;
    private static String schoolId;
    private static String secondSchoolId;
    private static Account school;
    private static ConnectedApplication connectedApp;

    @isTest
    static void authorizeUser_withValidUser_withRValidConnApp_userAuthorizedForConnApp() {

        System.runAs(CurrentUser.getCurrentUser()) {
            connectedApp = UserProvisioning.ConnectorTestUtil.createConnectedApp('TestApp');
        }

        setupTestData(false);
        FinancialAidApiService.AuthorizationRequest authRequest = new FinancialAidApiService.AuthorizationRequest(schoolPortalUser.Id, connectedApp.Id);

        System.runAs(CurrentUser.getCurrentUser()) {
            Test.startTest();
            FinancialAidApiService.Response resp = FinancialAidApiService.Instance.authorizeUserForApi(authRequest);
            getUserById();
            List<IntegrationLog__c> logs = getLoggedException();
            System.assertEquals(true, logs.isEmpty(), 'Exception Logs shouldnt exist');
            System.assertEquals(true, schoolPortalUser.Financial_Aid_API_Apps__c != null, 'Connected App wasnt assigned to user');
            System.assertEquals('TestApp', schoolPortalUser.Financial_Aid_API_Apps__c, 'School Portal User should have TestApp Connected App assigned');
            System.assertEquals(true, resp.isSuccessful(), 'User wasnt successfully authorized for the connected app');

            Test.stopTest();
        }
    }

    @isTest
    static void authorizeUser_withValidUser_withInvalidConnApp_userNotAuthorized() {
        System.runAs(CurrentUser.getCurrentUser()) {
            connectedApp = UserProvisioning.ConnectorTestUtil.createConnectedApp('TestAppOne');
        }

        setupTestData(false);
        FinancialAidApiService.AuthorizationRequest authRequest = new FinancialAidApiService.AuthorizationRequest(schoolPortalUser.Id, connectedApp.Id);

        System.runAs(CurrentUser.getCurrentUser()) {
            Test.startTest();
            FinancialAidApiService.Response resp = FinancialAidApiService.Instance.authorizeUserForApi(authRequest);
            getUserById();
            List<IntegrationLog__c> logs = getLoggedException();

            System.assertEquals('FATAL', logs[0].Status__c, 'Log Excpetion should be FATAL');
            System.assertEquals(false, schoolPortalUser.Financial_Aid_API_Apps__c != null, 'Connected App was assigned to user');
            System.assertEquals(false, resp.isSuccessful(), 'User wasnt successfully authorized for the connected app');
            Test.stopTest();
        }
    }

    @isTest
    static void authorizeUser_withNonConnectedUser_withValidConnApp_userAuthorized() {
        System.runAs(CurrentUser.getCurrentUser()) {
            connectedApp = UserProvisioning.ConnectorTestUtil.createConnectedApp('TestApp');
        }

        setupTestData(false);
        FinancialAidApiService.AuthorizationRequest authRequest = new FinancialAidApiService.AuthorizationRequest(schoolPortalUser.Id, connectedApp.Id);

        System.runAs(CurrentUser.getCurrentUser()) {
            Test.startTest();
            FinancialAidApiService.Response resp = FinancialAidApiService.Instance.authorizeUserForApi(authRequest);
            getUserById();
            List<IntegrationLog__c> logs = getLoggedException();
            System.assertEquals(true, logs.isEmpty(), 'Exception Logs shouldnt exist');
            System.assertEquals(true, schoolPortalUser.Financial_Aid_API_Apps__c != null, 'Connected App wasnt assigned to user');
            System.assertEquals('TestApp', schoolPortalUser.Financial_Aid_API_Apps__c, 'School Portal User should have TestApp Connected App assigned');
            System.assertEquals(true, resp.isSuccessful(), 'User wasnt successfully authorized for the connected app');
            Test.stopTest();
        }
    }

    static void getUserById() {
        schoolPortalUser = [SELECT Id, Financial_Aid_API_Apps__c
                  FROM User
        WHERE Id =: schoolPortalUser.Id][0];
    }

    static List<IntegrationLog__c> getLoggedException() {
        return [Select Id, Status__c, Source__c
                FROM IntegrationLog__c
                WHERE Source__c = 'Financial Aid API - Authentication'];
    }

    static void setupTestData(Boolean multiSchool) {

        school = AccountTestData.Instance
                    .asSchool()
                    .DefaultAccount;

        schoolId = school.Id;

        // Staff contact
        Contact schoolStuff = ContactTestData.Instance
                .forAccount(school.Id)
                .asSchoolStaff()
                .DefaultContact;

        // Create staff user
        System.runAs(CurrentUser.getCurrentUser()) {
            schoolPortalUser = UserTestData.createSchoolPortalUser();
            schoolPortalUser.In_Focus_School__c = school.Id;
            insert schoolPortalUser;
        }


        // Sharing account with user
        insertAccountShare(school.Id, schoolPortalUser.Id);

        // Second school, used as possible invalid Id or for multiple school cases
        Account schoolTwo = AccountTestData.Instance
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .insertAccount();

        secondSchoolId = schoolTwo.Id;

        // Create Affiliation and second application when testing multiple school cases
        if (multiSchool) {

            Affiliation__c affiliation = AffiliationTestData.Instance
                    .forType('Additional School User')
                    .forStatus('Current')
                    .forOrganizationId(schoolTwo.Id)
                    .createAffiliationWithoutReset();

            insert affiliation;
        }
    }

    private static void insertAccountShare(Id accountId, Id userId) {
        insert new AccountShare(
                AccountId = AccountId,
                CaseAccessLevel = 'Edit',
                OpportunityAccessLevel = 'Edit',
                AccountAccessLevel = 'Edit',
                UserOrGroupId = userId
        );
    }


}