/**
 * MassEmailSendDataAccessServiceTest.cls
 *
 * @description: Test class for MassEmailSendDataAccessService using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
private class MassEmailSendDataAccessServiceTest {

    /* test data setup */
    @testSetup static void setupTestData() {

        MassEmailSendTestDataFactory.createEmailTemplate();

        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {

            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
        }
    }

    /* negative test cases */

    // Attempt getMassEmailSendById with null param, expect null object ref returned
    @isTest static void getMassEmailSendById_withNull_nullMassEmailSend() {

        // Arrange
        Mass_Email_Send__c massES = new Mass_Email_Send__c();

        // Act
        massES = new MassEmailSendDataAccessService().getMassEmailSendById( null);

        // Assert
        System.assertEquals( null, massES);
    }

    // Attempt getMassEmailSendByIdSet with null param, expect null object ref returned
    @isTest static void getMassEmailSendByIdSet_withNull_nullMassEmailSend() {

        // Arrange

        // Act
        List<Mass_Email_Send__c> massESList = new MassEmailSendDataAccessService().getMassEmailSendByIdSet( null);

        // Assert
        System.assertEquals( null, massESList);
    }

    // Attempt getMassEmailSendBySchoolId with null param, expect null object ref returned
    @isTest static void getMassEmailSendBySchoolId_withNull_nullMassEmailSend() {

        // Arrange

        // Act
        List<Mass_Email_Send__c> massESList = new MassEmailSendDataAccessService().getMassEmailSendBySchoolId( null);

        // Assert
        System.assertEquals( null, massESList);
    }

    // Attempt getMassEmailSendTemplateId with null param, expect null object ref returned
    @isTest static void getMassEmailSendTemplateId_withNull_nullId() {

        // Arrange
        Id testId;

        // Act
        testId = new MassEmailSendDataAccessService().getMassEmailSendTemplateId( null);

        // Assert
        System.assertEquals( null, testId);
    }

    // Attempt cloneMassEmailRecord with null param, expect null object ref returned
    @isTest static void cloneMassEmailRecord_withNull_nullId() {

        // Arrange
        Id testId;

        // Act
        testId = new MassEmailSendDataAccessService().cloneMassEmailRecord( null);

        // Assert
        System.assertEquals( null, testId);
    }


    /* postive test cases */

    // Attempt getMassEmailSendById with valid param, expect MES record returned
    @isTest static void getMassEmailSendById_withValidId_massEmailSendRecord() {

        // Arrange
        Mass_Email_Send__c massES = [select Id from Mass_Email_Send__c limit 1];

        // Act
        massES = new MassEmailSendDataAccessService().getMassEmailSendById( massES.Id);

        // Assert
        System.assertNotEquals( null, massES);
    }

    // Attempt getMassEmailSendByIdSet with valid param, expect valid object ref returned
    @isTest static void getMassEmailSendByIdSet_withValidParam_validList() {

        // Arrange
        Mass_Email_Send__c massES = [select Id from Mass_Email_Send__c limit 1];

        // Act
        List<Mass_Email_Send__c> massESList = new MassEmailSendDataAccessService().getMassEmailSendByIdSet( new Set<String> { massES.Id});

        // Assert
        System.assertNotEquals( null, massESList);
    }

    // Attempt getMassEmailSendBySchoolId with valid param, expect valid object ref returned
    @isTest static void getMassEmailSendBySchoolId_withValid_validList() {

        // Arrange
        Account acct = [select Id from Account where Name = :MassEmailSendTestDataFactory.TEST_SCHOOL_NAME];

        // Act
        List<Mass_Email_Send__c> massESList = new MassEmailSendDataAccessService().getMassEmailSendBySchoolId( acct.Id);

        // Assert
        System.assertNotEquals( null, massESList);
    }

    // Attempt getMassEmailSendTemplateId with valid param, expect valid Id returned
    @isTest static void getMassEmailSendTemplateId_withValidParam_validTemplateId() {

        // Arrange
        Id testId;

        // Act
        testId = new MassEmailSendDataAccessService().getMassEmailSendTemplateId( 'MassEmailTemplate');

        // Assert
        System.assertNotEquals( null, testId);
    }

    // Attempt triggerMassEmailResend with valid param, expect valid object ref returned
    @isTest static void triggerMassEmailResend_withValidParam_validUpdate() {

        // Arrange
        Mass_Email_Send__c massES = [select Id from Mass_Email_Send__c limit 1];

        // Act
        Test.startTest();

            // Create Academic Year, Applicant, PFS && sPFS Assignment
            MassEmailSendTestDataFactory.createRequiredPFSData();
            Test.setMock( HttpCalloutMock.class, new SendgridMockHttpResponseGenerator());
            new MassEmailSendDataAccessService().triggerMassEmailResend( massES.Id);
        Test.stopTest();

        // Assert
        // no work
    }

    // Attempt cloneMassEmailRecord with valid param, expect valid object ref returned
    @isTest static void cloneMassEmailRecord_withValidParam_validId() {

        // Arrange
        Id testId;
        Mass_Email_Send__c massES = [select Id from Mass_Email_Send__c limit 1];

        // Act
        testId = new MassEmailSendDataAccessService().cloneMassEmailRecord( massES.Id);

        // Assert
        System.assertNotEquals( null, testId);
    }

    // Attempt deleteMassEmailRecord with valid param, expect valid object ref returned
    @isTest static void deleteMassEmailRecord_withValidParam_validDelete() {

        // Arrange
        Mass_Email_Send__c massES = [select Id from Mass_Email_Send__c limit 1];

        // Act
        new MassEmailSendDataAccessService().deleteMassEmailRecord( massES.Id);

        // Assert
        // no work
    }

}