/**
* @description Class implemented to contain the common components
* shared between family portal pages.
*/
public class FamilyTemplateController  implements FamilyPortalTopBarInterface {
    private static FamilyTemplateController instance = null;
    public String pageTitle { get; set; }
    public String tabSelected { get; set; }
    public String bodyClass { get; set; }
    public String pfsId { get; set; }
    public PFS__c pfsRecord { get; set; }
    public String academicYearId { get; set; }
    public Academic_Year__c academicYear { get; set; }
    public FamilyPortalHiddenFields hiddenFields { get; set; }
    public List<PFS__c> pfsRecords;
    private String pageName;

    public FamilyTemplateController() {
        initializeSettingsForAllUsers();
        initializeSettingsForAuthenticatedUsers();
    }//End-Constructor
    
    private void checkHiddenItemsUI(PFS__c tmpPFS) {        
        if (this.pageName == 'familyapplicationmain' || this.pageName == 'familydashboard') {            
            hiddenFields = PfsProcessService.Instance.checkHiddenItemsUI(tmpPFS.Submission_Process__c);
        }
    }//End:checkHiddenItemsUI

    public String getTemplate() {
        String tpl = (Site.getTemplate()!=null?(Site.getTemplate()).getURL():null);
        return (tpl == null || tpl == '' || tpl == '/site/SiteTemplate.apexp'
                    ? 'FamilyTemplate' : tpl);
    }//End:getTemplate

    public static FamilyTemplateController getInstance() {
        if (instance == null) {
            instance = new FamilyTemplateController();
        }
        return instance;
    }//End:getInstance

    /**
     * @description These settings are for both authenticated and non authenticated users.
     */
    private void initializeSettingsForAllUsers() {
        instance = this;
        this.pageName = FamilyPageUtils.getPageName(ApexPages.currentPage());
        pageSettings settings = this.getPageSettings(this.pageName);

        String integrationParams = ApexPages.currentPage().getParameters().get('saoId');
        if( String.isNotEmpty( integrationParams)) {

            this.pfsId = integrationParams.split(':')[0];
        } else {

            this.pfsId = ApexPages.currentPage().getParameters().get(settings.pfsIdentifier);
        }
        this.tabSelected = settings.tabName;
        this.bodyClass = settings.bodyClass;
        this.pageTitle = FamilyPageUtils.getFamilyPageTitle(ApexPages.currentPage());
    }

    /**
     * @description Only authenticated users should have these settings initialized. An example is
     *              querying the user's pfs.
     */
    private void initializeSettingsForAuthenticatedUsers() {
        if (CurrentUser.isGuest()) { return; }

        this.academicYearId = ApexPages.currentPage().getParameters().get('AcademicYearId');
        this.academicYearId = (this.academicYearId==null?(GlobalVariables.getCurrentAcademicYear()).Id:this.academicYearId);

        this.academicYear = [select Id, Name, PFS_Workbook_Url__c from Academic_Year__c where Id = :academicYearId];
        this.loadPFS();
    }

    /**
    * @description Method implemented to retrive settings for the current page.
    * @param pageName The current page name.
    *
    * @return The settings (tab's name for c:FamilyPortalTopBar, body Styleclass,
    * URL identifier for pfs's id) for the current page.
    */
    private pageSettings getPageSettings(String pageName) {
        Map<String, pageSettings> settings = new Map<String, pageSettings>{
                            'familymessages' => new pageSettings('Messages', 'singleColumn withPageBackground', 'id'),
                            'familymessagesnew' => new pageSettings('Messages', 'singleColumn withPageBackground', 'pfsId'),
                            'familymessagesedit' => new pageSettings('Messages', 'singleColumn withPageBackground', 'id'),
                            'familydocuments' => new pageSettings('SchoolRequiredDocument', 'singleColumn withPageBackground', 'id'),
                            'familyhelp' => new pageSettings('FamilyHelpPage', 'singleColumn withPageBackground', 'id'),
                            'familydashboard' => new pageSettings('FamilyDashboard', 'portalPage familyPortalDashboard withPageBackground', 'id'),
                            'familypaymentfeewaiverapplied' => new pageSettings('FamilyDashboard', 'singleColumn withPageBackground', 'id'),
                            'familyapplicationmain' => new pageSettings('FamilyApplicationMain', 'portalPage withPageBackground', 'id'),
                            'familypayment' => new pageSettings('FamilyApplicationMain', 'portalPage withPageBackground', 'id'),
                            'familypaymenterror' => new pageSettings('FamilyApplicationMain', 'portalPage withPageBackground', 'id'),
                            'familypaymentprevpaid' => new pageSettings('FamilyApplicationMain', 'portalPage withPageBackground', 'id'),
                            'familypaymentsubmit' => new pageSettings('FamilyApplicationMain', 'portalPage withPageBackground', 'id'),
                            'familypaymentcomplete' => new pageSettings('FamilyApplicationMain', '', 'id'),
                            'familypaymentwaivercomplete' => new pageSettings('FamilyApplicationMain', '', 'id'),
                            // TODO SFP-1750 the 'id' should be used as the PFS Identifier not the folder Id param.
                            // If I understand correctly it's pfs Id already not folder Id
                            'familyawards' => new pageSettings('Awards', '', 'id')
                            };
        return (settings.containsKey(pageName)
                ?(settings.get(pageName))
                :new pageSettings('NAIS', 'singleColumn withPageBackground', 'id'));
    }//End:getPageSettings

    private void loadPFS() {
        FamilyTemplateController.config configurations;

        if(this.pageName == 'familymessages') {
            configurations = FamilyMessagesController.loadPFS(this.pfsId, this.academicYearId);
        } else if (this.pageName == 'familymessagesnew') {
            configurations = FamilyMessagesNewController.loadPFS(this.pfsId, this.academicYearId);
        } else if (this.pageName == 'familymessagesedit') {
            configurations = FamilyMessagesEditController.loadPFS(this.pfsId, this.academicYearId);
        } else if (this.pageName == 'familydocuments') {
            configurations = FamilyDocumentsController.loadPFS(this.pfsId, this.academicYearId);
        } else if (this.pageName == 'familyhelpsearchresults') {
            configurations = FamilyHelpSearchResultsController.loadPFS(this.pfsId, this.academicYearId);
        } else if (this.pageName == 'familyhelpresultsdetail') {
            configurations = FamilyHelpResultsDetailController.loadPFS(this.pfsId, this.academicYearId);
        } else if (this.pageName == 'familydashboard' || this.pageName == 'familypaymentfeewaiverapplied') {
            configurations = FamilyDashboardController.loadPFS(this.pfsId, this.academicYearId);
        } else if (this.pageName == 'familyapplicationmain') {
            configurations = FamilyAppMainController.loadPFS(this.pfsId, this.academicYearId);
        } else if (this.pageName == 'familypayment' || this.pageName == 'familypaymenterror' || this.pageName == 'familypaymentprevpaid' || this.pageName == 'familypaymentsubmit') {
            configurations = FamilyPaymentController.loadPFS(this.pfsId, this.academicYearId);
        } else if (this.pageName == 'familypaymentcomplete' || this.pageName == 'familypaymentwaivercomplete') {
           configurations = FamilyPaymentCompleteController.loadPFS(this.pfsId, this.academicYearId);
        } else if (this.pageName == 'familyaward') {
            String folderId = ApexPages.currentPage().getParameters().get('id');
            String pfsId = ApexPages.currentPage().getParameters().get('pfsId');

System.debug('FamilyTemplateController folderId' + folderId);
System.debug('FamilyTemplateController pfsId' + pfsId);
            configurations = FamilyAwardController.loadPFS(folderId, pfsId);
        } else {
            PFS__c pfs;
            if (this.pfsId != null) {
                List<PFS__c> records = PfsSelector.Instance.selectById(new Set<Id> { this.pfsId });
                pfs = (!records.isEmpty()) ? records[0] : null;
            } else {
                User currentUser = GlobalVariables.getCurrentUser();
                List<PFS__c> records = PfsSelector.Instance.selectByContactIdAndAcademicYearId(currentUser.contactId, this.academicYearId);
                pfs = (records != null && !records.isEmpty()) ? records[0] : null;
            }

            configurations = new FamilyTemplateController.config(pfs, null);
        }

        this.pfsRecord = configurations.pfsRecord;
        this.pfsRecords = configurations.pfsRecords;
        this.pfsId = (this.pfsRecord!=null?this.pfsRecord.Id:null);
        this.checkHiddenItemsUI(this.pfsRecord);
    }//End:loadPFS

    public FamilyTemplateController Me {
        get { return this; }
    }//End:Me

    public PageReference YearSelector_OnChange(Id newAcademicYearId){
        if(this.pageName == 'familypayment' || this.pageName == 'familypaymenterror' || this.pageName == 'familypaymentprevpaid' || this.pageName == 'familypaymentsubmit') {
            PageReference pr = Page.FamilyPayment;
            pr.setRedirect(true);
               User u = GlobalVariables.getCurrentUser();
               // for admin transactions, need to look up the new PFS Id
            if (!GlobalVariables.isDatabankUser(u) && u.ContactId == null) {
                for (PFS__c pfs : [SELECT Id FROM PFS__c
                                        WHERE Parent_A__c = :this.pfsRecord.Parent_A__c
                                            AND Academic_Year_Picklist__c = :GlobalVariables.getAcademicYear(newAcademicYearId).Name
                                            LIMIT 1]) {
                    pr.getParameters().put('id', pfs.Id);
                }
            } else { // otherwise, just pass along the new academic year
                pr.getParameters().put('academicyearid', newAcademicYearId);
            }

            return pr;
        } else if (this.pageName == 'familypaymentwaivercomplete' || this.pageName == 'familypaymentcomplete') {
            return FamilyPaymentCompleteController.returnToDashboard();
        }
        return null;
    }//End:YearSelector_OnChange

    public String getAcademicYearId() {
        return GlobalVariables.getAcademicYearByName(pfsRecord.Academic_Year_Picklist__c).Id;
    }//End:getAcademicYearId

    public class config {
        public PFS__c pfsRecord{get;set;}
        public List<PFS__c> pfsRecords{get;set;}

        public config(PFS__c varPfs, List<PFS__c> varPfsRecords) {
            this.pfsRecord = (varPfs==null?new PFS__c():varPfs);
            this.pfsRecords = (varPfsRecords==null?new List<PFS__c>():varPfsRecords);
        }
    }

    private class pageSettings {
        public String tabName{ get; set; }
        public String bodyClass{ get; set; }
        public String pfsIdentifier{ get; set; }

        public pageSettings(String varTab, String varBody, String varPfs) {
            this.tabName = varTab;
            this.bodyClass = varBody;
            this.pfsIdentifier = varPfs;
        }//End-Constructor
    }//End-Class
}//End-Class