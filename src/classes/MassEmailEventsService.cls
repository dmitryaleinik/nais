/**
 * MassEmailEventsService.cls
 *
 * @description Service class for handling common interations with Mass Email provider Event data, stored on the SPA as raw JSON.
 *
 * @author Chase Logan @ Presence PG
 */
public without sharing class MassEmailEventsService {
    
    // default ctor
    public MassEmailEventsService() {}

    
    /* static methods */

    /**
     * @description: Event's are stored as plain text comma separated JSON objects, wrap events in leading and trailing braces
     * to make valid JSON array before deserializing to corresponding SendgridEvent object
     * 
     * @param: spa - a valid School_PFS_Assignment__c record
     *
     * @return: A list of serialized Sendgrid Event's
     */
    public static List<SendgridEvent> getDeserializedEventData( School_PFS_Assignment__c spa) {
        List<SendgridEvent> returnList;

        // early return on invalid data
        if ( spa == null || String.isBlank( spa.Mass_Email_Events__c)) return returnList;
        
        try {

            String jsonArrayString = '[' + spa.Mass_Email_Events__c + ']';
            returnList = ( List<SendgridEvent>)JSON.deserialize( jsonArrayString, List<SendgridEvent>.class);
        } catch ( Exception e) {

            System.debug( 'DEBUG:::exception occurred in MassEmailEventsService.getDeserializedEventData, message:' +
                e.getMessage() + ' - ' + e.getStackTraceString());
        }
        
        return returnList;
    }
 
    /* public methods */

    /**
     * @description: Calculates aggregate counts for webhook events stored in Mass_Email_Events__c field, 
     * counts are stored in aggregate total fields on Mass_Email_Send__c object
     * 
     * @param: newSObjects - Trigger.newMap
     * @param: oldSObjects - Trigger.oldMap
     */
    public void calculateAggregateEventCounts( Map<Id,sObject> newSObjects, Map<Id,sObject> oldSObjects) {
        
        // early return on invalid data
        if ( newSObjects == null || newSObjects.isEmpty() || oldSObjects == null || oldSObjects.isEmpty()) return;

        Set<String> massEmailIdSet = new Set<String>();
        Set<String> schoolIdSet = new Set<String>();
        Set<String> academicYearSet = new Set<String>();
        for ( School_PFS_Assignment__c spa : ( List<School_PFS_Assignment__c>)newSObjects.values()) {

            School_PFS_Assignment__c oldSPA = ( School_PFS_Assignment__c)oldSObjects.get( spa.Id);
            if ( String.isNotBlank( spa.Mass_Email_Events__c) && spa.Mass_Email_Events__c != oldSPA.Mass_Email_Events__c) {

                // Event's are stored as plain text JSON objects, deserialize into corresponding SendgridEvent objects 
                List<SendgridEvent> eventList = MassEmailEventsService.getDeserializedEventData( spa);

                if ( eventList != null) {

                    for ( SendgridEvent evt : eventList) {

                        if ( !massEmailIdSet.contains( evt.mass_email_send_id)) massEmailIdSet.add( evt.mass_email_send_id);
                        if ( !schoolIdSet.contains( evt.school_id)) schoolIdSet.add( evt.school_id);
                        if ( !academicYearSet.contains( evt.academic_year)) academicYearSet.add( evt.academic_year);
                    }
                }
            }
        }

        // must query all related Mass_Email_Send__c records and retrieve comma separated list of original recips
        // from Recipients__c field, then must query all recips SPA's by school and school year to get
        // all applicable event data for each MES record.
        if ( massEmailIdSet.size() > 0) {

            List<Mass_Email_Send__c> massESList = 
                new MassEmailSendDataAccessService().getMassEmailSendByIdSet( massEmailIdSet);

            if ( massESList != null) {

                Set<String> recipIdSet = new Set<String>();
                for ( Mass_Email_Send__c massES : massESList) {

                    if ( String.isNotBlank( massES.Recipients__c)) {

                        List<String> splitList = massES.Recipients__c.split( SchoolMassEmailSendModel.SPLIT_CHAR);
                        for ( String s : splitList) {

                            if ( !recipIdSet.contains( s)) recipIdSet.add( s);
                        }
                    }
                }

                if ( recipIdSet.size() > 0) {

                    List<School_PFS_Assignment__c> spaList = 
                        new SchoolPFSAssignmentDataAccessService().getSchoolPFSByParentAIdSet( 
                            recipIdSet, schoolIdSet, academicYearSet, false);


                    // must iterate back over returned SPA's and grab JSON stored in Mass_Email_Events__c field,
                    // de-dupe by recipient, store counts by MES record, pass counts off for update of MES record(s)
                    // any time one SPA with event data is updated/changed, counts are recalculated for every MES record stored on that SPA
                    if ( spaList != null) {

                        this.updateMassEmailAggCountFields( this.deDupeEventData( spaList));
                    }
                }
            }
        }
    }

    /* private instance methods */

    /**
     * @description: Given a list of SPA's, aggregate and de-dupe all email event data stored in each SPA's Mass_Email_Events__c
     * field by recipient and event, each recipient can only have one event type per MES record
     * 
     * @param: spaList - a valid List of School_PFS_Assignment__c records
     *
     * @return: a valid Map by mass email ID, each containing a map of event data by recip Id
     */
    private Map<String,Map<String,EventDataModel>> deDupeEventData( List<School_PFS_Assignment__c> spaList) {
        Map<String,Map<String,EventDataModel>> returnMap = new Map<String,Map<String,EventDataModel>>();

        // early return on invalid data
        if ( spaList == null || spaList.isEmpty()) return returnMap;

        for ( School_PFS_Assignment__c spa : spaList) {

            if ( String.isNotBlank( spa.Mass_Email_Events__c)) {

                String jsonArrayString = '[' + spa.Mass_Email_Events__c + ']';
                List<SendgridEvent> eventList = 
                    ( List<SendgridEvent>)JSON.deserialize( jsonArrayString, List<SendgridEvent>.class);

                for ( SendgridEvent evt : eventList) {

                    if ( evt.event.equalsIgnoreCase( SchoolMassEmailSendModel.STATUS_OPENED)) {

                        returnMap = this.populateEventDataMap( evt.mass_email_send_id,
                            SchoolMassEmailSendModel.STATUS_OPENED, spa, returnMap);

                    } else if ( evt.event.equalsIgnoreCase( SchoolMassEmailSendModel.STATUS_BOUNCED)) {

                        returnMap = this.populateEventDataMap( evt.mass_email_send_id,
                            SchoolMassEmailSendModel.STATUS_BOUNCED, spa, returnMap);
                    } else if ( evt.event.equalsIgnoreCase( SchoolMassEmailSendModel.STATUS_UNSUB)) {

                        returnMap = this.populateEventDataMap( evt.mass_email_send_id,
                            SchoolMassEmailSendModel.STATUS_UNSUB, spa, returnMap);
                    }
                }
            }
        }

        return returnMap;
    }

    /**
     * @description: Given a Map by mass email Id, each containing a map by with event data by recipient ID,
     * handles summing and actual updating of applicable MES records agg count fields
     * 
     * @param: eventDataByMESIdMap - a valid Map by mass email ID, each containing a map of event data by recip Id
     */
    private void updateMassEmailAggCountFields( Map<String,Map<String,EventDataModel>> eventDataByMESIdMap) {

        // early return on invalid data
        if ( eventDataByMESIdMap == null || eventDataByMESIdMap.isEmpty()) return;

        // query all MES records to be updated, lock records for update
        List<Mass_Email_Send__c> massESList = 
            new MassEmailSendDataAccessService().getMassEmailSendByIdSet( eventDataByMESIdMap.keySet(), true);

        if ( massESList == null) return;

        Boolean needsUpdate = false;
        for ( Mass_Email_Send__c massES : massESList) {

            Integer openedCount = 0;
            Integer bouncedCount = 0;
            Integer unsubCount = 0;
            if ( eventDataByMESIdMap.containsKey( massES.Id)) {

                needsUpdate = true;
                Map<String,EventDataModel> tempMap = eventDataByMESIdMap.get( massES.Id);
                for ( EventDataModel eDModel : tempMap.values()) {

                    if ( eDModel.massESId == massES.Id &&
                            eDModel.eventMap.containsKey( SchoolMassEmailSendModel.STATUS_OPENED) &&
                                eDModel.eventMap.get( SchoolMassEmailSendModel.STATUS_OPENED)) {

                        openedCount++;
                    } else if ( eDModel.massESId == massES.Id &&
                                       eDModel.eventMap.containsKey( SchoolMassEmailSendModel.STATUS_BOUNCED) &&
                                        eDModel.eventMap.get( SchoolMassEmailSendModel.STATUS_BOUNCED)) {

                        bouncedCount++;
                    } else if ( eDModel.massESId == massES.Id &&
                                    eDModel.eventMap.containsKey( SchoolMassEmailSendModel.STATUS_UNSUB) &&
                                        eDModel.eventMap.get( SchoolMassEmailSendModel.STATUS_UNSUB)) {

                        unsubCount++;
                    }
                }

                massES.Opened__c = openedCount;
                massES.Bounced__c = bouncedCount;
                massES.Unsubscribed__c = unsubCount;
            }
        }

        // update MES records with new agg counts
        if ( needsUpdate) {

            try {

                update massESList;
            } catch ( Exception e) {

                System.debug( 'Exception occurred in SchoolPFSAssignmentTriggerHandler.updateMassEmailAggCountFields()' + 
                    ' message: ' + e.getMessage() + ' - ' + e.getStackTraceString());
            }
        }
    }

    // helper method for resuable map logic by status
    private Map<String,Map<String,EventDataModel>> populateEventDataMap( String massESId,
                                                        String status, School_PFS_Assignment__c spa, 
                                                            Map<String,Map<String,EventDataModel>> workingMap) {
        // early return on invalid data
        if ( String.isBlank( massESId) || String.isBlank( status) || spa == null || workingMap == null) return null;

        if ( workingMap.get( massESId) != null) {

            Map<String,EventDataModel> tempMap = workingMap.get( massESId);
            if ( ( tempMap.get( spa.Applicant__r.PFS__r.Parent_A__c) == null) || 
                    ( tempMap.get( spa.Applicant__r.PFS__r.Parent_A__c) != null &&
                        !tempMap.get( spa.Applicant__r.PFS__r.Parent_A__c).eventMap.containsKey( status))) {

                EventDataModel eDModel = new EventDataModel( spa.Applicant__r.PFS__r.Parent_A__c, massESId);
                eDModel.eventMap.put( status, true);
                tempMap.put( spa.Applicant__r.PFS__r.Parent_A__c, eDModel);
            }
        } else {

            Map<String,EventDataModel> tempMap = new Map<String,EventDataModel>();
            EventDataModel eDModel = new EventDataModel( spa.Applicant__r.PFS__r.Parent_A__c, massESId);
            eDModel.eventMap.put( status, true);
            tempMap.put( spa.Applicant__r.PFS__r.Parent_A__c, eDModel);
            workingMap.put( massESId, tempMap);
        }

        return workingMap;
    }

    /* Model wrapper classes */

    // model class to combine Event and Contact data
    public class EventDataModel {

        public String recipId { get; set; }
        public String massESId { get; set; }
        public Map<String,Boolean> eventMap { get; set; }

        public EventDataModel( String recipId, String massESId) {

            this.recipId = recipId;
            this.massESId = massESId;
            this.eventMap = new Map<String,Boolean>();
        }
    }

}