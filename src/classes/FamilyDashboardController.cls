/**
* @ FamilyDashboardController.cls
* @date 6/25/2013
* @author  Charles Howard for Exponent Partners and Drew Piston for Exponent Partners
* @description Page controller for the FamilyDashboard page
*/

/*
* Family Portal Dashboard NAIS-483 - Drew Piston
*
* Copyright Exponent Partners 2013
*
* Tab/link in the nav to the main dashboard which is also the page they are taken to when they log in.
* This assumes that the data from prior years has been migrated in an accessible format.
* The links to prior year PFS's will be available on the Dashboard.
* Once a PFS has been marked as having a fee credit, that should be visible in the portal on the Dashboard
* The Payment Due reminder will contain a link to the payment flow, and is based on a field on the PFS
*
* The Family Portal should provide a dashboard as the central location for information on the application process and for driving families to the correct location
* The families should be able to access a printable PDF of their application from the prior year (assuming they applied that year)
* System should display a prominent visual progress indicator to the family as they progress through the application
* It should be obvious to the family in their portal that they have received an application fee credit
* Dashboard should show when a payment is due.
* The dashboard should display PFS deadlines if the school has opted in to PFS notifications
*/


public without sharing class FamilyDashboardController {

    @testVisible
    private static final String SUCCESS_STATUS = 'success';
    @testVisible
    private static final String FAILED_STATUS = 'failed';
    private static final String DEADLINE_TYPE_HARD = AnnualSettingHelper.DEADLINE_TYPE_HARD;
    private static final String DEADLINE_TYPE_SOFT = AnnualSettingHelper.DEADLINE_TYPE_SOFT;

    /*Initialization*/
    public FamilyDashboardController(FamilyTemplateController c) {
        // Return early if it is the guest user. We will end up redirecting them to the login page.
        if (CurrentUser.isGuest()) { return; }

        this.controller = c;
        this.pfsRecord = this.controller.pfsRecord;
        this.pfsId = this.pfsRecord != null ? this.pfsRecord.Id : null;
        currentAcademicYear = this.controller.academicYear;

        // [CH] NAIS-2335 Added to provide naming for new columns
        // Split the academic year name into individual years
        List<String> yearsList = this.currentAcademicYear.Name.split('-');

        // Put the current and previous year values into a Set for querying
        yearValueList = new List<String>{};
        if(yearsList != null){
            Integer yearInt = Integer.valueOf(yearsList[0]);
            // [CH] NAIS-1607 This was throwing off the current and previous year
            //                     values, and doesn't seem to be necessary
            yearValueList.add(String.valueOf(yearInt-1));
            yearValueList.add(String.valueOf(yearInt-2));
        }
        // Look up the current user record's related Contact record
        userRecord = [
            SELECT Id, Name, ContactId, Contact.LastName, Contact.Name, Contact.MailingStreet, Contact.MailingCity,
                Contact.MailingState, Contact.MailingPostalCode, Contact.MailingCountry, Contact.Gender__c,
                Contact.Birthdate, Contact.Email, Contact.HomePhone, Contact.MobilePhone, Contact.Preferred_Phone__c,
                Contact.FirstName, Contact.Middle_Name__c, Contact.Suffix__c, Contact.Salutation, Contact.Phone,
                Contact.Security_Question_1__c, Contact.Security_Question_2__c, Contact.Security_Question_3__c,
                Contact.Security_Answer_1__c, Contact.Security_Answer_2__c, Contact.Security_Answer_3__c,
                LanguageLocaleKey, ProfileId, IntegrationSource__c, Early_Access__c
            FROM User
            WHERE Id = :UserInfo.getUserId()];
        // [GH] NAIS-2174 Start: Checks if userRecord has contactId, field used to determine which query will be called for the PFS History Section
        if(userRecord.ContactId != null){
            userHasContId = true;
            pfsContact = new Contact(
            Id = userRecord.ContactId,
            Security_Question_1__c = userRecord.Contact.Security_Question_1__c,
            Security_Question_2__c = userRecord.Contact.Security_Question_2__c,
            Security_Question_3__c = userRecord.Contact.Security_Question_3__c,
            Security_Answer_1__c = userRecord.Contact.Security_Answer_1__c,
            Security_Answer_2__c = userRecord.Contact.Security_Answer_2__c,
            Security_Answer_3__c = userRecord.Contact.Security_Answer_3__c);
            
            questionAreAnswered = questionAnswerFieldsPopulated(pfsContact);
            
            if (!questionAreAnswered) {
                secQuestionPicklist1 = PickListService.getSelectOptionsFromField(Contact.Security_Question_1__c);
                secQuestionPicklist2 = PickListService.getSelectOptionsFromField(Contact.Security_Question_2__c);
                secQuestionPicklist3 = PickListService.getSelectOptionsFromField(Contact.Security_Question_3__c);
            }
        } else {
            userHasContId = false;
        }
        schPFSWrappers = new List<SchoolPFSWrapper>();
        pfsHistories = new List<PFSHistoryWrapper>();
    }//End-Constructor

    public static FamilyTemplateController.config loadPFS(String pfsId, String academicYearId)
    {
        PFS__c pfsRecordTmp;
        //Academic_Year__c currentAcademicYear = [select Id, Name, PFS_Workbook_Url__c from Academic_Year__c where Id = :academicYearId];
        User currentUser = [Select Id, ContactId, ProfileId from User where Id = :UserInfo.getUserId()];

        // use applciation utils to get pfsrecord -- this gives us all fields, plus some related child records (that we probably don't need)
        // if it is a sys admin or call center user, get it based on the pfsId passed in
        if (GlobalVariables.isSysAdminUser(currentUser) || GlobalVariables.isCallCenterUser(currentUser) || GlobalVariables.isDatabankUser(currentUser)){
            pfsRecordTmp = ApplicationUtils.queryPFSRecord(pfsId, null, null);
        // if this not a sys admin or call center user, get record based on con id and acad year
        } else {
            pfsRecordTmp = ApplicationUtils.queryPFSRecord(null, currentUser.ContactId, academicYearId);
        }

        return new FamilyTemplateController.config(pfsRecordTmp, null);
    }//End:loadPFS

    // this happens on pageload.  dml may happen here which is why it's not in the constructor
    public PageReference pageLoad() {
        if (CurrentUser.isGuest()) {
            return getLoginPage();
        }
        processEarlyAccess();

        boolean reloadPage = false;
        // pass in "isSchoolPortal" boolean param of false
        if (!GlobalVariables.userHasAcceptedTermsAndConditions(false)){
            return Page.FamilyTermsAndConditions;
        }

        schoolIds = new Set<Id>(); // [DP] 07.13.2015 moving instantiation here to avoid null reference error

        // If there's not a current PFS record for the user and Academic Year, generate one and insert it
        // added check to prevent non-family users from getting PFSs created [dp] NAIS-1512
        if (userRecord.ProfileId == GlobalVariables.familyPortalProfileId && (this.pfsRecord == null || this.pfsRecord.Id == null)){
            this.pfsRecord = ApplicationUtils.generateBlankPFS(this.controller.academicYearId, userRecord);
            reloadPage = true;
            // requery with all of the fields we need
            this.pfsRecord = ApplicationUtils.queryPFSRecord(this.pfsRecord.Id, null, null);
        }
        // otherwise, get the school pfs assignments for the existing one
        else{
            for (School_PFS_Assignment__c spfsa : querySchoolPFSs()){
                schPFSWrappers.add(new SchoolPFSWrapper(spfsa));
                schoolIds.add(spfsa.School__c);
            }

            renderFamilyReport = false;
            // need to get annual settings records to set the deadline for each wrapper
            if (!schoolIds.isEmpty()){
                Map<Id, Annual_Setting__c> schoolIdToAnnualSettings = new Map<Id, Annual_Setting__c>();

                // [CH] NAIS-2335, 4.1.2015 Added deadline fields to query
                for (Annual_Setting__c annSet : [Select Id, PFS_Deadline_Type__c, School__c, PFS_New__c, PFS_Returning__c, Academic_Year__c, Allow_families_to_receive_Family_Report__c,
                                                        Current_Year_Tax_Documents_New__c, Prior_Year_Tax_Documents_New__c, Current_Year_Tax_Documents_Returning__c, Prior_Year_Tax_Documents_Returning__c
                                                    from Annual_Setting__c WHERE School__c in :schoolIds AND Academic_Year__c = :this.controller.academicYearId]){

                    schoolIdToAnnualSettings.put(annSet.School__c, annSet);
                    if (annSet.Allow_families_to_receive_Family_Report__c == 'Yes'){
                        renderFamilyReport = true;
                    }
                }

                for (SchoolPFSWrapper wrapper : schPFSWrappers){
                    Boolean pfsSubmitted = this.pfsRecord.PFS_Status__c == 'Application Submitted';
                    wrapper.setDeadlineValues(schoolIdToAnnualSettings.get(wrapper.spa.School__c), pfsSubmitted);
                }
            }
        }

        // query other pfss for the PFS History section
        for (PFS__c p : queryOtherPFSs()){
            pfsHistories.add(new PFSHistoryWrapper(p));
        }

        // instantiate an ApplicationUtils class that we will use for the main label and other funtionality
        appUtils = new ApplicationUtils(userRecord.LanguageLocaleKey, this.pfsRecord, this.pfsRecord.systemLastScreenParam__c);

        // pfsId used in VF
        pfsId = this.pfsRecord.Id;
        this.controller.pfsId = this.pfsRecord.Id;
        this.controller.pfsRecord = this.pfsRecord;
        // [SL] NAIS-1031: BEGIN get application fee from opportunity if exists, otherwise from the application fee settings
        fee = 0;
        if ((this.pfsRecord != null) && (this.pfsRecord.Id != null)) {
            Opportunity appFeeOpportunity = null;
            for (Opportunity opp : [SELECT Id, Amount, Net_Amount_Due__c
                                        FROM Opportunity
                                        WHERE RecordTypeId = :RecordTypes.pfsApplicationFeeOpportunityTypeId
                                        AND PFS__c = :this.pfsRecord.Id
                                        ORDER BY CreatedDate DESC LIMIT 1])    {
                appFeeOpportunity = opp;
            }
            // an opportunity exists -- use the amount from there
            if (appFeeOpportunity != null) {
                // [DP] 07.06.2015 NAIS-1933 now getting fee amount from generic method
                fee = FinanceAction.getAmountTotal(appFeeOpportunity, null, false);
                fee.setScale(2);
            }

            // the opportunity doesn't exist yet -- get the amount from the application fee settings
            else {
                // return discounted fee if they are applying to ONLY KS school, otherwise full fee
                Boolean onlyKS = FinanceAction.isKSOnlyPFS(schoolIds);
                String acadYearName = GlobalVariables.getAcademicYear(this.controller.academicYearId).Name;

                // [DP] 07.06.2015 NAIS-1933 now getting fee amount from generic method
                fee = FinanceAction.getAmountTotal(null, acadYearName, onlyKS);
            }
        }
        // [SL] NAIS-1031: END

        // [SL] NAIS-1246: Query latest auto-payment TLI for receipt link BEGIN

        // [SL] NAIS-1246: get the most recent TLI for the receipt link
        lastPaymentTransactionLineItemId = null;
        for (Transaction_Line_Item__c tli : [SELECT Id
                                                FROM Transaction_Line_Item__c
                                                WHERE Opportunity__r.PFS__c = :this.pfsRecord.Id
                                                AND RecordTypeId = :RecordTypes.paymentAutoTransactionTypeId
                                                AND Transaction_Status__c = 'Posted'
                                                ORDER BY Transaction_Date__c DESC, CreatedDate DESC
                                                LIMIT 1]) {

            lastPaymentTransactionLineItemId = tli.Id;
        }
        // NAIS-1246: END
        PageReference newPage = Page.familydashboard;
        newPage.setRedirect(true);
        return (reloadPage?newPage:null);//To avoid exception when the PFS is created for the first time, the page is reloaded.
    }
    /*End Initialization*/

    /*Properties*/
    private FamilyTemplateController controller;
    public PFS__c pfsRecord {get; set;}
    public String pfsId {get; set;}
    public Academic_Year__c currentAcademicYear {get; set;}
    public List<String> yearValueList {get; set;}
    public User userRecord {get; set;}
    public Boolean renderFamilyReport {get; set;}
    public Boolean userHasContId {get; set;}// [GH] NAIS-2174: Used for TCN agent Family View of family PFS
    public Contact pfsContact {get; set;}
    public Boolean questionAreAnswered {get; set;}
    public List<SelectOption> secQuestionPicklist1 {get; set;}
    public List<SelectOption> secQuestionPicklist2 {get; set;}
    public List<SelectOption> secQuestionPicklist3 {get; set;}
    public Decimal fee {get; set;} // [SL] NAIS-1031: set this propery during initialization

    // the pfs that gets created if one doesn't exist
    public PFS__c newPFS {get; set;}

    // used for general utility methods involving the application
    public ApplicationUtils appUtils {get; set;}

    // list of wrappers for "Schools you've applied to" section
    public List<SchoolPFSWrapper> schPFSWrappers {get; set;}

    // list of wrappers for PFS History section
    public List<PFSHistoryWrapper> pfsHistories {get; set;}

    // PREPARE = prepare to apply, COMPLETING = complete your pfs, PAY = pay for and submit, DOCS = manage required docs
    public Enum STATUSENUM {PREPARE, COMPLETING, PAY, DOCS}

    // [DP] 07.07.2015 NAIS-1933 used to capture all school ids for this PFS
    public Set<Id> schoolIds {get; set;}

    // [SL] NAIS-1246: most recent auto payment TLI, for use in displaying the receipt
    public Id lastPaymentTransactionLineItemId {get; set;}

    // calc the stage based on pfs.pfs status and application completion
    public String getPFSStage(){
        STATUSENUM pfsStatus;

        // if we created a new PFS on pageload then we are in the PREPARE stage
        if (newPFS != null){
            pfsStatus = STATUSENUM.PREPARE;
        } else {

            // if status is null or not begun we are in the PREPARE stage
            if (this.pfsRecord.PFS_Status__c == null || this.pfsRecord.PFS_Status__c == 'Application Not Begun'){
                pfsStatus = STATUSENUM.PREPARE;

            // if status is In Progress...
            } else if (this.pfsRecord.PFS_Status__c == 'Application In Progress'){
                //... if all sections are done we are NO LONGER in PAY -- see note three lines below, changed
                if(getAllSectionsDone()){
                    //pfsStatus = STATUSENUM.PAY;
                    // Shanthi says Sara wants this to be COMPLETING, not PAY, and the text underneath the image will change
                    pfsStatus = STATUSENUM.COMPLETING;
                //... if all sections are NOT done we are in COMPLETING
                } else {
                    pfsStatus = STATUSENUM.COMPLETING;
                }
            // if status is Submitted...
            } else if (this.pfsRecord.PFS_Status__c == 'Application Submitted'){
                //... if payment is done we are in DOCS
                if (getIsPaid()){
                    pfsStatus = STATUSENUM.DOCS;
                //... if payment is NOT done we are in PAY
                } else {
                    pfsStatus = STATUSENUM.PAY;
                }
            }
        }

        return pfsStatus.Name();
    }

    public boolean getFeeIsWaived(){
        Boolean waived = false;
        if (this.pfsRecord.Fee_Waived__c != null && this.pfsRecord.Fee_Waived__c == 'Yes'){
            waived = true;
        }

        // [DP] NAIS-1583 -- if the fee waiver was assigned before it's been submitted, it still counts as waived even though the PFS won't be marked as waived
        if (getWaiverIsAssigned()){
            waived = true;
        }
        return waived;
    }

    public List<Application_Fee_Waiver__c> thisYearsWaivers{
        get {
            if (thisYearsWaivers == null){
                thisYearsWaivers = [Select Id, Status__c from Application_Fee_Waiver__c
                                                    where Contact__C = :this.pfsRecord.Parent_A__c
                                                    AND Academic_Year__r.Name = :this.pfsRecord.Academic_Year_Picklist__c
                                                    AND Status__c != 'Declined'];
            }
            return thisYearsWaivers;
        }
        private set{}
    }

    /**
    * @description accessor method for getting knowledge articles to display on the dashboard
    * 
    * @return Boolean on whether all business Farms are complete or not.
    */
    public List<KnowledgeDataAccessService.KnowledgeWrapper> getFaqs() {
        return new KnowledgeDataAccessService().getTopFamilyKnowledgeArticlesAbbreviated( 5, false, true);
    }

    /**
    * @description Method for checking to see is Business Farm Fields are complete. 
    * 
    * @param businessFarms - a list of business fam objects
    * 
    * @return Boolean on whether all business Farms are complete or not.
    */
    public Boolean areBusinessFarmsComplete( List<Business_Farm__c> businessFarms) {
        Boolean result = true;

        for( Business_Farm__c businessFarm : businessFarms ) {
            if( !businessFarm.statBusinessInformation__c || !businessFarm.statBusinessIncome__c || !businessFarm.statBusinessExpenses__c || !businessFarm.statBusinessAssets__c) {
                result = false;
            }
        }

        return result;
    }

    /**
    * @description Method to get family Portal outstand items. This will give you a list of strings of items that are not complete. 
    * for a given a picklist field.
    * 
    * @return List of text strings of the items not compelte. 
    */
    public List<String> getNextOutStandingItems() {
        final String BUSINESS_FARM = 'Business/Farm';
        final String ADDITIONAL_QUESTIONS = 'AdditionalQuestions';
        final String MONTHLY_INCOME_EXPESNSES = 'MonthlyIncomeExpenses';

        List<String> incompleteItems = new List<String>();
        List<OutstandingItemsWrapper> outstandingItems = new List<OutstandingItemsWrapper>();
        Set<String> valuesToIgnore = new Set<String>{'BusinessInformation', 'BusinessAssets', 'BusinessIncome', 'BusinessExpenses'};
        
        // only gets called with there is an outstanding item. 
        // Get next outstanding item. 
        Map<String, FamilyAppSettings__c> screenSettings = appUtils.screenSettingsMap;
        Boolean businessFarms = areBusinessFarmsComplete( appUtils.pfs.Business_Farms__r);
        if( !businessFarms && this.pfsRecord.Own_Business_or_Farm__c == 'Yes') {
            outstandingItems.add( new OutstandingItemsWrapper( BUSINESS_FARM)); 
        }
        
        //appUtils.labels - returns a Question_Label__c object
        for( FamilyAppSettings__c family : screenSettings.values()) {
            if( family.Status_Field__c != null) {
                
                if( PfsProcessService.Instance.isSectionHidden(pfsRecord.Submission_Process__c, family.Name)) {
                    continue;
                }
                
                if( pfsRecord.Own_Business_or_Farm__c != 'Yes' && family.Name == 'BusinessSummary') {
                    continue;
                }
                
                if( family.Name == ADDITIONAL_QUESTIONS && !appUtils.getRequiredAdditionalQuestions()) {
                    continue;
                }

                if( family.Name == MONTHLY_INCOME_EXPESNSES && !appUtils.isMIERequired) {
                    continue;
                }

                Boolean isComplete = Boolean.valueOf( this.pfsRecord.get( family.Status_Field__c));
                if( !isComplete && !valuesToIgnore.contains( family.Name)) { // see which child is not complete.
                    String myLabel = String.valueOf( appUtils.labels.get( family.Main_Label_Field__c) );
                    if( family.Sub_Label_Field__c != null) {
                        String labelValue = String.valueOf( appUtils.labels.get( family.Sub_Label_Field__c));
                        outstandingItems.add( new OutstandingItemsWrapper( labelValue)); 
                    }
                }
            }
        }

        // sort so they are always in order
        outstandingItems.sort();

        // now put it all back in a list for simplicity. 
        for( OutstandingItemsWrapper sortedItem : outstandingItems) {
            incompleteItems.add( sortedItem.label);
        }

        return incompleteItems;
    }
    
    public Boolean getWaiverIsAssigned(){
        Boolean waiverAssigned = false;

        for (Application_Fee_Waiver__c afw : thisYearsWaivers){
            if (afw.Status__c == 'Assigned'){
                waiverAssigned = true;
            }
        }
        return waiverAssigned;
    }

    //public Boolean waiverRequested;
    public Boolean getWaiverRequestedButNotYetWaived(){
        Boolean waiverRequested = false;
        Boolean waiverAssigned = false;

        for (Application_Fee_Waiver__c afw : thisYearsWaivers){
            if (afw.Status__c == 'Assigned'){
                waiverAssigned = true;
            } else if (afw.Status__c == 'Pending Approval' || afw.Status__c == null){
                waiverRequested = true;
            }
        }

        if (waiverAssigned){
            waiverRequested = false;
        }
        return waiverRequested;
    }

    // get name of last section completed
    public String getLastSectionName(){

        if(this.pfsRecord.systemLastScreenParam__c == null){return null;}

        FamilyAppSettings__c famAppSettings = appUtils.ScreenSettingsMap.get(this.pfsRecord.systemLastScreenParam__c);
        if(famAppSettings == null || famAppSettings.Sub_Label_Field__c == null){return null;}
        if(appUtils.labels.get(famAppSettings.Sub_Label_Field__c) == null){return null;}

        return String.valueOf(appUtils.labels.get(famAppSettings.Sub_Label_Field__c));

        //return String.valueOf(appUtils.labels.get(appUtils.ScreenSettingsMap.get(pfsRecord.systemLastScreenParam__c).Sub_Label_Field__c));
    }

    // get url for last section completed
    public PageReference getLastSectionURL(){

        if(this.pfsRecord.systemLastScreenParam__c == null){
            this.pfsRecord.systemLastScreenParam__c = 'HouseholdInformation';
        }

        PageReference returnTarget = Page.FamilyApplicationMain;
        returnTarget.getParameters().put('id', this.pfsRecord.Id);
        returnTarget.getParameters().put('screen', this.pfsRecord.systemLastScreenParam__c);
        returnTarget.setRedirect(true);

        return returnTarget;
    }

    // loop through status boolean fields and return "true" if they are all completed
    public Boolean getAllSectionsDone(){
        return appUtils.applicationIsComplete;
    }

    public Boolean getIsPaid(){
        return (this.pfsRecord.Payment_Status__c == 'Paid in Full');
    }

    public String getAcadYearString(){
        return GlobalVariables.getAcademicYearByName(this.pfsRecord.Academic_Year_Picklist__c).Id;
    }

    //NAIS-1928 Start
    /*public List<Global_Message__c> getGlobalMessages(){
        Id currentUser = UserInfo.getUserId();
        //List<Global_Message__c> globalMessages = [Select Id, Name, Message__c from Global_Message__c where Display_End_Date_Time__c >= :System.now() AND Display_Start_Date_Time__c <= :System.now() AND Portal__c INCLUDES ('Family Portal')];
        List<Global_Message__c> globalMessages = GlobalVariables.GlobalMessages(currentUser, true);
        return globalMessages;
    }*/
    //NAIS-1928 End

    private list<IntegrationSource__c> integrationSources;
    public list<IntegrationSource__c> getIntegrationSources(){
          if(integrationSources == null){
              integrationSources = new list<IntegrationSource__c>();
              if(userRecord.IntegrationSource__c != null)
                  for(string each:userRecord.IntegrationSource__c.split(';'))
                      integrationSources.add(IntegrationSource__c.getInstance(each));
        }
        return integrationSources;
    }

    private IntegrationSource__c integrationSource;
    public IntegrationSource__c  getIntegrationSource(){
        if(integrationSource == null){
            string source = ApexPages.currentPage().getParameters().get('source');
            if(!string.isEmpty(source))
               integrationSource = IntegrationSource__c.getInstance(source);


        }
        return integrationSource;
    }

    /*End Properties*/

    /*Action Methods*/
    public void applyIntegrationSource(){
        string integrationSourceFromCookie = ApexPages.currentPage().getParameters().get('integrationSource');
        //system.debug('**'+integrationSourceFromCookie);
           if(!String.isEmpty(integrationSourceFromCookie)){
            if(userRecord.IntegrationSource__c != null){
                if(!userRecord.IntegrationSource__c.contains(integrationSourceFromCookie)){
                    userRecord.IntegrationSource__c = userRecord.IntegrationSource__c +integrationSourceFromCookie;
                }
            }
            else{
                userRecord.IntegrationSource__c = integrationSourceFromCookie;
            }

                update userRecord;
            integrationSources = null;
        }

    }

    public string selectedIntegrationSource {get;set;}
    public PageReference kickoffIntegration(){
        system.debug(selectedIntegrationSource);
        selectedIntegrationSource = ApexPages.currentPage().getParameters().get('selectedIntegrationSource');
        system.debug(selectedIntegrationSource);
        system.debug(Network.getLoginUrl('familyportal'));
        system.debug(Site.getbasesecureurl());
        return new PageReference(Auth.AuthConfiguration.getAuthProviderSsoUrl(null, '/FamilyDashboard', selectedIntegrationSource));
    }

    public string getToken(){
        string t = Auth.AuthToken.getAccessToken('0SOJ00000008OKV','Open ID Connect');
        system.debug('***ACCESS TOKEN****'+t);
        return t;
    }

    public string getTest(){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://staging.ravenna-hub.com/api/v1/applicants/?auto_group=gender');
        req.setMethod('GET');
        req.setHeader('Authorization','Bearer 8e7ffff86c74403cc00c2bc5838ca67d6390a654');
        //req.setBody(body);
        HttpResponse res;
         res = h.send(req);
         system.debug('***'+res);
        return 'test';
    }
    // TODO get submit URL
    public PageReference submit(){
        PageReference pr = Page.FamilyApplicationMain;
        pr.getParameters().put('id', pfsId);
        pr.getParameters().put('academicyearid', this.controller.academicYearId);
        pr.getParameters().put('screen', 'PFSSubmit');
        return pr;
    }

    public PageReference submitPFS(){

        // [CH] This has moved to the Submit / Confirm screen
        //pfsRecord.PFS_Status__c = 'Application Submitted';
        //update pfsRecord;

        PageReference submitPage = Page.FamilyApplicationMain;
        submitPage.getParameters().put('id', this.pfsRecord.Id);
        submitPage.getParameters().put('screen', 'PFSSubmit');
        submitPage.setRedirect(true);
        return submitPage;
    }

    // TODO get pay URL
    public PageReference payNow(){
        return submit();
    }


    /*End Action Methods*/

    /*Helper Methods*/

    // get other PFSs for PFS History
    public List<PFS__c> queryOtherPFSs(){
        // [GH] NAIS-2174 START: if userHasContId = true will query PFS based on userRecord.contactId. If False will query PFS based on current pfsRecord's Parent A Id.
        if(userHasContId){
            return [Select Id,
                    Name,
                    Academic_Year_Picklist__c
                    From PFS__c where Parent_A__c = :userRecord.ContactID
                    //AND Academic_Year__c != :this.controller.academicYearId
                    ];
        } else {
            return [Select Id,
                    Name,
                    Academic_Year_Picklist__c
                    From PFS__c where Parent_A__c = :this.pfsRecord.Parent_A__c
                    //AND Academic_Year__c != :this.controller.academicYearId
                    ];
        }
        //NAIS-2174 END
    }

    // get School PFSs for Schools You've Applied To section
    public List<School_PFS_Assignment__c> querySchoolPFSs(){
        return ApplicationUtils.getAllSchools(this.pfsRecord.Id);
    }
    
    /**
     *  @description Remote action for updating security questions and answers of Pfs contact
     *  @param contactToUpdate sObject record with populated question and answer fields
     */
    @RemoteAction
    public static String saveSecurityQuestions(Contact contactToUpdate){
        if (questionAnswerFieldsPopulated(contactToUpdate)) {
            try {
                update contactToUpdate;
    
                return SUCCESS_STATUS;
            } catch (Exception ex) {
                return ex.getMessage();
            }
        }
    
        return FAILED_STATUS;
    }
    
    /**
     *  @description Validates if a contact record exists and has all necessary security fields populated
     *  @param contact sObject record with populated question and answer fields
     */
    @testVisible
    private static Boolean questionAnswerFieldsPopulated(Contact contact) {
        if (contact != null && contact.Id != null && !String.isBlank(contact.Security_Question_1__c)
            && !String.isBlank(contact.Security_Question_2__c) && !String.isBlank(contact.Security_Question_3__c)
            && !String.isBlank(contact.Security_Answer_1__c) && !String.isBlank(contact.Security_Answer_2__c)
            && !String.isBlank(contact.Security_Answer_3__c)) {
            return true;
        }
        return false;
    }
    
    /**
     * @description Returns whether the family dashboard should render
     *              the request for security questions or not. If the
     *              user is not a family portal user then do not render.
     *              This allows TCN/CST to use Family View on the PFS.
     * @return True if the current user is a family portal user otherwise
     *         false.
     */
    public Boolean getRenderSecurityQuestions() {
        return GlobalVariables.isFamilyPortalUser();
    }

    /*End Helper Methods*/

    /*Wrapper Classes*/
    /* Wrapper class for outstanding items, implmenets comparable so that
       we can sort and get all the wrappers in a predicable order */
    public class OutstandingItemsWrapper implements Comparable {
        public String label { get; set; }
        public Integer value { get; set; }

        public OutstandingItemsWrapper( String label) {
            this.label = label;
            this.value = getStartingValue( label);
        }

        public Integer compareTo( Object compareTo) {
            OutstandingItemsWrapper compareToRating = (OutstandingItemsWrapper)compareTo;
            
            Integer returnVal = 0;
            //check for nulls first incase the value is not a number. 
            if ( this.value == null) return 1;
            if ( compareToRating.value == null) return -1;

            // if no nulls sort by value. 
            if( this.value > compareToRating.value ) {
                returnVal = 1;
            } else if (this.value < compareToRating.value) {
                returnVal = -1;
            }

            return returnVal;
        }


        private Integer getStartingValue( String value) {
            if ( String.isNotBlank( value)) {
                try
                {
                    return Integer.valueOf( value.splitByCharacterType()[0]);
                }
                catch (TypeException typeEx) { /*NaN*/ }
            }
            return null;
        }
    }

    public class SchoolPFSWrapper{
        public School_PFS_Assignment__c spa {get; set;}
        public String applicantName {get; set;}
        public String schoolName {get; set;}
        public String sssCode {get; set;}
        public Boolean isLocked {get; set;}
        public Boolean isSubscribed {get; set;}
        public Boolean isReturning {get; set;}

        //public Annual_Setting__c annSet;
        public Boolean deadlinePassed {get; set;}
        public Date newDeadline {get; set;}
        public Date returningDeadline {get; set;}

        // [CH] NAIS-2335, 3.31.2015 Adding to include document deadline dates
        public Date newCurrentDocDeadline {get; set;}
        public Date newPreviousDocDeadline {get; set;}
        public Date returningCurrentDocDeadline {get; set;}
        public Date returningPreviousDocDeadline {get; set;}

        public SchoolPFSWrapper(School_PFS_Assignment__c spaParam){
            spa = spaParam;
            sssCode = spa.School__r.SSS_School_Code__c;
            schoolName = spa.School__r.Name;
            applicantName = spa.Applicant__r.Contact__r.Name;

            //isSubscribed = spa.School__r.SSS_Subscriber_Status__c == GlobalVariables.activeSSSStatuses;
            isSubscribed = spa.School__r.SSS_Subscriber_Status__c != null && GlobalVariables.activeSSSStatuses.contains(spa.School__r.SSS_Subscriber_Status__c);
            isLocked = spa.Family_May_Submit_Updates__c == 'No';
            deadlinePassed = false;
        }

        /**
         * @description Checks deadlines values in annual settings, assign appropriate values for page variables
         *              and highlights the row in case of 'Hard Deadline'
         * @param annSet Annual Setting.
         * @param pfsSubmitted PFS record.
         */
        public void setDeadlineValues(Annual_Setting__c annSet, Boolean pfsSubmitted){
            isReturning = spa.Student_Folder__r.New_Returning__c == 'Returning';
            if (annSet == null){
                deadlinePassed = false;
            } else {
                if (annSet.PFS_Deadline_Type__c == DEADLINE_TYPE_HARD
                        || annSet.PFS_Deadline_Type__c == DEADLINE_TYPE_SOFT) {
                    newDeadline = annSet.PFS_New__c;
                    returningDeadline = annSet.PFS_Returning__c;
                    Date deadlineToUse = isReturning ? returningDeadline : newDeadline;
                    deadlinePassed = !pfsSubmitted && annSet.PFS_Deadline_Type__c == DEADLINE_TYPE_HARD
                        && deadlineToUse < System.today();
                }

                // [CH] NAIS-2335, 3.31.2015 Adding to include document deadline dates
                newCurrentDocDeadline = annSet.Current_Year_Tax_Documents_New__c;
                newPreviousDocDeadline = annSet.Prior_Year_Tax_Documents_New__c;
                returningCurrentDocDeadline = annSet.Current_Year_Tax_Documents_Returning__c;
                returningPreviousDocDeadline = annSet.Prior_Year_Tax_Documents_Returning__c;
            }
        }
    }

    public class PFSHistoryWrapper{
        public PFS__c pfs {get; set;}
        public String pfsName {get; set;}
        public PageReference pfsPrintURL {get; set;}

        public PFSHistoryWrapper(PFS__c pfsParam){
            PageReference printPage = Page.FamilyAppViewPFSPDF;
            pfs = pfsParam;
            pfsName = 'PFS ' + pfs.Academic_Year_Picklist__c;

            printPage.getParameters().put('id', pfs.id);
            pfsPrintURL = printPage;
        }
    }
    /*End Wrapper Classes*/

    @testVisible
    private void processEarlyAccess() {
        AccessService.Instance.grantEarlyAccess();
    }

    /**
     * @description Create the page reference to the FamilyLogin page and populate it with current page parameters.
     */
    private PageReference getLoginPage() {
        PageReference login = Page.FamilyLogin;
        login.getParameters().putAll(ApexPages.currentPage().getParameters());
        login.setRedirect(true);
        return login;
    }
}