/**
 * @description This class is responsible for querying KnowledgeArticle records.
 */
public with sharing class KnowledgeArticleSelector extends fflib_SObjectSelector
{

    @testVisible private static final String ARTICLE_NUMBERS_PARAM  = 'articleNumbers';

    /**
     * @description Queries all Knowledge Articles.
     * @return A list of KnowledgeArticle records.
     */
    public List<KnowledgeArticle> selectAll()
    {
        return Database.query(newQueryFactory().toSOQL());
    }

    /**
     * @description Selects KnowledgeArticle records by ArticleNumber.
     * @param articleNumbers The Article Numbers to query for KnowledgeArticle records.
     * @return A list of KnowledgeArticle records.
     * @throws An ArgumentNullException if articleNumbers is null.
     */
    public List<KnowledgeArticle> selectByArticleNumber(Set<String> articleNumbers)
    {
        ArgumentNullException.throwIfNull(articleNumbers, ARTICLE_NUMBERS_PARAM);

        assertIsAccessible();

        String query = newQueryFactory(true)
            .setCondition('ArticleNumber IN :articleNumbers')
            .toSOQL();

        return Database.query(query);
    }

    private Schema.SObjectType getSObjectType()
    {
        return KnowledgeArticle.getSObjectType();
    }

    private List<Schema.SObjectField> getSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
            KnowledgeArticle.ArticleNumber
        };
    }

    /**
     * @description Creates a new instance of the KnowledgeArticleSelector.
     * @return An instance of KnowledgeArticleSelector.
     */
    public static KnowledgeArticleSelector newInstance()
    {
        return new KnowledgeArticleSelector();
    }
    
    /**
     * @description Singleton property ensuring external sources do not
     *              instantiate this directly.
     */
    public static KnowledgeArticleSelector Instance
    {
        get 
        {
            if (Instance == null)
            {
                Instance = new KnowledgeArticleSelector();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to enforce singleton access
     *              via the Instance property.
     */
    private KnowledgeArticleSelector() {}
}