/**
* @ FamilyAppMainController.cls
* @date 6/13/2013
* @author  Charles Howard for Exponent Partners
* @description Common controller for page components that are
*  part of the Family Application process
*/
public virtual class FamilyAppCompController
{

    @testVisible private static final String APPLICANT_INFORMATION_SCREEN = 'ApplicantInformation';

    /**
     * @description Placeholder constructor.
     */
    public FamilyAppCompController() { }

    /*Properties*/
    // Main properties passed in by pages that use components
    public PFS__c pfs {get; set;}
    public FamilyPortalHiddenFields formHiddenFields { get; set; }

    // NAIS-2494 START
    public Business_Farm__c bf {get; set;}
    // NAIS-2494 END

    public ApplicationUtils appUtils { get; set; }
    public Dependents__c dependentItem { get; set; }

    // Value holders for params passed in by Apex Param tags
    public String parentDelete { get; set; }
    public User loggedUser {get; set;}//SFP-10, [G.S]
    public boolean showPopup  {get; set; }
    public String taxableWarningMessage {get;set;}
    public PfsProcessService.RecalculateResult EZRecalculateResult{ get; set; }

    public Boolean showPriorColumn {
        get {
            if(showPriorColumn == null){
                showPriorColumn = false;
                if(priorYearPFS != null && priorYearPFS.Id != null){
                    showPriorColumn = true;
                }
            }
            return showPriorColumn;
        }
        set;
    }

    public Boolean showReceivedDate {
        get {
            // NAIS-1688 [dp] For Databank Paper Entry -- modified from the below to be consistent
            if (GlobalVariables.isDatabankUser(null)) {
                return true;
            } else {
                return false;
            }
        }
        set;
    }

    private static Family_Portal_Settings__c familySetting {
        get {
            if (familySetting == null) {
                familySetting = Family_Portal_Settings__c.getInstance('Family');
            }
            return familySetting;
        }
        set;
    }


    public PageReference cancelTotalTaxable(){
        return Page.FamilyApplicationMain;
    }

    //SFP-264, [G.S]
    // save records and go to next
    public PageReference saveAndContinueTotalTaxable() {
        if (saveTotalTaxable()) {
            return getSaveAndContinuePageReference();
        }
        return null;
    }

    // save records and go to dashboard
    public PageReference saveAndExitTotalTaxable() {
        if (saveTotalTaxable()) {
            PageReference returnTarget = Page.FamilyDashboard;
            returnTarget.setRedirect(true);
            return returnTarget;
        }
        return null;
    }

    private Boolean saveTotalTaxable() {

        if(validateTaxableFields()){
            pfs.put(appUtils.statusField, true);
            Database.SaveResult sr =Database.update(pfs, false);

            if (!sr.isSuccess()) {
                for(Database.Error err : sr.getErrors()) {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,err.getMessage()));
                }
            }else {
                return true;
            }
        }
        return false;
    }
    
    private PageReference getSaveAndContinuePageReference() {
        PageReference returnTarget = Page.FamilyApplicationMain;
        returnTarget.getParameters().put('id', pfs.Id);

        FamilyNavigationService.Response navigationResponse = appUtils.NextSection;
        String nextPage = navigationResponse.NextPage;
        Business_Farm__c currentBusinessFarm = navigationResponse.BusinessFarm;

        if (currentBusinessFarm != null) {
            returnTarget.getParameters().put('bfId', currentBusinessFarm.Id);
        }

        returnTarget.getParameters().put('screen', nextPage);
        returnTarget.setRedirect(true);
        
        return returnTarget;
    }//End:getSaveAndContinuePageReference
    
    public PageReference saveAndExitBasicTax() {
        
        if(validateBasicTax() && saveBasicTax()) {
            PageReference returnTarget = Page.FamilyDashboard;
            returnTarget.setRedirect(true);
            return returnTarget;
        }
        
        return null;
    }//End:saveAndExitBasicTax
    
    public PageReference saveAndContinueBasicTax() {
        
        if (validateBasicTax() && saveBasicTax()) {
            return getSaveAndContinuePageReference();
        }
        return null;
    }//End:saveAndContinueBasicTax
    
    private Boolean saveBasicTax(){
        
        if (pfs.Own_Business_or_Farm__c == 'Yes' && pfs.Number_of_Businesses__c == null) {
            pfs.Number_of_Businesses__c = '1';
        }
        
        //SFP-779: hidden fields for EZ must be completed for section 7. Once, the pfs have changed to FULL.
        if(EZRecalculateResult == null || !EZRecalculateResult.hasChanged) {
            pfs.put(appUtils.statusField, true);
        }
        
        Database.SaveResult sr =Database.update(pfs, false);

        if (!sr.isSuccess()) {
            for(Database.Error err : sr.getErrors()) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,err.getMessage()));
            }
        }else{
           return true;
        }
        
        return false;
    }//End:saveBasicTax
    
    public PageReference yesWarningBasicTax() {
        
        if(EZNeedsToBeUpdatedToFull(pfs, false, 'BasicTax').hasChanged) {
            pfs = PfsProcessService.Instance.toFull(pfs, false);
        }
        
        saveBasicTax();
        
        showPopup = false;
        
        return getSaveAndContinuePageReference();
    }//End:yesWarningBasicTax
    
    public PageReference noWarningBasicTax() {

        showPopup = false;
        return null;
    }//End:noWarningBasicTax
    
    private Boolean validateBasicTax(){
        
        if(EZNeedsToBeUpdatedToFull(pfs, true, 'BasicTax').hasChanged) {
            showPopup = true;
            return false;
        }
        
        return true;
    }//End:validateBasicTax
    
    public Boolean getIsEZProcessActive(){
        return PfsProcessService.Instance.isEZProcessActive;
    }//End:getIsEZProcessActive
    
    public PageReference yesWarning() {
        
        if(EZNeedsToBeUpdatedToFull(pfs, false, 'TotalTaxable').hasChanged) {
            //SFP-939: hidden fields for EZ must be completed for section 7. Once, the pfs have changed to FULL.
            pfs = PfsProcessService.Instance.toFull(pfs, false);
        } else {
            pfs.put(appUtils.statusField, true);
        }
        Database.update(pfs);
        PageReference returnTarget = Page.FamilyApplicationMain;
        returnTarget.getParameters().put('id', pfs.Id);
        returnTarget.getParameters().put('screen', appUtils.nextPage);
        returnTarget.setRedirect(true);
        return returnTarget;
    }

    public PageReference noWarning()
    {
        this.showPopup = false;
        return null;
    }

    private Boolean validateTaxableFields()
    {
        taxableWarningMessage = '';
        this.showPopup = false;

        if (((pfs.Other_Income_Current__c!=null && pfs.Other_Income_Current__c > 0) || ( pfs.Other_Income_Est__c!=null && pfs.Other_Income_Est__c > 0))
            && String.isBlank(pfs.Other_Taxable_Income_Descrip__c))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.You_must_enter_a_value));
            return false;
        }

        if(pfs.IRS_Adjustments_to_Income_Descrip__c == null && (
            pfs.Deductible_Part_SE_Tax_Current__c > 0 ||
            pfs.Deductible_Part_SE_Tax_Est__c > 0 ||
            pfs.Keogh_Plan_Payment_Current__c > 0 ||
            pfs.Keogh_Plan_Payment_Est__c > 0 ||
            pfs.Untaxed_IRA_Plan_Payment_Current__c > 0 ||
            pfs.Untaxed_IRA_Plan_Payment_Est__c > 0 ||
            pfs.IRS_Adjustments_to_Income_Current__c > 0 ||
            pfs.IRS_Adjustments_to_Income_Est__c > 0))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.Itemize_Adjustments_to_Income_Section));

            return false;
        }

        Decimal salaryB = pfs.Salary_Wages_Parent_B__c!=null?pfs.Salary_Wages_Parent_B__c:0;
        Decimal salaryA = pfs.Salary_Wages_Parent_A__c!=null?pfs.Salary_Wages_Parent_A__c:0;
        Decimal adjustedIncome = (salaryA + salaryB + zeroIfNull(pfs.Interest_Income_Current__c)
            + zeroIfNull(pfs.Dividend_Income_Current__c) 
            + zeroIfNull(pfs.Alimony_Current__c)
            - zeroIfNull(pfs.IRS_Adjustments_to_Income_Current__c));

        if (pfs.IRS_Adjustments_to_Income_Current__c != null && familySetting.Adjusment_High_Percentage__c != null 
            && pfs.IRS_Adjustments_to_Income_Current__c >= familySetting.Adjusment_High_Percentage__c*(salaryA + salaryB)) 
        {
            taxableWarningMessage = taxableWarningMessage + '. Your adjustments are unusually high at $'+pfs.IRS_Adjustments_to_Income_Current__c+' and your adjusted income is now '+adjustedIncome+'. Are you sure this is correct?<br/>';
        }

        if (pfs.Salary_Wages_Parent_A__c!=null && salaryA > 0 && salaryA < familySetting.Parent_A_Total_Salary__c) 
        {
            taxableWarningMessage = taxableWarningMessage + '. Your income is unusually low at $'+salaryA+'. Are you sure this is correct?<br/>';
        }

        if (pfs.Salary_Wages_Parent_B__c!=null && salaryB > 0 && salaryB < familySetting.Parent_B_Total_Salary__c) 
        {
            taxableWarningMessage = taxableWarningMessage + '. Your income is unusually low at $'+salaryB+'. Are you sure this is correct?<br/>';
        }
        
        if (String.isNotBlank(taxableWarningMessage)) 
        {
            this.showPopup = true;
            return false;
        }
        
        if(EZNeedsToBeUpdatedToFull(pfs, true, 'TotalTaxable').hasChanged) 
        {
            this.showPopup = true;
            return false;
        }

        return true;
    }
    
    /**
    * @description Determines if the current PFS with Submission_Processs__c='EZ' shoulb be set to 'Full'. 
    *              This happens if total income is greater than the calculated income threshold.
    */
    @testVisible private PfsProcessService.RecalculateResult EZNeedsToBeUpdatedToFull(PFS__c ezPFS, Boolean clearResult, String currentScreen) {
        
        if(clearResult || EZRecalculateResult == null) {
            EZRecalculateResult = PfsProcessService.Instance.reCalculateSubmissionProcess(ezPFS, currentScreen);
        }
        
        return EZRecalculateResult;
    }//End:EZNeedsToBeUpdatedToFull
    
    private Decimal zeroIfNull(Decimal value) {
        return value == null ? 0 : value;
    }

    //SFP-10, [G.S]
    public List<SelectOption> getLanguages() {
        List<SelectOption> langs = new List<SelectOption>();
        langs.add(new SelectOption('en_US', 'English'));
        langs.add(new SelectOption('es', 'Spanish'));
        return langs;
    }

    // Returns the Parent information re-formatted into an interable list so
    //  it can be displayed in pageblocktable and datatable components
    public List<ParentListWrapper> parentsList {
        get {
            List<ParentListWrapper> returnList = new List<ParentListWrapper>{};

            // Create the Parent A row
            if (pfs.Parent_A_First_Name__c != null && pfs.Parent_A_First_Name__c != '') {
                ParentListWrapper parentARow = new ParentListWrapper();
                parentARow.Label = 'Parent A';

                parentARow.FullName = pfs.Parent_A_First_Name__c + ' ' + initial(pfs.Parent_A_Middle_Name__c) + ' ' + pfs.Parent_A_Last_Name__c;
                parentARow.Gender = pfs.Parent_A_Gender__c;
                parentARow.Birthdate = pfs.Parent_A_Birthdate__c;

                returnList.add(parentARow);
            }

            // Create the Parent B row
            if (pfs.Parent_B_First_Name__c != null && pfs.Parent_B_First_Name__c != '') {
                ParentListWrapper parentBRow = new ParentListWrapper();
                parentBRow.Label = 'Parent B';
                parentBRow.FullName = pfs.Parent_B_First_Name__c + ' ' + initial(pfs.Parent_B_Middle_Name__c) + ' ' + pfs.Parent_B_Last_Name__c;
                parentBRow.Gender = pfs.Parent_B_Gender__c;
                parentBRow.Birthdate = pfs.Parent_B_Birthdate__c;
                returnList.add(parentBRow);
            }

            // Create the Other Parent
            if (pfs.Addl_Parent_First_Name__c != null && pfs.Addl_Parent_First_Name__c != '') {
                ParentListWrapper otherParentRow = new ParentListWrapper();
                otherParentRow.Label = 'Other Parent';
                otherParentRow.FullName = pfs.Addl_Parent_First_Name__c + ' ' + initial(pfs.Addl_Parent_Middle_Initial__c) + ' ' + pfs.Addl_Parent_Last_Name__c;
                returnList.add(otherParentRow);
            }

            return returnList;
        }
    }

    // Returns the Dependents information re-formatted into an interable list so
    //  it can be displayed in pageblocktable and datatable components
    public List<Dependents__c> dependentsList {
        get{
            dependentsList = new List<Dependents__c>{};

            for(Dependents__c dep : pfs.Dependents__r){

                // Create Dependent 1 row
                if(dep.Dependent_1_Full_Name__c != null && pfs.Number_of_Dependent_Children__c>0){
                    dependentsList.add(new Dependents__c(
                                            Dependent_1_Full_Name__c = dep.Dependent_1_Full_Name__c,
                                            Dependent_1_Gender__c = dep.Dependent_1_Gender__c,
                                            Dependent_1_Birthdate__c = dep.Dependent_1_Birthdate__c));
                }

                // Create Dependent 2 row
                if(dep.Dependent_2_Full_Name__c != null && pfs.Number_of_Dependent_Children__c>1){
                    dependentsList.add(new Dependents__c(
                                            Dependent_1_Full_Name__c = dep.Dependent_2_Full_Name__c,
                                            Dependent_1_Gender__c = dep.Dependent_2_Gender__c,
                                            Dependent_1_Birthdate__c = dep.Dependent_2_Birthdate__c));
                }

                // Create Dependent 3 row
                if(dep.Dependent_3_Full_Name__c != null && pfs.Number_of_Dependent_Children__c>2){
                    dependentsList.add(new Dependents__c(
                                            Dependent_1_Full_Name__c = dep.Dependent_3_Full_Name__c,
                                            Dependent_1_Gender__c = dep.Dependent_3_Gender__c,
                                            Dependent_1_Birthdate__c = dep.Dependent_3_Birthdate__c));
                }

                // Create Dependent 4 row
                if(dep.Dependent_4_Full_Name__c != null && pfs.Number_of_Dependent_Children__c>3){
                    dependentsList.add(new Dependents__c(
                                            Dependent_1_Full_Name__c = dep.Dependent_4_Full_Name__c,
                                            Dependent_1_Gender__c = dep.Dependent_4_Gender__c,
                                            Dependent_1_Birthdate__c = dep.Dependent_4_Birthdate__c));
                }

                // Create Dependent 5 row
                if(dep.Dependent_5_Full_Name__c != null && pfs.Number_of_Dependent_Children__c>4){
                    dependentsList.add(new Dependents__c(
                                            Dependent_1_Full_Name__c = dep.Dependent_5_Full_Name__c,
                                            Dependent_1_Gender__c = dep.Dependent_5_Gender__c,
                                            Dependent_1_Birthdate__c = dep.Dependent_5_Birthdate__c));
                }

                // Create Dependent 6 row
                if(dep.Dependent_6_Full_Name__c != null && pfs.Number_of_Dependent_Children__c>5){
                    dependentsList.add(new Dependents__c(
                                            Dependent_1_Full_Name__c = dep.Dependent_6_Full_Name__c,
                                            Dependent_1_Gender__c = dep.Dependent_6_Gender__c,
                                            Dependent_1_Birthdate__c = dep.Dependent_6_Birthdate__c));
                }
            }

            return dependentsList;
        }
        set;
    }

    public String dependentsPageUrl {
        get{
            return applicationMainPR('DependentInformation').getURL();
        }
    }
    public String applicantsPageUrl {
        get{
            return applicationMainPR(APPLICANT_INFORMATION_SCREEN).getURL();
        }
    }
    public String parentsPageUrl {
        get{
            return applicationMainPR('ParentsGuardians').getURL();
        }
    }

    //[CH] NAIS-1419 Adjusting to use dynamic year labels instead of static custom labels
    public String currentTaxYearLabel {
        get{
            if(currentTaxYearLabel == null){
                if(pfs.Academic_Year_Picklist__c != null)
                    currentTaxYearLabel = GlobalVariables.getDocYearStringFromAcadYearName(pfs.Academic_Year_Picklist__c);
                else
                    currentTaxYearLabel = '';
            }
            return currentTaxYearLabel;
        }
        set;
    }
    public String futureTaxYearLabel {
        get{
            if(futureTaxYearLabel == null){
                if(pfs.Academic_Year_Picklist__c != null)
                    futureTaxYearLabel = GlobalVariables.getNextDocYearStringFromAcadYearName(pfs.Academic_Year_Picklist__c);
                else
                    futureTaxYearLabel = '';
            }
            return futureTaxYearLabel;
        }
        set;
    }

    public Academic_Year__c currentPFSYear {
        get{
            if(currentPFSYear == null){
                currentPFSYear = [select Id, Name from Academic_Year__c where Name = :pfs.Academic_Year_Picklist__c];
            }
            return currentPFSYear;
        }
        set;
    }
    // [CH] NAIS-1201 Updated to use the GlobalVariables method and to tolerate
    //                  the case where there is no prior year PFS record
    public Academic_Year__c previousPFSYear {
        get{
            if(previousPFSYear == null){
                previousPFSYear = GlobalVariables.getPreviousAcademicYear(currentPFSYear.Id);
            }
            return previousPFSYear;
        }
        set;
    }

    public PFS__c priorYearPFS {
        get {
            if(priorYearPFS == null) {
                priorYearPFS = new PFS__c();
                if(previousPFSYear != null) {
                    priorYearPFS = ApplicationUtils.queryPFSRecord(null, pfs.Parent_A__c, previousPFSYear.Id, null);
                }
            }
            return priorYearPFS;
        }
        set;
    }

    public Decimal getPFSIncome(){
        return FamilyTotalIncomeCalculator.calculateTotalIncome(pfs.Id);
    }

    /*End Properties*/

    /* Helper Methods */
    private String initial(String name){
        return name == null ? '' : name.Left(1) + '.';
    }

    // Simply allow a value to be set from the page to the controller record and refresh an element in the page
    public PageReference setParentValue() {
        return null;
    }

    public PageReference applicationMainPR(String screen){
        PageReference pr = Page.FamilyApplicationMain;
        pr.getParameters().put('id', pfs.Id);
        pr.getParameters().put('screen', screen);
        return pr;
    }

    //NAIS-1778 Start
    public Boolean getMedDentReq(){
        if((!String.isEmpty(String.valueOf(pfs.Medical_Dental_Exp_Current__c)) && pfs.Medical_Dental_Exp_Current__c > 0 ) || (!String.isEmpty(String.valueOf(pfs.Medical_Dental_Exp_Est__c)) && pfs.Medical_Dental_Exp_Est__c > 0 )){
            return true;
        } else {
            return false;
        }
    }

    public Boolean getUnusualReq(){
        if((!String.isEmpty(String.valueOf(pfs.Unusual_Expenses_Current__c)) && pfs.Unusual_Expenses_Current__c > 0 ) || (!String.isEmpty(String.valueOf(pfs.Unusual_Expenses_Est__c)) && pfs.Unusual_Expenses_Est__c > 0 )){
            return true;
        } else {
            return false;
        }
    }

    public Boolean getClubReq(){
        if(!String.isEmpty(String.valueOf(pfs.Annual_Club_Fees__c)) && pfs.Annual_Club_Fees__c > 0 ){
            return true;
        } else {
            return false;
        }
    }

    public Boolean getLessCampReq(){
        if(!String.isEmpty(String.valueOf(pfs.Camps_Lessons_Annual_Cost__c)) && pfs.Camps_Lessons_Annual_Cost__c > 0 ){
            return true;
        } else {
            return false;
        }
    }
    //NAIS-1778 End
    /* End Helper Methods */

    // [DP] 03.25.2016 SFP-201 refactored from FamilyAppApplicantCont into virtual method so it can also be used by FamilyAppHouseSumCont.cls
    public void deleteApplicantAndUpdateRelatedPFS(Id theApplicantId){
        List<Applicant__c> applicantToDelete = [SELECT Id, PFS__c FROM Applicant__c WHERE Id = :theApplicantId];
        Id relatedPFSId = applicantToDelete[0].PFS__c;
        List<School_PFS_Assignment__c> spas = [SELECT Id FROM School_PFS_Assignment__c WHERE Applicant__c = :theApplicantId];
        List<School_Document_Assignment__c> sdas = [SELECT Id FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c IN :spas];
        // SFP-194 [DP] 11.23.2015 also delete SBFAs
        List<School_Biz_Farm_Assignment__c> sbfas = [Select Id From School_Biz_Farm_Assignment__c WHERE School_PFS_Assignment__c IN :spas];
        delete sbfas;
        delete sdas;
        delete spas;
        delete applicantToDelete;

        pfs.statEducationalExpenses__c = false;
        pfs.statStudentIncome__c = false;
        pfs.statForKamehamehaApplicant__c = false;
        pfs.statForKamehamehaProgram__c = false;
        pfs.statSelectSchools__c = false;
        pfs.statHouseholdSummary__c = false;

        // Query for Applicant records still related to the PFS
        List<Applicant__c> remainingApplicants = [select Id from Applicant__c where PFS__c = :relatedPFSId];

        // If there aren't any then set the status flag on the PFS to incomplete
        if(remainingApplicants == null || remainingApplicants.size() == 0){

            pfs.statApplicantInformation__c = false;
        }

        //SFP-1310: Make sure that stats for not visible sections in EZ PFS remains set to true.
        pfs = pfsProcessService.Instance.lockStatsUpdateForEZ(pfs);

        update pfs;
    }

    public class ParentListWrapper{

        @testVisible public String Label { get; private set; }
        @testVisible public String FullName { get; private set; }
        @testVisible public String Gender { get; private set; }
        @testVisible public Date Birthdate { get; private set; }

        public ParentListWrapper() {
            Label = '';
            FullName = '';
            Gender = '';
            Birthdate = null;
        }
    }
    
    public PageReference monthlyNetIncome() {
        
        MonthlyIncomeAndExpensesService.Instance.getMonthlyNetIncome(pfs);
        
        return null;
    }//End:monthlyNetIncome
}