@isTest
public class DocumentRequirementServiceTest {
    
    public static School_PFS_Assignment__c spa1, spa2, spa3;
    public static Student_Folder__c studentFolder1, studentFolder2, studentFolder3;
    public static Academic_Year__c academicYear;
    public static Household__c household;
    
    public static void Setup() {
        
        household = HouseholdTestData.Instance.DefaultHousehold;
        
        //1. Create the current academic year.
        academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        
        //2. Create a school.
        Account school = AccountTestData.Instance.forName('School1')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccount();
        
        //3. Create one parent with 3 applicants.
        Contact parentA = ContactTestData.Instance.asParent().forFirstName('ParentA').forLastName('ParentA').forHousehold(household.Id).create();
        Contact parentAA = ContactTestData.Instance.asParent().forFirstName('ParentA').forLastName('ParentA').create();
        Contact student1 = ContactTestData.Instance.asStudent().forFirstName('Student1').forLastName('Student1').create();
        Contact student2 = ContactTestData.Instance.asStudent().forFirstName('Student2').forLastName('Student2').create();
        Contact student3 = ContactTestData.Instance.asStudent().forFirstName('Student3').forLastName('Student3').create();
        insert new List<Contact> { parentA, parentAA, student1, student2, student3 };
        
        //Create Verification records for each PFS.        
        Verification__c verification1 = new Verification__c();
        Verification__c verification2 = new Verification__c();
        insert new List<Verification__c>{verification1, verification2};
        
        //4. Create one PFS.        
        PFS__c pfs1 = PfsTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forParentA(parentA.Id)
            .asPaid()
            .forVerification(verification1.Id)
            .create();
        PFS__c pfs2 = PfsTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forParentA(parentAA.Id)
            .asPaid()
            .forVerification(verification2.Id)
            .create();
        insert new List<PFS__c>{pfs1, pfs2};
        
        //5. Create 3 applicants.
        Applicant__c applicant1 = ApplicantTestData.Instance
            .forContactId(student1.Id)
            .forPfsId(pfs1.Id)
            .create();
        Applicant__c applicant2 = ApplicantTestData.Instance
            .forContactId(student2.Id)
            .forPfsId(pfs1.Id)
            .create();
        Applicant__c applicant3 = ApplicantTestData.Instance
            .forContactId(student3.Id)
            .forPfsId(pfs2.Id)
            .create();
        insert new List<Applicant__c>{applicant1, applicant2, applicant3};
        
        //6. Create one student folder for each student.    
        studentFolder1 = StudentFolderTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student1.Id)
            .create();
        studentFolder2 = StudentFolderTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student2.Id) 
            .create();
        studentFolder3 = StudentFolderTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student3.Id) 
            .create();
        insert new List<Student_Folder__c>{studentFolder1, studentFolder2, studentFolder3};
        
        //7. Create an SPA for each Student Folder.
        spa1 = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicant1.Id)
            .forStudentFolderId(studentFolder1.Id)
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .createSchoolPfsAssignmentWithoutReset();
        spa2 = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicant2.Id)
            .forStudentFolderId(studentFolder2.Id)
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .createSchoolPfsAssignmentWithoutReset();
        spa3 = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicant3.Id)
            .forStudentFolderId(studentFolder3.Id)
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .createSchoolPfsAssignmentWithoutReset();
        insert new List<School_PFS_Assignment__c>{spa1, spa2, spa3};
    }
    
    @isTest
    private static void saveNewDocument_multipleSelectedFolders_createNewSDAs() {
        
        Setup();
        
        DocumentRequirementService.Request request = new DocumentRequirementService.Request(
            'Paycheck/Paystub Copy', academicYear.Name, new Set<Id>{studentFolder1.Id, studentFolder3.Id}, true, true);
        
        DocumentRequirementService.Response response = DocumentRequirementService.Instance.requestAdditionalDocuments(request);
        
        List<School_Document_Assignment__c> sdas1, sdas2, sdas3;
        Test.startTest();
            //8. Verify that the new additional documents are required for each SPA
            sdas1 = new List<School_Document_Assignment__c>([SELECT Id FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: spa1.Id]);
            System.AssertEquals(1, sdas1.size());
              
            sdas2 = new List<School_Document_Assignment__c>([SELECT Id FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: spa2.Id]);
            System.AssertEquals(1, sdas2.size());
            
            sdas3 = new List<School_Document_Assignment__c>([SELECT Id FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: spa3.Id]);
            System.AssertEquals(1, sdas3.size());
        
        Test.stopTest();
    }    
    
    @isTest
    private static void saveNewDocument_singleSelectedFolder_createNewSDA() {
        
        Setup();
        
        DocumentRequirementService.Request request = new DocumentRequirementService.Request(
            'Paycheck/Paystub Copy', academicYear.Name, new Set<Id>{studentFolder3.Id}, true, true);
        
        DocumentRequirementService.Response response = DocumentRequirementService.Instance.requestAdditionalDocuments(request);
        
        List<School_Document_Assignment__c> sdas1, sdas2, sdas3;
        Test.startTest();
            //8. Verify that the new additional documents are required for each SPA
            sdas1 = new List<School_Document_Assignment__c>([SELECT Id FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: spa1.Id]);
            System.AssertEquals(0, sdas1.size());
              
            sdas2 = new List<School_Document_Assignment__c>([SELECT Id FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: spa2.Id]);
            System.AssertEquals(0, sdas2.size());
            
            sdas3 = new List<School_Document_Assignment__c>([SELECT Id FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: spa3.Id]);
            System.AssertEquals(1, sdas3.size());
        
        Test.stopTest();
    }    
    
    @isTest
    private static void saveNewDocument_singleSelectedFolderWithSibling_createNewSDAs() {
        
        Setup();
        
        DocumentRequirementService.Request request = new DocumentRequirementService.Request(
            'Paycheck/Paystub Copy', academicYear.Name, new Set<Id>{studentFolder1.Id}, true, true);
        
        DocumentRequirementService.Response response = DocumentRequirementService.Instance.requestAdditionalDocuments(request);
        
        List<School_Document_Assignment__c> sdas1, sdas2, sdas3;
        Test.startTest();
            //8. Verify that the new additional documents are required for each SPA
            sdas1 = new List<School_Document_Assignment__c>([SELECT Id FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: spa1.Id]);
            System.AssertEquals(1, sdas1.size());
              
            sdas2 = new List<School_Document_Assignment__c>([SELECT Id FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: spa2.Id]);
            System.AssertEquals(1, sdas2.size());
            
            sdas3 = new List<School_Document_Assignment__c>([SELECT Id FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: spa3.Id]);
            System.AssertEquals(0, sdas3.size());
        
        Test.stopTest();
    }
    
    @isTest
    private static void getFamilyDocByHouseholdId_sda1040WithFDForSchool1_sda1040ShareSameFDWithSchool2() {
        
        Setup();
        
        //2. Create a school.
        Account school2 = AccountTestData.Instance.forName('School2')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccount();
        
        //3. Create one parent with 3 applicants.
        Contact parentA2 = ContactTestData.Instance.asParent().forFirstName('ParentA2').forLastName('ParentA2').create();
        Contact student4 = ContactTestData.Instance.asStudent().forFirstName('Student4').forLastName('Student4').create();
        insert new List<Contact> { parentA2, student4 };
        
        //4. Create one PFS.
        PFS__c pfs4 = PfsTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forParentA(parentA2.Id)
            .asPaid()
            .insertPFS();
        
        //5. Create new applicant for the new school2.
        Applicant__c applicant4 = ApplicantTestData.Instance
            .forContactId(student4.Id)
            .forPfsId(pfs4.Id)
            .insertApplicant();
        
        //6. Create a new student folder for the new student.    
        Student_Folder__c studentFolder4 = StudentFolderTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student4.Id)
            .insertStudentFolder();
        
        //7. Create an SPA for the Student Folder.
        School_PFS_Assignment__c spa4 = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicant4.Id)
            .forStudentFolderId(studentFolder4.Id)
            .forSchoolId(school2.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .insertSchoolPfsAssignment();
        
        //8. Create 1040 RD.
        RequiredDocumentTestData.Instance
                .forSchoolId(school2.Id)
                .forDocType('1040')
                .forAcademicYearId(academicYear.Id)
                .forYear(academicYear.Name.left(4))
                .insertRequiredDocument();
        
        //9. Create a 1040 FD that will be automatically attached to the new RD
        Family_Document__c document1040 = FamilyDocumentTestData.Instance
                .forDocumentType('1040')
                .forAcademicYearPicklist(academicYear.Name)
                .forDocYear(academicYear.Name.left(4))
                .forHousehold(household.Id)
                .insertFamilyDocument();
        
        DocumentRequirementService.Request request = new DocumentRequirementService.Request(
            '1040 with all filed schedules and attachments', 
            academicYear.Name.left(4), 
            new Set<Id>{studentFolder1.Id}, true, true);
        
        DocumentRequirementService.Response response = DocumentRequirementService.Instance.requestAdditionalDocuments(request);
        
        List<School_Document_Assignment__c> sdas1, sdas2, sdas3;
        Test.startTest();
            sdas1 = new List<School_Document_Assignment__c>([SELECT Id, Document__c FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: spa1.Id]);
            System.AssertEquals(1, sdas1.size());
            System.AssertEquals(document1040.Id, sdas1[0].Document__c);
              
            sdas2 = new List<School_Document_Assignment__c>([SELECT Id, Document__c FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: spa2.Id]);
            System.AssertEquals(1, sdas2.size());
            System.AssertEquals(document1040.Id, sdas2[0].Document__c);
            
            sdas3 = new List<School_Document_Assignment__c>([SELECT Id FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: spa3.Id]);
            System.AssertEquals(0, sdas3.size());
        
        Test.stopTest();
    }    
}