/* 
 * Spec-109 School Portal - Tuition and Fees;
 * BR, Exponent Partners, 2013
 */
       
public with sharing class SchoolTuitionAndFeesController implements SchoolAcademicYearSelectorInterface {

    /*Initialization*/
    
    public SchoolTuitionAndFeesController() {        
        init();
    }
    
    public void init() {
        
        // Get the currently selected AcademicYearId from url parameters if there is one 
        String academicYearId = ApexPages.currentPage().getParameters().get('academicYearId');
        if(academicYearId != null){
            myAnnualSettings = GlobalVariables.getCurrentAnnualSettings(false, academicYearId)[0];    
        }
        else{
            myAnnualSettings = GlobalVariables.getCurrentAnnualSettings()[0];
        }
           
           // Default to 'One Schedule per Grade'. Need to do again for SchoolAcademicYearSelector_OnChange().
           if (myAnnualSettings.Tuition_Schedule_Type__c == null) {
               myAnnualSettings.Tuition_Schedule_Type__c = 'One Schedule per Grade';
           }

           // Need to do again for SchoolAcademicYearSelector_OnChange().
           // myAnnualSettings.Tuition_Schedule_Type__c should be set before calling getGradesList().
           getGradesList();
           
           // School_Type__c, Day or Boarding. Did AnnualSettings get this somewhere so we don't have to?
        Id schoolId = myAnnualSettings.School__c;
        Account mySchool = [SELECT Id, School_Type__c FROM Account WHERE Id = :schoolId];
           bBoarding = (mySchool.School_Type__c == 'Boarding') || (mySchool.School_Type__c == 'Day and Boarding');
        bDay = (mySchool.School_Type__c == 'Day') || (mySchool.School_Type__c == 'Day and Boarding');            
    }

    /*Properties*/
    
    public Annual_Setting__c myAnnualSettings { get; private set; }
    
    public Boolean bDay { get; private set; }
    public Boolean bBoarding { get; private set; }
    
    public Boolean bOneSchedulePerGrade { 
        get { return (myAnnualSettings.Tuition_Schedule_Type__c == 'One Schedule per Grade'); }
    }
    
    public SchoolTuitionAndFeesController Me {
        get { return this; }
    }

    public Component.Apex.InputField getTestField() {
        Component.Apex.InputField testField = new Component.Apex.InputField();
        testField.expressions.value = '{!Annual_Setting__c.K_Day_Tuition__c}';
        return testField;
    }

    /*Action Methods*/
    
    public PageReference tuitionSchedule_onchange() {
        return null;
    }
    
    public PageReference applyWNoTuition() {
        
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No.');
        ApexPages.addMessage(msg);
        
        putGradesList();
        
        update myAnnualSettings;
        
        return null;
    }

    public PageReference applyAll() {
        
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Saved.');
        ApexPages.addMessage(msg);
        
        putGradesList();
        
        update myAnnualSettings;
        
        return null;
    }
    
    public PageReference cancel() {
        
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Cancelled.');
        ApexPages.addMessage(msg);
        
        init();
        
        return null;
    }

    /* Helper Methods */
    
    public class Grade {
        
        public String Name { get; set; }

        public Decimal Boarding_Fees { get; set; }
        public Decimal Boarding_New_Student_Expense { get; set; }
        public Decimal Boarding_Travel_Expense { get; set; }
        public Decimal Boarding_Tuition { get; set; }
        
        public Decimal Day_Fees { get; set; }
        public Decimal Day_New_Student_Expense { get; set; }
        public Decimal Day_Travel_Expense { get; set; }
        public Decimal Day_Tuition { get; set; }
    }
    
    public List<Grade> Grades { get; private set; }
    
    static public List<String> GradeNames = new List<String> {
        'Preschool', 'Pre K', 'Jr K', 'K', 'Pre-First', 'Grade 1', 'Grade 2', 'Grade 3', 'Grade 4', 'Grade 5',
        'Grade 6', 'Grade 7', 'Grade 8', 'Grade 9', 'Grade 10', 'Grade 11', 'Grade 12'
    };
    // This must match Annual_Settings__c.
    static public List<String> GradeFieldNames = new List<String> {
        'Preschool', 'Pre_K', 'Jr_K', 'K', 'Pre_First', 'Grade_1', 'Grade_2', 'Grade_3', 'Grade_4', 'Grade_5',
        'Grade_6', 'Grade_7', 'Grade_8', 'Grade_9', 'Grade_10', 'Grade_11', 'Grade_12'
    };
    
    public List<Grade> getGradesList() {
        
        grades = new List<Grade>();
       
        for (Integer i = 0; i < GradeFieldNames.size(); i++)
        {
            Grade myGrade = new Grade();
            myGrade.Name = GradeNames[i];
            
            myGrade.Day_Fees = (Decimal)myAnnualSettings.get(GradeFieldNames[i] + '_Day_Fees__c');
            myGrade.Day_New_Student_Expense = (Decimal)myAnnualSettings.get(GradeFieldNames[i] + '_Day_New_Student_Expense__c');
            myGrade.Day_Travel_Expense = (Decimal)myAnnualSettings.get(GradeFieldNames[i] + '_Day_Travel_Expense__c');
            myGrade.Day_Tuition = (Decimal)myAnnualSettings.get(GradeFieldNames[i] + '_Day_Tuition__c');
            
            myGrade.Boarding_Fees = (Decimal)myAnnualSettings.get(GradeFieldNames[i] + '_Boarding_Fees__c');
            myGrade.Boarding_New_Student_Expense = (Decimal)myAnnualSettings.get(GradeFieldNames[i] + '_Boarding_New_Student_Expense__c');
            myGrade.Boarding_Travel_Expense = (Decimal)myAnnualSettings.get(GradeFieldNames[i] + '_Boarding_Travel_Expense__c');
            myGrade.Boarding_Tuition = (Decimal)myAnnualSettings.get(GradeFieldNames[i] + '_Boarding_Tuition__c');
        
            grades.add(myGrade);
        }
        
        return grades;
    }
    
    public void putGradesList() {
        
        if (bOneSchedulePerGrade) 
        {
            for (Integer i = 0; i < GradeFieldNames.size(); i++)
            {
                myAnnualSettings.put(GradeFieldNames[i] + '_Day_Fees__c', grades[i].Day_Fees);
                myAnnualSettings.put(GradeFieldNames[i] + '_Day_New_Student_Expense__c', grades[i].Day_New_Student_Expense);
                myAnnualSettings.put(GradeFieldNames[i] + '_Day_Travel_Expense__c', grades[i].Day_Travel_Expense);
                myAnnualSettings.put(GradeFieldNames[i] + '_Day_Tuition__c', grades[i].Day_Tuition);
    
                myAnnualSettings.put(GradeFieldNames[i] + '_Boarding_Fees__c', grades[i].Boarding_Fees);
                myAnnualSettings.put(GradeFieldNames[i] + '_Boarding_New_Student_Expense__c', grades[i].Boarding_New_Student_Expense);
                myAnnualSettings.put(GradeFieldNames[i] + '_Boarding_Travel_Expense__c', grades[i].Boarding_Travel_Expense);
                myAnnualSettings.put(GradeFieldNames[i] + '_Boarding_Tuition__c', grades[i].Boarding_Tuition);
            }
        } 
        else 
        {
            // Copy the first row into AnnualSettings and to all the others in our own list.
            for (Integer i = 0; i < GradeFieldNames.size(); i++)
            {
                myAnnualSettings.put(GradeFieldNames[i] + '_Day_Fees__c', grades[0].Day_Fees);
                myAnnualSettings.put(GradeFieldNames[i] + '_Day_New_Student_Expense__c', grades[0].Day_New_Student_Expense);
                myAnnualSettings.put(GradeFieldNames[i] + '_Day_Travel_Expense__c', grades[0].Day_Travel_Expense);
                myAnnualSettings.put(GradeFieldNames[i] + '_Day_Tuition__c', grades[0].Day_Tuition);
    
                myAnnualSettings.put(GradeFieldNames[i] + '_Boarding_Fees__c', grades[0].Boarding_Fees);
                myAnnualSettings.put(GradeFieldNames[i] + '_Boarding_New_Student_Expense__c', grades[0].Boarding_New_Student_Expense);
                myAnnualSettings.put(GradeFieldNames[i] + '_Boarding_Travel_Expense__c', grades[0].Boarding_Travel_Expense);
                myAnnualSettings.put(GradeFieldNames[i] + '_Boarding_Tuition__c', grades[0].Boarding_Tuition);
                
                // First one won't do anything.
                grades[i].Day_Fees = grades[0].Day_Fees;
                grades[i].Day_New_Student_Expense = grades[0].Day_New_Student_Expense;
                grades[i].Day_Travel_Expense = grades[0].Day_Travel_Expense;
                grades[i].Day_Tuition = grades[0].Day_Tuition;

                grades[i].Boarding_Fees = grades[0].Boarding_Fees;
                grades[i].Boarding_New_Student_Expense = grades[0].Boarding_New_Student_Expense;
                grades[i].Boarding_Travel_Expense = grades[0].Boarding_Travel_Expense;
                grades[i].Boarding_Tuition = grades[0].Boarding_Tuition;
            }      
        }
    }
    
    /* SchoolAcademicYearSelectorInterface Implementation */
    
    public String getAcademicYearId() {

        return myAnnualSettings.Academic_Year__c;
    }
    
    public void setAcademicYearId(String academicYearId) {

        myAnnualSettings.Academic_Year__c = academicYearId;
    }
    
    public PageReference SchoolAcademicYearSelector_OnChange(Boolean saveRecord) {
        
        PageReference pageRef = Page.SchoolTuitionAndFees;
        if (myAnnualSettings.Academic_Year__c != null && myAnnualSettings.Academic_Year__c != '') {
            pageRef.getParameters().put('academicYearId', myAnnualSettings.Academic_Year__c);
        }
        pageRef.setRedirect(true);
        
        return pageRef;
        
        /*List<Annual_Setting__c> annualSettings = GlobalVariables.getCurrentAnnualSettings(false, datetime.now().date());
        for (Annual_Setting__c setting : annualSettings) {
            if (setting.Academic_Year__c == myAnnualSettings.Academic_Year__c) {
                myAnnualSettings = setting;
            }
        }*/
    }
}