/**
 * SchoolMassEmailDashboardController.cls
 *
 * @description: Server side controller for Mass Email Dashboard functionality, handles interactions w/ data model and view state manipulation.
 *
 * @author: Chase Logan @ Presence PG
 */
public class SchoolMassEmailDashboardController implements SchoolAcademicYearSelectorInterface {

    /* member vars and properties */
    public Boolean massEmailEnabled {

        get {
            // Handle app enabled/disabled
            return MassEmailUtil.isMassEmailGloballyEnabledByAccount( 
                        GlobalVariables.getCurrentSchoolId(), UserInfo.getProfileId());
        }
        private set;
    }
    public String selectedFilter { get; set; }
    public String selectedMassESRecord { get; set; }
    public List<SchoolMassEmailSendModel> massEmailModelList { get; set; }

    public List<SelectOption> getFilterOptions() {

        List<SelectOption> selectList = new List<SelectOption>();
        selectList.add( new SelectOption( '', 'All'));
        selectList.add( new SelectOption( MassEmailSendDataAccessService.FILTER_DELIVERED, 'Completed'));
        selectList.add( new SelectOption( MassEmailSendDataAccessService.FILTER_DRAFT, 'Draft'));
        selectList.add( new SelectOption( MassEmailSendDataAccessService.FILTER_ERROR, 'Error'));
        return selectList;
    }

    /* class constants */
    private final String RESEND_FAIL_MESSAGE = 'Resend attempt failed, detailed message: ';
    private final String CLONE_FAIL_MESSAGE = 'Clone attempt failed, detailed message: ';

    // default ctor
    public SchoolMassEmailDashboardController() {
        
        academicyearid = System.currentPagereference().getParameters().get('academicyearid');
        // initialize view, no filter applied
        init( false);
    }

    public PageReference reInitView() {

        // initialize view, no filter applied
        init( false);

        return null;
    }

    public PageReference changeFilter() {

        // initialize view, apply selected filter
        init( true);
        return null;
    }

    // launch new record wizard
    public PageReference createNewMassEmail() {

        PageReference pRef = Page.SchoolSelectEmailRecipients;
        pRef.setRedirect( true);
        
        if (academicyearid!=null) {
            
            pRef.getParameters().put('academic_year__c',  GlobalVariables.getAcademicYear( academicyearid).Name);
        } else {
            
            pRef.getParameters().put('academic_year__c',  GlobalVariables.getCurrentAcademicYear().Name);
        }
        
        return pRef;
    }

    // view record stub, launch view page
    public PageReference viewRecord() {

        PageReference pRef = Page.SchoolMessageDetail;
        pRef.setRedirect( true);
        pRef.getParameters().put( SchoolMessageDetailController.QUERY_STRING_MESSAGE_DETAIL, this.selectedMassESRecord);

        return pRef;
    }

    // edit record, launch edit page
    public PageReference editRecord() {

        PageReference pRef = Page.SchoolSelectedRecipients;
        pRef.setRedirect( true);
        pRef.getParameters().put( SchoolCreateMessageController.QUERY_STRING_MESSAGE_DETAIL, this.selectedMassESRecord);

        return pRef;
    }

    // delete record, delete draft, refresh
    public PageReference deleteRecord() {

        PageReference pRef = Page.SchoolMassEmailDashboard;
        pRef.setRedirect( true);
        if ( this.selectedMassESRecord != null) {

            new MassEmailSendDataAccessService().deleteMassEmailRecord( this.selectedMassESRecord);
        }

        return pRef;
    }

    // clone record, launch edit page
    public PageReference cloneRecord() {

        PageReference pRef = Page.SchoolSelectedRecipients;
        pRef.setRedirect( true);
        pRef.getParameters().put( 
            SchoolCreateMessageController.QUERY_STRING_MESSAGE_DETAIL, 
                new MassEmailSendDataAccessService().cloneMassEmailRecord( this.selectedMassESRecord));

        return pRef;
    }

    /* sort functions */
    
    // sort by status ascending
    public PageReference sortTableByStatusAsc() {

        this.massEmailModelList = SchoolMassEmailSendModel.sortByStatusAsc( this.massEmailModelList);
        return null;
    }

    // sort by status descending
    public PageReference sortTableByStatusDesc() {

        this.massEmailModelList = SchoolMassEmailSendModel.sortByStatusDesc( this.massEmailModelList);
        return null;
    }

    // sort by name ascending
    public PageReference sortTableByNameAsc() {

        this.massEmailModelList = SchoolMassEmailSendModel.sortByNameAsc( this.massEmailModelList);
        return null;
    }

    // sort by name descending
    public PageReference sortTableByNameDesc() {

        this.massEmailModelList = SchoolMassEmailSendModel.sortByNameDesc( this.massEmailModelList);
        return null;
    }

    // sort by sent by ascending
    public PageReference sortTableBySentByAsc() {

        this.massEmailModelList = SchoolMassEmailSendModel.sortBySentByNameAsc( this.massEmailModelList);
        return null;
    }

    // sort by sent by descending
    public PageReference sortTableBySentByDesc() {

        this.massEmailModelList = SchoolMassEmailSendModel.sortBySentByNameDesc( this.massEmailModelList);
        return null;
    }

    // sort by date sent ascending
    public PageReference sortTableByDateSentAsc() {

        this.massEmailModelList = SchoolMassEmailSendModel.sortByDateSentAsc( this.massEmailModelList);
        return null;
    }

    // sort by date sent descending
    public PageReference sortTableByDateSentDesc() {

        this.massEmailModelList = SchoolMassEmailSendModel.sortByDateSentDesc( this.massEmailModelList);
        return null;
    }

    // sort by total recip ascending
    public PageReference sortTableByRecipAsc() {

        this.massEmailModelList = SchoolMassEmailSendModel.sortByTotalRecipAsc( this.massEmailModelList);
        return null;
    }

    // sort by total recip descending
    public PageReference sortTableByRecipDesc() {

        this.massEmailModelList = SchoolMassEmailSendModel.sortByTotalRecipDesc( this.massEmailModelList);
        return null;
    }

    // sort by total opened ascending
    public PageReference sortTableByOpenedAsc() {

        this.massEmailModelList = SchoolMassEmailSendModel.sortByTotalOpenedAsc( this.massEmailModelList);
        return null;
    }

    // sort by total opened descending
    public PageReference sortTableByOpenedDesc() {

        this.massEmailModelList = SchoolMassEmailSendModel.sortByTotalOpenedDesc( this.massEmailModelList);
        return null;
    }

    // sort by total bounced ascending
    public PageReference sortTableByBouncedAsc() {

        this.massEmailModelList = SchoolMassEmailSendModel.sortByTotalBouncedAsc( this.massEmailModelList);
        return null;
    }

    // sort by total bounced descending
    public PageReference sortTableByBouncedDesc() {

        this.massEmailModelList = SchoolMassEmailSendModel.sortByTotalBouncedDesc( this.massEmailModelList);
        return null;
    }

    // sort by total unsub ascending
    public PageReference sortTableByUnsubAsc() {

        this.massEmailModelList = SchoolMassEmailSendModel.sortByTotalUnsubAsc( this.massEmailModelList);
        return null;
    }

    // sort by total unsub descending
    public PageReference sortTableByUnsubDesc() {

        this.massEmailModelList = SchoolMassEmailSendModel.sortByTotalUnsubDesc( this.massEmailModelList);
        return null;
    }

    /* remoting methods */

    // Trigger resend of a given MES record
    @RemoteAction
    public static void resendRecord( String selectedMassESRecord) {

        if ( String.isNotEmpty( selectedMassESRecord)) {

            try {

                new MassEmailSendDataAccessService().triggerMassEmailResend( selectedMassESRecord);
            } catch ( Exception e) {

                // log it
                System.debug( 'DEBUG:::exception in MassEmailDashboardController.resendRecord' +
                    e.getMessage());
            }
        }
    }

    // Get a given MES records current status
    @RemoteAction
    public static String getRecordStatus( String selectedMassESRecord) {
        String returnVal = '';

        if ( String.isNotEmpty( selectedMassESRecord)) {

            try {

                Mass_Email_Send__c massES = new MassEmailSendDataAccessService().getMassEmailSendById( selectedMassESRecord);
                returnVal = massES.Status__c;
            } catch ( Exception e) {

                // log it
                System.debug( 'DEBUG:::exception in MassEmailDashboardController.getRecordStatus' +
                    e.getMessage());
            }
        }

        return returnVal;
    }

    /* private instance methods */

    // handles page initilization
    private void init( Boolean applyFilter) {

        // Get all MES records assigned to current user's current school
        List<Mass_Email_Send__c> massESList = new List<Mass_Email_Send__c>();
        massESList = 
            new MassEmailSendDataAccessService().getMassEmailSendBySchoolId( 
                GlobalVariables.getCurrentSchoolId(), ( applyFilter == true ? this.selectedFilter : ''));

        this.massEmailModelList = SchoolMassEmailSendModel.getModels(massESList);
    }


    /* 
     * Solely for supporting header and academic year selector, copied from existing code to stay consistent
     */
       public Id academicyearid;
    public Academic_Year__c currentAcademicYear { get; private set; }

    public SchoolMassEmailDashboardController Me {
        get { return this; }
    }
    public String getAcademicYearId() {
        return academicyearid;
    }
    public void setAcademicYearId( String academicYearIdParam) {
        academicyearid = academicYearIdParam;
        currentAcademicYear = GlobalVariables.getAcademicYear( academicyearid);
    }
    // on change of academic year, reload page with new folder
    public PageReference SchoolAcademicYearSelector_OnChange( Boolean saveRecord) {
        PageReference newPage = new PageReference( ApexPages.currentPage().getUrl());
        newPage.getParameters().put( 'academicyearid', getAcademicYearId());
        newPage.setRedirect( true);
        return newPage;
    }
    // Due to the selector_onchange not refreshing properly, we redirect to the same page with academicyearid parameter.
    public void loadAcademicYear() {
        Map<String, String> parameters = ApexPages.currentPage().getParameters();
        String academicYearId = null;
        if ( parameters.containsKey( 'academicyearid'))
            setAcademicYearId(parameters.get( 'academicyearid'));
        else {
            setAcademicYearId( GlobalVariables.getCurrentAcademicYear().Id);
        }
    }
    /* end header support */
    

}