/*
 * Controller for SchoolSubtabs component referenced in School Setup pages
 *
 *  NAIS-2270 Add logic to control visibility of subscription renewal tab
 *
 */
@isTest
private class SchoolSubtabsControllerTest {
    
    @isTest
    private static void testRenewalTab() {
        String subscriptionTypeBasic = 'Basic';
        String no = 'No';

        Renewal_Annual_Settings__c rs = new Renewal_Annual_Settings__c(
            Name = String.valueOf(System.today().year()),
            Renewal_Season_Start_Date__c = System.today(),
            Renewal_Season_End_Date__c = System.today().addDays(10));
        insert rs;
        
        Account school;
        Subscription__c subscription;
        User portalUser;
        
        System.runAs(UserSelector.newInstance().selectById(new Set<Id>{UserInfo.getUserId()})[0]) {
            school = AccountTestData.Instance
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forSSSSubscriberStatus(GlobalVariables.getActiveSSSStatusForTest())
                .forPortalVersion(SubscriptionTypes.BASIC_TYPE).DefaultAccount;

            portalUser = UserTestData.createSchoolPortalUser();
            portalUser.SYSTEM_Terms_and_Conditions_Accepted__c = System.now().addDays(-1);
            insert portalUser;

            subscription = SubscriptionTestData.Instance
                .forEndDate(system.today().addDays(1))
                .forStartDate(system.today().addDays(-14))
                .forCancelled(no)
                .forSubscriptionType(subscriptionTypeBasic).insertSubscription();
        }
        
        Test.startTest();
            System.runAs(portalUser) {
                SchoolSubtabsController testController = new SchoolSubtabsController();
                
                System.assertEquals(true,   testController.getIsEligibleForRenew());
                System.assertEquals(false,  testController.getSubscriptionExpired());
            }
            
            subscription.End_Date__c = system.today().addDays(-1);
            update subscription;
            
            System.runAs(portalUser) {
                SchoolSubtabsController testController = new SchoolSubtabsController();
                
                System.assertEquals(true,   testController.getIsEligibleForRenew());
                System.assertEquals(true,   testController.getSubscriptionExpired());
            }
            
            rs.Renewal_Season_Start_Date__c = System.today().addDays(2);
            update rs;
            subscription.End_Date__c = system.today();
            update subscription;
            
            System.runAs(portalUser) {
                SchoolSubtabsController testController = new SchoolSubtabsController();
                
                System.assertEquals(false,  testController.getIsEligibleForRenew());
                System.assertEquals(false,  testController.getSubscriptionExpired());
            }
            
            delete subscription;
            
            System.runAs(portalUser) {
                SchoolSubtabsController testController = new SchoolSubtabsController();
                
                System.assertEquals(true,   testController.getIsEligibleForRenew());
                System.assertEquals(true,   testController.getSubscriptionExpired());
            }
        Test.stopTest();
    }
}