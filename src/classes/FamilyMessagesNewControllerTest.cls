@isTest
private class FamilyMessagesNewControllerTest
{

    private static User CreateData() {
        // create test data
        TestUtils.createAcademicYears();
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        
        Id portalProfileId = GlobalVariables.familyPortalProfileId;

        Account account1 = TestUtils.createAccount('individual', RecordTypes.individualAccountTypeId, 3, true);
        
        Contact parentA = TestUtils.createContact('Parent A', account1.Id, RecordTypes.parentContactTypeId, true);
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User u = TestUtils.createPortalUser('User 1', 'u1@test.org', 'u1', parentA.Id, portalProfileId, true, true);
        
        System.runAs(u){
            PFS__c pfs1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
            Dml.WithoutSharing.insertObjects(new List<PFS__c>{ pfs1});
        }
        
        TestUtils.createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_SINGLE, true);

        TestUtils.createStateTaxTable(academicYearId, true);
        
        TestUtils.createEmploymentAllowance(academicYearId, true);
        
        TestUtils.createBusinessFarm(academicYearId, true);

        TestUtils.createRetirementAllowance(academicYearId, true);
        
        TestUtils.createAssetProgressivity(academicYearId, true);
        
        TestUtils.createIncomeProtectionAllowance(academicYearId, true);

        TestUtils.createExpectedContributionRate(academicYearId, true);

        TestUtils.createHousingIndexMultiplier(academicYearId, true);
        
        return u;
    }
    
    @isTest
    private static void TestLoad()
    {
        User u = FamilyMessagesNewControllerTest.CreateData();
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        PFS__c pfs = [SELECT Id, Name FROM PFS__c LIMIT 1];
        
        Case c = new Case(subject = 'Test', description = 'Test', contactId = u.ContactId, RecordTypeId = RecordTypes.familyPortalCaseTypeId);
        insert c;

        Test.startTest();
        
        System.runAs(u) {
            PageReference pageRef = Page.FamilyMessagesNew;
            pageRef.getParameters().put('pfsId',pfs.Id);
            pageRef.getParameters().put('AcademicYearId',academicYearId);
            pageRef.getParameters().put('id',c.Id);
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController stdcontroller = new ApexPages.StandardController(c);
            FamilyTemplateController template = new FamilyTemplateController();
            FamilyMessagesNewController controller = new FamilyMessagesNewController(stdcontroller);
            
            System.assertNotEquals(template.Me, null);
            System.assertEquals(3, controller.ReasonOptions.size());
            
            //System.assertEquals(controller.newCase.ContactId, u.ContactId);
            System.assertEquals(controller.newCase.Origin, 'Family Portal');
            System.assertEquals(controller.newCase.RecordTypeId, RecordTypes.familyPortalCaseTypeId);
        }
        
        Test.stopTest();
    }
    
    @isTest
    private static void TestCancelNewMessage()
    {
        User u = FamilyMessagesNewControllerTest.CreateData();
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        PFS__c pfs = [SELECT Id, Name FROM PFS__c LIMIT 1];

        Test.startTest();

        Case c = new Case();
        
        System.runAs(u) {
            PageReference pageRef = Page.FamilyMessagesNew;
            pageRef.getParameters().put('pfsId',pfs.Id);
            pageRef.getParameters().put('AcademicYearId',academicYearId);
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController stdcontroller = new ApexPages.StandardController(c);
            
            FamilyMessagesNewController controller = new FamilyMessagesNewController(stdcontroller);
    
            System.assertNotEquals(null, controller.CancelNewMessage());
        }
        
        Test.stopTest();
    }
    
    @isTest
    private static void TestSaveNewMessage() {
        TestDataFamily tdf = new TestDataFamily();
        update new List<Contact>{tdf.student1, tdf.student2};
        Account alaska1 = new Account(Name = 'alaska1', BillingCountryCode = 'US', BillingStateCode = 'AK',Billingcity = 'Fairbanks',BillingPostalCode='99701', SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest(), SSS_School_Code__c = 'alask1');
        insert alaska1;
        
        System.runAs(tdf.familyPortalUser) {
            PageReference pageRef = Page.FamilyMessagesNew;
            pageRef.getParameters().put('pfsId',tdf.pfsA.ID);
            pageRef.getParameters().put('AcademicYearId',tdf.academicYearId);
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController stdcontroller = new ApexPages.StandardController(new Case());
            
            FamilyMessagesNewController controller = new FamilyMessagesNewController(stdcontroller);
    
            controller.newCase.Reason = 'I have a question for a school';
            controller.newCase.Subject = 'test';
            controller.newCase.Description = 'test';
            controller.selectedSchools = 'Other';
            System.assertEquals(null, controller.SaveNewMessage());
            System.assertNotEquals(null, apexpages.getMessages());
            System.assertEquals('School Name: You must enter a value', (apexpages.getMessages()[0]).getDetail());
            
            controller.searchSchoolName = 'alaska2';
            controller.dummyContact.MailingCountryCode = 'US';
            controller.dummyContact.MailingStateCode = 'AK';            
            controller.dummyContact.MailingCity = 'Fairbanks';            
            controller.isSpecifiApplicant =true;
            System.assertEquals(null, controller.SaveNewMessage());
            System.assertNotEquals(null, apexpages.getMessages());
            System.assertEquals('We could not find any SSS Subscribers with the given school information.', (apexpages.getMessages()[1]).getDetail());
            
            controller.searchSchoolName = 'alaska1';
            System.assertNotEquals(null, controller.SaveNewMessage());
        }
    }
    
    @isTest
    private static void TestSaveNewMessage2() {
        TestDataFamily tdf = new TestDataFamily();
        update new List<Contact>{tdf.student1, tdf.student2};
        Account alaska1 = new Account(Name = 'alaska1', BillingCountryCode = 'US', BillingStateCode = 'AK',Billingcity = 'Fairbanks',BillingPostalCode='99701', SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest(), SSS_School_Code__c = 'alask1');
        insert alaska1;
        
        System.runAs(tdf.familyPortalUser) {
            PageReference pageRef = Page.FamilyMessagesNew;
            pageRef.getParameters().put('pfsId',tdf.pfsA.ID);
            pageRef.getParameters().put('AcademicYearId',tdf.academicYearId);
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController stdcontroller = new ApexPages.StandardController(new Case());
            
            FamilyMessagesNewController controller = new FamilyMessagesNewController(stdcontroller);
            System.assertNotEquals(null, controller.applicants);
    
            controller.newCase.Reason = 'I have a question for a school';
            controller.newCase.Subject = 'test';
            controller.newCase.Description = 'test';
            controller.selectedSchools = 'alask1 - alaska1';
            controller.isSpecifiApplicant =false;
            System.assertEquals(null, controller.SaveNewMessage());
            System.assertNotEquals(null, apexpages.getMessages());
            System.assertEquals('Please select either "This is not about a specific applicant" or "Applicant"', (apexpages.getMessages()[0]).getDetail());
            
            controller.selectedApplicant = tdf.applicant2A.id;            
            System.assertNotEquals(null, controller.SaveNewMessage());
        }
    }
}