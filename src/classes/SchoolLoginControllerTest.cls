@isTest
private class SchoolLoginControllerTest {
    @IsTest(SeeAllData = true)
    private static void testForgotPasswordController() {
        // Instantiate a new controller with all parameters in the page
        SchoolLoginController controller = new SchoolLoginController();
        PageReference pr = controller.login();
        controller.username = 'test@salesforce.com';
        System.assertEquals(controller.forgotPassword(), null);
    }
}