@isTest
private class StateTaxTableCacheTest
{
    
    @isTest
    private static void testGetTaxTables_queryException()
    {
        Academic_Year__c currentYear = AcademicYearTestData.Instance.DefaultAcademicYear;

        try
        {
            Map<String, List<State_Tax_Table__c>> stateTaxTables = StateTaxTableCache.getTaxTables(currentYear.Id);
        } catch (Exception ex) 
        {
            system.assertEquals(StateTaxTableCache.queryTaxTablesException.replace('{0}', currentYear.Id), ex.getMessage());
        }
    }

    @isTest
    private static void testGetTaxTables_getRecords()
    {
        Id currentYearId = AcademicYearTestData.Instance.DefaultAcademicYear.Id;
        Academic_Year__c priorAcademicYear = AcademicYearTestData.Instance.asPreviousYear().insertAcademicYear();

        State_Tax_Table__c currentYearStateTaxTable1 = StateTaxTableTestData.Instance.forIncomeLow(10).create();
        State_Tax_Table__c currentYearStateTaxTable2 = StateTaxTableTestData.Instance.forIncomeLow(20).create();
        State_Tax_Table__c priorYearStateTaxTable = StateTaxTableTestData.Instance
            .forAcademicYearId(priorAcademicYear.Id).create();
        insert new List<State_Tax_Table__c>{currentYearStateTaxTable1, currentYearStateTaxTable2, priorYearStateTaxTable};

        Test.startTest();
            System.assert(!Cache.Org.contains(StateTaxTableCache.CACHE_KEY_PREFIX + String.valueOf(currentYearId)));
            
            Map<String, List<State_Tax_Table__c>> stateTaxTables = StateTaxTableCache.getTaxTables(currentYearId);
            System.assertEquals(1, stateTaxTables.keySet().size());
            System.assertEquals(2, stateTaxTables.values()[0].size());
            System.assertEquals(currentYearStateTaxTable1.Id, stateTaxTables.values()[0][0].Id);

            //// Clearing of the stateTaxTables Map
            stateTaxTables = new Map<String, List<State_Tax_Table__c>>();

            Integer currentNumberOfSOQLs = Limits.getQueries();
            
            stateTaxTables = StateTaxTableCache.getTaxTables(currentYearId);
            System.assertEquals(currentNumberOfSOQLs, Limits.getQueries(), 
                'Number of soqls didn\'t increase - the tax table was received from the Org Cache but not from the database');

            //// We check that stateTaxTables list still contains the same values as at the previous step
            System.assertEquals(1, stateTaxTables.keySet().size());
            System.assertEquals(2, stateTaxTables.values()[0].size());
            System.assertEquals(currentYearStateTaxTable1.Id, stateTaxTables.values()[0][0].Id);
        Test.stopTest();
    }
}