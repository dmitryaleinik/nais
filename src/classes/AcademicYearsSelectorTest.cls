@isTest
private class AcademicYearsSelectorTest {

    private static void assertStartDateDescendingOrder(List<Academic_Year__c> records) {
        System.assertNotEquals(null, records, 'Expected the records to not be null.');

        Date previousDate;
        for (Academic_Year__c record : records) {
            System.assertNotEquals(null, record.Start_Date__c, 'The start date should not be null for this assertion.');

            // For the first record, the previous date will be null. Set it, and continue to the next record.
            if (previousDate == null) {
                previousDate = record.Start_Date__c;
                continue;
            }

            System.assert(previousDate >= record.Start_Date__c,
                    formatStartDateAssertion(previousDate, record.Start_Date__c));
        }
    }

    private static String formatStartDateAssertion(Date previousRecordDate, Date nextRecordDate) {
        String assertionMessage = 'Expected the previous date to be equal to our after the past date. ' +
                'Previous Date: {0} Next Record Date: {1}';
        return String.format(assertionMessage, new List<String> { String.valueOf(previousRecordDate), String.valueOf(nextRecordDate) });
    }

    private static Map<Id, Academic_Year__c> insertRecords() {
        return new Map<Id, Academic_Year__c>(TestUtils.createAcademicYears());
    }

    @isTest
    private static void selectById_emptySetOfIds_expectEmptyList() {
        Map<Id, Academic_Year__c> recordsById = insertRecords();

        Set<Id> recordIds = new Set<Id>();

        List<Academic_Year__c> queriedRecords = AcademicYearsSelector.newInstance().selectById(recordIds);

        System.assertNotEquals(null, queriedRecords, 'Expected the queried records to not be null.');
        System.assertEquals(0, queriedRecords.size(), 'Expected zero records to be queried.');
    }

    @isTest
    private static void selectById_setWithRecordIds_expectRecordsQueriedInCorrectOrder() {
        Map<Id, Academic_Year__c> recordsById = insertRecords();

        Set<Id> recordIds = recordsById.keySet();

        List<Academic_Year__c> queriedRecords = AcademicYearsSelector.newInstance().selectById(recordIds);

        System.assertNotEquals(null, queriedRecords, 'Expected the queried records to not be null.');
        System.assert(!queriedRecords.isEmpty(), 'Expected records to have actually been queried.');
        System.assertEquals(recordsById.size(), queriedRecords.size(), 'Expected all inserted records to be queried.');
        assertStartDateDescendingOrder(queriedRecords);
    }

    @isTest
    private static void selectAll_noRecordsInserted_expectEmptyList() {
        List<Academic_Year__c> queriedRecords = AcademicYearsSelector.newInstance().selectAll();

        System.assertNotEquals(null, queriedRecords, 'Expected the queried records to not be null.');
        System.assertEquals(0, queriedRecords.size(), 'Expected zero records to be queried.');
    }

    @isTest
    private static void selectAll_recordsInserted_expectRecordsQueriedInCorrectOrder() {
        Map<Id, Academic_Year__c> recordsById = insertRecords();

        List<Academic_Year__c> queriedRecords = AcademicYearsSelector.newInstance().selectAll();

        System.assertNotEquals(null, queriedRecords, 'Expected the queried records to not be null.');
        System.assert(!queriedRecords.isEmpty(), 'Expected records to have actually been queried.');
        System.assertEquals(recordsById.size(), queriedRecords.size(), 'Expected all inserted records to be queried.');
        assertStartDateDescendingOrder(queriedRecords);
    }
}