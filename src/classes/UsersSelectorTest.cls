@isTest
private class UsersSelectorTest {

    @isTest
    private static void selectById_nullIdSet_expectArgumentNullException() {
        try {
            UsersSelector.newInstance().selectById(null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, UsersSelector.ID_SET_PARAM);
        }
    }

    @isTest
    private static void selectById_emptyIdSet_expectEmptyList() {
        Set<Id> userIds = new Set<Id>();

        List<User> users = UsersSelector.newInstance().selectById(userIds);

        System.assertNotEquals(null, users, 'Expected the users to not be null.');
        System.assert(users.isEmpty(), 'Expected the users to be empty.');
    }

    @isTest
    private static void selectById_userRecordsExist_expectUserReturned() {
        User targetUser = UserTestData.insertFamilyPortalUser();

        Set<Id> userIds = new Set<Id> { targetUser.Id };

        List<User> userRecords = UsersSelector.newInstance().selectById(userIds);

        System.assertNotEquals(null, userRecords, 'Expected the user records to not be null.');
        System.assertEquals(1, userRecords.size(), 'Expected the list of records to contain one user.');
        System.assertEquals(targetUser.Id, userRecords[0].Id, 'Expected the inserted user to be queried.');
    }
}