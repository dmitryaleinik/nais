/**
* @SchoolAcademicYearWarning.cls
* @date 5/27/2013 
* @author Charles Howard, Exponent Partners, 2013
* @description Controller for the component that displays a small warning message
*     below the Academic Year Selector in the School portal
*/
public with sharing class SchoolAcademicYearWarning {

    /*Initialization*/
    public SchoolAcademicYearWarning(){

    }
    /*End Initialization*/
    
    /*Properties*/
    public Boolean renderComponent{
        get{
            List<Academic_Year__c> academicYears = GlobalVariables.getCurrentAcademicYears();
        
            if(academicYears != null && academicYears.size() > 1){
                return true;
            }
            else{
                return false;
            }
        } 
        set;
    }
    /*End Properties*/
    
    /*Action Methods*/
    /*End Action Methods*/
    
    /*Helper Methods*/
    /*End Helper Methods*/
}