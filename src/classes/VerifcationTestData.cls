/**
 * @description This class is used to create Verifcation records for unit tests.
 */
@isTest
public class VerifcationTestData extends SObjectTestData {
    /**
     * @description Get the default values for the Verification__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {

        };
    }

    /**
     * @description Insert the current working Verification__c record.
     * @return The currently operated upon Verification__c record.
     */
    public Verification__c insertVerifcation() {
        return (Verification__c)insertRecord();
    }

    /**
     * @description Create the current working Verifcation record without resetting
     *             the stored values in this instance of VerifcationTestData.
     * @return A non-inserted Verification__c record using the currently stored field
     *             values.
     */
    public Verification__c createVerifcationWithoutReset() {
        return (Verification__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Verification__c record.
     * @return The currently operated upon Verification__c record.
     */
    public Verification__c create() {
        return (Verification__c)super.buildWithReset();
    }

    /**
     * @description The default Verification__c record.
     */
    public Verification__c DefaultVerifcation {
        get {
            if (DefaultVerifcation == null) {
                DefaultVerifcation = createVerifcationWithoutReset();
                insert DefaultVerifcation;
            }
            return DefaultVerifcation;
        }
        private set;
    }

    /**
     * @description Get the Verification__c SObjectType.
     * @return The Verification__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Verification__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static VerifcationTestData Instance {
        get {
            if (Instance == null) {
                Instance = new VerifcationTestData();
            }
            return Instance;
        }
        private set;
    }

    private VerifcationTestData() { }
}