/**
 * IntegrationApiCallProvider.cls
 *
 * @description: 
 *
 * @author: Mike Havrila @ Presence PG
 */

public interface IntegrationApiCallProvider {

    //Define the method signature to be implemented in classes that implements the interface
    //Example method
    IntegrationApiCallProvider preProcess( String source, String apiBaseUrl);

  IntegrationApiCallProvider process();

  IntegrationApiCallProvider postProcess();
}