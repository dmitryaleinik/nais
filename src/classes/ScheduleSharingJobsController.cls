global class ScheduleSharingJobsController {
    
    private static final String EFC_CALCULATION_JOB_NAME = 'EFC Calculation Job ';
    private static final String EFC_CALCULATION_JOBS_NAME = 'EFC Calculation jobs';
    public static final String SHARING_JOB_NAME = 'Sharing Job';
    private static final String SHARING_JOBS_NAME = 'Sharing jobs ';
    private static final String SHARING_BATCH_STARTED_MESSAGE = 'Sharing Job execution started successfully.';
    public static final String COPY_VERIFICATION_TO_SCHOOL_PFSA_JOB_NAME = 'Copy Verification To School PFS Assignments Job';
    public static final String DOCUMENT_VERIFICATION_REQUESTS_JOB_NAME = 'Document Verification Requests Job';
    public static final String CLEAR_PAYMENT_INFO_JOB_NAME = 'Clear Payment Info Job';
    public static final String FAMILY_DOCUMENT_CLEANUP_JOB_NAME = 'Family Document cleanup job';
    private static final String DISABLE_PORTAL_ACCESS_FOR_INACTIVE = 'Disable Portal Access for inactive users';
    public static final String DOC_CLEANUP_JOB = 'Doc Cleanup Job';
    public static final String CLEAR_ORPHANED_SDA_JOB_NAME = 'Clear Orphaned SDAs Job';
    public static final String RECALC_SHARING_FOR_MERGED_CONTACTS_JOB_NAME = 'Recalc Sharing For Merged Contacts';

    public static String schedule0 = '0  0 * * * ?';
    public static String schedule1 = '0 30 * * * ?';
    public static String midnightScheduleString = '0 0 0 * * ?'; 

    public static String job0Id;
    public static String job1Id;
    
    public static String accountSchedule0 = '0 0 2 * * ?';
    
    public static String accountJob0Id;
    
    // [SL 8/15/13] NAIS-502: EFC Calculation scheduled jobs should run every 15 minutes 
    public static String efcCalcSchedule0 = '0 5, * * * ?';
    public static String efcCalcSchedule1 = '0 20, * * * ?';
    public static String efcCalcSchedule2 = '0 35, * * * ?';
    public static String efcCalcSchedule3 = '0 50, * * * ?';
    
    public static String disablePortalAccess = '0  0 * * * ?';
    
    public static String efcCalcJob0Id;
    public static String efcCalcJob1Id;
    public static String efcCalcJob2Id;
    public static String efcCalcJob3Id;

    public static String scheduleDocumentCleanupString = '0 59 23 * * ?';
    public Account errorDocsAccount {get; set;}
    public String errorLevel { get; set; }
    public String messageName { get; set; }
	
    public ScheduleSharingJobsController(){
        errorDocsAccount = new Account();
        errorDocsAccount.Profile_Updated__c = Date.newInstance(2017,10,25);
    }
    
    public PageReference schedule() {
        SharingScheduler share = new SharingScheduler();
        
        // schedule 2 jobs to run for every 30 minutes
        try {
            job0Id = System.schedule(SHARING_JOB_NAME + '1', schedule0, share);
            job1Id = System.schedule(SHARING_JOB_NAME + '2', schedule1, share);

            showSuccessfullyScheduledMessage(SHARING_JOBS_NAME);
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        
        return null;
    }

    public PageReference runSharingJob() {
        try {
            Database.executeBatch(new SchoolStaffShareBatch(), 20);

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, SHARING_BATCH_STARTED_MESSAGE));
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        
        return null;
    }
    
    public PageReference scheduleIsPortalEnabled() {
        scheduledDisablePortalAccess scheduler = new scheduledDisablePortalAccess();
        try {
            job0Id = System.schedule(DISABLE_PORTAL_ACCESS_FOR_INACTIVE, disablePortalAccess, scheduler);

            showSuccessfullyScheduledMessage(DISABLE_PORTAL_ACCESS_FOR_INACTIVE);
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        
        return null;
    }
    
    // [SL 8/15/13] NAIS-502 EFC Calculation scheduled jobs
    public PageReference scheduleEfcCalcJobs() {
        // schedule SSS calc to run every 15 minutes
        try {
            doScheduleEfcCalcJobs();

            showSuccessfullyScheduledMessage(EFC_CALCULATION_JOBS_NAME);
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        
        return null;
    }
    
    // [CH] NAIS-1604 Schedules job to clean up Family Document records
    public PageReference scheduleDocumentCleanup(){
        // schedule Family Document cleanup to run every day
        try {
            // Look up lifespan custom setting
            Document_Cleanup__c docCleanupSetting = Document_Cleanup__c.getValues('Family Document Life');
            
            if(docCleanupSetting != null){
                scheduleDocumentCleanupString = docCleanupSetting.Schedule_String__c;
            }
            
            FamilyDocumentCleanupScheduler docCleanup = new FamilyDocumentCleanupScheduler();
    
            System.schedule(DOC_CLEANUP_JOB, scheduleDocumentCleanupString, docCleanup);
            
            showSuccessfullyScheduledMessage(FAMILY_DOCUMENT_CLEANUP_JOB_NAME);
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        
        return null;
    }

    public PageReference scheduleCopyVerificationToSchoolPFSAssignments() {
        try {
            System.schedule(COPY_VERIFICATION_TO_SCHOOL_PFSA_JOB_NAME, midnightScheduleString, new CopyVerifForPfsaBatchScheduler());

            showSuccessfullyScheduledMessage(COPY_VERIFICATION_TO_SCHOOL_PFSA_JOB_NAME);
        } catch(Exception ex) {
            if (ex.getMessage() == getAlreadyScheduledJobErrorMessage(COPY_VERIFICATION_TO_SCHOOL_PFSA_JOB_NAME)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()));
            }
        } 

        return null;
    }

    public PageReference scheduleDocumentVerificationRequests() {
        try {
            System.schedule(DOCUMENT_VERIFICATION_REQUESTS_JOB_NAME, midnightScheduleString, new DocumentVerificationBatchScheduler());

            showSuccessfullyScheduledMessage(DOCUMENT_VERIFICATION_REQUESTS_JOB_NAME);
        } catch(Exception ex) {
            if (ex.getMessage() == getAlreadyScheduledJobErrorMessage(DOCUMENT_VERIFICATION_REQUESTS_JOB_NAME)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()));
            }
        } 

        return null;
    }

    public PageReference scheduleClearPaymentInformationNightly() {
        try {
            System.schedule(CLEAR_PAYMENT_INFO_JOB_NAME, midnightScheduleString, new ClearPaymentInfoBatchScheduler());

            showSuccessfullyScheduledMessage(CLEAR_PAYMENT_INFO_JOB_NAME);
        } catch(Exception ex) {
            if (ex.getMessage() == getAlreadyScheduledJobErrorMessage(CLEAR_PAYMENT_INFO_JOB_NAME)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()));
            }
        } 

        return null;
    }
    
    public PageReference scheduleClearOrphanedSchoolDocumentAssignmentsNightly() {
        try {
            System.schedule(CLEAR_ORPHANED_SDA_JOB_NAME, midnightScheduleString, new ClearOrphanedSDABatchScheduler());

            showSuccessfullyScheduledMessage(CLEAR_ORPHANED_SDA_JOB_NAME);
        } catch(Exception ex) {
            if (ex.getMessage() == getAlreadyScheduledJobErrorMessage(CLEAR_ORPHANED_SDA_JOB_NAME)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()));
            }
        } 

        return null;
    }

    public PageReference scheduleRecalcSharingForMergedContacts()
    {
        try
        {
            System.schedule(RECALC_SHARING_FOR_MERGED_CONTACTS_JOB_NAME, schedule0, new RecalcSharMergedContactsBatchScheduler());

            showSuccessfullyScheduledMessage(RECALC_SHARING_FOR_MERGED_CONTACTS_JOB_NAME);
        }
        catch(Exception ex)
        {
            if (ex.getMessage() == getAlreadyScheduledJobErrorMessage(RECALC_SHARING_FOR_MERGED_CONTACTS_JOB_NAME))
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()));
            }
        }

        return null;
    }

    private static void showSuccessfullyScheduledMessage(String jobName) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, jobName + ' scheduled successfully.'));
    }

    public static String getAlreadyScheduledJobErrorMessage(String jobName) {
        return 'The Apex job named \"' + jobName + '\" is already scheduled for execution.';
    }

    // [SL 8/15/13] NAIS-502 EFC Calculation scheduled jobs
    private static void doScheduleEfcCalcJobs() {
        EfcCalculatorBatchScheduler efcCalc = new EfcCalculatorBatchScheduler();
        
        efcCalcJob0Id = System.schedule(EFC_CALCULATION_JOB_NAME + '1', efcCalcSchedule0, efcCalc);
        
        // schedule the remaining jobs only if not running in test mode, otherwise will run into test limit
        if (!Test.isRunningTest()) {
            efcCalcJob1Id = System.schedule(EFC_CALCULATION_JOB_NAME + '2', efcCalcSchedule1, efcCalc);
            efcCalcJob2Id = System.schedule(EFC_CALCULATION_JOB_NAME + '3', efcCalcSchedule2, efcCalc);
            efcCalcJob3Id = System.schedule(EFC_CALCULATION_JOB_NAME + '4', efcCalcSchedule3, efcCalc);
        }
    }

    webservice static Boolean scheduleJobsRemotely() {
        try {
            SharingScheduler share = new SharingScheduler();
            job0Id = System.schedule(SHARING_JOB_NAME + '1', schedule0, share);
            job1Id = System.schedule(SHARING_JOB_NAME + '2', schedule1, share);
            
            AccountActionScheduler accountStatus = new AccountActionScheduler();
            accountJob0Id = System.schedule('Rollup Action Status', accountSchedule0, accountStatus);
            
            doScheduleEfcCalcJobs(); // [SL 8/15/13] NAIS-502 EFC Calculation scheduled jobs
            
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    webservice static Boolean deleteJobsRemotely() {
        List<CronTrigger> scheduledJobs = [SELECT Id FROM CronTrigger];
        try {
            for (CronTrigger ct : scheduledJobs) {
                System.abortJob(ct.id);
            }
            return true;
        } catch (Exception e) {
            return false;
        }   
    }
    
    //SFP-1822 : Batch Job For Sending Error Notifications For Stuck Documents
    public PageReference runStuckDocsBatch() {  
        Date uploadedSinceDate = Date.newInstance(2017,10,25).addDays(-1);
        if (errorDocsAccount.Total_Admissions_Applications__c == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Days since upload can not be empty.'));
            return null;
        }
        else if (errorDocsAccount.Total_Admissions_Applications__c <= 2) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Days since upload can not be less than 3.'));
            return null;
        } 
        else if (errorDocsAccount.Profile_Updated__c != null && errorDocsAccount.Profile_Updated__c <= uploadedSinceDate) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Uploaded since dates cannot be before 10/25/2017'));
            System.debug(errorDocsAccount.Profile_Updated__c);
            System.debug(uploadedSinceDate);
            return null;
        } 
        else {
            //Call the batch.
            Integer i = Integer.valueOf(errorDocsAccount.Total_Admissions_Applications__c);
            System.debug(i);
            System.debug(errorDocsAccount.Profile_Updated__c.format());
            SendNotificationsForStuckDocsBatch b = new SendNotificationsForStuckDocsBatch(i, errorDocsAccount.Profile_Updated__c);
            try {
                database.executebatch(b,20);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, SHARING_BATCH_STARTED_MESSAGE));

            } catch (Exception e) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            }
        }	
        return null;
    }
	
}