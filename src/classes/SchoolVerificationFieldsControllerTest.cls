/*
* NAIS-357 
* Separate Visualforce page for the verification fields
*
* Nathan, Exponent Partners, 2013
*/
@isTest
private class SchoolVerificationFieldsControllerTest {

    @isTest
    private static void TestW2() {
        String documentType = 'W2';
        School_PFS_Assignment__c spfsa = createAndAssignFamilyDocuments(documentType);

        Test.startTest();
            PageReference verifyPage = Page.SchoolVerificationFields;
            verifyPage.getParameters().put('SchoolPFSAssignmentId', spfsa.Id);
            Test.setCurrentPage(verifyPage);
            SchoolVerificationFieldsController controller = new SchoolVerificationFieldsController();
        Test.stopTest(); 
            
        System.assertEquals(2, controller.sdaListW2.size());
    }

    @isTest
    private static void TestTaxScheduleC() {
        String documentType = 'Tax Schedule C';
        School_PFS_Assignment__c spfsa = createAndAssignFamilyDocuments(documentType);
        
        Test.startTest();
            PageReference verifyPage = Page.SchoolVerificationFields;
            verifyPage.getParameters().put('SchoolPFSAssignmentId', spfsa.Id);
            Test.setCurrentPage(verifyPage);
            SchoolVerificationFieldsController controller = new SchoolVerificationFieldsController();
        Test.stopTest(); 
        
        system.assertEquals(2, controller.sdaListScheduleC.size());
    }
    
    @isTest
    private static void TestVerificationForCurrentAndPreviousYear() {
        School_PFS_Assignment__c spfsa1 = createAndAssignFamilyDocuments(null);

        Contact student1 = [SELECT Id FROM Contact WHERE RecordTypeId = :RecordTypes.studentContactTypeId][0];
        Id parentAId = ContactTestData.Instance.DefaultContact.Id;
        String parentAEmail = 'test@test.com';
        update new Contact(Id = parentAId, Email = parentAEmail);
        
        String prevAcademicYear = GlobalVariables.getPreviousAcademicYearStr(spfsa1.Academic_Year_Picklist__c);
        Academic_Year__c academicYearPrev = [Select Id, Name from Academic_Year__c where Name=:prevAcademicYear limit 1];
        
        Verification__c verification2 = TestUtils.createVerification(true);
        
        String yesOption = 'Yes';
        PFS__c pfs2 = PfsTestData.Instance
            .forAcademicYearPicklist(prevAcademicYear)
            .forParentA(parentAId)
            .forVerification(verification2.Id)
            .forFeeWaived(yesOption)
            .forParentAEmail(parentAEmail)
            .asSubmitted()
            .asVerified().create();
        upsert new List<PFS__c>{new PFS__c (Id = PfsTestData.Instance.DefaultPfs.Id, Parent_A_Email__c = parentAEmail), pfs2};

        Test.startTest();
            Applicant__c applicant2 = TestUtils.createApplicant(student1.Id, pfs2.Id, true);
            
            Student_Folder__c studentFolder2 = TestUtils.createStudentFolder(
                'Student Folder 2', academicYearPrev.Id, student1.Id, true);
            School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(
                academicYearPrev.Id, applicant2.Id, AccountTestData.Instance.DefaultAccount.Id, studentFolder2.Id, true);
            
            School_PFS_Assignment__c testSPA = [
                SELECT Id, School__c, Applicant__r.PFS__r.Parent_A_Email__c 
                FROM School_PFS_Assignment__c
                WHERE Id = :spfsa1.Id
                  LIMIT 1];
            system.assertEquals(true, testSPA.School__c != null);
            system.assertEquals(true, testSPA.Applicant__r.PFS__r.Parent_A_Email__c != null);
            PageReference verifyPage = Page.SchoolVerificationFields;
            verifyPage.getParameters().put('SchoolPFSAssignmentId', spfsa1.Id);
            Test.setCurrentPage(verifyPage);
            SchoolVerificationFieldsController controller = new SchoolVerificationFieldsController();
            
            system.assertEquals(true, controller.pyschoolPFSAssignment != null);
            Academic_Year__c academicYear = GlobalVariables.getCurrentAcademicYear();
            controller.setAcademicYearId(academicYear.Id);
            system.assertEquals(academicYear.Id, controller.getAcademicYearId());
        Test.stopTest(); 
    }

    private static School_PFS_Assignment__c createAndAssignFamilyDocuments(String documentType) {
        AcademicYearTestData.Instance.insertAcademicYears(6);
        Academic_Year__c academicYear = GlobalVariables.getCurrentAcademicYear();
        PFS__c pfs = PfsTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .asVerified().DefaultPfs;
        Family_Document__c familyDoc1 = FamilyDocumentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forHousehold(HouseholdTestData.Instance.DefaultHousehold.Id)
            .forDocumentType(documentType).create();
        Family_Document__c familyDoc2 = FamilyDocumentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forHousehold(HouseholdTestData.Instance.DefaultHousehold.Id)
            .forDocumentType(documentType).create();
        insert new List<Family_Document__c>{familyDoc1, familyDoc2};
        
        Required_Document__c rd1 = RequiredDocumentTestData.Instance.forAcademicYearId(academicYear.Id).create();
        Required_Document__c rd2 = RequiredDocumentTestData.Instance.forAcademicYearId(academicYear.Id).create();
        insert new List<Required_Document__c> {rd1, rd2};

        School_PFS_Assignment__c spfsa = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name).DefaultSchoolPfsAssignment;

        String documentYear = String.valueOf(Date.Today().addYears(-1).year());
        School_Document_Assignment__c sda1 = SchoolDocumentAssignmentTestData.Instance
            .forDocumentYear(documentYear)
            .forDocument(familyDoc1.Id)
            .forPfsAssignment(spfsa.Id)
            .forRequirement(rd1.Id).create();
        School_Document_Assignment__c sda2 = SchoolDocumentAssignmentTestData.Instance
            .forDocumentYear(documentYear)
            .forDocument(familyDoc2.Id)
            .forPfsAssignment(spfsa.Id)
            .forRequirement(rd2.Id).create();
        insert new List <School_Document_Assignment__c> {sda1, sda2};

        return spfsa;
    }
}