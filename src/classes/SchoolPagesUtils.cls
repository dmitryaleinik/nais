/*
* NAIS-2369 School Portal - School Portal Tab Naming
* 
* Leydi, 04-2015
*/
public with sharing class SchoolPagesUtils {
    /*Properties*/
    private static map<String, String> SchoolPagesTitleByName;
    
    public static String getSchoolPageTitle(PageReference currentPage)
    {
        String pageName = getPageName(currentPage);
        
        SchoolPagesTitleByName = new map<String, String>{
                                    'schooldashboard' => 'Dashboard',
                                    'schoolfeewaivers' => 'Fee Waivers {!academicYear}',
                                    'schoolfeewaiversassigned' => 'Assigned Waivers',
                                    'studentfolderlist' => 'My Applicants',
                                    'schoolfoldersummary' => '{!studentName} Folder Summary',
                                    'schoolpfssummary' => '{!studentName} PFS Summary',
                                    'schoolpfsfieldhistory' => '{!studentName} PFS Field History Change',
                                    'professionaljudgmentfieldhistory' => '{!studentName} Professional Judgment Change Log',
                                    'schoolfinancialaid' => '{!studentName} Financial Aid',
                                    'schoolfamilycontributionworksheet' => '{!studentName} Family Contribution Worksheet',
                                    'schoolhouseholdinformation' => '{!studentName} Household Information',
                                    'schoolpfsadditionalinformation'    => '{!studentName} Additional Information',
                                    'schoolw2summary' => '{!studentName} W2 Summary',
                                    'schoolpfsdocument' => '{!studentName} Documents',
                                    'schoolschoolprofilewrapper'    => 'School Profile',
                                    'schoolschoolprofile'    => 'School Profile',
                                    'schoolannualsettings' => 'Annual Settings',
                                    'schoolrequireddocument' => 'Required Documents',
                                    'schooltuitionandfees' => 'Tuition & Fees',
                                    'schoolglobalprojudgment' => 'Professional Judgment',
                                    'schoolschoolbudgeting' => 'Budget Management',
                                    'schoolnonneedbased' => 'Add School-Initiated Folder',
                                    'schooluseradmin' => 'User Administration',
                                    'schoolefcsimulator' => 'EFC Simulator',
                                    'schooluserprofile' => 'My Profile',
                                    'schooluseradminprofile' => (System.currentPageReference().getParameters().get('o')=='1'?
                                                                'My Profile':'User Profile'),
                                    'schoolappsupport' => 'Help',
                                    'schoolfinancialaid' => '{!studentName} Financial Aid',
                                    'studentadvancedlist' => 'Advanced Lists',
                                    'schoolsupportticket' => 'Support Tickets',
                                    'schoollistmessage' => 'Parent Messages',
                                    'schoolverificationfields' => 'Verification Values {!academicYear}',
                                    'schoolmassemaildashboard' => 'Mass Email',
                                    'schoolcommdashboard' => 'Communications',
                                    'schoolmessagedetail' => 'Message Detail',
                                    'schoolmassemailconfig' => 'Mass Email Configuration',
                                    'schoolfamilymonthlyincome' => 'Monthly Income And Expenses'
                                };
        return String.escapeSingleQuotes(SchoolPagesTitleByName!=null && SchoolPagesTitleByName.containsKey(pageName)
                ?replaceVariables(SchoolPagesTitleByName.get(pageName), pageName)
                :'' );
    }//End:getSchoolPageTitle
    /*End Properties*/
    
    /*Action Methods*/
    
    /*End Action Methods*/
    
    /*Helper Methods*/
    
    private static String replaceVariables(String currentText, String pageName)
    {
        String varValue = '';
        if( currentText!=null && currentText!='' )
        {
            if( currentText.contains('{!studentName}') )
            {            
                String id = getCurrentId(pageName);
                varValue = getCurrentStudentName(id, pageName);
                currentText = currentText.replace('{!studentName}', varValue);
            }
            if( currentText.contains('{!academicYear}') )
            {
                currentText = currentText.replace('{!academicYear}', getCurrentAcademicYear(pageName));
            }
        }
        return currentText;        
    }//End:replaceVariables
    
    private static String getCurrentAcademicYear(String pageName)
    {
        String academicYearId = ApexPages.currentPage().getParameters().get('academicyearid');
        String paramId = ApexPages.currentPage().getParameters().get('schoolpfsassignmentid');
        if((academicYearId!=null && academicYearId!='') || (paramId!=null)){
            try{
                if(pageName == 'schoolverificationfields'){
                    School_PFS_Assignment__c pfsAssigment = [select Academic_Year_Picklist__c  
                                                from School_PFS_Assignment__c 
                                                where Id = :paramId limit 1];
                    return pfsAssigment.Academic_Year_Picklist__c;
                }else{
                    Academic_Year__c academicYear = [select Id, Name 
                                                from Academic_Year__c 
                                                where Id=:academicYearId limit 1];
                    return academicYear.Name;
                }
            }catch(Exception e){
                system.debug('Exception[SchoolPagesUtils.getCurrentAcademicYear]: '+e);
            }
        }
        return GlobalVariables.getCurrentAcademicYear().Name;        
    }//End:getCurrentAcademicYear
    
    private static String getCurrentStudentName(String paramId, String pageName)
    {
        if( paramId!=null )
        {
            try{
                if(pageName!='schoolpfsfieldhistory' 
                && (pageName == 'schoolfoldersummary' 
                    || pageName == 'schoolfinancialaid' )){
                    Student_Folder__c folderStudent = [select Id, Student_Name__c  
                                                from Student_Folder__c 
                                                where Id=:paramId limit 1];
                    return folderStudent.Student_Name__c;                    
                }else{
                    School_PFS_Assignment__c pfsAssigment = [select Parent_A_First_Name__c,
                                                Applicant_First_Name__c,
                                                Applicant_Last_Name__c, 
                                                Parent_A_Last_Name__c 
                                                from School_PFS_Assignment__c 
                                                where Id = :paramId limit 1];
                    return pfsAssigment.Applicant_First_Name__c+' '+pfsAssigment.Applicant_Last_Name__c;
                }
            }catch(Exception e){
                system.debug('Exception[SchoolPagesUtils.getCurrentStudentName]: '+e);
            }
        }
        return '';
    }//End:getCurrentStudentName
    
    private static String getCurrentId(String pageName)
    {
        String id = ApexPages.currentPage().getParameters().get('Id');
        try{
            id = (id==null || (id!=null && pageName=='schoolpfsfieldhistory')
                ?ApexPages.currentPage().getParameters().get('schoolpfsassignmentid')
                :id);
        }catch(Exception e){
                system.debug('Exception[SchoolPagesUtils.getCurrentId]: '+e);
        }
        return id;
    }//End:getCurrentId
    
    public static String getPageName(PageReference thisPage)
    {
        String pageName = thisPage.getUrl();
        try{
            pageName = (pageName.contains('apex/')?pageName.split('apex/')[1]:pageName);
            pageName = EncodingUtil.urlEncode(pageName, 'UTF-8');
            string[] pageNameExtra = pageName.split('%3F',0);
            pageName = pageNameExtra[0];
        }catch(Exception e){
                system.debug('Exception[SchoolPagesUtils.getPageName]: '+e);
                return '';
        }
        return pageName.toLowerCase();
    }//End:getPageName
    /*End Helper Methods*/
}