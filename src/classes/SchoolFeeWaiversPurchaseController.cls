/**
 *    NAIS-1877 [jB]
 *    SFP-86 -     Due to conflicts with other work (SFP-81) in the SchoolFeeWaiversPurchaseController
 *                This class is being separated into it's own controller moving methods
 *                from SchoolFeeWaiverPurchaseController here [jB]
 */
public with sharing class SchoolFeeWaiversPurchaseController implements SchoolAcademicYearSelectorInterface {
    public Id mySchoolId { get; private set; }
    public Integer feeWaiversPurchaseQuantity { get; set; }
    public Integer feeWaiverPurchaseTotal {get; set;}
    public Boolean feeWaiversPurchaseQuantityError {get; set;} // set if quantity is greater than 100
    public Academic_Year__c currentAcademicYear { get; private set; }
    public SchoolFeeWaiversPurchaseController Me { get { return this; } }
    public Annual_Setting__c myAnnualSettings { get; set; }
    private Id academicYearId;

    public String getAcademicYearId() {
        return academicYearId;
    }

    public SchoolFeeWaiversPurchaseController() {

    }
    /**
     *
     */
    public void setAcademicYearId(String idValue) {
        // [CH] NAIS-1666 and NAIS-1678
        academicYearId = idValue;
        currentAcademicYear = GlobalVariables.getAcademicYear(idValue);
        // myAcademicYear = [SELECT Id, Name, Start_Date__c FROM Academic_Year__c WHERE Id = :academicYearId];
    }


    public void initCommon() {

        mySchoolId = GlobalVariables.getCurrentSchoolId();
        loadAcademicYear();
        loadAnnualSettings(getAcademicYearId());

        // NAIS-1877
        feeWaiversPurchaseQuantityError = false;
    }

    public Annual_Setting__c loadAnnualSettings(Id academicYearId) {

        List<Annual_Setting__c> annualSettings = GlobalVariables.getCurrentAnnualSettings(true, academicYearId);
        for (Annual_Setting__c setting : annualSettings) {
            if (setting.Academic_Year__c == academicYearId) {
                myAnnualSettings = setting;
            }
        }

        // Check Waivers_Assigned__c and Total_Waivers_Override_Default__c.
        if (myAnnualSettings.Waivers_Assigned__c == null) {
            myAnnualSettings.Waivers_Assigned__c = 0;
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Annual Settings.Waivers Assigned is empty.'));
        }
        if (myAnnualSettings.Total_Waivers_Override_Default__c == null) {
            myAnnualSettings.Total_Waivers_Override_Default__c = 0;
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Annual Settings.Waiver Credit Total is empty.'));
        }

        return myAnnualSettings;
    }

    /**
     *    [jB] NAIS-1877
     */
    public void calculateWaiverPurchaseTotal() {

        feeWaiversPurchaseQuantityError = false;
        try {
            feeWaiversPurchaseQuantity = Integer.valueOf(feeWaiversPurchaseQuantity);
            if (feeWaiversPurchaseQuantity == null || feeWaiversPurchaseQuantity < 0 || feeWaiversPurchaseQuantity > 100) {
                feeWaiversPurchaseQuantityError = true;
                feeWaiverPurchaseTotal = 0;
            } else {
                feeWaiverPurchaseTotal = feeWaiversPurchaseQuantity * Integer.valueOf(getCurrentApplicationFees().Product_Amount__c);
            }
        }    catch (Exception E) {

        }
    }

    public void loadAcademicYear() {

        // Due to the selector_onchange not refreshing properly, we redirect to the same page with academicyearid parameter.
        Map<String, String> parameters = ApexPages.currentPage().getParameters();
        String academicYearId = null;
        if (parameters.containsKey('academicyearid'))
            setAcademicYearId(parameters.get('academicyearid'));
        else {
            // [CH] NAIS-1666
            setAcademicYearId(GlobalVariables.getCurrentAcademicYear().Id);
        }
    }

    /**
    *    [jB] NAIS-1877
    *    Return the current application fee cost calculated from TLI Settings
    *    for current academic year following the same methodlogy as the
    *    PaymentUtils does to create the TLIs
    */
    public Transaction_Annual_Settings__c getCurrentApplicationFees() {

        // Create a map of Transaction_Annual_Settings records that uses the quantity and year as the key
        Map<String, Transaction_Annual_Settings__c> annualSettingsWaiverMap = new Map<String, Transaction_Annual_Settings__c> {};

        // reusing '1 Pack - XXXX' transaction annual settings
        for (Transaction_Annual_Settings__c annualSetting : Transaction_Annual_Settings__c.getAll().values()) {
            annualSettingsWaiverMap.put(annualSetting.Quantity__c + '|' + annualSetting.Year__c, annualSetting);
        }

        try {
            if (annualSettingsWaiverMap.containsKey('1.0|' + currentAcademicYear.Name.split('-')[0])) {
                return annualSettingsWaiverMap.get('1.0|' + currentAcademicYear.Name.split('-')[0]);
            } else {
                return null;
            }
        } catch (Exception e) {
            throw e;
            return null;
        }
    }

    /**
     *
     */
    public PageReference feeWaiverCancel() {
        PageReference pr = Page.SchoolFeeWaivers;
        pr.setRedirect(true);
        return pr;
    }

    /**
     *    [jB] NAIS-1877 Create Opportunity for purchasing Fee Waivers, pass to payment type select page
     */
    public PageReference feeWaiverPurchase() {
        PageReference pr = Page.SchoolFeeWaiverPayment;
        Opportunity opp;

        try {

            opp = new Opportunity();
            opp.RecordTypeId = RecordTypes.opportunityFeeWaiverPurchaseTypeId;
            opp.AccountId = mySchoolId;
            opp.Name = [SELECT Name FROM Account WHERE Id = :mySchoolId].Name + ' Fee Waivers ' + currentAcademicYear.Name;
            opp.CloseDate = Date.today();
            opp.StageName = 'Prospecting';
            opp.Product_Quantity__c = feeWaiversPurchaseQuantity;
            opp.Academic_Year_Picklist__c = currentAcademicYear.Name;

            insert opp;

            pr.getParameters().put('id', opp.id);
            return pr;
        } catch (Exception e) {
            return null;
        }

    }


    public PageReference SchoolAcademicYearSelector_OnChange(Boolean saveRecord) {
        PageReference newPage = new PageReference(ApexPages.currentPage().getUrl());
        newPage.getParameters().put('academicyearid', getAcademicYearId());
        // newPage.getParameters().put('academicyearid', myAcademicYear.Id);
        newPage.setRedirect(true);
        return newPage;
    }


}