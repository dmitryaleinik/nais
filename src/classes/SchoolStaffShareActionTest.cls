/*
 * Spec-134 School Portal - Multiple User Security; Req# R-493
 *    Batch job on School PFS Assignment, Trigger on User and Affiliation to share all PFS and Student Folder records relevant to the school with the School users
 *    for each school they are affiliated with.
 *
 * NAIS-339 Multiple User Security - Account Change
 *    Trigger on Contact to share all PFS and Student Folder records to a school staff user upon change to a different school Account.
 *
 * NAIS-1617 New Process for BulkSharing
 *    Delegate sharing in Trigger on User, Affiliation, and Contact to scheduled batch job on User when asynchronous sharing would have exceeded DML Limit.
 *
 * WH, Exponent Partners, 2013, 2014
 */
@isTest
private class SchoolStaffShareActionTest {
    // [CH] NAIS-1766 Adding to help avoid Mixed DML Errors
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

    // [CH] NAIS-1766 Refactored to fit new sharing expectations
    // assert that there are manual PFS__Share for all these combinations of PFSs and users, and total manual share count
    private static void assertPFSShares(List<Id> pfsIds, List<Id> userIds, Integer expectedCount) {
        List<PFS__Share> shares = [select ParentId, UserOrGroupId, AccessLevel
        from PFS__Share where ParentId in :pfsIds and UserOrGroupId in :userIds and RowCause = 'Manual'];

        System.assertEquals(expectedCount, shares.size());
        for (PFS__Share ps : shares) {
            System.assertEquals('Edit', ps.AccessLevel);
        }
    }

    // [CH] NAIS-1766 Refactored to fit new sharing expectations
    // assert that there are manual Student_Folder__Share for all these combinations of student folders and users, and total manual share count
    private static void assertStudentFolderShares(List<Id> studentFolderIds, List<Id> userIds, Integer expectedCount) {
        List<Student_Folder__Share> shares = [select ParentId, UserOrGroupId, AccessLevel
        from Student_Folder__Share where ParentId in :studentFolderIds and UserOrGroupId in :userIds and RowCause = 'Manual'];

        System.assertEquals(expectedCount, shares.size());
        for (Student_Folder__Share sfs : shares) {
            System.assertEquals('Edit', sfs.AccessLevel);
        }
    }

    // assert that there is no manual PFS__Share for all these combinations of PFSs and users
    private static void assertNoPFSShares(List<Id> pfsIds, List<Id> userIds) {
        System.assertEquals(0,
        [select count() from PFS__Share where ParentId in :pfsIds and UserOrGroupId in :userIds and RowCause = 'Manual']);
    }

    // assert that there is no manual Student_Folder__Share for all these combinations of student folders and users
    private static void assertNoStudentFolderShares(List<Id> studentFolderIds, List<Id> userIds) {
        System.assertEquals(0,
        [select count() from Student_Folder__Share where ParentId in :studentFolderIds and UserOrGroupId in :userIds and RowCause = 'Manual']);
    }

    @isTest
    private static void testShareToAffiliatedGroups() {
        Id schoolProfileId = [select Id from Profile where Name = :ProfileSettings.SchoolUserProfileName limit 1].Id;

        Profile_Type_Grouping__c ptg = new Profile_Type_Grouping__c();
        ptg.Name = 'School Portal User';
        ptg.Is_School_Profile__c = true;
        ptg.Profile_Name__c = ProfileSettings.SchoolUserProfileName;
        insert ptg;

        Account account1 = TestUtils.createAccount('Account 1', RecordTypes.schoolAccountTypeId, 1, false);
        Account account2 = TestUtils.createAccount('Account 1', RecordTypes.schoolAccountTypeId, 1, false);
        Account account3 = TestUtils.createAccount('Account 1', RecordTypes.schoolAccountTypeId, 1, false);
        Account account4 = TestUtils.createAccount('Account 1', RecordTypes.schoolAccountTypeId, 1, false);
        Database.insert(new List<Account>{account1, account2, account3, account4});

        Contact contact1 = TestUtils.createContact('Test User', account1.Id, RecordTypes.schoolStaffContactTypeId, false);
        Database.insert(new List<Contact>{contact1});

        User user1 = TestUtils.createPortalUser('testuser1@testing.stuff', 'testuser1@testing.stuff', 'tst1', contact1.Id, schoolProfileId, true, false);

        System.runAs(thisUser) {
            Database.insert(new List<User>{user1});
        }

        TestUtils.insertAccountShare(contact1.AccountId, user1.Id);

        System.runAs(thisUser) {
            Test.startTest();

            Affiliation__c affiliation1 = new Affiliation__c(Contact__c = contact1.Id, Organization__c = account2.Id, Status__c = 'Current');
            Affiliation__c affiliation2 = new Affiliation__c(Contact__c = contact1.Id, Organization__c = account3.Id, Status__c = 'Current');
            Affiliation__c affiliation3 = new Affiliation__c(Contact__c = contact1.Id, Organization__c = account4.Id, Status__c = 'Current');

            Database.insert(new List<Affiliation__c>{affiliation1, affiliation2, affiliation3});

            Test.stopTest();
        }

        List<String> groupNames = new List<String>();
        groupNames.add('X' + account1.Id );
        groupNames.add('X' + account2.Id );
        groupNames.add('X' + account3.Id );
        groupNames.add('X' + account4.Id );

        // Query GroupMembers
        Map<String, GroupMember> groupMembersCheckMap = new Map<String, GroupMember>();
        for (GroupMember member : [select Id, UserOrGroupId, Group.DeveloperName from GroupMember where Group.DeveloperName in :groupNames and UserOrGroupId = :user1.Id]) {
            groupMembersCheckMap.put(member.UserOrGroupId + '-' + member.Group.DeveloperName, member);
        }

        // There should now be GroupMember records for the account that the contact belongs to
        //  as well as all of the accounts they're affiliated with
        System.assert(groupMembersCheckMap.get(user1.Id + '-' + 'X' + account1.Id) != null);
        System.assert(groupMembersCheckMap.get(user1.Id + '-' + 'X' + account2.Id) != null);
        System.assert(groupMembersCheckMap.get(user1.Id + '-' + 'X' + account3.Id) != null);
        System.assert(groupMembersCheckMap.get(user1.Id + '-' + 'X' + account4.Id) != null);
    }

    // 3 staff members for 2 schools; 2 applicants, 1 with 2 PFS and the other with 1 PFS for 2 schools
    @isTest
    private static void familyRecordsSharedToSchoolStaffOnSPFSAInsert() {
        Account school1 = TestUtils.createAccount('Test School 1', RecordTypes.schoolAccountTypeId, 3, false);
        Account school2 = TestUtils.createAccount('Test School 2', RecordTypes.schoolAccountTypeId, 3, false);
        insert new List<Account> { school1, school2 };

        List<String> groupNames = new List<String>{'X' + school1.Id, 'X' + school2.Id};
        List<Id> groupIds = new List<Id>();
        for (Group groupRecord : [select Id from Group where DeveloperName in :groupNames]) {
            groupIds.add(groupRecord.Id);
        }

        Contact staff1 = TestUtils.createContact('Staff 1', school1.Id, RecordTypes.schoolStaffContactTypeId, false);    // school1
        Contact staff2 = TestUtils.createContact('Staff 2', school1.Id, RecordTypes.schoolStaffContactTypeId, false);    // school1, school2 (aff)
        Contact staff3 = TestUtils.createContact('Staff 3', school2.Id, RecordTypes.schoolStaffContactTypeId, false);    // school2

        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        Contact parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);

        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        Contact student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);

        insert new List<Contact> { staff1, staff2, staff3, parentA, parentB, student1, student2 };

        Id portalProfileId = [select Id from Profile where Name = :ProfileSettings.SchoolAdminProfileName].Id;

        User u1 = TestUtils.createPortalUser('User 1', 'u1@test.org', 'u1', staff1.Id, portalProfileId, true, false);
        User u2 = TestUtils.createPortalUser('User 2', 'u2@test.org', 'u2', staff2.Id, portalProfileId, true, false);
        User u3 = TestUtils.createPortalUser('User 3', 'u3@test.org', 'u3', staff3.Id, portalProfileId, true, false);

        System.runAs(thisUser) {
            insert new List<User> { u1, u2, u3 };

            Affiliation__c aff31 = TestUtils.createAffiliation(staff3.Id, school1.Id, 'Additional School User', 'Former', false);
            Affiliation__c aff22 = TestUtils.createAffiliation(staff2.Id, school2.Id, 'Additional School User', 'Current', false);

            insert new List<Affiliation__c> { aff31, aff22 };
        }

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
        PFS__c pfsB = TestUtils.createPFS('PFS B', academicYearId, parentB.Id, false);
        insert new List<PFS__c> { pfsA, pfsB };

        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, false);
        Applicant__c applicant1B = TestUtils.createApplicant(student1.Id, pfsB.Id, false);
        Applicant__c applicant2A = TestUtils.createApplicant(student2.Id, pfsA.Id, false);
        insert new List<Applicant__c> { applicant1A, applicant1B, applicant2A };

        Student_Folder__c studentFolder11 = TestUtils.createStudentFolder('Student Folder 1.1', academicYearId, student1.Id, false);
        Student_Folder__c studentFolder21 = TestUtils.createStudentFolder('Student Folder 2.1', academicYearId, student2.Id, false);
        Student_Folder__c studentFolder22 = TestUtils.createStudentFolder('Student Folder 2.2', academicYearId, student2.Id, false);
        insert new List<Student_Folder__c> { studentFolder11, studentFolder21, studentFolder22 };

        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder11.Id, false);
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1B.Id, school1.Id, studentFolder11.Id, false);
        School_PFS_Assignment__c spfsa3 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.Id, school1.Id, studentFolder21.Id, false);
        School_PFS_Assignment__c spfsa4 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.Id, school2.Id, studentFolder22.Id, false);
        List<School_PFS_Assignment__c> allSPFSAs = new List<School_PFS_Assignment__c> { spfsa1, spfsa2, spfsa3, spfsa4 };

        System.runAs(thisUser) {
            Test.startTest();
            insert allSPFSAs;
            // call scheduled batch Apex to process all school pfs assignments
            Database.executeBatch(new SchoolStaffShareBatch());
            Test.stopTest();
        }

        // student1 with 2 PFS's apply to school1 -
        //    {pfsA, pfsB} shared to school1 staff {u1, u2}                    (2 x 2 = 4 new PFS__Share)
        //    {studentFolder11} shared to school1 staff {u1, u2}                (1 x 2 = 2 new Student_Folder__Share)
        //    => {spfsa1, spfsa2, applicant1A, applicant1B}
        assertPFSShares(new List<Id> {pfsA.Id, pfsB.Id}, groupIds, 3);
        assertStudentFolderShares(new List<Id> {studentFolder11.Id}, groupIds, 1);

        // student2 with 1 PFS apply to school1 -
        //    {pfsA} shared to school1 staff {u1, u2}                            (verified above)
        //    {studentFolder21} shared to school1 staff {u1, u2}                (1 x 2 = 2 new Student_Folder__Share)
        //    => {spfsa3, applicant2A}
        assertStudentFolderShares(new List<Id> {studentFolder21.Id}, groupIds, 1);

        // student2 with 1 PFS apply to school2 -
        //    {pfsA} shared to school2 staff {u2, u3}                            (1 x 1 = 1 new PFS__Share)
        //    {studentFolder22} shared to school2 staff {u2, u3}                (1 x 2 = 2 new Student_Folder__Share)
        //    => {spfsa4, applicant2A}
        assertPFSShares(new List<Id> {pfsA.Id}, groupIds, 2);
        assertStudentFolderShares(new List<Id> {studentFolder22.Id}, groupIds, 1);

        // u1 in school1 has no acccess to studentFolder22 applied to school2
        assertNoStudentFolderShares(new List<Id> {studentFolder22.Id}, new List<Id> {u1.Id});

        // u3 in school2 has no access to pfsB applied to school1
        assertNoPFSShares(new List<Id> {pfsB.Id}, new List<Id> {u3.Id});

        // u3 in school2 has no access to studentFolder11 and studentFolder21 applied to school1
        assertNoStudentFolderShares(new List<Id> {studentFolder11.Id, studentFolder21.Id}, new List<Id> {u3.Id});

        // school pfs assignments are marked processed
        for (School_PFS_Assignment__c spfsa : [select Sharing_Processed__c from School_PFS_Assignment__c where Id in :allSPFSAs]) {
            System.assertEquals(true, spfsa.Sharing_Processed__c);
        }
    }

    //[CH] NAIS-2291
    @isTest
    private static void testUnwithdrawnSPAs() {
        Account school1 = TestUtils.createAccount('Test School 1', RecordTypes.schoolAccountTypeId, 3, false);
        Account school2 = TestUtils.createAccount('Test School 2', RecordTypes.schoolAccountTypeId, 3, false);
        insert new List<Account> { school1, school2 };

        String groupName = 'X' + school1.Id;
        Id groupId = [select Id from Group where DeveloperName = :groupName limit 1].Id;

        Contact staff1 = TestUtils.createContact('Staff 1', school1.Id, RecordTypes.schoolStaffContactTypeId, false);    // school1
        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        insert new List<Contact> { staff1, parentA, student1 };

        Id portalProfileId = [select Id from Profile where Name = :ProfileSettings.SchoolAdminProfileName].Id;

        User u1 = TestUtils.createPortalUser('User 1', 'u1@test.org', 'u1', staff1.Id, portalProfileId, true, false);
        System.runAs(thisUser) {
            insert new List<User> { u1 };
        }

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, true);

        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, true);

        Student_Folder__c studentFolder11 = TestUtils.createStudentFolder('Student Folder 1.1', academicYearId, student1.Id, true);

        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder11.Id, false);
        spfsa1.Withdrawn__c = 'Yes';
        insert spfsa1;

        System.runAs(thisUser) {
            Test.startTest();
                spfsa1.Withdrawn__c = 'No';
                update spfsa1;

                Database.executeBatch(new SchoolStaffShareBatch());
            Test.stopTest();

            assertPFSShares(new List<Id> {pfsA.Id}, new List<Id> {groupId}, 1);
        }

    }

    /* [CH] NAIS-1766 This is no longer needed as User Inserts are handled synchronously in UserAction.cls
    // 1 new active and 1 inactive staff member for 1 school; 2 applicants, 1 with 2 PFS and the other with 1 PFS for 1 school
    static testMethod void familyRecordsSharedToNewSchoolStaffOnUserInsert() { [Code Removed] }
    */

    /* [CH] NAIS-1766 This is no longer needed as User Inserts are handled synchronously in UserAction.cls
    // 1 reactivated staff member for 1 school; 2 applicants, 1 with 2 PFS and the other with 1 PFS for 1 school
    static testMethod void familyRecordsSharedToNewSchoolStaffOnUserUpdate() { [Code Removed] }
    */

    /* [CH] NAIS-1766 This is no longer needed as sharing based on affiliations is tested testShareToAffiliatedGroups
    // 2 new staff members for 1 school; 2 applicants, 1 with 2 PFS and the other with 1 PFS for 1 school
    static testMethod void familyRecordsSharedToNewSchoolStaffOnAffiliationInsertOrUpdate() { [Code Removed] }
    */

    /* [CH] NAIS-1766 This is no longer needed as it is covered by the ContactActionTest.cls
     * NAIS-339 Multiple User Security - Account Change
    // 1 staff member changing from one school to another; 2 applicants, each with 1 PFS for 1 school
    static testMethod void familyRecordsSharedToSchoolStaffOnContactAccountUpdate() { [Code Removed] }
    */

    /* NAIS-1617 New Process for BulkSharing
     *    DML Limit overridden in SchoolStaffShareAction.cls to speed up testing
     * [CH] NAIS-1766 This is no longer needed as Contact updates are handled synchronously in ContactAction.cls
    // 10 new active staff members for 1 school; 6 applicants, each with 1 PFS for the school
    static testMethod void dmlLimitExceededOnUserInsert() { [Code Removed] }
    */

    /* [CH] NAIS-1766 This is no longer needed as Contact updates are handled synchronously in ContactAction.cls
    // 10 active staff members moved to 1 school; 6 applicants, each with 1 PFS for the school
    static testMethod void dmlLimitExceededOnContactUpdate() { [Code Removed] }
    */

    /* [CH] NAIS-1766 This is no longer needed as SchoolStaffNewUserShareBatch is no longer used.  Users have GroupMember records added to Public Groups
     *                    synchronously in UserAction.cls
    // Delegated from trigger on User, Affiliation, and Contact to scheduled batch job on User when asynchronous sharing would have exceeded DML Limit
    // 2 staff for 1 school, 1 old 1 new flagged for sharing; 2 applications with 1 PFS for school, 1 old 1 new flagged for sharing process
    static testMethod void familyRecordsSharedToNewSchoolStaff() { [Code Removed] }
    */

    /* [CH] NAIS-1766 This is no longer needed as SchoolStaffNewUserShareBatch is no longer used.  Users have GroupMember records added to Public Groups
     *                    synchronously in UserAction.cls
    // Exception handling in batch job on User when asynchronous sharing would have exceeded DML Limit
    //    exact same data as familyRecordsSharedToNewSchoolStaff() except for simulated exception in sharing
    static testMethod void userFlagNotResetWithExceptionInSharingJob() { [Code Removed] }
    */

    // 2 staff for 1 school, 1 old 1 new flagged for sharing; 2 applications with 1 PFS for school, 1 old 1 new flagged for sharing process
    // batch job shares all records to new user, then batch job shares new applications to all users
    @isTest
    private static void familyRecordsSharedFlow1() {
        // suppress User Trigger to avoid @future action sharing records outside the batch job
        SchoolStaffShareAction.suppressUserTrigger = true;

        Account school = TestUtils.createAccount('Test School', RecordTypes.schoolAccountTypeId, 3, true);

        String groupName = 'X' + school.Id;
        Id groupId = [select Id from Group where DeveloperName = :groupName limit 1].Id;

        Contact staff1 = TestUtils.createContact('Staff 1', school.Id, RecordTypes.schoolStaffContactTypeId, false);
        Contact staff2 = TestUtils.createContact('Staff 2', school.Id, RecordTypes.schoolStaffContactTypeId, false);

        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        Contact parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);

        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        Contact student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);

        insert new List<Contact> { staff1, staff2, parentA, parentB, student1, student2 };

        Id portalProfileId = [select Id from Profile where Name = :ProfileSettings.SchoolAdminProfileName].Id;

        User u1 = TestUtils.createPortalUser('User 1', 'u1@test.org', 'u1', staff1.Id, portalProfileId, true, false);    // old user
        User u2 = TestUtils.createPortalUser('User 2', 'u2@test.org', 'u2', staff2.Id, portalProfileId, true, false);    // new user
        u2.Process_Bulk_Share__c = true;

        System.runAs(thisUser) {
            insert new List<User> { u1, u2 };
        }

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
        PFS__c pfsB = TestUtils.createPFS('PFS B', academicYearId, parentB.Id, false);
        insert new List<PFS__c> { pfsA, pfsB };

        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, false);
        Applicant__c applicant2B = TestUtils.createApplicant(student2.Id, pfsB.Id, false);
        insert new List<Applicant__c> { applicant1A, applicant2B };

        Student_Folder__c studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
        Student_Folder__c studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', academicYearId, student2.Id, false);
        insert new List<Student_Folder__c> { studentFolder1, studentFolder2 };

        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school.Id, studentFolder1.Id, false);    // old application
        spfsa1.Sharing_Processed__c = true;
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2B.Id, school.Id, studentFolder2.Id, false);    // new application
        insert new List<School_PFS_Assignment__c> { spfsa1, spfsa2 };

        System.runAs([select Id from User where Id = :UserInfo.getUserId()][0]) {
            Test.startTest();
                // scheduled batch Apex to share all applications to "new" users only
                // NAIS-1766 Database.executeBatch(new SchoolStaffNewUserShareBatch());
                // scheduled batch Apex to share "new" applications only to all users
                Database.executeBatch(new SchoolStaffShareBatch());
            Test.stopTest();
        }

        // assertPFSShares(new List<Id> {pfsA.Id, pfsB.Id}, new List<Id> {u2.Id}, 3);
        assertPFSShares(new List<Id> {pfsB.Id}, new List<Id> {groupId}, 1);
        // assertStudentFolderShares(new List<Id> {studentFolder1.Id, studentFolder2.Id}, new List<Id> {u2.Id}, 3);

        assertStudentFolderShares(new List<Id> {studentFolder2.Id}, new List<Id> {groupId}, 1);

        // old user u1 in school has no acccess to old application (not processed by either batch job)
        assertNoPFSShares(new List<Id> {pfsA.Id}, new List<Id> {groupId});

        assertNoStudentFolderShares(new List<Id> {studentFolder1.Id}, new List<Id> {groupId});

        // flag on new school pfs assignment spfsa2 reset
        for (School_PFS_Assignment__c spfsa : [select Id, Sharing_Processed__c from School_PFS_Assignment__c where Id in (:spfsa1.Id, :spfsa2.Id)]) {
            System.assertEquals(true, spfsa.Sharing_Processed__c);
        }
    }

    // 2 staff for 1 school, 1 old 1 new flagged for sharing; 2 applications with 1 PFS for school, 1 old 1 new flagged for sharing process
    // batch job shares new applications to all users, then batch job shares all records to new user
    //    exact same result as familyRecordsSharedFlow1, regardless of order of execution of batch jobs
    @isTest
    private static void familyRecordsSharedFlow2() {
        // suppress User Trigger to avoid @future action sharing records outside the batch job
        SchoolStaffShareAction.suppressUserTrigger = true;

        Account school = TestUtils.createAccount('Test School', RecordTypes.schoolAccountTypeId, 3, true);

        String groupName = 'X' + school.Id;
        Id groupId = [select Id from Group where DeveloperName = :groupName limit 1].Id;

        Contact staff1 = TestUtils.createContact('Staff 1', school.Id, RecordTypes.schoolStaffContactTypeId, false);
        Contact staff2 = TestUtils.createContact('Staff 2', school.Id, RecordTypes.schoolStaffContactTypeId, false);

        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        Contact parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);

        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        Contact student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);

        insert new List<Contact> { staff1, staff2, parentA, parentB, student1, student2 };

        Id portalProfileId = [select Id from Profile where Name = :ProfileSettings.SchoolAdminProfileName].Id;

        User u1 = TestUtils.createPortalUser('User 1', 'u1@test.org', 'u1', staff1.Id, portalProfileId, true, false);    // old user
        User u2 = TestUtils.createPortalUser('User 2', 'u2@test.org', 'u2', staff2.Id, portalProfileId, true, false);    // new user
        u2.Process_Bulk_Share__c = true;

        System.runAs(thisUser) {
            insert new List<User> { u1, u2 };
        }

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
        PFS__c pfsB = TestUtils.createPFS('PFS B', academicYearId, parentB.Id, false);
        insert new List<PFS__c> { pfsA, pfsB };

        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, false);
        Applicant__c applicant2B = TestUtils.createApplicant(student2.Id, pfsB.Id, false);
        insert new List<Applicant__c> { applicant1A, applicant2B };

        Student_Folder__c studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
        Student_Folder__c studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', academicYearId, student2.Id, false);
        insert new List<Student_Folder__c> { studentFolder1, studentFolder2 };

        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school.Id, studentFolder1.Id, false);    // old application
        spfsa1.Sharing_Processed__c = true;
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2B.Id, school.Id, studentFolder2.Id, false);    // new application
        insert new List<School_PFS_Assignment__c> { spfsa1, spfsa2 };

        System.runAs([select Id from User where Id = :UserInfo.getUserId()][0]) {
            Test.startTest();
            // scheduled batch Apex to share "new" applications only to all users
            Database.executeBatch(new SchoolStaffShareBatch());
            // scheduled batch Apex to share all applications to "new" users only
            //Database.executeBatch(new SchoolStaffNewUserShareBatch());
            Test.stopTest();
        }

        assertPFSShares(new List<Id> {pfsA.Id, pfsB.Id}, new List<Id> {groupId}, 1);
        assertPFSShares(new List<Id> {pfsB.Id}, new List<Id> {groupId}, 1);
        assertStudentFolderShares(new List<Id> {studentFolder1.Id, studentFolder2.Id}, new List<Id> {groupId}, 1);
        assertStudentFolderShares(new List<Id> {studentFolder2.Id}, new List<Id> {groupId}, 1);

        // old user u1 in school has no acccess to old application (not processed by either batch job)
        assertNoPFSShares(new List<Id> {pfsA.Id}, new List<Id> {groupId});
        assertNoStudentFolderShares(new List<Id> {studentFolder1.Id}, new List<Id> {groupId});

        // flag on new school pfs assignment spfsa2 reset
        for (School_PFS_Assignment__c spfsa : [select Id, Sharing_Processed__c from School_PFS_Assignment__c where Id in (:spfsa1.Id, :spfsa2.Id)]) {
            System.assertEquals(true, spfsa.Sharing_Processed__c);
        }
    }

    // [CH] NAIS-2052
    private static void testFolderDeletion() {
        Account school1 = TestUtils.createAccount('Test School 1', RecordTypes.schoolAccountTypeId, 3, false);
        insert new List<Account> { school1 };

        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        Contact parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);

        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        student1.Gender__c='Female';
        insert new List<Contact> { parentA, parentB, student1};

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
        pfsA.Parent_A_Last_Name__c = 'Parent A';
        pfsA.Parent_B_Last_Name__c = 'Parent B';
        pfsA.Filing_Status__c = 'Married, Filing Jointly';
        pfsA.Parent_B__c = parentB.id;
        pfsA.Original_Submission_Date__c = System.today().adddays(-5);

        PFS__c pfsB = TestUtils.createPFS('PFS B', academicYearId, parentB.Id, false);
        pfsB.Parent_A_Last_Name__c = 'Parent B';
        pfsB.Parent_B_Last_Name__c = 'Parent A';
        pfsB.Original_Submission_Date__c = System.today().adddays(-4);
        insert new List<PFS__c> {pfsA, pfsB};

        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, false);
        Applicant__c applicant2A = TestUtils.createApplicant(student1.Id, pfsB.Id, false);
        insert new List<Applicant__c> { applicant1A, applicant2A };

        Student_Folder__c studentFolder11 = TestUtils.createStudentFolder('Student Folder 1.1', academicYearId, student1.Id, true);

        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder11.Id, false);
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.Id, school1.Id, studentFolder11.Id, false);

        insert new List<School_PFS_Assignment__c> { spfsa1, spfsa2 };

        Test.startTest();

        delete spfsa1;

        List<Student_Folder__c> folderVerification = [select Id from Student_Folder__c];
        System.assertEquals(folderVerification.size(), 1);

        delete spfsa2;

        folderVerification = [select Id from Student_Folder__c];
        System.assertEquals(folderVerification, null);

        Test.stopTest();
    }

    @isTest
    private static void shareRecordsWithSchools_schoolFolderPresent_expectShared() {
        SchoolStaffShareAction.suppressUserTrigger = true;
        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        Account school = TestUtils.createAccount('Test School', RecordTypes.schoolAccountTypeId, 3, true);
        Contact student = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, true);
        Student_Folder__c studentFolder = TestUtils.createStudentFolder('Student Folder', academicYearId, student.Id, true);

        Map<Id, Set<Id>> studentFolderBySchoolId = new Map<Id, Set<Id>>();
        studentFolderBySchoolId.put(school.Id, new Set<Id> { studentFolder.Id });

        System.runAs([select Id from User where Id = :UserInfo.getUserId()][0]) {
            Test.startTest();
            SchoolStaffShareAction.shareRecordsWithSchools(new Map<Id, Set<Id>>(), studentFolderBySchoolId, new Map<Id, Set<Id>>(), true);
            Test.stopTest();
        }

        String groupName = 'X' + school.Id;
        Id groupId = [select Id from Group where DeveloperName = :groupName limit 1].Id;

        assertStudentFolderShares(new List<Id> { studentFolder.Id }, new List<Id> { groupId }, 1);
    }

    @isTest
    private static void shareRecordsWithSchools_schoolFolderPresent_expectParentAccountShared() {
        SchoolStaffShareAction.suppressUserTrigger = true;
        Account school1 = TestUtils.createAccount('Test School 1', RecordTypes.schoolAccountTypeId, 3, false);
        insert new List<Account> { school1 };

        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);

        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        student1.Gender__c='Female';
        insert new List<Contact> { parentA, student1};

        Id parentAccountId = [SELECT AccountId FROM Contact WHERE Id = :parentA.Id].AccountId;

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
        pfsA.Parent_A_Last_Name__c = 'Parent A';
        pfsA.Filing_Status__c = 'Single';
        pfsA.Original_Submission_Date__c = System.today().adddays(-5);

        insert new List<PFS__c> { pfsA };

        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, false);
        insert new List<Applicant__c> { applicant1A };

        Student_Folder__c studentFolder11 = TestUtils.createStudentFolder('Student Folder 1.1', academicYearId, student1.Id, true);

        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder11.Id, false);

        insert new List<School_PFS_Assignment__c> { spfsa1 };

        List<School_PFS_Assignment__c> spas = [select Id, Applicant__r.PFS__c, Applicant__r.PFS__r.Parent_A__r.AccountId, School__c, Student_Folder__c from School_PFS_Assignment__c where Sharing_Processed__c = false order by Name asc];
        System.runAs([select Id from User where Id = :UserInfo.getUserId()][0]) {
            Test.startTest();
            SchoolStaffShareAction.shareRecordsToSchoolUsers(spas, null);
            Test.stopTest();
        }

        String groupName = 'X' + school1.Id;
        Id groupId = [select Id from Group where DeveloperName = :groupName limit 1].Id;

        System.assertNotEquals(null, parentAccountId, 'Expected parentA to have AccountId populated');
        List<AccountShare> shares = [SELECT AccountId FROM AccountShare WHERE AccountId = :parentAccountId AND UserOrGroupId = :groupId];
        System.assertEquals(1, shares.size(), 'Expected parents account to be shared out with school group');

    }

}