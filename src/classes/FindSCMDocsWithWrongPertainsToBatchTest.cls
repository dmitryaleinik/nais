@isTest
private class FindSCMDocsWithWrongPertainsToBatchTest {

    @isTest
    private static void constructor_parameterless_expectNoErrors() {
        FindSCMDocsWithWrongPertainsToBatch batchToTest = new FindSCMDocsWithWrongPertainsToBatch();

        System.assertNotEquals(null, batchToTest, 'Expected the batch to be created without errors.');
    }

    @isTest
    private static void constructor_validParams_expectNoErrors() {
        Set<String> docTypes = new Set<String> { 'W2' };
        Set<String> docSources = new Set<String> { 'Uploaded by Parent' };
        Date createdAfterDate = FindSCMDocsWithWrongPertainsToBatch.SCALEHUB_GO_LIVE.addDays(10);

        FindSCMDocsWithWrongPertainsToBatch batchToTest =
                new FindSCMDocsWithWrongPertainsToBatch(docTypes, docSources, createdAfterDate);

        System.assertNotEquals(null, batchToTest, 'Expected the batch to be created without errors.');
    }

    @isTest
    private static void constructor_nullDocSources_expectError() {
        Set<String> docTypes = new Set<String> { 'W2' };
        Set<String> docSources = null;
        Date createdAfterDate = Date.today().addDays(-10);

        try {
            FindSCMDocsWithWrongPertainsToBatch batchToTest =
                    new FindSCMDocsWithWrongPertainsToBatch(docTypes, docSources, createdAfterDate);

            ExceptionTestUtils.expectException(ArgumentNullException.class);
        } catch (Exception ex) {
            ExceptionTestUtils.assertArgumentNullException(ex, FindSCMDocsWithWrongPertainsToBatch.DOC_SOURCES_TO_CHECK_PARAM);
        }
    }

    @isTest
    private static void constructor_nullDocTypes_expectError() {
        Set<String> docTypes = null;
        Set<String> docSources = new Set<String> { 'Mailed' };
        Date createdAfterDate = Date.today().addDays(-10);

        try {
            FindSCMDocsWithWrongPertainsToBatch batchToTest =
                    new FindSCMDocsWithWrongPertainsToBatch(docTypes, docSources, createdAfterDate);

            ExceptionTestUtils.expectException(ArgumentNullException.class);
        } catch (Exception ex) {
            ExceptionTestUtils.assertArgumentNullException(ex, FindSCMDocsWithWrongPertainsToBatch.DOC_TYPES_TO_CHECK_PARAM);
        }
    }

    @isTest
    private static void constructor_nullCreatedAfterDate_expectError() {
        Set<String> docTypes = new Set<String> { 'W2' };
        Set<String> docSources = new Set<String> { 'Mailed' };
        Date createdAfterDate = null;

        try {
            FindSCMDocsWithWrongPertainsToBatch batchToTest =
                    new FindSCMDocsWithWrongPertainsToBatch(docTypes, docSources, createdAfterDate);

            ExceptionTestUtils.expectException(ArgumentNullException.class);
        } catch (Exception ex) {
            ExceptionTestUtils.assertArgumentNullException(ex, FindSCMDocsWithWrongPertainsToBatch.CREATED_ON_OR_AFTER_PARAM);
        }
    }

    @isTest
    private static void constructor_emptyDocSources_expectError() {
        Set<String> docTypes = new Set<String> { 'W2' };
        Set<String> docSources = new Set<String>();
        Date createdAfterDate = Date.today().addDays(-10);

        try {
            FindSCMDocsWithWrongPertainsToBatch batchToTest =
                    new FindSCMDocsWithWrongPertainsToBatch(docTypes, docSources, createdAfterDate);

            ExceptionTestUtils.expectException(FindSCMDocsWithWrongPertainsToBatch.ParamException.class);
        } catch (Exception ex) {
            ExceptionTestUtils.assertException(FindSCMDocsWithWrongPertainsToBatch.ParamException.class, ex);
        }
    }

    @isTest
    private static void constructor_emptyDocTypes_expectError() {
        Set<String> docTypes = new Set<String>();
        Set<String> docSources = new Set<String> { 'Mailed' };
        Date createdAfterDate = Date.today().addDays(-10);

        try {
            FindSCMDocsWithWrongPertainsToBatch batchToTest =
                    new FindSCMDocsWithWrongPertainsToBatch(docTypes, docSources, createdAfterDate);

            ExceptionTestUtils.expectException(FindSCMDocsWithWrongPertainsToBatch.ParamException.class);
        } catch (Exception ex) {
            ExceptionTestUtils.assertException(FindSCMDocsWithWrongPertainsToBatch.ParamException.class, ex);
        }
    }

    @isTest
    private static void constructor_createdAfterDateBeforeScalehubGoLive_expectError() {
        Set<String> docTypes = new Set<String> { 'W2' };
        Set<String> docSources = new Set<String> { 'Mailed' };
        Date createdAfterDate = FindSCMDocsWithWrongPertainsToBatch.SCALEHUB_GO_LIVE.addDays(-10);

        try {
            FindSCMDocsWithWrongPertainsToBatch batchToTest =
                    new FindSCMDocsWithWrongPertainsToBatch(docTypes, docSources, createdAfterDate);

            ExceptionTestUtils.expectException(FindSCMDocsWithWrongPertainsToBatch.ParamException.class);
        } catch (Exception ex) {
            ExceptionTestUtils.assertException(FindSCMDocsWithWrongPertainsToBatch.ParamException.class, ex);
        }
    }
}