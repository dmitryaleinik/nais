/**
 * Encapsulates all behaviour logic relating to the Article_Disposition_Mapping__c object
 **/
public class ArticleDispositionMappings extends fflib_SObjectDomain 
{

    public ArticleDispositionMappings(List<Article_Disposition_Mapping__c> records) 
    {
        super(records); 
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable
    {
        public fflib_SObjectDomain construct(List<SObject> sObjectList)
        {
            return new ArticleDispositionMappings(sObjectList);
        }
    }

    public override void onBeforeInsert()
    {
        checkArticleExistenceForEachRecord(records);
    }

    public override void onBeforeUpdate(Map<Id, SObject> oldRecords)
    {
        checkArticleExistenceForEachRecord(records);
    }


    /* Helper methods */

    private static void checkArticleExistenceForEachRecord(List<Article_Disposition_Mapping__c> records)
    {
        Set<String> inputtedArticleNumbers = new Set<String>{};
        Map<String, Article_Disposition_Mapping__c> articleNumberToADMmapping = new Map<String, Article_Disposition_Mapping__c>();
        
        // Check if there are any records with empty ArticleNumber field in the list
        for (Article_Disposition_Mapping__c record : records)
        {
            // Show the appropriate error for these records if 'Yes'
            if (String.isBlank(record.Article_Number__c))
            {
                record.addError(Label.Empty_Knowledge_Article_Number + record.Name);
            }
            else
            {
                inputtedArticleNumbers.add(record.Article_Number__c);
                articleNumberToADMmapping.put(record.Article_Number__c, record);
            }
        }
        // If ArticleNumbers of all records are empty, skip the following logic
        if (inputtedArticleNumbers.isEmpty())
        {
            return;
        }

        // Get all KnowledgeArticle records by existing ArticleNumbers from the ADM records list
        List<KnowledgeArticle> selectedArticles = KnowledgeArticleSelector.newInstance()
            .selectByArticleNumber(inputtedArticleNumbers);
        
        // If there are no KnowledgeArticle records with such ArticleNumbers in the database - show approprate
        // error message for these adm records and skip the following logic 
        if (selectedArticles.isEmpty())
        {
            for (String key : articleNumberToADMmapping.keySet())
            {
                articleNumberToADMmapping.get(key).addError(
                    Label.Knowledge_Article_Not_Found + articleNumberToADMmapping.get(key).Article_Number__c);
            }
            return;
        }

        Set<String> selectedArticleNumbers = new Set<String>{};
        for (KnowledgeArticle selectedArticle : selectedArticles)
        {
            selectedArticleNumbers.add(selectedArticle.ArticleNumber);
        }

        // Check that all ADM records to insert/update with populated Article_Number__c fields have corresponding
        // KnowledgeArticles in the database. If 'No' - show appropriate error message
        for (String key : articleNumberToADMmapping.keySet())
        {
            if (!selectedArticleNumbers.contains(key))
            {
                articleNumberToADMmapping.get(key).addError(
                    Label.Knowledge_Article_Not_Found + articleNumberToADMmapping.get(key).Article_Number__c);
            }
        }
    }
}