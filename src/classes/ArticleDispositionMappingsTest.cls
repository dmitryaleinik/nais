@isTest
private class ArticleDispositionMappingsTest
{
    
    @isTest
    private static void testUpdateADMwithNullArticleNumber()
    {
        Article_Disposition_Mapping__c admRecord = ArticleDispositionMappingTestData.Instance.DefaultArticleDispositionMapping;
        admRecord.Article_Number__c = null;
        
        Test.startTest();
            try
            {
                update admRecord;
            }
            catch (Exception ex)
            {
                System.assert(ex.getMessage().contains(Label.Empty_Knowledge_Article_Number));
            }
        Test.stopTest();
    }

    @isTest
    private static void testInsertADM_NullArticleNumber()
    {
        Integer numberOfAdms = 5;
        List<Article_Disposition_Mapping__c> admRecords = new List<Article_Disposition_Mapping__c>();
        
        for (Integer i=0; i<numberOfAdms; i++)
        {
            Article_Disposition_Mapping__c adm = ArticleDispositionMappingTestData.Instance
                .forArticleNumber(null).create();
            admRecords.add(adm);
        }

        Test.startTest();
            try
            {
                insert admRecords;
            }
            catch (Exception ex)
            {
                System.assert(ex.getMessage().contains(Label.Empty_Knowledge_Article_Number));

            }
        Test.stopTest();
    }

    @isTest
    private static void testInsertADM_EmptyArticleNumber()
    {
        Integer numberOfAdms = 5;
        List<Article_Disposition_Mapping__c> admRecords = new List<Article_Disposition_Mapping__c>();
        
        for (Integer i=0; i<numberOfAdms; i++)
        {
            Article_Disposition_Mapping__c adm = ArticleDispositionMappingTestData.Instance
                .forArticleNumber('').create();
            admRecords.add(adm);
        }

        Test.startTest();
            try
            {
                insert admRecords;
            }
            catch (Exception ex)
            {
                System.assert(ex.getMessage().contains(Label.Empty_Knowledge_Article_Number));

            }
        Test.stopTest();
    }

    @isTest
    private static void testInsertADM_incorrectArticleNumber()
    {
        Integer numberOfAdms = 5;
        String testArticleNumber = 'TestNumber';
        List<Article_Disposition_Mapping__c> admRecords = new List<Article_Disposition_Mapping__c>();
        
        for (Integer i=0; i<numberOfAdms; i++)
        {
            Article_Disposition_Mapping__c adm = ArticleDispositionMappingTestData.Instance
                .forArticleNumber(testArticleNumber).create();
            admRecords.add(adm);
        }

        Test.startTest();
            try
            {
                insert admRecords;
            }
            catch (Exception ex)
            {
                System.assert(ex.getMessage().contains(Label.Knowledge_Article_Not_Found));

            }
        Test.stopTest();
    }

    @isTest
    private static void testInsertADM_recordsInsertedSucessfully()
    {
        Integer numberOfAdms = 5;
        List<Knowledge__kav> knowledges = KnowledgeAVTestData.Instance.insertKnowledgeAVs(numberOfAdms);
        List<KnowledgeArticle> articles = KnowledgeArticleSelector.newInstance().selectAll();
        List<Article_Disposition_Mapping__c> admRecords = new List<Article_Disposition_Mapping__c>();
        Set<String> allArticleNumbers = new Set<String>{};
        
        for (Integer i=0; i<numberOfAdms; i++)
        {
            Article_Disposition_Mapping__c adm = ArticleDispositionMappingTestData.Instance
                .forArticleNumber(articles[i].ArticleNumber).create();
            admRecords.add(adm);

            allArticleNumbers.add(articles[i].ArticleNumber);
        }

        Test.startTest();
            insert admRecords;

            admRecords = ArticleDispositionMappingSelector.newInstance().selectByArticleNumber(allArticleNumbers);
            System.assertEquals(5, admRecords.size());
            for (Article_Disposition_Mapping__c admRecord : admRecords)
            {
                System.assert(allArticleNumbers.contains(admRecord.Article_Number__c));
            }
        Test.stopTest();
    }

    @isTest
    private static void testInsertADM_partOfAdmsWithWrongArticleNumbers()
    {
        Integer numberOfAdms = 5;
        List<Knowledge__kav> knowledges = KnowledgeAVTestData.Instance.insertKnowledgeAVs(numberOfAdms);
        List<KnowledgeArticle> articles = KnowledgeArticleSelector.newInstance().selectAll();
        List<Article_Disposition_Mapping__c> admRecords = new List<Article_Disposition_Mapping__c>();
        Set<String> allArticleNumbers = new Set<String>{};
        
        for (Integer i=0; i<numberOfAdms; i++)
        {
            Article_Disposition_Mapping__c adm = ArticleDispositionMappingTestData.Instance
                .forArticleNumber(articles[i].ArticleNumber).create();
            admRecords.add(adm);

            allArticleNumbers.add(articles[i].ArticleNumber);
        }

        String wrongArticleNumber = 'TestNumber';
        admRecords[3].Article_Number__c = wrongArticleNumber;
        allArticleNumbers.add(wrongArticleNumber);

        Test.startTest();
            try
            {
                insert admRecords;
            }
            catch (Exception ex)
            {
                admRecords = ArticleDispositionMappingSelector.newInstance().selectByArticleNumber(allArticleNumbers);
                System.assert(admRecords.isEmpty());
                System.assert(ex.getMessage().contains(Label.Knowledge_Article_Not_Found + wrongArticleNumber));
            }
        Test.stopTest();
    }
}