@isTest
private class FamilyAppKSApplicantInfoContTest {

    private static Account school1;
    private static Account school2;
    private static Student_Folder__c studentFolder11;
    private static Student_Folder__c studentFolder21;
    private static PFS__c pfsA;
    private static Applicant__c applicant1A;
    private static Applicant__c applicant2A;

    static void SetupData()
    {
        //setup PFS
        // create test data
        TestUtils.createAcademicYears();
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        
        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, true);
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, true);
        
        Contact student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, true);
        studentFolder11 = TestUtils.createStudentFolder('Student Folder 1.1', academicYearId, student1.Id, true);
        studentFolder21 = TestUtils.createStudentFolder('Student Folder 2.1', academicYearId, student2.Id, true);
        
        school1 = TestUtils.createAccount('KS School', RecordTypes.schoolAccountTypeId, 3, true);
        school2 = TestUtils.createAccount('Non KS School', RecordTypes.schoolAccountTypeId, 3, true);
        school1.SSS_Subscriber_Status__c = 'Current';
        school2.SSS_Subscriber_Status__c = 'Current';
        update new List<Account> { school1, school2 }; 
        
        pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, true);
        applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, true);
        applicant2A = TestUtils.createApplicant(student2.Id, pfsA.Id, true);
        School_PFS_Assignment__c assignment1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.id, school1.id, studentFolder11.id, true);
        School_PFS_Assignment__c assignment2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.id, school2.id, studentFolder21.id, true);

        assignment1.Withdrawn__c = 'No';
        assignment2.Withdrawn__c = 'No';
        
        update new List<School_PFS_Assignment__c> { assignment1, assignment2 } ;
        //reload pfs
        String pfsSql = 'SELECT ' + String.Join(ExpCore.GetAllFieldNames('PFS__c'), ',') + ', (SELECT ' + String.Join(ExpCore.GetAllFieldNames('Applicant__c'), ',') + ' FROM Applicants__r) FROM PFS__c LIMIT 1';
        pfsA = Database.query(pfsSql);
        
        SchoolPortalSettings.KS_School_Account_Id = school1.id;        
    }
    
    @isTest
    private static void TestUpdateBirthParentsOnApplication()
    {
        FamilyAppKSApplicantInfoContTest.SetupData();
        
        FamilyAppKSApplicantInfoCont controller = new FamilyAppKSApplicantInfoCont();
        controller.pfs = pfsA;
        controller.currentApplicantId = pfsA.Applicants__r[0].Id;
        controller.birthParentsOnApplication = 'Yes';
        
        System.assert(controller.UpdateBirthParentsOnApplication() == null);
        System.assertEquals(controller.pfs.Applicants__r[0].Birth_Parents_Included_in_Application__c, 'Yes');
    }
    
    @isTest
    private static void TestRetrieveApplicants()
    {
        FamilyAppKSApplicantInfoContTest.SetupData();
        
        FamilyAppKSApplicantInfoCont controller = new FamilyAppKSApplicantInfoCont();
        controller.pfs = pfsA;
        
        System.assertEquals(1, controller.ksApplicants.size());
        System.assertEquals(applicant1A.id, controller.ksApplicants[0].id);
    }    
    
    @isTest
    private static void TestUpdateParentsMaritalStatus()
    {
        FamilyAppKSApplicantInfoContTest.SetupData();
        
        FamilyAppKSApplicantInfoCont controller = new FamilyAppKSApplicantInfoCont();
        controller.pfs = pfsA;
        controller.currentApplicantId = pfsA.Applicants__r[0].Id;
        controller.parentsMaritalStatus = 'Married';
        
        System.assert(controller.UpdateParentsMaritalStatus() == null);
        System.assertEquals(controller.pfs.Applicants__r[0].Birth_Parents_Marital_Status__c, 'Married');
    }
    
    @isTest
    private static void TestPopulateMailAddress()
    {
        FamilyAppKSApplicantInfoContTest.SetupData();
        system.debug(pfsA);
        
        FamilyAppKSApplicantInfoCont controller = new FamilyAppKSApplicantInfoCont();
        controller.pfs = pfsA;
        
        controller.pfs.KS_Mail_Household__c = true;
        controller.populateMail();
        
        System.assertEquals(controller.pfs.KS_Household_Address__c, controller.pfs.KS_Mailing_Address__c);
        System.assertEquals(controller.pfs.KS_Household_City__c, controller.pfs.KS_Mailing_City__c);
        System.assertEquals(controller.pfs.KS_Household_State_Province__c, controller.pfs.KS_Mailing_State_Province__c);
        System.assertEquals(controller.pfs.KS_Household_Zip_Postal_Code__c, controller.pfs.KS_Mailing_Zip_Postal_Code__c);
        System.assertEquals(controller.pfs.KS_Household_Country__c, controller.pfs.KS_Mailing_Country__c);
    }
}