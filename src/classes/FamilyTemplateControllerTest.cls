@isTest
private class FamilyTemplateControllerTest
{

    private static User CreateData() {
        // create test data
        TestUtils.createAcademicYears();
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        
        Id portalProfileId = GlobalVariables.familyPortalProfileId;

        Account account1 = TestUtils.createAccount('individual', RecordTypes.individualAccountTypeId, 3, true);
        Contact parentA = TestUtils.createContact('Parent A', account1.Id, RecordTypes.parentContactTypeId, true);
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User u = TestUtils.createPortalUser('User 1', 'u1@test.org', 'u1', parentA.Id, portalProfileId, true, true);
        
        System.runAs(u){
            PFS__c pfs1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
            Dml.WithoutSharing.insertObjects(new List<PFS__c>{ pfs1});
        }
        return u;
    }
    
    @isTest
    private static void loadTemplateMessages()
    {
        User u = CreateData();
        Test.startTest();
        System.Runas(u) {
            PageReference pageRef = Page.FamilyMessages;
            String pfsId = [SELECT Id, Name FROM PFS__c LIMIT 1].Id;
            pageRef.getParameters().put('id',pfsId);
            pageRef.getParameters().put('AcademicYearId',GlobalVariables.getCurrentAcademicYear().Id);
            Test.setCurrentPage(pageRef);
            FamilyTemplateController template = new FamilyTemplateController();
            FamilyMessagesController controller = new FamilyMessagesController(template);
            system.assertEquals(template.pfsId, pfsId);
            system.assertEquals(FamilyTemplateController.getInstance(), template);
        }
        Test.stopTest();
    }
    
    @isTest
    private static void loadTemplateDashboard()
    {
        User u = CreateData();
        Test.startTest();
        System.Runas(u) {
            PageReference pageRef = Page.FamilyDashboard;
            Test.setCurrentPage(pageRef);
            FamilyTemplateController template = new FamilyTemplateController();
            FamilyDashboardController controller = new FamilyDashboardController(template);
            system.assertEquals(true, controller.pfsRecord!=null);
            system.assertEquals(true, controller.pfsRecord.Id!=null);
            controller.pageLoad();
            system.assertEquals(template.pfsRecord.Id, controller.pfsRecord.Id);
            system.assertEquals(template.pfsRecord.Id, controller.pfsId);
            system.assertEquals(FamilyTemplateController.getInstance(), template);
            Test.setCurrentPage(Page.FamilyPayment);
            template = new FamilyTemplateController();
            controller = new FamilyDashboardController(template);
            system.assertEquals('/apex/familypayment?academicyearid='+template.getAcademicYearId(), (template.YearSelector_OnChange(template.getAcademicYearId())).getURL());
            system.assertEquals('FamilyTemplate',template.getTemplate());
        }
        Test.stopTest();
    }
}