// Spec-105 School Portal - School Budgeting
// BR, Exponent Partners, 2013
@isTest
private class SchoolSchoolBudgetingControllerTest {

    private static final Decimal BUDGET_TOTAL = 10000;
    private static User schoolPortalUser;
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    private static Academic_Year__c currentAcademicYear, previousAcademicYear;
    private static Account school;
    private static Budget_Group__c budget1, budget2, budget3;

    private static void createTestData() {
        Account family = AccountTestData.Instance.asFamily().create();
        school = AccountTestData.Instance.asSchool().create();
        insert new List<Account>{family, school};

        List<Academic_Year__c> academicYears = GlobalVariables.getAllAcademicYears();
        currentAcademicYear = GlobalVariables.getCurrentAcademicYear();
        previousAcademicYear = GlobalVariables.getPreviousAcademicYear();

        createSssConstants();

        Contact parentA = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(school.Id).create();
        Contact student1 = ContactTestData.Instance
            .asStudent()
            .forAccount(family.Id).create();
        insert new List<Contact> {schoolStaff, parentA, student1};

        System.runAs(thisUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);

        createBudgets();
    }

    @isTest
    private static void testBudgetGroupInit() {
        createTestData();

        System.runAs(schoolPortalUser) {
            Test.setCurrentPage(Page.SchoolSchoolBudgeting);

            Test.startTest();
                SchoolSchoolBudgetingController ext = new SchoolSchoolBudgetingController();
            Test.stopTest();
            
            System.assertEquals(3, ext.myBudgetGroups.size());
        }
    }
    
    @isTest
    private static void testBudgetNewAndCancel() {
        createTestData();
        
        System.runAs(schoolPortalUser) {
            Test.setCurrentPage(Page.SchoolSchoolBudgeting);

            Test.startTest();
                SchoolSchoolBudgetingController ext = new SchoolSchoolBudgetingController();

                ext.newbutton();
                ext.cancel();
            Test.stopTest();

            // return to New Budget state
            System.assertEquals(null, ext.myBudgetGroup.Id);
        }
    }   
       
    @isTest
    private static void testBudgetEditAndSave() {
        createTestData();
        
        System.runAs(schoolPortalUser) {
            Test.setCurrentPage(Page.SchoolSchoolBudgeting);

            Test.startTest();
                SchoolSchoolBudgetingController ext = new SchoolSchoolBudgetingController();

                ext.selectedBudGrp = budget1.Id;
                ext.editlink();
                // We could update a field and check after save but that doesn't test much.
                ext.savebutton();
            Test.stopTest();            

            // return to New Budget state
            System.assertEquals(null, ext.myBudgetGroup.Id);
        }
    }
       
    @isTest
    private static void testBudgetDelete() {
        createTestData();
        
        System.runAs(schoolPortalUser) {
            Test.setCurrentPage(Page.SchoolSchoolBudgeting);

            Test.startTest();
                SchoolSchoolBudgetingController ext = new SchoolSchoolBudgetingController();

                ext.recordToDelete = budget3.Id;
                ext.deletelink();
            Test.stopTest();            

            System.assertEquals(2, ext.myBudgetGroups.size());
        }
    }

    @isTest
    private static void testBudgetCopy() {
        createTestData();

        system.runAs(schoolPortaluser){
            Test.setCurrentPage(Page.SchoolSchoolBudgeting);
            
            Test.startTest();            
                createPreviousYearBudgetGroup();
                SchoolSchoolBudgetingController controller = new SchoolSchoolBudgetingController();
                system.assertEquals(3, controller.myBudgetGroups.size());
                system.assertEquals(false, controller.isCopyPriorYear);

                controller.copyPreviousYear();
                system.assertEquals(4, controller.myBudgetGroups.size());
                
                controller.getBudgetGroupWrappers();
                controller.reportlink();
                controller.getAcademicYearId();
                controller.setAcademicYearId(currentAcademicYear.Id);
                controller.SchoolAcademicYearSelector_OnChange(false);
            Test.stopTest();
        }
    }

    private static void createPreviousYearBudgetGroup() {
        Budget_Group__c budget =  BudgetGroupTestData.Instance
            .forAcademicYearId(previousAcademicYear.Id)
            .forSchoolId(school.Id)
            .forTotalFunds(BUDGET_TOTAL).insertBudgetGroup();
    }

    private static void createBudgets() {
        budget1 =  BudgetGroupTestData.Instance
            .forAcademicYearId(currentAcademicYear.Id)
            .forSchoolId(school.Id)
            .forTotalFunds(BUDGET_TOTAL).create();
        budget2 =  BudgetGroupTestData.Instance
            .forAcademicYearId(currentAcademicYear.Id)
            .forSchoolId(school.Id)
            .forTotalFunds(BUDGET_TOTAL).create();
        budget3 =  BudgetGroupTestData.Instance
            .forAcademicYearId(currentAcademicYear.Id)
            .forSchoolId(school.Id)
            .forTotalFunds(BUDGET_TOTAL).create();
        insert new List<Budget_Group__c> { budget1, budget2, budget3};
    }

    private static void createSssConstants() {
        SSS_Constants__c sssConst1 = SssConstantsTestData.Instance
            .forAcademicYearId(currentAcademicYear.Id).create();
        SSS_Constants__c sssConst2 = SssConstantsTestData.Instance
            .forAcademicYearId(previousAcademicYear.Id).create();
        insert new List<SSS_Constants__c> {sssConst1, sssConst2};
    }
}