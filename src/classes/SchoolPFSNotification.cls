/*
 * Spec-096 School Portal - School PFS - Notification, Req# R-425, R-429
 *  An option Account.Alert_Frequency__c sets the frequency of PFS Submission notification: Once per Day, Once per Week.
 *
 *  Whenever a PFS is submitted, each school on the School PFS Assignments is checked to see what their frequency is, and when the last send was.
 *  If the last email send (read-only field Account.Last_PFS_Notification_Date__c) is more than the day or week depending on the frequency, all of the PFS
 *  records for that time period are grabbed (based on Submission Date) and listed in the email. Once the email is sent, Account.Last_PFS_Notification_Date__c
 *  is updated.
 *
 *  Recipients are all staff contacts at that school who are set to receive email: Contact.PFS_Alert_Preferences__c.
 *
 * WH, Exponent Partners, 2013
 */
public class SchoolPFSNotification
{

    public static final String PFS_PAYMENT_STATUS_PAID_IN_FULL = 'Paid in Full';
    public static final String PFS_STATUS_APPLICATION_SUBMITTED = 'Application Submitted';
    private static final List<String> yearsToSendAlertsFor = new List<String>{
        GlobalVariables.getCurrentYearString(),
        GlobalVariables.getPreviousYearString()
    };
    
    public static void sendNotification(Map<Id, Account> schools)
    {
        if(schools==null || schools.isEmpty()) return;
        
        Account tmpAccount;
        List<School_PFS_Assignment__c> spas;
        List<Account> accountToUpdate = new List<Account>();
        List<School_PFS_Assignment__c> spaToUpdate = new List<School_PFS_Assignment__c>();
        wrapperPFSNotification helper = new wrapperPFSNotification();        
        Map<Id, List<User>> contactsBySchoolId = helper.getAllEmailsBySchoolId(selectSchoolPFSAssignments(schools.keyset()));
        
        if(contactsBySchoolId.isEmpty()) return;
        
        // send an email to staff in each account, and update the notification date
        List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage>();
        for (Id schoolId : contactsBySchoolId.keySet())
        {
            try {
                spas = helper.spaMapBySchoolId.get(schoolId);
                tmpAccount = schools.get(schoolId);
                // do not send the first batch of submissions when alert is turned on; 
                //wait for the next cycle to send new submissions for the last day or week
                if(spas!=null && tmpAccount.Last_PFS_Notification_Date__c != null) {
                    emailsToSend = calculateEmailsToBeSend(contactsBySchoolId.get(schoolId), spas, emailsToSend, schools.get(schoolId));
                }
                
                tmpAccount.Last_PFS_Notification_Date__c = System.today();
                accountToUpdate.add(tmpAccount);
                
                for (School_PFS_Assignment__c spfsa : spas) {
                    spfsa.School_Alert_Sent__c = true;
                    spaToUpdate.add(spfsa);
                }
                
            } catch(System.Exception e) {
                throw new SchoolPFSNotificationException('Did not update Last_PFS_Notification_Date__c: ' + e.getMessage()+' Line:'+e.getLineNumber()+' School:'+schoolId);
            }
        }
        
        if(!emailsToSend.isEmpty()){
            sendEmails(emailsToSend);
        }
        
        if (!spaToUpdate.isEmpty()) {
            update spaToUpdate;
        }
        if (!accountToUpdate.isEmpty()) {
            update accountToUpdate;
        }
    }//End:sendNotification
    
    private static Id getSSSOrganizationWideAddress()
    {
        List<OrgWideEmailAddress> orgWide = new List<OrgWideEmailAddress>(
                                                [select Id 
                                                        from OrgWideEmailAddress 
                                                        where Address =: SchoolPortalSettings.SSS_Org_Wide_Email_Address 
                                                        limit 1]);
        return !orgWide.isEmpty() ? orgWide[0].Id : null;
    }//End:getSSSOrganizationWideAddress
    
    @testVisible private static List<Messaging.SingleEmailMessage> calculateEmailsToBeSend(List<User> users, 
                                                List<School_PFS_Assignment__c> pfsList, 
                                                List<Messaging.SingleEmailMessage> emailsToSend,
                                                Account school)
    {
        
        Id sssOrgWideEmailId = getSSSOrganizationWideAddress();
        List<Messaging.SingleEmailMessage> result = new List<Messaging.SingleEmailMessage>();
        
        // should have at least 1 Contact and 1 School PFS Assignment
        if (users == null || users.size() == 0 ||  pfsList == null || pfsList.size() == 0 || school == null) return emailsToSend;
        
        
        
        String frequency = school.Alert_Frequency__c;
        String subject = (frequency.startsWith('Daily') ? 'Daily' : 'Weekly') + ' Summary of PFS Submissions';
        
        String body = '<p>To ' + school.Name + ':</p><p></p>';
        
        body += '<table><tr align="left"><th>Applicant First Name</th><th>Applicant Last Name</th><th>Grade Applying</th><th>Gender</th><th>PFS ID</th></tr>';
        
        ID pfsId = null;
        for (School_PFS_Assignment__c pfs : pfsList) {  // school PFS assignments sorted in order of PFS Id
            body += '<tr align="left"><td>' + 
                        (pfs.Applicant_First_Name__c != null ? pfs.Applicant_First_Name__c : '') + '</td><td>' + 
                        (pfs.Applicant_Last_Name__c != null ? pfs.Applicant_Last_Name__c : '') + '</td><td>' + 
                        (pfs.Grade_Applying__c != null ? pfs.Grade_Applying__c : '') + '</td><td>' + 
                        (pfs.Gender__c != null ? pfs.Gender__c : '') + '</td><td>' + 
                        (pfs.PFS_Number__c != null ? pfs.PFS_Number__c : '') + '</td></tr>';
        }
        body += '</table>';

        body = body + '<p></p><p>You are receiving this email per your SSS School Portal user profile settings.' 
            + ' You can change your alert options in the SSS School Portal.</p><p></p><p>' + Label.Company_Name_Full + '</p>';
        
        Messaging.SingleEmailMessage email;
        for(User u : users)
        {
            email = new Messaging.SingleEmailMessage();
            if (sssOrgWideEmailId != null) {
                email.setOrgWideEmailAddressId(sssOrgWideEmailId);
            } else {
                email.setSenderDisplayName(Label.Company_Name_Full);
            }
            email.setTargetObjectId(u.ContactId);
            email.setSubject(subject);
            email.setHtmlBody(body);
            result.add(email);
        }
        
        if(!result.isEmpty()){
            emailsToSend.addAll(result);
        }
        
        //For testing purpose only
        if (Test.isRunningTest()) {
            
            Note n = new Note(ParentId = pfsList[0].School__c , Title = result[0].getSubject());
            String recipients = '';
            List<Id> contactsSent = new List<Id>();
            for(Messaging.SingleEmailMessage ee : result)
            {
                contactsSent.add(ee.getTargetObjectId());                       
            }
            for(Contact cc:[SELECT id, Email from Contact WHERE id IN:contactsSent]){
                recipients +=  cc.Email + ', ';
            }
            n.Body = recipients + email.getHtmlBody();
            insert n;
        }
        
        return emailsToSend;
    }//End:calculateEmailsToBeSend
    
    private static void sendEmails(List<Messaging.SingleEmailMessage> emailsToSend) {
        if (emailsToSend.isEmpty()) return;
        
        if (!Test.isRunningTest()) {
            try {
                //SendEmail with parameter allOrNothing=false, to avoid fail the entire batch

                Messaging.SendEmailResult[] result = Messaging.sendEmail(emailsToSend);

                if (!result[0].isSuccess()) {
                    throw new SchoolPFSNotificationException('Failed to send PFS notification: ' + result[0].getErrors()[0].getMessage() + ' id=' + result[0].getErrors()[0].getTargetObjectId());
                }
            } catch (System.Exception e) {
                throw new SchoolPFSNotificationException('Failed to send PFS notification: ' + e.getMessage() + e.getLineNumber());
            }
        }
    }//End:sendEmails

    @testVisible private static List<School_PFS_Assignment__c> selectSchoolPFSAssignments(Set<Id> schools) {
        return [
            SELECT Id, Grade_Applying__c, Applicant_First_Name__c, Applicant_Last_Name__c, Gender__c,
                PFS_Number__c, Orig_PFS_Submission_Date__c, School__c  
            FROM School_PFS_Assignment__c
            WHERE School__c IN :schools 
                AND Academic_Year_Picklist__c IN :yearsToSendAlertsFor  // added for selectivity and families cant apply past the previous year.
                AND PFS_Status__c = :PFS_STATUS_APPLICATION_SUBMITTED
                AND Payment_Status__c = :PFS_PAYMENT_STATUS_PAID_IN_FULL
                AND School_Alert_Sent__c = FALSE 
                AND Sharing_Processed__c = TRUE  
                AND Withdrawn__c = 'No' 
            ORDER BY School__c, PFS_Number__c, Applicant_First_Name__c, Applicant_Last_Name__c];
    }//End:selectSchoolPFSAssignments
    
    /* Start Helper Methods */
    public class wrapperPFSNotification
    {
        public Map<Id, List<User>> contactsBySchoolId{ get; private set; }
        public Map<Id, List<School_PFS_Assignment__c>> spaMapBySchoolId{ get; private set; }
        
        public wrapperPFSNotification()
        {
            contactsBySchoolId = new Map<Id, List<User>>();
            spaMapBySchoolId = new Map<Id, List<School_PFS_Assignment__c>>();
        }//End-Constructor
        
        /**
        * @description gather for each school list of School PFS Assignments submitted 
        * within the daily or weekly notification period
        */
        @testVisible private Map<Id, List<School_PFS_Assignment__c>> getSpasBySchoolId(List<School_PFS_Assignment__c> spas)
        {
            List<School_PFS_Assignment__c> spasTmp;
            
            for(School_PFS_Assignment__c spa : spas)
            {
                spasTmp =  !spaMapBySchoolId.containsKey(spa.School__c)
                            ? new List<School_PFS_Assignment__c>()
                            : spaMapBySchoolId.get(spa.School__c);
                spasTmp.add(spa);
                spaMapBySchoolId.put(spa.School__c, spasTmp);
            }
            
            return spaMapBySchoolId;
        }//End:mappingSPASBySchoolId
        
        /**
        * @description gather for each school list of email addresses of staff opted-in for 
        * PFS notifications
        */
        public Map<Id, List<User>> getAllEmailsBySchoolId(List<School_PFS_Assignment__c> spas)
        {
            spaMapBySchoolId = getSpasBySchoolId(spas);
            List<User> contactListTmp;
            
            for (User u : [SELECT Id, ContactId, Contact.AccountId, Email 
                                FROM User 
                                WHERE 
                                    AccountId in: spaMapBySchoolId.keyset() 
                                    AND Contact.PFS_Alert_Preferences__c = 'Receive Alerts' 
                                    AND Contact.Email != null 
                                    AND Contact.HasOptedOutOfEmail = false 
                                    AND Contact.RecordTypeId = :RecordTypes.schoolStaffContactTypeId 
                                    AND IsActive=true 
                                    AND ContactId!=null 
                                    AND Contact.IsEmailBounced=false 
                                ORDER BY Email])
            {
                contactListTmp = contactsBySchoolId.containsKey(u.Contact.AccountId)
                            ? contactsBySchoolId.get(u.Contact.AccountId)
                            : new List<User>();
                contactListTmp.add(u);
                contactsBySchoolId.put(u.Contact.AccountId, contactListTmp);
            }
            
            return contactsBySchoolId;
        }//End:getEmailsBySchoolId
        
    }//End-Class:wrapperPFSNotification
    
    public class SchoolPFSNotificationException extends Exception{}
}