/**
 * Class: FamilyAppBusinessFarmSummaryController
 *
 * Copyright (C) 2015
 *
 * Purpose: Controller for Business Summary page of PFS app. Section 19.
 * 
 * Where Referenced: FamilyAppBusinessFarmSummary.component
 *   
 *
 * Change History:
 *
 * Developer         Date                          Description
 * ---------------------------------------------------------------------------------------
 * Drew Piston      2015.09.01            Initial Development
 *
 * 
 */


public with sharing class FamilyAppBusinessFarmSummaryController extends FamilyAppCompController{
    // all biz farms, queried in application utils
    public List<Business_Farm__c> bizFarmList 
    {
        get{
            return this.pfs.Business_Farms__r;
        } 
        set;
    }

    // constructor -- default to "biz" for grouping 
    public FamilyAppBusinessFarmSummaryController() {
        // no longer using dynamic grouping [DP] 09.14.2015
        //theGrouping = System.currentPagereference().getParameters().get('grp');
        //if (theGrouping == null){
        //    theGrouping = 'entity'; 
        //}
    }

    public String theGrouping {get; set;} // used to determine how to group the biz/farms. Leftover logic from 

    // this property is displayed on the page
    public List<BizFarmGroupWrapper> getBizOrFarmGroupWrappers(){
        List<BizFarmGroupWrapper> groupedBizFarmList = new List<BizFarmGroupWrapper>();
        Integer i = 0;
        // no longer using dynamic grouping [DP] 09.14.2015
        // group the biz/farms according to the selected criteria 
        //if (theGrouping == 'biz'){ // businesses then farms
        //    groupedBizFarmList.add(getRelevantBizFarmList(new Map<String, String>{'Business_or_Farm__c' => 'Business'}, 'Business', 'Business Type', i++));
        //    groupedBizFarmList.add(getRelevantBizFarmList(new Map<String, String>{'Business_or_Farm__c' => 'Farm'}, 'Farm', 'Farm Type', i++));
        //} else if (theGrouping == 'entity'){ // businesses by type, farms by type
            groupedBizFarmList.add(getRelevantBizFarmList(new Map<String, String>{'Business_or_Farm__c' => 'Business', 'Business_Entity_Type__c' => 'Sole Proprietorship'}, 'Businesses - Sole Proprietorship', null, i++));
            groupedBizFarmList.add(getRelevantBizFarmList(new Map<String, String>{'Business_or_Farm__c' => 'Business', 'Business_Entity_Type__c' => 'Partnership'}, 'Businesses - Partnership', null, i++));
            groupedBizFarmList.add(getRelevantBizFarmList(new Map<String, String>{'Business_or_Farm__c' => 'Business', 'Business_Entity_Type__c' => 'Corporation'}, 'Business - Corporation', null, i++));
            groupedBizFarmList.add(getRelevantBizFarmList(new Map<String, String>{'Business_or_Farm__c' => 'Farm', 'Business_Entity_Type__c' => 'Sole Proprietorship'}, 'Farms - Sole Proprietorship', null, i++));
            groupedBizFarmList.add(getRelevantBizFarmList(new Map<String, String>{'Business_or_Farm__c' => 'Farm', 'Business_Entity_Type__c' => 'Partnership'}, 'Farms - Partnership', null, i++));
            groupedBizFarmList.add(getRelevantBizFarmList(new Map<String, String>{'Business_or_Farm__c' => 'Farm', 'Business_Entity_Type__c' => 'Corporation'}, 'Farm - Corporation', null, i++));
        //}
        return groupedBizFarmList;
    }

    // generic method to get the relevant biz/farms, takes:
    // map of field names to desired field values
    // header label for col 1 of table
    // header label for col 2 of table
    // index integer
    public BizFarmGroupWrapper getRelevantBizFarmList(Map<String, String> fieldNameToFieldValue, String header1, String header2, Integer iParam) {
        List<Business_Farm__c> localList = new List<Business_Farm__c>();
        for (Business_Farm__c bf :bizFarmList){
            Boolean addBizFarm = true;
            for (String fieldName : fieldNameToFieldValue.keySet()){
                if (bf.get(fieldName) != fieldNameToFieldValue.get(fieldName)){
                    addBizFarm = false;
                }
            }
            if (addBizFarm){
                localList.add(bf);
            }

        }

        return new BizFarmGroupWrapper(localList, header1, header2, iParam);
    }

    public class BizFarmGroupWrapper{
        public List<Business_Farm__c> bizFarmList {get; set;}
        public String header1 {get; set;}
        public String header2 {get; set;}
        public Business_Farm__c dummyBizFarm {get; set;}
        public String indexString {get; set;}

        public bizFarmGroupWrapper(List<Business_Farm__c> bizFarmListParam, String header1Param, String header2Param, Integer indexParam){
            bizFarmList = bizFarmListParam;
            header1 = header1Param;
            header2 = header2Param;
            indexString = String.valueOf(indexParam);

            dummyBizFarm = new Business_Farm__c();
            calculateTotals();

        }

        // calculate the dummy totals 
        public void calculateTotals(){
            Decimal income = 0;
            Decimal expenses = 0;
            Decimal netprofitloss = 0;
            Decimal share = 0;

            for (Business_Farm__c bf : bizFarmList){
                income += (bf.Business_Total_Income_Current__c == null) ? 0 : bf.Business_Total_Income_Current__c;
                expenses += (bf.Business_Total_Expenses_Current__c == null) ? 0 : bf.Business_Total_Expenses_Current__c;
                netprofitloss += (bf.Net_Profit_Loss_Business_Farm_Current__c == null) ? 0 : bf.Net_Profit_Loss_Business_Farm_Current__c;
                share += (bf.Business_Net_Profit_Share_Current__c == null) ? 0 : bf.Business_Net_Profit_Share_Current__c;
            }

            dummyBizFarm.Business_Total_Income_Current__c = income;
            dummyBizFarm.Business_Total_Expenses_Current__c = expenses;
            dummyBizFarm.Net_Profit_Loss_Business_Farm_Current__c = netprofitloss;
            dummyBizFarm.Business_Net_Profit_Share_Current__c = share;
        }

    }
}