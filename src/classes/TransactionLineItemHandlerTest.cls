@isTest
private class TransactionLineItemHandlerTest {
    @isTest
    private static void afterInsert_handleRefundTransactionLineItems_expectSubscriptionDeleted() {
        Opportunity opportunity = OpportunityTestData.Instance.DefaultOpportunity;
        Subscription__c subscription = SubscriptionTestData.Instance
                .forAcademicYear(opportunity.Academic_Year_Picklist__c.left(4)).insertSubscription();

        Integer startYear = Integer.valueOf(opportunity.Academic_Year_Picklist__c.left(4));
        System.assertNotEquals(null, startYear, 'Expected a start year to be available.');

        List<Subscription__c> subscriptions = SubscriptionSelector.Instance
                .selectByAccountIdForAcademicYear(new Set<Id> { opportunity.AccountId }, startYear);
        System.assert(!subscriptions.isEmpty(), 'Expected there to be a subscription found.');

        Test.startTest();
        TransactionLineItemTestData.Instance.forOpportunity(opportunity.Id)
                .forAmount(opportunity.Amount).asRefund().insertTransactionLineItem();
        Test.stopTest();

        List<Subscription__c> remainingSubscriptions = SubscriptionSelector.Instance
                .selectByAccountIdForAcademicYear(new Set<Id> { opportunity.AccountId }, startYear);
        System.assert(remainingSubscriptions.isEmpty(), 'Expected there to be no subscriptions remaining.');
    }
}