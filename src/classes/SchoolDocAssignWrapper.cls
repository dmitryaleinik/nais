/**
 * @description [SL] NAIS-934 - wrapper class for SDA records
 **/
public class SchoolDocAssignWrapper implements Comparable {

    private static Map<String, Document_Custom_Settings__c> docTypeToCustomSettingMap = null;

    public School_Document_Assignment__c sda {get; set;}

    public SchoolDocAssignWrapper(School_Document_Assignment__c schoolDocumentAssignment) {
        this.sda = schoolDocumentAssignment;
    }

    public boolean documentReceived {
        get {
            if (sda.Document__c != null &&
                sda.Document__r.Deleted__c != true &&
                (sda.Document__r.Document_Status__c == 'Received/In Progress'
                    || sda.Document__r.Document_Status__c == 'Processed')
                ) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    public String documentTypePicklistValue {
        get {
            String documentType = null;

            // if a document exists, then use that doc type, otherwise use the doc type on the sda record
            if (documentReceived) {
                documentType = sda.Document__r.Document_Type__c;
            }
            else {
                if (sda.Document_Type__c != null) {
                    documentType = sda.Document_Type__c;
                }
                else {
                    if (sda.Required_Document__c != null) {
                        documentType = sda.Required_Document__r.Document_Type__c;
                    }
                }
            }
            return documentType;
        }

    }

    public String documentType {
        get {

            String documentType = documentTypePicklistValue;

            // if it's a school/organization document, get the label from the "Required Document"
            if (documentType == ApplicationUtils.schoolSpecificDocType()) {
                if (sda.Required_Document__r != null) {
                    documentType = sda.Required_Document__r.Label_for_School_Specific_Document__c;
                }
            }


            return documentType;
        }
    }

    public String springCMDocumentType {
        get {
            return SpringCMAction.getSpringCMDocTypeFromSFDocType(documentTypePicklistValue);
        }
    }

    public String documentYear {
        get {
            String documentYear = null;
            // if a document exists, then use that doc year, otherwise use the doc year on the sda record
            if (documentReceived) {
                documentYear = sda.Document__r.Document_Year__c;
            }
            else {
                if (sda.Document_Year__c != null) {
                    documentYear = sda.Document_Year__c;
                }
                else {
                    if (sda.Required_Document__c != null) {
                        documentYear = sda.Required_Document__r.Document_Year__c;
                    }
                }
            }
            return documentYear;
        }
    }

    public String documentLabel {
        get {
            return documentType + ' - ' + documentYear;
        }
    }

    public Date dateReceived {
        get {
            if (documentReceived) {
                return sda.Document__r.Date_Received__c;
            }
            else {
                return null;
            }
        }
    }

    public Date dateVerified {
        get {
            if (documentReceived) {
                return sda.Document__r.Date_Verified__c;
            }
            else {
                return null;
            }
        }
    }

    public String source {
        get {
            if (documentReceived) {
                return sda.Document__r.Document_Source__c;
            }
            else {
                return null;
            }
        }
    }

    public String documentPertainsTo {
        get {
            if (sda.Document__r != null) {
                return sda.Document__r.Document_Pertains_To__c;
            }
            else {
                return null;
            }
        }
    }

    public boolean documentVerified {
        get {
            boolean documentVerified = false;
            if (documentReceived) {
                if (sda.Document__r.Date_Verified__c != null) {
                    documentVerified = true;
                }
            }
            return documentVerified;
        }
    }

    public boolean requestToWaiveRequirement {
        get {
            boolean requestToWaiveRequirement = false;

            if ((sda.Document__c != null)
                    && (sda.Document__r.Deleted__c != true)) {
                if (sda.Document__r.Request_to_Waive_Requirement__c == 'Yes') {
                    requestToWaiveRequirement = true;
                }
            }
            return requestToWaiveRequirement;
        }
    }

    public boolean waiverApproved {
        get {
            return (sda.Requirement_Waiver_Status__c == 'Approved');
        }
    }

    public boolean waiverDeclined {
        get {
            return (sda.Requirement_Waiver_Status__c == 'Not Approved');
        }
    }

    public boolean uploadPending {
        get {
            return (sda.Document__c != null && sda.Document__r.Deleted__c != true && sda.Document__r.Document_Status__c == 'Upload Pending');
        }
    }

    public boolean isAddAnotherDoc {get; set;}

    public String statusIconCSS {
        get{
            // Logic for displaying the Status icons
            if(sda.Document__c == null ||
                sda.Document__r.Deleted__c ||
                sda.Document__r.Document_Status__c == null ||
                sda.Document__r.Document_Status__c == '' ||
                sda.Document__r.Document_Status__c == 'Not Received'){
                return 'notReceived';
            }
            else if(sda.Document__r.Document_Status__c == 'Upload Pending'){
                return 'pending';
            }
            else if(sda.Document__r.Document_Status__c == 'Processed' || sda.Document__r.Document_Status__c == 'Received/In Progress'){
                return 'complete';
            }
            else if(sda.Document__r.Document_Status__c == 'Not Applicable/Waive Requested'){
                return 'notApplicable';
            }
            else if(sda.Document__r.Document_Status__c == 'Error/Invalid Document'){
                return 'uploadError';
            }
            else{
                return 'uploadError';
            }
        }
        set;
    }

    public String sortPrefix {
        get {

            String prefix = '';

            String docType = documentTypePicklistValue;

            // sort priority #1: documents in custom settings
            Map<String, Document_Custom_Settings__c> customSettingsMap = getCustomSettingsMap();
            Document_Custom_Settings__c customSetting = customSettingsMap.get(docType);
            if (customSetting != null) {
                prefix = '1:' + customSetting.Name + ':';
            }

            // sort priority #2: school/organization documents
            else if (docType == ApplicationUtils.schoolSpecificDocType()) {
                prefix = '2:';
            }

            // sort priority #3: tax schedules
            else if ((docType != null) && (docType.startsWithIgnoreCase('Tax Schedule'))) {
                prefix = '3:';
            }

            // everything else is priority #4
            else {
                prefix = '4:';
            }

            return prefix;
        }
    }

    public String sortLabel {
        get {
            return sortPrefix + documentLabel;
        }
    }

    // implement Comparable interface to allow sorting
    public Integer compareTo(Object compareTo) {
        SchoolDocAssignWrapper sdaWrapper = (SchoolDocAssignWrapper) compareTo;
        return (sortLabel).compareTo(sdaWrapper.sortLabel);
    }

    // retrieve the custom settings map
    private static Map<String, Document_Custom_Settings__c> getCustomSettingsMap() {
        if (docTypeToCustomSettingMap == null) {
            docTypeToCustomSettingMap = new Map<String, Document_Custom_Settings__c>();
            for (Document_Custom_Settings__c customSetting : [SELECT Name, Value__c FROM Document_Custom_Settings__c WHERE Active__c = true]) {
                if (!String.isBlank(customSetting.Value__c)) {
                    docTypeToCustomSettingMap.put(customSetting.Value__c, customSetting);
                }
            }
        }
        return docTypeToCustomSettingMap;
    }

    // create wrappers for SDA records related to a School PFS Assignment
    // [CH] NAIS-1485 Updating method to take a School PFS Assignment record as an argument
    //                 and to account for 1040 documents when Filing Status is Married, Filing Separately
    public static List<SchoolDocAssignWrapper> getSchoolDocAssignWrappers(School_PFS_Assignment__c schoolPFSAssignment) {
        List<SchoolDocAssignWrapper> sdaWrapperList = new List<SchoolDocAssignWrapper>();

        // [CH] NAIS-1470 - Adding support for "Add Another" button
        Set<String> documentTypeSet = new Set<String>{};
        // NAIS-1842 [DP] 03.30.2015 refactor to avoid querying
        //for(Document_Type_Settings__c setting : [select Name from Document_Type_Settings__c where Add_Another__c = true]){
        for(Document_Type_Settings__c setting : ApplicationUtils.getAllDocTypeSettings()){
            if (setting.Add_Another__c){
                documentTypeSet.add(setting.Name);
            }
        }

        // Booleans for controlling creation of Dummy 1040 records
        Boolean parentACurrentFound = false;
        Boolean parentBCurrentFound = false;
        Boolean parentAPrevFound = false;
        Boolean parentBPrevFound = false;
        // Previous and Current year variables to support 1040 logic
        String currentTaxYear = GlobalVariables.getDocYearStringFromAcadYearName(schoolPFSAssignment.Applicant__r.PFS__r.Academic_Year_Picklist__c);
        String previousTaxYear = GlobalVariables.getPrevDocYearStringFromAcadYearName(schoolPFSAssignment.Applicant__r.PFS__r.Academic_Year_Picklist__c);

        for (School_Document_Assignment__c sda : schoolPFSAssignment.School_Document_Assignments__r) {

            // If the filing status is "Married, Filing Separately" and the Document Type is 1040
            //  we need to create dummy records to display a dummy row, if necessary, and to display
            //  correct Petains To values
            if(schoolPFSAssignment.Applicant__r.PFS__r.Filing_Status__c == 'Married, Filing Separately' &&
                sda.Document_Type__c == '1040 with all filed schedules and attachments')
            {
                // Figure out which 1040 Document the current record fulfills.
                // If this is a 1040 for the current year
                if(sda.Document_Year__c == currentTaxYear){
                    // If there is no Document, there is a document with no pertains to, or the Pertains To is
                    //     Parent A then count the current document as the Parent A 1040.
                    if(sda.Document__c == null || sda.Document__r.Document_Pertains_To__c == null || sda.Document__r.Document_Pertains_To__c == 'Parent A'){
                        if(sda.Document__r == null){
                            sda.Document__r = createDummyFamilyDoc(currentTaxYear, 'Parent A');
                        }
                        else if(sda.Document__r.Document_Pertains_To__c == null){
                            sda.Document__r.Document_Pertains_To__c = 'Parent A';
                        }

                        parentACurrentFound = true;
                    }
                    else if(sda.Document__c != null && sda.Document__r.Document_Pertains_To__c == 'Parent B'){
                        parentBCurrentFound = true;
                    }
                }
                // If it's a 1040 for the previous year
                else if(sda.Document_Year__c == previousTaxYear){
                    // If there is no Document, there is a document with no pertains to, or the Pertains To is
                    //     Parent A then count the current document as the Parent A 1040.
                    if(sda.Document__c == null || sda.Document__r.Document_Pertains_To__c == null || sda.Document__r.Document_Pertains_To__c == 'Parent A'){
                        if(sda.Document__r == null){
                            sda.Document__r = createDummyFamilyDoc(previousTaxYear, 'Parent A');
                        }
                        else if(sda.Document__r.Document_Pertains_To__c == null){
                            sda.Document__r.Document_Pertains_To__c = 'Parent A';
                        }
                        parentAPrevFound = true;
                    }
                    else if(sda.Document__c != null && sda.Document__r.Document_Pertains_To__c == 'Parent B'){
                        parentBPrevFound = true;
                    }
                }
            }

            SchoolDocAssignWrapper schoolDocAssign = new SchoolDocAssignWrapper(sda);

            // [CH] NAIS-1470 Adding support for Add Another button
            if(sda.Document__c != null && sda.Document__r.Document_Status__c != 'Not Received' && documentTypeSet.contains(sda.Document_Type__c)){
                schoolDocAssign.isAddAnotherDoc = true;
            }
            else{
                schoolDocAssign.isAddAnotherDoc = false;
            }

            // Add the processed document to the wrapper list
            sdaWrapperList.add(schoolDocAssign);
        }

        // Check to see if the 1040 Booleans have been set to true, which would
        //  indicate that there is at least one 1040 in the required documents list
        if(parentACurrentFound || parentBCurrentFound){
            if(!parentACurrentFound){
                // There was a Parent B record found, but not a Parent A
                sdaWrapperList.add(new SchoolDocAssignWrapper(createDummy1040SDA(currentTaxYear, 'Parent A')));
            }
            else if(!parentBCurrentFound){
                // There was a Parent A record found, but not a Parent B
                sdaWrapperList.add(new SchoolDocAssignWrapper(createDummy1040SDA(currentTaxYear, 'Parent B')));
            }
        }

        if(parentAPrevFound || parentBPrevFound){
            if(!parentAPrevFound){
                // There was a Parent B record found, but not a Parent A
                sdaWrapperList.add(new SchoolDocAssignWrapper(createDummy1040SDA(previousTaxYear, 'Parent A')));
            }
            else if(!parentBPrevFound){
                // There was a Parent A record found, but not a Parent B
                sdaWrapperList.add(new SchoolDocAssignWrapper(createDummy1040SDA(previousTaxYear, 'Parent B')));
            }
        }

        // sort the list
        sdaWrapperList.sort();

        return sdaWrapperList;
    }

    private static School_Document_Assignment__c createDummy1040SDA(String docYear, String pertainsTo){
        School_Document_Assignment__c dummySDA = new School_Document_Assignment__c();
        dummySDA.Document_Type__c = '1040 with all filed schedules and attachments';
        dummySDA.Document_Year__c = docYear;
        dummySDA.Document__r = createDummyFamilyDoc(docYear, pertainsTo);
        return dummySDA;
    }

    private static Family_Document__c createDummyFamilyDoc(String docYear, String pertainsTo){
        Family_Document__c familyDoc = new Family_Document__c();
        familyDoc.Deleted__c = false;
        familyDoc.Document_Pertains_to__c = pertainsTo;
        familyDoc.Document_Status__c = 'Not Received';
        familyDoc.Document_Type__c = '1040 with all filed schedules and attachments';
        familyDoc.Document_Year__c = docYear;
        return familyDoc;
    }

    /*
    public static List<SchoolDocAssignWrapper> getSchoolDocAssignWrappers(Id schoolPFSAssignmentId) {
        List<SchoolDocAssignWrapper> sdaWrapperList = new List<SchoolDocAssignWrapper>();
        for (School_Document_Assignment__c sda : [SELECT Id, Document_Type__c, Document_Year__c, Requirement_Waiver_Status__c,
                                                            Requirement_Waiver_Comments__c,
                                                            Document__r.Name, Document__r.Document_Status__c,
                                                            Document__r.Id, Document__r.Document_Type__c, Document__r.Document_Year__c,
                                                            Document__r.Date_Received__c, Document__r.Date_Verified__c,
                                                            Document__r.Document_Source__c, Document__r.Document_Pertains_To__c,
                                                            Document__r.Request_to_Waive_Requirement__c, Document__r.Issue_Categories__c,
                                                            Document__r.Other_Document_Label__c, Document__r.Description__c,
                                                            Document__r.Reason_for_Request__c, Document__r.Notes__c,
                                                            Document__r.Import_Id__c,
                                                            Document__r.Deleted__c,
                                                            Required_Document__r.Id ,
                                                            Required_Document__r.Document_Type__c,
                                                            Required_Document__r.Label_for_School_Specific_Document__c,
                                                            Required_Document__r.Document_Year__c,
                                                            School_PFS_Assignment__r.School__r.SSS_School_Code__c // NAIS-1265

                                                    FROM School_Document_Assignment__c
                                                    WHERE School_PFS_Assignment__c = :schoolPFSAssignmentId
                                                    AND Deleted__c = false
                                                    ORDER BY Document_Type__c, Document_Year__c]) {
            sdaWrapperList.add(new SchoolDocAssignWrapper(sda));
        }

        // sort the list
        sdaWrapperList.sort();

        return sdaWrapperList;
    }
    */

    public static List<SchoolDocAssignWrapper> getOutstandingSchoolDocAssignWrappers(Id schoolPFSAssignmentId) {
        List<SchoolDocAssignWrapper> sdaWrapperList = new List<SchoolDocAssignWrapper>();
        for (School_Document_Assignment__c sda : [SELECT Id, Document_Type__c, Document_Year__c, Requirement_Waiver_Status__c,
                                                            Requirement_Waiver_Comments__c,
                                                            Document__r.Name, Document__r.Document_Status__c,
                                                            Document__r.Id, Document__r.Document_Type__c, Document__r.Document_Year__c,
                                                            Document__r.Date_Received__c, Document__r.Date_Verified__c,
                                                            Document__r.Document_Source__c, Document__r.Document_Pertains_To__c,
                                                            Document__r.Request_to_Waive_Requirement__c, Document__r.Issue_Categories__c,
                                                            Document__r.Other_Document_Label__c, Document__r.Description__c,
                                                            Document__r.Reason_for_Request__c, Document__r.Notes__c,
                                                            Document__r.Import_Id__c,
                                                            Document__r.Deleted__c,
                                                            Required_Document__r.Id ,
                                                            Required_Document__r.Document_Type__c,
                                                            Required_Document__r.Label_for_School_Specific_Document__c,
                                                            Required_Document__r.Document_Year__c,
                                                            School_PFS_Assignment__r.School__r.SSS_School_Code__c // NAIS-1265

                                                    FROM School_Document_Assignment__c
                                                    WHERE School_PFS_Assignment__c = :schoolPFSAssignmentId
                                                    AND Deleted__c = false
                                                    AND (Document__c = null
                                                            OR Document__r.Deleted__c = true
                                                            OR Document__r.Document_Status__c IN ('Not Received', 'Not Applicable/Waive Requested', 'Error/Invalid Document'))
                                                    AND (Requirement_Waiver_Status__c != 'Approved')
                                                    ORDER BY Document_Type__c, Document_Year__c]) {
            sdaWrapperList.add(new SchoolDocAssignWrapper(sda));
        }

        // sort the list
        sdaWrapperList.sort();

        return sdaWrapperList;
    }

}