/**
 * @description A request to make a refund using the PaymentProcessor.
 **/
public class RefundRequest extends PaymentProcessorRequest {
    @testVisible private static final String TRANSACTION_LINE_ITEM_PARAM = 'transactionLineItem';

    /**
     * @description The Opportunity record associated with the Transaction
     *              Line Item.
     */
    public Opportunity Opportunity {
        get {
            if (Opportunity == null) {
                if (TransactionLineItem != null && TransactionLineItem.Opportunity__r != null) {
                    Opportunity = TransactionLineItem.Opportunity__r;
                } else if (TransactionLineItem != null && TransactionLineItem.Opportunity__c != null) {
                    List<Opportunity> opportunities = OpportunitySelector.Instance
                            .selectByIdWithPfs(new Set<Id> { TransactionLineItem.Opportunity__c });

                    if (!opportunities.isEmpty()) {
                        Opportunity = opportunities[0];
                    }
                }
            }
            return Opportunity;
        }
        set;
    }

    /**
     * @description The Transaction Line Item to refund.
     */
    public Transaction_Line_Item__c TransactionLineItem { get; set; }

    /**
     * @description The Transaction Number to associate the refund Transaction
     *              Line Item to.
     */
    public String TransactionNumber {
        get {
            if (TransactionNumber == null && TransactionLineItem != null && TransactionLineItem.Transaction_ID__c != null) {
                TransactionNumber = TransactionLineItem.Transaction_ID__c;
            }
            return TransactionNumber;
        }
        set;
    }

    /**
     * @description The Vault GUID to set on the Transaction Line Item.
     */
    public String VaultGUID {
        get {
            if (VaultGUID == null && TransactionLineItem != null && TransactionLineItem.VaultGUID__c != null) {
                VaultGUID = TransactionLineItem.VaultGUID__c;
            }
            return VaultGUID;
        }
        set;
    }

    /**
     * @description Construct the refund request, taking in a Transaction
     *              Line Item that can be refunded.
     * @param transactionLineItem The refundable Transaction Line Item.
     */
    public RefundRequest(Transaction_Line_Item__c transactionLineItem) {
        ArgumentNullException.throwIfNull(transactionLineItem, TRANSACTION_LINE_ITEM_PARAM);

        this.TransactionLineItem = transactionLineItem;
    }
}