/**
 * @description Domain class for SpringCM_Document__c records that is used to store constants related to these records.
 */
public class SpringCmDocuments extends fflib_SObjectDomain {

    // The constants below correspond to values of the Match_Not_Found_Reason__c picklist.
    public static final String NO_DOC_TYPE_MAPPING = 'No Doc Type Mapping';
    public static final String DOC_TYPE_NOT_VERIFIED = 'Doc Type Is Not Verified';
    public static final String JOINT_FILERS_1040_RELATED_DOC = '1040 Or Related Doc For Joint Filers';
    public static final String PFS_NOT_FOUND = 'PFS Not Found';
    public static final String UNABLE_TO_FIND_MATCH = 'Unable To Find Match';
    public static final String MATCH_PROVIDED = 'Match Provided By Doc Processor';
    public static final String DOC_SOURCE_MAILED = 'Mailed';
    
    //Map used to dinamically define the Family_Document__c fields that contains info that can be set as default
    //for SpringCM_Document__c records. This map has the following format: 
    //      SpringCMFieldToSet => FamilyDocumentFieldThatContainsTheDefaultValue
    private static final Map<String, String> defaultFamilyDocFieldsForMissingSCMDocData = new Map<String, String>{
        'Document_Type__c' => 'Document_Type__c', 
        'Document_Year__c' => 'Document_Year__c'
    };
    
    private SpringCmDocuments(List<SpringCM_Document__c> springCMDocuments) {
        super(springCMDocuments);
    }
    
    /**
     * @description Handles logic that should be done before SpringCM_Document__c records are inserted.
     */
    public override void onBeforeInsert() {
        
        // SFP-1679: Check for records that are missing doc type and doc year. Try to default the values by pulling them
        // from the family document with the same import Id.
        defaultMissingInfo((List<SpringCM_Document__c>)this.Records);

        // Determine who the document pertains to by matching the name on the SCM Doc records to the parents, applicants for the PFS.
        // This became necessary after switching to scalehub who provides us with the person's name from the document but
        // does not determine the pertains to for us.
        // This logic is only executed if the Match_Parent_To_SpringCM_Doc__c feature toggle is enabled.
        // Otherwise this method just returns instantly.
        DocumentNameMatchService.Instance.setDocumentPertainsTo((List<SpringCM_Document__c>)this.Records);
    }

    private void defaultMissingInfo(List<SpringCM_Document__c> springCMDocuments) {
        // If the feature toggle is disabled, return early.
        if (!FeatureToggles.isEnabled('Default_Missing_SCM_Doc_Info__c')) {
            return;
        }

        List<SpringCM_Document__c> springCMDocumentsMissingInfo = new List<SpringCM_Document__c>();
        Set<String> importIdsToQuery = new Set<String>();
        // When the feature toggle is enabled, we will default doc type and doc year information for any SpringCM Document that is missing it.
        for (SpringCM_Document__c scmDoc : springCMDocuments) {
            if (isMissingRequiredInfo(scmDoc)) {

                // Only add the import Id to the query parameters if it isn't blank.
                // Regardless, we will still add this record to the list of records with missing info.
                // Records that are missing info and don't have a family doc to pull default values from will be marked for review in reports.
                if (String.isNotBlank(scmDoc.Import_ID__c)) {
                    importIdsToQuery.add(scmDoc.Import_ID__c);
                }
                springCMDocumentsMissingInfo.add(scmDoc);
            }
        }

        // If no records are missing info, return early.
        if (springCMDocumentsMissingInfo.isEmpty()) {
            return;
        }

        // We default this information by querying family document records by import Id and then transferring the doc type
        // and doc year from the family document record to the SpringCM Doc record.
        Map<String, Family_Document__c> familyDocsByImportId = getFamilyDocsByImportId(importIdsToQuery);

        transferFamilyDocInfoToSpringCMDocs(springCMDocumentsMissingInfo, familyDocsByImportId);
    }

    private void transferFamilyDocInfoToSpringCMDocs(List<SpringCM_Document__c> springCMDocsToUpdate, Map<String, Family_Document__c> familyDocsByImportId) {
        for (SpringCM_Document__c springCMDocument : springCMDocsToUpdate) {
            Family_Document__c familyDoc = familyDocsByImportId.get(springCMDocument.Import_ID__c);

            // If we have gotten this far, the SCM Doc should be marked as one where the required info was defaulted.
            springCMDocument.Doc_Info_Defaulted__c = true;

            // If we can't find a family doc for this record or we were unable to fill in the missing info for any
            // reason, move on to the next record.
            if (familyDoc == null || String.isBlank(familyDoc.Document_Year__c) || String.isBlank(familyDoc.Document_Type__c)) {
                continue;
            }

            // We get the SpringCM doc type code from the value on the family document. This makes it so that the SpringCM Doc is
            // populated with the correct document code that we would normally expect from SpringCM.
            springCMDocument.Document_Type__c = SpringCMAction.getSpringCMDocTypeFromSFDocType(familyDoc.Document_Type__c);
            springCMDocument.Document_Year__c = familyDoc.Document_Year__c;
        }
    }

    private Map<String, Family_Document__c> getFamilyDocsByImportId(Set<String> importIds) {
        Map<String, Family_Document__c> familyDocsByImportId = new Map<String, Family_Document__c>();

        if (importIds == null || importIds.isEmpty()) {
            return familyDocsByImportId;
        }

        List<Family_Document__c> familyDocs = [SELECT Id, Document_Type__c, Document_Year__c, Import_ID__c FROM Family_Document__c WHERE Import_ID__c IN :importIds AND Deleted__c = false];

        for (Family_Document__c familyDocument : familyDocs) {
            familyDocsByImportId.put(familyDocument.Import_ID__c, familyDocument);
        }

        return familyDocsByImportId;
    }

    private Boolean isMissingRequiredInfo(SpringCM_Document__c springCMDocument) {
        // Check the doc type and doc year fields for the record and return true if either of them are null.
        for (String springCMField : defaultFamilyDocFieldsForMissingSCMDocData.keySet()) {
            if (springCMDocument.get(springCMField) == null) {
                return true;
            }
        }

        return false;
    }

    /**
     * @description Creates a new instance of the SpringCmDocuments domain class from the specified records.
     * @param springCmDocumentRecords The records to create a new domain class for.
     * @return An instance of the SpringCmDocuments domain class.
     */
    public static SpringCmDocuments newInstance(List<SpringCM_Document__c> springCmDocumentRecords) {
        ArgumentNullException.throwIfNull(springCmDocumentRecords, 'springCmDocumentRecords');
        return new SpringCmDocuments(springCmDocumentRecords);
    }
    
}