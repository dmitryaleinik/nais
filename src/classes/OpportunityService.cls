/**
 * @description Handles interactions with Opportunities.
 **/
public class OpportunityService {
    @testVisible private static final String OPPORTUNITY_ID_PARAM = 'opportunityId';
    @testVisible private static final String OPPORTUNITY_IDS_PARAM = 'opportunityIds';
    @testVisible private static final String OPPORTUNITY_PARAM = 'opportunity';
    @testVisible private static final String OPPORTUNITIES_PARAM = 'opportunities';
    @testVisible private static final String PROCESS_DML_PARAM = 'processDml';
    @testVisible private static final String INVALID_SOBJECT_TYPE = 'Invalid SObject Type.';
    @testVisible private static final String CLOSED_WON = 'Closed Won';

    private static final Set<String> CLOSED_STAGES = new Set<String> { 'Closed Won', 'Closed', 'Paid in Full' };
    private static final Set<String> PAID_STATUSES = new Set<String> { 'Paid', 'Overpaid' };

    /**
     * @description Determine whether or not an Opportunity is
     *              marked in some form of closed status.
     * @param opportunity The opportunity to check.
     * @return True if the opportunity is marked as closed, otherwise false.
     */
    public Boolean isMarkedAsClosed(Opportunity opportunity) {
        ArgumentNullException.throwIfNull(opportunity, OPPORTUNITY_PARAM);

        return String.isNotBlank(opportunity.StageName) && CLOSED_STAGES.contains(opportunity.StageName);
    }

    /**
     * @description Determine whether or not an Opportunity is
     *              marked in some form of paid status.
     * @param opportunity The opportunity to check.
     * @return True if the opportunity is paid, otherwise false.
     */
    public Boolean isPaid(Opportunity opportunity) {
        ArgumentNullException.throwIfNull(opportunity, OPPORTUNITY_PARAM);

        return String.isNotBlank(opportunity.Paid_Status__c) && PAID_STATUSES.contains(opportunity.Paid_Status__c);
    }

    /**
     * @description Mark the Opportunity as Closed and Won.
     * @param opportunityId The Id of the opportunity to mark as Closed/Won.
     * @return The updated Opportunity.
     */
    public Opportunity markAsClosedWon(Id opportunityId) {
        ArgumentNullException.throwIfNull(opportunityId, OPPORTUNITY_ID_PARAM);
        if (opportunityId.getSobjectType() != Opportunity.getSObjectType()) {
            throw new ArgumentException(INVALID_SOBJECT_TYPE);
        }

        return markAsClosedWon(new Opportunity(Id = opportunityId));
    }

    /**
     * @description Mark an Opportunity as Closed and Won.
     * @param opportunity The Opportunity to mark as Closed/Won.
     * @return The updated Opportunity.
     */
    public Opportunity markAsClosedWon(Opportunity opportunity) {
        return markAsClosedWon(opportunity, true);
    }

    /**
     * @description Mark an Opportunity as Closed and Won and update the
     *              record if processDml is true.
     * @param opportunity The Opportunity to mark as Closed/Won.
     * @param processDml Whether or not to update the record.
     * @return The updated Opportunity.
     */
    public Opportunity markAsClosedWon(Opportunity opportunity, Boolean processDml) {
        ArgumentNullException.throwIfNull(opportunity, OPPORTUNITY_PARAM);
        ArgumentNullException.throwIfNull(processDml, PROCESS_DML_PARAM);

        opportunity.StageName = CLOSED_WON;
        opportunity.CloseDate = System.today();

        if (processDml) {
            update opportunity;
        }

        return opportunity;
    }

    /**
     * @description Mark a collection of Opportunities as Closed and Won.
     * @param opportunityIds A set of Opportunity Ids to mark as Closed/Won.
     * @return A list of updated Opportunities.
     */
    public List<Opportunity> markAsClosedWon(Set<Id> opportunityIds) {
        ArgumentNullException.throwIfNull(opportunityIds, OPPORTUNITY_IDS_PARAM);

        List<Opportunity> opportunities = new List<Opportunity>();

        for (Id opportunityId : opportunityIds) {
            opportunities.add(markAsClosedWon(opportunityId));
        }

        return markAsClosedWon(opportunities);
    }

    /**
     * @description Mark a collection of Opportunities as Closed and Won.
     * @param opportunities The collection of Opportunities to mark as Closed/Won.
     * @return A list of updated Opportunities.
     */
    public List<Opportunity> markAsClosedWon(List<Opportunity> opportunities) {
        ArgumentNullException.throwIfNull(opportunities, OPPORTUNITIES_PARAM);

        List<Opportunity> updatedOpportunities = new List<Opportunity>();

        for (Opportunity opportunity : opportunities) {
            updatedOpportunities.add(markAsClosedWon(opportunity, false));
        }

        update updatedOpportunities;

        return updatedOpportunities;
    }

    /**
     * @description Singleton Instance property.
     */
    public static OpportunityService Instance {
        get {
            if (Instance == null) {
                Instance = new OpportunityService();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private singleton constructor.
     */
    private OpportunityService() {}
}