@isTest
private class SchoolFamilyContribWrksheetContTest {
    private static SchoolFamilyContribWrksheetController setupController() {
        Academic_Year__c academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        SssConstantsTestData.Instance.forAcademicYearId(academicYear.Id).insertSssConstants();
        School_PFS_Assignment__c spa = SchoolPfsAssignmentTestData.Instance
                .forAcademicYearPicklist(academicYear.Name)
                .insertSchoolPfsAssignment();

        ApexPages.currentPage().getParameters().put('schoolpfsassignmentid', spa.Id);
        return new SchoolFamilyContribWrksheetController();
    }

    @isTest
    private static void schoolAcademicYearSelectorStudent_OnChange_expectPageReferenceReturned() {
        PageReference expectedPageReference = Page.SchoolFamilyContributionWorksheet;

        PageReference returnedPageReference = setupController()
                .SchoolAcademicYearSelectorStudent_OnChange(expectedPageReference);
        System.assertEquals(expectedPageReference, returnedPageReference);
    }

    @isTest
    private static void schoolAcademicYearSelector_OnChange_expectNull() {
        SchoolFamilyContribWrksheetController controller = setupController();

        System.assertEquals(null, controller.SchoolAcademicYearSelector_OnChange(true));
        System.assertEquals(null, controller.SchoolAcademicYearSelector_OnChange(false));
    }

    @isTest
    private static void setAcademicYearId_expectAcademicYearSet() {
        SchoolFamilyContribWrksheetController controller = setupController();
        Academic_Year__c expectedAcademicYear = AcademicYearTestData.Instance.insertAcademicYear();

        controller.setAcademicYearId(expectedAcademicYear.Id);
        System.assertEquals(expectedAcademicYear.Id, controller.getAcademicYearId());
    }

    @isTest
    private static void me_expectControllerReturned() {
        SchoolFamilyContribWrksheetController controller = setupController();

        System.assertEquals(controller, controller.Me);
    }
}