public with sharing class SchoolRequireAdditionalDocController implements ISchoolRequireAdditionalDoc {

    public Boolean showSaveAndNew { get; set; }
    public Boolean showSaveAndupload { get; set; }
    public String academicYearName { get; set; }
    public ISchoolRequireAdditionalDoc handler { get; set; }
    
    public SchoolRequireAdditionalDocController() {
        
    }
    
    public PageReference saveNewDocument() {
        return handler.saveNewDocument();
    }
    
    public PageReference saveAndNewAdditionalDocument() {
        return handler.saveAndNewAdditionalDocument();
    }
    
    public PageReference saveAndUploadRequiredDocuments() {
        return handler.saveAndUploadRequiredDocuments();
    }
    
    public PageReference cancelAdditionalDocument() {
        return handler.cancelAdditionalDocument();
    }
    
    public School_Document_Assignment__c getAdditionalSchoolDocAssign() {        
        return handler.getAdditionalSchoolDocAssign();
    }
    
    public List<SelectOption> getNonSchoolSpecificDocumentTypes() {        
        return handler.getNonSchoolSpecificDocumentTypes();
    }
    
    public List<SelectOption> getValidDocumentYears() {
        return handler.getValidDocumentYears();
    }
    
    public Boolean getIsTaxDocument() {
        return handler.getIsTaxDocument();
    }
    
    public PageReference loadDocumentYears() {
        return handler.loadDocumentYears();
    }
    
    public String getSubtitle() {
        return handler.getSubtitle();
    }
}