@isTest
private class ExpensesCalculatorTest
{

    private static School_PFS_Assignment__c spfsa1;
    private static Student_Folder__c studentFolder1;

    private static void setupdata() {
        Account school1 = TestUtils.createAccount('Test School 1', RecordTypes.schoolAccountTypeId, 3, false);
        school1.SSS_Subscriber_Status__c = 'Current';
        insert school1;
        
        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        Contact parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
        
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        student1.Gender__c='Female';
        Contact student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
        student2.Gender__c='Male';
        insert new List<Contact> { parentA, parentB, student1, student2 };
        
        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        
        PFS__c pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
        pfsA.Parent_A_Last_Name__c = 'Parent A';
        pfsA.Parent_B_Last_Name__c = 'Parent B';    
        pfsA.Filing_Status__c = 'Married, Filing Jointly';
        pfsA.Parent_B__c = parentB.id;
        pfsA.Original_Submission_Date__c = System.today().adddays(-5);
        
        PFS__c pfsB = TestUtils.createPFS('PFS B', academicYearId, parentB.Id, false);
        pfsB.Parent_A_Last_Name__c = 'Parent B';
        pfsB.Parent_B_Last_Name__c = 'Parent A';            
        pfsB.Original_Submission_Date__c = System.today().adddays(-4);  
        insert new List<PFS__c>{pfsA,pfsB};
        
        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, false);
        applicant1A.Gender__c='Male';
        insert applicant1A;
        
        studentFolder1 = TestUtils.createStudentFolder('Student Folder 1.1', academicYearId, student1.Id, false);
        studentFolder1.Grade_Applying__c = '1';
        studentFolder1.Day_Boarding__c = 'Day';
        studentFolder1.New_Returning__c = 'New';
        insert studentFolder1;
        
        spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder1.Id, false);
        insert spfsa1;
    }
    
    @isTest
    private static void newStudenTestDayNew()
    {
        setupdata();
        
        List<Annual_Setting__c> ann = GlobalVariables.getCurrentAnnualSettings(
                true,
                datetime.now().date(),
                GlobalVariables.getCurrentAcademicYear().Id,
                spfsa1.School__c);
        
        ann[0].Tuition_Schedule_Type__c = 'One Schedule per Grade';
        List<String> gradesPrefix = new List<String>{
                                        'Preschool_',
                                        'Pre_K_',            
                                        'Jr_K_',
                                        'K_',  
                                        'Pre_First_',
                                        'Grade_1_',  
                                        'Grade_2_',
                                        'Grade_3_',  
                                        'Grade_4_',
                                        'Grade_5_',
                                        'Grade_6_',
                                        'Grade_7_',
                                        'Grade_8_',
                                        'Grade_9_',
                                        'Grade_10_',
                                        'Grade_11_',
                                        'Grade_12_',
                                        'Post_Grad_'
                                    };
        List<String> gradesFields = new List<String>{
                                        'Day_Tuition__c',
                                        'Day_Fees__c',            
                                        'Day_Travel_Expense__c',
                                        'Day_New_Student_Expense__c',  
                                        'Day_Ret_Student_Expense__c',
                                        'Boarding_Tuition__c',  
                                        'Boarding_Fees__c',
                                        'Boarding_Travel_Expense__c',  
                                        'Boarding_New_Student_Expense__c',
                                        'Boarding_Ret_Student_Expense__c'
                                    };
        Decimal value = 100;
        for(String grade:gradesPrefix){
            for(String field:gradesFields){
                ann[0].put(grade+field, value);
                value++;
            }
        }
        update ann[0];
        system.assertEquals(ann[0].Academic_Year__c,GlobalVariables.getAcademicYearByName(spfsa1.Academic_Year_Picklist__c).id);
        
        Test.startTest();
            ExpensesCalculator controller = new ExpensesCalculator();
            studentFolder1.Grade_Applying__c = '2';
            Student_Folder__c studentFolder = controller.calculateExpenses(spfsa1.Id,'1','2',studentFolder1,studentFolder1.Day_Boarding__c, null);
            system.assertEquals(ann[0].Grade_2_Day_Tuition__c,studentFolder.Student_Tuition__c);
            system.assertEquals(ann[0].Grade_2_Day_Travel_Expense__c,studentFolder.Travel_Expenses__c);
            system.assertEquals(ann[0].Grade_2_Day_Fees__c,studentFolder.Student_Fees__c);
            system.assertEquals(ann[0].Grade_2_Day_New_Student_Expense__c,studentFolder.New_Student_Expenses__c);
            system.assertEquals(null,studentFolder.Returning_Student_Expenses__c);
        Test.stopTest();
    }

    @isTest
    private static void newStudentTestReturningBoarding()
    {
        setupdata();
        
        studentFolder1.Day_Boarding__c = 'Boarding';
        studentFolder1.New_Returning__c = 'Returning';
        update studentFolder1;
        
        List<Annual_Setting__c> ann = GlobalVariables.getCurrentAnnualSettings(
                true,
                datetime.now().date(),
                GlobalVariables.getCurrentAcademicYear().Id,
                spfsa1.School__c);
        
        ann[0].Tuition_Schedule_Type__c = 'One Schedule per Grade';
        List<String> gradesPrefix = new List<String>{
                                        'Preschool_',
                                        'Pre_K_',            
                                        'Jr_K_',
                                        'K_',  
                                        'Pre_First_',
                                        'Grade_1_',  
                                        'Grade_2_',
                                        'Grade_3_',  
                                        'Grade_4_',
                                        'Grade_5_',
                                        'Grade_6_',
                                        'Grade_7_',
                                        'Grade_8_',
                                        'Grade_9_',
                                        'Grade_10_',
                                        'Grade_11_',
                                        'Grade_12_',
                                        'Post_Grad_'
                                    };
        List<String> gradesFields = new List<String>{
                                        'Day_Tuition__c',
                                        'Day_Fees__c',            
                                        'Day_Travel_Expense__c',
                                        'Day_New_Student_Expense__c',  
                                        'Day_Ret_Student_Expense__c',
                                        'Boarding_Tuition__c',  
                                        'Boarding_Fees__c',
                                        'Boarding_Travel_Expense__c',  
                                        'Boarding_New_Student_Expense__c',
                                        'Boarding_Ret_Student_Expense__c'
                                    };
        Decimal value = 100;
        for(String grade:gradesPrefix){
            for(String field:gradesFields){
                ann[0].put(grade+field, value);
                value++;
            }
        }
        update ann[0];
        system.assertEquals(ann[0].Academic_Year__c,GlobalVariables.getAcademicYearByName(spfsa1.Academic_Year_Picklist__c).id);
        
        Test.startTest();
            ExpensesCalculator controller = new ExpensesCalculator();
            studentFolder1.Grade_Applying__c = '3';
            Student_Folder__c studentFolder = controller.calculateExpenses(spfsa1.Id,'1','3',studentFolder1,studentFolder1.Day_Boarding__c, null);
            system.assertEquals(ann[0].Grade_3_Boarding_Tuition__c,studentFolder.Student_Tuition__c);
            system.assertEquals(ann[0].Grade_3_Boarding_Travel_Expense__c,studentFolder.Travel_Expenses__c);
            system.assertEquals(ann[0].Grade_3_Boarding_Fees__c,studentFolder.Student_Fees__c);
            system.assertEquals(ann[0].Grade_3_Boarding_Ret_Student_Expense__c,studentFolder.Returning_Student_Expenses__c);
            system.assertEquals(null,studentFolder.New_Student_Expenses__c);
        Test.stopTest();
    }

    @isTest
    private static void newStudenTestOneSchedule()
    {
        setupdata();
        
        studentFolder1.Day_Boarding__c = 'Boarding';
        studentFolder1.New_Returning__c = 'Returning';
        update studentFolder1;
        
        List<Annual_Setting__c> ann = GlobalVariables.getCurrentAnnualSettings(
                true,
                datetime.now().date(),
                GlobalVariables.getCurrentAcademicYear().Id,
                spfsa1.School__c);
        
        ann[0].Tuition_Schedule_Type__c = 'One Schedule for Entire School';
        
        List<String> gradesFields = new List<String>{
                                        'Tuition__c',
                                        'Fees__c',            
                                        'Travel_Expense__c',
                                        'New_Student_Expense__c',  
                                        'Ret_Student_Expense__c'
                                    };
        Decimal value = 100;
        for(String prefix:new List<String>{'_Day_','_Boarding_'})
        {
            for(String field:gradesFields){
                ann[0].put((prefix=='_Boarding_' && (field=='New_Student_Expense__c' || field=='Ret_Student_Expense__c')
                            ?'One_Sched':'One_Schedule')+prefix+field, value);
                value++;
            }
        }
        update ann[0];
        system.assertEquals(ann[0].Academic_Year__c,GlobalVariables.getAcademicYearByName(spfsa1.Academic_Year_Picklist__c).id);
        
        Test.startTest();
            ExpensesCalculator controller = new ExpensesCalculator();
            studentFolder1.Grade_Applying__c = '3';
            Student_Folder__c studentFolder = controller.calculateExpenses(spfsa1.Id,'1','3',studentFolder1,studentFolder1.Day_Boarding__c, ann[0]);
            system.assertEquals(ann[0].One_Schedule_Boarding_Tuition__c,studentFolder.Student_Tuition__c);
            system.assertEquals(ann[0].One_Schedule_Boarding_Travel_Expense__c,studentFolder.Travel_Expenses__c);
            system.assertEquals(ann[0].One_Schedule_Boarding_Fees__c,studentFolder.Student_Fees__c);
            system.assertEquals(ann[0].One_Sched_Boarding_Ret_Student_Expense__c,studentFolder.Returning_Student_Expenses__c);
            system.assertEquals(null,studentFolder.New_Student_Expenses__c);
        Test.stopTest();
    }
}