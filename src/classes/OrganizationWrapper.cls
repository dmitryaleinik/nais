/**
 * @description Used to access information about the Organization standard object.
 */
public with sharing class OrganizationWrapper {

    private static Organization orgRecord;
    /**
     * @description Returns whether or not the Organization is a sanbox.
     * @return True if the org is a sandbox, otherwise false.
     */
    public static Boolean isSandbox() {
        if(orgRecord == null) {
            orgRecord = [SELECT Id, IsSandbox FROM Organization LIMIT 1][0];
        }
        return orgRecord.IsSandbox;
    }
}