/**
 * Cases.cls
 *
 * @description Case domain class using fflib domain pattern, implemented solely for handling trigger interactions.
 *
 */
public class Cases extends fflib_SObjectDomain {
    
    public final static Set<String> CLOSED_STATUS = new Set<String>{'Closed', 'Escalation Requested'};
    
    /**
     * @description Default constructor.
     */
    private Cases(List<Case> recordsList) {

        super(recordsList);
        Configuration.disableTriggerCRUDSecurity();
    }
    
    /**
     * @description Handles logic that should be done before Cases are updated.
     * @param oldRecords The old records before any updates were made.
     */
    public override void onBeforeUpdate(Map<Id, SObject> oldRecords) {
        
        //We get the list of cases that have just been closed
        List<Case> closedCases = getCasesWereClosed((List<Case>)Records, (Map<Id, Case>)oldRecords);
        
        // Now we can call our service class to set the Disposition, Type, and Sub-category fields based on the related articles.
	    // In this method, we will skip any record that doesn't have the right case.RecordTypeId or case.Type
	    CaseDispositionService.Instance.setDispositionBasedOnArticles(closedCases);
    }
    
    /**
    * @description Get a list of cases that have just been closed, these are 
    *              the ones that we need to update based on related knowledge articles.
    * @param newRecord The trigger.new records.
    * @param oldRecord The trigger.old records.
    * @return A list of cases that have been closed.
    */
    private List<Case> getCasesWereClosed(List<Case> newRecords, Map<Id, Case> oldRecords) {
        
        List<Case> result = new List<Case>();
        
        for (Case c : newRecords) {
            
            if (CLOSED_STATUS.contains(c.Status) && oldRecords.get(c.Id).Status != c.Status) {
                
                result.add(c);
            }
        }
        
        return result;
    }
    
    /**
     * @description Creates a new instance of the Cases domain class.
     * @param caseRecords The records to create the domain class for.
     * @return An instance of Cases.
     * @throws ArgumentNullException if is caseRecords  null.
     */
    public static Cases newInstance(List<Case>  caseRecords) {
    
        ArgumentNullException.throwIfNull(caseRecords, 'caseRecords');
        
        return new Cases(caseRecords);
    }
}