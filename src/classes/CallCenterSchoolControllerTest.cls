@IsTest
private class CallCenterSchoolControllerTest
{

    private static Account a1;

    private static void  setupData()
    {
        FieldIDbyName__c dnisSetting = new FieldIDbyName__c();
        dnisSetting.Field_ID__c = '1234';
        dnisSetting.Name='dnis';

        FieldIDbyName__c anisSetting = new FieldIDbyName__c();
        anisSetting.Field_ID__c = '2234';
        anisSetting.Name='ani';

        insert new List<FieldIDbyName__c>{dnisSetting, anisSetting};


        a1 = new Account(name = 'account1',SSS_School_Code__c = 'asdf', phone='(222) -123-4444', RecordTypeID = RecordTypes.schoolAccountTypeId);
        Account a2 = new Account(name = 'account2',SSS_School_Code__c = 'asdfg', phone='(222) -123-7232', RecordTypeID = RecordTypes.schoolAccountTypeId);

        insert new List<Account>{a1,a2};
        List<Contact> contacts = new List<Contact>();
        contacts.add(new Contact(accountid =a1.id, LastName = 'c1', mobilephone='(222) -123-1234', RecordTypeID = RecordTypes.schoolStaffContactTypeId));
        contacts.add(new Contact(accountid =a1.id, LastName = 'c2',phone='2221231234', RecordTypeID = RecordTypes.schoolStaffContactTypeId));
        contacts.add(new Contact(accountid =a1.id, LastName = 'c3',homephone='222-123-1234',RecordTypeID = RecordTypes.schoolStaffContactTypeId));
        contacts.add(new Contact(accountid =a1.id, LastName = 'c4', phone='(222) -123-7232', RecordTypeID = RecordTypes.schoolStaffContactTypeId));
        contacts.add(new Contact(accountid =a1.id, LastName = 'c5',homephone='222-123-1234',RecordTypeID = RecordTypes.parentContactTypeId));

        insert contacts;
    }

    @isTest
    private static void testControllerNoContactFound()
    {
        setupData();
        PageReference pageRef = new PageReference('/apex/ContactCenterSchoolCaller?dnis=schooldnis&phone=1234&sessionID=thession&ani=asdf');
        Test.setCurrentPage(pageRef);

        CallCenterSchoolController controller = new CallCenterSchoolController();
        controller.findContacts();

        System.assertEquals(controller.ani, 'asdf');

        System.assertEquals(controller.searchResults, null);

        System.assertEquals(controller.url, null);
        String accountUrl = '/001/e?RecordType='+RecordTypes.schoolAccountTypeId+'&acc10=asdf' +
                + '&retURL='
                + EncodingUtil.urlEncode('/apex/ContactCenterSchoolCaller?', 'UTF-8');
        System.assert(controller.accounturl.startsWith(accountUrl));    // actual order of params in retURL could be scrambled
        //search phone
        controller.searchTerm='7232';
        controller.doSearch();
        System.assertEquals(controller.searchResults.size() , 1);
        //search sss code
        controller.searchTerm='asdf';
        controller.doSearch();
        System.assertEquals(controller.searchResults.size() , 2);
        //invalid search
        controller.searchTerm='3peeo';
        controller.doSearch();
        System.assertEquals(controller.searchResults , null);
        //search name
        controller.searchTerm='account';
        controller.doSearch();
        System.assertEquals(controller.searchResults.size() , 2);
    }

    @isTest
    private static void testControllerSingleContact()
    {
        setupData();
        PageReference pageRef = new PageReference('/apex/ContactCenterSchoolCaller?dnis=schooldnis&phone=1234&sessionID=thession&ani=(222) -123-7232');
        Test.setCurrentPage(pageRef);

        CallCenterSchoolController controller = new CallCenterSchoolController();
        controller.findContacts();

        System.assertEquals(controller.ani, '(222) -123-7232');

        System.assertEquals(controller.searchresults.size(), 1);

        System.assertEquals(controller.url, '/'+controller.searchresults[0].accountid);
    }

    @isTest
    private static void testControllerMultipleResults()
    {
        setupData();
        PageReference pageRef = new PageReference('/apex/ContactCenterAction?dnis=dd&phone=1234&callID=callID&ani=222-123-1234');
        Test.setCurrentPage(pageRef);

        CallCenterSchoolController controller = new CallCenterSchoolController();
        controller.findContacts();

        System.assertEquals(controller.ani, '222-123-1234');
        System.assertEquals(controller.dnis,'dd');
        System.assertEquals(controller.callID, 'callID');

        System.assertEquals(controller.searchresults.size(), 3);

        System.assertEquals(controller.url, '/' + a1.id);
    }
}