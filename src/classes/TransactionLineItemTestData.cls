/**
 * @description This class is used to create Transaction Line Item records for unit tests.
 */
@isTest
public class TransactionLineItemTestData extends SObjectTestData {
    /**
     * @description Get the default values for the Transaction_Line_Item__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Transaction_Line_Item__c.RecordTypeId => getRecordTypeId('Payment'),
                Transaction_Line_Item__c.Amount__c => 400.00,
                Transaction_Line_Item__c.Transaction_Status__c => 'Posted',
                Transaction_Line_Item__c.Opportunity__c => OpportunityTestData.Instance.DefaultOpportunity.Id,
                Transaction_Line_Item__c.Transaction_Date__c => Date.today()
        };
    }

    /**
     * @description Set the record type to Sale.
     * @return The current TransactionLineItemTestData instance.
     */
    public TransactionLineItemTestData asSale() {
        return (TransactionLineItemTestData) with(Transaction_Line_Item__c.RecordTypeId, getRecordTypeId('Sale'));
    }

    /**
     * @description Set the record type to Refund and populate the Status
     *             and Posting date accordingly for a valid Refund Transaction
     *             Line Item.
     * @return The current working instance of TransactionLineItemTestData.
     */
    public TransactionLineItemTestData asRefund() {
        return (TransactionLineItemTestData) with(Transaction_Line_Item__c.RecordTypeId, getRecordTypeId('Refund'))
                                            .with(Transaction_Line_Item__c.Transaction_Status__c, 'Posted')
                                            .with(Transaction_Line_Item__c.Posting_Date__c, Date.today());
    }

    /**
     * @description Set the record type to School Discount Transaction and populate the Transaction Type
     *             as School Discount.
     * @return The current working instance of TransactionLineItemTestData.
     */
    public TransactionLineItemTestData asSchoolDiscount() {
        Id recordTypeId = RecordTypes.schoolDiscountTransactionTypeId;
        return (TransactionLineItemTestData) with(Transaction_Line_Item__c.Transaction_Type__c, 'School Discount')
                                            .with(Transaction_Line_Item__c.RecordTypeId, recordTypeId);
    }

    /**
     * @description Set the amount of the Transaction Line Item to the desired decimal.
     * @param amount The value to set the Amount__c field to.
     * @return The current TransactionLineItemTestData instance.
     */
    public TransactionLineItemTestData forAmount(Decimal amount) {
        return (TransactionLineItemTestData) with(Transaction_Line_Item__c.Amount__c, amount);
    }

    /**
     * @description Set up the necessary fields of the Transaction Line Item to
     *             be a check payment.
     * @return The current TransactionLineItemTestData instance.
     */
    public TransactionLineItemTestData asCheck() {
        return (TransactionLineItemTestData) with(Transaction_Line_Item__c.Transaction_Type__c, 'Check – SSS Account')
                                            .with(Transaction_Line_Item__c.RecordTypeId, getRecordTypeId('Payment'))
                                            .with(Transaction_Line_Item__c.Transaction_Status__c, 'Posted')
                                            .with(Transaction_Line_Item__c.Check_Date__c, Date.today())
                                            .with(Transaction_Line_Item__c.Source__c, 'Mail')
                                            .with(Transaction_Line_Item__c.Transaction_Code__c, '4140')
                                            .with(Transaction_Line_Item__c.Tender_Type__c, 'Check')
                                            .with(Transaction_Line_Item__c.Account_Code__c, '1003')
                                            .with(Transaction_Line_Item__c.Account_Label__c, 'Bank Acct SSS CAO')
                                            .with(Transaction_Line_Item__c.Check_Number__c, '123456')
                                            .with(Transaction_Line_Item__c.VaultGUID__c, 'leVaultGuid')
                                            .with(Transaction_Line_Item__c.Transaction_Date__c, Date.today());
    }

    /**
     * @description Set the Opportunity of the Transaction Line Item record.
     * @return The current TransactionLineItemTestData instance.
     */
    public TransactionLineItemTestData forOpportunity(Id opportunityId) {
        return (TransactionLineItemTestData) with(Transaction_Line_Item__c.Opportunity__c, opportunityId);
    }

    /**
     * @description Insert the current working Transaction_Line_Item__c record.
     * @return The currently operated upon Transaction_Line_Item__c record.
     */
    public Transaction_Line_Item__c insertTransactionLineItem() {
        return (Transaction_Line_Item__c)insertRecord();
    }

    /**
     * @description Create the current working Transaction Line Item record without resetting
     *             the stored values in this instance of TransactionLineItemTestData.
     * @return A non-inserted Transaction_Line_Item__c record using the currently stored field
     *             values.
     */
    public Transaction_Line_Item__c createTransactionLineItemWithoutReset() {
        return (Transaction_Line_Item__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Transaction_Line_Item__c record.
     * @return The currently operated upon Transaction_Line_Item__c record.
     */
    public Transaction_Line_Item__c create() {
        return (Transaction_Line_Item__c)super.buildWithReset();
    }

    /**
     * @description The default Transaction_Line_Item__c record.
     */
    public Transaction_Line_Item__c DefaultTransactionLineItem {
        get {
            if (DefaultTransactionLineItem == null) {
                DefaultTransactionLineItem = createTransactionLineItemWithoutReset();
                insert DefaultTransactionLineItem;
            }
            return DefaultTransactionLineItem;
        }
        private set;
    }

    /**
     * @description Get the Transaction_Line_Item__c SObjectType.
     * @return The Transaction_Line_Item__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Transaction_Line_Item__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static TransactionLineItemTestData Instance {
        get {
            if (Instance == null) {
                Instance = new TransactionLineItemTestData();
            }
            return Instance;
        }
        private set;
    }

    private TransactionLineItemTestData() { }
}