/**
 * @description This class is used to create Family Document records for unit tests.
 */
@isTest
public class HouseholdTestData extends SObjectTestData {

    private static final String NAME = 'Household Test Name';

    private HouseholdTestData() { }

    /**
     * @description Get the default values for the Family_Document__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Household__c.Name => NAME
        };
    }

    /**
     * @description Inserts the household record specified by the current test data.
     * @return A new Household__c record.
     */
    public Household__c insertHousehold() {
        return (Household__c)insertRecord();
    }

    /**
     * @description Gets the Household__c SObjectType.
     * @return The Household__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Household__c.SObjectType;
    }

    /**
     * @description Create the current working Household__c record.
     * @return The currently operated upon Household__c record.
     */
    public Household__c create() {
        return (Household__c)super.buildWithReset();
    }

    /**
     * @description A household record with all the default values.
     */
    public Household__c DefaultHousehold {
        get {
            if (DefaultHousehold == null) {
                DefaultHousehold = (Household__c)insertDefaultRecord();
            }
            return DefaultHousehold;
        }
        private set;
    }

    /**
     * @description Singleton instance of this class.
     */
    public static HouseholdTestData Instance {
        get {
            if (Instance == null) {
                Instance = new HouseholdTestData();
            }
            return Instance;
        }
        private set;
    }
}