/**
 * @description The request to interact with the PaymentProcessor.
 **/
public virtual class PaymentProcessorRequest {}