/**
 * @description This class is used to create Opportunity records for unit tests.
 */
@isTest
public class OpportunityTestData extends SObjectTestData {
    private static final String ACADEMIC_YEAR = AcademicYearService.Instance.getCurrentAcademicYear().Name;

    /**
     * @description Get the default values for the Opportunity object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Opportunity.AccountId => AccountTestData.Instance.DefaultAccount.Id,
                Opportunity.Name => 'Placeholder Name',
                Opportunity.New_Renewal__c => 'New',
                Opportunity.StageName => 'Prospecting',
                Opportunity.CloseDate => System.today(),
                Opportunity.Subscription_Period_Start_Year__c => ACADEMIC_YEAR.left(4),
                Opportunity.Academic_Year_Picklist__c => ACADEMIC_YEAR,
                Opportunity.Subscription_Type__c => 'Full',
                Opportunity.Amount => 47.00,
                Opportunity.RecordTypeId => getRecordTypeId('Subscription Fee')
        };
    }

    /**
     * @description Set the AccountId to the given accountId.
     * @param accountId The Account Id to set the accountId field to.
     * @return The current OpportunityTestData instance.
     */
    public OpportunityTestData forAccount(Id accountId) {
        return (OpportunityTestData) with(Opportunity.AccountId, accountId);
    }

    /**
     * @description Set the PFS__c to the given Pfs Id.
     * @param pfsId The Id of the PFS record to set PFS__c to.
     * @return The current working OpportunityTestData instance.
     */
    public OpportunityTestData forPfs(Id pfsId) {
        return (OpportunityTestData) with(Opportunity.RecordTypeId, getRecordTypeId('PFS Application Fee'))
                                    .with(Opportunity.PFS__c, pfsId);
    }

    /**
     * @description Set the StageName to Open.
     * @return The current working OpportunityTestData instance.
     */
    public OpportunityTestData asOpen() {
        return (OpportunityTestData) with(Opportunity.StageName, 'Open');
    }

    /**
     * @description Creates Opportunity record as system creates after Fee Waiver Purchasing.
     * @return The current working OpportunityTestData instance.
     */
    public OpportunityTestData asFeeWaiverPurchasing() {
        return (OpportunityTestData) with(Opportunity.StageName, 'Closed')
                                    .with(Opportunity.Amount, 48.00)
                                    .with(Opportunity.RecordTypeId, getRecordTypeId('PFS Application Fee'))
                                    .with(Opportunity.PFS__c, PfsTestData.Instance.DefaultPfs.Id);
    }

    /**
     * @description Insert the current working Opportunity record.
     * @return The currently operated upon Opportunity record.
     */
    public Opportunity insertOpportunity() {
        return (Opportunity)insertRecord();
    }

    /**
     * @description Insert the opportunity, a sale Transaction Line Item, and 1 payment
     *             Transaction Line Item to force this Opportunity to be marked as paid.
     * @return The Opportunity record currently be constructed.
     */
    public Opportunity insertAsPaid() {
        Opportunity record = insertOpportunity();

        TransactionLineItemTestData.Instance.forOpportunity(record.Id).asSale().forAmount(record.Amount).insertTransactionLineItem();
        TransactionLineItemTestData.Instance.forOpportunity(record.Id).asCheck().forAmount(record.Amount).insertTransactionLineItem();

        record = OpportunitySelector.Instance.selectByIdWithPfs(new Set<Id> { record.Id })[0];

        return record;
    }

    /**
     * @description Insert the opportunity, a sale Transaction Line Item, and 2 payment
     *             Transaction Line Items to force this Opportunity to be marked as overpaid.
     * @return The Opportunity record currently be constructed.
     */
    public Opportunity insertAsOverpaid() {
        Opportunity record = insertOpportunity();

        TransactionLineItemTestData.Instance.forOpportunity(record.Id).asSale().forAmount(record.Amount).insertTransactionLineItem();
        TransactionLineItemTestData.Instance.forOpportunity(record.Id).asCheck().forAmount(record.Amount).insertTransactionLineItem();
        TransactionLineItemTestData.Instance.forOpportunity(record.Id).asCheck().forAmount(record.Amount).insertTransactionLineItem();

        record = OpportunitySelector.Instance.selectByIdWithPfs(new Set<Id> { record.Id })[0];

        return record;
    }

    /**
     * @description Insert the opportunity, and a sale Transaction Line Item to force
     *             this Opportunity to be marked as unpaid. Update the StageName to be
     *             Closed Won.
     * @return The Opportunity currently being constructed.
     */
    public Opportunity insertAsClosedWon() {
        Opportunity record = insertOpportunity();

        TransactionLineItemTestData.Instance.forOpportunity(record.Id).asSale().forAmount(record.Amount).insertTransactionLineItem();
        record.StageName = 'Closed Won';
        update record;

        record = OpportunitySelector.Instance.selectByIdWithPfs(new Set<Id> { record.Id })[0];

        return record;
    }

    /**
     * @description Insert the opportunity, a sale Transaction Line Item and a payment
     *             Transaction Line Item to force this Opportunity to be marked as
     *             unpaid. Update the StageName to be Closed Won.
     * @return The Opportunity currently being constructed.
     */
    public Opportunity insertAsClosedWonAndPaid() {
        Opportunity record = insertOpportunity();

        TransactionLineItemTestData.Instance.forOpportunity(record.Id).asSale().forAmount(record.Amount).insertTransactionLineItem();
        TransactionLineItemTestData.Instance.forOpportunity(record.Id).asCheck().forAmount(record.Amount).insertTransactionLineItem();
        record.StageName = 'Closed Won';
        update record;

        record = OpportunitySelector.Instance.selectByIdWithPfs(new Set<Id> { record.Id })[0];

        return record;
    }

    /**
     * @description Create the current working Opportunity record without resetting
     *             the stored values in this instance of OpportunityTestData.
     * @return A non-inserted Opportunity record using the currently stored field
     *             values.
     */
    public Opportunity createOpportunityWithoutReset() {
        return (Opportunity)buildWithoutReset();
    }

    /**
     * @description Create the current working Opportunity record.
     * @return The currently operated upon Opportunity record.
     */
    public Opportunity create() {
        return (Opportunity)super.buildWithReset();
    }

    /**
     * @description The default Opportunity record.
     */
    public Opportunity DefaultOpportunity {
        get {
            if (DefaultOpportunity == null) {
                DefaultOpportunity = createOpportunityWithoutReset();
                insert DefaultOpportunity;
            }
            return DefaultOpportunity;
        }
        private set;
    }

    /**
     * @description Get the Opportunity SObjectType.
     * @return The Opportunity SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Opportunity.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static OpportunityTestData Instance {
        get {
            if (Instance == null) {
                Instance = new OpportunityTestData();
            }
            return Instance;
        }
        private set;
    }

    private OpportunityTestData() { }
}