/* 
 * Copyright (c) 2016, salesforce.com, inc.
 * All rights reserved.
 * Licensed under the BSD 3-Clause license. 
 * For full license text, see LICENSE.txt file in the repo root  or https://opensource.org/licenses/BSD-3-Clause
 */
 
@IsTest
private class CustomMetadataLoaderControllerTest
{

    @isTest
    private static void testCheckMdApiSucceeds() {
        CustomMetadataLoaderController cntlr = setup();
        PageReference result = cntlr.checkMdApi();
        System.assertEquals(Page.CustomMetadataRecordUploader.getUrl(), result.getUrl());
    }
    
    @isTest
    private static void testCheckMdApiFails() {
        CustomMetadataLoaderController cntlr = setup();
        ApexPages.currentPage().getHeaders().put('Host', 'na1.salesforce.com');
        MetadataUtil.mdApiStatus = MetadataUtil.Status.UNAVAILABLE;
        try {
            PageReference result = cntlr.checkMdApi();
            System.assertEquals(result, null);
            System.assertEquals('na1', cntlr.prefixOrLocal);
        } finally {
            MetadataUtil.mdApiStatus = MetadataUtil.Status.NOT_CHECKED;
        }
    }
    
    @isTest
    private static void testDisplayMetadataResponse() {
        CustomMetadataLoaderController cntlr = setup();
        cntlr.metadataResponse = '';
        cntlr.displayMetadataResponse();
        System.assert(!cntlr.metadataConnectionWarning);
        cntlr.metadataResponse = 'Danger, Will Robinson!';
        cntlr.displayMetadataResponse();
        System.assert(cntlr.metadataConnectionWarning);
    }
    
    private static CustomMetadataLoaderController setup() {
        Test.setMock(WebServiceMock.class, new MDWrapperWebServiceMock());
        return new CustomMetadataLoaderController();
    }
}