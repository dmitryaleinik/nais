/**
 * @description This batch class will query all SpringCM Documents created after a certain date and determine if the
 *              pertains to value was incorrectly calculated. If so, a new checkbox field will be checked and those
 *              records will be updated.
 */
public class FindSCMDocsWithWrongPertainsToBatch implements Database.Batchable<SObject> {

    @testVisible private static final String DOC_TYPES_TO_CHECK_PARAM = 'docTypesToCheck';
    @testVisible private static final String DOC_SOURCES_TO_CHECK_PARAM = 'docSourcesToCheck';
    @testVisible private static final String CREATED_ON_OR_AFTER_PARAM = 'createdOnOrAfter';
    // The scalehub go live date is the date that we added logic for matching the doc pertains to ourselves. Before this date we relied on Databank for doing this.
    // Therefore, this is the default date filter used by the query. We shouldn't run this job on any records created before this date.
    // If someone attempts to run this batch for records created before this date, an exception will be thrown.
    @testVisible private static final Date SCALEHUB_GO_LIVE = Date.newInstance(2017, 10, 25);

    public Set<String> DocTypes { get; private set; }
    public Set<String> DocSources { get; private set; }
    public Date CreatedAfterDate { get; private set; }

    public FindSCMDocsWithWrongPertainsToBatch() {
        DocTypes = new Set<String> { 'W2', 'IRS_1040' };
        DocSources = new Set<String> { 'Uploaded By Parent', 'Uploaded By School' };
        // Default date is the date that we went live with scalehub.
        CreatedAfterDate = SCALEHUB_GO_LIVE;
    }

    public FindSCMDocsWithWrongPertainsToBatch(Set<String> docTypesToCheck, Set<String> docSourcesToCheck, Date createdOnOrAfter) {
        ArgumentNullException.throwIfNull(docTypesToCheck, DOC_TYPES_TO_CHECK_PARAM);
        ArgumentNullException.throwIfNull(docSourcesToCheck, DOC_SOURCES_TO_CHECK_PARAM);
        ArgumentNullException.throwIfNull(createdOnOrAfter, CREATED_ON_OR_AFTER_PARAM);

        if (docTypesToCheck.isEmpty() || docSourcesToCheck.isEmpty()) {
            throw new ParamException('The Doc Types To Check and Doc Sources To Check can not be empty.');
        }

        if (createdOnOrAfter < SCALEHUB_GO_LIVE) {
            throw new ParamException('The date filter can not be before the Scalehub Go Live Date: 10/25/2017. This batch should only be run on records where our system calculated the Document Pertains To value.');
        }

        DocTypes = docTypesToCheck;
        DocSources = docSourcesToCheck;
        CreatedAfterDate = createdOnOrAfter;
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT Id, PFS_Number__c, First_Name__c, Last_Name__c, Name__c, Document_Pertains_to__c, Document_Type__c, Document_Source__c, Match_Not_Found_Reason__c, Match_Not_Found__c FROM SpringCM_Document__c WHERE CreatedDate >= :CreatedAfterDate AND Document_Type__c IN :DocTypes AND Document_Source__c IN :DocSources ORDER BY CreatedDate, Id]);
    }

    public void execute(Database.BatchableContext bc, List<SObject> scope) {
        List<SpringCM_Document__c> springCMDocuments = (List<SpringCM_Document__c>)scope;

        // If the list of records to process is empty or the Match_Parent_To_SpringCM_Doc__c feature is disabled return early.
        // If the Match_Parent_To_SpringCM_Doc__c feature is disabled, the DocumentNameMatchService won't do any calculations
        // so there would be no need for any DML.
        if (!FeatureToggles.isEnabled('Match_Parent_To_SpringCM_Doc__c') || springCMDocuments.isEmpty()) {
            return;
        }

        // Clone the SpringCM Documents and place them into a map so we can compare the Pertains To values after re-running
        // the DocumentNameMatchService and determine which ones are incorrect.
        Map<Id, SpringCM_Document__c> scmDocsToCompareById = new Map<Id, SpringCM_Document__c>(springCMDocuments.deepClone(true));

        // Now clear out the Pertains To values on the SCM Docs to recalc so that the DocumentNameMatchService does a recalculation.
        for (SpringCM_Document__c scmDoc : springCMDocuments) {
            scmDoc.Document_Pertains_to__c = null;
        }

        DocumentNameMatchService.Instance.setDocumentPertainsTo(springCMDocuments);

        Map<Id, SpringCM_Document__c> springCMDocumentsToUpdateById = new Map<Id, SpringCM_Document__c>();

        for (SpringCM_Document__c scmDoc : springCMDocuments) {
            SpringCM_Document__c scmDocToCompare = scmDocsToCompareById.get(scmDoc.Id);

            if (scmDocToCompare == null) {
                continue;
            }

            // If the Pertains To Value has changed, set the original value and mark the record so that we know the new
            // pertains to value needs to be transferred to the family document and that we will have to run some
            // verification sum recalcs.
            if (scmDoc.Document_Pertains_to__c != scmDocToCompare.Document_Pertains_to__c) {
                scmDoc.Original_Pertains_To__c = scmDocToCompare.Document_Pertains_to__c;
                scmDoc.Pertains_To_Transfer_Required__c = true;
                springCMDocumentsToUpdateById.put(scmDoc.Id, scmDoc);
            }
        }

        if (!springCMDocumentsToUpdateById.isEmpty()) {
            update springCMDocumentsToUpdateById.values();
        }
    }

    public void finish(Database.BatchableContext bc) { }

    /**
     * @description This exception is thrown if someone tries to start the batch with an invalid parameter.
     */
    public class ParamException extends Exception { }
}