/**
 * @description Trigger Handler for Transaction Line Items.
 **/
public class TransactionLineItemHandler implements TriggerHandler {
    // used to prevent recursive calls
    public static Boolean isRunning = false;

    /**
     * @description Handle after insert processes for Transaction Line Items.
     * @param newSObjects The new Transaction Line Items that have been inserted.
     */
    public void afterInsert(Map<Id,sObject> newSObjects) {
        // This is to retain previous functionality to guard against an infinite recursive loop.
        if (isRunning) return;
        isRunning = true;

        Map<Id, Transaction_Line_Item__c> transactionLineItemsById = (Map<Id, Transaction_Line_Item__c>)newSObjects;
        handleSubscriptions(transactionLineItemsById);

        // TODO: TDS-10 Refactor Existing logic retained for write off of reversals.
        // requery to get fields we need for all methods [dp]
        List<Transaction_Line_Item__c> transactionLineItems = [Select Opportunity__c, Opportunity__r.Amount, Id, Transaction_Type__c, Opportunity__r.StageName, RecordTypeId, Transaction_Status__c, RecordType.Name, Opportunity__r.PFS__c, Opportunity__r.PFS__r.Payment_Status__c from Transaction_Line_Item__c where id in :transactionLineItemsById.keySet()];
        // auto-create Write-Off Reversals before we update statuses

        feeWriteOffReversalCreation(transactionLineItems);
    }

    /**
     * @description Handle after delete processes for Transaction Line Items.
     * @param newSObjects There will be no new SObjects in the after delete trigger.
     * @param oldSObjects The SObjects that were deleted.
     */
    public void afterDelete(Map<Id,sObject> newSObjects, Map<Id,sObject> oldSObjects) {
        Map<Id, Transaction_Line_Item__c> transactionLineItemsById = (Map<Id, Transaction_Line_Item__c>)oldSObjects;
        handleSubscriptions(transactionLineItemsById);
    }

    /**
     * @description Handle before insert processes for Transaction Line Items.
     * @param newSObjects The SObjects that are going to be inserted.
     */
    public void beforeInsert(List<SObject> newSObjects) {
        TransactionDuplicateFlagAction.flagDuplicateTransactions((List<Transaction_Line_Item__c>)newSObjects);
    }

    /**
     * @description Placeholder methods allowing the handler to conform to the interface.
     */
    public void beforeUpdate(Map<Id,sObject> newSObjects, Map<Id,sObject> oldSObjects) { }
    public void afterUpdate(Map<Id,sObject> newSObjects, Map<Id,sObject> oldSObjects) { }
    public void beforeDelete(Map<Id,sObject> newSObjects, Map<Id,sObject> oldSObjects) { }
    public void afterUnDelete(Map<Id,sObject> newSObjects, Map<Id,sObject> oldSObjects) { }

    /*
     * When the Opportunity of type Application Fee is paid then the Payment status
     * on the PFS needs to be updated - Partially Paid, Paid in Full and Written Off
     * are the possible values that will need to be set when the opp is updated.
     * Nathan Shinn, Exponent Partners, 2013
     */
    // [SL] NAIS-943 - code refactor - trigger this off the Opportunity instead.
    /*
        [CH] NAIS-2442 OLD CODE REMOVED
    */


    /* NAIS-530 - Finance - Fee Write-off Reversal
    * When a payment is received after a write-off has already been posted, a new Transaction Line Item of type Write-off Reversal
    * should be automatically created, with the same amount as the Write-off Transaction, and the status of the Opp and PFS would
    * change to paid which would cause the schools to have immediate access to it.
    *
    * Drew Piston, Exponent Partners, 2013
    */
    private static void feeWriteOffReversalCreation(list<Transaction_Line_Item__c> transactionLineItems){
        // this list is used for fee write-off reversal
        List<Transaction_Line_Item__c> paymentsForFeeWriteOffReversal = new List<Transaction_Line_Item__c>();
        // this list is for new transaction line items
        List<Transaction_Line_Item__c> transactionsToInsert = new List<Transaction_Line_Item__c>();

        // set of payment record type Ids
        Set<Id> paymentRecordTypeIds = new Set<Id>();
        paymentRecordTypeIds.add(RecordTypes.paymentTransactionTypeId);
        paymentRecordTypeIds.add(RecordTypes.paymentAutoTransactionTypeId);
        paymentRecordTypeIds.add(RecordTypes.pfsFeeWaiverTransactionTypeId); // [SL] NAIS-1235: reverse write-off for waivers too

        for(Transaction_Line_Item__c t : transactionLineItems)
        {
            // populate the paymentsForFeeWriteOffReversal list if this is a payment TLI, and the opp has been written off or PFS is written off
            // [SL] NAIS-1235: added check to make sure it's not a waiver reversal
            if (paymentRecordTypeIds.contains(t.RecordTypeId) && (t.Opportunity__r.StageName == 'Written Off') && (t.Transaction_Type__c != 'Fee Waiver Reversal')){
                paymentsForFeeWriteOffReversal.add(t);
            }
        }

        // if conditions have been met, create new Write Off Reversals with negative amount of payment
        if (!paymentsForFeeWriteOffReversal.isEmpty()) {
            Set<Id> oppIds = new Set<Id>();

            for (Transaction_Line_Item__c t : paymentsForFeeWriteOffReversal) {
                oppIds.add(t.Opportunity__c);
            }

            // opp Id to Opp
            map<Id, Opportunity> oppMap = new map<Id, Opportunity>();
            oppMap.putAll([Select Id, Total_Write_Offs__c FROM Opportunity where Id in :oppIds]);

            for (Transaction_Line_Item__c t : paymentsForFeeWriteOffReversal) {
                Decimal newAmount = 0;
                newAmount = oppMap.get(t.Opportunity__c) != null ? oppMap.get(t.Opportunity__c).Total_Write_Offs__c : newAmount;
                newAmount = newAmount < 0 ? 0 : newAmount;
                Transaction_Line_Item__c newT = new Transaction_Line_Item__c();
                newT.Opportunity__c = t.Opportunity__c;
                newT.RecordTypeID = RecordTypes.adjustmentTransactionTypeId;
                newT.Transaction_Type__c = 'Write-off Reversal';

                // [CH] NAIS-1489 Adding auto-population of codes from custom setting
                Transaction_Settings__c settingRecord = Transaction_Settings__c.getValues(newT.Transaction_Type__c);

                if(settingRecord != null){
                    newT.Transaction_Code__c = settingRecord.Transaction_Code__c;
                    newT.Account_Code__c = settingRecord.Account_Code__c;
                    newT.Account_Label__c = settingRecord.Account_Label__c;
                }

                newT.Transaction_Status__c = 'Posted';
                newT.Transaction_Date__c = System.today();
                newT.Amount__c = newAmount;
                transactionsToInsert.add(newT);
            }
        }

        if (!transactionsToInsert.isEmpty()) {
            insert transactionsToInsert;
        }
    }

    private static void handleSubscriptions(Map<Id, Transaction_Line_Item__c> transactionLineItemsByIds) {
        if (GlobalVariables.isFamilyPortalUser() ||
            transactionLineItemsByIds == null ||
            transactionLineItemsByIds.isEmpty()) {

            return;
        }

        List<Transaction_Line_Item__c> transactionLineItems = transactionLineItemsByIds.values();
        Set<Id> opportunityIds = new Set<Id>();

        for (Transaction_Line_Item__c transactionLineItem : transactionLineItems) {
            if (transactionLineItem.Transaction_Status__c == 'Posted' &&
                transactionLineItem.Opportunity__c != null) {
                opportunityIds.add(transactionLineItem.Opportunity__c);
            }
        }

        if (opportunityIds.isEmpty()) {
            return;
        }

        List<Opportunity> opportunities = OpportunitySelector.Instance
                .selectByIdWithTransactionLineItems(opportunityIds);
        if (opportunities.isEmpty()) {
            return;
        }

        List<Opportunity> opportunitiesWithSubscriptionsToDelete = new List<Opportunity>();
        for (Opportunity opportunity : opportunities) {
            if (opportunity.Amount == null) {
                continue;
            }

            List<Transaction_Line_Item__c> opportunityTransactionLineItems = opportunity.Transaction_Line_Items__r;
            Decimal newTotalDue = opportunity.Amount;

            for (Transaction_Line_Item__c opportunityTransactionLineItem : opportunityTransactionLineItems) {
                if (opportunityTransactionLineItem.RecordTypeId == RecordTypes.saleTransactionTypeId ||
                    opportunityTransactionLineItem.Amount__c == null) {

                    continue;
                }

                if (opportunityTransactionLineItem.RecordTypeId == RecordTypes.refundTransactionTypeId) {
                    newTotalDue += opportunityTransactionLineItem.Amount__c;
                } else {
                    newTotalDue -= opportunityTransactionLineItem.Amount__c;
                }
            }

            if (newTotalDue > 0) {
                opportunitiesWithSubscriptionsToDelete.add(Opportunity);
            }
        }

        SubscriptionService.Instance.deleteSubscriptions(opportunitiesWithSubscriptionsToDelete);
    }
}