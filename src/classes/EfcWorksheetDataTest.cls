@isTest
private class EfcWorksheetDataTest {
    @isTest
    private static void isDaySchool_noMainApplicantSpecified_expectTrue() {
        // We are just using an academic year Id since it is simple to create and the actual type of Id doesn't matter for these tests.
        Id mockApplicantId = TestUtils.createAcademicYear('2016-2017', true).Id;

        // Create worksheet data without specifying the main applicant.
        EfcWorksheetData worksheet = new EfcWorksheetData();

        System.assertEquals(true, worksheet.isDaySchool(),
                'Expected true if the main applicant is not specified like we would see when creating EFC Data from a PFS.');
    }

    @isTest
    private static void isDaySchool_mainApplicantSpecified_applicantIsDaySchool_expectTrue() {
        // We are just using an academic year Id since it is simple to create and the actual type of Id doesn't matter for these tests.
        Id mockApplicantId = TestUtils.createAcademicYear('2016-2017', true).Id;

        EfcWorksheetData worksheet = new EfcWorksheetData();

        worksheet.MainApplicantId = mockApplicantId;
        worksheet.applicants[0].sourceApplicantId = mockApplicantId;
        worksheet.applicants[0].dayOrBoarding = EfcPicklistValues.DAYBOARDING_DAY;

        System.assertEquals(true, worksheet.isDaySchool(),
                'Expected true if the main applicant is attending day school.');
    }

    @isTest
    private static void isDaySchool_mainApplicantSpecified_applicantIsBoardingSchool_expectFalse() {
        // We are just using an academic year Id since it is simple to create and the actual type of Id doesn't matter for these tests.
        Id mockApplicantId = TestUtils.createAcademicYear('2016-2017', true).Id;

        EfcWorksheetData worksheet = new EfcWorksheetData();

        worksheet.MainApplicantId = mockApplicantId;
        worksheet.applicants[0].sourceApplicantId = mockApplicantId;
        worksheet.applicants[0].dayOrBoarding = EfcPicklistValues.DAYBOARDING_BOARDING;

        System.assertEquals(false, worksheet.isDaySchool(),
                'Expected false if the main applicant is attending boarding school.');
    }

    @isTest
    private static void isDaySchool_mainApplicantSpecified_applicantDayBoardingValueNull_expectTrue() {
        // We are just using an academic year Id since it is simple to create and the actual type of Id doesn't matter for these tests.
        Id mockApplicantId = TestUtils.createAcademicYear('2016-2017', true).Id;

        EfcWorksheetData worksheet = new EfcWorksheetData();

        worksheet.MainApplicantId = mockApplicantId;
        worksheet.applicants[0].sourceApplicantId = mockApplicantId;
        worksheet.applicants[0].dayOrBoarding = null;

        System.assertEquals(true, worksheet.isDaySchool(),
                'Expected true if the main applicant does not specify day or boarding school.');
    }
}