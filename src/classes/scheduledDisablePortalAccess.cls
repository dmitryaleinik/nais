/*
 * NAIS-2013
 *
 *    Scheduled batch Apex to disable Portal Access to Inactive users
 *
 * Leydi, 2015
 */
global class scheduledDisablePortalAccess implements Schedulable {
   global void execute(SchedulableContext SC) {
      Database.executeBatch(new userPortalEnabledActionBatch());
   }
}