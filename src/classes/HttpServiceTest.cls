/**
 * HttpServiceTest.cls
 *
 * @description: Test class for HttpService using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest
private class HttpServiceTest {
    
    /* test data setup */
    @testSetup static void setupTestData() {

        // any necessary data setup
    }
    
    /* negative test cases */

    // call sendHttpRequest without an endpoint
    @isTest static void sendHttpRequest_noEndpoint_emptyResponse() {

        // Arrange
        Test.setMock( HttpCalloutMock.class, new SendgridMockHttpResponseGenerator());

        // Act
        String httpResp = HttpService.sendHttpRequest( null, null);

        // Assert
        System.assertEquals( '', httpResp);
    }


    /* postive test cases */

    // call overloaded sendHttpRequest with endpoint & method
    @isTest static void sendHttpRequest_withTwoParams_validResponse() {

        // Arrange
        Test.setMock( HttpCalloutMock.class, new SendgridMockHttpResponseGenerator());

        // Act
        String httpResp = HttpService.sendHttpRequest( 'myEndpoint', HttpService.HTTP_POST);

        // Assert
        System.assertEquals( '{"message":"success"}', httpResp);
    }

    // call overloaded sendHttpRequest with endpoint, body, & method
    @isTest static void sendHttpRequest_withThreeParams_validResponse() {

        // Arrange
        Test.setMock( HttpCalloutMock.class, new SendgridMockHttpResponseGenerator());

        // Act
        String httpResp = HttpService.sendHttpRequest( 'myEndpoint', 'myBody', HttpService.HTTP_POST);

        // Assert
        System.assertEquals( '{"message":"success"}', httpResp);
    }

    // call sendHttpRequest with endpoint, body, headers, & method
    @isTest static void sendHttpRequest_withFourParams_validResponse() {

        // Arrange
        Test.setMock( HttpCalloutMock.class, new SendgridMockHttpResponseGenerator());

        // Act
        String httpResp = HttpService.sendHttpRequest( 'myEndpoint', new Map<String,String> { 'header' => 'val'}, 'myBody', HttpService.HTTP_POST);

        // Assert
        System.assertEquals( '{"message":"success"}', httpResp);
    }
    
}