/**
 * IntegrationFinalize
 *
 * @description Use this class to handle DML operations of Applicants, Student_Folders and Student PFS Assignemtns.
 * Best used as a post process on the last api call in Integration_API_Settings__c;
 *
 * @author Mike Havrilla @ Presence PG
 */
public class SAOProfileIntegrationFinalize implements IntegrationApiModel.IAction {

    public void invoke( IntegrationApiModel.ApiState apiState) {
        
        // handles all the dml operations for creating applicants, student folders and student pfs assignments.
        IntegrationUtils.handleIntegrationDML(apiState, FamilyIntegrationModel.STUDENT_APPLICATION);
    }
}