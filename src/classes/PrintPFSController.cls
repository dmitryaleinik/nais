public with sharing class PrintPFSController {
    
    public transient PFS__c pfs { get; set; }
    public transient ApplicationUtils appUtils { get; set; }
    public transient String currentScreen {get; set;}

}