@isTest
public class SchoolSetWeightedPFSTestHelpers {
    
    public static List<Weighted_PFS__c> getWeightedPfssByAnnualSetAndPfsId(Id annualSettingId, Id pfsId) {
        return [
            SELECT Id, School_Count__c, Annual_Setting__r.School__c, Weighted_School_Count__c
            FROM Weighted_PFS__c
            WHERE Annual_Setting__c = :annualSettingId 
                AND PFS__c = :pfsId];
    }

    public static List<Weighted_PFS__c> getWeightedPfssByPfsId(Id pfsId) {
        return [
            SELECT Id, Annual_Setting__r.School__c, School_Count__c
            FROM Weighted_PFS__c
            WHERE PFS__c = :pfsId];
    }

    public static List<Weighted_PFS__c> getAllWeightedPfs() {
        return [SELECT Id, PFS__c, Annual_Setting__c FROM Weighted_PFS__c];        
    }

    ////public static PFS__c getPfsById(Id pfsId) {
    ////    return [SELECT Id, SYSTEM_WPFS_Recalc__c FROM PFS__c WHERE Id = :pfsId];
    ////}

    ////public static Annual_Setting__c getAnnualSettingById(Id settingId) {
    ////    return [SELECT Id, Total_PFS_Count__c, wPFS_Count__c FROM Annual_Setting__c WHERE Id = :settingId];
    ////}

    ////public static List<Annual_Setting__c> getAllAnnualSettings() {
    ////    return [Select Id, Total_PFS_Count__c, WPFS_Count__c FROM Annual_Setting__c];
    //}



    
}