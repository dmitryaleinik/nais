@isTest
private class SchoolFCWQuickTest
{

    private static Id academicYearIdTmp;
    private static Account school1;
    private static School_PFS_Assignment__c spfsa1, spfsa2;
    private static Student_Folder__c studentFolder11, studentFolder12;
    private static PFS__c pfsA1, pfsA2;

    @isTest
    private static void testSchoolFamilyContributionWorksheetQuick()
    {
        createTestData();

        PageReference fcwQuick = Page.GuidedFolderReview;
        fcwQuick.getParameters().put('schoolpfsassignmentid', spfsa1.id);
        Test.setCurrentPage(fcwQuick);
        SchoolFamilyContributionWorksheetQuick fcwq = new SchoolFamilyContributionWorksheetQuick();
        boolean testboo = fcwq.showFullFCW;
        fcwq.toggleFullFCW();
        fcwq.getSchoolPFSAssignment();
        fcwq.getPFS();
        fcwq.getVerification();
        fcwq.getSSSConstants();
        fcwq.getFamilyDocumentIds();
        fcwq.getUnusualConditions();
        fcwq.getStudentNamePossessive();
        fcwq.getFolder();
        fcwq.getLastYearFolder();
        fcwq.getCurrentYearPFS();
        fcwq.getPreviousYearPFS();
        fcwq.getCurrentAcademicYear();
        fcwq.getPreviousAcademicYear();
        fcwq.getFieldSizeLimitMap();
        fcwq.getPFS();
        fcwq.commentToMark = 'testing';
        fcwq.statusToMark = 'Not An Issue';
        fcwq.statusRowToMark = 'studentAssets';
        fcwq.markRowStatus();
        fcwq.commentToMark = null;
        fcwq.statusToMark = 'Not An Issue';
        fcwq.statusRowToMark = 'studentAssets';
        fcwq.markRowStatus();
        fcwq.getThresholdRowData();
        fcwq.getRowNames();
        fcwq.getRowData();
        fcwq.getPositiveCommentary();
        fcwq.getNegativeCommentary();
        fcwq.getLineNumber();

        fcwq.getpyThreshold();
        fcwq.getCurrentYearString();
        fcwq.getPreviousYearString();
        fcwq.getCommentIndicatorMap();
        fcwq.getAcademicYearId();

        fcwq.setAcademicYearId(fcwq.getAcademicYearId());
        fcwq.SchoolAcademicYearSelectorStudent_OnChange(fcwquick);
        fcwq.continueReviewFromFCW();

        fcwq.getcurrentYearFamilyDocuments();
        fcwq.getpreviousYearFamilyDocuments();

        SchoolFamilyContributionWorksheetQuick me = fcwq.me;
        string path = fcwq.commentPath;
        fcwq.frmBulk = true;

        ContentVersion contentDoc = new ContentVersion(
            Title = SchoolFamilyContributionWorksheetQuick.GUIDED_FOLDER_REVIEW_HELP_DOC,
            PathOnClient = 'test', VersionData = Blob.valueOf('test'));
        insert contentDoc;
        System.assertEquals(contentDoc.Id, fcwq.guidedFolderReviewDocId);
    }

    @isTest
    private static void testPFSRowData()
    {
        PFSRowData prd = new PFSRowData('test','test',2016,'test','Not An Issue');
    }

    @isTest
    private static void testFCWTable()
    {
        SchoolFCWTable fcwt = new SchoolFCWTable();
        fcwt.fcwController = new SchoolFCWController();
    }

    @isTest
    private static void testSchoolFCWQPyAnalysis()
    {
        SchoolFCWQPyAnalysis analysis = new SchoolFCWQPyAnalysis();
        analysis.getRowNameToProperName();
    }

    @isTest
    private static void testSchoolFCWContainer()
    {
        createTestData();

        PageReference fcwContainer = Page.GuidedFolderReview;
        fcwContainer.getParameters().put('schoolpfsassignmentid', spfsa1.id);
        Test.setCurrentPage(fcwContainer);
        SchoolFCWContainer fcwc = new SchoolFCWContainer();
        fcwc.getSchoolPFSAssignment();
        fcwc.getPFS();
        fcwc.getVerification();
        fcwc.getSSSConstants();
        fcwc.getFamilyDocumentIds();
        fcwc.getCurrentYearFamilyDocuments();
        fcwc.getPreviousYearFamilyDocuments();
        fcwc.getUnusualConditions();
        fcwc.getCurrentAcademicYear();
        fcwc.getCurrentYearPFS();
    }

    @isTest
    private static void testStudentFolder_empty()
    {
        PageReference fcwContainer = Page.GuidedFolderReview;
        fcwContainer.getParameters().put('schoolpfsassignmentid', 'invalidId');
        Test.setCurrentPage(fcwContainer);

        Test.startTest();
            SchoolFamilyContributionWorksheetQuick controller = new SchoolFamilyContributionWorksheetQuick();
            try
            {
                system.assertEquals(null, controller.studentFolder.Id);
            }
            catch(Exception ex)
            {
                ex.getMessage().contains('Invalid Id');
            }
        Test.stopTest();
    }

    @isTest
    private static void testStudentFolder_NotEmpty()
    {
        Student_Folder__c studentFolder = StudentFolderTestData.Instance.DefaultStudentFolder;
        School_PFS_Assignment__c spfsa = SchoolPfsAssignmentTestData.Instance
            .forStudentFolderId(studentFolder.Id).DefaultSchoolPFSAssignment;
        
        PageReference fcwContainer = Page.GuidedFolderReview;
        fcwContainer.getParameters().put('schoolpfsassignmentid', spfsa.Id);
        Test.setCurrentPage(fcwContainer);
        
        Test.startTest();
            SchoolFamilyContributionWorksheetQuick controller = new SchoolFamilyContributionWorksheetQuick();
            spfsa = controller.getSchoolPFSAssignment();

            System.assertEquals(spfsa.Id, controller.currentSchoolPFSAssignmentId);
            System.assertEquals(studentFolder.Id, controller.studentFolder.Id);
        Test.stopTest();
    }

    @isTest
    private static void testPriorYearSchoolPFSAssignment_empty()
    {
        Student_Folder__c studentFolder = StudentFolderTestData.Instance
            .forAcademicYearPicklist(AcademicYearTestData.Instance.DefaultAcademicYear.Name).DefaultStudentFolder;
        School_PFS_Assignment__c spfsa = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(AcademicYearTestData.Instance.DefaultAcademicYear.Name)
            .forStudentFolderId(studentFolder.Id).DefaultSchoolPFSAssignment;
        PageReference fcwContainer = Page.GuidedFolderReview;
        fcwContainer.getParameters().put('schoolpfsassignmentid', spfsa.Id);
        Test.setCurrentPage(fcwContainer);

        Test.startTest();
            SchoolFamilyContributionWorksheetQuick controller = new SchoolFamilyContributionWorksheetQuick();
            controller.previousYearPFS = controller.getPreviousYearPFS();
            system.assertEquals(null, controller.previousYearPFS.pfs);
        Test.stopTest();
    }

    @isTest
    private static void testPriorYearSchoolPFSAssignment_NotEmpty()
    {
        Student_Folder__c studentFolder = StudentFolderTestData.Instance
            .forAcademicYearPicklist(AcademicYearTestData.Instance.DefaultAcademicYear.Name).DefaultStudentFolder;
        School_PFS_Assignment__c spfsa = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(AcademicYearTestData.Instance.DefaultAcademicYear.Name)
            .forStudentFolderId(studentFolder.Id).DefaultSchoolPFSAssignment;
        
        Academic_Year__c priorAcademicYear = AcademicYearTestData.Instance
            .asPreviousYear().insertAcademicYear();
        Student_Folder__c previousYearStudentFolder = StudentFolderTestData.Instance
            .forAcademicYearPicklist(priorAcademicYear.Name).insertStudentFolder();
        Pfs__c priorYearPfs = PfsTestData.Instance
            .forAcademicYearPicklist(priorAcademicYear.Name).insertPfs();
        Applicant__c priorYearApplicant = ApplicantTestData.Instance
            .forPfsId(priorYearPfs.Id).insertApplicant();
        School_PFS_Assignment__c priorYearSpfsa = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(priorYearApplicant.Id)
            .forStudentFolderId(previousYearStudentFolder.Id)
            .forAcademicYearPicklist(priorAcademicYear.Name).insertSchoolPfsAssignment();
        
        PageReference fcwContainer = Page.GuidedFolderReview;
        fcwContainer.getParameters().put('schoolpfsassignmentid', spfsa.Id);
        Test.setCurrentPage(fcwContainer);
        
        Test.startTest();
            SchoolFamilyContributionWorksheetQuick controller = new SchoolFamilyContributionWorksheetQuick();
            spfsa = controller.getSchoolPFSAssignment();
            controller.previousYearPFS = controller.getPreviousYearPFS();

            System.assertEquals(spfsa.Id, controller.currentSchoolPFSAssignmentId);
            System.assertEquals(priorYearSpfsa.Id, controller.previousYearPFS.pfs.Id);
        Test.stopTest();
    }

    @isTest
    private static void testParentOfferToPay()
    {
        School_PFS_Assignment__c spfsa = SchoolPfsAssignmentTestData.Instance.DefaultSchoolPFSAssignment;
        PageReference fcwContainer = Page.GuidedFolderReview;
        fcwContainer.getParameters().put('schoolpfsassignmentid', spfsa.Id);
        Test.setCurrentPage(fcwContainer);
        Decimal testValue = 100.50;
        SchoolFamilyContributionWorksheetQuick controller = new SchoolFamilyContributionWorksheetQuick();
        SchoolFamilyContributionWorksheetQuick.SchoolPfs schoolPfs = new SchoolFamilyContributionWorksheetQuick.SchoolPfs(spfsa);

        Test.startTest();
            schoolPfs.parentOfferToPay = testValue;

            System.assertEquals(testValue, schoolPfs.parentOfferToPay);
        Test.stopTest();
    }

    @isTest
    private static void testUpdateFinancialAidNotes()
    {
        Student_Folder__c studentFolder = StudentFolderTestData.Instance.DefaultStudentFolder;
        School_PFS_Assignment__c spfsa = SchoolPfsAssignmentTestData.Instance.DefaultSchoolPFSAssignment;

        PageReference fcwContainer = Page.GuidedFolderReview;
        fcwContainer.getParameters().put('schoolpfsassignmentid', spfsa.Id);
        Test.setCurrentPage(fcwContainer);
        SchoolFamilyContributionWorksheetQuick controller = new SchoolFamilyContributionWorksheetQuick();
        spfsa = controller.getSchoolPFSAssignment();
        String testTextNote = 'Test text for Tuition and Expense field';
        
        Test.startTest();
            controller.studentFolder.Tuition_and_Expense_Notes_Long__c = testTextNote;
            controller.updateFinancialAidNotes();

            studentFolder = StudentFolderSelector.newInstance().selectByIdWithCustomFieldList(
                new Set<Id>{studentFolder.Id}, new List<String>{'Tuition_and_Expense_Notes_Long__c'})[0];
            System.assertEquals(testTextNote, studentFolder.Tuition_and_Expense_Notes_Long__c);
        Test.stopTest();
    }

    private static void createTestData()
    {
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        academicYearIdTmp = academicYearId;

        Id previousAcademicYearId = GlobalVariables.getPreviousAcademicYear().id;

        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, true);
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, true);

        studentFolder11 = TestUtils.createStudentFolder('Student Folder 1.1', academicYearId, student1.Id, true);
        studentFolder12 = TestUtils.createStudentFolder('Student Folder 1.1x', previousacademicYearId, student1.Id, true);

        school1 = TestUtils.createAccount('Test School 1', RecordTypes.schoolAccountTypeId, 3, true);

        pfsA1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, true);
        pfsA2 = TestUtils.createPFS('PFS A', previousacademicYearId, parentA.Id, true);

        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA1.Id, true);
        Applicant__c applicant2A = TestUtils.createApplicant(student1.Id, pfsA2.Id, true);

        spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder11.Id, true);
        spfsa1.Salary_Wages_Parent_A__c = 10000;
        spfsa2 = TestUtils.createSchoolPFSAssignment(previousacademicYearId, applicant2A.Id, school1.Id, studentFolder12.Id, true);

        spfsa1.Discretionary_Income_Currency__c = 2;
        spfsa2.Orig_Discretionary_Income__c = 5;
        spfsa1.Effective_Income_Currency__c = 2;
        spfsa2.Orig_Effective_Income_Currency__c = 5;
        spfsa1.Est_Family_Contribution__c = 2;
        spfsa2.Orig_Est_Family_Contribution__c = 3;
        spfsa1.Income_Supplement__c = 5;
        spfsa2.Orig_Income_Supplement__c = 6;
        spfsa1.Total_Allowances__c = 4;
        spfsa2.Orig_Total_Allowances__c = 2;
        spfsa1.Total_Income_Currency__c = 6;
        spfsa2.Orig_Total_Income__c = 4;
        spfsa1.Total_Taxable_Income__c = 3;
        spfsa2.Orig_Total_Taxable_Income__c = 2;
        spfsa1.Total_Nontaxable_Income__c = 4;
        spfsa2.Orig_Total_Nontaxable_Income__c = 6;
        spfsa1.Unusual_Expense__c = 2;
        spfsa2.Orig_Unusual_Expense__c = 3;
        spfsa1.Total_Assets_Currency__c = 2;
        spfsa2.Orig_Total_Assets_Currency__c = 5;
        spfsa1.Total_Debts__c = 2;
        spfsa2.Orig_Total_Debts__c = 3;
        spfsa1.Net_Worth__c = 4;
        spfsa2.Orig_Net_Worth__c= 3;
        spfsa1.Adjusted_Effective_Income_Currency__c = 6;
        spfsa2.Orig_Adjusted_Effective_Income_Currency__c = 1;
        spfsa1.Revised_Adjusted_Effective_Income__c = 3;
        spfsa2.Orig_Revised_Adjusted_Effective_Income__c = 5;
        spfsa1.Est_Parental_Contribution_All_Students__c = 2;
        spfsa2.Orig_Est_Parental_Contribution_All__c = 5;
        spfsa1.Num_Children_in_Tuition_Schools__c = 2;
        spfsa2.Orig_Num_Children_in_Tuition_Schools__c = 3;
        spfsa1.Est_Parental_Contribution_per_Child__c = 3;
        spfsa2.Orig_Est_Parental_Contribution_per_Chil__c = 4;
        spfsa1.Student_Assets__c = 6;
        spfsa2.Orig_Student_Assets__c = 5 ;
        spfsa1.Student_Asset_Contribution__c = 6;
        spfsa2.Orig_Student_Asset_Contribution__c = 5;

        update spfsa2;
        update spfsa1;
    }
}