/**
 * @description This class is used to create Required Document records for unit tests.
 */
@isTest
public class RequiredDocumentTestData extends SObjectTestData {
    @testVisible private static final String DOCUMENT_TYPE = 'W2';
    @testVisible private static final String DOCUMENT_YEAR = '2015';

    /**
     * @description Get the default values for the Required_Document__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Required_Document__c.Academic_Year__c => AcademicYearTestData.Instance.DefaultAcademicYear.Id,
                Required_Document__c.School__c => AccountTestData.Instance.DefaultAccount.Id,
                Required_Document__c.Document_Type__c => DOCUMENT_TYPE,
                Required_Document__c.Document_Year__c => DOCUMENT_YEAR
        };
    }

    /**
     * @description Set the Academic_Year__c field on the current Required Document record.
     * @param academicYearId The Academic Year Id to set on the Required Document.
     * @return The current working instance of RequiredDocumentTestData.
     */
    public RequiredDocumentTestData forAcademicYearId(Id academicYearId) {
        return (RequiredDocumentTestData) with(Required_Document__c.Academic_Year__c, academicYearId);
    }

    /**
     * @description Set the School__c field on the current Required Document record.
     * @param accountId The School Id to set on the Required Document.
     * @return The current working instance of RequiredDocumentTestData.
     */
    public RequiredDocumentTestData forSchoolId(Id accountId) {
        return (RequiredDocumentTestData) with(Required_Document__c.School__c, accountId);
    }

    /**
     * @description Set the Document_Type__c field on the current Required Document record.
     * @param docType The document type to set on the Required Document.
     * @return The current working instance of RequiredDocumentTestData.
     */
    public RequiredDocumentTestData forDocType(String docType) {
        return (RequiredDocumentTestData) with(Required_Document__c.Document_Type__c, docType);
    }

    /**
     * @description Set the Document_Year__c field on the current Required Document record.
     * @param docYear The document year to set on the Required Document.
     * @return The current working instance of RequiredDocumentTestData.
     */
    public RequiredDocumentTestData forYear(String docYear) {
        return (RequiredDocumentTestData) with(Required_Document__c.Document_Year__c, docYear);
    }

    /**
     * @description Insert the current working Required_Document__c record.
     * @return The currently operated upon Required_Document__c record.
     */
    public Required_Document__c insertRequiredDocument() {
        return (Required_Document__c)insertRecord();
    }

    /**
     * @description Create the current working Required Document record without resetting
     *             the stored values in this instance of RequiredDocumentTestData.
     * @return A non-inserted Required_Document__c record using the currently stored field
     *             values.
     */
    public Required_Document__c createRequiredDocumentWithoutReset() {
        return (Required_Document__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Required_Document__c record.
     * @return The currently operated upon Required_Document__c record.
     */
    public Required_Document__c create() {
        return (Required_Document__c)super.buildWithReset();
    }

    /**
     * @description The default Required_Document__c record.
     */
    public Required_Document__c DefaultRequiredDocument {
        get {
            if (DefaultRequiredDocument == null) {
                DefaultRequiredDocument = createRequiredDocumentWithoutReset();
                insert DefaultRequiredDocument;
            }
            return DefaultRequiredDocument;
        }
        private set;
    }

    /**
     * @description Get the Required_Document__c SObjectType.
     * @return The Required_Document__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Required_Document__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static RequiredDocumentTestData Instance {
        get {
            if (Instance == null) {
                Instance = new RequiredDocumentTestData();
            }
            return Instance;
        }
        private set;
    }

    private RequiredDocumentTestData() { }
}