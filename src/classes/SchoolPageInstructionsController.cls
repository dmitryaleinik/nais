public without sharing class SchoolPageInstructionsController
{

    /*Initialization*/
    public SchoolPageInstructionsController(){}
    /*End Initialization*/
   
    /*Properties*/
    public String labelName {get; set;}
    public String fieldName {get; set;}
        
    public Boolean getRenderLabel()
    {
        Boolean b = false;
        try
        {
            User u = [
                SELECT Id, ContactId, Contact.Name, Contact.AccountId, Contact.Account.Name, ProfileId, Contact.Account.Portal_Version__c,
                    In_Focus_School__c, Persist_School_Focus__c,
                    SYSTEM_Hide_Instructions_Dashboard__c, SYSTEM_Hide_Instructions_Folder_Summary__c, SYSTEM_Hide_Instructions_PFS_Summary__c,
                    SYSTEM_Hide_Instructions_Budg_Management__c, SYSTEM_Hide_Instructions_School_Profile__c, SYSTEM_Hide_Instructions_Tuition_Fees__c,
                    SYSTEM_Hide_Instructions_Req_Documents__c, SYSTEM_Hide_Instructions_Annual_Settings__c, SYSTEM_Hide_Instructions_Pro_Judge__c,
                    SYSTEM_Terms_and_Conditions_Accepted__c, SYSTEM_Hide_Instructions_User_Admin__c, SYSTEM_Hide_Instructions_Folder_Lists__c,
                    SYSTEM_Hide_Instructions_Additional_Info__c, SYSTEM_Hide_Instructions_Assigned_Waiver__c, SYSTEM_Hide_Instructions_Documents__c,
                    SYSTEM_Hide_Instructions_EFC_Simulator__c, SYSTEM_Hide_Instructions_Fee_Waivers__c, SYSTEM_Hide_Instructions_Folder_Fin_Aid__c,
                    SYSTEM_Hide_Instructions_Household_Finan__c, SYSTEM_Hide_Instructions_Household_Infor__c, SYSTEM_Hide_Instructions_W2_Summary__c,
                    SYSTEM_Hide_Instructions_PFS_Assig_Tab__c, SYSTEM_Hide_Instructions_Advanced_Lists__c, SYSTEM_Hide_Instructions_Non_Need_Based__c,
                    SYSTEM_Hide_Instructions_School_Init__c
                FROM User
                WHERE Id = :UserInfo.getUserId()];
            b = !(Boolean)u.get(fieldName);
        }
        catch (Exception e)
        {
            System.debug('ERROR getting field ' + e);
        }
        
        return b;
    }
    /*End Properties*/
       
    /*Action Methods*/
    public void hideInstructionText()
    {
        try
        {
            User u = GlobalVariables.getCurrentUser();
            u.put(fieldName, true);
            update u;
        }
        catch (Exception e)
        {
            System.debug('ERROR updating User ' + e);
        }
    }   
    /*End Action Methods*/
}