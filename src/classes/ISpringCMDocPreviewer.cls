public interface ISpringCMDocPreviewer {
    
    PageReference getPDF();
    
    String getPublishUrl();
    
    String getErrorMessage();
}