/**
* Scheduler for the batch that every hour recalculates sharing for merged contacts.
*/
    global class RecalcSharMergedContactsBatchScheduler implements Schedulable
    {

        global void execute(SchedulableContext sc)
        {
            Database.executebatch(new RecalcSharingForMergedContactsBatch());
        }
    }