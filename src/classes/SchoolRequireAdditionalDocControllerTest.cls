@isTest
private class SchoolRequireAdditionalDocControllerTest {
    @isTest
    private static void saveNewDocument_multipleSelectedFolders_createNewSDAs() {
        
        DocumentRequirementServiceTest.Setup();
        
        Test.startTest();
            //1. Create a handler of the class that we are going to test.
            List<Student_Folder__c> folders = new List<Student_Folder__c>{DocumentRequirementServiceTest.studentFolder1};
            ApexPages.StandardSetController handler2 = new ApexPages.StandardSetController(folders);
            handler2.setSelected(folders);
            
            SchoolAdditionalDocsBulkController handler = new SchoolAdditionalDocsBulkController(handler2);
            handler.loadDocumentYears();
            
            //2. Create an instance of SchoolRequireAdditionalDocController.
            SchoolRequireAdditionalDocController  controller = new SchoolRequireAdditionalDocController();
            controller.handler = handler;
            handler.AdditionalSchoolDocAssign.Document_Type__c = 'Proof of College Enrollment';
            controller.saveNewDocument();
           
            //3. Verify that 2 of the 3 applicants has related SDAS.
            System.AssertEquals(1, (new List<School_Document_Assignment__c>([SELECT Id FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: DocumentRequirementServiceTest.spa1.Id])).size());
            System.AssertEquals(1, (new List<School_Document_Assignment__c>([SELECT Id FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: DocumentRequirementServiceTest.spa2.Id])).size());
            System.AssertEquals(0, (new List<School_Document_Assignment__c>([SELECT Id FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c =: DocumentRequirementServiceTest.spa3.Id])).size());
            
            //4. Verify that other methods are working.
            System.AssertNotEquals(null, controller.getAdditionalSchoolDocAssign());
            System.AssertNotEquals(null, controller.getNonSchoolSpecificDocumentTypes());
            System.AssertNotEquals(null, controller.getValidDocumentYears());
            System.AssertEquals(false, controller.getIsTaxDocument());
            System.AssertEquals(null, controller.saveAndUploadRequiredDocuments());
            System.AssertEquals(null, controller.cancelAdditionalDocument());
            System.AssertEquals(null, controller.saveAndNewAdditionalDocument());
            
        Test.stopTest();
            
    }
}