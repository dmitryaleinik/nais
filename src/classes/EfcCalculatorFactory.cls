/**
 * @description Build the Efc Calculator steps based on source.
 */
public class EfcCalculatorFactory {
    public static List<EfcCalculationSteps.Step> getEfcCalculatorSteps() {
        List<EfcCalculationSteps.Step> steps = new List<EfcCalculationSteps.Step>();

        steps.add(new EfcCalculationSteps.TaxableIncome());
        steps.add(new EfcCalculationSteps.NonTaxableIncome());
        steps.add(new EfcCalculationSteps.TotalIncome());
        steps.add(new EfcCalculationSteps.TotalAllowances());
        steps.add(new EfcCalculationSteps.EffectiveIncome());
        steps.add(new EfcCalculationSteps.HomeEquity());
        steps.add(new EfcCalculationSteps.NetWorth());
        steps.add(new EfcCalculationSteps.DiscretionaryNetWorth());
        steps.add(new EfcCalculationSteps.IncomeSupplement());
        steps.add(new EfcCalculationSteps.AdjustedEffectiveIncome());
        steps.add(new EfcCalculationSteps.RevisedAdjustedEffectiveIncome());
        steps.add(new EfcCalculationSteps.IncomeProtectionAllowance());
        steps.add(new EfcCalculationSteps.DiscretionaryIncome());
        steps.add(new EfcCalculationSteps.EstimatedParentalContribution());
        steps.add(new EfcCalculationSteps.EstimatedParentalContributionPerChild());
        steps.add(new EfcCalculationSteps.StudentAssetContribution());
        steps.add(new EfcCalculationSteps.EstimatedFamilyContribution());

        return steps;
    }
}