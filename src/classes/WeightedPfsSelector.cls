/**
 * @description Query selector for Weighted PFS records.
 */
public class WeightedPfsSelector extends fflib_SObjectSelector {

    @testVisible private static final String PFS_ID_SET_PARAM = 'pfsIds';
    @testVisible private static final String ANNUAL_SETTING_ID_SET_PARAM = 'annualSettingIdSet';
    @testVisible private static final String FIELDS_TO_SELECT_PARAM = 'fieldsToSelect';

    /**
     * @description Select Weighted PFS records by their PFS Ids with custom field list.
     * @param pfsIds The set of Pfs Ids to select Weighted Pfs records by.
     * @param fieldsToSelect The list of fields to select from Weighted Pfs.
     * @returns A list of Weighted Pfs records.
     * @throws An ArgumentNullException if pfsIds is null.
     * @throws An ArgumentNullException if fieldsToSelect is null.
     */
    public List<Weighted_PFS__c> selectWithCustomFieldListByPfsId(Set<Id> pfsIds, List<String> fieldsToSelect) {
        ArgumentNullException.throwIfNull(pfsIds, PFS_ID_SET_PARAM);
        ArgumentNullException.throwIfNull(fieldsToSelect, FIELDS_TO_SELECT_PARAM);

        assertIsAccessible();

        String weightedPfsQuery = newQueryFactory(false)
            .selectFields(fieldsToSelect)
            .setCondition('PFS__c IN :pfsIds')
            .toSOQL();

        return Database.query(weightedPfsQuery);
    }

    /**
     * @description Select Weighted PFS records with custom field list by PFS Id and Annual Setting Id.
     * @param pfsIds The set of Pfs Ids to select Weighted Pfs records by.
     * @param annualSettingIds The set of Annual Setting Ids to select Weighted Pfs records by.
     * @param fieldsToSelect The list of fields to select from Weighted Pfs.
     * @returns A list of Weighted Pfs records.
     * @throws An ArgumentNullException if pfsIds is null.
     * @throws An ArgumentNullException if annualSettingIds is null.
     * @throws An ArgumentNullException if fieldsToSelect is null.
     */
    public List<Weighted_PFS__c> selectWithCustomFieldListByPfsAndAnnSetIds(Set<Id> pfsIds, Set<Id> annualSettingIds, List<String> fieldsToSelect) {
        ArgumentNullException.throwIfNull(pfsIds, PFS_ID_SET_PARAM);
        ArgumentNullException.throwIfNull(annualSettingIds, ANNUAL_SETTING_ID_SET_PARAM);
        ArgumentNullException.throwIfNull(fieldsToSelect, FIELDS_TO_SELECT_PARAM);

        assertIsAccessible();

        String weightedPfsQuery = newQueryFactory(false)
            .selectFields(fieldsToSelect)
            .setCondition('PFS__c IN :pfsIds')
            .setCondition('Annual_Setting__c IN :annualSettingIds')
            .toSOQL();

        return Database.query(weightedPfsQuery);
    }

    /**
     * @description Select All Weighted PFS records.
     * @returns A list of Weighted Pfs records.
     */
    public List<Weighted_PFS__c> selectAllWeightedPfs() {
        assertIsAccessible();

        String weightedPfsQuery = newQueryFactory(false)
            .toSOQL();

        return Database.query(weightedPfsQuery);
    }

    private Schema.SObjectType getSObjectType() {
        return Weighted_PFS__c.getSObjectType();
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            Weighted_PFS__c.Name,
            Weighted_PFS__c.Id,
            Weighted_PFS__c.Academic_Year__c,
            Weighted_PFS__c.Annual_Setting__c,
            Weighted_PFS__c.PFS__c,
            Weighted_PFS__c.PFS_Original_Submission_Week__c,
            Weighted_PFS__c.PFS_Payment_Month__c,
            Weighted_PFS__c.PFS_Payment_Week__c,
            Weighted_PFS__c.PFS_Year__c,
            Weighted_PFS__c.School__c,
            Weighted_PFS__c.School_Count__c,
            Weighted_PFS__c.Weighted_School_Count__c
        };
    }

    /**
     * @description Singleton property ensuring external sources do not
     *              instantiate this directly.
     */
    public static WeightedPfsSelector Instance {
        get {
            if (Instance == null) {
                Instance = new WeightedPfsSelector();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to enforce singleton access
     *              via the Instance property.
     */
    private WeightedPfsSelector() {}
}