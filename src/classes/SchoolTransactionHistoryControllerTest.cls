/*
 * NAIS-2376 Transaction History Page in School Portal
 *
 * WH, Exponent Partners, 2015
 */
@isTest
private class SchoolTransactionHistoryControllerTest
{

    @isTest
    private static void testPageLoad() {
        Academic_Year__c newAcademicYear = new Academic_Year__c();
        newAcademicYear.Name = '2015-2016';
        insert newAcademicYear;
        
        Subscription_Settings__c sSettings = new Subscription_Settings__c();
        sSettings.Name = 'Subscription Settings';
        sSettings.Start_Month__c = 10;
        sSettings.Start_Day__c = 1;
        sSettings.End_Month__c = 9;
        sSettings.End_Day__c = 30;
        sSettings.Number_of_Months__c = 12;
        sSettings.Number_of_Months_Extended__c = 36;
        insert sSettings;
        
        Account testAccount = TestUtils.createAccount('Test Account', RecordTypes.schoolAccountTypeId, 5, true);
                
        Opportunity subscriptionOpp = new Opportunity();
        subscriptionOpp.Name = 'test subscription fee opp';
        subscriptionOpp.AccountId = testAccount.Id;
        subscriptionOpp.RecordTypeId = RecordTypes.opportunitySubscriptionFeeTypeId;
        subscriptionOpp.Academic_Year_Picklist__c = newAcademicYear.Name;
        subscriptionOpp.New_Renewal__c = 'Renewal';
        subscriptionOpp.StageName = 'Prospecting';
        subscriptionOpp.CloseDate = Date.today();
        subscriptionOpp.Subscription_Type__c = 'Sub Type';
        subscriptionOpp.SYSTEM_Suppress_Auto_Sale_TLI_Insert__c = true;
        Opportunity waiverOpp = new Opportunity();
        waiverOpp.Name = 'test waiver fee purchase opp';
        waiverOpp.AccountId = testAccount.Id;
        waiverOpp.RecordTypeId = RecordTypes.opportunityFeeWaiverPurchaseTypeId;
        waiverOpp.Academic_Year_Picklist__c = newAcademicYear.Name;
        waiverOpp.StageName = 'Paid in Full';
        waiverOpp.CloseDate = Date.today().addDays(-1);
        waiverOpp.Product_Quantity__c = 2;
        waiverOpp.SYSTEM_Suppress_Auto_Sale_TLI_Insert__c = true;
        Opportunity openOpp = new Opportunity();
        openOpp.Name = 'test open opp';
        openOpp.AccountId = testAccount.Id;
        openOpp.RecordTypeId = RecordTypes.opportunitySubscriptionFeeTypeId;
        openOpp.Academic_Year_Picklist__c = newAcademicYear.Name;
        openOpp.New_Renewal__c = 'Renewal';
        openOpp.StageName = 'Prospecting';
        openOpp.CloseDate = Date.today().addDays(100);
        openOpp.Subscription_Type__c = 'Sub Type';
        openOpp.SYSTEM_Suppress_Auto_Sale_TLI_Insert__c = true;
        Opportunity otherOpp = new Opportunity();
        otherOpp.Name = 'test open opp';
        otherOpp.AccountId = testAccount.Id;
        otherOpp.RecordTypeId = RecordTypes.opportunityUnmatchedCheckTypeId;
        otherOpp.Academic_Year_Picklist__c = newAcademicYear.Name;
        otherOpp.StageName = 'Closed';
        otherOpp.CloseDate = Date.today().addDays(-10);
        otherOpp.SYSTEM_Suppress_Auto_Sale_TLI_Insert__c = true;
        insert new List<Opportunity> { subscriptionOpp, waiverOpp, openOpp, otherOpp };
        
        Contact testContact = TestUtils.createContact('Test Staff', testAccount.Id, RecordTypes.schoolStaffContactTypeId, true);
        
        User testUser = TestUtils.createPortalUser('Test User', 'suser@test.org.test', 'sutot', testContact.Id, GlobalVariables.schoolPortalUserProfileId, true, true);
        
        Transaction_Line_Item__c tliRecord = new Transaction_Line_Item__c(RecordTypeId=RecordTypes.saleTransactionTypeId,
                                            Transaction_Status__c='Posted',
                                            Amount__c=0,
                                            Transaction_Type__c='School Subscription',
                                            Opportunity__c=subscriptionOpp.Id,
                                            Product_Code__c='121-100 School Sub - Full - 2016-17',
                                            Product_Amount__c='400.00');
        insert tliRecord;
        
        subscriptionOpp.StageName = 'Closed Won';
        upsert subscriptionOpp;
        
        Test.startTest();
        
        System.runAs(testUser) {
            PageReference pr = Page.SchoolTransactionHistory;
            Test.setCurrentPage(pr);
            SchoolTransactionHistoryController controller = new SchoolTransactionHistoryController();
            
            // for coverage of academic year selector interface methods
            System.assertNotEquals(null,            controller.Me);
            System.assertEquals(null,                controller.getAcademicYearId());
            controller.setAcademicYearId(newAcademicYear.Id);
            System.assertEquals(newAcademicYear.Id,    controller.getAcademicYearId());
            System.assertEquals(null,                controller.SchoolAcademicYearSelector_OnChange(true));
            
            // closed subscription and fee waiver transactions sorted by CloseDate desc
            System.assertEquals(2, controller.transactions.size());
            System.assertEquals(subscriptionOpp.Id,        controller.transactions[0].oppId);
            System.assert(controller.transactions[0].name.contains('Subscription Fee - Sub Type'));
            System.assertEquals(newAcademicYear.Name,    controller.transactions[0].academicYear);
            System.assertEquals(waiverOpp.Id,        controller.transactions[1].oppId);
            System.assert(controller.transactions[1].name.contains('Fee Waiver Purchase'));
            System.assertEquals(newAcademicYear.Name,    controller.transactions[1].academicYear);
        }
        
        Test.stopTest();
    }
}