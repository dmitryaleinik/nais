/**
 * @description This class is used to update SDA records by linking family document records.
 */
public class DocumentService {

    @testVisible private static final String PFS_RECORD_ID_PARAM = 'pfsRecordId';
    @testVisible private static final String FAMILY_DOCUMENT_ID_PARAM = 'familyDocumentId';
    @testVisible private static final String NEW_DOCUMENT_TYPE_PARAM = 'newDocumentType';
    @testVisible private static final String NEW_DOCUMENT_YEAR_PARAM = 'newDocumentYear';
    @testVisible private static final String REQUEST_PARAM = 'request';

    private DocumentService() { }

    /**
     * @description Determines which School Document Assignment records should be linked to a newly inserted family
     *              document record. The SDAs are queried and the ones that are relevant to the new family document are
     *              linked to that family document record.
     * @param request The class containing details related to the new family document and the PFS that it was uploaded for.
     * @throws ArgumentNullException if the request is null.
     */
    public void updateDocumentAssignments(SDAUpdateRequest request) {
        ArgumentNullException.throwIfNull(request, REQUEST_PARAM);

        List<School_Document_Assignment__c> documentAssignments = getSchoolDocumentAssignments(request.PfsId, request.DocumentType, request.DocumentYear, request.SchoolCode, request.SchoolSpecificDocumentName);

        if (is1040DocType(request.DocumentType)) {
            List<School_Document_Assignment__c> sdasToUpdatePerSchool = getSDAsToUpdateForEachApplicant(documentAssignments);
            updateSDARecords(sdasToUpdatePerSchool, request.DocumentId, request.DocumentName);
        } else {
            updateSDARecords(documentAssignments, request.DocumentId, request.DocumentName);
        }
    }

    private List<School_Document_Assignment__c> getSchoolDocumentAssignments(Id pfsId, String docType, String docYear, String schoolCode, String schoolSpecificLabel) {
        List<School_Document_Assignment__c> sdaRecords = new List<School_Document_Assignment__c>();

        // if not a school specific document, then get all SDA records for the doc type/year
        if (docType != ApplicationUtils.schoolSpecificDocType()) {
            sdaRecords = [SELECT Id, Name, School_Id__c, School_PFS_Assignment__r.Applicant__c, School_PFS_Assignment__r.Applicant__r.PFS__r.Filing_Status__c, Document_Type__c,
                    Document__r.Document_Status__c, Document__r.Deleted__c
            FROM School_Document_Assignment__c
            WHERE Document_Type__c = :docType
            AND Document_Year__c = :docYear
            AND School_PFS_Assignment__r.Applicant__r.PFS__c = :pfsId
            AND Deleted__c != true];
        }

        // for school-specific docs, only get SDA records that match the school code and school specific document label
        else {
            if (schoolCode != null) {
                sdaRecords = [SELECT Id, Name, School_Id__c, School_PFS_Assignment__r.Applicant__c, School_PFS_Assignment__r.Applicant__r.PFS__r.Filing_Status__c, Document_Type__c,
                        Document__r.Document_Status__c, Document__r.Deleted__c
                FROM School_Document_Assignment__c
                WHERE School_PFS_Assignment__r.School__r.SSS_School_Code__c = :schoolCode
                AND Document_Type__c = :docType
                AND Document_Year__c = :docYear
                AND Required_Document__r.Label_for_School_Specific_Document__c = :schoolSpecificLabel
                AND School_PFS_Assignment__r.Applicant__r.PFS__c = :pfsId
                AND Deleted__c != true];
            }
        }

        return sdaRecords;
    }

    private void updateSDARecords(List<School_Document_Assignment__c> sdaList, Id familyDocumentId, String docName) {
        List<School_Document_Assignment__c> sdaRecordsToUpdate = new List<School_Document_Assignment__c>();
        for (School_Document_Assignment__c sda : sdaList) {
            // make sure there's not already a document attached to the SDA
            if (!hasAttachedDocument(sda)) {
                sda.Document__c = familyDocumentId;
                if (String.isNotBlank(docName)) {
                    sda.Document_Name__c = docName;
                }
                sdaRecordsToUpdate.add(sda);
            }
        }

        // save the updated SDA records
        if (!sdaRecordsToUpdate.isEmpty()) {
            update sdaRecordsToUpdate;
        }
    }

    private Boolean hasAttachedDocument(School_Document_Assignment__c sda) {
        boolean hasDoc = false;

        if ((sda.Document__c != null) && (sda.Document__r.Document_Status__c != 'Not Received') && (sda.Document__r.Deleted__c != true)) {
            hasDoc = true;
        }

        return hasDoc;
    }

    private Map<String, List<School_Document_Assignment__c>> getSDAsWithoutDocsBySchoolId(List<School_Document_Assignment__c> sdasToGroup) {
        Map<String, List<School_Document_Assignment__c>> sdasBySchoolId = new Map<String, List<School_Document_Assignment__c>>();

        for (School_Document_Assignment__c sda : sdasToGroup) {
            List<School_Document_Assignment__c> sdaGroup = sdasBySchoolId.get(sda.School_Id__c);

            if (sdaGroup == null) {
                sdaGroup = new List<School_Document_Assignment__c>();
            }

            if (!hasAttachedDocument(sda)) {
                sdaGroup.add(sda);
            }

            sdasBySchoolId.put(sda.School_Id__c, sdaGroup);
        }

        return sdasBySchoolId;
    }

    private List<School_Document_Assignment__c> getSDAsToUpdateForEachApplicant(List<School_Document_Assignment__c> sdasForAllSchools) {
        List<School_Document_Assignment__c> sdasToUpdate = new List<School_Document_Assignment__c>();

        // Get SDAs without family documents by school Id. The provided SDAs are queried by doc type and doc year.
        Map<String, List<School_Document_Assignment__c>> sdasBySchoolId = getSDAsWithoutDocsBySchoolId(sdasForAllSchools);

        // After grouping empty SDAs by school, grab the one SDA for each unique applicant from each group.
        // This prevents us from updating all SDAs of a single doc type/year with one fam doc in situations where we
        // have multiple SDAs for the same doc type/year that should be populated with unique fam doc records. This
        // situation is most relevant to 1040s where parents are married and filing separately.
        for (String schoolId : sdasBySchoolId.keySet()) {
            List<School_Document_Assignment__c> sdasForSchool = sdasBySchoolId.get(schoolId);

            sdasToUpdate.addAll(getOneSDAForEachApplicant(sdasForSchool));
        }

        return sdasToUpdate;
    }

    private List<School_Document_Assignment__c> getOneSDAForEachApplicant(List<School_Document_Assignment__c> sdasForOneSchool) {
        List<School_Document_Assignment__c> sdasForUniqueApplicants = new List<School_Document_Assignment__c>();

        if (sdasForOneSchool == null || sdasForOneSchool.isEmpty()) {
            return sdasForUniqueApplicants;
        }

        Set<Id> applicantIds = new Set<Id>();

        for (School_Document_Assignment__c docAssignment : sdasForOneSchool) {
            // Get the applicant Id from the current SDA and check if we already have an SDA for that applicant.
            Id applicantId = getApplicantId(docAssignment);

            // If we already have an SDA for this applicant, continue on to the next SDA.
            if (applicantIds.contains(applicantId)) {
                continue;
            }

            sdasForUniqueApplicants.add(docAssignment);
            applicantIds.add(applicantId);
        }

        return sdasForUniqueApplicants;
    }

    private Id getApplicantId(School_Document_Assignment__c documentAssignment) {
        if (documentAssignment == null) {
            return null;
        }

        return documentAssignment.School_PFS_Assignment__c == null ? null : documentAssignment.School_PFS_Assignment__r.Applicant__c;
    }

    private Boolean is1040DocType(String docType) {
        return String.isNotBlank(docType) && docType.contains('1040');
    }

    /**
     * @description Request class used to specify various details necessary for updating related SDAs after saving a new family document.
     */
    public class SDAUpdateRequest {

        /**
         * @description Constructor for a request which specifies the information used to create a new family document.
         * @param pfsRecordId The Id of the PFS record that a family document was uploaded for.
         * @param familyDocumentId The Id of the new family document record.
         * @param newDocumentType The type of document of the newly inserted family document record.
         * @param newDocumentYear The document year for the newly inserted family document record.
         * @param schoolCodeForDoc The school code used if the document type is unique to a specific school.
         *                         This is an optional parameter.
         * @param otherDocTypeName The name of document type that is unique to a specific school.
         *                         This is an optional parameter.
         * @param newDocumentName The name of the document that should be listed on the SDA records.
         *                        This parameter is optional.
         * @throws ArgumentNullException if pfsRecordId, familyDocumentId, newDocumentType, or newDocumentYear are null.
         */
        public SDAUpdateRequest(Id pfsRecordId, Id familyDocumentId, String newDocumentType, String newDocumentYear,
                    String schoolCodeForDoc, String otherDocTypeName, String newDocumentName) {
            ArgumentNullException.throwIfNull(pfsRecordId, PFS_RECORD_ID_PARAM);
            ArgumentNullException.throwIfNull(familyDocumentId, FAMILY_DOCUMENT_ID_PARAM);
            ArgumentNullException.throwIfNull(newDocumentType, NEW_DOCUMENT_TYPE_PARAM);
            ArgumentNullException.throwIfNull(newDocumentYear, NEW_DOCUMENT_YEAR_PARAM);

            this.PfsId = pfsRecordId;
            this.DocumentId = familyDocumentId;
            this.DocumentType = newDocumentType;
            this.DocumentYear = newDocumentYear;
            this.SchoolCode = schoolCodeForDoc;
            this.SchoolSpecificDocumentName = otherDocTypeName;
            this.DocumentName = newDocumentName;
        }

        /**
         * @description The Id of the PFS which documents were uploaded for.
         */
        public Id PfsId { get; private set; }

        /**
         * @description The Ids of the documents that have been uploaded and need to be related to SDAs.
         */
        public Id DocumentId { get; private set; }

        public String DocumentName { get; private set; }

        public String DocumentType { get; private set; }

        public String DocumentYear { get; private set; }

        public String SchoolCode { get; private set; }

        public String SchoolSpecificDocumentName { get; private set; }
    }

    /**
     * @description Singleton instance of the document service.
     */
    public static DocumentService Instance {
        get {
            if (Instance == null) {
                Instance = new DocumentService();
            }
            return Instance;
        }
        private set;
    }
}