/**
 * @description: Test class for FamilyHelpController.cls using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 * @author: Mike Havrila @ Presence PG
 */
@isTest
private class FamilyHelpControllerTest {

    private static User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    private static User schoolPortalUser;
    private static Academic_Year__c currentAcademicYear;
    private static PFS__c pfs1;

    private static void createTestData() {
        Account family = AccountTestData.Instance.asFamily().create();
        Account school = AccountTestData.Instance.asSchool().create();
        insert new List<Account>{family, school};

        currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;

        Contact parentA = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(school.Id).create();
        Contact student1 = ContactTestData.Instance
            .asStudent()
            .forAccount(family.Id).create();
        insert new List<Contact> {schoolStaff, parentA, student1};

        System.runAs(currentUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);
        String pfsStatusUnpaid = 'Unpaid';
        String folderSource = 'PFS';

        System.runAs(schoolPortalUser){
            pfs1 = PfsTestData.Instance
                .asSubmitted()
                .forPaymentStatus(pfsStatusUnpaid)
                .forParentA(parentA.Id)
                .forOriginalSubmissionDate(System.today().addDays(-3))
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<PFS__c> {pfs1});

            Applicant__c applicant1A = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student1.Id).create();
            Dml.WithoutSharing.insertObjects(new List<Applicant__c> {applicant1A});

            Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
                .forFolderSource(folderSource)
                .forStudentId(student1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<Student_Folder__c> {studentFolder1});

            String fifthGrade = '5';
            School_PFS_Assignment__c spfsa1 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1A.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> {spfsa1});
        }

        // Share records to school staff
        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);
    }
    
    // test page action method w/ valid user, valid redirect
    @isTest 
    private static void initModelAndView_redirectToCategoryPage_successRedirect() {
        createTestData();

        System.runAs(currentUser) {
            Test.startTest();
                PageReference initPage = Page.FamilyHelp;
                initPage.getParameters().put('id', pfs1.Id);
                initPage.getParameters().put('academicYearId', currentAcademicYear.Id);

                Test.setCurrentPage( initPage);

                FamilyTemplateController templateContoller = new FamilyTemplateController();
                FamilyHelpController controller = new FamilyHelpController( templateContoller);
                controller.category = 'Documents__c';
                PageReference resultsPage = controller.goToCategory();
            Test.stopTest();

            System.assertEquals( 'Documents__c', resultsPage.getParameters().get('category'), 'Categories do not match what was entered');
        }
    }
}