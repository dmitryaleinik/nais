/*
* SPEC-099
* NAIS-282 PFS Doc Statuses.xlsx
*
* Once all of the required documents have been provided and verified, the (document) stage of the folder should change
*
* Nathan, Exponent Partners, 2013
*/
@isTest
private class SchoolSetSPFSDocumentStatusTest
{

    private static Academic_Year__c academicYear;
    private static Account school;
    private static Contact parent;
    private static Contact student;
    private static PFS__c pfs;
    private static Applicant__c applicant;
    private static Student_Folder__c studentFolder;
    private static School_PFS_Assignment__c schoolPFSAssignment;
    private static Family_Document__c docNotReceived;
    private static Family_Document__c docReceivedVerification;
    private static Family_Document__c docReceivedNonVerification;
    private static Family_Document__c docProcessed;
    private static Family_Document__c cyDocReceivedVerification, cyDocReceivedNonVerification, cyDocProcessed, pyDocNotReceived, pyDocReceivedVerification, pyDocReceivedNonVerification, pyDocProcessed;
    private static Family_Document__c naRequested1, naRequested2, naRequested3;
    private static Household__c testHousehold;
    private static School_Document_Assignment__c sdaA, sdaB, sdaC;
        
    private static void setupTestData(){
        // create a custom setting for a verfiable document type
        insert new Document_Custom_Settings__c(
                Name='Verifiable_Document_Type_01',
                Active__c = true,
                Value__c = 'W2'
        );

        // create test academic year
        academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        
        // create a school
        school = TestUtils.createAccount('testSchool', RecordTypes.schoolAccountTypeId, null, true); 
        
        // create a parent
        parent = TestUtils.createContact('testParent', null, RecordTypes.parentContactTypeId, true);
        
        // create a student
        student = TestUtils.createContact('testStudent', null, RecordTypes.studentContactTypeId, true);
        
        // [11.12.16] SFP-640 need household__c associated to parents and family_document__c [jB] 
        testHousehold = TestUtils.createHousehold('hh1', true);
        parent.Household__c = testHousehold.Id;
        update parent;

        // create a pfs
        pfs = TestUtils.createPFS('testPFS', academicYear.Id, parent.Id, true);
        
        // SFP-640 - create test verification required for document processing
        Verification__c testVerification = new Verification__c();
        insert testVerification;

        pfs.Verification__c = testVerification.id;
        update pfs;

        // create an applicant
        applicant = TestUtils.createApplicant(student.Id, pfs.Id, true);
        
        // create a student folder
        studentFolder = TestUtils.createStudentFolder('testStudentFolder', academicYear.Id, student.Id, true);
        
        // create a school PFS assignment
        schoolPFSAssignment = TestUtils.createSchoolPFSAssignment(academicYear.Id, applicant.Id, school.Id, studentFolder.Id, true);

    }
    // [SL] NAIS-282 reworking test class
    @isTest
    private static void testCalculateDocStatuses() {
        setupTestData();

        // create test family docs
        docNotReceived = TestUtils.createFamilyDocument('docNotReceived', academicYear.Id, false);
        // SFP-640
        docNotReceived.Household__c = testHousehold.id;
        docNotReceived.Document_Status__c = 'Not Received';
        
        docReceivedVerification = TestUtils.createFamilyDocument('docReceivedVerification', academicYear.Id, false);
        // SFP-640
        docReceivedVerification.Household__c = testHousehold.id;
        docReceivedVerification.Document_Status__c = 'Received/In Progress';
        docReceivedVerification.Document_Type__c = 'W2';
        
        docReceivedNonVerification = TestUtils.createFamilyDocument('docReceivedNonVerification', academicYear.Id, false);
        // SFP-640
        docReceivedNonVerification.Household__c = testHousehold.id;
        docReceivedNonVerification.Document_Status__c = 'Received/In Progress';
        
        docProcessed = TestUtils.createFamilyDocument('docProcessed', academicYear.Id, false);
        // SFP-640
        docProcessed.Household__c = testHousehold.id;       
        docProcessed.Document_Status__c = 'Processed';
        
        insert new List<Family_Document__c> {docNotReceived, docReceivedVerification, docReceivedNonVerification, docProcessed};

        Test.startTest();

        // Verify scenarios with 1 status
        School_Document_Assignment__c sda1 = TestUtils.createSchoolDocumentAssignment('testSDA1', docNotReceived.Id, null, schoolPFSAssignment.Id, true);
        System.assertEquals('No Documents Received', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);
        
        sda1.Document__c = docReceivedVerification.Id;
        update sda1;
        System.assertEquals('Required Documents Received/In Progress', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);
    
        sda1.Document__c = docReceivedNonVerification.Id;
        update sda1;
        System.assertEquals('Required Documents Complete', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);
    
        sda1.Document__c = docProcessed.Id;
        update sda1;
        System.assertEquals('Required Documents Complete', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);

        // Verify scenarios with 2 statuses
        School_Document_Assignment__c sda2 = TestUtils.createSchoolDocumentAssignment('testSDA2', null, null, schoolPFSAssignment.Id, true);
        sda1.Document__c = docNotReceived.Id;
        sda2.Document__c = docReceivedVerification.Id;
        update new List<School_Document_Assignment__c> {sda1, sda2};
        System.assertEquals('Required Documents Outstanding', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);
        
        sda1.Document__c = docReceivedVerification.Id;
        sda2.Document__c = docReceivedNonVerification.Id;
        update new List<School_Document_Assignment__c> {sda1, sda2};
        System.assertEquals('Required Documents Received/In Progress', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);
        
        sda1.Document__c = docReceivedVerification.Id;
        sda2.Document__c = docProcessed.Id;
        update new List<School_Document_Assignment__c> {sda1, sda2};
        System.assertEquals('Required Documents Received/In Progress', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);
        
        sda1.Document__c = docReceivedNonVerification.Id;
        sda2.Document__c = docProcessed.Id;
        update new List<School_Document_Assignment__c> {sda1, sda2};
        System.assertEquals('Required Documents Complete', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);
        
        // [CH] NAIS-2125 Test inserting Misc/Other SDA record
        School_Document_Assignment__c sdaOther = TestUtils.createSchoolDocumentAssignment('testSDAOther', null, null, schoolPFSAssignment.Id, false);
        sdaOther.Document_Type__c = 'Other/Misc Tax Form';
        insert sdaOther;
        System.assertEquals('Required Documents Complete', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);
    
        // Verify scenarios with 3 statuses
        School_Document_Assignment__c sda3 = TestUtils.createSchoolDocumentAssignment('testSDA3', null, null, schoolPFSAssignment.Id, true);
        sda1.Document__c = docNotReceived.Id;
        sda2.Document__c = docReceivedVerification.Id;
        sda3.Document__c = docReceivedNonVerification.Id;
        update new List<School_Document_Assignment__c> {sda1, sda2, sda3};
        System.assertEquals('Required Documents Outstanding', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);

        sda1.Document__c = docReceivedVerification.Id;
        sda2.Document__c = docReceivedNonVerification.Id;
        sda3.Document__c = docProcessed.Id;
        update new List<School_Document_Assignment__c> {sda1, sda2, sda3};
        System.assertEquals('Required Documents Received/In Progress', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);
    
        // Verify scenarios with 4 statuses
        School_Document_Assignment__c sda4 = TestUtils.createSchoolDocumentAssignment('testSDA4', null, null, schoolPFSAssignment.Id, true);
        sda1.Document__c = docNotReceived.Id;
        sda2.Document__c = docReceivedVerification.Id;
        sda3.Document__c = docReceivedNonVerification.Id;
        sda4.Document__c = docProcessed.Id;
        update new List<School_Document_Assignment__c> {sda1, sda2, sda3, sda4};
        System.assertEquals('Required Documents Outstanding', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);
    
        // test delete SDA
        delete sda1;
        System.assertEquals('Required Documents Received/In Progress', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);
        
        // test delete a family doc
        delete docReceivedVerification;
        System.assertEquals('Required Documents Outstanding', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);
    
        Test.stopTest();
    }

    @isTest
    private static void testCalculateDocStatusesWithNARequest() {
        setupTestData();

        docReceivedVerification = TestUtils.createFamilyDocument('docReceivedVerification', academicYear.Id, false);
        docReceivedVerification.Document_Status__c = 'Received/In Progress';
        docReceivedVerification.Document_Type__c = 'W2';
        
        naRequested1 = TestUtils.createFamilyDocument('naRequested1', academicYear.Id, false);
        naRequested1.Document_Status__c = 'Not Applicable/Waive Requested';
        naRequested1.Document_Type__c = 'W2';

        naRequested2 = TestUtils.createFamilyDocument('naRequested2', academicYear.Id, false);
        naRequested2.Document_Status__c = 'Not Applicable/Waive Requested';
        naRequested2.Document_Type__c = 'W2';

        naRequested3 = TestUtils.createFamilyDocument('naRequested3', academicYear.Id, false);
        naRequested3.Document_Status__c = 'Not Applicable/Waive Requested';
        naRequested3.Document_Type__c = 'W2';
        
        insert new List<Family_Document__c> {docReceivedVerification, naRequested1, naRequested2, naRequested3};

        Test.startTest();

        // Verify scenarios with 1 status
        School_Document_Assignment__c sda1 = TestUtils.createSchoolDocumentAssignment('testSDA1', docReceivedVerification.Id, null, schoolPFSAssignment.Id, true);
        System.assertEquals('Required Documents Received/In Progress', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);
    
        sda1.Document__c = naRequested1.Id;
        update sda1;
        System.assertEquals('No Documents Received', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);

        sda1.Requirement_Waiver_Status__c = 'Approved';
        update sda1;
        System.assertEquals('Required Documents Complete', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);

        sda1.Requirement_Waiver_Status__c = 'Not Approved';
        update sda1;
        System.assertEquals('No Documents Received', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);

        // Verify scenarios with 2 statuses
        School_Document_Assignment__c sda2 = TestUtils.createSchoolDocumentAssignment('testSDA2', null, null, schoolPFSAssignment.Id, true);
        sda2.Document__c = naRequested2.Id;
        sda1.Requirement_Waiver_Status__c = 'Approved';
        update new List<School_Document_Assignment__c> {sda1, sda2};
        System.assertEquals('Required Documents Outstanding', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);
        
        sda1.Requirement_Waiver_Status__c = 'Approved';
        sda2.Requirement_Waiver_Status__c = 'Denied';
        update new List<School_Document_Assignment__c> {sda1, sda2};
        System.assertEquals('Required Documents Outstanding', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);
        
        sda1.Requirement_Waiver_Status__c = 'Approved';
        sda2.Requirement_Waiver_Status__c = 'Approved';
        update new List<School_Document_Assignment__c> {sda1, sda2};
        System.assertEquals('Required Documents Complete', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);
        
    
        // Verify scenarios with 3 statuses
        School_Document_Assignment__c sda3 = TestUtils.createSchoolDocumentAssignment('testSDA3', null, null, schoolPFSAssignment.Id, true);
        sda3.Document__c = naRequested3.Id;
        sda1.Requirement_Waiver_Status__c = 'Approved';
        sda2.Requirement_Waiver_Status__c = 'Approved';
        sda3.Requirement_Waiver_Status__c = null;
        update new List<School_Document_Assignment__c> {sda1, sda2, sda3};
        System.assertEquals('Required Documents Outstanding', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);

        sda1.Requirement_Waiver_Status__c = 'Approved';
        sda2.Requirement_Waiver_Status__c = 'Approved';
        sda3.Requirement_Waiver_Status__c = 'Denied';
        update new List<School_Document_Assignment__c> {sda1, sda2, sda3};
        System.assertEquals('Required Documents Outstanding', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);

        sda1.Requirement_Waiver_Status__c = 'Approved';
        sda2.Requirement_Waiver_Status__c = 'Approved';
        sda3.Requirement_Waiver_Status__c = 'Approved';
        update new List<School_Document_Assignment__c> {sda1, sda2, sda3};
        System.assertEquals('Required Documents Complete', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);

        sda1.Requirement_Waiver_Status__c = 'Declined';
        update sda1;
        System.assertEquals('Required Documents Outstanding', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);

        delete sda1;
        System.assertEquals('Required Documents Complete', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);      

        Test.stopTest();
    }

    // [SL] NAIS-282 reworking test class
    @isTest
    private static void testCalculateDocStatusesFromOverride() {
        setupTestData();

        // create test family docs
        docNotReceived = TestUtils.createFamilyDocument('docNotReceived', academicYear.Id, false);
        // SFP-640
        docNotReceived.Household__c = testHousehold.id;
        docNotReceived.Document_Status__c = 'Not Received';
        
        docReceivedVerification = TestUtils.createFamilyDocument('docReceivedVerification', academicYear.Id, false);
        // SFP-640
        docReceivedVerification.Household__c = testHousehold.id;    
        docReceivedVerification.Document_Status__c = 'Received/In Progress';
        docReceivedVerification.Document_Type__c = 'W2';
        
        docReceivedNonVerification = TestUtils.createFamilyDocument('docReceivedNonVerification', academicYear.Id, false);
        // SFP-640
        docReceivedNonVerification.Household__c = testHousehold.id;
        docReceivedNonVerification.Document_Status__c = 'Received/In Progress';
        
        docProcessed = TestUtils.createFamilyDocument('docProcessed', academicYear.Id, false);
        // SFP-640
        docProcessed.Household__c = testHousehold.id;
        docProcessed.Document_Status__c = 'Processed';
        
        insert new List<Family_Document__c> {docNotReceived, docReceivedVerification, docReceivedNonVerification, docProcessed};

        Test.startTest();

        // Verify scenarios with 1 status
        School_Document_Assignment__c sda1 = TestUtils.createSchoolDocumentAssignment('testSDA1', docNotReceived.Id, null, schoolPFSAssignment.Id, true);
        System.assertEquals('No Documents Received', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);

        sda1.Document__c = docReceivedVerification.Id;
        update sda1;
        System.assertEquals('Required Documents Received/In Progress', [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);

        schoolPFSAssignment.PFS_Document_Status_Override__c = true;
        update schoolPFSAssignment;
        System.assertEquals(SchoolSetSPFSDocumentStatus.requiredDocsCompleteStatus, [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);

        schoolPFSAssignment.PFS_Document_Status_Override__c = false;
        update schoolPFSAssignment;
        System.assertNotEquals(SchoolSetSPFSDocumentStatus.requiredDocsCompleteStatus, [SELECT PFS_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:schoolPFSAssignment.Id].PFS_Document_Status__c);
    }
    
    @isTest
    private static void testCalculateTaxDocStatuses()
    {
        setupTestData();
        List<Document_Type_Settings__c> dtsList = new List<Document_Type_Settings__c>();
        dtsList.add(
            new Document_Type_Settings__c(
                Name = 'W2',
                Full_Type_Name__c = 'W2',
                Index_Order__c = 1,
                Tax_Document__c = true
        ));

        insert dtsList;
        
        // create test academic year prior year
        Academic_Year__c priorAcademicYear= TestUtils.createAcademicYear(GlobalVariables.getPreviousYearString(), true);
        String previousYearStr = priorAcademicYear.name.left(4);
        String currentYearStr = academicYear.name.left(4);
        String lastYearStr = String.valueOf(Integer.valueof(previousYearStr)-1);
        
        // create test family docs
        docNotReceived = TestUtils.createFamilyDocument('docNotReceived', academicYear.Id, false);
        // SFP-640
        docNotReceived.Household__c = testHousehold.id;
        docNotReceived.Document_Status__c = 'Not Received';
        docNotReceived.Document_Type__c = 'W2';
        docNotReceived.Document_Year__c = previousYearStr;
        
        cyDocReceivedVerification = TestUtils.createFamilyDocument('docReceivedVerification', academicYear.Id, false);
        // SFP-640
        cyDocReceivedVerification.Household__c = testHousehold.id;
        cyDocReceivedVerification.Document_Status__c = 'Received/In Progress';
        cyDocReceivedVerification.Document_Type__c = 'W2';
        cyDocReceivedVerification.Document_Year__c = previousYearStr;
        
        cyDocReceivedNonVerification = TestUtils.createFamilyDocument('docReceivedNonVerification', academicYear.Id, false);
        // SFP-640
        cyDocReceivedNonVerification.Household__c = testHousehold.id;
        cyDocReceivedNonVerification.Document_Status__c = 'Received/In Progress';
        cyDocReceivedNonVerification.Document_Type__c = 'W2';
        cyDocReceivedNonVerification.Document_Year__c = previousYearStr;
        
        cyDocProcessed = TestUtils.createFamilyDocument('docProcessed', academicYear.Id, false);
        // SFP-640
        cyDocProcessed.Household__c = testHousehold.id;     
        cyDocProcessed.Document_Status__c = 'Processed';
        cyDocProcessed.Document_Type__c = 'W2';
        cyDocProcessed.Document_Year__c = previousYearStr;
        
        insert new List<Family_Document__c> {docNotReceived, cyDocReceivedVerification, cyDocReceivedNonVerification, cyDocProcessed};

        pyDocNotReceived = TestUtils.createFamilyDocument('docNotReceived', academicYear.Id, false);
        // SFP-640
        pyDocNotReceived.Household__c = testHousehold.id;   
        pyDocNotReceived.Document_Status__c = 'Not Received';
        pyDocNotReceived.Document_Type__c = 'W2';
        pyDocNotReceived.Document_Year__c = lastYearStr;

        pyDocReceivedVerification = TestUtils.createFamilyDocument('docReceivedVerification', academicYear.Id, false);
        // SFP-640
        pyDocReceivedVerification.Household__c = testHousehold.id;
        pyDocReceivedVerification.Document_Status__c = 'Received/In Progress';
        pyDocReceivedVerification.Document_Type__c = 'W2';
        pyDocReceivedVerification.Document_Year__c = lastYearStr;
        
        pyDocReceivedNonVerification = TestUtils.createFamilyDocument('docReceivedNonVerification', academicYear.Id, false);
        // SFP-640
        pyDocReceivedNonVerification.Household__c = testHousehold.id;
        pyDocReceivedNonVerification.Document_Status__c = 'Received/In Progress';
        pyDocReceivedNonVerification.Document_Type__c = 'W2';
        pyDocReceivedNonVerification.Document_Year__c = lastYearStr;
        
        pyDocProcessed = TestUtils.createFamilyDocument('docProcessed', academicYear.Id, false);
        // SFP-640
        pyDocProcessed.Household__c = testHousehold.id;
        pyDocProcessed.Document_Status__c = 'Processed';
        pyDocProcessed.Document_Type__c = 'W2';
        pyDocProcessed.Document_Year__c = lastYearStr;
        
        insert new List<Family_Document__c> {pyDocNotReceived, pyDocReceivedVerification, pyDocReceivedNonVerification, pyDocProcessed};

        Test.startTest();

        // Verify scenarios with 1 status
        School_Document_Assignment__c sda1 = TestUtils.createSchoolDocumentAssignment('testSDA1', docNotReceived.Id, null, schoolPFSAssignment.Id, true);
        School_Document_Assignment__c sda1b = TestUtils.createSchoolDocumentAssignment('testSDA1b', docNotReceived.Id, null, schoolPFSAssignment.Id, true);
        sda1.Document_Type__c = 'W2';
        sda1.Document_Year__c = previousYearStr;
        sda1b.Document_Type__c = 'W2';
        sda1b.Document_Year__c = previousYearStr;
        update new List<School_Document_Assignment__c>{sda1,sda1b};
        
        School_PFS_Assignment__c spfsAssignment = getSchoolPFSAssignment(schoolPFSAssignment.Id);
        System.assertEquals('No Documents Received', spfsAssignment.PFS_Document_Status__c);
        System.assertEquals('No Documents Received', spfsAssignment.PFS_Current_Year_Tax_Document_Status__c);
        System.assertEquals('No Documents Received', spfsAssignment.PFS_Prior_Year_Tax_Document_Status__c);
            
        sda1.Document__c = cyDocReceivedVerification.Id;
        update sda1;

        spfsAssignment = getSchoolPFSAssignment(schoolPFSAssignment.Id);
        System.assertEquals('Required Documents Outstanding', spfsAssignment.PFS_Document_Status__c);
        System.assertEquals('Required Documents Outstanding', spfsAssignment.PFS_Current_Year_Tax_Document_Status__c);
        System.assertEquals('No Documents Received', spfsAssignment.PFS_Prior_Year_Tax_Document_Status__c);

        sda1b.Document__c = cyDocReceivedVerification.Id;
        update sda1b;

        spfsAssignment = getSchoolPFSAssignment(schoolPFSAssignment.Id);
        System.assertEquals('Required Documents Received/In Progress', spfsAssignment.PFS_Document_Status__c);
        System.assertEquals('Required Documents Received/In Progress', spfsAssignment.PFS_Current_Year_Tax_Document_Status__c);
        System.assertEquals('No Documents Received', spfsAssignment.PFS_Prior_Year_Tax_Document_Status__c);
    
        sda1.Document__c = cyDocReceivedNonVerification.Id;
        update sda1;
        spfsAssignment = getSchoolPFSAssignment(schoolPFSAssignment.Id);
        System.assertEquals('Required Documents Received/In Progress', spfsAssignment.PFS_Document_Status__c);
        System.assertEquals('Required Documents Received/In Progress', spfsAssignment.PFS_Current_Year_Tax_Document_Status__c);
        System.assertEquals('No Documents Received', spfsAssignment.PFS_Prior_Year_Tax_Document_Status__c);
    
        sda1.Document__c = cyDocProcessed.Id;
        update sda1;
        spfsAssignment = getSchoolPFSAssignment(schoolPFSAssignment.Id);
        System.assertEquals('Required Documents Received/In Progress', spfsAssignment.PFS_Document_Status__c);
        System.assertEquals('Required Documents Received/In Progress', spfsAssignment.PFS_Current_Year_Tax_Document_Status__c);
        System.assertEquals('No Documents Received', spfsAssignment.PFS_Prior_Year_Tax_Document_Status__c);

        sda1b.Document__c = docNotReceived.Id;
        update sda1b;

        spfsAssignment = getSchoolPFSAssignment(schoolPFSAssignment.Id);
        System.assertEquals('Required Documents Outstanding', spfsAssignment.PFS_Document_Status__c);
        System.assertEquals('Required Documents Outstanding', spfsAssignment.PFS_Current_Year_Tax_Document_Status__c);
        System.assertEquals('No Documents Received', spfsAssignment.PFS_Prior_Year_Tax_Document_Status__c);


        sda1b.Document__c = cyDocProcessed.Id;
        update sda1b;

        spfsAssignment = getSchoolPFSAssignment(schoolPFSAssignment.Id);
        System.assertEquals('Required Documents Complete', spfsAssignment.PFS_Document_Status__c);
        System.assertEquals('Required Documents Complete', spfsAssignment.PFS_Current_Year_Tax_Document_Status__c);
        System.assertEquals('No Documents Received', spfsAssignment.PFS_Prior_Year_Tax_Document_Status__c);
    
        // Add Prior Year
        // Verify scenarios with 2 statuses
        School_Document_Assignment__c sda2 = TestUtils.createSchoolDocumentAssignment('testSDA2', pyDocNotReceived.Id, null, schoolPFSAssignment.Id, true);
        sda2.Document_Type__c = 'W2';
        sda2.Document_Year__c = lastYearStr;
        update sda2;

        spfsAssignment = getSchoolPFSAssignment(schoolPFSAssignment.Id);
        System.assertEquals('Required Documents Outstanding', spfsAssignment.PFS_Document_Status__c);
        System.assertEquals('Required Documents Complete', spfsAssignment.PFS_Current_Year_Tax_Document_Status__c);
        System.assertEquals('No Documents Received', spfsAssignment.PFS_Prior_Year_Tax_Document_Status__c);

        sda2.Document__c = pyDocReceivedNonVerification.Id;
        update sda2;

        spfsAssignment = getSchoolPFSAssignment(schoolPFSAssignment.Id);
        System.assertEquals('Required Documents Received/In Progress', spfsAssignment.PFS_Document_Status__c);
        System.assertEquals('Required Documents Complete', spfsAssignment.PFS_Current_Year_Tax_Document_Status__c);
        System.assertEquals('Required Documents Received/In Progress', spfsAssignment.PFS_Prior_Year_Tax_Document_Status__c);

        sda2.Document__c = pyDocProcessed.Id;
        update sda2;

        spfsAssignment = getSchoolPFSAssignment(schoolPFSAssignment.Id);
        System.assertEquals('Required Documents Complete', spfsAssignment.PFS_Document_Status__c);
        System.assertEquals('Required Documents Complete', spfsAssignment.PFS_Current_Year_Tax_Document_Status__c);
        System.assertEquals('Required Documents Complete', spfsAssignment.PFS_Prior_Year_Tax_Document_Status__c);

        Test.stopTest();
    }

    private static School_PFS_Assignment__c getSchoolPFSAssignment(String id) {
        return [SELECT PFS_Document_Status__c, PFS_Current_Year_Tax_Document_Status__c, PFS_Prior_Year_Tax_Document_Status__c FROM School_PFS_Assignment__c WHERE Id=:id];
    }
    
    private static void createTestDataWithSDAS()
    {
        //Create Academic year
        academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        
        //Create School
        school = TestUtils.createAccount('testSchool', RecordTypes.schoolAccountTypeId, null, true); 
        
        //Create Current Tax Year
        String currentTaxYear = GlobalVariables.getDocYearStringFromAcadYearName(GlobalVariables.getCurrentYearString());
        
        //Create Parent
        parent = TestUtils.createContact('testParent', null, RecordTypes.parentContactTypeId, true);
        
        //Create a student
        Contact student1 = TestUtils.createContact('testStudent1', null, RecordTypes.studentContactTypeId, false);
        Contact student2 = TestUtils.createContact('testStudent2', null, RecordTypes.studentContactTypeId, false);
        Contact student3 = TestUtils.createContact('testStudent3', null, RecordTypes.studentContactTypeId, false);
        insert new List<Contact>{student1, student2, student3};
        
        //Create a pfs
        pfs = TestUtils.createPFS('testPFS', academicYear.Id, parent.Id, true);

        //Create an applicant
        Applicant__c applicant1 = TestUtils.createApplicant(student1.Id, pfs.Id, false);
        Applicant__c applicant2 = TestUtils.createApplicant(student2.Id, pfs.Id, false);
        Applicant__c applicant3 = TestUtils.createApplicant(student3.Id, pfs.Id, false);
        insert new List<Applicant__c>{applicant1, applicant2, applicant3};
        
        //Create a student folder
        Student_Folder__c studentFolder1 = TestUtils.createStudentFolder('testStudentFolder1', academicYear.Id, student1.Id, false);
        Student_Folder__c studentFolder2 = TestUtils.createStudentFolder('testStudentFolder2', academicYear.Id, student2.Id, false);
        Student_Folder__c studentFolder3 = TestUtils.createStudentFolder('testStudentFolder3', academicYear.Id, student3.Id, false);
        insert new List<Student_Folder__c>{studentFolder1, studentFolder2, studentFolder3};
        
        //Create School PFS Assigments
        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYear.Id, applicant1.Id, school.Id, studentFolder1.Id, false);
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(academicYear.Id, applicant2.Id, school.Id, studentFolder2.Id, false);
        School_PFS_Assignment__c spfsa3 = TestUtils.createSchoolPFSAssignment(academicYear.Id, applicant3.Id, school.Id, studentFolder3.Id, false);       
        insert new List<School_PFS_Assignment__c>{spfsa1, spfsa2, spfsa3};
        
        //Create Family Document
        docProcessed = TestUtils.createFamilyDocument('FamilyDoc1', academicYear.Id, true);
        
        //3. Create Required Document
        Required_Document__c rd1 = TestUtils.createRequiredDocument(academicYear.Id, school.Id, true);
        
        //Create School Documen Assigments
        sdaA = TestUtils.createSchoolDocumentAssignment('sdaA', docProcessed.Id, null, spfsa1.Id, false);
        sdaA.Document_Year__c = currentTaxYear;
        sdaB = TestUtils.createSchoolDocumentAssignment('sdaB', docProcessed.Id, null, spfsa2.Id, false);
        sdaB.Document_Year__c = currentTaxYear;
        sdaC = TestUtils.createSchoolDocumentAssignment('sdaC', docProcessed.Id, rd1.Id, spfsa3.Id, false);
        sdaC.Document_Year__c = currentTaxYear; 
        
    }//End:createTestDataWithSDAS
    
    @isTest
    private static void updateDocumentStatusFromFamilyDocuments_notRequiredDocumentCreatedThroughAPI_removeRelatedSDAS()
    {
        //1. Create test data
        createTestDataWithSDAS();
        
        //2. Create an API User to be used to insert the SDAS
        User apiUser = UserTestData.Instance.forProfileId(GlobalVariables.securityProfilesMap.get('API Profile')).create();

        Test.startTest();
        
            System.runAs(apiUser) {
                insert new List<School_Document_Assignment__c>{sdaA, sdaB, sdaC};
            }
        
           //3. Emulate that the FamilyDocument is deleted from Family Portal
           docProcessed.Deleted__c = true;
           update docProcessed;
           
        Test.stopTest();
        
        List<School_Document_Assignment__c> resultSDA1, resultSDA2, resultSDA3;
        
        //4. If the SDA is not for a required document, and is created by API User/not a school user, remove the SDA
        resultSDA1 = new List<School_Document_Assignment__c>([Select Id from School_Document_Assignment__c WHERE Id=:sdaA.Id LIMIT 1]);
        System.assertEquals(0, resultSDA1.size(), 'If the SDA is not for a required document, and is created by API User/not a school user, remove the SDA.');
        
        resultSDA2 = new List<School_Document_Assignment__c>([Select Id from School_Document_Assignment__c WHERE Id=:sdaB.Id LIMIT 1]);
        System.assertEquals(0, resultSDA2.size(), 'If the SDA is not for a required document, and is created by API User/not a school user, remove the SDA');
        
        resultSDA3 = new List<School_Document_Assignment__c>([Select Id, Document__c from School_Document_Assignment__c WHERE Id=:sdaC.Id LIMIT 1]);
        System.assertEquals(1, resultSDA3.size());
        System.assertEquals(null, resultSDA3[0].Document__c, 'The Document__c must be blank, after the FD was deleted.');
    }//End:updateDocumentStatusFromFamilyDocuments_notRequiredDocumentCreatedThroughAPI_removeRelatedSDAS
}