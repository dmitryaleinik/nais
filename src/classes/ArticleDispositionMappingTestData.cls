/**
 * @description This class is used to create Article Disposition Mapping records for unit tests.
 */
@isTest
public with sharing class ArticleDispositionMappingTestData extends SObjectTestData
{

    private static final String ADM_TYPE = 'Documents';

   /**
     * @description Get the default values for the Article_Disposition_Mapping__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap()
    {
        return new Map<Schema.SObjectField,Object> 
        {
            Article_Disposition_Mapping__c.Name => KnowledgeAVTestData.Instance.DefaultKnowledgeAV.Title,
            Article_Disposition_Mapping__c.Title__c => KnowledgeAVTestData.Instance.DefaultKnowledgeAV.Title,
            Article_Disposition_Mapping__c.Article_Number__c => KnowledgeAVTestData.Instance.DefaultKnowledgeAV.ArticleNumber,
            Article_Disposition_Mapping__c.Visible_In_Public_Knowledge_Base__c =>  true,
            Article_Disposition_Mapping__c.Visible_to_Customer__c =>  true,
            Article_Disposition_Mapping__c.Type__c =>  ADM_TYPE
        };
    }

    /**
     * @description Set the Article_Number__c of the Article_Disposition_Mapping__c record.
     * @param articleNumber The KnowledgeArticle record ArticleNumber to set the current Article_Disposition_Mapping__c record to.
     * @return The current instance of ArticleDispositionMappingTestData.
     */
    public ArticleDispositionMappingTestData forArticleNumber(String articleNumber)
    {
        return (ArticleDispositionMappingTestData) with(Article_Disposition_Mapping__c.Article_Number__c, articleNumber);
    }

    /**
     * @description Insert the current working Article_Disposition_Mapping__c record.
     * @return The currently operated upon Article_Disposition_Mapping__c record.
     */
    public Article_Disposition_Mapping__c insertArticleDispositionMapping()
    {
        return (Article_Disposition_Mapping__c)insertRecord();
    }

    /**
     * @description Inserts a list of Article_Disposition_Mapping__c and ties into the before and after hooks.
     * @param numToInsert the number of Article_Disposition_Mapping__c to insert.
     * @return The inserted SObjects.
     */
    public List<Article_Disposition_Mapping__c> insertArticleDispositionMappings(Integer numToInsert)
    {
        return (List<Article_Disposition_Mapping__c>)insertRecords(numToInsert);
    }

    /**
     * @description Create the current working Article_Disposition_Mapping__c record without resetting
     *             the stored values in this instance of ArticleDispositionMappingTestData.
     * @return A non-inserted Article_Disposition_Mapping__c record using the currently stored field
     *             values.
     */
    public Article_Disposition_Mapping__c createArticleDispositionMappingWithoutReset()
    {
        return (Article_Disposition_Mapping__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Article_Disposition_Mapping__c record.
     * @return The currently operated upon Article_Disposition_Mapping__c record.
     */
    public Article_Disposition_Mapping__c create()
    {
        return (Article_Disposition_Mapping__c)super.buildWithReset();
    }

    /**
     * @description The default Article_Disposition_Mapping__c record.
     */
    public Article_Disposition_Mapping__c DefaultArticleDispositionMapping
    {
        get
        {
            if (DefaultArticleDispositionMapping == null)
            {
                DefaultArticleDispositionMapping = createArticleDispositionMappingWithoutReset();
                insert DefaultArticleDispositionMapping;
            }
            return DefaultArticleDispositionMapping;
        }
        private set;
    }

    /**
     * @description Get the Article_Disposition_Mapping__c SObjectType.
     * @return The Article_Disposition_Mapping__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType()
    {
        return Article_Disposition_Mapping__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static ArticleDispositionMappingTestData Instance
    {
        get
        {
            if (Instance == null)
            {
                Instance = new ArticleDispositionMappingTestData();
            }
            return Instance;
        }
        private set;
    }

    private ArticleDispositionMappingTestData() {}
}