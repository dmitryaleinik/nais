@isTest
private with sharing class ApplicationFeeWaiverServiceTest
{

    private static PFS__c pfsA;
    private static Annual_Setting__c currAnnualSettings;
    private static Account school;
    private static Academic_Year__c academicYear;

    private static void initTestData(Boolean autoFee) {
        AcademicYearTestData.Instance.insertAcademicYears(5);
        academicYear = GlobalVariables.getCurrentAcademicYear();


        Account account = AccountTestData.Instance
                .forName('Family Account')
                .forRecordTypeId(RecordTypes.individualAccountTypeId)
                .forSSSSchoolCode('123456780')
                .DefaultAccount;

        school = AccountTestData.Instance
                .forName('Test School')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .DefaultAccount;

        Contact parentA = ContactTestData.Instance
                .forLastName('Parent A')
                .forAccount(account.Id)
                .forRecordTypeId(RecordTypes.parentContactTypeId)
                .DefaultContact;

        Contact student = ContactTestData.Instance
                .forLastName('Student')
                .forRecordTypeId(RecordTypes.studentContactTypeId)
                .DefaultContact;

        Student_Folder__c studentFolder = StudentFolderTestData.Instance
                .forName('Student Folder')
                .forAcademicYearPicklist(academicYear.Name)
                .forStudentId(student.Id)
                .forDayBoarding('Day')
                .forFolderSource('PFS')
                .DefaultStudentFolder;


        SchoolPortalSettings.KS_School_Account_Id = school.Id;


        currAnnualSettings = AnnualSettingsTestData.Instance
                .forAcademicYearId(academicYear.Id)
                .forSchoolId(school.Id)
                .DefaultAnnualSettings;


        pfsA = PfsTestData.Instance
                .forParentA(parentA.Id)
                .forAcademicYearPicklist(academicYear.Name)
                .DefaultPfs;


        Applicant__c applicant = ApplicantTestData.Instance
                .forPfsId(pfsA.Id)
                .forContactId(student.Id)
                .DefaultApplicant;


        String pfsSql = 'SELECT ' + String.Join(ExpCore.GetAllFieldNames('PFS__c'), ',') + ', (SELECT ' +
                String.Join(ExpCore.GetAllFieldNames('Applicant__c'), ',') + ' FROM Applicants__r) FROM PFS__c LIMIT 1';
        pfsA = Database.query(pfsSql);


        School_PFS_Assignment__c spfsa = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(pfsA.applicants__r[0].Id)
                .forStudentFolderId(studentFolder.Id)
                .forSchoolId(school.Id)
                .forAcademicYearPicklist(academicYear.Name)
                .DefaultSchoolPfsAssignment;



    }

    private static void createCurrentAnnualSettings(Integer totalWaivers) {
        currAnnualSettings = [SELECT Academic_Year__c, School__c, School__r.Current_Year_Waiver_Balance__c, School__r.Current_Year_Waivers_Assigned__c,
                                     Total_Waivers_Override_Default__c, Waivers_Assigned__c
                              FROM Annual_Setting__c
                              WHERE School__c = :school.id];

        currAnnualSettings.Total_Waivers_Override_Default__c = totalWaivers;
        currAnnualSettings.Waivers_Assigned__c = 0;

        update currAnnualSettings;
    }

    private static List<Annual_Setting__c> getCurrentAnnualSettings() {
        return [SELECT Academic_Year__c, School__c, School__r.Current_Year_Waiver_Balance__c, School__r.Current_Year_Waivers_Assigned__c,
                       Total_Waivers_Override_Default__c, Waivers_Assigned__c
                  FROM Annual_Setting__c
                 WHERE School__c = :school.id];
    }

    private static Application_Fee_Waiver__c insertPendingWaiver() {
        Application_Fee_Waiver__c waiver = ApplicationFeeWaiverTestData.Instance
                .forAcademicYearId(academicYear.Id)
                .forAccountId(school.id)
                .forContactId(pfsA.Parent_A__c)
                .asPending()
                .DefaultApplicationFeeWaiver;

        return waiver;
    }

    @isTest
    private static void applyAutoWaiver_AutoWaiverEnabled_schoolHasWaiversAvailable_expectAutoWaiverAssigned() {
        setupAutoWaiverAndData(true, 3);

        Test.startTest();

        ApplicationFeeWaiverService.Instance.applyAvailableWaivers(pfsA);

        List<Application_Fee_Waiver__c> afws = [SELECT Id, Account__c, Status__c FROM Application_Fee_Waiver__c WHERE Contact__c = :pfsA.Parent_A__c];
        assertSizeAndAssigned(false, 1, afws);
        assertSchoolDecreasedAutoWaiverCount();

        Test.stopTest();

    }

    @isTest
    private static void applyAutoWaivers_autoWaiverEnabled_schoolHasExistingWaiver_useAutoWaiver_expectExistingDeclined() {

        setupAutoWaiverAndData(true, 3);


        // Insert pending waiver to be used
        Application_Fee_Waiver__c pendingWaiver = insertPendingWaiver();

        Test.startTest();

        ApplicationFeeWaiverService.Instance.applyAvailableWaivers(pfsA);

        // Get all waiver's for the PFS
        List<Application_Fee_Waiver__c> afws = [SELECT Id, Account__c, Status__c FROM Application_Fee_Waiver__c WHERE Contact__c = :pfsA.Parent_A__c];

        assertSizeAndAssigned(false, 2, afws);
        assertSchoolDecreasedAutoWaiverCount();

        Test.stopTest();
    }

    @isTest
    private static void applyAutoWaivers_autoWaiverEnabled_schoolMBFWEligible_autoWaiverAssigned() {
        setupAutoWaiverAndData(true, 3);

        // Create mbfw Account
        Account meansBasedFeeWaiverAccount = TestUtils.createAccount('MBFW Account', RecordTypes.meansBasedFeeWaiverAccountTypeId, 5, false);
        insert new List<Account> { meansBasedFeeWaiverAccount };

        // Create settings for MBFW
        createApplicationFeeSettings();

        Test.startTest();

        ApplicationFeeWaiverService.Instance.applyAvailableWaivers(pfsA);

        List<Application_Fee_Waiver__c> afws = [SELECT Id, Account__c, Status__c FROM Application_Fee_Waiver__c WHERE Contact__c = :pfsA.Parent_A__c];

        assertSizeAndAssigned(false, 1, afws);
        assertSchoolDecreasedAutoWaiverCount();

        Test.stopTest();


    }

    @isTest
    private static void applyMbfw_autoWaiverEnabled_autoWaiversEmpty_schoolMbfwEligible_mbfwAssigned() {
        setupAutoWaiverAndData(true, 0);

        // Create mbfw Account
        Account meansBasedFeeWaiverAccount = TestUtils.createAccount('MBFW Account', RecordTypes.meansBasedFeeWaiverAccountTypeId, 5, false);
        insert new List<Account> { meansBasedFeeWaiverAccount };

        // Create settings for MBFW
        createApplicationFeeSettings();

        //update pfs for MBFW qualification
        pfsA.Own_Business_or_Farm__c = 'No';
        pfsA.Family_Size__c = 2;
        pfsA.Salary_Wages_Parent_A__c = 20000;
        pfsA.Bank_Account_Value__c = 1000;
        pfsA.Parent_A_Country__c = 'United States';
        update pfsA;


        // Insert FeeLunchGuidelines
        Free_Lunch_Guideline__c flG = new Free_Lunch_Guideline__c(Academic_Year__c = GlobalVariables.getCurrentAcademicYear().Id, Family_Size__c = 2.0, Total_Income__c = 20700.00);
        insert flG;

        Test.startTest();

        ApplicationFeeWaiverService.Instance.applyAvailableWaivers(pfsA);

        List<Application_Fee_Waiver__c> afws = [SELECT Id, Account__c, Status__c FROM Application_Fee_Waiver__c WHERE Contact__c = :pfsA.Parent_A__c];

        assertSizeAndAssigned(false, 1, afws);

        // Waiver assigned should be MBFW
        System.assertEquals(afws[0].Account__c, meansBasedFeeWaiverAccount.Id);

        Test.stopTest();
    }

    @isTest
    private static void applyMbfwFlow_autoWaiversDisabled_schoolMbfwEligible_mbfwAssigned() {

        setupAutoWaiverAndData(false, 3);

        // Create mbfw Account
        Account meansBasedFeeWaiverAccount = TestUtils.createAccount('MBFW Account', RecordTypes.meansBasedFeeWaiverAccountTypeId, 5, false);
        insert new List<Account> { meansBasedFeeWaiverAccount };

        // Create settings for MBFW
        createApplicationFeeSettings();

        //update pfs for MBFW qualification
        pfsA.Own_Business_or_Farm__c = 'No';
        pfsA.Family_Size__c = 2;
        pfsA.Salary_Wages_Parent_A__c = 20000;
        pfsA.Bank_Account_Value__c = 1000;
        pfsA.Parent_A_Country__c = 'United States';
        update pfsA;


        // Insert FeeLunchGuidelines
        Free_Lunch_Guideline__c flG = new Free_Lunch_Guideline__c(Academic_Year__c = GlobalVariables.getCurrentAcademicYear().Id, Family_Size__c = 2.0, Total_Income__c = 20700.00);
        insert flG;

        Test.startTest();

        ApplicationFeeWaiverService.Instance.applyAvailableWaivers(pfsA);

        List<Application_Fee_Waiver__c> afws = [SELECT Id, Account__c, Status__c FROM Application_Fee_Waiver__c WHERE Contact__c = :pfsA.Parent_A__c];

        assertSizeAndAssigned(false, 1, afws);

        // Waiver assigned should be MBFW
        System.assertEquals(afws[0].Account__c, meansBasedFeeWaiverAccount.Id);

        Test.stopTest();
    }

    @isTest
    private static void applyMbfwFlow_autoWaiversDisabled_schoolNotMbfwEligibleWithExistingWaiver_existingWaiverAssigned() {
        setupAutoWaiverAndData(false, 3);

        // Insert pending waiver to be used
        Application_Fee_Waiver__c pendingWaiver = insertPendingWaiver();

        Test.startTest();

        ApplicationFeeWaiverService.Instance.applyAvailableWaivers(pfsA);

        // Get all waiver's for the PFS
        List<Application_Fee_Waiver__c> afws = [SELECT Id, Account__c, Status__c FROM Application_Fee_Waiver__c WHERE Contact__c = :pfsA.Parent_A__c];


        assertSizeAndAssigned(false, 1, afws);

        // Waiver should be the pending waiver
        System.assertEquals(afws[0].Id, pendingWaiver.Id);

        Test.stopTest();
    }

    @isTest
    private static void applyAutoWaiver_autoWaiversEnabled_zeroWaiversAvailableAndAssigned_noWaiversAssigned() {
        setupAutoWaiverAndData(true, 0);

        Test.startTest();

        ApplicationFeeWaiverService.Instance.applyAvailableWaivers(pfsA);

        // Get all waiver's for the PFS
        List<Application_Fee_Waiver__c> afws = [SELECT Id, Account__c, Status__c FROM Application_Fee_Waiver__c WHERE Contact__c = :pfsA.Parent_A__c];

        // Pending waivers, auto waivers and MBFW weren't available
        assertSizeAndAssigned(true, 0, afws);

        Test.stopTest();
    }

    @isTest
    private static void applyMbfwFlow_autoWaiversDisabled_nonMbfwEligibleAndZeroWaiversAssigned_noWaiversAssigned() {

        setupAutoWaiverAndData(false, 0);

        Test.startTest();

        ApplicationFeeWaiverService.Instance.applyAvailableWaivers(pfsA);

        // Get all waiver's for the PFS
        List<Application_Fee_Waiver__c> afws = [SELECT Id, Account__c, Status__c FROM Application_Fee_Waiver__c WHERE Contact__c = :pfsA.Parent_A__c];

        assertSizeAndAssigned(true, 0, afws);


        Test.stopTest();
    }

    private static void setupAutoWaiverAndData(Boolean autoWaiverEnabled, Integer totalWaivers) {
        // Turn on Auto Waiver feature
        FeatureToggles.setToggle('Auto_Fee_Waiver__c', autoWaiverEnabled);

        initTestData(true);

        // Enable Auto Fee Waiver's for the school
        school.Auto_Fee_Waiver__c = true;
        update school;

        // Create Annual Setting with Waiver available
        createCurrentAnnualSettings(totalWaivers);
    }

    private static void assertSizeAndAssigned(Boolean empty, Integer size, List<Application_Fee_Waiver__c> afws) {
        System.assertEquals(empty, afws.isEmpty());

        System.assertEquals(size, afws.size());

        if (size > 1) {
            // Waiver should be assigned
            Boolean assigned = false;
            Boolean declined = false;
            for (Application_Fee_Waiver__c waiver : afws) {
                if (waiver.Status__c == ApplicationFeeWaiverService.ASSIGNED) {
                    assigned = true;
                } else if (waiver.Status__c == ApplicationFeeWaiverService.WAIVER_DENIED_STATUS) {
                    declined = true;
                }
            }
            System.assert(assigned);
            System.assert(declined);
        } else if (size > 0) {
            System.assertEquals(afws[0].Status__c, ApplicationFeeWaiverService.ASSIGNED);
        }

    }

    private static void assertSchoolDecreasedAutoWaiverCount() {
        List<Annual_Setting__c> annualSettings = getCurrentAnnualSettings();
        System.assert(annualSettings[0].School__r.Current_Year_Waiver_Balance__c == 2);
        System.assert(annualSettings[0].School__r.Current_Year_Waivers_Assigned__c == 1);

    }

    private static void createApplicationFeeSettings() {
        Application_Fee_Settings__c afs = new Application_Fee_Settings__c(Name = GlobalVariables.getCurrentYearString(),
                Amount__c = 42,
                Discounted_Amount__c = 42,
                Discounted_Product_Code__c = '131-200 PFS Fee - KS - 2015',
                Product_Code__c = '131-100 PFS Fee - 2015',
                Means_Based_Fee_Waiver_Record_Type_Name__c
                        = ApplicationFeeSettingsService.MEANS_BASED_FEE_WAIVER_RECORDTYPE,
                Maximum_Assets_Waiver__c = 100000);
        insert afs;
    }

    private static void setupTotalIncomeForMbfwEligible(String HouseholdHasReceivedBenefits) {
        setupAutoWaiverAndData(true, 3);

        // Create mbfw Account
        Account meansBasedFeeWaiverAccount = TestUtils.createAccount('MBFW Account', RecordTypes.meansBasedFeeWaiverAccountTypeId, 5, false);
        insert new List<Account> { meansBasedFeeWaiverAccount };

        // Create settings for MBFW
        createApplicationFeeSettings();
        
        // Insert FeeLunchGuidelines
        Free_Lunch_Guideline__c fLG = new Free_Lunch_Guideline__c(
        Academic_Year__c = GlobalVariables.getCurrentAcademicYear().Id, 
        Family_Size__c = 2.0, 
        Total_Income__c = 20700.00);
        insert fLG;
        
        //update pfs for MBFW qualification
        pfsA.Household_Has_Received_Benefits__c = HouseholdHasReceivedBenefits;
        pfsA.Own_Business_or_Farm__c = 'No';
        pfsA.Family_Size__c = 2;
        pfsA.Salary_Wages_Parent_A__c = 20000;
        pfsA.Bank_Account_Value__c = 1000;
        pfsA.Parent_A_Country__c = 'United States';
        pfsA.Have_Other_Nontaxable_Income__c = 'Yes';//8c. To avoid applicationUtils.clearNonTaxableWorksheet clear Welfare_Veterans_Workers_Comp_Current__c
        pfsA.Welfare_Veterans_Workers_Comp_Current__c = 10000;        
        update pfsA;
    }

    @isTest
    private static void applyAutoWaivers_autoWaiverEnabledAndPfsEzOptOut_EzQuestion3IsNo_autoWaiverAssigned()
    {
        setupTotalIncomeForMbfwEligible('No');

        Test.startTest();
            Academic_Year__c academicYear = GlobalVariables.getCurrentAcademicYear();
            Free_Lunch_Guideline__c fLG = [SELECT Total_Income__c FROM Free_Lunch_Guideline__c
                                            WHERE Family_Size__c=2 AND Academic_Year__c =:academicYear.Id LIMIT 1];
            
            //Query PFS with all fields.
            Id pfsId = pfsA.Id;
            String pfsSql = 'SELECT ' + String.Join(ExpCore.GetAllFieldNames('PFS__c'), ',') + ', (SELECT ' +
                String.Join(ExpCore.GetAllFieldNames('Applicant__c'), ',') + ' FROM Applicants__r) FROM PFS__c WHERE Id =: pfsId LIMIT 1';
            pfsA = Database.query(pfsSql);
            
            //Just to verify that we have the correct data to for a NON MBFW elegible.
            EfcWorksheetData worksheet1 = EfcCalculatorAction.calculateSssEfc(new Set<Id> {pfsA.Id}, false)[0];
            System.AssertEquals(10000, worksheet1.welfareVeteransAndWorkersComp);
            System.AssertEquals(30000, worksheet1.totalIncome);
            System.AssertEquals(1000, worksheet1.totalAssets);
            System.AssertEquals(true, worksheet1.totalAssets < Application_Fee_Settings__c.getInstance(academicYear.Name).Maximum_Assets_Waiver__c);
            System.AssertEquals(false, worksheet1.totalIncome < fLG.Total_Income__c);
            
            //We test the mbfwEligible method.
            System.AssertEquals(false, ApplicationFeeWaiverService.Instance.mbfwEligible(pfsA));
        Test.stopTest();
    }

    @isTest
    private static void applyAutoWaivers_autoWaiverEnabledAndPfsEzOptOut_EzQuestion3IsYes_autoWaiverAssigned()
    {
        //When Question #3 on the PFS EZ questionnaire = yes. Then, the total income calculation for the MBW formula,
        //subtract Welfare_Veterans_Workers_Comp_Current__c value from the PFS.totalIncome.
        //That means that in this case PFS.totalIncome = 30000 (Salary_Wages_Parent_A__c + Welfare_Veterans_Workers_Comp_Current__c) - 5000 (Welfare_Veterans_Workers_Comp_Current__c)
        setupTotalIncomeForMbfwEligible('Yes');

        Test.startTest();
            Academic_Year__c academicYear = GlobalVariables.getCurrentAcademicYear();
            Free_Lunch_Guideline__c fLG = [SELECT Total_Income__c FROM Free_Lunch_Guideline__c
                                            WHERE Family_Size__c=2 AND Academic_Year__c =:academicYear.Id LIMIT 1];
        
        
	        Id pfsId = pfsA.Id;
	        String pfsSql = 'SELECT ' + String.Join(ExpCore.GetAllFieldNames('PFS__c'), ',') + ', (SELECT ' +
	            String.Join(ExpCore.GetAllFieldNames('Applicant__c'), ',') + ' FROM Applicants__r) FROM PFS__c WHERE Id =: pfsId LIMIT 1';
	        pfsA = Database.query(pfsSql);
	        
	        //Just to verify that we have the correct data to for a MBFW elegible.
	        EfcWorksheetData worksheet = EfcCalculatorAction.calculateSssEfc(new Set<Id> {pfsA.Id}, false)[0];
	        Decimal aFSAssetMax = Application_Fee_Settings__c.getInstance(academicYear.Name).Maximum_Assets_Waiver__c;
	        Decimal totalIncome = worksheet.totalIncome - EfcUtil.nullToZero(worksheet.welfareVeteransAndWorkersComp);            
	        System.AssertEquals(10000, worksheet.welfareVeteransAndWorkersComp);
	        System.AssertEquals(30000, worksheet.totalIncome);
	        System.AssertEquals(20000, totalIncome);
	        System.AssertEquals(true, worksheet.totalAssets < Application_Fee_Settings__c.getInstance(academicYear.Name).Maximum_Assets_Waiver__c);
	        System.AssertEquals(true, totalIncome < fLG.Total_Income__c);
	        
	        //We test the mbfwEligible method.
	        System.AssertEquals(true, ApplicationFeeWaiverService.Instance.mbfwEligible(pfsA));
        Test.stopTest();
    }
}