public with sharing class FamilyAppBusinessFarmInfoController extends FamilyAppCompController {
    
    public List<SelectOption> ParentOptions {
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('', ''));
            
            List<Schema.Picklistentry> entries = PFS__c.Business_Farm_Owner__c.getDescribe().getPickListValues();
            
            for(Schema.Picklistentry entry : entries) {
                
                if(entry.getValue() == 'Parent A' && this.pfs.Parent_A__c != null) {

                    options.add(new SelectOption(entry.getValue(), this.pfs.Parent_A__r.FirstName + ' ' + this.pfs.Parent_A__r.LastName));

                } else if(entry.getValue() == 'Parent B' && this.pfs.Parent_B__c != null) {

                    options.add(new SelectOption(entry.getValue(), this.pfs.Parent_B__r.FirstName + ' ' + this.pfs.Parent_B__r.LastName));

                } else if(this.pfs.Parent_B__c != null){

                    options.add(new SelectOption(entry.getValue(), entry.getLabel()));

                }
            }
            
            return options;
        }
    }

}