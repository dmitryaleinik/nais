/**
 * @description Query selector for Case records.
 */
public class CaseSelector extends fflib_SObjectSelector {
    @testVisible private static final String CASE_RECORD_TYPE_IDS  = 'recordTypeIds';
    @testVisible private static final String SCHOOL_IDS = 'schoolIds';
    @testVisible private static final String ACADEMIC_YEAR_START_DATE  = 'academicYearStartDate';
    @testVisible private static final String ACADEMIC_YEAR_END_DATE  = 'academicYearEndDate';
    @testVisible private static final String SELECT_LIMIT_SIZE  = 'limitSize';
    @testVisible private static final String SELECT_OFFSET_SIZE  = 'offsetSize';

    /**
     * @description Select Case records by parameters considering limit size and offset size.
     * @param recordTypeIds The list of Case Record Type Ids.
     * @param schoolIds The Ids of schools
     * @param academicYearStartDate The Academic Year Start Date.
     * @param academicYearEndDate The Academic Year End Date.
     * @param filterTerm The query filter term.
     * @param sortFieldName The query sort field name.
     * @param ascOrDesc The query sorting.
     * @param limitSize The query limit size.
     * @param offsetSize The query offset size.
     * @return A list of Case records associated with the provided parameters.
     * @throws An ArgumentNullException if recordTypeIds is null.
     * @throws An ArgumentNullException if academicYearStartDate is null.
     * @throws An ArgumentNullException if academicYearEndDate is null.
     */
    public List<Case> selectWithCustomLimitOffsetOrder(
            List<Id> recordTypeIds,
            List<Id> schoolIds,
            DateTime academicYearStartDate,
            DateTime academicYearEndDate,
            String filterTerm,
            String sortFieldName,
            String ascOrDesc,
            Integer limitSize,
            Integer offsetSize) {

        ArgumentNullException.throwIfNull(recordTypeIds, CASE_RECORD_TYPE_IDS);
        ArgumentNullException.throwIfNull(academicYearStartDate, ACADEMIC_YEAR_START_DATE);
        ArgumentNullException.throwIfNull(academicYearEndDate, ACADEMIC_YEAR_END_DATE);

        String condition = 'RecordTypeId IN :recordTypeIds '
            + ' AND CreatedDate >= :academicYearStartDate AND CreatedDate <= :academicYearEndDate ';

        // SFP-1700: If we receive school Ids, then filter by those as well. Otherwise we won't use the accountId filter.
        // This is important for parent messages in the school portal which are linked to the parent's account instead
        // of the school account.
        if (schoolIds != null) {
            condition = 'AccountId IN :schoolIds AND ' + condition;
        }

        if (String.isNotBlank(filterTerm)) {
            condition += ' AND (' + 
                '    (Subject LIKE \'%' + filterTerm + '%\') ' +
                ' OR (CreatedBy.Name LIKE \'%' + filterTerm + '%\') ' +
                ' OR (Applicant__r.First_Name__c LIKE \'%' + filterTerm + '%\') ' +
                ' OR (Applicant__r.Last_Name__c LIKE \'%' + filterTerm + '%\') ' +
                ' OR (Parent_A_Name__c LIKE \'%' + filterTerm + '%\') ' +
                ' OR (PFS__r.PFS_Number__c LIKE \'%' + filterTerm + '%\') ' +
                ' OR (CaseNumber LIKE \'%' + filterTerm + '%\') )' ;
        }

        String fieldsToSelectString = 
              'PFS__r.PFS_Number__c, '
            + 'CreatedBy.Name, '
            + 'CreatedBy.Contact.AccountId, '
            + 'LastModifiedBy.Name, '
            + 'Applicant__r.Name, '
            + 'Applicant__r.First_Name__c, '
            + 'Applicant__r.Last_Name__c,'
            + 'Applicant__r.PFS__c, '
            + 'Applicant__r.PFS__r.PFS_Number__c';

        String orderingString = String.isNotBlank(sortFieldName) ? (sortFieldName + ((ascOrDesc == 'ASC') ? ' ASC' : ' DESC')) : getOrderBy();

        String caseQuery = String.format('SELECT {0}, {1} FROM {2} WHERE {3} ORDER BY {4} LIMIT {5} {6}',
            new List<String> {
                    fieldsToSelectString,
                    getFieldListString(),
                    getSObjectName(),
                    condition,
                    orderingString,
                    (limitSize != null) ? String.valueOf(limitSize) : '1000',
                    (offsetSize != null) ? (' OFFSET ' + offsetSize) : ''
            });

        return Database.query(caseQuery);
    }

    /**
     * @description Select Case records by parameters not considering limit size and offset size.
     * @param recordTypeIds The list of Case Record Type Ids.
     * @param academicYearStartDate The Academic Year Start Date.
     * @param academicYearEndDate The Academic Year End Date.
     * @param filterTerm The query filter term.
     * @param sortFieldName The query sort field name.
     * @param ascOrDesc The query sorting.
     * @return A list of Case records associated with the provided parameters.
     * @throws An ArgumentNullException if recordTypeIds is null.
     * @throws An ArgumentNullException if academicYearStartDate is null.
     * @throws An ArgumentNullException if academicYearEndDate is null.
     */
    public List<Case> selectWithoutLimitAndOffset(
            List<Id> recordTypeIds,
            List<Id> schoolIds,
            DateTime academicYearStartDate,
            DateTime academicYearEndDate,
            String filterTerm,
            String sortFieldName,
            String ascOrDesc) {

        return selectWithCustomLimitOffsetOrder(recordTypeIds, schoolIds, academicYearStartDate, academicYearEndDate, filterTerm, sortFieldName, ascOrDesc, null, null);
    }

    private Schema.SObjectType getSObjectType() {
        return Case.getSObjectType();
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                Case.CaseNumber,
                Case.Subject,
                Case.Status,
                Case.CreatedDate,
                Case.LastModifiedDate,
                Case.LastModifiedById,
                Case.CreatedById,
                Case.ContactId,
                Case.PFS__c,
                Case.Applicant__c
        };
    }

    /**
     * @description Singleton property ensuring external sources do not
     *              instantiate this directly.
     */
    public static CaseSelector Instance {
        get {
            if (Instance == null) {
                Instance = new CaseSelector();
            }
            return Instance;
        }
        private set;
    }

    private CaseSelector() { }
}