@isTest
private class FamilyTotalIncomeCalculatorTest {
    @isTest static void testCalculateTotalIncome() {
        // create the academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        
        // create a test PFS record
        PFS__c pfs = EfcTestData.createTestPfs(academicYear.Id, true);
        
        // test calculating the Total Income
        Decimal totalIncome = FamilyTotalIncomeCalculator.calculateTotalIncome(pfs.Id);
        System.debug('TOTAL INCOME = ' + totalIncome);
        
        // verify the income was calculated 
        // (NOTE: just make sure we got a value -- the actual calculation is tested in EfcCalculatorTest)
        System.assertNotEquals(null, totalIncome); 
    }
}