@isTest
private class SchoolProfJudgementTest {

    private static User schoolPortalUser;
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    private static Academic_Year__c currentAcademicYear;
    private static Student_Folder__c studentFolder1;
    private static Applicant__c applicant1A;

    private static void createTestData() {
        Account family = AccountTestData.Instance.asFamily().create();
        Account school = AccountTestData.Instance.asSchool().create();
        insert new List<Account>{family, school};

        currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;

        SSS_Constants__c sssConst1 = TestUtils.createSSSConstants(currentAcademicYear.Id, false);
        Database.insert(new List<SSS_Constants__c>{sssConst1});

        Contact parentA = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(school.Id).create();
        Contact student1 = ContactTestData.Instance
            .asStudent()
            .forAccount(family.Id).create();
        insert new List<Contact> {schoolStaff, parentA, student1};

        System.runAs(thisUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);
        String pfsStatusUnpaid = 'Unpaid';
        String folderSource = 'PFS';

        System.runAs(schoolPortalUser){
            PFS__c pfs1 = PfsTestData.Instance
                .asSubmitted()
                .forPaymentStatus(pfsStatusUnpaid)
                .forParentA(parentA.Id)
                .forOriginalSubmissionDate(System.today().addDays(-3))
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<PFS__c> {pfs1});

            applicant1A = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student1.Id).create();
            Dml.WithoutSharing.insertObjects(new List<Applicant__c> {applicant1A});

            studentFolder1 = StudentFolderTestData.Instance
                .forFolderSource(folderSource)
                .forStudentId(student1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<Student_Folder__c> {studentFolder1});

            String fifthGrade = '5';
            School_PFS_Assignment__c spfsa1 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1A.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> {spfsa1});
        }

        // Share records to school staff
        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);
    }

    @isTest
    private static void testBatchProcessing() {
        System.assertEquals(1000, SchoolProfJudgement.getEFCBatchSize());

        // Prevent this logic from running on initial insert so we can monitor
        // chunking records more effectively
        SchoolProfJudgement.isCalculatingEFC = true;
        Integer iterations = 100;

        createTestData();

        List<Account> testAccounts = new List<Account>();
        for (Integer i=0; i<iterations; i++) {
            testAccounts.add(TestUtils.createAccount('testschool' + i, RecordTypes.schoolAccountTypeId, 5, false));
        }

        Test.startTest();
            insert testAccounts;

            // Create 100 SPA records to test chunking
            List<School_PFS_Assignment__c> newSPARecords = new List<School_PFS_Assignment__c>{};
            for(Integer i=0; i<iterations; i++) {
                newSPARecords.add(TestUtils.createSchoolPFSAssignment(
                    currentAcademicYear.Id, applicant1A.Id, testAccounts[i].Id, studentFolder1.Id, false));
            }

            Database.insert(newSPARecords);

            // Turn the trigger logic back on for the test
            SchoolProfJudgement.isCalculatingEFC = false;

            List<String> processingResults = SchoolProfJudgement.runEFCRecalculations(newSPARecords);
            System.assertEquals(1, processingResults.size());

            Batch_Size_Settings__c newBatchSize = new Batch_Size_Settings__c(Name='EFC', Batch_Size__c = 20);
            Database.insert(newBatchSize);

            // Check that when a chunk size is specified it processes
            //  the records in chucks that are the specified size
            System.assertEquals(20, SchoolProfJudgement.getEFCBatchSize());

            processingResults = SchoolProfJudgement.runEFCRecalculations(newSPARecords);
            System.assertEquals(5, processingResults.size());
        Test.stopTest();
    }
}