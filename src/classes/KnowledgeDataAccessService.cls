/**
 * KnowledgeDataAccessService.cls
 *
 * @description Data access service class for Knowledge
 *
 * @author Mike Havrilla @ Presence PG
 */

public class KnowledgeDataAccessService extends DataAccessService {
    
    /**
    * @description: Queries Knowledge__kav (knowledge article version) by number of articles with the default visibility for that user. 
    * @param numberOfArticles to return
    * @return A List of Knowledge__kav records
    */
    public List<Knowledge__kav> getTopFamilyKnowledgeArticles( Integer numOfArticles) {
        return getTopFamilyKnowledgeArticles(numOfArticles, false, false);
    }

    public List<Knowledge__kav> getTopFamilyKnowledgeArticles( Integer numOfArticles, Boolean shouldFilterDocuments) {
        return getTopFamilyKnowledgeArticles(numOfArticles, shouldFilterDocuments, false);
    }

    public List<Knowledge__kav> getTopFamilyKnowledgeArticlesFaq( Integer numOfArticles) {
        return getTopFamilyKnowledgeArticles(numOfArticles, false, true);
    }

    /**
    * @description: Queries Knowledge__kav (knowledge article version)by number of articles
    * includes the ability to filter by just Documents.. 
    * @param numberOfArticles to return
    * @param shouldFilterDocumets filters by just documents as the Audience. 
    * @return A List of Knowledge__kav records
    */
    public List<Knowledge__kav> getTopFamilyKnowledgeArticles( Integer numOfArticles, Boolean shouldFilterDocuments, Boolean shouldFilterFaq) {
        List<Knowledge__kav> returnVal;

        Set<Integer> articleSequence = new Set<Integer>();

        for( Integer i = 0; i < numOfArticles; i++) {
            articleSequence.add(i);
        }

        // Build out the query. 
        String query = 'select ' + String.join( super.getSObjectFieldNames( 'Knowledge__kav'), ',') +
                        ' from Knowledge__kav' + 
                       ' where PublishStatus = \'Online\'' +
                       '   and Language = \'en_US\' ' + 
                       '   and Sequence__c in :articleSequence';

        if( shouldFilterDocuments) {
            query += ' with data category Audience__c at (Documents__c)';
        }

        if( shouldFilterFaq) {
            query += ' with data category Audience__c below (Pre_Payment_FAQ__c)';
        }

        query += ' order by Sequence__c asc' +  
                    ' limit :numOfArticles';
        try {
            returnVal = Database.query( query);
        } catch ( Exception e) {

            // log it and bubble to caller for handling
            System.debug( 'DEBUG:::exception in KnowledgeDataAccessService.getTopTenKnowledgeArticles' + e.getMessage());
            throw new DataAccessServiceException( e);
        }

        return returnVal;
    }


    /**
    * @description: Queries Knowledge__kav (knowledge article version)by number of articles by category
    * @param categoryName
    * @return A List of Knowledge__kav records
    */
    public Integer getKnowledgeArticleByCategoryCount( String categoryName) {
        Integer returnVal;

        // Build out the query. Don't Fret, Data Categories do not like bind variables, so we have to do dynamic soql here. 
        // Hopefully this can be refactored in the future. 
        String query = 'select count()' + 
                        ' from Knowledge__kav' + 
                       ' where PublishStatus = \'Online\'' +
                       '   and Language = \'en_US\' ' + 
          ' with data category Audience__c at ( ' + categoryName + ' ) ';

        try {
            returnVal = Database.countQuery( query);
        } catch ( Exception e) {

            // log it and bubble to caller for handling
            System.debug( 'DEBUG:::exception in KnowledgeDataAccessService.getKnowledgeArticleByCategory: ' + e.getMessage());
            throw new DataAccessServiceException( e);
        }

        return returnVal;
    }

    /**
    * @description: Queries Knowledge__kav (knowledge article version)by number of articles by category
    * SFDC QUIRK: using offset 
    * @param categoryName
    * @return A List of Knowledge__kav records
    */
    public List<Knowledge__kav> getKnowledgeArticleByCategory( String categoryName, Integer offset, Integer limitAmount) {
        List<Knowledge__kav> returnVal;

        // Build out the query. Don't Fret, Data Categories do not like bind variables, so we have to do dynamic soql here. 
        // Hopefully this can be refactored in the future. 
        String fields = 'Id, Title, KnowledgeArticleId, Article_body__c, Summary, LastModifiedDate, LastPublishedDate ';

        String query = 'select ' + fields +
                        ' from Knowledge__kav' + 
                       ' where PublishStatus = \'Online\'' +
                       '   and Language = \'en_US\' ' + 
          ' with data category Audience__c at ( ' + categoryName + ' ) ' +
                    ' order by LastModifiedDate desc' + 
                       ' limit :limitAmount' + 
                       ' offset :offset';

        try {

            returnVal = Database.query( query);

        } catch ( Exception e) {
            // log it and bubble to caller for handling
            System.debug( 'DEBUG:::exception in KnowledgeDataAccessService.getKnowledgeArticleByCategory: ' + e.getMessage());
            throw new DataAccessServiceException( e);
        }

        return returnVal;
    }

    /**
    * @description: Queries Knowledge__kav (knowledge article version)by number of articles
    * includes the ability to filter by just Documents.. 
    * @param knowledgeArticle id to query
    * @return A single knowledgeArticleById
    */
    public Knowledge__kav getKnowledgeArticleByArticleId( Id articleId) {
        Knowledge__kav returnVal;

        // Build out the query. 
        String query = 'select ' + String.join( super.getSObjectFieldNames( 'Knowledge__kav'), ',') +
                        ' from Knowledge__kav' + 
                       ' where PublishStatus = \'Online\'' +
                       '   and Language = \'en_US\' ' + 
                       '   and KnowledgeArticleId = :articleId';
        try {
            returnVal = Database.query( query);

        } catch ( Exception e) {
            // log it and bubble to caller for handling
            System.debug( 'DEBUG:::exception in KnowledgeDataAccessService.getKnowledgeArticleByArticleId' + e.getMessage());
            throw new DataAccessServiceException( e);
        }

        return returnVal;
    }

    /**
    * @description: Queries Knowledge__kav (knowledge article version) by keyword
    * @param keyword
    * @param offset
    * @param limitAmount 
    * @return searchResults a lite of Search
    */
    public Search.SearchResults searchKnowledgeArticlesByKeyword( String keyword, Integer offset, Integer limitAmount) {
        Search.SearchResults searchResults;
        String query = 'find :keyword ' + 
              'in all fields ' + 
                  'returning Knowledge__kav( Id, KnowledgeArticleId, Title, Summary, LastPublishedDate, Article_body__c ' +
                      'where PublishStatus = \'Online\' AND Language = \'en_US\' ' +
                      'limit :limitAmount ' +
                     'offset :offset )' +
        ' with data category Audience__c below (Pre_Payment_FAQ__c) ' +
               'with snippet (target_length = 150) '+ 
               'UPDATE TRACKING, VIEWSTAT ';

        try {
            searchResults = Search.find( query);

        } catch ( Exception e) {
            // log it and bubble to caller for handling
            System.debug( 'DEBUG:::exception in KnowledgeDataAccessService.searchKnowledgeArticlesByKeyword: ' + e.getMessage());
            throw new DataAccessServiceException( e);
        }

        return searchResults;
    }

    /**
    * @description: Queries Knowledge__kav (knowledge article version) by keyword
    * @param keyword
    * @return searchResults count
    */
    public Integer getKnowledgeArticleByKeywordCount( String keyword) {
        Search.SearchResults searchResults;
        String query = 'find :keyword ' + 
              'in all fields ' + 
                  'returning Knowledge__kav( Id, KnowledgeArticleId, Title, Summary, LastPublishedDate, Article_body__c ' +
                      'where PublishStatus = \'Online\' AND Language = \'en_US\') '+
        ' with data category Audience__c below (Pre_Payment_FAQ__c) ' +
               'with snippet (target_length = 150) ';

        try {
            searchResults = Search.find( query);

        } catch ( Exception e) {
            // log it and bubble to caller for handling
            System.debug( 'DEBUG:::exception in KnowledgeDataAccessService.getKnowledgeArticleByKeywordCount: ' + e.getMessage());
            throw new DataAccessServiceException( e);
        }

        return searchResults.get( 'Knowledge__kav').size();
    }

    /**
    * @description: Queries Knowledge__kav (knowledge article version) by keyword
    * @param numberOfArticles to return
    * @param shouldFilterDocumets filters by just post payment faq as the Audience.
    * @param shouldFilterFaq filters by just pre payment faq as the Audience.
    * @return KnowledgeWrapper List of items with abbreviated text. 
    */
    public List<KnowledgeWrapper> getTopFamilyKnowledgeArticlesAbbreviated( Integer numOfArticles, Boolean shouldFilterDocuments, Boolean shouldFilterFaq) {
        List<KnowledgeWrapper> knowledgeWrappers = new List<KnowledgeWrapper>();
        List<Knowledge__kav> knowledgeArticles = getTopFamilyKnowledgeArticles(numOfArticles,  shouldFilterDocuments, shouldFilterFaq);

        for( Knowledge__kav knowledgeArticle : knowledgeArticles) {
            knowledgeWrappers.add( new KnowledgeWrapper( knowledgeArticle));
        }

        return knowledgeWrappers;
    }

    public class KnowledgeWrapper {
        public Knowledge__kav knowledgeArticle { get; set; }
        public String abbreviatedBody { get; set; }
        public Boolean hasAbbreviatedBody { get; set; }

        public KnowledgeWrapper( Knowledge__kav knowledgeArticle) {
            this.knowledgeArticle = knowledgeArticle;
            abbreviatedBody = getAbbreviatedArticleBody( knowledgeArticle.Article_Body__c, 200 );
            hasAbbreviatedBody = knowledgeArticle.Article_Body__c.length() >= 200 ? true : true; // defaulting to true for now.
        }

        public String getAbbreviatedArticleBody( String articleBody, Integer numberOfChars) {
            // set response
            String returnVal = articleBody;

            if( returnVal != null) {

                // Strip out all html tags
                returnVal = articleBody.stripHtmlTags();

                // Check the length and abbrivate if necessary
                if( returnVal.length() >= numberOfChars) {
                    // next get the substring of based on the number of characters supplied. 
                    String abbreviatedString = returnVal.subString( 0, numberOfChars);
                    
                    // incase we finished right in the middle of a word, split at the last space
                    // If there are no more spaces after the index then just return the whole body.
                    Integer lastSpaceIndex = abbreviatedString.lastIndexOf(' ');
                    if( lastSpaceIndex >= 0 ) {
                        abbreviatedString = abbreviatedString.substring( 0, lastSpaceIndex);
                    } else {
                        abbreviatedString = returnVal;
                    }
                    
                    returnVal = abbreviatedString + '...';
                }
            }
            return returnVal;
        }
    }
}