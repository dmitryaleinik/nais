/**
 * @description This class is used to create Field Count records for unit tests.
 */
@isTest
public class FieldCountTestData extends SObjectTestData
{

    /**
     * @description Get the default values for the Field_Count__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() 
    {
        return new Map<Schema.SObjectField,Object> 
        {
            Field_Count__c.Academic_Year__c => AcademicYearTestData.Instance.DefaultAcademicYear.Id,
            Field_Count__c.School__c => AccountTestData.Instance.asSchool().DefaultAccount.Id
        };
    }

    /**
     * @description Insert the current working Field_Count__c record.
     * @return The currently operated upon Field_Count__c record.
     */
    public Field_Count__c insertFieldCount() 
    {
        return (Field_Count__c)insertRecord();
    }

    /**
     * @description Create the current working Field Count record without resetting
     *              the stored values in this instance of FieldCountTestData.
     * @return A non-inserted Field Count record using the currently stored field values.
     */
    public Field_Count__c createFieldCountWithoutReset() 
    {
        return (Field_Count__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Field_Count__c record.
     * @return The currently operated upon Field_Count__c record.
     */
    public Field_Count__c create() 
    {
        return (Field_Count__c)super.buildWithReset();
    }

    /**
     * @description The default Field_Count__c record.
     */
    public Field_Count__c DefaultFieldCount 
    {
        get 
        {
            if (DefaultFieldCount == null) 
            {
                DefaultFieldCount = createFieldCountWithoutReset();
                insert DefaultFieldCount;
            }
            return DefaultFieldCount;
        }
        private set;
    }

    /**
     * @description Get the Field_Count__c SObjectType.
     * @return The Field_Count__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() 
    {
        return Field_Count__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static FieldCountTestData Instance 
    {
        get 
        {
            if (Instance == null) 
            {
                Instance = new FieldCountTestData();
            }
            return Instance;
        }
        private set;
    }

    private FieldCountTestData() {}
}