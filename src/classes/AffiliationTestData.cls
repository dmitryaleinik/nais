/**
 * @description This class is used to create Affiliation records for unit tests.
 */
@isTest
public class AffiliationTestData extends SObjectTestData {
    @testVisible private static final String LAST_NAME = 'Contact Name';

    /**
     * @description Get the default values for the Affiliation__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Affiliation__c.Contact__c => ContactTestData.Instance.DefaultContact.Id,
                Affiliation__c.Organization__c => AccountTestData.Instance.DefaultAccount.Id
        };
    }

    /**
     * @description Set the Contact__c field on the current Affiliation record.
     * @param contactId The Contact Id to set on the Affiliation.
     * @return The current working instance of AffiliationTestData.
     */
    public AffiliationTestData forContactId(Id contactId) {
        return (AffiliationTestData) with(Affiliation__c.Contact__c, contactId);
    }

    /**
     * @description Set the Organization__c field on the current Affiliation record.
     * @param accountId The Organization Id to set on the Affiliation.
     * @return The current working instance of AffiliationTestData.
     */
    public AffiliationTestData forOrganizationId(Id accountId) {
        return (AffiliationTestData) with(Affiliation__c.Organization__c, accountId);
    }

    /**
     * @description Set the Type__c field on the current Affiliation record.
     * @param affType The Type to set on the Affiliation.
     * @return The current working instance of AffiliationTestData.
     */
    public AffiliationTestData forType(String affType) {
        return (AffiliationTestData) with(Affiliation__c.Type__c, affType);
    }

    /**
     * @description Set the Status__c field on the current Affiliation record.
     * @param status The Status to set on the Affiliation.
     * @return The current working instance of AffiliationTestData.
     */
    public AffiliationTestData forStatus(String status) {
        return (AffiliationTestData) with(Affiliation__c.Status__c, status);
    }

    /**
     * @description Set the Role__c field on the current Affiliation record.
     * @param role The Role to set on the Affiliation.
     * @return The current working instance of AffiliationTestData.
     */
    public AffiliationTestData forRole(String role) {
        return (AffiliationTestData) with(Affiliation__c.Role__c, role);
    }

    /**
     * @description Insert the current working Affiliation__c record.
     * @return The currently operated upon Affiliation__c record.
     */
    public Affiliation__c insertAffiliation() {
        return (Affiliation__c)insertRecord();
    }

    /**
     * @description Create the current working Affiliation record without resetting
     *             the stored values in this instance of AffiliationTestData.
     * @return A non-inserted Affiliation__c record using the currently stored field
     *             values.
     */
    public Affiliation__c createAffiliationWithoutReset() {
        return (Affiliation__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Affiliation__c record.
     * @return The currently operated upon Affiliation__c record.
     */
    public Affiliation__c create() {
        return (Affiliation__c)super.buildWithReset();
    }

    /**
     * @description The default Affiliation__c record.
     */
    public Affiliation__c DefaultAffiliation {
        get {
            if (DefaultAffiliation == null) {
                DefaultAffiliation = createAffiliationWithoutReset();
                insert DefaultAffiliation;
            }
            return DefaultAffiliation;
        }
        private set;
    }

    /**
     * @description Get the Affiliation__c SObjectType.
     * @return The Affiliation__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Affiliation__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static AffiliationTestData Instance {
        get {
            if (Instance == null) {
                Instance = new AffiliationTestData();
            }
            return Instance;
        }
        private set;
    }

    private AffiliationTestData() { }
}