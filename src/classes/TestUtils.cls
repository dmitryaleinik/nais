/*
 * Utility methods for use in tests
 *  create<Object>  - return an sObject; insert object if last argument 'insertData' is true
 */
@isTest
public class TestUtils {

    public static Account createAccount(String name, Id recordTypeId, Integer maxUsers, Boolean insertData) {
        Account a = new Account();
        a.Name = name;
        a.RecordTypeId = recordTypeId;
        a.Max_Number_of_Users__c = maxUsers;

        if (insertData) insert a;
        return a;
    }

    public static Contact createContact(String lastName, Id accountId, Id recordTypeId, Boolean insertData) {
        return createContact(null, lastName, accountId, recordTypeId, insertData);
    }

    public static Contact createContact(String firstName, String lastName, Id accountId, Id recordTypeId, Boolean insertData) {
        Contact c = new Contact();
        if(firstName!=null) c.FirstName = firstName;
        c.LastName = lastName;
        c.AccountId = accountId;
        c.RecordTypeId = recordTypeId;
        c.Birthdate = System.today().addYears(-10);

        if (insertData) insert c;
        return c;
    }

    public static User createPortalUser(String name, String email, String alias, Id contactId, Id profileId, Boolean isActive, Boolean insertData) {
        User u = new User();
        u.LastName = name;
        u.Username = email;
        u.Email = email;
        u.Alias = alias;
        u.TimeZoneSidKey = 'America/New_York';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.ProfileId = profileId;
        u.LanguageLocaleKey = 'en_US';
        u.ContactId = contactId;
        u.isActive = isActive;

        if (insertData) insert u;
        return u;
    }

    public static User createPlatformUser(String name, String email, String alias, Id profileId, Boolean isActive, Boolean insertData) {
        User u = new User();
        u.LastName = name;
        u.Username = email;
        u.Email = email;
        u.Alias = alias;
        u.TimeZoneSidKey = 'America/New_York';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.ProfileId = profileId;
        u.LanguageLocaleKey = 'en_US';
        u.isActive = isActive;

        if (insertData) insert u;
        return u;
    }
    
    public static List<Academic_Year__c> createAcademicYears() {

        List<Academic_Year__c> years = new List<Academic_Year__c>();
        Integer currentYear = Date.Today().year();

        years.add(new Academic_Year__c(
                Start_Date__c = date.newInstance(currentYear, 10, 1),
                Family_Portal_Start_Date__c = date.newInstance(currentYear, 10, 1),
                End_Date__c = date.newInstance(currentYear+1, 9, 30),
                Family_Portal_End_Date__c = date.newInstance(currentYear+1, 9, 30),
                Name = String.ValueOf(currentYear+1)+'-'+String.ValueOf(currentYear+2)));
        years.add(new Academic_Year__c(
                Start_Date__c = date.newInstance(currentYear-1, 10, 1),
                Family_Portal_Start_Date__c = date.newInstance(currentYear-1, 10, 1),
                End_Date__c = date.newInstance(currentYear, 9, 30),
                Family_Portal_End_Date__c = date.newInstance(currentYear, 9, 30),
                Name = String.ValueOf(currentYear)+'-'+String.ValueOf(currentYear+1)));
        years.add(new Academic_Year__c(
                Start_Date__c = date.newInstance(currentYear-2, 10, 1),
                Family_Portal_Start_Date__c = date.newInstance(currentYear-2, 10, 1),
                End_Date__c = date.newInstance(currentYear-1, 9, 30),
                Family_Portal_End_Date__c = date.newInstance(currentYear-1, 9, 30),
                Name = String.ValueOf(currentYear-1)+'-'+String.ValueOf(currentYear)));
        years.add(new Academic_Year__c(
                Start_Date__c = date.newInstance(currentYear-3, 10, 1),
                Family_Portal_Start_Date__c = date.newInstance(currentYear-3, 10, 1),
                End_Date__c = date.newInstance(currentYear-2, 9, 30),
                Family_Portal_End_Date__c = date.newInstance(currentYear-2, 9, 30),
                Name = String.ValueOf(currentYear-2)+'-'+String.ValueOf(currentYear-1)));
        years.add(new Academic_Year__c(
                Start_Date__c = date.newInstance(currentYear-4, 10, 1),
                Family_Portal_Start_Date__c = date.newInstance(currentYear-4, 10, 1),
                End_Date__c = date.newInstance(currentYear-3, 9, 30),
                Family_Portal_End_Date__c = date.newInstance(currentYear-3, 9, 30),
                Name = String.ValueOf(currentYear-3)+'-'+String.ValueOf(currentYear-2)));
        years.add(new Academic_Year__c(
                Start_Date__c = date.newInstance(currentYear-5, 10, 1),
                Family_Portal_Start_Date__c = date.newInstance(currentYear-5, 10, 1),
                End_Date__c = date.newInstance(currentYear-4, 9, 30),
                Family_Portal_End_Date__c = date.newInstance(currentYear-4, 9, 30),
                Name = String.ValueOf(currentYear-4)+'-'+String.ValueOf(currentYear-3)));

        insert years;

        return years;
    }

    /**
     * @description Inserts the specified number of academic years. The start date of the first record will be for
     *              October 1st of last year. The end date of the first record will be September 30th of this year.
     *              Every record after that will simply have the start and end dates 1 year before the previous record.
     * @param numberToInsert The number of records to insert.
     * @return A list of academic year records.
     */
    public static List<Academic_Year__c> insertAcademicYears(Integer numberToInsert) {
        List<Academic_Year__c> recordsToInsert = new List<Academic_Year__c>();

        Integer currentYear = Date.Today().year();

        Integer startYearOffset = 1;
        Integer endYearOffset = 0;

        Integer startYearNameOffset = 0;
        Integer endYearNameOffset = -1;
        for (Integer i = 0; i < numberToInsert; i++) {
            Integer startYear = currentYear - startYearOffset;
            Integer endYear = currentYear - endYearOffset;

            String startYearName = String.valueOf(currentYear - startYearNameOffset);
            String endYearName = String.valueOf(currentYear - endYearNameOffset);

            recordsToInsert.add(new Academic_Year__c(
                    Start_Date__c = date.newInstance(startYear, 10, 1),
                    Family_Portal_Start_Date__c = date.newInstance(startYear, 10, 1),
                    End_Date__c = date.newInstance(endYear, 9, 30),
                    Family_Portal_End_Date__c = date.newInstance(endYear, 9, 30),
                    Name = startYearName + '-' + endYearName));

            startYearOffset--;
            endYearOffset--;
            startYearNameOffset--;
            endYearNameOffset--;
        }

        insert recordsToInsert;

        return recordsToInsert;
    }

    public static SSS_Constants__c createSSSConstants(Id academicYearId, Boolean insertRecord){
        SSS_Constants__c newSSSConstants = new SSS_Constants__c(
            Academic_Year__c = academicYearId,
            Exemption_Allowance__c = 5000,
            Std_Deduction_Joint_Surviving__c = 12000,
            Std_Deduction_Head_of_Household__c = 11000,
            Std_Deduction_Single_Filing_Separately__c = 10000,
            // Employment_Allowance_Maximum__c = 20000,
            Employment_Allowance_Maximum__c = 5430,  // SL: setting this to a more realistic number
            Medicare_Tax_Rate__c = 0.10,
            Medical_Dental_Allowance_Percent__c = 0.05,
            Percentage_for_Imputing_Assets__c = 0.2,
            Default_COLA_Value__c = 0.85,
            Negative_Contribution_Cap_Constant__c = 200,
            Boarding_School_Food_Allowance__c = 400,
            Social_Security_Tax_Rate__c = 0.042,
            Social_Security_Tax_Threshold__c = 110100,
            IPA_For_Each_Additional__c = 6116,
            IPA_Housing_For_Each_Additional__c = 2134, // [CH] NAIS-1885
            IPA_Other_For_Each_Additional__c = 4312, // [CH] NAIS-1885
            IPA_Housing_Multiplier_for_Additional__c = 0.18 // [CH] NAIS-1885

            // [SL 5/3/13] FICA no longer used
            //FICA_Tax_Wage_Earner_Base_Rate__c = 0.25,
            //FICA_Tax_Wage_Earner_Top_Rate__c = 0.25,
            //FICA_Tax_Self_Employment_Base_Rate__c = 0.25,
            //FICA_Tax_Self_Employment_Top_Rate__c = 0.25,
            //Full_FICA_Rate_Maximum__c = 0.25
        );

        if(insertRecord){
            Database.insert(newSSSConstants);
        }

        return newSSSConstants;
    }

    public static Federal_Tax_Table__c createFederalTaxTable(Id academicYearId, String filingStatus, Boolean insertRecord){
        Federal_Tax_Table__c newTaxTable = new Federal_Tax_Table__c(
            Academic_Year__c = academicYearId,
            Filing_Type__c = filingStatus,
            Taxable_Income_Low__c = 40000,
            US_Income_Tax_Base__c = 10000,
            US_Income_Tax_Rate__c = 0.25,
            US_Income_Tax_Rate_Threshold__c = 0.47
        );

        if(insertRecord){
            Database.insert(newTaxTable);
        }

        return newTaxTable;
    }

    public static State_Tax_Table__c createStateTaxTable(Id academicYearId, Boolean insertRecord){
        State_Tax_Table__c newTaxTable = new State_Tax_Table__c();
        newTaxTable.Academic_Year__c = academicYearId;
        newTaxTable.Income_Low__c = 20000;
        newTaxTable.State__c = 'Colorado';
        newTaxTable.Percent_of_Total_Income__c = 0.25;

        if(insertRecord){
            Database.insert(newTaxTable);
        }

        return newTaxTable;
    }

    public static Employment_Allowance__c createEmploymentAllowance(Id academicYearId, Boolean insertRecord){
        Employment_Allowance__c empAllow = new Employment_Allowance__c();
        empAllow.Academic_Year__c = academicYearId;
        empAllow.Income_Low__c = 10000;
        empAllow.Income_High__c = 200000;
        empAllow.Allowance_Base_Amount__c = 1000;
        empAllow.Allowance_Threshold__c = 10000;
        empAllow.Allowance_Rate__c = 0.10;

        if(insertRecord){
            Database.insert(empAllow);
        }

        return empAllow;

    }

    public static Net_Worth_of_Business_Farm__c createBusinessFarm(Id academicYearId, Boolean insertRecord){
        Net_Worth_of_Business_Farm__c busFarm = new Net_Worth_of_Business_Farm__c();
        busFarm.Academic_Year__c = academicYearId;
        busFarm.Expected_Contribution_Base__c = 1000;
        busFarm.Expected_Contribution_Rate__c = 0.20;
        busFarm.Net_Worth_of_Business_Low__c = 0;
        busFarm.Net_Worth_of_Business_High__c = 200000;
        busFarm.Threshold_to_Apply_Rate__c = 50000;

        if(insertRecord){
            Database.insert(busFarm);
        }

        return busFarm;
    }

    public static Academic_Year__c createAcademicYear(String name, Boolean insertData) {
        //first make sure one doesn't already exist
        Academic_Year__c ayr;

        if (insertData) //don't look for an existing record if the call isn't to insert, or else it will return something with an ID in it
        {
            ayr = GlobalVariables.getAcademicYearByName(name);
            if (ayr != null)
            {
                return ayr;
            }
        }
        ayr = new Academic_Year__c();
        ayr.Name = name;
        String[] years = name.split('-');
        ayr.Start_Date__c = date.newInstance(Integer.valueOf(years[0])-1, 10, 1);
        ayr.Family_Portal_Start_Date__c = date.newInstance(Integer.valueOf(years[0])-1, 10, 1);
        ayr.End_Date__c = date.newInstance(Integer.valueOf(years[0]), 9, 30);
        ayr.Family_Portal_End_Date__c = date.newInstance(Integer.valueOf(years[0]), 9, 30);
        if (insertData) {insert ayr;}
        return ayr;
    }

    public static Unusual_Conditions__c createUnusualConditions(Id academicYearId, Boolean insertData) {

        Unusual_Conditions__c testUC = new Unusual_Conditions__c();

        testUC.Academic_Year__c = academicYearId;
        testUC.High_Unusual_Expenses_Variable__c = 15;
        testUC.High_Medical_Expense_Variable__c = 5;
        testUC.Home_Equity_Capped_Variable__c = 3;
        testUC.Student_Assets_Variable__c = 2000;
        testUC.Large_Income_Change_Variable__c = 15;
        testUC.High_Income_Adjustments_Variable__c = 25;
        testUC.Low_Dividends_Interest_Variable_1__c = 5000;
        testUC.Low_Dividends_Interest_Variable_2__c = 2;
        testUC.High_Dividends_Interest_Variable_1__c = 250;
        testUC.High_Dividends_Interest_Variable_2__c = 2;
        testUC.No_Income_Tax_Variable__c = 20000;
        testUC.Income_Tax_Low_Variable__c = 80;
        testUC.Income_Tax_High_Variable__c = 20;

        if (insertData) insert testUC;
        return testUC;
    }

    // NAIS-2417 [DP] 05.19.2015 overload to handle new picklist field
    public static PFS__c createPFS(String name, Id academicYearId, Id parentId, Boolean insertData) {
        String acadYearName = null;
        //System.Debug('249 ayID' + academicYearId);
        Academic_Year__c ay = GlobalVariables.getAcademicYear(academicYearId);
        //System.Debug('251 ay' + ay);
        if (ay != null){
            acadYearName = ay.Name;
        }

        return createPFS(name, acadYearName, parentId, insertData);
    }


    public static PFS__c createPFS(String name, String academicYearName, Id parentId, Boolean insertData) {
        PFS__c pfs = new PFS__c();
        pfs.Name = name;
        pfs.Academic_Year_Picklist__c = academicYearName;
        pfs.Parent_A__c = parentId;
        pfs.Filing_Status__c = 'Single';
        pfs.Salary_Wages_Parent_A__c = 160000; // [DP] 04.06.2015 NAIS-2383 changing from "60000" to "160000", which is the value in expbox and prod
        pfs.Salary_Wages_Parent_B__c = 40000;
        pfs.Parent_A_State__c = 'Colorado';
        pfs.Family_Size__c = 4;
        pfs.Num_Children_in_Tuition_Schools__c = 2;
        pfs.Number_Cars_Boats_RVs_Owned__c = '2';
        pfs.Student_Has_Parent_B__c = 'No';
        pfs.Other_Parent__c = 'No';

        if (insertData) insert pfs;
        return pfs;
    }

    // [CH] NAIS-1866 Adding to allow keeping the original Contact name after Applicant insert
    public static Applicant__c createApplicant(Contact student, Id pfsId, Boolean insertData) {

        Applicant__c app = new Applicant__c();
        app.Contact__c = student.Id;
        app.PFS__c = pfsId;
        app.Grade_in_Entry_Year__c = '6';
        app.First_Name__c = student.FirstName;
        app.Last_Name__c = student.LastName;
        app.Birthdate_new__c = student.Birthdate;
        app.Grade_in_Entry_Year__c = '6';
        app.SSN__c = '9999'; // [DP] NAIS-1806 now 4 digit SSN

        if (insertData) insert app;
        return app;
    }

    public static Applicant__c createApplicant(Id studentId, Id pfsId, Boolean insertData) {

        Applicant__c app = new Applicant__c();
        app.Contact__c = studentId;
        app.PFS__c = pfsId;
        app.Grade_in_Entry_Year__c = '6';
        app.First_Name__c = 'Test';
        app.Last_Name__c = 'Applicant';
        app.Grade_in_Entry_Year__c = '6';
        app.SSN__c = '9999'; // [DP] NAIS-1806 now 4 digit SSN

        if (insertData) insert app;
        return app;
    }

    // NAIS-2417 [DP] 05.13.2015 overloading this method to account for removal of picklist
    public static Student_Folder__c createStudentFolder(String name, Id academicYearId, Id studentId, Boolean insertData) {
        String acadYearName = null;

        Academic_Year__c ay = GlobalVariables.getAcademicYear(academicYearId);
        if (ay != null){
            acadYearName = ay.Name;
        }

        return createStudentFolder(name, acadYearName, studentId, insertData);
    }

    public static Student_Folder__c createStudentFolder(String name, String academicYearName, Id studentId, Boolean insertData) {

        Student_Folder__c sf = new Student_Folder__c();
        sf.Name = name;
        sf.Academic_Year_Picklist__c = academicYearName;
        sf.Student__c = studentId;
        sf.Day_Boarding__c = 'Day'; // NAIS-2252 [DP] 02.05.2015
        sf.Folder_Source__c = 'PFS';

        if (insertData) insert sf;
        return sf;
    }

    public static School_PFS_Assignment__c createSchoolPFSAssignment(Id academicYearId, Id applicantId, Id schoolId, Id studentFolderId, Boolean insertData) {
        String acadYearName = null;

        Academic_Year__c ay = GlobalVariables.getAcademicYear(academicYearId);
        if (ay != null){
            acadYearName = ay.Name;
        }

        return createSchoolPFSAssignment(acadYearName, applicantId, schoolId, studentFolderId, insertData); // NAIS-2417 [DP] 05.19.2015 will uncomment this once I hand SPA.Academic_Year_Picklist__c
    }


    public static School_PFS_Assignment__c createSchoolPFSAssignment(String acadYearName, Id applicantId, Id schoolId, Id studentFolderId, Boolean insertData) {
        School_PFS_Assignment__c spfsa = new School_PFS_Assignment__c();
        spfsa.Academic_Year_Picklist__c = acadYearName;
        spfsa.Applicant__c = applicantId;
        spfsa.School__c = schoolId;
        spfsa.Student_Folder__c = studentFolderId;
        spfsa.Filing_Status__c = 'Single';
        spfsa.Salary_Wages_Parent_A__c = 60000;
        spfsa.Salary_Wages_Parent_B__c = 40000;
        spfsa.Parent_State__c = 'Colorado';
        spfsa.Impute__c = 0.70;
       // spfsa.Family_Status__c = '1 Parent';
        spfsa.Orig_Family_Status__c = '1 Parent';
        spfsa.Family_Size__c = 4;
        spfsa.Num_Children_in_Tuition_Schools__c = 2;
        spfsa.Grade_Applying__c = '5';
        //spfsa.Day_Boarding__c = 'Day'; // NAIS-2252 [DP] 02.05.2015


        if (insertData) insert spfsa;
            return spfsa;
    }

    public static Retirement_Allowance__c createRetirementAllowance(Id academicYearId, Boolean insertData) {
        Retirement_Allowance__c retAllow = new Retirement_Allowance__c();
        retAllow.Academic_Year__c = academicYearId;
        retAllow.Age_Low__c = 20;
        retAllow.Allowance_One_Parent_Family__c = 1000;
        retAllow.Conversion_Coefficient_One_Parent_Fam__c = 0.2;
        retAllow.Allowance_Two_Parent_Family__c = 2000;
        retAllow.Conversion_Coefficient_Two_Parent_Fam__c = 0.2;
        retAllow.Age_High__c = 100;

        if (insertData) insert retAllow;
            return retAllow;
    }

    public static Income_Protection_Allowance__c createIncomeProtectionAllowance(Id academicYearId, Boolean insertData) {
        Income_Protection_Allowance__c incPro = new Income_Protection_Allowance__c();
        incPro.Academic_Year__c = academicYearId;
        incPro.Family_Size__c = 4;
        incPro.Allowance__c = 4000;
        incPro.Housing_Allowance__c=1000;
        incPro.Other_Allowance__c=3000;
        incPro.Family_Size_Multiplier__c=1.0;

        if (insertData) insert incPro;
            return incPro;
    }

    public static Asset_Progressivity__c createAssetProgressivity(Id academicYearId, Boolean insertData) {
        Asset_Progressivity__c assetProg = new Asset_Progressivity__c();
        assetProg.Academic_Year__c = academicYearId;
        assetProg.Discretionary_Net_Worth_Low__c = 5000;
        assetProg.Index__c = 5.1;

        if (insertData) insert assetProg;
            return assetProg;
    }

    public static Expected_Contribution_Rate__c createExpectedContributionRate(Id academicYearId, Boolean insertData) {
        Expected_Contribution_Rate__c expContRate = new Expected_Contribution_Rate__c();
        expContRate.Academic_Year__c = academicYearId;
        //expContRate.Discretionary_Income_Low__c = 10000;
        expContRate.Discretionary_Income_Low__c = 1000;
        expContRate.Expected_Contribution__c = 1000;
        expContRate.Expected_Contribution_Percentage__c = 0.05;
        expContRate.Threshold__c = 999;

        if (insertData) insert expContRate;
            return expContRate;
    }

    public static Housing_Index_Multiplier__c createHousingIndexMultiplier(Id academicYearId, Boolean insertData) {
        Housing_Index_Multiplier__c houseIndex = new Housing_Index_Multiplier__c();
        houseIndex.Academic_Year__c = academicYearId;
        houseIndex.Home_Purchase_Year__c = '2004';
        houseIndex.Housing_Index_Multiplier__c = 0.25;

        if (insertData) insert houseIndex;
            return houseIndex;
    }

    public static Required_Document__c createRequiredDocument(Id academicYearId, Id schoolId, Boolean insertData) {
        Required_Document__c rd = new Required_Document__c();
        rd.Academic_Year__c = academicYearId;
        rd.School__c = schoolId;

        String currentTaxYear = GlobalVariables.getDocYearStringFromAcadYearName(GlobalVariables.getCurrentYearString());
        rd.Document_Year__c = currentTaxYear;

        if (insertData) insert rd;
        return rd;
    }

    public static Required_Document__c createRequiredDocument(Id academicYearId, Id schoolId, String documentType, String documentYear, Boolean insertData) {
        Required_Document__c rd = new Required_Document__c();
        rd.Academic_Year__c = academicYearId;
        rd.School__c = schoolId;
        rd.Document_Type__c = documentType;
        rd.Document_Year__c = documentYear;

        if (insertData) insert rd;
        return rd;
    }

    // NAIS-2383 [DP] 04.02.2015 overload for refactor to get name for picklist value
    public static Family_Document__c createFamilyDocument(String name, Id academicYearId, Boolean insertData) {
        return createFamilyDocument(name, academicYearId, null, null, insertData);
    }

    // NAIS-2383 [DP] 04.02.2015 overload for refactor to get name for picklist value
    public static Family_Document__c createFamilyDocument(String name, String academicYearName, Boolean insertData) {
        return createFamilyDocument(name, academicYearName, null, null, insertData);
    }

    // NAIS-2383 [DP] 04.02.2015 overload for refactor to get name for picklist value
    public static Family_Document__c createFamilyDocument(String name, Id academicYearId, String docType, Id householdId, Boolean insertData) {
        String acadYearName;
        Academic_Year__c ay = GlobalVariables.getAcademicYear(academicYearId);
        if (ay != null){
            acadYearName = ay.Name;
        }
        return createFamilyDocument(name, acadYearName, docType, householdId, insertData);
    }

    // NAIS-2383 [DP] 04.02.2015 overload for refactor to get name for picklist value
    public static Family_Document__c createFamilyDocument(String name, String academicYearName, String docType, Id householdId, Boolean insertData) {
        Family_Document__c fd = new Family_Document__c();
        fd.Name = name;
        fd.Academic_Year_Picklist__c = academicYearName;
        fd.Document_Type__c = docType;
        fd.Household__c = householdId;

        if (insertData) insert fd;
        return fd;
    }

    // SFP-640 [jB] Add additional fields to assignments for creating test family documents
    public static Family_Document__c createFamilyDocument(String name, String academicYearName, String docType, Id householdId, String documentPertainsTo, String importId, Boolean insertData) {
        Family_Document__c fd = new Family_Document__c();
        fd.Name = name;
        fd.Academic_Year_Picklist__c = academicYearName;
        fd.Document_Type__c = docType;
        fd.Household__c = householdId;
        fd.Import_Id__c = importId;
        fd.Document_Pertains_to__c = documentPertainsTo;
        fd.Document_Year__c = GlobalVariables.getDocYearStringFromAcadYearName(academicYearName);
        if (insertData) insert fd;
        return fd;
    }

    public static School_Document_Assignment__c createSchoolDocumentAssignment (String name, Id familyDocumentId, Id requiredDocumentId, Id schoolPFSAssignmentId, Boolean insertData) {
        School_Document_Assignment__c sda = new School_Document_Assignment__c();
        // CH 7/10/2013 - It looks like the name field for this object was changed to an
        //                   auto-number, which causes and error at this line
        //sda.Name = name;
        sda.Document__c = familyDocumentId;

        sda.Required_Document__c = requiredDocumentId;
        sda.School_PFS_Assignment__c = schoolPFSAssignmentId;

        String currentTaxYear = GlobalVariables.getDocYearStringFromAcadYearName(GlobalVariables.getCurrentYearString());
        sda.Document_Year__c = currentTaxYear;

        if (insertData) insert sda;
        return sda;
    }

    public static Household__c createHousehold (String name, Boolean insertData) {
        Household__c h = new Household__c();
        h.Name = name;

        if (insertData) insert h;
        return h;
    }

    public static Annual_Setting__c createAnnualSetting(String schoolId, String academicYearId, Boolean insertData) {
        Annual_Setting__c annSet = new Annual_Setting__c();
        annSet.School__c = schoolId;
        annSet.Academic_Year__c = academicYearId;

        if (insertData) insert annSet;
        return annSet;
    }

    public static Application_Fee_Waiver__c createApplicationFeeWaiver(String accountId, String contactId, String academicYearId, Boolean insertData) {
        Application_Fee_Waiver__c afw = new Application_Fee_Waiver__c();
        afw.Account__c = accountId;
        afw.Contact__c = contactId;
        afw.Academic_Year__c = academicYearId;

        if (insertData) insert afw;
        return afw;
    }

    public static Opportunity createOpportunity(String name, String recordTypeId, String pfsId, String academicYearName, Boolean insertData) {

         return createOpportunity(name,recordTypeId,pfsId,academicYearName,insertData, 'Closed');
    }

    public static Opportunity createOpportunity(String name, String recordTypeId, String pfsId, String academicYearName, Boolean insertData, string Stage) {
        Opportunity oppty = new Opportunity();
        oppty.Name = name;
        oppty.RecordTypeId = recordTypeId;
        oppty.PFS__c = pfsId;
        oppty.Academic_Year_Picklist__c = academicYearName;
        oppty.StageName = Stage;
        oppty.CloseDate = Date.today();

        if (insertData) insert oppty;
        return oppty;
    }
    public static Transaction_Line_Item__c createTransactionLineItem(String opportunityId, String recordTypeId, Boolean insertData) {
        Transaction_Line_Item__c tli = new Transaction_Line_Item__c();
        tli.Opportunity__c = opportunityId;
        tli.RecordTypeId = recordTypeId;
        tli.Transaction_Type__c = 'Application Fee';

        if (insertData) insert tli;
        return tli;
    }

    public static Affiliation__c createAffiliation(Id contactId, Id accountId, String Type, String status, Boolean insertData) {
        Affiliation__c aff = new Affiliation__c();
        aff.Contact__c = contactId;
        aff.Organization__c = accountId;
        aff.Type__c = Type;
        aff.Status__c = status;

        if (insertData) insert aff;
        return aff;
    }

    public static Verification__c createVerification(Boolean insertData) {
        Verification__c verification = new Verification__c();

        if (insertData) insert verification;
        return verification;
    }

    /**
     *  SFP-83 Create verification data with random values to make sure they're all copied
     */
    public static Verification__c createVerificationRandomData(Boolean insertData) {
        Verification__c verification = new Verification__c();
        Map<String, Schema.SObjectField> fieldByName = Schema.SObjectType.Verification__c.fields.getMap();
        for(String field : fieldByName.keySet()){
            if(field.contains('__c') && !field.contains('py_')
                && fieldByName.get(field).getDescribe().isUpdateable()
                && !ApplicationUtils.formulaFields.contains(field)
                && !ApplicationUtils.textFields.contains(field)){
                    verification.put(field,Math.random() * 1000);
            }
        }
        if (insertData) insert verification;
        return verification;
    }

    public static Help_Configuration__c createHelp(Id acadYearId, Boolean insertData){
        Help_Configuration__c help = new Help_Configuration__c();
        help.Language__c = 'en_US';
        help.Academic_Year__c = acadYearId;

        if (insertData) insert help;
        return help;
    }

    public static Subscription__c createSubscription(Id accountId, Date startDate, Date endDate, String cancel, String stype, Boolean insertData) {
        Subscription__c s = new Subscription__c();
        s.Account__c = accountId;
        s.Start_Date__c = startDate;
        s.End_Date__c = endDate;
        s.Cancelled__c = cancel;
        if(cancel == 'Yes'){
            s.Cancellation_Date__c = Date.today();
            s.Reason_Cancelled__c = 'Test Reason;';
            s.Charge_Cancellation_Fee__c = 'No';
        }
        s.Subscription_Type__c = stype;

        if (insertData) insert s;
        return s;
    }

    public static Business_Farm__c createBusinessFarm(Id pfsId, String name, String bizOrFarm, String entityType, Boolean insertData){
        Business_Farm__c bf = new Business_Farm__c();
        bf.PFS__c = pfsId;
        bf.Name = name;
        bf.Business_or_Farm__c = bizOrFarm;
        bf.Business_Entity_Type__c = entityType;

        if (insertData) {
            insert bf;
        }
        return bf;
    }

    // NAIS-2494 START
    public static Business_Farm__c createBusinessFarm(Id pfsId, String name, Integer sequence, String bizOrFarm, String entityType, Boolean insertData) {
        Business_Farm__c bf = createBusinessFarm(pfsId, name, bizOrFarm, entityType, false);
        bf.Sequence_Number__c = sequence;

        if (insertData) {
            insert bf;
        }
        return bf;
    }
    // NAIS-2494 END

    public static Id createDataForIndividualAccountModel()
    {
        if(Family_Portal_Settings__c.getValues('Family')==null){
            insert new Family_Portal_Settings__c(Name='Family');
        }
        Family_Portal_Settings__c familySetting = Family_Portal_Settings__c.getValues('Family');
        if(familySetting.Individual_Account_Owner_Id__c==null){
            familySetting.Individual_Account_Owner_Id__c = UserInfo.getUserId();
            update familySetting;
        }
        return familySetting.Individual_Account_Owner_Id__c;
    }//End:createDataForIndividualAccountModel

    /**
     * @description Insert an account share for the portal users account because criteria based sharing does not
     *             work in unit tests.
     * @param accountId Account Id that the user will now have access to.
     * @param userId Id of the user that will now have access to the account.
     */
    public static void insertAccountShare(Id accountId, Id userId) {
        insert new AccountShare(AccountId=AccountId, CaseAccessLevel='Edit', OpportunityAccessLevel='Edit', AccountAccessLevel='Edit', UserOrGroupId=userId);
    }
}