/**
 * Class: AttachmentsHandler
 *
 * Copyright (C) 2015 NAIS
 *
 * Purpose: Handle the actions to create attachments related to an specific record,
 * relationship is determined by Attachment__c.Parent_ID__c. Once the Attachment__c 
 * object is created, then a new Attachment is created to save the file. This was 
 * implemented in this way to ensure sharing attachment between partner users, and
 * for those records which sharing is 'Controlled By Parent'.
 * 
 * Where Referenced:
 *   School Portal - SchoolRequiredDocument page
 *   Family Portal - FamilyDocumentList page
 *
 * Change History:
 *
 * Developer           Date        JIRA Ref     Description
 * ---------------------------------------------------------------------------------------
 * Leydi Rangel         05/14/15    NAIS-2345   Initial Development
 *
 * 
 */
 public class AttachmentsHandler {
    /*  Initialization  */
    public AttachmentsHandler()
    {
        this.clearInputFields();
        this.clearOutputFields();
    }
    /*  End Initialization  */
    
    
    /*Properties*/
    public blob inputFileBody {get; set;}
    public String inputFileName {get; set;}
    public String outputFileLink {get; set;}
    public  Attachment attachmentRecord {get; set;}
    public map<String, Attachment> mapAttachmentRecordByParentId {get; set;}
    public map<String, String> mapParentIdByAttachmentId {get; set;}
    /*End Properties*/
    
    
    /*Action Methods*/    
    /*End Action Methods*/
    
    /* Helper Mehtods */
    public void deleteAttachment(Id parentId, boolean isLogicalDelete)
    {
         list<Attachment__c> cutomAttachment = [Select Id, Deleted__c from Attachment__c 
                                                 where Parent_ID__c=:parentId];
        list<Attachment__c> modifiedList = new list<Attachment__c>();
                            
        if(cutomAttachment!=null && cutomAttachment.size()>0)
        {
            if(isLogicalDelete)
            {
                for(Attachment__c a:cutomAttachment){
                    a.Deleted__c = true;
                    modifiedList.add(a);
                }
                update modifiedList;
            }else{
                delete cutomAttachment;
            }
        }
    }//End:deleteAttachment
    
    public void clearInputFields()
    {
        this.inputFileBody=null;
        this.inputFileName=null;
    }//End:clearInputFields
    
    public void clearOutputFields()
    {
        this.outputFileLink='';
    }//End:clearOutputFields
    
    public void clearMaps()
    {
        this.mapAttachmentRecordByParentId=new map<String, Attachment>();
        this.mapParentIdByAttachmentId=new map<String, String>();
    }//End:clearOutputFields
    
    public void saveAttachmentRecord(Id parentId, String parentType)
    {
        if(this.inputFileBody!=null && parentId!=null)
        {
            Attachment__c parentAttachmentObject=null;
            list<Attachment__c> parentAttachmentList = [Select Id 
                                                        from Attachment__c 
                                                        where Parent_ID__c=:parentId 
                                                        and Parent_Type__c=:parentType 
                                                        ORDER BY LastModifiedDate DESC 
                                                        limit 1];
            
            if(parentAttachmentList!=null && parentAttachmentList.size()>0)
            {
                parentAttachmentObject=parentAttachmentList[0];
            }else{
                parentAttachmentObject = new Attachment__c(Parent_ID__c=parentId, 
                                                           Parent_Type__c=parentType);
                insert parentAttachmentObject;
            }
        
            if(attachmentRecord==null || attachmentRecord.Id==null)
            {
                attachmentRecord = new Attachment();
                attachmentRecord.ParentId = parentAttachmentObject.Id;
            }else{
                attachmentRecord = attachmentRecord;
            }
            attachmentRecord.Body = this.inputFileBody;
            attachmentRecord.Name = this.inputFileName;
            attachmentRecord.IsPrivate = false;
            upsert attachmentRecord;
            
            parentAttachmentObject.Attachment__c = attachmentRecord.Id;
            upsert parentAttachmentObject;
            this.createSharingPermissions(parentAttachmentObject.Id);
        }
    }//End:saveAttachmentRecord
    
    private void createSharingPermissions(String parentId)
    {
        String schoolId='X'+(Test.isRunningTest()
                        ?ApexPages.CurrentPage().getParameters().get('schoolId')
                        :Id.ValueOf(GlobalVariables.getCurrentSchoolId()));
        if(schoolId!=null && schoolId.length()==19)
        {
            try{
                Group groupSecurity = [Select Id, Name, DeveloperName 
                                                from Group 
                                                where DeveloperName=:schoolId 
                                                limit 1];
                insert new Attachment__Share(
                                        AccessLevel='Edit',
                                        UserOrGroupId=groupSecurity.Id, 
                                        ParentId=Id.ValueOf(parentId));
            }catch(Exception e){
                system.debug('AttachmentsHandler[createSharingPermissions]:'+e);
            }
        }
    }//End:createSharingPermissions
    
    public void setSelectedAttachmentVariables(Id parentId, String linkStr)
    {
        try{
            if(mapAttachmentRecordByParentId.containsKey(parentId))
            {
                this.attachmentRecord=mapAttachmentRecordByParentId.get(parentId);
                this.outputFileLink = linkStr;
            }
        }catch(Exception e){
            System.debug('AttachmentsHandler[setCurrentFileLinkAndGetCurrentRecordLabel:'+e.getLineNumber()+']: '+e);
        }
    }//End:setCurrentFileLink
    
    public String getAttachmentLinkByParentId(Id parentId, String linkLabel)
    {
        if(this.mapAttachmentRecordByParentId!=null  && this.mapAttachmentRecordByParentId.containsKey(parentId) 
            && this.mapAttachmentRecordByParentId.get(parentId)!=null){
                return this.getAttachmentLink((this.mapAttachmentRecordByParentId.get(parentId)).Id,linkLabel); 
        }
        return '';        
    }//End:getAttachmentLinkByParentId

    public static String retriveAttachmentLink(String attachmentId, String linkLabel)
    {
        return (attachmentId!=null ? '&nbsp;<a target="_blank" href="'+Site.getBaseCustomUrl()+Site.getPathPrefix()+'/servlet/servlet.FileDownload?file='+attachmentId+'" class="sample_link">'+linkLabel+'</a>' : '');
    }//End:retriveAttachmentLink
    
    public String getAttachmentLink(String attachmentId, String linkLabel)
    {
        return AttachmentsHandler.retriveAttachmentLink(attachmentId, linkLabel);
    }//End:getAttachmentLink
    /* End Helper Mehtods */
}