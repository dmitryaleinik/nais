public with sharing class SpringCMDocPreviewer_REST implements ISpringCMDocPreviewer{
    
    private Databank_Settings__c databankSettings;
    private string publishUrlStr {get;set;}
    public String errorMessageStr {get; set;}
    @testvisible private boolean isValidPage {get; set;}
    
    public SpringCMDocPreviewer_REST() {
    
        databankSettings = Databank_Settings__c.getInstance('Databank');
        isValidPage = false;
    }
    
    @testvisible private String Token {
        get{
            if (token == null || token == 'ERROR') {
                authorizeAndSetTheToken();
            }
            return token;
        }
        
        private set;
    }
    
    private void authorizeAndSetTheToken() {
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();

        String baseUrl = Site.getBaseUrl();
        baseUrl = String.isBlank(baseUrl) ? System.URL.getSalesforceBaseUrl().toExternalForm() : baseURL;
        String url = baseUrl + '/services/Soap/u/18.0/' + UserInfo.getOrganizationId();

        req.setTimeout(60000);
        req.setHeader('Accept', 'application/json');
        req.setHeader('content-type', 'application/x-www-form-urlencoded');
        req.setEndpoint(databankSettings.getTokenEndpoint__c);
        req.setMethod('POST');
        req.setBody('SFSession=' + UserInfo.getSessionId() + '|' + EncodingUtil.urlEncode(url, 'UTF-8') + 
            '&ClientId=' + databankSettings.clientId__c + 
            '&ClientSecret=' + databankSettings.clientSecret__c);
        
        //Execute web service call here
        try {
            HTTPResponse res = http.send(req);
            
            if (res.getStatusCode() == 200) {
                Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
                token = String.ValueOf(m.get('AccessTokenValue'));
            } else {
                token = 'ERROR';
                errorMessageStr = Label.SpringCM_Authentication_Error;
            }
        } catch (Exception e) {
            token = 'ERROR';
            errorMessageStr = Label.SpringCMDocPreviewer_Exception_Message_REST;
        }
    }
    
    /**
    * @description Allow to retrieve the first version of a 1040 document.
    * @return The id of the first version of the document.
    */
    public String doGetPrevVersionDocId() {

        String firstVersionDocId = null;
        
        if (familyDocument.Document_Type__c != null && familyDocument.Document_Type__c.contains('1040')) {
            
            String sessionId = String.ValueOf(UserInfo.getSessionId());
            String documentVersionsEndpoint = databankSettings.Use_UAT__c || Test.isRunningTest() ? databankSettings.Document_Versions_Endpoint_UAT__c : databankSettings.Document_Versions_Endpoint_PROD__c;
            documentVersionsEndpoint = documentVersionsEndpoint.replace('{id}', springCMDocId);
        
	        HttpRequest req = new HttpRequest();
	        req.setMethod('GET');
	        req.setEndpoint(documentVersionsEndpoint);
	        
	        req.setHeader('Authorization', 'oauth ' + token);
	        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
	        req.setHeader('Authorization', 'oauth ' + token);
	        req.setHeader('SFSession', sessionId);

	        Http http = new Http();
	        
	        try{
	            
	            HTTPResponse res = http.send(req);
	            
	            if (res.getStatusCode() == 200) {
            
	                Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
	                List<Object> versions  = (List<Object>)responseMap.get('Items');
	                
	                for (Object v : versions) {
	                    
	                    Map<String, Object> version = (Map<String, Object>)v;
	                    
	                    if (String.ValueOf(version.get('Version')) == '1.0') {
	                        
		                    List<String> parser = String.ValueOf(version.get('Href')).split('/');
		                    
		                    firstVersionDocId = parser[parser.size() - 1];
		                    
		                    break;
	                    }
	                }
	                
	            }
	            
	        } catch (Exception e) {
            
	            errorMessageStr = Label.SpringCMDocPreviewer_Exception_Message_REST;
	            CustomDebugService.Instance.withCustomMessage('SpringCM Document Version API Error: ' + springCMDocId).logException(e);
	        }
            
        }
        
        return firstVersionDocId;
    }
    
    private Family_Document__c familyDocument {
        
        get {
            
            if (familyDocument == null) {
                
                String docId = ApexPages.currentPage().getParameters().get('id');
                
                List<Family_Document__c> familyDocuments = [
	                SELECT Id, Document_Type__c
	                FROM Family_Document__c
	                WHERE Import_Id__c = :docId LIMIT 1];
	                
                familyDocument = familyDocuments.size() > 0 ? familyDocuments[0] : null;
            }
            
            return familyDocument;
        }
        set;
    }
    
    private String springCMDocId {
        get {
            
            if (springCMDocId == null) {
                
                springCMDocId =  ApexPages.currentPage().getParameters().get('id');
            }
            
            return springCMDocId;
        }
        set;
    }
    
    public PageReference getPDF() {
        
        isValidPage = false;
        String sessionId = String.ValueOf(UserInfo.getSessionId());
        Integer minutesToAdd = Integer.ValueOf(databankSettings.PDF_Publish_Minutes__c);
        Datetime expirationDate = Datetime.now().addMinutes(minutesToAdd);
        String documentsEndpoint = databankSettings.Use_UAT__c ? databankSettings.Documents_Endpoint_UAT__c : databankSettings.Documents_Endpoint_PROD__c;
        String sharelinkEndpoint = databankSettings.Use_UAT__c ? databankSettings.Sharelinks_Endpoint_UAT__c : databankSettings.Sharelinks_Endpoint_PROD__c;
        
        String firstVersionFor1040Only = doGetPrevVersionDocId();
        
        if (firstVersionFor1040Only != null) {
            
            springCMDocId = firstVersionFor1040Only;
        }
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(sharelinkEndpoint);
        
        req.setHeader('Authorization', 'oauth ' + token);
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setHeader('Authorization', 'oauth ' + token);
        req.setHeader('SFSession', sessionId);
        
        req.setBody('ExpirationDate=' + expirationDate +
            '&PreviewBehavior=DownloadInline' +
            '&Document[href]=' + documentsEndpoint + springCMDocId);
        
        Http http = new Http();
        
        try{
            
            HTTPResponse res = http.send(req);
            
            if (res.getStatusCode() == 201) {
            
                Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
                publishUrlStr  = String.ValueOf(responseMap.get('Url'));
                isValidPage = true;
                errorMessageStr = null;
            } else {
                
                throw new SpringCMRESTApiException('SpringCM Sharelink API Error. Response Code: '+res.getStatusCode() + ' ' + res.getStatus()+ ' ' + res.getBody());
            }
        }catch(Exception e) {
            
            errorMessageStr = Label.SpringCMDocPreviewer_Exception_Message_REST;
            CustomDebugService.Instance.withCustomMessage('SpringCM Sharelink API Error: ' + springCMDocId).logException(e);
        }
        
        return null; 
    }
    
    public String getpublishUrl() {
    
        return publishUrlStr;
    }
    
    public String getErrorMessage() {
        
        return errorMessageStr;
    }

    public class SpringCMRESTApiException extends Exception {}
}