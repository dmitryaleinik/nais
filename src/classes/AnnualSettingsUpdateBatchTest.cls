@isTest
private with sharing class AnnualSettingsUpdateBatchTest
{

    @isTest
    private static void testAnnualSettingsUpdateBatch()
    {
        String remindersType = 'Missing required documents only';
        String woRemindersOption = 'Do not send any reminders';
        Integer numberOfStudents = 50;

        Academic_Year__c academicYear = GlobalVariables.getPreviousAcademicYear();
        Account school = AccountTestData.Instance.asSchool().DefaultAccount;
        Annual_Setting__c annualSettings = AnnualSettingsTestData.Instance
            .forAcademicYearId(academicYear.Id)
            .withReminders(remindersType).DefaultAnnualSettings;

        List<Contact> students = createStudents(numberOfStudents);
        List<Applicant__c> applicants = createApplicants(new Map<Id, Contact>(students).keySet());
        List<School_PFS_Assignment__c> spfsas = createSpfsas(new Map<Id, Applicant__c>(applicants).keySet(), academicYear.Name);

        Test.startTest();
            annualSettings.Email_Reminders_to_Parents__c = woRemindersOption;

            AnnualSettingsUpdateBatch annualSettingsUpdateBatch = new AnnualSettingsUpdateBatch(new List<Annual_Setting__c>{annualSettings});

            Database.executeBatch(annualSettingsUpdateBatch);
        Test.stopTest();

        List<Annual_Setting__c> updatedAnnualSettings = AnnualSettingsSelector.Instance.selectBySchoolAndAcademicYear(
            new Set<Id>{school.Id}, new Set<String>{academicYear.Name}, new List<String>{'Email_Reminders_to_Parents__c'});
        System.assertEquals(1, updatedAnnualSettings.size());
        System.assertEquals(woRemindersOption, updatedAnnualSettings[0].Email_Reminders_to_Parents__c);
    }

    private static List<Contact> createStudents(Integer numberOfStudents)
    {
        List<Contact> students = new List<Contact>();
        for (Integer i = 0; i < numberOfStudents; i++)
        {
            students.add(ContactTestData.Instance.asStudent().create());
        }
        insert students;

        return students;
    }

    private static List<Applicant__c> createApplicants(Set<Id> studentIds)
    {
        List<Applicant__c> applicants = new List<Applicant__c>();
        for (Id studentId : studentIds)
        {
            applicants.add(ApplicantTestData.Instance.forContactId(studentId).create());
        }
        insert applicants;

        return applicants;
    }

    private static List<School_PFS_Assignment__c> createSpfsas(Set<Id> applicantIds, String academicYear)
    {
        List<School_PFS_Assignment__c> spfsas = new List<School_PFS_Assignment__c>();
        for (Id applicantId : applicantIds)
        {
            spfsas.add(SchoolPfsAssignmentTestData.Instance
                .forAcademicYearPicklist(academicYear)
                .forApplicantId(applicantId).create());
        }
        insert spfsas;

        return spfsas;
    }
}