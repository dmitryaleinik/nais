@isTest
private class KnowledgeAVSelectorTest
{

    @isTest
    private static void selectById_emptySetOfIds_expectEmptyList()
    {
        try
        {
            List<Knowledge__kav> queriedRecords = KnowledgeAVSelector.newInstance().selectById(null);
        }
        catch (Exception ex)
        {
            System.assert(ex.getMessage().contains(KnowledgeAVSelector.ID_SET_PARAM));
        }
    }

    @isTest
    private static void selectById_setWithRecordIds_expectRecordsQueried()
    {
        Integer numberOfRecordsToInsert = 10;
        List<Knowledge__kav> knowledgeArticles = KnowledgeAVTestData.Instance.insertKnowledgeAVs(numberOfRecordsToInsert);

        List<Knowledge__kav> queriedRecords = KnowledgeAVSelector.newInstance().selectById(
            new Map<Id, Knowledge__kav>(knowledgeArticles).keySet());

        System.assert(!queriedRecords.isEmpty(), 'Expected records to have actually been queried.');
        System.assertEquals(numberOfRecordsToInsert, queriedRecords.size(), 'Expected all inserted records to be queried.');
    }
}