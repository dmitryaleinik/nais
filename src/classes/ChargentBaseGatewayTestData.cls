/**
 * @description Test data for the Chargent Base Gateway object.
 */
@isTest
public class ChargentBaseGatewayTestData extends SObjectTestData {
    /**
     * @description Get the default values for the Gateway object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        /* We have RecordTypes.cls (Please, remove this comment if it's fine) */
        Id stripeRecordTypeId = ChargentBase__Gateway__c.getSObjectType().getDescribe().getRecordTypeInfosByName()
                .get('Stripe').getRecordTypeId();
        return new Map<Schema.SObjectField,Object> {
                ChargentBase__Gateway__c.ChargentBase__Active__c => true,
                ChargentBase__Gateway__c.ChargentBase__Merchant_ID__c => '12345',
                ChargentBase__Gateway__c.ChargentBase__ModusLink_Test_Mode__c => null,
                ChargentBase__Gateway__c.RecordTypeId => stripeRecordTypeId
        };
    }

    /**
     * @description Insert the current working Gateway record.
     * @return The currently operated upon Gateway record.
     */
    public ChargentBase__Gateway__c insertGateway() {
        return (ChargentBase__Gateway__c)insertRecord();
    }

    /**
     * @description Create the current working Chargent Base Gateway record without resetting
     *             the stored values in this instance of ChargentBaseGatewayTestData.
     * @return A non-inserted ChargentBase__Gateway__c record using the currently stored field
     *             values.
     */
    public ChargentBase__Gateway__c createChargentBaseGatewayWithoutReset() {
        return (ChargentBase__Gateway__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Gateway record.
     * @return The currently operated upon Gateway record.
     */
    public ChargentBase__Gateway__c create() {
        return (ChargentBase__Gateway__c)super.buildWithReset();
    }

    /**
     * @description The default Gateway record.
     */
    public ChargentBase__Gateway__c DefaultGateway {
        get {
            if (DefaultGateway == null) {
                DefaultGateway = createChargentBaseGatewayWithoutReset();
                insert DefaultGateway;
            }
            return DefaultGateway;
        }
        private set;
    }

    /**
     * @description Get the ChargentBase Gateway SObjectType.
     * @return The ChargentBase Gateway SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return ChargentBase__Gateway__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static ChargentBaseGatewayTestData Instance {
        get {
            if (Instance == null) {
                Instance = new ChargentBaseGatewayTestData();
            }
            return Instance;
        }
        private set;
    }

    private ChargentBaseGatewayTestData() { }
}