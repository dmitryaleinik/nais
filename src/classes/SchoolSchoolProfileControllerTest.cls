@isTest
private class SchoolSchoolProfileControllerTest
{

    private static final Id portalProfileId = [select Id from Profile where Name = :ProfileSettings.SchoolAdminProfileName].Id;
    
    // [CH] NAIS-1766 Adding to help avoid Mixed DML Errors
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    
    /*Initialization*/
    
    // normal use case - access page from within School Portal
    @isTest
    private static void initFromWithinSchoolPortal() {
        Account school = TestUtils.createAccount('School A', RecordTypes.schoolAccountTypeId, 2, true);
        Contact staff = TestUtils.createContact('Staff A', school.Id, RecordTypes.schoolStaffContactTypeId, true);
        User portalUser = TestUtils.createPortalUser('Staff A', 'sa@test.org', 'sa', staff.Id, portalProfileId, true, false);
    
        System.runAs(thisUser){
            insert portalUser;    
        }
        
        System.runAs(portalUser) {
            Test.setCurrentPage(Page.SchoolSchoolProfile);
            
            SchoolSchoolProfileController ext = new SchoolSchoolProfileController();
            
            // get the school of the current portal user
            System.assertEquals(school.Id, ext.mySchool.Id);
        }
    }
    
    /*Action Methods*/
    @isTest
    private static void testSave() {
        Account school = TestUtils.createAccount('School A', RecordTypes.schoolAccountTypeId, 2, true);
        Contact staff = TestUtils.createContact('Staff A', school.Id, RecordTypes.schoolStaffContactTypeId, true);
        User portalUser = TestUtils.createPortalUser('Staff A', 'sa@test.org', 'sa', staff.Id, portalProfileId, true, false);
        
        System.runAs(thisUser){
            insert portalUser;    
        }

        TestUtils.insertAccountShare(staff.AccountId, portalUser.Id);
        
        System.runAs(portalUser) {
            Test.setCurrentPage(Page.SchoolSchoolProfile);
            
            SchoolSchoolProfileController ext = new SchoolSchoolProfileController();
            
            ext.mySchool.BillingStreet = '901 Mission';
            
            PageReference pRef = ext.saveSettings();
            
            Account updatedSchool = [select BillingStreet from Account where Id = :school.Id];
            
            // verify that edit is saved
            System.assertEquals('901 Mission', updatedSchool.BillingStreet);
            // verify page redirection - should go to portal home?
            System.assertEquals(null, pRef);
        }
    }

    @isTest
    private static void testCancel() {
        Account school = TestUtils.createAccount('School A', RecordTypes.schoolAccountTypeId, 2, true);
        Contact staff = TestUtils.createContact('Staff A', school.Id, RecordTypes.schoolStaffContactTypeId, true);
        User portalUser = TestUtils.createPortalUser('Staff A', 'sa@test.org', 'sa', staff.Id, portalProfileId, true, false);
        
        System.runAs(thisUser){
            insert portalUser;    
        }
        
        System.runAs(portalUser) {
            Test.setCurrentPage(Page.SchoolSchoolProfile);
            
            SchoolSchoolProfileController ext = new SchoolSchoolProfileController();
            
            ext.mySchool.BillingStreet = '901 Mission';
            
            PageReference pRef = ext.cancel();
            
            Account updatedSchool = [select BillingStreet from Account where Id = :school.Id];
            
            // verify that edit is not saved
            System.assertEquals(null, updatedSchool.BillingStreet);
            // verify page redirection - should go to portal home?
            System.assertEquals(Page.SchoolSchoolProfileWrapper.getUrl(), pRef.getUrl());
        }
    }
}