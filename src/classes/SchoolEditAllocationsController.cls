/**
 * Class: SchoolEditAllocationsController
 *
 * Copyright (C) 2015 NAIS
 *
 * Purpose: The Allocate Awards tab, $ pop-up to give an award has a picklist at the left for budget group.
 * 1. this should not be required because we do allow people to allocate awards with no budget group.
 *
 * The requirements for this are for it to act just like the Financial Aid page, the reason we took out the
 * type in is because they needed to be able to also allocate from a budget. Now we have a feature where they
  * can only allocate from a budget, and not do a straight allocation any longer.
 *
 * Notes from discussion w team:
 * Looks problematic to solve this with Skuid alone.  Discussed possibility of calling a VF page in the pop-up
 * instead of a Skuid page, or automatically creating a default budget group for schools that don't use budget
 * groups, or dynamically displaying different Skuid tabs depending on whether the school used budget groups, but
 * none of these seem like great options.
 *
 * UPDATE: solution to pursue is replacing Skuid popup with VF page.  If school has no budget groups for the
 * year of the selected folder, do not offer the award aid from budget groups option.  If school has budget groups
 * for the selected year, the award aid from budget groups option should be pre-selected.  (Note that in the future
 * NAIS may want a VF list page for aid allocation with the award aid option inline in each row, in case that has any
 * bearing on design decisions.)
 *
 * Where Referenced:
 *   School Portal - SchoolEditAllocations page
 *
 * Change History:
 *
 * Developer           Date        JIRA Ref     Description
 * ---------------------------------------------------------------------------------------
 * Leydi Rangel         05/14/15    NAIS-2313   Initial Development
 *
 *
 */
public with sharing class SchoolEditAllocationsController extends financialAidEditAllocationsBase{
    /*  Initialization  */
    public SchoolEditAllocationsController()
    {
        this.isAfterSave = (System.currentPagereference().getParameters().get('reload')=='1'?true:false);
        this.studentFolderId = System.currentPagereference().getParameters().get('id');
        this.folder = this.getFolderData(this.studentFolderId);
        this.studentFolderOriginal = this.folder.clone();
        this.loadBudgets();
        this.newBudgetInit();
        this.loadBudgetGroups();

        if (useBudgets == null){
            budgetAllocations = (budgetAllocations == null) ? new List<Budget_Allocation__c>() : budgetAllocations;
            this.budgetAllocationsOriginal = budgetAllocations.clone();
            budgetGroups = (budgetGroups == null) ? new List<Budget_Group__c>() : budgetGroups;

            // NEW LOGIC to handle case when grant has been awarded from the MyApplicants page - [dp] 6.17.14
            // default to use budgets if 1. there are budget groups and -- either 2. budget allocations exist or 3. grant awarded is null
            useBudgets = (budgetGroups.size() > 0 && (budgetAllocations.size() > 0 || folder.Grant_Awarded__c == null)) ? 'Yes' : 'No';

        }

        // allow the budget choice if there are budget groups
        allowBudgetChoice = budgetGroups != null && budgetGroups.size() > 0 ? true : false;
        budgetChange = false;
    }
    /*  End Initialization  */

    /*Properties*/
    public String studentFolderId{get; set;}
    private Student_Folder__c studentFolderOriginal{get; set;}
    public boolean isValidFolder{get; set;}
    public boolean isAfterSave{get; set;}
    public list<Budget_Allocation__c> budgetAllocationsOriginal{get; set;}
    /*End Properties*/

    /*Action Methods*/
    public PageReference saveAction() {
        amountErrorMsg = false;
        AmountErrorMessage = null;

        if (newBudgetAlloc.Amount_Allocated__c != null) {
            amountErrorMsg = true;
            AmountErrorMessage = Label.AwardAmountNotSaved;
            return null;
        }
        PageReference thisPage = Page.SchoolEditAllocations;
        thisPage.getParameters().put('id',this.studentFolderId);
        thisPage.setRedirect(true);

        folder.Use_Budget_Groups__c=useBudgets;
        if(folder.Use_Budget_Groups__c=='Yes')
        {
            saveWhenBudgetGroupIsYes();
        }else{
            saveWhenBudgetGroupIsNo();
        }
        if(!ApexPages.hasMessages(ApexPages.Severity.ERROR)){
            thisPage.getParameters().put('reload','1');
        }
        return (!ApexPages.hasMessages(ApexPages.Severity.ERROR)?thisPage:null);
    }
    /*End Action Methods*/

    /* Helper Mehtods */
    private Student_Folder__c getFolderData(String folderId)
    {
        Student_Folder__c thisFolder = null;
        try{
            thisFolder = [Select Financial_Need__c, Use_Budget_Groups__c, Grant_Awarded__c,
                                    School__c, Academic_Year_Picklist__c, Other_Aid__c, Tuition_Remission__c,
                                    Total_Aid__c, Unmet_Financial_Need__c
                                    FROM Student_Folder__c
                                    where Id=:folderId
                                    limit 1];
                isValidFolder = true;
        }catch(Exception e){
            isValidFolder=false;
        }
        return thisFolder;
    }//End:getFolderData

    public void loadBudgetGroups()
    {
        String school_id = (folder.School__c!=null?folder.School__c:GlobalVariables.getCurrentSchoolId());
        budgetGroups = [Select Id, Name from Budget_Group__c
                                where School__c=:school_id
                                and Academic_Year__r.Name = :folder.Academic_Year_Picklist__c
                                order by Name asc];
        budgetGroupIdToBudgetGroupMap = new Map<Id, Budget_Group__c>(budgetGroups);
    }//End:loadBudgetGroups

    private void saveStudentFolderValues()
    {
        if(this.folder!=null
        && (this.studentFolderOriginal.Grant_Awarded__c!=this.folder.Grant_Awarded__c
        || this.studentFolderOriginal.Other_Aid__c!=this.folder.Other_Aid__c
        || this.studentFolderOriginal.Tuition_Remission__c!=this.folder.Tuition_Remission__c
        || this.studentFolderOriginal.Use_Budget_Groups__c!=this.folder.Use_Budget_Groups__c
        )){
            update this.folder;
        }
    }//End:saveStudentFolderValues

    private void saveWhenBudgetGroupIsYes()
    {
        Savepoint sp = Database.setSavepoint();
        try{
            this.folder.Grant_Awarded__c = totalFromBudgets;
            this.saveStudentFolderValues();

            if (useBudgets == 'Yes'){
                handleBudgetUpdates();
            }
        }catch(Exception e){
            System.debug(LoggingLevel.ERROR, 'ERROR ON SAVE: ' + e.getMessage()  + '; ' + e.getStackTraceString());
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            Database.rollback(sp);
        }
    }//End:saveWhenBudgetGroupIsYes

    private void saveWhenBudgetGroupIsNo()
    {
        Savepoint sp = Database.setSavepoint();
        try{
            //StudentFolderAfter.trigger is deleting the related Budget_Allocation__c
            //records through SchoolBudgetGroupAction.recalcStudentFolderGrantAwarded
            this.saveStudentFolderValues();
        }catch(Exception e){
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            System.debug(LoggingLevel.ERROR, 'ERROR ON SAVE: ' + e.getMessage()  + '; ' + e.getStackTraceString());
            Database.rollback(sp);
        }
    }//End:saveWhenBudgetGroupIsNo
    /* End Helper Mehtods */
}