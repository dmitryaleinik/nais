@isTest
private class SubscriptionServiceTest {
    @isTest
    private static void shouldCreateSubscription_nullOpp_expectArgumentNullException() {
        try {
            Test.startTest();
            SubscriptionService.Instance.shouldCreateSubscription(null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, SubscriptionService.OPPORTUNITY_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void shouldCreateSubscription_nullAccountId_expectFalse() {
        System.assert(!SubscriptionService.Instance.shouldCreateSubscription(new Opportunity()), 'Expected false.');
    }

    @isTest
    private static void shouldCreateSubscription_nullAcademicYear_expectFalse() {
        System.assert(!SubscriptionService.Instance.shouldCreateSubscription(
                new Opportunity(AccountId = AccountTestData.Instance.DefaultAccount.Id)), 'Expected false.');
    }

    @isTest
    private static void shouldCreateSubscription_notClosedButPaid_expectFalse() {
        Opportunity opportunity = OpportunityTestData.Instance
                .forAccount(AccountTestData.Instance.DefaultAccount.Id).insertAsPaid();
        System.assertEquals('Paid', opportunity.Paid_Status__c, 'Expected the opportunity to be paid.');
        System.assertEquals('Prospecting', opportunity.StageName, 'Expected the opportunity to be closed won.');

        Test.startTest();
        System.assert(!SubscriptionService.Instance.shouldCreateSubscription(opportunity), 'Expected false.');
        Test.stopTest();
    }

    @isTest
    private static void updateAnnualSettingDisplayEstimatesField_forSchoolWithPremiumSubscription()
    {
        Account schoolA = AccountTestData.Instance.forName('schoolA').create();
        Account schoolB = AccountTestData.Instance.forName('schoolB').create();
        Account schoolC = AccountTestData.Instance.forName('schoolC').create();
        insert new List<Account>{schoolA, schoolB, schoolC};

        List<Academic_Year__c> academicYears = AcademicYearTestData.Instance.createAcademicYears(3);
        insert academicYears;

        Annual_Setting__c annualSettingA_AY1 = AnnualSettingsTestData.Instance
            .forSchoolId(schoolA.Id)
            .forAcademicYearId(academicYears[0].Id)
            .forDisplayNextYearTaxEstimates(true).create();
        Annual_Setting__c annualSettingA_AY2 = AnnualSettingsTestData.Instance
            .forSchoolId(schoolA.Id)
            .forAcademicYearId(academicYears[1].Id)
            .forDisplayNextYearTaxEstimates(true).create();
        Annual_Setting__c annualSettingA_AY3 = AnnualSettingsTestData.Instance
            .forSchoolId(schoolA.Id)
            .forAcademicYearId(academicYears[2].Id)
            .forDisplayNextYearTaxEstimates(true).create();
        Annual_Setting__c annualSettingB_AY1 = AnnualSettingsTestData.Instance
            .forSchoolId(schoolB.Id)
            .forAcademicYearId(academicYears[0].Id)
            .forDisplayNextYearTaxEstimates(true).create();
        Annual_Setting__c annualSettingB_AY2 = AnnualSettingsTestData.Instance
            .forSchoolId(schoolB.Id)
            .forAcademicYearId(academicYears[1].Id)
            .forDisplayNextYearTaxEstimates(true).create();
        Annual_Setting__c annualSettingC_AY1 = AnnualSettingsTestData.Instance
            .forSchoolId(schoolC.Id)
            .forAcademicYearId(academicYears[0].Id)
            .forDisplayNextYearTaxEstimates(true).create();
        Annual_Setting__c annualSettingC_AY2 = AnnualSettingsTestData.Instance
            .forSchoolId(schoolC.Id)
            .forAcademicYearId(academicYears[1].Id)
            .forDisplayNextYearTaxEstimates(true).create();
        List<Annual_Setting__c> annualSettings = new List<Annual_Setting__c>{annualSettingA_AY1, annualSettingA_AY2, annualSettingA_AY3,
            annualSettingB_AY1, annualSettingB_AY2, annualSettingC_AY1, annualSettingC_AY2};
        insert annualSettings;

        Subscription__c oldSubscriptionA = SubscriptionTestData.Instance
            .forStartDate(System.today())
            .forEndDate(System.today())
            .forAccount(schoolA.Id)
            .forSubscriptionType(SubscriptionService.SUBSCRIPTION_TYPE_PREMIUM).create();
        Subscription__c oldSubscriptionB = SubscriptionTestData.Instance
            .forAcademicYear(academicYears[0].Name.left(4))
            .forAccount(schoolB.Id)
            .forSubscriptionType(SubscriptionService.SUBSCRIPTION_TYPE_PREMIUM).create();
        Subscription__c oldSubscriptionC = SubscriptionTestData.Instance
            .forAcademicYear(academicYears[0].Name.left(4))
            .forAccount(schoolC.Id)
            .forSubscriptionType(SubscriptionService.SUBSCRIPTION_TYPE_PREMIUM).create();
        insert new List<Subscription__c>{oldSubscriptionA, oldSubscriptionB, oldSubscriptionC};

        Set<Id> allASIds = new Map<Id, Annual_Setting__c>(annualSettings).keySet();
        List<String> asFieldsToSelect = new List<String>{'Display_Next_Year_Tax_Estimates__c'};

        Test.startTest();
            for (Annual_Setting__c annualSetting : AnnualSettingsSelector.Instance.selectWithCustomFieldListById(allASIds, asFieldsToSelect))
            {
                System.assertEquals(true, annualSetting.Display_Next_Year_Tax_Estimates__c);
            }

            oldSubscriptionA.Subscription_Type__c = SubscriptionService.SUBSCRIPTION_TYPE_FULL;
            oldSubscriptionB.Subscription_Type__c = SubscriptionService.SUBSCRIPTION_TYPE_FULL;
            update new List<Subscription__c>{oldSubscriptionA, oldSubscriptionB};


            for (Annual_Setting__c annualSetting : AnnualSettingsSelector.newInstance().selectWithCustomFieldListById(
                new Set<Id>{annualSettingC_AY1.Id, annualSettingC_AY2.Id}, asFieldsToSelect))
            {
                System.assertEquals(true, annualSetting.Display_Next_Year_Tax_Estimates__c);
            }

            for (Annual_Setting__c annualSetting : AnnualSettingsSelector.newInstance().selectWithCustomFieldListById(
                new Set<Id>{annualSettingA_AY1.Id, annualSettingA_AY2.Id, annualSettingA_AY3.Id, annualSettingB_AY1.Id, annualSettingB_AY2.Id},
                asFieldsToSelect))
            {
                System.assertEquals(false, annualSetting.Display_Next_Year_Tax_Estimates__c);
            }
        Test.stopTest();
    }

    @isTest
    private static void shouldCreateSubscription_notClosedButOverpaid_expectFalse() {
        Opportunity opportunity = OpportunityTestData.Instance
                .forAccount(AccountTestData.Instance.DefaultAccount.Id).insertAsOverpaid();
        System.assertEquals('Overpaid', opportunity.Paid_Status__c, 'Expected the opportunity to be paid.');
        System.assertEquals('Prospecting', opportunity.StageName, 'Expected the opportunity to be closed won.');

        Test.startTest();
        System.assert(!SubscriptionService.Instance.shouldCreateSubscription(opportunity), 'Expected false.');
        Test.stopTest();
    }

    @isTest
    private static void shouldCreateSubscription_closedNotPaid_expectFalse() {
        Opportunity opportunity = OpportunityTestData.Instance
                .forAccount(AccountTestData.Instance.DefaultAccount.Id).insertAsClosedWon();
        System.assertEquals('Unpaid', opportunity.Paid_Status__c, 'Expected the opportunity to be unpaid.');
        System.assertEquals('Closed Won', opportunity.StageName, 'Expected the opportunity to be closed won.');

        Test.startTest();
        System.assert(!SubscriptionService.Instance.shouldCreateSubscription(opportunity), 'Expected false.');
        Test.stopTest();
    }

    @isTest
    private static void shouldCreateSubscription_closedPaid_expectTrue() {
        Opportunity opportunity = OpportunityTestData.Instance
                .forAccount(AccountTestData.Instance.DefaultAccount.Id).insertAsClosedWonAndPaid();
        System.assertEquals('Paid', opportunity.Paid_Status__c, 'Expected the opportunity to be paid.');
        System.assertEquals('Closed Won', opportunity.StageName, 'Expected the opportunity to be closed won.');

        Test.startTest();
        System.assert(SubscriptionService.Instance.shouldCreateSubscription(opportunity), 'Expected true.');
        Test.stopTest();
    }

    @isTest
    private static void shouldCreateSubscription_closedPaidHasSubscriptionAlready_expectFalse() {
        Opportunity opportunity = OpportunityTestData.Instance
                .forAccount(AccountTestData.Instance.DefaultAccount.Id).insertAsClosedWonAndPaid();
        System.assertEquals('Paid', opportunity.Paid_Status__c, 'Expected the opportunity to be paid.');
        System.assertEquals('Closed Won', opportunity.StageName, 'Expected the opportunity to be closed won.');

        SubscriptionTestData.Instance.forAccount(AccountTestData.Instance.DefaultAccount.Id)
                .forAcademicYear(opportunity.Academic_Year_Picklist__c.left(4)).insertSubscription();

        Test.startTest();
        System.assert(!SubscriptionService.Instance.shouldCreateSubscription(opportunity), 'Expected false.');
        Test.stopTest();
    }

    @isTest
    private static void shouldDeleteSubscription_nullOpp_expectArgumentNullException() {
        try {
            Test.startTest();
            SubscriptionService.Instance.shouldDeleteSubscription(null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, SubscriptionService.OPPORTUNITY_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void shouldDeleteSubscription_nullAccountId_expectFalse() {
        System.assert(!SubscriptionService.Instance.shouldDeleteSubscription(new Opportunity()), 'Expected false.');
    }

    @isTest
    private static void shouldDeleteSubscription_nullAcademicYear_expectFalse() {
        System.assert(!SubscriptionService.Instance.shouldDeleteSubscription(
                new Opportunity(AccountId = AccountTestData.Instance.DefaultAccount.Id)), 'Expected false.');
    }

    @isTest
    private static void shouldDeleteSubscription_notClosedButPaid_expectTrue() {
        Opportunity opportunity = OpportunityTestData.Instance
                .forAccount(AccountTestData.Instance.DefaultAccount.Id).insertAsPaid();
        System.assertEquals('Paid', opportunity.Paid_Status__c, 'Expected the opportunity to be paid.');
        System.assertEquals('Prospecting', opportunity.StageName, 'Expected the opportunity to be closed won.');

        SubscriptionTestData.Instance.forAccount(AccountTestData.Instance.DefaultAccount.Id)
                .forAcademicYear(opportunity.Academic_Year_Picklist__c.left(4)).insertSubscription();

        Test.startTest();
        System.assert(SubscriptionService.Instance.shouldDeleteSubscription(opportunity), 'Expected true.');
        Test.stopTest();
    }

    @isTest
    private static void shouldDeleteSubscription_notClosedButOverpaid_expectTrue() {
        Opportunity opportunity = OpportunityTestData.Instance
                .forAccount(AccountTestData.Instance.DefaultAccount.Id).insertAsOverpaid();
        System.assertEquals('Overpaid', opportunity.Paid_Status__c, 'Expected the opportunity to be paid.');
        System.assertEquals('Prospecting', opportunity.StageName, 'Expected the opportunity to be closed won.');

        SubscriptionTestData.Instance.forAccount(AccountTestData.Instance.DefaultAccount.Id)
                .forAcademicYear(opportunity.Academic_Year_Picklist__c.left(4)).insertSubscription();

        Test.startTest();
        System.assert(SubscriptionService.Instance.shouldDeleteSubscription(opportunity), 'Expected true.');
        Test.stopTest();
    }

    @isTest
    private static void shouldDeleteSubscription_closedNotPaid_expectTrue() {
        Opportunity opportunity = OpportunityTestData.Instance
                .forAccount(AccountTestData.Instance.DefaultAccount.Id).insertAsClosedWon();
        System.assertEquals('Unpaid', opportunity.Paid_Status__c, 'Expected the opportunity to be unpaid.');
        System.assertEquals('Closed Won', opportunity.StageName, 'Expected the opportunity to be closed won.');

        SubscriptionTestData.Instance.forAccount(AccountTestData.Instance.DefaultAccount.Id)
                .forAcademicYear(opportunity.Academic_Year_Picklist__c.left(4)).insertSubscription();

        Test.startTest();
        System.assert(SubscriptionService.Instance.shouldDeleteSubscription(opportunity), 'Expected true.');
        Test.stopTest();
    }

    @isTest
    private static void shouldDeleteSubscription_closedNotPaidNoSubscription_expectFalse() {
        Opportunity opportunity = OpportunityTestData.Instance
                .forAccount(AccountTestData.Instance.DefaultAccount.Id).insertAsClosedWon();
        System.assertEquals('Unpaid', opportunity.Paid_Status__c, 'Expected the opportunity to be unpaid.');
        System.assertEquals('Closed Won', opportunity.StageName, 'Expected the opportunity to be closed won.');

        Test.startTest();
        System.assert(!SubscriptionService.Instance.shouldDeleteSubscription(opportunity), 'Expected false.');
        Test.stopTest();
    }

    @isTest
    private static void shouldDeleteSubscription_closedPaid_expectFalse() {
        Opportunity opportunity = OpportunityTestData.Instance
                .forAccount(AccountTestData.Instance.DefaultAccount.Id).insertAsClosedWonAndPaid();
        System.assertEquals('Paid', opportunity.Paid_Status__c, 'Expected the opportunity to be paid.');
        System.assertEquals('Closed Won', opportunity.StageName, 'Expected the opportunity to be closed won.');

        SubscriptionTestData.Instance.forAccount(AccountTestData.Instance.DefaultAccount.Id)
                .forAcademicYear(opportunity.Academic_Year_Picklist__c.left(4)).insertSubscription();

        Test.startTest();
        System.assert(!SubscriptionService.Instance.shouldDeleteSubscription(opportunity), 'Expected false.');
        Test.stopTest();
    }

    @isTest
    private static void deleteSubscriptions_nullOpportunity_expectArgumentNullException() {
        try {
            Test.startTest();
            SubscriptionService.Instance.deleteSubscriptions((Opportunity)null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, SubscriptionService.OPPORTUNITY_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void deleteSubscriptions_nullOpportunityList_expectArgumentNullException() {
        try {
            Test.startTest();
            SubscriptionService.Instance.deleteSubscriptions((List<Opportunity>)null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, SubscriptionService.OPPORTUNITIES_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void deleteSubscriptions_validRequest_expectSubscriptionsDeleted() {
        Opportunity opportunity = OpportunityTestData.Instance.DefaultOpportunity;
        Subscription__c subscription = SubscriptionTestData.Instance
                .forAcademicYear(opportunity.Academic_Year_Picklist__c.left(4)).insertSubscription();

        Integer startYear = Integer.valueOf(opportunity.Academic_Year_Picklist__c.left(4));
        System.assertNotEquals(null, startYear, 'Expected a start year to be available.');

        List<Subscription__c> subscriptions = SubscriptionSelector.Instance
                .selectByAccountIdForAcademicYear(new Set<Id> { opportunity.AccountId }, startYear);
        System.assert(!subscriptions.isEmpty(), 'Expected there to be a subscription found.');

        Test.startTest();
        SubscriptionService.Instance.deleteSubscriptions(opportunity);
        Test.stopTest();

        List<Subscription__c> remainingSubscriptions = SubscriptionSelector.Instance
                .selectByAccountIdForAcademicYear(new Set<Id> { opportunity.AccountId }, startYear);
        System.assert(remainingSubscriptions.isEmpty(), 'Expected there to be no subscriptions remaining.');
    }
    
    @isTest
    private static void insert_multipleOpportunities_expectOpportunitiesInserted() {
        
        List<Opportunity> opps = new List<Opportunity>();
        
        for(Integer i=0; i<=200; i++) {
            
            opps.add(OpportunityTestData.Instance.forAccount(AccountTestData.Instance.DefaultAccount.Id).create());
        }
        
        insert opps;
    }
}