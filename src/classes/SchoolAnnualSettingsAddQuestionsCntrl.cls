/** 
 *    SFP-604
 *    [8.1.2016] Move these items to component and individual controller for reusibility [jB]
 */
public with sharing class SchoolAnnualSettingsAddQuestionsCntrl {

    public Annual_Setting__c annualSettingsModel {get; set;}
    public Id academicYearId {get; set;}
    
    // SFP-668 [8.11.16] determine if custom questions have labels and hide them if they do not [jB]
    public Map<String,Boolean> customQuestionHasLabel {get {
        if(customQuestionHasLabel==null){
            customQuestionHasLabel = new Map<String,Boolean>();
            for(String cclabel : AnnualSettingHelper.customQuestionLabelsQueryString.split(','))
            {
                customQuestionHaslabel.put(ccLabel,
                    (labels.get(ccLabel)!=null && String.valueOf(labels.get(ccLabel))!=''));
            }
        }
        return customQuestionHasLabel;
    } private set;} 

    // SFP-668 [8.14.16] determine if no questions have labels, hide the entire section SFP-676, SFP-677
    public Boolean getNoQuestionLabels(){
        Boolean retVal = true;
        for(String questionLabel : customQuestionHaslabel.keyset()){
            if(customQuestionHaslabel.get(questionLabel)){
                retVal= false;
                break;
            }
        }
        return retVal;
    }

    public SchoolAnnualSettingsAddQuestionsCntrl() {

    }

    /**
     *  Pull custom question labels from Question_Label__c record for current academic 
     */
    private static Question_Labels__c questionLabels;
    public Question_Labels__c labels {get {
        if(questionLabels == null){
            questionLabels = new Question_Labels__c();
            Schema.FieldSet questionLabelsFieldset = Schema.SObjectType.Question_Labels__c.fieldSets.getMap().get('AnnualSettingsCustomQuestionBank');    
            List<String> questionFieldNames = new List<String>();
            for (FieldSetMember field : questionLabelsFieldset.getFields()) {
                questionFieldNames.add(field.getFieldPath());
            }
            List<String> questionFields = new List<String>(Schema.SObjectType.Question_Labels__c.fields.getMap().keySet());
            String questionQueryString = 'SELECT ' + String.Join(questionFields, ',');
                questionQueryString += ' from Question_Labels__c where Academic_Year__c = :academicYearId and Language__c = \'en_US\'';

            List<Question_Labels__c> labelsList = Database.query(questionQueryString);
            if (!labelsList.isEmpty()) {
                questionLabels = labelsList[0];
            }
        }
        return questionLabels;

    } private set;}

    public String customQuestionLimit{ get{
        return String.valueOf(AnnualSettingHelper.customQuestionLimit(annualSettingsModel));
    }}

    public String countCustomQuestionsUsed { get {
        return String.valueOf(AnnualSettingHelper.getCountCustomQuestionsUsed(annualSettingsModel));
    }}

    public String customQuestionsLimitUsed { get {
        return '('+String.valueOf(AnnualSettingHelper.getCountCustomQuestionsUsed(annualSettingsModel))+'/'+String.valueOf(AnnualSettingHelper.customQuestionLimit(annualSettingsModel))+')';
    }}

    public String customQuestionLimitText { get {
        String label = 'Please select up to '+String.valueOf(AnnualSettingHelper.customQuestionLimit(annualSettingsModel))+' additional questions.';
        return label;
    }}

    /**
     * @description Determines whether or not the custom questions should be read only. Custom questions will be read
     *              only if the family portal has opened for the academic year.
     */
    public Boolean customQuestionBankEditable {
        get {
            return !AcademicYearService.Instance.hasFamilyPortalOpened(academicYearId);
        }
    }

    /**
     * @description Returns string name of the next Academic Year.
     */
    public String nextAcademicYear { get {
        return ' ' + GlobalVariables.getNextAcademicYearStr(AcademicYearService.Instance.getCurrentAcademicYear().Name);
    }} 
}