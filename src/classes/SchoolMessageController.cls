/**
 * Class: SchoolMessageController
 *
 * Copyright (C) 2013 NAIS
 *
 * Purpose: Provides the functionality to create a new message (case) for a school admin
 * 
 * Where Referenced:
 *   Family Portal - FamilyApplicationMain Page
 *
 * Change History:
 *
 * Developer           Date        JIRA Ref     Description
 * ---------------------------------------------------------------------------------------
 * Brian Clift         10/1/13    NAIS-1153     Initial Development - Apparently funcationality
 *                                              using standard page layouts was working, but 
 *                                              due to removing record types from the portal user
 *                                              profile, user's no longer able to use edit functionality
 *                                              from standard page.
 *
 * exponent partners
 * 901 Mission Street, Suite 105
 * San Francisco, CA  94103
 * +1 (800) 918-2917
 * 
 */

public with sharing class SchoolMessageController {
    private ApexPages.StandardController caseController;
    
    // [CH] NAIS-1995 Adding support for linking back to PFS Summary
    public String retUrl {
        get{
            return ApexPages.currentPage().getParameters().get('retUrl');
        } 
        set;
    }
    
    public String pfsId {
        get{
            return ApexPages.currentPage().getParameters().get('pfsId');
        } 
        set;
    }
    
    public String folderId {
        get{
            return ApexPages.currentPage().getParameters().get('folderId');
        } 
        set;
    }
    //NAIS-1973
    public string caseId {get;set;}
    public String recordType {
        get{
            return ApexPages.currentPage().getParameters().get('RecordType');
        } 
        set;
    }
    public Boolean isSupport {
        get{
            return (ApexPages.currentPage().getParameters().get('isSupport')!=null)?true:false;
        } 
        set;
    }
    //SFP-50, [G.S]
    public Boolean fromPFS {
        get{
            return (ApexPages.currentPage().getParameters().get('fromPFS')!=null)?true:false;
        } 
        set;
    }
    public Boolean editMode {
        get{
            return (ApexPages.currentPage().getParameters().get('mode')!=null)?true:false;
        } 
        set;
    }
    //End NAIS-1973
    public Student_Folder__c studentFolder {
        get{
            if(studentFolder == null){//SFP-50, [G.S]
                if(folderId!=null) studentFolder = [select Id, Student__r.Name, (SELECT Applicant_ID__c FROM School_PFS_Assignments__r) from Student_Folder__c where id = :folderId limit 1];
                else studentFolder = new Student_Folder__c(); //NAIS-1973
            }            
            return studentFolder;
        }
        set;
    }
    
    public PFS__c pfsRecord {
        get{
            if(pfsRecord == null){//SFP-50, [G.S]
                if(pfsId!= null){
                    pfsRecord = [select Id, Parent_A__r.Name, Name, Academic_Year_Picklist__c, 
                                        (select Id, first_name__c, last_name__c from Applicants__r)
                                   from PFS__c where Id = :pfsId];
                }
                else if(theCase !=null && String.isNotBlank(theCase.Id) && String.isNotBlank(theCase.Applicant__c) && String.isNotBlank(theCase.Applicant__r.PfS__c)){
                    pfsRecord = [select Id, Parent_A__r.Name, Name, Academic_Year_Picklist__c, 
                                        (select Id, first_name__c, last_name__c from Applicants__r)
                                   from PFS__c where Id = :theCase.Applicant__r.PfS__c];
                }
                else pfsRecord = new PFS__c();//NAIS-1973
            }
            return pfsRecord;
        }
        set;
    }

    public Case theCase {get; set;}
    
    private List<SelectOption> applicants_x = null;
    public List<SelectOption> applicants {
        get {
            if (this.applicants_x == null) {
                this.applicants_x = new List<SelectOption>();
                this.applicants_x.add(new SelectOption('', '--none--'));
                if(pfsRecord.applicants__r != null && pfsRecord.applicants__r.size() > 0){
                    for(Applicant__c app : pfsRecord.applicants__r) {
                       this.applicants_x.add(new SelectOption(app.Id, app.first_name__c+' '+app.Last_name__c));
                    }
                }
            }
            return this.applicants_x;
        }
    }
    
    
    public SchoolMessageController(ApexPages.StandardController caseController) {
        Map<String, String> params = ApexPages.currentPage().getParameters();
        
        this.caseController = caseController;
        theCase = (Case) caseController.getRecord(); // CH Fix  
        
        // CH Fix
        if(theCase.Id !=null)      
        {    
            caseId = theCase.Id; 
        }
        else{
            theCase = new Case();
            theCase.origin = 'School Portal';

            // If the accountId param has been set use that value on the new case.
            // If not, then use the user's current school.
            if (params.get('accountId') != null) {
                theCase.accountId = params.get('accountId');
            } else {
                theCase.accountId = GlobalVariables.getCurrentSchoolId();
            }
            
            if (params.get('contactId') != null) {
                theCase.contactId = params.get('contactId');
            }
            
            User loggedUser = [select Id, accountId, contactId, Current_School__c from User where Id = :UserInfo.getUserId()];
        
            List<Account> loggedSchools = AccountSelector.Instance
                .selectById(new Set<Id>{loggedUser.Current_School__c}, 
                            new Set<String>{'Id', 'Name', 'SSS_School_Code__c'});
                        
            if (!loggedSchools.isEmpty()) {
                
                theCase.School_Name__c = loggedSchools[0].Name;
                theCase.School_Code__c = loggedSchools[0].SSS_School_Code__c;
            }
            
            if (pfsId != null) {
                theCase.PFS__c = pfsId;
            }
        }
        
        theCase.RecordTypeId = params.get('RecordType');
    }    
    //NAIS-1973
    public pageReference cancelR(){
        PageReference p= new PageReference('/apex/'+retUrl); 
        p.setRedirect(true);  
        return p;
    }
    //End NAIS-1973
    public PageReference saveNew()
    {      
        try {
            if(!isSupport && this.theCase.Applicant__c==null && studentFolder.School_PFS_Assignments__r!=null 
            && !studentFolder.School_PFS_Assignments__r.isEmpty()){
                this.theCase.Applicant__c = studentFolder.School_PFS_Assignments__r[0].Applicant_ID__c;
            }
            //We run the dml without sharing to avoid exception with: INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY
            //insufficient access rights on cross-reference id: CONTACT_ID
            Dml.WithoutSharing.upsertObjects(new List<Case>{ theCase });
            
            caseId =theCase.Id; //NAIS-1973     
              
            // remove Comment NAIS-1973
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Message successfully saved.'));
        } catch (DMLException ex) {
            System.debug(LoggingLevel.ERROR, 'An error occured attempting to save the messge: ' + ex.getDMLMessage(0) );
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'An error occured attempting to save the messge: ' + ex.getDMLMessage(0)));
        }        
        
        // [CH] NAIS-1995 Updated to leverage the getter method
        
        return view(); //NAIS-1973
    }
    //NAIS-1973
    public PageReference edit()
    {
        return this.view(null);
    }
    
    public PageReference view()
    {
        return this.view('1');
    }
    
    public PageReference view(String mode)
    {
        PageReference p= new PageReference('/apex/SchoolMessage'); 
        p.getParameters().put('mode', mode);
        p.getParameters().put('id',caseId);         
        p.getParameters().put('retUrl', retUrl);
        p.getParameters().put('RecordType', recordType);
        if(!isSupport)
        {            
            p.getParameters().put('pfsId',pfsId);   
            p.getParameters().put('folderId',folderId); 
        }
        else
            p.getParameters().put('isSupport','1');
        
        //SFP-50, [G.S]
        if(fromPFS)
        {     
            p.getParameters().put('pfsId',pfsId);   
            p.getParameters().put('folderId',folderId);
            p.getParameters().put('fromPFS','1');
        }
        
        p.setRedirect(true);  
        return p;
    }
    
    //SFP-50, [G.S]
    public PageReference closeCase()
    {
        try{
            theCase.Status = 'Closed';
            update theCase;   
            caseId =theCase.Id; 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Message has been closed.'));
        } catch (DMLException ex) {
            System.debug(LoggingLevel.ERROR, 'An error occured attempting to close the messge: ' + ex.getDMLMessage(0) );
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'An error occured attempting to close the messge: ' + ex.getDMLMessage(0)));
        }   
        return view();
    } 
    
    public Pagereference uploadAttachment()
    {
        return null;
    }
    //End NAIS-1973 
    //NAIS-2472   
    public String getAcademicYearId(){
        Academic_Year__c ay = GlobalVariables.getAcademicYearByName(pfsRecord.Academic_Year_Picklist__c);
        return (ay == null ? null : ay.Id);
    }
    //End NAIS-2472 
}