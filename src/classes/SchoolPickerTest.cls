@isTest
private class SchoolPickerTest
{

    private static User schoolPortalUser;
    private static Account school1;
    private static Account school2;

    static {
        schoolPortalUser = UserTestData.insertSchoolPortalUser();


        school1 = AccountTestData.Instance
                .forName('Great School 1')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forSSSSchoolCode('123456780')
                .createAccountWithoutReset();
        school2 = AccountTestData.Instance
                .forName('Great School 2')
                .forSSSSchoolCode('123456781')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .createAccountWithoutReset();
        insert new List<Account> { school1, school2 };


        insert new List<Affiliation__c> {
            AffiliationTestData.Instance
                    .forType('Additional School User')
                    .forStatus('Current')
                    .forOrganizationId(school1.Id)
                    .createAffiliationWithoutReset(),

            AffiliationTestData.Instance
                    .forType('Additional School User')
                    .forStatus('Current')
                    .forOrganizationId(school2.Id)
                    .forRole('drewtest')
                    .createAffiliationWithoutReset()
        };
    }

    @isTest private static void testSchoolPickerController_validData_schoolSize() {
        System.runAs(schoolPortalUser) {
            Test.startTest();
                PageReference pr = Page.SchoolPicker;
                pr.getParameters().put('selectedSchoolId', school1.Id);
                Test.setCurrentPageReference(pr);

                SchoolPicker testController = new SchoolPicker();
                System.assertEquals(3, testController.schoolWrappers.size());

                testController.setInFocusSchoolAndRedirect();
            Test.stopTest();
        }
    }


    /*
    @isTest
    private static void testPickSchool(){
        td = new TestData();

        System.runAs(sysAdminUser){

            testAffs = new List<Affiliation__c>();
            testAffs.add(new Affiliation__c(Contact__c = td.staff1.Id, Type__c = 'Additional School User', Organization__c = td.school1.Id, Status__c = 'Current'));
            testAffs.add(new Affiliation__c(Contact__c = td.staff1.Id, Type__c = 'Additional School User', Organization__c = td.school2.Id, Status__c = 'Current', Role__c = 'drewtest'));
            testAffs.add(new Affiliation__c(Contact__c = td.staff1.Id, Type__c = 'WRONG TYPE', Organization__c = td.school2.Id, Status__c = 'Current', Role__c = 'drewtest'));

            insert testAffs;
        }

        System.runAs(td.PortalUser){
            Test.startTest();

                SchoolPicker testController = new SchoolPicker();

                System.assertEquals(2, testController.schoolWrappers.size());

                PageReference pr = Page.SchoolPicker;
                pr.getParameters().put('selectedSchoolId', testAffs[1].Organization__c);
                Test.setCurrentPage(pr);

                //testController.selectedSchoolId = testAffs[1].Organization__c;
                System.assertNotEquals(null, testController.setInFocusSchoolAndRedirect());
                System.assertEquals(td.School2.Id, [Select Id, In_Focus_School__c from User where Id = :td.PortalUser.Id][0].In_Focus_School__c);

            Test.stopTest();
        }
    }

    @isTest
    private static void testDashboardRedirectToThisPageNoInFocusSchool(){
        td = new TestData();

        System.runAs(sysAdminUser){

            testAffs = new List<Affiliation__c>();
            testAffs.add(new Affiliation__c(Contact__c = td.staff1.Id, Type__c = 'Additional School User', Organization__c = td.school1.Id, Status__c = 'Current'));
            testAffs.add(new Affiliation__c(Contact__c = td.staff1.Id, Type__c = 'Additional School User', Organization__c = td.school2.Id, Status__c = 'Current', Role__c = 'drewtest'));

            insert testAffs;

            // won't matter that we persist because in focus school is blank
            td.portalUser.Persist_School_Focus__c = true;
            td.portalUser.SYSTEM_Terms_and_Conditions_Accepted__c = System.now();
            update td.portalUser;
        }

        System.runAs(td.PortalUser){
            Test.startTest();
                // expect to redirect to school picker because this is a multi-school user without a school in focus
                SchoolDashboardController testController = new SchoolDashboardController();
                System.assert(testController.init().getURL().contains('schoolpicker'));
            Test.stopTest();
        }
    }

    @isTest
    private static void testDashboardNonRedirectWITHInFocusSchool(){
        td = new TestData();

        System.runAs(sysAdminUser){
            testAffs = new List<Affiliation__c>();
            testAffs.add(new Affiliation__c(Contact__c = td.staff1.Id, Type__c = 'Additional School User', Organization__c = td.school1.Id, Status__c = 'Current'));
            testAffs.add(new Affiliation__c(Contact__c = td.staff1.Id, Type__c = 'Additional School User', Organization__c = td.school2.Id, Status__c = 'Current', Role__c = 'drewtest'));

            insert testAffs;

            td.portalUser.In_Focus_School__c = td.School2.Id;
            td.portalUser.SYSTEM_Terms_and_Conditions_Accepted__c = System.now();
            update td.portalUser;
        }

        System.runAs(td.PortalUser){
            Test.startTest();
                // expect NOT to redirect to school picker because this is a multi-school user WITH a school in focus
                SchoolDashboardController testController = new SchoolDashboardController();
                System.assertEquals(null, testController.init());
            Test.stopTest();
        }
    }

    @isTest
    private static void testDashboardRedirectToThisPageOnLogin(){
        td = new TestData();

        System.runAs(sysAdminUser){
            testAffs = new List<Affiliation__c>();
            testAffs.add(new Affiliation__c(Contact__c = td.staff1.Id, Type__c = 'Additional School User', Organization__c = td.school1.Id, Status__c = 'Current'));
            testAffs.add(new Affiliation__c(Contact__c = td.staff1.Id, Type__c = 'Additional School User', Organization__c = td.school2.Id, Status__c = 'Current', Role__c = 'drewtest'));

            insert testAffs;

            td.portalUser.In_Focus_School__c = td.School2.Id;
            td.portalUser.SYSTEM_Terms_and_Conditions_Accepted__c = System.now();
            update td.portalUser;
        }

        System.runAs(td.PortalUser){
            Test.startTest();
                // expect to redirect to school picker because this is a multi-school user on login without persist set
                PageReference pr = Page.SchoolDashboard;
                pr.getParameters().put('login', 'true');
                Test.setCurrentPage(pr);

                SchoolDashboardController testController = new SchoolDashboardController();
                System.assert(testController.init().getURL().contains('schoolpicker'));
            Test.stopTest();
        }
    }

    @isTest
    private static void testNODashboardRedirectToThisPageOnLoginBecauseOfPersist(){
        td = new TestData();

        System.runAs(sysAdminUser){
            testAffs = new List<Affiliation__c>();
            testAffs.add(new Affiliation__c(Contact__c = td.staff1.Id, Type__c = 'Additional School User', Organization__c = td.school1.Id, Status__c = 'Current'));
            testAffs.add(new Affiliation__c(Contact__c = td.staff1.Id, Type__c = 'Additional School User', Organization__c = td.school2.Id, Status__c = 'Current', Role__c = 'drewtest'));

            insert testAffs;

            td.portalUser.In_Focus_School__c = td.School2.Id;
            td.portalUser.Persist_School_Focus__c = true;
            td.portalUser.SYSTEM_Terms_and_Conditions_Accepted__c = System.now();
            update td.portalUser;
        }

        System.runAs(td.PortalUser){
            Test.startTest();
                // expect NOT to redirect to school picker even on login because persist is set and in focus is set
                PageReference pr = Page.SchoolDashboard;
                pr.getParameters().put('login', 'true');
                Test.setCurrentPage(pr);

                SchoolDashboardController testController = new SchoolDashboardController();
                System.assertEquals(null, testController.init());
            Test.stopTest();
        }
    }
    */
}