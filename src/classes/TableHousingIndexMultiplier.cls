public with sharing class TableHousingIndexMultiplier
{
    
    private static Map<Id, Map<String, Decimal>> himByPurchaseYearByAcademicYear
                    = new Map<Id, Map<String, Decimal>>();
    private static Map<Id, Housing_Index_Multiplier__c> firstHimByAcademicYear
                    = new Map<Id, Housing_Index_Multiplier__c>();
    
    public static Decimal getHousingIndexMultiplier(Id academicYearId, String homePurchaseYear)
    {
        // see if the HIM data for the academic year has already been loaded
        Map<String, Decimal> himByPurchaseYear = himByPurchaseYearByAcademicYear.get(academicYearId);
        System.debug('TableHousingIndexMultiplier.getHousingIndexMultiplier.himByPurchaseYear: ' + himByPurchaseYear);
        if (himByPurchaseYear == null) {
            // HIM data for academic year hasn't already been loaded, need to read from db
            List<Housing_Index_Multiplier__c> himData = [SELECT Home_Purchase_Year__c, Housing_Index_Multiplier__c 
                                                            FROM Housing_Index_Multiplier__c
                                                            WHERE Academic_Year__c = :academicYearId
                                                            ORDER BY Home_Purchase_Year__c ASC];
            System.debug('TableHousingIndexMultiplier.getHousingIndexMultiplier.himData: ' + himData);
            if (!himData.isEmpty())    {                                        
                himByPurchaseYear = new Map<String, Decimal>();
                for (Housing_Index_Multiplier__c him : himData) {
                    himByPurchaseYear.put(him.Home_Purchase_Year__c, him.Housing_Index_Multiplier__c);
                }
                himByPurchaseYearByAcademicYear.put(academicYearId, himByPurchaseYear);
                firstHimByAcademicYear.put(academicYearId, himData[0]);
            }
        }
        
        // do the lookup
        Decimal himValue = null;
        System.debug('TableHousingIndexMultiplier.getHousingIndexMultiplier.himByPurchaseYear: ' + himByPurchaseYear);
        if (himByPurchaseYear != null) {
            himValue = himByPurchaseYear.get(homePurchaseYear);
            if (himValue == null) {
                // no match, but maybe the purchase year is less than the first year in the table
                Housing_Index_Multiplier__c firstHim = firstHimByAcademicYear.get(academicYearId);
                System.debug('TableHousingIndexMultiplier.getHousingIndexMultiplier.firstHim: ' + firstHim);
                if ((firstHim != null) && (firstHim.Home_Purchase_Year__c != null)) {
                    System.debug('TableHousingIndexMultiplier.getHousingIndexMultiplier.homePurchaseYear: ' + homePurchaseYear);
                    System.debug('TableHousingIndexMultiplier.getHousingIndexMultiplier.firstHim.Home_Purchase_Year__c: ' + firstHim.Home_Purchase_Year__c);
                    if ((homePurchaseYear != null) && (homePurchaseYear.length()>0) && (Integer.valueOf(homePurchaseYear) < Integer.valueOf(firstHim.Home_Purchase_Year__c))) {
                        // purchase year is less than the first year in the table, so use the HIM for the first year in the table
                        himValue = firstHim.Housing_Index_Multiplier__c;
                    }
                }
            }
        }
        
        if (himValue == null) {
            throw new EfcException.EfcMissingDataException('No HIM value found for academic year \'' + EfcUtil.getAcademicYearName(academicYearId) + '\' and Home Purchase Year = ' + homePurchaseYear);
        }
    
        return himValue; 
    }
}