/**
 * @description Handles Account Subscription Types.
 **/
public class SubscriptionTypes
{

    public static final String BASIC_TYPE = 'Basic';
    public static final String SUBSCRIPTION_FULL_TYPE = 'Full';
    public static final String EXTENDED_TYPE = 'Extended';
    public static final String PREMIUM_TYPE = 'Premium';

    @testVisible private static final String PORTAL_VERSION_FULL_TYPE = 'Full-featured';

    /**
     * @description Private parameterless constructor to keep this from
     *              being instantiated externally.
     */
    private SubscriptionTypes() { }

    /**
     * @description Converts the SSS_Subscription_Type_Current__c picklist value to
     *              the appropriate Portal_Version__c value.
     * @param sssSubscriptionType The value of the SSS_Subscription_Type_Current__c
     *        field being converted.
     * @returns A string value of the Portal_Version__c picklist value.
     */
    public String getPortalVersionSubscriptionType(String sssSubscriptionType)
    {
        String subscriptionType = null;

        if (sssSubscriptionType == SUBSCRIPTION_FULL_TYPE || sssSubscriptionType == PREMIUM_TYPE)
        {
            subscriptionType = PORTAL_VERSION_FULL_TYPE;
        }
        else if (sssSubscriptionType == BASIC_TYPE)
        {
            subscriptionType = BASIC_TYPE;
        }

        return subscriptionType;
    }

    /**
     * @description A Singleton instance of the SubscriptionTypes class.
     */
    public static SubscriptionTypes Instance {
        get {
            if (Instance == null) {
                Instance = new SubscriptionTypes();
            }
            return Instance;
        }
        private set;
    }
}