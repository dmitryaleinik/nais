/**
---   INITIAL DEVELOPMENT -- SFP-206, [G.S]
**/

global class BatchUpdateTuitionDependents implements Database.Batchable <sObject> {
    
    String oQuery = '';
    global BatchUpdateTuitionDependents(String query)
    {
        this.oQuery =  query;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(oQuery);
    } 
    
    //Execute the Batch 
    global void execute(Database.BatchableContext BC, List <PFS__c> pfsList)
    {
        Decimal dependentParentSources;
        List<PFS__c> updatePfsList = new List<PFS__c>();
        for(PFS__c pfs : pfsList){
            if( pfs.Dependents__r != null &&  pfs.Dependents__r.size() > 0){
                dependentParentSources = 0;
                for (Dependents__c dependent : pfs.Dependents__r) {
                    if(dependent.Dependent_1_Parent_Sources_Est__c != null){
                         dependentParentSources = dependentParentSources + dependent.Dependent_1_Parent_Sources_Est__c;
                    }
                     if(dependent.Dependent_2_Parent_Sources_Est__c != null){
                          dependentParentSources = dependentParentSources + dependent.Dependent_2_Parent_Sources_Est__c;
                     }
                    if(dependent.Dependent_3_Parent_Sources_Est__c != null){
                         dependentParentSources = dependentParentSources + dependent.Dependent_3_Parent_Sources_Est__c;
                    }
                      if(dependent.Dependent_4_Parent_Sources_Est__c != null){
                           dependentParentSources = dependentParentSources + dependent.Dependent_4_Parent_Sources_Est__c;
                    }
                       if(dependent.Dependent_5_Parent_Sources_Est__c != null){
                        dependentParentSources = dependentParentSources + dependent.Dependent_5_Parent_Sources_Est__c;
                    }
                    if(dependent.Dependent_6_Parent_Sources_Est__c != null){
                         dependentParentSources = dependentParentSources + dependent.Dependent_6_Parent_Sources_Est__c;
                     }
                }
                if(dependentParentSources > 0){
                    pfs.Tuition_Able_to_Pay_All_Dependents__c = dependentParentSources;
                    updatePfsList.add(pfs);
                }
            }
        }
        
        if(updatePfsList.size()>0){
            Database.update(updatePfsList);
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {
      
    }
    
}