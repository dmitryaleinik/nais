@isTest
private class SAOProfileIntegrationPostProcessTest {
    
    private static StaticResourceCalloutMock mockReponse;
    private static Academic_Year__c currentAcademicYear;
    private static Account school_1, school_2;
    private static Contact parentA;
    private static PFS__c pfs;
    
    
    private static final String INTEGRATION_SOURCE = 'sao';
    
    //Data from Static Resource "SAOProfilesMock"
    private static final String SAO_JSON_RESPONSE = 'SAOProfilesMock';
    private static final String SAO_JSON_APPLICATION_SCHOOL_1 = '1111';
    private static final String SAO_JSON_APPLICATION_SCHOOL_2 = '5902';
    private static final String SAO_JSON_SAO_ID_APPLICANT_1 = '170173536';
    private static final String SAO_JSON_FIRSTNAME_APPLICANT_1 = 'Test';
    private static final String SAO_JSON_LASTNAME_APPLICANT_1 = 'Student One';
    private static final Date   SAO_JSON_BIRTHDATE_APPLICANT_1 = date.parse('12/14/2005');
    private static final String SAO_JSON_GENDER_APPLICANT_1 = 'Female';
    private static final String SAO_JSON_CURRENT_GRADE_APPLICANT_1 = 'Grade8';
    private static final String SAO_JSON_SAO_ID_STUDENT_FOLDER_1 = 'da0dcec0-adaa-e611-99ae-000d3a1641e5';
    private static final String SAO_JSON_SAO_ID_STUDENT_FOLDER_2 = 'da0dcec0-adaa-e611-99ae-000000000000';
    
    private static void Setup() {
        
        //Create academic year, we must create the same AY that we defined in the staticResource.SAOProfilesMock
		currentAcademicYear = AcademicYearTestData.Instance
		  .forName('2017-2018')
		  .insertAcademicYear();

		//Create Schools.
		school_1 = AccountTestData.Instance.asSchool().create();
		school_1.NCES_ID__c = SAO_JSON_APPLICATION_SCHOOL_1;
        school_1.SSS_School_Code__c = SAO_JSON_APPLICATION_SCHOOL_1;
        school_1.SSS_Subscriber_Status__C = 'Current';
        
		school_2 = AccountTestData.Instance.asSchool().create();
        school_2.NCES_ID__c = SAO_JSON_APPLICATION_SCHOOL_2;
        school_2.SSS_School_Code__c = SAO_JSON_APPLICATION_SCHOOL_2;
        school_2.SSS_Subscriber_Status__C = 'Current';
        
        insert new List<Account>{school_1, school_2};

		//Create parentA.
		Contact parentA = ContactTestData.Instance.asParent().insertContact();

		//Create PFS record.
		pfs = PfsTestData.Instance
		    .forParentA(parentA.Id)
		    .forAcademicYearPicklist(currentAcademicYear.Name)
		    .insertPfs();
        
        //Create settings for SAO integration.
        insert new IntegrationSource__c(
            Name = INTEGRATION_SOURCE,
            BaseCalloutURL__c = 'http://api.piedpiper.com/v1/');
            
        //Specify a fake response for testing HTTP callouts.
        mockReponse = new StaticResourceCalloutMock();
        mockReponse.setStaticResource(SAO_JSON_RESPONSE);
        mockReponse.setStatusCode(200);
        mockReponse.setHeader('Content-Type', 'application/json');
    }
    
    @isTest
    private static void invoke_dataFromSAOProfilesMock_FieldsAreProperlyMerged() {
        
        Setup();
        
        //We make sure that we are only using the new "profile" endpoint.
        List<Integration_API_Setting__mdt> integrationProcess = new List<Integration_API_Setting__mdt>([
           SELECT Api_Endpoint__c, ApiName__c, DeveloperName, Id, IntegrationSource__c,
                   IsActive__c, IsFirstApi__c, Iterate_Over_Next_Call__c, Iterate_Over_With_Key__c,
                   Next_Api_Name__c, Next_Api_Parameters__c, PreProcessClass__c, PostProcessClass__c 
           FROM Integration_API_Setting__mdt 
           WHERE IsActive__c = true 
           AND IntegrationSource__c = :INTEGRATION_SOURCE + '_Sandbox']);
        
        System.AssertEquals(1, integrationProcess.size());
        System.AssertEquals('Profiles', integrationProcess[0].ApiName__c);
        System.AssertEquals(true, integrationProcess[0].IsFirstApi__c);
        
        Test.startTest();
        
            Test.setMock(HttpCalloutMock.class, mockReponse);
            try {
                
                IntegrationApiService.Instance.executeIntegrationApis(pfs, INTEGRATION_SOURCE);
                
            } catch ( IntegrationApiService.IntegrationConfigurationException e) {
                
                System.assertEquals(false, true, e + ' ' + e.getStackTraceString());
            }
            
        Test.stopTest();
        
        //Applicants
        Applicant__c applicant = [
	        SELECT Id, First_Name__c, Last_Name__c, Birthdate_New__c, Gender__c, Current_Grade__c, SaoId__c  
	        FROM Applicant__c 
	        WHERE SaoId__c =: SAO_JSON_SAO_ID_APPLICANT_1 
	        LIMIT 1];
        
        System.assertNotEquals(null, applicant);
        System.assertEquals(SAO_JSON_FIRSTNAME_APPLICANT_1, applicant.First_Name__c);
        System.assertEquals(SAO_JSON_LASTNAME_APPLICANT_1, applicant.Last_Name__c);
        System.assertEquals(SAO_JSON_BIRTHDATE_APPLICANT_1, applicant.Birthdate_New__c);
        System.assertEquals(SAO_JSON_GENDER_APPLICANT_1, applicant.Gender__c);
        System.assertEquals(SAO_JSON_CURRENT_GRADE_APPLICANT_1.replace('Grade',''), applicant.Current_Grade__c);
        System.assertEquals(SAO_JSON_SAO_ID_APPLICANT_1, applicant.SaoId__c);
        
        //Student Folders: There's must be 2, one for each school.
        Student_Folder__c studentFolder_1 = [
            SELECT Id, School__r.NCES_ID__c, SaoId__c 
            FROM Student_Folder__c  
            WHERE SaoId__c =: SAO_JSON_SAO_ID_STUDENT_FOLDER_1 
            LIMIT 1];
        
        System.assertNotEquals(null, studentFolder_1);
        System.assertEquals(SAO_JSON_APPLICATION_SCHOOL_1, studentFolder_1.School__r.NCES_ID__c);
        
        Student_Folder__c studentFolder_2 = [
            SELECT Id, School__r.NCES_ID__c, SaoId__c 
            FROM Student_Folder__c  
            WHERE SaoId__c =: SAO_JSON_SAO_ID_STUDENT_FOLDER_2 
            LIMIT 1];
        
        System.assertNotEquals(null, studentFolder_2);
        System.assertEquals(SAO_JSON_APPLICATION_SCHOOL_2, studentFolder_2.School__r.NCES_ID__c);
        
        //School PFS Assignment
        School_PFS_Assignment__c spa_1 = [
            SELECT Id, SaoId__c, School__r.NCES_ID__c 
            FROM School_PFS_Assignment__c 
            WHERE SaoId__c =: SAO_JSON_SAO_ID_STUDENT_FOLDER_1 
            LIMIT 1];
        
        System.assertNotEquals(null, spa_1);
        System.assertEquals(SAO_JSON_APPLICATION_SCHOOL_1, spa_1.School__r.NCES_ID__c);
        
        School_PFS_Assignment__c spa_2 = [
            SELECT Id, SaoId__c, School__r.NCES_ID__c 
            FROM School_PFS_Assignment__c 
            WHERE SaoId__c =: SAO_JSON_SAO_ID_STUDENT_FOLDER_2 
            LIMIT 1];
        
        System.assertNotEquals(null, spa_2);
        System.assertEquals(SAO_JSON_APPLICATION_SCHOOL_2, spa_2.School__r.NCES_ID__c);
        
    }
}