@isTest
private class StudentFolderListControllerTest {

	private static void init()
	{
		List<Student_Folder_List__c> sfls = new List<Student_Folder_List__c>{		
						new Student_Folder_List__c(
							Name = 'Status/Demographics',
							Tab_Name__c = 'Test Tab 1',
							Report_URL__c = 'Test Report Name 1'
						),						
						new Student_Folder_List__c(
							Name = 'Updates/Awards',
							Tab_Name__c = 'Test Tab 2',
							Report_URL__c = 'Test Report Name 2'
						),
						new Student_Folder_List__c(
							Name = 'Financials/Analysis',
							Tab_Name__c = 'Test Tab 3',
							Report_URL__c = 'Test Report Name 3'
						),
						new Student_Folder_List__c(
							Name = 'Business/Farm Owners',
							Tab_Name__c = 'Test Tab 4',
							Report_URL__c = 'Test Report Name 4'
						),
						new Student_Folder_List__c(
							Name = 'Multiple Campus',
							Tab_Name__c = 'Test Tab 5',
							Report_URL__c = 'Test Report Name 5'
						),
						new Student_Folder_List__c(
							Name = 'Updates/Awards without Budget Group',
							Tab_Name__c = 'Test Tab 6',
							Report_URL__c = 'Test Report Name 6'
						),
						new Student_Folder_List__c(
							Name = 'Basic View',
							Tab_Name__c = 'Test Tab 7',
							Report_URL__c = 'Test Report Name 7'
						),
						new Student_Folder_List__c(
							Name = 'Non Need',
							Tab_Name__c = 'Test Tab 8',
							Report_URL__c = 'Test Report Name 8'
						)
			};
		insert sfls;
	}//End:init

	@isTest
	private static void testStudentFolderListController(){
		init();
		StudentFolderListController sflc = new StudentFolderLIstController();
		
		system.assertEquals('Status/Demographics', sflc.statDemo.Name);
		system.assertEquals('Test Tab 1', sflc.statDemo.Tab_Name__c);
		system.assertEquals('Test Report Name 1', sflc.statDemo.Report_URL__c);
		
		system.assertEquals('Updates/Awards without Budget Group', sflc.updateAward.Name);
		system.assertEquals('Test Tab 6', sflc.updateAward.Tab_Name__c);
		system.assertEquals('Test Report Name 6', sflc.updateAward.Report_URL__c);
		
		system.assertEquals('Financials/Analysis', sflc.finAnalysis.Name);
		system.assertEquals('Test Tab 3', sflc.finAnalysis.Tab_Name__c);
		system.assertEquals('Test Report Name 3', sflc.finAnalysis.Report_URL__c);
		
		system.assertEquals('Business/Farm Owners', sflc.busFarm.Name);
		system.assertEquals('Test Tab 4', sflc.busFarm.Tab_Name__c);
		system.assertEquals('Test Report Name 4', sflc.busFarm.Report_URL__c);
		
		system.assertEquals('Multiple Campus', sflc.multCamp.Name);
		system.assertEquals('Test Tab 5', sflc.multCamp.Tab_Name__c);
		system.assertEquals('Test Report Name 5', sflc.multCamp.Report_URL__c);
		
		system.assertEquals(false, sflc.multiCampus);
		system.assertEquals(false, StudentFolderListController.getIsLimitedSchoolView());
		system.assertEquals(null, sflc.searchTerm);
		system.assertEquals('Updates/Awards without Budget Group', sflc.updateAward.Name);
	}//End:testStudentFolderListController

	@isTest
	private static void testStudentFolderListControllerSchoolWithBudgetGroup() {
		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		User schoolPortalUser;
		Account school;
		
		System.runAs(thisUser){
			school = TestUtils.createAccount('School', RecordTypes.schoolAccountTypeId, 3, true);
			Contact staff1 = TestUtils.createContact('staff1', school.Id, RecordTypes.schoolStaffContactTypeId, true);
			TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
			
			schoolPortalUser = TestUtils.createPortalUser('User 1', 'u1@test.org', 'u1', staff1.Id, GlobalVariables.schoolPortalAdminProfileId, true, true);
		}
    	TestUtils.insertAccountShare(school.Id, schoolPortalUser.Id);
    	Test.startTest();
    	init();
    	System.runAs(schoolPortalUser){
    		insert new Budget_Group__c(Name = 'Budget 1', Academic_Year__c = GlobalVariables.getCurrentAcademicYear().Id, School__c = school.Id, Total_Funds__c = 5000);

    		Test.setCurrentPage(new ApexPages.Pagereference('/apex/StudentFolderList?q=test'));
    		StudentFolderListController sflc = new StudentFolderLIstController();
    		system.assertEquals('test', sflc.searchTerm);
			system.assertEquals('Updates/Awards', sflc.updateAward.Name);
    	}
    	Test.stopTest();
	}//End:testStudentFolderListControllerSchoolWithBudgetGroup
}