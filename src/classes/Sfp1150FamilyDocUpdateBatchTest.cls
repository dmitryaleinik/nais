@isTest
private class Sfp1150FamilyDocUpdateBatchTest {

    private static void assertVerificationRequestedDate(Family_Document__c docToCheck, Date expectedDate) {
        Family_Document__c record = [SELECT Id, Verification_Requested_Date__c FROM Family_Document__c WHERE Id = :docToCheck.Id LIMIT 1];

        if (expectedDate == null) {
            System.assertEquals(expectedDate, record.Verification_Requested_Date__c, 'Expected the correct date.');
        } else {
            System.assertNotEquals(null, record.Verification_Requested_Date__c, 'Expected date to not be null.');
            System.assertEquals(expectedDate, record.Verification_Requested_Date__c.date(), 'Expected the correct date.');
        }
    }

    @isTest
    private static void execute_forMailedDocs_1040Doc_verificationNotRequested_expectVerificationDateUpdated() {
        String docType = '1040';
        String academicYear = '2017-2018';
        String docYear = '2016';
        String docSource = FamilyDocumentTestData.DOC_SOURCE_MAILED;
        String docStatus = FamilyDocuments.DOC_STATUS_PROCESSED;

        Date expectedDate = System.today();

        Family_Document__c familyDoc = FamilyDocumentTestData.Instance
                .forDocumentType(docType)
                .forDocYear(docYear)
                .forAcademicYearPicklist(academicYear)
                .isProcessed()
                .withSource(docSource)
                .verificationRequestedOn(null)
                .withDateVerified(expectedDate)
                .insertFamilyDocument();

        School_PFS_Assignment__c pfsAssignment = SchoolPfsAssignmentTestData.Instance.forAcademicYearPicklist(academicYear).insertSchoolPfsAssignment();

        SchoolDocumentAssignmentTestData.Instance.verifyData().forPfsAssignment(pfsAssignment.Id).forDocument(familyDoc.Id).forDocumentYear(docYear).insertSchoolDocumentAssignment();

        Set<String> academicYears = new Set<String> { academicYear };
        Set<String> documentTypes = new Set<String> { docType };
        Set<String> documentStatuses = new Set<String> { docStatus };

        Sfp1150FamilyDocUpdateBatch updateBatch = new Sfp1150FamilyDocUpdateBatch(academicYears, true, documentTypes, documentStatuses);

        Test.startTest();
        Database.executeBatch(updateBatch);
        Test.stopTest();

        assertVerificationRequestedDate(familyDoc, expectedDate);
    }

    @isTest
    private static void execute_forUploadedDoc_1040Doc_verificationNotRequested_expectVerificationDateUpdated() {
        String docType = '1040';
        String academicYear = '2017-2018';
        String docYear = '2016';
        String docSource = FamilyDocumentTestData.DOC_SOURCE_PARENT_UPLOAD;
        String docStatus = FamilyDocuments.DOC_STATUS_PROCESSED;

        Date expectedDate = System.today();

        Family_Document__c familyDoc = FamilyDocumentTestData.Instance
                .forDocumentType(docType)
                .forDocYear(docYear)
                .forAcademicYearPicklist(academicYear)
                .isProcessed()
                .withSource(docSource)
                .verificationRequestedOn(null)
                .withDateVerified(expectedDate)
                .insertFamilyDocument();

        School_PFS_Assignment__c pfsAssignment = SchoolPfsAssignmentTestData.Instance.forAcademicYearPicklist(academicYear).insertSchoolPfsAssignment();

        SchoolDocumentAssignmentTestData.Instance.verifyData().forPfsAssignment(pfsAssignment.Id).forDocument(familyDoc.Id).forDocumentYear(docYear).insertSchoolDocumentAssignment();

        Set<String> academicYears = new Set<String> { academicYear };
        Set<String> documentTypes = new Set<String> { docType };
        Set<String> documentStatuses = new Set<String> { docStatus };

        Sfp1150FamilyDocUpdateBatch updateBatch = new Sfp1150FamilyDocUpdateBatch(academicYears, false, documentTypes, documentStatuses);

        Test.startTest();
        Database.executeBatch(updateBatch);
        Test.stopTest();

        assertVerificationRequestedDate(familyDoc, expectedDate);
    }

    @isTest
    private static void execute_forUploadedDoc_receivedNotProcessed_1040Doc_verificationNotRequested_expectVerificationDateUpdated() {
        String docType = '1040';
        String academicYear = '2017-2018';
        String docYear = '2016';
        String docSource = FamilyDocumentTestData.DOC_SOURCE_PARENT_UPLOAD;
        String docStatus = FamilyDocuments.DOC_STATUS_RECEIVED;

        Date expectedDate = System.today();

        Family_Document__c familyDoc = FamilyDocumentTestData.Instance
                .forDocumentType(docType)
                .forDocYear(docYear)
                .forAcademicYearPicklist(academicYear)
                .withStatus(docStatus)
                .withSource(docSource)
                .verificationRequestedOn(null)
                .withDateVerified(expectedDate)
                .insertFamilyDocument();

        School_PFS_Assignment__c pfsAssignment = SchoolPfsAssignmentTestData.Instance.forAcademicYearPicklist(academicYear).insertSchoolPfsAssignment();

        SchoolDocumentAssignmentTestData.Instance.verifyData().forPfsAssignment(pfsAssignment.Id).forDocument(familyDoc.Id).forDocumentYear(docYear).insertSchoolDocumentAssignment();

        Set<String> academicYears = new Set<String> { academicYear };
        Set<String> documentTypes = new Set<String> { docType };
        Set<String> documentStatuses = new Set<String> { docStatus };

        Sfp1150FamilyDocUpdateBatch updateBatch = new Sfp1150FamilyDocUpdateBatch(academicYears, false, documentTypes, documentStatuses);

        Test.startTest();
        Database.executeBatch(updateBatch);
        Test.stopTest();

        assertVerificationRequestedDate(familyDoc, expectedDate);
    }

    @isTest
    private static void execute_forMailedDoc_receivedNotProcessed_1040Doc_verificationNotRequested_expectVerificationDateUpdated() {
        String docType = '1040';
        String academicYear = '2017-2018';
        String docYear = '2016';
        String docSource = FamilyDocumentTestData.DOC_SOURCE_MAILED;
        String docStatus = FamilyDocuments.DOC_STATUS_RECEIVED;

        Date expectedDate = System.today();

        Family_Document__c familyDoc = FamilyDocumentTestData.Instance
                .forDocumentType(docType)
                .forDocYear(docYear)
                .forAcademicYearPicklist(academicYear)
                .withStatus(docStatus)
                .withSource(docSource)
                .verificationRequestedOn(null)
                .withDateVerified(expectedDate)
                .insertFamilyDocument();

        School_PFS_Assignment__c pfsAssignment = SchoolPfsAssignmentTestData.Instance.forAcademicYearPicklist(academicYear).insertSchoolPfsAssignment();

        SchoolDocumentAssignmentTestData.Instance.verifyData().forPfsAssignment(pfsAssignment.Id).forDocument(familyDoc.Id).forDocumentYear(docYear).insertSchoolDocumentAssignment();

        Set<String> academicYears = new Set<String> { academicYear };
        Set<String> documentTypes = new Set<String> { docType };
        Set<String> documentStatuses = new Set<String> { docStatus };

        Sfp1150FamilyDocUpdateBatch updateBatch = new Sfp1150FamilyDocUpdateBatch(academicYears, true, documentTypes, documentStatuses);

        Test.startTest();
        Database.executeBatch(updateBatch);
        Test.stopTest();

        assertVerificationRequestedDate(familyDoc, expectedDate);
    }

    @isTest
    private static void execute_allParamsNull_expectBatchRunsWithoutError() {
        // This test is just giving us a little more code coverage.
        Set<String> academicYears = null;
        Set<String> documentTypes = null;
        Set<String> documentStatuses = null;

        Sfp1150FamilyDocUpdateBatch updateBatch = new Sfp1150FamilyDocUpdateBatch(academicYears, true, documentTypes, documentStatuses);

        Test.startTest();
        Database.executeBatch(updateBatch);
        Test.stopTest();
    }
}