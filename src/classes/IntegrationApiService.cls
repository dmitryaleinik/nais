/**
 * IntegrationApiService.cls
 *
 * @description: Integration Service to handle, making calls to external apis,
 * Process applications for users.
 *
 * @author: Mike Havrillla @ Presence PG
 */

public class IntegrationApiService {

    /**
     * @description Singleton instance of the IntegrationApiService.
     */
    public static IntegrationApiService Instance {
        get {
            if (Instance == null) {
                Instance = new IntegrationApiService();
            }
            return Instance;
        }
        private set;
    }

    // constants
    private final String PARAM_DELIMITER = ':';

    private IntegrationApiService() {}

    public IntegrationApiService.Response executeIntegration( IntegrationApiService.Request integrationRequest) {

        IntegrationApiService.Response integrationResponse = new IntegrationApiService.Response();
        IntegrationApiFactory.Response response;
        try {

            IntegrationApiFactory.Request request = new IntegrationApiFactory.Request( integrationRequest.source, integrationRequest.pfs);
            response = IntegrationApiFactory.Instance.getAPIModels( request);

            IntegrationApiModel.ApiState apiState = new IntegrationApiModel.ApiState( integrationRequest.source, integrationRequest.pfs, response.integrationModels);

            for ( IntegrationApiModel integrationModel : response.integrationModels.values()) {

                if ( integrationModel.isEntryApi) this.makeCallouts( integrationModel, response.integrationModels, apiState);
            }

            integrationResponse.success = true;
            integrationResponse.messageToDisplay = System.Label.Integration_Complete_Message + ' ' + response.apiDisplayName;

        } catch ( Exception e) {
            System.debug('DEBUG:::Error Processing integration: ' + e + ' ' + e.getStackTraceString());
            IntegrationUtils.writeToIntegrationLog( e.getStackTraceString(), e.getMessage(), 'User ' + String.valueOf( UserInfo.getUserId()), integrationRequest.source);

            integrationResponse.messageToDisplay = System.Label.Integration_Error_Message;
            if( response != null ) {

                integrationResponse.messageToDisplay += ' ' + response.apiDisplayName;
            }
            integrationResponse.messageToDisplay += ' Message:: ' +  e.getMessage();

        }

        return integrationResponse;
    }

    public void executeIntegrationApis( PFS__c pfs, String source) {

        IntegrationApiFactory.Request request = new IntegrationApiFactory.Request( source, pfs);
        IntegrationApiFactory.Response response = IntegrationApiFactory.Instance.getAPIModels( request);

        IntegrationApiModel.ApiState apiState = new IntegrationApiModel.ApiState( source, pfs, response.integrationModels);

        for ( IntegrationApiModel integrationModel : response.integrationModels.values()) {

            if ( integrationModel.isEntryApi) this.makeCallouts( integrationModel, response.integrationModels, apiState);
        }
    }

    private void makeCallouts( IntegrationApiModel integrationModel, Map<String, IntegrationApiModel> integrationModelsByNameMap, IntegrationApiModel.ApiState apiState) {

        integrationModel.preProcess( apiState);
        apiState.currentApi = integrationModel.apiName;

        Map<String, String> endpointToParamMap = new Map<String, String>();

        // We store the endpoints in a set so that we don't call the same endpoint twice. This could happen if a family has multiple applicants in the same school.
        // Instead of calling the schools endpoint twice for the same school, we will only call it once.
        Set<String> endpoints = new Set<String>();
        // hasParams is true and the httpParmsByApiMap contains the current apiName
        // Build out endpoints with those params. (i.e Map<Students, {11111:22222}, {33333:4444}>)
        if ( apiState.httpParamsByApi.containsKey( integrationModel.apiName)) {

            if ( apiState.httpParamsByApi.get( integrationModel.apiName) != null && apiState.httpParamsByApi.get( integrationModel.apiName).size() != 0 ) {

                // loop over each item in the list of params by apiName (split based on delimeter type)
                // and format them into endpoint. (i.e. reqParam = '11111:22222'
                // and apiEndpoint = '/api/v1/test/{0}/{1}');
                // The resulting endpoint would be: /api/v1/test/11111/22222 after string.format.
                for ( String reqParam : apiState.httpParamsByApi.get( integrationModel.apiName)) {
                    System.debug('DEBUG:::buildEndpoints' + reqParam);
                    if ( reqParam == null) {

                        System.debug('DEBUG:::reqParam is null');
                    }

                    String formattedEndpoint = String.format( integrationModel.apiBaseUrl + integrationModel.apiEndpoint, reqParam.split( PARAM_DELIMITER));
                    endpoints.add( formattedEndpoint);

                    // add to the map of endpoints toparms, for querying later.
                    endpointToParamMap.put( formattedEndpoint, reqParam);
                }
            }
        } else {

            // if there are no parameters and the apiEndpoint is static,
            // then add that to the collection of endpoints. (Will always end up being one.)
            endpoints.add( integrationModel.apiBaseUrl + integrationModel.apiEndpoint);
        }

        // Start looping over the collection of endpoints.
        for ( String endpoint : endpoints) {
            System.debug('&&& endpoint: ' + endpoint);
            // Make the HTTP callout, and return a list of objects.
            // This uses JSON.deserialized Untyped for a response.
            // (NOTE: the other option here, is we can implement a Class, that builds out the datastructure if needed);
            // And deserialize into a typed class.
            if( apiState.authToken == null ) {
                apiState.authToken = IntegrationUtils.getUserAuthToken( apiState.apiSource, 'Open ID Connect');
            }
            Object httpResponse = this.makeRequest( endpoint, integrationModel.apiName, apiState.authToken);

            // Add the object response to a list based on APIName, so we can process the list as needed
            // I.e. Map<Students, List<Objects>> where eobjects is the response from the webservice callout.
            this.addResponseToMap( apiState.httpResponseByApi, integrationModel.apiName, httpResponse);
            if( endpointToParamMap.containsKey( endpoint)) {

                this.addResponseToMapByParams( apiState.httpResponseByParamMap, endpointToParamMap.get( endpoint), httpResponse);
            }

            // As long as the nextAPI name is not null, let's build up the collections for that callout.
            if ( String.isNotBlank( integrationModel.nextApiName)) {

                List<String> params = new List<String>();
                // If NextApiParameters is not null, then we know we need to parse some data out of here.
                // nextApiParamets should be in a delimited format that reference fields from the previous call.
                // (i.e. studentId:applicationId)
                if ( String.isNotBlank( integrationModel.nextApiParameters)) {

                    // split each one of the values in nextApiParameters, into a list.
                    params = integrationModel.nextApiParameters.split( PARAM_DELIMITER);
                }

                // The : is used to distinguish between multiple params used in a single request.
                List<String> nextParams = new List<String>();
                if ( apiState.httpParamsByApi.containsKey( integrationModel.nextApiName)) {

                    // If the response had mutilple objects, it's possible we have a list
                    // of items that we need to add to the collection of params for the next callout.
                    nextParams = apiState.httpParamsByApi.get( integrationModel.nextApiName);
                }

                System.debug('&&& Current API: ' + integrationModel.apiName);
                System.debug('&&& Next API: ' + integrationModel.nextApiName);
                System.debug('&&& Next API Parameters: ' + integrationModel.nextApiParameters);
                // Check to see if the response is an instance of map, or list, then handle it.
                // We grab the values from the response that we need to pass as parameters to the next endpoints.
                if( httpResponse instanceof Map<String, Object>) {
                    System.debug('&&& Single Response: Get params from map');
                    // Add them to a list of Strings, in the same delimeted format
                    // (i.e. 9999:112343)
                    List<String> futureParams = this.getFutureParams( httpResponse, params);
                    nextParams.add(String.join(futureParams, PARAM_DELIMITER));
                } else if( httpResponse instanceof List<Object>) {
                    System.debug('&&& Multiple Responses, get params from list');
                    for( Object o : (List<Object>)httpResponse) {
                        nextParams.add(String.join(this.getFutureParams(o, params), PARAM_DELIMITER));
                    }
                }

                System.debug('DEBUG:::integrationModel.nextApiName: ' + integrationModel.nextApiName + ' params: ' + nextParams );
                apiState.httpParamsByApi.put( integrationModel.nextApiName, nextParams);
            }
        }

        integrationModel.postProcess( apiState);
        //System.debug('DEBUG:::integrationModel.postProcess' + apiState);
        // double check one more time, if nextApiName is not null, then recursively call this process.
        if ( String.isNotBlank(integrationModel.nextApiName)) {

            this.makeCallouts( apiState.integrationModelByNameMap.get( integrationModel.nextApiName), integrationModelsByNameMap, apiState);
        }
    }

    private List<String> getFutureParams( Object responseObject, List<String> params) {

        List<String> futureParams = new List<String>();
        Map<String, Object> responseValues = ( Map<String, Object>)responseObject;
        // loop over the supplied params and get those values from teh map.
        for ( String param : params) {

            String paramValue = String.valueOf( IntegrationUtils.getValuesFromObject( responseValues, param));
            if( paramValue == null) {

                paramValue = 'none';
            }
            futureParams.add( paramValue);
        }

        System.debug('&&& future params from responseObject: ' + futureParams);
        return futureParams;
    }

    private Object makeRequest( String httpRequest, String apiName, String authToken) {
        final String AUTHORIZATION_KEY = 'Authorization';
        final String CONTENT_TYPE_KEY = 'Content-Type';
        final String APP_JSON_VALUE = 'application/json';

        Object returnVal;

        Map<String, String> httpHeaders = new Map<String, String>();
        httpHeaders.put( AUTHORIZATION_KEY, 'Bearer ' + authToken);
        httpHeaders.put( CONTENT_TYPE_KEY, APP_JSON_VALUE);

        returnVal = this.getObjectFromHttpRequest( httpRequest, httpHeaders, null, HttpService.HTTP_GET);

        return returnVal;
    }

    public Object getObjectFromHttpRequest( String httpEndpoint, Map<String, String> httpHeaders, String httpBody, String httpMethod) {

        Object returnVal;
        String httpResponse = HttpService.sendHttpRequest( httpEndpoint, httpHeaders, httpBody, HttpService.HTTP_GET);
        System.debug('DEBUG:::InegrationApiService.httpResponse httpEndpoint = ' + httpEndpoint);
        //System.debug('DEBUG:::InegrationApiService.httpResponse httpHeaders = ' + httpHeaders);
        System.debug('DEBUG:::InegrationApiService.httpResponse httpBody = ' + httpBody);
        //System.debug('DEBUG:::InegrationApiService.httpResponse httpMethod = ' + HttpService.HTTP_GET);
        System.debug('DEBUG:::InegrationApiService.httpResponse' + httpResponse);

        if( String.isNotBlank( httpResponse)) {

            returnVal  = (Object)JSON.deserializeUntyped( httpResponse);
        } else {

            throw new IntegrationException( 'No Data Available. Endpoint:' + httpEndpoint);
        }

        if( returnVal != null && returnVal instanceof List<Object>) {

            returnVal = (List<Object>)JSON.deserializeUntyped( httpResponse);
        } else if( returnVal != null && returnVal instanceof Map<String, Object> ) {

            returnVal = (Map<String, Object>)JSON.deserializeUntyped( httpResponse);
        } else {

            throw new IntegrationException( 'Unable to Parse Data Not in the correct format: ' + httpEndpoint);
        }

        return returnVal;
    }

    private void addResponseToMap( Map<String, List<Object>> httpResponseByApi, String apiName, Object responseToAdd) {

        List<Object> responseList = new List<Object>();
        if ( httpResponseByApi.containsKey( apiName)) {

            responseList = httpResponseByApi.get( apiName);
        }

        // Need to check to see if the response is a List<Objects> if they are
        // Parse and add each object to the list.
        if ( responseToAdd instanceof List<Object>) {

          for ( Object o : (List<Object>)responseToAdd) {
              responseList.add( o);
          }
        } else {

            responseList.add( responseToAdd);
        }

        httpResponseByApi.put( apiName, responseList);
    }

    private void addResponseToMapByParams( Map<String, List<Object>> httpResponseByParamMap, String paramKey, Object responseToAdd) {

        List<Object> responseList = new List<Object>();
        if ( httpResponseByParamMap.containsKey( paramKey)) {

            responseList = httpResponseByParamMap.get( paramKey);
        }

        // Need to check to see if the response is a List<Objects> if they are
        // Parse and add each object to the list.
        if ( responseToAdd instanceof List<Object>) {

            for ( Object o : (List<Object>)responseToAdd) {
                responseList.add( o);
            }
        } else {
            responseList.add( responseToAdd);
        }

        httpResponseByParamMap.put( paramKey, responseList);
    }

    public class Request {
        public String source;
        public String sourceName;
        public PFS__c pfs;

        public Request( String source, PFS__c pfs) {
            this.source = source.toLowerCase();
            this.sourceName = source;
            this.pfs = pfs;
        }
    }

    public class Response {

        public String messageToDisplay { get; set; }
        public Boolean success { get; set; }

        public Response() {
            this.success = false;
        }
    }


    public class IntegrationException extends Exception {}
    public class ApplicantExistsException extends Exception {}
    public class IntegrationConfigurationException extends Exception { }
}