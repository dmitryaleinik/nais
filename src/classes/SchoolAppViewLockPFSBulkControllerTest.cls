@isTest
private class SchoolAppViewLockPFSBulkControllerTest
{

    @isTest
    private static void test1()
    {
        // create a new PFS
        List<Academic_Year__c> academicYears = TestUtils.createAcademicYears();
        Account testAccount = TestUtils.createAccount('Test Account', RecordTypes.individualAccountTypeId, 5, true);
        Contact theParent = TestUtils.createContact('Parent A', testAccount.Id, RecordTypes.parentContactTypeId, true);
        User parentAUser = TestUtils.createPortalUser('Test User', 'test@parentAUser.com', 'TestP', theParent.Id, GlobalVariables.familyPortalProfileId, true, true);                 
        PFS__c thePFS = TestUtils.createPFS('Test PFS', academicYears[0].id, theParent.id, true);
        thePFS.pfs_status__c = 'Application Submitted';
        thePFS.Payment_Status__c = 'Paid in Full';
        update thePFS;
        Contact theStudent = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, true);
        
        // create applicant 
        Applicant__c theApplicant = TestUtils.createApplicant(theStudent.Id, thePFS.Id, true);
        Student_Folder__c theStudentFolder = TestUtils.createStudentFolder('Student Folder 1', academicYears[0].id, theStudent.Id, true);
        Account theSchool = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, null, true);
        
        // Create School assignment 
        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYears[0].id, theApplicant.Id, theSchool.Id, theStudentFolder.Id, true);
        spfsa1.Family_May_Submit_Updates__c = 'Yes';
        
        Test.startTest();
        update spfsa1;
        PageReference pageRef = Page.SchoolAppViewLockPFSBulk;
        Test.setCurrentPage(pageRef);        
        list<Student_Folder__c> folders = new list<Student_Folder__c>();
        folders.add(theStudentFolder);
        System.assertEquals(folders.size(), 1);
        ApexPages.StandardSetController controllerMain = new ApexPages.StandardSetController(folders);
        controllerMain.setSelected(folders);
        SchoolAppViewLockPFSBulkController controller = new SchoolAppViewLockPFSBulkController(controllerMain);
        controller.lockSelectedPFS();
        System.assertEquals(controller.totalRecordsProcessed, 1);
        System.assertEquals('No',[Select Family_May_Submit_Updates__c from School_PFS_Assignment__c where Id=:spfsa1.Id limit 1].Family_May_Submit_Updates__c);
        controller.lockSelectedPFS();
        Test.stopTest();    
    }
    
    @isTest
    private static void testBatch()
    {
        Id batchId;
        Account theSchool = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, 100, true);
        Contact portalContact = TestUtils.createContact('SchoolStaff', theSchool.Id, RecordTypes.schoolStaffContactTypeId, true);
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User portalUser = TestUtils.createPortalUser(portalContact.LastName, 
                                                'SchoolStaff@unittest.com', 
                                                'alias', 
                                                portalContact.Id, 
                                                GlobalVariables.schoolPortalAdminProfileId, true, false);
        System.runAs(thisUser){
            insert portalUser;
        }
        
        System.runAs(portalUser){
            // create a new PFS
            List<Academic_Year__c> academicYears = TestUtils.createAcademicYears();
            Contact theParent = TestUtils.createContact('Parent A', theSchool.Id, RecordTypes.parentContactTypeId, true);
            User parentAUser = TestUtils.createPortalUser('Test User', 'test@parentAUser.com', 'TestP', theParent.Id, GlobalVariables.familyPortalProfileId, true, true);                 
            PFS__c thePFS = TestUtils.createPFS('Test PFS', academicYears[0].id, theParent.id, true);
            Contact theStudent = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, true);
            
            // create applicant 
            Applicant__c theApplicant = TestUtils.createApplicant(theStudent.Id, thePFS.Id, true);
            Student_Folder__c theStudentFolder = TestUtils.createStudentFolder('Student Folder 1', academicYears[0].id, theStudent.Id, true);
            
            // Create School assignment 
            School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(GlobalVariables.getCurrentAcademicYear().Id, theApplicant.Id, theSchool.Id, theStudentFolder.Id, true);
            spfsa1.Family_May_Submit_Updates__c = 'Yes';
            update spfsa1;
            
            Test.startTest();
            lockPFSActionBatch pfsLockeBatch = new lockPFSActionBatch();
            batchId = Database.executeBatch(pfsLockeBatch);
            Test.stopTest();
            
            System.assertEquals('Completed',[Select status, TotalJobItems from AsyncApexJob where Id=:batchId limit 1].status);
            System.assertEquals(1,[Select status, TotalJobItems from AsyncApexJob where Id=:batchId limit 1].TotalJobItems);
            System.assertEquals('No',[Select Family_May_Submit_Updates__c from School_PFS_Assignment__c where Id=:spfsa1.Id limit 1].Family_May_Submit_Updates__c);
        }
    }
}