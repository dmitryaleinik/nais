/**
 * MassEmailSendTestDataFactory.cls
 *
 * @description: Test data factory class for MassEmailSend related functionality, generates necessary test data for unit tests
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest
public class MassEmailSendTestDataFactory {

    // avoid mixed DML during init
    public static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    // Constants for use in retreiving created records in external test classes
    public static final String TEST_SCHOOL_CONTACT_EMAIL = 'testyschoolcontact293903982@gmail.com';
    public static final String TEST_SCHOOL2_CONTACT_EMAIL = 'testyschoolcontact29390398311@gmail.com';
    public static final String TEST_PARENT1_CONTACT_EMAIL = 'testyparentcontact293903981@gmail.com';
    public static final String TEST_PARENT2_CONTACT_EMAIL = 'testyparentcontact293903980@gmail.com';
    public static final String TEST_STUDENT1_CONTACT_EMAIL = 'testystudentcontact29391823@gmail.com';
    public static final String TEST_STUDENT2_CONTACT_EMAIL = 'testystudentcontact29391844@gmail.com';
    // school name
    public static final String TEST_SCHOOL_NAME = 'Test School';
    // student folder names
    public static final String TEST_STUDENT_FOLDER1_NAME = 'TestFolder1';
    public static final String TEST_STUDENT_FOLDER2_NAME = 'TestFolder2';
    // PFS names
    public static final String TEST_PFS1_NAME = 'TestPFS1';
    public static final String TEST_PFS2_NAME = 'TestPFS2';
    // mass email send names
    public static final String TEST_MASS_EMAIL1_NAME = 'TestMES1';
    public static final String TEST_MASS_EMAIL2_NAME = 'TestMES2';
    // portal user
    public User portalUser { get; set; }
    public Account school1Acct { get; set; }
    public Contact staff1Contact { get; set; }
    

    // default ctor, always fires setup
    public MassEmailSendTestDataFactory() {

        this( true);
    }

    // overloaded ctor gives option not to run setup
    public MassEmailSendTestDataFactory( Boolean runSetup) {

        if ( runSetup) {

            setupTestData();
        } else {

            this.createPortalUser();
        }
    }

    private void createPortalUser() {

        // create portal user
        System.runAs( thisUser) {

        school1Acct = TestUtils.createAccount( 'PortalSchoolAccount', RecordTypes.schoolAccountTypeId, 5, false);
        school1Acct.Portal_Version__c = 'Basic';
        List<Account> aList = new List<Account> { school1Acct};
        insert aList;

        staff1Contact = TestUtils.createContact( 'Testy Schoolcontact', school1Acct.Id, RecordTypes.schoolStaffContactTypeId, false);
        staff1Contact.Email = MassEmailSendTestDataFactory.TEST_SCHOOL_CONTACT_EMAIL;
        List<Contact> contactList = new List<Contact> { staff1Contact};
        insert contactList;
            
        portalUser = TestUtils.createPortalUser(
                        staff1Contact.LastName, staff1Contact.Email + String.valueOf( Math.random()) + String.valueOf( Math.random()), 
                            'alias', staff1Contact.Id, GlobalVariables.schoolPortalAdminProfileId, true, true);
        }
        
        // Create Share between portalUser and its school because criteria based sharing rules do not run in
        // unit tests.
        TestUtils.insertAccountShare( staff1Contact.AccountId, portalUser.Id);
    }

    /**
     * @description: Non-setup object data, can all be created at once w/ same user
     *
     */
    public static void setupTestData() {
        
        // Create School & Parent Accts
        List<Account> acctList = new List<Account>();
        acctList.add( new Account( Name = MassEmailSendTestDataFactory.TEST_SCHOOL_NAME, SSS_School_Code__c = '1234567890', RecordTypeId = RecordTypes.schoolAccountTypeId));
        acctList.add( new Account( Name = 'Tester Individual', RecordTypeId = RecordTypes.individualAccountTypeId));
        insert acctList;

        // Create Contacts
        List<Contact> contactList = new List<Contact>();
        contactList.add( new Contact( FirstName = 'Testy', LastName = 'SchoolContact2', Email = MassEmailSendTestDataFactory.TEST_SCHOOL2_CONTACT_EMAIL, Account = acctList[0],
                         RecordTypeId = RecordTypes.schoolStaffContactTypeId));
        contactList.add( new Contact( FirstName = 'Testy', LastName = 'Parentcontact', Email = MassEmailSendTestDataFactory.TEST_PARENT1_CONTACT_EMAIL, Account = acctList[1],
                         RecordTypeId = RecordTypes.parentContactTypeId));
        contactList.add( new Contact( FirstName = 'Testy2', LastName = 'Parentcontact2', Email = MassEmailSendTestDataFactory.TEST_PARENT2_CONTACT_EMAIL, Account = acctList[1],
                         RecordTypeId = RecordTypes.parentContactTypeId));
        contactList.add( new Contact( FirstName = 'StudentTesty', LastName = 'Studentcontact', Email = MassEmailSendTestDataFactory.TEST_STUDENT1_CONTACT_EMAIL, Account = acctList[1],
                         RecordTypeId = RecordTypes.studentContactTypeId));
        contactList.add( new Contact( FirstName = 'StudentTesty2', LastName = 'Studentcontact2', Email = MassEmailSendTestDataFactory.TEST_STUDENT2_CONTACT_EMAIL, Account = acctList[1],
                         RecordTypeId = RecordTypes.studentContactTypeId));
        insert contactList;

        // Create Student Folders
        List<Student_Folder__c> studentFolderList = new List<Student_Folder__c>();
        studentFolderList.add( new Student_Folder__c( Name = MassEmailSendTestDataFactory.TEST_STUDENT_FOLDER1_NAME, Folder_Source__c = 'PFS', 
                               Student__c = contactList[3].Id, Primary_Parent__c = contactList[1].Id, Academic_Year_Picklist__c = '2016-2017'));
        studentFolderList.add( new Student_Folder__c( Name = MassEmailSendTestDataFactory.TEST_STUDENT_FOLDER2_NAME, Folder_Source__c = 'PFS',
                               Student__c = contactList[4].Id, Primary_Parent__c = contactList[2].Id, Academic_Year_Picklist__c = '2016-2017'));
        insert studentFolderList;

        // Create Mass_Email_Send__c records, one with HTML body, one with plain text body
        List<Mass_Email_Send__c> massESList = new List<Mass_Email_Send__c>();
        massESList.add( new Mass_Email_Send__c( Subject__c = 'Test', Html_Body__c = '<span>hello there</span>', Footer__c = '<span>test footer</span>', Status__c = 'Draft', 
                        Sent_By__c = contactList[0].Id, Recipients__c = String.valueOf( contactList[1].Id) + ',' + String.valueOf( contactList[2].Id), 
                        Reply_To_Address__c = 'replyto@test.com', Name = MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME, Visualforce_Template__c = 'MassEmailTemplate',
                        School__c = acctList[0].Id));
        massESList.add( new Mass_Email_Send__c( Subject__c = 'Test Again', Text_Body__c = 'text test', Status__c = 'Draft', Sent_By__c = contactList[0].Id,
                        Recipients__c = String.valueOf( contactList[1].Id) + ',', Reply_To_Address__c = 'replyto@test.com', 
                        Name = MassEmailSendTestDataFactory.TEST_MASS_EMAIL2_NAME, Visualforce_Template__c = 'MassEmailTemplate',
                        School__c = acctList[0].Id));
        insert massESList;
    }

    /**
     * @description: "setup object" data (IE User/EmailTemplate/Profile/etc), run as a different user in calling test class
     *
     */
    public static void createEmailTemplate() {

        String markupString = '<messaging:emailTemplate subject="Default Subject" recipientType="Contact" relatedToType="Mass_Email_Send__c"> ' +
                                   ' <messaging:htmlEmailBody > ' +
                                   ' </messaging:htmlEmailBody> ' +
                              '</messaging:emailTemplate>';
        // Create Email Template
        EmailTemplate massEmailTemplate = 
            new EmailTemplate( isActive = true, Name = 'MET', DeveloperName = 'MassEmailTemplate', TemplateType = 'visualforce', 
                               FolderId = UserInfo.getUserId(), Markup = markupString, Encoding = 'ISO-8859-1', TemplateStyle = 'none');
        insert massEmailTemplate;
    }

    /**
     * @description: Creates a Mass Email formatted JSON Sendgrid webhook event to mock webhook responses
     * API docs: https://sendgrid.com/docs/API_Reference/Webhooks/event.html
     *
     * @param: spaId - valid School_PFS_Assignment__c Id
     * @param: schoolId - valid Account (School) Id
     * @param: recipId - valid Contact Id
     * @param: massESId - valid Mass_Email_Send__c Id
     *
     * @return: a formatted JSON string that can be inserted into Student_Folder__c.Mass_Email_Events__c
     */
    public static String createMassEmailJSONEvent( String spaId, String schoolId, String recipId, String massESId, String eventType) {
        String returnVal = '';

        returnVal = '{ ' +
                     '"useragent" : "Mozilla/5.0 (Windows NT 5.1; rv:11.0) Gecko Firefox/11.0 (via ggpht.com GoogleImageProxy)", ' +
                     '"url_offset" : null, ' +
                     '"url" : null, ' +
                     '"tls" : null, ' +
                     '"timestamp" : 1472502125, ' +
                     '"spa_id" : "' + ( String.isNotEmpty( spaId) ? spaId : '') + '", ' +
                     '"sg_message_id" : "GYK1z6BwT-GFpEcYzSeuBw.filter0988p1mdw1.403.57C4995D34.0", ' +
                     '"sg_event_id" : "MDg2MjIzOGQtZmI1My00NTI1LWJiODctNzRmY2E5ODhiZjAx", ' +
                     '"sender_id" : "003J000001Nc0ZAIAZ", ' +
                     '"send_at" : null, ' +
                     '"school_id" : "' + ( String.isNotEmpty( schoolId) ? schoolId : '') + '", ' +
                     '"response" : null, ' +
                     '"recipient_id" : "' + ( String.isNotEmpty( recipId) ? recipId : '') + '", ' +
                     '"reason" : null, ' +
                     '"mass_email_send_id" : "' + ( String.isNotEmpty( massESId) ? massESId : '') + '", ' +
                     '"ip" : "66.102.6.175", ' +
                     '"event" : "' + ( String.isNotEmpty( eventType) ? eventType : 'open') + '", ' +
                     '"email" : "test.test@test.com", ' +
                     '"cert_err" : null, ' +
                     '"attempt" : null, ' +
                     '"asm_group_id" : null, ' +
                     '"academic_year" : "2016-2017" ' +
                    '} ';

        return returnVal;
    }

    /**
     * @description: Creates required PFS, Applicant, and School PFS to form proper data and relationships for end-to-end send testing via Batch process.
     * Assumes setupTestData() has already run. 
     *
     */
    public static void createRequiredPFSData() {

        createRequiredPFSData( null);
    }

    /**
     * @description: Creates required PFS, Applicant, and School PFS to form proper data and relationships for end-to-end send testing via Batch process.
     * Assumes setupTestData() has already run. 
     *
     */
    public static void createRequiredPFSData( Id acctId) {

        // Create Academic Year, Applicant, PFS && PFS Assignment
        try {

            List<Academic_Year__c> yearList = TestUtils.createAcademicYears();
            Account schoolAcct = [select Id from Account where Name = :MassEmailSendTestDataFactory.TEST_SCHOOL_NAME];
            // parent/student 1
            Contact parent1Contact = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_PARENT1_CONTACT_EMAIL];
            Contact student1Contact = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_STUDENT1_CONTACT_EMAIL];
            Student_Folder__c studFolder1 = [select Id from Student_Folder__c where Name = :MassEmailSendTestDataFactory.TEST_STUDENT_FOLDER1_NAME]; 
            PFS__c pfs1 = TestUtils.createPFS( MassEmailSendTestDataFactory.TEST_PFS1_NAME, yearList[0].Id, parent1Contact.Id, true);
            Applicant__c applicant1 = TestUtils.createApplicant( student1Contact.Id, pfs1.Id, true);
            School_PFS_Assignment__c sPFS1 = TestUtils.createSchoolPFSAssignment( yearList[0].Id, applicant1.Id, 
                ( acctId == null ? schoolAcct.Id : acctId), studFolder1.Id, true);

            // parent/student 2
            Contact parent2Contact = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_PARENT2_CONTACT_EMAIL];
            Contact student2Contact = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_STUDENT2_CONTACT_EMAIL];
            Student_Folder__c studFolder2 = [select Id from Student_Folder__c where Name = :MassEmailSendTestDataFactory.TEST_STUDENT_FOLDER2_NAME]; 
            PFS__c pfs2 = TestUtils.createPFS( MassEmailSendTestDataFactory.TEST_PFS2_NAME, yearList[0].Id, parent2Contact.Id, true);
            Applicant__c applicant2 = TestUtils.createApplicant( student2Contact.Id, pfs2.Id, true);
            School_PFS_Assignment__c sPFS2 = TestUtils.createSchoolPFSAssignment( yearList[0].Id, applicant2.Id, 
                ( acctId == null ? schoolAcct.Id : acctId), studFolder2.Id, true);

            // update massES records w/ SPA Id's
            Mass_Email_Send__c massES1 = [select School_PFS_Assignments__c from Mass_Email_Send__c where Name =:MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
            Mass_Email_Send__c massES2 = [select School_PFS_Assignments__c from Mass_Email_Send__c where Name =:MassEmailSendTestDataFactory.TEST_MASS_EMAIL2_NAME];
            massES1.School_PFS_Assignments__c = sPFS1.Id + ',' + sPFS2.Id;
            massES2.School_PFS_Assignments__c = sPFS1.Id + ',';
            List<Mass_Email_Send__c> updateList = new List<Mass_Email_Send__c>{ massES1, massES2};

            update updateList;
        } catch ( Exception e) {

            // log it, don't care otherwise
            System.debug( 'DEBUG:::exception occurred in MassEmailSendTestDataFactory.createRequiredPFSData(), message: ' + 
                e.getMessage() + e.getStackTraceString());
        }
    }

    /**
	 * @description: Populates Mass_Email_Merge_Setting__c custom setting 
	 *
	 */
	public static void createMassEmailMergeSettings() {

		List<Mass_Email_Merge_Setting__c> mergeSettingList = new List<Mass_Email_Merge_Setting__c>();
		Mass_Email_Merge_Setting__c mergeSettingFN = new Mass_Email_Merge_Setting__c();
		mergeSettingFN.Name = 'ParentAFN';
		mergeSettingFN.Merge_Name__c = '{{ParentAFN}}';
		mergeSettingFN.Display_Name__c = 'Parent A First Name';
		mergeSettingFN.Model_Name__c = 'Parent_A_First_Name__c';
		mergeSettingList.add( mergeSettingFN);

		Mass_Email_Merge_Setting__c mergeSettingLN = new Mass_Email_Merge_Setting__c();
		mergeSettingLN.Name = 'ParentALN';
		mergeSettingLN.Merge_Name__c = '{{ParentALN}}';
		mergeSettingLN.Display_Name__c = 'Parent A Last Name';
		mergeSettingLN.Model_Name__c = 'Parent_A_Last_Name__c';
		mergeSettingList.add( mergeSettingLN);

		insert mergeSettingList;
	}

}