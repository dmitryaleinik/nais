/**
 * SchoolMassEmailSendModelTest.cls
 *
 * @description: Test class for SchoolMassEmailSendModel using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
private class SchoolMassEmailSendModelTest {
    
    /* test data setup */
    @testSetup 
    static void setupTestData() {
        MassEmailSendTestDataFactory.createEmailTemplate();
        
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
        }
    }

    /* negative test cases */

    // test ascending sorts with invalid mass ES data
    @isTest 
    static void sortByAsc_invalidRecord_noSort() {
        
        // Arrange
        // no work

        // Act
        Test.startTest();

            List<SchoolMassEmailSendModel> massESModelList;
        Test.stopTest();

        // Assert
        System.assert( SchoolMassEmailSendModel.sortByNameAsc( massESModelList).isEmpty());
        System.assert( SchoolMassEmailSendModel.sortByDateSentAsc( massESModelList).isEmpty());
        System.assert( SchoolMassEmailSendModel.sortByTotalBouncedAsc( massESModelList).isEmpty());
        System.assert( SchoolMassEmailSendModel.sortByTotalOpenedAsc( massESModelList).isEmpty());
        System.assert( SchoolMassEmailSendModel.sortByTotalUnsubAsc( massESModelList).isEmpty());
        System.assert( SchoolMassEmailSendModel.sortByTotalRecipAsc( massESModelList).isEmpty());
        System.assert( SchoolMassEmailSendModel.sortByStatusAsc( massESModelList).isEmpty());
        System.assert( SchoolMassEmailSendModel.sortBySentByNameAsc( massESModelList).isEmpty());
    }

    // test descending sorts with invalid mass ES data
    @isTest 
    static void sortByDesc_invalidRecord_noSort() {
        
        // Arrange
        // no work

        // Act
        Test.startTest();

            List<SchoolMassEmailSendModel> massESModelList;
        Test.stopTest();

        // Assert
        System.assert( SchoolMassEmailSendModel.sortByNameDesc( massESModelList).isEmpty());
        System.assert( SchoolMassEmailSendModel.sortByDateSentDesc( massESModelList).isEmpty());
        System.assert( SchoolMassEmailSendModel.sortByTotalBouncedDesc( massESModelList).isEmpty());
        System.assert( SchoolMassEmailSendModel.sortByTotalOpenedDesc( massESModelList).isEmpty());
        System.assert( SchoolMassEmailSendModel.sortByTotalUnsubDesc( massESModelList).isEmpty());
        System.assert( SchoolMassEmailSendModel.sortByTotalRecipDesc( massESModelList).isEmpty());
        System.assert( SchoolMassEmailSendModel.sortByStatusDesc( massESModelList).isEmpty());
        System.assert( SchoolMassEmailSendModel.sortBySentByNameDesc( massESModelList).isEmpty());
    }


    /* postive test cases */

    // test constructor with valid MassES record, valid init
    @isTest 
    static void constructor_validRecord_validInit() {
        
        // Arrange
        Account acct = [select Id from Account where Name = :MassEmailSendTestDataFactory.TEST_SCHOOL_NAME];
        List<Mass_Email_Send__c> massESList = new MassEmailSendDataAccessService().getMassEmailSendBySchoolId( acct.Id);

        // Act
        Test.startTest();

            SchoolMassEmailSendModel massESModel = SchoolMassEmailSendModel.getModel( massESList[0]);
        Test.stopTest();

        // Assert
        System.assertEquals( MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME, massESModel.name);
        System.assertNotEquals( null, massESModel.status);
        System.assertNotEquals( null, massESModel.id);
    }

    // test ascending sorts with valid Mass ES data
    @isTest 
    static void sortByAsc_validRecord_validSort() {
        
        // Arrange
        Account acct = [select Id from Account where Name = :MassEmailSendTestDataFactory.TEST_SCHOOL_NAME];
        List<Mass_Email_Send__c> massESList = new MassEmailSendDataAccessService().getMassEmailSendBySchoolId( acct.Id);

        // Act
        Test.startTest();

            List<SchoolMassEmailSendModel> massESModelList = SchoolMassEmailSendModel.getModels(massESList);
            
            Test.stopTest();

        // Assert
        System.assertNotEquals( null, SchoolMassEmailSendModel.sortByNameAsc( massESModelList));
        System.assertEquals( 2, SchoolMassEmailSendModel.sortByNameAsc( massESModelList).size());
        System.assertNotEquals( null, SchoolMassEmailSendModel.sortByStatusAsc( massESModelList));
        System.assertEquals( 2, SchoolMassEmailSendModel.sortByStatusAsc( massESModelList).size());
        System.assertNotEquals( null, SchoolMassEmailSendModel.sortByDateSentAsc( massESModelList));
        System.assertEquals( 2, SchoolMassEmailSendModel.sortByDateSentAsc( massESModelList).size());
        System.assertNotEquals( null, SchoolMassEmailSendModel.sortByTotalBouncedAsc( massESModelList));
        System.assertEquals( 2, SchoolMassEmailSendModel.sortByTotalBouncedAsc( massESModelList).size());
        System.assertNotEquals( null, SchoolMassEmailSendModel.sortByTotalOpenedAsc( massESModelList));
        System.assertEquals( 2, SchoolMassEmailSendModel.sortByTotalOpenedAsc( massESModelList).size());
        System.assertNotEquals( null, SchoolMassEmailSendModel.sortByTotalUnsubAsc( massESModelList));
        System.assertEquals( 2, SchoolMassEmailSendModel.sortByTotalUnsubAsc( massESModelList).size());
        System.assertNotEquals( null, SchoolMassEmailSendModel.sortByTotalRecipAsc( massESModelList));
        System.assertEquals( 2, SchoolMassEmailSendModel.sortByTotalRecipAsc( massESModelList).size());
        System.assertNotEquals( null, SchoolMassEmailSendModel.sortBySentByNameAsc( massESModelList));
        System.assertEquals( 2, SchoolMassEmailSendModel.sortBySentByNameAsc( massESModelList).size());
    }

    // test descending sorts with valid Mass ES data
    @isTest 
    static void sortByDesc_validRecord_validSort() {
        
        // Arrange
        Account acct = [select Id from Account where Name = :MassEmailSendTestDataFactory.TEST_SCHOOL_NAME];
        List<Mass_Email_Send__c> massESList = new MassEmailSendDataAccessService().getMassEmailSendBySchoolId( acct.Id);
        
        // Act
        Test.startTest();

            List<SchoolMassEmailSendModel> massESModelList = SchoolMassEmailSendModel.getModels(massESList);

        Test.stopTest();

        // Assert
        System.assertNotEquals( null, SchoolMassEmailSendModel.sortByNameDesc( massESModelList));
        System.assertEquals( 2, SchoolMassEmailSendModel.sortByNameDesc( massESModelList).size());
        System.assertNotEquals( null, SchoolMassEmailSendModel.sortByStatusDesc( massESModelList));
        System.assertEquals( 2, SchoolMassEmailSendModel.sortByStatusDesc( massESModelList).size());
        System.assertNotEquals( null, SchoolMassEmailSendModel.sortByDateSentDesc( massESModelList));
        System.assertEquals( 2, SchoolMassEmailSendModel.sortByDateSentDesc( massESModelList).size());
        System.assertNotEquals( null, SchoolMassEmailSendModel.sortByTotalBouncedDesc( massESModelList));
        System.assertEquals( 2, SchoolMassEmailSendModel.sortByTotalBouncedDesc( massESModelList).size());
        System.assertNotEquals( null, SchoolMassEmailSendModel.sortByTotalOpenedDesc( massESModelList));
        System.assertEquals( 2, SchoolMassEmailSendModel.sortByTotalOpenedDesc( massESModelList).size());
        System.assertNotEquals( null, SchoolMassEmailSendModel.sortByTotalUnsubDesc( massESModelList));
        System.assertEquals( 2, SchoolMassEmailSendModel.sortByTotalUnsubDesc( massESModelList).size());
        System.assertNotEquals( null, SchoolMassEmailSendModel.sortByTotalRecipDesc( massESModelList));
        System.assertEquals( 2, SchoolMassEmailSendModel.sortByTotalRecipDesc( massESModelList).size());
        System.assertNotEquals( null, SchoolMassEmailSendModel.sortBySentByNameDesc( massESModelList));
        System.assertEquals( 2, SchoolMassEmailSendModel.sortBySentByNameDesc( massESModelList).size());
    }
    
    @isTest 
    static void getModels_moreThan100MassEmailSendRecords_expectNoRTooManySoqlQueriesException() {
        
        Account acct = [select Id from Account where Name = :MassEmailSendTestDataFactory.TEST_SCHOOL_NAME];
        
        Contact testContact = [SELECT Id, Firstname, Lastname FROM Contact WHERE email =: MassEmailSendTestDataFactory.TEST_STUDENT1_CONTACT_EMAIL LIMIT 1];
        
        List<Mass_Email_Send__c> massESList = new List<Mass_Email_Send__c>();
        
        for (Integer i=0; i<150; i++) {
           
            massESList.add( new Mass_Email_Send__c(
                Subject__c = 'Test ' + i, 
                Html_Body__c = '<span>hello there</span>', 
                Footer__c = '<span>test footer</span>', 
                Status__c = 'Draft', 
                Sent_By__c =testContact.Id, 
                Recipients__c = String.valueOf(testContact.Id) + ',' + String.valueOf(testContact.Id), 
                Reply_To_Address__c = 'replyto@test.com', 
                Name = MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME, 
                Visualforce_Template__c = 'MassEmailTemplate',
                School__c = acct.Id));
        }
        
        insert massESList;
        
        Test.startTest();
        
            List<Mass_Email_Send__c> massESListF = [SELECT Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c, 
                Date_Sent__c, School_PFS_Assignments__c, Subject__c, Text_Body__c, Html_Body__c, Footer__c, Reply_To_Address__c, 
                Failed_to_Send__c, Send_Email_For_Each_Applicant__c, Is_Award_Letter__c FROM Mass_Email_Send__c where School__c =: acct.Id];
                
            List<SchoolMassEmailSendModel> massESModelList = SchoolMassEmailSendModel.getModels(massESListF);
    
        Test.stopTest();
    
        System.assertNotEquals( null, SchoolMassEmailSendModel.sortByNameDesc( massESModelList));
        System.assertEquals( 152, SchoolMassEmailSendModel.sortByNameDesc( massESModelList).size());
    }
}