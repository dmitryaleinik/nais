@isTest
global class SendgridMockHttpResponseGenerator implements HttpCalloutMock {

      // Implement required interface method
    global HTTPResponse respond( HTTPRequest req) {
      
          // Create the response
          HttpResponse res = new HttpResponse();

          res.setHeader( 'Content-Type', 'application/json');
          res.setBody( '{"message":"success"}');
          res.setStatusCode( 200);

          return res;
      }

}