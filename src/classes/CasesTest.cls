@isTest
private class CasesTest {
    
    private static User currentUser = [Select Id from User where Id = :UserInfo.getUserId()];
    
    private static String getSaveResultMessage(Database.SaveResult[] result) {
        String messages = '';
        
        for(Integer i=0; i<result.size(); i++) {
            
            Database.Saveresult sr = result[i];
            
            if(!sr.isSuccess()){
                
                for(Database.Error err : sr.getErrors()) {
                   messages += err.getMessage();
                }
            }
        }
        
        return messages;
    }
    
    private static void SetupCase1(Id recordTypeId) {
        
        Contact parentA = ContactTestData.Instance.asParent().insertContact();
        
        Case case1 = CaseTestData.Instance
            .forRecordTypeId(recordTypeId)
            .forSubject('Test Case 1')
            .forDescription('Test description')
            .createCaseWithoutReset();
        case1.Type = 'Documents';
        case1.Status = 'In Progress';
        case1.ContactId = parentA.Id;
        insert case1;
        
        Test.StartTest();
            
            case1.Status = 'Closed';
            Database.SaveResult[] result = Database.update(new List<Case>{case1}, false);
            
            String messages = getSaveResultMessage(result);
            
            System.assertEquals(true, !String.isBlank(messages));
            
            System.assertEquals(true, messages.contains(Label.Case_Requires_Knowledge_Article));
            
            System.assertEquals('In Progress', [SELECT id, Status FROM Case WHERE Id =: case1.Id].Status);
            
        Test.StopTest();
    }
    
    private static void SetupCase2(Id recordTypeId) {
        
        currentUser.UserPermissionsKnowledgeUser = true;
        update currentUser;
        
        Case case1;
        Contact parentA = ContactTestData.Instance.asParent().insertContact();

        System.runAs( currentUser) {
            case1 = CaseTestData.Instance
                .forRecordTypeId(recordTypeId)
                .forSubject('Test Case 1')
                .forDescription('Test description')
                .createCaseWithoutReset();
            case1.Type = 'Documents';
            case1.Status = 'In Progress';
            case1.ContactId = parentA.Id;
            insert case1;
            
            Integer numberOfArticles = 15;
            List<Knowledge__kav> articles = KnowledgeAVTestData.Instance.insertKnowledgeAVs(numberOfArticles);

            // Create articles with the default category.
            String dataCategoryName = 'Pre_Payment_FAQ';
            List<Knowledge__DataCategorySelection> categories = new List<Knowledge__DataCategorySelection>();
            for (Integer i=0; i<numberOfArticles; i++) {
                categories.add(KnowledgeDCSTestData.Instance
                    .forDataCategoryName(dataCategoryName)
                    .forParentId(articles[i].Id).create());
            }
            insert categories;
            
            //This line needs to be right before publish the articles, to avoid exception on CaseArticle insert with:
            //System.DmlException: Insert failed. First exception on row 0; first error: FIELD_INTEGRITY_EXCEPTION, Article ID: id value of incorrect type"
            Knowledge__kav article = [SELECT Id, KnowledgeArticleId, ArticleNumber FROM Knowledge__kav WHERE Id =: articles[0].Id LIMIT 1];
            
            //Publish the articles
            articles = KnowledgeTestHelper.getKnowledgeArticleByIds(articles);
            KnowledgeTestHelper.publishArticles(articles);
           
            //Add a new metadata item to Article_Disposition_Mapping__c, for testing purposes
            Map<String, Article_Disposition_Mapping__c> metadata = ArticleDispositionMappingSelector.Instance
                .getMapByArticleNumber();
                
            Article_Disposition_Mapping__c testArticle = new Article_Disposition_Mapping__c(
                Title__c ='Uploading Scans or Pictures (JPGs) of 1040 and Other Documents', 
                Visible_In_Public_Knowledge_Base__c=  true, 
                Visible_to_Customer__c=   true, 
                Article_Number__c=    article.ArticleNumber, 
                Type__c=  'Documents' , 
                Disposition__c=   'Document Question' , 
                Sub_Category__c=  'How to Upload Documents');
            
            ArticleDispositionMappingSelector.Instance.mapByArticleNumber.put(article.ArticleNumber, testArticle);
            
            //Attach an article to the case
            CaseArticle ca = new CaseArticle();
            ca.CaseId = case1.Id;
            ca.KnowledgeArticleId = article.KnowledgeArticleId;
            insert ca;
        }
        
        Test.StartTest();
            
            case1.Status = 'Escalation Requested';
            case1.Action_Taken__c = 'Resolved';
            Database.SaveResult[] result = Database.update(new List<Case>{case1}, false);
            
            String messages = getSaveResultMessage(result);
            
            System.assertEquals(true, String.isBlank(messages));
            
            Case caseResult = [SELECT id, Status, Type, Disposition__c, Sub_category__c  FROM Case WHERE Id =: case1.Id];
            
            System.assertEquals('Escalation Requested', caseResult.Status);
            
            System.assertNotEquals(null, caseResult.Type);
            
            System.assertNotEquals(null, caseResult.Disposition__c);
            
            System.assertNotEquals(null, caseResult.Sub_category__c);
            
        Test.StopTest();
    }
    
    @isTest 
    private static void shouldRequireKnowledgeArticle_parentCallCase_caseWithoutKnowledgeArticle_expectUpdateToCloseFailed() {
        
        //Test scenario for when a "Parent Call" case is inserted with "In Progress" status.
        //And the user try to change the case status to "Closed". In this case, an error message
        //must be shown, that indicates that the case must have a Knowledge Article attached before being closed.
        //This message is saved in Label.Case_Requires_Knowledge_Article
        SetupCase1(RecordTypes.parentCallCaseTypeId);        
    }
    
    @isTest 
    private static void shouldRequireKnowledgeArticle_familyEmailCase_caseWithoutKnowledgeArticle_expectUpdateToCloseFailed() {
        
        //Test scenario for when a "Email - Family" case is inserted with "In Progress" status.
        //And the user try to change the case status to "Closed". In this case, an error message
        //must be shown, that indicates that the case must have a Knowledge Article attached before being closed.
        //This message is saved in Label.Case_Requires_Knowledge_Article
        SetupCase1(RecordTypes.familyEmailCaseTypeId);        
    }
    
    @isTest 
    private static void shouldRequireKnowledgeArticle_familyLiveAgentCase_caseWithoutKnowledgeArticle_expectUpdateToCloseFailed() {
        
        //Test scenario for when a "Parent Live Chat" case is inserted with "In Progress" status.
        //And the user try to change the case status to "Closed". In this case, an error message
        //must be shown, that indicates that the case must have a Knowledge Article attached before being closed.
        //This message is saved in Label.Case_Requires_Knowledge_Article
        SetupCase1(RecordTypes.familyLiveAgentCaseTypeId);        
    }
    
    @isTest 
    private static void shouldRequireKnowledgeArticle_familyPortalCase_caseWithoutKnowledgeArticle_expectUpdateToCloseFailed() {
        
        //Test scenario for when a "Family Portal Case for NAIS" case is inserted with "In Progress" status.
        //And the user try to change the case status to "Closed". In this case, an error message
        //must be shown, that indicates that the case must have a Knowledge Article attached before being closed.
        //This message is saved in Label.Case_Requires_Knowledge_Article
        SetupCase1(RecordTypes.familyPortalCaseTypeId);        
    }
    
    @isTest 
    private static void shouldRequireKnowledgeArticle_parentCallCase_caseWithKnowledgeArticle_expectUpdateToCloseSuccess() {
        
        //Test scenario for when a "Parent Call" case is inserted with "In Progress" status.
        //And the user try to change the case status to "Closed". In this case, NO error message must be shown.
        //And the case status must be successfully updated to "Closed".
        SetupCase2(RecordTypes.parentCallCaseTypeId);        
    }
    
    @isTest 
    private static void shouldRequireKnowledgeArticle_familyEmailCase_caseWithKnowledgeArticle_expectUpdateToCloseSuccess() {
        
        //Test scenario for when a "Email - Family" case is inserted with "In Progress" status.
        //And the user try to change the case status to "Closed". In this case, NO error message must be shown.
        //And the case status must be successfully updated to "Closed".
        SetupCase2(RecordTypes.familyEmailCaseTypeId);        
    }
    
    @isTest 
    private static void shouldRequireKnowledgeArticle_familyLiveAgentCase_caseWithKnowledgeArticle_expectUpdateToCloseSuccess() {
        
        //Test  scenario for when a "Parent Live Chat" case is inserted with "In Progress" status.
        //And the user try to change the case status to "Closed". In this case, NO error message must be shown.
        //And the case status must be successfully updated to "Closed".
        SetupCase2(RecordTypes.familyLiveAgentCaseTypeId);        
    }
    
    @isTest 
    private static void shouldRequireKnowledgeArticle_familyPortalCase_caseWithKnowledgeArticle_expectUpdateToCloseSuccess() {
        
        //Test scenario for when a "Family Portal Case for NAIS" case is inserted with "In Progress" status.
        //And the user try to change the case status to "Closed". In this case, NO error message must be shown.
        //And the case status must be successfully updated to "Closed".
        SetupCase2(RecordTypes.familyPortalCaseTypeId);        
    }
}