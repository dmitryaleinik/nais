/*
 * NAIS-502: Batch Apex for EFC calcs
 *
 * SL, Exponent Partners, 2013
 */
public class EfcCalculatorBatchProcessor implements Database.Batchable<SObject> {

    private static final EfcCalculator theCalculator = new EfcCalculator();
    
    public enum CalcType {SSS, REVISION}
    private CalcType calcType;
    private string customQueryString = null;
    
    private boolean calcSSSAndRevision = false;
    
    // if no calc type is passed in, then chain together both a SSS and Revision batch job
    public EfcCalculatorBatchProcessor() {
        calcSSSAndRevision = true;
        
        // start with SSS
        this.calcType = EfcCalculatorBatchProcessor.CalcType.SSS;
    }
    
    // execute either a SSS or Revision calculation
    public EfcCalculatorBatchProcessor(EfcCalculatorBatchProcessor.CalcType calcType) {
        this.calcType = calcType;
    }
    
    // execute either a SSS or Revision calculation, using a custom query string for the list of records to process
    public EfcCalculatorBatchProcessor(EfcCalculatorBatchProcessor.CalcType calcType, String queryStringOverride) {
        this.calcType = calcType;
        this.customQueryString = queryStringOverride;
    }       
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        String query;
        String recalcStatus = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE_BATCH;
        
        if (customQueryString != null) {
            query = customQueryString;
        }
        else if (calcType == EfcCalculatorBatchProcessor.CalcType.SSS) {
           query = 'SELECT Id FROM PFS__c WHERE SSS_EFC_Calc_Status__c = :recalcStatus';
        }
        else {
           query = 'SELECT Id FROM School_PFS_Assignment__c WHERE Revision_EFC_Calc_Status__c = :recalcStatus';
        }
        
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        
        List<EfcWorksheetData> worksheets;
        Set<Id> idsToRecalc = new Set<Id>();
        for(sObject record : scope) {
            idsToRecalc.add(record.Id);
        }
            
        if (calcType == EfcCalculatorBatchProcessor.CalcType.SSS) {
            worksheets = EfcDataManager.createEfcWorksheetDataForPFSs(idsToRecalc);
        }
        else {
            worksheets = EfcDataManager.createEfcWorksheetDataForSchoolPFSAssignments(idsToRecalc);
        }
        
        theCalculator.calculateEfc(worksheets); 
        EfcDataManager.commitEfcResults(worksheets);
        
    }
    
    public void finish(Database.BatchableContext BC) {
        // launch a new Revision calc if chaining together jobs and the SSS job just finished
        if ((this.calcSSSAndRevision == true) && (this.calcType == EfcCalculatorBatchProcessor.CalcType.SSS)) {
              doExecuteBatch(EfcCalculatorBatchProcessor.CalcType.Revision);
        }
    }
    
    public static Id doExecuteBatch() {
        // set the batch size to the EFC batch threshhold in custom settings
        Integer batchSize = (Integer) EfcUtil.getEfcSettingBatchCalculationThreshold();
        return Database.executeBatch(new EfcCalculatorBatchProcessor(), batchSize);
    }
    
    public static Id doExecuteBatch(EfcCalculatorBatchProcessor.CalcType calcType) {
        // set the batch size to the EFC batch threshhold in custom settings
        Integer batchSize = (Integer) EfcUtil.getEfcSettingBatchCalculationThreshold();
        return Database.executeBatch(new EfcCalculatorBatchProcessor(calcType), batchSize);
    }
    
    public static Id doExecuteBatch(EfcCalculatorBatchProcessor.CalcType calcType, String queryStringOverride) {
        // set the batch size to the EFC batch threshhold in custom settings
        Integer batchSize = (Integer) EfcUtil.getEfcSettingBatchCalculationThreshold();
        return Database.executeBatch(new EfcCalculatorBatchProcessor(calcType, queryStringOverride), batchSize);
    }
    
}