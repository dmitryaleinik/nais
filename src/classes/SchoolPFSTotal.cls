/*
* SPEC-091
*
* The EFC should be visible throughout the folder view and aid allocation processes, because the school users 
* will be changing fields and needing to understand the ramifications on the EFC
* The system should provide a summary area for each folder with the most relevant information that appears on each screen
*
* Nathan, Exponent Partners, 2013
*/

// Wrapper class to pass data to component SchoolPFSTotal
public class SchoolPFSTotal {

    public PFS__c pfs {get; set;}
    public School_PFS_Assignment__c schoolPFSAssignment {get; set;}
    public Applicant__c applicant {get; set;}
    
    public String title {get; set;}
    public String status {get; set;}
    public String parentAName {get; set;}
    public String parentBName {get; set;}
    public String parentARelation {get; set;}
    public String parentBRelation {get; set;}
    
    public String studentId {get;set;} //NAIS-1597
    //NAIS-1896
    public String parentalContributionAllStudentsTitle{get; set;}
    public Decimal parentalContributionAllStudents{get; set;}
    public Decimal origParentalContributionAllStudents{get; set;}
    
    // The label is dynamic, and displays either "EFC Day" or "EFC Boarding" depending on the Day_Boarding__c field
    public String efcTitle {get; set;}
    
    public Decimal origDiscretionaryIncome {get; set;}   
    public Decimal origEffectiveIncome {get; set;}   
    public Decimal origEstFamilyContribution {get; set;}   
    public Decimal origIncomeSupplement {get; set;}   
    public Decimal origTotalAllowances {get; set;}   
    public Decimal origTotalIncome {get; set;}
    public Decimal origTotalTaxableIncome {get; set;}
    public Decimal origTotalNonTaxableIncome {get; set;}
    public Decimal origUnusualExpense {get; set;}      
    public Decimal origTotalAssets {get; set;}
    public Decimal origTotalDebts {get; set;}   

    public Decimal discretionaryIncome {get; set;}   
    public Decimal effectiveIncome {get; set;}   
    public Decimal estFamilyContribution {get; set;}   
    public Decimal incomeSupplement {get; set;}   
    public Decimal totalAllowances {get; set;}   
    public Decimal totalIncome {get; set;}
    public Decimal totalTaxableIncome {get; set;}
    public Decimal totalNonTaxableIncome {get; set;}
    public Decimal unusualExpense {get; set;}      
    public Decimal totalAssets {get; set;}
    public Decimal totalDebts {get; set;}      
    
    public Decimal parentCanAffordToPay {get; set;}  

    public Boolean existData {get; set;}
    
    // constructor
    public SchoolPFSTotal() {
        pfs = new PFS__c();
        schoolPFSAssignment = new School_PFS_Assignment__c();
        applicant = new Applicant__c();
        
        efcTitle = 'EFC';
        origDiscretionaryIncome = 0;   
        origEffectiveIncome = 0;   
        origEstFamilyContribution = 0;   
        origIncomeSupplement = 0;   
        origTotalAllowances = 0;   
        origTotalIncome = 0;   
    
        discretionaryIncome = 0;   
        effectiveIncome = 0;   
        estFamilyContribution = 0;   
        incomeSupplement = 0;   
        totalAllowances = 0;   
        totalIncome = 0; 
        
        parentCanAffordToPay = 0;
        
        existData = false; 
        
        //NAIS-1896
        parentalContributionAllStudentsTitle = 'Parent Contrib. All Students';
        parentalContributionAllStudents = 0;
        origParentalContributionAllStudents = 0;
    } 
}