@isTest
private class SchoolPagesUtilsTest {

    private static User schoolPortalUser;
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    private static Contact student1;
    private static Applicant__c applicant1A;
    private static Student_Folder__c studentFolder1;
    private static School_PFS_Assignment__c spfsa1;

    private static void createTestData() {
        Account family = AccountTestData.Instance.asFamily().create();
        Account school = AccountTestData.Instance.asSchool().create();
        insert new List<Account>{family, school};

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;

        Contact parentA = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(school.Id).create();
        student1 = ContactTestData.Instance
            .asStudent()
            .forAccount(family.Id).create();
        insert new List<Contact> {schoolStaff, parentA, student1};

        System.runAs(thisUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);
        String pfsStatusUnpaid = 'Unpaid';

        System.runAs(schoolPortalUser){
            PFS__c pfs1 = PfsTestData.Instance
                .asSubmitted()
                .forPaymentStatus(pfsStatusUnpaid)
                .forParentA(parentA.Id)
                .forOriginalSubmissionDate(System.today().addDays(-3))
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<PFS__c> {pfs1});

            applicant1A = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student1.Id).create();
            Dml.WithoutSharing.insertObjects(new List<Applicant__c> {applicant1A});

            studentFolder1 = StudentFolderTestData.Instance
                .forStudentId(student1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<Student_Folder__c> {studentFolder1});

            String fifthGrade = '5';
            spfsa1 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1A.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> {spfsa1});
        }

        // Share records to school staff
        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);
    }

    @isTest
    private static void testPFSPages() {
        createTestData();

        applicant1A.First_Name__c = 'Jhon';
        applicant1A.Last_Name__c = 'Doe';
                
        Test.startTest();
            update applicant1A;
            PageReference testPage = Page.SchoolPFSSummary;
            testPage.getParameters().put('schoolpfsassignmentid', spfsa1.Id);
            
            Test.setCurrentPage(testPage);
            String title = SchoolPagesUtils.getSchoolPageTitle(testPage);
            system.assertEquals('Jhon Doe PFS Summary', title);
        Test.stopTest();
    }

    @isTest
    private static void testFolderSummaryPages() {
        createTestData();
        
        applicant1A.First_Name__c = 'Jane';
        applicant1A.Last_Name__c = 'Doe';

        studentFolder1.First_Name__c = 'Jane';
        studentFolder1.Last_Name__c = 'Doe';

        Test.startTest();
            update new List<Applicant__c> { applicant1A };
            update new List<Student_Folder__c> { studentFolder1 };
            PageReference testPage = Page.SchoolFinancialAid;
            testPage.getParameters().put('id', studentFolder1.Id);
            
            Test.setCurrentPage(testPage);
            String title = SchoolPagesUtils.getSchoolPageTitle(testPage);
            system.assertEquals('Jane Doe Financial Aid', title);
        Test.stopTest();
    }

    @isTest
    private static void testFeeWaiversPages() {
        createTestData();
        
        applicant1A.First_Name__c = 'Jane';
        applicant1A.Last_Name__c = 'Doe';
        
        Test.startTest();
            update applicant1A;
            PageReference testPage = Page.schoolfeewaivers;
            testPage.getParameters().put('id', studentFolder1.Id);
            testPage.getParameters().put('academicyearid', GlobalVariables.getCurrentAcademicYear().Id);
            
            Test.setCurrentPage(testPage);
            String title = SchoolPagesUtils.getSchoolPageTitle(testPage);
            system.assertEquals('Fee Waivers '+GlobalVariables.getCurrentAcademicYear().Name, title);
        Test.stopTest();
    }
}