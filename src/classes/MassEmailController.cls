/**
 * MassEmailController.cls
 *
 * @description Controller for MassEmail.component, handles loading a Mass_Email_Send__c record and providing body and footer content to view.
 *
 * @author Chase Logan @ Presence PG
 */
public class MassEmailController {
    
    /* instance variables */
    private Mass_Email_Send__c massES;
    private final String DEFAULT_LOGO = 'SSS Line Logo';
    private final String DEFAULT_FOOTER = 
                '<p>' +
                    Label.Copyright_Info +
                '</p>' +
                '<p>' + 
                    'Our mailing address is:<br>    sss@solutionsbysss.com' +
                '</p>' +
                '<p>' +
                    'Want to change how you receive these emails?<br>    You can update your preferences or unsubscribe from this list' +
                '</p>';

    /* properties */
    public Id massEmailSendId { 
        get; 
        set {
            if ( this.massES == null) {
                this.init( value);
            }
            this.massEmailSendId = value;
        } 
    }
    public Boolean mockLoad { 
        get; 
        set {
            if ( this.massES == null && this.massEmailSendId == null) {
                this.init( value);
            }
            this.mockLoad = value;
        } 
    }
    public String mockHeaderLogoName { get; set; }
    public String mockFooter { get; set; }
    public Boolean hasPlainTextBody { get; set; }
    public Boolean hasHtmlBody { get; set; }
    public Boolean hasFooter { get; set; }
    public Boolean hasDefaultFooter { get; set; }
    public String logoURL { get; set; } 

    /* view getters */

    public String getHtmlBody() {
        
        return ( this.massES != null && this.hasHtmlBody ? this.massES.Html_Body__c : '');
    }

    public String getPlainTextBody() {

        return ( this.massES != null && this.hasPlainTextBody ? this.massES.Text_Body__c : '');
    }

    public String getFooter() {

        return ( this.massES != null && this.hasFooter ? this.massES.Footer__c : '');
    }

    public String getDefaultFooter() {

        return ( this.massES != null && this.hasDefaultFooter ? this.massES.Default_Footer__c : '');
    }

    public String getMockFooterContent() {

        return ( this.massES == null && this.mockLoad && 
                    String.isNotEmpty( this.mockFooter) ? this.mockFooter : this.DEFAULT_FOOTER);
    }

    public String getMockLogoURL() {

        return ( this.massES == null && this.mockLoad && 
                    String.isNotEmpty( this.mockHeaderLogoName) ? 
                        this.constructLogoURL( this.mockHeaderLogoName) : this.constructLogoURL( this.DEFAULT_LOGO));
    }

    /**
     * @description: handles constructing a valid massES logo URL, exposed publicly for instance access
     *
     * @param: documentName - A valid Document name
     */ 
    public String constructLogoURL( String documentName) {
        String returnVal;

        if ( documentName == null) return returnVal;

        String OID_PARAM = '&oid=';
        String BASE_URL = 'LogoBaseURL';

        Document d;
        try {

            // if multiple Documents have been uploaded with the same name, grab most recent
            d = [select Id, Name 
                   from Document 
                  where Name = :documentName
               order by LastModifiedDate desc 
                  limit 1];
            Mass_Email_Setting__mdt massEmailMdt = [select Value__c from Mass_Email_Setting__mdt where DeveloperName = :BASE_URL];

            if  ( massEmailMdt != null) {

                returnVal = massEmailMdt.Value__c + d.Id + OID_PARAM + UserInfo.getOrganizationId();
            }
        } catch ( Exception e) {

            System.debug( 'DEBUG:::exception occurred in MassEmailController.constructLogoURL(), message: ' + 
                            e.getMessage() + e.getStackTraceString());
        }

        return returnVal;
    }

    // init component view if valid Mass_Email_Send__c Id is present, else hide all
    private void init( Id massESId) {

        if ( massESId != null) {

            this.massES = new MassEmailSendDataAccessService().getMassEmailSendById( massESId);
            
            // check for footer, if no footer exists, insert default footer
            Boolean needsUpdate = false;
            if ( String.isEmpty( this.massES.Footer__c) && String.isEmpty( this.massES.Default_Footer__c)) {

                this.massES.Default_Footer__c = this.DEFAULT_FOOTER;
                needsUpdate = true;
            }

            // check for logo, if none exists apply default
            if ( String.isEmpty( this.massES.Logo_Name__c)) {

                this.massES.Logo_Name__c = this.DEFAULT_LOGO;
                needsUpdate = true;
            }

            if ( needsUpdate) {

                update this.massES;
                // requery updated massES record
                this.massES = new MassEmailSendDataAccessService().getMassEmailSendById( massESId);
            }

            // check for HTML or plain text body on massES record, both can be rendered together
            this.hasHtmlBody = ( this.massES != null && String.isNotEmpty( this.massES.Html_Body__c) ? true : false);
            this.hasPlainTextBody = ( this.massES != null && String.isNotEmpty( this.massES.Text_Body__c) ? true : false);
            this.hasFooter =  ( this.massES != null && String.isNotEmpty( this.massES.Footer__c) ? true : false);
            this.hasDefaultFooter =  ( this.massES != null && !this.hasFooter ? true : false);
            this.logoURL = this.constructLogoURL( this.massES.Logo_Name__c);
        } else {

            this.hasHtmlBody = false;
            this.hasPlainTextBody = false;
            this.hasFooter = false;
            this.hasDefaultFooter = false;
        }
    }

    // init component view in "mock mode" if Mass ES ID is not present and mockLoad is true
    private void init( Boolean mockLoad) {

        if ( mockLoad == null || mockLoad == false) return;

        this.hasHtmlBody = false;
        this.hasPlainTextBody = false;
        this.hasFooter = false;
        this.hasDefaultFooter = false;
    }    

}