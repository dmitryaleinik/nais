/**
 * IntegrationApiServiceTest.cls
 *
 * @description Test class for IntegrationApiService
 *
 * @author Mike Havrilla @ Presence PG
 */

@isTest (seealldata=false)
public class IntegrationApiServiceTest {

    private static PFS__c pfs;

    private static PFS__c setupTestPfsForAcademicYear(String academicYearName) {
        // Create School & Parent Accts
        List<Account> acctList = new List<Account>();
        acctList.add( new Account( Name = 'SAO Test Shool Number 2', NCES_ID__c = '5902', SSS_School_Code__c = '5902', SSS_Subscriber_Status__c = 'Current', RecordTypeId = RecordTypes.schoolAccountTypeId));
        acctList.add( new Account( Name = 'SAO Test School of the Greats', NCES_ID__c = '1111', SSS_School_Code__c = '1111', SSS_Subscriber_Status__c = 'Current', RecordTypeId = RecordTypes.schoolAccountTypeId));

        acctList.add( new Account( Name = 'Test Student', RecordTypeId = RecordTypes.individualAccountTypeId));
        insert acctList;

        List<Contact> contactList = new List<Contact>();
        contactList.add( new Contact( FirstName = 'Testy', LastName = 'SchoolContact2', Email = MassEmailSendTestDataFactory.TEST_SCHOOL2_CONTACT_EMAIL, Account = acctList[0],
                RecordTypeId = RecordTypes.schoolStaffContactTypeId));
        contactList.add( new Contact( FirstName = 'Testy', LastName = 'Parentcontact', Email = MassEmailSendTestDataFactory.TEST_PARENT1_CONTACT_EMAIL, Account = acctList[1],
                RecordTypeId = RecordTypes.parentContactTypeId, Birthdate = Date.newInstance(2015, 01, 01)) );

        insert contactList;

        pfs = TestUtils.createPFS( 'Test', academicYearName, contactList[1].Id, true);

        return pfs;
    }

    @isTest static void init_validateConfiguration_noBaseUrl() {
        String academicYearName = '2017-2018';
        AcademicYearTestData.Instance.forName(academicYearName).insertAcademicYear();
        setupTestPfsForAcademicYear(academicYearName);

        // Arrange
        final String INTEGRATION_NAME = 'sao';
        Boolean hasIntegrationException = false;
        String errorMessage;
        IntegrationSource__c setting = new IntegrationSource__c();
        setting.Name = INTEGRATION_NAME;
        insert setting;

        // Act
        Test.startTest();

        try {

            IntegrationApiService.Instance.executeIntegrationApis(pfs, INTEGRATION_NAME);
        } catch ( IntegrationApiService.IntegrationConfigurationException e) {
            errorMessage = e.getMessage();
            hasIntegrationException = true;
        }

        Test.stopTest();

        // Assert
        // At this point were just making sure no configuration/callout exceptions happened.
        System.assert( hasIntegrationException);
        System.assert( errorMessage.contains( 'IntegrationSource__c.BaseCalloutUrl__c is null'));
    }

    @isTest static void init_validateConfiguration_noCustomSettingData() {
        String academicYearName = '2017-2018';
        AcademicYearTestData.Instance.forName(academicYearName).insertAcademicYear();
        setupTestPfsForAcademicYear(academicYearName);

        // Arrange
        final String INTEGRATION_NAME = 'INVALID_NAME';
        Boolean hasIntegrationException = false;
        String errorMessage;

        // Act
        Test.startTest();

        try {

            IntegrationApiService.Instance.executeIntegrationApis(new PFS__c(), INTEGRATION_NAME);
        } catch ( IntegrationApiService.IntegrationConfigurationException e) {

            errorMessage = e.getMessage();
            hasIntegrationException = true;
        }

        Test.stopTest();

        // Assert
        // At this point were just making sure no configuration/callout exceptions happened.
        System.assert( hasIntegrationException);
        System.assert( errorMessage.contains( 'No Integration Source record for'));
    }

    /* Positive Test Cases */

    @isTest static void initModels_withSAO_validData() {
        String academicYearName = '2017-2018';
        AcademicYearTestData.Instance.forName(academicYearName).insertAcademicYear();
        setupTestPfsForAcademicYear(academicYearName);

        // Arrange
        final String INTEGRATION_NAME = 'sao';
        Boolean hasIntegrationException = false;
        IntegrationSource__c setting = new IntegrationSource__c();
        setting.BaseCalloutURL__c = 'http://api.piedpiper.com/v1/';
        setting.Name = INTEGRATION_NAME;
        insert setting;

        // Validate there is data in Integration_Process__mdt
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('SAOProfilesMock');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');


        // Act
        Test.startTest();
        Test.setMock( HttpCalloutMock.class, mock);
        try {
            IntegrationApiService.Instance.executeIntegrationApis(pfs, INTEGRATION_NAME);
        } catch ( Exception e) {

            hasIntegrationException = true;
        }

        Test.stopTest();

        // Assert
        // At this point were just making sure no configuration/callout exceptions happened.
        System.assert( !hasIntegrationException);

        List<Applicant__c> applicants = [Select Id, Name, Current_Grade__c, SaoId__c from Applicant__c where SaoId__c = '170173536'];
        List<Student_Folder__c> studentFolder = [Select Id, School__c, Student__c, Name, SaoId__c from Student_Folder__c where SaoId__c in ('da0dcec0-adaa-e611-99ae-000d3a1641e5', 'da0dcec0-adaa-e611-99ae-000000000000')];
        List<School_PFS_Assignment__c> spas = [Select Id, RavennaId__c, Student_Folder__c, School__c, SaoId__c, Name from School_PFS_Assignment__c where SaoId__c in ('da0dcec0-adaa-e611-99ae-000d3a1641e5', 'da0dcec0-adaa-e611-99ae-000000000000')];

        System.assertEquals( 1, applicants.size());
        System.assertEquals( 2, studentFolder.size());
        System.assertEquals( 2, spas.size());
    }

    // Test with Valid custom setting but no values in Integration Process mdt
    @isTest static void init_withSourceandValidMDT_successCallout() {
        String academicYearName = '2017-2018';
        AcademicYearTestData.Instance.forName(academicYearName).insertAcademicYear();
        setupTestPfsForAcademicYear(academicYearName);

        // Arrange
        final String INTEGRATION_NAME = 'sao';
        Boolean hasIntegrationException = false;

        IntegrationSource__c setting = new IntegrationSource__c();
        setting.BaseCalloutURL__c = 'http://api.piedpiper.com';
        setting.Name = INTEGRATION_NAME;
        insert setting;

        // Act
        Test.startTest();
        try {
            // Set mock for HTTP, and exceute apis
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('SAOProfilesMock');
            mock.setStatusCode(200);
            mock.setHeader('Content-Type', 'application/json');

            Test.setMock( HttpCalloutMock.class, mock);
            //Test.setMock( HttpCalloutMock.class, new SaoMockHttpResponseGenerator());
            IntegrationApiService.Instance.executeIntegrationApis(pfs, INTEGRATION_NAME);
        } catch ( Exception e) {
            system.assertEquals(null, e, e.getStackTraceString());
            hasIntegrationException = true;
        }

        Test.stopTest();

        // Assert
        // At this point were just making sure no configuration/callout exceptions happened.
        System.assert( !hasIntegrationException);
    }

    // Test with Valid custom setting but no values in Integration Process mdt
    // Becasue we're using MDT, if this call doesn't have nested params then it will not generate
    // enough test coverage
    @isTest static void init_withSourceandValidMDT_Ravenna_successCalloutWithParams() {
        String academicYearName = '2016-2017';
        AcademicYearTestData.Instance.forName(academicYearName).insertAcademicYear();
        AcademicYearTestData.Instance.forName('2017-2018').insertAcademicYear();
        setupTestPfsForAcademicYear(academicYearName);

        // insert Applicant Database
        // Arrange
        final String INTEGRATION_NAME = 'ravenna';
        final STring BASE_URL = 'http://api.piedpiper.com';
        final String STUDENTS_API_ENDPOINT =  BASE_URL + '/api/v1/students/';
        final String SCHOOLS_API_ENDPOINT = BASE_URL + '/api/v1/schools/59371';
        final String STUDENT_APPLICATION_API_ENDPOINT = BASE_URL + '/api/v1/students/90919/applications?admission_year=2015-2016';
        final String STUDENT_APPLICATION_API_ENDPOINT2 = BASE_URL + '/api/v1/students/91023/applications?admission_year=2015-2016';

        List<Account> acctList = new List<Account>();
        acctList.add( new Account( Name = MassEmailSendTestDataFactory.TEST_SCHOOL_NAME, NCES_ID__c = '01324523', SSS_School_Code__c = '01324523', SSS_Subscriber_Status__c = 'Current', RecordTypeId = RecordTypes.schoolAccountTypeId));
        acctList.add( new Account( Name = 'Test Student1', RecordTypeId = RecordTypes.individualAccountTypeId));
        insert acctList;

        List<Contact> contactList = new List<Contact>();
        contactList.add( new Contact( FirstName = 'Testy1', LastName = 'SchoolContact2', Email = MassEmailSendTestDataFactory.TEST_SCHOOL2_CONTACT_EMAIL, Account = acctList[0],
                RecordTypeId = RecordTypes.schoolStaffContactTypeId));
        contactList.add( new Contact( FirstName = 'Testy1', LastName = 'Parentcontact', Email = MassEmailSendTestDataFactory.TEST_PARENT1_CONTACT_EMAIL, Account = acctList[1],
                RecordTypeId = RecordTypes.parentContactTypeId, Birthdate = Date.newInstance(2000, 08, 01)));
        //contactList.add( new Contact( FirstName = 'Test Student', LastName = 'SSS', Email = MassEmailSendTestDataFactory.TEST_STUDENT1_CONTACT_EMAIL, Account = acctList[1],
        //       RecordTypeId = RecordTypes.studentContactTypeId));
        insert contactList;


        Boolean hasIntegrationException = false;
        IntegrationSource__c setting = new IntegrationSource__c();
        setting.BaseCalloutURL__c = BASE_URL;
        setting.Name = INTEGRATION_NAME;
        insert setting;

        // Act
        Test.startTest();

        MultiStaticResourceCalloutMock multiMock = new MultiStaticResourceCalloutMock();
        multiMock.setStaticResource( STUDENTS_API_ENDPOINT,  'RavennaStudentsMockResponse');
        multiMock.setStaticResource( STUDENT_APPLICATION_API_ENDPOINT,  'RavennaApplicationMockResponse');
        multiMock.setStaticResource( STUDENT_APPLICATION_API_ENDPOINT2,  'RavennaApplicationStudent2MockResponse');
        multiMock.setStaticResource( SCHOOLS_API_ENDPOINT,  'RavennaSchoolMockResponse');
        multiMock.setStatusCode(200);
        multiMock.setHeader('Content-Type', 'application/json');

        Test.setMock( HttpCalloutMock.class, multiMock);
        IntegrationApiService.Instance.executeIntegrationApis(pfs, INTEGRATION_NAME);
        Test.stopTest();
    }

    /**
     * @description This method takes in an academic year name and generates the corresponding ravenna admission year.
     *              The ravenna admission year is one less than the corresponding academic year in our system.
     */
    private static String getRavennaAdmissionYear(String academicYearName) {
        Integer startYear = Integer.valueOf(academicYearName.left(4)) - 1;
        Integer endYear = Integer.valueOf(academicYearName.right(4)) - 1;

        return String.valueOf(startYear) + '-' + String.valueOf(endYear);
    }

    private static final String RAVENNA_SOURCE = 'ravenna';
    private static final String RAVENNA_EXTERNAL_ID_FIELD = 'RavennaId__c';
    private static final String BASE_URL = 'http://api.piedpiper.com';
    private static final String RAVENNA_STUDENTS_API_ENDPOINT =  BASE_URL + '/api/v1/students/';
    private static final String RAVENNA_SCHOOL_API_ENDPOINT_FORMAT = BASE_URL + '/api/v1/schools/{0}';
    private static final String RAVENNA_STUDENT_APPLICATION_ENDPOINT_FORMAT = BASE_URL + '/api/v1/students/{0}/applications?admission_year={1}';
    private static final List<String> RAVENNA_STUDENT_IDS = new List<String> { '90001', '90002', '90003' };
    private static final Map<String, String> RAVENNA_NCES_IDS_BY_RAVENNA_IDS = new Map<String, String> {
            '80001' => '00000001',
            '80002' => '00000002',
            '80003' => '00000003'
    };

    private static IntegrationSource__c insertIntegrationSource(String sourceName, String baseUrl) {
        IntegrationSource__c setting = new IntegrationSource__c();
        setting.Name = sourceName;
        setting.BaseCalloutURL__c = baseUrl;
        insert setting;

        return setting;
    }

    private static String formatRavennaSchoolEndpoint(String ravennaSchoolId) {
        return String.format(RAVENNA_SCHOOL_API_ENDPOINT_FORMAT, new List<String> { ravennaSchoolId });
    }

    private static String formatRavennaApplicationsEndpoint(String ravennaStudentId, String admissionYear) {
        return String.format(RAVENNA_STUDENT_APPLICATION_ENDPOINT_FORMAT, new List<String> { ravennaStudentId, admissionYear});
    }

    private static Map<String, Account> createSchoolsByExternalId(Map<String, String> ncesIdsByExternalIds, String externalIdFieldName) {
        Map<String, Account> schoolsByExternalId = new Map<String, Account>();

        for (String externalId : ncesIdsByExternalIds.keySet()) {
            Account school = AccountTestData.Instance.asSchool().withNcesId(ncesIdsByExternalIds.get(externalId)).create();
            school.put(externalIdFieldName, externalId);
            schoolsByExternalId.put(externalId, school);
        }

        return schoolsByExternalId;
    }

    @isTest
    private static void executeIntegrationApis_ravennaSource_2017_2018_noRavennaApplications_expectIntegrationApiException() {
        String currentAcademicYearName = '2017-2018';
        String nextAcademicYearName = '2018-2019';
        String ravennaAdmissionYear = getRavennaAdmissionYear(currentAcademicYearName);

        AcademicYearTestData.Instance.forName(currentAcademicYearName).insertAcademicYear();
        AcademicYearTestData.Instance.forName(nextAcademicYearName).insertAcademicYear();

        PFS__c testPfs = PfsTestData.Instance.forAcademicYearPicklist(currentAcademicYearName).insertPfs();

        Integer expectedNumberOfApplicants = 0;
        Map<String, Integer> expectedNumberOfAppsByStudentExternalId = new Map<String, Integer> {
                '90001' => 0,
                '90002' => 0,
                '90003' => 0
        };

        Map<String, Account> schoolsByExternalIds = createSchoolsByExternalId(RAVENNA_NCES_IDS_BY_RAVENNA_IDS, RAVENNA_EXTERNAL_ID_FIELD);
        for (Account school : schoolsByExternalIds.values()) {
            school.SSS_Subscriber_Status__c = 'Current';
        }
        insert schoolsByExternalIds.values();

        insertIntegrationSource(RAVENNA_SOURCE, BASE_URL);

        MultiStaticResourceCalloutMock multiMock = new MultiStaticResourceCalloutMock();
        multiMock.setStaticResource(RAVENNA_STUDENTS_API_ENDPOINT, 'RavennaMockResponse_Students');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90001', ravennaAdmissionYear),  'MockResponse_EmptyArray');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90002', ravennaAdmissionYear),  'MockResponse_EmptyArray');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90003', ravennaAdmissionYear),  'MockResponse_EmptyArray');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80001'),  'RavennaMockResponse_School1');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80002'),  'RavennaMockResponse_School2');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80003'),  'RavennaMockResponse_School3');
        multiMock.setStatusCode(200);
        multiMock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, multiMock);

        Test.startTest();
        try {
            IntegrationApiService.Instance.executeIntegrationApis(testPfs, RAVENNA_SOURCE);

            System.assert(false, 'An exception should have been thrown.');
        } catch (IntegrationApiService.IntegrationException ex) {
            System.assert(true, 'Expected an integration exception to be thrown.');
            System.assert(ex.getMessage().containsIgnoreCase(Label.Integration_No_Subscriber_School_Error),
                    'Expected the integration exception to be thrown because the students do not have any applications.');
        }
        Test.stopTest();

        // Assert the correct number of applicants exist
        List<Applicant__c> applicants = [SELECT Id, RavennaId__c, PFS__c, (SELECT Id FROM School_PFS_Assignments__r) FROM Applicant__c WHERE PFS__c = :testPfs.Id];
        System.assertEquals(expectedNumberOfApplicants, applicants.size(), 'Expected the correct number of applicants.');

        // Assert the correct number of applications for each applicant
        for (Applicant__c applicant : applicants) {
            System.assertEquals(expectedNumberOfAppsByStudentExternalId.get(applicant.RavennaId__c), applicant.School_PFS_Assignments__r.size(),
                    'Expected the right number of applications for each student.');
        }
    }

    @isTest
    private static void executeIntegrationApis_ravennaSource_2017_2018_threeStudentsHaveRavennaApplications_noSSSSchoolsExist_expectIntegrationException() {
        String currentAcademicYearName = '2017-2018';
        String nextAcademicYearName = '2018-2019';
        String ravennaAdmissionYear = getRavennaAdmissionYear(currentAcademicYearName);

        AcademicYearTestData.Instance.forName(currentAcademicYearName).insertAcademicYear();
        AcademicYearTestData.Instance.forName(nextAcademicYearName).insertAcademicYear();

        PFS__c testPfs = PfsTestData.Instance.forAcademicYearPicklist(currentAcademicYearName).insertPfs();

        Integer expectedNumberOfApplicants = 0;
        Map<String, Integer> expectedNumberOfAppsByStudentExternalId = new Map<String, Integer> {
                '90001' => 0,
                '90002' => 0,
                '90003' => 0
        };

        insertIntegrationSource(RAVENNA_SOURCE, BASE_URL);

        Test.startTest();
        MultiStaticResourceCalloutMock multiMock = new MultiStaticResourceCalloutMock();
        multiMock.setStaticResource(RAVENNA_STUDENTS_API_ENDPOINT, 'RavennaMockResponse_Students');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90001', ravennaAdmissionYear),  'RavennaMockResponse_Applications_Student1');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90002', ravennaAdmissionYear),  'RavennaMockResponse_Applications_Student2');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90003', ravennaAdmissionYear),  'RavennaMockResponse_Applications_Student3');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80001'),  'RavennaMockResponse_School1');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80002'),  'RavennaMockResponse_School2');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80003'),  'RavennaMockResponse_School3');
        multiMock.setStatusCode(200);
        multiMock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, multiMock);

        try {
            IntegrationApiService.Instance.executeIntegrationApis(testPfs, RAVENNA_SOURCE);

            System.assert(false, 'An exception should have been thrown.');
        } catch (IntegrationApiService.IntegrationException ex) {
            System.assert(true, 'Expected an integration exception to be thrown.');
            System.assert(ex.getMessage().containsIgnoreCase(Label.Integration_No_Subscriber_School_Error),
                    'Expected the integration exception to be thrown because the students do not have any applications.');
        }
        Test.stopTest();

        // Assert the correct number of applicants exist
        List<Applicant__c> applicants = [SELECT Id, RavennaId__c, PFS__c, (SELECT Id FROM School_PFS_Assignments__r) FROM Applicant__c WHERE PFS__c = :testPfs.Id];
        System.assertEquals(expectedNumberOfApplicants, applicants.size(), 'Expected the correct number of applicants.');

        // Assert the correct number of applications for each applicant
        for (Applicant__c applicant : applicants) {
            System.assertEquals(expectedNumberOfAppsByStudentExternalId.get(applicant.RavennaId__c), applicant.School_PFS_Assignments__r.size(),
                    'Expected the right number of applications for each student.');
        }
    }

    @isTest
    private static void executeIntegrationApis_ravennaSource_2017_2018_threeStudentsHaveRavennaApplications_allSchoolsAreCurrentSubscribers_expectAllApplicationsInserted() {
        String currentAcademicYearName = '2017-2018';
        String nextAcademicYearName = '2018-2019';
        String ravennaAdmissionYear = getRavennaAdmissionYear(currentAcademicYearName);

        AcademicYearTestData.Instance.forName(currentAcademicYearName).insertAcademicYear();
        AcademicYearTestData.Instance.forName(nextAcademicYearName).insertAcademicYear();

        PFS__c testPfs = PfsTestData.Instance.forAcademicYearPicklist(currentAcademicYearName).insertPfs();

        Integer expectedNumberOfApplicants = 3;
        Map<String, Integer> expectedNumberOfAppsByStudentExternalId = new Map<String, Integer> {
                '90001' => 3,
                '90002' => 2,
                '90003' => 1
        };

        Map<String, Account> schoolsByExternalIds = createSchoolsByExternalId(RAVENNA_NCES_IDS_BY_RAVENNA_IDS, RAVENNA_EXTERNAL_ID_FIELD);
        for (Account school : schoolsByExternalIds.values()) {
            school.SSS_Subscriber_Status__c = 'Current';
        }
        insert schoolsByExternalIds.values();

        insertIntegrationSource(RAVENNA_SOURCE, BASE_URL);

        Test.startTest();
        MultiStaticResourceCalloutMock multiMock = new MultiStaticResourceCalloutMock();
        multiMock.setStaticResource(RAVENNA_STUDENTS_API_ENDPOINT, 'RavennaMockResponse_Students');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90001', ravennaAdmissionYear),  'RavennaMockResponse_Applications_Student1');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90002', ravennaAdmissionYear),  'RavennaMockResponse_Applications_Student2');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90003', ravennaAdmissionYear),  'RavennaMockResponse_Applications_Student3');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80001'),  'RavennaMockResponse_School1');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80002'),  'RavennaMockResponse_School2');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80003'),  'RavennaMockResponse_School3');
        multiMock.setStatusCode(200);
        multiMock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, multiMock);
        IntegrationApiService.Instance.executeIntegrationApis(testPfs, RAVENNA_SOURCE);
        Test.stopTest();

        // Assert the correct number of applicants exist
        List<Applicant__c> applicants = [SELECT Id, RavennaId__c, PFS__c, (SELECT Id FROM School_PFS_Assignments__r) FROM Applicant__c WHERE PFS__c = :testPfs.Id];
        System.assertEquals(expectedNumberOfApplicants, applicants.size(), 'Expected the correct number of applicants.');

        // Assert the correct number of applications for each applicant
        for (Applicant__c applicant : applicants) {
            System.assertEquals(expectedNumberOfAppsByStudentExternalId.get(applicant.RavennaId__c), applicant.School_PFS_Assignments__r.size(),
                    'Expected the right number of applications for each student.');
        }
    }

    @isTest
    private static void executeIntegrationApis_ravennaSource_2017_2018_threeStudentsHaveRavennaApplications_allSchoolsAreCurrentSubscribers_onlySchoolRavennaIdsPopulated_expectAllApplicationsInserted() {
        String currentAcademicYearName = '2017-2018';
        String nextAcademicYearName = '2018-2019';
        String ravennaAdmissionYear = getRavennaAdmissionYear(currentAcademicYearName);

        AcademicYearTestData.Instance.forName(currentAcademicYearName).insertAcademicYear();
        AcademicYearTestData.Instance.forName(nextAcademicYearName).insertAcademicYear();

        PFS__c testPfs = PfsTestData.Instance.forAcademicYearPicklist(currentAcademicYearName).insertPfs();

        Integer expectedNumberOfApplicants = 3;
        Map<String, Integer> expectedNumberOfAppsByStudentExternalId = new Map<String, Integer> {
                '90001' => 3,
                '90002' => 2,
                '90003' => 1
        };

        Map<String, Account> schoolsByExternalIds = createSchoolsByExternalId(RAVENNA_NCES_IDS_BY_RAVENNA_IDS, RAVENNA_EXTERNAL_ID_FIELD);
        for (Account school : schoolsByExternalIds.values()) {
            school.SSS_Subscriber_Status__c = 'Current';
            school.NCES_ID__c = null; // Clear out NCES Id to make sure we can still match on ravenna Id.
        }
        insert schoolsByExternalIds.values();

        insertIntegrationSource(RAVENNA_SOURCE, BASE_URL);

        Test.startTest();

        MultiStaticResourceCalloutMock multiMock = new MultiStaticResourceCalloutMock();
        multiMock.setStaticResource(RAVENNA_STUDENTS_API_ENDPOINT, 'RavennaMockResponse_Students');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90001', ravennaAdmissionYear),  'RavennaMockResponse_Applications_Student1');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90002', ravennaAdmissionYear),  'RavennaMockResponse_Applications_Student2');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90003', ravennaAdmissionYear),  'RavennaMockResponse_Applications_Student3');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80001'),  'RavennaMockResponse_School1');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80002'),  'RavennaMockResponse_School2');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80003'),  'RavennaMockResponse_School3');
        multiMock.setStatusCode(200);
        multiMock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, multiMock);

        IntegrationApiService.Instance.executeIntegrationApis(testPfs, RAVENNA_SOURCE);
        Test.stopTest();

        // Assert the correct number of applicants exist
        List<Applicant__c> applicants = [SELECT Id, RavennaId__c, PFS__c, (SELECT Id FROM School_PFS_Assignments__r) FROM Applicant__c WHERE PFS__c = :testPfs.Id];
        System.assertEquals(expectedNumberOfApplicants, applicants.size(), 'Expected the correct number of applicants.');

        // Assert the correct number of applications for each applicant
        for (Applicant__c applicant : applicants) {
            System.assertEquals(expectedNumberOfAppsByStudentExternalId.get(applicant.RavennaId__c), applicant.School_PFS_Assignments__r.size(),
                    'Expected the right number of applications for each student.');
        }
    }

    @isTest
    private static void executeIntegrationApis_ravennaSource_2017_2018_threeStudentsHaveRavennaApplications_allSchoolsAreCurrentSubscribers_onlyNcesIdsPopulated_expectAllApplicationsInserted() {
        String currentAcademicYearName = '2017-2018';
        String nextAcademicYearName = '2018-2019';
        String ravennaAdmissionYear = getRavennaAdmissionYear(currentAcademicYearName);

        AcademicYearTestData.Instance.forName(currentAcademicYearName).insertAcademicYear();
        AcademicYearTestData.Instance.forName(nextAcademicYearName).insertAcademicYear();

        PFS__c testPfs = PfsTestData.Instance.forAcademicYearPicklist(currentAcademicYearName).insertPfs();

        Integer expectedNumberOfApplicants = 3;
        Map<String, Integer> expectedNumberOfAppsByStudentExternalId = new Map<String, Integer> {
                '90001' => 3,
                '90002' => 2,
                '90003' => 1
        };

        Map<String, Account> schoolsByExternalIds = createSchoolsByExternalId(RAVENNA_NCES_IDS_BY_RAVENNA_IDS, RAVENNA_EXTERNAL_ID_FIELD);
        for (Account school : schoolsByExternalIds.values()) {
            school.SSS_Subscriber_Status__c = 'Current';
            // Clear out the ravenna external Ids to make sure we can find the right school with NCES Id alone.
            school.put(RAVENNA_EXTERNAL_ID_FIELD, null);
        }
        insert schoolsByExternalIds.values();

        insertIntegrationSource(RAVENNA_SOURCE, BASE_URL);

        Test.startTest();
        MultiStaticResourceCalloutMock multiMock = new MultiStaticResourceCalloutMock();
        multiMock.setStaticResource(RAVENNA_STUDENTS_API_ENDPOINT, 'RavennaMockResponse_Students');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90001', ravennaAdmissionYear),  'RavennaMockResponse_Applications_Student1');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90002', ravennaAdmissionYear),  'RavennaMockResponse_Applications_Student2');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90003', ravennaAdmissionYear),  'RavennaMockResponse_Applications_Student3');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80001'),  'RavennaMockResponse_School1');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80002'),  'RavennaMockResponse_School2');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80003'),  'RavennaMockResponse_School3');
        multiMock.setStatusCode(200);
        multiMock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, multiMock);

        IntegrationApiService.Instance.executeIntegrationApis(testPfs, RAVENNA_SOURCE);
        Test.stopTest();

        // Assert the correct number of applicants exist
        List<Applicant__c> applicants = [SELECT Id, RavennaId__c, PFS__c, (SELECT Id FROM School_PFS_Assignments__r) FROM Applicant__c WHERE PFS__c = :testPfs.Id];
        System.assertEquals(expectedNumberOfApplicants, applicants.size(), 'Expected the correct number of applicants.');

        // Assert the correct number of applications for each applicant
        for (Applicant__c applicant : applicants) {
            System.assertEquals(expectedNumberOfAppsByStudentExternalId.get(applicant.RavennaId__c), applicant.School_PFS_Assignments__r.size(),
                    'Expected the right number of applications for each student.');
        }
    }

    @isTest
    private static void executeIntegrationApis_ravennaSource_2017_2018_threeStudentsHaveRavennaApplications_noSchoolsAreCurrentSubscribers_expectIntegrationApiException() {
        String currentAcademicYearName = '2017-2018';
        String nextAcademicYearName = '2018-2019';
        String ravennaAdmissionYear = getRavennaAdmissionYear(currentAcademicYearName);

        AcademicYearTestData.Instance.forName(currentAcademicYearName).insertAcademicYear();
        AcademicYearTestData.Instance.forName(nextAcademicYearName).insertAcademicYear();

        PFS__c testPfs = PfsTestData.Instance.forAcademicYearPicklist(currentAcademicYearName).insertPfs();

        Integer expectedNumberOfApplicants = 0;
        Map<String, Integer> expectedNumberOfAppsByStudentExternalId = new Map<String, Integer> {
                '90001' => 0,
                '90002' => 0,
                '90003' => 0
        };

        // Just insert the schools without making them current subscribers
        Map<String, Account> schoolsByExternalIds = createSchoolsByExternalId(RAVENNA_NCES_IDS_BY_RAVENNA_IDS, RAVENNA_EXTERNAL_ID_FIELD);
        insert schoolsByExternalIds.values();

        insertIntegrationSource(RAVENNA_SOURCE, BASE_URL);

        Test.startTest();

        MultiStaticResourceCalloutMock multiMock = new MultiStaticResourceCalloutMock();
        multiMock.setStaticResource(RAVENNA_STUDENTS_API_ENDPOINT, 'RavennaMockResponse_Students');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90001', ravennaAdmissionYear),  'RavennaMockResponse_Applications_Student1');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90002', ravennaAdmissionYear),  'RavennaMockResponse_Applications_Student2');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90003', ravennaAdmissionYear),  'RavennaMockResponse_Applications_Student3');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80001'),  'RavennaMockResponse_School1');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80002'),  'RavennaMockResponse_School2');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80003'),  'RavennaMockResponse_School3');
        multiMock.setStatusCode(200);
        multiMock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, multiMock);

        try {
            IntegrationApiService.Instance.executeIntegrationApis(testPfs, RAVENNA_SOURCE);

            System.assert(false, 'An exception should have been thrown.');
        } catch (IntegrationApiService.IntegrationException ex) {
            System.assert(true, 'Expected an integration exception to be thrown.');
            System.assert(ex.getMessage().containsIgnoreCase(Label.Integration_No_Subscriber_School_Error),
                    'Expected the integration exception to be thrown because the students do not have any applications.');
        }
        Test.stopTest();

        // Assert the correct number of applicants exist
        List<Applicant__c> applicants = [SELECT Id, RavennaId__c, PFS__c, (SELECT Id FROM School_PFS_Assignments__r) FROM Applicant__c WHERE PFS__c = :testPfs.Id];
        System.assertEquals(expectedNumberOfApplicants, applicants.size(), 'Expected the correct number of applicants.');

        // Assert the correct number of applications for each applicant
        for (Applicant__c applicant : applicants) {
            System.assertEquals(expectedNumberOfAppsByStudentExternalId.get(applicant.RavennaId__c), applicant.School_PFS_Assignments__r.size(),
                    'Expected the right number of applications for each student.');
        }
    }

    @isTest
    private static void executeIntegrationApis_ravennaSource_2017_2018_threeStudentsHaveRavennaApplications_oneSchoolIsCurrentSubscriber_expectApplicationsForOneSchoolInserted() {
        String currentAcademicYearName = '2017-2018';
        String nextAcademicYearName = '2018-2019';
        String ravennaAdmissionYear = getRavennaAdmissionYear(currentAcademicYearName);

        AcademicYearTestData.Instance.forName(currentAcademicYearName).insertAcademicYear();
        AcademicYearTestData.Instance.forName(nextAcademicYearName).insertAcademicYear();

        PFS__c testPfs = PfsTestData.Instance.forAcademicYearPicklist(currentAcademicYearName).insertPfs();

        Integer expectedNumberOfApplicants = 3;
        Map<String, Integer> expectedNumberOfAppsByStudentExternalId = new Map<String, Integer> {
                '90001' => 1,
                '90002' => 1,
                '90003' => 1
        };

        // Insert our schools but only make one of them a current subscriber. This should cause us to only have applications for one school.
        String currentSubscriberExternalId = '80001';
        Map<String, Account> schoolsByExternalIds = createSchoolsByExternalId(RAVENNA_NCES_IDS_BY_RAVENNA_IDS, RAVENNA_EXTERNAL_ID_FIELD);
        Account school = schoolsByExternalIds.get(currentSubscriberExternalId);
        school.SSS_Subscriber_Status__c = 'Current';
        insert schoolsByExternalIds.values();

        insertIntegrationSource(RAVENNA_SOURCE, BASE_URL);

        Test.startTest();
        MultiStaticResourceCalloutMock multiMock = new MultiStaticResourceCalloutMock();
        multiMock.setStaticResource(RAVENNA_STUDENTS_API_ENDPOINT, 'RavennaMockResponse_Students');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90001', ravennaAdmissionYear),  'RavennaMockResponse_Applications_Student1');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90002', ravennaAdmissionYear),  'RavennaMockResponse_Applications_Student2');
        multiMock.setStaticResource(formatRavennaApplicationsEndpoint('90003', ravennaAdmissionYear),  'RavennaMockResponse_Applications_Student3');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80001'),  'RavennaMockResponse_School1');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80002'),  'RavennaMockResponse_School2');
        multiMock.setStaticResource(formatRavennaSchoolEndpoint('80003'),  'RavennaMockResponse_School3');
        multiMock.setStatusCode(200);
        multiMock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, multiMock);

        IntegrationApiService.Instance.executeIntegrationApis(testPfs, RAVENNA_SOURCE);
        Test.stopTest();

        // Assert the correct number of applicants exist
        List<Applicant__c> applicants = [SELECT Id, RavennaId__c, PFS__c, (SELECT Id FROM School_PFS_Assignments__r) FROM Applicant__c WHERE PFS__c = :testPfs.Id];
        System.assertEquals(expectedNumberOfApplicants, applicants.size(), 'Expected the correct number of applicants.');

        // Assert the correct number of applications for each applicant
        for (Applicant__c applicant : applicants) {
            System.assertEquals(expectedNumberOfAppsByStudentExternalId.get(applicant.RavennaId__c), applicant.School_PFS_Assignments__r.size(),
                    'Expected the right number of applications for each student.');
        }
    }
}