@isTest
private class FamilyRedirectTemplateControllerTest
{

    @isTest
    private static void testCommunityTest() {
        Account testAcc = TestUtils.createAccount('Staff A Account', RecordTypes.individualAccountTypeId, 2, true);
        Contact parent = TestUtils.createContact('Staff A', testAcc.Id, RecordTypes.parentContactTypeId, true);
        User portalUser = TestUtils.createPortalUser('Staff A', 'sa@test.org', 'sa', parent.Id, GlobalVariables.familyPortalProfileId, true, false);
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            insert portalUser;
        }
        
        System.runAs(portalUser){
            PageReference pr = Page.FamilyDashboard;
            pr.getParameters().put('redirectToCommunity','/FamilyDashboard');
            pr.getParameters().put('id','test');
            Test.setCurrentPage(pr);
            FamilyRedirectTemplateController controller = new FamilyRedirectTemplateController(new FamilyTemplateController());
            String communityURL = controller.getCommunityURL();
            System.assertEquals('?id=test',FamilyRedirectTemplateController.getPageParametersAsString(pr));
            System.assertEquals(true,communityURL.contains(FamilyRedirectTemplateController.getFamilyCommunityURL()));
        }
    }
}