/**
 * IntegrationService.cls
 *
 * @description: 
 *
 * @author: Mike Havrila @ Presence PG
 */

public class IntegrationService {


    /* Implementation of lazy-loaded Singleton pattern */
    private static IntegrationService instance = null;

    // private constructor means getInstance() is the only way to get an instance of IntegrationService
    private IntegrationService() {}
    
    // returns a static instance of IntegrationService, prevents multiple instantiations of the same object in a single transaction
    public static IntegrationService getInstance() { 
        
        return ( instance == null ? instance = new IntegrationService() : instance);
    }

    /**
     * @description: get Integration Mapping Vlaues and return a Map
     *
     * @return - A map of IntegrationMapping__mdt based on SourceObject__c;
     */
    public Map<String, List<IntegrationMappingModel>> getMappingConfig( String source) {

        Map<String, List<IntegrationMappingModel>> returnVal = new Map<String, List<IntegrationMappingModel>>();
        List<IntegrationMapping__mdt> mappingTypes = new IntegrationMappingDataAccessService().getMappingsByIntegration( source);
        if( mappingTypes == null) {

            mappingTypes = new List<IntegrationMapping__mdt>();
        }


        for( IntegrationMapping__mdt mappingType : mappingTypes) {

            List<IntegrationMappingModel> model = new List<IntegrationMappingModel>();
            if( returnVal.containsKey( mappingType.SourceObject__c)) {

                model = returnVal.get( mappingType.SourceObject__c);
            }

            model.add( new IntegrationMappingModel( mappingType));
            returnVal.put( mappingType.SourceObject__c, model);
        }

        return returnVal;
    }

    /**
     * @description: get Integration Mapping Vlaues and return a Map
     * 
     * @param source - The source name of the Auth Provider. 
     *
     * @param provierType - The Provder type to retreive the auth token (Open Id Connect);
     * 
     * @return An Auth Token from Auth Provider. 
     */
    public String getAuthToken( String source, String providerType) {
        String returnVal; 

        if( source == null && providerType == null) return null;
        try {

           Id authId = [select Id 
                          from AuthPRovider 
                         where DeveloperName = :source 
                         limit 1].Id;

           returnVal = Auth.AuthToken.getAccessToken( authId, providerType);
        } catch ( Exception e) {

            System.debug('DEBUG:::IntegrationService.getAuthToken Error: ' + e.getMessage());
        }
        
        return returnVal;
    }

    /**
     * @description: get Integration Mapping Vlaues and return a Map
     * 
     * @param source - The source name of the Auth Provider. 
     *
     * @param provierType - The Provder type to retreive the auth token (Open Id Connect);
     * 
     * @return An Auth Token from Auth Provider. 
     */
    public String getCurrentSchools( List<IntegrationMappingModel> mappings, String dataType, String targetObject, Set<String> schoolids) {
        Set<String> returnVal;

        String schoolId;
        for( IntegrationMappingModel mapping : mappings) {
            if( mapping.dataType == dataType && mapping.targetObject == targetObject) {
                schoolId = mapping.targetField.split(':')[1].split(' ')[1];
                break;
            }
        }

        String query = ' select id, name, ' + schoolId +
                         ' from Account' +
                        ' where ' + schoolId +
                           ' in  :schoolIdList ' + 
                          ' and SSS_Subscriber_Status__C = \'Current\'';

        for(sObject each : Database.query( query)) {
            //GlobalVariables.getCurrentAnnualSettings(true, null, GlobalVariables.getAcademicYearByName(ipfs.pfs.Academic_Year_Picklist__c).id, (id)each.get('id'));

        }
        return 'Test';

    }


    public List<Object> getStudentToApplicants( List<Object> studentObject, String dataType, String targetObject) {
        return new List<Object>();
    }


    public void removeStudentsWithoutSchools( List<String> students) {
        Boolean hasSchool = false;
        for( String student : students) {
            hasSchool = false;
        }
    }


    public Map<String, sObject> getObjectByIds( List<Sobject> objects, String targetFieldName) {
        Map<String, sObject> returnVal = new Map<String, sObject>();
        for( sObject obj : objects) {

            returnVal.put( (String)obj.get( targetFieldName), obj);
        }
        return returnVal;
    }


    public class StudentSearchWrapper {
        public List<String> firstNames { get; set; }
        public List<String> lastNames { get; set; }
        public List<String> birthDates { get; set; }

        public StudentSearchWrapper() {
            this.firstNames = new List<String>();
            this.lastNames = new List<String>();
            this.birthDates = new List<String>();
        }

        private void addFirstName( String firstName) {
            if( String.isNotEmpty( firstName)) {
                firstNames.add( firstName);
            }
        }

        private void addLastName( String lastName) {
            if( String.isNotEmpty( lastName)) {
                lastNames.add( lastName);
            }
            
        }

        private void addBirthDate( String birthDate) {
            if( String.isNotEmpty( birthDate)) {
                birthDates.add( birthDate);
            }
        }

        public void addItems( String firstName, String lastName, String birthDate) {
            this.addFirstName( firstName);
            this.addLastName( lastName);
            this.addBirthDate( birthDate);

        }
    }

}