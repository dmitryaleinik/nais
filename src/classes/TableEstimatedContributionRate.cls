public with sharing class TableEstimatedContributionRate
{
    
    private static Map<Id, List<Expected_Contribution_Rate__c>> estimatedContributionRateDataByYear = new Map<Id, List<Expected_Contribution_Rate__c>>();
    
    public static List<Expected_Contribution_Rate__c> getEstimatedContributionRateData(Id academicYearId) {
        List<Expected_Contribution_Rate__c> estimatedContributionRateData = estimatedContributionRateDataByYear.get(academicYearId);
        if (estimatedContributionRateData == null) {
            estimatedContributionRateData = [SELECT Discretionary_Income_Low__c, Discretionary_Income_High__c, 
                                                    Expected_Contribution__c, Expected_Contribution_Percentage__c,
                                                    Threshold__c, Academic_Year__r.Name
                                                FROM Expected_Contribution_Rate__c 
                                                WHERE Academic_Year__c = :academicYearId 
                                                ORDER BY Discretionary_Income_Low__c];
            estimatedContributionRateDataByYear.put(academicYearId, estimatedContributionRateData);
        }
        return estimatedContributionRateData;
    }
    
    public static Decimal getEstimatedContributionRate(Id academicYearId, Decimal discretionaryIncome) {
        Decimal estimatedContribution = null;
        
        if (discretionaryIncome == null) {
            throw new EfcException('Missing discretionary income for Estimated Contribution Rate lookup');
        }
        
        List<Expected_Contribution_Rate__c> estimatedContributionRateData = getEstimatedContributionRateData(academicYearId);
        if (estimatedContributionRateData.isEmpty()) {
            throw new EfcException.EfcMissingDataException('No Estimated Contribution Rate data found for academic year \'' + EfcUtil.getAcademicYearName(academicYearId) + '\'');
        }
        
        // make sure the discretionary income is rounded
        Decimal roundedDiscretionaryIncome = discretionaryIncome.round(RoundingMode.HALF_UP);
        
        // NAIS-1047: For negative discretionary income, use the expected contribution rate that corresponds to 0
        Decimal nonNegativeRoundedDiscretionaryIncome = (roundedDiscretionaryIncome < 0) ? 0 : roundedDiscretionaryIncome;
        
        Expected_Contribution_Rate__c matchingEcr = null;
        for (Expected_Contribution_Rate__c ecr : estimatedContributionRateData) {
            System.debug(LoggingLevel.ERROR, 'TESTINGDrewECR ' + nonNegativeRoundedDiscretionaryIncome  + '; ' +  ecr.Academic_Year__r.Name  + '; ' + ecr);

            // NAIS-1047: Do the lookup based on the non-negative discretionary income
            if (nonNegativeRoundedDiscretionaryIncome >= ecr.Discretionary_Income_Low__c) {
                if ((ecr.Discretionary_Income_High__c == null) || (nonNegativeRoundedDiscretionaryIncome <= ecr.Discretionary_Income_High__c)) {
                    matchingEcr = ecr;
                    break;
                }
            }
        }
        
        if (matchingEcr != null) {
            if (matchingEcr.Threshold__c == null) {
                throw new EfcException('Missing Threshold value in Estimated Contribution table for income value = ' + discretionaryIncome);
            }
            estimatedContribution = matchingEcr.Expected_Contribution__c 
                    + (matchingEcr.Expected_Contribution_Percentage__c * (roundedDiscretionaryIncome - matchingEcr.Threshold__c));
        }
        
        if (estimatedContribution == null) {
            throw new EfcException.EfcMissingDataException('No Estimated Contribution found for academic year \'' + EfcUtil.getAcademicYearName(academicYearId) + '\' for value = ' + discretionaryIncome);
        }
        
        return estimatedContribution.round(RoundingMode.HALF_UP);
    }
}