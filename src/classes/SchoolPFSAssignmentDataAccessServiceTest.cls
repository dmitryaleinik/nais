/**
 * SchoolPFSAssignmentDataAccessServiceTest.cls
 *
 * @description: Test class for SchoolPFSAssignmentDataAccessService using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
private class SchoolPFSAssignmentDataAccessServiceTest {
    
    /* test data setup */
    @testSetup 
    static void setupTestData() {
        MassEmailSendTestDataFactory.createEmailTemplate();
        
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
        }
    }
    
    /* negative test cases */

    // Test getSchoolPFSAssignmentById with null param
    @isTest 
    static void getSchoolPFSAssignmentById_nullParam_nullResult() {
        
        // Arrange
        School_PFS_Assignment__c expectedResult;

        // Act
        Test.startTest();
            expectedResult = new SchoolPFSAssignmentDataAccessService().getSchoolPFSAssignmentById( null);
        Test.stopTest();

        // Assert
        System.assertEquals( null, expectedResult);
    }

    // Test getSPAEmailEventByIdSet with null params
    @isTest 
    static void getSPAEmailEventByIdSet_nullParams_nullResult() {
        
        // Arrange
        List<School_PFS_Assignment__c> expectedResultList;

        // Act
        Test.startTest();
            expectedResultList = new SchoolPFSAssignmentDataAccessService().getSPAEmailEventByIdSet( null, null);
        Test.stopTest();

        // Assert
        System.assertEquals( null, expectedResultList);
    }

    // Test getSchoolPFSByIdSet with null param
    @isTest 
    static void getSchoolPFSByIdSet_nullParam_nullResult() {
        
        // Arrange
        List<School_PFS_Assignment__c> expectedResultList;

        // Act
        Test.startTest();
            expectedResultList = new SchoolPFSAssignmentDataAccessService().getSchoolPFSByIdSet( null);
        Test.stopTest();

        // Assert
        System.assertEquals( null, expectedResultList);
    }

    // Test getSchoolPFSByParentAIdSet with null params
    @isTest 
    static void getSchoolPFSByParentAIdSet_nullParams_nullResult() {
        
        // Arrange
        List<School_PFS_Assignment__c> expectedResultList;

        // Act
        Test.startTest();
            expectedResultList = new SchoolPFSAssignmentDataAccessService().getSchoolPFSByParentAIdSet( null, null, null, null);
        Test.stopTest();

        // Assert
        System.assertEquals( null, expectedResultList);
    }

    // Test getSchoolPFSByStudentFolderId with null param
    @isTest 
    static void getSchoolPFSByStudentFolderId_nullParam_nullResult() {
        
        // Arrange
        List<School_PFS_Assignment__c> expectedResultList;

        // Act
        Test.startTest();
            expectedResultList = new SchoolPFSAssignmentDataAccessService().getSchoolPFSByStudentFolderId( null);
        Test.stopTest();

        // Assert
        System.assertEquals( null, expectedResultList);
    }
    
    
    /* positive test cases */
    
    // Test getSchoolPFSAssignmentById with valid param
    @isTest 
    static void getSchoolPFSAssignmentById_validParam_validResult() {
        
        // Arrange
        MassEmailSendTestDataFactory.createRequiredPFSData();
        School_PFS_Assignment__c expectedResult = [select Id from School_PFS_Assignment__c limit 1];

        // Act
        Test.startTest();
            expectedResult = new SchoolPFSAssignmentDataAccessService().getSchoolPFSAssignmentById( expectedResult.Id);
        Test.stopTest();

        // Assert
        System.assertNotEquals( null, expectedResult);
    }

    // Test getSPAEmailEventByIdSet with valid params
    @isTest 
    static void getSPAEmailEventByIdSet_validParams_validResult() {
        
        // Arrange
        MassEmailSendTestDataFactory.createRequiredPFSData();
        List<School_PFS_Assignment__c> expectedResultList = [select Id from School_PFS_Assignment__c];
        Set<String> idSet = new Set<String>();
        for ( School_PFS_Assignment__c sPFS : expectedResultList) {

            idSet.add( sPFS.Id);
        }
        expectedResultList = null;

        // Act
        Test.startTest();
            expectedResultList = new SchoolPFSAssignmentDataAccessService().getSPAEmailEventByIdSet( idSet, true);
        Test.stopTest();

        // Assert
        System.assertNotEquals( null, expectedResultList);
    }

    // Test getSchoolPFSByIdSet with valid param
    @isTest 
    static void getSchoolPFSByIdSet_validParam_validResult() {
        
        // Arrange
        MassEmailSendTestDataFactory.createRequiredPFSData();
        List<School_PFS_Assignment__c> expectedResultList = [select Id from School_PFS_Assignment__c];
        Set<String> idSet = new Set<String>();
        for ( School_PFS_Assignment__c sPFS : expectedResultList) {

            idSet.add( sPFS.Id);
        }
        expectedResultList = null;

        // Act
        Test.startTest();
            expectedResultList = new SchoolPFSAssignmentDataAccessService().getSchoolPFSByIdSet( idSet);
        Test.stopTest();

        // Assert
        System.assertNotEquals( null, expectedResultList);
    }

    // Test getSchoolPFSByParentAIdSet with valid param
    @isTest 
    static void getSchoolPFSByParentAIdSet_validParam_validResult() {
        
        // Arrange
        MassEmailSendTestDataFactory.createRequiredPFSData();
        List<Contact> contactList = [select Id from Contact];
        List<Account> acctList = [select Id from Account];
       	Set<String> acYearSet = new Set<String> { '2016-2017'};
        Set<String> contactIdSet = new Set<String>();
        for ( Contact c : contactList) {

            contactIdSet.add( c.Id);
        }
        Set<String> acctIdSet = new Set<String>();
        for ( Account a : acctList) {

            acctIdSet.add( a.Id);
        }

        // Act
        Test.startTest();
            List<School_PFS_Assignment__c> expectedResultList = 
            	new SchoolPFSAssignmentDataAccessService().getSchoolPFSByParentAIdSet( contactIdSet, acctIdSet, acYearSet, false);
        Test.stopTest();

        // Assert
        System.assertNotEquals( null, expectedResultList);
    }

    // Test getSchoolPFSByStudentFolderId with valid param
    @isTest 
    static void getSchoolPFSByStudentFolderId_validParam_validResult() {
        
        // Arrange
        MassEmailSendTestDataFactory.createRequiredPFSData();
        Student_Folder__c studentFolder = [select Id from Student_Folder__c limit 1];

        // Act
        Test.startTest();
            List<School_PFS_Assignment__c> expectedResultList = 
            	new SchoolPFSAssignmentDataAccessService().getSchoolPFSByStudentFolderId( studentFolder.Id);
        Test.stopTest();

        // Assert
        System.assertNotEquals( null, expectedResultList);
    }

}