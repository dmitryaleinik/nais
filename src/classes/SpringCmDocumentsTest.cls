@isTest
private class SpringCmDocumentsTest {
    
    private static Id springCMDocumentId;
    private static Id familyDocumentId;

    private static List<Databank_Doc_Type_Mapping__c> insertDefaultDocTypeMappings() {
        List<Databank_Doc_Type_Mapping__c> testMappings = new List<Databank_Doc_Type_Mapping__c>();
        testMappings.add(new Databank_Doc_Type_Mapping__c(name = 'testmapping01', Databank_Name__c = 'W2', Salesforce_Name__c = 'W2', Active__c = true));
        testMappings.add(new Databank_Doc_Type_Mapping__c(name = 'testmapping02', Databank_Name__c = 'IRS_1040', Salesforce_Name__c = '1040', Active__c = true));
        testMappings.add(new Databank_Doc_Type_Mapping__c(name = 'testmapping02.01', Databank_Name__c = 'IRS_1040', Salesforce_Name__c = '1040 and all DOCS', Active__c = true, For_Salesforce_to_Databank_Mapping_ONLY__c = true));
        testMappings.add(new Databank_Doc_Type_Mapping__c(name = 'testmapping03', Databank_Name__c = 'IRS_1040A', Salesforce_Name__c = '1040A', Active__c = true));
        testMappings.add(new Databank_Doc_Type_Mapping__c(name = 'testmapping04', Databank_Name__c = 'IRS_1040EZ', Salesforce_Name__c = '1040EZ', Active__c = true));
        testMappings.add(new Databank_Doc_Type_Mapping__c(name = 'testmapping06', Databank_Name__c = 'TX_SCH C', Salesforce_Name__c = 'Tax Schedule C', Active__c = true));
        testMappings.add(new Databank_Doc_Type_Mapping__c(name = 'testmapping07', Databank_Name__c = 'TX_SCH_CEZ', Salesforce_Name__c = 'Tax Schedule C-EZ', Active__c = true));
        testMappings.add(new Databank_Doc_Type_Mapping__c(name = 'testmapping08', Databank_Name__c = 'TX_SCH E', Salesforce_Name__c = 'Tax Schedule E', Active__c = true));
        testMappings.add(new Databank_Doc_Type_Mapping__c(name = 'testmapping09', Databank_Name__c = 'TX_SCH F', Salesforce_Name__c = 'Tax Schedule F', Active__c = true));
        testMappings.add(new Databank_Doc_Type_Mapping__c(name = 'testmapping10', Databank_Name__c = 'TAXFORM 4562', Salesforce_Name__c = '4562 Depreciation and Section 179 Exp', Active__c = true));
        testMappings.add(new Databank_Doc_Type_Mapping__c(name = 'testmapping11', Databank_Name__c = 'TX_4506', Salesforce_Name__c = 'Form 4506, 4506-T or 4506T-EZ (signed and dated)', Active__c = true));
        testMappings.add(new Databank_Doc_Type_Mapping__c(name = 'testmapping12', Databank_Name__c = 'TX_4506_T', Salesforce_Name__c = 'Form 4506, 4506-T or 4506T-EZ (signed and dated)', Active__c = true));
        testMappings.add(new Databank_Doc_Type_Mapping__c(name = 'testmapping13', Databank_Name__c = 'TX_4506_TEZ', Salesforce_Name__c = 'Form 4506, 4506-T or 4506T-EZ (signed and dated)', Active__c = true));
        testMappings.add(new Databank_Doc_Type_Mapping__c(name = 'testmapping21', Databank_Name__c = 'TX_SCH_A', Salesforce_Name__c = 'Tax Schedule A', Active__c = true, X1040_Related_Doc__c = true));

        insert testMappings;

        return testMappings;
    }

    private static void testDataForDefaultedInfoForSpringCMDoc(
        Boolean isFeatureEnabled,
        String documentType,
        String documentYear) {
        
        insert new Databank_Settings__c(
	        Name = 'Databank',
	        Username__c = 'Databank',
	        apiKey__c = 'Databank',
	        accountId__c = 'Databank');
        
        //0. Create current academic year.
        Academic_Year__c academicYear = AcademicYearTestData.Instance.insertAcademicYear();
        
        //1. Create School.
        Account school = AccountTestData.Instance.asSchool().insertAccount();
        
        //2. Create contacts for parent and student.
        Contact parent = ContactTestData.Instance.asParent().create();
        Contact student = ContactTestData.Instance.asStudent().create();
        insert new List<Contact> { parent, student };
        
        //3. Create PFS record.
        PFS__c pfs = PfsTestData.Instance
            .forParentA(parent.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .asPaid()
            .insertPfs();
            
        //4. Create applicant.
        Applicant__c applicant = ApplicantTestData.Instance
            .forContactId(student.Id)
            .forPfsId(pfs.Id)
            .insertApplicant();
            
        //5. Create Folder.
        Student_Folder__c folder = StudentFolderTestData.Instance.forName('Folder1')
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student.Id)
            .forSchoolId(school.Id)
            .insertStudentFolder();
            
        //6. Create SPA.
        School_PFS_Assignment__c spa = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forApplicantId(applicant.Id)
            .forSchoolId(school.Id)
            .forStudentFolderId(folder.Id)
            .insertSchoolPfsAssignment();
            
        //7. Create the household that will be related to test parents.
        Household__c household = HouseholdTestData.Instance.DefaultHousehold;
            
        //8. Create Family Document.
        Family_Document__c familyDocument = FamilyDocumentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forHousehold(household.Id)
            .withImportId('unittest1a')
            .forDocYear(documentYear)
            .forDocumentType(documentType)
            .insertFamilyDocument();
            
        //9. Set the toggle to enable/disable the defaultInfoFromFamilyDocument feature
        FeatureToggles.setToggle('Default_Missing_SCM_Doc_Info__c', isFeatureEnabled);
        
        //12. Get the PFS Number
        String pfsNumber = [SELECT Id, PFS_Number__c FROM PFS__c WHERE Id =: pfs.Id LIMIT 1].PFS_Number__c;
        
        //11. Create the SpringnCM document.
        SpringCM_Document__c springCMDocument = SpringCMDocumentTestData.Instance
                .forPfs(pfsNumber)
                .forFamilyDocumentId(familyDocument.Id)
                .withImportId('unittest1a')
                .forDocumentYear(null)
                .forDocumentType(null)
                .insertScmDocument();
        
        //12. Get the SpringCM Id.
        springCMDocumentId = springCMDocument.Id;
        
        //13. Get the FamilyDocument Id.
        familyDocumentId = familyDocument.Id;
    }

    @testSetup
    private static void setupTests() {
        insertDefaultDocTypeMappings();
    }

    @isTest 
    private static void processDefaultMissingSCMDocInfo_SpringCMDocWithEmptyInfo_expectSpringCMDocGetDefaultValues() {
        
        String documentType = '1040'; // This is the doc type we use to create the family document for.
        String expectedDocType = 'IRS_1040'; // This is the doc type we expect on the SCM Doc record;
        String documentYear = '2018';
        
        //0. Create test data.
        testDataForDefaultedInfoForSpringCMDoc(true, documentType, documentYear);
        
        Test.startTest();
           
           //1. Get the SpringCMDocument record with all its fields.
           List<String> springCMDocumentFields = new List<String>();
           springCMDocumentFields.addAll((Schema.SObjectType.SpringCM_Document__c.fields.getMap()).keySet());
        
           SpringCM_Document__c springCMDocument = Database.query('SELECT ' + String.join(springCMDocumentFields, ',') + 
               ' FROM SpringCM_Document__c WHERE Id =: springCMDocumentId LIMIT 1');
               
           System.assertEquals(documentYear, springCMDocument.Document_Year__c,
                'The SpringCMDocument documentYear field must be defaulted to its related FamilyDocument.DocumentYear');
           
           System.assertEquals(expectedDocType, springCMDocument.Document_Type__c,
                'The SpringCMDocument documentType field must be defaulted to its related FamilyDocument.DocumentType');
            
            System.assertEquals(true, springCMDocument.Doc_Info_Defaulted__c,
                'The SpringCMDocument fields must be defaulted to its related FamilyDocument.');
            
            String issueCategories = [SELECT Id, Issue_Categories__c FROM Family_Document__c WHERE Id =: familyDocumentId LIMIT 1].Issue_Categories__c;
            
            System.assertEquals(true, issueCategories.contains('Unable To Capture Doc Type/Year'),
                'We need to make sure the right issue categories are selected so that families are told that '+
                'they need to upload a new document.');
        
        Test.stopTest();
    }
    
    
    @isTest 
    private static void processDefaultMissingSCMDocInfo_SpringCMDocWithEmptyInfoAndFeatureDisabled_expectSpringCMDocGetDefaultValues() {
        
        String documentTypeFD = '1040';
        String documentYearFD = '2018';
        
        //0. Create test data.
        testDataForDefaultedInfoForSpringCMDoc(false, documentTypeFD, documentYearFD);
        
        Test.startTest();
           
           //1. Get the SpringCMDocument record with all its fields.
           List<String> springCMDocumentFields = new List<String>();
           springCMDocumentFields.addAll((Schema.SObjectType.SpringCM_Document__c.fields.getMap()).keySet());
        
           SpringCM_Document__c springCMDocument = Database.query('SELECT ' + String.join(springCMDocumentFields, ',') + 
               ' FROM SpringCM_Document__c WHERE Id =: springCMDocumentId LIMIT 1');
               
           System.assertEquals(null, springCMDocument.Document_Year__c,
                'The SpringCMDocument documentYear field must be defaulted to its related FamilyDocument.DocumentYear' + 
                'only if the Toggle.Default_Missing_SCM_Doc_Info__c is Enabled.');
           
           System.assertEquals(null, springCMDocument.Document_Type__c,
                'The SpringCMDocument documentType field must be defaulted to its related FamilyDocument.DocumentType' + 
                'only if the Toggle.Default_Missing_SCM_Doc_Info__c is Enabled.');
                
            System.assertEquals(false, springCMDocument.Doc_Info_Defaulted__c);
        
        Test.stopTest();
    }
}