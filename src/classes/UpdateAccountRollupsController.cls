public with sharing class UpdateAccountRollupsController {
    
    public UpdateAccountRollupsController(){
        
    }
    
    public PageReference updateAccountRollups(){
        //executing the bactch job
        UpdateAccountRollUpsBatch uarBatch = new UpdateAccountRollUpsBatch();
        Id batchJobId = Database.executeBatch(uarBatch, 100);
        Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'A batch has been started to update these records, it could take quite a while. Please check the Apex Jobs area in Set-up for more information.'));
        return null;
    }
     
     
}