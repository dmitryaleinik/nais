/**
* @description This class is used to create Transaction Annual Settings records for unit tests.
*/
public with sharing class TransactionAnnualSettingsTestData extends sObjectTestData
{

    @testVisible private static final String TRANSACTION_ANNUAL_SETTINGS_NAME = 'Transaction Annual Settings';

    /**
   * @description Get the default values for the TransactionAnnualSettings__c object.
   * @return A map of Schema.SObjectFields to their default values.
   */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap()
    {
        return new Map<Schema.SObjectField,Object>
            {
                  Transaction_Annual_Settings__c.Name => TRANSACTION_ANNUAL_SETTINGS_NAME
            };
    }

    /**
    * @description Get the Transaction_Annual_Settings__c SObjectType.
    * @return The Transaction_Annual_Settings__c SObjectType.
    */
    protected override Schema.sObjectType getSObjectType()
    {
        return Transaction_Annual_Settings__c.SObjectType;
    }

    /**
    * @description Static singleton instance property.
    */
   public static TransactionAnnualSettingsTestData Instance
   {
       get
       {
           if (Instance == null)
           {
               Instance = new TransactionAnnualSettingsTestData();
           }
           return Instance;
       }
       private set;
   }

    /**
    * @description Set the Name field on the current Transaction Annual Settings record.
    * @param name The Name to set on the Transactional Annual Settings.
    * @return The current working instance of TransactionAnnualSettingsTestData.
    */
    public  TransactionAnnualSettingsTestData forName(String name)
    {
        return (TransactionAnnualSettingsTestData) with(Transaction_Annual_Settings__c.Name, name);
    }

    /**
   * @description Set the Quantity__c field on the current Transaction Annual Settings record.
   * @param quantity The Quantity to set on the Transactional Annual Settings.
   * @return The current working instance of TransactionAnnualSettingsTestData.
   */
    public  TransactionAnnualSettingsTestData forQuantity(Decimal quantity)
    {
        return (TransactionAnnualSettingsTestData) with(Transaction_Annual_Settings__c.Quantity__c, quantity);
    }

    /**
   * @description Set the Year__c field on the current Transaction Annual Settings record.
   * @param year The Year to set on the Transactional Annual Settings.
   * @return The current working instance of TransactionAnnualSettingsTestData.
   */
    public  TransactionAnnualSettingsTestData forYear(String year)
    {
        return (TransactionAnnualSettingsTestData) with(Transaction_Annual_Settings__c.Year__c, year);
    }

    /**
   * @description Set the Product_Code__c field on the current Transaction Annual Settings record.
   * @param productCode The Product Code to set on the Transactional Annual Settings.
   * @return The current working instance of TransactionAnnualSettingsTestData.
   */
    public  TransactionAnnualSettingsTestData forProductCode(String productCode)
    {
        return (TransactionAnnualSettingsTestData) with(Transaction_Annual_Settings__c.Product_Code__c, productCode);
    }

    /**
       * @description Set the Product_Amount__c field on the current Transaction Annual Settings record.
       * @param productAmount The Product Amount to set on the Transactional Annual Settings.
       * @return The current working instance of TransactionAnnualSettingsTestData.
       */
    public  TransactionAnnualSettingsTestData forProductAmount(Decimal productAmount)
    {
        return (TransactionAnnualSettingsTestData) with(Transaction_Annual_Settings__c.Product_Amount__c, productAmount);
    }

    /**
     * @description Create the current working Transaction_Annual_Setting__c record.
     * @return The currently operated upon  Transaction_Annual_Setting__c.
     */
    public Transaction_Annual_Settings__c create()
    {
        return (Transaction_Annual_Settings__c)super.buildWithReset();
    }

    /**
     * @description Create the current working Transaction_Annual_Settings__c record without resetting
     *              the stored values in this instance of TransactionAnnualSettingsTestData.
     * @return A non-inserted Transaction_Annual_Settings__c record using the currently stored field values.
     */
    public Transaction_Annual_Settings__c createTransactionAnnualSettingsWithoutReset()
    {
        return (Transaction_Annual_Settings__c)buildWithoutReset();
    }

    /**
    * @description Insert the current working Transaction_Annual_Settings__c record.
    * @return The currently operated upon Transaction_Annual_Settings__c record.
    */
    public Transaction_Annual_Settings__c insertTransactionAnnualSettings()
    {
        return (Transaction_Annual_Settings__c)insertRecord();
    }

    /**
    * @description The default Transaction_Annual_Settings__c record.
    */
    public Transaction_Annual_Settings__c DefaultTransactionAnnualSettings
    {
        get
        {
            if (DefaultTransactionAnnualSettings == null)
            {
                DefaultTransactionAnnualSettings = createTransactionAnnualSettingsWithoutReset();
                insert DefaultTransactionAnnualSettings;
            }
            return DefaultTransactionAnnualSettings;
        }
        private set;
    }

    public TransactionAnnualSettingsTestData(){}
}