/**
 * @description Controls the FamilyDocumentSpringUpload component.
 **/
public class FamilyDocumentSpringUploadController {
    /* EFORM SETTINGS */
    public String SPRINGCM_BASE_URL;
    public String SPRINGCM_PARAM_AID;
    public String SPRINGCM_PARAM_FORMUID;
    public String SPRINGCM_PARAM_SUPPRESSNAVIGATION = 'true';
    public String SPRINGCM_PARAM_RU_BASE;

    /* PROPERTIES */
    public PFS__c pfsRecordParam {get; set;}
    public String academicYearIdParam {get; set;}
    public String documentSourceParam {get; set;}
    public String returnUrlParam {get; set;}
    public String docYear {get; set;}
    public String docType {get; set;}
    public String docName {get; set;}//NAIS-2334
    public String docId {get; set;}
    public String schoolCode {get; set;}
    public boolean createSchoolAsmnt {get; set;}
    public String pertainsTo; //
    public String springCMDocType {get; set;}
    public String schoolSpecificLabel {get; set;}
    public String naReason {get; set;}
    public String dialogPage {get; set;}
    public String currentRequestUrl {get; set;}
    public String sdaId {get; set;}

    public Family_Document__c issueFamDoc {get; set;}     // NAIS-2256 [DP] 02.12.2015

    public Boolean showSafariText {get; set;}
    public Boolean userAlreadyOptedOutOfSafariText {get; set;}
    public Boolean dontShowSafariTextAgainInput {get; set;}
    private User theUser;

    public Boolean famDocIllegible {get; set;}     // NAIS-2256 [DP] 02.12.2015
    public Boolean famDocMisLabeled {get; set;}    // NAIS-2256 [DP] 02.12.2015

    public Boolean renderSchoolRequiredText {get; set;}

    public PageReference getSpringCMUploadNewWindow(){
        PageReference pr = Page.SpringCMAjaxUploadPage;
        SpringCMAjaxUploadController.persistURLParams(pr);
        pr.getParameters().put('fDocId', docId);
        pr.getParameters().put('pfsnum', pfsRecordParam.PFS_Number__c);
        pr.getParameters().put('fellback', 'true');
        return pr;
    }


    public Help_Configuration__c theHelp;
    public Help_Configuration__c getTheHelp(){
        if (theHelp == null){
            theHelp = GlobalVariables.getCurrentTermsAndConditionsHelpRecord();
        }
        return theHelp;
    }

    public String getPertainsTo(){
        if (pertainsTo == 'A' || pertainsTo == 'B'){
            return 'Parent ' + pertainsTo;
        }
        return pertainsTo;
    }

    public void setPertainsTo(String s){
        pertainsTo = SpringCMAction.getDatabankPertainsValueFromSFPertainsValue(s);
    }

    // [CH] NAIS-888 - Adding ability to select specific schools
    public String selectedSchools {get; set;}
    public List<SelectOption> schoolsList {
        get{
            if(schoolsList == null){
                schoolsList = new List<SelectOption>{};
                Map<String, School_PFS_Assignment__c> dedupeMap = new Map<String, School_PFS_Assignment__c>{};
                // Create a list of select options with the School name as the label and School Id as the value
                for (School_PFS_Assignment__c spa : [SELECT Id, School__c, School__r.Name
                                                        FROM School_PFS_Assignment__c
                                                        WHERE School_PFS_Assignment__c.Applicant__r.PFS__c = :pfsRecordParam.Id
                                                        AND Withdrawn__c != 'Yes'
                                                        ORDER BY School__r.Name desc]) {
                    // Need to put into a map first to deduplicate
                    dedupeMap.put(spa.School__c, spa);
                }

                for(School_PFS_Assignment__c spa : dedupeMap.values()){
                    schoolsList.add(new SelectOption(spa.School__c, spa.School__r.Name));
                }
            }
            return schoolsList;
        }
        set;
    }
    // NAIS-1805 Start
    public Boolean isSchoolPortal {
        get {
            Id currentUser = UserInfo.getProfileId();
            return GlobalVariables.isSchoolPortalProfile(currentUser);
        }
        set;
    }
    // NAIS-1805 End

    public Boolean isFamilyPortal {
        get {
            Id currentUser = UserInfo.getProfileId();
            return GlobalVariables.isFamilyPortalProfile(currentUser);
        }
        set;
    }

    // SFP-29 START
    public String DBVerifyFlag {
        get {
            Boolean doVerify = false;
            for (School_Document_Assignment__c sda : [select Verify_Data__c from School_Document_Assignment__c where Document__c = :docId and Verify_Data__c = true]) {
                doVerify = sda.Verify_Data__c;
            }
            return doVerify ? 'T' : 'F';
        }
    }
    // SFP-29 END

    // SFP-65, SFP-66 START
    public Family_Document__c fDoc {
        get {
            Family_Document__c fd = [Select Id, Name, Document_Year__c, Document_Type__c, Household__c, Household__r.Name, Document_Source__c, School_Code__c FROM Family_Document__c WHERE Id = :docId];
            return fd;
        }
    }
    // SFP-65, SFP-66 END

    //SFP-171 : disable viewing and uploading of docs when within a maintenance window
    public boolean getIsMaintenanceWindow(){
        return GlobalVariables.isMaintenanceWindow();
    }

    /* INITIALIZATION */
    public FamilyDocumentSpringUploadController() {
        system.debug(isSchoolPortal);
        // query User to see if they have alreayd opted out of Safari text
        showSafariText = false;
        for (User u : [Select Id, SYSTEM_Hide_Safari_Text__c, SpringCM_Authentication_Errors__c From User where Id = :UserInfo.getUserId() limit 1]){
            theUser = u;
        }
        if (theUser != null){
            userAlreadyOptedOutOfSafariText = theUser.SYSTEM_Hide_Safari_Text__c;
        }

        currentRequestUrl = System.URL.getCurrentRequestUrl().toExternalForm();

        // get SpringCM settings
        if (!Test.isRunningTest()) {
            Databank_Settings__c scmSettings;
            scmSettings = Databank_Settings__c.getInstance('Databank');
            if (scmSettings != null) {
                SPRINGCM_BASE_URL = scmSettings.Upload_EForm_Base_Url__c;
                SPRINGCM_PARAM_AID = scmSettings.AccountId__c;
                SPRINGCM_PARAM_FORMUID = scmSettings.Upload_EForm_Form_UID__c;
                SPRINGCM_PARAM_RU_BASE = scmSettings.Upload_EForm_RU_Param__c;
            }
        }
        else {
            SPRINGCM_BASE_URL = 'Unit Test Value';
            SPRINGCM_PARAM_AID = 'Unit Test Value';
            SPRINGCM_PARAM_FORMUID = 'Unit Test Value';
            SPRINGCM_PARAM_RU_BASE = 'Unit Test Value';
        }

        renderSchoolRequiredText = FALSE;
    }

    /* ACTION METHODS */
    public void doInitNADialog() {
        dialogPage = 'NA_DOC';
    }

    // NAIS-2194 [DP] 01.29.2015 allow parent to unrequest an NA
    public void doInitNARemoveDialog() {
        dialogPage = 'NA_DOC_REMOVE';
    }

    public void doDocRemoveDialog() {
        dialogPage = 'DOC_REMOVE';
    }

    //NAIS-1898 [DP] try authentication and return different dialog boxes depending on result
    public void doInitUploadDialog() {
        // TRY AUTHENTICATION HERE
        Boolean autheticateSuccessful = tryAuthentication();
        if (autheticateSuccessful){
            dialogPage = 'SELECT_DOC';
        } else {
            dialogPage = 'AUTH_ERROR';
        }
    }


    // [DP] Added for NAIS-1548 2.4.14
    public void doInitIECOMP() {
        dialogPage = 'IE_COMP';
    }

    // NAIS-2256 [DP] 02.12.2015
    public void doInitNADenyDialog() {
        dialogPage = 'NA_DOC_DENY';
    }

    // NAIS-2256 [DP] 02.12.2015
    public void doInitIssueSetDialog() {
        dialogPage = 'ISSUE_DOC_SET';
        famDocIllegible = false;
        famDocMisLabeled = false;
        if (String.isBlank(docId)){
            System.debug('ERROR No Doc Id');
        } else {
            issueFamDoc = [Select Id, Issue_Categories__c FROM Family_Document__c where Id = :docId];
            famDocIllegible = !String.isBlank(issueFamDoc.Issue_Categories__c) && issueFamDoc.Issue_Categories__c.contains(SpringCMAction.illegibleValue);
            famDocMisLabeled = !String.isBlank(issueFamDoc.Issue_Categories__c) && issueFamDoc.Issue_Categories__c.contains(SpringCMAction.mislabledValue);
        }
    }

    // SPF-67 [DP] 10.13.2015 split this into two to avoid pending work errors
    public void doSpringUploadStep1() {
        if (createSchoolAsmnt != true) {
            if (String.isBlank(docId)) {
                if (docType == 'Other/Misc Tax Form') {//NAIS-2334
                    docId = saveNewDocument(pfsRecordParam, docType, docYear, pertainsTo, documentSourceParam, schoolCode, schoolSpecificLabel, docName);
                }else{
                    docId = saveNewDocument(pfsRecordParam, docType, docYear, pertainsTo, documentSourceParam, schoolCode, schoolSpecificLabel, null);
                }
            }
            else {
                if (docType == 'Other/Misc Tax Form') {//NAIS-2334
                    docId = replaceDocument(docId, pfsRecordParam, docType, docYear, pertainsTo, documentSourceParam, schoolSpecificLabel, docName);
                }else{
                    docId = replaceDocument(docId, pfsRecordParam, docType, docYear, pertainsTo, documentSourceParam, schoolSpecificLabel, null);
                }
            }
        }
        else {
            if (docType == 'Other/Misc Tax Form') {//NAIS-2334
                docId = saveAdditionalDocument(pfsRecordParam, docYear, pertainsTo, documentSourceParam, selectedSchools, docName);
            }
            else {
                docId = addAndSaveNewDocument(pfsRecordParam, docType, docYear, pertainsTo, documentSourceParam, schoolCode, schoolSpecificLabel);
            }
        }
    }

    public PageReference doSpringUploadStep2(){
        return getSpringCMUploadNewWindow();
    }

    public PageReference doSpringUploadAndRedirect() {
        if (docType == 'Other/Misc Tax Form' && !isSchoolPortal && String.isBlank(selectedSchools)) {
                renderSchoolRequiredText = true;
                return null;
        }
        doSpringUploadStep1();
        return doSpringUploadStep2();
    }

    public void doSetDocNA() {
        docId = setNADocument(pfsRecordParam, docType, docYear, pertainsTo, documentSourceParam, schoolCode, schoolSpecificLabel, naReason, docName);
    }

    // NAIS-2194 [DP] 01.29.2015 allow parent to unrequest an NA
    public void removeNADoc() {
        if (String.isBlank(docId)){
            System.debug('ERROR No Doc Id');
        } else {
            Family_Document__c fd = 
                FamilyDocumentsSelector.newInstance().selectByIdWithSchoolDocumentAssignmentsWithCustomFieldLists(
                    new Set<Id>{docId},
                    new List<String>{'Id','Document_Status__c','Reason_for_Request__c','Request_to_Waive_Requirement__c'},
                    new List<String>{'Id','Requirement_Waiver_Status__c'},
                    true)[0];

            fd.Document_Status__c = 'Not Received';
            fd.Reason_for_Request__c = '';
            fd.Request_to_Waive_Requirement__c = 'No';

            List<School_Document_Assignment__c> sdaListForUpdate = new List<School_Document_Assignment__c>();
            for (School_Document_Assignment__c sda : fd.School_Document_Assignments__r){
                if (sda.Requirement_Waiver_Status__c != null && sda.Requirement_Waiver_Status__c != ''){
                    sda.Requirement_Waiver_Status__c = '';
                    sdaListForUpdate.add(sda);
                }
            }

            update fd;

            if (!sdaListForUpdate.isEmpty()){
                update sdaListForUpdate;
            }

        }
    }

    public void removeDoc() {
        if (String.isBlank(docId)) {
            return;
        }

        List<Family_Document__c> familyDocuments = 
            FamilyDocumentsSelector.newInstance().selectByIdWithSchoolDocumentAssignmentsWithCustomFieldLists(
                new Set<Id>{docId},
                new List<String>{'Id','Document_Status__c','Reason_for_Request__c','Request_to_Waive_Requirement__c, Deleted__c'},
                new List<String>{'Id','Requirement_Waiver_Status__c, Document__c'},
                true);

        if (familyDocuments.isEmpty()) {
            return;
        }
        
        //SFP-1118: Logic to update related SDAs was moved to FamilyDocumentAfter.trigger (updateDocumentStatusFromFamilyDocuments)
        Family_Document__c familyDocToUpdate = familyDocuments[0];
        familyDocToUpdate.Document_Status__c = 'Not Received';
        familyDocToUpdate.Deleted__c = true;
        update familyDocToUpdate;
    }

    // NAIS-2256 [DP] 02.12.2015
    public void doApproveNADoc(){
        if( !String.isBlank(sdaId) ){
            dialogPage = 'NA_DOC_APPROVE';
            this.propagatingRequirementWaiverStatusToSiblings(sdaId, 'Approved');
        }
    }

    // NAIS-2256 [DP] 02.12.2015
    public void doDenyNADoc(){//familydocument, for the same school
        if( !String.isBlank(sdaId) ){
            this.propagatingRequirementWaiverStatusToSiblings(sdaId, 'Not Approved');
        }
    }//End:doDenyNADoc
    
    public void propagatingRequirementWaiverStatusToSiblings(Id sdaId, String newStatus)
    {
        //[G.S], SFP-21, Denying an N/A document request should replicate across siblings
        List<School_Document_Assignment__c> sdaList = [select Id, document__c,document__r.name, school__c 
                                                        from School_Document_Assignment__c 
                                                            where Id =: sdaId];

        if(sdaList != null && sdaList.size() > 0)
        {
            List<School_Document_Assignment__c> sibSdaList = new List<School_Document_Assignment__c>();
            School_Document_Assignment__c sda = sdaList.get(0);
            if(String.isNotBlank(sda.Document__c))
            {
                for(School_Document_Assignment__c sibSda : [select Id, Requirement_Waiver_Status__c 
                                                                from School_Document_Assignment__c 
                                                                    where document__c = :sda.document__c 
                                                                    and School__c = :sda.School__c]){
                    sibSda.Requirement_Waiver_Status__c = newStatus;
                    sibSdaList.add(sibSda);
                }
            }
            else{
                sda.Requirement_Waiver_Status__c = newStatus;
                sibSdaList.add(sda);
            }
            update sibSdaList;
        }
    }//End:propagatingRequirementWaiverStatusToSiblings
    
    // NAIS-2256 [DP] 02.12.2015
    public void doReportDocIssue(){
        if (String.isBlank(docId)){
            System.debug('ERROR No Doc Id');
        } else {

            if (issueFamDoc == null || issueFamDoc.Id != docId){
                issueFamDoc = [Select Id, Issue_Categories__c FROM Family_Document__c where Id = :docId];
            }
            Set<String> issueCats = new Set<String>();
            if (!String.isBlank(issueFamDoc.Issue_Categories__c)){
                issueCats.addAll(issueFamDoc.Issue_Categories__c.split(';'));
            }

            if (famDocMisLabeled){
                issueCats.add(SpringCMAction.mislabledValue);
            }

            if (famDocIllegible){
                issueCats.add(SpringCMAction.illegibleValue);
            }

            String issues = '';
            for (String s : issueCats){
                issues += s + ';';
            }

            issueFamDoc.Issue_Categories__c = issues;
            update issueFamDoc;
        }
    }

    public void optOutOfSafariText(){
        userAlreadyOptedOutOfSafariText = true;
        if (theUser.SYSTEM_Hide_Safari_Text__c != true){
            theUser.SYSTEM_Hide_Safari_Text__c = true;
            update theUser;
        }
    }

    public void optIntoSafariText(){
        userAlreadyOptedOutOfSafariText = false;
        if (theUser.SYSTEM_Hide_Safari_Text__c != false){
            theUser.SYSTEM_Hide_Safari_Text__c = false;
            update theUser;
        }
    }
    /* HELPER METHODS */

    public static Id saveNewDocument(PFS__c pfsRecord, String docType, String docYear, String pertainsTo, String documentSource,
                                            String schoolCode, String schoolSpecificLabel, String docName) {
        // create a family doc
        Family_Document__c fd = new Family_Document__c();
        fd.School_Code__c = schoolCode; // NAIS-1364
        setFamilyDocumentFields(fd, pfsRecord, docType, docYear, pertainsTo, documentSource, schoolSpecificLabel);
        insert fd;

        DocumentService.SDAUpdateRequest request =
            new DocumentService.SDAUpdateRequest(
                    pfsRecord.Id,
                    fd.Id,
                    docType,
                    docYear,
                    schoolCode,
                    schoolSpecificLabel,
                    docName);

        DocumentService.Instance.updateDocumentAssignments(request);

        // return the family doc id
        return fd.Id;
    }

    public static Id replaceDocument(Id familyDocumentId, PFS__c pfsRecord, String docType, String docYear, String pertainsTo, String documentSource, String schoolSpecificLabel, String docName) {
        if (familyDocumentId != null) {
            // update the existing family document
            Family_Document__c fd = new Family_Document__c(Id = familyDocumentId);
            nullifyFields(fd); // NAIS-2193 [DP] 01.21.2015 clearing out fields so it filename, import id, and verification don't persist
            setFamilyDocumentFields(fd, pfsRecord, docType, docYear, pertainsTo, documentSource, schoolSpecificLabel);
            update fd;
        }

        return familyDocumentId;
    }

    public static Id setNADocument(PFS__c pfsRecord, String docType, String docYear, String pertainsTo, String documentSource,
                                            String schoolCode, String schoolSpecificLabel, String naReason, String docName) {
        // create a family doc
        Family_Document__c fd = new Family_Document__c();
        fd.School_Code__c = schoolCode; // NAIS-1364
        setFamilyDocumentFields(fd, pfsRecord, docType, docYear, pertainsTo, documentSource, null);
        fd.Document_Status__c = 'Not Applicable/Waive Requested';
        fd.Request_to_Waive_Requirement__c = 'Yes';
        fd.Reason_for_Request__c = naReason;
        insert fd;

        DocumentService.SDAUpdateRequest request =
                new DocumentService.SDAUpdateRequest(
                        pfsRecord.Id,
                        fd.Id,
                        docType,
                        docYear,
                        schoolCode,
                        schoolSpecificLabel,
                        docName);

        DocumentService.Instance.updateDocumentAssignments(request);

        // return the family doc id
        return fd.Id;
    }

    public static Id addAndSaveNewDocument(PFS__c pfsRecord, String docType, String docYear, String pertainsTo, String documentSource,
                                                String schoolCode, String schoolSpecificLabel) {
        // create a family doc
        Family_Document__c fd = new Family_Document__c();
        fd.School_Code__c = schoolCode; // NAIS-1364
        setFamilyDocumentFields(fd, pfsRecord, docType, docYear, pertainsTo, documentSource, schoolSpecificLabel);
        insert fd;

        // [CH] NAIS-1572 Refactoring to get all Applicants on the PFS, with SPAs for the current school
        // get the school document assignments corresponding to the doc type/year
        // List<School_Document_Assignment__c> sdaList = getSchoolDocumentAssignments(pfsRecord.Id, docType, docYear, schoolCode, schoolSpecificLabel);

        // get the corresponding School PFS Assignment Ids
        Set<Id> schoolPfsAssignmentIds = new Set<Id>();

        // If we want to pull the group of Applicants with the current school selected (School Portal)
        Map<Id, School_PFS_Assignment__c> spaMap;

        if(schoolCode != null && schoolCode != ''){
            spaMap = new Map<Id, School_PFS_Assignment__c> ([SELECT Id FROM School_PFS_Assignment__c WHERE School_PFS_Assignment__c.Applicant__r.PFS__c = :pfsRecord.Id and School__r.SSS_School_Code__c = :schoolCode]);
        }
        // If we want to pull all Applicants related to a PFS (Family Portal)
        else{
            spaMap = new Map<Id, School_PFS_Assignment__c> ([SELECT Id FROM School_PFS_Assignment__c WHERE School_PFS_Assignment__c.Applicant__r.PFS__c = :pfsRecord.Id]);
        }

        // create additional SDA documents for each of the existing SDAs
        // insertAdditionalSDARecords(docType, docYear, schoolPfsAssignmentIds, fd.Id);
        insertAdditionalSDARecords(docType, docYear, schoolSpecificLabel, spaMap.keySet(), fd.Id, null);

        // return the family doc id
        return fd.Id;

    }

    public static Id saveAdditionalDocument(PFS__c pfsRecord, String docYear, String pertainsTo, String documentSource, String selectedSchools, String docName) {
        //[CH] NAIS-888 Functionality for sending to specific schools
        // Parse selectedSchools list
        List<String> schoolIdList = new List<String>{};
        if(selectedSchools != null && selectedSchools != ''){
            schoolIdList = selectedSchools.split(':');
        }

        // create a family doc
        Family_Document__c fd = new Family_Document__c();
        setFamilyDocumentFields(fd, pfsRecord, 'Other/Misc Tax Form', docYear, pertainsTo, documentSource, null);
        insert fd;

        // get the School PFS Assignments for the PFS
        //[CH] NAIS-888 Refactored to accept list and default to pulling all
        Set<Id> schoolPfsAssignmentIds = new Set<Id>();
        List<School_PFS_Assignment__c> spaList;
        if(schoolIdList.size() > 0){
            spaList = [SELECT Id FROM School_PFS_Assignment__c WHERE School_PFS_Assignment__c.Applicant__r.PFS__c = :pfsRecord.Id and School__c in :schoolIdList];
        }
        else{
            spaList = [SELECT Id FROM School_PFS_Assignment__c WHERE School_PFS_Assignment__c.Applicant__r.PFS__c = :pfsRecord.Id];
        }

        for (School_PFS_Assignment__c spa : spaList) {
            schoolPfsAssignmentIds.add(spa.Id);
        }

        // create additional SDA documents for each of the School PFS Assignments
        insertAdditionalSDARecords('Other/Misc Tax Form', docYear, null, schoolPfsAssignmentIds, fd.Id, docName);

        // return the family doc id
        return fd.Id;
    }

    // creates (but not inserts) a new Family Document record for the document type/year
    private static void setFamilyDocumentFields(Family_Document__c fd, PFS__c pfsRecord, String docType, String docYear, String pertainsTo, String documentSource, String schoolSpecificLabel) {
        // NAIS-2383 [DP] 04.02.2015 refactor to get name for picklist value
        fd.Academic_Year_Picklist__c = pfsRecord.Academic_Year_Picklist__c;
        fd.Household__c = pfsRecord.Parent_A__r.Household__c;
        fd.Document_Type__c = docType;
        fd.Document_Year__c = docYear;
        fd.Document_Type_Original__c = docType; //NAIS-2313 [DP] 03.05.2015
        fd.Document_Year_Original__c = docYear; //NAIS-2313 [DP] 03.05.2015
        fd.Other_Document_Label__c = schoolSpecificLabel;

        fd.Name = pfsRecord.Parent_A__r.Name + ' ' + docType + ' ' + docYear;
        if(fd.Name.length() > 80){
            Integer lengthToTrim = fd.Name.Length() - 80;
            //Integer parentNameLength = pfsRecord.Parent_A__r.Name.length();
            //String trimmedParentName = pfsRecord.Parent_A__r.Name.substring(0, parentNameLength - lengthToTrim);
            Integer docTypeLength = docType.length();
            String trimmedDocType = docType.substring(0, docTypeLength - lengthToTrim);
            fd.Name = pfsRecord.Parent_A__r.Name + ' ' + trimmedDocType + ' ' + docYear;
        }
        fd.Parent_A_Email__c = pfsRecord.Parent_A_Email__c;
        fd.Parent_A_First_Name__c = pfsRecord.Parent_A_First_Name__c;

        if (pertainsTo != null) {
            // refactored to use mapping -- vf page just has single chars e.g., 'A'
            fd.Document_Pertains_to__c = SpringCMAction.getSFPertainsValueFromSpringCMPertainsValue(pertainsTo);
        }

        fd.Document_Source__c = documentSource;
        fd.Date_Received__c = System.today();
        fd.Document_Status__c = 'Not Received';

        // [DP] 05.18.2016 SFP-336 set the new Uploader_SSS_Code field
        fd.Uploader_SSS_Code__c = GlobalVariables.getCurrentSchoolSSSNumber();
    }

    // [CH] NAIS-1590 Re-factoring to handle Add-Another logic for School/Organization Other Docs
    private static void insertAdditionalSDARecords(String docType, String docYear, String schoolSpecificLabel, Set<Id> schoolPFSAssignmentIds, Id familyDocumentId, String docName) {
        List<School_Document_Assignment__c> newSDAList = new List<School_Document_Assignment__c>();

        // [DP] NAIS-2081 12.04.2014 if this is NOT an other/misc doc, we only want to create SDAs for schools that have required this doc type/year (and therefore already have an SDA)
        Set<Id> schoolPFSAssignmentIdsForThisDocType = new Set<Id>();
        if (docType != 'Other/Misc Tax Form'){
            for (School_Document_Assignment__c sda :    [Select Id, School_PFS_Assignment__c FROM School_Document_Assignment__c
                                                            WHERE School_PFS_Assignment__c in :schoolPFSAssignmentIds
                                                            AND Document_Year__c = :docYear
                                                            AND (Document_Type__c = :docType OR Required_Document__r.Document_Type__c = :docType)
                                                            ]){
                schoolPFSAssignmentIdsForThisDocType.add(sda.School_PFS_Assignment__c);
            }
        }

        if (schoolPFSAssignmentIds != null) {
            for (Id spaId : schoolPFSAssignmentIds) {
                // [DP] 12.04.2014 create a new SDA if this is an other/misc or if the SPA already has an SDA of this type/year
                if (docType == 'Other/Misc Tax Form' || schoolPFSAssignmentIdsForThisDocType.contains(spaId)){
                    newSDAList.add(
                        new School_Document_Assignment__c(
                            Document_Type__c = docType,
                            Document_Year__c = docYear,
                            Label_for_School_Specific_Document__c = schoolSpecificLabel,
                            School_PFS_Assignment__c = spaId,
                            Document__c = familyDocumentId,
                            Document_Name__c = docName
                        )
                    );
                }
            }
        }

        if (!newSDAList.isEmpty()) {
            system.debug('>>>>>>>>>>>>>>>>>>>>>>>>newSDAList='+newSDAList );
            insert newSDAList;
        }
    }

    public String getSessionId() {
        return UserInfo.getSessionId();
    }

    public String getHouseholdId() {
        return pfsRecordParam.Parent_A__r.Household__c;
    }

    public String getHouseholdName() {
        return pfsRecordParam.Parent_A__r.Household__r.Name;
    }

    public String getCurrentRequestUrl() {
        // return the current URL
        return System.URL.getCurrentRequestUrl().toExternalForm();
    }

    // returns the current timestamp value - used as a dummy param in the iframe src to prevent caching
    public Long getTimeNow() {
        return System.now().getTime();
    }

    /* Getter methods for SpringCM Params */
    public String getScmBaseUrl() {
        return SPRINGCM_BASE_URL;
    }

    public String getScmParamAID() {
        return SPRINGCM_PARAM_AID;
    }

    public String getScmParamFormUID() {
        return SPRINGCM_PARAM_FORMUID;
    }

    public String getScmParamSuppressNavigation() {
        return SPRINGCM_PARAM_SUPPRESSNAVIGATION;
    }

    public String getScmParamRUBase() {
        return SPRINGCM_PARAM_RU_BASE;
    }

    public String getRetUrl(Boolean success) {
        String dest;
        if (String.isNotBlank(returnUrlParam)) {
            dest = returnUrlParam;
        }
        else {
            dest = currentRequestUrl;
        }

        String successValue = (success == true) ? '1' : '0';

        // set the base URL to the site URL if in a Site page, otherwise use the standard Salesforce base URL
        String baseUrl = Site.getCurrentSiteUrl();
        if (baseUrl != null) {
            // remove the trailing '/'
            baseUrl = baseUrl.removeEnd('/');
        }
        else {
            baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        }

        return baseUrl + Page.SpringCMReturnHandler.getUrl()
                    + '?dest=' + EncodingUtil.urlEncode(dest,'UTF-8')
                    + '&docid=' + docId
                    + '&success=' + successValue;
    }

    //NAIS-1898 [DP] now going to autoclose popup via the return page, it will refresh the "parent" page
    public String getScmParamRURetUrl() {
        return getRetUrl(true); // [DP] 10.27.14 resetting to SpringCM Return Handler so we can set FamDoc as "Upload Pending"
        //return URL.getSalesforceBaseUrl().toExternalForm() + Page.SystemAutoClose.getURL();
    }

    //NAIS-1898 [DP] now going to autoclose popup, it will refresh the "parent" page
    public String getScmParamCancelUrl() {
        return getRetUrl(false); // [DP] 10.27.14 resetting to SpringCM Return Handler so we can set FamDoc as "Upload Pending" or what have you
        //return URL.getSalesforceBaseUrl().toExternalForm() + Page.SystemAutoClose.getURL();
    }

    //NAIS-1898 [DP] try the authentication before continuing, return "true" for success
    public static final boolean UAT = Test.isRunningTest() ? false : Databank_Settings__c.getInstance('Databank').Use_UAT__c;
    public static final SpringCMUAT.SpringCMServiceSoap uatService = new SpringCMUAT.SpringCMServiceSoap();
    public static final SpringCMProd.SpringCMServiceSoap prodService = new SpringCMProd.SpringCMServiceSoap();
    private String token {get;set;}

    public Boolean tryAuthentication()
    {
        Boolean authenticateSuccess = true;
        return authenticateSuccess;
    }

    public static void nullifyFields(Family_Document__c fd){
        //fd.fieldsToNull.add('filename');
        for (String s : famDocFieldNamesToNull){
            fd.put(s, null);
        }
    }

    public static Set<String> famDocFieldNamesToNull = new Set<String>{
        'Birthdate__c',
        'City__c',
        //'Deleted__c',
        'Document_Control_Number__c',
        'Duplicate__c',
        'Duplicate_Family_Document__c',
        'Email__c',
        'Filename__c',
        'First_Name__c',
        'Household__c',
        'Illegible_Doc_Alert_Sent_to_Parent__c',
        'Import_Data_ID__c',
        'Import_Email_Student__c',
        'Import_ID__c',
        'Import_Process__c',
        'Import_SpringCM_Doc_Type__c',
        //'Import_SpringCM_FilePath__c',
        'Issue_Categories__c',
        'Description__c',
        'Last_Name__c',
        'Notes__c',
        //'Opt_Out_of_Verification__c',
        'Other_Document_Label__c',
        'Parent_A_Email__c',
        'Parent_A_First_Name__c',
        'PFS_Number__c',
        'Phone_Number__c',
        'Reason_for_Request__c',
        'Request_to_Waive_Requirement__c',
        'Sch_C_Depreciation_Sect179__c',
        'Sch_C_Exp_Bus_Uses_Home__c',
        'Sch_C_Gross_Income__c',
        'Sch_E_Depreciation_Total__c',
        'Sch_E_Partnerships_S_Corp_Inc__c',
        'Sch_E_Rents_Income_Total__c',
        'Sch_E_Royalties_Income_Total__c',
        'Sch_F_Depreciation_Sec179_Exp__c',
        'Sch_F_Gross_Income__c',
        'School_Code__c',
        'State_Province__c',
        'System_Processing_Error__c',
        'W2_Deferred_Untaxed_1_A__c',
        'W2_Deferred_Untaxed_2_B__c',
        'W2_Deferred_Untaxed_3_C__c',
        'W2_Deferred_Untaxed_4_D__c',
        'W2_Deferred_Untaxed_5_E__c',
        'W2_Deferred_Untaxed_6_F__c',
        'W2_Deferred_Untaxed_7_G__c',
        'W2_Deferred_Untaxed_8_H__c',
        'W2_Employee_ID__c',
        'W2_Employer_ID__c',
        'W2_Medicare__c',
        'W2_SS_Tax__c',
        'W2_Wages__c',
        'ZIP_Postal_Code__c',
        'File_Hash__c',//SFP-1108
        'Page_Count__c'//SFP-1108
    };

}