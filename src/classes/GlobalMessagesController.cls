public with sharing class GlobalMessagesController
{

    public GlobalMessagesController() {}

    public String portalName {get; set;}
    public String addPortalName {get; set;}
    public Id academicYearId {get; set;}

    public List<Global_Message__c> getGlobalMessages()
    {
        String portalFilter = buildPortalFilter(portalName, addPortalName);
        String academicYearName = '';
        if (String.isNotBlank(academicYearId))
        {
            academicYearName = AcademicYearsSelector.Instance.selectById(new Set<Id>{academicYearId})[0].Name;
        }
        String currentPageOrScreenName = getCurrentPageOrScreenName();
        List<String> pageAndScreenList = getListOfCurrentPageAndScreen(currentPageOrScreenName);

        List<Global_Message__c> globalMessages = GlobalMessagesSelector.newInstance().selectRelevantByPortalPageAcademicYear(
            portalFilter, pageAndScreenList, academicYearName);

        return globalMessages;
    }

    private String buildPortalFilter(String portal, String additionalPortalFilter) {
        String portalConditionString = '';

        if (String.isNotBlank(portal)) {
            portalConditionString += portal + ';';
        }

        if (String.isNotBlank(additionalPortalFilter)) {
            portalConditionString += additionalPortalFilter;
        }

        return portalConditionString;
    }

    private static String getCurrentPageOrScreenName()
    {
        String currentPageURL = ApexPages.currentPage().getUrl();
        Map<String, String> currentPageParameters = ApexPages.currentPage().getParameters();
        String familyApplicationMainPage = 'FamilyApplicationMain';
        String currentPageName = '';

        if (currentPageURL.containsIgnoreCase(familyApplicationMainPage))
        {
            String currentScreen = currentPageParameters.get('screen');

            if (currentScreen == null)
            {
                currentPageName = familyApplicationMainPage;
            }
            else
            {
                currentPageName = currentScreen;
            }
        }
        else
        {
            if (currentPageParameters != null && !currentPageParameters.isEmpty())
            {
                currentPageName = currentPageURL.substringBetween('/apex/', '?');
            }
            else
            {
                currentPageName = currentPageURL.substringAfter('/apex/');
            }
        }

        return currentPageName;
    }

    private static List<String> getListOfCurrentPageAndScreen(String page)
    {
        List<String> pageAndScreenList = new List<String>(new List<String> {page});
        Set<String> familyDashboardScreenNames = new Set<String>();
        String familyApplicationMainPage = 'FamilyApplicationMain';

        for (FamilyAppSettings__c fas : FamilyAppSettings__c.getAll().values())
        {
            familyDashboardScreenNames.add(fas.Name.toLowerCase());
        }

        if (familyDashboardScreenNames.contains(page.toLowerCase()))
        {
            pageAndScreenList.add(familyApplicationMainPage);
        }

        return pageAndScreenList;
    }
}