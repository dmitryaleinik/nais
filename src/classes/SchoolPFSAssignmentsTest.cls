/**
 * SchoolPFSAssignmentsTest.cls
 *
 * @description: Test class for SchoolPFSAssignments using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest
private class SchoolPFSAssignmentsTest {    

    /* negative test cases */

    // Test newInstance with null list
    @isTest 
    private static void newInstance_withNullParam_exceptionThrown() {

        // Arrange
        List<School_PFS_Assignment__c> spaList;
        Boolean exceptionThrown = false;

        // Act
        try {

            SchoolPFSAssignments spa = SchoolPFSAssignments.newInstance(spaList);
        } catch (ArgumentNullException e) {

            exceptionThrown = true;
        }

        // Assert
        System.assert(exceptionThrown);
    }


    /* positive test cases */

    // Test newInstance with valid list
    @isTest 
    private static void newInstance_withValidParam_validInstance() {

        // Arrange
        List<School_PFS_Assignment__c> spaList = new List<School_PFS_Assignment__c>();

        // Act
        SchoolPFSAssignments spa = SchoolPFSAssignments.newInstance(spaList);

        // Assert
        System.assertNotEquals(null, spa);
    }

    // Test onAfterUpdate with valid list
    @isTest 
    private static void onAfterUpdate_withValidParam_validInstance() {

        // Arrange
        List<School_PFS_Assignment__c> spaList = new List<School_PFS_Assignment__c>();

        // Act
        Map<Id, SObject> existingObjMap = new Map<Id, SObject>();
        SchoolPFSAssignments spa = SchoolPFSAssignments.newInstance(spaList);
        spa.onAfterUpdate(existingObjMap);

        // Assert
        System.assertNotEquals(null, spa);
    }

    @isTest
    private static void onBeforeUpdate_applicantHasNotChanged_sharingProcessedTrue_expectSharingProcessedTrue() {
        School_PFS_Assignment__c recordBeforeUpdate = SchoolPfsAssignmentTestData.Instance.DefaultSchoolPfsAssignment;
        recordBeforeUpdate.Sharing_Processed__c = true;

        School_PFS_Assignment__c recordAfterUpdate = recordBeforeUpdate.clone(true, true);

        Map<Id, School_PFS_Assignment__c> recordsBeforeUpdateById = new Map<Id, School_PFS_Assignment__c>();
        recordsBeforeUpdateById.put(recordBeforeUpdate.Id, recordBeforeUpdate);

        List<School_PFS_Assignment__c> recordsAfterUpdate = new List<School_PFS_Assignment__c> { recordAfterUpdate };

        // The domain class should be created from the new records or the records as they would be after someone tries to update the applicant.
        SchoolPFSAssignments domain = SchoolPFSAssignments.newInstance(recordsAfterUpdate);
        domain.onBeforeUpdate(recordsBeforeUpdateById);

        System.assert(recordAfterUpdate.Sharing_Processed__c, 'Expected Sharing Processed to still be true.');
    }
    
    @isTest
    private static void onBeforeUpdate_applicantChangedToNull_sharingProcessedTrue_expectSharingProcessedTrue() {
        School_PFS_Assignment__c recordBeforeUpdate = SchoolPfsAssignmentTestData.Instance.DefaultSchoolPfsAssignment;
        recordBeforeUpdate.Sharing_Processed__c = true;

        // Set Applicant__c to null so we can make sure we won't recalculate sharing for SPAs without applicants.
        School_PFS_Assignment__c recordAfterUpdate = recordBeforeUpdate.clone(true, true);
        recordAfterUpdate.Applicant__c = null;

        Map<Id, School_PFS_Assignment__c> recordsBeforeUpdateById = new Map<Id, School_PFS_Assignment__c>();
        recordsBeforeUpdateById.put(recordBeforeUpdate.Id, recordBeforeUpdate);

        List<School_PFS_Assignment__c> recordsAfterUpdate = new List<School_PFS_Assignment__c> { recordAfterUpdate };

        // The domain class should be created from the new records or the records as they would be after someone tries to update the applicant.
        SchoolPFSAssignments domain = SchoolPFSAssignments.newInstance(recordsAfterUpdate);
        domain.onBeforeUpdate(recordsBeforeUpdateById);

        System.assert(recordAfterUpdate.Sharing_Processed__c, 'Expected Sharing Processed to still be true.');
    }
    
    //SFP-1043
    @isTest
    private static void onBeforeDelete_studentFoldersWithTwoPFS_clearFolderRollupsForDeletedPfsAssignments() {
        
        //1. Create the current academic year.
        Academic_Year__c academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        
        //2. Create a school.
        Account school = AccountTestData.Instance.forName('School1')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccount();
        
        //3. Create 2 parents and 2 students.   
        Contact parentA = ContactTestData.Instance.asParent().forFirstName('ParentA').forLastName('ParentA').create();
        Contact parentB = ContactTestData.Instance.asParent().forFirstName('ParentB').forLastName('ParentB').create();
        Contact student1 = ContactTestData.Instance.asStudent().forFirstName('Student1').forLastName('Student1').create();
        Contact student2 = ContactTestData.Instance.asStudent().forFirstName('Student2').forLastName('Student2').create();
        insert new List<Contact> { parentA, parentB, student1, student2 };
       
       //4. Create 2 PFS, one for each parent. 
        PFS__c pfsA = PfsTestData.Instance
            .forParentA(parentA.Id)
            .forParentB(parentB.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .asPaid()
            .create();
        pfsA.Parent_A_First_Name__c = 'ParentA';
        pfsA.Parent_A_Last_Name__c = 'ParentA';
        pfsA.Parent_B_First_Name__c = 'ParentB';
        pfsA.Parent_B_Last_Name__c = 'ParentB';
        pfsA.Original_Submission_Date__c = System.today().addDays(-2);
        PFS__c pfsB = PfsTestData.Instance
            .forParentA(parentB.Id)
            .forParentB(parentA.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .asPaid()
            .create();
        pfsB.Parent_A_First_Name__c = 'ParentB';
        pfsB.Parent_A_Last_Name__c = 'ParentB';
        pfsB.Parent_B_First_Name__c = 'ParentA';
        pfsB.Parent_B_Last_Name__c = 'ParentA';
        pfsB.Original_Submission_Date__c = System.today().addDays(-2);
        insert new List<PFS__c>{pfsA, pfsB};
            
        //5. Create 2 applicats for each student, one for each PFS.
        Applicant__c applicant1A = ApplicantTestData.Instance
            .forContactId(student1.Id)
            .forPfsId(pfsA.Id)
            .create();
        Applicant__c applicant1B = ApplicantTestData.Instance
            .forContactId(student1.Id)
            .forPfsId(pfsB.Id)
            .create();
        Applicant__c applicant2A = ApplicantTestData.Instance
            .forContactId(student2.Id)
            .forPfsId(pfsA.Id)
            .create();
        Applicant__c applicant2B = ApplicantTestData.Instance
            .forContactId(student2.Id)
            .forPfsId(pfsB.Id)
            .create();
        insert new List<Applicant__c>{applicant1A, applicant1B, applicant2A, applicant2B};
        
        //6. Create one folder for each student.    
        Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student1.Id)
            .create();
        Student_Folder__c studentFolder2 = StudentFolderTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student2.Id) 
            .create();
        insert new List<Student_Folder__c>{studentFolder1, studentFolder2};
        
        //7. Create 2 spas for each student folder, one for each PFS.
        School_PFS_Assignment__c schoolPfsAssignment1A = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicant1A.Id)
            .forStudentFolderId(studentFolder1.Id)
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .createSchoolPfsAssignmentWithoutReset();
        setPFSRollupFields(schoolPfsAssignment1A, 20000.0, System.today(), 10000.0, 5000.0, 'Preschool', 2.0);
        School_PFS_Assignment__c schoolPfsAssignment1B = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicant1B.Id)
            .forStudentFolderId(studentFolder1.Id)
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .createSchoolPfsAssignmentWithoutReset();
        setPFSRollupFields(schoolPfsAssignment1B, 21000.0, System.today(), 11000.0, 5100.0, 'Preschool', 2.0);
        School_PFS_Assignment__c schoolPfsAssignment2A = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicant2A.Id)
            .forStudentFolderId(studentFolder2.Id)
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .createSchoolPfsAssignmentWithoutReset();
        setPFSRollupFields(schoolPfsAssignment2A, 22000.0, System.today(), 12000.0, 5200.0, 'Pre-Kindergarten', 2.0);
        School_PFS_Assignment__c schoolPfsAssignment2B = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicant2B.Id)
            .forStudentFolderId(studentFolder2.Id)
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .createSchoolPfsAssignmentWithoutReset();
        setPFSRollupFields(schoolPfsAssignment2B, 23000, System.today(), 13000, 5300, 'Pre-Kindergarten', 2.0);
        insert new List<School_PFS_Assignment__c>{ schoolPfsAssignment1A, schoolPfsAssignment1B, schoolPfsAssignment2A, schoolPfsAssignment2B};
        
        Student_Folder__c folder1, folder2;
        
        Test.startTest();
            //8. Verify if Student Folder 1 has its PFS rollup fields properly set.
            folder1 = [
                SELECT Id, PFS1_Total_Salary_and_Wages__c, PFS2_Total_Salary_and_Wages__c, 
                PFS1_Parent_A__c, PFS1_Parent_B__c,  PFS1_Income_Supplement_Revised__c, PFS1_Effective_Income_Revised__c, 
                PFS2_Parent_A__c, PFS2_Parent_B__c,  PFS2_Income_Supplement_Revised__c, PFS2_Effective_Income_Revised__c 
                FROM Student_Folder__c 
                WHERE Id =: studentFolder1.Id LIMIT 1];
            
            System.AssertEquals(20000, folder1.PFS1_Total_Salary_and_Wages__c);
            System.AssertEquals(21000, folder1.PFS2_Total_Salary_and_Wages__c);
            System.AssertEquals(parentA.FirstName + ' ' + parentA.LastName, folder1.PFS1_Parent_A__c);
            System.AssertEquals(parentB.FirstName + ' ' + parentB.LastName, folder1.PFS2_Parent_A__c);
            
            //9. Verify if Student Folder 2 has its PFS rollup fields properly set.
            folder2 = [
                SELECT Id, PFS1_Total_Salary_and_Wages__c, PFS2_Total_Salary_and_Wages__c, 
                PFS1_Parent_A__c, PFS1_Parent_B__c,  PFS1_Income_Supplement_Revised__c, PFS1_Effective_Income_Revised__c, 
                PFS2_Parent_A__c, PFS2_Parent_B__c,  PFS2_Income_Supplement_Revised__c, PFS2_Effective_Income_Revised__c 
                FROM Student_Folder__c 
                WHERE Id =: studentFolder2.Id LIMIT 1];
            
            System.AssertEquals(22000, folder2.PFS1_Total_Salary_and_Wages__c);
            System.AssertEquals(23000, folder2.PFS2_Total_Salary_and_Wages__c);
            System.AssertEquals(parentA.FirstName + ' ' + parentA.LastName, folder2.PFS1_Parent_A__c);
            System.AssertEquals(parentB.FirstName + ' ' + parentB.LastName, folder2.PFS2_Parent_A__c);
            
            //10. Delete SPA for Folder 1.
            delete schoolPfsAssignment1A;
        Test.stopTest();
        
        //11. Verify if Student Folder 1 has its PFS rollup fields properly recalculated.
        folder1 = [
            SELECT Id, PFS1_Total_Salary_and_Wages__c, PFS2_Total_Salary_and_Wages__c, 
            PFS1_Parent_A__c, PFS1_Parent_B__c,  PFS1_Income_Supplement_Revised__c, PFS1_Effective_Income_Revised__c, 
            PFS2_Parent_A__c, PFS2_Parent_B__c,  PFS2_Income_Supplement_Revised__c, PFS2_Effective_Income_Revised__c 
            FROM Student_Folder__c 
            WHERE Id =: studentFolder1.Id LIMIT 1];
        
        System.AssertEquals(21000, folder1.PFS1_Total_Salary_and_Wages__c);
        System.AssertEquals(null, folder1.PFS2_Total_Salary_and_Wages__c);
        System.AssertEquals(parentB.FirstName + ' ' + parentB.LastName, folder1.PFS1_Parent_A__c);
        System.AssertEquals(null, folder1.PFS2_Parent_A__c);
        
        //12. Verify if Student Folder 2 has its PFS rollup fields properly set.
        folder2 = [
            SELECT Id, PFS1_Total_Salary_and_Wages__c, PFS2_Total_Salary_and_Wages__c, 
            PFS1_Parent_A__c, PFS1_Parent_B__c,  PFS1_Income_Supplement_Revised__c, PFS1_Effective_Income_Revised__c, 
            PFS2_Parent_A__c, PFS2_Parent_B__c,  PFS2_Income_Supplement_Revised__c, PFS2_Effective_Income_Revised__c 
            FROM Student_Folder__c 
            WHERE Id =: studentFolder2.Id LIMIT 1];
        
        System.AssertEquals(22000, folder2.PFS1_Total_Salary_and_Wages__c);
        System.AssertEquals(23000, folder2.PFS2_Total_Salary_and_Wages__c);
        System.AssertEquals(parentA.FirstName + ' ' + parentA.LastName, folder2.PFS1_Parent_A__c);
        System.AssertEquals(parentB.FirstName + ' ' + parentB.LastName, folder2.PFS2_Parent_A__c);
    }

    @isTest
    private static void testAcademicYearAutoPopulating() {
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        Pfs__c pfs = PfsTestData.Instance
            .forAcademicYearPicklist(currentAcademicYear.Name).DefaultPfs;
        
        Test.startTest();
            School_PFS_Assignment__c spfsa = SchoolPfsAssignmentTestData.Instance
                .forAcademicYearPicklist(null).DefaultSchoolPfsAssignment;
        Test.stopTest();

        List<School_PFS_Assignment__c> spfsas = SchoolPfsAssignmentsSelector.newInstance().selectByIdWithCustomFieldList(
            new Set<Id>{spfsa.Id}, new List<String>{'PFS_Academic_Year__c', 'Academic_Year_Picklist__c'});

        System.assertEquals(1, spfsas.size());
        System.assertEquals(currentAcademicYear.Name, spfsas[0].PFS_Academic_Year__c);
        System.assertEquals(currentAcademicYear.Name, spfsas[0].Academic_Year_Picklist__c);
    }
    
    private static void setPFSRollupFields(
        School_PFS_Assignment__c spa, 
        Decimal salary, 
        Date lastModified, 
        Decimal incomeEffective, 
        Decimal incomeSupplement, 
        String grade, 
        Decimal childrens) {
        
        spa.Salary_Wages_Parent_A__c = salary;
        spa.Application_Last_Modified_by_Family__c = lastModified;
        spa.Effective_Income_Currency__c = incomeEffective;
        spa.Income_Supplement__c = incomeSupplement;
        spa.Grade_Applying__c = grade;
        spa.Num_Children_in_Tuition_Schools__c = childrens;
        spa.Update_Student_Folder__c = true;
    }
}