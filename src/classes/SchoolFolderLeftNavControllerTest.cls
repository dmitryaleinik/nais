@isTest
private class SchoolFolderLeftNavControllerTest {

    @isTest
    private static void testController(){
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);

        //TestData tData = new TestData();
        List<Academic_Year__c> academicYears = TestUtils.createAcademicYears();
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        Id academicYearPastId = GlobalVariables.getPreviousAcademicYear().Id;

        Account family = TestUtils.createAccount('Individual Account', RecordTypes.individualAccountTypeId, 5, false);
        Account school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, 5, false);
        
        insert new List<Account>{family,school1}; 
        
        Contact student1 = TestUtils.createContact('Student 1', family.Id, RecordTypes.studentContactTypeId, false);
        insert student1;
        
        Student_Folder__c studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
        insert studentFolder1;        

        Contact parentA = TestUtils.createContact('Parent A', family.Id, RecordTypes.parentContactTypeId, false);
        Contact parentB = TestUtils.createContact('Parent B', family.Id, RecordTypes.parentContactTypeId, false);

        insert new list<Contact>{parentA,parentB};

        SSS_Constants__c sssConst1 = TestUtils.createSSSConstants(academicYearId, false);
        SSS_Constants__c sssConst2 = TestUtils.createSSSConstants(academicYearPastId, false);
        Database.insert(new List<SSS_Constants__c>{sssConst1, sssConst2});

       List<Federal_Tax_Table__c> fedTaxTables = new List<Federal_Tax_Table__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                fedTaxTables.add(TestUtils.createFederalTaxTable(academicYears[i].Id, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_SINGLE, false));
            }
            Database.insert(fedTaxTables);
               
        List<State_Tax_Table__c> stateTaxTables = new List<State_Tax_Table__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                stateTaxTables.add(TestUtils.createStateTaxTable(academicYears[i].Id, false));
            }
            Database.insert(stateTaxTables);

        List<Employment_Allowance__c>emplAllowances = new List<Employment_Allowance__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                emplAllowances.add(TestUtils.createEmploymentAllowance(academicYears[i].Id, false));
            }
            Database.insert(emplAllowances);
            
        List<Net_Worth_of_Business_Farm__c> busFarm = new List<Net_Worth_of_Business_Farm__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                busFarm.add(TestUtils.createBusinessFarm(academicYears[i].Id, false));
            }
            Database.insert(busFarm);
            
        List<Retirement_Allowance__c> retAllowances = new List<Retirement_Allowance__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                retAllowances.add(TestUtils.createRetirementAllowance(academicYears[i].Id, false));
            }
            Database.insert(retAllowances);
            
            List<Asset_Progressivity__c> assetProgs = new List<Asset_Progressivity__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                assetProgs.add(TestUtils.createAssetProgressivity(academicYears[i].Id, false));
            }
            Database.insert(assetProgs);
            
            List<Income_Protection_Allowance__c> incProAllowances = new List<Income_Protection_Allowance__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                incProAllowances.add(TestUtils.createIncomeProtectionAllowance(academicYears[i].Id, false));
            }
            Database.insert(incProAllowances);
            
            List<Expected_Contribution_Rate__c> expContRates = new List<Expected_Contribution_Rate__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                expContRates.add(TestUtils.createExpectedContributionRate(academicYears[i].Id, false));
            }
            Database.insert(expContRates);
            
            List<Housing_Index_Multiplier__c> houseIndexes = new List<Housing_Index_Multiplier__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                houseIndexes.add(TestUtils.createHousingIndexMultiplier(academicYears[i].Id, false));
            }
            Database.insert(houseIndexes);


        PFS__c pfs1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
        pfs1.Original_Submission_Date__c = (Datetime)System.today().addDays(-3);
        pfs1.Payment_Status__c = EfcPicklistValues.PAYMENT_STATUS_PAID_IN_FULL;
        pfs1.PFS_Status__c = 'Application Submitted';
        pfs1.Visible_to_Schools_Until__c = System.today().addDays(2);

        PFS__c pfs2 = TestUtils.createPFS('PFS B', academicYearId, parentB.Id, false);
        pfs2.Original_Submission_Date__c = (Datetime)System.today().addDays(-5);
        pfs2.Payment_Status__c = EfcPicklistValues.PAYMENT_STATUS_PAID_IN_FULL;
        pfs2.PFS_Status__c = 'Application Submitted';
        pfs2.Visible_to_Schools_Until__c = System.today().addDays(2);

        insert pfs1;
        insert pfs2;
        //insert new List<PFS__C>{pfs1,pfs2};
        
        Test.startTest();
        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfs1.Id, false);
        Applicant__c applicant1B = TestUtils.createApplicant(student1.Id, pfs2.Id, false);
        insert new List<Applicant__c>{applicant1A,applicant1B};

                
        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder1.Id, false);
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1B.Id, school1.Id, studentFolder1.Id, false);
        insert new List<School_PFS_Assignment__c>{spfsa1,spfsa2};
        
        SchoolFolderLeftNavController testController = new SchoolFolderLeftNavController();

        testController.studentFolderId = studentFolder1.Id;
        testController.loadRecords();
        
        System.assert(spfsa1.Id == testController.spfs1Record.Id || spfsa1.Id == testController.spfs2Record.Id);
        System.assert(spfsa2.Id == testController.spfs1Record.Id || spfsa2.Id == testController.spfs2Record.Id);

                
        System.assert(testController.getSpfs1Submitted());
        System.assert(testController.getSpfs2Submitted());
    
        testController.lockPFS1();

        for (School_PFS_Assignment__c updatedSPFS : [Select Id, Family_May_Submit_Updates__c from School_PFS_Assignment__c where Id = :testController.spfs1Record.Id ]){
            System.assertEquals('No', updatedSPFS.Family_May_Submit_Updates__c);
        }

        testController.unlockPFS1();

        for (School_PFS_Assignment__c updatedSPFS : [Select Id, Family_May_Submit_Updates__c from School_PFS_Assignment__c where Id = :testController.spfs1Record.Id ]){
            System.assertEquals('Yes', updatedSPFS.Family_May_Submit_Updates__c);
        }

        testController.lockPFS2();
        
        for (School_PFS_Assignment__c updatedSPFS : [Select Id, Family_May_Submit_Updates__c from School_PFS_Assignment__c where Id = :testController.spfs2Record.Id]){
            System.assertEquals('No', updatedSPFS.Family_May_Submit_Updates__c);
        }

        testController.unlockPFS2();
        
        for (School_PFS_Assignment__c updatedSPFS : [Select Id, Family_May_Submit_Updates__c from School_PFS_Assignment__c where Id = :testController.spfs2Record.Id]){
            System.assertEquals('Yes', updatedSPFS.Family_May_Submit_Updates__c);
        }
        
        System.assert(testController.getFolderURL() != null);
        System.assertEquals(studentFolder1.Id, testController.getStudentFolder().Id);

        testController.getSpfs1Record();
        testController.getSpfs2Record();
        testController.getAtLeastOneSubmitted();
        Test.stopTest();
    }
}