/**
 *  @description Class for handling retrieving the Federal_Tax_Table__c records
 *               from the platform cache by academic year Id.
 *               The cache key will be "federalTax“ + academic year id
 */
public with sharing class FederalTaxTableCache
{

    @testVisible private static final String CACHE_KEY_PREFIX = 'local.sssSystem.federalTax';
    @testVisible private static String queryTaxTablesException = 'Federal Tax Tables for the Academic Year Id = {0} '
        + 'were not found. Please contact your administrator for assistance.';

    /**
     *  @description Checks the cache first and if null, queries the records.
     *               After querying, populate the cache.
     *  @param academicYearId Id of an academic year to use it as criteria in query
     */
    public static List<Federal_Tax_Table__c> getTaxTables(Id academicYearId)
    {
        String cacheKey = CACHE_KEY_PREFIX + String.valueOf(academicYearId);

        List<Federal_Tax_Table__c> federalTaxTables = 
            (List<Federal_Tax_Table__c>) Cache.Org.get(FederalTaxTableInfoCache.class, cacheKey);
            
        if (federalTaxTables == null || federalTaxTables.isEmpty())
        {
            throw new TaxTablesException(queryTaxTablesException.replace('{0}', academicYearId));
        }

        return federalTaxTables;
    }

    private static List<Federal_Tax_Table__c> queryTaxTables(Id academicYearId)
    {
        return [
            SELECT Taxable_Income_Low__c, Taxable_Income_High__c, US_Income_Tax_Base__c, US_Income_Tax_Rate_Threshold__c,
                    US_Income_Tax_Rate__c, Filing_Type__c
            FROM Federal_Tax_Table__c 
            WHERE Academic_Year__c = :academicYearId
              ORDER BY Taxable_Income_Low__c ASC];
    }

    private class FederalTaxTableInfoCache implements Cache.CacheBuilder 
    {
        public Object doLoad(String cacheKey) 
        {
            //doLoad can never return null. The returned value can be of any type which you then cast to the appropriate type.
            //This means that we can not return null or thrown an exception from here. Since, it will cause an unexpected exception
            //that can not be cached in code.
            String academicYearId = Id.valueOf(cacheKey.removeStart('federalTax'));
            List<Federal_Tax_Table__c> federalTaxTables = (List<Federal_Tax_Table__c>)queryTaxTables(academicYearId);

            return federalTaxTables;
        }
    }

    private class TaxTablesException extends Exception {}
}