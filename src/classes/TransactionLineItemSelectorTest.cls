@isTest
private class TransactionLineItemSelectorTest {
    private static Transaction_Line_Item__c setupTransactionLineItem() {
        Opportunity opportunity = new Opportunity(
                Name = 'User - Application Fee',
                StageName = 'Open',
                CloseDate = Date.today(),
                RecordTypeId = RecordTypes.pfsApplicationFeeOpportunityTypeId);
        insert opportunity;

        Transaction_Line_Item__c transactionLineItem = new Transaction_Line_Item__c(
                Opportunity__c = opportunity.Id,
                RecordTypeId = RecordTypes.paymentTransactionTypeId);
        insert transactionLineItem;

        return transactionLineItem;
    }

    @isTest
    private static void selectById_nullParam_expectArgumentNullException() {
        try {
            Test.startTest();
            TransactionLineItemSelector.Instance.selectById(null);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, TransactionLineItemSelector.TRANSACTION_LINE_ITEM_IDS_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void selectById_noTransactionLineItemExists_expectEmptyList() {
        Transaction_Line_Item__c transactionLineItem = setupTransactionLineItem();

        Id transactionLineItemId = transactionLineItem.Id;
        delete transactionLineItem;

        Test.startTest();
        List<Transaction_Line_Item__c> transactionLineItems = TransactionLineItemSelector.Instance
                .selectById(new Set<Id> { transactionLineItemId });
        Test.stopTest();

        System.assert(transactionLineItems.isEmpty(), 'Expected the returned list to be empty.');
    }

    @isTest
    private static void selectById_transactionLineItemExists_expectRecordReturned() {
        Transaction_Line_Item__c transactionLineItem = setupTransactionLineItem();

        Test.startTest();
        List<Transaction_Line_Item__c> transactionLineItems = TransactionLineItemSelector.Instance
                .selectById(new Set<Id> { transactionLineItem.Id });
        Test.stopTest();

        System.assert(!transactionLineItems.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(1, transactionLineItems.size(), 'Expected there to be one transaction line item returned.');
        System.assertEquals(transactionLineItem.Id, transactionLineItems[0].Id,
                'Expected the Id of the returned transaction line item to match the requested Id.');
    }

    @isTest
    private static void selectByOpportunityAndRecordType_nullOpportunityParam_expectArgumentNulLException() {
        try {
            Test.startTest();
            TransactionLineItemSelector.Instance.selectByOpportunityAndRecordType(null, null);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, TransactionLineItemSelector.OPPORTUNITY_IDS_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void selectByOpportunityAndRecordType_nullRecordTypesParam_expectArgumentNulLException() {
        try {
            Test.startTest();
            TransactionLineItemSelector.Instance.selectByOpportunityAndRecordType(new Set<Id>(), null);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, TransactionLineItemSelector.RECORD_TYPE_IDS_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void selectByOpportunityAndRecordType_noRecordExists_expectEmptyList() {
        Transaction_Line_Item__c transactionLineItem = setupTransactionLineItem();

        Id opportunityId = transactionLineItem.Opportunity__c;
        Id recordTypeId = transactionLineItem.RecordTypeId;

        delete transactionLineItem;

        Test.startTest();
        List<Transaction_Line_Item__c> transactionLineItems = TransactionLineItemSelector.Instance
                .selectByOpportunityAndRecordType(new Set<Id> { opportunityId }, new Set<Id> { recordTypeId });
        Test.stopTest();

        System.assert(transactionLineItems.isEmpty(), 'Expected the returned list to be empty.');
    }

    @isTest
    private static void selectByOpportunityAndRecordType_recordExists_expectRecordReturned() {
        Transaction_Line_Item__c transactionLineItem = setupTransactionLineItem();

        Id opportunityId = transactionLineItem.Opportunity__c;
        Id recordTypeId = transactionLineItem.RecordTypeId;

        Test.startTest();
        List<Transaction_Line_Item__c> transactionLineItems = TransactionLineItemSelector.Instance
                .selectByOpportunityAndRecordType(new Set<Id> { opportunityId }, new Set<Id> { recordTypeId });
        Test.stopTest();

        System.assert(!transactionLineItems.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(1, transactionLineItems.size(), 'Expected there to be one transaction line item returned.');
        System.assertEquals(transactionLineItem.Id, transactionLineItems[0].Id,
                'Expected the returned transaction line item Id to match the requested Id.');
    }

    @isTest
    private static void selectRefundableTransactionLineItems_nullOpportunityParam_expectArgumentNullException() {
        try {
            Test.startTest();
            TransactionLineItemSelector.Instance.selectRefundableTransactionLineItems(null);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, TransactionLineItemSelector.OPPORTUNITY_IDS_PARAM);
        } finally {
            Test.stopTest();
        }
    }
}