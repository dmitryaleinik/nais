@isTest
private class FamilyNavigationServiceTest
{
    
    private static final String DUMMY_NEXT_PAGE = 'Dummy Next Page';
    private static final String DEFAULT_LANGUAGE_NAME = 'en_US';
    private static final String EXPECT_NEXTSCREEN_NOT_NULL = 'Expected nextScreen to not be null.';

    @isTest
    private static void getNextSection_nullNextPageParam_expectArgumentNullException()
    {
        try {
            Test.startTest();
                FamilyNavigationService.Response response = FamilyNavigationService.Instance.getNextSection(null);

                TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, FamilyNavigationService.NEXT_PAGE_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void getNextSection_nullSettingParam_expectArgumentNullException()
    {
        try {
            Test.startTest();
                FamilyNavigationService.Response response = FamilyNavigationService.Instance.getNextSection(null, null);

                TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, FamilyNavigationService.SETTING_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void getNextSection_nullApplicationUtilsParam_expectArgumentNullException()
    {
        try {
            Test.startTest();
                FamilyNavigationService.Response response = FamilyNavigationService.Instance
                        .getNextSection(new FamilyAppSettings__c(), null);

                TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, FamilyNavigationService.APP_UTILS_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void getNextScreen_screenProvided_expectNextPagePopulated()
    {
        Test.startTest();
            FamilyNavigationService.Response response = FamilyNavigationService.Instance.getNextSection(DUMMY_NEXT_PAGE);
        Test.stopTest();

        String nextPage = response.NextPage;
        Business_Farm__c businessFarm = response.BusinessFarm;

        System.assertEquals(DUMMY_NEXT_PAGE, nextPage,
                'Expect the dummy next page to be passed back in the Next Page property.');
        System.assertEquals(null, businessFarm, 'Expected there to be no business farm.');
    }

    @isTest
    private static void getNextScreen_noNextPageOnFamilyAppSetting_expectNull()
    {
        Pfs__c pfs = FamilyNavigationServiceTestHelper.insertPfs(true);

        ApplicationUtils appUtils = new ApplicationUtils(DEFAULT_LANGUAGE_NAME, pfs);

        Test.startTest();
            FamilyNavigationService.Response response = FamilyNavigationService.Instance
                .getNextSection(new FamilyAppSettings__c(), appUtils);
        Test.stopTest();

        System.assertEquals(null, response.NextPage, 'Expected the next page to be null.');
        System.assertEquals(null, response.BusinessFarm, 'Expected the business farm to be null.');
    }

    @isTest
    private static void getNextScreen_HouseholdInformation()
    {
        PFS__c pfs = FamilyNavigationServiceTestHelper.insertPfs(true);
        FamilyNavigationServiceTestHelper.insertFamilyAppSettings();

        FamilyAppSettings__c setting = FamilyAppSettings__c.getInstance(FamilyNavigationServiceTestHelper.FAP_HOUSEHOLD_INFORMATION);
        System.assertNotEquals(null, setting, 'Expected a setting to be returned.');

        ApplicationUtils appUtils = new ApplicationUtils(DEFAULT_LANGUAGE_NAME, pfs);

        Test.startTest();
            FamilyNavigationService.Response response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            // We are expecting ParentsGuardians here, because we have a safeguard that keeps us from sending the
            // user to the submit page if they're simply attempting to thumb through all sections when they
            // are all completed.
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_PARENTS_GUARDIANS, response.NextPage);
            
            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_APPLICANT_INFORMATION, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_DEPENDENT_INFORMATION, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_HOUSEHOLD_SUMMARY, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_SCHOOL_SELECTION, response.NextPage);
        Test.stopTest();
    }

    @isTest
    private static void getNextScreen_SchoolSelection()
    {
        PFS__c pfs = FamilyNavigationServiceTestHelper.insertPfs(true);
        FamilyNavigationServiceTestHelper.insertFamilyAppSettings();
        ApplicationUtils appUtils = new ApplicationUtils(DEFAULT_LANGUAGE_NAME, pfs);

        Test.startTest();
            FamilyAppSettings__c setting = FamilyAppSettings__c.getInstance(FamilyNavigationServiceTestHelper.FAP_SCHOOL_SELECTION);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            FamilyNavigationService.Response response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_SELECT_SCHOOLS, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_FAMILY_INCOME, response.NextPage);
        Test.stopTest();
    }

    @isTest
    private static void getNextScreen_FamilyIncome()
    {
        PFS__c pfs = FamilyNavigationServiceTestHelper.insertPfs(true);
        FamilyNavigationServiceTestHelper.insertFamilyAppSettings();
        ApplicationUtils appUtils = new ApplicationUtils(DEFAULT_LANGUAGE_NAME, pfs);

        Test.startTest();
            FamilyAppSettings__c setting = FamilyAppSettings__c.getInstance(FamilyNavigationServiceTestHelper.FAP_FAMILY_INCOME);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            FamilyNavigationService.Response response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_BASIC_TAX, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_TOTAL_TAXABLE, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_TOTAL_NON, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_STUDENT_INCOME, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_FAMILY_ASSETS, response.NextPage);
        Test.stopTest();
    }

    @isTest
    private static void getNextScreen_FamilyAssets()
    {
        PFS__c pfs = FamilyNavigationServiceTestHelper.insertPfs(true);
        FamilyNavigationServiceTestHelper.insertFamilyAppSettings();
        ApplicationUtils appUtils = new ApplicationUtils(DEFAULT_LANGUAGE_NAME, pfs);

        Test.startTest();
            FamilyAppSettings__c setting = FamilyAppSettings__c.getInstance(FamilyNavigationServiceTestHelper.FAP_FAMILY_ASSETS);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            FamilyNavigationService.Response response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_REAL_ESTATE, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_VEHICLES, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_OTHER_ASSETS, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_FAMILY_EXPENSES, response.NextPage);
        Test.stopTest();
    }

    @isTest
    private static void getNextScreen_FamilyExpenses()
    {
        PFS__c pfs = FamilyNavigationServiceTestHelper.insertPfs(true);
        FamilyNavigationServiceTestHelper.insertFamilyAppSettings();
        Business_Farm__c businessFarm = FamilyNavigationServiceTestHelper.insertBusinessFarm(pfs.Id);
        ApplicationUtils appUtils = new ApplicationUtils(DEFAULT_LANGUAGE_NAME, pfs);

        Test.startTest();
            FamilyAppSettings__c setting = FamilyAppSettings__c.getInstance(FamilyNavigationServiceTestHelper.FAP_FAMILY_EXPENSES);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            FamilyNavigationService.Response response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_EDUCATIONAL_EXPENSES, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_OTHER_EXPENSES, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_BUSINESS_FARM, response.NextPage);
            System.assertNotEquals(null, response.BusinessFarm, 'Expected BusinessFarm to not be null.');
            System.assertEquals(businessFarm.Id, response.BusinessFarm.Id,
                'Expected the BusinessFarm to match the provided one.');
        Test.stopTest();
    }

    @isTest
    private static void getNextScreen_BusinessFarm()
    {
        PFS__c pfs = FamilyNavigationServiceTestHelper.insertPfs(true);
        FamilyNavigationServiceTestHelper.insertFamilyAppSettings();
        Business_Farm__c businessFarm = FamilyNavigationServiceTestHelper.insertBusinessFarm(pfs.Id);
        ApplicationUtils appUtils = new ApplicationUtils(DEFAULT_LANGUAGE_NAME, pfs);
        
        Test.startTest();
            FamilyAppSettings__c setting = FamilyAppSettings__c.getInstance(FamilyNavigationServiceTestHelper.FAP_BUSINESS_FARM);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            FamilyNavigationService.Response response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_BUSINESS_INFORMATION, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_BUSINESS_INCOME, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_BUSINESS_EXPENSES, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_BUSINESS_ASSETS, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_BUSINESS_SUMMARY, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_MONTHLY_INCOME_EXPENSES_MAIN, response.NextPage);
        Test.stopTest();
    }

    @isTest
    private static void getNextScreen_2_BusinessFarms()
    {
        PFS__c pfs = FamilyNavigationServiceTestHelper.insertPfs(true);
        FamilyNavigationServiceTestHelper.insertFamilyAppSettings();
        
        Business_Farm__c businessFarm1 = FamilyNavigationServiceTestHelper.createBusinessFarm(pfs.Id);
        Business_Farm__c businessFarm2 = FamilyNavigationServiceTestHelper.createBusinessFarm(pfs.Id);
        businessFarm1.statBusinessInformation__c = false;
        businessFarm1.statBusinessExpenses__c = false;
        businessFarm2.Sequence_Number__c = 2;
        businessFarm2.statBusinessIncome__c = false;
        businessFarm2.statBusinessAssets__c = false;
        insert new List<Business_Farm__c>{businessFarm1, businessFarm2};

        ApplicationUtils appUtils = new ApplicationUtils(DEFAULT_LANGUAGE_NAME, pfs);
        
        Test.startTest();
            FamilyAppSettings__c setting = FamilyAppSettings__c.getInstance(FamilyNavigationServiceTestHelper.FAP_BUSINESS_FARM);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            FamilyNavigationService.Response response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_BUSINESS_INFORMATION, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            businessFarm1.statBusinessInformation__c = true;
            update businessFarm1;
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_BUSINESS_EXPENSES, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            businessFarm1.statBusinessExpenses__c = true;
            update businessFarm1;
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_BUSINESS_INCOME, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            businessFarm2.statBusinessIncome__c = true;
            update businessFarm2;
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_BUSINESS_ASSETS, response.NextPage);
        Test.stopTest();
    }

    @isTest
    private static void getNextScreen_MonthlyIncomeExpensesMain()
    {
        PFS__c pfs = FamilyNavigationServiceTestHelper.insertPfs(true);
        FamilyNavigationServiceTestHelper.insertFamilyAppSettings();
        ApplicationUtils appUtils = new ApplicationUtils(DEFAULT_LANGUAGE_NAME, pfs);

        Test.startTest();
            FamilyAppSettings__c setting = FamilyAppSettings__c.getInstance(FamilyNavigationServiceTestHelper.FAP_MONTHLY_INCOME_EXPENSES_MAIN);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            FamilyNavigationService.Response response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_MONTHLY_INCOME_EXPENSES, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_OTHER_INFORMATION, response.NextPage);
        Test.stopTest();
    }

    @isTest
    private static void getNextScreen_OtherInformation()
    {
        PFS__c pfs = FamilyNavigationServiceTestHelper.insertPfs(true);
        FamilyNavigationServiceTestHelper.insertFamilyAppSettings();
        ApplicationUtils appUtils = new ApplicationUtils(DEFAULT_LANGUAGE_NAME, pfs);

        Test.startTest();
            FamilyAppSettings__c setting = FamilyAppSettings__c.getInstance(FamilyNavigationServiceTestHelper.FAP_OTHER_INFORMATION);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            FamilyNavigationService.Response response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_OTHER_CONSIDERATIONS, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_ADDITIONAL_QUESTIONS, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertNotEquals(null, response.NextPage, 'Expected nextScreen to not be null.');
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_PFS_SUBMIT, response.NextPage);
        Test.stopTest();
    }

    @isTest
    private static void getNextScreen_noBusinessFarm_expectNavigationException()
    {
        PFS__c pfs = FamilyNavigationServiceTestHelper.insertPfs(true);
        FamilyNavigationServiceTestHelper.insertFamilyAppSettings();
        ApplicationUtils appUtils = new ApplicationUtils(DEFAULT_LANGUAGE_NAME, pfs);
        FamilyAppSettings__c setting = FamilyAppSettings__c.getInstance(FamilyNavigationServiceTestHelper.FAP_OTHER_EXPENSES);
        System.assertNotEquals(null, setting, 'Expected a setting to be returned.');
        FamilyNavigationService.Response response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);

        Test.startTest();
            try
            {
                String nextPage = response.NextPage;
                TestHelpers.expectedNavigationException();
            } catch (Exception e) {
                TestHelpers.assertNavigationException(e, FamilyNavigationService.NO_BUSINESS_FARMS_ERROR);
            }
        Test.stopTest();
    }

    @isTest
    private static void getNextScreen_speedBumpPages()
    {
        PFS__c pfs = FamilyNavigationServiceTestHelper.createPfs(true);
        pfs.statApplicantInformation__c = false;
        pfs.statHouseholdSummary__c = false;
        pfs.statSelectSchools__c = false;
        pfs.statTotalNon__c = false;
        pfs.statStudentIncome__c = false;
        pfs.statVehicles__c = false;
        pfs.statBusinessIncome__c = false;
        pfs.statBusinessSummary__c = false;
        pfs.statAdditionalQuestions__c = false;
        //pfs.statMonthlyIncomeExpenses__c = false;

        insert pfs;

        FamilyNavigationServiceTestHelper.insertBusinessFarm(pfs.Id);
        FamilyNavigationServiceTestHelper.insertFamilyAppSettings();
        ApplicationUtils appUtils = new ApplicationUtils(DEFAULT_LANGUAGE_NAME, pfs);

        Test.startTest();
            FamilyAppSettings__c setting = FamilyAppSettings__c.getInstance(FamilyNavigationServiceTestHelper.FAP_HOUSEHOLD_INFORMATION);
            FamilyNavigationService.Response response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_APPLICANT_INFORMATION, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            pfs.statApplicantInformation__c = true;
            update pfs;
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_HOUSEHOLD_SUMMARY, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            pfs.statHouseholdSummary__c = true;
            update pfs;
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_SCHOOL_SELECTION, response.NextPage);

            // Expected that user will be redirected to the speedBumpPage at first 
            // and after the pressing "Save and Next" button again
            // he will be redirected to the incomplete step of the section
            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_SELECT_SCHOOLS, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            pfs.statSelectSchools__c = true;
            update pfs;
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_FAMILY_INCOME, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_TOTAL_NON, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            pfs.statTotalNon__c = true;
            update pfs;
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_STUDENT_INCOME, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            pfs.statStudentIncome__c = true;
            update pfs;
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_FAMILY_ASSETS, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_VEHICLES, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            pfs.statVehicles__c = true;
            update pfs;
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_BUSINESS_FARM, response.NextPage);

            /**
            *   The test block below includes test for correct expected logic behavior and should not be deleted.
            *   Since now logic, related to business farms, is working incorrectly this block had to be commented.
            *   
            *   The root-cause of the incorrect behavior of the logic is using status fields of Business Farm sobject but
            *   not Pfs sobject(as it is for all other sections). It seems like the problem with displaying
            *   some particular step in business farm section as complete(when it's actually not) is also related
            *   to using these status fields of business farms. Also there can be other issues related to this
            *   status fields duplicating in the logic. 
            */

            /*
            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_BUSINESS_INCOME, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            pfs.statBusinessIncome__c = true;
            update pfs;
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_BUSINESS_SUMMARY, response.NextPage);
            */

            /* Block to remove when the logic is fixed */
            // -->
            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_BUSINESS_SUMMARY, response.NextPage);
            // <--

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            pfs.statBusinessSummary__c = true;
            update pfs;
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_OTHER_INFORMATION, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_ADDITIONAL_QUESTIONS, response.NextPage);

            setting = FamilyAppSettings__c.getInstance(response.NextPage);
            pfs.statAdditionalQuestions__c = true;
            update pfs;
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_PFS_SUBMIT, response.NextPage);

            setting.Next_Page__c = null;
            response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(null, response.NextPage);
        Test.stopTest();
    }

    @isTest
    private static void getNextScreen_MIE()
    {
        Account school = AccountTestData.Instance.asSchool().DefaultAccount;
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        String businessOrFarmOwner = 'Yes';
        PFS__c pfs = PfsTestData.Instance
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forOwnBusinessOrFarm(businessOrFarmOwner)
            .asCompleted()
            .forStatMonthlyIncomeExpenses(false).DefaultPfs;

        Applicant__c applicant = ApplicantTestData.Instance.DefaultApplicant;

        School_PFS_Assignment__c spfsa = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(currentAcademicYear.Name).DefaultSchoolPfsAssignment;
        String collectMIE = 'Yes';
        Annual_Setting__c annualSetting = AnnualSettingsTestData.Instance
            .forCollectMIE(collectMIE).DefaultAnnualSettings;

        FamilyNavigationServiceTestHelper.insertBusinessFarm(pfs.Id);
        FamilyNavigationServiceTestHelper.insertFamilyAppSettings();

        List<PFS__c> pfss = [
            SELECT Academic_Year_Picklist__c, StatMonthlyIncomeExpensesMain__c, StatMonthlyIncomeExpenses__c, (SELECT Pfs__c FROM Applicants__r) FROM PFS__c];

        ApplicationUtils appUtils = new ApplicationUtils(DEFAULT_LANGUAGE_NAME, pfss[0]);

        Test.startTest();
            FamilyAppSettings__c setting = FamilyAppSettings__c.getInstance(FamilyNavigationServiceTestHelper.FAP_HOUSEHOLD_INFORMATION);
            FamilyNavigationService.Response response = FamilyNavigationService.Instance.getNextSection(setting, appUtils);
            System.assertEquals(FamilyNavigationServiceTestHelper.FAP_MONTHLY_INCOME_EXPENSES_MAIN, response.NextPage);
        Test.stopTest();
    }
}