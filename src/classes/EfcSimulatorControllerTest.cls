@isTest
private class EfcSimulatorControllerTest
{

    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

    private static void populateWorksheet(EfcWorksheetData worksheet) {
        worksheet.parentState = 'Alabama';
        worksheet.familyStatus = '2 Parents';
        worksheet.ageParentA = 45;
        worksheet.ageParentB = 40;
        worksheet.familySize = 5;
        worksheet.numChildrenInTuitionSchools = 2;
        worksheet.gradeApplying = '4';
        worksheet.dayOrBoarding = 'Day';
        worksheet.filingStatus = 'Married, Filing Jointly';
        worksheet.incomeTaxExemptions = 4;
        worksheet.salaryWagesParentA = 100000;
        worksheet.salaryWagesParentB = 50000;
        worksheet.interestIncome = 10000;
        worksheet.otherIncome = 10000;
        worksheet.otherUntaxedIncome = 10000;
        worksheet.medicalDentalExpenses = 10000;
        worksheet.unusualExpenses = 10000;
        worksheet.homePurchaseYear = '2000';
        worksheet.homePurchasePrice = 400000;
        worksheet.homeMarketValue = 500000;
        worksheet.unpaidPrincipal1stMortgage = 100000;
        worksheet.otherRealEstateMarketValue = 100000;
        worksheet.otherRealEstateUnpaidMortgagePrincipal = 10000;
        worksheet.bankAccounts = 10000;
        worksheet.totalDebts = 1000;
        worksheet.studentAssets = 1000;
        worksheet.pjOverrideDefaultColaValue = 1.2;
        worksheet.pjUseDividendInterestIncomeToImputeAssets = true;
        worksheet.pjPercentageForImputingAssets = 2;
        worksheet.pjUseHomeEquity = true;
        worksheet.pjUseHomeEquityCap = true;
        worksheet.pjUseHousingIndexMultiplier = true;
    }

    @isTest
    private static void testSchoolEfcSimulatorController() {
        // set up the test data
        Account a = TestUtils.createAccount('test account', RecordTypes.schoolAccountTypeId, 500, true);
        Contact c = TestUtils.createContact('test contact', a.Id, RecordTypes.schoolStaffContactTypeId, true);
        Id portalProfileId = [select Id from Profile where Name = :ProfileSettings.SchoolAdminProfileName].Id;

        User u = TestUtils.createPortalUser('test portaluser', 'testEFCSimulate1@exponentpartners.com', 'test', c.Id, portalProfileId, true, false);

        System.runAs(thisUser){
            insert u;
        }

        List<Academic_Year__c> years = TestUtils.createAcademicYears();
        Academic_Year__c academicYear = GlobalVariables.getCurrentAcademicYear();
        TestUtils.createSSSConstants(academicYear.Id, true);
        TableTestData.populateAllEfcTables(academicYear.Id);

        // run as the portal user
        System.runAs(u) {
            // instantiate the controller
            Test.setCurrentPageReference(Page.SchoolEfcSimulator);
            EfcSimulatorController controller = new EfcSimulatorController();
            controller.setIsRunInternal(false);

            // set up the worksheet inputs
            populateWorksheet(controller.worksheet);


            // do the calculation action
            controller.doCalculate();

            // verify that there is no error
            System.assertEquals(null, controller.message);

            // verify calculated values were written back to the worksheet
            System.assertNotEquals(null, controller.worksheet.totalTaxableIncome);
            System.assertNotEquals(null, controller.worksheet.totalNonTaxableIncome);
            System.assertNotEquals(null, controller.worksheet.totalIncome);
            System.assertNotEquals(null, controller.worksheet.federalIncomeTaxCalculated);
            System.assertNotEquals(null, controller.worksheet.socialSecurityTaxAllowanceCalculated);
            System.assertNotEquals(null, controller.worksheet.medicareTaxAllowanceCalculated);
            System.assertNotEquals(null, controller.worksheet.stateOtherTaxAllowanceCalculated);
            System.assertNotEquals(null, controller.worksheet.employmentAllowance);
            System.assertNotEquals(null, controller.worksheet.medicalAllowance);
            System.assertNotEquals(null, controller.worksheet.totalAllowances);
            System.assertNotEquals(null, controller.worksheet.effectiveIncome);
            System.assertNotEquals(null, controller.worksheet.homeEquity);
            System.assertNotEquals(null, controller.worksheet.homeEquityCap);
            System.assertNotEquals(null, controller.worksheet.homeEquityUncapped);
            System.assertNotEquals(null, controller.worksheet.otherRealEstateEquity);
            System.assertNotEquals(null, controller.worksheet.imputedAssets);
            System.assertNotEquals(null, controller.worksheet.totalDebts);
            System.assertNotEquals(null, controller.worksheet.netWorth);
            System.assertNotEquals(null, controller.worksheet.incomeSupplement);
        }
    }

    @isTest
    private static void testSchoolEfcSimulatorValidatation() {
        // set up the test data
        Account a = TestUtils.createAccount('test account', RecordTypes.schoolAccountTypeId, 500, true);
        Contact c = TestUtils.createContact('test contact', a.Id, RecordTypes.schoolStaffContactTypeId, true);
        Id portalProfileId = [select Id from Profile where Name = :ProfileSettings.SchoolAdminProfileName].Id;
        User u = TestUtils.createPortalUser('test portaluser', 'testEFCSimulate2@exponentpartners.com', 'test', c.Id, portalProfileId, true, false);

        System.runAs(thisUser){
            insert u;
        }

        List<Academic_Year__c> years = TestUtils.createAcademicYears();
        Academic_Year__c academicYear = GlobalVariables.getCurrentAcademicYear();
        TestUtils.createSSSConstants(academicYear.Id, true);
        TableTestData.populateAllEfcTables(academicYear.Id);

        System.runAs(u) {
            // instantiate the controller
            Test.setCurrentPageReference(Page.SchoolEfcSimulator);
            EfcSimulatorController controller = new EfcSimulatorController();
            controller.setIsRunInternal(false);

            // verify validation errors
            populateWorksheet(controller.worksheet);
            controller.worksheet.parentState = null;
            controller.doCalculate();
            System.assert(controller.getHasMessage() && controller.message.contains('Parent State'));

            populateWorksheet(controller.worksheet);
            controller.worksheet.familyStatus = null;
            controller.doCalculate();
            System.assert(controller.getHasMessage() && controller.message.contains('Family Status'));

            populateWorksheet(controller.worksheet);
            controller.worksheet.ageParentA = null;
            controller.doCalculate();
            System.assert(controller.getHasMessage() && controller.message.contains('Parent A Age'));

            populateWorksheet(controller.worksheet);
            controller.worksheet.familySize = null;
            controller.doCalculate();
            System.assert(controller.getHasMessage() && controller.message.contains('Family Size'));

            populateWorksheet(controller.worksheet);
            controller.worksheet.numChildrenInTuitionSchools = null;
            controller.doCalculate();
            System.assert(controller.getHasMessage() && controller.message.contains('Number of Children in Tuition-Charging Schools'));

            populateWorksheet(controller.worksheet);
            controller.worksheet.gradeApplying = null;
            controller.doCalculate();
            System.assert(controller.getHasMessage() && controller.message.contains('Grade Applying'));

            populateWorksheet(controller.worksheet);
            controller.worksheet.dayOrBoarding = null;
            controller.doCalculate();
            System.assert(controller.getHasMessage() && controller.message.contains('Day/Boarding'));

            populateWorksheet(controller.worksheet);
            controller.worksheet.filingStatus = null;
            controller.doCalculate();
            System.assert(controller.getHasMessage() && controller.message.contains('Filing Status'));

            populateWorksheet(controller.worksheet);
            controller.worksheet.incomeTaxExemptions = null;
            controller.doCalculate();
            System.assert(controller.getHasMessage() && controller.message.contains('Income Tax Exemptions'));

            populateWorksheet(controller.worksheet);
            controller.worksheet.salaryWagesParentA = null;
            controller.doCalculate();
            System.assert(controller.getHasMessage() && controller.message.contains('Salary Parent A'));

            populateWorksheet(controller.worksheet);
            controller.worksheet.pjUseHousingIndexMultiplier = true;
            controller.worksheet.homePurchaseYear = null;
            controller.doCalculate();
            System.assert(controller.getHasMessage() && controller.message.contains('Home Year Purchased'));

            populateWorksheet(controller.worksheet);
            controller.worksheet.pjUseHousingIndexMultiplier = true;
            controller.worksheet.homePurchasePrice = null;
            controller.doCalculate();
            System.assert(controller.getHasMessage() && controller.message.contains('Home Purchase Price'));

            populateWorksheet(controller.worksheet);
            controller.worksheet.adjustHousingPortion = 'Yes';
            controller.worksheet.incomeProtectionAllowanceHousing = null;
            controller.doCalculate();
            System.assert(controller.getHasMessage() && controller.message.contains('Annual Housing Amount'));

            System.assert(true, (controller.getFilingStatusOptions()).size()>0);
            System.assert(true, (controller.getParentBFilingStatusOptions()).size()>0);
        }
    }
    
    @isTest
    private static void testSchoolEfcSimulatorProfilePortal() {
        Account account = TestUtils.createAccount('Account #1', RecordTypes.schoolAccountTypeId, 500, true);
        Contact contact = TestUtils.createContact('Contact #1', account.Id, RecordTypes.schoolStaffContactTypeId, true);
        Id portalProfileId = [
                SELECT Id 
                FROM Profile 
                WHERE Name = :ProfileSettings.SchoolAdminProfileName].Id;

        User portalUser = TestUtils.createPortalUser('Portal User #1', 'EFCProfile@testmethod.com', 'Alias', contact.Id, portalProfileId, true, false);
        
        System.runAs(thisUser) {
            insert portalUser;
        }

        TestUtils.createAcademicYears();
        Academic_Year__c academicYear = GlobalVariables.getCurrentAcademicYear();
        TestUtils.createSSSConstants(academicYear.Id, true);
        TableTestData.populateAllEfcTables(academicYear.Id);
        
        System.runAs(portalUser) {
            Account inFocusAccount = new Account(
                Name = 'In Focus Account'
            );
            insert inFocusAccount;
            
            portalUser.In_Focus_School__c = inFocusAccount.Id;
            update portalUser;
            
            insert new EFC_Profile__c(
                Name = 'EFC Profile #1',
                Account__c = inFocusAccount.Id
            );
            
            Test.setCurrentPageReference(Page.SchoolEfcSimulator);
            EfcSimulatorController controller = new EfcSimulatorController();
            controller.setIsRunInternal(false);
            
            System.assertEquals(true, controller.isProfileCanWork);
            System.assertEquals(false, controller.isShowAcademicYearList);
            System.assertEquals(null, controller.profileId);
            System.assertEquals(2, controller.profileSelectOptions.size());

            populateWorksheet(controller.worksheet);
            
            controller.saveAsName = 'EFC Profile #2';
            controller.profileSaveAs();
            System.assertEquals(3, controller.profileSelectOptions.size());
            System.assertEquals(true, ApexPages.hasMessages());
            
            controller.profileSave();
            System.assertEquals(true, ApexPages.hasMessages());
            
            controller.profileDelete();
            System.assertEquals(2, controller.profileSelectOptions.size());
            System.assertEquals(null, controller.profileId);
            System.assertEquals(true, ApexPages.hasMessages());
            
            controller.profileId = controller.profileSelectOptions[1].getValue();
            controller.changeProfile();
        }
    }
    
    @isTest
    private static void testSchoolEfcSimulatorProfileInternalNoAccount() {
        TestUtils.createAcademicYears();
        Academic_Year__c academicYear = GlobalVariables.getCurrentAcademicYear();
        TestUtils.createSSSConstants(academicYear.Id, true);
        TableTestData.populateAllEfcTables(academicYear.Id);
        
        Test.setCurrentPageReference(Page.InternalEfcSimulator);
        EfcSimulatorController controller = new EfcSimulatorController();
        controller.setIsRunInternal(true);
        
        System.assertEquals(true, ApexPages.hasMessages());
        System.assertEquals(false, controller.isProfileCanWork);
    }

    @isTest
    private static void testSchoolEfcSimulatorProfileInternal() {
        TestUtils.createAcademicYears();
        Academic_Year__c academicYear = GlobalVariables.getCurrentAcademicYear();
        TestUtils.createSSSConstants(academicYear.Id, true);
        TableTestData.populateAllEfcTables(academicYear.Id);
        
        Account account = TestUtils.createAccount('SSS Training School', RecordTypes.schoolAccountTypeId, 500, true);
        
        Test.setCurrentPageReference(Page.InternalEfcSimulator);
        EfcSimulatorController controller = new EfcSimulatorController();
        controller.setIsRunInternal(true);
        
        System.assertEquals(false, ApexPages.hasMessages());
        System.assertEquals(true, controller.isProfileCanWork);
        System.assertEquals(true, controller.isShowAcademicYearList);
        
        controller.academicYearId = controller.academicYearSelectOptions[1].getValue();
        controller.changeAcademicYear();
        
        System.assertEquals(controller.academicYear, GlobalVariables.getAcademicYear(controller.academicYearId));
        
        System.assertEquals(true, controller.getIsRunInternal());
    }
}