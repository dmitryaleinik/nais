/**
 * Created by hansen on 3/30/16.
 */

@IsTest
private class SchoolCommunityFooterControllerTest {
    @isTest
    private static void getFooterHtml_expectUnhandledExceptionNotThrown() {
        //The method will return an empty string since we aren't inserting documents.
        String footerHtml = new SchoolCommunityFooterController().getFooterHtml();
        System.assertNotEquals(null, footerHtml,
                'Expected footer html to be returned from component.');
    }
}