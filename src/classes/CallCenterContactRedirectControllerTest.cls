@isTest
private class CallCenterContactRedirectControllerTest
{

    /****tests****/
    private static Account skool;
    private static  Contact parentContact;
    private static  Contact schoolContact;

    private static void setupData()
    {
        FieldIDbyName__c dnisSetting  = new FieldIDbyName__c(name = 'dnis', Field_ID__c = 'dnisFieldID');
        FieldIDbyName__c aniSetting  =  new FieldIDbyName__c(name = 'ani', Field_ID__c = 'aniFieldID');
        FieldIDbyName__c callIDSetting  =  new FieldIDbyName__c(name = 'callID', Field_ID__c = 'callIDfieldID');
        insert new List <FieldIDbyName__c> {dnisSetting,aniSetting,callIDSetting};
        skool = new Account(name = 'account1',SSS_School_Code__c = 'asdf', phone='(222) -123-4444', RecordTypeID = RecordTypes.schoolAccountTypeId);

        insert skool;
        schoolContact = new Contact (accountid =skool.id, LastName = 'c1', mobilephone='(222) -123-1234', RecordTypeID = RecordTypes.schoolStaffContactTypeId);
        parentContact = new Contact( LastName = 'c1', mobilephone='(222) -555-1234', RecordTypeID = RecordTypes.parentContactTypeId);
        insert new List<Contact> { schoolContact,parentContact};
    }

    @isTest
    private static void testParentCase()
    {
        setupData();

        Cookie cDnis = new Cookie('dnis','cookieDnis',null,-1,false);
        Cookie cAni =new Cookie('ani','cookieAni',null,-1,false);
        Cookie cUsername = new Cookie('userName','cookieuserName',null,-1,false);
        Cookie cCallID = new Cookie('callID','cookiecallID',null,-1,false);

        PageReference pageRef = new PageReference('/apex/CallCenterNewParentCase?id=' +parentContact.id );
        pageRef.setCookies(new Cookie[]{cDnis,cAni,cUsername,cCallID});
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(parentContact);
        CallCenterContactRedirectController redirectController = new CallCenterContactRedirectController(sc);

        PageReference redirectPageref = redirectController.newParentCaseFromContact();
        System.debug(redirectPageref.getUrl());
        String expectedUrl = '/console?tsid=' + CallCenterContactRedirectController.CallCenterConsoleAppId + '#' + EncodingUtil.urlEncode('/500/e?RecordType='
                + CallCenterContactRedirectController.parentSupportCaseTypeId + '&def_contact_id='
                + parentContact.id + '&dnisFieldID=cookieDnis&aniFieldID=cookieAni&callIDfieldID=cookiecallID&retURL=/' + parentContact.Id + '&writephonetocase=1', 'UTF-8');
        System.assertequals(expectedUrl, redirectPageref.getUrl());
    }

    @isTest
    private static void testSchoolCaseFromContact()
    {
        setupData();

        Cookie cDnis = new Cookie('dnis','cookieDnis',null,-1,false);
        Cookie cAni =new Cookie('ani','cookieAni',null,-1,false);
        Cookie cUsername = new Cookie('userName','cookieuserName',null,-1,false);
        Cookie cCallID = new Cookie('callID','cookiecallID',null,-1,false);

        PageReference pageRef = new PageReference('/apex/CallCenterNewSchoolCase?id=' +schoolContact.id );
        pageRef.setCookies(new Cookie[]{cDnis,cAni,cUsername,cCallID});
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(schoolContact);
        CallCenterContactRedirectController redirectController = new CallCenterContactRedirectController(sc);

        PageReference redirectPageref = redirectController.newSchoolCaseFromContact();
        System.debug(redirectPageref.getUrl());
        System.assertequals(redirectPageref.getUrl(),
                '/500/e?aniFieldID=cookieAni&callIDfieldID=cookiecallID&def_account_id='  + skool.id +
                        '&def_contact_id=' + schoolContact.id +'&dnisFieldID=cookieDnis&RecordType='+ CallCenterContactRedirectController.schoolSupportCaseTypeId +
                        '&retURL=%2F' + schoolContact.Id + '&writephonetocase=1');
    }

    @isTest
    private static void testSchoolCaseFromAccount()
    {
        setupData();

        Cookie cDnis = new Cookie('dnis','cookieDnis',null,-1,false);
        Cookie cAni =new Cookie('ani','cookieAni',null,-1,false);
        Cookie cUsername = new Cookie('userName','cookieuserName',null,-1,false);
        Cookie cCallID = new Cookie('callID','cookiecallID',null,-1,false);

        PageReference pageRef = new PageReference('/apex/CallCenterNewSchoolCaseFromAccount?id=' +skool.id );
        pageRef.setCookies(new Cookie[]{cDnis,cAni,cUsername,cCallID});
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(skool);
        CallCenterContactRedirectController redirectController = new CallCenterContactRedirectController(sc);

        PageReference redirectPageref = redirectController.newSchoolCaseFromAccount();
        System.debug(redirectPageref.getUrl());


        System.assertequals(redirectPageref.getUrl(),
                '/500/e?aniFieldID=cookieAni&callIDfieldID=cookiecallID&def_account_id=' + skool.id +
                        '&dnisFieldID=cookieDnis&RecordType='+ CallCenterContactRedirectController.schoolSupportCaseTypeId +
                        '&retURL=%2F' + skool.Id + '&writephonetocase=1');
    }
}