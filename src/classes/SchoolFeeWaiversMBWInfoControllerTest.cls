/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SchoolFeeWaiversMBWInfoControllerTest
{

    @isTest
    private static void testFeeWaiversMBW()
    {
        List<Academic_Year__c> years = TestUtils.createAcademicYears();
        Test.setCurrentPage(Page.SchoolFeeWaiversMBWInfo);
        ApexPages.currentPage().getParameters().put('academicyearid', GlobalVariables.getCurrentAcademicYear().Id);
        SchoolFeeWaiversMBWInfoController testCon = new SchoolFeeWaiversMBWInfoController();
        testCon.initCommon();

        PageReference pageRef = testCon.purchaseAdditionalWaivers();
        System.assertEquals(pageRef.geturl(),'/apex/schoolfeewaiverspurchase');
        pageRef = testCon.feeWaiverCancel();
        System.assertEquals(pageRef.geturl(),'/apex/schoolfeewaivers');
        pageRef = testCon.feeWaiverPurchase();
        System.assertEquals(pageRef.geturl(),'/apex/schoolfeewaiverspurchase');
    }
}