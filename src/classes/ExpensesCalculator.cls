public class ExpensesCalculator
{
    private School_PFS_Assignment__c querySPA(String schoolpfsassignmentid)
    {
        if(schoolpfsassignmentid!=null)
        {
            List<School_PFS_Assignment__c> spas = [Select Id, School__c, Applicant__r.PFS__c, 
                                                            Applicant__r.Grade_in_Entry_Year__c, 
                                                            Student_Folder__c, Academic_Year_Picklist__c 
                                                                from School_PFS_Assignment__c  
                                                                where Id=:schoolpfsassignmentid 
                                                                    and School__r.SSS_Subscriber_Status__c in :GlobalVariables.activeSSSStatuses 
                                                                limit 1];
            return (spas!=null && !spas.isEmpty()?spas[0]:null);
        }
        return null;
    }//End:getSchoolPFSAssigment
    
    private Annual_Setting__c queryAnnualSetting(String schoolId, String academicYearId)
    {
        List<String> fieldNames = new List<String>(Schema.SObjectType.Annual_Setting__c.fields.getMap().keySet());
        String queryString = 'SELECT ' + String.Join(fieldNames, ',');
        queryString += ' FROM Annual_Setting__c WHERE Academic_Year__c = :academicYearId AND School__c =:schoolId';
        List<Annual_Setting__c> ann = Database.query(queryString);
        return (ann!=null && !ann.isEmpty()?ann[0]:null);
    }//End:queryAnnualSetting
    
    /**
    * @description Method implemented to calculate the tuition&fees values
    * of a single SPA's folder.
    * @param schoolpfsassignmentid The id of the SPA.
    * @param prevGradeApplying The previous value of the Grade_Applying__c field.
    * If its a new folder it should be null.
    * @param newGradeApplying The new value selected for Grade_Applying__c field.
    * @param folder The student_folder__c record that may contain modified values
    * from the controller from which this function was invoked.
    */
    public Student_Folder__c calculateExpenses(String schoolpfsassignmentid, 
                                                String prevGradeApplying,
                                                String newGradeApplying,
                                                Student_Folder__c finalFolder,
                                                String prevDayBoarding,
                                                Annual_Setting__c annualSetting)
    {
        School_PFS_Assignment__c spa = this.querySPA(schoolpfsassignmentid);
        return calculateExpenses(spa, prevGradeApplying, newGradeApplying, finalFolder, prevDayBoarding, annualSetting);
    }
    
    public Student_Folder__c calculateExpenses(School_PFS_Assignment__c spa, 
                                                String prevGradeApplying,
                                                String newGradeApplying,
                                                Student_Folder__c finalFolder,
                                                String prevDayBoarding,
                                                Annual_Setting__c annualSetting)
    {
        if(spa!=null && spa.School__c!=null && prevGradeApplying!=newGradeApplying  && newGradeApplying!=null)
        {
            annualSetting = (annualSetting!=null?annualSetting:
                                this.queryAnnualSetting(spa.School__c, 
                                                            GlobalVariables.getAcademicYearByName(spa.Academic_Year_Picklist__c).id));
            if(annualSetting!=null)
            {
                return this.calculateExpensesValues(annualSetting, prevGradeApplying, newGradeApplying, finalFolder, prevDayBoarding);
            }
        }
        return finalFolder;
    }//End:getTuitionFees
    
    private Student_Folder__c calculateExpensesValues(Annual_Setting__c annualSetting,
                                        String prevGradeApplying, 
                                        String newGradeApplying,
                                        Student_Folder__c finalFolder,
                                        String prevDayBoarding)
    {
        TuitionFeesWrapper newGrade = new TuitionFeesWrapper(newGradeApplying,
                                                            annualSetting.Tuition_Schedule_Type__c, 
                                                            finalFolder.Day_Boarding__c == 'Day');
                                                            
        TuitionFeesWrapper previousGrade = new TuitionFeesWrapper(prevGradeApplying,
                                                            annualSetting.Tuition_Schedule_Type__c, 
                                                            prevDayBoarding == 'Day');
                                                            
        finalFolder.Student_Tuition__c = this.getFieldValue(finalFolder.Student_Tuition__c, 
                                                    this.getAnnualSettingFieldValue(annualSetting, newGrade.tuitionFieldName),
                                                    this.getAnnualSettingFieldValue(annualSetting, previousGrade.tuitionFieldName));
        finalFolder.Travel_Expenses__c = this.getFieldValue(finalFolder.Travel_Expenses__c, 
                                                    this.getAnnualSettingFieldValue(annualSetting, newGrade.travelExpFieldName),
                                                    this.getAnnualSettingFieldValue(annualSetting, previousGrade.travelExpFieldName));
        finalFolder.Student_Fees__c = this.getFieldValue(finalFolder.Student_Fees__c, 
                                                    this.getAnnualSettingFieldValue(annualSetting, newGrade.studentFeesFieldName),
                                                    this.getAnnualSettingFieldValue(annualSetting, previousGrade.studentFeesFieldName));
        
        finalFolder.New_Student_Expenses__c = finalFolder.New_Returning__c == 'New' ? 
                                                    this.getFieldValue(finalFolder.New_Student_Expenses__c, 
                                                        this.getAnnualSettingFieldValue(annualSetting, newGrade.newStudentExpFieldName),
                                                        this.getAnnualSettingFieldValue(annualSetting, previousGrade.newStudentExpFieldName))
                                                        : null;
        finalFolder.Returning_Student_Expenses__c = finalFolder.New_Returning__c == 'Returning'?                                                     
                                                     this.getFieldValue(finalFolder.Returning_Student_Expenses__c,
                                                        this.getAnnualSettingFieldValue(annualSetting, newGrade.retStudentExpFieldName),
                                                        this.getAnnualSettingFieldValue(annualSetting, previousGrade.retStudentExpFieldName)) 
                                                       : null;
        return finalFolder;
    }//End:calculate
    
    private Object getAnnualSettingFieldValue(Annual_Setting__c record, String fieldName)
    {
        return (fieldName!=null?record.get(fieldName):null);
    }//End:getAnnualSettingFieldValue
    
    private Double getFieldValue(Decimal currentFieldValue, Object newGradeValue, Object previousGradeValue)
    {
        return this.getFieldValue(currentFieldValue, Double.valueOf(newGradeValue), previousGradeValue==null?-1:Double.valueOf(previousGradeValue));
    }//End:getFieldValue
    
    /**
    * @description  the tuition fields should be updated to reflect the new grade only if the current 
    * values match what the previous grade was or they are blank. For example, if a school has set 
    * 2nd grade student tuition to be $15,000 and they update a student folder from 2nd to 3rd grade, 
    * the Student Tuition field would only be updated if the value was $15,000 or blank.
    * @param currentFieldValue The current value in the Student_Folder
    * @param newGradeValue The new value calculated with the new selected Grade and the values of the annual setting.
    * @param previousGradeValue The previous value calculated with the previous selected Grade and the values of the annual setting.
    * @return The value to be set in the StudentFolder field.
    */
    private Double getFieldValue(Decimal currentFieldValue, Double newGradeValue, Double previousGradeValue)
    {
        return previousGradeValue==-1 || previousGradeValue==currentFieldValue || currentFieldValue==null
                    ? newGradeValue : currentFieldValue;
    }//End:getFieldValue
    
    public class TuitionFeesWrapper
    {       
        public String tuitionFieldName{ get; set; }
        public String travelExpFieldName{ get; set; }
        public String studentFeesFieldName{ get; set; }
        public String newStudentExpFieldName{ get; set; }
        public String retStudentExpFieldName{ get; set; }
            
        public TuitionFeesWrapper(String gradeApplying, String tuitionScheduleType, Boolean isDay)
        {
            if (tuitionScheduleType == 'One Schedule for Entire School')
            {
                this.tuitionFieldName = 'One_Schedule'+(isDay ? '_Day_Tuition__c' : '_Boarding_Tuition__c');
                this.travelExpFieldName = 'One_Schedule'+(isDay ? '_Day_Travel_Expense__c' : '_Boarding_Travel_Expense__c');
                this.studentFeesFieldName = 'One_Schedule'+(isDay ? '_Day_Fees__c' : '_Boarding_Fees__c');
                this.newStudentExpFieldName = 'One_Sched'+(isDay ? 'ule_Day_New_Student_Expense__c' : '_Boarding_New_Student_Expense__c');
                this.retStudentExpFieldName = 'One_Sched' + (isDay ? 'ule_Day_Ret_Student_Expense__c' : '_Boarding_Ret_Student_Expense__c');
            } else if(gradeApplying!=null){
                Map<String, String> gradeFieldPicklistToAnnSetFields = this.populateGradeMap();
                String gradeString = gradeFieldPicklistToAnnSetFields.get(gradeApplying);
                this.tuitionFieldName = gradeString + (isDay ? '_Day_Tuition__c' : '_Boarding_Tuition__c');
                this.travelExpFieldName = gradeString + (isDay ? '_Day_Travel_Expense__c' : '_Boarding_Travel_Expense__c');
                this.studentFeesFieldName = gradeString + (isDay ? '_Day_Fees__c' : '_Boarding_Fees__c');
                this.newStudentExpFieldName = gradeString + (isDay ? '_Day_New_Student_Expense__c' : '_Boarding_New_Student_Expense__c');
                this.retStudentExpFieldName = gradeString + (isDay ? '_Day_Ret_Student_Expense__c' : '_Boarding_Ret_Student_Expense__c');
            }
        }//End-Constructor
           
    
        private Map<String, String> populateGradeMap(){
            return new Map<String, String>{
                                    'Preschool' => 'Preschool',
                                    'Pre-Kindergarten' => 'Pre_K',
                                    'Junior Kindergarten' => 'Jr_K',
                                    'Kindergarten' => 'K',
                                    'Pre-First' => 'Pre_First',
                                    '1' => 'Grade_1',
                                    '2' => 'Grade_2',
                                    '3' => 'Grade_3',
                                    '4' => 'Grade_4',
                                    '5' => 'Grade_5',
                                    '6' => 'Grade_6',
                                    '7' => 'Grade_7',
                                    '8' => 'Grade_8',
                                    '9' => 'Grade_9',
                                    '10' => 'Grade_10',
                                    '11' => 'Grade_11',
                                    '12' => 'Grade_12',
                                    'Post Graduation' => 'Post_Grad'};
        }//End:populateGradeMap
    }//End-Class:TuitionFeesWrapper 
}//End-Class