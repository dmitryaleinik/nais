/**
 * @description This class is used to handle operations related to required documents such as request additional RD from School Portal.
 */

public class DocumentRequirementService {
    @testVisible private static final String DOC_TYPE_PARAM = 'docType';
    @testVisible private static final String DOC_YEAR_PARAM = 'docYear';
    @testVisible private static final String SPA_PARAM = 'spa';
    @testVisible private static final String SEND_EMAIL_ALERT_PARAM = 'sendEmailAlert';
    @testVisible private static final String FOLDER_IDS_PARAM = 'folderIds';
    @testVisible private static final String RUN_DML_PARAM = 'dml';
    
    public DocumentRequirementService.Response requestAdditionalDocuments(DocumentRequirementService.Request request) {
        
        DocumentRequirementService.Response response = new DocumentRequirementService.Response();
        
        if(request.docType == 'Other/Misc Tax Form') return response;
        
        response.familyDocumentByHousehold = getFamilyDocByHouseholdId(request);
        
        School_Document_Assignment__c sda;
        for(School_PFS_Assignment__c spa : request.spas) {
            
            sda = new School_Document_Assignment__c(
                School_PFS_Assignment__c = spa.Id,
                Document_Type__c = request.docType,
                Document_Year__c = request.docYear,
                Document__c = response.familyDocumentByHousehold.get(spa.Applicant__r.PFS__r.Parent_A_Household_ID__c),
                PFS_Specific__c = true, 
                Send_Email_Alert__c = request.shouldSendEmail,
                Parent_A_Email__c = spa.Applicant__r.PFS__r.Parent_A_Email__c);
            
            sda.Source__c = request.source;
            response.sdasToInsert.add(sda);
            
            response.sdasByPFS.put(spa.PFS_ID__c, sda);
        }
        
        response.siblingsSDAsToInsert = requestAdditionalDocumentsToSiblings(request, response.sdasByPFS);
        
        if(request.runDml && !response.sdasToInsertFinal.isEmpty()) {
            insert response.sdasToInsertFinal;
        }
        
        return response;
    }//End:requestAdditionalDocuments
    
    private List<School_Document_Assignment__c> requestAdditionalDocumentsToSiblings(
        DocumentRequirementService.Request request, 
        Map<Id, School_Document_Assignment__c> sdasByPFS){
        
        School_Document_Assignment__c sda;
        List<School_Document_Assignment__c> siblingsSDAsToInsert = new List<School_Document_Assignment__c>();
        
        for (School_PFS_Assignment__c spa : [
            SELECT Id, School__c, Applicant__c, PFS_ID__c 
            FROM School_PFS_Assignment__c
            WHERE PFS_ID__c = :request.pfsIds 
            AND School__c = :request.schoolIds 
            AND Applicant__c != :request.applicantIds 
            AND Withdrawn__c != 'Yes']){
            
            if(sdasByPFS.containsKey(spa.PFS_ID__c)) {
                
                sda = (sdasByPFS.get(spa.PFS_ID__c)).clone(false, true, false, false);
                sda.School_PFS_Assignment__c = spa.Id;
                sda.Send_Email_Alert__c = false; //Prevent an email being sent for every SDA record.
                sda.Import_Id__c = null; //Prevent error from external id on clone.
                sda.Source__c = request.source;
                siblingsSDAsToInsert.add(sda);
            }
        }
        
        return siblingsSDAsToInsert;
    }//End:requestAdditionalDocumentsToSiblings
    
    private Map<Id, Id> getFamilyDocByHouseholdId(DocumentRequirementService.Request request) {
        
        Map<Id, Id> familyDocumentByHousehold = new Map<Id, Id>();
        Set<Id> householdIds = request.householdIds;
        String docYear = request.docYear;
        String docType = request.docType;
        
        String queryStr = 'SELECT Id, Household__c ' +
            'FROM Family_Document__c  ' +
            'WHERE Household__c IN: householdIds  ' +
            'AND Document_Year__c =: docYear  ' +
            'AND Document_Type__c ' + ( !request.docType.contains('1040') ?  '=: docType  ' : ' LIKE \'%1040%\' ' ) +
            'AND Deleted__c = false';
            
        for (Family_Document__c fd : database.query(queryStr)) {

            familyDocumentByHousehold.put(fd.Household__c, fd.Id);
        }
        
        return familyDocumentByHousehold;
    }//End:getFamilyDocByHouseholdId
    
    /**
     * @description Singleton instance of the DocumentRequirementService.
     */
    public static DocumentRequirementService Instance {
        get {
            if (Instance == null) {
                Instance = new DocumentRequirementService();
            }
            return Instance;
        }
        private set;
    }
    
    public class Response {
        
        public Map<Id, Id> familyDocumentByHousehold { get; set; }
        public Map<Id, School_Document_Assignment__c> sdasByPFS  { get; set; }
        public List<School_Document_Assignment__c> sdasToInsert  { get; set; }
        public List<School_Document_Assignment__c> siblingsSDAsToInsert  { get; set; }
        public List<School_Document_Assignment__c> sdasToInsertFinal  {
            get {
                if( sdasToInsertFinal == null && sdasToInsert != null && siblingsSDAsToInsert != null) {
                    sdasToInsertFinal = sdasToInsert.clone();
                    sdasToInsertFinal.addAll(siblingsSDAsToInsert);
                }
                return sdasToInsertFinal;
            }
            private set;
        }
        public Integer totalNewSDAs {
            get {
                return sdasToInsertFinal != null ? sdasToInsertFinal.size() : 0;
            }
            private set;
        }
        
        public Response() {
            
            familyDocumentByHousehold = new Map<Id, Id>();
            sdasByPFS = new Map<Id, School_Document_Assignment__c>();
            sdasToInsert = new List<School_Document_Assignment__c>();
            siblingsSDAsToInsert = new List<School_Document_Assignment__c>();
        }
    }//End-Class:Response
    
    public class Request {
        
        public String docType { get; set; }
        public String docYear { get; set; }
        public List<Id> applicantIds { get; set; }
        public List<Id> pfsIds { get; set; }
        public List<Id> schoolIds { get; set; }
        public Set<Id> householdIds { get; set; }
        public List<School_PFS_Assignment__c> spas { get; set; }
        public Boolean shouldSendEmail { get; set; }
        public Boolean runDml { get; set; }
        public String source { get; set; }
        
        public Request(String documentType, String documentYear, Set<Id> folderIds, Boolean sendEmailAlert, Boolean dml) {
        
            ArgumentNullException.throwIfNull(documentType, DOC_TYPE_PARAM);
            ArgumentNullException.throwIfNull(documentYear, DOC_YEAR_PARAM);
            ArgumentNullException.throwIfNull(folderIds, FOLDER_IDS_PARAM);
            ArgumentNullException.throwIfNull(sendEmailAlert, SEND_EMAIL_ALERT_PARAM);
            ArgumentNullException.throwIfNull(dml, RUN_DML_PARAM);
            
            Init(documentType, documentYear, sendEmailAlert, dml);
            
            for(School_PFS_Assignment__c spa : [
                SELECT Id, Student_Folder__c, 
                Applicant__r.PFS__r.Parent_A_Household_ID__c, Applicant__r.PFS__r.Parent_A_Email__c,
                PFS_ID__c, School__c, Applicant__c, 
                   (SELECT Id 
                   FROM School_Document_Assignments__r 
                   WHERE Document_Year__c =:documentYear 
                   AND Document_Type__c =:documentType 
                   AND Deleted__c = false)
                FROM School_PFS_Assignment__c 
                WHERE Student_Folder__c IN: folderIds]) {
                
                if(!spa.School_Document_Assignments__r.isEmpty()) {
                    continue;//To avoid insert duplicated required documents.
                }
                
                spas.add(spa);
                applicantIds.add(spa.Applicant__c);
                pfsIds.add(spa.PFS_ID__c);
                schoolIds.add(spa.School__c);

                if (spa.Applicant__r.PFS__r.Parent_A_Household_ID__c != null ) {
                    householdIds.add(spa.Applicant__r.PFS__r.Parent_A_Household_ID__c);
                }
            }
        }
        
        public Request(String documentType, String documentYear, School_PFS_Assignment__c spa, Boolean sendEmailAlert, Boolean dml) {
            
            ArgumentNullException.throwIfNull(documentType, DOC_TYPE_PARAM);
            ArgumentNullException.throwIfNull(documentYear, DOC_YEAR_PARAM);
            ArgumentNullException.throwIfNull(spa, SPA_PARAM);
            ArgumentNullException.throwIfNull(sendEmailAlert, SEND_EMAIL_ALERT_PARAM);
            ArgumentNullException.throwIfNull(dml, RUN_DML_PARAM);
            
            Init(documentType, documentYear, sendEmailAlert, dml);
            
            applicantIds = new List<Id>{spa.Applicant__c};
            pfsIds = new List<Id>{spa.PFS_ID__c};
            schoolIds = new List<Id>{spa.School__c};
            householdIds = new Set<Id>();
            
            if (spa.Applicant__r.PFS__r.Parent_A_Household_ID__c != null) {
                householdIds.add(spa.Applicant__r.PFS__r.Parent_A_Household_ID__c);
            }
            
            spas = new List<School_PFS_Assignment__c>{spa};

        }
        
        private void Init(String documentType, String documentYear, Boolean sendEmailAlert, Boolean dml) {
            
            docType = documentType;
            docYear = documentYear;
            applicantIds = new List<Id>();
            pfsIds = new List<Id>();
            schoolIds = new List<Id>();
            householdIds = new Set<Id>();
            spas = new List<School_PFS_Assignment__c>();
            shouldSendEmail = sendEmailAlert;
            runDml = dml;
            source = '';
        }
    }//End-Class:Request
}