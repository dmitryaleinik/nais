@isTest
private class ArticleDispositionMappingSelectorTest
{
    
    @isTest 
    private static void getMapByArticleNumber_defaultData_expect185Record()
    {
        System.assertEquals(true, ArticleDispositionMappingSelector.Instance.getMapByArticleNumber().size() > 0);
    }

    @isTest
    private static void selectByArticleNumber_admExists_expectRecordReturned()
    {
        Article_Disposition_Mapping__c adm = ArticleDispositionMappingTestData.Instance.DefaultArticleDispositionMapping;

        Test.startTest();
            List<Article_Disposition_Mapping__c> admRecords = ArticleDispositionMappingSelector.newInstance()
                .selectByArticleNumber(new Set<String>{adm.Article_Number__c});
        Test.stopTest();

        System.assert(!admRecords.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(1, admRecords.size(), 'Expected there to be one adm record returned.');
        System.assertEquals(adm.Id, admRecords[0].Id, 'Expected the Ids of the adm records to match.');
    }

    @isTest
    private static void selectByArticleNumber_nullSet_expectArgumentNullException()
    {
        Article_Disposition_Mapping__c adm = ArticleDispositionMappingTestData.Instance.DefaultArticleDispositionMapping;

        Test.startTest();
            try {
                ArticleDispositionMappingSelector.newInstance().selectByArticleNumber(null);

                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, ArticleDispositionMappingSelector.ARTICLE_NUMBERS_PARAM);
            }
        Test.stopTest();
    }
}