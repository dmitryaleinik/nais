@isTest
private class SchoolFeeWaiversPurchaseControllerTest
{

    // [CH] NAIS-1766 Adding to help avoid Mixed DML Errors
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

    private static Contact parentA;
    private static PFS__c pfs1, pfs2, pfs3, pfs4;
    private static Application_Fee_Waiver__c afw1;

    /**
     *    NAIS-1877 [jb] Test methods associated with purchasing additional fee waivers
     *                added to this class
     */
    @isTest
    private static void testPurchaseSchoolFeeWaiverRelatedMethods() {
        // set up access org data
        User accessOrgPortalUser = setupAccessOrg();

        // Adding transaction fee line item to calculate costs
        Transaction_Annual_Settings__c fees = new Transaction_Annual_Settings__c(Name = '1 Pack - 2015', Product_Amount__c = 47.00,
                Product_Code__c = '123-100 Extra Fee Waivers - 1 waiver - 2015-16', Quantity__c = 1.0, Year__c = GlobalVariables.getCurrentAcademicYear().Name.split('-')[0]);
        insert fees;

        system.runAs(accessOrgPortalUser) {
            Test.setCurrentPage(Page.SchoolFeeWaiversPurchase);
            ApexPages.currentPage().getParameters().put('academicyearid', GlobalVariables.getCurrentAcademicYear().Id);

            SchoolFeeWaiversPurchaseController testCon = new SchoolFeeWaiversPurchaseController();

            testCon.initCommon();
            PageReference changeACYear = testCon.SchoolAcademicYearSelector_OnChange(false);
            System.assert(changeACYear != null);

            PageReference cancel = testCon.feeWaiverCancel();
            System.assert(cancel != null);

            SchoolFeeWaiversPurchaseController me = testCon.Me;

            Transaction_Annual_Settings__c currentAppFees = testCon.getCurrentApplicationFees();
            System.assertEquals(currentAppFees.Name, fees.Name);
            System.assertEquals(currentAppFees.Product_Amount__c, 47.00);
            System.assertEquals(currentAppFees.Product_Code__c, '123-100 Extra Fee Waivers - 1 waiver - 2015-16');
            System.assertEquals(currentAppFees.Quantity__c, 1);
            System.assertEquals(currentAppFees.Year__c, GlobalVariables.getCurrentAcademicYear().Name.split('-')[0]);

            testCon.feeWaiversPurchaseQuantity = 10;

            testCon.calculateWaiverPurchaseTotal();

            System.assertEquals(testCon.feeWaiverPurchaseTotal, 470.00);

            testCon.feeWaiversPurchaseQuantity = 101;

            testCon.calculateWaiverPurchaseTotal();

            System.assertEquals(testCon.feeWaiversPurchaseQuantityError, true);

            testCon.feeWaiversPurchaseQuantity = 10;

            PageReference pr = testCon.feeWaiverPurchase();

            System.assertNotEquals(pr, null);

            String opportunityId = pr.getParameters().get('id');

            System.assertNotEquals(opportunityId, null);

        }
    }

    private static User setupAccessOrg() {
        Account family = TestUtils.createAccount('Individual Account', RecordTypes.individualAccountTypeId, 5, false);
        Account school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, 5, false);
        Account school2 = TestUtils.createAccount('school2', RecordTypes.schoolAccountTypeId, 5, false);
        Account accessOrg = TestUtils.createAccount('accessOrg', RecordTypes.accessOrgAccountTypeId, 5, false);
        insert new List<Account> { family, school1, school2, accessOrg };

        Contact accessStaff = TestUtils.createContact('Access Org Staff', accessOrg.Id, RecordTypes.schoolStaffContactTypeId, false);
        accessStaff.Email = 'aos@access.org';

        parentA = TestUtils.createContact('Parent A', family.Id, RecordTypes.parentContactTypeId, false);
        Contact parentB = TestUtils.createContact('Parent B', family.Id, RecordTypes.parentContactTypeId, false);
        Contact parentC = TestUtils.createContact('Parent C', family.Id, RecordTypes.parentContactTypeId, false);
        Contact parentD = TestUtils.createContact('Parent D', family.Id, RecordTypes.parentContactTypeId, false);

        Contact student1 = TestUtils.createContact('Student 1', family.Id, RecordTypes.studentContactTypeId, false);
        Contact student2 = TestUtils.createContact('Student 2', family.Id, RecordTypes.studentContactTypeId, false);
        Contact student3 = TestUtils.createContact('Student 3', family.Id, RecordTypes.studentContactTypeId, false);

        insert new List<Contact> { accessStaff, parentA, parentB, parentC, parentD, student1, student2, student3 };

        User accessOrgPortalUser = TestUtils.createPortalUser(accessStaff.LastName, accessStaff.Email, 'aos', accessStaff.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);

        System.runAs(thisUser) {
            insert accessOrgPortalUser;
        }

        TestUtils.createAcademicYears();
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        return accessOrgPortalUser;
    }
}