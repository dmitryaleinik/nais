@isTest
private class BatchUpdateTuitionDependentsTest {
    @isTest
    private static void updateTuitionDependentsTest() {
        PFS__c pfs1 = PfsTestData.Instance.DefaultPFS;
        System.AssertEquals(null, pfs1.Tuition_Able_to_Pay_All_Dependents__c);
        Dependents__c dep = DependentsTestData.Instance.DefaultDependents;

        Test.startTest();
        dep.Dependent_1_Parent_Sources_Est__c = 1000;
        dep.Dependent_2_Parent_Sources_Est__c = 2000;
        update dep;

        String pfsQuery;
        pfsQuery = 'select Id, ( select Id,Dependent_1_Parent_Sources_Est__c,Dependent_2_Parent_Sources_Est__c,Dependent_3_Parent_Sources_Est__c, ';
        pfsQuery = pfsQuery +'Dependent_4_Parent_Sources_Est__c,Dependent_5_Parent_Sources_Est__c,Dependent_6_Parent_Sources_Est__c from Dependents__r) ';
        pfsQuery = pfsQuery +'from PFS__c';
        BatchUpdateTuitionDependents btd = new BatchUpdateTuitionDependents(pfsQuery);
        Database.executeBatch(btd);
        Test.stopTest();

        PFS__c pfs = [select Id, Tuition_Able_to_Pay_All_Dependents__c from PFS__c where Id = :pfs1.Id];
        System.AssertEquals(3000,pfs.Tuition_Able_to_Pay_All_Dependents__c);
    }
}