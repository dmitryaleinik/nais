/**
 * @description This class is used to create Account Settings records for unit tests.
 */
@isTest
public class AccountSettingsTestData extends SObjectTestData
{

    @testVisible private static final String ACCOUNT_SETTINGS_NAME = 'New Accounts';
    @testVisible private static final Decimal LAST_SSS_CODE = 200000;

    /**
     * @description Get the default values for the Account_Settings__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap()
    {
        return new Map<Schema.SObjectField,Object>
        {
                Account_Settings__c.name =>  ACCOUNT_SETTINGS_NAME,
                Account_Settings__c.Last_SSS_Code__c =>  LAST_SSS_CODE
        };
    }

    /**
     * @description Set the Last SSS Code field on the current Account Settings record.
     * @param lastSSSCode The last sss code to set on the  Account Settings.
     * @return The current working instance of AccountSettingsTestData.
     */
    public AccountSettingsTestData forLastSSSCode(Integer lastSSSCode)
    {
        return (AccountSettingsTestData) with(Account_Settings__c.Last_SSS_Code__c, lastSSSCode);
    }

    /**
     * @description Create the current working Account Settings record without resetting
     *              the stored values in this instance of AccountSettingsTestData.
     * @return A non-inserted Account_Settings__c record using the currently stored field values.
     */
    public Account_Settings__c createAccountSettingsWithoutReset()
    {
        return (Account_Settings__c)buildWithoutReset();
    }

    /**
     * @description The default Account_Settings__c record.
     */
    public Account_Settings__c DefaultAccountSettings
    {
        get
        {
            if (DefaultAccountSettings == null)
            {
                DefaultAccountSettings = createAccountSettingsWithoutReset();
                insert DefaultAccountSettings;
            }
            return DefaultAccountSettings;
        }
        private set;
    }

    /**
     * @description Get the Account_Settings__c SObjectType.
     * @return The Account_Settings__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType()
    {
        return Account_Settings__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static AccountSettingsTestData Instance
    {
        get
        {
            if (Instance == null)
            {
                Instance = new AccountSettingsTestData();
            }
            return Instance;
        }
        private set;
    }

    private AccountSettingsTestData() {}
}