/**
 * @description This class is used to create Case records for unit tests.
 */
@isTest
public class CaseTestData extends SObjectTestData {
    @testVisible private static final Id RECORD_TYPE_ID = RecordTypes.expClientCaseTypeId;

    /**
     * @description Get the default values for the Case object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Case.RecordTypeId => RECORD_TYPE_ID
        };
    }

    /**
     * @description Set the RecordTypeId field on the current Case record.
     * @param recordTypeId The Record Type Id to set on the Case.
     * @return The current working instance of CaseTestData.
     */
    public CaseTestData forRecordTypeId(Id recordTypeId) {
        return (CaseTestData) with(Case.RecordTypeId, recordTypeId);
    }

    /**
     * @description Set the Subject field on the current Case record.
     * @param subject The Subject to set on the Case.
     * @return The current working instance of CaseTestData.
     */
    public CaseTestData forSubject(String subject) {
        return (CaseTestData) with(Case.Subject, subject);
    }

    /**
     * @description Set the Description field on the current Case record.
     * @param description The Description to set on the Case.
     * @return The current working instance of CaseTestData.
     */
    public CaseTestData forDescription(String description) {
        return (CaseTestData) with(Case.Description, description);
    }

    /**
     * @description Set the ContactId field on the current Case record.
     * @param contactId The Contact Id to set on the Case.
     * @return The current working instance of CaseTestData.
     */
    public CaseTestData forContactId(Id contactId) {
        return (CaseTestData) with(Case.ContactId, contactId);
    }

    /**
     * @description Set the PFS__c field on the current Case record.
     * @param pfsId The Pfs Id to set on the Case.
     * @return The current working instance of CaseTestData.
     */
    public CaseTestData forPfsId(Id pfsId) {
        return (CaseTestData) with(Case.PFS__c, pfsId);
    }

    /**
     * @description Set the AccountId field on the current Case record.
     * @param accountId The Account Id to set on the Case.
     * @return The current working instance of CaseTestData.
     */
    public CaseTestData forAccountId(Id accountId)
    {
        return (CaseTestData) with(Case.AccountId, accountId);
    }

    /**
     * @description Insert the current working Case record.
     * @return The currently operated upon Case record.
     */
    public Case insertCase() {
        return (Case)insertRecord();
    }

    /**
     * @description Create the current working Case record without resetting
     *             the stored values in this instance of CaseTestData.
     * @return A non-inserted Case record using the currently stored field
     *             values.
     */
    public Case createCaseWithoutReset() {
        return (Case)buildWithoutReset();
    }

    /**
     * @description Create the current working Case record.
     * @return The currently operated upon Case record.
     */
    public Case create() {
        return (Case)super.buildWithReset();
    }

    /**
     * @description The default Case record.
     */
    public Case DefaultCase {
        get {
            if (DefaultCase == null) {
                DefaultCase = createCaseWithoutReset();
                insert DefaultCase;
            }
            return DefaultCase;
        }
        private set;
    }

    /**
     * @description Get the Case SObjectType.
     * @return The Case SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Case.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static CaseTestData Instance {
        get {
            if (Instance == null) {
                Instance = new CaseTestData();
            }
            return Instance;
        }
        private set;
    }

    private CaseTestData() { }
}