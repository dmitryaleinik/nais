/**
 * @description Controls processing payments with the available payment processor
 **/
public without sharing class PaymentProcessor {
    public static final String UNEXPECTED_ERROR = 'An unexpected error occurred processing the payment.';

    @testVisible private static final String PAYMENT_PROCESSOR_NAME = 'NAIS';
    @testVisible private static final String NO_PAYMENT_PROVIDER_FOUND = 'No Payment Provider record has been found.';
    @testVisible private static final String NO_PAYMENT_PROCESSOR_FOUND = 'No Payment Processor record has been found.';
    @testVisible private static final String PAYMENT_PROVIDER_CLASS_DOES_NOT_EXIST =
            'The Payment Provider class does not exist.';
    @testVisible private static final String PAYMENT_PROVIDER_CLASS_NOT_IPAYMENTPROVIDER =
            'The Payment Provider class is not of type IPaymentProvider.';

    private static Payment_Processor__mdt paymentProcessorRecord;
    private static Payment_Provider__mdt paymentProviderRecord;

    /**
     * @description The Payment Processor PFS Application Fee Tracker.
     */
    public static String PfsApplicationFeeTracker {
        get {
            return getPaymentProcessor().PFS_Application_Fee_Tracker__c;
        }
    }

    /**
     * @description The PFS Application Fee Item Description.
     */
    public static String PfsApplicationFeeItemDescription {
        get {
            return getPaymentProcessor().PFS_Application_Fee_Item_Description__c;
        }
    }

    /**
     * @description The Credit Card Statement Descriptor.
     */
    public static String CreditCardStatementDescriptor {
        get {
            return getPaymentProcessor().Credit_Card_Statement_Descriptor__c;
        }
    }

    /**
     * @description Primary debug flag.
     */
    public static Boolean CustomDebug {
        get {
            return getPaymentProviderRecord().Custom_Debug__c == true;
        }
    }

    /**
     * @description Secondary debug flag.
     */
    public static Boolean CustomDebug2 {
        get {
            return getPaymentProviderRecord().Custom_Debug_2__c == true;
        }
    }

    /**
     * @description The email address to use as reply-to for receipt emails.
     */
    public static String ReceiptOrgWideEmailAddress {
        get {
            return getPaymentProcessor().Receipt_Org_Wide_Email_Address__c;
        }
    }

    /**
     * @description The subject to use for receipt emails.
     */
    public static String ReceiptEmailSubject {
        get {
            return getPaymentProcessor().Receipt_Email_Subject__c;
        }
    }

    /**
     * @description Determine whether the payment processor is in test mode or not.
     *              A null or false value will return true, otherwise false.
     */
    public static Boolean InTestMode {
        get {
            return (getPaymentProviderRecord().Enable_Live_Transactions__c == null ||
                    getPaymentProviderRecord().Enable_Live_Transactions__c == false);
        }
    }

    /**
     * @description The billing country select options for display on
     *              visualforce pages.
     */
    public static List<SelectOption> getBillingCountrySelectOptions() {
        return getPaymentProvider().getBillingCountrySelectOptions();
    }

    /**
     * @description The billing US State select options for display on
     *              visualforce pages.
     */
    public static List<SelectOption> getBillingStateUSSelectOptions() {
        return getPaymentProvider().getBillingStateUSSelectOptions();
    }

    /**
     * @description The Month expiration select options for display on
     *              visualforce pages.
     */
    public static List<SelectOption> getExpireMonthSelectOptions() {
        return getPaymentProvider().getExpireMonthSelectOptions();
    }

    /**
     * @description The Year expiration select options for display on
     *              visualforce pages.
     */
    public static List<SelectOption> getExpireYearSelectOptions() {
        return getPaymentProvider().getExpireYearSelectOptions();
    }

    /**
     * @description The Account type select options for display on
     *              visualforce pages.
     */
    public static List<SelectOption> getAccountTypeSelectOptions() {
        return getPaymentProvider().getAccountTypeSelectOptions();
    }

    /**
     * @description The Country code associated with a country name.
     */
    public static String getCountryCode(String countryName) {
        return getPaymentProvider().getCountryCode(countryName);
    }

    /**
     * @description The Default Country code to be used on orders.
     */
    public static String getDefaultCountryCode() {
        return getPaymentProvider().getDefaultCountryCode();
    }

    /**
     * @description The Country name associated with a country code.
     */
    public static String getCountryName(String countryCode) {
        return getPaymentProvider().getCountryName(countryCode);
    }

    /**
     * @description The Account type label associated with an Account type value.
     */
    public static String getAccountTypeLabel(String accountTypeValue) {
        return getPaymentProvider().getAccountTypeLabel(accountTypeValue);
    }

    /**
     * @description Process the preparation for the payment or refund transaction
     *              to the provider.
     * @param request The PaymentProcessorRequest to send to the Payment Provider.
     * @return A PaymentResult with the outcome of the processing.
     */
    public static PaymentResult processBefore(PaymentProcessorRequest request) {
        IPaymentProvider provider = getPaymentProvider();
        PaymentResult result = new PaymentResult();

        result = provider.processBefore(request);

        return result;
    }

    /**
     * @description Facade to process payments and refunds via the currently
     *              active payment provider.
     * @param request The request to process either a refund or payment.
     * @return A PaymentResult with the outcome of the request.
     */
    public static PaymentResult process(PaymentProcessorRequest request) {
        IPaymentProvider provider = getPaymentProvider();
        PaymentResult result = new PaymentResult();

        result = provider.process(request);

        return result;
    }

    /**
     * @description Exposing the currently active Payment Provider to allow
     *              access for error reporting.
     * @return An implementation of the IPaymentProvider class.
     */
    public static IPaymentProvider getPaymentProvider() {
        String paymentProviderClassName = getPaymentProviderRecord().Payment_Provider_Class__c;
        if (paymentProviderClassName == null || Type.forName(paymentProviderClassName) == null) {
            throw new ConfigurationException(PAYMENT_PROVIDER_CLASS_DOES_NOT_EXIST);
        }

        Object instance = Type.forName(paymentProviderClassName).newInstance();

        if (!(instance instanceof IPaymentProvider)) {
            throw new ConfigurationException(PAYMENT_PROVIDER_CLASS_NOT_IPAYMENTPROVIDER);
        }

        return (IPaymentProvider)instance;
    }

    private static Payment_Processor__mdt getPaymentProcessor() {
        if (paymentProcessorRecord == null) {
            List<Payment_Processor__mdt> processors = PaymentProcessorSelector.Instance
                    .selectByName(new Set<String> { PAYMENT_PROCESSOR_NAME });

            if (processors == null || processors.isEmpty()) {
                throw new ConfigurationException(NO_PAYMENT_PROCESSOR_FOUND);
            }

            paymentProcessorRecord = processors[0];
        }
        return paymentProcessorRecord;
    }

    private static Payment_Provider__mdt getPaymentProviderRecord() {
        if (paymentProviderRecord == null) {
            String providerName = getPaymentProcessor().Payment_Provider__c;
            if (providerName == null) {
                throw new ConfigurationException(NO_PAYMENT_PROVIDER_FOUND);
            }

            List<Payment_Provider__mdt> paymentProviders = PaymentProviderSelector.Instance
                    .selectByName(new Set<String> { providerName });
            if (paymentProviders == null || paymentProviders.isEmpty()) {
                throw new ConfigurationException(NO_PAYMENT_PROVIDER_FOUND);
            }
            paymentProviderRecord = paymentProviders[0];
        }
        return paymentProviderRecord;
    }
}