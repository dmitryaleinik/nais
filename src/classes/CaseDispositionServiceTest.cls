@isTest
private class CaseDispositionServiceTest {
    
    private static User currentUser = [Select Id from User where Id = :UserInfo.getUserId()];
    
    private static void SetupCase1(Id recordTypeId) {
        
        currentUser.UserPermissionsKnowledgeUser = true;
        update currentUser;
        
        Contact parentA = ContactTestData.Instance.asParent().insertContact();
        Case case1;

        System.runAs( currentUser) {
            case1 = CaseTestData.Instance
                .forRecordTypeId(recordTypeId)
                .forSubject('Test Case 1')
                .forDescription('Test description')
                .createCaseWithoutReset();
            case1.Type = 'Documents';
            case1.Status = 'In Progress';
            case1.ContactId = parentA.Id;
            insert case1;
            
            Integer numberOfArticles = 15;
            List<Knowledge__kav> articles = KnowledgeAVTestData.Instance.insertKnowledgeAVs(numberOfArticles);

            // Create articles with the default category.
            String dataCategoryName = 'Pre_Payment_FAQ';
            List<Knowledge__DataCategorySelection> categories = new List<Knowledge__DataCategorySelection>();
            for (Integer i=0; i<numberOfArticles; i++) {
                categories.add(KnowledgeDCSTestData.Instance
                    .forDataCategoryName(dataCategoryName)
                    .forParentId(articles[i].Id).create());
            }
            insert categories;
            
            //This line needs to be right before publish the articles, to avoid exception on CaseArticle insert with:
            //System.DmlException: Insert failed. First exception on row 0; first error: FIELD_INTEGRITY_EXCEPTION, Article ID: id value of incorrect type"
            Knowledge__kav article = [SELECT Id, KnowledgeArticleId, ArticleNumber FROM Knowledge__kav WHERE Id =: articles[0].Id LIMIT 1];
            
            //Publish the articles
            articles = KnowledgeTestHelper.getKnowledgeArticleByIds(articles);
            KnowledgeTestHelper.publishArticles(articles);
           
            //Add a new metadata item to Article_Disposition_Mapping__c, for testing purposes
            Map<String, Article_Disposition_Mapping__c> metadata = ArticleDispositionMappingSelector.Instance
                .getMapByArticleNumber();
                
            Article_Disposition_Mapping__c testArticle = new Article_Disposition_Mapping__c(
                Title__c ='Uploading Scans or Pictures (JPGs) of 1040 and Other Documents', 
                Visible_In_Public_Knowledge_Base__c=  true, 
                Visible_to_Customer__c=   true, 
                Article_Number__c=    article.ArticleNumber, 
                Type__c=  'Documents' , 
                Disposition__c=   'Document Question' , 
                Sub_Category__c=  'How to Upload Documents');
            
            ArticleDispositionMappingSelector.Instance.mapByArticleNumber.put(article.ArticleNumber, testArticle);
            
            //Attach an article to the case
            CaseArticle ca = new CaseArticle();
            ca.CaseId = case1.Id;
            ca.KnowledgeArticleId = article.KnowledgeArticleId;
            insert ca;
        }
        
        List<Case> cases = new List<Case>([
            SELECT id, Type, Disposition__c, Sub_category__c, RecordTypeId  
            FROM Case WHERE Id =: case1.Id]);
        
        Test.StartTest();
            
            update CaseDispositionService.Instance.setDispositionBasedOnArticles(cases);
            
            Case caseResult = [SELECT id, Type, Disposition__c, Sub_category__c FROM Case WHERE Id =: case1.Id LIMIT 1];
            
            System.AssertNotEquals(null, caseResult.Type);
            
            System.AssertNotEquals(null, caseResult.Disposition__c);
            
            System.AssertNotEquals(null, caseResult.Sub_category__c);
        Test.StopTest();
    }
    
    @isTest 
    private static void setDispositionBasedOnArticles_parentCallCase_caseWithAttachedArticle_expectCaseFieldsMergedWithArticleFields() {
        
        SetupCase1(RecordTypes.parentCallCaseTypeId);
    }
    
    @isTest 
    private static void setDispositionBasedOnArticles_familyEmailCase_caseWithAttachedArticle_expectCaseFieldsMergedWithArticleFields() {
        
        SetupCase1(RecordTypes.familyEmailCaseTypeId);
    }
    
    @isTest 
    private static void setDispositionBasedOnArticles_familyLiveAgentCase_caseWithAttachedArticle_expectCaseFieldsMergedWithArticleFields() {
        
        SetupCase1(RecordTypes.familyLiveAgentCaseTypeId);
    }
    
    @isTest 
    private static void setDispositionBasedOnArticles_familyPortalCase_caseWithAttachedArticle_expectCaseFieldsMergedWithArticleFields() {
        
        SetupCase1(RecordTypes.familyPortalCaseTypeId);
    }
}