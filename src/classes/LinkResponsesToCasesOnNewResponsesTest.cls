@IsTest
private class LinkResponsesToCasesOnNewResponsesTest
{

    @isTest
    private static void runTests()
    {
        String chatKey = '00001';
    	FeatureToggles.setToggle('Link_Survey_Responses_To_Cases__c', true);
        //Creating the LiveAgentTranscript

        User feedbackUser = UserTestData.Instance.DefaultUser;
        LiveChatVisitor liveChatVisitor = new LiveChatVisitor();
        insert liveChatVisitor;
        Case newCase = CaseTestData.Instance.DefaultCase;

        LiveChatTranscript l = new LiveChatTranscript (CaseID = newCase.Id, ChatKey = chatKey, LiveChatVisitorID = liveChatVisitor.Id);
        insert l;
            
        //Creating the Feedback Survey Response
        Feedback_Survey_Response__c f = new Feedback_Survey_Response__c (Name = 'Test', OwnerID = feedbackUser.Id, ChatKey__c = chatKey);
        insert f;
    }
}