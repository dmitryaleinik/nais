/**
 * FamilyHelpSearchResultsControllerTest.cls
 *
 * @description: Test class for FamilyHelpSearchResultsControllerTest.cls using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Mike Havrila @ Presence PG
 */

@isTest (seeAllData = false)
private class FamilyHelpSearchResultsControllerTest {
    private static List<Knowledge__kav> articles;
    private static User testUser;
    static PFS__c pfs;

    /* test data setup */
    static {
        testUser = CurrentUser.getCurrentUser();
        testUser.UserPermissionsKnowledgeUser = true;
        update testUser;

        System.runAs( testUser) {
            pfs = PfsTestData.Instance.DefaultPfs;

            // Setup article creation.
            articles = KnowledgeAVTestData.Instance.asConcatenatedArticleBody('FamilySearchValue').insertKnowledgeAVs(14);

            // Create articles with the default category.
            List<Knowledge__DataCategorySelection> categories = new List<Knowledge__DataCategorySelection>();

            for (Knowledge__kav article : articles) {
                categories.add(KnowledgeDCSTestData.Instance.forParentId(article.Id).create());
            }

            insert categories;

            articles = KnowledgeTestHelper.getKnowledgeArticleByIds(articles);

            // Publish the Articles.
            KnowledgeTestHelper.publishArticles( articles);
        }
    }


    private static Id[] getFixedSearchResults(Integer size) {
        Id[] fixedSearchResults = new Id[] { };

        for (Integer i = 0; i < size; i++) {
            fixedSearchResults.add(articles[i].Id);
        }

        return fixedSearchResults;
    }

    /* negative test cases */

    //
    @isTest private static void initModelAndView_firstPage_successInit() {

        // Arrange
        // Created in the static block

        // Act
        System.runAs( testUser) {
            Test.setFixedSearchResults(getFixedSearchResults(1));

            PageReference searchPage = Page.FamilyHelpSearchResults;
            Test.setCurrentPage(searchPage);
            searchPage.getParameters().put('id', pfs.Id);
            searchPage.getParameters().put('searchq', 'FamilySearchValue');
            searchPage.getParameters().put('page', '1');

            Test.startTest();
                FamilyTemplateController template = new FamilyTemplateController();
                FamilyHelpSearchResultsController results = new FamilyHelpSearchResultsController( template);
            Test.stopTest();

            // Assert
            System.assertEquals(1, results.getTotalRecords(), '1 Record should be returned via the search');
        }
    }

    @isTest private static void initModelAndView_allArticlesShown_successPageOne() {

        // Arrange
        // Setup SOSL Fixed Results.
        // If we don't the sosl search will return an empty list
        Test.setFixedSearchResults(getFixedSearchResults(articles.size()));

        // Act
        System.runAs( testUser) {

            PageReference searchPage = Page.FamilyHelpSearchResults;
            Test.setCurrentPage(searchPage);
            searchPage.getParameters().put('id', pfs.Id);
            searchPage.getParameters().put('searchq', 'FamilySearchValue');
            searchPage.getParameters().put('page', '1');

            Test.startTest();
                FamilyTemplateController template = new FamilyTemplateController();
                FamilyHelpSearchResultsController results = new FamilyHelpSearchResultsController( template);
            Test.stopTest();

            // Assert
            System.assertEquals(articles.size(), results.getTotalRecords(), 'List of Articles size and total records do not match');
            System.assertEquals(2, results.getPageList().size(), 'Expected total pages and actual pages do not match');
            System.assertEquals(1, results.getCurrentPageNumber(), 'Expected Current Page number and actual page number do not match');

            // Test Pagination
            System.assert(!results.getPrevRequired(), 'Previous page will be rendered, it should not be since we are on page 1');
            System.assert(results.getNextRequired(), 'The next page is not available');
        }
    }

    @isTest private static void initModelAndView_nextPage_successPageTwo() {

        // Arrange
        String nextPageString = '/apex/familyhelpsearchresults?id=' + pfs.Id + '&page=2&searchq=FamilySearchValue';

        Test.setFixedSearchResults(getFixedSearchResults(articles.size()));

        // Act
        System.runAs( testUser) {

            PageReference searchPage = Page.FamilyHelpSearchResults;
            Test.setCurrentPage(searchPage);
            searchPage.getParameters().put('id', pfs.Id);
            searchPage.getParameters().put('searchq', 'FamilySearchValue');
            searchPage.getParameters().put('page', '1');

            Test.startTest();
                FamilyTemplateController template = new FamilyTemplateController();
                FamilyHelpSearchResultsController results = new FamilyHelpSearchResultsController( template);
                PageReference nextPage = results.next();
            Test.stopTest();

            // Assert
            System.assertEquals(articles.size(), results.getTotalRecords(), 'List of Articles size and total records do not match');
            System.assertEquals(2, results.getPageList().size(), 'Expected total pages and actual pages do not match');
            System.assertEquals(2, results.getCurrentPageNumber(), 'Expected Current Page number and actual page number do not match');

            // Now Test the page resfs
            System.assertEquals(nextPageString, nextPage.getUrl(), 'The initModelAndView_nextPage_successPageTwo URL does not match the exepected Value');
            System.assertEquals(pfs.Id, nextPage.getParameters().get('id'), 'The pfsId is not the same one from the init of the controller');
            System.assertEquals('2', nextPage.getParameters().get('page'), 'The Page query string was expected at 2.');
            System.assertEquals('FamilySearchValue', nextPage.getParameters().get('searchq'), 'The searchq query string parameter was incorrect');

            // Test Pagination
            System.assert(results.getPrevRequired(),'Previous Value should be availble when on Page 2');
            System.assert(!results.getNextRequired(),'Next Value should be availble when on Page 2');
        }
    }

    @isTest private static void initModelAndView_lastPage_successLastPage() {

        // Arrange
        String nextPageString = '/apex/familyhelpsearchresults?id=' + pfs.Id + '&page=2&searchq=FamilySearchValue';

        Test.setFixedSearchResults(getFixedSearchResults(articles.size()));

        // Act
        System.runAs( testUser) {

            PageReference searchPage = Page.FamilyHelpSearchResults;
            Test.setCurrentPage(searchPage);
            searchPage.getParameters().put('id', pfs.Id);
            searchPage.getParameters().put('searchq', 'FamilySearchValue');
            searchPage.getParameters().put('page', '2');

            Test.startTest();
                FamilyTemplateController template = new FamilyTemplateController();
                FamilyHelpSearchResultsController results = new FamilyHelpSearchResultsController( template);

                PageReference nextPage = results.next();

            Test.stopTest();

            // Assert
            System.assertEquals(articles.size(), results.getTotalRecords(), 'List of Articles size and total records do not match');
            System.assertEquals(2, results.getPageList().size(), 'Expected total pages and actual pages do not match');
            System.assertEquals(2, results.getCurrentPageNumber(), 'Expected Current Page number and actual page number do not match');

            // Now Test the page resfs
            System.assertEquals(nextPageString, nextPage.getUrl(), 'The nextPage URL does not match the exepected Value');
            System.assertEquals(pfs.Id, nextPage.getParameters().get('id'), 'The pfsId is not the same one from the init of the controller');
            System.assertEquals('2', nextPage.getParameters().get('page'), 'The Page query string was expected at 2.');
            System.assertEquals('FamilySearchValue', nextPage.getParameters().get('searchq'), 'The searchq query string parameter was incorrect');

            // Test Pagination
            System.assert(results.getPrevRequired(),'Previous Value should be availble when on current page');
            System.assert(!results.getNextRequired(),'Next Value should not be availble when on the current page');
        }
    }

    @isTest private static void initModelAndView_previousPage_successPageOne() {

        // Arrange
        String previousPageString = '/apex/familyhelpsearchresults?id=' + pfs.Id + '&page=1&searchq=FamilySearchValue';

        Test.setFixedSearchResults(getFixedSearchResults(articles.size()));

        // Act
        System.runAs( testUser) {

            PageReference searchPage = Page.FamilyHelpSearchResults;
            Test.setCurrentPage(searchPage);
            searchPage.getParameters().put('id', pfs.Id);
            searchPage.getParameters().put('searchq', 'FamilySearchValue');
            searchPage.getParameters().put('page', '2');

            Test.startTest();
                FamilyTemplateController template = new FamilyTemplateController();
                FamilyHelpSearchResultsController results = new FamilyHelpSearchResultsController( template);

                String returnURL = results.getReturnUrl();
                PageReference previousPage = results.previous();
            Test.stopTest();

            // Assert
            System.assertEquals(articles.size(), results.getTotalRecords(), 'List of Articles size and total records do not match');
            System.assertEquals(2, results.getPageList().size(), 'Expected total pages and actual pages do not match');
            System.assertEquals(1, results.getCurrentPageNumber(), 'Expected Current Page number and actual page number do not match');

            // Now Test the page resfs
            System.assertEquals(previousPageString, previousPage.getUrl(), 'The nextPage URL does not match the exepected Value');
            System.assertEquals(pfs.Id, previousPage.getParameters().get('id'), 'The pfsId is not the same one from the init of the controller');
            System.assertEquals('1', previousPage.getParameters().get('page'), 'The Page query string was expected at 1.');
            System.assertEquals('FamilySearchValue', previousPage.getParameters().get('searchq'), 'The searchq query string parameter was incorrect');

            // Test Pagination
            System.assert(!results.getPrevRequired(),'Previous Value should be availble when on current page');
            System.assert(results.getNextRequired(),'Next Value should not be availble when on the current page');

        }
    }

    @isTest private static void initModelAndView_verifyMaxResults_maxResultsReturned() {

        // Arrange
        String pageResultsString = '11 - ' +  articles.size();

        Test.setFixedSearchResults(getFixedSearchResults(articles.size()));

        // Act
        System.runAs( testUser) {

            PageReference searchPage = Page.FamilyHelpSearchResults;
            Test.setCurrentPage(searchPage);
            searchPage.getParameters().put('id', pfs.Id);
            searchPage.getParameters().put('searchq', 'FamilySearchValue');
            searchPage.getParameters().put('page', '2');

            Test.startTest();
                FamilyTemplateController template = new FamilyTemplateController();
                FamilyHelpSearchResultsController results = new FamilyHelpSearchResultsController( template);

            Test.stopTest();

            // Assert
            System.assertEquals(articles.size(), results.getTotalRecords(), 'List of Articles size and total records do not match');
            System.assertEquals(2, results.getPageList().size(), 'Expected total pages and actual pages do not match');
            System.assertEquals(2, results.getCurrentPageNumber(), 'Expected Current Page number and actual page number do not match');
            System.assertEquals(pageResultsString, results.getDisplayResults(), 'Display Results do not match for page 2.');

            // Test Pagination
            System.assert(results.getPrevRequired(),'Previous Value should be availble when on current page');
            System.assert(!results.getNextRequired(),'Next Value should not be availble when on the current page');

        }
    }

    @isTest private static void initModelAndView_skipToPage_pageThree() {

        // Arrange
        String jumpToPageString = '/apex/familyhelpsearchresults?id=' + pfs.Id + '&page=3&searchq=FamilySearchValue';

        Test.setFixedSearchResults(getFixedSearchResults(articles.size()));

        // Act
        System.runAs( testUser) {

            PageReference searchPage = Page.FamilyHelpSearchResults;
            Test.setCurrentPage(searchPage);
            searchPage.getParameters().put('id', pfs.Id);
            searchPage.getParameters().put('searchq', 'FamilySearchValue');
            searchPage.getParameters().put('page', '1');

            Test.startTest();
                FamilyTemplateController template = new FamilyTemplateController();
                FamilyHelpSearchResultsController results = new FamilyHelpSearchResultsController( template);
                results.jumpToPage = '3';
                PageReference nextPage = results.skipToPage();
            Test.stopTest();

            // Assert
            System.assertEquals(articles.size(), results.getTotalRecords(), 'List of Articles size and total records do not match');
            System.assertEquals(2, results.getPageList().size(), 'Expected total pages and actual pages do not match');
            System.assertEquals(1, results.getCurrentPageNumber(), 'Expected Current Page number and actual page number do not match');
            System.assertEquals('1 - 10', results.getDisplayResults(), 'Display Results do not match for page 1.');
            // Now Test the page resfs
            System.assertEquals(jumpToPageString, nextPage.getUrl(), 'The nextPage URL does not match the exepected Value');
            System.assertEquals(pfs.Id, nextPage.getParameters().get('id'), 'The pfsId is not the same one from the init of the controller');
            System.assertEquals('3', nextPage.getParameters().get('page'), 'The Page query string was expected at 3.');
            System.assertEquals('FamilySearchValue', nextPage.getParameters().get('searchq'), 'The searchq query string parameter was incorrect');

            // Test Pagination
            System.assert(!results.getPrevRequired(),'Previous Value should not be availble when on current page');
            System.assert(results.getNextRequired(),'Next Value should be availble when on the current page');
        }
    }

    @isTest private static void initModelAndView_noSearchString_failedInit() {

        // Arrange

        // Act
        System.runAs( testUser) {

            PageReference searchPage = Page.FamilyHelpSearchResults;
            Test.setCurrentPage(searchPage);
            searchPage.getParameters().put('id', pfs.Id);

            Test.startTest();
                FamilyTemplateController template = new FamilyTemplateController();
                FamilyHelpSearchResultsController results = new FamilyHelpSearchResultsController( template);
            Test.stopTest();

            System.assertEquals(1, results.getCurrentPageNumber(), 'Expected Current Page number and actual page number do not match');
            System.assertEquals(1, results.getTotalRecords(), 'Expected Total Reocrds and actual total records do not match');
        }
    }
}