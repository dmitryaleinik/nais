/**
 * Class: FamilyAppPrintPFSPDFController
 *
 * Copyright (C) 2013 NAIS
 *
 * Purpose: Retrieves the appropriate PFS information for display in a PDF document
 *
 * Where Referenced:
 *   Family Portal - FamilyAppViewPFSPDF Component
 *
 * Change History:
 *
 * Developer           Date        JIRA Ref     Description
 * ---------------------------------------------------------------------------------------
 * Brian Clift         09/11/13    NAIS-526     Initial development
 *
 * exponent partners
 * 901 Mission Street, Suite 105
 * San Francisco, CA  94103
 * +1 (800) 918-2917
 *
 */

public class FamilyAppPrintPFSPDFController {
    public PFS__c pfs { get; set; }
    public String academicYear { get { if (academicYear == null) { setAcademicYear(); }  return academicYear; }  private set; }
    public String priorTaxYear { get { if ( priorTaxYear == null) { setAcademicYear(); } return priorTaxYear; }  private set; }
    public String priorTaxYear2 { get { if ( priorTaxYear2 == null) { setAcademicYear(); } return priorTaxYear2; }  private set; }
    public String currentTaxYear { get { if ( currentTaxYear == null) { setAcademicYear(); } return currentTaxYear; } private set; }
    public ApplicationUtils appUtils { get { if (appUtils == null) { retrieveApplicationUtils(); } return appUtils; } private set; }
    public Map<Id, List<School_PFS_Assignment__c>> applicationSchoolAssignmentMap {
        get {
            if (applicationSchoolAssignmentMap == null) {
                    applicationSchoolAssignmentMap = bulkController.bulkApplicationSchoolAssignmentMap;
                    for(Applicant__c a : pfs.Applicants__r) {
                        if( !applicationSchoolAssignmentMap.containsKey(a.Id) ) {
                            applicationSchoolAssignmentMap.put(a.Id, new List<School_PFS_Assignment__c>());
                        }
                    }
            }
            return applicationSchoolAssignmentMap;
        }
        private set;
    }
    public List<String> sectionIds { get { if (sectionIds == null) sectionIds = new List<String>{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'}; return sectionIds; } private set; }
    // SFP-606
    public Set<String> schoolIds { get { if (schoolIds == null) { setSchoolIds(); }  return schoolIds; } private set; }
    public Map<String, Boolean> customQuestionsRequestedBySchools { get { if (customQuestionsRequestedBySchools == null) { setCustomQuestionsRequestedBySchools(); } return customQuestionsRequestedBySchools; } private set; }
    public Boolean displayCustomQuestions {get { if (displayCustomQuestions == null) { setDisplayCustomQuestions(); } return displayCustomQuestions; } private set; }
    public Boolean isSchoolPortalUser { get {if (isSchoolPortalUser == null) { isSchoolPortalUser = GlobalVariables.isSchoolPortalProfile(UserInfo.getProfileId()); } return isSchoolPortalUser; } private set; }
    public String academicYearId {get { if (academicYearId == null) { academicYearId = GlobalVariables.getAcademicYearByName(academicYear).Id; } return academicYearId; } private set; }
    // Reference to Bulk PDFPDF Controller, passed by page when generating bulk view PFSs
    // Use information from controller to generate a static Map of applicant School Assignments, by retrieving the list of PFS ids from the bulk controller
    public FamilyAppViewPFSPDFBulkController bulkController{ get; set; }
    private static Boolean debugOn = false;

    //SFP-210, [G.S]
    public Map<Id, List<Business_farm__c>> businessFarmMap
    {
        get{
            if (businessFarmMap == null) {
                businessFarmMap = bulkController != null ? bulkController.Business.businessFarmMap : new Map<Id, List<Business_farm__c>>();
            }
            return businessFarmMap;
        }
        private set;
    }

    public Academic_Year__c currentPFSYear {
        get{
            if(currentPFSYear == null) {
                 currentPFSYear = bulkController.mapAcademicYears.get(pfs.Academic_Year_Picklist__c);
            }
            return currentPFSYear;
        }
        set;
    }

    public String currentYearColumnName {
        get{
            if(String.isBlank(currentYearColumnName)){
                String retVal = currentPFSYear.Name.subString(0,5);
                retVal += currentPFSYear.Name.subString(7,9);
                currentYearColumnName = retVal;
            }
            return currentYearColumnName;
        }
        set;
    }

    public String previousYearColumnName {
        get{
            if(String.isBlank(previousYearColumnName)){
                Academic_Year__c prevYear = GlobalVariables.getPreviousAcademicYear(currentPFSYear.Id);
                if(prevYear != null)
                    previousYearColumnName = prevYear.name;
            }
            return previousYearColumnName;
        }
        set;
    }

    public FamilyAppPrintPFSPDFController() {
        debugOn = ApexPages.currentPage().getParameters().get('debug') == '1';
    }

    private void setAcademicYear() {
        if (pfs!= null && pfs.Academic_Year_Picklist__c != null) {

            if( bulkController != null && bulkController.mapAcademicYears.containsKey(pfs.Academic_Year_Picklist__c) ) {

                Academic_Year__c theAcademicYear = bulkController.mapAcademicYears.get(pfs.Academic_Year_Picklist__c);
                academicYear = theAcademicYear.Name;

                if (theAcademicYear.Start_Date__c != null) {
                    // format tax year labels
                    priorTaxYear2 = theAcademicYear.Start_Date__c.addYears(-2).year().format().replaceAll(',','');
                    priorTaxYear = theAcademicYear.Start_Date__c.addYears(-1).year().format().replaceAll(',','');
                    currentTaxYear = theAcademicYear.Start_Date__c.year().format().replaceAll(',','');
                }
            }
        }
    }

    private static Map<String, ApplicationUtils> applicationUtilsMap = new Map<String, ApplicationUtils>();

    private void retrieveApplicationUtils() {
        if (pfs != null && pfs.Academic_Year_Picklist__c != null) {
            if (!applicationUtilsMap.containsKey(pfs.Academic_Year_Picklist__c)) {
                //SFP-140, [G.S]
                if(businessfarmMap != null && businessfarmMap.size() > 0 && businessFarmMap.get(pfs.Id) != null && businessFarmMap.get(pfs.Id).size() > 0){
                    applicationUtilsMap.put(pfs.Academic_Year_Picklist__c, new ApplicationUtils(UserInfo.getLanguage(), pfs,businessFarmMap.get(pfs.Id).get(0), 'PFSView'));
                }
                else{
                    applicationUtilsMap.put(pfs.Academic_Year_Picklist__c, new ApplicationUtils(UserInfo.getLanguage(), pfs, 'PFSView'));
                }
            }

            appUtils = applicationUtilsMap.get(pfs.Academic_Year_Picklist__c);
        }
    }

    /**
     *  SFP-606 conditional display of questions requested by schools applied too
     *  - modify to use current users' annual settings for questions display when printed from School Portal
     *    or use subscribed schools annual settings when in family portal (instead of currentSchoolAnnualSettings)
     *    which ultimately returns a 'dunny' annual settings record based on the family-portal-user's account id. [8.4.16 jB]
     */

    private void setCustomQuestionsRequestedBySchools() {
        customQuestionsRequestedBySchools = new Map<String, Boolean>();
        for (Integer i = 1; i <= AnnualSettingHelper.CUSTOM_QUESTION_BANK_SIZE; i++) {
            // school portal user renders selected in their schools annual settings for the current academic year
            if (isSchoolPortalUser) {
                if (Boolean.valueOf(currentSchoolAnnualSettings.get('Custom_Question_' + String.valueOf(i) + '__c'))) {
                    customQuestionsRequestedBySchools.put('Custom_Question_' + String.valueOf(i) + '__c', true);
                } else {
                    customQuestionsRequestedBySchools.put('Custom_Question_' + String.valueOf(i) + '__c', false);
                }
            } else {
                // family portal user renders questions selected for ALL subscribed schools to which they are applying
                // roll up all applicants schools
                customQuestionsRequestedBySchools = AnnualSettingHelper.getCustomQuestionsEnabledForYear(schoolIds, bulkController.mapAcademicYears.get(pfs.Academic_Year_Picklist__c).Id);
            }
        }
    }


    private void setDisplayCustomQuestions() {
        if (isSchoolPortalUser) {
            displayCustomQuestions = currentSchoolAnnualSettings.Use_Additional_Questions__c == 'Yes';
        } else {
            displayCustomQuestions = AnnualSettingHelper.getCustomQuestionsEnabledForSchoolIds(schoolIds, academicYearId);
        }
    }

    private void setSchoolIds() {
        schoolIds = new Set<String>();
        for (Id applicantId : applicationSchoolAssignmentMap.keySet()) {
            for (School_PFS_Assignment__c pfsAssignment : applicationSchoolAssignmentMap.get(applicantId)) {
                schoolIds.add(pfsAssignment.School__c);
            }
        }
    }

    // END SFP-606

    private static Map<String, Annual_Setting__c> annualSettingsMap = new Map<String, Annual_Setting__c> ();

    public Annual_Setting__c currentSchoolAnnualSettings {        
        get {
            if(currentSchoolAnnualSettings == null){
                
                String annualSettingKey = GlobalVariables.getCurrentSchoolId() + ':' + pfs.Academic_Year_Picklist__c;
                
                if (bulkController != null && bulkController.annualSettingBySchoolAndAcademicYear!=null && bulkController.annualSettingBySchoolAndAcademicYear.containsKey(annualSettingKey)) {
                    
                    //Logic to handle bulk of folders.
                    currentSchoolAnnualSettings = bulkController.annualSettingBySchoolAndAcademicYear.get(annualSettingKey);
                
                } else {
                    
                    //Logic to handle a single folder.
                    List<Annual_Setting__c> annualSettings = GlobalVariables.getCurrentAnnualSettings(false, null, GlobalVariables.getAcademicYearByName(pfs.Academic_Year_Picklist__c).Id, GlobalVariables.getCurrentSchoolId());
                    
                    if(annualSettings != null && annualSettings.size() > 0){
                        
                        currentSchoolAnnualSettings = annualSettings[0];
                    }
                }
            }
            return currentSchoolAnnualSettings;
        }
        
        private set;
    }
    
    //SFP-210, [G.S]
    public Map<Id,List<BizFarmGroupWrapper>> getBizOrFarmGroupWrappersMap(){

        return bulkController != null ? bulkController.Business.groupedBizFarmMap : new Map<Id,List<BizFarmGroupWrapper>>();
    }

    public class BizFarmGroupWrapper{
        public List<Business_Farm__c> bizFarmList {get; set;}
        public String header1 {get; set;}
        public String header2 {get; set;}
        public Business_Farm__c dummyBizFarm {get; set;}
        public String indexString {get; set;}

        public bizFarmGroupWrapper(List<Business_Farm__c> bizFarmListParam, String header1Param, String header2Param, Integer indexParam){
            bizFarmList = bizFarmListParam;
            header1 = header1Param;
            header2 = header2Param;
            indexString = String.valueOf(indexParam);

            dummyBizFarm = new Business_Farm__c();
            calculateTotals();

        }

        // calculate the dummy totals
        public void calculateTotals(){
            Decimal income = 0;
            Decimal expenses = 0;
            Decimal netprofitloss = 0;
            Decimal share = 0;

            for (Business_Farm__c bf : bizFarmList){
                income += (bf.Business_Total_Income_Current__c == null) ? 0 : bf.Business_Total_Income_Current__c;
                expenses += (bf.Business_Total_Expenses_Current__c == null) ? 0 : bf.Business_Total_Expenses_Current__c;
                netprofitloss += (bf.Net_Profit_Loss_Business_Farm_Current__c == null) ? 0 : bf.Net_Profit_Loss_Business_Farm_Current__c;
                share += (bf.Business_Net_Profit_Share_Current__c == null) ? 0 : bf.Business_Net_Profit_Share_Current__c;
            }

            dummyBizFarm.Business_Total_Income_Current__c = income;
            dummyBizFarm.Business_Total_Expenses_Current__c = expenses;
            dummyBizFarm.Net_Profit_Loss_Business_Farm_Current__c = netprofitloss;
            dummyBizFarm.Business_Net_Profit_Share_Current__c = share;
        }

    }

    public Boolean isMIEActive {
        get{
            if(isMIEActive==null)
            {
                if(bulkController != null ) {//For bulk
                    
                    isMIEActive = bulkController.MIE.isMIEActive(pfs);
                
                }else {//For a single PFS
                    
                    isMIEActive = MonthlyIncomeAndExpensesService.isMIEActive(pfs);
                }
            }
            return isMIEActive;
        }
        set;
    }

    public String getApplicantsNames(){
        List<String> applicantNames = new List<String>();
        for (Applicant__c item : pfs.Applicants__r){
            applicantnames.add(item.First_Name__c + ' ' + item.Last_Name__c);
        }
        return String.join(applicantNames, ', ');
    }
}