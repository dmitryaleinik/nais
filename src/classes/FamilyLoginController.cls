/**
 *   Spec-021
 *
 *   The landing/login page should have well worded questions to point people to either register, or enter their username and password.
 *   For the launch, no one will have an existing account, so that needs to be taken into consideration. When they do register, we need to
 *   match to existing contact records based on email if it exists.
 *   Registration creates the contact record, puts it into the account bucket and creates the portal user record. The PFS is not created at that point.
 *   Before registration the user should be asked to make sure the school(s) they wish to apply to are subscribers in the system.
 *   There will be a search mechanism because they shouldn't register if none of their schools are in the system. If they aren't in the system,
 *   they should be able to submit a list of the schools for sales purposes.
 *
 *   -System should provide a custom login/landing page for the Family Portal
 *   -Landing/login page should obviously distinguish between returning and new applicants to the system.
 *   -System should allow for anyone to create a family portal user account in the system. System should check to make sure the
 *   email address is not already in the system
 *   -Applicant should be able to determine if the school(s) to which they are applying are current SSS subscribers before filling out the application
 *   -System should auto-provision a license for any new applicant accounts created
 *
 *   Login Scenarios
 *   -Brand new user � user should be alerted email address was not found, directed to account creation
 *   -User from old system, not yet in new system � user alerted that they are recognized, given "update account" form to create new user in this system
 *   -User in new system � logged in
 *
 *   (c) Exponent Partners 2013 Drew Piston
 *   extra commit line here
 **/
global without sharing class FamilyLoginController {
    @testVisible private static final String EARLY_ACCESS = 'earlyaccess';

    @testVisible private Boolean earlyAccessGranted = false;

    /*Initialization*/
    global FamilyLoginController () {
        dummyContact = new Contact(RecordTypeId = RecordTypes.parentContactTypeId);     // NAIS-638
        dummyAccount = new Account();
        dummyAccount.BillingCountryCode = 'US';
        dummyContactForContactCountry = new Contact();
        dummyContactForContactCountry.MailingCountryCode = 'US';
        accountResultList = new List<Account>();
        searchHasOccurred = false;
        sssSearch = false;
        locationSearch = false;
        needToUpdate = false;
        foreignAddress = false;

        limitToPilotSchools = false;
        Family_Portal_Settings__c fps = Family_Portal_Settings__c.getInstance('Family');
        if (fps != null && fps.In_Pilot__c != null && fps.In_Pilot__c) {
            limitToPilotSchools = true;
        }

        // Properties for Live Agent Rendering.
        showLoginLiveAgent = false;
        unrecognizedLoginCount = 0;
        
        // Early access handling. AccessService only grants early access if the early access url parameter has been set.
        AccessService.Instance.setEarlyAccessCookie();
    }
    /*End Initialization*/

    /*Properties*/
    global String username { get; set; }
    global String password { get; set; }
    global String emailConfirm { get; set; }

    global String sssCode { get; set; }
    global List<Account> accountResultList { get; set; }
    global Boolean searchHasOccurred { get; set; }
    global Boolean sssSearch { get; set; }
    global Boolean locationSearch { get; set; }

    // used when someone using the forgot password functionality has a contact but no user
    global Boolean needToUpdate { get; set; }

    global User existingUser { get; set; }
    global Contact existingContact { get; set; }
    global Contact dummyContact { get; set; }
    global Account dummyAccount { get; set; }
    global Contact dummyContactForContactCountry { get; set; }
    global String selectedLanguage { get; set; }//SFP-10, [G.S]
    global Boolean limitToPilotSchools;

    global Boolean foreignAddress { get; set; }

    // Live agent Properties, these are directly related to custom varibles, so don't change
    // them unless you chage the live agent rules too. 
    global Boolean showLoginLiveAgent { get; set; }
    global Integer unrecognizedLoginCount { get; set; }
    //public static String activeSSSStatus = GlobalVariables.getActiveSSSStatusForTest();

    //SFP-10, [G.S]
    global List<SelectOption> getLanguages() {
        List<SelectOption> langs = new List<SelectOption>();
        langs.add(new SelectOption('en_US', 'English'));
        langs.add(new SelectOption('es', 'Spanish'));
        return langs;
    }

    public List<SchoolWrapper> getAccountResults() {
        List<SchoolWrapper> temp = new List<SchoolWrapper>();
        for (Account a : accountResultList) {
            temp.add(new SchoolWrapper(a));
        }

        return temp;
    }

    private IntegrationSource__c integrationSource;
    public IntegrationSource__c  getIntegrationSource() {
        if(integrationSource == null) {
            string source = ApexPages.currentPage().getParameters().get('source');
            if(!string.isEmpty(source))
               integrationSource = IntegrationSource__c.getInstance(source);


        }
        return integrationSource;
    }

    private string dashboardURL;
    public string getDashboardURL() {
         if(dashboardURL == null)
              dashboardURL = 'https://'+ApexPages.currentPage().getHeaders().get('Host')+'/familyportal/FamilyDashboard?source='+ApexPages.currentPage().getParameters().get('source');
         return dashboardURL;
    }

    public string getDocumentLogoUrl() {
    //return '/servlet/servlet.ImageServer?id='+getIntegrationSource().logoid__c+'&oid=' + UserInfo.getOrganizationId();
    //string host = 'https://'+ApexPages.currentPage().getHeaders().get('Host');
    //host = host.replace('force.com','content.force.com');
    //    return host+ '/servlet/servlet.ImageServer?id='+getIntegrationSource().logoid__c+'&oid=' + UserInfo.getOrganizationId();
    return '/servlet/servlet.ImageServer?id='+getIntegrationSource().logoid__c+'&oid=' + UserInfo.getOrganizationId();
    }

    public string integrationSourceFromCookie { get; set; }

    /*End Properties*/

    /*Action Methods*/

    // login method does not necessarily try to log them in, says I and Patson agrees
    global PageReference login() {
        existingUser = null;
        existingContact = null;

        try {
            for (User u : [Select Id, ContactID, IsActive, Username, ProfileId, IntegrationSource__c, Early_Access__c from User where Username = :username order by CreatedDate desc]) {
                // if more than one user exists, we want the active one
                if (existingUser == null || u.IsActive) {
                    existingUser = u;
                }
            }

            // if the user exists in this system, log her in
            if (existingUser != null && existingUser.IsActive && existingUser.ContactId != null) {
                // NAIS-536 Account Suspension
                Contact c = [select Id, Account_Suspended__c, Household__c, FirstName, LastName, Account.OwnerId from Contact where Id = :existingUser.ContactId];
                if (c.Account_Suspended__c == 'Yes') {
                    return Page.FamilyLoginAccountSuspended;
                } else {
                    existingUser = applyIntegrationSource(existingUser, true);
                    return loginUser();
                }
            }

            // if user exists but is inactive, activate user and reload page so that they can login
            if (existingUser != null && !existingUser.IsActive && existingUser.ContactId != null) {
                existingUser = applyIntegrationSource(existingUser, false);
                existingUser.isActive = true;
                update existingUser;
                apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Your Account has been reactivated -- please login again.'));
                return null;
            }

            // if user exists but not connected to a Contact
            if (existingUser != null && existingUser.ContactId == null) {
                //this is rare enough we will not handle it, just set existingUser to null and continue
                existingUser = null;
            }

            if (existingUser == null) {
                searchForExistingContact(username);
            }

            System.debug('TESTING129 ' + existingContact);

            if (existingContact != null) {
                //return update account page
                return Page.FamilyLoginUpdateAccount;
            } else {
                // set a page varibale here.
                unrecognizedLoginCount++;
                showLoginLiveAgent = false;
                // return secondary attempt login page
                return Page.FamilyLoginUnrecognized;
            }
        } catch (Exception e) {
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an error logging in -- please contact the administrator.  Error message: ' + e));
            return null;
        }
    }

    public User applyIntegrationSource(User existingUser, boolean doUpdate) {

        if(!String.isEmpty(integrationSourceFromCookie)) {
            if(existingUser.IntegrationSource__c != null) {
                if(!existingUser.IntegrationSource__c.contains(integrationSourceFromCookie)) {
                    existingUser.IntegrationSource__c = existingUser.IntegrationSource__c + ';' + integrationSourceFromCookie;
                }
            }
            else{
                existingUser.IntegrationSource__c = integrationSourceFromCookie;
            }
            if(doUpdate)
                update existingUser;
        }
        return existingUser;
    }

    /**
     * @description Log the user in, sending them to the appropriate page afterwards:
     *              either the FamilyPortalLandingPage if the school is closed, otherwise
     *              the family dashboard
     */
    global PageReference loginUser() {
        SiteService.Request request = new SiteService.Request(SiteService.PortalType.FAMILY, username, password);
        PageReference loginResponse = SiteService.Instance.login(request);

        // This is needed to set show live agent invitation after a failed login.
        if( loginResponse == null) {
            showLoginLiveAgent = true;
            return null;
        }
        return loginResponse;
    }

    global PageReference createAccount() {
        // do validation first
        if (dummyContact.Email != emailConfirm) {
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'ERROR: Email fields do not match'));
            return null;
        }

        if (phoneErrorExists()) {
            return null;
        }

        existingUser = null;
        existingContact = null;

        for (User u : [Select Id, ContactID, IsActive, Username, ProfileId from User where Username = :dummyContact.Email order by CreatedDate desc]) {
            // if more than one user exists, we want the active one
            if (existingUser == null || u.IsActive) {
                existingUser = u;
            }
        }

        // if the user exists in this system and is active
        if (existingUser != null && existingUser.IsActive && existingUser.ContactId != null) {
            if (existingUser.ProfileId != GlobalVariables.familyPortalProfileId) {
                apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: this email address is already associated with a school user account. Please use a different email address.'));
                existingUser = null;
                existingContact = null;
            } else {
                apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.Family_User_Already_Exists));
            }

            return null;
        }

        // if user exists but is inactive
        if (existingUser != null && !existingUser.IsActive && existingUser.ContactId != null) {
            existingUser.isActive = true;
            existingUser = applyIntegrationSource(existingUser, false);
            update existingUser;
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Your Account has been reactivated -- please login or visit the Forgot Password page.'));
            return null;
        }

        // if user exists but not connected to a Contact
        if (existingUser != null && existingUser.ContactId == null) {
            //this is rare enough we will not handle it, just set existingUser to null and continue
            existingUser = null;
        }

        if (existingUser == null) {
            searchForExistingContact(dummyContact.Email);

            // if contact exists but no user, we will update the existing contact and use it
            if (existingContact != null) {
                apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'You already have an Account in the system - please verify the information below and update your Account.'));
                return null;
            }
        }

        Account contactAccount = ContactAction.getBucketAccountForFamilyContact(dummyContact, null);
        insert contactAccount;

        // make HH here
        Household__c hh = new Household__c();
        hh.Name = dummyContact.FirstName + ' ' + dummyContact.LastName + ' Household';
        insert hh;

        dummyContact.Household__c = hh.Id;
        dummyContact.AccountId = contactAccount.Id;
        dummyContact.OwnerId = GlobalVariables.getIndyOwner();
        // using account country code for default purposes
        dummyContact.MailingCountryCode = dummyContactForContactCountry.MailingCountryCode;
        dummyContact.MailingStateCode = dummyContactForContactCountry.MailingStateCode;

        System.debug('TESTING228 dummycontact ' + dummyContact);

        upsert dummyContact;

        User newUser = createUserRecord(dummyContact);
        newUser =  applyIntegrationSource(newUser, false);
        Id userId = Site.createPortalUser(newUser, dummyContact.AccountId, null, true);

        System.debug('TESTING284 ' + newUser);

        if (userId == null) {
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'ERROR updating Account'));
            return null;
        } else {
            PageReference pr = Page.FamilyLoginConfirmation;
            pr.getParameters().put('create', 'true');
            return pr;
        }
    }

    global PageReference updateAccount() {
        // do validation first
        if (phoneErrorExists()) {
            return null;
        }

        // make HH here
        if (existingContact.Household__c == null) {
            Household__c hh = new Household__c();
            hh.Name = existingContact.FirstName + ' ' + existingContact.LastName + ' Household';
            insert hh;

            existingContact.Household__c = hh.Id;
        }

        existingContact.Phone = dummyContact.Phone;

        existingContact.MailingStreet = dummyContact.MailingStreet;
        existingContact.MailingCity = dummyContact.MailingCity;
        // using account country code for default purposes
        existingContact.MailingCountryCode = dummyContactForContactCountry.MailingCountryCode;
        existingContact.MailingStateCode = dummyContactForContactCountry.MailingStateCode;
        existingContact.MailingPostalCode = dummyContact.MailingPostalCode;

        Account tempAccount = new Account();
        tempAccount.Name = 'thisisatempaccount';
        tempAccount.OwnerId = existingContact.Account.OwnerId;
        insert tempAccount;

        // put contact in its own account so we are SURE to use this one when creating the site user
        Id originalAccountId = existingContact.AccountId;
        existingContact.AccountId = tempAccount.Id;

        update existingContact;

        User newUser = createUserRecord(existingContact);
        newUser = applyIntegrationSource(newUser, false);
        Id userId = Site.createPortalUser(newUser, existingContact.AccountId, null, true);

        System.debug('TESTING228 ' + newUser);

        // put Contact back in its original account
        existingContact.AccountId = originalAccountId;
        update existingContact;

        delete tempAccount;

        if (userId == null) {
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'ERROR updating Account'));
            return null;
        } else {
            return Page.FamilyLoginConfirmation;
        }
    }

    // get contact with matching email address
    // use the Contact with a child PFS if one exists, otherwise just the most recent one
    public void searchForExistingContact(String emailQueryString) {
        List<Contact> matchingEmailContacts = [Select Id, Name, FirstName, LastName, Email, AccountId, Account.Name, Account.OwnerId, Household__c,
                                                    (Select ID from PFSs_Parent_A__r)
                                                    from Contact
                                                    where Email = :emailQueryString
                                                    order by CreatedDate desc];

        System.debug('TESTING 128 ' + matchingEmailContacts);
        // loop through once and set existingContact to the one that has been Parent A in the past
        for (Contact c : matchingEmailContacts) {
            if (existingContact == null && c.PFSs_Parent_A__r != null && c.PFSs_Parent_A__r.size() > 0) {
                existingContact = c;
            }
        }

        System.debug('TESTING existingContact 137 ' + existingContact);
        if (existingContact == null) {
            // loop through again if no one was found, just grab the first one
            for (Contact c : matchingEmailContacts) {
                if (existingContact == null) {
                    existingContact = c;
                }
            }
        }
    }

    public void findSubscribers() {
        searchHasOccurred = true;
        accountResultList = new List<Account>();
        sssSearch = sssCode != null && sssCode.length() > 0 ? true : false;
        locationSearch = dummyAccount.BillingStateCode != null || dummyAccount.BillingCountryCode != null ? true : false;

        // special exemption if Country is US but state not selected -- does not count as location search
        locationSearch = dummyAccount.BillingStateCode == null && dummyAccount.BillingCountryCode == 'US' ? false : locationSearch;

        System.debug('TESTING185 ' + sssSearch + locationSearch);

        if ((sssSearch && locationSearch) || (!sssSearch && !locationSearch)) {
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please provide either SSS Code or Country & State/Province information (but not both) to find Subscribers.'));
            searchHasOccurred = false;
            return;
        }

        String schoolSearchQuery = 'Select Id, Name, SSS_School_Code__c, BillingStreet, BillingCity, BillingCountryCode, BillingStateCode, BillingPostalCode from Account';

        if (sssSearch) {
            schoolSearchQuery += ' where SSS_School_Code__c = :sssCode';
        } else if (locationSearch) {
            // if country is us or canada, require a state
            if(dummyAccount.BillingStateCode == null && (dummyAccount.BillingCountryCode == 'US' || dummyAccount.BillingCountryCode == 'CA')) {
                apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'A State/Province is required for this Country.'));
                searchHasOccurred = false;
                return;
            }

            // if state is null, go by country
            if(dummyAccount.BillingStateCode == null) {
                schoolSearchQuery += ' where BillingCountryCode = \'' + dummyAccount.BillingCountryCode + '\' AND BillingCountryCode != null';
            } else if(dummyAccount.BillingCountryCode == null) {
                schoolSearchQuery += ' where BillingStateCode = \'' + dummyAccount.BillingStateCode + '\' AND BillingStateCode != null';
            } else {
                schoolSearchQuery += ' where BillingStateCode = \'' + dummyAccount.BillingStateCode + '\' AND BillingStateCode != null AND BillingCountryCode = \'' + dummyAccount.BillingCountryCode + '\' AND BillingCountryCode != null';
            }
        }

        // if we are in the pilot (stored in custom settings) we will limit these to pilot schools
        if (limitToPilotSchools) {
            schoolSearchQuery += ' AND Pilot_School__c = true';
        }

        Set<String> activeSSSStatuses = GlobalVariables.activeSSSStatuses;
        // limit to active schools
        schoolSearchQuery += ' AND SSS_Subscriber_Status__c in :activeSSSStatuses order by Name asc';
        accountResultList = Database.query(schoolSearchQuery);
    }

    global PageReference cancel() {
        return Page.FamilyLogin;
    }

    global PageReference forgotPassword() {
        needToUpdate = false;
        existingUser = null;
        existingContact = null;

        for (User u : [Select Id, ContactID, IsActive, Username from User where Username = :username order by CreatedDate desc]) {
            // if more than one user exists, we want the active one
            if (existingUser == null || u.IsActive) {
                existingUser = u;
            }
        }

        if (existingUser == null) {
            for (Contact c : [Select Id, Name, FirstName, LastName, Email, AccountId, Account.Name, Account.OwnerId, Household__c from Contact where Email = :username order by CreatedDate desc]) {
                if (existingContact == null) {
                    existingContact = c;
                }
            }
        }

        if (existingUser == null && existingContact != null) {
            needToUpdate = true;
            return Page.FamilyLoginUpdateAccount;
        }


        boolean success = Site.forgotPassword(username);
        PageReference pr = Page.FamilyLoginConfirmation;
        pr.setRedirect(true);

        if (success) {
            return pr;
        } else {
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Your password was not able to be reset.'));
        }

        return null;


    }

    /*End Action Methods*/

    /*Helper Methods*/

    public Boolean phoneErrorExists() {
        if ((dummyContact.HomePhone == null && dummyContact.Preferred_Phone__c == 'Home')
            ||
            (dummyContact.MobilePhone == null && dummyContact.Preferred_Phone__c == 'Mobile')
            ||
            (dummyContact.Phone == null && dummyContact.Preferred_Phone__c == 'Work')) {

            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'ERROR: Please provide the preferred Phone Number.'));
            return true;
        } else {
            return false;
        }
    }

    /**
     * @description Ensure that we are accessing the Family Portal from the Community and not the legacy
     *              Customer Portal. If we are in the portal because the user has an old like or bookmark,
     *              redirect them to the Community.
     */
    public PageReference enforceCommunityAccess(){
        PageReference pr;
        String startURL = ApexPages.currentPage().getParameters().get('startURL');
        if(CurrentUser.isGuest() && Site.getSiteType()=='Visualforce') {
            pr = new PageReference(FamilyRedirectTemplateController.calculateCommunityURL(startURL));
        }
        return pr;
    }


    public User createUserRecord(Contact c) {
        User newUser = new User();
        //SFP-10, [G.S]
        newUser.LanguageLocaleKey = selectedLanguage == null ? 'en_US' : selectedLanguage;
        newUser.FirstName = c.FirstName;
        newUser.LastName = c.LastName;
        newUser.Username = c.Email;
        newUser.ProfileId = GlobalVariables.familyPortalProfileId;
        newUser.ContactId = c.Id;
        newUser.Email = c.Email;
        newUser.SpringCM_PortalUser__c = true; // [DP] NAIS-1786 -- mark as Portal User

        newUser.CommunityNickName = newUser.Email.substring(0, newUser.Email.indexOf('@')) +'_'+Math.round((Math.random() * 100000));

        newUser.TimeZoneSidKey = 'America/Los_Angeles';
        return newUser;
    }
    /*End Helper Methods*/


    /* Remote Action Methods */
    @RemoteAction 
    public static LiveAgentDeploymentService.LiveAgentConfigurationWrapper getLiveAgentInformation( String deploymentType) {
        return LiveAgentDeploymentService.getLiveAgentInformation( deploymentType);
    }

    /*Wrapper Classes*/

    public class SchoolWrapper {
        public String schoolName { get; set; }
        public String schoolAddress { get; set; }
        public String sssCode { get; set; }

        public schoolWrapper(Account a) {
            schoolName = a.Name;
            sssCode = a.SSS_School_Code__c;

            schoolAddress = '';
            schoolAddress = a.BillingStreet == null ? schoolAddress :  schoolAddress + a.BillingStreet;
            schoolAddress = a.BillingCity == null ? schoolAddress :  schoolAddress + ', ' + a.BillingCity;
            schoolAddress = a.BillingStateCode == null ? schoolAddress :  schoolAddress + ', ' + a.BillingStateCode;
            schoolAddress = a.BillingCountryCode == null || a.BillingCountryCode == 'US' ? schoolAddress :  schoolAddress + ' ' + a.BillingCountryCode;
        }
    }

    /*End Wrapper Classes*/
}