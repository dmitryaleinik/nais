/**
 * SendgridEventTest.cls
 *
 * @description: Test class for SendgridEvent using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
private class SendgridEventTest {
    
    /* negative test cases */

    // empty object
    @isTest 
    static void constructor_noProperties_emptyObject() {
        
        // Arrange
        // no work

        // Act
        Test.startTest();
            SendgridEvent sEvent = new SendgridEvent();
        Test.stopTest();

        // Assert
        System.assertNotEquals( null, sEvent);

        // All props are null
        System.assertEquals( null, sEvent.response);
        System.assertEquals( null, sEvent.sg_event_id);
        System.assertEquals( null, sEvent.sg_message_id);
        System.assertEquals( null, sEvent.event);
        System.assertEquals( null, sEvent.email);
        System.assertEquals( null, sEvent.timestamp);
        System.assertEquals( null, sEvent.useragent);
        System.assertEquals( null, sEvent.send_at);
        System.assertEquals( null, sEvent.attempt);
        System.assertEquals( null, sEvent.school_id);
        System.assertEquals( null, sEvent.sender_id);
        System.assertEquals( null, sEvent.recipient_id);
        System.assertEquals( null, sEvent.spa_id);
        System.assertEquals( null, sEvent.mass_email_send_id);
        System.assertEquals( null, sEvent.academic_year);
        System.assertEquals( null, sEvent.asm_group_id);
        System.assertEquals( null, sEvent.ip);
        System.assertEquals( null, sEvent.tls);
        System.assertEquals( null, sEvent.cert_err);
        System.assertEquals( null, sEvent.reason);
        System.assertEquals( null, sEvent.url);
        System.assertEquals( null, sEvent.url_offset);
    }

    /* positive test cases */
    
    // populated object
    @isTest 
    static void constructor_allProperties_populatedObject() {
        
        // Arrange
        // no work

        // Act
        Test.startTest();
            SendgridEvent sEvent = new SendgridEvent();
            sEvent.response = 'test';
            sEvent.sg_event_id = 'test';
            sEvent.sg_message_id = 'test';
            sEvent.event = 'test';
            sEvent.email = 'test';
            sEvent.timestamp = 101010110;
            sEvent.useragent = 'test';
            sEvent.send_at = 'test';
            sEvent.attempt = 'test';
            sEvent.school_id = 'test';
            sEvent.sender_id = 'test';
            sEvent.recipient_id = 'test';
            sEvent.spa_id = 'test';
            sEvent.mass_email_send_id = 'test';
            sEvent.academic_year = '2016-2017';
            sEvent.asm_group_id = 123456;
            sEvent.ip = 'test';
            sEvent.tls = 'test';
            sEvent.cert_err = 'test';
            sEvent.reason = 'test';
            sEvent.url = 'test';
            sEvent.url_offset = new Map<String,String> { 'test' => 'test'};
        Test.stopTest();

        // Assert
        System.assertNotEquals( null, sEvent);
        System.assertEquals( 'test', sEvent.response);
        System.assertEquals( 'test', sEvent.sg_event_id);
        System.assertEquals( 'test', sEvent.sg_message_id);
        System.assertEquals( 'test', sEvent.event);
        System.assertEquals( 'test', sEvent.email);
        System.assertEquals( 101010110, sEvent.timestamp);
        System.assertEquals( 'test', sEvent.useragent);
        System.assertEquals( 'test', sEvent.send_at);
        System.assertEquals( 'test', sEvent.attempt);
        System.assertEquals( 'test', sEvent.school_id);
        System.assertEquals( 'test', sEvent.sender_id);
        System.assertEquals( 'test', sEvent.recipient_id);
        System.assertEquals( 'test', sEvent.spa_id);
        System.assertEquals( 'test', sEvent.mass_email_send_id);
        System.assertEquals( '2016-2017', sEvent.academic_year);
        System.assertEquals( 123456, sEvent.asm_group_id);
        System.assertEquals( 'test', sEvent.ip);
        System.assertEquals( 'test', sEvent.tls);
        System.assertEquals( 'test', sEvent.cert_err);
        System.assertEquals( 'test', sEvent.reason);
        System.assertEquals( 'test', sEvent.url);
        System.assertEquals( new Map<String,String> { 'test' => 'test'}, sEvent.url_offset);
    }
    
}