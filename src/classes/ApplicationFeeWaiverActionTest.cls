@isTest
private class ApplicationFeeWaiverActionTest 
{

    @testSetup
    private static void setup() 
    {
        User familyPortalUser = UserTestData.insertFamilyPortalUser();

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        Account school = AccountTestData.Instance.forRecordTypeId(RecordTypes.schoolAccountTypeId).insertAccount();
        
        PFS__c pfs = PfsTestData.Instance.forParentA(familyPortalUser.ContactId)
            .forAcademicYearPicklist(currentAcademicYear.Name).asSubmitted().insertPfs();
    }

    @isTest
    private static void testCreateFeeWaiverTLIOnInsert() 
    {
        PFS__c pfs = getPFSRecord()[0];
        Academic_Year__c currentAcademicYear = getAcademicYearRecord()[0];
        Account school = getSchoolRecord()[0];
        User familyPortalUser = getFamilyPortalUserRecord();

        // set up test data
        Application_Fee_Settings__c applicationFeeSettings = new Application_Fee_Settings__c();

        applicationFeeSettings.Name = GlobalVariables.getCurrentYearString();
        applicationFeeSettings.Amount__c = 50;
        applicationFeeSettings.Discounted_Amount__c = 40;
        applicationFeeSettings.Product_Code__c = 'Test Product Code'; // [SL] NAIS-1031: new required field
        applicationFeeSettings.Discounted_Product_Code__c = 'KS Test Product Code'; // [SL] NAIS-1031: new required field
        insert applicationFeeSettings;

        Transaction_Settings__c transSettings = new Transaction_Settings__c();
        transSettings.Name = 'PFS Fee Waiver';
        transSettings.Transaction_Code__c = '1234';
        transSettings.Account_Code__c = '4567';
        transSettings.Account_Label__c = 'Test Label';
        insert transSettings;

        FinanceAction.createOppAndTransaction(new Set<Id>{pfs.Id}); // [DP] 07.08.2015 NAIS-1933 -- need to call this explicitly now

        Test.startTest();
        // create a test fee waiver as assigned
        Application_Fee_Waiver__c feeWaiver = new Application_Fee_Waiver__c();
        feeWaiver.Academic_Year__c = currentAcademicYear.Id;
        feeWaiver.Account__c = school.Id;
        feeWaiver.Contact__c = familyPortalUser.ContactId;
        feeWaiver.Status__c = 'Assigned';
        insert feeWaiver;

        // get the opportunity
        Opportunity opp = [SELECT Id, Paid_Status__c, Amount, Total_Waivers__c FROM Opportunity WHERE PFS__c = :pfs.Id];

        // verify the TLI and opportunity
        List<Transaction_Line_Item__c> tliList = [SELECT Id, Amount__c, Waiver_Provider__c, Transaction_Code__c,
                                                    Account_Code__c, Account_Label__c
                                                    FROM Transaction_Line_Item__c
                                                    WHERE Opportunity__c = :opp.Id
                                                    AND RecordTypeId = :RecordTypes.pfsFeeWaiverTransactionTypeId];

        System.assertEquals(tliList[0].Transaction_Code__c, transSettings.Transaction_Code__c);
        System.assertEquals(tliList[0].Account_Code__c, transSettings.Account_Code__c);
        System.assertEquals(tliList[0].Account_Label__c, transSettings.Account_Label__c);
        System.assertEquals(1, tliList.size());
        System.assertEquals(opp.Amount, tliList[0].Amount__c);
        System.assertEquals(feeWaiver.Account__c, tliList[0].Waiver_Provider__c);
        System.assertEquals('Paid', opp.Paid_Status__c);
        System.assertEquals(opp.Amount, opp.Total_Waivers__c);
        Test.stopTest();
    }

    @isTest
    private static void testCreateFeeWaiverTLIOnUpdate() 
    {
        PFS__c pfs = getPFSRecord()[0];
        Academic_Year__c currentAcademicYear = getAcademicYearRecord()[0];
        Account school = getSchoolRecord()[0];
        User familyPortalUser = getFamilyPortalUserRecord();

        // set up test data
        Application_Fee_Settings__c feeSettings = new Application_Fee_Settings__c();
        feeSettings.Name = GlobalVariables.getCurrentYearString();
        feeSettings.Amount__c = 50;
        feeSettings.Discounted_Amount__c = 40;
        feeSettings.Product_Code__c = 'Test Product Code'; // [SL] NAIS-1031: new required field
        feeSettings.Discounted_Product_Code__c = 'KS Test Product Code'; // [SL] NAIS-1031: new required field
        insert feeSettings;

        FinanceAction.createOppAndTransaction(new Set<Id>{pfs.Id}); // [DP] 07.08.2015 NAIS-1933 -- need to call this explicitly now

        Test.startTest();
        // create a test fee waiver as assigned
        Application_Fee_Waiver__c feeWaiver = new Application_Fee_Waiver__c();
        feeWaiver.Academic_Year__c = currentAcademicYear.Id;
        feeWaiver.Account__c = school.Id;
        feeWaiver.Contact__c = familyPortalUser.ContactId;
        feeWaiver.Status__c = ApplicationFeeWaiverAction.WAIVER_PENDING_STATUS;
        insert feeWaiver;

        // get the opportunity
        Opportunity opp = [SELECT Id, Paid_Status__c, Amount, Total_Waivers__c FROM Opportunity WHERE PFS__c = :pfs.Id];

        // verify no TLI
        List<Transaction_Line_Item__c> tliList = [SELECT Id, Amount__c, Waiver_Provider__c
                                                    FROM Transaction_Line_Item__c
                                                    WHERE Opportunity__c = :opp.Id
                                                    AND RecordTypeId = :RecordTypes.pfsFeeWaiverTransactionTypeId];
        System.assertEquals(0, tliList.size());

        // change the status to assigned
        feeWaiver.Status__c = 'Assigned';
        update feeWaiver;

        // get the opportunity
        opp = [SELECT Id, Paid_Status__c, Amount, Total_Waivers__c FROM Opportunity WHERE PFS__c = :pfs.Id];

        // verify the TLI and opportunity
        tliList = [SELECT Id, Amount__c, Waiver_Provider__c
                    FROM Transaction_Line_Item__c
                    WHERE Opportunity__c = :opp.Id
                    AND RecordTypeId = :RecordTypes.pfsFeeWaiverTransactionTypeId];

        System.assertEquals(1, tliList.size());
        System.assertEquals(opp.Amount, tliList[0].Amount__c);
        System.assertEquals(feeWaiver.Account__c, tliList[0].Waiver_Provider__c);
        System.assertEquals('Paid', opp.Paid_Status__c);
        System.assertEquals(opp.Amount, opp.Total_Waivers__c);
        Test.stopTest();
    }

    @isTest
    private static void testRequestedWaiverStatusOnPFS() 
    {
        PFS__c pfs = getPFSRecord()[0];
        Academic_Year__c currentAcademicYear = getAcademicYearRecord()[0];
        Account school = getSchoolRecord()[0];
        User familyPortalUser = getFamilyPortalUserRecord();

        Test.startTest();
        // create a test fee waiver as assigned
        Application_Fee_Waiver__c feeWaiver = new Application_Fee_Waiver__c();
        feeWaiver.Academic_Year__c = currentAcademicYear.Id;
        feeWaiver.Account__c = school.Id;
        feeWaiver.Contact__c = familyPortalUser.ContactId;
        feeWaiver.Status__c = ApplicationFeeWaiverAction.WAIVER_PENDING_STATUS;
        insert feeWaiver;

        PFS__c testPFS = [Select Id, Fee_Waived__c from PFS__c where Id = :pfs.Id];
        System.assertEquals(ApplicationFeeWaiverAction.WAIVER_REQUESTED_STATUS, testPFS.Fee_Waived__c);
        Test.stopTest();
    }

    @isTest
    private static void testDeniedWaiverStatusOnPFS() 
    {
        PFS__c pfs = getPFSRecord()[0];
        Academic_Year__c currentAcademicYear = getAcademicYearRecord()[0];
        Account school = getSchoolRecord()[0];
        User familyPortalUser = getFamilyPortalUserRecord();

        // create a test fee waiver as assigned
        Application_Fee_Waiver__c feeWaiver = new Application_Fee_Waiver__c();
        feeWaiver.Academic_Year__c = currentAcademicYear.Id;
        feeWaiver.Account__c = school.Id;
        feeWaiver.Contact__c = familyPortalUser.ContactId;
        feeWaiver.Status__c = ApplicationFeeWaiverAction.WAIVER_PENDING_STATUS;
        insert feeWaiver;

        PFS__c testPFS = [Select Id, Fee_Waived__c from PFS__c where Id = :pfs.Id];
        System.assertEquals(ApplicationFeeWaiverAction.WAIVER_REQUESTED_STATUS, testPFS.Fee_Waived__c);

        Test.startTest();
        feeWaiver.Status__c = ApplicationFeeWaiverAction.WAIVER_DECLINED_STATUS;
        update feeWaiver;

        testPFS = [Select Id, Fee_Waived__c from PFS__c where Id = :pfs.Id];
        System.assertEquals(ApplicationFeeWaiverAction.WAIVER_DENIED_STATUS, testPFS.Fee_Waived__c);
        Test.stopTest();
    }

    @isTest
    private static void testRequestedAndDeniedWaiverStatusOnPFS() 
    {
        Academic_Year__c currentAcademicYear = getAcademicYearRecord()[0];
        Account school = getSchoolRecord()[0];
        User familyPortalUser = getFamilyPortalUserRecord();

        PFS__c pfs1 = getPFSRecord()[0];
        Contact contact2 = ContactTestData.Instance
            .forAccount(school.Id)
            .forRecordTypeId(RecordTypes.schoolStaffContactTypeId)
            .insertContact();
        PFS__c pfs2 = PfsTestData.Instance.forParentA(contact2.Id)
            .forAcademicYearPicklist(currentAcademicYear.Name).asSubmitted().insertPfs();

        Test.startTest();
        // create a test fee waiver as assigned
        Application_Fee_Waiver__c feeWaiver1 = new Application_Fee_Waiver__c();
        feeWaiver1.Academic_Year__c = currentAcademicYear.Id;
        feeWaiver1.Account__c = school.Id;
        feeWaiver1.Contact__c = familyPortalUser.ContactId;
        feeWaiver1.Status__c = ApplicationFeeWaiverAction.WAIVER_PENDING_STATUS;

        Application_Fee_Waiver__c feeWaiver2 = new Application_Fee_Waiver__c();
        feeWaiver2.Academic_Year__c = currentAcademicYear.Id;
        feeWaiver2.Account__c = school.Id;
        feeWaiver2.Contact__c = contact2.Id;
        feeWaiver2.Status__c = ApplicationFeeWaiverAction.WAIVER_DECLINED_STATUS;
        insert new List<Application_Fee_Waiver__c> {feeWaiver1, feeWaiver2};

        PFS__c testPFS1 = [Select Id, Fee_Waived__c from PFS__c where Id = :pfs1.Id];
        System.assertEquals(ApplicationFeeWaiverAction.WAIVER_REQUESTED_STATUS, testPFS1.Fee_Waived__c);

        PFS__c testPFS2 = [Select Id, Fee_Waived__c from PFS__c where Id = :pfs2.Id];
        System.assertEquals(ApplicationFeeWaiverAction.WAIVER_DENIED_STATUS, testPFS2.Fee_Waived__c);

        feeWaiver1.Status__c = ApplicationFeeWaiverAction.WAIVER_DECLINED_STATUS;
        feeWaiver2.Status__c = ApplicationFeeWaiverAction.WAIVER_PENDING_STATUS;
        update new List<Application_Fee_Waiver__c> {feeWaiver1, feeWaiver2};

        testPFS1 = [Select Id, Fee_Waived__c from PFS__c where Id = :pfs1.Id];
        System.assertEquals(ApplicationFeeWaiverAction.WAIVER_DENIED_STATUS, testPFS1.Fee_Waived__c);

        testPFS2 = [Select Id, Fee_Waived__c from PFS__c where Id = :pfs2.Id];
        System.assertEquals(ApplicationFeeWaiverAction.WAIVER_REQUESTED_STATUS, testPFS2.Fee_Waived__c);
        Test.stopTest();
    }

    @isTest
    private static void autoApplyAFW_nullPfs_expectNullPointerException() 
    {
        try 
        {
            Test.startTest();
            ApplicationFeeWaiverAction.autoApplyAFW(null);

            System.assert(false, 'Expected a System.NullPointerException.');
        } catch (Exception e) 
        {
            System.assertEquals('System.NullPointerException', e.getTypeName(),
                    'Expected a System.NullPointerException, not: ' + e.getTypeName() + '.');
        } finally 
        {
            Test.stopTest();
        }
    }

    @isTest
    private static void autoApplyAFW_expectNewFeeWaiverAssigned()
    {
        PFS__c pfs = getPFSRecord()[0];
        Account mbfwSchool = 
            AccountTestData.Instance.forRecordTypeId(RecordTypes.meansBasedFeeWaiverAccountTypeId).DefaultAccount;

        Application_Fee_Settings__c applicationFeeSettings = ApplicationFeeSettingsTestData.Instance
            .asCurrentYear()
            .forMeansBasedFeeWaiverSchoolId(mbfwSchool.Id).insertApplicationFeeSettings();

        Set<String> statuses = new Set<String> {
            ApplicationFeeWaiverAction.PENDING_QUALIFICATION,
            ApplicationFeeWaiverAction.WAIVER_DECLINED_STATUS,
            ApplicationFeeWaiverAction.ASSIGNED
        };

        Test.startTest();
            ApplicationFeeWaiverAction.autoApplyAFW(pfs);
            assertFeeWaivers(pfs);

            // Running a second time to ensure that we don't get a duplicate fee waiver
            ApplicationFeeWaiverAction.autoApplyAFW(pfs);
            assertFeeWaivers(pfs);

            List<Application_Fee_Waiver__c> feeWaivers = ApplicationFeeWaiverAction.getExistingApplicationFeeWaivers(
                pfs, statuses);
            System.assertEquals(1, feeWaivers.size());
            feeWaivers[0].Status__c = ApplicationFeeWaiverAction.PENDING_QUALIFICATION;
            update feeWaivers[0];

            ApplicationFeeWaiverAction.autoApplyAFW(pfs);
            feeWaivers = ApplicationFeeWaiverAction.getExistingApplicationFeeWaivers(pfs, statuses);
            System.assertEquals(2, feeWaivers.size());
        Test.stopTest();
    }

    private static List<PFS__c> getPFSRecord()
    {
        return [SELECT Id, Academic_Year_Picklist__c, Parent_A__c FROM PFS__c LIMIT 50000];
    }
    
    private static List<Academic_Year__c> getAcademicYearRecord()
    {
        return [SELECT Id, Name FROM Academic_Year__c LIMIT 50000];
    }
    
    private static List<Account> getSchoolRecord()
    {
        return [SELECT Id FROM Account WHERE RecordTypeId = :RecordTypes.schoolAccountTypeId LIMIT 50000];
    }

    private static User getFamilyPortalUserRecord()
    {
        return [
            SELECT ContactId 
            FROM User 
            WHERE Contact.RecordTypeId = :RecordTypes.parentContactTypeId 
                AND isActive = true 
                AND Name = 'MrNobody'
              LIMIT 1][0];
    }

    private static void assertFeeWaivers(PFS__c pfsFW) 
    {
        Set<String> statuses = new Set<String> {
                ApplicationFeeWaiverAction.PENDING_QUALIFICATION,
                ApplicationFeeWaiverAction.WAIVER_DECLINED_STATUS,
                ApplicationFeeWaiverAction.ASSIGNED
        };
        
        List<Application_Fee_Waiver__c> feeWaivers = ApplicationFeeWaiverAction.getExistingApplicationFeeWaivers(
                pfsFW, statuses);

        System.assertEquals(1, feeWaivers.size(), 'Expected there to be one fee waiver assigned.');
        System.assertEquals(ApplicationFeeWaiverAction.ASSIGNED, feeWaivers[0].Status__c,
                'Expected the Status to be ' + ApplicationFeeWaiverAction.ASSIGNED);
        System.assertEquals(pfsFW.Parent_A__c, feeWaivers[0].Contact__c,
                'Expected the Contact to be ' + pfsFW.Parent_A__c);
        System.assertEquals(ApplicationFeeSettingsService.Instance.getMeansBasedFeeWaiverAccountId(), feeWaivers[0].Account__c,
                'Expected the Account to be ' + ApplicationFeeSettingsService.Instance.getMeansBasedFeeWaiverAccountId());
    }
}