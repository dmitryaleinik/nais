@isTest
private class StudentFolderAfterTest
{
    private static final String STUDENT_FOLDER_STATUS_AWARD_APPROVED = 'Award Approved';
    private static final Integer INTEGER_0 = 0;
    private static final Integer INTEGER_100 = 100;
    private static final Integer INTEGER_200 = 200;

    @isTest
    private static void budgetAllocationGroupUpdate() {
        
        // Need Student_Folder, Budget_Allocation and Budget Group.
        Contact student = ContactTestData.Instance.asStudent().insertContact();
        Student_Folder__c studentFolder = StudentFolderTestData.Instance
            .forStudentId(student.Id)
            .forGrantAwarded(INTEGER_100)
            .forFolderStatus(STUDENT_FOLDER_STATUS_AWARD_APPROVED)
            .insertStudentFolder();
        Budget_Group__c budgetGroup = BudgetGroupTestData.Instance.DefaultBudgetGroup;

        Budget_Allocation__c budgetAllocation = new Budget_Allocation__c(
            Amount_Allocated__c = INTEGER_100,      
            Budget_Group__c = budgetGroup.Id,
            Student_Folder__c = studentFolder.Id);

        // Status comes from Student_Folder.Folder_Status
        insert budgetAllocation;
        
        Test.startTest();
        
        budgetGroup = getBudgetGroup(budgetGroup.Id);
        System.assertEquals(INTEGER_100, budgetGroup.Total_Allocated__c);
        
        budgetAllocation.Amount_Allocated__c = INTEGER_200;
        update budgetAllocation;

        budgetGroup = getBudgetGroup(budgetGroup.Id);
        System.assertEquals(INTEGER_200, budgetGroup.Total_Allocated__c);
        
        delete budgetAllocation;
    
        budgetGroup = getBudgetGroup(budgetGroup.Id);
        System.assertEquals(INTEGER_0, budgetGroup.Total_Allocated__c);
            
        Test.stopTest();
    }

    @isTest
    private static void budgetTotalAllocatedUpdateOnStudentFolderRemove() {
        
        // Need Student_Folder, Budget_Allocation and Budget Group.
        Contact student = ContactTestData.Instance.asStudent().insertContact();
        Budget_Group__c budgetGroup = BudgetGroupTestData.Instance.DefaultBudgetGroup;

        Test.startTest();
        
        Student_Folder__c studentFolder = StudentFolderTestData.Instance
            .forStudentId(student.Id)
            .forGrantAwarded(INTEGER_100)
            .forFolderStatus(STUDENT_FOLDER_STATUS_AWARD_APPROVED)
            .insertStudentFolder();
        
        Budget_Allocation__c budgetAllocation = new Budget_Allocation__c(
            Amount_Allocated__c = INTEGER_100,      
            Budget_Group__c = budgetGroup.Id,
            Student_Folder__c = studentFolder.Id);

        insert budgetAllocation;

        budgetGroup = getBudgetGroup(budgetGroup.Id);
        System.assertEquals(INTEGER_100, budgetGroup.Total_Allocated__c);
        
        delete studentFolder;

        System.assert([SELECT Id FROM Budget_Allocation__c LIMIT 50000].isEmpty());
        budgetGroup = getBudgetGroup(budgetGroup.Id);
        System.assertEquals(INTEGER_0, budgetGroup.Total_Allocated__c);
        
        Test.stopTest();
    }

    @isTest
    private static void studentEthnicityPopulatingFromContact()
    {
        String studentEthnicity = 'International';
        Contact student = ContactTestData.Instance
            .asStudent()
            .forEthnicity(studentEthnicity).DefaultContact;
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
        Academic_Year__c nextAcademicYear = AcademicYearTestData.Instance.asNextYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, nextAcademicYear};

        Student_Folder__c studentFolderForCurrentYear = StudentFolderTestData.Instance
            .forEthnicity(studentEthnicity)
            .forAcademicYearPicklist(currentAcademicYear.Name).DefaultStudentFolder;
        
        Test.startTest();
            Student_Folder__c studentFolderForNextYear = StudentFolderTestData.Instance
                .forAcademicYearPicklist(nextAcademicYear.Name).insertStudentFolder();

            studentFolderForNextYear = StudentFolderSelector.newInstance().selectByIdWithCustomFieldList(
                new Set<Id>{studentFolderForNextYear.Id}, new List<String>{'Ethnicity__c'})[0];
            System.assertEquals(studentEthnicity, studentFolderForNextYear.Ethnicity__c);
        Test.stopTest();
    }
    
    private static Budget_Group__c getBudgetGroup(Id budgetGroupId)
    {
        return [SELECT Total_Allocated__c FROM Budget_Group__c WHERE Id = :budgetGroupId][0];
    }
}