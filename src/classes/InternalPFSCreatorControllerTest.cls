/**
* @ InternalPFSCreatorControllerTest.cls
* @date 5/29/2014
* @author  Drew Piston for Exponent Partners
* @description Tests for Page controller for the InternalPFSController page
*/

/*
* NAIS-1670 Case # 5491: Paper Application Entry - Drew Piston
*
* Copyright Exponent Partners 2014
* Databank will be entering paper PFSs via the following process:
* 1. Search for contact. If found, click button to create the PFS and all associated records in the background. If not found, create new contact and then click button.
* 2. Navigate to PFS and click Family View button. From there, enter application via the family portal interface.
* The work required is to build the button and code to create the PFS and associated records, and create a profile with appropriate permissions for Databank users.
*/

@isTest
private class InternalPFSCreatorControllerTest {

    private static Contact parentA;
    
    // sets up custom settings, academic year, and parenta contact
    private static void setupData(){
        Family_Portal_Settings__c fps = new Family_Portal_Settings__c(); 
        fps.Name = 'Family';
        fps.Individual_Account_Owner_Id__c = [Select Id from User where IsActive = true AND UserRoleId != null AND Profile.Name = 'System Administrator'][0].Id;
        insert fps;

        //setup PFS
        // create test data
        List<Academic_Year__c> academicYears = TestUtils.createAcademicYears();
        String academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        parentA.Email = 'familyTestData@exp.exp';
        insert new List<Contact> { parentA};
    }

    // just testing contact query
    @isTest
    private static void testConstructor(){
        setupData();
        PageReference testPR = Page.InternalPFSCreator;
        testPR.getParameters().put('contactid', parentA.Id);
        Test.setCurrentPage(testPR);

        InternalPFSCreatorController testController = new InternalPFSCreatorController();
        System.assertEquals(parentA.Id, testController.theContact.Id);
    }
    
    // tests pfs create from init method
    @isTest
    private static void testPFSCreate(){
        setupData();
        PageReference testPR = Page.InternalPFSCreator;
        testPR.getParameters().put('contactid', parentA.Id);
        Test.setCurrentPage(testPR);

        InternalPFSCreatorController testController = new InternalPFSCreatorController();
        System.assertEquals(parentA.Id, testController.theContact.Id);

        System.assertEquals(0, [Select count() from PFS__c where Parent_A__c = :parentA.Id]);
        testController.init();
        System.assertEquals(1, [Select count() from PFS__c where Parent_A__c = :parentA.Id]);
    }

    // tests error from no contact id
    @isTest
    private static void testNoConId(){
        setupData();
        PageReference testPR = Page.InternalPFSCreator;
        //testPR.getParameters().put('contactid', parentA.Id);
        Test.setCurrentPage(testPR);

        InternalPFSCreatorController testController = new InternalPFSCreatorController();
        System.assertEquals(true, testController.hasErrors);
    }

    // tests error from bad contact id
    @isTest
    private static void testBadConId(){
        setupData();
        PageReference testPR = Page.InternalPFSCreator;
        // put in bad id
        testPR.getParameters().put('contactid', parentA.AccountId);
        Test.setCurrentPage(testPR);

        InternalPFSCreatorController testController = new InternalPFSCreatorController();
        System.assertEquals(true, testController.hasErrors);
    }

    // tests createpfs and booleans when there are existing PFSs
    @isTest
    private static void testExistingPFSCreate(){
        setupData();
        PageReference testPR = Page.InternalPFSCreator;
        testPR.getParameters().put('contactid', parentA.Id);
        Test.setCurrentPage(testPR);

        InternalPFSCreatorController testController = new InternalPFSCreatorController();
        System.assertEquals(parentA.Id, testController.theContact.Id);

        System.assertEquals(0, [Select count() from PFS__c where Parent_A__c = :parentA.Id]);
        testController.init();
        System.assertEquals(1, [Select count() from PFS__c where Parent_A__c = :parentA.Id]);

        testController = new InternalPFSCreatorController();
        System.assertEquals(parentA.Id, testController.theContact.Id);    
        testController.init();
        System.assertEquals(true, testController.existingPFS);
        System.assertEquals(false, testController.mulitpleExistingPFSs);
        System.assertEquals(1, [Select count() from PFS__c where Parent_A__c = :parentA.Id]);
        testController.createPFS();
        System.assertEquals(2, [Select count() from PFS__c where Parent_A__c = :parentA.Id]);

        testController = new InternalPFSCreatorController();
        System.assertEquals(parentA.Id, testController.theContact.Id);    
        testController.init();
        System.assertEquals(true, testController.existingPFS);
        System.assertEquals(true, testController.mulitpleExistingPFSs);
        System.assertEquals(2, [Select count() from PFS__c where Parent_A__c = :parentA.Id]);
        testController.createPFS();
        System.assertEquals(3, [Select count() from PFS__c where Parent_A__c = :parentA.Id]);
    }
}