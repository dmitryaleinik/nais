/**
 * SchoolSelectedRecipientsExtension.cls
 *
 * @description: Server side extension for Mass Email selected recipients page, handles interactions w/ data model and view state manipulation.
 *
 * @author: Chase Logan @ Presence PG
 */
public class SchoolSelectedRecipientsExtension {
    
    /* member vars and properties */
    public Boolean massEmailEnabled {

        get {
            // Handle app enabled/disabled
            return MassEmailUtil.isMassEmailGloballyEnabledByAccount( 
                        GlobalVariables.getCurrentSchoolId(), UserInfo.getProfileId());
        }
        private set;
    }
    public SchoolMassEmailSendModel massESModel { get; private set; }
    public List<SchoolStudentModel> sModelList { 
        
        get {
            return ( this.sModelList == null ? 
                        this.sModelList = new List<SchoolStudentModel>() : this.sModelList);
        }
        private set; 
    }
    public Integer selectedRecipCount { 

        get {
            return ( this.selectedRecipCount == null ?
                        this.selectedRecipCount = 0 : this.selectedRecipCount);
        }
         private set;
    }
    public Boolean discardDraft {
        
        get {
            return ( this.discardDraft == null ? this.discardDraft = false : this.discardDraft);
        }
        set;
    }
    public String selectedRecipient { get; set; }
    public Boolean initHasRun { get; set; }
    public Boolean displayCreatedDate { get; private set; }
    public String massESCreatedDate { get; private set; }

    private Mass_Email_Send__c massES;
    private Set<String> sPFSIdSet;

    /* class constants */
    public static final String QUERY_STRING_MESSAGE_DETAIL = 'messagedetailid';
    private final String NAME_DRAFT = 'New Mass Email';
    
    
    // default ctor, pulls recordset array out of StandardSetController 
    // passed by My Applicants or Advanced Lists select lists
    public SchoolSelectedRecipientsExtension( ApexPages.StandardSetController controller) {
        
        this.initHasRun = false;
        this.sPFSIdSet = new Set<String>();
        for ( School_PFS_Assignment__c sPFS : ( School_PFS_Assignment__c[])controller.getSelected() ) {

            this.sPFSIdSet.add( sPFS.Id);
        }
    }

    // handles page init, as DML is involved this is fired via actionFunction
    // bound to window.onload event
    public void initModelAndView() {

        this.init( this.sPFSIdSet);
    }

    // handle Next button navigation
    public PageReference navigateNext() {

        if ( this.massESModel != null && this.massESModel.status == EmailService.EMAIL_NEW_STATUS) {

            this.massESModel.status = EmailService.EMAIL_DRAFT_STATUS;
        }
        
        this.save();

        PageReference pRef = Page.SchoolCreateMessage;
        pRef.getParameters().put( SchoolCreateMessageController.QUERY_STRING_MESSAGE_DETAIL, this.massES.Id);
        pRef.setRedirect( true);

        return pRef;
    }

    // handle navigation back to dashboard
    public PageReference navigateBackToDashboard() {

        if ( !this.discardDraft) {

            this.massESModel.status = EmailService.EMAIL_DRAFT_STATUS;
            this.save();
        } else {

            if ( this.massESModel.status == EmailService.EMAIL_NEW_STATUS) {

                delete this.massES;
            }
        }

        PageReference pRef = Page.SchoolMassEmailDashboard;
        pRef.setRedirect( true);

        return pRef;
    }

    // Handles navigating away from page via Back link, save record if it's already draft status, 
    // delete record still in new status
    public PageReference handleUnload() {

        if ( !this.discardDraft) {

            if ( this.massES != null ) {

                this.massES = new MassEmailSendDataAccessService().getMassEmailSendById( this.massES.Id);
                this.massESModel = SchoolMassEmailSendModel.getModel(this.massES);

                if ( this.massESModel.status == EmailService.EMAIL_NEW_STATUS) {

                    delete this.massES;
                } else if ( this.massESModel != null && this.massESModel.status == EmailService.EMAIL_DRAFT_STATUS) {

                    this.save();
                }
            }
        }

        PageReference pRef = Page.SchoolSelectEmailRecipients;
        pRef.setRedirect( true);
        
        return pRef;
    }

    // handle removing a selected recipient from the list
    public PageReference removeRecipient() {

        if ( this.selectedRecipient != null && this.sModelList.size() > 1) {

            for ( Integer i = 0; i < this.sModelList.size(); i++) {

                if ( this.sModelList[i].id == this.selectedRecipient) {

                    this.sModelList.remove( i);
                    break;
                }
            }

            this.selectedRecipCount = this.sModelList.size();
        }

        return null;
    }

    
    /* private instance methods */

    // handles page initialization
    private void init( Set<string> sPFSIdSet) {

        if ( !this.initHasRun) {

            this.displayCreatedDate = false;
            // grab query string param and attempt lookup of corresponding massES record if this is an edit
            // otherwise create new massES record in EMAIL_DRAFT_STATUS if recipients have been selected
            String currentDetailId = 
                ApexPages.currentPage().getParameters().get( SchoolSelectedRecipientsExtension.QUERY_STRING_MESSAGE_DETAIL);

            if ( String.isNotEmpty( currentDetailId)) {

                this.massES = new MassEmailSendDataAccessService().getMassEmailSendById( currentDetailId);
                if ( this.massES.CreatedDate < Date.today()) {

                    this.displayCreatedDate = true;
                    this.massESCreatedDate = this.massES.CreatedDate.format( MassEmailUtil.COMMON_DATE_FORMAT);
                }

                // if this is a draft or existing message and changes have been made to default footer or logo, update them
                Account acct = new AccountDataAccessService().getAccountById( GlobalVariables.getCurrentSchoolId());
                this.massES.Footer__c = ( acct != null && String.isNotEmpty( acct.Mass_Email_Footer__c) 
                                                ? acct.Mass_Email_Footer__c : null);
                this.massES.Logo_Name__c = ( acct != null && String.isNotEmpty( acct.Mass_Email_Logo_Name__c)
                                                ? acct.Mass_Email_Logo_Name__c : null);
                update this.massES;

                this.massES = new MassEmailSendDataAccessService().getMassEmailSendById( currentDetailId);
                this.massESModel = SchoolMassEmailSendModel.getModel(this.massES);
            } else {

                if ( this.massES == null && sPFSIdSet != null && sPFSIdSet.size() > 0) {

                    Mass_Email_Setting__mdt EmailTemplate = [select Value__c from Mass_Email_Setting__mdt where DeveloperName = 'EmailTemplate'];
                    Account acct = new AccountDataAccessService().getAccountById( GlobalVariables.getCurrentSchoolId());
                    this.massES = new Mass_Email_Send__c( 
                                        Name = this.NAME_DRAFT, 
                                        Visualforce_Template__c = EmailTemplate.Value__c,
                                        Status__c = EmailService.EMAIL_NEW_STATUS,
                                        School__c = GlobalVariables.getCurrentSchoolId(),
                                        Sent_By__c = GlobalVariables.getCurrentUser().ContactId,
                                        Footer__c = ( acct != null && String.isNotEmpty( acct.Mass_Email_Footer__c) 
                                                        ? acct.Mass_Email_Footer__c : null),
                                        Reply_To_Address__c = ( acct != null && String.isNotEmpty( acct.Mass_Email_Reply_To__c)
                                                                    ? acct.Mass_Email_Reply_To__c : null),
                                        Logo_Name__c = ( acct != null && String.isNotEmpty( acct.Mass_Email_Logo_Name__c)
                                                            ? acct.Mass_Email_Logo_Name__c : null));
                    insert this.massES;

                    this.massES = new MassEmailSendDataAccessService().getMassEmailSendById( this.massES.Id);
                    this.massESModel = SchoolMassEmailSendModel.getModel(this.massES);
                }
            }

            // If user is coming from My Applications or Advanced List sPFSIdSet will be populated from 
            // StandardSetController. If user is editing or cloning existing MassES, pull recips off MassES record
            if ( sPFSIdSet != null && sPFSIdSet.size() > 0) {

                // Lookup School PFS record by incoming School PFS ID set, construct model wrapper instances
                List<School_PFS_Assignment__c> schoolPFSList = new
                    SchoolPFSAssignmentDataAccessService().getSchoolPFSByIdSet( sPFSIdSet);

                this.constructModelList( schoolPFSList);
            } else {

                // Either this is an edit/clone of an existing record, or the Create New Mass Email button on dashboard
                // was clicked, in that case, do nothing
                if ( this.massESModel != null && this.massESModel.massES.School_PFS_Assignments__c != null) {

                    sPFSIdSet = new Set<String>();
                    List<String> splitList = this.massESModel.massES.School_PFS_Assignments__c.split( SchoolMassEmailSendModel.SPLIT_CHAR);
                    if ( splitList != null && splitList.size() > 0) {

                        sPFSIdSet.addAll( splitList);
                    }

                    // Lookup School PFS record by incoming School PFS ID set, construct model wrapper instances
                    List<School_PFS_Assignment__c> schoolPFSList = new
                        SchoolPFSAssignmentDataAccessService().getSchoolPFSByIdSet( sPFSIdSet);

                    this.constructModelList( schoolPFSList);
                }
            }

            // flag init as run so refreshes don't cause it to fire again
            this.initHasRun = true;
        }
    }

    // construct instance list of model wrapper instances
    private void constructModelList( List<School_PFS_Assignment__c> schoolPFSList) {

        if ( schoolPFSList == null || schoolPFSList.isEmpty()) return;

        for ( School_PFS_Assignment__c sPFS : schoolPFSList) {

               SchoolStudentModel sModel = new SchoolStudentModel( sPFS);
               this.sModelList.add( sModel);
           }

           this.selectedRecipCount = this.sModelList.size();
    }

    // handles saving changes to local massES record
    private void save() {

        if ( this.massESModel != null && this.sModelList != null) {

            List<String> recipList = new List<String>();
            List<String> sPFSList = new List<String>();
            for ( SchoolStudentModel sModel : sModelList) {

                if ( String.isNotEmpty( sModel.parentId)) {

                    recipList.add( sModel.parentId);
                }

                if ( String.isNotEmpty( sModel.spaId)) {

                    sPFSList.add( sModel.spaId);
                }
            }
            
            this.massESModel.recipList = recipList;
            this.massESModel.sPFSList = sPFSList;
            this.massESModel.save();
        } else if ( this.massESModel != null) {

            this.massESModel.save();
        }
    }

    // Model wrapper class for UI presentation
    public class SchoolStudentModel {

        public String id { get; set; }
        public String spaId { get; set; }
        public String studentFirstName { get; set; }
        public String studentLastName { get; set; }
        public String parentId { get; set; }
        public String parentName { get; set; }
        public String parentEmail { get; set; }
        public String gradeApplying { get; set; }
        public String pfsID { get; set; }
        public String pfsStatus { get; set; }

        public SchoolStudentModel( School_PFS_Assignment__c studentPFS) {

            this.id = studentPFS.id;
            this.spaId = studentPFS.Id;
            this.studentFirstName = studentPFS.Applicant_First_Name__c;
            this.studentLastName = studentPFS.Applicant_Last_Name__c;
            this.parentId = studentPFS.Applicant__r.PFS__r.Parent_A__c;
            this.parentName = studentPFS.Parent_A__c;
            this.parentEmail = studentPFS.Parent_A_Email_Migrated__c;
            this.gradeApplying = studentPFS.Grade_Applying__c;
            this.pfsID = studentPFS.PFS_Number__c;
            this.pfsStatus = studentPFS.PFS_Combined_Status__c;
        }

    }

}