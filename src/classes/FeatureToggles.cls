/**
 * @description This class is used to interact with the Feature_Toggles__c hierarchical setting. This setting is used to
 *              hide new functionality and selectively enabled it for particular users.
 */
public class FeatureToggles {

    @testVisible private static final String FEATURE_TOGGLE_NAME_PARAM = 'featureToggleName';
    @testVisible private static final String FIELD_TO_SET_PARAM = 'fieldToSet';
    @testVisible private static final String FIELD_VALUE_PARAM = 'fieldValue';

    private static Feature_Toggles__c record;
    private static Set<String> fieldNames;

    /**
     * @description Determines if a feature is enabled for the current user based on the specified feature toggle name.
     *              The name should correspond to a field on the Feature_Toggles__c hierarchical setting.
     * @param featureToggleName The name of a feature toggle to check.
     * @return True if the specified feature is enabled for the current user.
     * @throws ArgumentNullException if featureToggleName is null.
     */
    public static Boolean isEnabled(String featureToggleName) {
        ArgumentNullException.throwIfNull(featureToggleName, FEATURE_TOGGLE_NAME_PARAM);

        // If the field exists in the metadata check the value, otherwise default to false.
        Boolean isFeatureEnabled = hasField(featureToggleName) ? (Boolean)getInstance().get(featureToggleName) : false;

        // If the value of the feature toggle is null, default to false.
        return isFeatureEnabled == null ? false : isFeatureEnabled;
    }

    /**
     * @description Sets a value on the internal record. This method permits unit testing without doing DML on
     *              hierarchical settings. This method is only meant to be used in unit tests and should be left private
     *              while using the testVisible annotation.
     * @param featureToggleName The field to populate.
     * @param fieldValue The value to set.
     * @throws ArgumentNullException if any parameter is null.
     */
    @testVisible
    private static void setToggle(String featureToggleName, Boolean fieldValue) {
        ArgumentNullException.throwIfNull(featureToggleName, FEATURE_TOGGLE_NAME_PARAM);
        ArgumentNullException.throwIfNull(fieldValue, FIELD_VALUE_PARAM);

        // If the field is a valid feature toggle, populate the value.
        if (hasField(featureToggleName)) {
            getInstance().put(featureToggleName, fieldValue);
        }
    }

    private static Feature_Toggles__c getInstance() {
        if (record != null) {
            return record;
        }

        record = Feature_Toggles__c.getInstance();

        // If nothing is defined, not even org defaults, create a new instance using the default values.
        // If we are in a unit test, use the default values.
        if (record == null || Test.isRunningTest()) {
            record = getDefault();
        }

        return record;
    }

    private static Feature_Toggles__c getDefault() {
        return (Feature_Toggles__c)Feature_Toggles__c.SObjectType.newSObject(null, true);
    }

    private static Set<String> getFieldNames() {
        if (fieldNames != null) {
            return fieldNames;
        }

        fieldNames = new Set<String>();

        for (String fieldName : Schema.SObjectType.Feature_Toggles__c.fields.getMap().keySet()) {
            fieldNames.add(fieldName.toLowerCase());
        }

        return fieldNames;
    }

    private static Boolean hasField(String fieldName) {
        return String.isNotBlank(fieldName) && getFieldNames().contains(fieldName.toLowerCase());
    }
}