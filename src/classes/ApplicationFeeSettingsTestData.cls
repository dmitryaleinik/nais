/**
 * @description This class is used to create Application Fee Settings records for unit tests.
 */
@isTest
public class ApplicationFeeSettingsTestData extends SObjectTestData {
    
    @testVisible private static final String NAME = 
        String.valueOf(Date.Today().AddYears(1).Year()) + '-' + String.valueOf(Date.Today().AddYears(2).Year());
    @testVisible private static final String NAME_CURRENT_YEAR = 
        String.valueOf(Date.Today().Year()) + '-' + String.valueOf(Date.Today().AddYears(1).Year());
    @testVisible private static final Decimal AMOUNT = 10;
    @testVisible private static final Decimal DISCOUNTED_AMOUNT = 15;
    @testVisible private static final String PRODUCT_CODE = 
        '131-100 PFS Fee - ' + String.valueOf(Date.Today().AddYears(1).Year());
    @testVisible private static final String PRODUCT_CODE_CURRENT = 
        '131-100 PFS Fee - ' + String.valueOf(Date.Today().AddYears(1).Year());
    @testVisible private static final String DISCOUNTED_PRODUCT_CODE =
        '131-200 PFS Fee - KS - ' + String.valueOf(Date.Today().AddYears(2).Year());
    @testVisible private static final String DISCOUNTED_PRODUCT_CODE_CURRENT =
        '131-200 PFS Fee - KS - ' + String.valueOf(Date.Today().AddYears(1).Year());

    /**
     * @description Get the default values for the Application_Fee_Settings__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Application_Fee_Settings__c.Name => NAME,
                Application_Fee_Settings__c.Amount__c => AMOUNT,
                Application_Fee_Settings__c.Discounted_Amount__c => DISCOUNTED_AMOUNT,
                Application_Fee_Settings__c.Product_Code__c => PRODUCT_CODE,
                Application_Fee_Settings__c.Discounted_Product_Code__c => DISCOUNTED_PRODUCT_CODE,
                Application_Fee_Settings__c.Means_Based_Fee_Waiver_Record_Type_Name__c => 
                    ApplicationFeeSettingsService.MEANS_BASED_FEE_WAIVER_RECORDTYPE
        };
    }

    /**
     * @description Set the Name field on the current ApplicationFeeSettings record.
     * @param afsName The Name to set to the Name field of the current ApplicationFeeSettings record.
     * @return The current instance of ApplicationFeeSettingsTestData.
     */
    public ApplicationFeeSettingsTestData forName(String afsName) {
        return (ApplicationFeeSettingsTestData) with(Application_Fee_Settings__c.Name, afsName);
    }

    /**
     * @description Set the Amount field on the current ApplicationFeeSettings record.
     * @param afsAmount The amount to set to the Amount__c field of the current ApplicationFeeSettings record.
     * @return The current instance of ApplicationFeeSettingsTestData.
     */
    public ApplicationFeeSettingsTestData forAmount(Integer afsAmount) {
        return (ApplicationFeeSettingsTestData) with(Application_Fee_Settings__c.Amount__c, afsAmount);
    }

    /**
     * @description Set the Discounted Amount field on the current ApplicationFeeSettings record.
     * @param afsDiscountedAmount The amount to set to the Discounted_Amount__c field 
     *        of the current ApplicationFeeSettings record.
     * @return The current instance of ApplicationFeeSettingsTestData.
     */
    public ApplicationFeeSettingsTestData forDiscountedAmount(Integer afsDiscountedAmount) {
        return (ApplicationFeeSettingsTestData) with(Application_Fee_Settings__c.Discounted_Amount__c, afsDiscountedAmount);
    }

    /**
     * @description Set the Product Code field on the current ApplicationFeeSettings record.
     * @param afsProductCode The product code to set to the Product_Code__c field 
     *        of the current ApplicationFeeSettings record.
     * @return The current instance of ApplicationFeeSettingsTestData.
     */
    public ApplicationFeeSettingsTestData forProductCode(String afsProductCode) {
        return (ApplicationFeeSettingsTestData) with(Application_Fee_Settings__c.Product_Code__c, afsProductCode);
    }

    /**
     * @description Set the Discounted Product Code field on the current ApplicationFeeSettings record.
     * @param afsDiscountedProductCode The product code to set to the Discounted_Product_Code__c field 
     *        of the current ApplicationFeeSettings record.
     * @return The current instance of ApplicationFeeSettingsTestData.
     */
    public ApplicationFeeSettingsTestData forDiscountedProductCode(String afsDiscountedProductCode) {
        return (ApplicationFeeSettingsTestData) with(Application_Fee_Settings__c.Discounted_Product_Code__c, afsDiscountedProductCode);
    }

    /**
     * @description Set the Means Based Fee Waiver Account Id on the current ApplicationFeeSettings record.
     * @param schoolId The Means Based Fee Waiver School Id to set Means_Based_Fee_Waiver_Acct_Id__c field 
     *        of the current ApplicationFeeSettings record.
     * @return The current instance of ApplicationFeeSettings.
     */
    public ApplicationFeeSettingsTestData forMeansBasedFeeWaiverSchoolId(Id schoolId) {
        return (ApplicationFeeSettingsTestData) with(Application_Fee_Settings__c.Means_Based_Fee_Waiver_Acct_Id__c, schoolId);
    }

    /**
     * @description Insert the current working Application_Fee_Settings__c record.
     * @return The currently operated upon Application_Fee_Settings__c record.
     */
    public Application_Fee_Settings__c insertApplicationFeeSettings() {
        return (Application_Fee_Settings__c)insertRecord();
    }

    /**
     * @description Set the Name, Product Code and Discounted Product Code on the current ApplicationFeeSettings record.
     * @return The current instance of ApplicationFeeSettings.
     */
    public ApplicationFeeSettingsTestData asCurrentYear() {
        return (ApplicationFeeSettingsTestData) with(Application_Fee_Settings__c.Name, NAME_CURRENT_YEAR)
                                                .with(Application_Fee_Settings__c.Product_Code__c, PRODUCT_CODE_CURRENT)
                                                .with(Application_Fee_Settings__c.Discounted_Product_Code__c, DISCOUNTED_PRODUCT_CODE_CURRENT);
    }

    /**
     * @description Create the current working Application Fee Settings record without resetting
     *          the stored values in this instance of ApplicationFeeSettingsTestData.
     * @return A non-inserted Application_Fee_Settings__c record using the currently stored field
     *          values.
     */
    public Application_Fee_Settings__c createApplicationFeeSettingsWithoutReset() {
        return (Application_Fee_Settings__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Application_Fee_Settings__c record.
     * @return The currently operated upon Application_Fee_Settings__c record.
     */
    public Application_Fee_Settings__c create() {
        return (Application_Fee_Settings__c)super.buildWithReset();
    }

    /**
     * @description The default Application_Fee_Settings__c record.
     */
    public Application_Fee_Settings__c DefaultApplicationFeeSettings {
        get {
            if (DefaultApplicationFeeSettings == null) {
                DefaultApplicationFeeSettings = createApplicationFeeSettingsWithoutReset();
                insert DefaultApplicationFeeSettings;
            }
            return DefaultApplicationFeeSettings;
        }
        private set;
    }

    /**
     * @description Get the Application_Fee_Settings__c SObjectType.
     * @return The Application_Fee_Settings__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Application_Fee_Settings__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static ApplicationFeeSettingsTestData Instance {
        get {
            if (Instance == null) {
                Instance = new ApplicationFeeSettingsTestData();
            }
            return Instance;
        }
        private set;
    }

    private ApplicationFeeSettingsTestData() { }
}