@isTest
private class batchUpdateAccountModelTest
{

    @isTest
    private static void testNewAccountModelForOldContacts()
    {
        TestUtils.createDataForIndividualAccountModel();
        Contact parentA;
        
        Test.startTest();
            Account testAccount = new Account(Name='Individual',RecordTypeId=RecordTypes.individualAccountTypeId);
            insert testAccount;
            parentA = TestUtils.createContact('Parent', 'Testerman', testAccount.Id, RecordTypes.parentContactTypeId, true);
            Database.executeBatch(new batchUpdateAccountModel());
        Test.stopTest();
            
        //Verify batch changes
        parentA = [Select Id, AccountId from Contact where Id=:parentA.Id limit 1];
        system.assertEquals(true,testAccount.Id!=parentA.AccountId);
        system.assertEquals('Parent Testerman Account', [Select Name from Account where Id=:parentA.AccountId 
                                                                and RecordTypeId=:RecordTypes.individualAccountTypeId limit 1].Name);
    }//End:testNewAccountModelForOldContacts
}//End-Class