@isTest
private class BusinessFarmSelectorTest
{

    @isTest
    private static void selectByPfsId_nullParam_expectArgumentNullException()
    {
        try {
            Test.startTest();
            BusinessFarmSelector.Instance.selectByPfsId(null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, BusinessFarmSelector.PFS_IDS_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void selectByPfsId_mixedBusinessFarms_businessFarmsForPfsReturned()
    {
        Business_Farm__c businessFarm = BusinessFarmTestData.Instance.DefaultBusinessFarm;
        BusinessFarmTestData.Instance.forPfsId(PfsTestData.Instance.InsertPfs().Id).insertBusinessFarm();

        Set<Id> pfsIds = new Set<Id> { PfsTestData.Instance.DefaultPfs.Id };

        Test.startTest();
        List<Business_Farm__c> businessFarms = BusinessFarmSelector.Instance.selectByPfsId(pfsIds);
        Test.stopTest();

        System.assertNotEquals(null, businessFarms, 'Expected the result list to not be null.');
        System.assertEquals(1, businessFarms.size(), 'Expected exactly one business farm returned.');
        System.assertEquals(businessFarm.Id, businessFarms[0].Id, 'Expected the given business farm to be returned.');
    }

    @isTest
    private static void testSelectByPfsIdWithSBFAsWithCustomFieldLists()
    {
        Pfs__c pfsA = PfsTestData.Instance.create();
        Pfs__c pfsB = PfsTestData.Instance.create();
        insert new List<Pfs__c>{pfsA, pfsB};

        Applicant__c applicantA = ApplicantTestData.Instance
            .forPfsId(pfsA.Id).create();
        Applicant__c applicantB = ApplicantTestData.Instance
            .forPfsId(pfsB.Id).create();
        insert new List<Applicant__c>{applicantA, applicantB};

        Business_Farm__c businessFarmA = BusinessFarmTestData.Instance
            .forPfsId(pfsA.Id).create();
        Business_Farm__c businessFarmB = BusinessFarmTestData.Instance
            .forPfsId(pfsB.Id).create();
        insert new List<Business_Farm__c>{businessFarmA, businessFarmB};

        School_PFS_Assignment__c spfsaA = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicantA.Id).create();
        School_PFS_Assignment__c spfsaB = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicantB.Id).create();
        insert new List<School_PFS_Assignment__c>{spfsaA, spfsaB};

        School_Biz_Farm_Assignment__c sbfaA = SchoolBusinessFarmAssignmentTestData.Instance
            .forBusinessFarm(businessFarmA.Id)
            .forSchoolPfsAssignment(spfsaA.Id).create();
        School_Biz_Farm_Assignment__c sbfaB = SchoolBusinessFarmAssignmentTestData.Instance
            .forBusinessFarm(businessFarmB.Id)
            .forSchoolPfsAssignment(spfsaB.Id).create();
        insert new List<School_Biz_Farm_Assignment__c>{sbfaA, sbfaB};

        Test.startTest();
            try
            {
                List<Business_Farm__c> queriedRecords = BusinessFarmSelector.newInstance()
                    .selectByPfsIdWithSBFAsWithCustomFieldLists(null, new List<String>{'Name'}, new List<String>{'Name'});
            } catch (Exception e) 
            {
                TestHelpers.assertArgumentNullException(e, BusinessFarmSelector.PFS_IDS_PARAM);
            }

            try
            {
                List<Business_Farm__c> queriedRecords = BusinessFarmSelector.newInstance()
                    .selectByPfsIdWithSBFAsWithCustomFieldLists(new Set<Id>{pfsA.Id}, null, new List<String>{'Name'});
            } catch (Exception e)
            {
                TestHelpers.assertArgumentNullException(e, BusinessFarmSelector.BUSINESS_FARM_FIELDS_PARAM);
            }

            try
            {
                List<Business_Farm__c> queriedRecords = BusinessFarmSelector.newInstance()
                    .selectByPfsIdWithSBFAsWithCustomFieldLists(new Set<Id>{pfsA.Id}, new List<String>{'Name'}, null);
            } catch (Exception e)
            {
                TestHelpers.assertArgumentNullException(e, BusinessFarmSelector.SBFA_FIELDS_PARAM);
            }
        
            List<Business_Farm__c> queriedRecords = BusinessFarmSelector.newInstance()
                .selectByPfsIdWithSBFAsWithCustomFieldLists(
                    new Set<Id>{pfsA.Id}, new List<String>{'Name'}, new List<String>{'Name'});
        Test.stopTest();
        
        System.debug(BusinessFarmSelector.newInstance().selectByPfsId(new Set<Id>{pfsA.Id}));

        System.assertNotEquals(null, queriedRecords, 'Expected the queried records to not be null');
        System.assertEquals(1, queriedRecords.size(), 'Expected 1 record to be returned');
        System.assertEquals(businessFarmA.Id, queriedRecords[0].Id, 'Expected records related to PfsA to be returned');
    }
}