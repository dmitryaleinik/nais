@isTest
private class PfsServiceTest {
    private class TestDataLocal {
        private Annual_Setting__c annualSetting;
        private PFS__c pfs;
        private School_PFS_Assignment__c spfsa;
        private Academic_Year__c academicYear;
        private Applicant__c applicant;
        private Student_Folder__c studentFolder;

        private TestDataLocal() {
            Account school = AccountTestData.Instance.DefaultAccount;

            Contact parentA = ContactTestData.Instance.asParent().create();
            Contact parentB = ContactTestData.Instance.asParent().create();
            Contact student = ContactTestData.Instance.asStudent().create();
            insert new List<Contact> { parentA, parentB, student };

            academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
            annualSetting = AnnualSettingsTestData.Instance
                    .forAcademicYearId(academicYear.Id)
                    .forSchoolId(school.Id)
                    .insertAnnualSettings();

            pfs = PfsTestData.Instance
                    .forParentA(parentA.Id)
                    .forParentB(parentB.Id)
                    .forAcademicYearPicklist(academicYear.Name)
                    .asPaid()
                    .insertPfs();

            applicant = ApplicantTestData.Instance.forContactId(student.Id).forPfsId(pfs.Id).insertApplicant();

            studentFolder = StudentFolderTestData.Instance
                    .forAcademicYearPicklist(academicYear.Name)
                    .forStudentId(student.Id)
                    .insertStudentFolder();

            spfsa = TestUtils.createSchoolPFSAssignment(
                    academicYear.Id, applicant.Id, school.Id, studentFolder.Id, true);
        }

        private School_PFS_Assignment__c applyToAdditionalSchool() {
            Account additionalSchool = AccountTestData.Instance.insertAccount();

            annualSetting = AnnualSettingsTestData.Instance
                    .forAcademicYearId(academicYear.Id)
                    .forSchoolId(additionalSchool.Id)
                    .insertAnnualSettings();

            return TestUtils.createSchoolPFSAssignment(
                    academicYear.Id, applicant.Id, additionalSchool.Id, studentFolder.Id, true);
        }
    }

    private static TestDataLocal setup() {
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);

        return new TestDataLocal();
    }

    @isTest
    private static void queueSetWeightedPfs_expectWeightedPfs() {
        TestDataLocal td = setup();

        Test.startTest();
        SchoolSetWeightedPFS.isExecuting = false;
        SchoolSetWeightedPFS.isExecutingFromPFS = false;
        td.pfs.PFS_Status__c = 'Application Submitted';
        update td.pfs;

        td.spfsa.PFS_Status__c = 'Application Submitted';
        update td.spfsa;
        Test.stopTest();

        List<AsyncApexJob> jobs = [SELECT Id, ApexClass.Name FROM AsyncApexJob WHERE ApexClass.Name = 'PfsService'];

        List<Weighted_PFS__c> weightedPfsRecords = [SELECT
                                                           Id
                                                      FROM Weighted_PFS__c
                                                     WHERE PFS__c = :td.pfs.Id];

        System.assertEquals(1, weightedPfsRecords.size(), 'Expected there to be exactly one weighted PFS.');
        System.assertEquals(1, jobs.size(), 'Expected only one job queued.');
    }

    @isTest
    private static void queueSetWeightedPfs_duplicate_expectExactlyTwoWeightedPfsRecords() {
        TestDataLocal td = setup();

        // Set up an additional school for the applicant
        School_PFS_Assignment__c additionalSpa = td.applyToAdditionalSchool();

        Test.startTest();
        SchoolSetWeightedPFS.isExecuting = false;
        SchoolSetWeightedPFS.isExecutingFromPFS = false;
        td.pfs.PFS_Status__c = 'Application Submitted';
        update td.pfs;

        td.spfsa.PFS_Status__c = 'Application Submitted';
        update td.spfsa;

        additionalSpa.PFS_Status__c = 'Application Submitted';
        update additionalSpa;
        Test.stopTest();

        List<AsyncApexJob> jobs = [SELECT Id FROM AsyncApexJob WHERE ApexClass.Name = 'PfsService'];

        List<Weighted_PFS__c> weightedPfsRecords = [SELECT
                                                           Id
                                                      FROM Weighted_PFS__c
                                                     WHERE PFS__c = :td.pfs.Id];

        System.assertEquals(2, weightedPfsRecords.size(), 'Expected there to be exactly two weighted PFS records.');
        System.assertEquals(1, jobs.size(), 'Expected only one job queued.');
    }

    @isTest
    private static void queueSetWeightedPfs_dmlExceptionUnableToLockRowMessage_expectRequeued() {
        TestDataLocal td = setup();

        Test.startTest();
        SchoolSetWeightedPFS.isExecuting = false;
        SchoolSetWeightedPFS.isExecutingFromPFS = false;

        // Trigger a DmlException
        PfsService.exceptionToThrow = new System.DmlException(PfsService.EXPECTED_LOCKED_ROW_MESSAGE);

        td.pfs.PFS_Status__c = 'Application Submitted';
        update td.pfs;

        td.spfsa.PFS_Status__c = 'Application Submitted';
        update td.spfsa;
        Test.stopTest();

        System.assert(PfsService.requeued, 'Expected the job to be requeued');
    }

    @isTest
    private static void queueSetWeightedPfs_dmlExceptionUnexpectedErrorMessage_expectNotRequeued() {
        TestDataLocal td = setup();

        Test.startTest();
        SchoolSetWeightedPFS.isExecuting = false;
        SchoolSetWeightedPFS.isExecutingFromPFS = false;

        // Trigger a DmlException
        PfsService.exceptionToThrow = new System.DmlException('invalid message');

        td.pfs.PFS_Status__c = 'Application Submitted';
        update td.pfs;

        td.spfsa.PFS_Status__c = 'Application Submitted';
        update td.spfsa;
        Test.stopTest();

        System.assert(!PfsService.requeued, 'Expected the job to not be requeued');
    }

    @isTest
    private static void queueSetWeightedPfs_argumentException_expectNotRequeued() {
        TestDataLocal td = setup();

        Test.startTest();
        SchoolSetWeightedPFS.isExecuting = false;
        SchoolSetWeightedPFS.isExecutingFromPFS = false;

        // Trigger a DmlException
        PfsService.exceptionToThrow = new ArgumentException();

        td.pfs.PFS_Status__c = 'Application Submitted';
        update td.pfs;

        td.spfsa.PFS_Status__c = 'Application Submitted';
        update td.spfsa;
        Test.stopTest();

        System.assert(!PfsService.requeued, 'Expected the job to not be requeued');
    }
}