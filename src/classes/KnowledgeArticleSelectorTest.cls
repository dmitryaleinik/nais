@isTest
private class KnowledgeArticleSelectorTest
{

    @isTest
    private static void selectAll_noRecordsInserted_expectEmptyList()
    {
        List<KnowledgeArticle> queriedRecords = KnowledgeArticleSelector.newInstance().selectAll();

        System.assertNotEquals(null, queriedRecords, 'Expected the queried records to not be null.');
        System.assertEquals(0, queriedRecords.size(), 'Expected zero records to be queried.');
    }

    @isTest
    private static void selectAll_recordsInserted_expectRecordsQueriedInCorrectOrder()
    {
        Integer numberOfRecordsToInsert = 10;
        List<Knowledge__kav> knowledgeArticles = KnowledgeAVTestData.Instance.insertKnowledgeAVs(numberOfRecordsToInsert);
        List<KnowledgeArticle> queriedRecords = KnowledgeArticleSelector.newInstance().selectAll();

        System.assert(!queriedRecords.isEmpty(), 'Expected records to have actually been queried.');
        System.assertEquals(numberOfRecordsToInsert, queriedRecords.size(), 'Expected all inserted records to be queried.');
    }

    @isTest
    private static void selectByArticleNumber_knowledgeArticleExists_expectRecordReturned()
    {
        Knowledge__kav knowledgeArticle = KnowledgeAVTestData.Instance.DefaultKnowledgeAV;

        Test.startTest();
            List<KnowledgeArticle> knowledgeArticleRecords = KnowledgeArticleSelector.newInstance()
                .selectByArticleNumber(new Set<String>{knowledgeArticle.ArticleNumber});
        Test.stopTest();

        System.assert(!knowledgeArticleRecords.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(1, knowledgeArticleRecords.size(), 
            'Expected there to be one knowledgeArticle record returned.');
        System.assertEquals(knowledgeArticle.ArticleNumber, knowledgeArticleRecords[0].ArticleNumber,
            'Expected the Ids of the knowledgeArticle records to match.');
    }

    @isTest
    private static void selectByArticleNumber_nullSet_expectArgumentNullException()
    {
        Knowledge__kav knowledgeArticle = KnowledgeAVTestData.Instance.DefaultKnowledgeAV;

        Test.startTest();
            try
            {
                KnowledgeArticleSelector.newInstance().selectByArticleNumber(null);

                TestHelpers.expectedArgumentNullException();
            } catch (Exception e)
            {
                TestHelpers.assertArgumentNullException(e, KnowledgeArticleSelector.ARTICLE_NUMBERS_PARAM);
            }
        Test.stopTest();
    }
}