public with sharing class SchoolFamilyContribWrksheetBulkCont {

    /*Initialization*/
    // [CH] NAIS-1766 Adding to help avoid Mixed DML Errors
    public static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

    public SchoolFamilyContribWrksheetBulkCont(ApexPages.StandardSetController controller){
        setFolders = controller;
        Set<Id> folderIds = new Set<Id>();

        for ( Student_Folder__c f : (Student_Folder__c[])setFolders.getSelected() ){
            folderIds.add(f.Id);
        }

        if (folderIds.size() > 100){
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You may only print 100 Folders at a time.'));
        } else {
            printCont = new SchoolFamilyContribWrksheetPrintCont(folderIds);
        }

    }
    /*End Initialization*/

    /*Properties*/
    ApexPages.StandardSetController setFolders;
    public SchoolFamilyContribWrksheetPrintCont printCont {get; set;}

    /*End Properties*/
}