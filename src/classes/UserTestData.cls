/**
 * @description This class is used to create and insert user records in a test context.
 */
@isTest
public with sharing class UserTestData extends SObjectTestData {
    private static final String SAMPLE_EMAIL = 'sample-email@test.org';
    private static final String SAMPLE_NAME = 'MrNobody';
    private static final String DEFAULT_TIMEZONE_SID_KEY = UserInfo.getTimeZone().getId();
    private static final String DEFAULT_LOCALE_SID_KEY = 'en_US';
    private static final String DEFAULT_EMAIL_ENCODING_KEY = 'ISO-8859-1';
    private static final String DEFAULT_LANGUAGE_LOCALE_KEY = 'en_US';
    private static final String DEFAULT_USER_ALIAS = 'user';

    public enum COMMUNITY_TYPE { SCHOOL, FAMILY }

    /**
     * @description Get the default values for the User object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
            User.LastName => SAMPLE_NAME,
            User.Username => SAMPLE_EMAIL,
            User.Email => SAMPLE_EMAIL,
            User.Alias => DEFAULT_USER_ALIAS,
            User.TimeZoneSidKey => DEFAULT_TIMEZONE_SID_KEY,
            User.LocaleSidKey => DEFAULT_LOCALE_SID_KEY,
            User.EmailEncodingKey => DEFAULT_EMAIL_ENCODING_KEY,
            User.ProfileId => GlobalVariables.sysAdminProfileId,
            User.LanguageLocaleKey => DEFAULT_LANGUAGE_LOCALE_KEY,
            User.isActive => true
        };
    }

    /**
     * @description Set the Name field on the current User record.
     * @param username The Username to set on the User.
     * @return The current working instance of UserTestData.
     */
    public UserTestData forUsername(String username) {
        return (UserTestData) with(User.Username, username);
    }

    /**
     * @description Set the ProfileId field on the current User record.
     * @param profileId The ProfileId to set on the User.
     * @return The current working instance of UserTestData.
     */
    public UserTestData forProfileId(Id profileId) {
        return (UserTestData) with(User.ProfileId, profileId);
    }

    /**
     * @description Set the ContactId field on the current User record.
     * @param contactId The ContactId to set on the User.
     * @return The current working instance of UserTestData.
     */
    public UserTestData forContactId(Id contactId) {
        return (UserTestData) with(User.ContactId, contactId);
    }

    /**
     * @description Set the SYSTEM_Terms_and_Conditions_Accepted__c field on the current User record.
     * @param systemTermsAndConditionsAccepted The datetime value to set on the User.
     * @return The current working instance of UserTestData.
     */
    public UserTestData forSystemTermsAndConditionsAccepted(Datetime systemTermsAndConditionsAccepted) {
        return (UserTestData) with(User.SYSTEM_Terms_and_Conditions_Accepted__c, systemTermsAndConditionsAccepted);
    }

    /**
     * @description Creates a school portal admin user and inserts a school account and staff contact.
     * @return A user record.
     */
    public static User createSchoolPortalUser() {
        return createPortalUser(COMMUNITY_TYPE.SCHOOL);
    }

    /**
     * @description Inserts a school portal admin user and inserts a school account and staff contact.
     * @return A user record.
     */
    public static User insertSchoolPortalUser() {
        User schoolPortalUser = createSchoolPortalUser();

        // Adding this line to help avoid mixed DML errors.
        System.runAs(CurrentUser.getCurrentUser()) {
            insert schoolPortalUser;
        }

        return schoolPortalUser;
    }

    /**
     * @description Inserts account and contact records then creates a family portal user for that contact.
     * @return A user record.
     */
    public static User createFamilyPortalUser() {
        return createPortalUser(COMMUNITY_TYPE.FAMILY);
    }

    /**
     * @description Inserts a family portal user along with a contact and account.
     * @return A user record.
     */
    public static User insertFamilyPortalUser() {
        User familyPortalUser = createFamilyPortalUser();

        insert familyPortalUser;

        return familyPortalUser;
    }


    private static User createPortalUser(COMMUNITY_TYPE communityType) {
        Account account;
        Contact contact;

        if (communityType == COMMUNITY_TYPE.SCHOOL) {
            account = AccountTestData.Instance
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .DefaultAccount;

            contact = ContactTestData.Instance
                .forAccount(account.Id)
                .forRecordTypeId(RecordTypes.schoolStaffContactTypeId)
                .DefaultContact;
        }

        if (communityType == COMMUNITY_TYPE.FAMILY) {
            account = AccountTestData.Instance
                .forRecordTypeId(RecordTypes.individualAccountTypeId)
                .DefaultAccount;

            contact = ContactTestData.Instance
                .forAccount(account.Id)
                .forRecordTypeId(RecordTypes.parentContactTypeId)
                .DefaultContact;
        }

        String testEmail = generateTestEmail();

        User portalUser = new User();
        portalUser.LastName = SAMPLE_NAME;
        portalUser.Username = testEmail;
        portalUser.Email = testEmail;
        portalUser.Alias = SAMPLE_NAME;
        portalUser.TimeZoneSidKey = DEFAULT_TIMEZONE_SID_KEY;
        portalUser.LocaleSidKey = 'en_US';
        portalUser.EmailEncodingKey = 'ISO-8859-1';

        if (communityType == COMMUNITY_TYPE.SCHOOL) {
            portalUser.ProfileId = GlobalVariables.schoolPortalAdminProfileId;
        }

        if (communityType == COMMUNITY_TYPE.FAMILY) {
            portalUser.ProfileId = GlobalVariables.familyPortalProfileId;
        }

        portalUser.LanguageLocaleKey = 'en_US';
        portalUser.ContactId = contact.Id;
        portalUser.isActive = true;

        return portalUser;
    }

    /**
     * @description Insert the current working User record.
     * @return The currently operated upon User record.
     */
    public User insertUser() {
        return (User)insertRecord();
    }

    /**
     * @description Create the current working User record without resetting
     *             the stored values in this instance of UserTestData.
     * @return A non-inserted User record using the currently stored field
     *             values.
     */
    public User createUserWithoutReset() {
        return (User)buildWithoutReset();
    }

    /**
     * @description Create the current working User record.
     * @return The currently operated upon User record.
     */
    public User create() {
        return (User)super.buildWithReset();
    }

    /**
     * @description The default User record.
     */
    public User DefaultUser {
        get {
            if (DefaultUser == null) {
                DefaultUser = createUserWithoutReset();
                insert DefaultUser;
            }
            return DefaultUser;
        }
        private set;
    }

    @testVisible
    private static String generateTestEmail() {
        return SAMPLE_EMAIL + String.valueOf(Math.random()) + String.valueOf(Math.random());
    }

    /**
     * @description Get the User SObjectType.
     * @return The User SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return User.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static UserTestData Instance {
        get {
            if (Instance == null) {
                Instance = new UserTestData();
            }
            return Instance;
        }
        private set;
    }

    private UserTestData() { }
}