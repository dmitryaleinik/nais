@isTest
private class CustomDebugServiceTest {
    private static final String GENERIC_MESSAGE = 'Gen message';

    private static Payment_Provider__mdt getPaymentProviderRecord() {
        List<Payment_Processor__mdt> paymentProcessors = PaymentProcessorSelector.Instance
                .selectByName(new Set<String> { PaymentProcessor.PAYMENT_PROCESSOR_NAME });

        System.assert(!paymentProcessors.isEmpty(), 'Expected a Payment Processor record to be returned.');
        System.assertEquals(1, paymentProcessors.size(), 'Expected there to be exactly 1 Payment Processor record.');

        String providerName = paymentProcessors[0].Payment_Provider__c;
        if (providerName == null) {
            throw new ConfigurationException('No Payment Provider found.');
        }

        List<Payment_Provider__mdt> paymentProviders = PaymentProviderSelector.Instance
                .selectByName(new Set<String> { providerName });
        if (paymentProviders == null || paymentProviders.isEmpty()) {
            throw new ConfigurationException('No Payment Provider found.');
        }
        return paymentProviders[0];
    }

    @isTest
    private static void logException_nullException_expectNullRecord() {
        Test.startTest();
        Custom_Debug__c customDebug = CustomDebugService.Instance.logException(null);
        Test.stopTest();

        System.assertEquals(null, customDebug, 'Expected a null Custom_Debug__c record.');
    }

    @isTest
    private static void logException_expectRecord() {
        Payment_Provider__mdt providerRecord = getPaymentProviderRecord();

        Custom_Debug__c expectedDebugRecord;

        Test.startTest();
        expectedDebugRecord = CustomDebugService.Instance.logException(new ArgumentException(GENERIC_MESSAGE));
        Test.stopTest();

        // Because we're dealing with Custom Metadata Types which are visible
        // to unit tests we can't test each individually explicitly. Instead
        // this determines which use case to assert for.
        if (providerRecord.Custom_Debug__c == true) {
            System.assertNotEquals(null, expectedDebugRecord, 'Expected the record to not be null.');
            System.assert(expectedDebugRecord.Message__c.contains(GENERIC_MESSAGE), 'Expected the message.');
            System.assert(String.isNotBlank(expectedDebugRecord.Type__c));
            System.assert(String.isNotBlank(expectedDebugRecord.Stack_Trace__c));
        } else {
            System.assertEquals(null, expectedDebugRecord, 'Expected the record to be null since debugging is off.');
        }
    }
}