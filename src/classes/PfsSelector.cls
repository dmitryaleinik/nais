/**
 * @description Query selector for PFS records.
 */
public class PfsSelector extends fflib_SObjectSelector {
    @testVisible private static final String PFS_IDS_PARAM  = 'pfsIds';
    @testVisible private static final String PFS_FIELDS_TO_SELECT_PARAM = 'fieldsToSelect';
    @testVisible private static final String CONTACT_ID_PARAM  = 'contactId';
    @testVisible private static final String CONTACT_IDS_PARAM = 'contactIds';
    @testVisible private static final String ACADEMIC_YEAR_ID_PARAM  = 'academicYearId';
    @testVisible private static final String ACADEMIC_YEARS_PARAM = 'academicYears';

    private static final String APPLICATION_SUBMITTED = 'Application Submitted';
    private static final String UNPAID = 'Unpaid';

    private static final String PAYMENT_OPPORTUNITY_STAGENAME_OPEN = 'Open';
    private static final String PAYMENT_OPPORTUNITY_STAGENAME_WRITTEN_OFF = 'Written Off';

    // Relations
    private static final String OPPORTUNITY_RELATION = 'Opportunities__r';

    /**
     * @description Select PFS records by their Ids.
     * @param pfsIds The set of Ids to select PFS records by.
     * @returns A list of PFS records associated with the set of Ids
     *          provided.
     * @throws An ArgumentNullException if pfsIds is null.
     */
    public List<PFS__c> selectByIdWithOpportunities(Set<Id> pfsIds) {
        ArgumentNullException.throwIfNull(pfsIds, PFS_IDS_PARAM);

        assertIsAccessible();
        if (OpportunitySelector.Instance.isEnforcingCRUD()) {
            OpportunitySelector.Instance.assertIsAccessible();
        }

        return Database.query(String.format('SELECT {0}, (SELECT {1} FROM {2}) FROM PFS__c WHERE Id IN :pfsIds ORDER BY {3} ASC',
                new List<String> { getFieldListString(), OpportunitySelector.Instance.getFieldListString(), OPPORTUNITY_RELATION, getOrderBy() }));
    }

    /**
     * @description Select PFS records and their associated Opportunities by
     *              by Parent A Contact Ids. The Opportunities must be pfs
     *              application fees with a stage of open or written off and
     *              not closed to be returned with the PFS records found.
     * @param contactIds The Ids of the Parent A Contact records to query for
     *        PFS records by.
     * @param academicYears The Academic Year names to query PFS records by.
     * @return A list of PFS Records.
     * @throws An ArgumentNullException if contactIds is null.
     */
    public List<PFS__c> selectWithOpportunitiesByParentAAndAcademicYears(Set<Id> contactIds, Set<String> academicYears) {
        ArgumentNullException.throwIfNull(contactIds, CONTACT_IDS_PARAM);
        ArgumentNullException.throwIfNull(academicYears, ACADEMIC_YEARS_PARAM);

        assertIsAccessible();
        OpportunitySelector oppSelector = OpportunitySelector.Instance;
        if (oppSelector.isEnforcingCRUD()) {
            oppSelector.assertIsAccessible();
        }

        String query = 'SELECT {0}, (SELECT {1} FROM {2}) FROM {3} WHERE Parent_A__c IN :contactIds ' +
                'AND Academic_Year_Picklist__c IN :academicYears ORDER BY {4}';

        return Database.query(String.format(query, new List<String> {
                getFieldListString(),
                oppSelector.getFieldListString(),
                OPPORTUNITY_RELATION,
                getSObjectName(),
                getOrderBy()
        }));
    }

    /**
     * @description Selects Unpaid but submitted PFS by Contact Id.
     * @param contactIds The Contact Ids to query for unpaid, submitted PFS records for.
     * @return A list of PFS records.
     * @throws An ArgumentNullException if contactIds is null.
     */
    public List<PFS__c> selectByContactIdSubmittedUnpaid(Set<Id> contactIds) {
        ArgumentNullException.throwIfNull(contactIds, CONTACT_IDS_PARAM);

        assertIsAccessible();

        return Database.query(String.format('SELECT {0} FROM {1} WHERE Parent_A__c IN :contactIds AND ' +
                'PFS_Status__c = :APPLICATION_SUBMITTED AND Payment_Status__c = :UNPAID ORDER BY ' +
                'Academic_Year_Picklist__c DESC',
                new List<String> {
                        getFieldListString(),
                        getSObjectName()
                }));
    }

    /**
     * @description Selects PFS records by Id.
     * @param pfsIds The Pfs Ids to query for PFS records.
     * @return A list of PFS records.
     * @throws An ArgumentNullException if pfsIds is null.
     */
    public List<PFS__c> selectById(Set<Id> pfsIds) {
        ArgumentNullException.throwIfNull(pfsIds, PFS_IDS_PARAM);

        assertIsAccessible();

        String pfsQuery = newQueryFactory()
                .setCondition('Id IN :pfsIds')
                .toSOQL();

        return Database.query(pfsQuery);
    }

    /**
     * @description Selects PFS records by Id with custom fields mentioned in the passed parameter.
     * @param pfsIds The Pfs Ids to query for PFS records.
     * @param fieldsToSelect The Pfs fields to query.
     * @return A list of PFS records.
     * @throws An ArgumentNullException if pfsIds is null.
     * @throws An ArgumentNullException if fieldsToSelect is null.
     */
    public List<PFS__c> selectWithCustomFieldListById(Set<Id> pfsIds, List<String> fieldsToSelect) {
        ArgumentNullException.throwIfNull(pfsIds, PFS_IDS_PARAM);
        ArgumentNullException.throwIfNull(fieldsToSelect, PFS_FIELDS_TO_SELECT_PARAM);

        assertIsAccessible();

        String pfsQuery = newQueryFactory(false)
                .selectFields(fieldsToSelect)
                .setCondition('Id IN :pfsIds')
                .toSOQL();

        return Database.query(pfsQuery);
    }

    /**
     * @description Selects All PFS records with custom fields mentioned in the passed parameter.
     * @param fieldsToSelect The Pfs fields to query.
     * @return A list of PFS records.
     * @throws An ArgumentNullException if fieldsToSelect is null.
     */
    public List<PFS__c> selectAllWithCustomFieldList(List<String> fieldsToSelect) {
        ArgumentNullException.throwIfNull(fieldsToSelect, PFS_FIELDS_TO_SELECT_PARAM);

        assertIsAccessible();

        String pfsQuery = newQueryFactory(false)
                .selectFields(fieldsToSelect)
                .toSOQL();

        return Database.query(pfsQuery);
    }

    /**
     * @description Selects PFS records by Ids and locks them for update.
     * @param pfsIds The Pfs Ids to query for PFS records.
     * @return A list of PFS records.
     * @throws An ArgumentNullException if pfsIds is null.
     */
    public List<PFS__c> selectByIdForUpdate(Set<Id> pfsIds) {
        ArgumentNullException.throwIfNull(pfsIds, PFS_IDS_PARAM);

        assertIsAccessible();

        String notFeeWaived = 'No';

        return Database.query(String.format(
            'SELECT {0} FROM {1} WHERE Id IN :pfsIds AND (Fee_Waived__c = null OR Fee_Waived__c = :notFeeWaived) FOR UPDATE',
                new List<String> {
                        getFieldListString(),
                        getSObjectName()
                }));
    }

    /**
     * @description Selects PFS records by ContactId and AcademicYearId.
     * @param contactId The Pfs Ids to query for PFS records.
     * @param academicYearId The Pfs fields to query.
     * @return A list of PFS records.
     * @throws An ArgumentNullException if contactId is null.
     * @throws An ArgumentNullException if academicYearId is null.
     */
    public List<PFS__c> selectByContactIdAndAcademicYearId(Id contactId, Id academicYearId) {
        ArgumentNullException.throwIfNull(contactId, CONTACT_ID_PARAM);
        ArgumentNullException.throwIfNull(academicYearId, ACADEMIC_YEAR_ID_PARAM);

        assertIsAccessible();


        Academic_Year__c academicYear = GlobalVariables.getAcademicYear(academicYearId);

        if (academicYear != null) {
            String academicYearName = academicYear.Name;

            String pfsQuery = newQueryFactory()
                    .setCondition('Parent_A__c = :contactId AND Academic_Year_Picklist__c = :academicYearName')
                    .toSOQL();
            return Database.query(pfsQuery);
        }

        return null;
    }

    /**
     * @description Selects PFS records by PFS_Number__c.
     * @param pfsNumbers The PFS_Number__c values to query for PFS records.
     * @param fieldsToSelect The Pfs fields to query.
     * @return A list of PFS records.
     * @throws An ArgumentNullException if pfsNumbers is null.
     * @throws An ArgumentNullException if fieldsToSelect is null.
     */
    public List<PFS__c> selectByPfsNumber(Set<String> pfsNumbers, Set<String> fieldsToSelect) {
        
        ArgumentNullException.throwIfNull(pfsNumbers, 'pfsNumbers');
        ArgumentNullException.throwIfNull(fieldsToSelect, 'fieldsToSelect');
        
        assertIsAccessible();
        
        String pfsQuery = newQueryFactory()
	        .selectFields(fieldsToSelect)
	        .setCondition('PFS_Number_Searchable__c IN :pfsNumbers AND PFS_Number_Searchable__c <> NULL')
	        .toSOQL();

        return Database.query(pfsQuery);
    }
    
    private Schema.SObjectType getSObjectType() {
        return PFS__c.getSObjectType();
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                PFS__c.Academic_Year_Picklist__c,
                PFS__c.Fee_Waived__c,
                PFS__c.Id,
                PFS__c.Name,
                PFS__c.Parent_A__c,
                PFS__c.Parent_A_Address__c,
                PFS__c.Parent_A_City__c,
                PFS__c.Parent_A_Country__c,
                PFS__c.Parent_A_Email__c,
                PFS__c.Parent_A_First_Name__c,
                PFS__c.Parent_A_Last_Name__c,
                PFS__c.Parent_A_State__c,
                PFS__c.Parent_A_ZIP_Postal_Code__c,
                PFS__c.Payment_Status__c,
                PFS__c.PFS_Number__c,
                PFS__c.PFS_Status__c,
                PFS__c.systemLastScreenParam__c,
                PFS__c.MIE_Requested__c,
                PFS__c.MIE_Requested_By_School_Id__c,
                PFS__c.MIE_Requested_By_School_Name__c,
                PFS__c.MIE_Requested_Date__c
        };
    }

    /**
     * @description Creates a new instance of the PfsSelector.
     * @return An instance of PfsSelector.
     */
    public static PfsSelector newInstance() {
        return new PfsSelector();
    }

    /**
     * @description Singleton property ensuring external sources do not
     *              instantiate this directly.
     */
    public static PfsSelector Instance {
        get {
            if (Instance == null) {
                Instance = new PfsSelector();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to enforce singleton access
     *              via the Instance property.
     */
    private PfsSelector() {}
}