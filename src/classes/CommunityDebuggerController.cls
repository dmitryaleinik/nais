/**
 * @description Used to get uncaught exceptions from communities. Uncaught exceptions are not typically written to the
 *              debug logs in the context of communities.
 *              To use, navigate to <community_url/CommunityDebugger/page=<pagename> and add any additional
 *              page parameters. OA-68.
 */
public with sharing class CommunityDebuggerController {
    public String failingPageResponse { get; set; }

    public String toLoad {get; private set;}
    public Map<String, String> params {get; private set;}
    public String queryString = '?';

    public CommunityDebuggerController() {
        params = ApexPages.currentPage().getParameters();
        toLoad = (String) params.get('pageName');
        params.remove('page');

        system.debug('params' + params);

        boolean first = true;
        for (String key : params.keyset()) {
            if(first){
                queryString = queryString + key + '=' + params.get(key);
                first = false;
            }
            else
                    queryString = queryString + '&' + key + '=' + params.get(key);
        }
    }

    public void fetchFailingPage() {
        try {
            system.debug('Loading this url: ' + toLoad +  queryString);
            PageReference fail = new PageReference('/' + toLoad + queryString);
            failingPageResponse = fail.getContent().toString();
        } catch (Exception e) {
            failingPageResponse = e.getTypeName() + ' : ' + e.getMessage() + ' : ' + e.getStackTraceString();
        }
    }
}