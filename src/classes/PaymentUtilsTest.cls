@isTest
private class PaymentUtilsTest
{

    @isTest
    private static void testInsertExtraFeeWaivers()
    {
        Transaction_Settings__c setting = new Transaction_Settings__c();
        setting.Name = 'Extra Fee Waivers';
        setting.Account_Code__c = '5950';
        setting.Transaction_Code__c = '1230';
        setting.Account_Label__c = 'Other Income';
        insert setting;
        
        Transaction_Annual_Settings__c annualSetting = new Transaction_Annual_Settings__c();
        annualSetting.Name = GlobalVariables.getCurrentYearString().left(4);
        annualSetting.Quantity__c = 5;
        annualSetting.Year__c = GlobalVariables.getCurrentYearString().left(4);
        insert annualSetting;
        
        Academic_Year__c newAcademicYear = new Academic_Year__c();
        newAcademicYear.Name = GlobalVariables.getCurrentYearString();
        insert newAcademicYear;
        
        Test.startTest();
            Opportunity newOpportunity = new Opportunity();
            newOpportunity.Name = 'test opp';
            newOpportunity.Product_Quantity__c = 5;
            newOpportunity.RecordTypeId = RecordTypes.opportunityFeeWaiverPurchaseTypeId;
            newOpportunity.Academic_Year_Picklist__c = newAcademicYear.Name;
            newOpportunity.StageName = 'Closed';
            newOpportunity.CloseDate = Date.today();
            insert newOpportunity;

            PaymentUtils.insertTransactionLineItemsFromOpportunities(new List<Opportunity>{newOpportunity}); // [DP] 07.29.2015 NAIS-2469
            
            List<Transaction_Line_Item__c> createdLineItems = 
                [select Id, Transaction_Date__c, Waiver_Quantity__c, Transaction_Code__c, Account_Code__c, Account_Label__c,
                    Product_Amount__c, Product_Code__c
                    from Transaction_Line_Item__c
                    where Opportunity__c = :newOpportunity.Id
                    and RecordTypeId = :RecordTypes.saleTransactionTypeId];
                    
            System.assertNotEquals(null, createdLineItems);
            System.assertEquals(1, createdLineItems.size());
            
            Transaction_Line_Item__c lineItem = createdLineItems[0];
            System.assertEquals(Date.today(), lineItem.Transaction_Date__c);
            System.assertEquals(newOpportunity.Product_Quantity__c, lineItem.Waiver_Quantity__c);
            System.assertEquals(setting.Transaction_Code__c, lineItem.Transaction_Code__c);
            System.assertEquals(setting.Account_Code__c, lineItem.Account_Code__c);
            System.assertEquals(setting.Account_Label__c, lineItem.Account_Label__c);
            
            System.assertEquals(String.valueOf(annualSetting.Product_Amount__c), lineItem.Product_Amount__c);
            System.assertEquals(annualSetting.Product_Code__c, lineItem.Product_Code__c);
            
        Test.stopTest();
    }

    @isTest
    private static void testInsertSubscriptionWaivers()
    {
        Academic_Year__c newAcademicYear = new Academic_Year__c();
        newAcademicYear.Name = GlobalVariables.getCurrentYearString();
        insert newAcademicYear;
        
        Transaction_Settings__c setting = new Transaction_Settings__c();
        setting.Name = 'School Subscription';
        setting.Account_Code__c = '5100';
        setting.Transaction_Code__c = '1210';
        setting.Account_Label__c = 'Subscription Revenue';
        insert setting;
        
        Transaction_Annual_Settings__c transAnnualSetting = new Transaction_Annual_Settings__c();
        transAnnualSetting.Product_Amount__c = 200.00;
        transAnnualSetting.Product_Code__c = 'Test Product Code';
        transAnnualSetting.Year__c = newAcademicYear.Name.left(4);
        transAnnualSetting.Name = 'Sample Name';
        transAnnualSetting.Subscription_Type__c = 'Sub Type';
        insert transAnnualSetting;
        
        Subscription_Settings__c sSettings = new Subscription_Settings__c();
        sSettings.Name = 'Subscription Settings';
        sSettings.Start_Month__c = 8;
        sSettings.Start_Day__c = 1;
        sSettings.Number_of_Months__c = 12;
        sSettings.Number_of_Months_Extended__c = 36;
        sSettings.Cutoff_Month__c = 3;
        insert sSettings;
                
        Account newAccount = TestUtils.createAccount('Test Account', RecordTypes.accessOrgAccountTypeId, 5, false);
        newAccount.SSS_Subscription_Type_Current__c = 'Sub Type';
        insert newAccount;
        
        Test.startTest();
            Opportunity newOpportunity = new Opportunity();
            newOpportunity.Name = 'test opp';
            newOpportunity.AccountId = newAccount.Id;
            newOpportunity.RecordTypeId = RecordTypes.opportunitySubscriptionFeeTypeId;
            newOpportunity.Academic_Year_Picklist__c = newAcademicYear.Name;
            newOpportunity.StageName = 'Closed';
            newOpportunity.CloseDate = Date.today();
            newOpportunity.Subscription_Type__c = 'Sub Type';
            insert newOpportunity;
            
            PaymentUtils.insertTransactionLineItemsFromOpportunities(new List<Opportunity>{newOpportunity}); // [DP] 07.29.2015 NAIS-2469
            
            List<Transaction_Line_Item__c> createdLineItems = 
                [select Id, Transaction_Date__c, Waiver_Quantity__c, Transaction_Code__c, Account_Code__c, Account_Label__c,
                    Product_Amount__c, Product_Code__c
                    from Transaction_Line_Item__c
                    where Opportunity__c = :newOpportunity.Id
                    and RecordTypeId = :RecordTypes.saleTransactionTypeId];
                    
            System.assertNotEquals(null, createdLineItems);
            System.assertEquals(1, createdLineItems.size());
            
            Transaction_Line_Item__c lineItem = createdLineItems[0];
            System.assertEquals(Date.today(), lineItem.Transaction_Date__c);
            System.assertEquals(setting.Transaction_Code__c, lineItem.Transaction_Code__c);
            System.assertEquals(setting.Account_Code__c, lineItem.Account_Code__c);
            System.assertEquals(setting.Account_Label__c, lineItem.Account_Label__c);
            
            System.assertEquals(String.valueOf(transAnnualSetting.Product_Amount__c), lineItem.Product_Amount__c);
            System.assertEquals(transAnnualSetting.Product_Code__c, lineItem.Product_Code__c);
            
        Test.stopTest();
    }

    @isTest
    private static void testCreateCreditAdjustments()
    {
        Transaction_Settings__c settingRecord = new Transaction_Settings__c();
        settingRecord.Name = 'School Courtesy Credit';
        settingRecord.Account_Code__c = '1234';
        settingRecord.Account_Label__c = 'Credit Adjustment';
        settingRecord.Transaction_Code__c = '4321';
        insert settingRecord;
        
        Account testAccount = TestUtils.createAccount('Test', RecordTypes.accessOrgAccountTypeId, 1, true);
        Academic_Year__c testAcademicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        
        Opportunity newOpp = new Opportunity();
        newOpp.Academic_Year_Picklist__c = GlobalVariables.getCurrentAcademicYear().Name;
        newOpp.AccountId = testAccount.Id;
        newOpp.RecordTypeId = RecordTypes.opportunityFeeWaiverPurchaseTypeId;
        newOpp.Name = testAccount.Name + ' - Fee Waivers';
        newOpp.StageName = 'Closed';
        newOpp.CloseDate = Date.today();
        // Note: This should not trigger the creation of a TLI record because
        //          the Product_Quantity__c is being left null
        Database.insert(newOpp);
        
           // Create a list of Subscription Line Items
        Transaction_Line_Item__c item1 = new Transaction_Line_Item__c();
        item1.RecordTypeId = RecordTypes.saleTransactionTypeId;
        item1.Opportunity__c = newOpp.Id;
        item1.Transaction_Status__c = 'Posted';
        item1.Transaction_Date__c = System.today();
        item1.Transaction_Type__c = 'School Subscription';
        item1.Product_Amount__c = '0';
        item1.Transaction_Code__c = '9999';
        item1.Account_Code__c = '5678';
        item1.Account_Label__c = 'Subscription';
        item1.Amount__c = 500;
        
        Transaction_Line_Item__c item2 = new Transaction_Line_Item__c();
        item2.RecordTypeId = RecordTypes.saleTransactionTypeId;
        item2.Opportunity__c = newOpp.Id;
        item2.Transaction_Status__c = 'Posted';
        item2.Transaction_Date__c = System.today();
        item2.Transaction_Type__c = 'School Subscription';
        item2.Product_Amount__c = '0';
        item2.Transaction_Code__c = '9999';
        item2.Account_Code__c = '5678';
        item2.Account_Label__c = 'Subscription';
        item2.Amount__c = 500;
        
        Database.insert(new List<Transaction_Line_Item__c>{item1, item2});
        
        PaymentUtils.createCreditAdjustmentLineItems(new List<Transaction_Line_Item__c>{item1, item2});
        
        List<Transaction_Line_Item__c> createdAdjustments = [select Id, RecordTypeId, Opportunity__c, Transaction_Status__c, Transaction_Date__c,
                                                                Transaction_Type__c, Product_Amount__c, Transaction_Code__c, Account_Code__c, Account_Label__c,
                                                                Amount__c
                                                                from Transaction_Line_Item__c 
                                                                where RecordTypeId = :RecordTypes.adjustmentTransactionTypeId
                                                                and Opportunity__c = :newOpp.Id];
                                                                
        System.assertEquals(2, createdAdjustments.size());
        
        for(Transaction_Line_Item__c lineItem : createdAdjustments){
            System.assertEquals(lineItem.Transaction_Type__c, 'School Courtesy Credit');
            System.assertEquals(lineItem.Amount__c, 500);
            System.assertEquals(lineItem.Account_Code__c, settingRecord.Account_Code__c);
            System.assertEquals(lineItem.Account_Label__c, settingRecord.Account_Label__c);
            System.assertEquals(lineItem.Transaction_Code__c, settingRecord.Transaction_Code__c);
        }
    }
}