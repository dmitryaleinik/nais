@isTest
private class FamilyPortalSettingsTest {
    
    @isTest
    private static void testFields() {
        
        FamilyPortalSettings.X4506_T_Sample_Link = 'http://test.com';
        FamilyPortalSettings.Adjusment_High_Percentage = 0.75;
        FamilyPortalSettings.CC_Amex = true;
        FamilyPortalSettings.CC_Discover = true;
        FamilyPortalSettings.CC_MasterCard = true;
        FamilyPortalSettings.CC_Visa = true;
        FamilyPortalSettings.Default_EZ_Household_Assets = 30000;
        FamilyPortalSettings.Disable_Auto_Pop_Dependents = false;
        FamilyPortalSettings.Early_Access_Start_Date = DateTime.parse('10/31/2017 12:40 PM');
        FamilyPortalSettings.Individual_Account_Owner_Id = UserInfo.getUserId();
        FamilyPortalSettings.In_Pilot = false;
        FamilyPortalSettings.Parent_A_Total_Salary = 600;
        FamilyPortalSettings.Parent_B_Total_Salary = 600;
        FamilyPortalSettings.Override_Today = system.today();
        
        system.assertEquals(true, FamilyPortalSettings.Record != null);
        system.assertEquals(true, FamilyPortalSettings.X4506_T_Sample_Link != null);
        system.assertEquals(true, FamilyPortalSettings.Adjusment_High_Percentage != null);
        system.assertEquals(true, FamilyPortalSettings.CC_Amex != null);
        system.assertEquals(true, FamilyPortalSettings.CC_Discover != null);
        system.assertEquals(true, FamilyPortalSettings.CC_MasterCard != null);
        system.assertEquals(true, FamilyPortalSettings.CC_Visa != null);
        system.assertEquals(true, FamilyPortalSettings.Default_EZ_Household_Assets != null);
        system.assertEquals(true, FamilyPortalSettings.Disable_Auto_Pop_Dependents != null);
        system.assertEquals(true, FamilyPortalSettings.Early_Access_Start_Date != null);
        system.assertEquals(true, FamilyPortalSettings.Individual_Account_Owner_Id != null);
        system.assertEquals(true, FamilyPortalSettings.In_Pilot != null);
        system.assertEquals(true, FamilyPortalSettings.Parent_A_Total_Salary != null);
        system.assertEquals(true, FamilyPortalSettings.Parent_B_Total_Salary != null);
        system.assertEquals(true, FamilyPortalSettings.Override_Today != null);
    }
}