/**
* @SchoolGlobalProJudgmentBatchUpdate.cls
* @date 6/4/2015
* @author CH for Exponent Partners
* @description NAIS-2389 Processes SPA record updates in bulk for changes made in the Professional Judgment page
*/

global class SchoolGlobalProJudgmentBatchUpdate implements Database.Batchable<sObject> {
    
    global List<School_PFS_Assignment__c> recordsToProcess { get; set; }
    
    global SchoolGlobalProJudgmentBatchUpdate() {
    }
    
    global List<School_PFS_Assignment__c> start(Database.BatchableContext bc) {
        return recordsToProcess;
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        update scope;
    }
    
    global void finish(Database.BatchableContext bc) {
    }
}