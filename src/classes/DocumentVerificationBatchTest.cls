@isTest
private class DocumentVerificationBatchTest {

    private static List<School_Document_Assignment__c> queryDocumentAssignments(Set<Id> recordIds) {
        return [SELECT Id, Verification_Request_Status__c, Verification_Request_Error__c FROM School_Document_Assignment__c WHERE Id IN :recordIds];
    }

    private static List<School_PFS_Assignment__c> insertPfsAssignmentsForSchools(List<Account> schools) {
        List<School_PFS_Assignment__c> pfsAssignments = new List<School_PFS_Assignment__c>();

        for (Account school : schools) {
            School_PFS_Assignment__c newRecord = SchoolPfsAssignmentTestData.Instance
                    .forSchoolId(school.Id)
                    .forAcademicYearPicklist('2017-2018')
                    .createSchoolPfsAssignmentWithoutReset();
            pfsAssignments.add(newRecord);
        }

        if (!pfsAssignments.isEmpty()) {
            DML.WithoutSharing.insertObjects(pfsAssignments);
        }

        return pfsAssignments;
    }

    private static List<School_Document_Assignment__c> insertDocAssignmentsForSchools(SchoolDocumentAssignmentTestData recordTestData, List<School_PFS_Assignment__c> pfsAssignments) {
        List<School_Document_Assignment__c> documentAssignments = new List<School_Document_Assignment__c>();

        for (School_PFS_Assignment__c spa : pfsAssignments) {
            School_Document_Assignment__c newRecord =
                    recordTestData.forPfsAssignment(spa.Id).createSchoolDocumentAssignmentWithoutReset();
            documentAssignments.add(newRecord);
        }

        if (!documentAssignments.isEmpty()) {
            DML.WithoutSharing.insertObjects(documentAssignments);
        }

        return documentAssignments;
    }

    private static void assertSdaRequestValues(List<School_Document_Assignment__c> documentAssignments, String expectedStatus, String expectedError) {
        for (School_Document_Assignment__c documentAssignment : documentAssignments) {
            System.assertEquals(expectedStatus, documentAssignment.Verification_Request_Status__c, 'Expected the correct request status.');
            System.assertEquals(expectedError, documentAssignment.Verification_Request_Error__c, 'Expected the correct request errors.');
        }
    }

    @isTest
    private static void execute_scheduledBatch_docsForMultipleSchools_featureDisabled_expectRecordsNotUpdated() {
        Integer numberOfSchools = 3;
        List<Account> schools = AccountTestData.Instance.forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccounts(numberOfSchools);

        List<School_PFS_Assignment__c> pfsAssignments = insertPfsAssignmentsForSchools(schools);

        Family_Document__c document = FamilyDocumentTestData.Instance
                .forDocumentType('W2')
                .withImportId('1234-1234-1234-1234-1234')
                .insertFamilyDocument();

        String requestStatus = SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE;
        SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(requestStatus)
                .asType('W2')
                .forDocumentYear('2017');

        List<School_Document_Assignment__c> documentAssignments =
                insertDocAssignmentsForSchools(SchoolDocumentAssignmentTestData.Instance, pfsAssignments);
        Map<Id, School_Document_Assignment__c> documentAssignmentsByIds =
                new Map<Id, School_Document_Assignment__c>(documentAssignments);

        documentAssignments = queryDocumentAssignments(documentAssignmentsByIds.keySet());
        assertSdaRequestValues(documentAssignments, requestStatus, null);

        Test.startTest();
        FeatureToggles.setToggle('Verify_Existing_Documents__c', false);
        Database.executeBatch(new DocumentVerificationBatch());
        Test.stopTest();

        documentAssignments = queryDocumentAssignments(documentAssignmentsByIds.keySet());
        assertSdaRequestValues(documentAssignments, SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE, null);
    }

    @isTest
    private static void execute_scheduledBatch_docsForMultipleSchools_featureEnabled_expectRecordsUpdated() {
        Integer numberOfSchools = 3;
        List<Account> schools = AccountTestData.Instance.forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccounts(numberOfSchools);

        List<School_PFS_Assignment__c> pfsAssignments = insertPfsAssignmentsForSchools(schools);

        Family_Document__c document = FamilyDocumentTestData.Instance
                .forDocumentType('W2')
                .withImportId('1234-1234-1234-1234-1234')
                .insertFamilyDocument();

        String requestStatus = SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE;
        SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(requestStatus)
                .asType('W2')
                .forDocumentYear('2017');

        List<School_Document_Assignment__c> documentAssignments =
                insertDocAssignmentsForSchools(SchoolDocumentAssignmentTestData.Instance, pfsAssignments);
        Map<Id, School_Document_Assignment__c> documentAssignmentsByIds =
                new Map<Id, School_Document_Assignment__c>(documentAssignments);

        documentAssignments = queryDocumentAssignments(documentAssignmentsByIds.keySet());
        assertSdaRequestValues(documentAssignments, requestStatus, null);

        Test.startTest();
        FeatureToggles.setToggle('Verify_Existing_Documents__c', true);
        Database.executeBatch(new DocumentVerificationBatch());
        Test.stopTest();

        documentAssignments = queryDocumentAssignments(documentAssignmentsByIds.keySet());
        assertSdaRequestValues(documentAssignments, SchoolDocumentAssignments.VERIFICATION_REQUEST_COMPLETE, null);
    }
}