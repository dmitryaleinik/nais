@isTest
private class FamilyAppDependentsContTest 
{
    
    @isTest 
    private static void testDependentNumber()
    {
        FamilyAppDependentsCont controller = new FamilyAppDependentsCont();
        Integer depNumber = 5;
        
        Test.startTest();
            controller.dependentNumber = depNumber;
        Test.stopTest();
        
        System.assertEquals(depNumber, controller.dependentNumber);
    }

    @isTest 
    private static void testAddDependent()
    {
        FamilyAppDependentsCont controller = new FamilyAppDependentsCont();
        controller.pfs = new Pfs__c();
        controller.dependentItem = new Dependents__c();
        System.assertEquals(null, controller.pfs.Number_of_Dependent_Children__c);
        System.assertEquals(null, controller.dependentItem.Number_of_Dependents__c);

        Test.startTest();
            controller.addDependent();
            System.assertEquals(1, controller.pfs.Number_of_Dependent_Children__c);
            System.assertEquals(1, controller.dependentItem.Number_of_Dependents__c);

            controller.addDependent();
            System.assertEquals(2, controller.pfs.Number_of_Dependent_Children__c);
            System.assertEquals(2, controller.dependentItem.Number_of_Dependents__c);     
        Test.stopTest();
    }
    
    @isTest 
    private static void testDeleteDependent()
    {
        FamilyAppDependentsCont controller = new FamilyAppDependentsCont();
        controller.pfs = new Pfs__c(Number_of_Dependent_Children__c = 7);
        controller.dependentItem = new Dependents__c(Number_of_Dependents__c = 7);
        controller.dependentNumber = 6;

        Test.startTest();
            controller.deleteDependent();
            ////expected that both 6th and empty 7th(additional) dependents will be removed.
            System.assertEquals(5, controller.pfs.Number_of_Dependent_Children__c);
            System.assertEquals(5, controller.dependentItem.Number_of_Dependents__c);

            controller.pfs.Number_of_Dependent_Children__c = 7;
            controller.dependentItem.Number_of_Dependents__c = 7;
            controller.dependentItem.Additional_Dependents__c = 'Test';
            controller.dependentNumber = 7;
            controller.deleteDependent();
            ////expected that only 7th(additional) dependent will be removed.
            System.assertEquals(6, controller.pfs.Number_of_Dependent_Children__c);
            System.assertEquals(6, controller.dependentItem.Number_of_Dependents__c);
            System.assertEquals(null, controller.dependentItem.Additional_Dependents__c);

            controller.pfs.Number_of_Dependent_Children__c = 7;
            controller.dependentItem.Number_of_Dependents__c = 7;
            controller.dependentItem.Dependent_1_Full_Name__c = 'Test User';
            controller.dependentItem.Additional_Dependents__c = 'Test Additional';
            controller.dependentNumber = 1;
            controller.deleteDependent();
            ////expected that only 1st dependent will be removed.
            System.assertEquals(6, controller.pfs.Number_of_Dependent_Children__c);
            System.assertEquals(6, controller.dependentItem.Number_of_Dependents__c);
            System.assertEquals(null, controller.dependentItem.Dependent_1_Full_Name__c);
            System.assertEquals('Test Additional', controller.dependentItem.Additional_Dependents__c);
        Test.stopTest();
    }
}