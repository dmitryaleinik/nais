/**
 * Test Client for the TransactionLineItemAfter trigger
 * Nathan Shinn, Exponent Partners, 2013 
 */
@isTest
private class TransactionLineItemAfterTest
{
 
 /* [SL] NAIS-943 this logic moved to OpportunityAction, with unit tests in OpportunityActionTest
    @isTest
    private static void runTest() {
        setupTestData();
        
        //Execute the trigger by inserting Transaction_Line_Item__c records
        // Transaction Line Item
        Map<String, Schema.RecordTypeInfo> tliTypeMap = Transaction_Line_Item__c.SObjectType.getDescribe().getRecordTypeInfosByName();
        Id tliTypeId = tliTypeMap.get('Payment').RecordTypeId;
        Transaction_Line_Item__c tli = TestUtils.createTransactionLineItem(opp.Id, tliTypeId, false);
        tli.Amount__c = 50;
        tli.Transaction_Type__c = 'ACH';
        insert tli;
        
       system.debug('::::::::Paid_Status__c:::::>'+ String.valueOf([Select Payment_Status__c from PFS__c where Id = :pfs.Id].Payment_Status__c));
        
        System.assertEquals('Partially Paid', [Select Payment_Status__c from PFS__c where Id = :pfs.Id].Payment_Status__c);
        
        // fake new trigger context [dp]
        TransactionLineItemHandler.isRunning = false;
        Transaction_Line_Item__c tli2 = TestUtils.createTransactionLineItem(opp.Id, tliTypeId, false);
        tli2.Amount__c = 50;
        tli2.Transaction_Type__c = 'Refund-ACH';
        insert tli2;
        
        System.assertEquals('Not Paid', [Select Payment_Status__c from PFS__c where Id = :pfs.Id].Payment_Status__c);
        
        // fake new trigger context [dp]
        TransactionLineItemHandler.isRunning = false;
        Transaction_Line_Item__c tli3 = TestUtils.createTransactionLineItem(opp.Id, tliTypeId, false);
        tli3.Amount__c = 100;
        tli3.Transaction_Type__c = 'ACH';
        insert tli3;
        
        System.assertEquals('Paid in Full', [Select Payment_Status__c from PFS__c where Id = :pfs.Id].Payment_Status__c);
    }
*/    

    @isTest
    private static void runTestForAutoWriteOffReversalCreation() {
        setupTestData();
        
        TransactionLineItemHandler.isRunning = false;
        //Execute the trigger by inserting Transaction_Line_Item__c records
          // write off full amount
        Transaction_Line_Item__c tli = TestUtils.createTransactionLineItem(opp.Id, RecordTypes.adjustmentTransactionTypeId, false);
        tli.Amount__c = 100;
        tli.Transaction_Type__c = 'Write-off';
        tli.Transaction_Status__c = 'Posted';
        insert tli;
                
        System.assertEquals('Written Off', [Select Payment_Status__c from PFS__c where Id = :pfs.Id].Payment_Status__c);
        System.assertEquals(0, [Select Id from Transaction_Line_Item__c where Opportunity__c = :opp.Id AND Transaction_Type__c = 'Write-Off Reversal'].size());
        
        // fake new trigger context [dp]
        TransactionLineItemHandler.isRunning = false;
        Transaction_Line_Item__c tli2 = TestUtils.createTransactionLineItem(opp.Id, RecordTypes.paymentTransactionTypeId, false);
        tli2.Amount__c = 100;
        tli2.Transaction_Type__c = 'ACH';
        tli2.Transaction_Status__c = 'Posted';
        insert tli2;
        
        List<Transaction_Line_Item__c> createdReversals = [Select Id, Amount__c from Transaction_Line_Item__c where Opportunity__c = :opp.Id AND Transaction_Type__c = 'Write-Off Reversal']; 
        System.assertEquals(1, createdReversals.size());
        System.assertEquals(100, createdReversals[0].Amount__c);
        System.assertNotEquals('Written Off', [Select Payment_Status__c from PFS__c where Id = :pfs.Id].Payment_Status__c);
                
    }
    
    @isTest
    private static void runTestForAutoWriteOffReversalCreationTwoTransactions() {
        setupTestData(false);
        PFS__c pfs2 = TestUtils.createPFS('TEST2', pfs.Academic_Year_Picklist__c, null, true);
        
        Opportunity opp2 = opp.clone(false, false);
        opp2.PFS__c = pfs2.Id;
        List<Opportunity> testOpps = new List<Opportunity> {opp, opp2};
        insert testOpps;
        
        
        TransactionLineItemHandler.isRunning = false;
        //Execute the trigger by inserting Transaction_Line_Item__c records
          // write off full amount
        Transaction_Line_Item__c tliOpp = TestUtils.createTransactionLineItem(opp.Id, RecordTypes.adjustmentTransactionTypeId, false);
        Transaction_Line_Item__c tliOpp2 = TestUtils.createTransactionLineItem(opp2.Id, RecordTypes.adjustmentTransactionTypeId, false);
        tliOpp.Amount__c = 100;
        tliOpp.Transaction_Type__c = 'Write-off';
        tliOpp.Transaction_Status__c = 'Posted';
        tliOpp2.Amount__c = 100;
        tliOpp2.Transaction_Type__c = 'Write-off';
        tliOpp2.Transaction_Status__c = 'Posted';
        insert new List<Transaction_Line_Item__c> {tliOpp, tliOpp2};
                
        System.assertEquals('Written Off', [Select Payment_Status__c from PFS__c where Id = :pfs.Id].Payment_Status__c);
        System.assertEquals(0, [Select Id from Transaction_Line_Item__c where Opportunity__c in :testOpps AND Transaction_Type__c = 'Write-Off Reversal'].size());
        
        // fake new trigger context [dp]
        TransactionLineItemHandler.isRunning = false;
        Transaction_Line_Item__c tliPaymentOpp = TestUtils.createTransactionLineItem(opp.Id, RecordTypes.paymentTransactionTypeId, false);
        Transaction_Line_Item__c tliPaymentOpp2 = TestUtils.createTransactionLineItem(opp2.Id, RecordTypes.paymentTransactionTypeId, false);
        tliPaymentOpp.Amount__c = 75;
        tliPaymentOpp.Transaction_Type__c = 'ACH';
        tliPaymentOpp.Transaction_Status__c = 'Posted';
        tliPaymentOpp2.Amount__c = 75;
        tliPaymentOpp2.Transaction_Type__c = 'ACH';
        tliPaymentOpp2.Transaction_Status__c = 'Posted';
        insert new List<Transaction_Line_Item__c> {tliPaymentOpp, tliPaymentOpp2};
        
        List<Transaction_Line_Item__c> createdReversals = [Select Id, Amount__c from Transaction_Line_Item__c where Opportunity__c in :testOpps AND Transaction_Type__c = 'Write-Off Reversal']; 
        System.assertEquals(2, createdReversals.size());
        System.assertEquals(100, createdReversals[0].Amount__c);
        System.assertEquals(100, createdReversals[1].Amount__c);
        System.assertNotEquals('Written Off', [Select Payment_Status__c from PFS__c where Id = :pfs.Id].Payment_Status__c);
        System.assertNotEquals('Written Off', [Select Payment_Status__c from PFS__c where Id = :pfs2.Id].Payment_Status__c);
    }
    
    // [SL] NAIS-1235
    @isTest
    private static void runTestForFeeWaiverAutoWriteOffReversalCreation() {
        setupTestData();
        
        TransactionLineItemHandler.isRunning = false;
        //Execute the trigger by inserting Transaction_Line_Item__c records
          // write off full amount
        Transaction_Line_Item__c tli = TestUtils.createTransactionLineItem(opp.Id, RecordTypes.adjustmentTransactionTypeId, false);
        tli.Amount__c = 100;
        tli.Transaction_Type__c = 'Write-off';
        tli.Transaction_Status__c = 'Posted';
        insert tli;
                
        System.assertEquals('Written Off', [Select Payment_Status__c from PFS__c where Id = :pfs.Id].Payment_Status__c);
        System.assertEquals(0, [Select Id from Transaction_Line_Item__c where Opportunity__c = :opp.Id AND Transaction_Type__c = 'Write-Off Reversal'].size());
        
        // fake new trigger context [dp]
        TransactionLineItemHandler.isRunning = false;
        Transaction_Line_Item__c tli2 = TestUtils.createTransactionLineItem(opp.Id, RecordTypes.pfsFeeWaiverTransactionTypeId, false);
        tli2.Amount__c = 100;
        tli2.Transaction_Type__c = 'PFS Fee Waiver';
        tli2.Transaction_Status__c = 'Posted';
        insert tli2;
        
        List<Transaction_Line_Item__c> createdReversals = [Select Id, Amount__c from Transaction_Line_Item__c where Opportunity__c = :opp.Id AND Transaction_Type__c = 'Write-Off Reversal']; 
        System.assertEquals(1, createdReversals.size());
        System.assertEquals(100, createdReversals[0].Amount__c);
        System.assertNotEquals('Written Off', [Select Payment_Status__c from PFS__c where Id = :pfs.Id].Payment_Status__c);
    }
    
    // test data
    private static Opportunity opp;
    private static PFS__c pfs;
    private static void setupTestData(){
        setupTestData(true);
    }    
    
    private static void setupTestData(Boolean insertOpp){
        // Create Test Data
        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        // loadTransTypeSetting();
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        pfs = TestUtils.createPFS('TEST', academicYearId, null, true);
        
        Map<String, Schema.RecordTypeInfo> opptyTypeMap = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName();
        Id rt = opptyTypeMap.get('PFS Application Fee').RecordTypeId;
        
        
        opp = TestUtils.createOpportunity('Test Opp', rt, pfs.Id, GlobalVariables.getCurrentAcademicYear().Name, false);
        opp.Amount = 100;
        opp.RecordTypeId = rt;
        opp.StageName = 'Open';    // required field
        //opp.Paid_Status__c = 'Unpaid';
        opp.CloseDate = system.today()+30;    // required field
        if (insertOpp) {insert opp;}
    }
    
    /*
    private static void loadTransTypeSetting()
    {
        list<DebitCreditTransactionType__c> dct= new list<DebitCreditTransactionType__c>{new DebitCreditTransactionType__c(Name = 'ACH', Debit_Credit__c = 'Debit')
                                                                                        ,new DebitCreditTransactionType__c(Name = 'Refund-ACH', Debit_Credit__c = 'Credit')
                                                                                        ,new DebitCreditTransactionType__c(Name = 'Write-off Reversal', Debit_Credit__c = 'Credit')
                                                                                        ,new DebitCreditTransactionType__c(Name = 'Write-off', Debit_Credit__c = 'Debit')};
        insert dct;
    }
    */
}