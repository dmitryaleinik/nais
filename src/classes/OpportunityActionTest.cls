@isTest
private class OpportunityActionTest {

    private static void createTestData() {
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        Application_Fee_Waiver__c afw1 = ApplicationFeeWaiverTestData.Instance.DefaultApplicationFeeWaiver;

        // create the default test data
        PFS__c pfs = PfsTestData.Instance
            .asSubmitted()
            .forAcademicYearPicklist(currentAcademicYear.Name).DefaultPfs;
    }

    @isTest
    private static void testUpdatePfsStatus() {
        createTestData();
        createAppFeeSetting(GlobalVariables.getCurrentYearString());
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        Test.startTest();
            FinanceAction.createOppAndTransaction(new Set<Id>{pfs.Id}); // [DP] 07.08.2015 NAIS-1933 -- need to call this explicitly now

            // get the opportunity
            Opportunity opp = [SELECT Id, Paid_Status__c FROM Opportunity WHERE PFS__c = :pfs.Id];
            // verify the PFS status is unpaid
            System.assertEquals('Unpaid', [SELECT Payment_Status__c FROM PFS__c WHERE Id = :pfs.Id].Payment_Status__c);

            // add a transaction line item partial payment
            Transaction_Line_Item__c tli1 = TestUtils.createTransactionLineItem(
                opp.Id, RecordTypes.paymentAutoTransactionTypeId, false);
            tli1.Transaction_Type__c = 'Credit/Debit Card';
            tli1.Transaction_Status__c = 'Posted';
            tli1.Amount__c = 25;
            insert tli1;
            // verify the PFS status is partially paid
            System.assertEquals('Partially Paid', [SELECT Payment_Status__c FROM PFS__c WHERE Id = :pfs.Id].Payment_Status__c);

            // add another payment for the full amount
            Transaction_Line_Item__c tli2 = TestUtils.createTransactionLineItem(
                opp.Id, RecordTypes.paymentAutoTransactionTypeId, false);
            tli2.Transaction_Type__c = 'Credit/Debit Card';
            tli2.Transaction_Status__c = 'Posted';
            tli2.Amount__c = 50;
            insert tli2;
            // verify the PFS status is fully paid
            System.assertEquals('Paid in Full', [SELECT Payment_Status__c FROM PFS__c WHERE Id = :pfs.Id].Payment_Status__c);

            // delete the partial payment transaction line item
            delete tli1;
            // verify the PFS status is still fully paid
            System.assertEquals('Paid in Full', [SELECT Payment_Status__c FROM PFS__c WHERE Id = :pfs.Id].Payment_Status__c);

            // delete the remaining payment
            delete tli2;
            // verify the PFS status is not paid
            System.assertEquals('Unpaid', [SELECT Payment_Status__c FROM PFS__c WHERE Id = :pfs.Id].Payment_Status__c);
        Test.stopTest();
    }

    @isTest
    private static void testUpdatePfsStatusWrittenOff() {
        createTestData();
        createAppFeeSetting(GlobalVariables.getCurrentYearString());
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        Test.startTest();
            FinanceAction.createOppAndTransaction(new Set<Id>{pfs.Id}); // [DP] 07.08.2015 NAIS-1933 -- need to call this explicitly now

            // get the opportunity
            Opportunity opp = [SELECT Id, Paid_Status__c FROM Opportunity WHERE PFS__c = :pfs.Id];

            // verify the PFS status is not paid
            System.assertEquals('Unpaid', [SELECT Payment_Status__c FROM PFS__c WHERE Id = :pfs.Id].Payment_Status__c);

            // add a write-off record
            Transaction_Line_Item__c tli3 = TestUtils.createTransactionLineItem(
                opp.Id, RecordTypes.adjustmentTransactionTypeId, false);
            tli3.Transaction_Type__c = 'Write-off';
            tli3.Transaction_Status__c = 'Posted';
            tli3.Amount__c = 50;
            insert tli3;
        Test.stopTest();
        
        // verify the PFS status is write-off
        System.assertEquals('Written Off', [SELECT Payment_Status__c FROM PFS__c WHERE Id = :pfs.Id].Payment_Status__c);
    }

    @isTest
    private static void testUpdatePfsAmount() {
        createTestData();
        PFS__c pfs1 = PfsTestData.Instance.DefaultPfs;
        
        String academicYearName = GlobalVariables.getCurrentYearString();
        Contact parent2 = ContactTestData.Instance
            .asParent().insertContact();
        PFS__c pfs2 = PfsTestData.Instance
            .forAcademicYearPicklist(academicYearName)
            .forParentA(parent2.Id)
            .asSubmitted().insertPfs();

        // Application Fee Settings
        insert new Application_Fee_Settings__c(
            Name = pfs1.Academic_Year_Picklist__c,
            Means_Based_Fee_Waiver_Record_Type_Name__c 
                    = ApplicationFeeSettingsService.MEANS_BASED_FEE_WAIVER_RECORDTYPE,
            Maximum_Assets_Waiver__c = 25000,
            Discounted_Amount__c = 48,
            Amount__c = 48,
            Discounted_Product_Code__c = 'Discounted Product Code',
            Product_Code__c = 'Product Code');

        Test.startTest();
            FinanceAction.createOppAndTransaction(new Set<Id>{pfs1.Id, pfs2.Id}); // [DP] 07.08.2015 NAIS-1933 -- need to call this explicitly now

            PFS__c pfsRecord = [select Amount_Paid__c from PFS__c where Id = :pfs1.Id];
            System.assertEquals(48.0, pfsRecord.Amount_Paid__c);

            // Add an Opportunity
            Opportunity opp2 = TestUtils.createOpportunity(
                'Test Opp 2', RecordTypes.pfsApplicationFeeOpportunityTypeId, pfs1.Id, academicYearName, true);

            // add a transaction line item partial payment
            Transaction_Line_Item__c tli1 = TestUtils.createTransactionLineItem(opp2.Id, RecordTypes.saleTransactionTypeId, false);
            tli1.Transaction_Type__c = 'PFS Sale';
            tli1.Transaction_Status__c = 'Posted';
            tli1.Amount__c = 50;
            insert tli1;

            pfsRecord = [select Amount_Paid__c from PFS__c where Id = :pfs1.Id];
            System.assertEquals(98.0, pfsRecord.Amount_Paid__c);

            PFS__c pfsRecord2 = [select Amount_Paid__c from PFS__c where Id = :pfs2.Id];
            System.assertEquals(48.0, pfsRecord2.Amount_Paid__c);
        Test.stopTest();
    }

    // [dp] NAIS-1628 When a School Subscription Fee opportunity is paid, a subscription record should be created with the subscription
    // type and the dates for that next year (in custom settings?) and the paid date as the date the opp has been marked as paid.
    @isTest
    private static void testSingleOppGetsSubscriptionRecord() {
        Academic_Year__c newAcademicYear = new Academic_Year__c();
        newAcademicYear.Name = '2014-2015';
        insert newAcademicYear;

        Subscription_Settings__c sSettings = new Subscription_Settings__c();
        sSettings.Name = 'Subscription Settings';
        sSettings.Start_Month__c = 10;
        sSettings.Start_Day__c = 1;
        sSettings.End_Month__c = 9;
        sSettings.End_Day__c = 30;
        sSettings.Number_of_Months__c = 12;
        sSettings.Number_of_Months_Extended__c = 36;
        insert sSettings;

        Transaction_Annual_Settings__c transAnnualSetting = new Transaction_Annual_Settings__c();
        transAnnualSetting.Product_Amount__c = 200.00;
        transAnnualSetting.Product_Code__c = 'Test Product Code';
        transAnnualSetting.Year__c = newAcademicYear.Name.left(4);
        transAnnualSetting.Name = 'Sample Name';
        transAnnualSetting.Subscription_Type__c = 'Sub Type';
        insert transAnnualSetting;

        Integer i = 0;
        List<Account> testAccounts = new List<Account>();
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        insert testAccounts;

        System.assertEquals(0, [Select count() from Subscription__c where Account__c in :testAccounts]);

        List<Opportunity> testSubscriptionOpps = new List<Opportunity>();
        i = 0;
        for (Account a : testAccounts) {
            Opportunity o = TestUtils.createOpportunity('subscription' + i, RecordTypes.opportunitySubscriptionFeeTypeId, null, null, false);
            o.AccountId = testAccounts[i].Id;
            o.CloseDate = Date.newInstance(2001, 12, 31);
            o.Academic_Year_Picklist__c = newAcademicYear.Name;
            o.Subscription_Type__c = 'Full';
            testSubscriptionOpps.add(o);
            i++;
        }
        insert testSubscriptionOpps;

        List<Subscription__c> testSubs = [Select Start_Date__c, End_Date__c from Subscription__c where Account__c in :testAccounts];
        System.assertEquals(testAccounts.size(), testSubs.size());
        // NAIS-2474 [WH] 06.24.2015 Subscription period is one year in advance of Academic Year on the Opp
        System.assertEquals(2013, testSubs[0].Start_Date__c.Year());
        System.assertEquals(Date.newInstance(2013, 10, 1), testSubs[0].Start_Date__c);
        System.assertEquals(Date.newInstance(2014, 9, 30), testSubs[0].End_Date__c);
    }

    // NAIS-2488 [WH] 2015-07-17 START
    @isTest
    private static void testNoDuplicateSubscriptionRecord() {
        Academic_Year__c newAcademicYear = new Academic_Year__c();
        newAcademicYear.Name = '2014-2015';
        insert newAcademicYear;

        Subscription_Settings__c sSettings = new Subscription_Settings__c();
        sSettings.Name = 'Subscription Settings';
        sSettings.Start_Month__c = 10;
        sSettings.Start_Day__c = 1;
        sSettings.End_Month__c = 9;
        sSettings.End_Day__c = 30;
        sSettings.Number_of_Months__c = 12;
        sSettings.Number_of_Months_Extended__c = 36;
        insert sSettings;

        Transaction_Annual_Settings__c transAnnualSetting = new Transaction_Annual_Settings__c();
        transAnnualSetting.Product_Amount__c = 200.00;
        transAnnualSetting.Product_Code__c = 'Test Product Code';
        transAnnualSetting.Year__c = newAcademicYear.Name.left(4);
        transAnnualSetting.Name = 'Sample Name';
        transAnnualSetting.Subscription_Type__c = 'Sub Type';
        insert transAnnualSetting;

        Integer i = 0;
        List<Account> testAccounts = new List<Account>();
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        insert testAccounts;

        System.assertEquals(0, [Select count() from Subscription__c where Account__c in :testAccounts]);

        List<Opportunity> testSubscriptionOpps = new List<Opportunity>();
        i = 0;
        for (Account a : testAccounts) {
            Opportunity o = TestUtils.createOpportunity('subscription' + i, RecordTypes.opportunitySubscriptionFeeTypeId, null, null, false);
            o.AccountId = testAccounts[i].Id;
            o.CloseDate = Date.newInstance(2001, 12, 31);
            o.Academic_Year_Picklist__c = newAcademicYear.Name;
            o.Subscription_Type__c = 'Full';
            testSubscriptionOpps.add(o);
            i++;
        }
        insert testSubscriptionOpps;

        System.assertEquals(testAccounts.size(), [Select count() from Subscription__c where Account__c in :testAccounts]);

        Test.startTest();

        // simulate second round of Opp update trigger firing for same set of Opps
        OpportunityAction.createSubscriptionRecordsFromPaidOpps(testSubscriptionOpps);

        Test.stopTest();

        // verify that no duplicate subscriptions are created
        System.assertEquals(testAccounts.size(), [Select count() from Subscription__c where Account__c in :testAccounts]);
    }

    @isTest
    private static void testSubscriptionRecordCreatedForNewOppNotPreviouslyProcessed() {
        Academic_Year__c newAcademicYear = new Academic_Year__c();
        newAcademicYear.Name = '2014-2015';
        insert newAcademicYear;

        Subscription_Settings__c sSettings = new Subscription_Settings__c();
        sSettings.Name = 'Subscription Settings';
        sSettings.Start_Month__c = 10;
        sSettings.Start_Day__c = 1;
        sSettings.End_Month__c = 9;
        sSettings.End_Day__c = 30;
        sSettings.Number_of_Months__c = 12;
        sSettings.Number_of_Months_Extended__c = 36;
        insert sSettings;

        Transaction_Annual_Settings__c transAnnualSetting = new Transaction_Annual_Settings__c();
        transAnnualSetting.Product_Amount__c = 200.00;
        transAnnualSetting.Product_Code__c = 'Test Product Code';
        transAnnualSetting.Year__c = newAcademicYear.Name.left(4);
        transAnnualSetting.Name = 'Sample Name';
        transAnnualSetting.Subscription_Type__c = 'Sub Type';
        insert transAnnualSetting;

        List<Account> testAccounts = new List<Account>();
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount0', RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount1', RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount2', RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount3', RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount4', RecordTypes.schoolAccountTypeId, 5, false));
        insert testAccounts;

        System.assertEquals(0, [Select count() from Subscription__c where Account__c in :testAccounts]);

        List<Opportunity> testSubscriptionOpps = new List<Opportunity>();
        for (Integer i = 0, l = testAccounts.size(); i < l; i++) {
            Opportunity o = TestUtils.createOpportunity('subscription' + i, RecordTypes.opportunitySubscriptionFeeTypeId, null, null, false);
            o.AccountId = testAccounts[i].Id;
            o.CloseDate = Date.newInstance(2001, 12, 31);
            o.Academic_Year_Picklist__c = newAcademicYear.Name;
            o.Subscription_Type__c = 'Full';
            testSubscriptionOpps.add(o);
        }
        insert testSubscriptionOpps;

        System.assertEquals(testAccounts.size(), [Select count() from Subscription__c where Account__c in :testAccounts]);

        Test.startTest();

        Opportunity newOpp = TestUtils.createOpportunity('New Opp for Account 0', RecordTypes.opportunitySubscriptionFeeTypeId, null, null, false, 'Open');
        newOpp.AccountId = testAccounts[0].Id;
        newOpp.CloseDate = Date.newInstance(2002, 12, 31);
        newOpp.Academic_Year_Picklist__c = newAcademicYear.Name;
        newOpp.Subscription_Type__c = 'Full';
        insert newOpp;
        testSubscriptionOpps.add(newOpp);

        System.assertEquals(testAccounts.size(), [Select count() from Subscription__c where Account__c in :testAccounts]);

        // simulate second round of Opp update trigger firing with one new Opp
        OpportunityAction.createSubscriptionRecordsFromPaidOpps(testSubscriptionOpps);

        // verify that 1 new subscription is created for account of the new Opp
        List<Subscription__c> testSubs = [Select Id, Account__c from Subscription__c where Account__c in :testAccounts order by Account__c];
        System.assertEquals(testAccounts.size() + 1, testSubs.size());
        System.assertEquals(testAccounts[0].Id, testSubs[0].Account__c);
        System.assertEquals(testAccounts[0].Id, testSubs[1].Account__c);
        for (Integer j = 1; j < testAccounts.size(); j++) {
            System.assertEquals(testAccounts[j].Id, testSubs[j+1].Account__c);
        }

        Test.stopTest();
    }
    // NAIS-2488 [WH] 2015-07-17 END

    // [dp] NAIS-1628 When a School Subscription Fee opportunity is paid, a subscription record should be created with the subscription
    // type and the dates for that next year (in custom settings?) and the paid date as the date the opp has been marked as paid.
    @isTest
    private static void testSingleOppGetsSubscriptionRecordExtended() {
        Academic_Year__c newAcademicYear = new Academic_Year__c();
        newAcademicYear.Name = '2014-2015';
        insert newAcademicYear;

        Subscription_Settings__c sSettings = new Subscription_Settings__c();
        sSettings.Name = 'Subscription Settings';
        sSettings.Start_Month__c = 10;
        sSettings.Start_Day__c = 1;
        sSettings.End_Month__c = 9;
        sSettings.End_Day__c = 30;
        sSettings.Number_of_Months__c = 12;
        sSettings.Number_of_Months_Extended__c = 36;
        insert sSettings;

        Transaction_Annual_Settings__c transAnnualSetting = new Transaction_Annual_Settings__c();
        transAnnualSetting.Product_Amount__c = 200.00;
        transAnnualSetting.Product_Code__c = 'Test Product Code';
        transAnnualSetting.Year__c = newAcademicYear.Name.left(4);
        transAnnualSetting.Name = 'Sample Name';
        transAnnualSetting.Subscription_Type__c = 'Sub Type';
        insert transAnnualSetting;

        Integer i = 0;
        List<Account> testAccounts = new List<Account>();
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        insert testAccounts;

        System.assertEquals(0, [Select count() from Subscription__c where Account__c in :testAccounts]);

        List<Opportunity> testSubscriptionOpps = new List<Opportunity>();
        i = 0;
        for (Account a : testAccounts) {
            Opportunity o = TestUtils.createOpportunity('subscription' + i, RecordTypes.opportunitySubscriptionFeeTypeId, null, null, false);
            o.AccountId = testAccounts[i].Id;
            o.CloseDate = Date.newInstance(2001, 12, 31);
            o.Academic_Year_Picklist__c = newAcademicYear.Name;
            o.Subscription_Type__c = 'Extended';
            testSubscriptionOpps.add(o);
            i++;
        }
        insert testSubscriptionOpps;

        List<Subscription__c> testSubs = [Select Start_Date__c, End_Date__c from Subscription__c where Account__c in :testAccounts];
        System.assertEquals(testAccounts.size(), testSubs.size());
        // NAIS-2474 [WH] 06.24.2015 Subscription period is one year in advance of Academic Year on the Opp
        System.assertEquals(2013, testSubs[0].Start_Date__c.Year());
        System.assertEquals(Date.newInstance(2013, 10, 1), testSubs[0].Start_Date__c);
        System.assertEquals(Date.newInstance(2016, 9, 30), testSubs[0].End_Date__c);
    }

    // [dp] NAIS-1628 When a School Subscription Fee opportunity is paid, a subscription record should be created with the subscription
    // type and the dates for that next year (in custom settings?) and the paid date as the date the opp has been marked as paid.
    @isTest
    private static void testSingleOppGetsSubscriptionRecordWithCloseDate() {
        Academic_Year__c newAcademicYear = new Academic_Year__c();
        newAcademicYear.Name = '2014-2015';
        insert newAcademicYear;

        Subscription_Settings__c sSettings = new Subscription_Settings__c();
        sSettings.Name = 'Subscription Settings';
        sSettings.Start_Month__c = 10;
        sSettings.Start_Day__c = 1;
        sSettings.End_Month__c = 9;
        sSettings.End_Day__c = 30;
        sSettings.Number_of_Months__c = 12;
        sSettings.Number_of_Months_Extended__c = 36;
        insert sSettings;

        Transaction_Annual_Settings__c transAnnualSetting = new Transaction_Annual_Settings__c();
        transAnnualSetting.Product_Amount__c = 200.00;
        transAnnualSetting.Product_Code__c = 'Test Product Code';
        transAnnualSetting.Year__c = newAcademicYear.Name.left(4);
        transAnnualSetting.Name = 'Sample Name';
        transAnnualSetting.Subscription_Type__c = 'Sub Type';
        insert transAnnualSetting;

        Integer i = 0;
        List<Account> testAccounts = new List<Account>();
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        insert testAccounts;

        System.assertEquals(0, [Select count() from Subscription__c where Account__c in :testAccounts]);

        List<Opportunity> testSubscriptionOpps = new List<Opportunity>();
        i = 0;
        for (Account a : testAccounts) {
            Opportunity o = TestUtils.createOpportunity('subscription' + i, RecordTypes.opportunitySubscriptionFeeTypeId, null, null, false);
            o.AccountId = testAccounts[i].Id;
            o.CloseDate = Date.newInstance(2013, 11, 11);    // NAIS-2474 [WH] 06.24.2015 Subscription period is one year in advance of Academic Year on the Opp
            o.Academic_Year_Picklist__c = newAcademicYear.Name;
            o.Subscription_Type__c = 'Full';
            testSubscriptionOpps.add(o);
            i++;
        }
        insert testSubscriptionOpps;

        List<Subscription__c> testSubs = [Select Start_Date__c, End_Date__c from Subscription__c where Account__c in :testAccounts];
        System.assertEquals(testAccounts.size(), testSubs.size());
        // NAIS-2474 [WH] 06.24.2015 Subscription period is one year in advance of Academic Year on the Opp
        System.assertEquals(2013, testSubs[0].Start_Date__c.Year());
        System.assertEquals(Date.newInstance(2013, 11, 11), testSubs[0].Start_Date__c);
        System.assertEquals(Date.newInstance(2014, 9, 30), testSubs[0].End_Date__c);
    }

    // [dp] NAIS-1628 When a School Subscription Fee opportunity is paid, a subscription record should be created with the subscription
    // type and the dates for that next year (in custom settings?) and the paid date as the date the opp has been marked as paid.
    // [WH] NAIS-2270 Take Suscription start year from Subscription_Period_Start_Year__c field if set explicitly in the Opp
    @isTest
    private static void testSingleOppGetsSubscriptionRecordWithSubscriptionPeriodStartYear() {
        Academic_Year__c newAcademicYear = new Academic_Year__c();
        newAcademicYear.Name = '2014-2015';
        insert newAcademicYear;

        Subscription_Settings__c sSettings = new Subscription_Settings__c();
        sSettings.Name = 'Subscription Settings';
        sSettings.Start_Month__c = 10;
        sSettings.Start_Day__c = 1;
        sSettings.End_Month__c = 9;
        sSettings.End_Day__c = 30;
        sSettings.Number_of_Months__c = 12;
        sSettings.Number_of_Months_Extended__c = 36;
        insert sSettings;

        Transaction_Annual_Settings__c transAnnualSetting = new Transaction_Annual_Settings__c();
        transAnnualSetting.Product_Amount__c = 200.00;
        transAnnualSetting.Product_Code__c = 'Test Product Code';
        transAnnualSetting.Year__c = newAcademicYear.Name.left(4);
        transAnnualSetting.Name = 'Sample Name';
        transAnnualSetting.Subscription_Type__c = 'Sub Type';
        insert transAnnualSetting;

        Integer i = 0;
        List<Account> testAccounts = new List<Account>();
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        testAccounts.add(TestUtils.createAccount('testSubscriptionAccount' + i++, RecordTypes.schoolAccountTypeId, 5, false));
        insert testAccounts;

        System.assertEquals(0, [Select count() from Subscription__c where Account__c in :testAccounts]);

        List<Opportunity> testSubscriptionOpps = new List<Opportunity>();
        i = 0;
        for (Account a : testAccounts) {
            Opportunity o = TestUtils.createOpportunity('subscription' + i, RecordTypes.opportunitySubscriptionFeeTypeId, null, null, false);
            o.AccountId = testAccounts[i].Id;
            o.CloseDate = Date.newInstance(2013, 11, 11);    // NAIS-2474 [WH] 06.24.2015 Subscription period is one year in advance of Academic Year on the Opp
            o.Academic_Year_Picklist__c = newAcademicYear.Name;
            o.Subscription_Type__c = 'Full';
            testSubscriptionOpps.add(o);
            i++;
        }
        testSubscriptionOpps[0].Subscription_Period_Start_Year__c = '2016';
        insert testSubscriptionOpps;

        List<Subscription__c> testSubs = [Select Start_Date__c, End_Date__c from Subscription__c where Account__c in :testAccounts];
        System.assertEquals(testAccounts.size(), testSubs.size());
        System.assertEquals(2013, testSubs[0].Start_Date__c.Year());
        System.assertEquals(Date.newInstance(2013, 11, 11), testSubs[0].Start_Date__c);
        System.assertEquals(Date.newInstance(2014, 9, 30), testSubs[0].End_Date__c);
        // NAIS-2474 [WH] 06.24.2015 Subscription period is one year in advance of Academic Year on the Opp
        System.assertEquals(2013, testSubs[1].Start_Date__c.Year());
        System.assertEquals(Date.newInstance(2013, 11, 11), testSubs[1].Start_Date__c);
        System.assertEquals(Date.newInstance(2014, 9, 30), testSubs[1].End_Date__c);
    }

    @isTest
    private static void testOpportunityStageChangedAndCreateSubscription() {
        Academic_Year__c newAcademicYear = new Academic_Year__c();
        newAcademicYear.Name = '2017-2018';
        insert newAcademicYear;

        Subscription_Settings__c sSettings = new Subscription_Settings__c();
        sSettings.Name = 'Subscription Settings';
        sSettings.Start_Month__c = 10;
        sSettings.Start_Day__c = 1;
        sSettings.End_Month__c = 9;
        sSettings.End_Day__c = 30;
        sSettings.Number_of_Months__c = 12;
        sSettings.Number_of_Months_Extended__c = 36;
        insert sSettings;

        Integer i = 0;
        Account testAccount = TestUtils.createAccount('Unittest School', RecordTypes.schoolAccountTypeId, 5, true);

        Opportunity o = TestUtils.createOpportunity('subscription1', RecordTypes.opportunitySubscriptionFeeTypeId, null, null, false);
        o.AccountId = testAccount.Id;
        o.CloseDate = Date.newInstance(2016, 6, 4);
        o.Academic_Year_Picklist__c = newAcademicYear.Name;
        o.Subscription_Type__c = 'Full';
        o.StageName = 'Closed Won';

        String validationRuleMessage = '';
        try{
            insert o;
        }catch(Exception e) {
            validationRuleMessage = e.getMessage();
        }
        System.assertEquals(true, validationRuleMessage!='','You should not be able to mark an opp as closed/won without any TLI records.');

        o.StageName = 'Prospecting';
        insert o;

        Transaction_Line_Item__c tliRecord = new Transaction_Line_Item__c(RecordTypeId=RecordTypes.saleTransactionTypeId,
                                            Transaction_Status__c='Posted',
                                            Amount__c=0,
                                            Transaction_Type__c='School Subscription',
                                            Opportunity__c=o.Id,
                                            Product_Code__c='121-100 School Sub - Basic - 2016-17',
                                            Product_Amount__c='0');
        insert tliRecord;
        
        o = new Opportunity(Id = o.Id);
        o.StageName = 'Closed Won';
        update o;

        Test.startTest();
        List<Subscription__c> subscriptions = new List<Subscription__c>([SELECT Id FROM Subscription__c
                                                                            where Account__c=:testAccount.Id limit 1]);
        Opportunity testOpp = [Select Id, Paid_Status__c, StageName, (SELECT Id FROM Transaction_Line_Items__r)
                                     from Opportunity where Id=:o.Id limit 1];
        System.assertEquals('Closed Won', testOpp.StageName);
        System.assertEquals('Paid', testOpp.Paid_Status__c);
        System.assertEquals(1, subscriptions.size(),
                            'When Opportunity\'s stage is \'Closed Won\' and Paid Status is \'Paid\'. Then, a subscription must be created.');

        System.assertEquals(1, testOpp.Transaction_Line_Items__r.size());
        testOpp.StageName = 'Prospecting';
        update testOpp;
        Test.stopTest();

        subscriptions = new List<Subscription__c>([SELECT Id FROM Subscription__c
                                                        where Account__c=:testAccount.Id limit 1]);
        System.assertEquals(0, subscriptions.size(),
                            'When Opportunity\'s stage is different than \'Closed Won\' and Paid Status is \'Paid\'. Then, no subscription must be created.');
    }//End:testOpportunityStageChangedAndCreateSubscription

    @isTest
    private static void testOpportunityStageChangedAndDoNotCreateSubscription() {
        Academic_Year__c newAcademicYear = new Academic_Year__c();
        newAcademicYear.Name = '2017-2018';
        insert newAcademicYear;

        Subscription_Settings__c sSettings = new Subscription_Settings__c();
        sSettings.Name = 'Subscription Settings';
        sSettings.Start_Month__c = 10;
        sSettings.Start_Day__c = 1;
        sSettings.End_Month__c = 9;
        sSettings.End_Day__c = 30;
        sSettings.Number_of_Months__c = 12;
        sSettings.Number_of_Months_Extended__c = 36;
        insert sSettings;

        Integer i = 0;
        Account testAccount = TestUtils.createAccount('Unittest School', RecordTypes.schoolAccountTypeId, 5, true);

        Opportunity o = TestUtils.createOpportunity('subscription1', RecordTypes.opportunitySubscriptionFeeTypeId, null, null, false);
        o.AccountId = testAccount.Id;
        o.CloseDate = Date.newInstance(2016, 6, 4);
        o.Academic_Year_Picklist__c = newAcademicYear.Name;
        o.Subscription_Type__c = 'Full';
        o.StageName = 'Prospecting';
        insert o;

        Transaction_Line_Item__c tliRecord = new Transaction_Line_Item__c(RecordTypeId=RecordTypes.saleTransactionTypeId,
                                            Transaction_Status__c='Posted',
                                            Amount__c=400,
                                            Transaction_Type__c='School Subscription',
                                            Opportunity__c=o.Id,
                                            Product_Code__c='121-100 School Sub - Full - 2016-17',
                                            Product_Amount__c='400.00');
        insert tliRecord;

        Test.startTest();
        o.StageName = 'Closed Won';
        update o;
        Opportunity testOpp = [Select Id, Paid_Status__c, StageName, (SELECT Id FROM Transaction_Line_Items__r)
                                     from Opportunity where Id=:o.Id limit 1];
        System.assertEquals('Unpaid', testOpp.Paid_Status__c);
        System.assertEquals('Closed Won', testOpp.StageName);

        List<Subscription__c> subscriptions = new List<Subscription__c>([SELECT Id FROM Subscription__c
                                                                            where Account__c=:testAccount.Id limit 1]);
        System.assertEquals(0, subscriptions.size(),
                            'When Opportunity\'s stage is \'Closed Won\' and Paid Status is \'Unpaid\'. Then, no subscription must be created.');

        tliRecord.Product_Code__c='121-100 School Sub - Basic - 2016-17';
        tliRecord.Product_Amount__c = '0';
        update tliRecord;
        Test.stopTest();

         testOpp = [Select Id, Paid_Status__c, StageName, (SELECT Id FROM Transaction_Line_Items__r)
                                     from Opportunity where Id=:o.Id limit 1];
        System.assertEquals('Paid', testOpp.Paid_Status__c);
        System.assertEquals('Closed Won', testOpp.StageName);
        subscriptions = new List<Subscription__c>([SELECT Id FROM Subscription__c
                                                                            where Account__c=:testAccount.Id limit 1]);
        System.assertEquals(1, subscriptions.size(),
                            'When Opportunity\'s stage is \'Closed Won\' and Paid Status is \'Paid\'. Then, a subscription must be created.');
    }//End:testOpportunityStageChangedAndDoNotCreateSubscription
    
    //SFP-896
    @isTest
    private static void manuallyCreateTli_overpaidStatus_expectSubscriptionNotDeleted() {
        
        //Insert settings that will be used during the creation of the subscription record
        insert new Subscription_Settings__c(
            Name = 'Subscription Settings',
            Start_Month__c = 8,
            Start_Day__c = 1,
            Number_of_Months__c = 12,
            Number_of_Months_Extended__c = 36,
            Cutoff_Month__c = 3);
        
        
        Account school = AccountTestData.Instance.insertAccount();
        Opportunity opportunity = OpportunityTestData.Instance.forAccount(school.Id).insertOpportunity();
        TransactionLineItemTestData.Instance
                .asSale()
                .forAmount(opportunity.Amount)
                .forOpportunity(opportunity.Id)
                .insertTransactionLineItem();
        TransactionLineItemTestData.Instance
                .forOpportunity(opportunity.Id)
                .asCheck()
                .forAmount(opportunity.Amount)
                .insertTransactionLineItem();

        opportunity = OpportunitySelector.Instance.selectByIdWithPfs(new Set<Id> { opportunity.Id })[0];
        System.assertEquals('Paid', opportunity.Paid_Status__c, 'Expected the Opportunity to be paid.');
        
        //We can not wait until OpportunityBefore.trigger change this automatically to "Closed Won"
        //Otherwise, we wont be able to check on startTest if the subscriptions was created or not.
        //Since, the change of stageName will be executed on startTest. And the subscription creation after stopTest
        opportunity.StageName = 'Closed Won';
        update opportunity;
        
        // Create a manual check Transaction Line Item and then verify that the
        // subscriber status on the school hasn't changed.
        Test.startTest();
            //We need to verify if the subscription record was created.             
	        List<Subscription__c> subscriptions = [SELECT Id, Status__c, Cancelled__c, Charge_Cancellation_Fee__c, Cancellation_Date__c, End_Date__c, Start_Date__c, Paid_Date__c, Subscription_Type__c, Grace_Period__c FROM Subscription__c WHERE Account__c =: school.Id];
	        System.assertEquals(1,subscriptions.size());
            TransactionLineItemTestData.Instance.forOpportunity(opportunity.Id).asCheck().insertTransactionLineItem();
        Test.stopTest();
        
        opportunity = OpportunitySelector.Instance.selectByIdWithPfs(new Set<Id> { opportunity.Id })[0];
        System.assertEquals('Overpaid', opportunity.Paid_Status__c, 'Expected the Opportunity to be overpaid.');

        school = [SELECT SSS_Subscriber_Status__c FROM Account WHERE Id = :school.Id];
        System.assertEquals(school.SSS_Subscriber_Status__c, 'Current',
                'Expected the Subscriber status to be Current.');
    }

    private static void createAppFeeSetting(String appFeeSettingName) {
        Integer appFeeSettingAmount = 50;
        Integer appFeeSettingDiscountedAmount = 40;
        String appFeeSettingProductCode = 'Test Product Code';
        String appFeeSettingDiscountedProductCode = 'KS Test Product Code';
        Application_Fee_Settings__c appFeeSettings = ApplicationFeeSettingsTestData.Instance
            .forName(appFeeSettingName)
            .forAmount(appFeeSettingAmount)
            .forDiscountedAmount(appFeeSettingDiscountedAmount)
            .forProductCode(appFeeSettingProductCode)
            .forDiscountedProductCode(appFeeSettingDiscountedProductCode).DefaultApplicationFeeSettings;
    }
}