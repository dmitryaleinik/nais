/**
 * @description This class is used to create Subscription records for unit tests.
 */
@isTest
public class SubscriptionTestData extends SObjectTestData {
    @testVisible private static final String SUBSCRIPTION_TYPE = 'Full';

    /**
     * @description Get the default values for the Subscription__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Subscription__c.Account__c => AccountTestData.Instance.DefaultAccount.Id,
                Subscription__c.Subscription_Type__c => SUBSCRIPTION_TYPE,
                Subscription__c.Start_Date__c => Date.today(),
                Subscription__c.End_Date__c => Date.today().addDays(6)
        };
    }

    /**
     * @description The account to set the subscription for.
     * @param accountId The Id to set Account__c to.
     * @return The current instance of SubscriptionTestData.
     */
    public SubscriptionTestData forAccount(Id accountId) {
        return (SubscriptionTestData) with(Subscription__c.Account__c, accountId);
    }

    /**
     * @description Set the End_Date__c to coincide with the provided academic year.
     * @param academicYear The beginning academic year for the subscription being
     *          generated.
     * @return The current instance of SubscriptionTestData.
     */
    public SubscriptionTestData forAcademicYear(String academicYear) {
        Date endDate = Date.newInstance(Integer.valueOf(academicYear), Date.today().month(), Date.today().day());

        return (SubscriptionTestData) with(Subscription__c.End_Date__c, endDate);
    }

    /**
     * @description Set the Start_Date__c of the Subscription record.
     * @param startDate The value of the Start Date field of the subscription.
     * @return The current instance of SubscriptionTestData.
     */
    public SubscriptionTestData forStartDate(Date startDate) {
        return (SubscriptionTestData) with(Subscription__c.Start_Date__c, startDate);
    }

    /**
     * @description Set the End_Date__c of the Subscription record.
     * @param endDate The value of the End Date field of the subscription.
     * @return The current instance of SubscriptionTestData.
     */
    public SubscriptionTestData forEndDate(Date endDate) {
        return (SubscriptionTestData) with(Subscription__c.End_Date__c, endDate);
    }

    /**
     * @description Set the Cancelled__c field of the Subscription record. If cancelled parameter is equeal to 'Yes',
     *              sets also Cancellation_Date__c, Reason_Cancelled__c, Charge_Cancellation_Fee__c fields of the record.
     * @param cancelled The value of the Cancelled field of the subscription.
     * @return The current instance of SubscriptionTestData.
     */
    public SubscriptionTestData forCancelled(String cancelled) {
        String cancellationTestReason = 'Test Reason;';
        String yes = 'Yes';
        String no = 'No';

        if (cancelled == yes) {
            return (SubscriptionTestData) 
                 with(Subscription__c.Cancellation_Date__c, Date.today())
                .with(Subscription__c.Reason_Cancelled__c, cancellationTestReason)
                .with(Subscription__c.Charge_Cancellation_Fee__c, no)
                .with(Subscription__c.Cancelled__c, cancelled);
        }

        return (SubscriptionTestData) with(Subscription__c.Cancelled__c, cancelled);
    }

    /**
     * @description Set the Subscription_Type__c field of the Subscription record.
     * @param subscriptionType The value of the Subscription Type field of the subscription.
     * @return The current instance of SubscriptionTestData.
     */
    public SubscriptionTestData forSubscriptionType(String subscriptionType) {
        return (SubscriptionTestData) with(Subscription__c.Subscription_Type__c, subscriptionType);
    }

    /**
     * @description Set the current Subscription__c to an expired state.
     * @return The current instance of SubscriptionTestData.
     */
    public SubscriptionTestData asExpired() {
        return (SubscriptionTestData) with(Subscription__c.End_Date__c, Date.today().addMonths(-1));
    }

    /**
     * @description Insert the current working Subscription__c record.
     * @return The currently operated upon Subscription__c record.
     */
    public Subscription__c insertSubscription() {
        return (Subscription__c)insertRecord();
    }

    /**
     * @description Create the current working Subscription record without resetting
     *          the stored values in this instance of SubscriptionTestData.
     * @return A non-inserted Subscription__c record using the currently stored field
     *          values.
     */
    public Subscription__c createSubscriptionWithoutReset() {
        return (Subscription__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Subscription__c record.
     * @return The currently operated upon Subscription__c record.
     */
    public Subscription__c create() {
        return (Subscription__c)super.buildWithReset();
    }

    /**
     * @description The default Subscription__c record.
     */
    public Subscription__c DefaultSubscription {
        get {
            if (DefaultSubscription == null) {
                DefaultSubscription = createSubscriptionWithoutReset();
                insert DefaultSubscription;
            }
            return DefaultSubscription;
        }
        private set;
    }

    /**
     * @description Get the Subscription__c SObjectType.
     * @return The Subscription__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Subscription__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static SubscriptionTestData Instance {
        get {
            if (Instance == null) {
                Instance = new SubscriptionTestData();
            }
            return Instance;
        }
        private set;
    }

    private SubscriptionTestData() { }
}