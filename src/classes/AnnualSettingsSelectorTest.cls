@isTest
private class AnnualSettingsSelectorTest {

    private static final String SCHOOL_NAME_PREFIX = 'SchoolName';
    
    @isTest
    private static void selectById() {
        Integer numberOfAcademicYears = 6;
        List<Academic_Year__c> academicYears = AcademicYearTestData.Instance.insertAcademicYears(numberOfAcademicYears);

        Integer numberOfSchools = 5;
        List<Account> schools = insertSchools(numberOfSchools);

        List<Annual_Setting__c> settings = insertSettingsForSchools(schools, academicYears[0].Id);

        Test.startTest();
            Set<Id> settingIds = new Set<Id>();

            List<Annual_Setting__c> queriedRecords = AnnualSettingsSelector.newInstance().selectById(settingIds);
            System.assertNotEquals(null, queriedRecords, 'Expected the queried records to not be null.');
            System.assertEquals(0, queriedRecords.size(), 'Expected no records to be returned.');

            settingIds = new Map<Id, Annual_Setting__c>(settings).keySet();

            queriedRecords = AnnualSettingsSelector.newInstance().selectById(settingIds);
        Test.stopTest();
        
        System.assertNotEquals(null, queriedRecords, 'Expected the queried records to not be null.');
        System.assertEquals(numberOfSchools, queriedRecords.size(), 'Expected all inserted records to be returned.');
    }

    @isTest
    private static void selectBySchoolAndAcademicYear() {
        Integer numberOfAcademicYears = 6;
        List<Academic_Year__c> academicYears = AcademicYearTestData.Instance.insertAcademicYears(numberOfAcademicYears);

        Integer numberOfSchools = 5;
        List<Account> schools = insertSchools(numberOfSchools);

        List<Annual_Setting__c> settings = insertSettingsForSchools(schools, academicYears[0].Id);

        Set<Id> schoolIds = new Map<Id, Account>(schools).keySet();
        Set<String> academicYearNames = new Set<String> { academicYears[0].Name };
        
        Account schoolWithoutSetting = insertSchools(1)[0];

        Test.startTest();
            List<Annual_Setting__c> queriedRecords = AnnualSettingsSelector.newInstance()
                .selectBySchoolAndAcademicYear(new Set<Id> {schoolWithoutSetting.Id}, new Set<String>{academicYears[0].Name});
            System.assertNotEquals(null, queriedRecords, 'Expected the queried records to not be null.');
            System.assertEquals(0, queriedRecords.size(), 'Expected no records to be returned.');

            queriedRecords = AnnualSettingsSelector.newInstance().selectBySchoolAndAcademicYear(schoolIds, academicYearNames);
        Test.stopTest();
        
        System.assertNotEquals(null, queriedRecords, 'Expected the queried records to not be null.');
        System.assertEquals(numberOfSchools, queriedRecords.size(), 'Expected all inserted records to be returned.');
    }

    @isTest
    private static void selectWithCustomFieldListById() {
        Integer numberOfAcademicYears = 6;
        List<Academic_Year__c> academicYears = AcademicYearTestData.Instance.insertAcademicYears(numberOfAcademicYears);

        Integer numberOfSchools = 5;
        List<Account> schools = insertSchools(numberOfSchools);

        List<Annual_Setting__c> settings = insertSettingsForSchools(schools, academicYears[0].Id);

        Test.startTest();
            try {
                List<Annual_Setting__c> queriedRecords = AnnualSettingsSelector.newInstance()
                    .selectWithCustomFieldListById(null, new List<String>{'Name'});
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, AnnualSettingsSelector.ID_SET_PARAM);
            }

            try {
                List<Annual_Setting__c> queriedRecords = AnnualSettingsSelector.newInstance()
                    .selectWithCustomFieldListById(new Map<Id, Annual_Setting__c>(settings).keySet(), null);
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, AnnualSettingsSelector.ANN_SET_FIELDS_TO_SELECT_PARAM);
            }
        
            List<Annual_Setting__c> queriedRecords = AnnualSettingsSelector.newInstance()
                .selectWithCustomFieldListById(new Map<Id, Annual_Setting__c>(settings).keySet(), new List<String>{'Name'});
        Test.stopTest();
        
        System.assertNotEquals(null, queriedRecords, 'Expected the queried records to not be null.');
        System.assertEquals(settings.size(), queriedRecords.size(), 'Expected all inserted records to be returned.');
    }

    @isTest
    private static void selectWithCustomFieldList() {
        Integer numberOfAcademicYears = 6;
        List<Academic_Year__c> academicYears = AcademicYearTestData.Instance.insertAcademicYears(numberOfAcademicYears);

        Integer numberOfSchools = 5;
        List<Account> schools = insertSchools(numberOfSchools);

        List<Annual_Setting__c> settings = insertSettingsForSchools(schools, academicYears[0].Id);

        Test.startTest();
            try {
                List<Annual_Setting__c> queriedRecords = AnnualSettingsSelector.newInstance()
                    .selectWithCustomFieldList(null);
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, AnnualSettingsSelector.ANN_SET_FIELDS_TO_SELECT_PARAM);
            }
        
            List<Annual_Setting__c> queriedRecords = AnnualSettingsSelector.newInstance()
                .selectWithCustomFieldList(new List<String>{'Name'});
        Test.stopTest();
        
        System.assertNotEquals(null, queriedRecords, 'Expected the queried records to not be null.');
        System.assertEquals(settings.size(), queriedRecords.size(), 'Expected all inserted records to be returned.');
    }

    @isTest
    private static void selectAllAnnualSettings() {
        Integer numberOfAcademicYears = 6;
        List<Academic_Year__c> academicYears = AcademicYearTestData.Instance.insertAcademicYears(numberOfAcademicYears);

        Integer numberOfSchools = 5;
        List<Account> schools = insertSchools(numberOfSchools);

        List<Annual_Setting__c> settings = insertSettingsForSchools(schools, academicYears[0].Id);

        Test.startTest();
            List<Annual_Setting__c> queriedRecords = AnnualSettingsSelector.newInstance()
                .selectAllAnnualSettings();
        Test.stopTest();
        
        System.assertNotEquals(null, queriedRecords, 'Expected the queried records to not be null.');
        System.assertEquals(settings.size(), queriedRecords.size(), 'Expected all inserted records to be returned.');
    }

    private static List<Account> insertSchools(Integer numberToInsert) {
        List<Account> schools = new List<Account>();

        for (Integer i = 0; i < numberToInsert; i++) {
            Account newSchool = AccountTestData.Instance
                .forName(SCHOOL_NAME_PREFIX + i)
                .forRecordTypeId(RecordTypes.schoolAccountTypeId).create();

            schools.add(newSchool);
        }

        insert schools;

        return schools;
    }

    private static List<Annual_Setting__c> insertSettingsForSchools(List<Account> schools, Id academicYearId) {
        List<Annual_Setting__c> settings = new List<Annual_Setting__c>();

        for (Account school : schools) {
            Annual_Setting__c newSetting = AnnualSettingsTestData.Instance
                .forSchoolId(school.Id)
                .forAcademicYearId(academicYearId).create();

            settings.add(newSetting);
        }

        insert settings;

        return settings;
    }
}