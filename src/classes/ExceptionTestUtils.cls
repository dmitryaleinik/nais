/**
 * @description This class is used only in unit tests for asserting exceptions.
 */
@isTest
public class ExceptionTestUtils {

    public static void expectException(Type expectedExceptionType) {
        System.assert(false, 'Expected the ' + expectedExceptionType.getName() + ' to be thrown.');
    }

    public static void assertException(Type expectedExceptionType, Exception actualException) {
        System.assertEquals(expectedExceptionType.getName(), actualException.getTypeName(),
                'Expected the correct exception to be thrown. Error: ' + actualException.getMessage());
    }

    public static void assertArgumentNullException(Exception actualException, String expectedNullParam) {
        assertException(ArgumentNullException.class, actualException);
        String expectedErrorMessage = String.format(ArgumentNullException.MESSAGE, new List<String> { expectedNullParam });
        System.assertEquals(expectedErrorMessage, actualException.getMessage(), 'Expected the correct exception message.');
    }
}