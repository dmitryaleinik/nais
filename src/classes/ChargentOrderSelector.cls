/**
 * @description Queries for ChargentOrders__ChargentOrder__c records.
 **/
public class ChargentOrderSelector extends fflib_SObjectSelector {
    @testVisible private static final String CHARGENT_ORDER_IDS_PARAM = 'chargentOrderIds';

    /**
     * @description Select Chargent Order records by their Ids.
     * @param chargentOrderIds The Ids to select the records by.
     * @return A list of ChargentOrders__ChargentOrder__c records.
     * @throws An ArgumentNullException if the param is null.
     */
    public List<ChargentOrders__ChargentOrder__c> selectById(Set<Id> chargentOrderIds) {
        ArgumentNullException.throwIfNull(chargentOrderIds, CHARGENT_ORDER_IDS_PARAM);

        assertIsAccessible();

        return Database.query(String.format('SELECT {0} FROM {1} WHERE Id IN :chargentOrderIds ORDER BY {2}',
                new List<String> {
                    getFieldListString(),
                    getSObjectName(),
                    getOrderBy()
        }));
    }

    private Schema.SObjectType getSObjectType() {
        return ChargentOrders__ChargentOrder__c.getSObjectType();
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                ChargentOrders__ChargentOrder__c.Name,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Account__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Balance_Due__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Bank_Account_Name__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Bank_Account_Number__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Bank_Account_Type__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Bank_Name__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Bank_Routing_Number__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Billing_Address__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Billing_Address_Line_2__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Billing_City__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Billing_Company__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Billing_Country__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Billing_Email__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Billing_Fax__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Billing_First_Name__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Billing_Last_Name__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Billing_Phone__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Billing_State__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Billing_State_Province__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Billing_Zip_Postal__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Card_Expiration_Month__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Card_Month_Indicator__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Card_Expiration_Year__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Card_Year_Indicator__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Card_Last_4__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Credit_Card_Name__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Card_Number__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Card_Security_Code__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Card_Type__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Charge_Amount__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Charge_Date__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Check_Number__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Credit_Amount__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Description__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Gateway__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Gateway_Card_Handling__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Last_Transaction__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Payment_Method__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Status__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Tokenization__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__Update_Token__c,
                ChargentOrders__ChargentOrder__c.ChargentOrders__UseTokenization__c
        };
    }

    /**
     * @description Return the records in order from newest to oldest.
     */
    public override String getOrderBy() {
        return 'CreatedDate DESC';
    }

    /**
     * @description Singleton property ensuring external sources do not
     *              instantiate this directly.
     */
    public static ChargentOrderSelector Instance {
        get {
            if (Instance == null) {
                Instance = new ChargentOrderSelector();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to enforce singleton access
     *              via the Instance property.
     */
    private ChargentOrderSelector() {}
}