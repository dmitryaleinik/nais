@isTest
private class ArgumentNullExceptionTest {
    private static final String PARAMETER_NAME = 'paramName';
    private static final String PARAMETER = 'Just a generic string.';

    @isTest
    private static void argumentNullException_parameterIsNotNull_expectNoException() {
        try {
            Test.startTest();
            ArgumentNullException.throwIfNull(PARAMETER, PARAMETER_NAME);
        } catch (Exception e) {
            System.assert(false, 'Expected there to not be an exception thrown.');
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void argumentNullException_parameterIsNull_expectException() {
        try {
            Test.startTest();
            ArgumentNullException.throwIfNull(null, PARAMETER_NAME);

            System.assert(false, 'Expected an exception to be thrown.');
        } catch (Exception e) {
            System.assertEquals(ArgumentNullException.class.getName(), e.getTypeName(),
                    'Expected an ArgumentNullException.');

            String expectedMessage = String.format(ArgumentNullException.MESSAGE, new List<String> { PARAMETER_NAME });
            System.assertEquals(expectedMessage, e.getMessage(), 'Expected the expectedMessage returned.');
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void argumentNullException_parameterNameIsNull_expectException() {
        try {
            Test.startTest();
            ArgumentNullException.throwIfNull(null, null);

            System.assert(false, 'Expected an exception to be thrown.');
        } catch (Exception e) {
            System.assertEquals(ArgumentNullException.class.getName(), e.getTypeName(),
                    'Expected an ArgumentNullException.');
            System.assertEquals(ArgumentNullException.NO_PARAMETER_MESSAGE, e.getMessage(),
                    'Expected the No Parameter message returned.');
        } finally {
            Test.stopTest();
        }
    }
}