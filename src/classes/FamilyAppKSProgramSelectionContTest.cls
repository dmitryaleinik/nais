@isTest
private class FamilyAppKSProgramSelectionContTest {

    private static Account school1;
    private static Student_Folder__c studentFolder11;
    private static PFS__c pfsA;
    private static Applicant__c applicant1A;
    private static Integer uniquefier = 1;
    
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

    static void SetupData() {
        Account schoolKS = TestUtils.createAccount('Test School KS 1', RecordTypes.schoolAccountTypeId, 3, true);
        SchoolPortalSettings.KS_School_Account_Id = schoolKS.Id;
        
        Account a = TestUtils.createAccount('test account', RecordTypes.schoolAccountTypeId, 500, true);
        Contact c = TestUtils.createContact('test contact', a.Id, RecordTypes.schoolStaffContactTypeId, true);
        
        Id portalProfileId = [select Id from Profile where Name = :ProfileSettings.SchoolAdminProfileName].Id;
        
        User u = TestUtils.createPortalUser('test portaluser', 'testFamAppKS@exponentpartners.com' + String.valueOf(uniquefier++), 'test', c.Id, portalProfileId, true, false);
        
        System.runAs(thisUser){
            Database.insert(u);
        }
        
        List<Academic_Year__c> years = TestUtils.createAcademicYears();
        
        List<Annual_Setting__c> settings = new List<Annual_Setting__c>();
        Integer i = 0;
        Integer numberOfAcademicYears = years.size();
        for(Academic_Year__c year : years) {
            // skip last year in list; '2011-2012'
            if (i < numberOfAcademicYears - 1) {
                Annual_Setting__c annualSetting = new Annual_Setting__c(
                    School__c = schoolKS.Id,
                    Academic_Year__c = year.Id,
                    PFS_Reminder_Date__c = date.newInstance(2013, 3, 31),
                    Pauahi_Keiki_Scholars_Schools_Cycle_1__c = 'School1; School2; School3',
                    Pauahi_Keiki_Schools_Cycle_1_N_Z__c = 'School4; School5; School6',
                    Pauahi_Keiki_Scholars_Schools_Cycle_2__c = 'School1; School2; School3',
                    Pauahi_Keiki_Schools_Cycle_2_N_Z__c = 'School4; School5; School6',
                    Kipona_Scholarship_Program_Schools__c = 'School1; School2; School3',
                    Document_Reminder_Date__c = date.newInstance(2013, 3, 31));
                
                settings.add(annualSetting);
            }
            i++;
        }

        insert settings;        
        
        //setup PFS
        // create test data
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        
        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, true);
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, true);
        
        studentFolder11 = TestUtils.createStudentFolder('Student Folder 1.1', academicYearId, student1.Id, true);
        
        school1 = TestUtils.createAccount('Test School 1', RecordTypes.schoolAccountTypeId, 3, true);
        
        pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, true);
        applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, true);
        
        //reload pfs
        String pfsSql = 'SELECT ' + String.Join(ExpCore.GetAllFieldNames('PFS__c'), ',') + ', (SELECT ' + String.Join(ExpCore.GetAllFieldNames('Applicant__c'), ',') + ' FROM Applicants__r) FROM PFS__c LIMIT 1';
        pfsA = Database.query(pfsSql);

    }
    
    @isTest
    private static void TestAnnualSetting()
    {
        FamilyAppKSProgramSelectionContTest.SetupData();
        
        System.Test.startTest();
        FamilyAppKSProgramSelectionCont controller = new FamilyAppKSProgramSelectionCont();
        controller.pfs = pfsA;
        
        System.assertNotEquals(controller.AnnualSetting, null);
        
        System.Test.stopTest();
    }
    
    @isTest
    private static void TestReceivedK12FinancialAidOptions()
    {
        System.Test.startTest();
        FamilyAppKSProgramSelectionCont controller = new FamilyAppKSProgramSelectionCont();
        System.assertNotEquals(controller.ReceivedK12FinancialAidOptions, null);
        System.assert(!controller.ReceivedK12FinancialAidOptions.isEmpty());
        
        System.Test.stopTest();
    }
    
    @isTest
    private static void TestReceivedPreSchoolFinancialAidOptions()
    {
        System.Test.startTest();
        FamilyAppKSProgramSelectionCont controller = new FamilyAppKSProgramSelectionCont();
        System.assertNotEquals(controller.ReceivedPreSchoolFinancialAidOptions, null);
        System.assert(!controller.ReceivedPreSchoolFinancialAidOptions.isEmpty());
        
        System.Test.stopTest();
    }
    
    @isTest
    private static void TestPkCycle1SchoolOptions()
    {
        FamilyAppKSProgramSelectionContTest.SetupData();
        
        System.Test.startTest();
        FamilyAppKSProgramSelectionCont controller = new FamilyAppKSProgramSelectionCont();
        controller.pfs = pfsA;
        
        System.assertNotEquals(controller.PkCycle1SchoolOptions, null);
        System.assert(!controller.PkCycle1SchoolOptions.isEmpty());
        
        System.Test.stopTest();
    }
    
    @isTest
    private static void TestPkCycle2SchoolOptions() {
        
        FamilyAppKSProgramSelectionContTest.SetupData();
        
        System.Test.startTest();
        FamilyAppKSProgramSelectionCont controller = new FamilyAppKSProgramSelectionCont();
        controller.pfs = pfsA;
        
        System.assertNotEquals(controller.PkCycle2SchoolOptions, null);
        System.assert(!controller.PkCycle2SchoolOptions.isEmpty());
        
        System.Test.stopTest();
    }
    
    @isTest
    private static void TestReceivedPKScholarshipOptions()
    {
        System.Test.startTest();
        FamilyAppKSProgramSelectionCont controller = new FamilyAppKSProgramSelectionCont();
        System.assertNotEquals(controller.ReceivedPKScholarshipOptions, null);
        System.assert(!controller.ReceivedPKScholarshipOptions.isEmpty());
        
        System.Test.stopTest();
    }
    
    @isTest
    private static void TestKiponaSchoolOptions()
    {
        FamilyAppKSProgramSelectionContTest.SetupData();
        
        System.Test.startTest();
        FamilyAppKSProgramSelectionCont controller = new FamilyAppKSProgramSelectionCont();
        controller.pfs = pfsA;
        
        System.assertNotEquals(controller.KiponaSchoolOptions, null);
        System.assert(!controller.KiponaSchoolOptions.isEmpty());
        
        System.Test.stopTest();
    }
    
    @isTest
    private static void TestReceivedKiponaScholarshipOptions()
    {
        System.Test.startTest();
        FamilyAppKSProgramSelectionCont controller = new FamilyAppKSProgramSelectionCont();
        System.assertNotEquals(controller.ReceivedKiponaScholarshipOptions, null);
        System.assert(!controller.ReceivedKiponaScholarshipOptions.isEmpty());
        
        System.Test.stopTest();
    }
}