/**
* @description This is the controller for FamilyAppEZProcess.component.
*              And handles the logic to show a popup with questions to determine if a FP user should fill a "EZ" or "Full" PFS.
*              The PFS Type is stored in the PFS__c.Submission_Process__c field, based in the following conditions:
*                   1.- This popup would NOT appear if there is another country listed in the parent A address, international families do not qualify.
*                   2.- This new page has 5 questions that determines if the current PFS is "EZ" or "Full":
*                       2.1.- "True" to 3 = PFS EZ (stop)
*                       2.2.- "True" to 4 OR 5 = PFS Full (stop)
*                       2.3.- "True" to 1 AND 2 AND "False" to 4 AND 5 (PFS EZ)
*                   3.- Sections 6 and beyond are locked (gray-out) until PFS EZ questions are answered.  
*/
public class FamilyAppEZProcessController {
    
    private final String REQUIRED_MESSAGE = 'Required fields are missing.';
    private String thisScreen;
    
    public ApplicationUtils appUtils { get; set; }
    public PFS__c pfs { get; set; }
    public Boolean cancelClicked { get; set; }
    public Boolean popupSubmitted { get; set; }//This flag used to determine if the EZ Form was successfully submited. And show a popup whit a message with an "OK" button. If the "OK" button is clicked, then a redirection is made to the next section returned by the FamilyNavigationService.
    
    public FamilyAppEZProcessController() {
        
        thisScreen = ApexPages.currentPage().getParameters().get('screen');
        cancelClicked = ApexPages.currentPage().getParameters().get('ez') != null;
        popupSubmitted = ApexPages.currentPage().getParameters().get(PfsProcessService.Instance.PARAM_SHOW_EZ_ALERT_MODAL) != null;
    }//End-Constructor
    
    /**
    * @description The academic year Id of the current PFS.
    */
    private Id AcademicYearId {
        get{
            if( AcademicYearId == null ) {
                AcademicYearId = GlobalVariables.getAcademicYearByName(pfs.Academic_Year_Picklist__c).Id;
            }
            return AcademicYearId;
        }
        private set;
    }//End:AcademicYearId
    
    /**
    * @description This value is retrieved from Household_Income_Threshold__c object records, for the given Academic Year and Family Size (SFP-956).
    */
    public Decimal getHouseholdIncomeUnderThreshold_X() {
        
        Decimal familySize = PfsProcessService.Instance.getFamilySize(pfs);
        return familySize != null ? IncomeThresholdService.Instance.getIncomeThreshold(Integer.ValueOf(familySize), AcademicYearId) : 0;
    }//End:getHouseholdIncomeUnderThreshold_X
    
    /**
    * @description Retrieves the default value for Household Assets Under Threshold.
    */
    public Decimal getHouseholdAssetsUnderThreshold_X() {
        
        return FamilyPortalSettings.Default_EZ_Household_Assets;
    }//End:getHouseholdAssetsUnderThreshold_X
    
    /**
    * @description Determines if sections 1-6 have been  completed. If it so, the EZ popup with questions needs to be shown.
    */
    public Boolean basicSectionsCompleted {
        get{
            return PfsProcessService.Instance.checkPFSStatsToAllowNavigation(pfs);
        }
    }//End:basicSectionsCompleted
    
    /**
    * @description Determines if the current section, given by the "screen" param, is a section where the EZ popup can be shown.
    *              Per requirement this popup will be shown if the current screen is a section beyond 5.
    */
    public Boolean showPopup {
        get{
            return !isAllowedSection && pfs.Submission_Process__c == null;
        }
    }//End:showPopup
    
    /**
    * @description Determines if the PFS may apply for EZ, based on its ParentA country address.
    */
    public Boolean selectedCountryApply {
        get{
            return PfsProcessService.Instance.validCountryForEZ(pfs.Parent_A_Country__c);
        }
    }//End:selectedCountryApply
    
    /**
    * @description Determines if the current section, given by the "screen" param, is a section that can be access without the need to answer EZ popup questions.
    */
    private Boolean isAllowedSection {
        get{
            return PfsProcessService.Instance.isAllowedSection(thisScreen);
        }
    }//End:isAllowedSection
    
    /**
    * @description Validation to be applied to each field in the EZ questions popup.
    */
    private Boolean validate() {

        Map<String, String> fiedsByLabel = new Map<String, String>{
            appUtils.labels.Household_Income_Under_Threshold__c => 'Household_Income_Under_Threshold__c',
            appUtils.labels.Household_Assets_Under_Threshold__c => 'Household_Assets_Under_Threshold__c',
            appUtils.labels.Household_Has_Received_Benefits__c => 'Household_Has_Received_Benefits__c',
            appUtils.labels.Guardian_Owns_Business__c => 'Guardian_Owns_Business__c',
            appUtils.labels.Guardian_Self_Employed_Freelancer__c => 'Guardian_Self_Employed_Freelancer__c'
        };
        
        for(String label : fiedsByLabel.keySet()) {
            
            if( pfs.get(fiedsByLabel.get(label)) == null ) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, REQUIRED_MESSAGE));
                return false;
            }
        }
        
        return true;
    }//End:validate
    
    /**
    * @description Action executed when the "Submit" button is clicked.
    */
    public PageReference saveFormEZ() {
        
        if( validate() ) {
            pfs.StatSubmissionProcess__c = true;
            pfs.Submission_Process__c = PfsProcessService.Instance.calculateSubmissionProcess(pfs);
            
            if(pfs.Submission_Process__c == 'EZ') {
                pfs = PfsProcessService.Instance.updateNavigationBarFlagsForEZ(pfs, true);
            }
            update pfs;
            
            if(!basicSectionsCompleted) {
                //Redirects to the next incompleted section from 1-5
                return incompleteSectionAction(true);
            } else if(pfs.Submission_Process__c == 'Full' || PfsProcessService.Instance.isAllowedSection(thisScreen)){
                //Remains in the same screen
                return getPageReferenceToRedirect(new Map<String, String>{ 'id' => pfs.Id, 'screen' => thisScreen, PfsProcessService.Instance.PARAM_SHOW_EZ_ALERT_MODAL => '1'});
            } else {
                //Redirects to the next incompleted section from 6-20 (since EZ has some sections hidden)
                FamilyNavigationService.Response response = appUtils.locateNextScreen('FamilyIncome');
                return getPageReferenceToRedirect(new Map<String, String>{ 'id' => pfs.Id, 'screen' => response.NextPage, PfsProcessService.Instance.PARAM_SHOW_EZ_ALERT_MODAL => '1'});
            }
        }
        return null;
    }//End:saveFormEZ
    
    /**
    * @description Actions executed after the user clicks on "Ok" button, rendered in the modal shown after the EZ popup questions were successfully submited.
    */
    public PageReference pageRedirectionAfterSaveAndAcceptPromptMessage() {
        
        return getPageReferenceToRedirect(new Map<String, String>{ 'id' => pfs.Id, 'screen' => thisScreen});
    }//End:pageRedirectionAfterSaveAndAcceptPromptMessage
    
    /**
    * @description Returns a reference to redirect to an allowed section 1-5.
    * @param ezsuccess It is true if the user just submited successfully the EZ popup with questions.
    */
    @testVisible private PageReference incompleteSectionAction(Boolean ezsuccess) {
        
        FamilyNavigationService.Response response = appUtils.locateNextScreen('HouseholdInformation');
        
        Map<String, String> params = new Map<String, String>{ 'id' => pfs.Id, 'screen' => response.NextPage};
        if(ezsuccess) {//To show the success modal
            params.put(PfsProcessService.Instance.PARAM_SHOW_EZ_ALERT_MODAL, '1');
        }
        return getPageReferenceToRedirect(params);
    }//End:incompleteSectionAction
    
    /**
    * @description Basic method retrieve a page reference with the given params.
    * @param params A map with the parameters to be added to the pageReference.
    */
    private PageReference getPageReferenceToRedirect(Map<String, String> params) {
        
        PageReference returnTarget = Page.FamilyApplicationMain;
        returnTarget.getParameters().putAll(params);
        returnTarget.setRedirect(true);
        return returnTarget;
    }//End:getPageReferenceToRedirect
    
    /**
    * @description Action executed when the "Cancel" button is clicked.
    */
    public PageReference cancelFormEZ() {
        
        cancelClicked = true;
        if(!isAllowedSection && !basicSectionsCompleted) {
            return incompleteSectionAction(false);
        }else {
            return getPageReferenceToRedirect(new Map<String, String>{ 'id' => pfs.Id, 'screen' => 'ParentsGuardians', 'ez' => '1'});
        }
    }//End:cancelFormEZ
    
    /**
    * @description This will tell us if we should disable (through JS+CSS) sections from 6-20, to avoid the EZ popup to be shown. 
    *              Because, at that point we do not have the data necessary to calculate the FamilySize, used to render the interest threshold.
    *              Or if we haven't complete section 5 either.
    */
    public Boolean getLockNavigationForNonAllowedSections() {
        
        return PfsProcessService.Instance.LockNavigationForNonAllowedSections(pfs);
    }//End:getLockNavigationForNonAllowedSections
}