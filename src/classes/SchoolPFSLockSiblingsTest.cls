@isTest
private class SchoolPFSLockSiblingsTest {
    
    private static final String PFS_STATUS_UNPAID = 'Unpaid';
    private static Account school1;
    private static Contact student1;
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    private static User schoolPortalUser;
    private static Academic_Year__c currentAcademicYear;
    private static Contact parentA, parentB;
    private static School_PFS_Assignment__c spfsa1, spfsa2, spfsa3;

    private static void createTestData() {
        Account family = AccountTestData.Instance.asFamily().create();

        String activeStatus = GlobalVariables.getActiveSSSStatusForTest();
        school1 = AccountTestData.Instance.asSchool()
            .forSSSSubscriberStatus(activeStatus)
            .forPortalVersion(SubscriptionTypes.BASIC_TYPE)
            .asSchool().create();
        insert new List<Account>{family, school1};

        Subscription__c subscription1 = SubscriptionTestData.Instance
            .forStartDate(system.today().addDays(-14))
            .forEndDate(system.today().addYears(1))
            .forAccount(school1.Id).insertSubscription();

        currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;

        parentA = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        parentB = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .forAccount(school1.Id)
            .asSchoolStaff().create();
        student1 = ContactTestData.Instance.asStudent().create();
        Contact student2 = ContactTestData.Instance.asStudent().create();
        insert new List<Contact>{parentA, parentB, schoolStaff, student1, student2};

        System.runAs(thisUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        // SFP-672 Create Share between schoolPortalUser and its school because criteria based sharing rules do not run in
        // unit tests.
        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);

        System.runAs(schoolPortalUser){
            PFS__c pfs1 = PfsTestData.Instance
                .asSubmitted()
                .forPaymentStatus(PFS_STATUS_UNPAID)
                .forParentA(parentA.Id)
                .forOriginalSubmissionDate(System.today().addDays(-3))
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            PFS__c pfs2 = PfsTestData.Instance
                .asSubmitted()
                .forPaymentStatus(PFS_STATUS_UNPAID)
                .forParentA(parentB.Id)
                .forOriginalSubmissionDate(System.today().addDays(-5))
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<PFS__c> {pfs1, pfs2});

            Applicant__c applicant1A = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student1.Id).create();
            Applicant__c applicant1B = ApplicantTestData.Instance
                .forPfsId(pfs2.Id)
                .forContactId(student1.Id).create();
            Applicant__c applicant2A = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student2.Id).create();
            Dml.WithoutSharing.insertObjects(new List<Applicant__c> {applicant1A, applicant1B, applicant2A});

            Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
                .forStudentId(student1.Id)
                .forSchoolId(school1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Student_Folder__c studentFolder2 = StudentFolderTestData.Instance
                .forStudentId(student2.Id)
                .forSchoolId(school1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<Student_Folder__c> {studentFolder1, studentFolder2});

            String fifthGrade = '5';
            spfsa1 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1A.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school1.Id).create();
            spfsa2 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1B.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school1.Id).create();
            spfsa3 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant2A.Id)
                .forStudentFolderId(studentFolder2.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school1.Id).create();
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> {spfsa1, spfsa2, spfsa3});
        }

        // Share records to school staff
        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);
    }

    @isTest
    private static void testOneSibling(){
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
        
        createTestData();
        
        Test.startTest();
            List<School_PFS_Assignment__c> testSiblingSPFSs = [Select Id, Family_May_Submit_Updates__c from School_PFS_Assignment__c where Id = :spfsa3.Id];
            for (School_PFS_Assignment__c testSPFS : testSiblingSPFSs){
                System.assert(testSPFS.Family_May_Submit_Updates__c == null || testSPFS.Family_May_Submit_Updates__c == 'Yes');
            }
            
            spfsa1.Family_May_Submit_Updates__c = 'No';
            update spfsa1;
            
            testSiblingSPFSs = [Select Id, Family_May_Submit_Updates__c from School_PFS_Assignment__c where Id = :spfsa3.Id];
            for (School_PFS_Assignment__c testSPFS : testSiblingSPFSs){
                System.assertEquals('No', testSPFS.Family_May_Submit_Updates__c);
            }
            
            //resetting firstrun context
            SchoolPFSLockSiblings.isFirstRun = true;
            spfsa1.Family_May_Submit_Updates__c = 'Yes';
            update spfsa1;
            
            testSiblingSPFSs = [Select Id, Family_May_Submit_Updates__c from School_PFS_Assignment__c where Id = :spfsa3.Id];
            for (School_PFS_Assignment__c testSPFS : testSiblingSPFSs){
                System.assertEquals('Yes', testSPFS.Family_May_Submit_Updates__c);
            }
        Test.stopTest();
    }

    @isTest
    private static void testThreeSiblingNo(){
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
        
        // SETTING UP RECORDS EXPLICITLY
        Account family = TestUtils.createAccount('Individual Account', RecordTypes.individualAccountTypeId, 5, false);
        school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, 5, false);
        school1.Alert_Frequency__c = 'Weekly digest of new applications';
        school1.SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest();
        Account school2 = TestUtils.createAccount('school2', RecordTypes.schoolAccountTypeId, null, false);
        school2.SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest();
        Account schoolKS = TestUtils.createAccount('Test School KS 1', RecordTypes.schoolAccountTypeId, 3, false);
        insert new List<Account> { family, school1, school2, schoolKS };
        
        // Create school custom settings
        SchoolPortalSettings.KS_School_Account_Id = schoolKS.Id;
        
        Contact staff1 = TestUtils.createContact('Staff 1', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
        staff1.Email = 'staff1@test.org';
        staff1.PFS_Alert_Preferences__c = 'Receive Alerts';
                
        parentA = TestUtils.createContact('Parent A', family.Id, RecordTypes.parentContactTypeId, false);
        parentB = TestUtils.createContact('Parent B', family.Id, RecordTypes.parentContactTypeId, false);
        Contact parentC = TestUtils.createContact('Parent C', family.Id, RecordTypes.parentContactTypeId, false);
        
        student1 = TestUtils.createContact('Student 1', family.Id, RecordTypes.studentContactTypeId, false);
        Contact student2 = TestUtils.createContact('Student 2', family.Id, RecordTypes.studentContactTypeId, false);
        Contact student3 = TestUtils.createContact('Student 3', family.Id, RecordTypes.studentContactTypeId, false);
        Contact student4 = TestUtils.createContact('Student 4', family.Id, RecordTypes.studentContactTypeId, false);
        
        insert new List<Contact> { staff1, parentA, parentB, parentC, student1, student2, student3, student4 };
        
        User portalUser = TestUtils.createPortalUser(staff1.LastName, staff1.Email + String.valueOf(Math.random()) + String.valueOf(Math.random()), 'alias', staff1.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);
        
        System.runAs(thisUser){
            insert portalUser;
        }
        
        List<Academic_Year__c> academicYears = TestUtils.createAcademicYears();
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        Id academicYearPastId = GlobalVariables.getPreviousAcademicYear().Id;
                
        // Create SSSConstants records for the Academic Years
        SSS_Constants__c sssConst1 = TestUtils.createSSSConstants(academicYearId, false);
        SSS_Constants__c sssConst2 = TestUtils.createSSSConstants(academicYearPastId, false);
        Database.insert(new List<SSS_Constants__c>{sssConst1, sssConst2});
        
        TestUtils.createUnusualConditions(academicYearId, true);

        
        PFS__c pfs1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
        pfs1.Original_Submission_Date__c = (Datetime)System.today().addDays(-3);
        pfs1.PFS_Status__c = 'Application Submitted';
        pfs1.Payment_Status__c = 'Unpaid';
        
        PFS__c pfs2 = TestUtils.createPFS('PFS B', academicYearId, parentB.Id, false);
        pfs2.Original_Submission_Date__c = (Datetime)System.today().addDays(-5);
        pfs2.PFS_Status__c = 'Application Submitted';
        pfs2.Payment_Status__c = 'Paid in Full';

        insert new List<PFS__c> { pfs1, pfs2 };

        
        Applicant__c applicant1 = TestUtils.createApplicant(student1.Id, pfs1.Id, false);
        Applicant__c applicant2 = TestUtils.createApplicant(student2.Id, pfs1.Id, false);
        Applicant__c applicant3 = TestUtils.createApplicant(student3.Id, pfs2.Id, false);
        Applicant__c applicant4 = TestUtils.createApplicant(student4.Id, pfs2.Id, false);
        insert new List<Applicant__c> { applicant1, applicant2, applicant3, applicant4};
        
        Student_Folder__c studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
        Student_Folder__c studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', academicYearId, student2.Id, false);
        Student_Folder__c studentFolder3 = TestUtils.createStudentFolder('Student Folder 3', academicYearId, student3.Id, false);
        Student_Folder__c studentFolder4 = TestUtils.createStudentFolder('Student Folder 4', academicYearId, student4.Id, false);
        insert new List<Student_Folder__c> { studentFolder1, studentFolder2, studentFolder3, studentFolder4};

        spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1.Id, school1.Id, studentFolder1.Id, false);
        spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2.Id, school1.Id, studentFolder2.Id, false);
        spfsa3 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant3.Id, school1.Id, studentFolder3.Id, false);
        School_PFS_Assignment__c spfsa4 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant4.Id, school1.Id, studentFolder4.Id, false);
        // put them in this order so that spfsa1 has earlier created date
        List<School_PFS_Assignment__c> allSPFSAs = new List<School_PFS_Assignment__c> { spfsa2, spfsa1, spfsa3, spfsa4};
        insert allSPFSAs;

        // starting test here to reset soql queries
        Test.startTest();
                
        List<School_PFS_Assignment__c> testUpdatedSPFSs = new List<School_PFS_Assignment__c> { spfsa1, spfsa3 };
        List<School_PFS_Assignment__c> testSiblingSPFSs = new List<School_PFS_Assignment__c> { spfsa2, spfsa4 };
        
        testSiblingSPFSs = [Select Id, Family_May_Submit_Updates__c from School_PFS_Assignment__c where Id in :testSiblingSPFSs];
        for (School_PFS_Assignment__c testSPFS : testSiblingSPFSs){
            System.assert(testSPFS.Family_May_Submit_Updates__c == null || testSPFS.Family_May_Submit_Updates__c == 'Yes');
        }
        
        
        for (School_PFS_Assignment__c updatedSPFS : testUpdatedSPFSs){
            updatedSPFS.Family_May_Submit_Updates__c = 'No';
        }
        update testUpdatedSPFSs;

        testSiblingSPFSs = [Select Id, Family_May_Submit_Updates__c from School_PFS_Assignment__c where Id in :testSiblingSPFSs];
        for (School_PFS_Assignment__c testSPFS : testSiblingSPFSs){
            System.assertEquals('No', testSPFS.Family_May_Submit_Updates__c);
        }
        Test.stopTest();
    }

    @isTest
    private static void testThreeSiblingYes(){
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
        
        // SETTING UP RECORDS EXPLICITLY
        Account family = TestUtils.createAccount('Individual Account', RecordTypes.individualAccountTypeId, 5, false);
        school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, 5, false);
        school1.Alert_Frequency__c = 'Weekly digest of new applications';
        school1.SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest();
        Account school2 = TestUtils.createAccount('school2', RecordTypes.schoolAccountTypeId, null, false);
        school2.SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest();
        Account schoolKS = TestUtils.createAccount('Test School KS 1', RecordTypes.schoolAccountTypeId, 3, false);
        insert new List<Account> { family, school1, school2, schoolKS };
        
        // Create school custom settings
        SchoolPortalSettings.KS_School_Account_Id = schoolKS.Id;
        
        Contact staff1 = TestUtils.createContact('Staff 1', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
        staff1.Email = 'staff1@test.org';
        staff1.PFS_Alert_Preferences__c = 'Receive Alerts';
                
        parentA = TestUtils.createContact('Parent A', family.Id, RecordTypes.parentContactTypeId, false);
        parentB = TestUtils.createContact('Parent B', family.Id, RecordTypes.parentContactTypeId, false);
        Contact parentC = TestUtils.createContact('Parent C', family.Id, RecordTypes.parentContactTypeId, false);
        
        student1 = TestUtils.createContact('Student 1', family.Id, RecordTypes.studentContactTypeId, false);
        Contact student2 = TestUtils.createContact('Student 2', family.Id, RecordTypes.studentContactTypeId, false);
        Contact student3 = TestUtils.createContact('Student 3', family.Id, RecordTypes.studentContactTypeId, false);
        Contact student4 = TestUtils.createContact('Student 4', family.Id, RecordTypes.studentContactTypeId, false);
        
        insert new List<Contact> { staff1, parentA, parentB, parentC, student1, student2, student3, student4 };
        
        User portalUser = TestUtils.createPortalUser(staff1.LastName, staff1.Email + String.valueOf(Math.random()) + String.valueOf(Math.random()), 'alias', staff1.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);
        
        System.runAs(thisUser){
            insert portalUser;
        }
        
        List<Academic_Year__c> academicYears = TestUtils.createAcademicYears();
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        Id academicYearPastId = GlobalVariables.getPreviousAcademicYear().Id;
                
        // Create SSSConstants records for the Academic Years
        SSS_Constants__c sssConst1 = TestUtils.createSSSConstants(academicYearId, false);
        SSS_Constants__c sssConst2 = TestUtils.createSSSConstants(academicYearPastId, false);
        Database.insert(new List<SSS_Constants__c>{sssConst1, sssConst2});
        
        TestUtils.createUnusualConditions(academicYearId, true);

        
        PFS__c pfs1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
        pfs1.Original_Submission_Date__c = (Datetime)System.today().addDays(-3);
        pfs1.PFS_Status__c = 'Application Submitted';
        pfs1.Payment_Status__c = 'Unpaid';
        
        PFS__c pfs2 = TestUtils.createPFS('PFS B', academicYearId, parentB.Id, false);
        pfs2.Original_Submission_Date__c = (Datetime)System.today().addDays(-5);
        pfs2.PFS_Status__c = 'Application Submitted';
        pfs2.Payment_Status__c = 'Paid in Full';

        insert new List<PFS__c> { pfs1, pfs2 };

        
        Applicant__c applicant1 = TestUtils.createApplicant(student1.Id, pfs1.Id, false);
        Applicant__c applicant2 = TestUtils.createApplicant(student2.Id, pfs1.Id, false);
        Applicant__c applicant3 = TestUtils.createApplicant(student3.Id, pfs2.Id, false);
        Applicant__c applicant4 = TestUtils.createApplicant(student4.Id, pfs2.Id, false);
        insert new List<Applicant__c> { applicant1, applicant2, applicant3, applicant4};
        
        Student_Folder__c studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
        Student_Folder__c studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', academicYearId, student2.Id, false);
        Student_Folder__c studentFolder3 = TestUtils.createStudentFolder('Student Folder 3', academicYearId, student3.Id, false);
        Student_Folder__c studentFolder4 = TestUtils.createStudentFolder('Student Folder 4', academicYearId, student4.Id, false);
        insert new List<Student_Folder__c> { studentFolder1, studentFolder2, studentFolder3, studentFolder4};

        spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1.Id, school1.Id, studentFolder1.Id, false);
        spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2.Id, school1.Id, studentFolder2.Id, false);
        spfsa3 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant3.Id, school1.Id, studentFolder3.Id, false);
        School_PFS_Assignment__c spfsa4 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant4.Id, school1.Id, studentFolder4.Id, false);
        // put them in this order so that spfsa1 has earlier created date
        List<School_PFS_Assignment__c> allSPFSAs = new List<School_PFS_Assignment__c> { spfsa2, spfsa1, spfsa3, spfsa4};
        insert allSPFSAs;
        
        // starting test here to reset soql queries
        Test.startTest();
                
        List<School_PFS_Assignment__c> testUpdatedSPFSs = new List<School_PFS_Assignment__c> { spfsa1, spfsa3 };
        List<School_PFS_Assignment__c> testSiblingSPFSs = new List<School_PFS_Assignment__c> { spfsa2, spfsa4 };
        
        testSiblingSPFSs = [Select Id, Family_May_Submit_Updates__c from School_PFS_Assignment__c where Id in :testSiblingSPFSs];
        for (School_PFS_Assignment__c testSPFS : testSiblingSPFSs){
            System.assert(testSPFS.Family_May_Submit_Updates__c == null || testSPFS.Family_May_Submit_Updates__c == 'Yes');
        }

        for (School_PFS_Assignment__c updatedSPFS : testUpdatedSPFSs){
            updatedSPFS.Family_May_Submit_Updates__c = 'Yes';
        }
        update testUpdatedSPFSs;
        
        testSiblingSPFSs = [Select Id, Family_May_Submit_Updates__c from School_PFS_Assignment__c where Id in :testSiblingSPFSs];
        for (School_PFS_Assignment__c testSPFS : testSiblingSPFSs){
            System.assertEquals('Yes', testSPFS.Family_May_Submit_Updates__c);
        }
        
        Test.stopTest();
    }
}