@isTest
private class FamilyAppApplicantContTest
{

    @isTest
    private static void testConstructor()
    {
        FamilyAppMainControllerTest.initData();
        
        FamilyAppApplicantCont appController = new FamilyAppApplicantCont();
        appController.pfs = FamilyAppMainControllerTest.pfsA;
        appController.appUtils = new ApplicationUtils(UserInfo.getLanguage(), appController.pfs);
        appController.applicantId = FamilyAppMainControllerTest.applicant1A.Id;

        System.assertEquals(true, appController.areApplicants);
        appController.deleteApplicant();
    }

    @isTest
    private static void testFamilyAppApplicantCont ()
    {
        TestDataFamily tdf = new TestDataFamily();
        tdf.applicant2A.SSN__c = '4444';
        update tdf.applicant2A;

        Business_Farm__c bizFarm = BusinessFarmTestData.Instance
            .forPfsId(tdf.pfsA.Id).DefaultBusinessFarm;

        ApplicationUtils appUtils = new ApplicationUtils(UserInfo.getLanguage(), tdf.pfsA);

        Test.startTest();
            FamilyAppApplicantCont compController = new FamilyAppApplicantCont();
            compController.pfs = tdf.pfsA;
            compController.appUtils = appUtils;

            System.assertNotEquals(null, compController.blankApplicant);
            System.assertEquals(null, compController.createApplicant);
            System.assertEquals(null, compController.rowNum);
            System.assertEquals(null, compController.applicantIndex);
            System.assertEquals(null, compController.applicantId);
            System.assertEquals(TRUE, compController.areApplicants);

            System.assertEquals(1, [
                SELECT count() 
                FROM School_Biz_Farm_Assignment__c 
                WHERE School_PFS_Assignment__r.Applicant__c = :tdf.applicant2A.id]);

            compController.applicantId = tdf.applicant2A.Id;
            compController.deleteApplicant();

            System.assertEquals(0, [
                SELECT count() 
                FROM School_Biz_Farm_Assignment__c 
                WHERE School_PFS_Assignment__r.Applicant__c = :tdf.applicant2A.id]);
        Test.stopTest();    
    }
}