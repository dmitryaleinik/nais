/**
 * @description This queueable class is used to transfer verification values to School PFS Assignments. This class will
 *              only process the records specified at the time this job is queued. The logic for actually copying the
 *              values can be found in the AutoSPAValueAssignBatch.cls.
 */
public class CopyVerificationForPfsAssignmentsQueue implements Queueable {

    private static final String RECORD_IDS_PARAM = 'recordIds';

    private Set<Id> pfsAssignmentIds;

    /**
     * @description Constructor for a new queueable job. The records to process are specified by the Ids.
     * @param recordIds The Ids of the school pfs assignments to process.
     * @throws ArgumentNullException if recordIds is null.
     */
    public CopyVerificationForPfsAssignmentsQueue(Set<Id> recordIds) {
        ArgumentNullException.throwIfNull(recordIds, RECORD_IDS_PARAM);

        this.pfsAssignmentIds = recordIds;
    }

    /**
     * @description Defers to the AutoSPAValueAssignBatch class to assign verification values to the specified School
     *              Pfs Assignment records.
     */
    public void execute(QueueableContext context) {
        AutoSPAValueAssignBatch.autoAssignVerificationValues(pfsAssignmentIds);
    }
}