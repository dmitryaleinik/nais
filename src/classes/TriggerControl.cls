/***
 * Utility class for tracking of Trigger execution and recursion
 * SL, Exponent Partners 2013
 */
public class TriggerControl {
    
    // keep track of number of levels of recursion
    public Integer triggerDepth = 0;
    
    // initialize the trigger context
    public void beginTrigger() {
        // increment the trigger depth
        triggerDepth += 1;
    }
    
    // finalize the trigger context
    public void endTrigger() {
        // decrement the trigger depth
        triggerDepth -= 1;
    }
    
    // returns true if the trigger is running in a recursive state
    public boolean isRecursiveContext() {
        if (triggerDepth > 1) {
            return true;
        }
        else {
            return false;
        }
    }
}