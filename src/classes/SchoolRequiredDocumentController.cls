/*
* SPEC-098, SPEC-102
* Nathan, Exponent Partners, 2013
*/
public class SchoolRequiredDocumentController implements SchoolAcademicYearSelectorInterface {

    // input
    public String schoolId {get; set;}
    public String errorMessageStr {get; set;}
    private map<String,DocumentRow> documentRowsById {get; set;}

    /*Properties*/
    
    public String schoolSpecificDocumentType {
        get{
            return ApplicationUtils.schoolSpecificDocType();
        }
        set;
    }

    public List <SelectOption> academicYearOptions {get; set;}
    public List <SelectOption> documentYearOptions {get; set;}
    public String selectedDocumentYear {get;set;}
    public Boolean premiumSubscription {
        get {
            Subscription__c subscription = GlobalVariables.getMostRecentSubscription();            
            if ( subscription.Subscription_Type__c == 'Premium' && subscription.Status__c == 'Current' ) {
                return true;
            } else {
                return false;
            }
        } set;        
    }
    
    public String selectedAcademicYear {
        get {
            if (selectedAcademicYear == null) {
                loadAcademicYearOptions();
            }
            return selectedAcademicYear;
        } set;
    }
    public SchoolRequiredDocumentController thisController {get {return this;}}
    public Account school {get; private set;}
    public List <Required_Document__c> requiredDocumentList {
        get {
            if (requiredDocumentList == null) {
                loadRequiredDocument();
            }
            return requiredDocumentList;
        } private set;
    }
    public list<DocumentRow> docRowList{get; set;}
    public Required_Document__c requiredDocument {get; set;}
    public Academic_Year__c academicYear {get; private set;}
    public Boolean isKSSchoolUser {get; private set;}
    public String errorMessage {get; private set;}    // used in test class
    public Boolean newRendered {get; private set;}
    public Boolean editRendered {get; private set;}
    public AttachmentsHandler attachments{get; set;}
    
    // [CH] NAIS-1521 Use custom setting to show and hide document year
    /*private Map<String, Document_Type_Settings__c> docTypeMap {
        get
        {
            if(docTypeMap == null){
                docTypeMap = new Map<String, Document_Type_Settings__c>{};
                for(Document_Type_Settings__c docTypeSetting : Document_Type_Settings__c.getAll().values()){
                    docTypeMap.put(docTypeSetting.Full_Type_Name__c, docTypeSetting);
                }
            }
            
            return docTypeMap;
        }
        set;
    }*/
    
    public Boolean isTaxDocument {
        get{

            // NAIS-1842 [DP] 03.30.2015 refactor to use generic method
            String theDocType = requiredDocument == null ? '' : requiredDocument.Document_Type__c;
            return ApplicationUtils.isTaxDocument(theDocType);
            
            //if(requiredDocument != null){
            //    Document_Type_Settings__c documentSetting = docTypeMap.get(requiredDocument.Document_Type__c);
            //    if(documentSetting != null){
            //        return documentSetting.Tax_Document__c;
            //    } 
            //    else{ return false; }
            //}
            //else{ return false; }
        }    
        set;    
    }
    
    /*End Properties*/
    
    /*Initialization*/

    Map <String, Academic_Year__c> id_academicYear_map = new Map <String, Academic_Year__c>();
    Map <String, Required_Document__c> id_requiredDocument_map = new Map <String, Required_Document__c>();
    
    // constructor
    public SchoolRequiredDocumentController() {
        this.readErrorMessages();
        attachments = new AttachmentsHandler();
        
        // init
        school = new Account();
        
        // Only conditionally instantiate this in case this is an entire page
        //  refresh with error messages 
        requiredDocument = new Required_Document__c();
            
        academicYear = new Academic_Year__c();
        newRendered = true;
        editRendered = false;
        
        // read from url params if specified. used for unit test
        String schoolIdParam = ApexPages.CurrentPage().getParameters().get('schoolId');
        if (schoolIdParam != null && schoolIdParam != '') {
            schoolId = schoolIdParam;
        } else {
            //schoolId = GlobalVariables.getCurrentUser().Contact.AccountId;
            schoolId = GlobalVariables.getCurrentSchoolId();
        }

        // read from url params if specified. used for unit test
        String isKSSchoolUserParam = ApexPages.CurrentPage().getParameters().get('isKSSchoolUser');
        if (isKSSchoolUserParam != null && isKSSchoolUserParam != '' &&
            (isKSSchoolUserParam == 'true' || isKSSchoolUserParam == 'false')) {
            if (isKSSchoolUserParam == 'true') {
                isKSSchoolUser = true;
            } else {
                isKSSchoolUser = false;
            }
        } else {
            isKSSchoolUser = GlobalVariables.isKSSchoolUser();
        }
        
        loadSchool();
        loadRequiredDocument();
        loadDocumentYears();
    }

    /*End Initialization*/

    /*Action Methods*/

    public void onChangeAcademicYear() {
    
        if (selectedAcademicYear != null && id_academicYear_map.containsKey(selectedAcademicYear)) {
            academicYear = id_academicYear_map.get(selectedAcademicYear);
        }
        loadRequiredDocument();
        requiredDocument = new Required_Document__c();
        newRendered = true;
        editRendered = false;
    }    
    public void onChangeDocumentType() {
    
       String theDocType = requiredDocument == null ? '' : requiredDocument.Document_Type__c;
       isTaxDocument= ApplicationUtils.isTaxDocument(theDocType);
    }    
/* [CH] Not needed for the ActionFunction tag
    public void onChangeDocumentType() {
    
        // Data dictionary for object: Required Document, field: Document Type
        // Notes: last four items are conditionally required for KS
     [bc] Removed per NAIS-1206
        if (requiredDocument != null && isKSSchoolUser == true) {
            String docType = requiredDocument.Document_Type__c;

            requiredDocument.Conditionally_Required__c = docType == 'Guardianship Document' || 
                                                         docType == 'Cash Benefit Summary Statement' || 
                                                         docType == 'Divorce Decree' || 
                                                         docType == 'Expense and Resources Statement' ? 'Yes' : 'No'; 
        }  
        
    }
    */
    
    public void validateDocument(){
        if (selectedAcademicYear == null || id_academicYear_map.containsKey(selectedAcademicYear) == false){
            ErrorMessage('Please select an academic year');
        }
        
        if (isTaxDocument && requiredDocument.Document_Year__c == null) {
            ErrorMessage('Document Year is required');
        }
        
        // [CH] NAIS-1390 Adjusting logic to prevent more than one School/Organization Document
        // [CH] NAIS-1520 Making duplicate prevention also include the document label
        //         Detect whether we're inserting a new document, or updating an existing document
        if(requiredDocument.Id == null){ // If this is an insert
            if (requiredDocument.Document_Type__c == ApplicationUtils.schoolSpecificDocType()) {
                if(requiredDocument.Label_for_School_Specific_Document__c == null){
                    ErrorMessage('Label for ' + ApplicationUtils.schoolSpecificDocType() + ' is required for Document Type: ' + requiredDocument.Document_Type__c);
                }   
                // [CH] NAIS-1590 Reverting to prevent more than one School-Specific Document
                for(Required_Document__c existingDoc : requiredDocumentList){
                    if(existingDoc.Document_Type__c == ApplicationUtils.schoolSpecificDocType()){
                        ErrorMessage('You have already required a School/Organization Other Document:  ' + existingDoc.Label_for_School_Specific_Document__c);
                        break;
                    }
                }
                
                /*  Note: We might want to restore this logic at some point
                for(Required_Document__c existingDoc : requiredDocumentList){
                    if(existingDoc.Document_Type__c == ApplicationUtils.schoolSpecificDocType() && existingDoc.Label_for_School_Specific_Document__c == requiredDocument.Label_for_School_Specific_Document__c){
                        ErrorMessage('The required document list already contains a ' + ApplicationUtils.schoolSpecificDocType() + ': ' + existingDoc.Label_for_School_Specific_Document__c);
                        break;
                    }
                }
                */
            }
            else{
                for(Required_Document__c existingDoc : requiredDocumentList){    
                    if(existingDoc.Document_Type__c == requiredDocument.Document_Type__c && existingDoc.Document_Year__c == requiredDocument.Document_Year__c){
                        ErrorMessage('The required document list already contains a ' + existingDoc.Document_Type__c + ' for ' + existingDoc.Document_Year__c);
                        break;
                    }
                }
            }          
        }
    }
    
    public PageReference saveDocument()
    {
        errorMessageStr = null;
        // If there is a required document to be saved
        if(requiredDocument != null && requiredDocument.Document_Type__c != null){
            
            // [CH] NAIS-1521 Default the Document Year value if this is a non-tax document
            //   Need to fill in document year ahead of validation to support avoiding duplicates
            if(!isTaxDocument){
                //requiredDocument.Document_Year__c = academicYear.Name.split('-')[0];
                // NAIS-2191 [DP] 01.28.2015 -- now making non-tax docs the year previous to the acad year (e.g., 2014-2015 non-tax docs get a year of 2013)
                requiredDocument.Document_Year__c = String.valueOf(Integer.valueOf(academicYear.Name.split('-')[0]) - 1); 
            }
            //SFP-59, [G.S]
            if(String.isNotBlank(SelectedDocumentYear)){
                requiredDocument.Document_Year__c = SelectedDocumentYear;
            }
            // Validate that the document has all requirements
            validateDocument();
             
            // If there are no errors after validation
            if(!ApexPages.hasMessages(ApexPages.Severity.ERROR)){
                Savepoint sp = Database.setSavepoint();
                try {
                    requiredDocument.Academic_Year__c = selectedAcademicYear;    // required field
                   
                    // There is a validation rule. set it to null to avoid error
                    if (requiredDocument.Document_Type__c != ApplicationUtils.schoolSpecificDocType()) {
                        requiredDocument.Label_for_School_Specific_Document__c = null;
                    }
                    
                    if (requiredDocument.Id == null) {
                        requiredDocument.School__c = school.Id;    // master field. non changeable
                        insert requiredDocument;
                    } else {
                        update requiredDocument;
                    }
                    
                    //NAIS-2345
                    if( (requiredDocument.Document_Type__c==schoolSpecificDocumentType  || requiredDocument.Document_Type__c=='Expense and Resources Statement')
                    && attachments.inputFileBody!=null && requiredDocument.Id!=null)
                    {
                        attachments.saveAttachmentRecord(requiredDocument.Id, 'Required Document');
                    }
                } catch(Exception e) {
                    ErrorMessage(e.getMessage());
                    Database.rollback(sp);
                    return null;
                } 
                
                // reload
                requiredDocument = new Required_Document__c();
                loadRequiredDocument();
                newRendered = true;
                editRendered = false;
                return this.reloadPage();
            }
            else{
                return this.reloadPage();
            }
        }
        return this.reloadPage();
    }

    public void cancelDocument() 
    {
        loadRequiredDocument();
        requiredDocument = new Required_Document__c();
        errorMessageStr = null;
        newRendered = true;
        editRendered = false;
    }
    
    public PageReference deleteDocument()
    {
        errorMessageStr = null;
        // wire frame note 5.
        // If a required document is deleted, we will set a delete flag on it, and remove all
        // School Document Assigments related to it
            
        String rdId = ApexPages.currentPage().getParameters().get('requiredDocumentId');
        if (rdId != null && id_requiredDocument_map.containsKey(rdId)) {
            Savepoint sp = Database.setSavepoint();
            try {
                Required_Document__c rd = id_requiredDocument_map.get(rdId);
                
                // delete school document assignments
                // deleting through inner class that bypass record level sharing
                // School_Document_Assignment__c is not in master detail relationship with required document
                RequiredDocumentWOS rdwos = new RequiredDocumentWOS();
                String errMsg = rdwos.deleteSchoolDocumentAssignment(rd.Id);
                if (errMsg != null) {
                    ErrorMessage(errMsg);
                    return this.reloadPage();
                }
                
                //NAIS-2345
               attachments.deleteAttachment(rdId, false);
               
                // we are not deleting required document
                rd.Deleted__c = true;
                update rd;
            } catch(Exception e) {
                ErrorMessage(e.getMessage());
                Database.rollback(sp);
                return this.reloadPage();
            }
        }

        // reload
        loadRequiredDocument();
        requiredDocument = new Required_Document__c();   
        newRendered = true;
        editRendered = false;
        return this.reloadPage();
    }

    public PageReference editDocument() 
    {
        errorMessageStr = null;
        
        DocumentRow tmpDocumentRow;
        requiredDocument = new Required_Document__c();
        String rdId = ApexPages.currentPage().getParameters().get('requiredDocumentId');
        if (rdId != null && documentRowsById.containsKey(rdId)) {
            tmpDocumentRow = (documentRowsById.get(rdId));
            requiredDocument = tmpDocumentRow.document;
        }
        
        if(tmpDocumentRow.attachmentLink!=''){
            attachments.setSelectedAttachmentVariables(rdId, tmpDocumentRow.attachmentLink);
        }
        newRendered = false;
        editRendered = true;

        return null;
    }
    
    /*End Action Methods*/
    
    /*Helper Methods*/
                
    private void loadAcademicYearOptions() {
        // init
        id_academicYear_map = new Map <String, Academic_Year__c>();
        
        List <Academic_Year__c> ayList = [select Id, Name from Academic_Year__c order by Name];
        for (Academic_Year__c ay : ayList) {
            id_academicYear_map.put(ay.Id, ay);
        }
        
        //[CH] NAIS-1359 Added default to current year is parameter is not provided
        String academicYearId = ApexPages.currentPage().getParameters().get('academicYearId');
        if(academicYearId == null){
            academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        }
        
        if (academicYearId != null && id_academicYear_map.containsKey(academicYearId)) {
            academicYear = id_academicYear_map.get(academicYearId);
            selectedAcademicYear = academicYear.Id;
        } else {
            List <Annual_Setting__c> asList = GlobalVariables.getCurrentAnnualSettings(true, datetime.now().date());
            if (asList.size() > 0) {
                 Annual_Setting__c annualSetting = asList[0]; 
                 if (annualSetting.Academic_Year__c != null && id_academicYear_map.containsKey(annualSetting.Academic_Year__c)) { 
                     academicYear = id_academicYear_map.get(annualSetting.Academic_Year__c);
                     selectedAcademicYear = annualSetting.Academic_Year__c; 
                 }
            }
        }
    }
        
    private void loadSchool() {
    
        if (schoolId != null && schoolId != '') {
            List <Account> schoolList = [select Id, Name
            from Account where Id = :schoolId];
        
            if (schoolList.size() > 0) {
                school = schoolList[0];
            }
        }
    }
    
    private void loadRequiredDocument() {
        attachments.clearMaps();
        docRowList = new List<DocumentRow>{};
        // init
        id_requiredDocument_map = new Map <String, Required_Document__c>();
        documentRowsById = new map<String, documentRow>();
        documentRow tmpDocumentRow;
        
        //[CH] NAIS-1609 Putting into a sortable list to sort by name
        for(Required_Document__c reqDoc : [select Id, Name, Academic_Year__c, Deleted__c, Document_Type__c,
                                            Document_Year__c, Label_for_School_Specific_Document__c, School__c
                                            from Required_Document__c 
                                            where School__c = :school.Id 
                                            and Academic_Year__c = :selectedAcademicYear 
                                            and Deleted__c != true])
        {
            if(reqDoc.Document_Type__c=='School/Organization Other Document' 
            || reqDoc.Document_Type__c=='Expense and Resources Statement'){
                //NAIS-2345, SFP-472
                attachments.mapAttachmentRecordByParentId.put(reqDoc.Id, null);
            }
            tmpDocumentRow = new DocumentRow(reqDoc);
            docRowList.add(tmpDocumentRow);
            documentRowsById.put(reqDoc.Id, tmpDocumentRow);
        }
        docRowList.sort();
        
        //Attachment__c
        for(Attachment__c attachmentCustom:[Select Id, Attachment__c, Parent_ID__c 
                                            from Attachment__c 
                                            where Parent_ID__c IN: attachments.mapAttachmentRecordByParentId.keyset()
                                            and Parent_Type__c='Required Document' 
                                            and Deleted__c=false]){//NAIS-2345
            attachments.mapParentIdByAttachmentId.put(attachmentCustom.Id,attachmentCustom.Parent_ID__c);
        }
        
        
        //Attachments
        for(Attachment attachmentSample:[Select Id, parentId, Body, Name from Attachment 
                                        where parentId IN: attachments.mapParentIdByAttachmentId.keyset()]){//NAIS-2345
            attachments.mapAttachmentRecordByParentId.put(
                        attachments.mapParentIdByAttachmentId.get(attachmentSample.parentId), 
                        attachmentSample);
        }
        
        // Read the sorted documents into the list Required_Document__c records 
        requiredDocumentList = new List<Required_Document__c>{};
        for(DocumentRow docRow : docRowList){
            requiredDocumentList.add(docRow.document);
            if(docRow.document.Document_Type__c=='School/Organization Other Document'
            || docRow.document.Document_Type__c=='Expense and Resources Statement'){//NAIS-2345
                docRow.attachmentLink = attachments.getAttachmentLinkByParentId(docRow.document.Id, 'View Sample');
            }
        }
        
        for (Required_Document__c rd : requiredDocumentList) {
            id_requiredDocument_map.put(rd.Id, rd);
        }
    }
    
    //SFP-59, [G.S]
    private void loadDocumentYears(){
        documentYearOptions = new List<SelectOption>();
        //Academic_Year__c currAcadYear = academicYear;
        Integer currentYear = integer.valueof(academicYear.name.substring(0,4));
        documentYearOptions.add(new SelectOption(string.valueof(currentYear-1),string.valueof(currentYear-1)));
        documentYearOptions.add(new SelectOption(string.valueof(currentYear-2),string.valueof(currentYear-2)));
        if ( premiumSubscription ) {
            documentYearOptions.add(new SelectOption(string.valueof(currentYear-3),string.valueof(currentYear-3)));
        }
    }
    
    public void ErrorMessage(String msgStr) {
        this.errorMessageStr = msgStr;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msgStr));
    }      
    private void writeErrorMessages(PageReference pageRef)
    {//Use cookies to save error messages because the form multipart/form-data do not allow rerender and we must reload the page.
        for(ApexPages.Message m:ApexPages.getMessages())
            this.errorMessageStr = m.getSummary();
        pageRef.setCookies(new Cookie[]{new Cookie('errorMessages',this.errorMessageStr,null,-1,false)});
    }//End:writeErrorMessages
    
    private void readErrorMessages()
    {
        try{
            this.errorMessageStr = String.ValueOf(ApexPages.currentPage().getCookies().get('errorMessages').getValue());
            ApexPages.currentPage().setCookies(new Cookie[]{new Cookie('errorMessages','',null,0,false)});
        }catch(Exception e){
            this.errorMessageStr='';
        }
    }//End:readErrorMessages
    
    private PageReference reloadPage()
    {
        PageReference pageRef = Page.SchoolRequiredDocument;
        pageRef.setRedirect(true);
        pageRef.getParameters().put('academicYearId', newAcademicYear);
        this.writeErrorMessages(pageRef);
        pageRef = this.addParametersToPage(pageRef, new String[]{'schoolId','academicyearid','isKSSchoolUser'});
        return pageRef;
    }
    
    private PageReference addParametersToPage(PageReference pageRef,String[] paramNames)
    {
        for(String param:paramNames)
        {
            if(ApexPages.CurrentPage().getParameters().get(param)!=null)
                pageRef.getParameters().put(param, ApexPages.CurrentPage().getParameters().get(param));
        }
        return pageRef;
    }//End:addParametersToPage
    /*End Helper Methods*/

    /*SchoolAcademicYearSelectorInterface Implementation*/
    public String newAcademicYear {get; set;}
    
    public String getAcademicYearId() {
        
        //return newAcademicYear ;
        return selectedAcademicYear ;
    }
    
    public void setAcademicYearId(String academicYearId) {
        
        newAcademicYear = academicYearId;
    }
    
    public PageReference SchoolAcademicYearSelector_OnChange(Boolean saveRecord) {
        
        if(saveRecord){
            saveDocument();
        }
        
        PageReference pageRef = Page.SchoolRequiredDocument;
        if (newAcademicYear != null && newAcademicYear != '') {
            pageRef.getParameters().put('academicYearId', newAcademicYear);
        }
        pageRef.setRedirect(true);
        
        return pageRef;
    }
    
    /*End SchoolAcademicYearSelectorInterface Implementation*/
    
    // inner class to delete School_Document_Assignment__c. It is not in master detail with required document
    // portal user cannot delete if they are not owner of account. This class bypass record sharing
    private without sharing class RequiredDocumentWOS {
    
        private String deleteSchoolDocumentAssignment(String rdId) {
            String errMsg;
            try {
                // delete school document assignments
                if (rdId != null) {
                    List <School_Document_Assignment__c> sdaList = [select Id, Name
                        from School_Document_Assignment__c
                        where Required_Document__c = :rdId];
                    if (sdaList.size() > 0) {
                        delete sdaList;
                    }
                }            
            } catch(Exception e) {
                errMsg = e.getMessage();
            }
            return errMsg;
        } 
    }
    
    /* Inner Classes */
    
    public class DocumentRow implements Comparable{
        public Required_Document__c document{get;set;}
        public String attachmentLink{get;set;}
        
        public DocumentRow(Required_Document__c doc){
            document = doc;
            attachmentLink = '';
        }
        
        public Integer compareTo(Object compareTo){
            // Cast argument to DocumentSort
            DocumentRow compareToDoc = (DocumentRow)compareTo;
            
            String thisDocName;
            String otherDocName;
            
            // Figure out what value to use for sorting the current doc
            if(document.Document_Type__c == ApplicationUtils.schoolSpecificDocType()){
                thisDocName = document.Label_for_School_Specific_Document__c;
            }
            else{
                thisDocName = document.Document_Type__c;
            }
            
            // Figure out what value to use for the other document
            if(compareToDoc.document.Document_Type__c == ApplicationUtils.schoolSpecificDocType()){
                otherDocName = compareToDoc.document.Label_for_School_Specific_Document__c;
            }
            else{
                otherDocName = compareToDoc.document.Document_Type__c;
            }
            
            if(thisDocName > otherDocName){
                return 1;
            }
            else if(thisDocName < otherDocName){
                return -1;
            }
            else{
                return 0;
            }
        }
        
    }
    
    /* End Inner Classes */
}