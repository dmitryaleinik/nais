// Spec-139 School Portal - Fee Waivers Rollups
// BR, Exponent Partners, 2013

@isTest
private class AFWAfterTest {

    @isTest
    private static void waiverBalanceCreditUpdateWhenAFWAssigned()
    {
        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        Account school = TestUtils.createAccount('School A', RecordTypes.schoolAccountTypeId, null, false);
        insert school;

        Contact contact = TestUtils.createContact('Contact A', school.Id, RecordTypes.schoolStaffContactTypeId, false);
        insert contact;
        Contact contactB = TestUtils.createContact('Contact B', school.Id, RecordTypes.schoolStaffContactTypeId, false);
        insert contactB;
        Contact contactC = TestUtils.createContact('Contact C', school.Id, RecordTypes.schoolStaffContactTypeId, false);
        insert contactC;

        Annual_Setting__c an = new Annual_Setting__c();
        an.Academic_Year__c = academicYearId;
        an.School__c = school.Id;
        an.Waivers_Assigned__c = 0;
        insert an;

        Application_Fee_Waiver__c afw = new Application_Fee_Waiver__c();
        afw.Academic_Year__c = academicYearId;
        afw.Account__c = school.Id;
        afw.Contact__c = contact.Id;
        afw.Status__c = 'Pending Approval';
        insert afw;
        Application_Fee_Waiver__c afw2 = new Application_Fee_Waiver__c();
        afw2.Academic_Year__c = academicYearId;
        afw2.Account__c = school.Id;
        afw2.Contact__c = contactB.Id;
        afw2.Status__c = 'Assigned';

        // SFP-190 add 'pending qualification' deducts from count
        Application_Fee_Waiver__c afw3 = new Application_Fee_Waiver__c();
        afw3.Academic_Year__c = academicYearId;
        afw3.Account__c = school.Id;
        afw3.Contact__c = contactC.Id;
        afw3.Status__c = 'Pending Qualification';

        Test.startTest();
        // Update AFW.
        afw.Status__c = 'Assigned';
        update afw;

        Annual_Setting__c asAfterUpdate = [select Waiver_Credit_Balance_Formula__c, Waivers_Assigned__c from Annual_Setting__c where Id = :an.Id];
        System.assertEquals(-1, asAfterUpdate.Waiver_Credit_Balance_Formula__c);
        System.assertEquals(1, asAfterUpdate.Waivers_Assigned__c);

        // The School update comes from the Annual Settings update so if it's correct, we should be ok.
        Account schoolAfterUpdate = [select Current_Year_Waiver_Balance__c, Current_Year_Waivers_Assigned__c from Account where Id = :school.Id];
        System.assertEquals(-1, schoolAfterUpdate.Current_Year_Waiver_Balance__c);
        System.assertEquals(1, schoolAfterUpdate.Current_Year_Waivers_Assigned__c);

        an.Total_Waivers_Override_Default__c = 30;
        update an;

        // Insert AFW;
        insert afw2;

        asAfterUpdate = [select Waiver_Credit_Balance_Formula__c, Waivers_Assigned__c from Annual_Setting__c where Id = :an.Id];
        System.assertEquals(28, asAfterUpdate.Waiver_Credit_Balance_Formula__c);
        System.assertEquals(2, asAfterUpdate.Waivers_Assigned__c);

        // The School update comes from the Annual Settings update so if it's correct, we should be ok.
        schoolAfterUpdate = [select Current_Year_Waiver_Balance__c, Current_Year_Waivers_Assigned__c from Account where Id = :school.Id];
        System.assertEquals(28, schoolAfterUpdate.Current_Year_Waiver_Balance__c);
        System.assertEquals(2, schoolAfterUpdate.Current_Year_Waivers_Assigned__c);

        // SFP-190
        insert afw3;

        asAfterUpdate = [select Waiver_Credit_Balance_Formula__c, Waivers_Assigned__c from Annual_Setting__c where Id = :an.Id];
        System.assertEquals(27, asAfterUpdate.Waiver_Credit_Balance_Formula__c);
        System.assertEquals(3, asAfterUpdate.Waivers_Assigned__c);

        // The School update comes from the Annual Settings update so if it's correct, we should be ok.
        schoolAfterUpdate = [select Current_Year_Waiver_Balance__c, Current_Year_Waivers_Assigned__c from Account where Id = :school.Id];
        System.assertEquals(27, schoolAfterUpdate.Current_Year_Waiver_Balance__c);
        System.assertEquals(3, schoolAfterUpdate.Current_Year_Waivers_Assigned__c);

        Test.stopTest();
    }
}