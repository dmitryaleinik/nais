@isTest
private class OrgWideEmailsTest {
    
    @isTest
    private static void setDoNotReplyAddress_expectNotNull() {
        
        System.assertEquals(true, OrgWideEmails.Instance.Do_Not_Reply_Address != null,
            'We expect a default value for Do_Not_Reply_Address__c.');
            
        String Do_Not_Reply_Address = 'test_1@test.com';
        OrgWideEmails.Instance.Do_Not_Reply_Address = Do_Not_Reply_Address;
        
        System.assertEquals(Do_Not_Reply_Address, OrgWideEmails.Instance.Do_Not_Reply_Address,
            'We expect that Do_Not_Reply_Address__c is set to "' + Do_Not_Reply_Address + '".');
    }
    
    @isTest
    private static void setSSSMainAddress_expectNotNull() {
        
        System.assertEquals(true, OrgWideEmails.Instance.SSS_Main_Address != null,
            'We expect a default value for SSS_Main_Address__c.');
            
        String SSS_Main_Address = 'test_1@test.com';
        OrgWideEmails.Instance.SSS_Main_Address = SSS_Main_Address;
        
        System.assertEquals(SSS_Main_Address, OrgWideEmails.Instance.SSS_Main_Address,
            'We expect that SSS_Main_Address__c is set to "' + SSS_Main_Address + '".');
    }
}