/**
* @ AccessOrgFeeWaiversExtension.cls
* @ Controller extension for AccessOrgFeeWaivers page, for inserting complimentary Fee Waiver records.
*/
public with sharing class CourtesyFeeWaiversExtension
{

    /*Initialization*/
    public CourtesyFeeWaiversExtension(ApexPages.StandardController stdController)
    {
        if(!test.isRunningTest()) // Need to put this in to prevent an error with addFields when running tests
        stdController.addFields(new List<String>{'Name'});
        accountRecord = (Account)stdController.getRecord();

        // Check to see if the current user has access to create courtesy waivers
        User currentUser = GlobalVariables.getCurrentUser();
        displayInterface = currentUser.Can_Apply_Courtesy_Waivers__c;

        Academic_Year__c currentAcademicYear = GlobalVariables.getCurrentAcademicYear();
        Academic_Year__c priorAcademicYear = GlobalVariables.getPreviousAcademicYear();

        academicYearsMap = new Map<String, Academic_Year__c>();
        academicYearsMap.put(currentAcademicYear.Name, currentAcademicYear);
        academicYearsMap.put(priorAcademicYear.Name, priorAcademicYear);
    }
    /*End Initialization*/

    /*Properties*/

    private Account accountRecord;

    public Map <String, Academic_Year__c> academicYearsMap;
    public List<SelectOption> getAcademicYears()
    {
        List<SelectOption> academicYearOptions = new List<SelectOption>();
        for (String  year : academicYearsMap.keySet())
        {
            academicYearOptions.add(new SelectOption(year, year));
        }

        return academicYearOptions;
    }
    public String selectedAcademicYearName
    {
        get
        {
            if (selectedAcademicYearName == null)
            {
                selectedAcademicYearName = GlobalVariables.getCurrentAcademicYear().Name;
            }

            return selectedAcademicYearName;
        }
        set;
    }

    public Boolean displayInterface {get; set;}
    public String selectedQuantity {get; set;}


    /*End Properties*/


    /*Action Methods*/

    public PageReference save()
    {
        if (accountRecord.SSS_Subscriber_Status__c != 'Current')
        {
            return null;
        }

        Academic_Year__c selectedAcademicYear = academicYearsMap.get(selectedAcademicYearName);
        // Insert an Opportunity record
        Opportunity newOpp = new Opportunity();
        newOpp.Academic_Year_Picklist__c = selectedAcademicYear.Name;
        newOpp.AccountId = accountRecord.Id;
        newOpp.RecordTypeId = RecordTypes.opportunityFeeWaiverPurchaseTypeId;
        newOpp.Name = accountRecord.Name + ' - Fee Waivers';
        newOpp.StageName = 'Closed';
        newOpp.CloseDate = Date.today();
        Database.insert(newOpp);


        if (!isExistsAnnualSetting(accountRecord.Id, selectedAcademicYear.Name)) {
            insert new Annual_Setting__c(
                School__c = accountRecord.Id,
                Academic_Year__c = selectedAcademicYear.Id
            );
        }

        // Insert the Sale Transaction Line Item
        List<Transaction_Line_Item__c> newLineItems = PaymentUtils.insertTransactionLineItemsFromOpportunities(new List<Opportunity> {newOpp}, true, Integer.valueOf(selectedQuantity));

        // Insert adjustment Transation Line Item
        PaymentUtils.createCreditAdjustmentLineItems(newLineItems);

        // Need to do this after TLI are added to trigger updating Fee Waiver numbers
        newOpp.StageName = 'Paid in Full';
        update newOpp;

        return new PageReference('/' + accountRecord.Id);
    }

    /*End Action Methods*/

    /*Helper Methods*/

    /**
     * @description Returns boolean value if Annual Setting is exists for the School and Academic Year object.
     * @param schoolId School Id
     * @param academicYearName Academic Year Name
     * @return Annual Setting record.
     */
    private Boolean isExistsAnnualSetting(Id schoolId, String academicYearName) {
        List<Annual_Setting__c> annualSettings = AnnualSettingsSelector.newInstance().selectBySchoolAndAcademicYear(
                new Set<Id> { schoolId },
                new Set<String> { academicYearName }
        );

        return !annualSettings.isEmpty();
    }
    /*End Helper Methods*/
}