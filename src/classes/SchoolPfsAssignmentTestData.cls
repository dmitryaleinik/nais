/**
 * @description This class is used to create Academic Year records for unit tests.
 */
@isTest
public class SchoolPfsAssignmentTestData extends SObjectTestData {

    private static final String STRING_NO = 'No';
    @testVisible private static final String NAME = 'SchoolPfsAssignment Name';

    private Integer counter = 0;

    /**
     * @description Get the default values for the School_PFS_Assignment__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                School_PFS_Assignment__c.Name => NAME,
                School_PFS_Assignment__c.Student_Folder__c => StudentFolderTestData.Instance.DefaultStudentFolder.Id,
                School_PFS_Assignment__c.Applicant__c => ApplicantTestData.Instance.DefaultApplicant.Id,
                School_PFS_Assignment__c.School__c => AccountTestData.Instance.DefaultAccount.Id,
                School_PFS_Assignment__c.Withdrawn__c => STRING_NO
        };
    }

    /**
     * @description Set the Orig_Medicare_Tax_Allowance_Calculated__c field to the
     *              given decimal value.
     * @param medicareAllowance The medicare value to set.
     * @return The current working instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData withOrigMedicareAllowance(Decimal medicareAllowance) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Orig_Medicare_Tax_Allowance_Calculated__c,
                medicareAllowance);
    }

    /**
     * @description Set the Orig_Social_Security_Tax_Allow_Calc__c field to the
     *              given decimal value.
     * @param socialSecurityAllowance The Social Security value to set.
     * @return The current working instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData withOrigSocialSecurityAllowance(Decimal socialSecurityAllowance) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Orig_Social_Security_Tax_Allow_Calc__c,
                socialSecurityAllowance);
    }

    /**
     * @description Set the Medicare_Tax_Allowance_Calc_Revision__c field to the
     *              given decimal value.
     * @param medicareAllowance The medicare value to set.
     * @return The current working instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData withRevisedMedicareAllowance(Decimal medicareAllowance) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Medicare_Tax_Allowance_Calc_Revision__c,
                medicareAllowance);
    }

    /**
     * @description Set the Medicare_Tax_Allowance_Calc__c field to the given decimal value.
     * @param medicareAllowance The medicare value to set.
     * @return The current working instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData withMedicareAllowance(Decimal medicareAllowance)
    {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Medicare_Tax_Allowance_Calc__c, medicareAllowance);
    }

    /**
     * @description Set the Social_Security_Tax_Allowance_Calc_Rev__c field to the
     *              given decimal value.
     * @param socialSecurityAllowance The Social Security value to set.
     * @return The current working instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData withRevisedSocialSecurityAllowance(Decimal socialSecurityAllowance) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Social_Security_Tax_Allowance_Calc_Rev__c,
                socialSecurityAllowance);
    }

    /**
     * @description Set the Social_Security_Tax_Allowance_Calc__c field to the given decimal value.
     * @param socialSecurityAllowance The Social Security value to set.
     * @return The current working instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData withSocialSecurityAllowance(Decimal socialSecurityAllowance)
    {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Social_Security_Tax_Allowance_Calc__c, socialSecurityAllowance);
    }

    /**
     * @description Set the Applicant__c field on the current School Pfs Assignment record.
     * @param applicantId The Applicant Id to set on the School Pfs Assignment.
     * @return The current working instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData forApplicantId(Id applicantId) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Applicant__c, applicantId);
    }

    /**
     * @description Set the Student_Folder__c field on the current School Pfs Assignment record.
     * @param studentFolderId The Student Folder Id to set on the School Pfs Assignment.
     * @return The current working instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData forStudentFolderId(Id studentFolderId) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Student_Folder__c, studentFolderId);
    }

    /**
     * @description Set the School__c field on the current School Pfs Assignment record.
     * @param accountId The School Id to set on the School Pfs Assignment.
     * @return The current working instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData forSchoolId(Id accountId) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.School__c, accountId);
    }

    /**
     * @description Set the Academic_Year_Picklist__c field on the current School Pfs Assignment record.
     * @param acYearPicklist The Academic Year Picklist to set on the School Pfs Assignment.
     * @return The current working instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData forAcademicYearPicklist(String acYearPicklist) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Academic_Year_Picklist__c, acYearPicklist);
    }

    /**
     * @description Set the PFS_Status__c field on the current School Pfs Assignment record.
     * @param pfsStatus The Pfs Status to set PFS_Status__c on the School Pfs Assignment.
     * @return The current working instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData forPfsStatus(String pfsStatus) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.PFS_Status__c, pfsStatus);
    }

    /**
     * @description Set the Revision_EFC_Calc_Status__c field on the current School Pfs Assignment record.
     * @param revisionEfcCalcStatus The revision efc calc Status to set Revision_EFC_Calc_Status__c on the School Pfs Assignment.
     * @return The current working instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData forRevisionEfcCalcStatus(String revisionEfcCalcStatus) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Revision_EFC_Calc_Status__c, revisionEfcCalcStatus);
    }

    /**
     * @description Set the Grade_Applying__c field on the current School Pfs Assignment record.
     * @param gradeApplying The Grade to set Grade_Applying__c on the School Pfs Assignment.
     * @return The current working instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData forGradeApplying(String gradeApplying) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Grade_Applying__c, gradeApplying);
    }

    /**
     * @description Set the Name field on the current School Pfs Assignment record.
     * @param spfsaName The Name to set Name on the School Pfs Assignment.
     * @return The current working instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData forName(String spfsaName) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Name, spfsaName);
    }

    /**
     * @description Set the Salary_Wages_Parent_A__c field on the current School Pfs Assignment record.
     * @param salaryWagesParentA The amount to set Salary Wages Parent A on the School Pfs Assignment.
     * @return The current working instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData forSalaryWagesParentA(Decimal salaryWagesParentA) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Salary_Wages_Parent_A__c, salaryWagesParentA);
    }

    /**
     * @description Set the Salary_Wages_Parent_B__c field on the current School Pfs Assignment record.
     * @param salaryWagesParentB The amount to set Salary Wages Parent B on the School Pfs Assignment.
     * @return The current working instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData forSalaryWagesParentB(Decimal salaryWagesParentB) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Salary_Wages_Parent_B__c, salaryWagesParentB);
    }

    /**
     * @description Set the Filling Status on the current School Pfs Assignment record.
     * @param fillingStatus The Filling Status to set the Filing_Status__c field to on the current School Pfs Assignment record.
     * @return The current instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData forFillingStatus(String fillingStatus) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Filing_Status__c, fillingStatus);
    }

    /**
     * @description Set the Number of Children in Tuition Schools on the current School Pfs Assignment record.
     * @param numChildreInTuitionSchools The Number of Children in Tuition Schools 
     *        to set the Num_Children_in_Tuition_Schools__c field to on the current School Pfs Assignment record.
     * @return The current instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData forNumChildreInTuitionSchools(Decimal numChildreInTuitionSchools) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Num_Children_in_Tuition_Schools__c, numChildreInTuitionSchools);
    }

    /**
     * @description Set the family status on the current School Pfs Assignment record.
     * @param origFamilyStatus The family status to set the Orig_Family_Status__c field to on the current School Pfs Assignment record.
     * @return The current instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData forOrigFamilyStatus(String origFamilyStatus) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Orig_Family_Status__c, origFamilyStatus);
    }

    public SchoolPfsAssignmentTestData forParentAEmail(String parentEmail) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Parent_A_Email__c, parentEmail);
    }

    /**
     * @description Set the family size on the current School Pfs Assignment record.
     * @param familySize The Number of Children in Tuition Schools 
     *        to set the Orig_Family_Size__c field to on the current School Pfs Assignment record.
     * @return The current instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData forFamilySize(Integer familySize) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Orig_Family_Size__c, familySize);
    }

    /**
  * @description Set the Day_Boarding__c picklist field on the current School Pfs Assignment record.
  * @param dayOrBoarding the value of the Day_Boarding__c picklist to set on the Day_Boarding__c field
  *        on the current School Pfs Assignment record.
  * @return The current instance of SchoolPfsAssignmentTestData.
  */
    public SchoolPfsAssignmentTestData forDayOrBoarding(String dayOrBoarding) {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Day_Boarding__c, dayOrBoarding);
    }

    /**
     * @description Set the Withdrawn__c field on the current School Pfs Assignment record with 'Yes' value.
     * @return The current working instance of SchoolPfsAssignmentTestData.
     */
    public SchoolPfsAssignmentTestData asWithdrawn() {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Withdrawn__c, 'Yes');
    }

    /**
    * @description Set the Sharing_Processed__c field on the current School Pfs Assignment record.
    * @param sharingProcessed the value of the Sharing_Processed__c to set on the current School Pfs Assignment record.
    * @return The current instance of SchoolPfsAssignmentTestData.
    */
    public SchoolPfsAssignmentTestData forSharingProcessed(Boolean sharingProcessed)
    {
        return (SchoolPfsAssignmentTestData) with(School_PFS_Assignment__c.Sharing_Processed__c, sharingProcessed);
    }

    /**
     * @description Ensures the names of records are unique by appending a counter var to them and incrementing it for
     *              the next record.
     * @param sObj The record to update.
     */
    protected override void beforeInsert(SObject sObj) {
        School_PFS_Assignment__c record = (School_PFS_Assignment__c)sObj;
        record.Name = record.Name + counter;
        counter++;
    }

    /**
     * @description Insert the current working School_PFS_Assignment__c record.
     * @return The currently operated upon School_PFS_Assignment__c record.
     */
    public School_PFS_Assignment__c insertSchoolPfsAssignment() {
        return (School_PFS_Assignment__c)insertRecord();
    }

    /**
     * @description Create the current working School Pfs Assignment record without resetting
     *          the stored values in this instance of SchoolPfsAssignmentTestData.
     * @return A non-inserted School_PFS_Assignment__c record using the currently stored field
     *          values.
     */
    public School_PFS_Assignment__c createSchoolPfsAssignmentWithoutReset() {
        return (School_PFS_Assignment__c)buildWithoutReset();
    }

    /**
     * @description Create the current working School_PFS_Assignment__c record.
     * @return The currently operated upon School_PFS_Assignment__c record.
     */
    public School_PFS_Assignment__c create() {
        return (School_PFS_Assignment__c)super.buildWithReset();
    }

    /**
     * @description The default School_PFS_Assignment__c record.
     */
    public School_PFS_Assignment__c DefaultSchoolPfsAssignment {
        get {
            if (DefaultSchoolPfsAssignment == null) {
                DefaultSchoolPfsAssignment = insertSchoolPfsAssignment();
            }
            return DefaultSchoolPfsAssignment;
        }
        private set;
    }

    /**
     * @description Get the School_PFS_Assignment__c SObjectType.
     * @return The School_PFS_Assignment__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return School_PFS_Assignment__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static SchoolPfsAssignmentTestData Instance {
        get {
            if (Instance == null) {
                Instance = new SchoolPfsAssignmentTestData();
            }
            return Instance;
        }
        private set;
    }

    private SchoolPfsAssignmentTestData() { }
}