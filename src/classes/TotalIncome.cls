/* 3. Calculate Total Income */
public class TotalIncome implements EfcCalculationSteps.Step {

    public void doCalculation(List<EfcWorksheetData> efcDataList) {
        for (EfcWorksheetData efcData : efcDataList) {
            doCalculation(efcData);
        }
    }

    public void doCalculation(EfcWorksheetData efcData) {
        // initialize inputs
        Decimal totalTaxableIncome = EfcUtil.nullToZero(efcData.totalTaxableIncome);
        Decimal totalNonTaxableIncome = EfcUtil.nullToZero(efcData.totalNonTaxableIncome);

        efcData.totalIncome = EfcUtil.negativeToZero(totalTaxableIncome + totalNonTaxableIncome);

    }
}