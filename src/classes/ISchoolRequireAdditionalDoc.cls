public interface ISchoolRequireAdditionalDoc {
    School_Document_Assignment__c getAdditionalSchoolDocAssign();
    
    List<SelectOption> getNonSchoolSpecificDocumentTypes();
    
    List<SelectOption> getValidDocumentYears();
    
    Boolean getIsTaxDocument();
    
    PageReference saveNewDocument();
    
    PageReference saveAndNewAdditionalDocument();
    
    PageReference saveAndUploadRequiredDocuments();
    
    PageReference cancelAdditionalDocument();
    
    PageReference loadDocumentYears();
    
    String getSubtitle();
}