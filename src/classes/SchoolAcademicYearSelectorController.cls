/* 
 * Spec-140, NAIS-333, NAIS-382 School Portal - Header Bar
 * CH, Exponent Partners, 2013
 *
 * 
 */

public with sharing class SchoolAcademicYearSelectorController {
    /*Initialization*/
    
    public SchoolAcademicYearSelectorController() {
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=edge');
    }
   
    /*End Initialization*/
   
    /*Properties*/
    
    public String saveRecord { get; set; }
    
    public Boolean showYears {
        get{
            if(showYears == null){
                return true;
            }
            else{
                return showYears;
            }
        }
         set;}
    
    public String errorString { get; set; }
        
    public SchoolAcademicYearSelectorInterface Consumer { get; set; }
    
    public String MergeField {get; 
        set{
            MergeField = value;
            NewAcademicYearId = value;
        }
    }
    
    public Boolean InOverlapPeriodOrMultipleYears {
        get {
            return (this.getAcademicYears().size() > 1);
        }
    }
    
    public String NewAcademicYearId { get; set; }
    
    public Academic_Year__c CurrentAcademicYear {
        get {
            Academic_Year__c year = null;
            if(this.Consumer != null) {
                if(this.Consumer.getAcademicYearId() == null || this.Consumer.getAcademicYearId() == '') {
                    this.Consumer.setAcademicYearId(GlobalVariables.getCurrentAcademicYear().Id);
                }
                
                return this.getAcademicYearMap().get(this.Consumer.getAcademicYearId());
            }
            
            return null;
        }
        
        set;
    }
    
    // making this list public so we can optionally pass in a list of academic years
    public List<Academic_Year__c> academicYears_x { get; set; }
    public List<Academic_Year__c> getAcademicYears() {
        
        if(this.academicYears_x == null) {
            this.academicYears_x = new List<Academic_Year__c>{};
            this.academicYears_x.add(GlobalVariables.getCurrentAcademicYear());
            
            // [CH] NAIS-2105 Refactoring to use Date-based methods correctly
            if(GlobalVariables.isInOverlapPeriod()){
                Academic_Year__c previousYear = GlobalVariables.getPreviousAcademicYear();
                // [DP] NAIS-2037 adding line prevent adding previous year if this is a basic school user
                if(previousYear != null && !GlobalVariables.isLimitedSchoolView()) {
                    this.academicYears_x.add(previousYear);
                }
            }        
        }
        
        return this.academicYears_x;
    }
    
    private Map<Id, Academic_Year__c> academicYearMap_x { get; set; }
    public Map<Id, Academic_Year__c> getAcademicYearMap() {
        if(this.academicYearMap_x == null) {    
            this.academicYearMap_x = new Map<Id, Academic_Year__c>(this.getAcademicYears());
        }
        
        return this.academicYearMap_x;
    }
    
    private List<SelectOption> academicYearOptions_x { get; set; }
    public List<SelectOption> getAcademicYearOptions() {
        if(this.academicYearOptions_x == null) {
            this.academicYearOptions_x = new List<SelectOption>();
            
            for(Academic_Year__c year : this.getAcademicYears()) {
                this.academicYearOptions_x.add(new SelectOption(year.Id, year.Name));
            }
        }
        
        return this.academicYearOptions_x;
    }
   
    /*End Properties*/
       
    /*Action Methods*/
    
    public PageReference SchoolAcademicYearSelector_OnChange() {   
        
        PageReference page = null;
        
        if(this.Consumer != null) {
            this.Consumer.setAcademicYearId(this.NewAcademicYearId);
            page = this.Consumer.SchoolAcademicYearSelector_OnChange(Boolean.valueOf(saveRecord));
            
            if(page != null) page.getParameters().remove('AJAXREQUEST');
        }
        return page;
    }
    
    /*End Action Methods*/
   
    /*Helper Methods*/
    /*End Helper Methods*/
}