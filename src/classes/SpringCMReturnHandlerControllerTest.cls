@isTest
private class SpringCMReturnHandlerControllerTest
{

    @isTest
    private static void testSuccess() {
        // create test family document
        Family_Document__c familyDocument = new Family_Document__c();
        insert familyDocument;
        
        // set the current page and params
        Test.setCurrentPage(Page.SpringCMReturnHandler);
        ApexPages.currentPage().getParameters().put('docid', familyDocument.Id);
        ApexPages.currentPage().getParameters().put('dest', 'http://some.other.page');
        ApexPages.currentPage().getParameters().put('success', '1');
        
        // instantiate the controller
        SpringCMReturnHandlerController controller = new SpringCMReturnHandlerController();
        PageReference pr = controller.pageLoadAction();
        
        // reload the family document, and verify the status is Upload Pending
        System.assertEquals('Upload Pending', [SELECT Document_Status__c FROM Family_Document__c WHERE Id=:familyDocument.Id].Document_Status__c);
        
        // verify redirect to dest url
        //System.assertEquals('http://some.other.page', pr.getUrl());
        System.assert(pr.getUrl().toLowerCase().contains('systemautoclose'));        
    }
    
    @isTest
    private static void testCancel()
    {
        // [CH] NAIS-2076
        Academic_Year__c currentYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        
        Account school1 = TestUtils.createAccount('Test School 1', RecordTypes.schoolAccountTypeId, 3, true);
        
        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        Database.insert(new List<Contact>{parentA, student1});
        
        PFS__c pfsA = TestUtils.createPFS('PFS A', currentYear.Id, parentA.Id, true);
        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, true);
        Student_Folder__c studentFolder11 = TestUtils.createStudentFolder('Student Folder 1.1', currentYear.Id, student1.Id, true);
        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(currentYear.Id, applicant1A.Id, school1.Id, studentFolder11.Id, true);
        
        // create test family document
        Family_Document__c familyDocument = new Family_Document__c();
        insert familyDocument;

        // SDAs need to have Document Year specified now.
        String documentYear = GlobalVariables.getDocYearStringFromAcadYearName(GlobalVariables.getCurrentYearString());
        School_Document_Assignment__c docAssign3 = new School_Document_Assignment__c(Document__c = familyDocument.Id, School_PFS_Assignment__c = spfsa1.Id);
        docAssign3.Document_Year__c = documentYear;
        insert docAssign3;
        
        // set the current page and params
        Test.setCurrentPage(Page.SpringCMReturnHandler);
        ApexPages.currentPage().getParameters().put('docid', familyDocument.Id);
        ApexPages.currentPage().getParameters().put('dest', 'http://some.other.page');
        ApexPages.currentPage().getParameters().put('success', '0');
        
        // instantiate the controller
        SpringCMReturnHandlerController controller = new SpringCMReturnHandlerController();
        PageReference pr = controller.pageLoadAction();
        
        // verify the family doc was deleted
        System.assertEquals(0, [SELECT count() FROM Family_Document__c WHERE Id=:familyDocument.Id]);
        
        // [CH] NAIS-2076 verify that that School Document Assignment was deleted
        System.assertEquals(0, [SELECT count() FROM School_Document_Assignment__c WHERE Id=:docAssign3.Id]);
        
        // verify redirect to dest url
        //System.assertEquals('http://some.other.page', pr.getUrl());
        System.assert(pr.getUrl().toLowerCase().contains('systemautoclose'));        
    }
}