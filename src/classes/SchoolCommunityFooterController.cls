/**
 * @description Controller for the SchoolCommunityFooter.component. Retrieves and renders the footer html.
 */
public with sharing class SchoolCommunityFooterController {
    private static final String FOOTER_NAME = 'School_Community_Footer';

    private String footerHtml;
    /**
     * @description Retrieves the footer html based on if the org is a sandbox or not.
     * @return Html of the footer.
     */
    public String getFooterHtml() {
        if(footerHtml != null) return footerHtml;

        String documentId = OrganizationWrapper.isSandbox() ? FOOTER_NAME + '_WalkMe_Test' : FOOTER_NAME;
        try {
            footerHtml = [Select Body From Document Where DeveloperName = :documentId].Body.toString();
        } catch(Exception e) {
            return '';
        }
        return footerHtml;
    }
}