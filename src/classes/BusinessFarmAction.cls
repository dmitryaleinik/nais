public without sharing class BusinessFarmAction
{

    // SFP-128 [DP] 11.02.2015 boolean to prevent changes to the SBFA from causing an update to the SPA, since a separate update to the SPA is imminent
    public static Boolean preventPFSUpdateFromBizFarmTrigger = false; 

    // [DP] 02.24.2016 SFP-323 set share for sole props
    public static String solePropString = 'Sole Proprietorship';

    public static Map<Id, PFS__c> pfsMap = new Map<Id, PFS__c>();
    public static void handleAfterInsertAndAfterUpdate(List<Business_Farm__c> triggerNew, List<Business_Farm__c> triggerOld, Map<Id, Business_Farm__c> triggerNewMap, Map<Id, Business_Farm__c> triggerOldMap){

        if (trigger.isAfter && (trigger.isInsert || trigger.isUpdate)){
            // [DP] 08.05.2015 NAIS-2513 this method creates new School Biz Farm Assignment records when a Biz Farm is inserted
            // It also keeps School Biz Farm Assignment orig fields for existing records in sync with the Business Farm fields (provided the SPA has not been locked by the School)
            upsertSBFARecordsWithSPAValues(triggerNew, triggerOldMap);
        }

        if (trigger.isAfter){
            // [DP] 08.25.2015 NAIS-2527 this method forces a PFS EFC recalc when one is needed due to updates to EFC fields on the Biz Farm
            recalcEFCIfNeeded(triggerNew, triggerOld, triggerNewMap, triggerOldMap);
        }
        
        // NAIS-2494 START - Roll up business farm section completion status to PFS on BF after insert, update, delete, undelete
        rollupOwnerAndBusinessFarmCompletionStatusToPFS(triggerNew, triggerOldMap, trigger.isUpdate, trigger.isDelete);
        // NAIS-2494 END - Roll up business farm section completion status to PFS on BF after insert, update, delete, undelete
    }

    public static void upsertSBFARecordsWithSPAValues(List<Business_Farm__c> triggerNew, Map<Id, Business_Farm__c> triggerOldMap){
        // list for eventual update or insert
        List<School_Biz_Farm_Assignment__c> sbfasForUpsert = new List<School_Biz_Farm_Assignment__c>();

        // if this is after update, collect all of the business farm ids
        if (Trigger.isUpdate && Trigger.isAfter){
            Map<Id, List<School_Biz_Farm_Assignment__c>> bizFarmIdToChildSBFAs = new Map<Id, List<School_Biz_Farm_Assignment__c>>();
            for (Business_Farm__c bf : triggerNew){
                bizFarmIdToChildSBFAs.put(bf.Id, new List<School_Biz_Farm_Assignment__c>());
            }

            // map of SPAs
            Map<Id, School_PFS_Assignment__c> spaMap = new Map<Id, School_PFS_Assignment__c>();

            List<School_Biz_Farm_Assignment__c> sbfas = [SELECT Id,
                                                               Business_Farm__c,
                                                               School_PFS_Assignment__c,
                                                               School_PFS_Assignment__r.Family_May_Submit_Updates__c,
                                                               School_PFS_Assignment__r.Applicant__r.PFS__r.Id,
                                                               School_PFS_Assignment__r.Applicant__r.PFS__r.Payment_Status__c,
                                                               School_PFS_Assignment__r.Applicant__r.PFS__r.PFS_Status__c,
                                                               School_PFS_Assignment__r.Applicant__r.PFS__r.Visible_to_Schools_Until__c,
                                                               School_PFS_Assignment__r.Applicant__r.PFS__r.SSS_EFC_Calc_Status__c
                                                          FROM School_Biz_Farm_Assignment__c
                                                         WHERE Business_Farm__c IN :bizFarmIdToChildSBFAs.keySet()
                                                           AND School_PFS_Assignment__c != null]; // don't try to update SBFAs without an SPA [DP] 02.08.2016 SFP-201

            for (School_Biz_Farm_Assignment__c sbfa : sbfas) {
                bizFarmIdToChildSBFAs.get(sbfa.Business_Farm__c).add(sbfa);
                spaMap.put(sbfa.School_PFS_Assignment__c, sbfa.School_PFS_Assignment__r);
                pfsMap.put(sbfa.School_PFS_Assignment__r.Applicant__r.PFS__r.Id, sbfa.School_PFS_Assignment__r.Applicant__r.PFS__r);
            }

            // loop through child SBFAs, set their orig fields, and add to the map
            for (Business_Farm__c bf : triggerNew){
                sbfasForUpsert.addAll(setFieldsOnChildSBFAs(bf, bizFarmIdToChildSBFAs.get(bf.Id), spaMap));
            }
        }

        // if this is after insert, collect all of the business farm ids and pfs ids
        if (Trigger.isInsert && Trigger.isAfter){
            Map<Id, List<School_Biz_Farm_Assignment__c>> bizFarmIdToChildSBFAs = new Map<Id, List<School_Biz_Farm_Assignment__c>>();
            Map<Id, List<School_PFS_Assignment__c>> pfsIdToSPAList = new Map<Id, List<School_PFS_Assignment__c>>();
            for (Business_Farm__c bf : triggerNew){
                bizFarmIdToChildSBFAs.put(bf.Id, new List<School_Biz_Farm_Assignment__c>());
                pfsIdToSPAList.put(bf.PFS__c, new List<School_PFS_Assignment__c>());
            }

            // populate the SPA map by PFS id
            Map<Id, School_PFS_Assignment__c> spaMap = new Map<Id, School_PFS_Assignment__c>([Select Id, Applicant__r.PFS__c, Family_May_Submit_Updates__c,
                                                        Applicant__r.PFS__r.Id, Applicant__r.PFS__r.Payment_Status__c, Applicant__r.PFS__r.PFS_Status__c, Applicant__r.PFS__r.Visible_to_Schools_Until__c,  
                                                        Applicant__r.PFS__r.SSS_EFC_Calc_Status__c
                                                        FROM School_PFS_Assignment__c 
                                                        WHERE Applicant__r.PFS__c in :pfsIdToSPAList.keySet()
                                                        // TODO -- exclude Withdrawn here?
                                                        ]);
            
            // populate pfs map with results from SPA query
            for (School_PFS_Assignment__c spa : spaMap.values()){
                pfsIdToSPAList.get(spa.Applicant__r.PFS__c).add(spa);
                pfsMap.put(spa.Applicant__r.PFS__c, spa.Applicant__r.PFS__r);
            }

            // loop through Business Farms, create new SBFAs, and set orig fields
            for (Business_Farm__c bf : triggerNew){
                List<School_Biz_Farm_Assignment__c> sbfaList = new List<School_Biz_Farm_Assignment__c>();
                for (School_PFS_Assignment__c spa : pfsIdToSPAList.get(bf.PFS__c)){
                    sbfaList.add(new School_Biz_Farm_Assignment__c(Business_Farm__c = bf.Id, School_PFS_Assignment__c = spa.ID));
                }

                sbfasForUpsert.addAll(setFieldsOnChildSBFAs(bf, sbfaList, spaMap));
            }
        }
        
        if (!sbfasForUpsert.isEmpty()){
            upsert sbfasForUpsert;
        }
    }

    // SFP-62 [DP] 10.06.2015
    public static void updateSSSFieldsOnUnlockedSBFAs(Set<Id> unlockedPFSIds){
        List<Business_Farm__c> unlockedBizFarmList = [Select Id, Academic_Year__c, PFS__c, Business_Entity_Type__c,Business_Farm_Assets__c,Business_Farm_Debts__c,Business_Farm_Owner__c,Business_Farm_Ownership_Percent__c,Business_Net_Profit_Share_Current__c,Business_Farm_Share__c,
                                                            (Select Orig_Business_Entity_Type__c, Orig_Business_Farm_Assets__c, Orig_Business_Farm_Debts__c, Orig_Business_Farm_Owner__c, Orig_Business_Farm_Ownership_Percent__c, Orig_Net_Profit_Loss_Business_Farm__c, Orig_Business_Farm_Share__c
                                                                FROM School_Biz_Farm_Assignments__r)
                                                        FROM Business_Farm__c WHERE PFS__c in :unlockedPFSIds];

        List<School_Biz_Farm_Assignment__c> sbfaListForUpdate = new List<School_Biz_Farm_Assignment__c>();
        for (Business_Farm__c bf : unlockedBizFarmList){
            List<School_Biz_Farm_Assignment__c> thisBizsSBFAList = bf.School_Biz_Farm_Assignments__r;
            for (School_Biz_Farm_Assignment__c sbfa : thisBizsSBFAList){
                // this method returns true if an update is needed
                if (setFieldsOnOneSBFA(bf, sbfa, true)){
                    sbfaListForUpdate.add(sbfa);
                }
            }
        }

        if (!sbfaListForUpdate.isEmpty()){
            update sbfaListForUpdate;
        }
    }

    // [DP] 08.05.2015 NAIS-2513 generic method to set orig fields on school biz farm assignment
    public static List<School_Biz_Farm_Assignment__c> setFieldsOnChildSBFAs(Business_Farm__c bizFarm, List<School_Biz_Farm_Assignment__c> sbfaList, Map<Id, School_PFS_Assignment__c> spaMap){
        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){
            // SFP-194 [DP] 11.23.2015 check for null
            School_PFS_Assignment__c pfsAssignmentForBizFarm = spaMap.get(sbfa.School_PFS_Assignment__c);
            if (sbfa.School_PFS_Assignment__c != null && pfsAssignmentForBizFarm != null && pfsAssignmentForBizFarm.Family_May_Submit_Updates__c != 'No'){
                setFieldsOnOneSBFA(bizFarm, sbfa);
            }
        }
        return sbfaList;
    }

    // [DP] 08.17.2015 generic method used for update and insert
    public static void setFieldsOnOneSBFA(Business_Farm__c bizFarm, School_Biz_Farm_Assignment__c sbfa){
        // pass in "false" for boolean check, since SBFA might not have fields for comparison
        setFieldsOnOneSBFA(bizFarm, sbfa, false);
    }

    // [DP] 08.17.2015 generic method used for update and insert, returns "true" if an update is needed
    public static Boolean setFieldsOnOneSBFA(Business_Farm__c bizFarm, School_Biz_Farm_Assignment__c sbfa, Boolean checkForUpdate){
        Boolean updateNeeded = false; // added this to prevent needless updates to SBFA when the SPA is unlockec
        if (checkForUpdate){
            if (
                sbfa.Orig_Business_Farm_Assets__c != bizFarm.Business_Farm_Assets__c ||
                sbfa.Orig_Business_Farm_Debts__c != bizFarm.Business_Farm_Debts__c ||
                sbfa.Orig_Business_Farm_Owner__c != bizFarm.Business_Farm_Owner__c ||
                sbfa.Orig_Business_Farm_Ownership_Percent__c != bizFarm.Business_Farm_Ownership_Percent__c ||
                sbfa.Orig_Net_Profit_Loss_Business_Farm__c != bizFarm.Business_Net_Profit_Share_Current__c ||
                sbfa.Orig_Business_Farm_Share__c != bizFarm.Business_Farm_Share__c
            ){
                updateNeeded = true;
            }

            // SFP-1617 See if the Business Entity Type is different than the orig field on the SBFA.
            // This is only necessary if the feature toggle is on and the biz farm is for 2018-2019 or beyond.
            // Before 2018-2019, we did not track business entity type on SBFAs.
            if (FeatureToggles.isEnabled('Exclude_S_Corps_Partnerships_From_Sums__c') &&
                    AcademicYearUtil.is20182019OrLater(bizFarm.Academic_Year__c) &&
                    (sbfa.Orig_Business_Entity_Type__c != bizFarm.Business_Entity_Type__c)) {
                updateNeeded = true;
            }
        }

        sbfa.Orig_Business_Farm_Assets__c = bizFarm.Business_Farm_Assets__c;
        sbfa.Orig_Business_Farm_Debts__c = bizFarm.Business_Farm_Debts__c;
        sbfa.Orig_Business_Farm_Owner__c = bizFarm.Business_Farm_Owner__c;
        sbfa.Orig_Business_Farm_Ownership_Percent__c = bizFarm.Business_Farm_Ownership_Percent__c;
        sbfa.Orig_Net_Profit_Loss_Business_Farm__c = bizFarm.Business_Net_Profit_Share_Current__c;
        sbfa.Orig_Business_Farm_Share__c = bizFarm.Business_Farm_Share__c;

        // SFP-1617 Transfer Business Entity Type to orig field on the SBFA. This only happens if the feature toggle is on and the biz farm is for 2018-2019 or beyond.
        // Before 2018-2019, we did not track business entity type on SBFAs.
        if (FeatureToggles.isEnabled('Exclude_S_Corps_Partnerships_From_Sums__c') && AcademicYearUtil.is20182019OrLater(bizFarm.Academic_Year__c)) {
            sbfa.Orig_Business_Entity_Type__c = bizFarm.Business_Entity_Type__c;
        }

        return updateNeeded;
    }



    public static void recalcEFCIfNeeded(List<Business_Farm__c> triggerNew, List<Business_Farm__c> triggerOld, Map<Id, Business_Farm__c> triggerNewMap, Map<Id, Business_Farm__c> triggerOldMap){
        // [DP] 08.17.2015 NAIS-2527 
        Set<Id> pfsIdsForPotentialEFCRecalc = new Set<Id>();

        List<Business_Farm__c> businessFarmRecordsForLoop = (trigger.isDelete) ? triggerOld : triggerNew;

        // [DP] 08.17.2015 NAIS-2527
        for (Business_Farm__c bf : businessFarmRecordsForLoop){
            Business_Farm__c bfOld = (trigger.isUpdate) ? triggerOldMap.get(bf.Id) : null;
            if (EfcCalculatorAction.businessFarmHasUpdatedEfcFields(bf, bfOld)){
                pfsIdsForPotentialEFCRecalc.add(bf.PFS__c);
            }
        }

        // if pfs map didn't get totally populated, we need to query PFS directly
        Set<Id> pfsIdsThatNeedQuery = new Set<Id>();
        for (Business_Farm__c bf : businessFarmRecordsForLoop){
            if (pfsMap.get(bf.PFS__c) == null){
                pfsIdsThatNeedQuery.add(bf.PFS__c);
            }
        }
        if (pfsIdsThatNeedQuery.size() > 0){
            for (PFS__c pfs : [Select Id, Payment_Status__c, PFS_Status__c, Visible_to_Schools_Until__c, SSS_EFC_Calc_Status__c, Name FROM PFS__c where Id in :pfsIdsThatNeedQuery]){
                pfsMap.put(pfs.Id, pfs);
            }
        }
    
        List<PFS__c> pfsForEFCRecalc = new List<PFS__c>();
        for (Id pfsId : pfsIdsForPotentialEFCRecalc){
            PFS__c thePFS = pfsMap.get(pfsId);
            if (thePFS.SSS_EFC_Calc_Status__c != EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE && EfcCalculatorAction.isStatusRecalculable(thePFS.PFS_Status__c, thePFS.Payment_Status__c, thePFS.Visible_to_Schools_Until__c)){
                pfsForEFCRecalc.add(new PFS__c(Id = thePFS.ID, SSS_EFC_Calc_Status__c = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE));
            }
        }
        // update PFSs that need to have EFC recalculated
        if (!pfsForEFCRecalc.isEmpty() && !preventPFSUpdateFromBizFarmTrigger){ // SFP-82 [DP] 12.04.2015 only proceed if we haven't prevented update from occurring
            update pfsForEFCRecalc;
        }
    }
    
    // NAIS-2494 START - Roll up business farm section completion status to PFS
    
    private static final List<String> statusFields = new List<String> { 'statBusinessAssets__c', 'statBusinessExpenses__c', 'statBusinessIncome__c', 'statBusinessInformation__c' };
    
    // NAIS-2500 Also set Business/Farm Owner
    public static void rollupOwnerAndBusinessFarmCompletionStatusToPFS(List<Business_Farm__c> triggerNew, Map<Id, Business_Farm__c> triggerOldMap, Boolean isUpdate, Boolean isDelete) {
        
        // get all PFSs that may need to be updated
        Set<Id> pfsIds = new Set<Id>();
        if (isUpdate) {
            // BF after update
            for (Business_Farm__c bf : triggerNew) {
                Business_Farm__c oldBF = triggerOldMap.get(bf.Id);
                if (bf.Business_Farm_Owner__c != oldBF.Business_Farm_Owner__c){
                    pfsIds.add(bf.PFS__c);
                } else {
                    for (String sf : statusFields) {
                        if (bf.get(sf) != oldBF.get(sf)) {
                            pfsIds.add(bf.PFS__c);
                            break;
                        }
                    }
                }
            }
        } else {
            // BF after insert, delete, undelete
            for (Business_Farm__c bf : (isDelete ? triggerOldMap.values() : triggerNew)) {
                pfsIds.add(bf.PFS__c);
            }
        }
        
        // special case for delete: reset status fields if PFS is left with no business farm records
        Set<Id> PFSWithNoBFs = new Set<Id>();
        if (isDelete) {
            PFSWithNoBFs.addAll(pfsIds);
            for (PFS__c pfs : [select Id, (select Id from Business_Farms__r limit 1) from PFS__c where Id in :pfsIds]) {
                for (Business_Farm__c bf : pfs.Business_Farms__r) {
                    PFSWithNoBFs.remove(pfs.Id);
                }
            }
        }
        
        // NAIS-2500 [DP] 09.21.2015 dummy records used to set business farm owner
        List<School_Biz_Farm_Assignment__c> dummySBFAList = new List<School_Biz_Farm_Assignment__c>();

        // rollup business farm section statuses to PFSs
        List<PFS__c> pfsToUpdate = new List<PFS__c>();
        for (PFS__c pfs : [select statBusinessAssets__c, statBusinessExpenses__c, statBusinessIncome__c, statBusinessInformation__c, Business_Farm_Owner__c, 
                            (select statBusinessAssets__c, statBusinessExpenses__c, statBusinessIncome__c, statBusinessInformation__c, Business_Farm_Owner__c 
                                from Business_Farms__r
                                // NAIS-2500 [DP] 09.21.2015 commented out so we get all Biz Farms to determine the Owner
                                //where statBusinessAssets__c = false or statBusinessExpenses__c = false or statBusinessIncome__c = false or statBusinessInformation__c = false
                                )
                            from PFS__c
                            where Id in :pfsIds]) {
            Boolean statBusinessAssets, statBusinessExpenses, statBusinessIncome, statBusinessInformation;
            if (isDelete && PFSWithNoBFs.contains(pfs.Id)) {
                statBusinessAssets = statBusinessExpenses = statBusinessIncome = statBusinessInformation = false;
                pfs.Business_Farm_Owner__c = null;
            } else {
                statBusinessAssets = statBusinessExpenses = statBusinessIncome = statBusinessInformation = true;
            }
            for (Business_Farm__c bf : pfs.Business_Farms__r) {
                statBusinessAssets = statBusinessAssets && bf.statBusinessAssets__c;
                statBusinessExpenses = statBusinessExpenses && bf.statBusinessExpenses__c;
                statBusinessIncome = statBusinessIncome && bf.statBusinessIncome__c;
                statBusinessInformation = statBusinessInformation && bf.statBusinessInformation__c;
                dummySBFAList.add(new School_Biz_Farm_Assignment__c(Business_Farm_Owner__c = bf.Business_Farm_Owner__c, Orig_Business_Farm_Owner__c = bf.Business_Farm_Owner__c));
            }
            Boolean isChanged = false;
            if (pfs.statBusinessAssets__c != statBusinessAssets) {
                pfs.statBusinessAssets__c = statBusinessAssets;
                isChanged = true;
            }
            if (pfs.statBusinessExpenses__c != statBusinessExpenses) {
                pfs.statBusinessExpenses__c = statBusinessExpenses;
                isChanged = true;
            }
            if (pfs.statBusinessIncome__c != statBusinessIncome) {
                pfs.statBusinessIncome__c = statBusinessIncome;
                isChanged = true;
            }
            if (pfs.statBusinessInformation__c != statBusinessInformation) {
                pfs.statBusinessInformation__c = statBusinessInformation;
                isChanged = true;
            }

            // NAIS-2500 [DP] 09.21.2015 dummy records used to set business farm owner
            String ownerString = SchoolPFSAction.summarizeBizFarmOwners(dummySBFAList);
            if (pfs.Business_Farm_Owner__c != ownerString){
                pfs.Business_Farm_Owner__c = ownerString;
                isChanged = true;
            }
            if (isChanged) {
                pfsToUpdate.add(pfs);
            }
        }
        
        if (!pfsToUpdate.isEmpty()) {
            update pfsToUpdate;
        }
        
    }
    
    // NAIS-2494 END - Roll up business farm section completion status to PFS

    // [DP] 02.22.2016 SFP-318 set totals on biz farm
    public static void setTotalsOnBizFarm(Business_Farm__c bf){
        bf.Business_Total_Expenses_Current__c = EfcUtil.nullToZero(bf.Business_Salary_Paid_Current__c) + EfcUtil.nullToZero(bf.Business_Other_Wages_Current__c) + EfcUtil.nullToZero(bf.Business_Addtl_Comp_Current__c) + EfcUtil.nullToZero(bf.Business_Farm_Asset_Depreciation__c) + EfcUtil.nullToZero(bf.Business_Rent_Cost_Current__c) + EfcUtil.nullToZero(bf.Business_Mortgage_Current__c) + EfcUtil.nullToZero(bf.Business_Other_Expenses_Current__c);
        bf.Business_Total_Income_Current__c = EfcUtil.nullToZero(bf.Business_Gross_Profit_Current__c) + EfcUtil.nullToZero(bf.Business_Other_Income_Current__c);
        bf.Net_Profit_Loss_Business_Farm_Current__c = EfcUtil.nullToZero(bf.Business_Total_Income_Current__c) - EfcUtil.nullToZero(bf.Business_Total_Expenses_Current__c);
        // [DP] 02.24.2016 SFP-323 set share for sole props
        if (bf.Business_Entity_Type__c == solePropString){
            bf.Business_Net_Profit_Share_Current__c = bf.Net_Profit_Loss_Business_Farm_Current__c;
        }

        bf.Business_Total_Expenses_Est__c = EfcUtil.nullToZero(bf.Business_Salary_Paid_Est__c) + EfcUtil.nullToZero(bf.Business_Other_Wages_Est__c) + EfcUtil.nullToZero(bf.Business_Addtl_Comp_Est__c) + EfcUtil.nullToZero(bf.Business_Farm_Asset_Depreciation__c) + EfcUtil.nullToZero(bf.Business_Rent_Cost_Est__c) + EfcUtil.nullToZero(bf.Business_Mortgage_Est__c) + EfcUtil.nullToZero(bf.Business_Other_Expenses_Est__c);
        bf.Business_Total_Income_Est__c = EfcUtil.nullToZero(bf.Business_Gross_Profit_Est__c) + EfcUtil.nullToZero(bf.Business_Other_Income_Est__c);
        bf.Net_Profit_Loss_Business_Farm_Est__c = EfcUtil.nullToZero(bf.Business_Total_Income_Est__c) - EfcUtil.nullToZero(bf.Business_Total_Expenses_Est__c);
        // [DP] 02.24.2016 SFP-323 set share for sole props
        if (bf.Business_Entity_Type__c == solePropString){
            bf.Business_Net_Profit_Share_Est__c = bf.Net_Profit_Loss_Business_Farm_Est__c;
        }
    }
    
}