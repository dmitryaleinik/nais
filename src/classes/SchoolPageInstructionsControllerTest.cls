@IsTest
private class SchoolPageInstructionsControllerTest
{

    public static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

    @isTest
    private static void testController()
    {
        Account school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, 5, false);
        insert new List<Account> { school1 };

        Contact staff1 = TestUtils.createContact('Staff 1', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
        staff1.Email = 'staff1@test.org';
        staff1.PFS_Alert_Preferences__c = 'Receive Alerts';

        insert new List<Contact> { staff1 };

        User portalUser = TestUtils.createPortalUser(
            staff1.LastName, staff1.Email + String.valueOf(Math.random()) + String.valueOf(Math.random()), 'alias', staff1.Id,
            GlobalVariables.schoolPortalAdminProfileId, true, false);

        System.runAs(thisUser){
            insert portalUser;
        }

        System.runAs(portalUser){
            SchoolPageInstructionsController roller = new SchoolPageInstructionsController();
            roller.labelName = 'School_Instructions_Annual_Settings';
            roller.fieldName = 'SYSTEM_Hide_Instructions_Annual_Settings__c';

            System.assertEquals(true, roller.getRenderLabel());

            roller.hideInstructionText();
        }

        System.runAs(portalUser){
            SchoolPageInstructionsController roller = new SchoolPageInstructionsController();
            roller.labelName = 'School_Instructions_Annual_Settings';
            roller.fieldName = 'SYSTEM_Hide_Instructions_Annual_Settings__c';

            System.assertEquals(false, roller.getRenderLabel());
        }
    }
}