/**
 * Class: SchoolBizFarmAssignmentAction
 *
 * Copyright (C) 2015 Exponent Partners - DP
 *
 * Purpose: NAIS-2501 Family select schools page updates
 * Need to create Business junction records on new SPA/withdrawn, etc
 * Same logic from B/F-003 but with different entry point
 * 
 * Where Referenced: SchoolPFSAssignmentAfter.trigger
 *   
 *
 * Change History:
 *
 * Developer         Date                          Description
 * ---------------------------------------------------------------------------------------
 * Drew Piston      2015.08.17           Initial Development
 *
 *
 */
public class SchoolBizFarmAssignmentAction 
{

    Map<String, String> sbfaSourceTargetMap = new Map<String, String>();
    Map<String, DescribeFieldResult> fieldResultsMap = new Map<String, DescribeFieldResult>();
    Map <String, Field_Count__c> key_fieldCount_map = new Map <String, Field_Count__c>();
    Map<ID,School_PFS_Assignment__c> spaMap = new Map<ID,School_PFS_Assignment__c>();
    List <Field_Count__c> fieldCountUpdateList = new List <Field_Count__c>();
    List <Field_Count__c> fieldCountNewList = new List <Field_Count__c>();
    Map<String,Field_Count__c> updateFCMap = new Map<String,Field_Count__c>();
    Set <String> schoolIdSet = new Set <String>();

    // SFP-128 [DP] 11.02.2015 boolean that is set to "true" if changes to the SBFA are going to result in the SPA needing an update
    public static Boolean spaEFCRecalcNeeded = false;
    // SFP-128 [DP] 11.02.2015 boolean to prevent changes to the SBFA from causing an update to the SPA, since a separate update to the SPA is imminent
    public static Boolean preventSPAUpdateFromSBATrigger = false;

    public static void createChildSbizFarmAs(List<School_PFS_Assignment__c> newSPAs)
    {
        if (newSPAs == null || newSPAs.isEmpty())
        {
            return;
        }

        // DP] 08.17.2015 query happens in prior method, see line 153 of SchoolPFSAssignmentAfter.trigger
        Set<Id> pfsIds = new Set<Id>();
        for (School_PFS_Assignment__c spa : newSPAs)
        {
            pfsIds.add(spa.Applicant__r.PFS__c);
        }

        List<Business_Farm__c> bizFarms = BusinessFarmSelector.newInstance().selectByPfsIdWithSBFAsWithCustomFieldLists(
            pfsIds, 
            new List<String> {'Id', 'Academic_Year__c', 'Business_Entity_Type__c', 'PFS__c', 'Business_Farm_Assets__c', 'Business_Farm_Debts__c', 'Business_Farm_Owner__c',
                'Business_Farm_Ownership_Percent__c', 'Business_Net_Profit_Share_Current__c', 
                'Net_Profit_Loss_Business_Farm_Current__c', 'Business_Farm_Share__c'}, 
            new List<String> {'School_PFS_Assignment__c', 'School_PFS_Assignment__r.School__c', 'Business_Farm_Assets__c', 
                'Business_Farm_Debts__c', 'Business_Farm_Owner__c', 'Business_Farm_Ownership_Percent__c', 
                'Business_Farm_Share__c', 'Net_Profit_Loss_Business_Farm__c'});

        Map<Id, List<Business_Farm__c>> pfsIdToBizFarmList = new Map<Id, List<Business_Farm__c>>();

        // find all business farms associated with these PFSs
        for (Business_Farm__c bizFarm : bizFarms)
        {
            if (pfsIdToBizFarmList.get(bizFarm.PFS__c) == null)
            {
                pfsIdToBizFarmList.put(bizFarm.PFS__c, new List<Business_Farm__c>());
            }

            pfsIdToBizFarmList.get(bizFarm.PFS__c).add(bizFarm);
        }

        // make an SBFA connected between every Business/Farm and SPA
        List<School_Biz_Farm_Assignment__c> sbfaListForInsert = new List<School_Biz_Farm_Assignment__c>();

        for (School_PFS_Assignment__c spa : newSPAs)
        {
            if (pfsIdToBizFarmList.get(spa.Applicant__r.PFS__c) != null)
            {
                for (Business_Farm__c bizFarm : pfsIdToBizFarmList.get(spa.Applicant__r.PFS__c))
                {
                    School_Biz_Farm_Assignment__c sbfa = new School_Biz_Farm_Assignment__c(
                        Business_Farm__c = bizFarm.Id, School_PFS_Assignment__c = spa.Id);
                    
                    BusinessFarmAction.setFieldsOnOneSBFA(bizFarm, sbfa);
                    
                    List<School_Biz_Farm_Assignment__c> existingSbfas = bizFarm.School_Biz_Farm_Assignments__r;

                    if (existingSbfas != null && !existingSbfas.isEmpty())
                    {
                        copyRevisionFieldsToNewSBFA(spa.School__c, sbfa, existingSbfas);
                    }

                    sbfaListForInsert.add(sbfa);
                }
            }
        }

        if (!sbfaListForInsert.isEmpty())
        {
            insert sbfaListForInsert;
        }
    }

    private static void copyRevisionFieldsToNewSBFA(
        Id newSbfaSchoolId, School_Biz_Farm_Assignment__c sbfa, List<School_Biz_Farm_Assignment__c> existingSbfas)
    {
        for (School_Biz_Farm_Assignment__c existingSbfa : existingSbfas)
        {
            if (allSbfaRevisionFieldsArePopulated(sbfa))
            {
                return;
            }

            if (newSbfaSchoolId != existingSbfa.School_PFS_Assignment__r.School__c)
            {
                continue;
            }

            if (sbfa.Business_Farm_Assets__c == null && existingSbfa.Business_Farm_Assets__c != null)
            {
                sbfa.Business_Farm_Assets__c = existingSbfa.Business_Farm_Assets__c;
            }

            if (sbfa.Business_Farm_Debts__c == null && existingSbfa.Business_Farm_Debts__c != null)
            {
                sbfa.Business_Farm_Debts__c = existingSbfa.Business_Farm_Debts__c;
            }

            if (sbfa.Business_Farm_Owner__c == null && existingSbfa.Business_Farm_Owner__c != null)
            {
                sbfa.Business_Farm_Owner__c = existingSbfa.Business_Farm_Owner__c;
            }

            if (sbfa.Business_Farm_Ownership_Percent__c == null && existingSbfa.Business_Farm_Ownership_Percent__c != null)
            {
                sbfa.Business_Farm_Ownership_Percent__c = existingSbfa.Business_Farm_Ownership_Percent__c;
            }

            if (sbfa.Business_Farm_Share__c == null && existingSbfa.Business_Farm_Share__c != null)
            {
                sbfa.Business_Farm_Share__c = existingSbfa.Business_Farm_Share__c;
            }

            if (sbfa.Net_Profit_Loss_Business_Farm__c == null && existingSbfa.Net_Profit_Loss_Business_Farm__c != null)
            {
                sbfa.Net_Profit_Loss_Business_Farm__c = existingSbfa.Net_Profit_Loss_Business_Farm__c;
            }
        }
    }

    private static Boolean allSbfaRevisionFieldsArePopulated(School_Biz_Farm_Assignment__c sbfa)
    {
        if (sbfa.Business_Farm_Assets__c != null && sbfa.Business_Farm_Debts__c != null && sbfa.Business_Farm_Owner__c != null
            && sbfa.Business_Farm_Ownership_Percent__c != null && sbfa.Business_Farm_Share__c != null
            && sbfa.Net_Profit_Loss_Business_Farm__c != null)
        {
            return true;
        }

        return false;
    }

    public static void recalcEfcIfNeeded(List<School_Biz_Farm_Assignment__c> sbfaList, Map<Id, School_Biz_Farm_Assignment__c> oldSBFAMap){
        Set<Id> spaIdsForPotentialEFCRecalc = new Set<Id>();
        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){
            School_Biz_Farm_Assignment__c sbfaOld = (oldSBFAMap == null) ? null : oldSBFAMap.get(sbfa.Id);
            if (EfcCalculatorAction.schoolbizfarmassignmentHasUpdatedEfcFields(sbfa, sbfaOld)){
                spaIdsForPotentialEFCRecalc.add(sbfa.School_PFS_Assignment__c);
            }
        }
        if (!spaIdsForPotentialEFCRecalc.isEmpty()){
            List<School_PFS_Assignment__c> schoolPfsAssignmentsToUpdate = new List<School_PFS_Assignment__c>();
            for (School_PFS_Assignment__c spa : [Select Id, PFS_Status__c, Payment_Status__c, Visible_to_Schools_Until__c, Revision_EFC_Calc_Status__c, Applicant__r.PFS__r.PFS_Status__c, Applicant__r.PFS__r.Payment_Status__c, Applicant__r.PFS__r.Visible_to_Schools_Until__c FROM School_PFS_Assignment__c WHERE Id in :spaIdsForPotentialEFCRecalc]){
                if (spa.Revision_EFC_Calc_Status__c != EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE && EfcCalculatorAction.isStatusRecalculable(spa.Applicant__r.PFS__r.PFS_Status__c, spa.Applicant__r.PFS__r.Payment_Status__c, spa.Applicant__r.PFS__r.Visible_to_Schools_Until__c)){
                    schoolPfsAssignmentsToUpdate.add(new School_PFS_Assignment__c(Id = spa.Id, Revision_EFC_Calc_Status__c = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE));
                }
            }
            if (!schoolPfsAssignmentsToUpdate.isEmpty()){
                spaEFCRecalcNeeded = true; // SFP-128 set this flag to true so we know that an EFC recalc is needed on the SPA
                if (preventSPAUpdateFromSBATrigger != true){ // SFP-128 only go through with update if we haven't explicitly prevented it because a different update is imminent
                    update schoolPfsAssignmentsToUpdate;
                }
            }
        }
    }

    //SFP-55, [G.S]
    public void SetRevisionMetrics(Map<Id, School_Biz_Farm_Assignment__c> newMap, Map<Id, School_Biz_Farm_Assignment__c> oldMap){

        Set<Id> spaSet = new Set<Id>();

        loadSBFASourceTargetMap();

        if (oldMap != null && oldMap.size() > 0) {
            for (School_Biz_Farm_Assignment__c sbfsa : oldMap.values()) {
                if(String.isNotBlank(sbfsa.School_PFS_Assignment__c)){
                    spaSet.add(sbfsa.School_PFS_Assignment__c);
                }
            }
        }
        if (newMap != null && newMap.size() > 0) {
            for (School_Biz_Farm_Assignment__c sbfsa : newMap.values()) {
                if(String.isNotBlank(sbfsa.School_PFS_Assignment__c)){
                    spaSet.add(sbfsa.School_PFS_Assignment__c);
                }
            }
        }
        if(spaSet != null && spaSet.size() > 0){
            spaMap = new Map<ID,School_PFS_Assignment__c>([select Id, School__c,Academic_Year_Picklist__c from School_PFS_Assignment__c  where Id in :spaSet]);

            for(School_PFS_Assignment__c spa : spaMap.values()){
                schoolIdSet.add(spa.School__c);
            }

            for (Field_Count__c fc : [select School__c,Business_Farm_Owner__c,Academic_Year__c,Business_Farm_Assets__c,
                    Business_Farm_Debts__c, Business_Farm_Ownership_Percent__c,
                    Net_Profit_Loss_Business__c from Field_Count__c where School__c in :schoolIdSet]) {
                String key = fc.School__c + '_' + fc.Academic_Year__c;
                key_fieldCount_map.put(key, fc);
            }
            // for each metrics field set correcsponding count field
            for (String sourceField : sbfaSourceTargetMap.keySet()) {
                String fieldName = (sbfaSourceTargetMap.get(sourceField)!=null)?sbfaSourceTargetMap.get(sourceField):sourceField;
                String origFieldName = 'Orig_'+sourceField;

                for (String sbfsaId : newMap.keySet()) {
                    School_Biz_Farm_Assignment__c newSbfsa = newMap.get(sbfsaId);
                    School_Biz_Farm_Assignment__c oldSbfsa = oldMap.get(sbfsaId);
                    if (newSbfsa.get(sourceField) != oldSbfsa.get(sourceField)){
                        createUpdateFieldCount(newSbfsa, fieldName, sourceField);
                    }
                }
            }

            // update
            if (updateFCMap.size() > 0) {
                upsert updateFCMap.values();
            }
        }
    }

    private void loadSBFASourceTargetMap() {
        sbfaSourceTargetMap.put('Business_Farm_Owner__c', null);
        sbfaSourceTargetMap.put('Business_Farm_Assets__c', null);
        sbfaSourceTargetMap.put('Business_Farm_Debts__c', null);
        sbfaSourceTargetMap.put('Business_Farm_Ownership_Percent__c', null);
        sbfaSourceTargetMap.put('Net_Profit_Loss_Business_Farm__c', 'Net_Profit_Loss_Business__c');
    }

    private void createUpdateFieldCount(School_Biz_Farm_Assignment__c sbfa, String sourceField, String sbfaName) {
        if(String.isNotBlank(sbfa.School_PFS_Assignment__c)){
            School_PFS_Assignment__c newSpfsa = spaMap.get(sbfa.School_PFS_Assignment__c);
            String key = newSpfsa.School__c + '_' + GlobalVariables.getAcademicYearByName(newSpfsa.Academic_Year_Picklist__c).Id;
            if (key_fieldCount_map.containsKey(key)) {
                Field_Count__c fc = key_fieldCount_map.get(key);
                if (fc.get(sourceField) != null) {
                    fc.put(sourceField, (Decimal)fc.get(sourceField) + 1);
                } else {
                    fc.put(sourceField, 1);
                }
                updateFCMap.put(key,fc);
            } else {
                Field_Count__c fc = new Field_Count__c();
                fc.School__c = newSpfsa.School__c;
                fc.Academic_Year__c = GlobalVariables.getAcademicYearByName(newSpfsa.Academic_Year_Picklist__c).Id;
                if (sbfa.get(sbfaName) != null) {
                    fc.put(sourceField, 1);
                }
                key_fieldCount_map.put(key, fc);
                updateFCMap.put(key,fc);
            }
        }
    }
}