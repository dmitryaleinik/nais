/**
 *    NAIS-503: Any changes made to the application data should be captured in an audit trail
 *
 *    SL, Exponent Partners, 2013
 */
 
 
public with sharing class AuditLogAction {

    /* List of PFS fields to audit */
    private static final List<String> PFS_AUDIT_FIELDS = new List<String> {
            'Parent_A_State__c',
            'Parent_B_State__c',
            'Number_of_Dependent_Children__c',
            'Salary_Wages_Parent_A__c',
            'Salary_Wages_Parent_B__c',
            'Dividend_Income_Current__c',
            'Interest_Income_Current__c',
            'Alimony_Current__c',
            'Other_Taxable_Income_Current__c',
            'Untaxed_IRA_Plan_Payment_Current__c',
            'Keogh_Plan_Payment_Current__c',
            'IRS_Adjustments_to_Income_Current__c',
            'Net_Profit_Loss_Business_Farm_Current__c',
            'Business_Farm_Owner__c',
            //'Business_Farm_Ownership_Percent__c',
            'Child_Support_Received_Current__c',
            'Social_Security_Benefits_Current__c',
            'Other_Non_Taxable_Income__c',
            'Home_Market_Value__c',
            'Unpaid_Principal_1st_Mortgage__c',
            'Unpaid_Principal_2nd_Mortgage__c',
            'Other_Real_Estate_Market_Value_Total__c',
            'Other_Real_Estate_Unpaid_Principal__c',
            'Bank_Account_Value__c',
            'Investments_Net_Value__c'            
    };
    
    /* List of Applicant fields to audit */
    private static final List<String> APPLICANT_AUDIT_FIELDS = (!Test.isRunningTest()) ? new List<String> {
            // currently no applicant fields specified for audit
            'Apply_for_Kipona_Scholars_Program__c',
            'Apply_for_Pauahi_Keiki_Scholars_Cycle_1__c',
            'Apply_for_Pauahi_Keiki_Scholars_Cycle_2__c'

        }
        
// >>> UNIT TEST VALUES ONLY - only set if Test.isRunningTest() == true
        : new List<String> {
               'Assets_Total_Value__c',
               'Grade_in_Entry_Year__c'
// <<< UNIT TEST VALUES ONLY
        };
    
    /* List of School PFS Assignment fields to audit */
    private static final List<String> SCHOOL_PFS_ASSIGNMENT_AUDIT_FIELDS = new List<String> {
            'Parent_State__c',
            'Dependent_Children__c',
            'Salary_Wages_Parent_A__c',
            'Salary_Wages_Parent_B__c',
            'Dividend_Income__c',
            'Interest_Income__c',
            'Alimony_Received__c',
            'Other_Taxable_Income__c',
            'Untaxed_IRA_Plan_Payment__c',
            'Keogh_Plan_Payment__c',
            'Adjustments_to_Income__c',
            //'Net_Profit_Loss_Business_Farm__c',
            //'Business_Farm_Owner__c', [DP] 03.02.2016 SFP-332
            //'Business_Farm_Ownership_Percent__c',
            'Child_Support_Received__c',
            'Social_Security_Benefits__c',
            'Other_Nontaxable_Income_Currency__c',
            'Home_Market_Value__c',
            'Unpaid_Principal_1st_Mortgage__c',
            'Unpaid_Principal_2nd_Mortgage__c',
            'Other_Real_Estate_Market_Value__c',
            'Other_Real_Estate_Unpaid_Principal__c',
            'Bank_Accounts__c',
            'Investments__c'  
    };
    
    /* List of PJ fields to audit */
    private static final List<String> SCHOOL_PFS_ASSIGNMENT_PJ_AUDIT_FIELDS = new List<String> {
            'Use_Home_Equity__c',
            'Use_Home_Equity_Cap__c',
            'Use_Housing_Index_Multiplier__c',
            'Use_Div_Int_to_Impute_Assets__c',
            'Impute__c',
            'Apply_Minimum_Income__c',
            'Minimum_Income_Amount__c',
            'Use_COLA__c',
            'Override_Default_COLA_Value__c'
    };
    
    /* character appended at the end of each log line*/
    private static final String NEWLINE = '\n';
    
    /* maximum string length of the log */
    public static final Integer LOG_MAX_SIZE = 32000;
    public static final Integer MIN_LOG_SIZE_AFTER_ARCHIVE = 16000;
    public static final String ARCHIVE_MESSAGE = '(...see archived change logs for earlier entries...)';
    
    /* message written to the log when there is a PJ change */
    public static final String PJ_CHANGE_ALERT = 'Global/Individual PJ Settings Applied';
    
    /* Schema field maps (used to get describe info) */
    private static Map<String, Schema.SObjectField> pfsFieldMap = null;
    private static Map<String, Schema.SObjectField> applicantFieldMap = null;
    private static Map<String, Schema.SObjectField> schoolPfsAssignmentFieldMap = null;
    
    /* Field label maps*/
    private static Map<String, String> pfsFieldLabelMap = new Map<String, String>();
    private static Map<String, String> applicantFieldLabelMap = new Map<String, String>();
    private static Map<String, String> schoolPfsAssignmentFieldLabelMap = new Map<String, String>();
    
    /* Represents one log entry (could have multiple lines) */
    public class LogEntry {
        private String logEntryString = '';
        
        // add a line to the log (automatically adds timestamp and username)
        public void addLine(String line) {
            logEntryString += System.now() + ' ' + UserInfo.getName() + ' ' + line + NEWLINE;
        }
        
        // adds a field change line to the log
        public void addFieldChange(String fieldLabel, String oldValue, String newValue) {
            addLine(fieldLabel + ' old:' + oldValue + ' new:' + newValue);
        }
        
        // returns true if the log entry is not blank
        public boolean hasLogInfo() {
            return (logEntryString.length() > 0);
        }
        
        // returns the log entry as a string
        public override String toString() {
            return logEntryString;
        }
    }
    
    /* entry point for PFS audit */
    public static void doAudit(List<PFS__c> newList, Map<Id, PFS__c> oldMap) {
        if (!PFS_AUDIT_FIELDS.isEmpty()) {
            // keep track of archived logs to insert
            List<Attachment> logArchives = new List<Attachment>();
            
            for (PFS__c pfs : newList) {
                // only log changes for submitted/paid NAIS-1137 [dp] 10.7.13
                if (pfs.PFS_Status__c == EfcPicklistValues.PFS_STATUS_SUBMITTED && pfs.Payment_Status__c == EfcPicklistValues.PAYMENT_STATUS_PAID_IN_FULL){
                    AuditLogAction.logFieldChange(pfs, oldMap.get(pfs.Id), logArchives);
                }
            }
            
            // insert any archived logs
            if (!logArchives.isEmpty()) {
                insert logArchives;
            }
        }
    }
    
    /* entry point for Applicant audit */    
    public static void doAudit(List<Applicant__c> newList, Map<Id, Applicant__c> oldMap) {
        if (!APPLICANT_AUDIT_FIELDS.isEmpty()) {
            // keep track of archived logs to insert
            List<Attachment> logArchives = new List<Attachment>();
            
            // only log changes for submitted/paid NAIS-1137 [dp] 10.7.13            
            Set<Id> PFSIds = new Set<Id>();
            Set<Id> submittedAndPaidPFSIds = new Set<Id>();
            for (Applicant__c applicant : newList) {
                PFSIds.add(applicant.PFS__c);
            }
            
            for (PFS__c pfs : [Select Id, Payment_Status__c, PFS_Status__c from PFS__c 
                                                                where Id in :PFSIds 
                                                                AND PFS_Status__c = :EfcPicklistValues.PFS_STATUS_SUBMITTED
                                                                AND Payment_Status__c = :EfcPicklistValues.PAYMENT_STATUS_PAID_IN_FULL]){
                submittedAndPaidPFSIds.add(pfs.Id);
            }    

            if (!submittedAndPaidPFSIds.isEmpty()){
                for (Applicant__c applicant : newList) {
                    if(applicant.PFS__c != null && submittedAndPaidPFSIds.contains(applicant.PFS__c)){
                        AuditLogAction.logFieldChange(applicant, oldMap.get(applicant.Id), logArchives);
                    }
                }
            }
            
            // insert any archived logs
            if (!logArchives.isEmpty()) {
                insert logArchives;
            }
        }
    }
    
    /* entry point for School PFS Assignment audit */
    public static void doAudit(List<School_PFS_Assignment__c> newList, Map<Id, School_PFS_Assignment__c> oldMap) {
        if ((!SCHOOL_PFS_ASSIGNMENT_AUDIT_FIELDS.isEmpty()) || (!SCHOOL_PFS_ASSIGNMENT_PJ_AUDIT_FIELDS.isEmpty())) {
            // keep track of archived logs to insert
            List<Attachment> logArchives = new List<Attachment>();
            
            for (School_PFS_Assignment__c schoolPfsAssignment : newList) {
                
                // [SL] NAIS-1021: only log for submitted/paid
                if ((schoolPfsAssignment.PFS_Status__c == EfcPicklistValues.PFS_STATUS_SUBMITTED) &&
                        (schoolPfsAssignment.Payment_Status__c == EfcPicklistValues.PAYMENT_STATUS_PAID_IN_FULL)) {  
                            
                    AuditLogAction.logFieldChange(schoolPfsAssignment, oldMap.get(schoolPfsAssignment.Id), logArchives);
                }
                       
            }
            
            // insert any archived logs
            if (!logArchives.isEmpty()) {
                insert logArchives;
            }
        }
    }
    
    /* Method to get the label for a PFS field */
    private static String getLabelForPfsField(String fieldName) {
        if (pfsFieldMap == null) {
            pfsFieldMap = Schema.SObjectType.PFS__c.fields.getMap();
        }
        // get the field label, but if we've already reached the field describe limit, then use just the API name
        String fieldLabel = pfsFieldLabelMap.get(fieldName);
        if (fieldLabel == null) {
            if (Limits.getFieldsDescribes() < Limits.getLimitFieldsDescribes()) {
                fieldLabel = pfsFieldMap.get(fieldName).getDescribe().getLabel();
            }
            else {
                fieldLabel = fieldName;
            }
            pfsFieldLabelMap.put(fieldName, fieldLabel);
        }
        return fieldLabel;
    }
    
    /* Method to get the label for a Applicant field */
    private static String getLabelForApplicantField(String fieldName) {
        if (applicantFieldMap == null) {
            applicantFieldMap = Schema.SObjectType.Applicant__c.fields.getMap();
        }
        // get the field label, but if we've already reached the field describe limit, then use just the API name
        String fieldLabel = applicantFieldLabelMap.get(fieldName);
        if (fieldLabel == null) {
            if (Limits.getFieldsDescribes() < Limits.getLimitFieldsDescribes()) {
                fieldLabel = applicantFieldMap.get(fieldName).getDescribe().getLabel();
            }
            else {
                fieldLabel = fieldName;
            }
            applicantFieldLabelMap.put(fieldName, fieldLabel);
        }
        return fieldLabel;
    }
    
    /* Method to get the label for a School PFS Assignment field */
    private static String getLabelForSchoolPfsAssignmentField(String fieldName) {
        if (schoolPfsAssignmentFieldMap == null) {
            schoolPfsAssignmentFieldMap = Schema.SObjectType.School_PFS_Assignment__c.fields.getMap();
        }
        // get the field label, but if we've already reached the field describe limit, then use just the API name
        String fieldLabel = schoolPfsAssignmentFieldLabelMap.get(fieldName);
        if (fieldLabel == null) {
            if (Limits.getFieldsDescribes() < Limits.getLimitFieldsDescribes()) {
                fieldLabel = schoolPfsAssignmentFieldMap.get(fieldName).getDescribe().getLabel();
            }
            else {
                fieldLabel = fieldName;
            }
            schoolPfsAssignmentFieldLabelMap.put(fieldName, fieldLabel);
        }
        return fieldLabel;
    }
    
    /* returns a string dump of the PJ settings from a School PFS Assignment record */
    private static String getPJSettingsString(School_PFS_Assignment__c schoolPfsAssignment) {
        List<String> pjFieldValues = new List<String>();
        for (String pjField : SCHOOL_PFS_ASSIGNMENT_PJ_AUDIT_FIELDS) {
            pjFieldValues.add(getLabelForSchoolPfsAssignmentField(pjField) + '=' + schoolPfsAssignment.get(pjField));
        }
        return String.join(pjFieldValues, ', ');
    }
    
    /* process log entry */
    private static String getUpdatedChangeLog(String oldChangeLog, LogEntry log, List<Attachment> logArchives, String archivePrefix, Id archiveParentId) {
        String newChangeLog = ((oldChangeLog != null) ? oldChangeLog : '') + NEWLINE + log;
        if (newChangeLog.length() > LOG_MAX_SIZE) {            
            Integer logSplitIndex = newChangeLog.lastIndexOf(NEWLINE, newChangeLog.length() - MIN_LOG_SIZE_AFTER_ARCHIVE - 1);
            if (logSplitIndex > 0) {
                String archiveLog = newChangeLog.left(logSplitIndex);
                Attachment attach = new Attachment();
                attach.Body = Blob.valueOf(archiveLog);
                attach.Name = archivePrefix + System.now().format('yyyyMMdd_HHmmss') + '.txt';
                attach.parentId = archiveParentId;
                if (logArchives != null) {
                   logArchives.add(attach);
                }
                newChangeLog = ARCHIVE_MESSAGE + newChangeLog.substring(logSplitIndex);
            }
        }
        return newChangeLog;
    }
    
    /* Overloaded methods for appending log entries to PFS, Applicant, and School PFS Assignment change logs */
    public static void appendLogEntry(PFS__c pfs, LogEntry log, List<Attachment> logArchives) {
        String newLog = getUpdatedChangeLog(pfs.Change_Log__c, log, logArchives, 'ChangeLog_', pfs.Id);
        pfs.Change_Log__c = newLog;    
    }
    
    public static void appendLogEntry(Applicant__c applicant, LogEntry log, List<Attachment> logArchives) {
        String newLog = getUpdatedChangeLog(applicant.Change_Log__c, log, logArchives, 'ChangeLog_', applicant.Id);
        applicant.Change_Log__c = newLog;     
    }
    
    public static void appendLogEntry(School_PFS_Assignment__c schoolPfsAssignment, LogEntry log, List<Attachment> logArchives) {
        String newLog = getUpdatedChangeLog(schoolPfsAssignment.Change_Log__c, log, logArchives, 'ChangeLog_', schoolPfsAssignment.Id);
        schoolPfsAssignment.Change_Log__c = newLog;   
    }
    
    /* appends log entries to School PFS Assignment PJ log */
    private static void appendPJLogEntry(School_PFS_Assignment__c schoolPfsAssignment, LogEntry pjLog, List<Attachment> logArchives) {
        String newLog = getUpdatedChangeLog(schoolPfsAssignment.Professional_Judgment_Log__c, pjLog, logArchives, 'ProfessionalJudgment_ChangeLog_', schoolPfsAssignment.Id);
        schoolPfsAssignment.Professional_Judgment_Log__c = newLog;  
    }
    
    /* Overloaded methods for checking and logging audited fields for PFS, Applicant, and School PFS Assignment */
    public static void logFieldChange(PFS__c newPfs, PFS__c oldPfs, List<Attachment> logArchives) {
        if (pfsFieldMap == null) {
            pfsFieldMap = Schema.SObjectType.PFS__c.fields.getMap();
        }
        LogEntry log = new LogEntry();
        for (String auditField : PFS_AUDIT_FIELDS) {
            if (newPfs.get(auditField) != oldPfs.get(auditField)) {
                String fieldLabel = getLabelForPfsField(auditField);
                log.addFieldChange(fieldLabel, String.valueOf(oldPfs.get(auditField)), String.valueOf(newPfs.get(auditField))); 
            }
        }
        if (log.hasLogInfo()) {
            appendLogEntry(newPfs, log, logArchives);
        }
    }
    
    public static void logFieldChange(Applicant__c newApplicant, Applicant__c oldApplicant, List<Attachment> logArchives) {
        if (applicantFieldMap == null) {
            applicantFieldMap = Schema.SObjectType.Applicant__c.fields.getMap();
        }
        LogEntry log = new LogEntry();
        for (String auditField : APPLICANT_AUDIT_FIELDS) {        
            if (newApplicant.get(auditField) != oldApplicant.get(auditField)) {
                String fieldLabel = getLabelForApplicantField(auditField);
                log.addFieldChange(fieldLabel, String.valueOf(oldApplicant.get(auditField)), String.valueOf(newApplicant.get(auditField))); 
            }
        }
        if (log.hasLogInfo()) {
            appendLogEntry(newApplicant, log, logArchives);
        }
    }
    
    public static void logFieldChange(School_PFS_Assignment__c newSchoolPfsAssignment, School_PFS_Assignment__c oldSchoolPfsAssignment, List<Attachment> logArchives) {
        if (schoolPfsAssignmentFieldMap == null) {
            schoolPfsAssignmentFieldMap = Schema.SObjectType.School_PFS_Assignment__c.fields.getMap();
        }
        LogEntry log = new LogEntry();
        for (String auditField : SCHOOL_PFS_ASSIGNMENT_AUDIT_FIELDS) {
            if (newSchoolPfsAssignment.get(auditField) != oldSchoolPfsAssignment.get(auditField)) {
                String fieldLabel = getLabelForSchoolPfsAssignmentField(auditField);
                log.addFieldChange(fieldLabel, String.valueOf(oldSchoolPfsAssignment.get(auditField)), String.valueOf(newSchoolPfsAssignment.get(auditField))); 
            }
        }
        LogEntry pjLog = new LogEntry();
        List<String> pjFieldsChanged = new List<String>();
        Integer pjChangedFieldCount = 0;
        for (String auditField : SCHOOL_PFS_ASSIGNMENT_PJ_AUDIT_FIELDS) {
            if (newSchoolPfsAssignment.get(auditField) != oldSchoolPfsAssignment.get(auditField)) {
                pjChangedFieldCount++; 
            }
        }
        if (pjChangedFieldCount > 0) {
            log.addLine(PJ_CHANGE_ALERT);
            pjLog.addLine(getPJSettingsString(newSchoolPfsAssignment));
        }
        if (log.hasLogInfo()) {
            appendLogEntry(newSchoolPfsAssignment, log, logArchives);
        }
        if (pjLog.hasLogInfo()) {
            appendPJLogEntry(newSchoolPfsAssignment, pjLog, logArchives);
        }
        
    }
    
}