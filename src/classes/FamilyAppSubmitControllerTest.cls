@isTest
private with sharing class FamilyAppSubmitControllerTest
{

    private static Annual_Setting__c currAnnualSettings;
    private static PFS__c testPfs;
    private static Applicant__c testApplicant;

    @isTest
    private static void testHelperMethods()
    {
        FamilyAppMainControllerTest.initData();

        insertFeatureToggleCustomSetting(false);

        FamilyAppSubmitController controller = new FamilyAppSubmitController();
        controller.pfs = FamilyAppMainControllerTest.pfsA;
        System.assertEquals(false, controller.areSubscribedSchools);

        controller.submitAndPay();
        testPFS = [SELECT Id, PFS_Status__c FROM PFS__c WHERE Id = :controller.pfs.Id];
        System.assertEquals('Application Submitted', testPFS.PFS_Status__c);
    }

    @isTest
    private static void testCurrentHelperConfig_ExpectNull()
    {
        FamilyAppSubmitController controller = new FamilyAppSubmitController();

        Test.startTest();
            System.assertEquals(null, controller.currentHelpConfig.Id,
                    'Expected the controller to fallback on an empty record without an Id.');
            List<Apexpages.Message> pageMessages = ApexPages.getMessages();
            System.assertEquals(0, pageMessages.size(), 'Expected no page messages.');
        Test.stopTest();
    }

    @isTest
    private static void testCurrentHelperConfig_ExpectValue()
    {
        Academic_Year__c currentYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        Help_Configuration__c testConfig = new Help_Configuration__c(
            Academic_Year__c = currentYear.Id,
            Language__c = UserInfo.getLanguage());
        insert testConfig;

        FamilyAppSubmitController controller = new FamilyAppSubmitController();

        Test.startTest();
            System.assertEquals(testConfig.Id, controller.currentHelpConfig.Id);
        Test.stopTest();
    }

    @isTest
    private static void testAutoWaiverAssignment()
    {
        FamilyAppMainControllerTest.initData();

        insertFeatureToggleCustomSetting(true);

        createCurrentAnnualSettings();

        FamilyAppMainControllerTest.School.Auto_Fee_Waiver__c = true;
        update FamilyAppMainControllerTest.School;

        createTestPFS();

        setTestApplicant();

        Test.startTest();
            FamilyAppSubmitController controller = new FamilyAppSubmitController();
            controller.pfs = testPFS;

            PageReference pr = controller.submitAndPay();

            assertApplicationSubmittedAndPaid(false);

            // verify the Auto Waiver was created
            Application_Fee_Waiver__c fw = [
                SELECT Id, Account__c, Status__c 
                FROM Application_Fee_Waiver__c 
                WHERE Contact__c = :FamilyAppMainControllerTest.pfsA.Parent_A__c];
            System.assert(fw != null);
            System.assert(fw.Account__c == FamilyAppMainControllerTest.School.Id);
            System.assertEquals('Assigned', fw.Status__c);

            assertOpportunityAndOppLineItemsCreated(1);
        Test.stopTest();
    }

    @isTest
    private static void testAutoWaiverAssignmentTurnedOff()
    {
        FamilyAppMainControllerTest.initData();

        insertFeatureToggleCustomSetting(false);

        createCurrentAnnualSettings();

        FamilyAppMainControllerTest.School.Auto_Fee_Waiver__c = true;
        update FamilyAppMainControllerTest.School;

        createTestPFS();

        setTestApplicant();

        Test.startTest();
            FamilyAppSubmitController controller = new FamilyAppSubmitController();
            controller.pfs = testPFS;

            PageReference pr = controller.submitAndPay();

            assertApplicationSubmittedAndPaid(false);

            // verify the Auto Waiver didn't assign waiver
            List<Application_Fee_Waiver__c> feeWaivers = [
                SELECT Id, Account__c, Status__c 
                FROM Application_Fee_Waiver__c 
                WHERE Contact__c = :FamilyAppMainControllerTest.pfsA.Parent_A__c];
            System.assert(feeWaivers.size() == 0);
        Test.stopTest();
    }

    /**
     *    PFS-1 Add test to check for a qualification of means based fee waiver [jB]
     */
    @isTest
    private static void testMBFWQualification()
    {
        insertFeatureToggleCustomSetting(false);

        FamilyAppMainControllerTest.initData();

        Account meansBasedFeeWaiverAccount = TestUtils.createAccount(
            'MBFW Account', RecordTypes.meansBasedFeeWaiverAccountTypeId, 5, false);
        insert new List<Account> { meansBasedFeeWaiverAccount };

        // Add MBFW settings
        createApplicationFeeSettings();

        createTestPFS();

        // Create Applicant
        setTestApplicant();

        // Insert FeeLunchGuidelines
        Free_Lunch_Guideline__c flG = new Free_Lunch_Guideline__c(
            Academic_Year__c = GlobalVariables.getCurrentAcademicYear().Id, Family_Size__c = 2.0, Total_Income__c = 20700.00);
        insert flG;

        Test.startTest();
            FamilyAppSubmitController controller = new FamilyAppSubmitController();
            controller.pfs = testPFS;

            PageReference pr = controller.submitAndPay();

            assertApplicationSubmittedAndPaid(true);

            // verify the AFW is created
            Application_Fee_Waiver__c fw = [
                SELECT Id, Account__c 
                FROM Application_Fee_Waiver__c 
                WHERE Contact__c = :FamilyAppMainControllerTest.pfsA.Parent_A__c];
            System.assert(fw != null);
            System.assert(fw.Account__c == meansBasedFeeWaiverAccount.Id);

            assertOpportunityAndOppLineItemsCreated(2);
        Test.stopTest();
    }

    @isTest
    private static void testMBFWWQualificationWithAutoTurnedOff()
    {
        FamilyAppMainControllerTest.initData();

        insertFeatureToggleCustomSetting(false);

        Account meansBasedFeeWaiverAccount = TestUtils.createAccount(
            'MBFW Account', RecordTypes.meansBasedFeeWaiverAccountTypeId, 5, false);
        insert new List<Account> { meansBasedFeeWaiverAccount };

        // Add MBFW settings
        createApplicationFeeSettings();

        createCurrentAnnualSettings();

        FamilyAppMainControllerTest.School.Auto_Fee_Waiver__c = true;
        update FamilyAppMainControllerTest.School;

        createTestPFS();

        // Create Applicant
        setTestApplicant();

        // Insert FeeLunchGuidelines
        Free_Lunch_Guideline__c flG = new Free_Lunch_Guideline__c(
            Academic_Year__c = GlobalVariables.getCurrentAcademicYear().Id, Family_Size__c = 2.0, Total_Income__c = 20700.00);
        insert flG;

        Test.startTest();
            FamilyAppSubmitController controller = new FamilyAppSubmitController();
            controller.pfs = testPFS;

            PageReference pr = controller.submitAndPay();

            assertApplicationSubmittedAndPaid(true);

            // verify the AFW is created
            Application_Fee_Waiver__c fw = [
                SELECT Id, Account__c 
                FROM Application_Fee_Waiver__c 
                WHERE Contact__c = :FamilyAppMainControllerTest.pfsA.Parent_A__c];
            System.assert(fw != null);
            System.assert(fw.Account__c == meansBasedFeeWaiverAccount.Id);

            assertOpportunityAndOppLineItemsCreated(2);
        Test.stopTest();
    }

    /**
     *    PFS-190 Test qualified pending fee waivers are updated to existing for fee waivers when MBFW
     *            is applied [jB]
     */
    @isTest
    private static void testExistingChosenWhileMBFWQualified()
    {
        FamilyAppMainControllerTest.initData();

        insertFeatureToggleCustomSetting(false);

        createCurrentAnnualSettings();

        Account meansBasedFeeWaiverAccount = TestUtils.createAccount(
            'MBFW Account', RecordTypes.meansBasedFeeWaiverAccountTypeId, 5, false);
        insert new List<Account> { meansBasedFeeWaiverAccount };

        // Add MBFW settings
        createApplicationFeeSettings();

        createTestPFS();

        // Create Applicant
        setTestApplicant();

        // create an existing fee waiver
        Application_Fee_Waiver__c afw = TestUtils.createApplicationFeeWaiver(
            FamilyAppMainControllerTest.school.id,
            FamilyAppMainControllerTest.pfsA.Parent_A__c,
            GlobalVariables.getCurrentAcademicYear().Id,
            false);

        afw.Status__c = 'Pending Qualification';
        insert afw;

        // balance shouldn't be deducted
        System.assertEquals(2, [
            SELECT Waiver_Credit_Balance_Formula__c, Waivers_Assigned__c 
            FROM Annual_Setting__c 
            WHERE Id = :currAnnualSettings.Id].Waiver_Credit_Balance_Formula__c);
        System.assertEquals(1, [
            SELECT Waiver_Credit_Balance_Formula__c, Waivers_Assigned__c 
            FROM Annual_Setting__c 
            WHERE Id = :currAnnualSettings.Id].Waivers_Assigned__c);
        System.assertEquals(2, [
            SELECT Current_Year_Waiver_Balance__c 
            FROM Account 
            WHERE Id = :FamilyAppMainControllerTest.school.id].Current_Year_Waiver_Balance__c);

        // Insert FeeLunchGuidelines
        Free_Lunch_Guideline__c flG = new Free_Lunch_Guideline__c(Academic_Year__c = GlobalVariables.getCurrentAcademicYear().Id, Family_Size__c = 2.0, Total_Income__c = 20700.00);
        insert flG;

        Test.startTest();
            FamilyAppSubmitController controller = new FamilyAppSubmitController();
            controller.pfs = testPFS;

            PageReference pr = controller.submitAndPay();

            System.assertEquals('Declined', [SELECT Status__c FROM Application_Fee_Waiver__c WHERE Id = :afw.Id].Status__c);
            assertApplicationSubmittedAndPaid(true);

            // verify the AFWs are created
            List<Application_Fee_Waiver__c> afws = [
                SELECT Id, Account__c 
                FROM Application_Fee_Waiver__c 
                WHERE Contact__c = :FamilyAppMainControllerTest.pfsA.Parent_A__c];
            System.assert(true, !afws.isEmpty());
            System.assertEquals(2, afws.size());

            // balance should be restored
            System.assertEquals(3, [
                SELECT Current_Year_Waiver_Balance__c 
                FROM Account 
                WHERE Id = :FamilyAppMainControllerTest.school.id].Current_Year_Waiver_Balance__c);
            System.assertEquals(3, [
                SELECT Waiver_Credit_Balance_Formula__c, Waivers_Assigned__c 
                FROM Annual_Setting__c 
                WHERE Id = :currAnnualSettings.Id].Waiver_Credit_Balance_Formula__c);
            System.assertEquals(null, [
                SELECT Waiver_Credit_Balance_Formula__c, Waivers_Assigned__c 
                FROM Annual_Setting__c 
                WHERE Id = :currAnnualSettings.Id].Waivers_Assigned__c);


            assertOpportunityAndOppLineItemsCreated(2);
        Test.stopTest();
    }

    /**
     *    PFS-190 Test qualified pending fee waivers are updated to existing for fee waivers when MBFW
     *            is applied [jB]
     */
    @isTest
    private static void testApplyExistingWaiverApplicantDoesntQualify()
    {
        FamilyAppMainControllerTest.initData();

        insertFeatureToggleCustomSetting(false);

        createCurrentAnnualSettings();

        Account meansBasedFeeWaiverAccount = TestUtils.createAccount(
            'MBFW Account', RecordTypes.meansBasedFeeWaiverAccountTypeId, 5, false);
        insert new List<Account> { meansBasedFeeWaiverAccount };

        // Add MBFW settings
        createApplicationFeeSettings();
        createTestPFS();

        // Create Applicant
        setTestApplicant();

        // create an existing fee waiver
        Application_Fee_Waiver__c afw = TestUtils.createApplicationFeeWaiver(
            FamilyAppMainControllerTest.school.id,
            FamilyAppMainControllerTest.pfsA.Parent_A__c,
            GlobalVariables.getCurrentAcademicYear().Id,
            false);

        afw.Status__c = 'Pending Qualification';
        insert afw;

        // balance should be deducted
        System.assertEquals(2, [
            SELECT Waiver_Credit_Balance_Formula__c, Waivers_Assigned__c 
            FROM Annual_Setting__c 
            WHERE Id = :currAnnualSettings.Id].Waiver_Credit_Balance_Formula__c);
        System.assertEquals(1, [
            SELECT Waiver_Credit_Balance_Formula__c, Waivers_Assigned__c 
            FROM Annual_Setting__c 
            WHERE Id = :currAnnualSettings.Id].Waivers_Assigned__c);

        // Insert FeeLunchGuidelines
        Free_Lunch_Guideline__c flG = new Free_Lunch_Guideline__c(Academic_Year__c = GlobalVariables.getCurrentAcademicYear().Id, Family_Size__c = 2.0, Total_Income__c = 20700.00);
        insert flG;

        Test.startTest();
            FamilyAppSubmitController controller = new FamilyAppSubmitController();
            controller.pfs = testPFS;

            PageReference pr = controller.submitAndPay();

            // should have updated AFW status to 'declined' as they qualified for a MBFW
            System.assertEquals('Declined', [SELECT Status__c FROM Application_Fee_Waiver__c WHERE Id = :afw.Id].Status__c);

            assertApplicationSubmittedAndPaid(true);

            // verify the AFWs are created
            List<Application_Fee_Waiver__c> afws = [
                SELECT Id, Account__c
                FROM Application_Fee_Waiver__c
                WHERE Contact__c = :FamilyAppMainControllerTest.pfsA.Parent_A__c];
            System.assert(true, !afws.isEmpty());
            System.assertEquals(2, afws.size());
            System.assertEquals(afw.Id, afws.get(0).id);

            assertOpportunityAndOppLineItemsCreated(2);
        Test.stopTest();
    }

    @isTest
    private static void testExceptions() 
    {
        // Create an Account with an active SSS subscriber status
        Account testAccount = TestUtils.createAccount('Test Account', RecordTypes.individualAccountTypeId, 5, false);
        testAccount.SSS_Subscriber_Status__c = 'Current';
        insert testAccount;

        SchoolPortalSettings.KS_School_Account_Id = testAccount.Id;

        Contact testParentA = TestUtils.createContact('Parent A', testAccount.Id, RecordTypes.parentContactTypeId, true);
        Contact testAppContact = TestUtils.createContact('Applicant 1', testAccount.Id, RecordTypes.studentContactTypeId, true);

        // Create an Academic Year
        Academic_Year__c testAcademicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        // Create Annual Settings with a deadline type of no fixed,
        //  and dates that are not in the future
        Annual_Setting__c testAnnualSetting = TestUtils.createAnnualSetting(testAccount.Id, testAcademicYear.Id, false);
        testAnnualSetting.PFS_Deadline_Type__c = 'No Fixed Deadlines';
        testAnnualSetting.PFS_New__c = Date.today().addDays(-2);
        testAnnualSetting.PFS_Returning__c = Date.today().addDays(-2);
        insert testAnnualSetting;

        // Create PFS
        testPFS = TestUtils.createPFS('Test PFS', testAcademicYear.Id, testParentA.Id, true);

        // Create Applicant
        testApplicant = TestUtils.createApplicant(testAppContact.Id, testPFS.Id, true);

        // Create Applicant
        Student_Folder__c testStudentFolder = TestUtils.createStudentFolder(
            'Test Folder', testAcademicYear.Id, testAppContact.Id, true);

        // Create SPA Record
        School_PFS_Assignment__c testSPA = TestUtils.createSchoolPFSAssignment(
            testAcademicYear.Id, testApplicant.Id, testAccount.Id, testStudentFolder.Id, true);

        Test.startTest();
            testPFS = ApplicationUtils.queryPFSRecord(testPFS.Id);
            ApplicationUtils appUtils = new ApplicationUtils(UserInfo.getLanguage(), testPFS, 'PFSSubmit');
            // Instantiate the controller with the PFS record
            FamilyAppSubmitController controller = new FamilyAppSubmitController();
            controller.pfs = testPFS;
            controller.appUtils = appUtils;
            // ** Verify that neither of the error conditions are met
            System.assertEquals(true, controller.areSubscribedSchools);
            System.assertEquals(true, controller.areOpenDeadlines);

            // Update the SSS subscriber status to non-active
            testAccount.SSS_Subscriber_Status__c = 'Former';
            update testAccount;

            // Reload PFS and App Utils
            testPFS = ApplicationUtils.queryPFSRecord(testPFS.Id);
            appUtils = new ApplicationUtils(UserInfo.getLanguage(), testPFS, 'PFSSubmit');
            controller = new FamilyAppSubmitController();
            controller.pfs = testPFS;
            controller.appUtils = appUtils;
            // ** Verify that the subscriber error condition is met
            // System.assertEquals(controller.areSubscribedSchools, false);
            System.assertEquals(controller.areOpenDeadlines, true);

            // Update the SSS subscriber status back to active
            testAccount.SSS_Subscriber_Status__c = 'Current';
            update testAccount;
            // Set the SPA record status to withdrawn
            testSPA.Withdrawn__c = 'Yes';
            update testSPA;
            // Reload PFS and App Utils
            testPFS = ApplicationUtils.queryPFSRecord(testPFS.Id);
            appUtils = new ApplicationUtils(UserInfo.getLanguage(), testPFS, 'PFSSubmit');
            controller = new FamilyAppSubmitController();
            controller.pfs = testPFS;
            controller.appUtils = appUtils;
            // ** Verify that the available error condition is met
            // System.assertEquals(controller.areSubscribedSchools, false);
            System.assertEquals(controller.areOpenDeadlines, false);

            // Update the SPA record status back to not withdrawn
            testSPA.Withdrawn__c = 'No';
            update testSPA;
            // Set the deadline type on the Annual Settings to a soft deadline
            testAnnualSetting.PFS_Deadline_Type__c = 'Soft Deadline';
            update testAnnualSetting;
            // Reload PFS and App Uils
            testPFS = ApplicationUtils.queryPFSRecord(testPFS.Id);
            appUtils = new ApplicationUtils(UserInfo.getLanguage(), testPFS, 'PFSSubmit');
            controller = new FamilyAppSubmitController();
            controller.pfs = testPFS;
            controller.appUtils = appUtils;
            // ** Verify that neither error condition is met
            System.assertEquals(controller.areSubscribedSchools, true);
            System.assertEquals(controller.areOpenDeadlines, true);

            // Set the deadline type on the Annual Settings to a hard deadline
            testAnnualSetting.PFS_Deadline_Type__c = 'Hard Deadline';
            update testAnnualSetting;
            // Reload PFS and App Utils
            testPFS = ApplicationUtils.queryPFSRecord(testPFS.Id);
            appUtils = new ApplicationUtils(UserInfo.getLanguage(), testPFS, 'PFSSubmit');
            controller = new FamilyAppSubmitController();
            controller.pfs = testPFS;
            controller.appUtils = appUtils;
            // ** Verify that the deadline error condition is met
            System.assertEquals(controller.areSubscribedSchools, true);
            System.assertEquals(controller.areOpenDeadlines, false);

            // Set the deadline dates to be in the future
            testAnnualSetting.PFS_New__c = Date.today().addDays(2);
            testAnnualSetting.PFS_Returning__c = Date.today().addDays(2);
            update testAnnualSetting;
            // Reload PFS and App Utils
            testPFS = ApplicationUtils.queryPFSRecord(testPFS.Id);
            appUtils = new ApplicationUtils(UserInfo.getLanguage(), testPFS, 'PFSSubmit');
            controller = new FamilyAppSubmitController();
            controller.pfs = testPFS;
            controller.appUtils = appUtils;
            // ** Verify that neither error condition is met
            System.assertEquals(controller.areSubscribedSchools, true);
            System.assertEquals(controller.areOpenDeadlines, true);
        Test.stopTest();
    }

    @isTest
    private static void testCancel()
    {
        FamilyAppSubmitController controller = new FamilyAppSubmitController();

        Test.startTest();
            System.assertEquals(Page.FamilyDashboard.getUrl(), controller.cancel().getUrl());
        Test.stopTest();

    }

    @isTest
    private static void testAreSchoolSetupComplete()
    {
        Academic_Year__c currentYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        Pfs__c pfs1 = PfsTestData.Instance.forAcademicYearPicklist(currentYear.Name).DefaultPfs;
        FamilyAppSubmitController controller = new FamilyAppSubmitController();
        controller.pfs = pfs1;

        Test.startTest();
            System.assertEquals(true, controller.areSchoolSetupComplete);
            System.assertEquals(true, controller.areSchoolSetupComplete);
        Test.stopTest();

    }

    @isTest
    private static void testSubmitAndPayException()
    {
        FamilyAppSubmitController controller = new FamilyAppSubmitController();
        
        Test.startTest();
            try
            {
                controller.submitAndPay();
            } catch(Exception ex) {
                List<Apexpages.Message> pageMessages = ApexPages.getMessages();
                System.assertEquals(1, pageMessages.size());
                System.assert(pageMessages[0].getDetail().contains(Label.PfsSubmitErrorGeneral));
            }
        Test.stopTest();
    }

    private static void insertFeatureToggleCustomSetting(Boolean autoFee)
    {
        FeatureToggles.setToggle('Auto_Fee_Waiver__c', autoFee);
    }

    private static void createTestPFS()
    {
        // Create PFS
        testPFS = FamilyAppMainControllerTest.pfsA;
        testPFS.Own_Business_or_Farm__c = 'No';
        testPFS.Family_Size__c = 2;
        // these values contribute to Income & Assets in MBFW testing
        testPFS.Salary_Wages_Parent_A__c = 20000;
        testPFS.Bank_Account_Value__c = 1000;
        testPFS.Parent_A_Country__c = 'United States';

        update testPFS;
    }

    private static void assertApplicationSubmittedAndPaid(boolean paid)
    {
        testPFS = ApplicationUtils.queryPFSRecord(testPFS.Id);
        System.assertEquals('Application Submitted', testPFS.PFS_Status__c);
        if (paid)
        {
            System.assertEquals('Paid in Full', testPFS.Payment_Status__c);
        } else {
            System.assertEquals('Unpaid', testPFS.Payment_Status__c);

        }
    }

    private static void assertOpportunityAndOppLineItemsCreated(Integer lineCount)
    {
        // Verify Opp and TLI were created
        Opportunity op = [
            SELECT Id, Parent_A__c, PFS__c, Fee_Waived__c, (SELECT Id FROM Transaction_Line_Items__r) 
            FROM Opportunity 
            WHERE PFS__c = :testPFS.Id];

        System.assert(op != null);
        System.assert(op.Parent_A__c == FamilyAppMainControllerTest.pfsA.Parent_A__c);
        System.assert(op.Transaction_Line_Items__r.size() == lineCount);
    }

    private static void createCurrentAnnualSettings()
    {
        currAnnualSettings = [
            SELECT Academic_Year__c, School__c, School__r.Current_Year_Waiver_Balance__c,
                School__r.Current_Year_Waivers_Assigned__c, Total_Waivers_Override_Default__c, Waivers_Assigned__c
            FROM Annual_Setting__c
            WHERE School__c = :FamilyAppMainControllerTest.school.id];

        currAnnualSettings.Total_Waivers_Override_Default__c = 3;
        currAnnualSettings.Waivers_Assigned__c = 0;

        update currAnnualSettings;
    }

    private static void setTestApplicant()
    {
        testApplicant = FamilyAppMainControllerTest.applicant1A;
    }

    private static void createApplicationFeeSettings()
    {
        Application_Fee_Settings__c afs = new Application_Fee_Settings__c(
            Name = GlobalVariables.getCurrentYearString(),
            Amount__c = 42,
            Discounted_Amount__c = 42,
            Discounted_Product_Code__c = '131-200 PFS Fee - KS - 2015',
            Product_Code__c = '131-100 PFS Fee - 2015',
            Means_Based_Fee_Waiver_Record_Type_Name__c
                    = ApplicationFeeSettingsService.MEANS_BASED_FEE_WAIVER_RECORDTYPE,
            Maximum_Assets_Waiver__c = 100000);
        insert afs;
    }
}