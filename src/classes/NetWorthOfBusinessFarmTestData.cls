/**
 * @description This class is used to create Net Worth Of Business Farm records for unit tests.
 */
@isTest
public class NetWorthOfBusinessFarmTestData extends SObjectTestData {
    @testVisible private static final Decimal NET_WORTH_OF_BUSINESS_LOW = 20;
    @testVisible private static final Decimal EXPECTED_CONTRIBUTION_BASE = 100;
    @testVisible private static final Decimal EXPECTED_CONTRIBUTION_RATE = 1.5;
    @testVisible private static final Decimal THRESHOLD_TO_APPLY_RATE = 50;

    /**
     * @description Get the default values for the Net_Worth_of_Business_Farm__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Net_Worth_of_Business_Farm__c.Academic_Year__c => AcademicYearTestData.Instance.DefaultAcademicYear.Id,
                Net_Worth_of_Business_Farm__c.Net_Worth_of_Business_Low__c => NET_WORTH_OF_BUSINESS_LOW,
                Net_Worth_of_Business_Farm__c.Expected_Contribution_Base__c => EXPECTED_CONTRIBUTION_BASE,
                Net_Worth_of_Business_Farm__c.Expected_Contribution_Rate__c => EXPECTED_CONTRIBUTION_RATE,
                Net_Worth_of_Business_Farm__c.Threshold_to_Apply_Rate__c => THRESHOLD_TO_APPLY_RATE
        };
    }

    /**
     * @description Set the Academic Year on the current Net Worth of Business Farm record.
     * @param academicYearId The AcademicYear to set on the current Net Worth of Business
     *             Farm record.
     * @return The current working instance of NetWorthOfBusinessFarmTestData.
     */
    public NetWorthOfBusinessFarmTestData forAcademicYearId(Id academicYearId) {
        return (NetWorthOfBusinessFarmTestData) with(Net_Worth_of_Business_Farm__c.Academic_Year__c, academicYearId);
    }

    /**
     * @description Insert the current working Net_Worth_of_Business_Farm__c record.
     * @return The currently operated upon Net_Worth_of_Business_Farm__c record.
     */
    public Net_Worth_of_Business_Farm__c insertNetWorthOfBusinessFarm() {
        return (Net_Worth_of_Business_Farm__c)insertRecord();
    }

    /**
     * @description Create the current working Net Worth Of Business Farm record without resetting
     *             the stored values in this instance of NetWorthOfBusinessFarmTestData.
     * @return A non-inserted Net_Worth_of_Business_Farm__c record using the currently stored field
     *             values.
     */
    public Net_Worth_of_Business_Farm__c createNetWorthOfBusinessFarmWithoutReset() {
        return (Net_Worth_of_Business_Farm__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Net_Worth_of_Business_Farm__c record.
     * @return The currently operated upon Net_Worth_of_Business_Farm__c record.
     */
    public Net_Worth_of_Business_Farm__c create() {
        return (Net_Worth_of_Business_Farm__c)super.buildWithReset();
    }

    /**
     * @description The default Net_Worth_of_Business_Farm__c record.
     */
    public Net_Worth_of_Business_Farm__c DefaultNetWorthOfBusinessFarm {
        get {
            if (DefaultNetWorthOfBusinessFarm == null) {
                DefaultNetWorthOfBusinessFarm = createNetWorthOfBusinessFarmWithoutReset();
                insert DefaultNetWorthOfBusinessFarm;
            }
            return DefaultNetWorthOfBusinessFarm;
        }
        private set;
    }

    /**
     * @description Get the Net_Worth_of_Business_Farm__c SObjectType.
     * @return The Net_Worth_of_Business_Farm__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Net_Worth_of_Business_Farm__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static NetWorthOfBusinessFarmTestData Instance {
        get {
            if (Instance == null) {
                Instance = new NetWorthOfBusinessFarmTestData();
            }
            return Instance;
        }
        private set;
    }

    private NetWorthOfBusinessFarmTestData() { }
}