/*
 * Spec-066 School Profile - User Profile; R-424, R-426, R-428
 * BR, Exponent Partners, 2013
 */
        
public with sharing class SchoolUserProfileController {
    
    public User myUser { get; set; }

    /*Initialization*/
    
    public SchoolUserProfileController(ApexPages.StandardController controller) {
            
        // Get the contactId from the passed in 'id', if that's null, then get it from the current user.
        Id contactId = System.currentPageReference().getParameters().get('id');
        if (contactId == null)
        {
            contactId = GlobalVariables.getCurrentUser().ContactId;
        }
        
        if (contactId != null)
        {
            myContact = [SELECT Id, Salutation, FirstName, LastName, Suffix__c, Title, Email, Phone, PFS_Alert_Preferences__c
                            FROM Contact
                            WHERE Id = :contactId];
        }
        
        myUser = [SELECT Id,User.TimeZoneSidKey FROM User WHERE User.ContactId =: contactId];
        
    }

    /*Properties*/
    public Contact myContact { get; set; }

    public PageReference editx() {
        return Page.SchoolUserProfileEdit;
    }

    /*Action Methods*/
    public PageReference save() {
        update myContact;
        update myUser;
        return Page.SchoolUserProfile;
    }

    public PageReference cancel() {
        return Page.SchoolUserProfile;
    }

    /*Helper Methods*/
}