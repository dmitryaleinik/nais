public virtual class EfcCalculatorBase {
    List<EfcCalculationSteps.Step> efcCalculationSteps = new List<EfcCalculationSteps.Step>();
    
    public EfcCalculatorBase() {
        initializeSteps();
    }
    
    public void addStep(EfcCalculationSteps.Step efcCalculationStep) {
        this.efcCalculationSteps.add(efcCalculationStep);
    }
    
    public virtual void initializeSteps() {}
    
    public void calculateEfc(EfcWorksheetData efcData) {
        if (efcCalculationSteps != null) {
            // [CH] Adding Exception Handling to prevent PFS records sitting with
            //         a status of Recalculate
            Set<Id> pfsIdsWithErrors = new Set<Id>{};
            
            for (EfcCalculationSteps.Step calculationStep : efcCalculationSteps) {
                try {
                    calculationStep.doCalculation(efcData);
                }
                catch(Exception e) {
                    System.debug(LoggingLevel.Error, 'EfcCalculatorBase Worksheet Processing Error: ' + e);
                    efcData.processingErrorText = 'EfcCalculatorBase Worksheet Processing Error: ' + e; // NAIS-2371 [CH] Capture the error message
                     efcData.processingError = true;
                }
            }
        }
    }
    
    public void calculateEfc(List<EfcWorksheetData> efcDataList) {
        if ((efcDataList != null) && (!efcDataList.isEmpty())) {
            if (efcCalculationSteps != null) {
                // [CH] Adding Exception Handling to prevent PFS records sitting with
                //         a status of Recalculate
                Set<Id> pfsIdsWithErrors = new Set<Id>{};
                
                for (EfcCalculationSteps.Step calculationStep : efcCalculationSteps) {
                    /* [CH] Adjusting the place that the list is iterated through
                        in order to catch exceptions on a record-by-record basis
                        calculationStep.doCalculation(efcDataList);
                    */
                    for(EfcWorksheetData efcData : efcDataList){
                        try{
                            calculationStep.doCalculation(efcData);
                        }
                        catch(Exception e) {
                            System.debug(LoggingLevel.Error, 'EfcCalculatorBase Worksheet List Processing Error: ' + e.getMessage()  + ': ' +  e.getStackTraceString());
                            efcData.processingErrorText = 'EfcCalculatorBase Worksheet List Processing Error: ' + e.getMessage()  + ': ' +  e.getStackTraceString(); // NAIS-2371 [CH] Capture the error message
                            efcData.processingError = true;
                        }
                    }
                }
            }
        }
    }
}