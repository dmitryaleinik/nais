/*
 * SPEC-121
 *
 * The school should be able to see all answers provided by the families on the main application screens
 * This is the Household Information link under the PFS as well as the Additional Information link under the PFS.
 * The idea is to show all fields which have not fed into the EFC, but are asked on the PFS so that the school
 * can see everything without needing to go into the printable PFS to see the information.
 *
 * Nathan, Exponent Partners, 2013
*/
@isTest
private class SchoolHouseholdMemberControllerTest
{

    private class TestData
    {
        Account school1;
        Contact staff1, parentA, parentB, student1, student2;
        String academicYearId;
        PFS__c pfs1;
        Applicant__c applicant1A;
        Student_Folder__c studentFolder1;
        School_PFS_Assignment__c spfsa1;
        //Dependents__c dependents1;

        private TestData() {
            // school
            school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, null, false);
            insert school1;

            // school staff
            staff1 = TestUtils.createContact('Staff 1', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
            insert staff1;

            // parents and students
            parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
            parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
            student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
            student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
            insert new List<Contact> {parentA, parentB, student1, student2};

            // academic year
            TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
            academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

            // psf
            pfs1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
            //pfs1.Parent_B__c = parentB.Id;
            //pfs1.Addl_Parent_Last_Name__c = 'test additional parent';
            insert new List<PFS__c> {pfs1};

            // applicant
            applicant1A = TestUtils.createApplicant(student1.Id, pfs1.Id, false);
            insert new List<Applicant__c> {applicant1A};

            // student folder
            studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
            insert studentFolder1;

            // school pfs assignment
            spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder1.Id, false);
            insert spfsa1;
        }
    }

    private static Dependents__c createDependents(PFS__c pfs1) {
        // dependents
        Dependents__c dependents1;
        dependents1 = new Dependents__c(PFS__c = pfs1.Id,
                                        Dependent_1_School_Next_Year__c = 'schoolnext',
                                        Dependent_1_Gender__c = 'female',
                                        Dependent_1_Full_Name__c = 'full name1',
                                        Dependent_1_Current_School__c = 'sacajawea',
                                        Dependent_1_Current_Grade__c = '5',
                                        Dependent_1_Birthdate__c = System.today(),
                                        Dependent_2_School_Next_Year__c = 'schoolnext',
                                        Dependent_2_Gender__c = 'female',
                                        Dependent_2_Full_Name__c = 'full name2',
                                        Dependent_2_Current_School__c = 'sacajawea',
                                        Dependent_2_Current_Grade__c = '5',
                                        Dependent_2_Birthdate__c = System.today());
        insert dependents1;
        return dependents1;
    }

    @isTest
    private static void testAddParentB() {
        TestData td = new TestData();

        Test.startTest();
        PageReference pageRef = Page.SchoolHouseholdMember;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        pageRef.getParameters().put('mode', 'New');
        Test.setCurrentPage(pageRef);
        SchoolHouseholdMemberController controller = new SchoolHouseholdMemberController();

        // add parent B. this will create a contact record and assign it as parent B in pfs
        controller.selectedFamilyMemberType = 'PARENT_B';
        controller.memberContact.LastName = 'TestParentB';
        controller.saveMemberContact();

        // verify that parent B is assigned
        system.assertEquals(controller.memberContact.Id, [select Parent_B__c from PFS__c where Id = :td.pfs1.Id].Parent_B__c);

        Test.stopTest();
    }

    /*----------- Commenting. Sibling cannot be added by school staff ---------------------
    @isTest
    private static void testAddSibling() {
        TestData td = new TestData();

        Test.startTest();
        PageReference pageRef = Page.SchoolHouseholdMember;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        pageRef.getParameters().put('mode', 'New');
        Test.setCurrentPage(pageRef);
        SchoolHouseholdMemberController controller = new SchoolHouseholdMemberController();

        // add parent B. this will create a contact record and assign it as parent B in pfs
        controller.selectedFamilyMemberType = 'SIBLING';
        controller.memberContact.LastName = 'TestSibling';
        controller.saveMemberContact();

        // verify sibling applicant is created
        system.assertEquals('TestSibling', [select Contact__r.LastName
            from Applicant__c
                where PFS__C = :td.pfs1.Id and Contact__r.LastName = 'TestSibling'
                limit 1].Contact__r.LastName);

        Test.stopTest();
    }
    ----------------------------------------------------------------------------------------*/

    @isTest
    private static void testEditStudent() {
        TestData td = new TestData();

        Test.startTest();
        PageReference pageRef = Page.SchoolHouseholdMember;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        pageRef.getParameters().put('householdMemberId', td.student1.Id);
        pageRef.getParameters().put('mode', 'Edit');
        pageRef.getParameters().put('relationship', 'Student');
        Test.setCurrentPage(pageRef);
        SchoolHouseholdMemberController controller = new SchoolHouseholdMemberController();

        controller.selectedFamilyMemberType = 'STUDENT';
        controller.memberContact.Email = 'xyz@abc.com';
        controller.saveMemberContact();

        // verify that email changed
        system.assertEquals('xyz@abc.com', [select Email from Contact where Id = :td.student1.Id].Email);

        Test.stopTest();
    }

    @isTest
    private static void testCreateDependent() {
        TestData td = new TestData();
        
        insert new Dependents__c(PFS__c=td.pfs1.Id, Number_of_Dependents__c=0);

        Test.startTest();
        PageReference pageRef = Page.SchoolHouseholdMember;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        pageRef.getParameters().put('mode', 'New');
        Test.setCurrentPage(pageRef);
        SchoolHouseholdMemberController controller = new SchoolHouseholdMemberController();
        controller.selectedFamilyMemberType = 'Dependent';
        controller.dependents.Dependent_1_Full_Name__c = 'TestDependent';
        controller.saveDependent();
        // verify that dependent is created
        // No Dependent record exists yet so this one will be created in 1st field
        Dependents__c dep = [select Dependent_1_Full_Name__c, Dependent_2_Full_Name__c, Dependent_3_Full_Name__c from Dependents__c];
        system.assertEquals('TestDependent', [select Dependent_1_Full_Name__c from Dependents__c].Dependent_1_Full_Name__c);

        Test.stopTest();
    }

    @isTest
    private static void testAddDependent() {
        TestData td = new TestData();

        Test.startTest();
        PageReference pageRef = Page.SchoolHouseholdMember;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        pageRef.getParameters().put('mode', 'New');
        Test.setCurrentPage(pageRef);
        SchoolHouseholdMemberController controller = new SchoolHouseholdMemberController();

        Dependents__c dep = createDependents(td.pfs1);

        controller.selectedFamilyMemberType = 'Dependent';
        controller.dependents.Dependent_1_Full_Name__c = 'TestDependent';
        controller.saveDependent();

        // verify that dependent is created
        // we have already 2 dependents. this one will be created in 3rd field
        system.assertEquals('TestDependent', [select Dependent_3_Full_Name__c from Dependents__c where Id = :dep.Id].Dependent_3_Full_Name__c);

        Test.stopTest();
    }

    @isTest
    private static void testEditDependent() {
        TestData td = new TestData();

        Dependents__c dep = createDependents(td.pfs1);

        Test.startTest();
        PageReference pageRef = Page.SchoolHouseholdMember;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        pageRef.getParameters().put('dependentsId', dep.Id);
        pageRef.getParameters().put('dependentNumber', '2');    // edit 2nd dependent
        pageRef.getParameters().put('mode', 'Edit');
        pageRef.getParameters().put('relationship', 'Dependent');
        Test.setCurrentPage(pageRef);
        SchoolHouseholdMemberController controller = new SchoolHouseholdMemberController();

        controller.selectedFamilyMemberType = 'DEPENDENT';
        controller.dependents.Dependent_1_Full_Name__c = 'TestDependent';
        controller.saveDependent();

        // verify that dependent name changed
        system.assertEquals('TestDependent', [select Dependent_2_Full_Name__c from Dependents__c where Id = :dep.Id].Dependent_2_Full_Name__c);

        Test.stopTest();
    }

    @isTest
    private static void testAddAdditionalParent() {
        TestData td = new TestData();

        Test.startTest();
        PageReference pageRef = Page.SchoolHouseholdMember;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        pageRef.getParameters().put('mode', 'New');
        Test.setCurrentPage(pageRef);
        SchoolHouseholdMemberController controller = new SchoolHouseholdMemberController();

        controller.selectedFamilyMemberType = 'ADDITIONAL_PARENT';
        controller.pfs.Addl_Parent_Last_Name__c = 'TestAdditionalParent';
        controller.saveAdditionalParent();

        // verify that additional parent name changed
        system.assertEquals('TestAdditionalParent', [select Addl_Parent_Last_Name__c from PFS__c where Id = :td.pfs1.Id].Addl_Parent_Last_Name__c);

        Test.stopTest();
    }

    @isTest
    private static void testCancelMethods()
    {
        TestData td = new TestData();

        Test.startTest();
        PageReference pageRef = Page.SchoolHouseholdMember;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        pageRef.getParameters().put('mode', 'New');
        Test.setCurrentPage(pageRef);
        SchoolHouseholdMemberController controller = new SchoolHouseholdMemberController();

        // nothing to verify. simply calling to increase test coverage
        controller.selectedFamilyMemberType = 'ADDITIONAL_PARENT';
        controller.cancelAdditionalParent();
        controller.selectedFamilyMemberType = 'DEPENDENT';
        controller.cancelDependent();
        controller.selectedFamilyMemberType = 'STUDENT';
        controller.cancelMemberContact();

        controller.cancelFamilyMemberType();

        Test.stopTest();
    }
}