/**
 * @description Custom Exception for payments.
 **/
public class PaymentException extends Exception { }