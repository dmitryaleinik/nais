/* 
 * Spec-037 School Portal - Fee Waivers; R-082, R-083
 * BR, Exponent Partners, 2013
 */
       
public with sharing class SchoolFeeWaiverDeclineController {

    private Map<Id, Account> currentSchoolsForUserMap {get; set;}
    public Account currentSchool {get; set;}

    /* Initialization */
    
    public SchoolFeeWaiverDeclineController() {

        Id afwId = ApexPages.currentPage().getParameters().get('id');
        
        /* [CH] NAIS-1372 - Refactored to work on multiple AFW records */
        selectedContactId = ApexPages.currentPage().getParameters().get('contactId');
        mySchoolId = ApexPages.currentPage().getParameters().get('schoolId');
        myAcademicYearId = ApexPages.currentPage().getParameters().get('academicYearId');
        
        currentSchoolsForUserMap = new Map<Id, Account>(GlobalVariables.getAllSchoolsForUser());
        currentSchool = currentSchoolsForUserMap.get(GlobalVariables.getCurrentSchoolId());

        // [DP] 02.02.2015 can't find where this code is ever accessed - I don't think that we ever decline multiple waivers at once. commenting out
        //if (selectedContactId != null && mySchoolId != null && myAcademicYearId != null) {
        //    afwRecords = [SELECT Decline_Message_to_Parent__c, Contact__r.Name,  SSS_Contact_Email__c, Status__c 
        //            FROM Application_Fee_Waiver__c 
        //            WHERE Contact__c = :selectedContactId 
        //                AND Account__c = :mySchoolId
        //                AND Academic_Year__c = :myAcademicYearId];
        //} else 
        if(afwId != null){ // NAIS-1706 [CH] adding ability to function off of a single id value
            afwRecords = [SELECT Decline_Message_to_Parent__c, Contact__r.Name,  SSS_Contact_Email__c, Status__c 
                    FROM Application_Fee_Waiver__c 
                    WHERE Id = :afwId];
        }
        else{
            afwRecords = new List<Application_Fee_Waiver__c>{};            
        }

        // NAIS-2215 [DP] 02.02.2015 
        if (afwRecords.isEmpty()){
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Fee Waiver found.  Please check that the "id" URL Parameter is correct.'));
        } else {
            afw = afwRecords[0];
        }

        // NAIS-2215 [DP] 02.02.2015 
        if (afw != null && String.isBlank(afw.Decline_Message_to_Parent__c)){
            afw.Decline_Message_to_Parent__c = declineMessage;
        }
    }

    /* Properties */
    
    public Application_Fee_Waiver__c afw { get; set; }
    public List<Application_Fee_Waiver__c> afwRecords {get; set;}
    
    public String applicantName {
        get{
            if(afwRecords != null && afwRecords.size() > 0){
                system.debug(afwRecords[0].Contact__r.Name);
                return afwRecords[0].Contact__r.Name;
            }
            else{
                system.debug('null');
                return '';
            }
        } 
        set;
    }
    
    /* [CH] NAIS-1372 - These properties were added to facilitate working with multiple records */
    public String selectedContactId {get; set;}
    public String mySchoolId {get; set;}
    public String myAcademicYearId {get; set;}
    public String declineMessage {
        get{
            // Define the default decline message
            if(declineMessage == null){
                //declineMessage = 'Dear ' + afwRecords[0].Contact__r.Name + ',\n';
                //declineMessage += 'Your request for an application fee waiver has been declined by ' + currentSchool.Name + '. Please contact the school directly if you feel you have received this message in error.';
                declineMessage = Label.Fee_Waiver_Decline_Text; // NAIS-2215 [DP] 02.02.2015 
            }
            return declineMessage;        
        } 
        set;
    }
    
    /* END Properties */
    
    /* Action Methods */
    
    public PageReference cancel() {
        
        PageReference pageRef = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
        //pageRef.getParameters().put('declined', afw.Id);
        return pageRef;
    }
    
    public PageReference declinewithmsg() {
        //if (String.isBlank(declineMessage)) {
        //    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a decline message.'));
        //    return null;
        //}

        if (String.isBlank(afw.Decline_Message_to_Parent__c)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a decline message.'));
            return null;
        }
        
        /* [CH] NAIS-1372 - Refactored to work on multiple AFW records */
        // Update Field values for any records that are out there
        for(Application_Fee_Waiver__c afwLocal : afwRecords){
            if(afwLocal.Decline_Message_to_Parent__c == null || afwLocal.Decline_Message_to_Parent__c == ''){
                afwLocal.Decline_Message_to_Parent__c = declineMessage;
            }
            afwLocal.Status__c = 'Declined';
        }
        update afwRecords;

        // NAIS-2215 [DP] 02.02.2015 email is coming from workflow, so I am commenting this out

        //// Since there should only be one person associated to the potential group of
        ////  AFW records, we only want to send one email
        //Application_Fee_Waiver__c afw = afwRecords[0];

        //// Send a notification email        
        //if (afw.SSS_Contact_Email__c != null)
        //{
        //    // Reserve email capacity for the current Apex transaction.
        //    Messaging.reserveSingleEmailCapacity(1);
        //    // Create a new single email message object.
        //    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //    String[] toAddresses = new String[] { afw.SSS_Contact_Email__c };
        //    mail.setToAddresses(toAddresses);
        //    // Specify the address used when the recipients reply to the email.
        //    mail.setReplyTo('staff@nais.org');
        //    // Specify the name used as the display name.
        //    mail.setSenderDisplayName('NAIS Staff');
        //    // Specify the subject line for your email address.
        //    mail.setSubject('Fee Waiver Declined: ' + afw.Contact__r.Name);
        //    // Set to True if you want to BCC yourself on the email.
        //    mail.setBccSender(false);
        //    mail.setUseSignature(false);
            
        //    // Specify the text content of the email. Add in some extra info? XXX
        //    mail.setPlainTextBody(afw.Decline_Message_to_Parent__c);
        //    mail.setHtmlBody(afw.Decline_Message_to_Parent__c);
            
        //    // Send the email you have created.
        //    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });        
        //}

        if (ApexPages.currentPage().getParameters().get('retURL') != null) {
            PageReference pageRef = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
            pageRef.getParameters().put('declined', afw.Id);
            return pageRef;
        } else {
            //return null;
            return Page.SchoolFeeWaivers; // NAIS-2215 [DP] 02.02.2015 sending to schoolfeewaivers page instead of reloading if there is no returl
        }
    }
}