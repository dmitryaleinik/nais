/**
 * @description This class is used to query School_PFS_Assignment__c records in all their glory.
 */
public class SchoolPfsAssignmentsSelector extends fflib_SObjectSelector {
    
    @testVisible private static final String ID_SET_PARAM = 'idSet';
    @testVisible private static final String SPFSA_NAME = 'spfsaName';
    @testVisible private static final String SPFSA_APPLICANT_CONTACT = 'spfsaApplicantContact';
    @testVisible private static final String SPFSA_ACADEMIC_YEAR = 'spfsaAcademicYear';
    @testVisible private static final String SPFSA_FIELDS_TO_SELECT_PARAM = 'spfsaFields';
    @testVisible private static final String SCHOOL_IDS= 'schoolIds';
    @testVisible private static final String FOLDER_IDS= 'folderIds';
    
    /**
     * @description Default constructor for an instance of the SchoolPfsAssignmentsSelector that will not include field sets while
     *              NOT enforcing FLS or CRUD.
     */
    public SchoolPfsAssignmentsSelector() {
        // Avoid enforcing FLS or CRUD out of fear for breaking something in our SchoolPFSAssignmentAfter.trigger
        // You might call me a coward, but this coward won't be fixing bugs on Christmas.
        super(false, false, false, false);
    }

    /**
     * @description Queries School_PFS_Assignment__c records by Id.
     * @param idSet The Ids to query by.
     * @return A list of School_PFS_Assignment__c records.
     */
    public List<School_PFS_Assignment__c> selectById(Set<Id> idSet) {
        ArgumentNullException.throwIfNull(idSet, ID_SET_PARAM);

        return (List<School_PFS_Assignment__c>)super.selectSObjectsById(idSet);
    }
    
    /**
     * @description Queries school pfs assignment records which folders has awards.
     * @param spfsaId The id of pfs records to query.
     * @param folderId The id of student folder records to query.
     * @param fieldsToSelect The list of fields to query.
     * @return A list of school pfs assignment records.
     * @throws ArgumentNullException if spfsaIds is null.
     * @throws ArgumentNullException if fieldsToSelect is null.
     */
    public List<School_PFS_Assignment__c> selectAwards(Id pfsId, Id folderId, List<String> fieldsToSelect) {
        ArgumentNullException.throwIfNull(pfsId, 'pfsId');
        ArgumentNullException.throwIfNull(fieldsToSelect, SPFSA_FIELDS_TO_SELECT_PARAM);

        assertIsAccessible();
        
        Set<String> folderStatuses = FamilyAwardsService.FOLDER_STATUS_AWARDED.keySet();
        // TODO SFP-1750 Do we need to exclude withdrawn PFSs here?
        //Done
        String query = newQueryFactory(false)
                .selectFields(fieldsToSelect)
                .setCondition(
                    'PFS_ID__c =: pfsId ' +
                    'AND Student_Folder__c <> null ' +
                    'AND Applicant_ID__c <> null ' +
                    'AND School_Family_Award_Acknowledgement__c = true ' +
                    (folderId != null ? 'AND Student_Folder__c =: folderId ' : '') +
                    'AND Student_Folder_Status__c IN: folderStatuses ' +
                    'AND Award_Letter_Sent__c = true ' +
                    'AND Withdrawn__c != \'Yes\''
                     ).toSOQL();
        return Database.query(query);
    }

    /**
    * @description Queries school pfs assignment records, that applied for the school with enabled School_Family_Award_Acknowledgement__c  .
    * @param pfsId The id of pfs records to query.
    * @param fieldsToSelect The list of fields to query.
    * @return A list of school pfs assignment records.
    * @throws ArgumentNullException if spfsaIds is null.
    * @throws ArgumentNullException if fieldsToSelect is null.
    */
    public List<School_PFS_Assignment__c> selectSPFSASForPFSWithSchoolFamAwardEnabled(Id pfsId,  List<String> fieldsToSelect) {
        ArgumentNullException.throwIfNull(pfsId, 'pfsId');
        ArgumentNullException.throwIfNull(fieldsToSelect, SPFSA_FIELDS_TO_SELECT_PARAM);

        assertIsAccessible();

        Set<String> folderStatuses = FamilyAwardsService.FOLDER_STATUS_AWARDED.keySet();
        String query = newQueryFactory(false)
                .selectFields(fieldsToSelect)
                .setCondition(
                        'PFS_ID__c =: pfsId ' +
                                'AND Student_Folder__c <> null ' +
                                'AND Applicant_ID__c <> null ' +
                                'AND School_Family_Award_Acknowledgement__c = true '
                ).toSOQL();
        return Database.query(query);
    }

    /**
     * @description Queries school pfs assignment records by ids with custom field list.
     * @param spfsaIds The ids of school pfs assignment records to query.
     * @param fieldsToSelect The list of fields to query.
     * @return A list of school pfs assignment records.
     * @throws ArgumentNullException if spfsaIds is null.
     * @throws ArgumentNullException if fieldsToSelect is null.
     */
    public List<School_PFS_Assignment__c> selectByIdWithCustomFieldList(Set<Id> spfsaIds, List<String> fieldsToSelect) {
        ArgumentNullException.throwIfNull(spfsaIds, ID_SET_PARAM);
        ArgumentNullException.throwIfNull(fieldsToSelect, SPFSA_FIELDS_TO_SELECT_PARAM);

        assertIsAccessible();

        String query = newQueryFactory(false)
                .selectFields(fieldsToSelect)
                .setCondition('Id IN :spfsaIds')
                .toSOQL();

        return Database.query(query);
    }

    public String selectForFcwByFields() {

        List<String> pfsFields = new List<String>(Schema.SObjectType.PFS__c.fields.getMap().keySet());
        List<String> spaFields = new List<String>(Schema.SObjectType.School_PFS_Assignment__c.fields.getMap().keySet());
        List<String> verificationFields = new List<String>(Schema.SObjectType.Verification__c.fields.getMap().keySet());
        List<String> bizFarmAssigmentFields = new List<String>(Schema.SObjectType.School_Biz_Farm_Assignment__c.fields.getMap().keySet());
        List<String> bizFarmFields = new List<String>(Schema.SObjectType.Business_Farm__c.fields.getMap().keySet());

        return 'SELECT Applicant__r.PFS__r.Parent_A__r.Name, Applicant__r.PFS__r.Parent_B__r.Name, ' +
               + ' Applicant__r.Contact__c, Applicant__r.First_Name__c, Applicant__r.Last_Name__c, ' +
               + ' Applicant__r.PFS__r.Parent_A__r.Household__c, Applicant__r.Assets_Description__c, ' +
               + ' Applicant__r.Asset_Contribution__c, Applicant__r.Assets_Total_Value__c, ' +
               + ' Student_Folder__r.Academic_Year_Picklist__c, Student_Folder__r.Student__c, '
               + ' Student_Folder__r.Tuition_and_Expense_Notes_Long__c, '
               + appendStringToFields(pfsFields, 'Applicant__r.PFS__r.') + ', ' +
               + appendStringToFields(verificationFields, 'Applicant__r.PFS__r.Verification__r.') + ', ' +
               + ' (SELECT ' + String.Join(bizFarmAssigmentFields, ',') + ', '+
               + appendStringToFields(bizFarmFields, 'Business_Farm__r.') +
               + ' FROM School_Biz_Farm_Assignments__r), ' +
               + String.Join(spaFields, ',').removeEnd(',')
               + ' FROM School_PFS_Assignment__c';
    }

    public List<School_PFS_Assignment__c> selectForFcwById(Set<Id> idSet) {

        ArgumentNullException.throwIfNull(idSet, ID_SET_PARAM);

        String queryString = selectForFcwByFields() + ' WHERE id IN :idSet ';

        return Database.query(queryString);
    }
    
    /**
     * @description Retrieves a list of related SPAS for the given PFS ids.
     * @param idSet The set of PFS ids.
     * @return The list of related SPAS for the given PFS ids.
     */
    public List<School_PFS_Assignment__c> selectByPFSId(Set<Id> idSet) {
        
        return selectByPFSId(idSet, null);
    }
    
    /**
     * @description Retrieves a list of related SPAS for the given PFS ids.
     * @param idSet The set of PFS ids.
     * @param fieldsToQuery The list of fields to query.
     * @return The list of related SPAS for the given PFS ids.
     */
    public List<School_PFS_Assignment__c> selectByPFSId(Set<Id> idSet, List<String> fieldsToQuery) {

        ArgumentNullException.throwIfNull(idSet, ID_SET_PARAM);

        assertIsAccessible();
        
        if (fieldsToQuery == null) {
            
            fieldsToQuery = new List<String>();
	        fieldsToQuery.add('Id');
	        fieldsToQuery.add('Student_Folder__c');
	        fieldsToQuery.add('School__c');
	        fieldsToQuery.add('Applicant__c');
	        fieldsToQuery.add('Applicant__r.Contact__r.Name');
	        fieldsToQuery.add('Applicant__r.Contact__r.Birthdate');
	        fieldsToQuery.add('Applicant__r.Contact__c');
	        fieldsToQuery.add('Applicant__r.Current_Grade__c');
	        fieldsToQuery.add('Applicant__r.Current_School__c');
	        fieldsToQuery.add('Applicant__r.Gender__c');
	        fieldsToQuery.add('Applicant__r.PFS__c');
	        fieldsToQuery.add('Student_Folder__r.Student__c');
	        fieldsToQuery.add('Applicant__r.Birthdate_new__c');
	        fieldsToQuery.add('Applicant__r.First_Name__c');
	        fieldsToQuery.add('Applicant__r.Last_Name__c');
	        fieldsToQuery.add('PFS_Document_Status_Override__c');
	        fieldsToQuery.add('Withdrawn__c');
        }
        
        String query = newQueryFactory(false)
            .selectFields(fieldsToQuery)
            .setCondition('PFS_ID__c IN :idSet AND Withdrawn__c <> \'Yes\' ')
            .toSOQL();

        return Database.query(query);
    }

    /**
     * @description Queries all School PFS Assignment records.
     * @return A list of School PFS Assignment records.
     */
    public List<School_PFS_Assignment__c> selectAll() {
        return Database.query(newQueryFactory().toSOQL());
    }

    /**
     * @description Selects All School PFS Assignment records with custom fields mentioned in the passed parameter.
     * @param fieldsToSelect The School PFS Assignment fields to query.
     * @return A list of School PFS Assignment records.
     * @throws An ArgumentNullException if fieldsToSelect is null.
     */
    public List<School_PFS_Assignment__c> selectAllWithCustomFieldList(List<String> fieldsToSelect) {
        ArgumentNullException.throwIfNull(fieldsToSelect, SPFSA_FIELDS_TO_SELECT_PARAM);

        assertIsAccessible();

        String query = newQueryFactory(false)
                .selectFields(fieldsToSelect)
                .toSOQL();

        return Database.query(query);
    }

    /**
     * @description Queries spfsa records by Name.
     * @param spfsaNames Names of Spfsa records for querying records.
     * @return A list of spfsa records.
     * @throws ArgumentNullException if spfsaNames is null.
     */
    public List<School_PFS_Assignment__c> selectByName(Set<String> spfsaNames) {
        ArgumentNullException.throwIfNull(spfsaNames, SPFSA_NAME);

        assertIsAccessible();

        String query = newQueryFactory(true)
            .setCondition('Name IN :spfsaNames')
            .toSOQL();

        return Database.query(query);
    }
    
    /**
     * @description Queries spfsa records by folderId.
     * @param folderIds Ids of Spfsa records for querying records.
     * @param fieldsToQuery fields to query.
     * @param conditional Filters to apply.
     * @return A list of spfsa records.
     * @throws ArgumentNullException if folderIds is null.
     */
    public List<School_PFS_Assignment__c> selectBySetOfStudentFolders(
        Set<Id> folderIds, 
        List<String> fieldsToQuery, 
        String conditional) {
            
        ArgumentNullException.throwIfNull(folderIds, FOLDER_IDS);

        assertIsAccessible();
        
        fflib_QueryFactory qf = newQueryFactory(true);
        
        if (fieldsToQuery != null && !fieldsToQuery.isEmpty()) {
            
            qf = qf.selectFields(fieldsToQuery);
        }
        
        if (!String.isBlank(conditional)) {
            
            qf = qf.setCondition(conditional);
        } else {
            
            qf = qf.setCondition('Student_Folder__c IN :folderIds');
        }
        
        String query = qf.toSOQL();
        
        return Database.query(query);
    }
    
    /**
     * @description Queries spfsa records by folderId.
     * @param folderIds Ids of Spfsa records for querying records.
     * @return A list of spfsa records.
     * @throws ArgumentNullException if folderIds is null.
     */
    public List<School_PFS_Assignment__c> selectByStudentFolder(Set<Id> folderIds) {
        ArgumentNullException.throwIfNull(folderIds, FOLDER_IDS);

        assertIsAccessible();

        String query = newQueryFactory(true)
            .setCondition('Student_Folder__c IN :folderIds')
            .toSOQL();

        return Database.query(query);
    }

    /**
     * @description Queries spfsa records by School, Student and Academic Year.
     * @param studentIds Ids of Students for querying records.
     * @param schoolIds Ids of Schools for querying records.
     * @param academicYear Name of Academic Year for querying records.
     * @return A list of spfsa records.
     * @throws ArgumentNullException if studentIds is null.
     * @throws ArgumentNullException if schoolIds is null.
     * @throws ArgumentNullException if academicYear is null.
     */
    public List<School_PFS_Assignment__c> selectByStudentAndSchoolForAcademicYear(Set<Id> studentIds, Set<Id> schoolIds, String academicYear)
    {
        ArgumentNullException.throwIfNull(studentIds, SPFSA_APPLICANT_CONTACT);
        ArgumentNullException.throwIfNull(schoolIds, SCHOOL_IDS);
        ArgumentNullException.throwIfNull(academicYear, SPFSA_ACADEMIC_YEAR);

        assertIsAccessible();

        String condition = 'Applicant__c != null  AND Student_Folder__c != null AND Withdrawn__c != \'Yes\''
            + ' AND School__c IN :schoolIds AND Applicant__r.Contact__c IN :studentIds AND Academic_Year_Picklist__c = :academicYear';

        String query = newQueryFactory(false)
            .selectFields(new List<String>{'Student_Folder__r.Total_Aid__c', 'Orig_Parent_Can_Afford_to_Pay__c'})
            .setCondition(condition)
            .toSOQL();

        return Database.query(query);
    }

    public List<School_PFS_Assignment__c> selectByStudentAndSchoolForAcademicYear(Set<String> schoolContactAcademicYearNameKeys)
    {
        String condition = 'School_Contact_Academic_Year_Name__c IN: schoolContactAcademicYearNameKeys';
        
        String query = newQueryFactory(false)
            .selectFields(new List<String>{'School_Contact_Academic_Year_Name__c', 'Student_Folder__r.Total_Aid__c', 'Orig_Parent_Can_Afford_to_Pay__c'})
            .setCondition(condition)
            .toSOQL();

        return Database.query(query);
    }

    /**
     * @description Queries spfsa records by School and Academic Year.
     * @param schoolIds Ids of Schools for querying records.
     * @param academicYear Name of Academic Year for querying records.
     * @return A list of spfsa records.
     * @throws ArgumentNullException if schoolIds is null.
     * @throws ArgumentNullException if academicYear is null.
     */
    public List<School_PFS_Assignment__c> selectBySchoolAndAcademicYear(Set<Id> schoolIds, String academicYear)
    {
        ArgumentNullException.throwIfNull(schoolIds, SCHOOL_IDS);
        ArgumentNullException.throwIfNull(academicYear, SPFSA_ACADEMIC_YEAR);

        assertIsAccessible();

        String condition = 'School__c IN :schoolIds AND Academic_Year_Picklist__c = :academicYear';

        String query = newQueryFactory(false)
            .setCondition(condition)
            .toSOQL();

        return Database.query(query);
    }

    private String appendStringToFields(List<String> fieldList, String prefix) {

        ArgumentNullException.throwIfNull(fieldList, ID_SET_PARAM);
        String result = '';

        for(String fieldName : fieldList) {
            result += prefix + fieldName + ', ';
        }

        return result != null ? result.removeEnd(', ') : '';
    }

    private Schema.SObjectType getSObjectType() {
        return School_PFS_Assignment__c.SObjectType;
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>{
                School_PFS_Assignment__c.Name,
                School_PFS_Assignment__c.Kamehameha_ID__c,
                School_PFS_Assignment__c.Academic_Year_Picklist__c,
                School_PFS_Assignment__c.Age__c,
                School_PFS_Assignment__c.Birthdate__c,
                School_PFS_Assignment__c.Grade_Applying__c,
                School_PFS_Assignment__c.Parent_A__c,
                School_PFS_Assignment__c.Parent_B__c,
                School_PFS_Assignment__c.PFS_Received__c,
                School_PFS_Assignment__c.Total_Salary_and_Wages__c,
                School_PFS_Assignment__c.Orig_Total_Salary_and_Wages__c,
                School_PFS_Assignment__c.Effective_Income_Currency__c,
                School_PFS_Assignment__c.Orig_Effective_Income_Currency__c,
                School_PFS_Assignment__c.Income_Supplement__c,
                School_PFS_Assignment__c.Orig_Income_Supplement__c,
                School_PFS_Assignment__c.Update_Student_Folder__c,
                School_PFS_Assignment__c.Student_Folder__c,
                School_PFS_Assignment__c.Application_Last_Modified_by_Family__c,
                School_PFS_Assignment__c.Applicant__c
        };
    }

    /**
     * @description Creates a new instance of the SchoolPfsAssignmentsSelector.
     * @return An instance of SchoolPfsAssignmentsSelector.
     */
    public static SchoolPfsAssignmentsSelector newInstance() {
        return new SchoolPfsAssignmentsSelector();
    }

    /**
     * @description Singleton instance of the selector to be reused at your leisure.
     */
    public static SchoolPfsAssignmentsSelector Instance {
        get {
            if (Instance == null) {
                Instance = newInstance();
            }
            return Instance;
        }
        private set;
    }
}