@isTest
private class WeightedPfsSelectorTest {
    
    @isTest
    private static void selectWithCustomFieldListByPfsId() {
        Weighted_PFS__c weightedPfs = WeightedPfsTestData.Instance.DefaultWeightedPfs;
        Pfs__c pfs = PfsTestData.Instance.DefaultPfs;

        Test.startTest();
            try {
                List<Weighted_PFS__c> queriedRecords = WeightedPfsSelector.Instance
                    .selectWithCustomFieldListByPfsId(null, new List<String>{'Name'});
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, WeightedPfsSelector.PFS_ID_SET_PARAM);
            }

            try {
                List<Weighted_PFS__c> queriedRecords = WeightedPfsSelector.Instance
                    .selectWithCustomFieldListByPfsId(new Set<Id>{pfs.Id}, null);
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, WeightedPfsSelector.FIELDS_TO_SELECT_PARAM);
            }

            List<Weighted_PFS__c> queriedRecords = WeightedPfsSelector.Instance
                .selectWithCustomFieldListByPfsId(new Set<Id>{pfs.Id}, new List<String>{'Name'});
        Test.stopTest();

        System.assertNotEquals(null, queriedRecords, 'Expected the queried records to not be null.');
        System.assertEquals(1, queriedRecords.size(), 'Expected all inserted records to be returned.');
        System.assertEquals(weightedPfs.Id, queriedRecords[0].Id, 'Expected Id of the queried record is equal to Id of inserted record.');
    }

    @isTest
    private static void selectWithCustomFieldListByPfsAndAnnSetIds() {
        Weighted_PFS__c weightedPfs = WeightedPfsTestData.Instance.DefaultWeightedPfs;
        Pfs__c pfs = PfsTestData.Instance.DefaultPfs;
        Annual_Setting__c annualSetting = AnnualSettingsTestData.Instance.DefaultAnnualSettings;

        Test.startTest();
            try {
                List<Weighted_PFS__c> queriedRecords = WeightedPfsSelector.Instance
                    .selectWithCustomFieldListByPfsAndAnnSetIds(null, new Set<Id>{annualSetting.Id}, new List<String>{'Name'});
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, WeightedPfsSelector.PFS_ID_SET_PARAM);
            }

            try {
                List<Weighted_PFS__c> queriedRecords = WeightedPfsSelector.Instance
                    .selectWithCustomFieldListByPfsAndAnnSetIds(new Set<Id>{pfs.Id}, null, new List<String>{'Name'});
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, WeightedPfsSelector.ANNUAL_SETTING_ID_SET_PARAM);
            }

            try {
                List<Weighted_PFS__c> queriedRecords = WeightedPfsSelector.Instance
                    .selectWithCustomFieldListByPfsAndAnnSetIds(new Set<Id>{pfs.Id}, new Set<Id>{annualSetting.Id}, null);
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, WeightedPfsSelector.FIELDS_TO_SELECT_PARAM);
            }

            List<Weighted_PFS__c> queriedRecords = WeightedPfsSelector.Instance
                .selectWithCustomFieldListByPfsAndAnnSetIds(new Set<Id>{pfs.Id}, new Set<Id>{annualSetting.Id}, new List<String>{'Name'});
        Test.stopTest();

        System.assertNotEquals(null, queriedRecords, 'Expected the queried records to not be null.');
        System.assertEquals(1, queriedRecords.size(), 'Expected all inserted records to be returned.');
        System.assertEquals(weightedPfs.Id, queriedRecords[0].Id, 'Expected Id of the queried record is equal to Id of inserted record.');
    }

    @isTest
    private static void selectAllWeightedPfs() {
        Weighted_PFS__c weightedPfs = WeightedPfsTestData.Instance.DefaultWeightedPfs;

        Test.startTest();
            List<Weighted_PFS__c> queriedRecords = WeightedPfsSelector.Instance.selectAllWeightedPfs();
        Test.stopTest();

        System.assertNotEquals(null, queriedRecords, 'Expected the queried records to not be null.');
        System.assertEquals(1, queriedRecords.size(), 'Expected all inserted records to be returned.');
        System.assertEquals(weightedPfs.Id, queriedRecords[0].Id, 'Expected Id of the queried record is equal to Id of inserted record.');
    }
}