@isTest
private class OpportunitySelectorTest
{

    @isTest
    private static void selectByIdWithTransactionLineItems_nullOpportunityIds_expectArgumentNullException() {
        try {
            Test.startTest();
            OpportunitySelector.Instance.selectByIdWithTransactionLineItems(null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, OpportunitySelector.OPPORTUNITY_IDS_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void selectByIdWithTransactionLineItems_opportunityWithoutTransactionLineItem_expectRecord() {
        Id opportunityId = OpportunityTestData.Instance.DefaultOpportunity.Id;

        Test.startTest();
        List<Opportunity> opportunities = OpportunitySelector.Instance
                .selectByIdWithTransactionLineItems(new Set<Id> { opportunityId });
        Test.stopTest();

        System.assertEquals(1, opportunities.size(), 'Expected one Opportunity returned.');
        System.assertEquals(opportunityId, opportunities[0].Id, 'Expected the given opportunity returned.');
        System.assert(opportunities[0].Transaction_Line_Items__r.isEmpty(), 'Expected no Transaction Line Items.');
    }

    @isTest
    private static void selectByIdWithTransactionLineItems_opportunityWithTransactionLineItem_expectRecords() {
        Transaction_Line_Item__c transactionLineItem = TransactionLineItemTestData.Instance.DefaultTransactionLineItem;

        Test.startTest();
        List<Opportunity> opportunities = OpportunitySelector.Instance
                .selectByIdWithTransactionLineItems(new Set<Id> { transactionLineItem.Opportunity__c });
        Test.stopTest();

        System.assertEquals(1, opportunities.size(), 'Expected one Opportunity returned.');
        System.assertEquals(transactionLineItem.Opportunity__c, opportunities[0].Id,
                'Expected the given opportunity returned.');
        System.assertEquals(1, opportunities[0].Transaction_Line_Items__r.size(),
                'Expected one Transaction Line Item returned.');
        System.assertEquals(transactionLineItem.Id, opportunities[0].Transaction_Line_Items__r[0].Id,
                'Expected the given Transaction Line Item returned.');
    }

    @isTest
    private static void testSelectAll()
    {
        Opportunity opportunityA = OpportunityTestData.Instance.create();
        Opportunity opportunityB = OpportunityTestData.Instance.create();
        Opportunity opportunityC = OpportunityTestData.Instance.create();
        insert new List<Opportunity>{opportunityA, opportunityB, opportunityC};

        Test.startTest();
            List<Opportunity> selectedOpportunities = OpportunitySelector.Instance.selectAll();
        Test.stopTest();

        System.assertEquals(3, selectedOpportunities.size());
        System.assert((new Map<Id, Opportunity>(selectedOpportunities).keySet()).containsAll(new Set<Id>{opportunityA.Id, opportunityB.Id, opportunityC.Id}));
    }
}