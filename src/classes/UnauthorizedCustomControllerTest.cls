@isTest
private class UnauthorizedCustomControllerTest {
    
    @isTest static void testFamilyRedirect() {
        PageReference pr = Page.FamilyDashboard;
        Test.setCurrentPage(pr);
        UnauthorizedCustomController unauthController = new UnauthorizedCustomController();
        unauthController.currentPR = pr;
        System.assert(unauthController.smartRedirect().getURL().contains('familylogin'));
    }
    
    @isTest static void testSchoolRedirect() {
        PageReference pr = new PageReference('/schoolportal/'+Page.SchoolDashboard.getURL());
        Test.setCurrentPage(pr);
        UnauthorizedCustomController unauthController = new UnauthorizedCustomController();
        unauthController.currentPR = pr;
        System.assert(unauthController.smartRedirect().getURL().contains('schoollogin'));
    }
}