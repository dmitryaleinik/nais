@isTest
private class SchoolFinancialReportPDFControllerTest {

    private static Contact contact;
    private static Student_Folder__c studentFolder;
    private static Account school;
    private static Applicant__c applicant;


    private static void initData() {
        List<Academic_Year__c> academicYears = AcademicYearTestData.Instance.insertAcademicYears(5);
        String acYearPicklist = academicYears[0].Name;

        contact = ContactTestData.Instance.forRecordTypeId(RecordTypes.studentContactTypeId).DefaultContact;
        studentFolder = StudentFolderTestData.Instance.DefaultStudentFolder;

        PFS__c pfs = PfsTestData.Instance.forParentA(contact.Id).DefaultPfs;

        school = AccountTestData.Instance.DefaultAccount;
        SchoolPortalSettings.KS_School_Account_Id = school.Id;
        applicant = ApplicantTestData.Instance.DefaultApplicant;

        School_PFS_Assignment__c pfsAssignment = SchoolPfsAssignmentTestData.Instance.forAcademicYearPicklist(acYearPicklist).DefaultSchoolPfsAssignment;
    }


    @isTest private static void testController_validData_folderId() {
        initData();

        User testUser = CurrentUser.getCurrentUser();

        Test.startTest();
            System.runAs(testUser) {
                PageReference pageRef = Page.SchoolFinancialHistoryReportPDF;

                pageRef.getParameters().put('studentFolderId', String.valueOf(studentFolder.Id));
                pageRef.getParameters().put('applicantId', String.valueOf(applicant.Id));
                pageRef.getParameters().put('schoolId', String.valueOf(school.Id));
                pageRef.getParameters().put('studentId', String.valueOf(contact.Id));
                pageRef.getParameters().put('pfsParentId', String.valueOf(contact.Id));

                Test.setCurrentPage(pageRef);

                SchoolFinancialReportPDFController controller = new SchoolFinancialReportPDFController();
                Student_Folder__c folder = controller.studentFolder;
                System.assertEquals(folder.Id, studentFolder.Id);
            }
        Test.stopTest();
    }
}