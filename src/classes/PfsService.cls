/**
 * @description Handle interactions with PFS records.
 **/
public class PfsService {
    private static final Integer MAX_NUMBER_OF_REQUEUES = 3;
    @testVisible private static final String EXPECTED_LOCKED_ROW_MESSAGE =
            'first error: UNABLE_TO_LOCK_ROW, unable to obtain exclusive access to this record: []';
    private static Boolean wPfsQueued = false;

    // Used in an effort to gain additional code coverage and test requeueing
    @testVisible private static Exception exceptionToThrow = null;
    @testVisible private static Boolean requeued = false;

    /**
     * @description Queue Set Weighted PFS from PFS Ids call. Normally a
     *              service method such as this would validate that the
     *              parameter being passed in is not null, however due to
     *              the uncertainty of the data being processed, it is
     *              more desireable to allow it to fail silently in this
     *              case.
     * @param pfsIds the PFS Ids to set Weighted PFS' for.
     */
    public void queueSetWeightedPfs(Set<Id> pfsIds, List<PFS__c> pfsRecordsToUpdate) {
        if (!wPfsQueued) {
            System.enqueueJob(new QueueSetWeightedPfs(pfsIds, pfsRecordsToUpdate));
            wPfsQueued = true;
        }
    }

    /**
     * @description This class represents a queueable implementation of the
     *              SchoolSetWeightedPFS.setWeightedPFSFromPFSIds() method.
     */
    private class QueueSetWeightedPfs implements Queueable {
        private Integer numberOfRequeues = 0;
        private Set<Id> pfsIds;
        private List<PFS__c> pfsRecordsToUpdate;

        /**
         * @description Execute the queued action of setting
         *              the Weighted PFS from PFS Ids.
         */
        public void execute(QueueableContext context) {
            try {
                SchoolSetWeightedPFS sswPFS = new SchoolSetWeightedPFS();
                sswPFS.setWeightedPFSFromPFSIds(this.pfsIds);
                if (!this.pfsRecordsToUpdate.isEmpty()) {
                    update this.pfsRecordsToUpdate;
                }

                // SFP-1160 - This allows the testing of the exception handling and
                //            requeueing.
                if (Test.isRunningTest() && exceptionToThrow != null) {
                    throw exceptionToThrow;
                }
            } catch (DmlException dmlException) {
                requeueLockedRow(dmlException);
            } catch (Exception e) {
                // Log the exception so we can make sure we catch any oddities.
                CustomDebugService.Instance.logException(e);
            }
        }

        /**
         * @description Constructor for the QueueSetWeightedPfs class taking in
         *              the PFS Ids to operate on.
         */
        public PfsService.QueueSetWeightedPfs(Set<Id> pfsIds, List<PFS__c> pfsRecordsToUpdate) {
            this.pfsIds = pfsIds;
            this.pfsRecordsToUpdate = pfsRecordsToUpdate;
        }

        private void requeueLockedRow(DmlException e) {
            if (e.getMessage().contains(EXPECTED_LOCKED_ROW_MESSAGE) &&
                    numberOfRequeues < MAX_NUMBER_OF_REQUEUES) {

                numberOfRequeues++;
                requeued = true;
                if (!Test.isRunningTest()) {
                    System.enqueueJob(this);
                }
            } else if (numberOfRequeues >= MAX_NUMBER_OF_REQUEUES) {
                CustomDebugService.Instance.withCustomMessage(Label.TooManyRequeues).logException(e);
            } else {
                CustomDebugService.Instance.logException(e);
            }
        }
    }

    /**
     * @description Singleton instance property.
     */
    public static PfsService Instance {
        get {
            if (Instance == null) {
                Instance = new PfsService();
            }
            return Instance;
        }
        private set;
    }

    private PfsService() {}
}