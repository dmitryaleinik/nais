/**
 * @description This class runs nightly to send information to a document processing vendor to notify them that a family
 *              document needs to be verified. This job will catch anything that isn't processed immediately by the
 *              DocumentVerificationQueue.cls
 */
public without sharing class DocumentVerificationBatch implements Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts {

    // TODO SFP-1061 Allow this job to be scheduled from the scheduled jobs page.

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
            [SELECT Id, Document__c, Document_Import_Id__c, Document_Type__c, School_Code__c, School_PFS_Assignment__c, School_PFS_Assignment__r.PFS_Number__c
                FROM School_Document_Assignment__c
                WHERE Verification_Request_Status__c = :SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE
                ORDER BY CreatedDate]);
    }

    public void execute(Database.BatchableContext bc, List<SObject> scope) {
        List<School_Document_Assignment__c> docAssignments = (List<School_Document_Assignment__c>)scope;

        Map<Id, School_Document_Assignment__c> recordsById = new Map<Id, School_Document_Assignment__c>(docAssignments);

        DocumentVerificationService.Instance.requestVerificationForProcessedDocs(recordsById.keySet());
    }

    public void finish(Database.BatchableContext bc) { }

    public void execute(SchedulableContext sc) {
        Database.executeBatch(new DocumentVerificationBatch(), 50);
    }
}