@isTest
private class AcademicYearServiceTest 
{

    private static void assertAcademicYears(Academic_Year__c expectedYear, Academic_Year__c actualYear) {
        String messageToFormat = 'Expected the academic year to be the one prior to the latest academic year. \n Expected Year: {0} \n Actual Year: {1}';

        String assertMessage = String.format(messageToFormat, new List<String> { expectedYear.Name, actualYear.Name });

        System.assertEquals(expectedYear.Id, actualYear.Id, assertMessage);
    }

    private static void setFamilyPortalStartInFuture(Academic_Year__c academicYearToUpdate) {
        Date currentDate = Date.today();

        academicYearToUpdate.Family_Portal_Start_Date__c = currentDate.addDays(1);
        academicYearToUpdate.Family_Portal_End_Date__c = currentDate.toStartOfMonth().addDays(-1).addYears(1);

        update academicYearToUpdate;
    }

    private static List<Academic_Year__c> filterFutureYears(List<Academic_Year__c> records) {
        List<Academic_Year__c> years = new List<Academic_Year__c>();

        for (Academic_Year__c record : records) {
            if (record.Start_Date__c <= System.today()) {
                years.add(record);
            }
        }

        return years;
    }

    @isTest private static void getCurrentAcademicYear_familyUserWithoutEarlyAccess_earlyAccessClosed_notInOverlapPeriod_expectPreviousYear() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        User familyUser = UserTestData.createFamilyPortalUser();

        List<Academic_Year__c> testAcademicYears = TestUtils.createAcademicYears();

        // make the first academic year start in the future for family users.
        setFamilyPortalStartInFuture(testAcademicYears[0]);

        // make the second academic year the current academic year for family users.
        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-1);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(1);
        update testAcademicYears[1];

        List<Academic_Year__c> allAcademicYears = GlobalVariables.getAllAcademicYears();
        Integer numberOfYears = allAcademicYears.size();

        // Get the academic year before the latest academic year which should be second in the list.
        Academic_Year__c expectedYear = allAcademicYears[1];

        Academic_Year__c actualYear;
        System.runAs(familyUser) {
            actualYear = AcademicYearService.Instance.getCurrentAcademicYear();
        }

        System.assertNotEquals(null, actualYear, 'Expected the academic year to not be null.');
        assertAcademicYears(expectedYear, actualYear);
    }

    @isTest private static void getCurrentAcademicYear_familyUserWithEarlyAccess_earlyAccessClosed_notInOverlapPeriod_expectPreviousYear() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        // Insert a family portal user with early access.
        User familyUser = UserTestData.createFamilyPortalUser();
        familyUser.Early_Access__c = true;
        insert familyUser;

        List<Academic_Year__c> testAcademicYears = TestUtils.createAcademicYears();

        // make the first academic year start in the future for family users.
        setFamilyPortalStartInFuture(testAcademicYears[0]);

        // make the second academic year the current academic year for family users.
        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-1);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(1);
        update testAcademicYears[1];

        List<Academic_Year__c> allAcademicYears = GlobalVariables.getAllAcademicYears();
        Integer numberOfYears = allAcademicYears.size();

        // Get the academic year before the latest academic year which should be second in the list.
        Academic_Year__c expectedYear = allAcademicYears[1];

        Academic_Year__c actualYear;
        System.runAs(familyUser) {
            actualYear = AcademicYearService.Instance.getCurrentAcademicYear();
        }

        System.assertNotEquals(null, actualYear, 'Expected the academic year to not be null.');
        assertAcademicYears(expectedYear, actualYear);
    }

    @isTest private static void getCurrentAcademicYear_familyUserWithEarlyAccess_earlyAccessOpen_notInOverlapPeriod_expectCurrentAcademicYear() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessOpened().DefaultFamilyPortalSettings;

        // Insert a family portal user with early access.
        User familyUser = UserTestData.createFamilyPortalUser();
        familyUser.Early_Access__c = true;
        insert familyUser;

        List<Academic_Year__c> testAcademicYears = TestUtils.createAcademicYears();

        // make the first academic year start in the future for family users.
        setFamilyPortalStartInFuture(testAcademicYears[0]);

        // make the second academic year the current academic year for family users.
        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-1);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(1);
        update testAcademicYears[1];

        List<Academic_Year__c> allAcademicYears = GlobalVariables.getAllAcademicYears();
        Integer numberOfYears = allAcademicYears.size();

        // Get the first year in the list of all academic years which should be the latest academic year.
        Academic_Year__c expectedYear = allAcademicYears[0];

        Academic_Year__c actualYear;
        System.runAs(familyUser) {
            actualYear = AcademicYearService.Instance.getCurrentAcademicYear();
        }

        System.assertNotEquals(null, actualYear, 'Expected the academic year to not be null.');
        assertAcademicYears(expectedYear, actualYear);
    }

    @isTest private static void getCurrentAcademicYear_familyUser_inOverlapPeriod_expectCurrentAcademicYear() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
                .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        User familyUser = UserTestData.createFamilyPortalUser();

        List<Academic_Year__c> testAcademicYears = TestUtils.createAcademicYears();

        // make the first academic year current based on todays date for family users.
        testAcademicYears[0].Family_Portal_Start_Date__c = Date.today().addDays(-1);
        testAcademicYears[0].Family_Portal_End_Date__c = Date.today().addDays(1);

        // make the second academic year current based on todays date for family users.
        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-10);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(1);
        update testAcademicYears;

        List<Academic_Year__c> allAcademicYears = GlobalVariables.getAllAcademicYears();

        // Get the first year in the list of all academic years which should be the latest academic year.
        Academic_Year__c expectedYear = allAcademicYears[0];

        Academic_Year__c actualYear;
        System.runAs(familyUser) {
            actualYear = AcademicYearService.Instance.getCurrentAcademicYear();
        }

        System.assertNotEquals(null, actualYear, 'Expected the academic year to not be null.');
        assertAcademicYears(expectedYear, actualYear);
    }

    @isTest private static void getCurrentAcademicYear_schoolUser_expectLatestAcademicYear() {
        AcademicYearTestData.Instance.insertAcademicYears(5);

        User schoolUser = UserTestData.insertSchoolPortalUser();

        List<Academic_Year__c> allAcademicYears = GlobalVariables.getAllAcademicYears();
        Integer numberOfYears = allAcademicYears.size();

        // Filter out future and past academic years to grab our assertion records.
        allAcademicYears = filterFutureYears(allAcademicYears);

        // Get the first available year in the list of all academic years.
        Academic_Year__c expectedYear = allAcademicYears[0];

        Academic_Year__c actualYear;
        System.runAs(schoolUser) {
            actualYear = AcademicYearService.Instance.getCurrentAcademicYear();
        }

        System.assertNotEquals(null, actualYear, 'Expected the academic year to not be null.');
        assertAcademicYears(expectedYear, actualYear);
    }

    @isTest private static void getPreviousAcademicYear_familyUserWithoutEarlyAccess_earlyAccessClosed_notInOverlapPeriod_expectNoPreviousAcademicYearAvailable() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
                .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        User familyUser = UserTestData.createFamilyPortalUser();

        List<Academic_Year__c> testAcademicYears = TestUtils.createAcademicYears();

        // make the first academic year start in the future for family users.
        setFamilyPortalStartInFuture(testAcademicYears[0]);

        // make the second academic year the current academic year for family users.
        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-1);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(1);
        update testAcademicYears[1];

        Academic_Year__c actualYear;
        System.runAs(familyUser) {
            actualYear = AcademicYearService.Instance.getPreviousAcademicYear();
        }

        System.assertEquals(null, actualYear, 'Expected the academic year to be null when there are no overlapping academic years and the user does not have early access.');
    }

    @isTest private static void getPreviousAcademicYear_familyUserWithEarlyAccess_earlyAccessClosed_notInOverlapPeriod_expectNoPreviousAcademicYearAvailable() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
                .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        // Insert a user with early access.
        User familyUser = UserTestData.createFamilyPortalUser();
        familyUser.Early_Access__c = true;
        insert familyUser;

        List<Academic_Year__c> testAcademicYears = TestUtils.createAcademicYears();

        // make the first academic year start in the future for family users.
        setFamilyPortalStartInFuture(testAcademicYears[0]);

        // make the second academic year the current academic year for family users.
        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-1);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(1);
        update testAcademicYears[1];

        Academic_Year__c actualYear;
        System.runAs(familyUser) {
            actualYear = AcademicYearService.Instance.getPreviousAcademicYear();
        }

        System.assertEquals(null, actualYear, 'Expected the previous academic year to be null when there are no overlapping academic years.');
    }

    @isTest private static void getPreviousAcademicYear_familyUserWithEarlyAccess_earlyAccessOpen_notInOverlapPeriod_expectPreviousAcademicYear() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessOpened().DefaultFamilyPortalSettings;

        // Insert a user with early access.
        User familyUser = UserTestData.createFamilyPortalUser();
        familyUser.Early_Access__c = true;
        insert familyUser;

        List<Academic_Year__c> testAcademicYears = TestUtils.createAcademicYears();

        // make the first academic year start in the future for family users.
        setFamilyPortalStartInFuture(testAcademicYears[0]);

        // make the second academic year the current academic year for family users.
        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-1);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(1);
        update testAcademicYears[1];

        List<Academic_Year__c> allAcademicYears = GlobalVariables.getAllAcademicYears();
        Integer numberOfYears = allAcademicYears.size();

        // Get the 2nd year in the list of all academic years which should be the year prior to the latest academic year.
        Academic_Year__c expectedYear = allAcademicYears[1];

        Academic_Year__c actualYear;
        System.runAs(familyUser) {
            actualYear = AcademicYearService.Instance.getPreviousAcademicYear();
        }

        System.assertNotEquals(null, actualYear, 'Expected the academic year to not be null.');
        assertAcademicYears(expectedYear, actualYear);
    }

    @isTest private static void getPreviousAcademicYear_familyUser_inOverlapPeriod_expectPreviousAcademicYear() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
                .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        User familyUser = UserTestData.createFamilyPortalUser();

        List<Academic_Year__c> testAcademicYears = TestUtils.createAcademicYears();

        // make the first academic year current based on todays date for family users.
        testAcademicYears[0].Family_Portal_Start_Date__c = Date.today().addDays(-1);
        testAcademicYears[0].Family_Portal_End_Date__c = Date.today().addDays(1);

        // make the second academic year current based on todays date for family users.
        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-10);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(1);
        update testAcademicYears;

        List<Academic_Year__c> allAcademicYears = GlobalVariables.getAllAcademicYears();
        Integer numberOfYears = allAcademicYears.size();

        // Get the 2nd year in the list of all academic years which should be the year prior to the latest academic year.
        Academic_Year__c expectedYear = allAcademicYears[1];

        Academic_Year__c actualYear;
        System.runAs(familyUser) {
            actualYear = AcademicYearService.Instance.getPreviousAcademicYear();
        }

        System.assertNotEquals(null, actualYear, 'Expected the academic year to not be null.');
        assertAcademicYears(expectedYear, actualYear);
    }

    @isTest private static void getPreviousAcademicYear_schoolUser_expectPreviousAcademicYear() {
        TestUtils.createAcademicYears();

        User schoolUser = UserTestData.insertSchoolPortalUser();

        List<Academic_Year__c> allAcademicYears = GlobalVariables.getAllAcademicYears();
        Integer numberOfYears = allAcademicYears.size();

        // Filter out future and past academic years to grab our assertion records.
        allAcademicYears = filterFutureYears(allAcademicYears);

        // Get the 2nd year in the list of all academic years which should be the year prior to the latest academic year.
        Academic_Year__c expectedYear = allAcademicYears[1];

        Academic_Year__c actualYear;
        System.runAs(schoolUser) {
            actualYear = AcademicYearService.Instance.getPreviousAcademicYear();
        }

        System.assertNotEquals(null, actualYear, 'Expected the academic year to not be null.');
        assertAcademicYears(expectedYear, actualYear);
    }

    @isTest private static void getAvailableAcademicYears_familyUser_noOverlappingAcademicYears_noEarlyAccess_expectOnlyCurrentAcademicYear() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        List<Academic_Year__c> testAcademicYears = TestUtils.createAcademicYears();

        // Make the first academic year in the future.
        setFamilyPortalStartInFuture(testAcademicYears[0]);

        // make the second academic year current based on todays date for family users.
        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-1);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(1);
        update testAcademicYears[1];

        User familyUser = UserTestData.createFamilyPortalUser();

        List<Academic_Year__c> availableAcademicYears;
        System.runAs(familyUser) {
            availableAcademicYears = AcademicYearService.Instance.getAvailableAcademicYears();
        }

        System.assertNotEquals(null, availableAcademicYears, 'Expected the list of available academic years to not be null.');
        System.assertEquals(1, availableAcademicYears.size(), 'Expected only one academic year to be open for the family user.');
        assertAcademicYears(testAcademicYears[1], availableAcademicYears[0]);
    }

    @isTest private static void getAvailableAcademicYears_familyUser_overlappingAcademicYears_noEarlyAccess_expectTwoAcademicYears() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        List<Academic_Year__c> testAcademicYears = TestUtils.createAcademicYears();

        // make the first academic year current based on todays date for family users.
        testAcademicYears[0].Family_Portal_Start_Date__c = Date.today().addDays(-1);
        testAcademicYears[0].Family_Portal_End_Date__c = Date.today().addDays(1);

        // make the second academic year current based on todays date for family users.
        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-10);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(1);
        update testAcademicYears;

        User familyUser = UserTestData.createFamilyPortalUser();

        List<Academic_Year__c> availableAcademicYears;
        System.runAs(familyUser) {
            availableAcademicYears = AcademicYearService.Instance.getAvailableAcademicYears();
        }

        System.assertNotEquals(null, availableAcademicYears, 'Expected the list of available academic years to not be null.');
        System.assertEquals(2, availableAcademicYears.size(), 'Expected 2 academic years to be open for the family user.');
        assertAcademicYears(testAcademicYears[0], availableAcademicYears[0]);
        assertAcademicYears(testAcademicYears[1], availableAcademicYears[1]);
    }

    @isTest private static void getAvailableAcademicYears_familyUser_noOverlappingAcademicYears_earlyAccessOpen_expectTwoAcademicYears() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessOpened().DefaultFamilyPortalSettings;

        List<Academic_Year__c> testAcademicYears = TestUtils.createAcademicYears();

        // Make the first academic year in the future.
        setFamilyPortalStartInFuture(testAcademicYears[0]);

        // make the second academic year current based on todays date for family users.
        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-1);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(1);
        update testAcademicYears[1];

        User familyUser = UserTestData.createFamilyPortalUser();
        familyUser.Early_Access__c = true;
        insert familyUser;

        List<Academic_Year__c> availableAcademicYears;
        System.runAs(familyUser) {
            availableAcademicYears = AcademicYearService.Instance.getAvailableAcademicYears();
        }

        System.assertNotEquals(null, availableAcademicYears, 'Expected the list of available academic years to not be null.');
        System.assertEquals(2, availableAcademicYears.size(), 'Expected 2 academic years to be open for the family user.');
        assertAcademicYears(testAcademicYears[0], availableAcademicYears[0]);
        assertAcademicYears(testAcademicYears[1], availableAcademicYears[1]);
    }

    @isTest private static void getOverlapStartDate_familyUserWithoutEarlyAccess_noAcademicYearAfterCurrentYear_expectNull() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        Integer numberOfRecords = 1;

        List<Academic_Year__c> testAcademicYears = AcademicYearTestData.Instance.createAcademicYears(numberOfRecords);

        // make the academic year current based on todays date for family users.
        testAcademicYears[0].Family_Portal_Start_Date__c = Date.today().addDays(-1);
        testAcademicYears[0].Family_Portal_End_Date__c = Date.today().addDays(1);
        insert testAcademicYears;

        User familyUser = UserTestData.createFamilyPortalUser();

        String overlapStartDate;
        System.runAs(familyUser) {
            overlapStartDate = AcademicYearService.Instance.getOverlapStartDate();
        }

        System.assertEquals(null, overlapStartDate,
                'Expected overlap start date to be null when there is no academic year after the current.');
    }

    @isTest private static void getOverlapStartDate_familyUserWithoutEarlyAccess_noOverlappingAcademicYears_lastAcademicYearNotStarted_expectLastAYStartDate() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        Integer numberOfRecords = 2;

        List<Academic_Year__c> testAcademicYears = AcademicYearTestData.Instance.createAcademicYears(numberOfRecords);

        Date expectedOverlapStartDate = Date.today().addDays(10);

        // make first academic year in the future for family users.
        testAcademicYears[0].Family_Portal_Start_Date__c = expectedOverlapStartDate;
        testAcademicYears[0].Family_Portal_End_Date__c = Date.today().addDays(20);

        System.debug('&&& first test AY === ' +testAcademicYears[0].Name);

        // make the second academic year current based on todays date for family users.
        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-1);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(1);

        System.debug('&&& second test AY === ' +testAcademicYears[1].Name);

        insert testAcademicYears;

        User familyUser = UserTestData.createFamilyPortalUser();

        String overlapStartDate;
        System.runAs(familyUser) {
            overlapStartDate = AcademicYearService.Instance.getOverlapStartDate();
        }

        System.assertEquals(expectedOverlapStartDate.format(), overlapStartDate,
                'Expected the overlap start date to be the start date of the future academic year.');
    }

    @isTest private static void getOverlapStartDate_familyUserWithoutEarlyAccess_overlappingAcademicYears_lastAcademicYearInFuture_expectLastAcademicYearStartDate() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        Integer numberOfRecords = 3;

        List<Academic_Year__c> testAcademicYears = AcademicYearTestData.Instance.createAcademicYears(numberOfRecords);

        Date expectedOverlapStartDate = Date.today().addDays(10);

        // make first academic year in the future for family users.
        testAcademicYears[0].Family_Portal_Start_Date__c = expectedOverlapStartDate;
        testAcademicYears[0].Family_Portal_End_Date__c = Date.today().addDays(20);

        // make the second academic year current based on todays date for family users.
        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-5);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(5);

        // make the third academic year overlap with the current academic year for family users.
        testAcademicYears[2].Family_Portal_Start_Date__c = Date.today().addDays(-15);
        testAcademicYears[2].Family_Portal_End_Date__c = Date.today().addDays(1);

        insert testAcademicYears;

        User familyUser = UserTestData.createFamilyPortalUser();

        String overlapStartDate;
        System.runAs(familyUser) {
            overlapStartDate = AcademicYearService.Instance.getOverlapStartDate();
        }

        System.assertEquals(expectedOverlapStartDate.format(), overlapStartDate,
                'Expected the overlap start date to be the start date of the future academic year.');
    }

    @isTest private static void showLandingPage_familyUserWithoutEarlyAccess_noAcademicYearAfterCurrent_expectFalse() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        Integer numberOfRecords = 1;

        List<Academic_Year__c> testAcademicYears = AcademicYearTestData.Instance.createAcademicYears(numberOfRecords);

        // make the academic year current based on todays date for family users.
        testAcademicYears[0].Family_Portal_Start_Date__c = Date.today().addDays(-1);
        testAcademicYears[0].Family_Portal_End_Date__c = Date.today().addDays(1);
        insert testAcademicYears;

        User familyUser = UserTestData.createFamilyPortalUser();

        Boolean showLandingPage;
        System.runAs(familyUser) {
            showLandingPage = AcademicYearService.Instance.showLandingPage();
        }

        System.assertEquals(false, showLandingPage, 'Expected the landing page to not be shown if there is no academic year the current one.');
    }

    @isTest private static void showLandingPage_familyUserWithoutEarlyAccess_nextAcademicYearStartingAfterLandingPageStartDate_landingPageStartsInPast_expectTrue() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        Integer numberOfRecords = 3;

        List<Academic_Year__c> testAcademicYears = AcademicYearTestData.Instance.createAcademicYears(numberOfRecords);

        // make first academic year in the future for family users.
        DateTime landingPageStart = DateTime.now().addDays(-2);
        testAcademicYears[0].Family_Portal_Start_Date__c = Date.today().addDays(10);
        testAcademicYears[0].Family_Portal_End_Date__c = Date.today().addDays(20);
        testAcademicYears[0].Family_Landing_Page_Start_Date__c = landingPageStart;

        // make the second academic year current based on todays date for family users.
        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-5);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(5);

        // make the third academic year overlap with the current academic year for family users.
        testAcademicYears[2].Family_Portal_Start_Date__c = Date.today().addDays(-15);
        testAcademicYears[2].Family_Portal_End_Date__c = Date.today().addDays(1);
        insert testAcademicYears;

        User familyUser = UserTestData.createFamilyPortalUser();

        Boolean showLandingPage;
        System.runAs(familyUser) {
            showLandingPage = AcademicYearService.Instance.showLandingPage();
        }

        System.assertEquals(true, showLandingPage,
                'Expected landing page to be shown when the next academic year starts after the landing page start date.');
    }

    @isTest private static void showLandingPage_familyUserWithoutEarlyAccess_nextAcademicYearStartingAfterLandingPageStartDate_landingPageStartsInFuture_expectFalse() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        Integer numberOfRecords = 3;

        List<Academic_Year__c> testAcademicYears = AcademicYearTestData.Instance.createAcademicYears(numberOfRecords);

        // make first academic year in the future for family users.
        DateTime landingPageStart = DateTime.now().addDays(2);
        testAcademicYears[0].Family_Portal_Start_Date__c = Date.today().addDays(10);
        testAcademicYears[0].Family_Portal_End_Date__c = Date.today().addDays(20);
        testAcademicYears[0].Family_Landing_Page_Start_Date__c = landingPageStart;

        // make the second academic year current based on todays date for family users.
        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-5);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(5);

        // make the third academic year overlap with the current academic year for family users.
        testAcademicYears[2].Family_Portal_Start_Date__c = Date.today().addDays(-15);
        testAcademicYears[2].Family_Portal_End_Date__c = Date.today().addDays(1);
        insert testAcademicYears;

        User familyUser = UserTestData.createFamilyPortalUser();

        Boolean showLandingPage;
        System.runAs(familyUser) {
            showLandingPage = AcademicYearService.Instance.showLandingPage();
        }

        System.assertEquals(false, showLandingPage,
                'Expected landing page to not be shown when the landing page start date is in the future.');
    }

    @isTest private static void showLandingPage_familyUserWithoutEarlyAccess_nextAcademicYearStartingBeforeLandingPageStartDate_expectFalse() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        Integer numberOfRecords = 3;

        List<Academic_Year__c> testAcademicYears = AcademicYearTestData.Instance.createAcademicYears(numberOfRecords);

        // make first academic year in the future for family users.
        DateTime landingPageStart = DateTime.now().addDays(12);
        testAcademicYears[0].Family_Portal_Start_Date__c = Date.today().addDays(10);
        testAcademicYears[0].Family_Portal_End_Date__c = Date.today().addDays(20);
        testAcademicYears[0].Family_Landing_Page_Start_Date__c = landingPageStart;

        // make the second academic year current based on todays date for family users.
        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-5);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(5);

        // make the third academic year overlap with the current academic year for family users.
        testAcademicYears[2].Family_Portal_Start_Date__c = Date.today().addDays(-15);
        testAcademicYears[2].Family_Portal_End_Date__c = Date.today().addDays(1);
        insert testAcademicYears;

        User familyUser = UserTestData.createFamilyPortalUser();

        Boolean showLandingPage;
        System.runAs(familyUser) {
            showLandingPage = AcademicYearService.Instance.showLandingPage();
        }

        System.assertEquals(false, showLandingPage,
                'Expected landing page to not be shown when the next academic year starts before the landing page start date.');
    }

    @isTest private static void showLandingPage_familyUserWithEarlyAccess_nextAcademicYearStartingAfterLandingPageStartDate_landingPageStartsInPast_expectFalse() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessOpened().DefaultFamilyPortalSettings;

        Integer numberOfRecords = 3;

        List<Academic_Year__c> testAcademicYears = AcademicYearTestData.Instance.createAcademicYears(numberOfRecords);

        // make first academic year in the future for family users.
        DateTime landingPageStart = DateTime.now().addDays(-2);
        testAcademicYears[0].Family_Portal_Start_Date__c = Date.today().addDays(10);
        testAcademicYears[0].Family_Portal_End_Date__c = Date.today().addDays(20);
        testAcademicYears[0].Family_Landing_Page_Start_Date__c = landingPageStart;

        // make the second academic year current based on todays date for family users.
        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-5);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(5);

        // make the third academic year overlap with the current academic year for family users.
        testAcademicYears[2].Family_Portal_Start_Date__c = Date.today().addDays(-15);
        testAcademicYears[2].Family_Portal_End_Date__c = Date.today().addDays(1);
        insert testAcademicYears;

        User familyUser = UserTestData.createFamilyPortalUser();
        familyUser.Early_Access__c = true;
        insert familyUser;

        Boolean showLandingPage;
        System.runAs(familyUser) {
            showLandingPage = AcademicYearService.Instance.showLandingPage();
        }

        System.assertEquals(false, showLandingPage,
                'Expected the landing page to not be shown if a family portal user has early access to the future academic year.');
    }

    @isTest private static void hasFamilyPortalOpened_familyPortalStartDateNull_expectFalse() {
        Integer numberOfYears = 1;

        List<Academic_Year__c> testAcademicYears = AcademicYearTestData.Instance.createAcademicYears(numberOfYears);
        testAcademicYears[0].Family_Portal_Start_Date__c = null;
        insert testAcademicYears;

        Id testId = testAcademicYears[0].Id;

        Boolean hasFamilyPortalOpened = AcademicYearService.Instance.hasFamilyPortalOpened(testId);

        System.assertEquals(false, hasFamilyPortalOpened,
                'Expected false if the academic year did not specify a start to the family portal.');
    }

    @isTest private static void hasFamilyPortalOpened_familyPortalStartDateInFuture_expectFalse() {
        Integer numberOfYears = 1;

        List<Academic_Year__c> testAcademicYears = AcademicYearTestData.Instance.createAcademicYears(numberOfYears);
        testAcademicYears[0].Family_Portal_Start_Date__c = Date.today().addDays(10);
        insert testAcademicYears;

        Id testId = testAcademicYears[0].Id;

        Boolean hasFamilyPortalOpened = AcademicYearService.Instance.hasFamilyPortalOpened(testId);

        System.assertEquals(false, hasFamilyPortalOpened,
                'Expected false if the family portal start date is in the future for the specified academic year.');
    }

    @isTest private static void hasFamilyPortalOpened_familyPortalStartDateToday_expectTrue() {
        Integer numberOfYears = 1;

        List<Academic_Year__c> testAcademicYears = AcademicYearTestData.Instance.createAcademicYears(numberOfYears);
        testAcademicYears[0].Family_Portal_Start_Date__c = Date.today();
        insert testAcademicYears;

        Id testId = testAcademicYears[0].Id;

        Boolean hasFamilyPortalOpened = AcademicYearService.Instance.hasFamilyPortalOpened(testId);

        System.assertEquals(true, hasFamilyPortalOpened,
                'Expected true if the family portal start date is today for the specified academic year.');
    }

    @isTest private static void hasFamilyPortalOpened_familyPortalStartDateInPast_expectTrue() {
        Integer numberOfYears = 1;

        List<Academic_Year__c> testAcademicYears = AcademicYearTestData.Instance.createAcademicYears(numberOfYears);
        testAcademicYears[0].Family_Portal_Start_Date__c = Date.today().addDays(-10);
        insert testAcademicYears;

        Id testId = testAcademicYears[0].Id;

        Boolean hasFamilyPortalOpened = AcademicYearService.Instance.hasFamilyPortalOpened(testId);

        System.assertEquals(true, hasFamilyPortalOpened,
                'Expected true if the family portal start date is in the past for the specified academic year.');
    }

    @isTest
    private static void isFamilyPortalOpen_familyPortalDatesNull_expectFalse() {
        String academicYearName = '2018-2019';

        Date startDate = null;
        Date endDate = null;

        Academic_Year__c academicYearRecord = AcademicYearTestData.Instance.forName(academicYearName).forFamilyPortalStartDate(startDate).forFamilyPortalEndDate(endDate).insertAcademicYear();

        System.assert(!AcademicYearService.Instance.isFamilyPortalOpen(academicYearRecord.Id),
                'Expected the family portal to not be open.');
        System.assert(!AcademicYearService.Instance.isFamilyPortalOpen(academicYearRecord.Name),
                'Expected the family portal to not be open.');
    }

    @isTest
    private static void isFamilyPortalOpen_todaysDateOutsideFamilyPortalDates_expectFalse() {
        String academicYearName = '2018-2019';

        Date startDate = Date.today().addDays(-20);
        Date endDate = Date.today().addDays(-10);

        Academic_Year__c academicYearRecord = AcademicYearTestData.Instance.forName(academicYearName).forFamilyPortalStartDate(startDate).forFamilyPortalEndDate(endDate).insertAcademicYear();

        System.assert(!AcademicYearService.Instance.isFamilyPortalOpen(academicYearRecord.Id),
                'Expected the family portal to not be open.');
        System.assert(!AcademicYearService.Instance.isFamilyPortalOpen(academicYearRecord.Name),
                'Expected the family portal to not be open.');
    }

    @isTest
    private static void isFamilyPortalOpen_todaysDateInsideFamilyPortalDates_expectTrue() {
        String academicYearName = '2018-2019';

        Date startDate = Date.today().addDays(-10);
        Date endDate = Date.today().addDays(10);

        Academic_Year__c academicYearRecord = AcademicYearTestData.Instance.forName(academicYearName).forFamilyPortalStartDate(startDate).forFamilyPortalEndDate(endDate).insertAcademicYear();

        System.assert(AcademicYearService.Instance.isFamilyPortalOpen(academicYearRecord.Id),
                'Expected the family portal to be open.');
        System.assert(AcademicYearService.Instance.isFamilyPortalOpen(academicYearRecord.Name),
                'Expected the family portal to be open.');
    }

    @isTest
    private static void isFamilyPortalOpen_todaysDateOnFamilyPortalDate_expectTrue() {
        String academicYearName = '2018-2019';

        Date startDate = Date.today().addDays(-10);
        Date endDate = Date.today();

        Academic_Year__c academicYearRecord = AcademicYearTestData.Instance.forName(academicYearName).forFamilyPortalStartDate(startDate).forFamilyPortalEndDate(endDate).insertAcademicYear();

        System.assert(AcademicYearService.Instance.isFamilyPortalOpen(academicYearRecord.Id),
                'Expected the family portal to be open.');
        System.assert(AcademicYearService.Instance.isFamilyPortalOpen(academicYearRecord.Name),
                'Expected the family portal to be open.');
    }
}