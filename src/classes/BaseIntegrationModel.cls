/**
 * BaseIntegrationModel.cls
 *
 * @description: Base Integration Model to be extended byFamilyIntegrationModel or School IntegrationModel classes.
 * Contains the logic to map fields by inegration mapping. etc.
 *
 * @author: Mike Havrila @ Presence PG
 */

public abstract class BaseIntegrationModel {

    private final String LOOPKUP_TYPE = 'LOOKUP';
    private final String ID_TYPE = 'ID';
    private final String NUMBER_TYPE = 'NUMBER';
    private final String BOOLEAN_TYPE = 'BOOLEAN';
    private final String PICKLIST_TYPE = 'PICKLIST';
    private final String DATE_TYPE = 'DATE';

    /**
     * @description: Takes a list of raw objects loops over the integrationmapping models, then maps the
     * raw data to sss data model.
     *
     * @param: List<Object> dataToMap - raw data to map. (API response)
     *
     * @param: List<IntegrationMappingModel> mappings - list of Integration Mapping Models to loop over.
     *
     * @param: String targetObject: target object to create a new instance of. (i.e. Applicant__c, Student Folder__c)
     *
     * @return - List of new sObjects of targetObject type
     */
    protected List<sObject> mapRecords( List<Object> dataToMap, List<IntegrationMappingModel> mappings, String targetObject) {
        List<sObject> recordsToCreate = new List<sObject>();
        for( Object source : dataToMap) {
            // create a new instance of the targetObject
            Map<String, List<Object>> valuesToIterateOver = new Map<String, List<Object>>();

            Map<String, Object> sourceValues = ( Map<String, Object>)source;
            sObject newObject = Schema.getGlobalDescribe().get( targetObject).newSObject();
            for( IntegrationMappingModel mapping : mappings) {
                Object tempValue;
                // This handles source fields that have more than once value;
                for(String splitSource : mapping.sourceField.split(',')) {

                    if( sourceValues.get( splitSource) != null) {

                        tempValue = sourceValues.get( splitSource);
                        break;
                    } else {

                        tempValue = IntegrationUtils.getValuesFromObject( sourceValues, mapping.sourceField);
                    }
                }

                // Only process records that have a mapping object we are currently processing.
                if( mapping.targetObject == targetObject) {

                    if( tempValue instanceof List<Object>) {

                        // creates a collection of lists that need to be mapped and added to the object
                        valuesToIterateOver.put( mapping.targetField, (List<Object>)tempValue);
                    } else {

                        // if we don't have to handle nested arrays, then just map the record.
                        newObject = this.mapRecord( mapping, tempValue, targetObject, newObject);
                    }
                }

            }

            // only do this if there are values in valuesToIterateOver
            if ( valuesToIterateOver.size() != 0) {
                List<sObject> clonedObjects = new List<sObject>();

                // Because there might be multiple values in the map
                // we need to only create cloned objects based on the size
                // of the list. To create multiple objects (Student_Folder__c, School_PFS_Assignment)
                // This happens when the student applys for more than one school.
                // Example (SaoId__c, (12345, 12346, 123458); ApplicationId for school.
                Integer iterator = 0;
                for ( String field : valuesToIterateOver.keySet()) {
                    // set's the iterator size. This assumes all List<Object> should be the same size;
                    // If they aren;t this will have to be revisted.
                    iterator = valuesToIterateOver.get(field).size();
                    break;
                }

                // Loop over based on iterator size and clone each record.
                // if a student applied for 3 schools, we need to create 3 objects
                // with a different school and application id.
                for (Integer i = 0; i < iterator; i++) {
                    sObject clonedObject = newObject.clone(false, true, false, false);
                    clonedObjects.add( clonedObject);
                }

                // finally loop over each keyset and add it the object.
                for ( String field : valuesToIterateOver.keySet()) {
                    Integer i = 0;
                    for ( Object o : valuesToIterateOver.get( field)) {
                        String objectField = field;
                        if ( field.contains(':')) {
                            objectField = field.split(':')[0];
                        }

                        // if i becomes greater than cloned objects
                        // then we need to revisit this.
                        if ( i < clonedObjects.size()) {

                            clonedObjects[i].put( objectField, (String)o);
                            i++;
                        }
                    }
                }
                recordsToCreate.addAll( clonedObjects);

            } else {
                recordsToCreate.add( newObject);
            }
        }



        // do lookups if necessary
        for( IntegrationMappingModel mapping : mappings) {
            // SFP-1686 For the School__c lookups on PFS Assignments and Folders, we put a temporary value in their first which is the Integration Id.
            // Then we use that value to try querying for the school record and then replace it with the account's actual Id.
            // This will be the Ravenna Id unless it is null in which case we use the NCES Id. Same thing with the SAO Id.
            if( mapping.dataType == LOOPKUP_TYPE && mapping.targetObject == targetObject) {

                recordsToCreate = this.lookupHelper( recordsToCreate, mapping);
            }
        }

        return recordsToCreate;
    }

    /**
     * @description: Maps single value to a field on an sobject
     *
     * @param: IntegrationMappingModel mapping - a single instance of Integration Mapping Model.
     *
     * @param: Object tempValue - value to map to sobject field.
     *
     * @param: String targetObject: target object to create a new instance of. (i.e. Applicant__c, Student Folder__c)
     *
     * @param: sObject newObject: new instance of an sobject type, that the tempValue is associated to
     *
     * @return - List of new sObjects of targetObject type
     */
    private sObject mapRecord( IntegrationMappingModel mapping, Object tempValue, String targetObject, sObject newObject ) {
        // there may be source objects mapped to multiple sf objects, processed separately
        if( mapping.targetObject == targetObject && tempValue != null &&  mapping.targetField != null) {

            if( mapping.dataType == LOOPKUP_TYPE) {

                newObject.put( mapping.targetField.split(':')[0], String.valueOf( tempValue));
            } else if( mapping.dataType == ID_TYPE) {
                // if this is an id, treat it as an external id from a spefic external system : prepend this source
                newObject.put( mapping.targetField, String.valueOf( tempValue));
            } else if( mapping.dataType == NUMBER_TYPE) {

                // implement me
            } else if( mapping.dataType == BOOLEAN_TYPE) {

                // implement me
            } else if( mapping.dataType == PICKLIST_TYPE) {

                //String mappedValue = mapPicklist(mapping.sourceField__c, string.valueOf(tempValue) );
                newObject.put( mapping.targetField, String.valueOf( tempValue));
            } else if( mapping.dataType == DATE_TYPE) {
                Date d = this.dateParser( String.valueOf( tempValue));
                newObject.put(mapping.targetField, d);
            } else {
                if( String.isNotEmpty( mapping.targetField)) {
                    newObject.put(mapping.targetField, tempValue);
                }
            }
        }

        return newObject;
    }

    /**
     * @description: Util Method based on a list of Sobjects and mapping model with type of Lookup, does a lookup
     * to a target object based on external Id, and appends the id to the list of objects.
     *
     * @param: List<sObject> sObjectsList sobjects up update from query.
     *
     * @param: IntegrationMappingModel mappingModel: instance of IntegratioNMappingModel
     *
     * @return - The initial list of sObjects with lookup fields populated.
     */
    private List<SObject> lookupHelper( List<sObject> sObjectsList, IntegrationMappingModel mappingModel ) {
        // Split the mapping mode target field (Ie)
        // Student_Folder__c:Student_Folder__c RavennaId__c
        // targetField =  RavennaId__c
        // targetObject = Student_Folder__c
        List<String> fields = mappingModel.targetField.split(':');
        String targetField = fields[1].split(' ')[1];
        String targetObject = fields[1].split(' ')[0];

        List<String> externalIds = new List<String>();
        for ( sObject sObj : sObjectsList) {

            externalIds.add( ( String)sObj.get( fields[0]));
        }

        // find potential lookup matches by searching on the key
        String query = 'select Id,' + targetfield;
        query += ' from ' + targetObject;
        query += ' where ' + targetField + ' in :externalIds';

        List<SObject> sobjectResults = Database.query(query);

        Map<String,sObject> externalIdToRecordsMap = new Map<String,sObject>();
        for ( sObject sObj : sobjectResults) {
            externalIdToRecordsMap.put( (String)sObj.get(targetField), sObj);
        }

        // take the temporary value in the lookup field to try to
        // get the id of the record, and use it. if none, null so no lookup value
        for ( sObject sObj : sObjectsList) {

            sObject lookup = externalIdToRecordsMap.get( ( String)sObj.get( fields[0]));
            if ( lookup != null) {

                sObj.put(fields[0], lookup.Id);
            } else {

                sObj.put( fields[0], null);
            }
        }

        return sObjectsList;
    }

    /**
     * @description: Parses dateTimes and returns a date instance
     *
     * @param: String  dateToParse
     *
     * @return - An instance of a date based on dateTime
     */
    private Date dateParser( String dateToParse) {

        Date d;
        String dateString = dateToParse;
        if ( dateToParse.indexOf( 'T') > 0) {

            dateString = dateToParse.split( 'T')[0];
        } else if ( dateToParse.indexOf( ' ') > 0) {

            dateString = dateToParse.split( ' ')[0];
        }

        if( String.isNotEmpty( dateString)) {

            List<String> dateValues = dateString.split( '-');
            Integer year = Integer.valueOf( dateValues[0]);
            Integer month = Integer.valueOf( dateValues[1]);
            Integer day = Integer.valueOf( dateValues[2]);

            d = Date.newInstance( year, month, day);
        }

        return d;
    }
}