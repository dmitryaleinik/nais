@isTest
private class SchoolPfsAssignmentsControllerTest {

    @isTest
    private static void getSchoolPfsAssignments_noPfsAssignments_expectEmptyList() {
        PFS__c pfsRecord = PfsTestData.Instance.insertPfs();

        PageReference pageRef = Page.SchoolPFSAssignments;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', pfsRecord.Id);
        ApexPages.StandardController stdController = new ApexPages.StandardController(pfsRecord);
        SchoolPfsAssignmentsController controller = new SchoolPfsAssignmentsController(stdController);

        List<School_PFS_Assignment__c> schoolPfsAssignments = controller.getSchoolPfsAssignments();
        System.assertNotEquals(null, schoolPfsAssignments, 'Expected the list to not be null.');
        System.assertEquals(0, schoolPfsAssignments.size(), 'Expected zero records to be returned.');
    }

    @isTest
    private static void getSchoolPfsAssignments_pfsAssignmentsExist_expectRecords() {
        PFS__c pfsRecord = PfsTestData.Instance.insertPfs();

        Contact student = ContactTestData.Instance.asStudent().insertContact();

        Applicant__c applicant = ApplicantTestData.Instance.forContactId(student.Id).forPfsId(pfsRecord.Id).insertApplicant();

        Account school = AccountTestData.Instance.asCurrent().insertAccount();

        Student_Folder__c folder = StudentFolderTestData.Instance.forSchoolId(school.Id).forStudentId(student.Id).insertStudentFolder();

        School_PFS_Assignment__c spa = SchoolPfsAssignmentTestData.Instance.forSchoolId(school.Id).forApplicantId(applicant.Id).forStudentFolderId(folder.Id).insertSchoolPfsAssignment();

        PageReference pageRef = Page.SchoolPFSAssignments;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', pfsRecord.Id);
        ApexPages.StandardController stdController = new ApexPages.StandardController(pfsRecord);
        SchoolPfsAssignmentsController controller = new SchoolPfsAssignmentsController(stdController);

        List<School_PFS_Assignment__c> schoolPfsAssignments = controller.getSchoolPfsAssignments();
        System.assertNotEquals(null, schoolPfsAssignments, 'Expected the list to not be null.');
        System.assertEquals(1, schoolPfsAssignments.size(), 'Expected one record to be returned.');
    }
}