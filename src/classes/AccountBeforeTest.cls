@isTest
private class AccountBeforeTest {
    private static Account createAccount() {
        return TestUtils.createAccount('School A', RecordTypes.schoolAccountTypeId, null, false);
    }

    // reset last notification date so that legacy submissions are not sent in the first batch after alert is activated
    @isTest
    private static void lastAlertDateResetWhenAlertIsChanged() {
        Account school = createAccount();
        school.Alert_Frequency__c = 'No Alerts';
        school.Last_PFS_Notification_Date__c = System.today() - 2;
        insert school;

        Test.startTest();
        school.Alert_Frequency__c = 'Daily digest of new applications';
        update school;
        Test.stopTest();

        Account schoolAfterUpdate = [select Last_PFS_Notification_Date__c from Account where Id = :school.Id];

        System.assertEquals(null, schoolAfterUpdate.Last_PFS_Notification_Date__c);
    }

    // no need to exclude existing submissions when alert frequency is reduced from daily to weekly
    @isTest
    private static void lastAlertDateNotResetWhenAlertChangesFromDailyToWeekly() {
        Account school = createAccount();
        school.Alert_Frequency__c = 'Daily digest of new applications';
        school.Last_PFS_Notification_Date__c = System.today() - 2;
        insert school;

        Test.startTest();
        school.Alert_Frequency__c = 'Weekly digest of new applications';
        update school;
        Test.stopTest();

        Account schoolAfterUpdate = [select Last_PFS_Notification_Date__c from Account where Id = :school.Id];

        System.assertEquals(System.today() - 2, schoolAfterUpdate.Last_PFS_Notification_Date__c);
    }

    @isTest
    private static void beforeInsert_sssSubscriptionTypeCurrentNotSet_expectPortalVersionNull() {
        Account school = createAccount();

        Test.startTest();
        insert school;
        Test.stopTest();

        System.assertNotEquals(null, school.Id, 'Expected the Id to not be null.');

        // Requery for the school to ensure we validate the Portal_Version__c field correctly
        Account updatedSchool = [SELECT Portal_Version__c FROM Account WHERE Id = :school.Id];
        System.assertNotEquals(null, updatedSchool, 'Expected the school to be returned when queried.');
        System.assertEquals(null, updatedSchool.Portal_Version__c, 'Expected the Portal_Version__c field to be null.');
    }

    @isTest
    private static void beforeInsert_sssSubscriptionTypeCurrentSetToExtended_expectPortalVersionNull() {
        Account school = createAccount();

        Test.startTest();
        school.SSS_Subscription_Type_Current__c = 'Extended';
        insert school;
        Test.stopTest();

        System.assertNotEquals(null, school.Id, 'Expected the Id to not be null.');

        // Requery for the school to ensure we validate the Portal_Version__c field correctly
        Account updatedSchool = [SELECT Portal_Version__c FROM Account WHERE Id = :school.Id];
        System.assertNotEquals(null, updatedSchool, 'Expected the school to be returned when queried.');
        System.assertEquals(null, updatedSchool.Portal_Version__c, 'Expected the Portal_Version__c field to be null.');
    }

    @isTest
    private static void beforeInsert_sssSubscriptionTypeCurrentSetToWaiverDistributor_expectPortalVersionNull() {
        Account school = createAccount();

        Test.startTest();
        school.SSS_Subscription_Type_Current__c = 'Waiver Distributor';
        insert school;
        Test.stopTest();

        System.assertNotEquals(null, school.Id, 'Expected the Id to not be null.');

        // Requery for the school to ensure we validate the Portal_Version__c field correctly
        Account updatedSchool = [SELECT Portal_Version__c FROM Account WHERE Id = :school.Id];
        System.assertNotEquals(null, updatedSchool, 'Expected the school to be returned when queried.');
        System.assertEquals(null, updatedSchool.Portal_Version__c, 'Expected the Portal_Version__c field to be null.');
    }

    @isTest
    private static void beforeUpdate_sssSubscriptionTypeCurrentNotSet_expectPortalVersionNull() {
        Account school = createAccount();
        school.SSS_Subscription_Type_Current__c = SubscriptionTypes.BASIC_TYPE;
        insert school;

        Test.startTest();
        school.SSS_Subscription_Type_Current__c = null;
        update school;
        Test.stopTest();

        System.assertNotEquals(null, school.Id, 'Expected the Id to not be null.');

        // Requery for the school to ensure we validate the Portal_Version__c field correctly
        Account updatedSchool = [SELECT Portal_Version__c FROM Account WHERE Id = :school.Id];
        System.assertNotEquals(null, updatedSchool, 'Expected the school to be returned when queried.');
        System.assertEquals(null, updatedSchool.Portal_Version__c, 'Expected the Portal_Version__c field to be null.');
    }

    @isTest
    private static void beforeUpdate_sssSubscriptionTypeCurrentSetToExtended_expectPortalVersionNull() {
        Account school = createAccount();
        insert school;

        Test.startTest();
        school.SSS_Subscription_Type_Current__c = 'Extended';
        update school;
        Test.stopTest();

        System.assertNotEquals(null, school.Id, 'Expected the Id to not be null.');

        // Requery for the school to ensure we validate the Portal_Version__c field correctly
        Account updatedSchool = [SELECT Portal_Version__c FROM Account WHERE Id = :school.Id];
        System.assertNotEquals(null, updatedSchool, 'Expected the school to be returned when queried.');
        System.assertEquals(null, updatedSchool.Portal_Version__c, 'Expected the Portal_Version__c field to be null.');
    }

    @isTest
    private static void beforeUpdate_sssSubscriptionTypeCurrentSetToWaiverDistributor_expectPortalVersionNull() {
        Account school = createAccount();
        insert school;

        Test.startTest();
        school.SSS_Subscription_Type_Current__c = 'Waiver Distributor';
        update school;
        Test.stopTest();

        System.assertNotEquals(null, school.Id, 'Expected the Id to not be null.');

        // Requery for the school to ensure we validate the Portal_Version__c field correctly
        Account updatedSchool = [SELECT Portal_Version__c FROM Account WHERE Id = :school.Id];
        System.assertNotEquals(null, updatedSchool, 'Expected the school to be returned when queried.');
        System.assertEquals(null, updatedSchool.Portal_Version__c, 'Expected the Portal_Version__c field to be null.');
    }

    @isTest
    private static void beforeUpdate_sssSubscriptionTypeCurrentSetToFull_expectPortalVersionFullFeatured() {
        Account school = createAccount();
        insert school;

        Test.startTest();
        school.SSS_Subscription_Type_Current__c = SubscriptionTypes.SUBSCRIPTION_FULL_TYPE;
        update school;
        Test.stopTest();

        System.assertNotEquals(null, school.Id, 'Expected the Id to not be null.');

        // Requery for the school to ensure we validate the Portal_Version__c field correctly
        Account updatedSchool = [SELECT Portal_Version__c FROM Account WHERE Id = :school.Id];
        System.assertNotEquals(null, updatedSchool, 'Expected the school to be returned when queried.');
        System.assertEquals(SubscriptionTypes.PORTAL_VERSION_FULL_TYPE, updatedSchool.Portal_Version__c,
                'Expected the Portal_Version__c field to be ' + SubscriptionTypes.PORTAL_VERSION_FULL_TYPE + '.');
    }

    @isTest
    private static void beforeUpdate_sssSubscriptionTypeCurrentSetToBasic_expectPortalVersionBasic() {
        Account school = createAccount();
        insert school;

        Test.startTest();
        school.SSS_Subscription_Type_Current__c = SubscriptionTypes.BASIC_TYPE;
        update school;
        Test.stopTest();

        System.assertNotEquals(null, school.Id, 'Expected the Id to not be null.');

        // Requery for the school to ensure we validate the Portal_Version__c field correctly
        Account updatedSchool = [SELECT Portal_Version__c FROM Account WHERE Id = :school.Id];
        System.assertNotEquals(null, updatedSchool, 'Expected the school to be returned when queried.');
        System.assertEquals(SubscriptionTypes.BASIC_TYPE, updatedSchool.Portal_Version__c,
                'Expected the Portal_Version__c field to be ' + SubscriptionTypes.BASIC_TYPE + '.');
    }
}