/**
 * @description Credit Card payment information.
 */
public class CreditCardInformation extends PaymentInformation {
    public String CardType { get; set; }
    public String NameOnCard { get; set; }
    public String CardNumber { get; set; }
    public String Cvv2 { get; set; }
    public String ExpirationMM { get; set; }
    public String ExpirationYY { get; set; }

    public String CardNumberLastFour {
        get {
            if (CardNumber != null && CardNumber.length() >= 4) {
                return CardNumber.right(4);
            }
            return null;
        }
    }
}