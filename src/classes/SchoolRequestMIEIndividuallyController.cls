public class SchoolRequestMIEIndividuallyController {
    
    public PFS__c pfsRecord { get; set; }
    public Boolean justRequested { get; set; }

    public Integer isEnabledRequestMIEIndividually {
        get {
            if (isEnabledRequestMIEIndividually == null) {
                isEnabledRequestMIEIndividually = MonthlyIncomeAndExpensesService.isRequestMIEIndividuallyEnabled(pfsRecord);
            }
            return isEnabledRequestMIEIndividually;
        }
        set;
    }

    public Boolean isMIERequestEnabled {

        get {
            return isEnabledRequestMIEIndividually == MonthlyIncomeAndExpensesService.MIE_Request_Enabled;
        }
        private set;

    }
    
    public Boolean isMIERequestedAndCompleted {
        
        get {
            return isEnabledRequestMIEIndividually == MonthlyIncomeAndExpensesService.MIE_Request_Completed;
        }
        private set;
        
    }//End:isMIERequestedAndCompleted
    
    public Boolean isMIERequestedAndIncompleted {
        
        get {

            return isEnabledRequestMIEIndividually == MonthlyIncomeAndExpensesService.MIE_Request_Incompleted;
        }
        private set;
        
    }//End:isMIERequestedAndIncompleted
    
    public SchoolRequestMIEIndividuallyController() {

        justRequested = false;
        
    }//End-Constructor
    
    public PageReference requestMIEAction() {
        
        MonthlyIncomeAndExpensesService.requestMIEIndividuallyDML(pfsRecord.Id);

        isEnabledRequestMIEIndividually = null;
        
        //Temporal variables, just to rerender the alreadyRequested message without reload the entired page.
        justRequested = true;
        
        //These values won't be saved to the database. They are used for visual purposes.
        //To load values right after rerender onComplete.
        pfsRecord.MIE_Requested_Date__c = system.today();
        pfsRecord.MIE_Requested_By_School_Name__c = GlobalVariables.getCurrentSchool().Name;

        return null;
    }//End:requestMIEAction
    
    public String getMIERequestedDate() {
        
        Date d = pfsRecord.MIE_Requested_Date__c;
        
        return d != null ? d.month() + '/' + d.day() + '/' + d.year() : '';
    }
}