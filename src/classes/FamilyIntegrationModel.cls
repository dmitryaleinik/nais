/**
 * FamilyIntegrationModel.cls
 *
 * @description: Handles data containment for Integgrations in teh family Portal.
 * Contains helper methods to inflate the correct objects while iterating through api calls.
 *
 * @author: Mike Havrilla @ Presence PG
 */

public class FamilyIntegrationModel extends BaseIntegrationModel {

    private static final String ACCOUNT_EXT_ID_FIELD_NAME_PARAM = 'accountExternalIdFieldName';

    /* properties */
    public Map<String, List<IntegrationMappingModel>> mappingModelBySourceMap;
    public String integrationSource { get; set; }

    /* student properties*/
    /**
     * @description The keys are the Ids of students in a 3rd party system (e.g. Ravenna, SAO). The values are the
     *              applicant SObjects that represent the student in our system.
     */
    public Map<String, sObject> studentIdToApplicantMap { get; set; }
    public Map<String, List<String>> studentIdToSchoolsMap { get; set; }

    /**
     * @description The keys are the Ids of students in a 3rd party system (e.g. Ravenna, SAO). The values are the raw
     *              responses that represent a single student in the other system.
     */
    public Map<String, List<Object>> studentIdToResponseMap { get; set; }

    /**
     * @description A map of raw JSON responses from an external API where the keys are student Ids in another system and
     *              the values are a list of applications. This map contains the raw applications from another system
     *              grouped by the student Id. These should ultimately be used to generate the applications in our system.
     */
    public Map<String, List<Object>> ApplicationResponsesByStudentId {
        get {
            if (ApplicationResponsesByStudentId == null) {
                ApplicationResponsesByStudentId = new Map<String, List<Object>>();
            }
            return ApplicationResponsesByStudentId;
        }
        private set;
    }

    /**
     * @description Contains the models of each school retrieved from the schools API response by the name of the
     *              account field that is used as the external Id.
     */
    private Map<String, SchoolIntegrationModels> schoolModelsByExternalIdFieldName;
    public SchoolIntegrationModels getSchoolModels(String accountExternalIdFieldName) {
        ArgumentNullException.throwIfNull(accountExternalIdFieldName, ACCOUNT_EXT_ID_FIELD_NAME_PARAM);

        SchoolIntegrationModels schoolModels = schoolModelsByExternalIdFieldName.get(accountExternalIdFieldName);

        if (schoolModels != null) {
            return schoolModels;
        }

        schoolModels = SchoolIntegrationModels.newInstance(accountExternalIdFieldName);

        schoolModelsByExternalIdFieldName.put(accountExternalIdFieldName, schoolModels);

        return schoolModels;
    }


    public Map<String, sObject> schoolIdsToAccountMap { get; set; }

    // sOBjects
    public List<sObject> applicantsToUpsert { get; set; }
    public List<sObject> studentApplicants { get; set; }

    /* Public Static Variables */
    public static final String APPLICANT_OBJECT = 'Applicant__c';
    public static final String STUDENT_FOLDER_OBJECT = 'Student_Folder__c';
    public static final String SCHOOL_PFS_ASSIGN_OBJECT = 'School_PFS_Assignment__c';
    public static final String SCHOOL_OBJECT = 'School__c';
    public static final String STUDENT_OBJECT = 'Student__c';

    public static final String STUDENTS_TYPE = 'Students';
    public static final String STUDENT_APPLICATION = 'Student Applications';
    
    public static final String ID_FIELD = 'ID';
    public static final String LOOPKUP_FIELD = 'LOOKUP';

    // ctor
    public FamilyIntegrationModel( String source) {
        this.integrationSource = source;

        this.studentIdToApplicantMap = new Map<String, sObject>();
        this.studentIdToSchoolsMap = new Map<String, List<String>>();
        this.studentIdToResponseMap = new Map<String, List<Object>>();
        this.schoolIdsToAccountMap = new Map<String, sObject>();

        this.studentApplicants = new List<sObject>();
        this.mappingModelBySourceMap = this.getMappingConfig( source);
        this.schoolModelsByExternalIdFieldName = new Map<String, SchoolIntegrationModels>();
    }

    /**
     * @description: get Integration Mapping Vlaues and return a Map
     *
     * @param: Source - Integration Name: I.e. (ravenna, sao)
     *
     * @return - A map of IntegrationMapping__mdt based on Source_-c;
     */
    private Map<String, List<IntegrationMappingModel>> getMappingConfig( String source) {

        Map<String, List<IntegrationMappingModel>> returnVal = new Map<String, List<IntegrationMappingModel>>();
        List<IntegrationMapping__mdt> mappingTypes = new IntegrationMappingDataAccessService().getMappingsByIntegration( source);
        if( mappingTypes == null) {

            mappingTypes = new List<IntegrationMapping__mdt>();
        }

        for( IntegrationMapping__mdt mappingType : mappingTypes) {

            List<IntegrationMappingModel> model = new List<IntegrationMappingModel>();
            if( returnVal.containsKey( mappingType.SourceObject__c)) {

                model = returnVal.get( mappingType.SourceObject__c);
            }

            model.add( new IntegrationMappingModel( mappingType));
            returnVal.put( mappingType.SourceObject__c, model);
        }

        return returnVal;
    }

    /**
     * @description: Builds a map of Student Id's to Sobject Type
     *
     * @param: objects - List of Sobjects
     *
     * @param: targetFieldName - field name from Object to use as Map Key
     *
     */
    public void setStudentIdsToSobject( List<Sobject> objects, String targetFieldName) {

        this.studentIdToApplicantMap = this.getObjectByIds( objects, targetFieldName);
    }

    /**
     * @description: Returns a field name from IntegrationMappingModel with a type of ID.
     *
     * @param: mappingModels - a List of IntegrationMappingModel's
     *
     * @param: pfsId - targetObject target object in IntegrationMappingModel to compare against.
     *
     */
    public String getExternalIdFieldByObject( List<IntegrationMappingModel> mappingModels, String targetObject) {

        return this.getMappingFieldByType( mappingModels, 'ID', targetObject);
    }

    /**
     * @description: Get mapping field by source
     * 
     * @param mappingModel - an instance of mapping model
     *
     * @param targetFieldType - target field type: (String, Lookup, etc);
     * 
     * @param targetObject - target object;
     *
     * @return the correct mapping field type
     */
    public String getMappingFieldByType( List<IntegrationMappingModel> mappingModel, String targetFieldType, String targetObject) {
        
        String fieldToReturn;
        for(IntegrationMappingModel mapping : mappingModel) {

            if( mapping.dataType == targetFieldType && mapping.targetObject == targetObject) {
                fieldToReturn = mapping.targetField;
                break;
            }
        }

        return fieldToReturn;
    }

    /**
     * @description: populates fields on sObject by type. 
     * 
     * @param records - a list of records to loop over
     *
     * @param value - value to populate.
     *
     * @return List<sObject> that have the record populated.
     */
    public List<sObject> populatPfsIdOnObject( List<sObject> records, Id pfsId) {
        if( pfsId == null) return records;

        final String PFS_FIELD = 'PFS__c';
        for( sObject obj : records) {

            obj = this.populateFieldByType( obj, PFS_FIELD, pfsId);
        }

        return records;
    }

    /**
     * @description: populates a single field on an sObject
     * 
     * @param sourceField - soure field to update.
     *
     * @param value - value to populate.
     *
     * @return a single sObject that have the record populated.
     */
    public sObject populateFieldByType( sObject record, String sourceField, String value) {
        if( sourceField == null) return record;

        record.put( sourceField, value);

        return record;
    }

    /**
     * @description: Builds a collection of sObjects by a target id.
     *
     * @param: objects - a List of sObject Types
     *
     * @param: targetFieldName - the field Name in that sobject to use as the Map Key.
     *
     * @return: collection of objects by targetFieldName Id.
     */
    public Map<String, sObject> getObjectByIds( List<Sobject> objects, String targetFieldName) {
        Map<String, sObject> returnVal = new Map<String, sObject>();
        for( sObject obj : objects) {

            returnVal.put( (String)obj.get( targetFieldName), obj);
        }
        return returnVal;
    }

    /**
     * @description: Maps a List of students to applicant Sobject
     *
     * @param: dataToMap: Collection of objects to map to Applicant__c sobject
     *
     * @param: applicantMapping - List of IntegrationMappingModel for applicants
     *
     * @return: A collection of converted Objects to Applicant__c records
     */
    public List<sObject> mapStudentsToApplicant( List<Object> dataToMap, List<IntegrationMappingModel> applicantMapping) {
        List<sObject> returnVal =  super.mapRecords( dataToMap, applicantMapping, FamilyIntegrationModel.APPLICANT_OBJECT);

        return  returnVal;
    }

    /**
     * @description: Maps a List of students to Student_Folder__c Sobject
     *
     * @param: dataToMap: Collection of objects to map to Student_Folder__c sobject
     *
     * @param: applicantMapping - List of IntegrationMappingModel for applicants
     *
     * @return: A collection of converted Objects to Student_Folder__c records
     */
    public List<sObject> mapStudentsToFolders( List<Object> dataToMap, List<IntegrationMappingModel> applicantMapping) {
        List<sObject> returnVal =  super.mapRecords( dataToMap, applicantMapping, FamilyIntegrationModel.STUDENT_FOLDER_OBJECT);

        return  returnVal;
    }

    /**
     * @description: Maps a List of students to School_Pfs_Assignment__c Sobject
     *
     * @param: dataToMap: Collection of objects to map to School_Pfs_Assignment__c sobject
     *
     * @param: applicantMapping - List of IntegrationMappingModel for applicants
     *
     * @return: A collection of converted Objects to School_Pfs_Assignment__c records
     */
    public List<sObject> mapStudentToSPA( List<Object> dataToMap, List<IntegrationMappingModel> applicantMapping) {
        List<sObject> returnVal =  super.mapRecords( dataToMap, applicantMapping, FamilyIntegrationModel.SCHOOL_PFS_ASSIGN_OBJECT);

        return  returnVal;
    }
}