@isTest
private class SchoolRequestMIEIndividuallyControllTest {
    
    @isTest
    private static void isEnabledRequestMIEIndividually_PFSWithoutIndividualMIERequest_expectRequestMIEForPFS() {
        
        //Activate the MIE for the entired site.
        SchoolPortalSettings.MIE_Pilot = true;
        
        //Create current academic year.
        Academic_Year__c academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        
        //Create test School.
        Account schoolMIE1 = AccountTestData.Instance.asSchool().forName('schoolMIE1').create();
        Account aParent1 = AccountTestData.Instance.asFamily().create();
        insert new List<Account>{schoolMIE1, aParent1};
        
        //Create Annual Setting record for the school.
        Annual_Setting__c annualSettingMIE1 = AnnualSettingsTestData.Instance
            .forSchoolId(schoolMIE1.Id)
            .forAcademicYearId(academicYear.Id)
            .insertAnnualSettings();
            
        //Create parent and student.
        Contact parent1 = ContactTestData.Instance
            .asParent()
            .forAccount(aParent1.Id)
            .forEmail('aParent1@test.com')
            .forFirstName('aParent1')
            .forLastName('aParent1')
            .create();
        Contact student1 = ContactTestData.Instance.asStudent().create();
        insert new List<Contact>{parent1, student1};
        
        //Create PFS record.
        PFS__c pfsMIE1 = PfsTestData.Instance
            .forParentA(parent1.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .asPaid()
            .insertPfs();
            
        //Create applicant.
        Applicant__c applicant1 = ApplicantTestData.Instance
            .forContactId(student1.Id)
            .forPfsId(pfsMIE1.Id)
            .insertApplicant();
            
        //Create Folders
        Student_Folder__c folder1 = StudentFolderTestData.Instance.forName('Folder Applicant1')
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student1.Id)
            .forSchoolId(schoolMIE1.Id)
            .insertStudentFolder();
            
        //Create SPA record.
        School_PFS_Assignment__c spa1 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forApplicantId(applicant1.Id)
            .forSchoolId(schoolMIE1.Id)
            .forStudentFolderId(folder1.Id)
            .insertSchoolPfsAssignment();
            
        Contact staff = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(schoolMIE1.Id)
            .insertContact();
        
        User schoolPortalUserMIE;
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            
            schoolPortalUserMIE = UserTestData.Instance
                .forUsername('SchoolPortalUser@test.com')
                .forContactId(staff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId)
                .insertUser();
                
            Test.startTest();                
                // call scheduled batch Apex to process new access pfs and folders.
                Database.executeBatch(new SchoolStaffShareBatch());
            Test.stopTest();
            
            System.runAs(schoolPortalUserMIE){
                
                SchoolRequestMIEIndividuallyController controller = new SchoolRequestMIEIndividuallyController();
                controller.pfsRecord = pfsMIE1;
                System.AssertEquals(MonthlyIncomeAndExpensesService.MIE_Request_Enabled, controller.isEnabledRequestMIEIndividually);
                System.AssertEquals(true, controller.isMIERequestEnabled);
                System.AssertEquals(false, controller.isMIERequestedAndCompleted);
                System.AssertEquals(false, controller.isMIERequestedAndIncompleted);
                
                controller.requestMIEAction();
                Date d = system.today();
                System.AssertEquals(d.month() + '/' + d.day() + '/' + d.year(), controller.getMIERequestedDate());
            }
        }
    }
}