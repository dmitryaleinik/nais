global class AutoAssignChatterGroupQueueable implements Queueable {
    
    list<id> userIds;
    list<NetworkMember> membersToUpdate;

    global AutoAssignChatterGroupQueueable(list<id> userIds) {
        this.userIds = userIds;
        
    }


    global void execute(QueueableContext context) {
        set<id> communityIds = new set<id>();
        set<id> chatterGroupIds = new set<id>();

        for(ChatterGroupAutoAssign__c each : ChatterGroupAutoAssign__c.getAll().values())
            chatterGroupIds.add(each.ChatterGroupId__c);

        for(CollaborationGroup each : [SELECT NetworkId FROM CollaborationGroup WHERE Id IN :chatterGroupIds])
            communityIds.add(each.networkId);

        membersToUpdate = new list<NetworkMember>();
        for(NetworkMember each :[SELECT PreferencesDisableAllFeedsEmail
                                FROM NetworkMember 
                                WHERE NetworkId IN :communityIds AND MemberId IN :userIds])
            if(!each.PreferencesDisableAllFeedsEmail){
                each.PreferencesDisableAllFeedsEmail = true;
                membersToUpdate.add(each);
            }

        if(!membersToUpdate.isEmpty())
            update membersToUpdate;

        List<CollaborationGroupMember> chatterMembersToInsert = new List<CollaborationGroupMember>();
        // loop through list of users (already filtered in userafter trigger to be school portal profiles)
        // to add to each of the chatter groups specified in the custom settings.
        for(id u : userIds)
            for(ChatterGroupAutoAssign__c each : ChatterGroupAutoAssign__c.getAll().values())
                            if(each.enabled__c){
                                chatterMembersToInsert.add(new CollaborationGroupMember(CollaborationGroupId = each.ChatterGroupId__c, MemberId = u)); 
                            }
               database.insert(chatterMembersToInsert,false);
    }
    

    global void finish(Database.BatchableContext BC){
        for(NetworkMember each : membersToUpdate)
            each.PreferencesDisableAllFeedsEmail = false;

        update membersToUpdate;
    }
}