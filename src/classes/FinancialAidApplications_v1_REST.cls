/**
 * @description Provides access to the financial aid applications for a school during a particular academic year.
 */
@RestResource(urlMapping='/financialaid/v1/applications/')
global with sharing class FinancialAidApplications_v1_REST {

    private static final String ENDPOINT = '/financialaid/v1/applications/';

    @testvisible private static final String INVALID_REQUEST_ERROR = 'Invalid request. See errors for more details.';
    @testvisible private static final String MULTIPLE_SCHOOLS_FOUND =
            'The school-id parameter is required for users with access to multiple schools. ' +
                    'Check the schools property for the specific school you are looking for.';
    @testvisible private static final String SUCCESSFUL_REQUEST_APPLICATIONS = 'Successful request for applications.';
    @testvisible private static final String USER_ACCESS_ERROR = 'The user does not have access to the requested data. See errors for more details.';
    @testvisible private static final String GENERIC_500_ERROR = 'Unexpected error. The SSS development team has been notified.';
    @testVisible private static final String UPDATE_FAILED = 'Update Failed. Check errors for more details.';
    private static final String FIN_AID_API_LOG_SOURCE = 'Financial Aid API';

    @testVisible private static final String ACADEMIC_YEAR_REQUEST_PARAM = 'academic_year';
    @testVisible private static final String SCHOOL_ID_REQUEST_PARAM = 'school_id';
    @testVisible private static final String APPLICATION_ID_REQUEST_PARAM = 'application_id';
    @testvisible private static final String LINK_REL_FINANCIAL_AID = 'financial_aid';

    // These constants are used to provide a link to the financial aid page in our school portal.
    private static final String FIN_AID_LINK_ID_PARAM = 'id';
    private static final String FIN_AID_LINK_TAB_PARAM = 'tabName';
    private static final String FIN_AID_LINK_TAB_PARAM_VALUE = 'Financial Aid';

    /**
     *  @description - This method returns the current academic year's
     *               student folders that the user has access to.
     */
    @HttpGet
    global static void getFinancialApplications() {
        RestResponse response = RestContext.response;
        GET_Response responseBody = new GET_Response();

        try {

            responseBody.request = RestContext.request;


            GET_Request request = new GET_Request(RestContext.request);
            request.validate(response, responseBody);

            // Determine if the request has all the necessary information.
            if (!request.isValid) {
                response.responseBody = Blob.valueOf(JSON.serialize(responseBody));
                logGETRequest(responseBody, 500, IntegrationLogs.Status.ERROR.name());
                return;
            }

            // An additional check is done to see if user has access to multiple schools. If so, they are required to specify the Id of the school they are working with.
            // If a multi school user has not included the Id of a school, a 300 response is returned with all the
            // schools that user has access to so that the caller can provide the school Id in subsequent calls
            if (request.hasMultipleSchools() && request.schoolId == null) {
                responseBody.addSchools(request.schoolsForUserById);
                responseBody.Message = MULTIPLE_SCHOOLS_FOUND;
                response.responseBody = Blob.valueOf(JSON.serialize(responseBody));
                response.statusCode = 300;
                logGETRequest(responseBody, 300, IntegrationLogs.Status.WARN.name());

                return;
            } else {
                request.setAsSingleSchool();
            }

            // Mark active school here. For single school users, we will just mark the only school they have access to. For multi campus users, we rely on them to specify the school Id.
            activateApiForSchool(request.schoolId);

            responseBody.addSchool(request.schoolsForUserById.get(request.schoolId));
            responseBody.applications = getApplicationData(request);

            responseBody.message = SUCCESSFUL_REQUEST_APPLICATIONS;
            response.responseBody = Blob.valueOf(JSON.serialize(responseBody));
            response.statusCode = 200;

            logGETRequest(responseBody, 200, IntegrationLogs.Status.SUCCESS.name());

            return;
        } catch (Exception ex) {
            response.statusCode = 500;
            responseBody.message = GENERIC_500_ERROR;
            logGETException(ex, responseBody);
        }
    }

    private static List<Application> getApplicationData(GET_Request getRequest) {
        List<Student_Folder__c> studentFolders = getStudentFolders(getRequest);

        List<Application> applications = new List<Application>();

        if (studentFolders == null || studentFolders.isEmpty()) {
            return applications;
        }

        Map<Id, List<Student_Folder__c>> otherSchoolsFoldersByStudentId = getApplicationsForOtherSchools(getRequest, studentFolders);

        for (Student_Folder__c folder : studentFolders) {
            List<Student_Folder__c> studentsOtherApplications = otherSchoolsFoldersByStudentId.get(folder.Student__c);
            Application finAidApp = new Application(folder);
            finAidApp.addSchoolsFromOtherApps(studentsOtherApplications);
            applications.add(finAidApp);
        }

        return applications;
    }

    private static List<Student_Folder__c> getStudentFolders(GET_Request getRequest) {
        // Currently the GET_REQUEST constructor will query for the student folders if a single folder Id was provided in the REST request.
        // This method is just a step towards making the flow of operations more consistent regardless of the data sent to the API.
        if (getRequest.applicationId != null) {
            return getRequest.applications;
        }

        return getCurrentYearStudentFolders(getRequest.schoolId, getRequest.academicYearName);
    }

    private static Map<Id, List<Student_Folder__c>> getApplicationsForOtherSchools(GET_Request getRequest, List<Student_Folder__c> currentSchoolsFolders) {
        // First get the student Ids from the student folders we queried for the current school.
        Set<Id> studentIds = new Set<Id>();
        for (Student_Folder__c folder : currentSchoolsFolders) {
            studentIds.add(folder.Student__c);
        }

        // Now use the service class to get the folders the students have for other schools.
        return FinancialAidAppsService.Instance.getOtherSchoolAppsByStudentId(studentIds, getRequest.academicYearName, getRequest.schoolId);
    }

    private static FinancialAidApiService.Response activateApiForSchool(Id schoolId) {
        FinancialAidApiService.ActivationRequest request = new FinancialAidApiService.ActivationRequest(schoolId);

        FinancialAidApiService.Response activationResponse = FinancialAidApiService.Instance.activateApiForCurrentUsersSchool(request);

        return activationResponse;
    }

    /**
     *  @description - This method allows a user to update student folder's admission status.
     */
    @HttpPut
    global static void putFinancialApplicationStatus() {
        RestResponse response = RestContext.response;
        PUT_Response responseBody = new PUT_Response();

        try {
            // Deserialize request body, just an array applications
            PUT_Request request = (PUT_Request)JSON.deserialize(RestContext.request.requestBody.toString(), PUT_Request.class);

            // Build list of <Student_Folder__c> records to update with just the Admission_Enrollment_Status__c field.
            // validate Admission_Enrollment_Status__c values; make sure they are one of our picklist values.
            // The context class will contain the records that we would update and any errors we found while generating them.
            PUT_Context context = buildPutContext(request);

            // If we have any errors, put them on our response and send it back to the caller.
            if (!context.Errors.isEmpty()) {
                responseBody.errors = context.Errors;
                responseBody.message = UPDATE_FAILED;
                response.statusCode = 400;
                response.responseBody = Blob.valueOf(JSON.serialize(responseBody));
                logPUTRequest(responseBody, request, 400, IntegrationLogs.Status.ERROR.name());
                return;
            }

            // If we actually have records to update, do it...
            if (!context.FoldersToUpdateById.isEmpty()) {
                update context.FoldersToUpdateById.values();
            }
            // If we make it here, the update was successful, return 200!
            response.statusCode = 200;

            logPUTRequest(responseBody, request, 200, IntegrationLogs.Status.SUCCESS.name());
            return;
        } catch (DmlException ex) {
            String exceptionMessage = ex.getMessage();

            // We will determine the log status based on the type of exception. If the exception is due to sharing, the status will be error.
            // Otherwise, we will use a FATAL status since it is unexpected.
            String logStatus;

            // If the exception is because the user doesn't have access to the records, we will return a 403 response
            // since they are forbidden from completing the operation.
            // Otherwise we will return 500 since the error is likely to be because of our own logic.
            if (exceptionMessage.contains('INSUFFICIENT_ACCESS_OR_READONLY')) {
                response.statusCode = 403;
                logStatus = IntegrationLogs.Status.ERROR.name();
            } else {
                response.statusCode = 500;
                logStatus = IntegrationLogs.Status.FATAL.name();
            }

            responseBody.message = UPDATE_FAILED;
            responseBody.errors = new List<Error> { new Error(exceptionMessage) };

            response.responseBody = Blob.valueOf(JSON.serialize(responseBody));
            logPUTException(ex, responseBody, logStatus);

            return;
        } catch (Exception ex) {
            System.debug(ex.getStackTraceString());
            response.statusCode = 500;
            responseBody.message = GENERIC_500_ERROR;
            response.responseBody = Blob.valueOf(JSON.serialize(responseBody));
            logPUTException(ex, responseBody);
            return;
        }
    }

    global class GET_Response {

        global RestRequest request;
        global List<Application> applications;
        global List<School> schools;
        global List<Error> errors;
        global String message;

        global Boolean isSuccess() {
            return errors.isEmpty();
        }

        public void addError(String errorMessage) {
            if (errors == null) {
                errors = new List<Error>();
            }
            // add the error to the collection...
            errors.add(new Error(errorMessage));
        }

        public void addSchools(Map<Id, Account> schoolsById) {
            if (schools == null) {
                schools = new List<School>();
            }
            for (Account schoolAccount : schoolsById.values()) {
                schools.add(new School(schoolAccount));
            }
        }

        public void addSchool(Account school) {
            if (schools == null) {
                schools = new List<School>();
            }
            schools.add(new School(school));
        }

        public void addApplications(List<Student_Folder__c> folders) {
            if (applications == null) {
                applications = new List<Application>();
            }
            for (Student_Folder__c folder: folders) {
                applications.add(new Application(folder));
            }
        }
    }

    global class GET_Request {
        global String academicYearName;
        global Id schoolId;
        global Id applicationId;
        global Boolean isValid;

        global Map<Id, Account> schoolsForUserById;
        List<Student_Folder__c> applications;

        GET_Request(RestRequest request) {
            academicYearName = request.params.get(ACADEMIC_YEAR_REQUEST_PARAM);
            schoolId = String.isNotBlank(request.params.get(SCHOOL_ID_REQUEST_PARAM)) ? request.params.get(SCHOOL_ID_REQUEST_PARAM) : null;
            applicationId = String.isNotBlank(request.params.get(APPLICATION_ID_REQUEST_PARAM)) ? request.params.get(APPLICATION_ID_REQUEST_PARAM) : null;
            isValid = true;

            schoolsForUserById = new Map<Id, Account>(GlobalVariables.getAllSchoolsForUser());

        }

        global void validate(RestResponse response, GET_Response responseBody) {

            if (!hasValidAcademicYear()) {
                responseBody.Message = INVALID_REQUEST_ERROR;
                String academicYearErrorFormat = '{0} is not a valid academic year. Please make sure values have the correct format. For example, 2017-2018.';
                String academicYearError = String.format(academicYearErrorFormat, new List<String> { academicYearName });
                responseBody.addError(academicYearError);
                response.statusCode = 400;
            }

            if (!hasValidSchoolId()) {
                responseBody.Message = USER_ACCESS_ERROR;
                responseBody.addError('The user does not have access to the school with Id ' + schoolId + '.');
                // The request could contain the necessary information. Returning 403 because the user does not have access.
                response.statusCode = 403;
            }

            if (schoolsForUserById.isEmpty()) {
                responseBody.Message = USER_ACCESS_ERROR;
                responseBody.addError('The user does not have access to any schools.');
                response.statusCode = 403;
                isValid = false;
            }

            if (!hasValidApplicationId()) {
                responseBody.Message = INVALID_REQUEST_ERROR;
                responseBody.addError('The application with Id ' + applicationId + ' could not be found. Either it does not exist or you do not have access.');
                response.statusCode = 400;
            }
        }

        global Boolean hasMultipleSchools() {
            return (schoolsForUserById.size() > 1);
        }

        global void setAsSingleSchool() {
            if (schoolId == null) {
                schoolId = schoolsForUserById.values()[0].Id;
            }
        }

        global Boolean hasValidAcademicYear() {
            if (academicYearName == null) {
                academicYearName = GlobalVariables.getCurrentAcademicYear().Name;
            } else if (GlobalVariables.getAcademicYearByName(academicYearName) == null) {
                isValid = false;
                return false;
            }
            return true;
        }

        global Boolean hasValidApplicationId() {
            if (applicationId != null) {
                applications = getStudentFoldersById();
                if (applications.isEmpty()) {
                    isValid = false;
                    return false;
                }
            }

            return true;
        }

        global Boolean hasValidSchoolId() {
            if (schoolId != null) {
                if (!schoolsForUserById.containsKey(schoolId)) {
                    isValid = false;
                    return false;
                }
            }

            return true;
        }

        private List<Student_Folder__c> getStudentFoldersById() {
            return [SELECT School__c,
                    Academic_Year_Picklist__c,
                    Id,
                    School_Student_Id__c,
                    RavennaId__c,
                    EFC_Boarding__c,
                    EFC__c,
                    Total_EFC__c,
                    Total_Contributions__c,
                    Grant_Awarded__c,
                    Loan__c,
                    Folder_Status__c,
                    Aid_of_Tuition__c,
                    Aid_of_Total_Expenses__c,
                    Aid_of_Financial_Need__c,
                    Total_Aid__c,
                    Admission_Enrollment_Status__c,
                    Student__c,
                    Student_Ravenna_Id__c,
                    First_Name__c,
                    Last_Name__c,
                    Birthdate__c,
                    Grade_Applying__c,
                    School_NCES_Id__c,
                    First_PFS_Submission_Date__c,
                    Count_of_PFSs__c,
                    Count_of_Withdrawn_PFSs__c,
                    Student_Tuition__c,
                    Parent_Offer_to_Pay_combined__c,
                    Proposed_Award__c
            FROM Student_Folder__c
            WHERE Id =: applicationId];
        }
    }

    private static List<Student_Folder__c> getCurrentYearStudentFolders(Id accountId, String academicYearName) {
        return [SELECT School__c,
                Academic_Year_Picklist__c,
                Id,
                School_Student_Id__c,
                RavennaId__c,
                EFC_Boarding__c,
                EFC__c,
                Total_EFC__c,
                Total_Contributions__c,
                Grant_Awarded__c,
                Loan__c,
                Folder_Status__c,
                Aid_of_Tuition__c,
                Aid_of_Total_Expenses__c,
                Aid_of_Financial_Need__c,
                Total_Aid__c,
                Admission_Enrollment_Status__c,
                Student__c,
                Student_Ravenna_Id__c,
                First_Name__c,
                Last_Name__c,
                Birthdate__c,
                Grade_Applying__c,
                School_NCES_Id__c,
                First_PFS_Submission_Date__c,
                Count_of_PFSs__c,
                Count_of_Withdrawn_PFSs__c,
                Student_Tuition__c,
                Parent_Offer_to_Pay_combined__c,
                Proposed_Award__c
        FROM Student_Folder__c
        WHERE School__c =: accountId
        AND Academic_Year_Picklist__c =: academicYearName];
    }

    private static String getFinancialAidPageUrl(Id applicationId) {
        PageReference finAidPage = Page.SchoolFinancialAid;
        finAidPage.getParameters().put(FIN_AID_LINK_ID_PARAM, applicationId);
        finAidPage.getParameters().put(FIN_AID_LINK_TAB_PARAM, FIN_AID_LINK_TAB_PARAM_VALUE);

        String finAidUrl = Site.getBaseSecureUrl() + finAidPage.getUrl();

        return finAidUrl;
    }

    /**
     * @description Used to help serialize/deserialize Student_Folder__c record information passed in by callers and returned by the GET request.
     */
    global class Application {

        global Id id;

        global String school_student_id;
        global String admission_enrollment_status;
        global String ravenna_id;
        global String status;
        global String school_nces_id;
        global String student;
        global String student_ravenna_id;
        global String student_first_name;
        global String student_last_name;
        global Date student_birthdate;
        global String grade_applying;

        /**
         * @description The EFC calculation most relevant to the family based on their selection of Day/Boarding school
         *              during the application process. This attribute is populated with the Student_Folder__c.Total_EFC__c
         *              formula field which returns the correct EFC calculation based on whether the student will be
         *              attending Day or Boarding school.
         *
         *              In v0, this attribute contained Student_Folder__c.EFC__c which was misleading since we placed
         *              Student_Folder__c.Total_EFC__c in the total_estimated_family_contributions attribute implying
         *              that this was a total EFC for all the students applying for financial aid in a single household.
         */
        global Decimal estimated_family_contribution;

        global Decimal total_contributions;
        global Decimal grant_awarded;
        global Decimal loan;
        global Decimal aid_of_tuiton;
        global Decimal aid_of_total_expenses;
        global Decimal aid_of_financial_need;
        global Decimal total_aid;
        global Date submitted_date;
        global Integer number_of_households;
        global Decimal student_tuition;
        global Decimal parent_offer_to_pay;
        global Decimal proposed_award;
        global List<School> additional_schools;
        global List<Link> links;

        global Application(Student_Folder__c folderRecord) {
            id = folderRecord.id;
            school_student_id = folderRecord.School_Student_Id__c;
            admission_enrollment_status = folderRecord.Admission_Enrollment_Status__c;
            status = folderRecord.Folder_Status__c;
            ravenna_id = folderRecord.RavennaId__c;
            estimated_family_contribution = folderRecord.Total_EFC__c;
            total_contributions = folderRecord.Total_Contributions__c;
            grant_awarded = folderRecord.Grant_Awarded__c;
            loan = folderRecord.Loan__c;
            aid_of_tuiton = folderRecord.Aid_of_Tuition__c;
            aid_of_financial_need = folderRecord.Aid_of_Financial_Need__c;
            aid_of_total_expenses = folderRecord.Aid_of_Total_Expenses__c;
            total_aid = folderRecord.Total_Aid__c;
            student = folderRecord.Student__c;
            student_ravenna_id = folderRecord.Student_Ravenna_Id__c;
            school_nces_id = folderRecord.School_NCES_Id__c;
            student_last_name = folderRecord.Last_Name__c;
            student_first_name = folderRecord.First_Name__c;
            student_birthdate = folderRecord.Birthdate__c;
            grade_applying = folderRecord.Grade_Applying__c;
            submitted_date = folderRecord.First_PFS_Submission_Date__c == null ? null : folderRecord.First_PFS_Submission_Date__c.date();
            setNumberOfHouseholds(folderRecord);
            student_tuition = folderRecord.Student_Tuition__c;
            parent_offer_to_pay = folderRecord.Parent_Offer_to_Pay_combined__c;
            proposed_award = folderRecord.Proposed_Award__c;
            additional_schools = new List<School>();
            links = new List<Link>();
            
            Link appLink = new Link();
            appLink.rel = LINK_REL_FINANCIAL_AID;
            appLink.href = getFinancialAidPageUrl(folderRecord.Id);

            links.add(appLink);
        }

        private void setNumberOfHouseholds(Student_Folder__c folderRecord) {
            // Only set the number of households if none of the numbers are null.
            if (folderRecord.Count_of_PFSs__c != null && folderRecord.Count_of_Withdrawn_PFSs__c != null) {
                number_of_households = Integer.valueOf(folderRecord.Count_of_PFSs__c) - Integer.valueOf(folderRecord.Count_of_Withdrawn_PFSs__c);
            }
        }

        // When we get to updating the folders we could even something crazy like...
        public Student_Folder__c toSObject() {
            return new Student_Folder__c(Id = id, Admission_Enrollment_Status__c = admission_enrollment_status);
        }

        /**
         * @description Checks this instance for any errors that would prevent us from updating the corresponding Student_Folder__c record in our system.
         */
        public List<Error> validate() {
            List<Error> errors = new List<Error>();

            if (isAdmissionEnrollmentInvalid()) {
                String acceptedValues = String.join(new List<String> (getAdmissionEnrollmentValues()), ', ');
                String errorMessageFormat = 'Error for application {0}. {1} is not a valid value for Admission Enrollment Status. Valid values are: {2}';
                String errorMessage = String.format(errorMessageFormat, new List<String> { id, admission_enrollment_status, acceptedValues });
                errors.add(new Error(errorMessage));
            }

            return errors;
        }

        /**
         * @description Used to add information for the other schools that this student is applying to.
         * @param folders The folders from other schools.
         */
        public void addSchoolsFromOtherApps(List<Student_Folder__c> folders) {
            if (folders == null || folders.isEmpty()) {
                return;
            }

            if (additional_schools == null) {
                additional_schools = new List<School>();
            }

            for (Student_Folder__c folder : folders) {
                additional_schools.add(new School(folder.School__r));
            }
        }

        private Boolean isAdmissionEnrollmentInvalid() {
            return String.isBlank(this.admission_enrollment_status) || !getAdmissionEnrollmentValues().contains(this.admission_enrollment_status.toLowerCase());
        }
    }

    /**
     * @description Used to represent a single school. Helps with JSON serialization.
     */
    global class School {

        global String id;
        global String name;
        global String nces_id;

        global School(Account schoolAccount) {
            id = schoolAccount.Id;
            name = schoolAccount.Name;
            nces_id = schoolAccount.NCES_ID__c;
        }
    }

    global class Error {

        global String message;

        global Error(String errMessage) {
            message = errMessage;
        }
    }

    global class Link
    {
        global String rel;
        global String href;
    }

    /**
     * @description Used to deserialize the JSON
     */
    global class PUT_Request {

        global List<Application> applications;
    }

    /**
     * @description Used to serialize the JSON for the response delivered for PUT requests.
     */
    global class PUT_Response {

        global String message;
        global List<Error> errors;
    }

    /**
     * @description Used to contain the data that needs to be updated and store errors we encounter while validating the request.
     */
    private class PUT_Context {

        /**
         * @description The folders that need to be updated by record Id.
         */
        public Map<Id, Student_Folder__c> FoldersToUpdateById {
            get {
                if (FoldersToUpdateById == null) {
                    FoldersToUpdateById = new Map<Id, Student_Folder__c>();
                }
                return FoldersToUpdateById;
            }
            private set;
        }

        /**
         * @description The list of any errors found while validating the folders to update. At this time, the errors will just be used to identify records with invalid Admission_Enrollment_Status__c values.
         */
        public List<Error> Errors {
            get {
                if (Errors == null) {
                    Errors = new List<Error>();
                }
                return Errors;
            }
            private set;
        }
    }

    private static PUT_Context buildPutContext(PUT_Request request) {
        PUT_Context context = new PUT_Context();

        // If we don't have any applications, return early.
        if (request.applications == null || request.applications.isEmpty()) {
            return context;
        }

        // Iterate through each application, make sure the new admission enrollment status is one of the Admission_Enrollment_Status__c picklist values.
        // For valid applications, add the record to the list to update.
        for (Application finAidApp : request.applications) {
            List<Error> errors = finAidApp.validate();

            if (errors == null || errors.isEmpty()) {
                context.FoldersToUpdateById.put(finAidApp.id, finAidApp.toSObject());
            } else {
                context.Errors.addAll(errors);
            }
        }

        return context;
    }

    // Cache for the Admission_Enrollment_Status__c picklist values since describe calls are expensive so we only want to do this once.
    private static Set<String> admissionEnrollmentValues;
    private static Set<String> getAdmissionEnrollmentValues() {
        if (admissionEnrollmentValues != null) {
            return admissionEnrollmentValues;
        }

        admissionEnrollmentValues = new Set<String>();

        DescribeFieldResult fieldDescribe = Student_Folder__c.Admission_Enrollment_Status__c.getDescribe();

        // Store all the picklist values in lower case so we can make a case insensitive comparison later on using the set.
        // We will convert the value we are checking for to lower case as well.
        for (Schema.PickListEntry picklist : fieldDescribe.getPicklistValues()) {
            admissionEnrollmentValues.add(picklist.getValue().toLowerCase());
        }

        return admissionEnrollmentValues;
    }

    /**
     * @description Populates information on an IntegrationLog__c record from the specified exception, original
     *              RestRequest passed into Salesforce, and the current state of the response we were building.
     */
    private static IntegrationLog__c logGETException(Exception exceptionToLog, GET_Response response) {
        IntegrationLog__c newLog = new IntegrationLog__c();

        String stackTrace = exceptionToLog.getStackTraceString();
        String message = exceptionToLog.getMessage();
        newLog.Errors__c = 'Exception Message: ' + ( String.isNotBlank(message) ? message: 'N/A') +
                ' Stack Trace: ' + ( String.isNotBlank(stackTrace) ? stackTrace : 'N/A');

        newLog.Source__c = FIN_AID_API_LOG_SOURCE;
        newLog.Http_Method__c = response.request.httpMethod;
        newLog.Endpoint__c = response.request.requestURI;
        newLog.Raw_Request__c = String.valueOf(response.request);
        newLog.Raw_Response__c = String.valueOf(response);
        newLog.Body__c = String.valueOf(response.request.requestBody);
        newLog.Parameters__c = JSON.serialize(response.request.params);
        newLog.Status__c = IntegrationLogs.Status.FATAL.name();
        insert newLog;

        return newLog;
    }

    private static void logPUTRequest(PUT_Response response, PUT_Request request, Integer statusCode, String status) {
        IntegrationLog__c newLog = new IntegrationLog__c();
        newLog.Raw_Request__c = String.valueOf(request);
        newLog.Raw_Response__c = String.valueOf(response);
        newLog.Endpoint__c = ENDPOINT;
        newLog.Source__c = FIN_AID_API_LOG_SOURCE;
        newLog.Body__c = String.valueOf(request.applications);
        newLog.Http_Method__c = 'PUT';
        newLog.Response_Code__c = statusCode;
        newLog.Status__c = status;
        newLog.Errors__c = String.valueOf(response.errors);

        insert newLog;
    }

    private static void logGETRequest(GET_Response response, Integer statusCode, String status) {
        IntegrationLog__c newLog = new IntegrationLog__c();
        newLog.Raw_Request__c = String.valueOf(response.request);
        newLog.Raw_Response__c = String.valueOf(response);
        newLog.Endpoint__c = response.request.requestURI;
        newLog.Source__c = FIN_AID_API_LOG_SOURCE;
        newLog.Http_Method__c = 'GET';
        newLog.Body__c = String.valueOf(response.request.requestBody);
        newLog.Response_Code__c = statusCode;
        newLog.Parameters__c = JSON.serialize(response.request.params);
        newLog.Status__c = status;
        newLog.Errors__c = String.valueOf(response.errors);

        insert newLog;
    }

    private static void logPUTException(Exception exceptionToLog, PUT_Response response) {
        logPUTException(exceptionToLog, response, IntegrationLogs.Status.FATAL.name());
    }

    private static void logPUTException(Exception exceptionToLog, PUT_Response response, String logStatus) {
        IntegrationLog__c newLog = new IntegrationLog__c();

        String stackTrace = exceptionToLog.getStackTraceString();
        String message = exceptionToLog.getMessage();
        newLog.Errors__c = 'Exception Message: ' + ( String.isNotBlank(message) ? message: 'N/A') +
                ' Stack Trace: ' + ( String.isNotBlank(stackTrace) ? stackTrace : 'N/A');

        newLog.Source__c = FIN_AID_API_LOG_SOURCE;
        newLog.Endpoint__c = ENDPOINT;
        newLog.Http_Method__c = 'PUT';
        newLog.Raw_Response__c = String.valueOf(response);
        newLog.Status__c = logStatus;
        insert newLog;
    }
}