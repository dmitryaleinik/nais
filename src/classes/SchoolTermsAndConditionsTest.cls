@isTest
private class SchoolTermsAndConditionsTest {

    private static User testUser;
    private static Help_Configuration__c testHelp;
    private static User sysAdminUser = [select Id from User where Id = :UserInfo.getUserId()];
    private static Account testAcct;

    private static void setupData(){
        setupData(GlobalVariables.schoolPortalAdminProfileId);
    }

    // [CH] NAIS-1766 Adding to help avoid Mixed DML Errors
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

    private static void setupData(Id portalProfileId){
        testAcct = TestUtils.createAccount('name', RecordTypes.schoolAccountTypeId, 1, true);
        Contact testCon = TestUtils.createContact('name', testAcct.Id, RecordTypes.schoolStaffContactTypeId, true);

        testUser = TestUtils.createPortalUser('name', 'drew@drew.dru123', 'dru123', testCon.Id, portalProfileId, true, false);

        System.runAs(thisUser){
            insert testUser;
        }

        testUser = [Select Id, SYSTEM_Terms_and_Conditions_Accepted__c from User where Id = :testUser.Id];

        Academic_Year__c testAY = testUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        //testAY.Start_Date__c = System.today().addYears(-1);
        //testAY.End_Date__c = System.today().addYears(1);
        //insert testAY;

        testHelp = testUtils.createHelp(testAY.Id, false);
        testHelp.School_Terms_and_Conditions_Date_Time__c = System.now().addDays(-100);
        insert testHelp;
    }

    @isTest
    private static void testUnAccepted(){
        setupData();

        System.assertEquals(null, testUser.SYSTEM_Terms_and_Conditions_Accepted__c);

        // test redirect
        System.runAs(testUser){
            SchoolDashboardController roller = new SchoolDashboardController();
            PageReference pr = roller.init();
            System.assert(pr.getURL().contains('schooltermsandconditions'));
            System.assert(pr.getURL().toLowerCase().contains('inschoolportal'));

        }

        // test accept and cancel
        System.runAs(testUser){
            SchoolTermsAndConditions roller = new SchoolTermsAndConditions();
            roller.accept();

            testUser = [Select Id, SYSTEM_Terms_and_Conditions_Accepted__c from User where Id = :testUser.Id];
            System.assertEquals(null, testUser.SYSTEM_Terms_and_Conditions_Accepted__c);

            roller.checkbox = true;
            roller.accept();

            testUser = [Select Id, SYSTEM_Terms_and_Conditions_Accepted__c from User where Id = :testUser.Id];
            System.assertNotEquals(null, testUser.SYSTEM_Terms_and_Conditions_Accepted__c);
            System.assert(testUser.SYSTEM_Terms_and_Conditions_Accepted__c > System.now().addDays(-1));

            roller.cancel();
        }
    }

    @isTest
    private static void testExpired()
    {
        System.runAs(sysAdminUser){
            setupData();
            testUser.SYSTEM_Terms_and_Conditions_Accepted__c = System.now();
            update testUser;
        }

        // test redirect
        System.runAs(testUser){
            SchoolDashboardController roller = new SchoolDashboardController();
            System.assertEquals(true, roller.isSubscriptionExpired);
            PageReference pr = roller.init();
            System.assert(pr.getURL().toLowerCase().contains('schoolrenewal'));
        }
    }

    @isTest
    private static void testAccepted()
    {
        System.runAs(sysAdminUser){
            setupData();
            testUser.SYSTEM_Terms_and_Conditions_Accepted__c = System.now();
            update testUser;
        }
        //Since, criteria-based sharing cannot be tested using Apex: https://goo.gl/FoZXEh
        List<Annual_Setting__c> ann = GlobalVariables.getCurrentAnnualSettings(
                true,
                datetime.now().date(),
                GlobalVariables.getCurrentAcademicYear().Id,
                testAcct.Id);
        ann[0].Send_PFS_Reminders__c = true;
        ann[0].Send_Prior_Year_Document_Reminders__c = true;
        ann[0].Send_Current_Year_Document_Reminders__c  = true;
        ann[0].Annual_Settings_Updated__c = System.today();
        update ann[0];

        // test redirect
        System.runAs(testUser){
            SchoolDashboardController roller = new SchoolDashboardController();
            roller.isSubscriptionExpired = false; // [DP] 05.06.2015 fake subscription
            PageReference pr = roller.init();
            System.assertEquals(null, pr);
        }
    }

    @isTest
    private static void testReAccept(){
        setupData();

        System.runAs(sysAdminUser){
            testUser.SYSTEM_Terms_and_Conditions_Accepted__c = System.now().addDays(-10);
            update testUser;
        }
        //Since, criteria-based sharing cannot be tested using Apex: https://goo.gl/FoZXEh
        List<Annual_Setting__c> ann = GlobalVariables.getCurrentAnnualSettings(
                true,
                datetime.now().date(),
                GlobalVariables.getCurrentAcademicYear().Id,
                testAcct.Id);
        ann[0].Send_PFS_Reminders__c = true;
        ann[0].Send_Prior_Year_Document_Reminders__c = true;
        ann[0].Send_Current_Year_Document_Reminders__c  = true;
        ann[0].Annual_Settings_Updated__c = Date.newInstance(System.now().year(), System.now().month(), System.now().addDays(-1).day());
        update ann[0];

        // test redirect
        System.runAs(testUser){
            SchoolDashboardController roller = new SchoolDashboardController();
            roller.isSubscriptionExpired = false; // [DP] 05.06.2015 fake subscription
            PageReference pr = roller.init();
            System.assertEquals(null, pr);

        }

        testHelp.School_Terms_and_Conditions_Date_Time__c = System.now();
        update testHelp;

        // test redirect
        System.runAs(testUser){
            SchoolDashboardController roller = new SchoolDashboardController();
            PageReference pr = roller.init();
            System.assert(pr.getURL().contains('schooltermsandconditions'));
            System.assert(pr.getURL().toLowerCase().contains('inschoolportal'));
        }

        // test accept
        System.runAs(testUser){
            SchoolTermsAndConditions roller = new SchoolTermsAndConditions();
            roller.accept();

            testUser = [Select Id, SYSTEM_Terms_and_Conditions_Accepted__c from User where Id = :testUser.Id];
            System.assert(System.now().addHours(-1) > testUser.SYSTEM_Terms_and_Conditions_Accepted__c);

            roller.checkbox = true;
            roller.accept();

            testUser = [Select Id, SYSTEM_Terms_and_Conditions_Accepted__c from User where Id = :testUser.Id];
            System.assertNotEquals(null, testUser.SYSTEM_Terms_and_Conditions_Accepted__c);
            System.assert(System.now().addHours(-1) < testUser.SYSTEM_Terms_and_Conditions_Accepted__c);
        }

        // test redirect
        System.runAs(testUser){
            SchoolDashboardController roller = new SchoolDashboardController();
            roller.isSubscriptionExpired = false; // [DP] 05.06.2015 fake subscription
            PageReference pr = roller.init();
            System.assertEquals(null, pr);
        }
    }

    @isTest
    private static void testReAcceptFAMILYUSER(){
        TestUtils.createDataForIndividualAccountModel();

        System.runAs(sysAdminUser){
            setupData(GlobalVariables.familyPortalProfileId);
            testUser.SYSTEM_Terms_and_Conditions_Accepted__c = System.now().addDays(-10);
            update testUser;
        }

        // test redirect
        System.runAs(testUser){
            FamilyDashboardController roller = new FamilyDashboardController(new FamilyTemplateController());
            PageReference pr = roller.pageLoad();
            //A new PFS was inserted and a redirect is made to load the new record in the FamilyDashboardController
            System.assertEquals('/apex/familydashboard', pr.getURL());
        }

        testHelp.NAIS_Terms_and_Conditions_Date_Time__c = System.now();
        update testHelp;

        // test redirect
        System.runAs(testUser){
            FamilyDashboardController roller = new FamilyDashboardController(new FamilyTemplateController());
            PageReference pr = roller.pageLoad();
            System.assert(pr.getURL().contains('familytermsandconditions'));
        }

        // test accept
        System.runAs(testUser){
            SchoolTermsAndConditions roller = new SchoolTermsAndConditions();
            roller.accept();

            testUser = [Select Id, SYSTEM_Terms_and_Conditions_Accepted__c from User where Id = :testUser.Id];
            System.assert(System.now().addHours(-1) > testUser.SYSTEM_Terms_and_Conditions_Accepted__c);

            roller.checkbox = true;
            roller.accept();

            testUser = [Select Id, SYSTEM_Terms_and_Conditions_Accepted__c from User where Id = :testUser.Id];
            System.assertNotEquals(null, testUser.SYSTEM_Terms_and_Conditions_Accepted__c);
            System.assert(System.now().addHours(-1) < testUser.SYSTEM_Terms_and_Conditions_Accepted__c);
        }

        // test redirect
        System.runAs(testUser){
            FamilyDashboardController roller = new FamilyDashboardController(new FamilyTemplateController());
            PageReference pr = roller.pageLoad();
            System.assertEquals('/apex/familydashboard', pr.getURL());
        }
    }

    @isTest
    private static void isKSUser(){
        setupData();

        TestDataFamily tdf = new TestDataFamily();
        testUser = tdf.familyPortalUser;

        // fake the context of the user
        GlobalVariables.u = testUser;
        GlobalVariables.lockCurrentUser = true;

        SchoolPortalSettings.KS_School_Account_Id = tdf.school1.Id;

        List<School_PFS_Assignment__c> assignments;

        System.assertEquals(true, ApplicationUtils.IsKSSchoolUserStatic());
    }
}