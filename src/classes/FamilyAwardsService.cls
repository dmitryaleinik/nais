public class FamilyAwardsService {
    
    /* The map of Stundent_Folder__c.Folder_Status__c with its equivalet label that will be shown in "FP > Awards" */
    public static Map<String, String> FOLDER_STATUS_AWARDED = new Map<String, String>{
        'Award Approved' => Label.Award_Approved,
        //'Appealed' => Label.Appealed,
        'Complete, Award Denied' => Label.Complete_Award_Denied,
        'Complete, Award Accepted' => Label.Complete_Award_Accepted,
        'Complete, Award Refused' => Label.Complete_Award_Refused
    };
    
    /* The map of Stundent_Folder__c.Folder_Status__c with its equivalet message that will be shown in "FP > Awards" */
    public static Map<String, String> FOLDER_STATUS_AWARDED_MESSAGES = new Map<String, String>{
        'Award Approved' => null,
        //'Appealed' => Label.Appealed_Message,
        'Complete, Award Denied' => Label.Complete_Award_Denied_Message,
        'Complete, Award Accepted' => Label.Complete_Award_Accepted_Message,
        'Complete, Award Refused' => Label.Complete_Award_Refused_Message
    };

    public static Map<String, String>  PARENT_INFO_FIELDS = new Map<String, String>
    {
        'Complete, Award Denied' => 'Award_Denied_By__c',
        'Complete, Award Accepted' => 'Award_Accepted_By__c'
    };

    public static Map<String, String> ACTION_DATE_FIELDS = new Map<String, String>{
        'Complete, Award Denied' => 'Award_Denied_Date__c',
        'Complete, Award Accepted' => 'Award_Accepted_Date__c'
    };

    public  Map<Id, Student_Folder__c> foldersMap {get; set;}
    
    private FamilyAwardsService() { }//End-Constructor
    
    /**
    * @description Determines if the "Insert Award Link" button must be shown in SP > Mass Email Advanced ckEditor, 
    *              for the current school.
    * @return True, if the button must be shown. Otherwise, false.
    */
    public Boolean isAwardLinkEnabledForCurrentSchool() {
        
        return FeatureToggles.isEnabled('Allow_Mass_Email_Per_Applicant__c')
                && GlobalVariables.getCurrentSchool().Family_Award_Acknowledgement_Enabled__c;
    }//End:isAwardLinkEnabledForCurrentSchool
    
    /**
    * @description Returns true if the "Award" tab must be visible for the current PFS in FP.
    *              We only want this feature available for schools that have paid for it. Lets 
    *              make the tab hidden if none of the schools the family applied to has the 
    *              Family_Award_Acknowledgement_Enabled__c field checked or the Mass Email Award Letter 
    *              feature toggle is off.
    * @param pfsId The PFS Id for which we want to show the "Awards" tab.
    * @return True, if the "Awards" tab must be visible in FP. Otherwise, false.
    */
    public Boolean showAwardsTabForPFS(Id pfsId)
    {
        if (!FeatureToggles.isEnabled('Allow_Mass_Email_Per_Applicant__c')) { return false; }

        // TODO+ SFP-1750 This query checks to see if any of the folders currently awards available for accepting/denying. Should we just check to see if the parent applied to any schools that paid for this feature or should this tab always be visible and then the Awards page itself just displays awards that are ready for accepting/rejecting.
        //Done
        List<School_PFS_Assignment__c> spfsas = SchoolPfsAssignmentsSelector.Instance.selectSPFSASForPFSWithSchoolFamAwardEnabled(
            pfsId, FamilyAwardsService.Instance.getSPAFieldsToQuery());
        
        return !spfsas.isEmpty();
    }//End:showAwardsTabForPFS
    
    /**
    * @description Defines the list of fields to query from School_PFS_Assignment__c.
    * @return The list of fields to query from School_PFS_Assignment__c.
    */ 
    @testVisible private List<String> getSPAFieldsToQuery() {
        
        return new List<String>{
                'Id', 
                'Applicant_ID__c',
                'Applicant_First_Name__c',
                'Applicant_Last_Name__c',
                'Student_Folder__c',
                'School_ID__c',
                'School_Family_Award_Acknowledgement__c',
                'Withdrawn__c',
                'Student_Folder__r.Id',
                'Student_Folder__r.School_Name__c',
                'Student_Folder__r.Folder_Status__c',
                'Student_Folder__r.Grant_Awarded__c',
                'Student_Folder__r.Award_Accepted_By__c',
                'Student_Folder__r.Award_Accepted_Date__c',
                'Student_Folder__r.PFS1_Parent_A__c',
                'Student_Folder__r.Award_Denied_Date__c',
                'Student_Folder__r.Award_Denied_By__c',
                'Student_Folder__r.School__c',
                'Student_Folder__r.Unmet_Financial_Need__c'

            };
    }//End:getSPAFieldsToQuery
    

    /**
    * @description Send an email to School's SSS Main Contact after a FP user Accept/Decline an award.
    * @param folderId The id of the StudentFolder for which the award belongs.
    * @param schoolId The id of the StudentFolder.School__c for which the award belongs.
    * @param emailTemplateName The name of the email template to be used.
    */
    @future public static void sendActionEmail(Id folderId, Id schoolId, String emailTemplateName) {

        if (folderId == null || schoolId == null) { return; }
        // TODO SFP-1750 We need to be careful of how many emails we'd be sending with this feature. Sending them through SendGrid is probably the best way but even then we have to make sure we have enough emails or we need to pay for increased sendgrid usage.
        // TODO SFP-1750 Although, using the contact id as the target object Id might avoid any limits. Just confirm.
        // If I understand correctly, we already use the method you suggested and ContactId as targetId

        Id emailTemplateId = SingleEmailService.Instance.getEmailTemplate(emailTemplateName);

        if (emailTemplateId == null) { return; }

        List<Contact> contacts = ContactSelector.Instance.selectSSSMainContactEmail(schoolId);
        
        String mainContactId = !contacts.isEmpty() ? contacts[0].Id : null;

        if (mainContactId == null) { return; }

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(mainContactId);
        mail.setTemplateId(emailTemplateId);
        mail.setWhatId(folderId);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }//End:sendActionEmail



    public List<ApplicantsWithAwardsWrapper> getApplicantsWithAwards(Id pfsId, Id folderId)
    {
        if (!FeatureToggles.isEnabled('Allow_Mass_Email_Per_Applicant__c'))
        {
            return null;
        }

        List<School_PFS_Assignment__c> spfsaWithAwards = SchoolPfsAssignmentsSelector.Instance.selectAwards(pfsId,
                 folderId, FamilyAwardsService.Instance.getSPAFieldsToQuery());

        foldersMap = getFoldersByIds(spfsaWithAwards);

        Map<Id, List<Student_Folder__c>> foldersByApplicantId = getFoldersByApplicantId(spfsaWithAwards);
        Map<Id, String> applicantNameById = getApplicantNamesById(spfsaWithAwards);

        List<ApplicantsWithAwardsWrapper> applicantsInfo = new List<ApplicantsWithAwardsWrapper>();
        for (Id applicantId : foldersByApplicantId.keySet())
        {
            applicantsInfo.add(new ApplicantsWithAwardsWrapper(applicantNameById.get(applicantId), foldersByApplicantId.get(applicantId)));
        }

        return applicantsInfo;
    }

    private Map<Id, Student_Folder__c> getFoldersByIds(List<School_PFS_Assignment__c> spfsas)
    {
        Map<Id, Student_Folder__c> foldersMap = new Map<Id, Student_Folder__c>();
        for (School_PFS_Assignment__c spfsa : spfsas)
        {
            foldersMap.put(spfsa.Student_Folder__r.Id, spfsa.Student_Folder__r);
        }
        return foldersMap;

    }

    private Map<Id, List<Student_Folder__c>> getFoldersByApplicantId(List<School_PFS_Assignment__c> spfsas)
    {
        Map<Id, List<Student_Folder__c>> foldersByApplicantId = new Map<Id, List<Student_Folder__c>>();
        for (School_PFS_Assignment__c spfsa : spfsas)
        {
            Id currentApplicant = spfsa.Applicant_ID__c;
            List<Student_Folder__c> foldersOfApplicant;

            if (foldersByApplicantId.containsKey(currentApplicant))
            {
                foldersOfApplicant = foldersByApplicantId.get(spfsa.Applicant_ID__c);
            }
            else
            {
                foldersOfApplicant = new List<Student_Folder__c>();
            }

            foldersOfApplicant.add(spfsa.Student_Folder__r);
            foldersByApplicantId.put(currentApplicant, foldersOfApplicant);
        }
        return foldersByApplicantId;
    }

    private Map<Id, String> getApplicantNamesById(List<School_PFS_Assignment__c> spfsas)
    {
        Map<Id, String> applicantNamesById = new Map<Id, String>();
        for (School_PFS_Assignment__c spfsa : spfsas)
        {
            applicantNamesById.put(spfsa.Applicant_ID__c, spfsa.Applicant_First_Name__c + ' ' + spfsa.Applicant_Last_Name__c);
        }
        return applicantNamesById;
    }

    /**
    * @description Singleton instance of the FamilyAwardsService.
    */
    public static FamilyAwardsService Instance {
        get {
            if (Instance == null) {
                Instance = new FamilyAwardsService();
            }
            return Instance;
        }
        private set;
    }
    
    /** 
    * @description Used on VF Page apex:repeat to list the awards for a given applicant.
    */
    public class ApplicantsWithAwardsWrapper {
        
        public String applicantName { get; set; }
        public List<AwardsWrapper> awards { get; set; }//The list of awards for the given applicant
        
        public ApplicantsWithAwardsWrapper(String applicantName, List<Student_Folder__c> folders) {
            
            this.applicantName = applicantName;
            this.awards = new List<AwardsWrapper>();
            
            for (Student_Folder__c folder : folders) {
                
                awards.add(new AwardsWrapper(folder));
            }
        }//End-Constructor
        
    }//End-ApplicantsWithAwardsWrapper
    
    /** 
    * @description Used on VF Page apex:repeat to list the details of the awards for a given applicant.
    */
    public class AwardsWrapper {

        public Id folderId { get; set; }
        public String SchoolName { get; set; }
        public Decimal amountAwarded { get; set; }
        public Decimal unmetFinancialNeed { get; set; }
        public String status { get; set; }
        public String infoMessage { get; set; }

        public AwardsWrapper(Student_Folder__c folder) {
            
            folderId = folder.Id;
            SchoolName = folder.School_Name__c;
            amountAwarded = folder.Grant_Awarded__c;
            unmetFinancialNeed = folder.Unmet_Financial_Need__c;
            status = FOLDER_STATUS_AWARDED.get(folder.Folder_Status__c);
            infoMessage = getInfoMessage(folder);
            
        }//End-Constructor

        // TODO SFP-1750 This class could contain the logic for accepting/denying the award and return early if getShowAcceptButton()/DenyButton() was false.
        // I moved that logic to FamilyAwardService class.
        // I'm not sure that we should care about the additional check, because we call this method only in Accept and Deny buttons
        /**
        * @description Determines if the "Accept" button must be visible for the current award record.
        */
        public Boolean getShowAcceptAndDenyButton()
        {
            return status == Label.Award_Approved;
        }

        /**
        * @description Used to define the info message that must be shown along with the award detail,
        *              based on its StudentFolder.Folder_Status__c.
        * @param folder The Student_Folder__c record of the current award.
        */
        private String getInfoMessage(Student_Folder__c folder)
        {
            String folderStatus = folder.Folder_Status__c;
            String message;
            if (PARENT_INFO_FIELDS.keySet().contains(folderStatus))
            {
                String parentActionInfoField = PARENT_INFO_FIELDS.get(folderStatus);
                String parentName = (String) folder.get(parentActionInfoField);

                String actionDateField = ACTION_DATE_FIELDS.get(folderStatus);
                Date d = (Date) folder.get(actionDateField);

                String dateStr = d == null ? '' : d.month() + '/' + d.day() + '/' + d.year();

                message = FOLDER_STATUS_AWARDED_MESSAGES.get(folder.Folder_Status__c);

                List<String> filters = new List<String>
                {
                        parentName, dateStr
                };

                //+ TODO SFP-1750 we should use String.format and date.format for this.
                //Done
                //+ TODO SFP-1750 we can probably avoid creating those maps each time by using static final maps that specify the field to use or just have this wrapper class make a decision and retrieve the values dynamically. For example, getDecisionDate() getDecisionMadeBy()
                //Done
                message = String.format(message, filters);
            }
            return message;
        }
    }


    public void acceptAward(Student_Folder__c folder)
    {
        String currentUserName = GlobalVariables.getCurrentUser().Contact.Name;

        if (folder != null)
        {
            folder.Folder_Status__c = 'Complete, Award Accepted';

            folder.put('Award_Accepted_Date__c', system.today());
            folder.put('Award_Accepted_By__c', currentUserName);

            update folder;

            FamilyAwardsService.sendActionEmail(folder.Id, folder.School__c, 'Family Accepted Award');
        }
    }

    public void denyAward(Student_Folder__c folder)
    {
        String currentUserName = GlobalVariables.getCurrentUser().Contact.Name;

        if (folder != null)
        {
            folder.Folder_Status__c = 'Complete, Award Denied';
            folder.put('Award_Denied_Date__c', system.today());
            folder.put('Award_Denied_By__c', currentUserName);

            update folder;

            FamilyAwardsService.sendActionEmail(folder.Id, folder.School__c, 'Family Denied Award');
        }
    }

    public class FamilyAwardsServiceException extends Exception {}
}