@isTest
private class SchoolPFSCommentsTest {

      public static Account school1;
      public static School_PFS_Assignment__c spa1;
      public static Student_Folder__c studentFolder11;
      public static PFS__c pfsA1;
      public static Id academicYearIdTmp;

        public static void createTestData() {

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        academicYearIdTmp = academicYearId;

        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, true);
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, true);

        studentFolder11 = TestUtils.createStudentFolder('Student Folder 1.1', academicYearId, student1.Id, true);
        school1 = TestUtils.createAccount('Test School 1', RecordTypes.schoolAccountTypeId, 3, true);

        pfsA1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, true);

        Business_Farm__c b1 = TestUtils.createBusinessFarm(pfsa1.id, 'test', 'Business', 'Partnership', true);
        Business_Farm__c b2 = TestUtils.createBusinessFarm(pfsa1.id, 'test2', 'Business', 'Corporation', true);
        Business_Farm__c b3 = TestUtils.createBusinessFarm(pfsa1.id, 'test', 'Farm', 'Sole Proprietorship', true);
        

        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA1.Id, true);
        spa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder11.Id, true);


        spa1.Comment_Bank_Accounts__c= 'test';
        spa1.Comment_Capital_Gain_Loss__c= 'test'; 
        spa1.Comment_Cash_Support_Gift_Income__c= 'test';
        spa1.Comment_Child_Support_Received__c= 'test'; 
        spa1.Comment_COLA_Value__c= 'test';
        spa1.Comment_Current_Tax_Return_Status__c= 'test';
        spa1.Comment_Deductible_Part_Self_Employ_Tax__c= 'test';
        spa1.Comment_Dependent_Children__c= 'test';
        spa1.Comment_Depreciation_Schedule_E__c= 'test';
        spa1.Comment_Depreciation_Sec_179_Exp_4562__c= 'test';
        spa1.Comment_Depreciation_Sec_179_Exp_Sch_C__c= 'test'; 
        spa1.Comment_Depreciation_Sec_179_Exp_Sch_F__c= 'test';
        spa1.Comment_Dividend_Income__c= 'test';
        spa1.Comment_Earned_Income_Credits__c= 'test';
        spa1.Comment_Family_Size__c= 'test'; 
        spa1.Comment_Federal_Income_Tax_Revision__c= 'test';
        spa1.Comment_Filing_Status__c= 'test';
        spa1.Comment_Food_Housing_Other_Allowance__c= 'test';
        spa1.Comment_Home_Bus_Expense_Sch_C__c= 'test'; 
        spa1.Comment_Home_Market_Value__c= 'test';
        spa1.Comment_Home_Purchase_Price__c= 'test';
        spa1.Comment_Home_Purchase_Year__c= 'test';
        spa1.Comment_Impute_Rate__c= 'test';
        spa1.Comment_Income_Earned_Abroad__c= 'test';
        spa1.Comment_Income_Tax_Exemptions__c= 'test';
        spa1.Comment_Interest_Income__c= 'test';
        spa1.Comment_Investments__c= 'test';
        spa1.Comment_IRA_Deduction__c= 'test';
        spa1.Comment_IRA_Distributions__c= 'test'; 
        spa1.Comment_Itemized_Deductions__c= 'test';
        spa1.Comment_Medical_Dental_Expense__c= 'test';
        spa1.Comment_Med_Tax_Allowance_Calc_Revision__c= 'test';
        spa1.Comment_Num_Children_in_Tuition_Schools__c= 'test';
        spa1.Comment_Other_Adjustments_to_Income__c= 'test';
        spa1.Comment_Other_Gains_Losses__c= 'test';
        spa1.Comment_Other_Income__c= 'test';
        spa1.Comment_Other_Real_Estate_Market_Value__c= 'test'; 
        spa1.Comment_Other_Real_Estate_Unpaid_Princip__c= 'test';
        spa1.Comment_Other_Support_fr_Divorced_Spouse__c= 'test';
        spa1.Comment_Other_Taxable_Income__c= 'test';
        spa1.Comment_Other_Untaxed_Income__c= 'test';
        spa1.Comment_Parent_State__c= 'test'; 
        spa1.Comment_Pensions_and_Annuities__c= 'test';
        spa1.Comment_Pre_Tax_Fringe_Benefit_Contr__c= 'test';
        spa1.Comment_Pre_Tax_Paymnts_to_Retirement__c= 'test'; 
        spa1.Comment_Prof_Judgment_Minimum_Income_Amt__c= 'test';
        spa1.Comment_Rental_Real_Estate_Etc__c= 'test';
        spa1.Comment_Salary_Wages_Parent_A__c= 'test';
        spa1.Comment_Salary_Wages_Parent_B__c= 'test';
        spa1.Comment_Self_Employed_Plans__c= 'test'; 
        spa1.Comment_SEP_and_SIMPLE_Plans__c= 'test';
        spa1.Comment_Social_Security_Benefits__c= 'test';
        spa1.Comment_Social_Security_Taxable__c= 'test';
        spa1.Comment_Soc_Sec_Tax_Allowance_Calc_Rev__c= 'test';
        spa1.Comment_State_Other_Tax_Allow_Revision__c= 'test';
        spa1.Comment_Student_Assets__c= 'test';
        spa1.Comment_Taxable_Refunds__c= 'test';
        spa1.Comment_Tax_Exempt_Investment_Income__c= 'test';
        spa1.Comment_Total_Debts__c= 'test';
        spa1.Comment_Total_SE_Tax_Paid__c= 'test';
        spa1.Comment_Unemployment_Comp__c= 'test'; 
        spa1.Comment_Unpaid_Principal_1st_Mortgage__c= 'test';
        spa1.Comment_Unpaid_Principal_2nd_Mortgage__c= 'test';
        spa1.Comment_Untaxed_IRA_Plan_Payment__c= 'test';
        spa1.Comment_Unusual_Expense__c= 'test';
        spa1.Comment_Welfare_Veterans_Workers_Comp__c= 'test';
        spa1.Comment_Alimony_Received__c = 'test';
        update spa1;

        list<School_Biz_Farm_Assignment__c> sbfas = [SELECT Business_Farm__r.name, Business_Farm__r.Business_or_Farm__c, Comment_Business_Farm_Assets__c, Comment_Business_Farm_Debts__c, Comment_Business_Farm_Owner__c, Comment_Business_Farm_Ownership_Perc__c, Comment_Net_Profit_Loss_Business_Farm__c
                        FROM School_Biz_Farm_Assignment__c
                        WHERE Business_Farm__c = :b1.id OR Business_Farm__c = :b2.id OR Business_Farm__c = :b3.id
                        ];
         for (School_Biz_Farm_Assignment__c each : sbfas) {
             each.Comment_Business_Farm_Assets__c = 'test';
             each.Comment_Business_Farm_Debts__c = 'test';
             each.Comment_Business_Farm_Owner__c = 'test';
             each.Comment_Business_Farm_Ownership_Perc__c = 'test';
             each.Comment_Net_Profit_Loss_Business_Farm__c = 'test';
         }

         update sbfas;
                    
      }

      @isTest static void testSchoolPFSComments() {
        createTestData();
        // Implement test code
        PageReference fcwComments = Page.SchoolPFSComments;
        fcwComments.getParameters().put('spaId', spa1.id);
        Test.setCurrentPage(fcwComments);
        SchoolPFSComments comm = new SchoolPFSComments();
        system.debug(comm.commentRows.size());
        system.debug(comm.bizFarmRows.size());

        system.assert(comm.commentRows.size() == 67);
        system.assert(comm.bizFarmRows.size() == 15);

        PageReference fcwComments2 = Page.SchoolPFSComments;
        fcwComments.getParameters().put('spaId', 'fakeid');
        Test.setCurrentPage(fcwComments2);
        SchoolPFSComments comm2 = new SchoolPFSComments();
    }

}