/**
 * SendgridEventWebhook.cls
 *
 * @description Apex REST class that exposes RESTful endpoint to Sendgrid Webhook API for event updates via HTTP POST
 *
 * @author Chase Logan @ Presence PG
 */
@RestResource( urlMapping='/SendgridEvent/*')
global class SendgridEventWebhook {
    
    global class EventException extends exception {}

    /**
     * @description Exposed HTTP POST operation for handling incoming Sendgrid events and storing results on School PFS Assignment
     *
     * @author Chase Logan @ Presence PG
     */
    @HttpPost
    global static void handleSendgridEvent() {

        // parse JSON request body via RestContext.request.requestBody blob
        // Sendgrid POST's events in an anonymous JSON array that breaks default deserialization
        List<SendgridEvent> events;
        try {

            events = ( List<SendgridEvent>)JSON.deserialize(
                   RestContext.request.requestBody.toString(), List<SendgridEvent>.class);
        } catch ( Exception e) {

            // log it and bubble to caller for handling
            System.debug( 'DEBUG:::exception in SendgridEventWebhook.handleSendgridEvent' +
                e.getMessage());

            if ( MassEmailUtil.isWebhookDebugEnabled()) {

                MassEmailUtil.writeToIntegrationLog( e.getStackTraceString(), 
                        e.getMessage(), RestContext.request.requestBody.toString());
            }
        }

        // early return on empty events
        if ( events == null || events.isEmpty()) return;

        // if debug is enabled, write raw event data to log
        if ( MassEmailUtil.isWebhookDebugEnabled()) {

            MassEmailUtil.writeToIntegrationLog( null, 
                    null, String.valueOf( events));
        }


        // handle validation of incoming event - incoming mass_email_send_id must be valid, incoming school_id must match
        // corresponding field on Mass_Email_Send__c record, and incoming recipient_id must be populated
        // early return on invalid data
        Map<String,List<SendgridEvent>> massEmailToSchoolValidationMap = new Map<String,List<SendgridEvent>>();
        for ( SendgridEvent sgEvent : events) {

            if ( String.isNotEmpty( sgEvent.mass_email_send_id) && String.isNotEmpty( sgEvent.school_id)
                    && String.isNotEmpty( sgEvent.recipient_id)) {

                if ( massEmailToSchoolValidationMap.containsKey( sgEvent.mass_email_send_id)) {

                    List<SendgridEvent> tempList = massEmailToSchoolValidationMap.get( sgEvent.mass_email_send_id);
                    tempList.add( sgEvent);
                    massEmailToSchoolValidationMap.put( sgEvent.mass_email_send_id, tempList);
                } else {

                    massEmailToSchoolValidationMap.put( sgEvent.mass_email_send_id, new List<SendgridEvent>{ sgEvent});
                }
            }
        }

        List<Mass_Email_Send__c> massEmailList = 
            new MassEmailSendDataAccessService().getMassEmailSendByIdSet( massEmailToSchoolValidationMap.keySet());

        if ( massEmailList == null || massEmailList.isEmpty()) return;

        List<SendgridEvent> validEvents = new List<SendgridEvent>();
        Set<String> recipSet = new Set<String>();
        Set<String> academicYearSet = new Set<String>();
        Set<String> schoolIdSet = new Set<String>();
        for ( Mass_Email_Send__c massES : massEmailList) {

            if ( massEmailToSchoolValidationMap.containsKey( massES.Id)) {

                for ( SendgridEvent sgEvent : massEmailToSchoolValidationMap.get( massES.Id)) {

                    if ( massES.School__c == sgEvent.school_id) {

                        validEvents.add( sgEvent);
                        recipSet.add( sgEvent.recipient_id);
                        schoolIdSet.add( sgEvent.school_id);
                        academicYearSet.add( sgEvent.academic_year);
                    }
                }    
            }
        }

        if ( validEvents == null || validEvents.isEmpty()) return;

        // attempt SPA lookup by event.recipient_id (Parent A), as sends correspond to a parent and a parent can have multiple SPA's per year, 
        // must update all of a parent's SPA's with the same event data, even if SPA wasn't originally selected during send
        List<School_PFS_Assignment__c> schoolPFSList = 
            new SchoolPFSAssignmentDataAccessService().getSchoolPFSByParentAIdSet( recipSet, schoolIdSet, academicYearSet, true);

        if ( schoolPFSList != null) {

            Map<String,Map<String,List<SendgridEvent>>> recipToEventsBySPFSMap = new Map<String,Map<String,List<SendgridEvent>>>();
            for ( School_PFS_Assignment__c sPFS : schoolPFSList) {

                for ( SendgridEvent sgEvent : validEvents) {

                    // store all SPA's that should be updated for this parent by recipient_id (Parent A Id)
                    // store all incoming events by SPA Id (Parent A Id) in case multiple events present for same recip
                    if ( sgEvent.recipient_id == sPFS.Applicant__r.PFS__r.Parent_A__c &&
                             sgEvent.academic_year == sPFS.Student_Folder__r.Academic_Year_Picklist__c &&
                                 sgEvent.school_id == sPFS.School__c) {

                        if ( recipToEventsBySPFSMap.containsKey( sgEvent.recipient_id)) {

                            Map<String,List<SendgridEvent>> tempMap = recipToEventsBySPFSMap.get( sgEvent.recipient_id);
                            if ( tempMap.containsKey( sPFS.Id)) {
                                
                                List<SendgridEvent> tempList = tempMap.get( sPFS.Id);
                                tempList.add( sgEvent);
                                tempMap.put( sPFS.Id, tempList);

                                recipToEventsBySPFSMap.put( sgEvent.recipient_id, tempMap);
                            } else {

                                List<SendgridEvent> tempList = new List<SendgridEvent>();
                                tempList.add( sgEvent);
                                tempMap.put( sPFS.Id, tempList);

                                recipToEventsBySPFSMap.put( sgEvent.recipient_id, tempMap);
                            }
                        } else {

                            Map<String,List<SendgridEvent>> tempMap = new Map<String,List<SendgridEvent>>();
                            tempMap.put( sPFS.Id, new List<SendgridEvent>{ sgEvent});
                            recipToEventsBySPFSMap.put( sgEvent.recipient_id, tempMap);
                        }
                    }
                }
            }

            // Assign raw JSON event data back to School PFS Assignment(s) (SPA) for future use, 
            // events should fire in order from Sendgrid, but all events are stored.
            try {

                Map<String,School_PFS_Assignment__c> sPFSToUpdateMap = new Map<String,School_PFS_Assignment__c>();
                for ( School_PFS_Assignment__c sPFS : schoolPFSList) {

                    for ( Map<String,List<SendgridEvent>> tempMap : recipToEventsBySPFSMap.values()) {

                        if ( tempMap.containsKey( sPFS.Id)) {

                            for ( SendgridEvent sgEvent : tempMap.get( sPFS.Id)) {

                                sPFS.Mass_Email_Events__c = 
                                    ( String.isEmpty( sPFS.Mass_Email_Events__c) ? JSON.serializePretty( sgEvent) : 
                                        sPFS.Mass_Email_Events__c + ',\r\n' + JSON.serializePretty( sgEvent));
                            }
                            sPFSToUpdateMap.put( sPFS.Id, sPFS);
                        }
                    }
                }

                List<School_PFS_Assignment__c> sPFSToUpdateList = new List<School_PFS_Assignment__c>( sPFSToUpdateMap.values());
                update sPFSToUpdateList;
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in SendgridEventWebhook.handleSendgridEvent' +
                    e.getMessage());

                if ( MassEmailUtil.isWebhookDebugEnabled()) {

                    MassEmailUtil.writeToIntegrationLog( e.getStackTraceString(), 
                            e.getMessage(), String.valueOf( recipToEventsBySPFSMap.values()));
                }
            }
        }
    } 

}