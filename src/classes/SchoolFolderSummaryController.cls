/*
*   SPEC-092 Student Folder & Aid Allocation (Folder Summary), R-167, R-180, R-125, R-161, R-117 20-24 hours [Drew]
*   The school folder is a VF page, with some folder level items and statuses, and links to the one or two PFS' within.
*   The main folder page is also currently where the Aid Allocation is done. There are a number of editable fields which display the inputs to the aid decision and some can be revised as well.
*   Need to populate the Tuition fields (4 of them) based on grade and the enrollment status, and day vs. boarding."
*   - School user should be able to easily navigate between sibling folders
*   - The UI should clearly differentiate folder information (which is relevant to all PFS's) versus single PFS information
*   - School user should be able to obviously see when a folder has two PFS's in it
*   - School should be able to adjust the tuition & fees at the folder level which will allow them to apply their own discounts for siblings, etc.
*   - A folder should have a visual indicator if the student is new or returning
*   - A school aid officer should be able to allocate financial aid from any number of budgets they have specified.
*/

public without sharing class SchoolFolderSummaryController implements SchoolAcademicYearSelectorStudInterface {

    private static final String STRING_YES = 'Yes';

    /*Initialization*/
    public SchoolFolderSummaryController(ApexPages.StandardController stdController){
        this.pfs1Missing = false;
        this.pfs2Missing = false;
        folder = (Student_Folder__c)stdController.getRecord();
        fullView = !GlobalVariables.isLimitedSchoolView();
        // query for child School PFS Assignments (SPFSs), too
        folder = GlobalVariables.getFolderAndSchoolPFSAssignments(folder.Id);
        currentUser = GlobalVariables.getCurrentUser();

        isKSSchoolUser = GlobalVariables.isKSSchoolUser();//SFP-40,[G.S]

        //skipText = currentUser.SYSTEM_Skip_Folder_Summary_Text__c;
        //skipText = [Select Id, SYSTEM_Skip_Folder_Summary_Text__c from Contact where Id = :currentUser.ContactId][0].SYSTEM_Skip_Folder_Summary_Text__c;

        // if there are SPFSs, set some of the properties and check to see if the docs are completed
        if(folder.School_PFS_Assignments__r.size() > 0){

            spfs1 = folder.School_PFS_Assignments__r.size() > 0 ? folder.School_PFS_Assignments__r[0] : null;
            spfs2 = folder.School_PFS_Assignments__r.size() > 1 ? folder.School_PFS_Assignments__r[1] : null;

            thisRecordsYear = GlobalVariables.getAcademicYearByName(folder.Academic_Year_Picklist__c);

            docsCompleted = spfs1.PFS_Document_Status__c == 'Required Documents Complete';
            docsCompleted = spfs2 == null ? docsCompleted : docsCompleted && spfs2.PFS_Document_Status__c == 'Required Documents Complete';

            // four possible statuses: Required Documents Complete, Required Documents Received/In Progress, Required Documents Outstanding, No Documents Received
            if (docsCompleted){
                documentStatus = 'Required Documents Complete';
            } else {
                if (spfs2 == null){
                    documentStatus = spfs1.PFS_Document_Status__c == null ? 'No Documents Received' : spfs1.PFS_Document_Status__c;
                } else {
                    String spfs1DocStatus = spfs1.PFS_Document_Status__c == null ? 'nostatus' : spfs1.PFS_Document_Status__c;
                    String spfs2DocStatus = spfs2.PFS_Document_Status__c == null ? 'nostatus' : spfs2.PFS_Document_Status__c;

                    // default to in Required Documents Outstanding
                    documentStatus = 'Required Documents Outstanding';
                    // if both have no documents received (or no status), use 'No Documents Received'
                    documentStatus = (spfs1DocStatus == 'nostatus' || spfs1DocStatus == 'No Documents Received') && (spfs2DocStatus == 'nostatus' || spfs2DocStatus == 'No Documents Received') ? 'No Documents Received' : documentStatus;
                    // if both have Required Documents Received/In Progress, use 'Required Documents Received/In Progress'
                    documentStatus = spfs1DocStatus == 'Required Documents Received/In Progress' && spfs2DocStatus == 'Required Documents Received/In Progress' ? 'Required Documents Received/In Progress' : documentStatus;
                    // if one has Required Documents Complete and the other has Required Documents Received/In Progress, use 'Required Documents Received/In Progress'
                    documentStatus = spfs1DocStatus == 'Required Documents Complete' && spfs2DocStatus == 'Required Documents Received/In Progress' ? 'Required Documents Received/In Progress' : documentStatus;
                    documentStatus = spfs1DocStatus == 'Required Documents Received/In Progress' && spfs2DocStatus == 'Required Documents Complete' ? 'Required Documents Received/In Progress' : documentStatus;
                }
            }

            grade = spfs1.Grade_Applying__c == null ? spfs1.Orig_Grade_Applying__c : spfs1.Grade_Applying__c;
            grade = grade == null ? null : suffixizeGrade(grade);

            // commenting out below lines as part of NAIS-522 [DP] 8.7.13
            //currentSchool = spfs1.Present_School__c == null ? spfs1.Orig_Present_School__c : spfs1.Present_School__c;
            currentSchool = spfs1.Applicant__r.Current_School__c;

            setUseParentABooleans();
            calcContactFields();

            // NAIS-2368 [DP] 04.20.2015 only need to get outstanding docs if the doc status is "Req Docs Outstanding"
            if (documentStatus == 'Required Documents Outstanding'){

                Set<String> docTypesToExclude = new Set<String> {'Other/Misc Tax Form'};

                // NAIS-2369 [DP] 04.20.2015
                List<School_Document_Assignment__c> pfs1SDAList = new List<School_Document_Assignment__c>();
                List<School_Document_Assignment__c> pfs2SDAList = new List<School_Document_Assignment__c>();
                Set<Id> spaIds = (spfs2 == null) ? new Set<Id> {spfs1.Id} : new Set<Id> {spfs1.Id, spfs2.Id};

                for (School_Document_Assignment__c sda : [Select Id, Document_Status__c, Document_Type__c, Document_Year__c, School_PFS_Assignment__c, Required_Document__c FROM School_Document_Assignment__c
                        WHERE School_PFS_Assignment__c in :spaIds
                        AND Deleted__c = false
                        AND Document_Type__c not in :docTypesToExclude
                        AND (Request_To_Waive_Requirement__c != :STRING_YES OR Requirement_Waiver_Status__c = 'Not Approved' )]){
                    if (sda.School_PFS_Assignment__c == spfs1.Id){
                        pfs1SDAList.add(sda);
                    } else {
                        pfs2SDAList.add(sda);
                    }
                }

                pfs1OutstandingWrapper = new OutstandingPFSDocsWrapper('PFS 1', spfs1.Id, pfs1SDAList);
                pfs2OutstandingWrapper = (spfs2 == null) ? null : new OutstandingPFSDocsWrapper('PFS 2', spfs2.Id, pfs2SDAList);
            }

        }

        docsCompleted = docsCompleted == null ? false : docsCompleted;
        calculateColorAndMore();

        if(thisRecordsYear != null){
            schoolHasBudgets = GlobalVariables.usersSchoolHasBudgetGroups(thisRecordsYear.Id);
        }

    }
    /*End Initialization*/

    /*Properties*/
    public Student_Folder__c folder {get; set;}
    public User currentUser {get; set;}

    public PFS__c pfs1 {get; set;}
    public PFS__c pfs2 {get; set;}

    public School_PFS_Assignment__c spfs1 {get; set;}
    public School_PFS_Assignment__c spfs2 {get; set;}

    public String email {get; set;}
    public String phone {get; set;}
    public String address {get; set;}

    // [CH] NAIS-1866 Adding to streamline selectively showing page sections
    public String pfsTableCellStyle { get{return getNumOfPFSs() == 0 ? 'displayNone' : 'displayTableCell';} }
    public String nonPFSTableCellStyle { get{return getNumOfPFSs() == 0 ? 'displayTableCell' : 'displayNone';} }
    public String pfsBlockStyle { get{return getNumOfPFSs() == 0 ? 'displayNone' : 'displayBlock';} }
    public String nonPFSBlockStyle { get{return getNumOfPFSs() == 0 ? 'displayBlock' : 'displayNone';} }

    //public Boolean skipText {get; set;}

    // date string
    //public String pfsRevision {get; set;}  //No longer on this page

    public String color {get; set;}
    public Boolean docsCompleted {get; set;}

    public Boolean schoolHasBudgets {get; set;}

    public Boolean useParentBInfo {get; set;}
    public Boolean useParentBInfoPFS2 {get; set;}

    public Boolean otherLivesWith {get; set;}
    public Boolean otherLivesWithPFS2 {get; set;}

    // academic years used for the acad year selector
    //public List<Academic_Year__c> acadYears {get; set;}
    //public Map <Id, Student_Folder__c> acadYearIdToFolder {get; set;}
    public Academic_Year__c thisRecordsYear {get; set;}
    public String grade {get; set;}
    public String currentSchool {get; set;}

    public Boolean renderEFCs {get; set;}
    public Boolean renderStatusDate {get; set;}
    public Double bigValue {get; set;}
    public String bigText {get; set;}
    public String documentStatus {get; set;}

    public Map<String,String> pfsDependentList { get; set; }
    public Map<String,String> masterSiblingsList { get; set; }


    public Boolean fullView {get; set;}

    public OutstandingPFSDocsWrapper pfs1OutstandingWrapper {get; set;}
    public OutstandingPFSDocsWrapper pfs2OutstandingWrapper {get; set;}


    // used for the acad year selector
    public SchoolFolderSummaryController Me {
        get { return this; }
    }

    public String getAcademicYearId() {
        return this.thisRecordsYear.Id;
    }

    public void setAcademicYearId(String academicYearId) {
        // just storing it here -- does not get updated
        this.folder.Academic_Year_Picklist__c = GlobalVariables.getAcademicYear(this.thisRecordsYear.Id).Name;
    }

    public Boolean isKSSchoolUser {set;get;}//SFP-40,[G.S]

    // these are all "related lists" of sorts on the page
    public List<Sibling> getSiblings(){
        Set<Id> pfsIds = new Set<Id>();
        for (School_PFS_Assignment__c spfs : folder.School_PFS_Assignments__r){
            pfsIds.add(spfs.Applicant__r.PFS__c);
        }

        List<Sibling> siblings = new List<Sibling>();
        // applicant and contact Ids
        Set<Id> siblingIds = new Set<Id>();

        // use these to sort by whether the siblings are applying to the same school or not.
        List<School_PFS_Assignment__c> sameSchoolSFPSs = new List<School_PFS_Assignment__c>();
        List<School_PFS_Assignment__c> differentSchoolSFPSs = new List<School_PFS_Assignment__c>();

        Map<String,Boolean> pfs1Siblings = new Map<String,Boolean>();
        Map<String,Boolean> pfs2Siblings = new Map<String,Boolean>();
        masterSiblingsList = new Map<String,String>();
        Set<String> fullApplicantsName = new Set<String>();

        if(spfs1 != null){  // [CH] NAIS-1866 To avoid null reference errors for non need-based folders
            for (School_PFS_Assignment__c spfs : GlobalVariables.getSiblingApplicantSchoolPFSAssignments(pfsIds, folder))
            {
                if (spfs.School__c == spfs1.School__c)
                {
                    sameSchoolSFPSs.add(spfs);
                } 
                else 
                {
                    differentSchoolSFPSs.add(spfs);
                }

                if ( spfs.Applicant__r.PFS__c == spfs1.Applicant__r.PFS__c ) {
                    pfs1Siblings.put(spfs.Applicant__r.Contact__r.Name.toLowerCase(),true);
                } else {
                    pfs2Siblings.put(spfs.Applicant__r.Contact__r.Name.toLowerCase(),true);
                }

                fullApplicantsName.add(spfs.Applicant__r.Contact__r.Name.toLowerCase());

            }
        }

        for(String fullName : fullApplicantsName ) {

            String tmpName = fullName.toLowerCase();
            String tmpStatus = '';
            //We are in PFS 1
            if ( pfs1Siblings.get(tmpName) != null) {
                tmpStatus = 'PFS1';
                if ( pfs2Siblings.get(tmpName) != null) {
                    //We have both
                    tmpStatus = 'Both';
                }
            }
            else if ( pfs2Siblings.get(tmpName) != null ) {
                tmpStatus = 'PFS2';
            }

            masterSiblingsList.put(tmpName,tmpStatus);
        }

        // put same school siblings first==
        for (School_PFS_Assignment__c spfs : sameSchoolSFPSs){
            if (!siblingIds.contains(spfs.Applicant__c) && !siblingIds.contains(spfs.Applicant__r.Contact__c)){
                siblingIds.add(spfs.Applicant__c);
                siblingIds.add(spfs.Applicant__r.Contact__c);
                siblings.add(new Sibling(spfs, spfs1.School__c));
            }
        }

        // then other school siblings
        for (School_PFS_Assignment__c spfs : differentSchoolSFPSs){
            if (!siblingIds.contains(spfs.Applicant__c) && !siblingIds.contains(spfs.Applicant__r.Contact__c)){
                siblingIds.add(spfs.Applicant__c);
                siblingIds.add(spfs.Applicant__r.Contact__c);
                siblings.add(new Sibling(spfs, spfs1.School__c));
            }
        }


        return siblings;
    }

    public List<Dependent> getDependents(){
        Set<Id> pfsIds = new Set<Id>();
        for (School_PFS_Assignment__c spfs : folder.School_PFS_Assignments__r){
            pfsIds.add(spfs.Applicant__r.PFS__c);
        }
        List<Dependent> dependents = new List<Dependent>();

        Map<String,Boolean> pfs1FullName = new Map<String,Boolean>();
        Map<String,Boolean> pfs2FullName = new Map<String,Boolean>();
        pfsDependentList = new Map<String,String>();

        Set<String> pfsNames = new Set<String>();

        Dependents__c pfs1Dependents;
        Dependents__c pfs2Dependents;

        Set<String> fullNameSet = new Set<String>();

        for (Dependents__c dep : GlobalVariables.getDependents(pfsIds)){

            for (Integer i = 1; i < 7; i++){
                Object fullname = dep.get('Dependent_' + String.valueOf(i) + '_Full_Name__c');
                if (fullname != null && !fullNameSet.contains(String.valueOf(fullname))){
                    fullNameSet.add(String.valueOf(fullname));
                    dependents.add(new Dependent(dep, String.valueOf(i)));
                }

                if (dep.PFS__c == spfs1.Applicant__r.PFS__c) {
                    pfs1Dependents = dep;
                }
                if (spfs2 != null && dep.PFS__c == spfs2.Applicant__r.PFS__c) { // WH 2013-08-23, NAIS-974
                    pfs2Dependents = dep;
                }
            }
        }

        if ( pfs1Dependents != null) {
            for (Integer i=1; i<7; i++) {
                Object fullName = pfs1Dependents.get('Dependent_'+i+'_Full_Name__c');
                if (fullName != null) {
                    //Store in lower case, so that we do not get duplicate keys if there are case issues
                    pfs1FullName.put(String.valueOf(fullName).toLowerCase(),true);
                    pfsNames.add(String.valueOf(fullName).toLowerCase());
                }
            }
        }
        if ( pfs2Dependents != null) {

            for (Integer i=1; i<7; i++) {
                Object fullName = pfs2Dependents.get('Dependent_'+i+'_Full_Name__c');

                if (fullName != null) {
                    //Store lowercase to make sure we do not get duplicate keys
                    pfs2FullName.put(String.valueOf(fullName).toLowerCase(),true);
                    pfsNames.add(String.valueOf(fullName).toLowerCase());
                }
            }
        }

        for (String fullName : pfsNames) {

            String tmpFullName = fullName.toLowercase();
            String tmpStatus = '';

            //Exists in PFS1
            if ( pfs1FullName.get(tmpFullName) != null) {
                tmpStatus = 'PFS1';
                //Exists in PFS2, so both
                if ( pfs2FullName.get(tmpFullName) != null) {
                    tmpStatus = 'Both';
                }
            }
            else if ( pfs2FullName.get(tmpFullName) != null) {
                tmpStatus = 'PFS2';
            }

            pfsDependentList.put(tmpFullName,tmpStatus);
            system.debug(pfsDependentList);
        }

        return dependents;
    }

    public List<OtherApp> getOtherApps(){

        Set<Id> schoolIdSet = new Set<Id>();
        List<OtherApp> otherApps = new List<OtherApp>();

        for (School_PFS_Assignment__c spfs : [
            SELECT Id, Student_Folder__c, School__c, School__r.Name, School__r.BillingCity, School__r.BillingState
            FROM School_PFS_Assignment__c
            WHERE Student_Folder__r.Student__c = :folder.Student__c
                AND Academic_Year_Picklist__c = :folder.Academic_Year_Picklist__c
                AND Student_Folder__c != :folder.Id
                AND School__r.SSS_Subscriber_Status__c IN :GlobalVariables.activeSSSStatuses
                AND Withdrawn__c != :STRING_YES
              ORDER BY School__r.Name ASC]){

            if (!schoolIdSet.contains(spfs.School__c)){
                schoolIdSet.add(spfs.School__c);
                otherApps.add(new OtherApp(spfs));
            }
        }

        return otherApps;
    }

    public Integer getNumOfPFSs(){
        Integer i = 0;
        i = spfs1 == null ? i : ++i;
        i = spfs2 == null ? i : ++i;
        return i;
    }

    public Boolean getHasAllocations(){
        return 0 < [Select count() from Budget_Allocation__c where Student_Folder__c = :folder.Id AND Amount_Allocated__c > 0];
    }
    /*End Properties*/

    /*Action Methods*/

    // called on page load since this is VF override
    public pageReference redirect(){
        if (currentUser.ContactId == null){ // [CH] NAIS-1866 Updating to facilitate non need-based Folders
        // if (currentUser.ContactId == null || folder.School_PFS_Assignments__r.size() == 0){
            return new PageReference('/' + folder.Id + '?nooverride=true');
        } else {
            return null;
        }
    }

    // on change of academic year, reload page with new folder
    public PageReference SchoolAcademicYearSelectorStudent_OnChange(PageReference newPage) {
        return newPage;
    }

    public PageReference makeAward(){
        PageReference pr = Page.SchoolFinancialAid;
        pr.getParameters().put('id', folder.Id);
        return pr;
        //return null;
    }

    /*End Action Methods*/

    /*Helper Methods*/

    public static String suffixizeGrade(String grade){
        grade = grade == '1' ? grade + 'st' : grade;
        grade = grade == '2' ? grade + 'nd' : grade;
        grade = grade == '3' ? grade + 'rd' : grade;

        Set<String> thSet = new Set<String>{'4','5','6','7','8','9','10','11','12'};
        grade = grade != null && thSet.contains(grade) ? grade + 'th' : grade;

        return grade;
    }

    public void setUseParentABooleans(){
        useParentBInfo = false;
        useParentBInfoPFS2 = false;
        if (spfs1.Student_Lives_With__c != null && spfs1.Student_Lives_With__c == 'Parent B'){
            useParentBInfo = true;
        }

        useParentBInfoPFS2 = false;
        if (spfs2 != null && spfs2.Student_Lives_With__c != null && spfs2.Student_Lives_With__c == 'Parent B'){
            useParentBInfoPFS2 = true;
        }

        if(spfs1 != null && spfs1.Applicant__r.Live_with_Other_Parent_Guardian__c != null && spfs1.Applicant__r.Live_with_Other_Parent_Guardian__c == STRING_YES){
            otherLivesWith = true;
            useParentBInfo = false;
        }

        if(spfs2 != null && spfs2.Applicant__r.Live_with_Other_Parent_Guardian__c != null && spfs2.Applicant__r.Live_with_Other_Parent_Guardian__c == STRING_YES){
            otherLivesWithPFS2 = true;
            useParentBInfoPFS2 = false;
        }
    }

    // TODO: get updated definition of this
    public void calculateColorAndMore(){
        renderStatusDate = false;
        bigText = null;
        bigValue = null;
        renderEFCs = false;

        String spfs1status = spfs1 == null ? null : spfs1.PFS_Document_Status__c;
        String spfs2status = spfs2 == null ? null : spfs2.PFS_Document_Status__c;
        color = calculateColor(folder, spfs1status, spfs2status);

        // if this is the basic version we always show the big EFCs and never the small EFCs
        if (!fullView){
            bigText = 'EFC';
            bigValue = folder.EFC__c;
            renderEFCs = false;
            color = 'white';
        } else

        if (!docsCompleted){
            bigText = documentStatus;
            bigValue = null;
            renderEFCs = false;
        } else

        if (folder.Folder_Status__c == 'Submitted'){
            bigText = 'Required Documents Complete';
            bigValue = null;
            renderEFCs = false;
        } else

        if (folder.Folder_Status__c == 'In progress'){
            bigText = docsCompleted ? null : documentStatus;
            bigValue = folder.EFC__c;
            renderEFCs = true;
        } else

        if (folder.Folder_Status__c == 'Verified'){
            bigText = null;
            bigValue = folder.EFC__c;
            renderEFCs = true;
        } else

        if (folder.Folder_Status__c == 'Award Proposed'){
            bigText = null;
            //bigValue = folder.Proposed_Grant__c; commented out as this field is being removed  [dp] 1.9.14
            bigValue = folder.Grant_Awarded__c;
            renderEFCs = true;
        } else

        if (folder.Folder_Status__c == 'Award Approved'){
            bigText = null;
            bigValue = folder.Grant_Awarded__c;
            renderEFCs = true;
        } else

        if (folder.Folder_Status__c == 'Award Denied' || folder.Folder_Status__c == 'Complete, Award Denied'){
            bigText = 'No Award';
            bigValue = null;
            renderEFCs = false;
        } else

        if (folder.Folder_Status__c == 'Wait Pooled'){
            bigText = null;
            bigValue = folder.Financial_Need__c;
            renderEFCs = false;
        } else

        if (folder.Folder_Status__c == 'Waiting on Family'){
            renderStatusDate = true;
            bigText = null;
            bigValue = folder.Grant_Awarded__c;
            renderEFCs = true;
        } else

        if (folder.Folder_Status__c == 'Appealed'){
            bigText = null;
            //bigValue = folder.Proposed_Grant__c; commented out as this field is being removed  [dp] 1.9.14
            bigValue = folder.Grant_Awarded__c;
            renderEFCs = true;
        } else

        if (folder.Folder_Status__c == 'Complete, Award Accepted'){
            bigText = null;
            bigValue = folder.Grant_Awarded__c;
            renderEFCs = true;
        } else

        if (folder.Folder_Status__c == 'Complete, Award Refused'){
            bigText = 'Award Not Accepted';
            bigValue = null;
            renderEFCs = false;
        }

        /*
        Set<String> purpleSet = new Set<String> ('In progress', 'Verified', 'Award Proposed');
        Set<String> greenSet = new Set<String> ('Award Denied', 'Award Approved', 'Complete, Award Accepted');
        Set<String> yellowSet = new Set<String> ('Wait Pooled', 'Waiting on Family', 'Appealed');
        Set<String> greySet = new Set<String> ('Complete, Award Refused');

        c = f.Folder_Status__c == 'Award Approved' ? 'green' : 'yellow';
        return c;
        */
    }

    // static method, called from anywhere
    public static String calculateColor(Student_Folder__c f, String pfs1DocStatus, String pfs2DocStatus){
        String c = 'white';
        String folderStatus = f.Folder_Status__c == null ? 'nostatus' : f.Folder_Status__c.toLowerCase();
        Set<String> purpleSet = new Set<String> {'in progress', 'verified', 'award proposed', 'submitted'};
        Set<String> greenSet = new Set<String> {'award approved', 'complete, award accepted'};
        Set<String> yellowSet = new Set<String> {'wait pooled', 'waiting on family', 'appealed'};
        Set<String> greySet = new Set<String> {'award denied', 'complete, award refused', 'complete, award denied'};

        // if docs are missing, always make it yellow
        if ((pfs1DocStatus != null && pfs1DocStatus != 'Required Documents Complete') || (pfs2DocStatus != null && pfs2DocStatus != 'Required Documents Complete')){
            c = 'yellow';
        } else {
            c = purpleSet.contains(folderStatus) ? 'purple' : c;
            c = greenSet.contains(folderStatus) ? 'green' : c;
            c = yellowSet.contains(folderStatus) ? 'yellow' : c;
            c = greySet.contains(folderStatus) ? 'grey' : c;
        }

        return c;

    }

    public void calcContactFields(){

    }

    /*End Helper Methods*/

    /* Internal Class */
    public class Sibling{
        public sibling(School_PFS_Assignment__c spfsParam, Id currentSchoolId){
            spfs = spfsParam;
            birthdate = spfs.Applicant__r.Contact__r.Birthdate == null ? null : Date.valueOf(spfs.Applicant__r.Contact__r.Birthdate);
            age = spfs.Applicant__r.Contact__r.Birthdate == null ? null : Integer.valueOf(spfs.Applicant__r.Contact__r.Birthdate.daysBetween(System.today()) / 365.2425);
            grade = String.valueOf(spfs.Applicant__r.Current_Grade__c);
            grade = suffixizeGrade(grade);
            gender = spfs.Applicant__r.Gender__c;
            name = spfs.Applicant__r.Contact__r.Name;
            currentSchool = spfs.Applicant__r.Current_School__c; //NAIS-1853

            applyingToThisSchool = spfs.School__c == currentSchoolId;
        }

        public String name {get; set;}
        public Date birthdate {get; set;}
        public String grade {get; set;}
        public String currentSchool {get; set;} //NAIS-1853
        public Integer age {get; set;}
        public String gender {get; set;}
        public School_PFS_Assignment__c spfs {get; set;}
        public Boolean applyingToThisSchool {get; set;}
        public String includedInPFS { get; set; }
    }

    public class Dependent{
        public dependent(Dependents__c dep, String num){
            try {
                name = dep.get('Dependent_' + num + '_Full_Name__c') == null ? null : String.valueOf(dep.get('Dependent_' + num + '_Full_Name__c'));
                grade = dep.get('Dependent_' + num + '_Current_Grade__c') == null ? null : String.valueOf(dep.get('Dependent_' + num + '_Current_Grade__c'));
                gender = dep.get('Dependent_' + num + '_Gender__c') == null ? null : String.valueOf(dep.get('Dependent_' + num + '_Gender__c'));
                birthdate = dep.get('Dependent_' + num + '_Birthdate__c') == null ? null : Date.valueOf(dep.get('Dependent_' + num + '_Birthdate__c'));
                currentSchool = dep.get('Dependent_' + num + '_Current_School__c') == null ? null : String.valueOf(dep.get('Dependent_' + num + '_Current_School__c')); //NAIS-1853
                age = birthdate == null ? null : Integer.valueOf(Date.valueOf(dep.get('Dependent_' + num + '_Birthdate__c')).daysBetween(System.today()) / 365.2425);

                grade = suffixizeGrade(grade);
            } catch (Exception e){
                // do nothing
            }
        }

        public String name {get; set;}
        public String grade {get; set;}
        public String gender {get; set;}
        public Date birthdate {get; set;}
        public Integer age {get; set;}
        public String belongsToPFS { get; set; }
        public String currentSchool { get; set; } //NAIS-1853

    }

    public class otherApp{
        public otherApp(School_PFS_Assignment__c spfsOther){
            name = spfsOther.School__r.Name;
            address = spfsOther.School__r.BillingCity == null ? address : spfsOther.School__r.BillingCity;
            if (address == null){
                address = spfsOther.School__r.BillingState == null ? address : spfsOther.School__r.BillingState;
            } else {
                address = spfsOther.School__r.BillingState == null ? address : address + ', ' + spfsOther.School__r.BillingState;
            }
        }

        public String name {get; set;}
        public String address {get; set;}

    }

    /* End Internal Class */

    /* Internal Class */
    public class OutstandingPFSDocsWrapper{
        public String pfsLabel {get; set;}
        public List<School_Document_Assignment__c> sdaList {get; set;}
        public PageReference docPageLink {get; set;}

        public OutstandingPFSDocsWrapper(String pfsLabelParam, Id spaId, List<School_Document_Assignment__c> sdaListParam){
            pfsLabel = pfsLabelParam;
            docPageLink = Page.SchoolPFSDocument;
            docPageLink.getParameters().put('SchoolPFSAssignmentId', spaId);
            docPageLink.getParameters().put('tabName', pfsLabel.replace(' ', ''));

            Set<String> receivedDocKeys = new Set<String>();
            for (School_Document_Assignment__c sda : sdaListParam){
                if (sda.Document_Status__c == 'Received/In Progress' || sda.Document_Status__c == 'Processed'){
                    receivedDocKeys.add(generateKey(sda));
                }
            }

            sdaList = new List<School_Document_Assignment__c>();
            for (School_Document_Assignment__c sda : sdaListParam){
                if (sda.Document_Status__c != 'Received/In Progress' && sda.Document_Status__c != 'Processed'){
                    if (!receivedDocKeys.contains(generateKey(sda)) || (sda.Required_Document__c != null)){
                        sdaList.add(sda);
                    }
                }
            }

        }

        public String generateKey(School_Document_Assignment__c sda){
            return '• ' + sda.Document_Type__c + ' - ' + sda.Document_Year__c;
        }
    }
    /* End Internal Class */

    ////////
    // Logic to handle missing sections
    ////////

    // determine if this is a kamehameha school by checking custom settings and comparing
    private boolean isKS;
    public boolean getIsKS(){
        if(isKS == null){
            isKs = SchoolPortalSettings.KS_School_Account_Id == spfs1.school__c ? true : false;
        }
        return isKS;
    }

    // pfs1/pfs2 having missing sections is set by the missingSectionsMap getter
    private boolean pfs1Missing;
    public boolean getPFS1Missing(){
        getMissingSectionsMap();
        return pfs1Missing;
    }

    private boolean pfs2Missing;
    public boolean getPFS2Missing(){
        getMissingSectionsMap();
        return pfs2Missing;
    }

    private boolean isMissingSections;
    public boolean getIsMissingSections(){
        getMissingSectionsMap();
        if(pfs1Missing || pfs2Missing)
            isMissingSections = true;

        return isMissingSections;
    }

    // spfs1 and spfs2 set by the constructor
    private map<string,map<string,boolean>> missingSectionsMap;
    public map<string,map<string,boolean>> getMissingSectionsMap(){
        if( missingSectionsMap == null && spfs1!=null){
            // get pfs and fields
            boolean isPFS2 = getNumOfPFSs()>1 ? true : false;

             String[] PFSFieldNames = ExpCore.GetAllFieldNames('PFS__c');

            for (Integer i = 0; i < PFSFieldNames.size(); i++) {
                PFSFieldNames[i] = 'Applicant__r.PFS__r.' + PFSFieldNames[i];
            }

            string spfs1id = spfs1.id;
            string spfs2id;
            if(isPFS2)
                 spfs2id = spfs2.id;

            string pfsFieldQueryString;
            pfsFieldQueryString = 'SELECT id, '+String.join(PFSFieldNames,', ');
            pfsFieldQueryString = pfsFieldQueryString.removeEnd(', ');
            pfsFieldQueryString+=' FROM School_PFS_Assignment__c WHERE Id=:spfs1id';

            if(isPFS2)
             pfsFieldQueryString+= ' OR Id=:spfs2id';

             PFS__c pfs1;
             PFS__c pfs2;

             // get the pfs' related to the spfs'
             list<School_PFS_Assignment__c> pfsResults = database.query(pfsFieldQueryString);
             for(School_PFS_Assignment__c  each : pfsResults )
                if(each.id == spfs1.id)
                    pfs1 = each.Applicant__r.PFS__r;
                else if(isPFS2 && each.id == spfs2.id)
                    pfs2 = each.Applicant__r.PFS__r;

            // populate a map of the sections and whether they are missing
            missingSectionsMap= new map<string,map<string,boolean>>{'PFS1' => new map<string,boolean>{'statStudentIncome'=> !pfs1.statStudentIncome__c,'statEducationalExpenses'=>!pfs1.statEducationalExpenses__c,'statBusinessFarm'=>((pfs1.statBusinessFarm__c != 'Complete'&& pfs1.Own_Business_or_Farm__c ==STRING_YES)? true : false),'statForKamehameha'=>(pfs1.statForKamehameha__c != 'Complete' && getIsKS() ? true : false)}};
            if(isPFS2)
                missingSectionsMap.put('PFS2' ,new map<string,boolean>{'statStudentIncome'=> !pfs2.statStudentIncome__c,'statEducationalExpenses'=>!pfs2.statEducationalExpenses__c,'statBusinessFarm'=>((pfs2.statBusinessFarm__c != 'Complete' && pfs2.Own_Business_or_Farm__c ==STRING_YES)? true : false),'statForKamehameha'=>(pfs2.statForKamehameha__c != 'Complete' && getIsKS() ? true : false)});

            pfs1Missing = false;
            pfs2Missing = false;

            // determine if missing sections are present, and if pfs is submitted
            for(boolean each : missingSectionsMap.get('PFS1').values())
                if(each == true && pfs1.PFS_status__c =='Application Submitted')
                    pfs1Missing = true;

            if(isPFS2)
                for(boolean each : missingSectionsMap.get('PFS2').values())
                    if(each == true && pfs2.PFS_status__c =='Application Submitted')
                        pfs2Missing = true;

        }
      return missingSectionsMap;
    }

}