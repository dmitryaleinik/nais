/*
 * NAIS-2270
 *
 * WH, Exponent Partners, 2015
 */
public class TransactionLineItemUtils {
    private static final String EARLY_BIRD_TRANSACTION_TYPE = 'Early Bird Subscription Discount';

    // caller must have following fields on the opp sObject populated:
    //    Id, Academic_Year__c, RecordTypeId, Subscription_Type__c
    public static Transaction_Line_Item__c createSaleTLI(Opportunity opp, Boolean doInsert) {

        // find the year from the opportunity; default to current calendar year if not defined
        Academic_Year__c academicYear;
        for (Academic_Year__c ayear : [select Id, Name from Academic_Year__c where Name = :opp.Academic_Year_Picklist__c]) {
            academicYear = ayear;
        }

        String year = (academicYear != null && academicYear.Name != null) ? academicYear.Name.split('-')[0] : String.valueOf(System.today().year());

        Transaction_Line_Item__c transactionLineItem = new Transaction_Line_Item__c();

        transactionLineItem.RecordTypeId = RecordTypes.saleTransactionTypeId;
        transactionLineItem.Opportunity__c = opp.Id;
        transactionLineItem.Transaction_Status__c = 'Posted';
        transactionLineItem.Transaction_Date__c = System.today();

        // fill in Subscription specific fields
        if (opp.RecordTypeId == RecordTypes.opportunitySubscriptionFeeTypeId) {
            transactionLineItem.Transaction_Type__c = 'School Subscription';
            transactionLineItem.Product_Amount__c = '0';

            // find the relevant transaction annual settings
            Transaction_Annual_Settings__c transactionAnnualSetting;
            for (Transaction_Annual_Settings__c annualSetting : Transaction_Annual_Settings__c.getAll().values()) {
                if (annualSetting.Subscription_Type__c == opp.Subscription_Type__c && annualSetting.Year__c == year && annualSetting.Discount_Type__c == null) { // NAIS-2475
                    transactionAnnualSetting = annualSetting;
                    break;
                }
            }

            // populate values from transaction annual settings
            if (transactionAnnualSetting != null) {
                transactionLineItem.Product_Amount__c = String.valueOf(transactionAnnualSetting.Product_Amount__c);
                transactionLineItem.Product_Code__c = transactionAnnualSetting.Product_Code__c;
                transactionLineItem.Amount__c = transactionAnnualSetting.Product_Amount__c;
            } else {
                transactionLineItem.Product_Amount__c = '0';
                transactionLineItem.Amount__c = 0;
            }
        }

        // fill in values from transaction settings
        Transaction_Settings__c transactionSetting = Transaction_Settings__c.getValues(transactionLineItem.Transaction_Type__c);
        if (transactionSetting != null) {
            transactionLineItem.Transaction_Code__c = transactionSetting.Transaction_Code__c;
            transactionLineItem.Account_Code__c = transactionSetting.Account_Code__c;
            transactionLineItem.Account_Label__c = transactionSetting.Account_Label__c;
        }

        // insert the TLI
        if (doInsert) {
            insert transactionLineItem;
        }

        return transactionLineItem;
    }

    /**
     * @description Creates an Early Bird Discount Transaction Line Item for the
     *              given opportunity for the provided discount amount. On the
     *              opportunity the Id, Academic_Year__c, RecordTypeId, and
     *              Subscription_Type__c fields must be populated in order for a
     *              discount Transaction Line Item to be created.
     * @param opp The opportunity to create an Early Bird Discount Transaction
     *        Line Item for.
     * @param discountAmount The amount that the Discount Transaction Line Item
     *        will be for.
     * @param doInsert Whether or not to insert the Transaction Line Item record
     *        that is created.
     * @return The created Early Bird Discount Transaction Line Item.
     */
    public static Transaction_Line_Item__c createSchoolEarlyBirdDiscountTLI(Opportunity opp, Decimal discountAmount, Boolean doInsert) {
        // First validate that there are no pre-existing Early Bird Discount Transaction Line Items.
        Transaction_Line_Item__c existingTransactionLineItem = getExistingEarlyBirdTransactionLineItem(opp);
        //There should be NO early bird TLI for basic opps (SFP-1389).
        Boolean isBasicOpp = opp.Subscription_Type__c == 'Basic';
        if (existingTransactionLineItem != null || isBasicOpp) {
            // Return null if the line item already exists. Or it is a Basic opportunity.
            return null;
        }

        // find the year from the opportunity; default to current calendar year if not defined
        Academic_Year__c academicYear;
        for (Academic_Year__c ayear : [select Id, Name from Academic_Year__c where Name = :opp.Academic_Year_Picklist__c]) {
            academicYear = ayear;
        }
        String year = (academicYear != null && academicYear.Name != null) ? academicYear.Name.split('-')[0] : String.valueOf(System.today().year());

        Transaction_Line_Item__c transactionLineItem = new Transaction_Line_Item__c();

        transactionLineItem.RecordTypeId = RecordTypes.schoolDiscountTransactionTypeId;
        transactionLineItem.Opportunity__c = opp.Id;
        transactionLineItem.Transaction_Status__c = 'Posted';
        transactionLineItem.Transaction_Date__c = System.today();
        transactionLineItem.Transaction_Type__c = EARLY_BIRD_TRANSACTION_TYPE;

        // fill in Subscription specific fields
        if (opp.RecordTypeId == RecordTypes.opportunitySubscriptionFeeTypeId) {
            transactionLineItem.Amount__c = discountAmount;

            // find the relevant transaction annual settings
            Transaction_Annual_Settings__c transactionAnnualSetting;
            for (Transaction_Annual_Settings__c annualSetting : Transaction_Annual_Settings__c.getAll().values()) {
                if (annualSetting.Subscription_Type__c == opp.Subscription_Type__c && annualSetting.Discount_Type__c == 'Early Bird' && annualSetting.Year__c == year) {
                    transactionAnnualSetting = annualSetting;
                    break;
                }
            }

            // populate values from transaction annual settings
            if (transactionAnnualSetting != null) {
                transactionLineItem.Product_Amount__c = String.valueOf(transactionAnnualSetting.Product_Amount__c);
                transactionLineItem.Product_Code__c = transactionAnnualSetting.Product_Code__c;
                transactionLineItem.Amount__c = transactionAnnualSetting.Product_Amount__c;
            }

            // should be the same amount
            if (transactionLineItem.Amount__c != discountAmount) {
                system.debug('>>> Discount on Opp [' + discountAmount + '] differs from discount defined in settings [' + transactionLineItem.Amount__c + ']');
            }
        }

        // fill in values from transaction settings
        Transaction_Settings__c transactionSetting = Transaction_Settings__c.getValues(transactionLineItem.Transaction_Type__c);
        if (transactionSetting != null) {
            transactionLineItem.Transaction_Code__c = transactionSetting.Transaction_Code__c;
            transactionLineItem.Account_Code__c = transactionSetting.Account_Code__c;
            transactionLineItem.Account_Label__c = transactionSetting.Account_Label__c;
        }

        // insert the TLI
        if (doInsert) {
            insert transactionLineItem;
        }

        return transactionLineItem;
    }

    // simplified method to retrieve just the product amount of School Subscription sale TLI
    public static Decimal getSchoolSubscriptionSaleTLIAmount(String subscriptionType, String year) {

        // find the relevant transaction annual settings
        Transaction_Annual_Settings__c transactionAnnualSetting;
        for (Transaction_Annual_Settings__c annualSetting : Transaction_Annual_Settings__c.getAll().values()) {
            if (annualSetting.Subscription_Type__c == subscriptionType && annualSetting.Year__c == year && annualSetting.Discount_Type__c == null) {// NAIS-2473 not a discount
                transactionAnnualSetting = annualSetting;
                break;
            }
        }

        // get product amount from transaction annual settings
        return (transactionAnnualSetting != null ? transactionAnnualSetting.Product_Amount__c : 0);
    }

    // simplified method to retrieve just the product amount of School Subscription Discount TLI
    public static Decimal getSchoolSubscriptionDiscountTLIAmount(String subscriptionType, String discountType, String year) {

        // find the relevant transaction annual settings
        Transaction_Annual_Settings__c transactionAnnualSetting;
        for (Transaction_Annual_Settings__c annualSetting : Transaction_Annual_Settings__c.getAll().values()) {
            if (annualSetting.Subscription_Type__c == subscriptionType && annualSetting.Discount_Type__c == discountType && annualSetting.Year__c == year) {
                transactionAnnualSetting = annualSetting;
                break;
            }
        }

        // get product amount from transaction annual settings
        return (transactionAnnualSetting != null ? transactionAnnualSetting.Product_Amount__c : 0);
    }

    /**
     * @description Gathers the associated Early Bird Transaction Line Items for
     *              the given opportunity.
     * @param opportunity The opportunity to find Early Bird Transaction Line Items
     *        for.
     * @return A Transaction Line Item if there is an Early Bird Transaction Line
     *         Item for the opportunity provided, otherwise null.
     */
    private static Transaction_Line_Item__c getExistingEarlyBirdTransactionLineItem(Opportunity opportunity) {
        Id recordTypeId = RecordTypes.schoolDiscountTransactionTypeId;

        List<Transaction_Line_Item__c> existingTransactions = [SELECT
                                                                      Account_Code__c,
                                                                      Account_Label__c,
                                                                      Amount__c,
                                                                      Opportunity__c,
                                                                      Product_Amount__c,
                                                                      Product_Code__c,
                                                                      RecordTypeId,
                                                                      Transaction_Code__c,
                                                                      Transaction_Date__c,
                                                                      Transaction_Status__c,
                                                                      Transaction_Type__c
                                                                 FROM Transaction_Line_Item__c
                                                                WHERE Opportunity__c = :opportunity.Id
                                                                  AND RecordTypeId = :recordTypeId
                                                                  AND Transaction_Type__c = :EARLY_BIRD_TRANSACTION_TYPE];

        if (!existingTransactions.isEmpty()) {
            return existingTransactions[0];
        }

        return null;
    }
}