/*
* SPEC-108, NAIS-40
*
* Maintain a per school count of revised fields for audit reporting requirement
*
* Nathan, Exponent Partners, 2013
*/
public without sharing class SchoolRevisionMetrics {

    // input
    public Map <Id, School_PFS_Assignment__c> oldSpfsaMap = new Map <Id, School_PFS_Assignment__c>();
    public Map <Id, School_PFS_Assignment__c> newSpfsaMap = new Map <Id, School_PFS_Assignment__c>();
    map<String, DescribeFieldResult> fieldResultsMap = new map<String, DescribeFieldResult>();
    // to prevent trigger recursive calls
    public static Boolean isExecuting = false;
    
    Map <String, String> spfsSourceTargetMap = new Map <String, String>();
    Map <String, String> spfsSourceToOrigMap = new Map <String, String>();
    Set <String> schoolIdSet = new Set <String>();
    Map <String, Field_Count__c> key_fieldCount_map = new Map <String, Field_Count__c>();
    
    List <Field_Count__c> fieldCountUpdateList = new List <Field_Count__c>();
    List <Field_Count__c> fieldCountNewList = new List <Field_Count__c>();
    Set <String> fieldCountUpdateSet = new Set <String>();    // to avoid adding dupes to list
    Set <String> fieldCountNewSet = new Set <String>();    // to avoid adding dupes to list
    //private static final String PORTAL_USERTYPE = 'PowerPartner';
    
    // constructor
    public SchoolRevisionMetrics() {
        loadSPFSASourceTargetMap();
        loadspfsSourceToOrigMapMap();
    }

    public void SetRevisionMetrics() {
        
        // start trigger execution
        isExecuting = true;
        
        Set<String> SPAfieldNames = new Set<String>();
        Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.School_PFS_Assignment__c.fields.getMap();
        SPAfieldNames.addAll(fieldMap.keySet());
        

        // collect all affected schools
        if (oldSpfsaMap != null && oldSpfsaMap.size() > 0) {
            for (School_PFS_Assignment__c spfsa : oldSpfsaMap.values()) {
                schoolIdSet.add(spfsa.School__c);
            }
        }
        if (newSpfsaMap != null && newSpfsaMap.size() > 0) {
            for (School_PFS_Assignment__c spfsa : newSpfsaMap.values()) {
                schoolIdSet.add(spfsa.School__c);
            }
        }
        
        // load Field Count record
        // I hope that one record is maitained for each school
        
        String selectStr = getFieldCountQuery();
        String queryStr = selectStr + ' where School__c in :schoolIdSet'; 
        
        List <Field_Count__c> fcList = Database.Query(queryStr);
        for (Field_Count__c fc : fcList) {
            String key = fc.School__c + '_' + fc.Academic_Year__c;
            key_fieldCount_map.put(key, fc);
        }

        // for each metrics field set correcsponding count field
        for (String sourceField : spfsSourceTargetMap.keySet()) {
            String targetField; 
            String fieldName = spfsSourceTargetMap.get(sourceField);
            String origFieldName = spfsSourceToOrigMap.get(sourceField);
            origFieldName = origFieldName == null ? 'Orig_'+sourceField : origFieldName;
            
            if (fieldName == null) {
                targetField = sourceField;
            } else {
                targetField = fieldName;
            }


            if (oldSpfsaMap != null && oldSpfsaMap.size() > 0 && newSpfsaMap != null && newSpfsaMap.size() > 0) {
                // update

                for (String spfsaId : newSpfsaMap.keySet()) {
                    School_PFS_Assignment__c newSpfsa = newSpfsaMap.get(spfsaId);
                    School_PFS_Assignment__c oldSpfsa = oldSpfsaMap.get(spfsaId);
                    

                    // refactored to use origFieldName [dp] 9.9.13
                    // if orig field exists, we'll only count this as an update if the value has changed AND is different than the orig
                    if(SPAfieldNames.contains(origFieldName.toLowerCase())){
                        
                        if (newSpfsa.get(sourceField) != oldSpfsa.get(sourceField)){
                            createUpdateFieldCount(newSpfsa, sourceField, targetField);
                        }
                    // otherwise we'll count this as an update if it has changed at all.
                    } else if (newSpfsa.get(sourceField) != oldSpfsa.get(sourceField)){
                        createUpdateFieldCount(newSpfsa, sourceField, targetField);
                    }
                }
            }
        } 
        
        // update 
        if (fieldCountUpdateList.size() > 0) {
            update fieldCountUpdateList;
        }                     

        // create 
        if (fieldCountNewList.size() > 0) {
            insert fieldCountNewList;
        }                     

        // end trigger execution -- don't do this, we want to keep the isExectuting boolean set to true to prevent
        // multiple calls to this in the same context
        // isExecuting = false;
    }
    
    private void loadSPFSASourceTargetMap() {
    
        // NAIS-40
        // Q: I think we need to store the field names that we count into custom settings. 
        // This will allow us to add further fields without changing code
        // A: Please do not store the fields in custom settings. If they want to add fields next year, 
        // the code will change, that is expected

        // convention is that if target field is different name then specify   
        spfsSourceTargetMap.put('Add_Deprec_Home_Bus_Expense__c', null);
        spfsSourceTargetMap.put('Apply_Minimum_Income__c', null);
        //spfsSourceTargetMap.put('Business_Farm_Assets__c', null);
        //spfsSourceTargetMap.put('Business_Farm_Debts__c', null);
        //spfsSourceTargetMap.put('Business_Farm_Owner__c', null); // [DP] 03.02.2016 SFP-332 SPA.Business Farm Owner is now an EFC output
        //spfsSourceTargetMap.put('Business_Farm_Ownership_Percent__c', null);
        spfsSourceTargetMap.put('Child_Support_Received__c', null); 
        spfsSourceTargetMap.put('Dependent_Children__c', 'Number_of_Dependent_Children__c');
        spfsSourceTargetMap.put('Dividend_Income__c', null);
        spfsSourceTargetMap.put('Family_Contribution__c', null);
        spfsSourceTargetMap.put('Family_Size__c', null);
        spfsSourceTargetMap.put('Home_Market_Value__c', null);
        spfsSourceTargetMap.put('Income_Protection_Allowance_Housing__c', null); //[G.S], SFP-8
        spfsSourceTargetMap.put('Interest_Income__c', null);
        spfsSourceTargetMap.put('Lock_Professional_Judgment__c', null);
        spfsSourceTargetMap.put('Medical_Dental_Expense__c', null); //[G.S], SFP-8
        //spfsSourceTargetMap.put('Net_Profit_Loss_Business_Farm__c', 'Net_Profit_Loss_Business__c');
        spfsSourceTargetMap.put('Num_Children_in_Tuition_Schools__c', null);
        spfsSourceTargetMap.put('Other_Real_Estate_Market_Value__c', null);
        spfsSourceTargetMap.put('Other_Real_Estate_Unpaid_Principal__c', null);
        spfsSourceTargetMap.put('Override_Default_COLA_Value__c', null);
        spfsSourceTargetMap.put('Salary_Wages_Parent_A__c', null);
        spfsSourceTargetMap.put('Salary_Wages_Parent_B__c', null);
        spfsSourceTargetMap.put('Social_Security_Benefits__c', null);
        spfsSourceTargetMap.put('Student_Assets__c', null);
        spfsSourceTargetMap.put('Unpaid_Principal_1st_Mortgage__c', null);
        spfsSourceTargetMap.put('Unpaid_Principal_2nd_Mortgage__c', null);
        spfsSourceTargetMap.put('Untaxed_IRA_Plan_Payment__c', null);
        spfsSourceTargetMap.put('Unusual_Expense__c', null); //[G.S], SFP-8
        spfsSourceTargetMap.put('Use_Home_Equity__c', null);
        spfsSourceTargetMap.put('Use_Home_Equity_Cap__c', null);
        spfsSourceTargetMap.put('Use_COLA__c', null);
        spfsSourceTargetMap.put('Use_Div_Int_to_Impute_Assets__c', null);
        spfsSourceTargetMap.put('Use_Housing_Index_Multiplier__c', null);
        
    }
    
    private void loadspfsSourceToOrigMapMap() {
        //spfsSourceToOrigMap.put('Self_Employment_Tax_Allowance_Currency_c', 'Orig_Self_Employment_Tax_Allowance_Curr_c');
        
        spfsSourceToOrigMap.put('Add_Deprec_Home_Bus_Expense__c', null);
        spfsSourceToOrigMap.put('Apply_Minimum_Income__c', null);
        //spfsSourceToOrigMap.put('Business_Farm_Assets__c', null);
        //spfsSourceToOrigMap.put('Business_Farm_Debts__c', null);
        //spfsSourceToOrigMap.put('Business_Farm_Owner__c', null); // [DP] 03.02.2016 SFP-332 SPA.Business Farm Owner is now an EFC output
        //spfsSourceToOrigMap.put('Business_Farm_Ownership_Percent__c', null);
        spfsSourceToOrigMap.put('Child_Support_Received__c', null); 
        spfsSourceToOrigMap.put('Dependent_Children__c', null);
        spfsSourceToOrigMap.put('Dividend_Income__c', null);
        spfsSourceToOrigMap.put('Family_Contribution__c', null);
        spfsSourceToOrigMap.put('Family_Size__c', null);
        spfsSourceToOrigMap.put('Home_Market_Value__c', null);
        spfsSourceToOrigMap.put('Income_Protection_Allowance_Housing__c', null); //[G.S], SFP-8
        spfsSourceToOrigMap.put('Interest_Income__c', null);
        spfsSourceToOrigMap.put('Lock_Professional_Judgment__c', null);
        spfsSourceToOrigMap.put('Medical_Dental_Expense__c', null); //[G.S], SFP-8
        //spfsSourceToOrigMap.put('Net_Profit_Loss_Business_Farm__c', null);
        spfsSourceToOrigMap.put('Num_Children_in_Tuition_Schools__c', null);
        spfsSourceToOrigMap.put('Other_Real_Estate_Market_Value__c', null);
        spfsSourceToOrigMap.put('Other_Real_Estate_Unpaid_Principal__c', null);
        spfsSourceToOrigMap.put('Override_Default_COLA_Value__c', null);
        spfsSourceToOrigMap.put('Salary_Wages_Parent_A__c', null);
        spfsSourceToOrigMap.put('Salary_Wages_Parent_B__c', null);
        spfsSourceToOrigMap.put('Social_Security_Benefits__c', null);
        spfsSourceToOrigMap.put('Student_Assets__c', null);
        spfsSourceToOrigMap.put('Unpaid_Principal_1st_Mortgage__c', null);
        spfsSourceToOrigMap.put('Unpaid_Principal_2nd_Mortgage__c', null);
        spfsSourceToOrigMap.put('Untaxed_IRA_Plan_Payment__c', null);
        spfsSourceToOrigMap.put('Unusual_Expense__c', null); //[G.S], SFP-8
        spfsSourceToOrigMap.put('Use_Home_Equity__c', null);
        spfsSourceToOrigMap.put('Use_Home_Equity_Cap__c', null);
        spfsSourceToOrigMap.put('Use_COLA__c', null);
        spfsSourceToOrigMap.put('Use_Div_Int_to_Impute_Assets__c', null);
        spfsSourceToOrigMap.put('Use_Housing_Index_Multiplier__c', null);
    }

    private void createUpdateFieldCount(School_PFS_Assignment__c newSpfsa, String sourceField, String targetField) {
    
        // NAIS-40
        // Q: What is the initial value set in counter when record is created ? Is it 0 or 1 ?
        // A: You are counting updates to a field, so when the field is updated, the counter increments. 
        // If you create the record before an update is made, it would be 0. If you create the record 
        // after an update is first made, it would be 1.
        
        String key = newSpfsa.School__c + '_' + GlobalVariables.getAcademicYearByName(newSpfsa.Academic_Year_Picklist__c).Id; // NAIS-2417 [DP] 05.19.2015
        if (key_fieldCount_map.containsKey(key)) {
            // update field count record
            Field_Count__c fc = key_fieldCount_map.get(key);
            if (fc.get(targetField) != null) {
                fc.put(targetField, (Decimal)fc.get(targetField) + 1);
            } else {
                fc.put(targetField, 1);    
            }
            
            // fc.Id != null will prevent adding new records to update
            if (fc.Id != null && fieldCountUpdateSet.add(fc.Id)) {
                fieldCountUpdateList.add(fc);    // add only once
            }
        } else {
            // create new field count record
            Field_Count__c fc = new Field_Count__c();
            fc.School__c = newSpfsa.School__c;
            fc.Academic_Year__c = GlobalVariables.getAcademicYearByName(newSpfsa.Academic_Year_Picklist__c).Id; // NAIS-2417 [DP] 05.19.2015
            if (newSpfsa.get(sourceField) != null) {
                fc.put(targetField, 1);    
            }
            if (fieldCountNewSet.add(key)) {
                fieldCountNewList.add(fc);    // add only once
                
                key_fieldCount_map.put(key, fc);
            }
        }
    }
    
    public class FieldCountException extends Exception{}
    
    private String getFieldCountQuery() {
    
        String fieldStr = '';
        Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Field_Count__c.fields.getMap();

        for (Schema.SObjectField field : fieldMap.values()) {
            Schema.DescribeFieldResult dfr = field.getDescribe();
            if (fieldStr == '') {
                fieldStr = dfr.getName();
            } else {
                fieldStr += ', ' + dfr.getName();
            }
            fieldResultsMap.put(dfr.getName(), dfr);
        }
        
        if(fieldStr == '' || fieldStr == ' '){
            throw new FieldCountException('No Field Counts found, please verify that the Field Count object is accessible to the current user.');
        }
        
        String query = 'select ' + fieldStr + ' from Field_Count__c ';
        
        return query;
    }    
}