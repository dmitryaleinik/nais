/*
 * NAIS-2013: New school user administration pages - new main page and new 
 * Add/View contact page. This Add/view contact page will double as the 
 * "My Profile" page as well - with some minor differences.
 * Leydi, 04-2015
 */
public class SchoolUserAdminProfileController{
    
    /*Initialization*/
    public SchoolUserAdminProfileController(ApexPages.StandardController controller)
    {
        this.isFuture = 0;
        String paramId = System.currentPageReference().getParameters().get('id');
        paramId=(System.currentPageReference().getParameters().get('o')=='1'?
                (GlobalVariables.getCurrentUser()).ContactId:paramId);
        this.showCancelButtonOnViewMode = (System.currentPageReference().getParameters().get('return')=='1'?true:false);
        this.currentContact = getContactById(paramId);
        this.priorEmail = (this.currentContact!=null && this.currentContact.Id!=null?this.currentContact.Email:null);
        this.currentUser = this.getUserByContactId(this.currentContact);
        this.selectedProfileHasTimeZone = (currentUserProfileId!=null && currentUserProfileId!=''?true:false);
        String action = System.currentPageReference().getParameters().get('edit');        
        this.showEditMode = (this.currentContact.Id==null || (action!=null && action=='1')?true:false);        
        this.contactInvalidId = (paramId!=null && this.currentContact.Id==null?true:false);    
        this.showPopup = false;
    }
    /*End Initialization*/
    
    /*Properties*/
    public boolean showCancelButtonOnViewMode{get; set; }
    public Integer isFuture{ get; set; }//1:Grant portal Access | 2:Remove Portal Access
    public static boolean fromFutureMethod{get;set;}//To avoid exception: Future method cannot be called from a future or batch method
    public Contact currentContact { get; set; }
    public User currentUser { get; set; }
    public boolean contactInvalidId{get;set;}
    //currentUserProfileId:Used for Edit mode, 'cause if we use User.ProfileId an 
    //exception is shown: core.apexpages.exceptions.ApexPagesDeveloperException: Invalid field ProfileId for SObject User. 
    public String currentUserProfileId { get; set; }
    private String priorUserProfileId{get; set; }
    @TestVisible private String priorEmail{get; set; }
    public boolean showEditMode  {get; set; }
    public boolean selectedProfileHasTimeZone  {get; set; }
    public boolean selectedProfileIsPortal {get{return currentUserProfileId == null || currentUserProfileId == '' ? false : GlobalVariables.isSchoolPortalProfile(currentUserProfileId);}set;}
    public list<SelectOption> currentAvailableProfiles  {get; set; }
    public boolean showPopup  {get; set; }//SFP#19, [G.S]
    
    /*End Properties*/
    
    /*Action Methods*/
    public boolean getIsMyProfilePage()
    {
        return (this.currentUser!=null && this.currentUser.Id!=null && this.currentUser.Id==(GlobalVariables.getCurrentUser()).Id?true:false);
    }//End:getIsMyProfilePage
    
    public PageReference editAction()
    {
        this.showEditMode = true;
        return null;
    }//End:editAction
    
    public PageReference cancelAction()
    {
        this.showEditMode = false;
        return null;
    }//End:cancelAction
    
    //SFP#19, [G.S]
    public PageReference cancelPopup()
    {
        this.showPopup = false;
        return null;
    }

    // [SFP-364] : chatter email notifications 
    public class NetworkMemberWrapper{
        public NetWorkMember member  {get;set;}
        public string networkId {get;set;}
        public boolean receiveEmails {get;set;}
        public boolean commentsOnAnItemIBookmarked {get;set;}
        public boolean commentsOnMyStatusOrAChangeIMade {get;set;}
        public boolean endorsesMeOnATopic {get;set;}
        public boolean followsMe  {get;set;}
        public boolean commentsAfterMe {get;set;}
        public boolean likesAPostOrCommentImade {get;set;}
        public boolean mentionsMeInAPost {get;set;}
        public boolean sendsMeAMessage {get;set;}
        public boolean postsOnMyProfile {get;set;}
        public boolean sharesAPostIMade {get;set;}
        public boolean commentsOnAnItemILike {get;set;}
        public boolean mentionsMeInAComment {get;set;}
        public boolean commentsOnAPostOnMyProfile {get;set;}
        
        public NetworkMemberWrapper(NetworkMember member){
            this.member = member;
            networkId = Network.getNetworkId();
            // boolean properties are all reverse of what we want them to show to the user in the UI
            receiveEmails = !member.PreferencesDisableAllFeedsEmail;
            commentsOnAnItemIBookmarked  = !member.PreferencesDisableBookmarkEmail;
            commentsOnMyStatusOrAChangeIMade   = !member.PreferencesDisableChangeCommentEmail;
            endorsesMeOnATopic    = !member.PreferencesDisableEndorsementEmail;
            followsMe    = !member.PreferencesDisableFollowersEmail;
            commentsAfterMe   = !member.PreferencesDisableLaterCommentEmail;
            likesAPostOrCommentImade   = !member.PreferencesDisableLikeEmail;
            mentionsMeInAPost   = !member.PreferencesDisableMentionsPostEmail;
            sendsMeAMessage     = !member.PreferencesDisableMessageEmail;
             postsOnMyProfile   = !member.PreferencesDisableProfilePostEmail;
            sharesAPostIMade     = !member.PreferencesDisableSharePostEmail;
            commentsOnAnItemILike    = !member.PreferencesDisCommentAfterLikeEmail;
            mentionsMeInAComment    = !member.PreferencesDisMentionsCommentEmail;
            commentsOnAPostOnMyProfile    = !member.PreferencesDisProfPostCommentEmail;
        }

        public void setMemberFields(){
            member.PreferencesDisableAllFeedsEmail = !receiveEmails;
            member.PreferencesDisableBookmarkEmail = !commentsOnAnItemIBookmarked;
            member.PreferencesDisableChangeCommentEmail = !commentsOnMyStatusOrAChangeIMade;
            member.PreferencesDisableEndorsementEmail = !endorsesMeOnATopic;
            member.PreferencesDisableFollowersEmail = !followsMe;
            member.PreferencesDisableLaterCommentEmail = !commentsAfterMe;
            member.PreferencesDisableLikeEmail = !likesAPostOrCommentImade;
            member.PreferencesDisableMentionsPostEmail = !mentionsMeInAPost;
            member.PreferencesDisableMessageEmail = !sendsMeAMessage;
            member.PreferencesDisableProfilePostEmail = !postsOnMyProfile;
            member.PreferencesDisableSharePostEmail = !sharesAPostIMade;
            member.PreferencesDisCommentAfterLikeEmail = !commentsOnAnItemILike;
            member.PreferencesDisMentionsCommentEmail = !mentionsMeInAComment;
            member.PreferencesDisProfPostCommentEmail = !commentsOnAPostOnMyProfile ;
        }
    }
    private NetworkMemberWrapper member;
    public NetworkMemberWrapper getCurrentNetworkMember(){
        if(member == null){
            String communityId= Network.getNetworkId();
            String userId = currentUser.id;

            list<NetworkMember> members = [SELECT NetworkId, DefaultGroupNotificationFrequency, DigestFrequency, PreferencesDisableAllFeedsEmail, PreferencesDisableBookmarkEmail, PreferencesDisableChangeCommentEmail, PreferencesDisableEndorsementEmail, PreferencesDisableFollowersEmail, PreferencesDisableLaterCommentEmail, PreferencesDisableLikeEmail, PreferencesDisableMentionsPostEmail, PreferencesDisableMessageEmail, PreferencesDisableProfilePostEmail, PreferencesDisableSharePostEmail, PreferencesDisCommentAfterLikeEmail, PreferencesDisMentionsCommentEmail, PreferencesDisProfPostCommentEmail
                                            FROM NetworkMember WHERE NetworkId=:communityId AND MemberId=:userId];
            if(!members.isEmpty())
                member = new NetworkMemberWrapper(members[0]);
            else
                member = new NetworkMemberWrapper(new NetworkMember());
        }
        return member;
    }
    
    //SFP#19, [G.S]
    public PageReference validateContact(){
        this.showPopup = false;
        
        if(currentContact.SSS_Main_Contact__c && String.isNotBlank(currentContact.AccountId)) {
            
            String cntctQry = 'Select Id,SSS_Main_Contact__c,AccountId from Contact where AccountId != null and AccountId = \'' + 
                              currentContact.AccountID + '\' and SSS_Main_Contact__c = true';
            
            if(String.isNotBlank(this.currentContact.Id)){
                
                cntctQry = cntctQry+' and Id != \''+this.currentContact.Id+'\'';
            }
            
            List<Contact> cntctList = Database.query(cntctQry);
            
            if(cntctList != null && cntctList.size() > 0) {
                
                this.showPopup = true;
                return null;
            }
        }
        
        return saveAction();
    }
    
    public PageReference saveAction()
    {
        if (contactEmailAlreadyExists())
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, Label.SP_Duplicate_Contact_Email_Error_Message));
            return null;
        }
        
        boolean isNewContact = (this.currentContact.Id==null?true:false);
        boolean hasAccessToPortal = (this.currentUser.Id!=null?true:false);
        Savepoint sp = Database.setSavepoint();
        try{
            upsert currentContact;
            priorUserProfileId = currentUser.ProfileId;
            currentUser.ProfileId = (currentUserProfileId==''?null:currentUserProfileId);
            this.upsertUser(isNewContact, hasAccessToPortal);
            this.showEditMode = false;

            // update chatter settings if this is an existing user
            if(!Test.isRunningTest() && !isNewContact && hasAccessToPortal){
                getCurrentNetworkMember().setMemberFields();
                update getCurrentNetworkMember().member;
            }

        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                    'SchoolUserAdminProfileController[saveAction]: '+e+' '+e.getLineNumber()));
            Database.rollback(sp);
        }
        return (!ApexPages.hasMessages(ApexPages.Severity.ERROR) && (this.showCancelButtonOnViewMode || isNewContact)
                ?(this.isFuture==0?this.redirectPage(Page.SchoolUserAdmin, new map<String,String>())
                                :this.redirectPage(Page.SchoolUserAdmin, new map<String,String>{'future' => String.ValueOf(this.isFuture)}))
                :null);
    }//End:saveAction
    
    private Boolean contactEmailAlreadyExists()
    {
        List<Contact> existingContactsWithSameEmail = ContactSelector.newInstance().selectBySchoolIdAndEmail(
            currentContact.AccountId, currentContact.Email);

        // Create a map of the contacts so we can easily make sure the current contact is not included in our check.
        Map<Id, Contact> existingContactsById = new Map<Id, Contact>(existingContactsWithSameEmail);

        // Make sure the current contact is not included in our map.
        existingContactsById.remove(currentContact.Id);

        return !existingContactsById.isEmpty();
    }

    public PageReference cancelVieModeAction()
    {
        return this.redirectPage(Page.SchoolUserAdmin, new map<String,String>{});
    }//End:cancelVieModeAction
    
    public PageReference reloadPage()
    {
        return this.redirectPage(Page.SchoolUserAdminProfile, new map<String,String>{'id' => currentContact.Id});
    }//End:reloadPage
    
    public PageReference onChangeSelectedUserProfile()
    {
        this.selectedProfileHasTimeZone = (currentUserProfileId!=null && currentUserProfileId!=''?true:false);
        return null;
    }//End:changeSelectedProfileId
    /*End Action Methods*/
    
    /*Helper Methods*/
    private void upsertUser(boolean isNewContact, boolean hasAccessToPortal)
    {
        //Case #01: New Contact with 'No Portal Access': Then a new contact is created.
        if(isNewContact && !hasAccessToPortal && this.currentUserProfileId!='' && this.currentUserProfileId!=null){
            /*Case #02: New Contact with 'Portal Access': Then a new contact is created and it's 
            related user is also created.*/
            this.grantAccessToPortal();
        }else if(!isNewContact){
            if(this.currentUser.ProfileId==null && this.currentUser.ProfileId!=priorUserProfileId){
                /*Case #3: Update Contact with 'Portal Access' to 'No Portal Access': Then the related 
                User.IsActive is set to false.*/
                this.isFuture=2;
                activateUser(this.currentUser.Id, false);
                this.currentUser = getEmptyUser();
            }else if(priorUserProfileId!=null && this.currentUser.ProfileId!=priorUserProfileId){
                /*Case #4: Update Contact with 'Portal Access' to a different Profile: Then the 
                User.ProfileId is updated.*/
                this.updateExistingUser(this.currentUser, false);
                changeUserProfile(this.currentUser.Id, currentUser.ProfileId);
            }else if(priorUserProfileId==null && this.currentUser.ProfileId!=null){
                /*Case #5: Update Contact with 'No Portal Access' to 'Portal Access': A new User is 
                created and related to the contact.*/
                this.grantAccessToPortal();
            }else{
                /*Case #6: Update Contact and Profile remains the same: Contact and User object 
                fields are updated. */
                this.updateExistingUser(this.currentUser, false);
            }
        }
    }//End:upsertUser
    
    private void grantAccessToPortal()
    {
        if (currentContact.Username_Exists_In_Other_Org__c) {
            currentContact.Username_Exists_In_Other_Org__c = false;
            update currentContact;
        }
        
        this.isFuture=1;
        User existingUser = SchoolUserAdminProfileController.searchCurrentUserByEmail((this.priorEmail!=null && this.priorEmail!=this.currentContact.Email?
                                    this.priorEmail:this.currentContact.Email), this.currentContact.Id);
        if(existingUser==null){
            createNewUser(this.currentContact.Id, this.currentUserProfileId, this.currentUser.TimeZoneSidKey, JSON.serialize(getCurrentNetworkMember()));
        }else{
            this.updateExistingUser(existingUser, true);
        }
    }//End:grantAccessToPortal
    
    private void updateExistingUser(User existingUser, boolean resetUser)
{        
    
        updateExistingUserFuture((this.priorEmail!=null && this.priorEmail!=this.currentContact.Email?
                                    this.priorEmail:this.currentContact.Email), 
                                    this.currentContact.Id, this.currentUser.TimeZoneSidKey, currentUserProfileId, resetUser);

        //Since we can't wait for @future method to update form variables we do it here
        this.currentUser = existingUser;
        this.currentUser = SchoolUserAdminProfileController.setUserFieldValuesFromContact(this.currentUser, this.currentContact);
        this.currentUser.TimeZoneSidKey = this.currentUser.TimeZoneSidKey;
        this.currentUser.profileId = currentUserProfileId;
        
    }//End:updateExistingUser
    
    @future 
    public static void createNewUser(String contactId, String profileId, String timeZoneSidKey, string nmw)
    {
        fromFutureMethod = true;
        Contact thisContact = getContactById(contactId);
        User newUser = setUserFieldDefaultValues(new User(), thisContact);
        newUser = setUserFieldValuesFromContact(newUser, thisContact);
        newUser.profileId = profileId;
        newUser.TimeZoneSidKey = timeZoneSidKey;
        newUser.IsActive = true;
        newUser.IsPrmSuperUser = true;
        newUser.SpringCMEos__SpringCM_User__c = false;
        newUser.UserPreferencesDisableAllFeedsEmail = true;
        newUser = setUserActiveFields(newUser, true);
        newUser = triggerNewUserEmail(newUser, true);//Triggers new user email
        
        try {
            insert newUser;
        }catch(Exception e) {
            if ( e.getMessage().contains('DUPLICATE_USERNAME') ) {
                
                thisContact.Username_Exists_In_Other_Org__c = true;
                update thisContact;
            }
            
            return;
        }
    
        // [SFP-364] : chatter email notifications for new users
        NetworkMemberWrapper nmwObject = (NetworkMemberWrapper) JSON.deserialize(nmw, NetworkMemberWrapper.class);
        NetWorkMember member;
        list<NetworkMember> members = [SELECT id, DefaultGroupNotificationFrequency, DigestFrequency, PreferencesDisableAllFeedsEmail, PreferencesDisableBookmarkEmail, PreferencesDisableChangeCommentEmail, PreferencesDisableEndorsementEmail, PreferencesDisableFollowersEmail, PreferencesDisableLaterCommentEmail, PreferencesDisableLikeEmail, PreferencesDisableMentionsPostEmail, PreferencesDisableMessageEmail, PreferencesDisableProfilePostEmail, PreferencesDisableSharePostEmail, PreferencesDisCommentAfterLikeEmail, PreferencesDisMentionsCommentEmail, PreferencesDisProfPostCommentEmail
                                            FROM NetworkMember WHERE NetworkId=:nmwObject.networkId AND MemberId=:newuser.Id];
        if(!members.isEmpty()){
            member = members[0];
            // can now update the chatter notifications settings on the newly created networkmember record for this user
            nmwObject.member.id = member.id;
            nmwObject.setMemberFields();
            update nmwObject.member;
        }
    }//End:createNewUser
    
    @future 
    public static void updateExistingUserFuture(String contactEmail, String contactId, String timeZone, String profileId, boolean resetUser)//@future methods can't receive SObjects
    {
        User newUser = SchoolUserAdminProfileController.searchCurrentUserByEmail(contactEmail, contactId);
        Contact currentContact = [Select Id, FirstName, LastName, Email 
                                from Contact where 
                                Id=:contactId limit 1];
        newUser = SchoolUserAdminProfileController.setUserFieldValuesFromContact(newUser, currentContact);
        newUser.TimeZoneSidKey = timeZone;
        newUser.profileId = profileId;
        newUser = setUserActiveFields(newUser, true);
        update newUser;
        if(resetUser){
            system.resetPassword(newUser.Id, true);//Triggers reset password email
        }
    }//End:updateExistingUser
    
    public static User setUserFieldDefaultValues(User newUser,Contact thisContact)
    {
        newUser.LanguageLocaleKey = 'en_US';
        newUser.LocaleSidKey = 'en_US';
        newUser.EmailEncodingKey = 'ISO-8859-1';        
        newUser.SpringCM_PortalUser__c = true;
        newUser.CommunityNickName = thisContact.Email.substring(0, thisContact.Email.indexOf('@')) +'_'+Math.round((Math.random() * 100000));
        newUser.Alias = string.valueof(thisContact.FirstName.substring(0,1) + thisContact.LastName.substring(0,1));        
        newUser = SchoolUserAdminProfileController.setUserFieldValuesFromContact(newUser, thisContact);
        if(newUser.Id==null){//To avoid exception (onUpdate):Field is not writeable: User.ContactId
            newUser.contactId = thisContact.Id;
        }
        return newUser;        
    }//End:setUserFieldDefaultValues
    
    public static User setUserFieldValuesFromContact(User newUser, Contact currentContact)
    {
        newUser.FirstName = currentContact.FirstName;
        newUser.LastName = currentContact.LastName;
        newUser.Username = (newUser.Username==null || newUser.Username==''
                            ?currentContact.Email:newUser.Username);
        newUser.Email = currentContact.Email;
        return newUser;        
    }//End:setUserFieldValuesFromContact
        
    @future 
    public static void updateUser(String userId, String ProfileId,String TimeZoneSidKey,boolean isActive)
    {
        try{
            User thisUser = [Select Id, TimeZoneSidKey, ProfileId, IsPrmSuperUser from User where Id=:userId limit 1];
            thisUser.ProfileId = ProfileId;
            thisUser.TimeZoneSidKey = TimeZoneSidKey;
            thisUser = setUserActiveFields(thisUser, isActive);
            update thisUser;
        }catch(Exception e){
            system.debug('SchoolUserAdminProfileController[updateUser]: '+e);
        }
    }//End:updateUser    
    
    @future 
    public static void activateUser(String userId, boolean isActive)
    {
        try{
            User thisUser = [Select Id, isActive, IsPrmSuperUser from User where Id=:userId limit 1];
            thisUser = setUserActiveFields(thisUser, isActive);
            update thisUser;
        }catch(Exception e){
            system.debug('SchoolUserAdminProfileController[activateUser]: '+e);
        }
    }//End:activateUser
    
    private static User setUserActiveFields(User thisUser, boolean isActive)
    {
        thisUser.IsActive = isActive;
        thisUser.IsPrmSuperUser = isActive;
        return thisUser;
    }//End:setUserActive
    
    @future 
    public static void changeUserProfile(String userId, Id profileId)
    {
        try{
            User thisUser = [Select Id, profileId from User where Id=:userId limit 1];
            thisUser.profileId = profileId;
            update thisUser;
        }catch(Exception e){
            system.debug('SchoolUserAdminProfileController[changeUserProfile]: '+e);
        }
    }//End:activateUser
    
    private PageReference redirectPage(PageReference page, map<String,String> params)
    {
        if( params!=null && params.size()>0 ){
            for(String s:params.keyset()){
                page.getParameters().put(s,params.get(s));
            }
        }
        page.setRedirect(true);
        return page;
    }//End:redirectPage
    
    public static User searchCurrentUserByEmail(String email, String contactId)
    {
        User foundUser = null;
        try{
            foundUser = [Select Id, LanguageLocaleKey, LocaleSidKey, EmailEncodingKey, 
                                    SpringCM_PortalUser__c, CommunityNickName, TimeZoneSidKey, 
                                    IsActive, Alias, contactId, FirstName, LastName, Username, 
                                    Email, profileId, IsPrmSuperUser 
                                    from User 
                                    where Email=:email 
                                    and ContactId=:contactId 
                                    and ContactId<>null 
                                    limit 1];
        }catch(Exception e){
                system.debug('SchoolUserAdminProfileController[getUserByEmail]: '+e);
        }
        return foundUser;
    }//End:searchCurrentUserByEmail
    
    private User getEmptyUser()
    {
        return new User(TimeZoneSidKey='America/Los_Angeles');
    }//End:getEmptyUser
    
    private User getUserByContactId(Contact valContact)
    {
        User foundUser = null;
        if(valContact!=null && valContact.Id!=null)
        {
            try{
                foundUser = [Select Id, TimeZoneSidKey, ProfileId, Email, IsPrmSuperUser, ContactId   
                                        from User 
                                        where ContactId=:valContact.Id 
                                        and IsActive=true 
                                        limit 1];
            }catch(Exception e){
                system.debug('SchoolUserAdminProfileController[getUserByContactId]: '+e);
            }
        }
        
        foundUser = (foundUser!=null
                    ?foundUser
                    :getEmptyUser());
                    
        foundUser.ProfileId = (foundUser.ProfileId!=null?foundUser.ProfileId:null);
        this.currentUserProfileId = foundUser.ProfileId;
        
        this.currentAvailableProfiles = this.getProfilesList(new list<String>{
                                                            ProfileSettings.SchoolAdminProfileName,
                                                            ProfileSettings.SchoolPowerUserProfileName,
                                                            ProfileSettings.SchoolUserProfileName},
                                                            foundUser.ProfileId);
        
        return foundUser;
    }//End:getUserByContactId
    
    public static Contact getContactById(String valId)
    {
        try{
            Contact foundContact = [Select Id, salutation, Username_Exists_In_Other_Org__c, 
                                    FirstName, LastName, Suffix__c, 
                                    Title, Role__c, Phone, PFS_Alert_Preferences__c, 
                                    Receive_SSS_Correspondence__c, SSS_Main_Contact__c, 
                                    AccountId, RecordTypeId, Email 
                                    from Contact 
                                    where Id=:valId 
                                    limit 1];
            return foundContact;
        }catch(Exception e){
            system.debug('SchoolUserAdminProfileController[getContactById]: '+e);            
        }
        return new Contact(AccountId=GlobalVariables.getCurrentSchoolId(),
                            Receive_SSS_Correspondence__c=true,
                            RecordTypeId=RecordTypes.schoolStaffContactTypeId);
    }//End:getContactById
    
    /**
    * @brief Method implemented to fill the SelectOption input with the available profiles only.
    * @param availableProfiles: The Profiles available to be listed in edit mode.
    * @param selectedProfile: The current Contact.User.ProfileId.
    * @return The Select Option list with the available profiles in edit mode.
    */
    private list<SelectOption> getProfilesList(list<String> availableProfiles, String selectedProfile)
    {
        boolean selectedProfileFound = (selectedProfile==null?true:false);
        list<SelectOption> selectListAvailableProfiles = new list<SelectOption>{
                                                    new SelectOption('','No Portal Access')
                                                };
        list<Profile> listProfiles = [Select Id, Name from Profile where Name IN: availableProfiles];
        if(listProfiles!=null){
            for(Profile p:listProfiles){
                selectListAvailableProfiles.add(new SelectOption(String.ValueOf(p.Id),p.Name));
                if( p.Id == selectedProfile){
                    selectedProfileFound = true;
                }
            }
        }
        
        selectListAvailableProfiles = this.createNewProfileSelectOption(!selectedProfileFound, selectedProfile, selectListAvailableProfiles);
        return selectListAvailableProfiles;
    }//End:getProfilesList
    
    /**
    * @brief Method implemented to add a new profile to SelectList options. This 
    * may happen if the cotact that is being viewed has a profile diferent than the allowed ones.
    * @param create: Indicates if a new profile needs to be added to the SelectOption list.
    * @param selectedProfile: The current Contact.User.ProfileId.
    * @param selectListAvailableProfiles: The Profiles list to be show in edit mode.
    * @return The Select Option list with the available profiles in edit mode.
    */
    private list<SelectOption> createNewProfileSelectOption(boolean create, 
                                                            String selectedProfile, 
                                                            list<SelectOption> selectListAvailableProfiles)
    {
        if(create){
            try{
                Profile selectProfileSearch = [Select Id, Name from Profile where Name=:selectedProfile limit 1];
                selectListAvailableProfiles.add(new SelectOption(selectProfileSearch.Id,selectProfileSearch.Name));
            }catch(Exception e){
                system.debug('SchoolUserAdminProfileController[createNewProfileSelectOption]: '+e);
            }
        }
        return selectListAvailableProfiles;
    }//End:createNewProfileSelectOption
    
    public static User triggerNewUserEmail(User thisUser, boolean isSend)
    {
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.EmailHeader.triggerUserEmail = isSend;
        thisUser.setOptions(dmo);
        return thisUser;
    }//End:sendEmail
    
    /**
    * @description Used to disable portal access to users. After the update is completed the following fields are automatically updated:
    *               * User.ContactId = null
    *               * User.IsActive = false
    * @param users The list of users to be updated.
    * @return The list contactIds that were related to disabled users.
    */ 
    public static Set<Id> disablePortalAccessForUsers(List<User> users) {
        
        List<User> result = new List<User>();
        Set<Id> contactIds = new Set<Id>();
        
        for (User tmpUser : users) {
            
            if (tmpUser.ContactId != null) {
                contactIds.add(tmpUser.ContactId);
            }
            
            tmpUser.IsPortalEnabled = false;
            result.add(tmpUser);
        }
        
        if (!result.isEmpty()) {
            update result;
        }
        
        return contactIds;
    }
    
    @future public static void linkContactWithAccount(Map<Id, Id> accountsBycontactId) {
        
        List<Contact> contactsToUpdate = new List<Contact>();
        
            
        for (Contact c : [SELECT Id, AccountId FROM Contact WHERE Id IN: accountsBycontactId.keySet()]) {
            
            c.AccountId = accountsBycontactId.get(c.Id);
            contactsToUpdate.add(c);
        }
        
        if (!contactsToUpdate.isEmpty()) {
            update contactsToUpdate;
        }
    }
    /*End Helper Methods*/
}