@isTest
private class PfsBeforeTest
{
    
    @isTest
    private static void testClearParentBIncomeInformation()
    {
        PFS__c singlePfs = createPfsWithParentBIncomeInfo(1)[0];
        List<PFS__c> pfsRecordsForBulkTest = createPfsWithParentBIncomeInfo(10);

        List<PFS__c> allPfsRecords = new List<Pfs__c>{singlePfs};
        allPfsRecords.addAll(pfsRecordsForBulkTest);
        insert allPfsRecords; 
        
        Set<Id> pfsIds = new Set<Id>{singlePfs.Id};
        singlePfs.Student_Has_Parent_B__c = 'No';

        for (Pfs__c pfs : pfsRecordsForBulkTest)
        {
            pfs.Student_Has_Parent_B__c = 'No';
            pfsIds.add(pfs.Id);
        }

        Test.startTest();
            update singlePfs;
            update pfsRecordsForBulkTest;
        Test.stopTest();

        Map<Id, Pfs__c> pfsMapping = new Map<id, Pfs__c>(PfsSelector.newInstance().selectWithCustomFieldListById(pfsIds, 
            new List<String>{'Salary_Wages_Parent_B__c', 'Salary_Wages_Parent_B_Est__c', 'Parent_B_Employee_Retirement_Plan__c'}));
        singlePfs = pfsMapping.get(singlePfs.Id);

        ////Expected that Parent B financional info was cleared both on single and bulk updates
        System.assertEquals(null, singlePfs.Salary_Wages_Parent_B__c);
        System.assertEquals(null, singlePfs.Salary_Wages_Parent_B_Est__c);
        System.assertEquals(null, singlePfs.Parent_B_Employee_Retirement_Plan__c);

        System.assertEquals(null, pfsMapping.get(pfsRecordsForBulkTest[0].Id).Salary_Wages_Parent_B__c);
        System.assertEquals(null, pfsMapping.get(pfsRecordsForBulkTest[0].Id).Salary_Wages_Parent_B_Est__c);
        System.assertEquals(null, pfsMapping.get(pfsRecordsForBulkTest[0].Id).Parent_B_Employee_Retirement_Plan__c);
        System.assertEquals(null, pfsMapping.get(pfsRecordsForBulkTest[9].Id).Salary_Wages_Parent_B__c);
        System.assertEquals(null, pfsMapping.get(pfsRecordsForBulkTest[9].Id).Salary_Wages_Parent_B_Est__c);
        System.assertEquals(null, pfsMapping.get(pfsRecordsForBulkTest[9].Id).Parent_B_Employee_Retirement_Plan__c);
    }

    private static List<PFS__c> createPfsWithParentBIncomeInfo(Integer numberOfRecordsToCreate)
    {
        List<PFS__c> pfsRecords = new List<PFS__c>();
        
        for (Integer i=0; i<numberOfRecordsToCreate; i++)
        {
            pfsRecords.add(
                PfsTestData.Instance
                    .forStudentHasParentB('Yes')
                    .forSalaryWagesParentB(1000)
                    .forSalaryWagesParentBEst(1000)
                    .forParentBEmployeeRetirementPlan('Yes').create());
        }

        return pfsRecords;
    }
}