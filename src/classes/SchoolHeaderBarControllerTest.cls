@isTest
private class SchoolHeaderBarControllerTest {

    @isTest
    private static void constructor_runAsSchoolUser_expectControllerCreated() {
        User schoolPortalUser = UserTestData.insertSchoolPortalUser();
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        Test.setCurrentPage(Page.SchoolDashboard);

        ApexPages.currentPage().getParameters().put('academicyearid', currentAcademicYear.Id);

        SchoolHeaderBarController controller;
        System.runAs(schoolPortalUser) {
            controller = new SchoolHeaderBarController();
        }

        System.assertNotEquals(null, controller, 'Expected the controller to not be null.');
    }
}