/*
 * ApplicantDataAccessService
 *
 * @description Data access service class for Applicant__c
 *
 * @author Mike Havrilla @ Presence PG
 */

public class ApplicantDataAccessService extends DataAccessService {


    /**
	 * @description: Queries an applicant by a collection of first anmes, last names, birthdays, extenralIds.
	 *
	 * @param firstNames - collection of first names
	 *
	 * @param lastNames - collection of last names
	 *
	 * @param birthDates - collection of birthDates
	 *
	 * @param applicantKeys - collection of applicant Keys to query against extenral id.
	 *
	 * @param applicantIdField - externalId fieldName to query on applicant
	 *
	 * @return A collection of applicant objects
	 */
    public List<sObject> getApplicantsByNames( List<String> firstNames, List<String> lastNames, List<Date> birthDates, Set<String> applicantKeys, String applicantIdField, Id pfsId) {
        List<sObject> returnval;

        if( firstNames == null && lastNames == null && birthDates == null && applicantKeys == null && applicantIdField == null && pfsId == null) return returnval;

        try {

            returnVal = Database.query( 'select ' + String.join( super.getSObjectFieldNames( 'Applicant__c'), ',') +
                    ' from Applicant__c' +
                    ' where (( First_Name__c in :firstNames ' +
                    ' and Last_Name__c in :lastNames ' +
                    ' and Birthdate_new__c in :birthdates ) ' +
                    ' or ( ' + applicantIdField + ' in :applicantKeys ))' +
                    ' and PFS__c = :pfsId' +
                    ' order by CreatedDate desc ');

        } catch( Exception e) {

            System.debug( 'DEBUG:::exception in ApplicantDataAccessService.getApplicantsByNames:Error:' + e.getMessage());
            throw new DataAccessServiceException( e);
        }

        return returnval;
    }
}