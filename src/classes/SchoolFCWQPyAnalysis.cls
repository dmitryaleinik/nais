public with sharing class SchoolFCWQPyAnalysis {

    private Map<String, String> rowNameToProperName;

    /**
     * @description The data used for analyzing changes of a single PFS value for a returning family. The data is
     *              queried and provided by the GuidedFolderReview.page via SchoolFamilyContributionWorksheetQuick.cls.
     */
    public PFSRowData row { get; set; }

    /**
     * @description Default constructor.
     */
    public SchoolFCWQPyAnalysis() { }

    /**
     * @description Gets a map containing the labels of the rows that should be shown in the GFR. The keys correspond to
     *              the name attributes of the SchoolFCWQPyAnalysis components on the GuidedFolderReview.page. The keys
     *              are case sensitive so any updates made here should be made to the components used on the
     *              GuidedFolderReview.page and vice versa. Failure to do so will break the page.
     * @return A map.
     */
    public Map<String, String> getRowNameToProperName(){
        if( rowNameToProperName == null ){
            rowNameToProperName = new Map<String, String> {
                    'totalTaxableIncome'=>'Total Taxable Income',
                    'totalNonTaxableIncome'=>'Total Nontaxable Income',
                    'TotalIncome'=>'Total Income',
                    'unusualExpense'=>'Unusual Expenses',
                    'TotalAllowances'=>'Total Allowances',
                    'totalAssets'=>'Total Assets',
                    'totalDebts'=>'Total Debt',
                    'netWorth'=>'Net Worth',
                    'effectiveIncome'=>'Effective Income',
                    'incomeSupplement'=>'Income Supplement',
                    'adjustedEffectiveIncome'=>'Adjusted Effective Income',
                    'revisedEffectiveIncome'=>'Revised Effective Income',
                    'discretionaryIncome'=>'Discretionary Income',
                    'estContributionAll'=>'Est. Parental Contrib. - All Students as Day',
                    'estContributionAllBrd'=>'Est. Parental Contrib. - All Students as Boarding',
                    'numberOfChildrenTuition'=>'Number of Children in tuition-charging schools',
                    'parentalContributionPer'=>'Parental Contrib. - Per Student - Day',
                    'studentAssets'=>'Student Assets',
                    'studentAssetContribution'=>'Student Asset Contribution',
                    'estFamilyContribution'=>'Est. Family Contrib. - This Student - Day',
                    'parentOfferToPay' => Label.GFR_Parent_Offer_to_Pay,
                    'grantAwarded' => Label.GFR_Grant_Awarded
            };
        }
        return rowNameToProperName;
    }
}