@isTest
private class BatchNotificationServiceTest {

    private static BatchNotificationService.JobWrapper createMockJob(String jobStatus, String extendedStatus, Integer totalJobItems, Integer numberOfErrors) {
        BatchNotificationService.JobWrapper newJob = new BatchNotificationService.JobWrapper(
                jobStatus, extendedStatus, totalJobItems, numberOfErrors);
        return newJob;
    }

    @isTest
    private static void notifyFailures_mockIdAndJobName_expectNoErrors() {
        Id mockId = AcademicYearTestData.Instance.DefaultAcademicYear.Id;

        Test.startTest();
        BatchNotificationService.Instance.notifyFailures(mockId, 'Test Apex Job');
        Test.stopTest();
    }

    @isTest
    private static void notifyFailures_nullIdAndJobName_expectNoErrors() {
        Test.startTest();
        BatchNotificationService.Instance.notifyFailures(null, null);
        Test.stopTest();
    }

    @isTest
    private static void sendApexJobSummaryEmail_mockFailedJob_expectNoErrors() {
        BatchNotificationService.JobWrapper job = createMockJob('Failed', 'Failed because the writer of this test said so.', 10, 3);

        Test.startTest();
        List<Messaging.SendEmailResult> results = BatchNotificationService.Instance.sendApexJobSummaryEmail(job, 'Test Apex Job', 'Null pointer exception.');
        Test.stopTest();

        System.assert(!results.isEmpty(), 'Expected to have email results.');
    }
}