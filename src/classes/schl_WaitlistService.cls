/**
 * @description 
 */
public without sharing class schl_WaitlistService {

    @AuraEnabled
    public static Response createWaitlist(Request createRequest) {
        // This request class could be used to pass captcha information to google reCaptcha API if necessary.
        Response createResponse = new Response();

        try {
            // It might be best to create the waitlist record
            SCHL_Waitlist__c newWaitlist = new SCHL_Waitlist__c();

            insert newWaitlist;

            createResponse.WaitlistId = newWaitlist.Id;
            createResponse.Waitlist = newWaitlist;

            return createResponse;
        } catch (Exception ex) {
            throw new AuraHandledException(ex.getStackTraceString());
//            throw new AuraHandledException('We are sorry, there was an issue adding you to the waitlist. Please try again.');
        }

        return createResponse;
    }

    public class Request {

        @AuraEnabled
        public String WaitlistId { get; set; }
    }

    public class Response {

        @AuraEnabled
        public String WaitlistId { get; set; }

        @AuraEnabled
        public SCHL_Waitlist__c Waitlist { get; set; }
    }
}