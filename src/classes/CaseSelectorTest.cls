@isTest
private class CaseSelectorTest {
    
    private without sharing class LocalTestData
    {
        public DateTime academicYearStartDate;
        public DateTime academicYearEndDate;
        Account school;

        public LocalTestData() {
            Academic_Year__c academicYear = AcademicYearTestData.Instance.createAcademicYearWithoutReset();
            insert academicYear;

            school = AccountTestData.Instance.asSchool().DefaultAccount;

            academicYearStartDate = academicYear.Start_Date__c;
            academicYearEndDate = academicYear.End_Date__c;

            insert new List<Case> {
                CaseTestData.Instance
                        .forRecordTypeId(RecordTypes.schoolPortalCaseTypeId)
                        .forAccountId(school.Id)
                        .forSubject('Test Case 1')
                        .createCaseWithoutReset(),
                CaseTestData.Instance
                        .forRecordTypeId(RecordTypes.schoolPortalCaseTypeId)
                        .forAccountId(school.Id)
                        .forSubject('Test Case 2')
                        .createCaseWithoutReset(),
                CaseTestData.Instance
                        .forRecordTypeId(RecordTypes.schoolPortalCaseTypeId)
                        .forAccountId(school.Id)
                        .forSubject('Test Case 3')
                        .createCaseWithoutReset()
            };
        }
    }

    @isTest 
    private static void selectWithLimitAndOffset_recordTypeIdsNull_expectArgumentNullException() {
        LocalTestData testData = new LocalTestData();

        try {
            Test.startTest();
            CaseSelector.Instance.selectWithCustomLimitOffsetOrder(null, new List<Id>{testData.school.Id}, 
                testData.academicYearStartDate, testData.academicYearEndDate, 'Test', 'Subject', 'ASC', 5, 0);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, CaseSelector.CASE_RECORD_TYPE_IDS);
        } finally {
            Test.stopTest();
        }
    }

    @isTest 
    private static void selectWithLimitAndOffset_academicYearStartDateNull_expectArgumentNullException() {
        LocalTestData testData = new LocalTestData();

        try {
            Test.startTest();
            List<Id> recordTypeIds = new List<Id> { RecordTypes.schoolPortalCaseTypeId };

            CaseSelector.Instance.selectWithCustomLimitOffsetOrder(recordTypeIds, new List<Id>{testData.school.Id},
                null, testData.academicYearEndDate, 'Test', 'Subject', 'ASC', 5, 0);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, CaseSelector.ACADEMIC_YEAR_START_DATE);
        } finally {
            Test.stopTest();
        }
    }

    @isTest 
    private static void selectWithLimitAndOffset_academicYearEndDateNull_expectArgumentNullException() {
        LocalTestData testData = new LocalTestData();

        try {
            Test.startTest();
            List<Id> recordTypeIds = new List<Id> { RecordTypes.schoolPortalCaseTypeId };

            CaseSelector.Instance.selectWithCustomLimitOffsetOrder(recordTypeIds, new List<Id>{testData.school.Id}, 
                testData.academicYearStartDate, null, 'Test', 'Subject', 'ASC', 5, 0);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, CaseSelector.ACADEMIC_YEAR_END_DATE);
        } finally {
            Test.stopTest();
        }
    }

    @isTest 
    private static void selectWithLimitAndOffset_caseRecordsExists_expectCasesReturned() {
        LocalTestData testData = new LocalTestData();

        List<Id> recordTypeIds = new List<Id> { RecordTypes.schoolPortalCaseTypeId };

        List<Case> casesReturned = CaseSelector.Instance.selectWithCustomLimitOffsetOrder(recordTypeIds, new List<Id>{testData.school.Id},
            testData.academicYearStartDate, testData.academicYearEndDate, 'Test', 'Subject', 'ASC', 5, 0);

        System.assertEquals(3, casesReturned.size());
    }

    @isTest
    private static void selectWithLimitAndOffset_caseRecordsExists_nullSchoolIds_expectCasesReturned() {
        LocalTestData testData = new LocalTestData();

        List<Id> recordTypeIds = new List<Id> { RecordTypes.schoolPortalCaseTypeId };

        List<Case> casesReturned = CaseSelector.Instance.selectWithCustomLimitOffsetOrder(recordTypeIds, null,
                testData.academicYearStartDate, testData.academicYearEndDate, 'Test', 'Subject', 'ASC', 5, 0);

        System.assertEquals(3, casesReturned.size());
    }

    @isTest 
    private static void selectWithoutLimitAndOffset_caseRecordsExists_expectCasesReturned() {
        LocalTestData testData = new LocalTestData();

        List<Id> recordTypeIds = new List<Id> { RecordTypes.schoolPortalCaseTypeId };

        List<Case> casesReturned = CaseSelector.Instance.selectWithoutLimitAndOffset(recordTypeIds, new List<Id>{testData.school.Id},
            testData.academicYearStartDate, testData.academicYearEndDate, 'Test', 'Subject', 'ASC');

        System.assertEquals(3, casesReturned.size());
    }
}