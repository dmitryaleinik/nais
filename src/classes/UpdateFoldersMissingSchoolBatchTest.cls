@isTest
private class UpdateFoldersMissingSchoolBatchTest {

    @isTest
    private static void execute_foldersMissingSchool_expectFoldersUpdated() {
        String academicYear = '2017-2018';

        Account school = AccountTestData.Instance.asSchool().insertAccount();

        // Insert a folder without specifying the school.
        Student_Folder__c folder = StudentFolderTestData.Instance.forSchoolId(null).forAcademicYearPicklist(academicYear).insertStudentFolder();

        // Insert a PFS Assignment for that folder with the school specified.
        School_PFS_Assignment__c pfsAssignment = SchoolPfsAssignmentTestData.Instance.forSchoolId(school.Id).forStudentFolderId(folder.Id).forAcademicYearPicklist(academicYear).insertSchoolPfsAssignment();

        folder = [SELECT Id, School__c FROM Student_Folder__c WHERE Id = :folder.Id LIMIT 1];
        System.assertEquals(null, folder.School__c, 'Expected no school to be specified on the folder.');

        Test.startTest();
        UpdateFoldersMissingSchoolBatch.startBatch(academicYear);
        Test.stopTest();

        folder = [SELECT Id, School__c FROM Student_Folder__c WHERE Id = :folder.Id LIMIT 1];
        pfsAssignment = [SELECT Id, School__c FROM School_PFS_Assignment__c WHERE Id = :pfsAssignment.Id LIMIT 1];
        System.assertEquals(school.Id, pfsAssignment.School__c,
                'Expected the correct school to be specified on the pfs assignment.');
        System.assertEquals(pfsAssignment.School__c, folder.School__c,
                'Expected the same school to be specified on the folder as the pfs assignment.');
    }

    @isTest
    private static void execute_foldersMissingSchool_specifyDifferentAcademicYear_expectFoldersNotUpdated() {
        String academicYear = '2017-2018';
        String academicYearToProcess = '2014-2015';

        Account school = AccountTestData.Instance.asSchool().insertAccount();

        // Insert a folder without specifying the school.
        Student_Folder__c folder = StudentFolderTestData.Instance.forSchoolId(null).forAcademicYearPicklist(academicYear).insertStudentFolder();

        // Insert a PFS Assignment for that folder with the school specified.
        School_PFS_Assignment__c pfsAssignment = SchoolPfsAssignmentTestData.Instance.forSchoolId(school.Id).forStudentFolderId(folder.Id).forAcademicYearPicklist(academicYear).insertSchoolPfsAssignment();

        folder = [SELECT Id, School__c FROM Student_Folder__c WHERE Id = :folder.Id LIMIT 1];
        System.assertEquals(null, folder.School__c, 'Expected no school to be specified on the folder.');

        Test.startTest();
        UpdateFoldersMissingSchoolBatch.startBatch(academicYearToProcess);
        Test.stopTest();

        folder = [SELECT Id, School__c FROM Student_Folder__c WHERE Id = :folder.Id LIMIT 1];
        pfsAssignment = [SELECT Id, School__c FROM School_PFS_Assignment__c WHERE Id = :pfsAssignment.Id LIMIT 1];
        System.assertEquals(school.Id, pfsAssignment.School__c,
                'Expected the correct school to be specified on the pfs assignment.');
        System.assertEquals(null, folder.School__c,
                'Expected the school to still not be specified on the folder.');
    }
}