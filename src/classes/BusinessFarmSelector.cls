/**
 * @description Query selector for Business Farm records.
 **/
public class BusinessFarmSelector extends fflib_SObjectSelector
{

    @testVisible private static final String PFS_IDS_PARAM = 'pfsIds';
    @testVisible private static final String BUSINESS_FARM_FIELDS_PARAM = 'businessFarmFields';
    @testVisible private static final String SBFA_FIELDS_PARAM = 'sbfaFields';
    private static final String SBFA_SOBJECT_NAME = 'School_Biz_Farm_Assignment__c';
    private static final String SBFA_RELATION = 'School_Biz_Farm_Assignments__r';

    /**
     * @description Select Business Farm records by Pfs record Id.
     * @param pfsIds The Pfs Ids to select Business Farm records by.
     * @return A list of Business Farm records.
     * @throws An ArgumentNullException if pfsIds is null.
     */
    public List<Business_Farm__c> selectByPfsId(Set<Id> pfsIds)
    {
        ArgumentNullException.throwIfNull(pfsIds, PFS_IDS_PARAM);

        assertIsAccessible();

        return Database.query(newQueryFactory().setCondition('PFS__c IN :pfsIds').toSOQL());
    }

    /**
     * @description Queries Business Farm records with associated School Business Farm Assignment
     *              records by Pfs Ids and with custom Field Lists.
     * @param pfsIds The Pfs Ids to select Business Farm records by.
     * @param businessFarmFields Fields of Business Farm records to query.
     * @param sbfaFields Fields of the School Business Farm Assignment records to query.
     * @return A list of Business Farm records.
     * @throws An ArgumentNullException if pfsIds is null.
     * @throws An ArgumentNullException if businessFarmFields is null.
     * @throws An ArgumentNullException if sbfaFields is null.
     */
    public List<Business_Farm__c> selectByPfsIdWithSBFAsWithCustomFieldLists(
        Set<Id> pfsIds, List<String> businessFarmFields, List<String> sbfaFields)
    {
        ArgumentNullException.throwIfNull(pfsIds, PFS_IDS_PARAM);
        ArgumentNullException.throwIfNull(businessFarmFields, BUSINESS_FARM_FIELDS_PARAM);
        ArgumentNullException.throwIfNull(sbfaFields, SBFA_FIELDS_PARAM);

        return Database.query(String.format('SELECT {0}, (SELECT {1} FROM {2}) FROM {3} WHERE PFS__c IN :pfsIds',
            new List<String> {
                buildFieldListString(getSObjectName(), businessFarmFields),
                buildFieldListString(SBFA_SOBJECT_NAME, sbfaFields),
                SBFA_RELATION,
                getSObjectName()
            }));
    }

    private Schema.SObjectType getSObjectType()
    {
        return Business_Farm__c.getSObjectType();
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                Business_Farm__c.PFS__c,
                Business_Farm__c.Sequence_Number__c
        };
    }

    private String buildFieldListString(String sObjectName, List<String> fields)
    {
        String fieldListString = '';
        for (String field : fields) {
            String fieldName = sObjectName + '.' + field;
            fieldListString += (String.isNotBlank(fieldListString)) ? ',' + fieldName : fieldName;
        }

        return fieldListString;
    }

    /**
     * @description Singleton property ensuring external sources do not
     *              instantiate this directly.
     */
    public static BusinessFarmSelector Instance {
        get {
            if (Instance == null) {
                Instance = new BusinessFarmSelector();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Creates a new instance of the BusinessFarmSelector.
     * @return An instance of BusinessFarmSelector.
     */
    public static BusinessFarmSelector newInstance()
    {
        return new BusinessFarmSelector();
    }

    /**
     * @description Private constructor to enforce singleton access
     *              via the Instance property.
     */
    private BusinessFarmSelector() {}
}