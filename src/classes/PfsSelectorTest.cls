@isTest
private class PfsSelectorTest {

    @isTest
    private static void selectByIdWithOpportunities_nullParam_expectArgumentNullException() {
        try {
            Test.startTest();
            PfsSelector.Instance.selectByIdWithOpportunities(null);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, PfsSelector.PFS_IDS_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void selectByIdWithOpportunities_noPfsExists_expectEmptyList() {
        PFS__c pfs = new PFS__c();
        insert pfs;

        Id pfsId = pfs.Id;
        delete pfs;

        Test.startTest();
        List<PFS__c> pfsRecords = PfsSelector.Instance.selectByIdWithOpportunities(new Set<Id> { pfsId });
        Test.stopTest();

        System.assert(pfsRecords.isEmpty(), 'Expected the returned list to be empty.');
    }

    @isTest
    private static void selectByIdWithOpportunities_pfsExists_expectRecordReturned() {
        PFS__c pfs = new PFS__c();
        insert pfs;

        Test.startTest();
        List<PFS__c> pfsRecords = PfsSelector.Instance.selectByIdWithOpportunities(new Set<Id> { pfs.Id });
        Test.stopTest();

        System.assert(!pfsRecords.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(1, pfsRecords.size(), 'Expected there to be one pfs record returned.');
        System.assertEquals(pfs.Id, pfsRecords[0].Id, 'Expected the Ids of the pfs records to match.');
    }

    @isTest
    private static void selectById_noPfsExists_expectEmptyList() {
        PFS__c pfs = new PFS__c();
        insert pfs;

        Id pfsId = pfs.Id;
        delete pfs;

        Test.startTest();
        List<PFS__c> pfsRecords = PfsSelector.Instance.selectById(new Set<Id> { pfsId });
        Test.stopTest();

        System.assert(pfsRecords.isEmpty(), 'Expected the returned list to be empty.');
    }

    @isTest
    private static void selectById_pfsExists_expectRecordReturned() {
        PFS__c pfs = new PFS__c();
        insert pfs;

        Test.startTest();
        List<PFS__c> pfsRecords = PfsSelector.Instance.selectById(new Set<Id> { pfs.Id });
        Test.stopTest();

        System.assert(!pfsRecords.isEmpty(), 'Expected the returned list to not be empty.');
    }

    @isTest
    private static void selectByContactIdAndAcademicYearId_noPfsExists_noContact_expectEmptyList() {
        Contact contact = new Contact(
            LastName = 'Contact #1',
            RecordTypeId = RecordTypes.parentContactTypeId
        );
        insert contact;

        Id contactId = contact.Id;
        delete contact;

        Academic_Year__c academicYear = new Academic_Year__c(
            Name = '2016-2017'
        );
        insert academicYear;

        Test.startTest();
        List<PFS__c> pfsRecords = PfsSelector.Instance.selectByContactIdAndAcademicYearId(contactId, academicYear.Id);
        Test.stopTest();

        System.assert(pfsRecords.isEmpty(), 'Expected the returned list to be empty.');
    }

    @isTest
    private static void selectByContactIdAndAcademicYearId_noPfsExists_noAcademicYear_expectNull() {
        Contact contact = new Contact(
            LastName = 'Contact #1',
            RecordTypeId = RecordTypes.parentContactTypeId
        );
        insert contact;

        Academic_Year__c academicYear = new Academic_Year__c(
            Name = '2016-2017'
        );
        insert academicYear;

        Id academicYearId = academicYear.Id;
        delete academicYear;

        Test.startTest();
        List<PFS__c> pfsRecords = PfsSelector.Instance.selectByContactIdAndAcademicYearId(contact.Id, academicYearId);
        Test.stopTest();

        System.assert(pfsRecords == null, 'Expected the returned null.');
    }

    @isTest
    private static void selectByContactIdAndAcademicYearId_pfsExists_expectRecordReturned() {
        Contact contact = new Contact(
            LastName = 'Contact #1',
            RecordTypeId = RecordTypes.parentContactTypeId
        );
        insert contact;

        Academic_Year__c academicYear = new Academic_Year__c(
            Name = '2016-2017'
        );
        insert academicYear;

        PFS__c pfs = new PFS__c(
            Parent_A__c = contact.Id,
            Academic_Year_Picklist__c = academicYear.Name
        );
        insert pfs;


        Test.startTest();
        List<PFS__c> pfsRecords = PfsSelector.Instance.selectByContactIdAndAcademicYearId(contact.Id, academicYear.Id);
        Test.stopTest();

        System.assert(!pfsRecords.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(1, pfsRecords.size(), 'Expected there to be one pfs record returned.');
        System.assertEquals(pfs.Id, pfsRecords[0].Id, 'Expected the Ids of the pfs records to match.');
    }

    @isTest
    private static void selectByContactIdSubmittedUnpaid_nullSet_expectArgumentNullException() {
        try {
            Test.startTest();
            PfsSelector.Instance.selectByContactIdSubmittedUnpaid(null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, PfsSelector.CONTACT_IDS_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void selectByContactIdSubmittedUnpaid_idProvided_expectRecord() {
        PFS__c expectedPfs = PfsTestData.Instance.asSubmitted().insertPfs();

        Test.startTest();
        List<PFS__c> pfsRecords = PfsSelector.Instance
                .selectByContactIdSubmittedUnpaid(new Set<Id> { expectedPfs.Parent_A__c });
        Test.stopTest();

        System.assertEquals(1, pfsRecords.size(), 'Expected there to be one Pfs record.');
        System.assertEquals(expectedPfs.Id, pfsRecords[0].Id, 'Expected the given record returned.');
    }

    @isTest
    private static void selectByIdForUpdate_pfsExists_expectRecordReturned() {
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        Test.startTest();
            List<PFS__c> pfsRecords = PfsSelector.Instance.selectByIdForUpdate(new Set<Id>{pfs.Id});
        Test.stopTest();

        System.assert(!pfsRecords.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(1, pfsRecords.size(), 'Expected there to be one pfs record returned.');
        System.assertEquals(pfs.Id, pfsRecords[0].Id, 'Expected the Ids of the pfs records to match.');
    }

    @isTest
    private static void selectByIdForUpdate_nullSet_expectArgumentNullException() {
        try {
            Test.startTest();
            PfsSelector.Instance.selectByIdForUpdate(null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, PfsSelector.PFS_IDS_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void selectWithCustomFieldListById_pfsExists_expectRecordReturned() {
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        Test.startTest();
            List<PFS__c> pfsRecords = PfsSelector.Instance.selectWithCustomFieldListById(new Set<Id>{pfs.Id}, new List<String>{'Name'});
        Test.stopTest();

        System.assert(!pfsRecords.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(1, pfsRecords.size(), 'Expected there to be one pfs record returned.');
        System.assertEquals(pfs.Id, pfsRecords[0].Id, 'Expected the Ids of the pfs records to match.');
    }

    @isTest
    private static void selectWithCustomFieldListById_nullSet_expectArgumentNullException() {
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        Test.startTest();
            try {
                PfsSelector.Instance.selectWithCustomFieldListById(new Set<Id>{pfs.Id}, null);

                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, PfsSelector.PFS_FIELDS_TO_SELECT_PARAM);
            }

            try {
                PfsSelector.Instance.selectWithCustomFieldListById(null, new List<String>{'Name'});

                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, PfsSelector.PFS_IDS_PARAM);
            }
        Test.stopTest();
    }

    @isTest
    private static void selectWithOpportunitiesByParentAAndAcademicYears_nullParams_expectArgumentNullException() {
        try {
            PfsSelector.Instance.selectWithOpportunitiesByParentAAndAcademicYears(null, null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, PfsSelector.CONTACT_IDS_PARAM);
        }

        try {
            PfsSelector.Instance.selectWithOpportunitiesByParentAAndAcademicYears(new Set<Id>(), null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, PfsSelector.ACADEMIC_YEARS_PARAM);
        }
    }

    @isTest
    private static void selectWithOpportunitiesByParentAAndAcademicYears_expectPfsAndOpp() {
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;
        Opportunity opportunity = OpportunityTestData.Instance.forPfs(pfs.Id).insertOpportunity();

        Set<String> academicYears = new Set<String> { pfs.Academic_Year_Picklist__c };
        Set<Id> contactIds = new Set<Id> { pfs.Parent_A__c };

        Test.startTest();
        List<PFS__c> pfsRecords =
                PfsSelector.Instance.selectWithOpportunitiesByParentAAndAcademicYears(contactIds, academicYears);
        Test.stopTest();

        System.assertNotEquals(null, pfsRecords, 'Expected the result list to not be null.');
        System.assertEquals(1, pfsRecords.size(), 'Expected there to be exactly one Pfs record.');
        System.assertEquals(pfs.Id, pfsRecords[0].Id, 'Expected the given Pfs record to be returned.');
        System.assertEquals(1, pfsRecords[0].Opportunities__r.size(), 'Expected one Opportunity to be returned.');
        System.assertEquals(opportunity.Id, pfsRecords[0].Opportunities__r[0].Id,
                'Expected the given Opportunity record to be returned.');
    }

    @isTest
    private static void selectAllWithCustomFieldList_pfsExists_expectRecordReturned() {
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        Test.startTest();
            List<PFS__c> pfsRecords = PfsSelector.Instance.selectAllWithCustomFieldList(new List<String>{'Name'});
        Test.stopTest();

        System.assert(!pfsRecords.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(1, pfsRecords.size(), 'Expected there to be one pfs record returned.');
        System.assertEquals(pfs.Id, pfsRecords[0].Id, 'Expected the Ids of the pfs records to match.');
    }

    @isTest
    private static void selectAllWithCustomFieldList_nullSet_expectArgumentNullException() {
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        Test.startTest();
            try {
                PfsSelector.Instance.selectAllWithCustomFieldList(null);

                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, PfsSelector.PFS_FIELDS_TO_SELECT_PARAM);
            }
        Test.stopTest();
    }
    
    

    @isTest
    private static void selectByPfsNumber_pfsExists_expectRecordReturned() {
        
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        Test.startTest();
        
            String pfsNumber = [SELECT Id, PFS_Number__c FROM PFS__c WHERE Id = :pfs.Id LIMIT 1].PFS_Number__c;
            
            List<PFS__c> pfsRecords = PfsSelector.Instance
                .selectByPfsNumber(new Set<String>{pfsNumber} , new Set<String>{'Id', 'PFS_Number__c'});
            
            System.AssertEquals(true, !pfsRecords.isEmpty());
            
            System.AssertEquals(pfsNumber, pfsRecords[0].PFS_Number__c);
        Test.stopTest();
    }
}