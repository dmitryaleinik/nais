@isTest
private class DocumentVerificationQueueTest {

    private static School_Document_Assignment__c queryDocumentAssignment(Id recordId) {
        return [SELECT Id, Verification_Request_Status__c, Verification_Request_Error__c FROM School_Document_Assignment__c WHERE Id = :recordId];
    }

    private static List<School_Document_Assignment__c> queryDocumentAssignments(Set<Id> recordIds) {
        return [SELECT Id, Verification_Request_Status__c, Verification_Request_Error__c FROM School_Document_Assignment__c WHERE Id IN :recordIds];
    }

    private static List<School_PFS_Assignment__c> insertPfsAssignmentsForSchools(List<Account> schools) {
        List<School_PFS_Assignment__c> pfsAssignments = new List<School_PFS_Assignment__c>();

        for (Account school : schools) {
            School_PFS_Assignment__c pfsAssignment = SchoolPfsAssignmentTestData.Instance
                    .forSchoolId(school.Id)
                    .forAcademicYearPicklist('2017-2018')
                    .createSchoolPfsAssignmentWithoutReset();
            pfsAssignments.add(pfsAssignment);
        }

        if (!pfsAssignments.isEmpty()) {
            DML.WithoutSharing.insertObjects(pfsAssignments);
        }

        return pfsAssignments;
    }

    private static List<School_Document_Assignment__c> insertDocAssignmentsForSchools(SchoolDocumentAssignmentTestData recordTestData, List<School_PFS_Assignment__c> pfsAssignments) {
        List<School_Document_Assignment__c> documentAssignments = new List<School_Document_Assignment__c>();

        for (School_PFS_Assignment__c pfsAssignment : pfsAssignments) {
            School_Document_Assignment__c newRecord =
                    recordTestData.forPfsAssignment(pfsAssignment.Id).createSchoolDocumentAssignmentWithoutReset();
            documentAssignments.add(newRecord);
        }

        if (!documentAssignments.isEmpty()) {
            DML.WithoutSharing.insertObjects(documentAssignments);
        }

        return documentAssignments;
    }

    private static void assertSdaRequestValues(List<School_Document_Assignment__c> documentAssignments, String expectedStatus, String expectedError) {
        for (School_Document_Assignment__c documentAssignment : documentAssignments) {
            System.assertEquals(expectedStatus, documentAssignment.Verification_Request_Status__c, 'Expected the correct request status.');
            System.assertEquals(expectedError, documentAssignment.Verification_Request_Error__c, 'Expected the correct request errors.');
        }
    }

    @isTest
    private static void execute_featureDisabled_expectFlaggedRecordsNotUpdated() {
        Account school = AccountTestData.Instance.forName('School1')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccount();

        School_PFS_Assignment__c pfsAssignment = SchoolPfsAssignmentTestData.Instance
                .forSchoolId(school.Id)
                .forAcademicYearPicklist('2017-2018')
                .insertSchoolPfsAssignment();

        Family_Document__c document = FamilyDocumentTestData.Instance
                .forDocumentType('W2')
                .withImportId('1234-1234-1234-1234-1234')
                .insertFamilyDocument();

        String requestStatus = SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE;
        School_Document_Assignment__c documentAssignment = SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(requestStatus)
                .forPfsAssignment(pfsAssignment.Id)
                .asType('W2')
                .forDocumentYear('2017')
                .insertSchoolDocumentAssignment();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(requestStatus, documentAssignment.Verification_Request_Status__c, 'Expected the request status to be set.');

        Test.startTest();
        DocumentVerificationQueue.queueVerificationRequests(new Set<Id> { documentAssignment.Id });
        Test.stopTest();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(requestStatus, documentAssignment.Verification_Request_Status__c, 'Expected the request status to be set.');
    }

    @isTest
    private static void execute_recordsFlaggedForVerification_expectRequestStatusComplete() {
        Account school = AccountTestData.Instance.forName('School1')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccount();

        School_PFS_Assignment__c pfsAssignment = SchoolPfsAssignmentTestData.Instance
                .forSchoolId(school.Id)
                .forAcademicYearPicklist('2017-2018')
                .insertSchoolPfsAssignment();

        Family_Document__c document = FamilyDocumentTestData.Instance
                .forDocumentType('W2')
                .withImportId('1234-1234-1234-1234-1234')
                .insertFamilyDocument();

        String requestStatus = SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE;
        School_Document_Assignment__c documentAssignment = SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(requestStatus)
                .forPfsAssignment(pfsAssignment.Id)
                .asType('W2')
                .forDocumentYear('2017')
                .insertSchoolDocumentAssignment();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(requestStatus, documentAssignment.Verification_Request_Status__c, 'Expected the request status to be correct.');

        Test.startTest();
        FeatureToggles.setToggle('Verify_Existing_Documents__c', true);
        DocumentVerificationQueue.queueVerificationRequests(new Set<Id> { documentAssignment.Id });
        Test.stopTest();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(SchoolDocumentAssignments.VERIFICATION_REQUEST_COMPLETE, documentAssignment.Verification_Request_Status__c, 'Expected the request status to be correct.');
    }

    @isTest
    private static void execute_docForMultipleSchools_recordsFlaggedForVerification_expectRequestStatusComplete() {
        Integer numberOfSchools = 3;
        List<Account> schools = AccountTestData.Instance.forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccounts(numberOfSchools);

        List<School_PFS_Assignment__c> pfsAssignments = insertPfsAssignmentsForSchools(schools);

        Family_Document__c document = FamilyDocumentTestData.Instance
                .forDocumentType('W2')
                .withImportId('1234-1234-1234-1234-1234')
                .insertFamilyDocument();

        String requestStatus = SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE;
        SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(requestStatus)
                .asType('W2')
                .forDocumentYear('2017');

        List<School_Document_Assignment__c> documentAssignments =
                insertDocAssignmentsForSchools(SchoolDocumentAssignmentTestData.Instance, pfsAssignments);
        Map<Id, School_Document_Assignment__c> documentAssignmentsByIds =
                new Map<Id, School_Document_Assignment__c>(documentAssignments);

        documentAssignments = queryDocumentAssignments(documentAssignmentsByIds.keySet());
        assertSdaRequestValues(documentAssignments, requestStatus, null);

        Test.startTest();
        FeatureToggles.setToggle('Verify_Existing_Documents__c', true);
        DocumentVerificationQueue.queueVerificationRequests(documentAssignmentsByIds.keySet());
        Test.stopTest();

        documentAssignments = queryDocumentAssignments(documentAssignmentsByIds.keySet());
        assertSdaRequestValues(documentAssignments, SchoolDocumentAssignments.VERIFICATION_REQUEST_COMPLETE, null);
    }

    @isTest
    private static void execute_recordsFlaggedForVerification_recordsDoNotHaveRequiredInfo_expectRequestStatusNull() {
        Account school = AccountTestData.Instance.forName('School1')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccount();

        School_PFS_Assignment__c pfsAssignment = SchoolPfsAssignmentTestData.Instance
                .forSchoolId(school.Id)
                .forAcademicYearPicklist('2017-2018')
                .insertSchoolPfsAssignment();

        // Insert a family document without an import Id.
        Family_Document__c document = FamilyDocumentTestData.Instance.forDocumentType('W2').insertFamilyDocument();

        String requestStatus = SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE;
        School_Document_Assignment__c documentAssignment = SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(requestStatus)
                .forPfsAssignment(pfsAssignment.Id)
                .forDocumentYear('2017')
                .insertSchoolDocumentAssignment();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(requestStatus, documentAssignment.Verification_Request_Status__c, 'Expected the request status to be correct.');

        Test.startTest();
        FeatureToggles.setToggle('Verify_Existing_Documents__c', true);
        DocumentVerificationQueue.queueVerificationRequests(new Set<Id> { documentAssignment.Id });
        Test.stopTest();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(SchoolDocumentAssignments.VERIFICATION_REQUEST_ERROR, documentAssignment.Verification_Request_Status__c,
                'Expected the request status to be null if the required information is missing.');
        System.assert(String.isNotBlank(documentAssignment.Verification_Request_Error__c),
                'Expected the errors to not be blank.');
    }
    
    @isTest
    private static void execute_recordsFlaggedForVerification_mockException_expectErrorStatus() {
        Account school = AccountTestData.Instance.forName('School1')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccount();

        School_PFS_Assignment__c pfsAssignment = SchoolPfsAssignmentTestData.Instance
                .forSchoolId(school.Id)
                .forAcademicYearPicklist('2017-2018')
                .insertSchoolPfsAssignment();

        Family_Document__c document = FamilyDocumentTestData.Instance
                .forDocumentType('W2')
                .withImportId('1234-1234-1234-1234-1234')
                .insertFamilyDocument();

        String requestStatus = SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE;
        School_Document_Assignment__c documentAssignment = SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(requestStatus)
                .forPfsAssignment(pfsAssignment.Id)
                .asType('W2')
                .forDocumentYear('2017')
                .insertSchoolDocumentAssignment();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(requestStatus, documentAssignment.Verification_Request_Status__c, 'Expected the request status to be correct.');

        // Create mock exception
        String expectedError = 'Failed to request verification from spring CM';
        SpringCMProd.springCMException mockException = new SpringCMProd.springCMException(expectedError);
        SpringCMAction.mockException = mockException;

        Test.startTest();
        FeatureToggles.setToggle('Verify_Existing_Documents__c', true);
        DocumentVerificationQueue.queueVerificationRequests(new Set<Id> { documentAssignment.Id });
        Test.stopTest();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(SchoolDocumentAssignments.VERIFICATION_REQUEST_ERROR, documentAssignment.Verification_Request_Status__c,
                'Expected the request status to be error when the callout fails.');
        System.assertEquals(expectedError, documentAssignment.Verification_Request_Error__c,
                'Expected the request error to have the exception message.');
    }

    @isTest
    private static void execute_oneDocForMultipleSchools_recordsFlaggedForVerification_mockException_expectErrorStatus() {
        Integer numberOfSchools = 3;
        List<Account> schools = AccountTestData.Instance.forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccounts(numberOfSchools);

        List<School_PFS_Assignment__c> pfsAssignments = insertPfsAssignmentsForSchools(schools);

        Family_Document__c document = FamilyDocumentTestData.Instance
                .forDocumentType('W2')
                .withImportId('1234-1234-1234-1234-1234')
                .insertFamilyDocument();

        String requestStatus = SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE;
        SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(requestStatus)
                .asType('W2')
                .forDocumentYear('2017');

        List<School_Document_Assignment__c> documentAssignments =
                insertDocAssignmentsForSchools(SchoolDocumentAssignmentTestData.Instance, pfsAssignments);
        Map<Id, School_Document_Assignment__c> documentAssignmentsByIds =
                new Map<Id, School_Document_Assignment__c>(documentAssignments);

        documentAssignments = queryDocumentAssignments(documentAssignmentsByIds.keySet());
        assertSdaRequestValues(documentAssignments, requestStatus, null);

        // Create mock exception
        String expectedError = 'Failed to request verification from spring CM';
        SpringCMProd.springCMException mockException = new SpringCMProd.springCMException(expectedError);
        SpringCMAction.mockException = mockException;

        Test.startTest();
        FeatureToggles.setToggle('Verify_Existing_Documents__c', true);
        DocumentVerificationQueue.queueVerificationRequests(documentAssignmentsByIds.keySet());
        Test.stopTest();

        documentAssignments = queryDocumentAssignments(documentAssignmentsByIds.keySet());
        assertSdaRequestValues(documentAssignments, SchoolDocumentAssignments.VERIFICATION_REQUEST_ERROR, expectedError);
    }
}