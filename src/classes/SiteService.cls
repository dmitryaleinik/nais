/**
 * @description This class is used to wrap the Site.cls methods and provide specific handling based on different types of users.
 */
public with sharing class SiteService {

    @testVisible private static final String REQUEST_PARAM = 'request';
    @testVisible private static final String USERNAME_PARAM = 'username';
    @testVisible private static final String PASSWORD_PARAM = 'password';
    @testVisible private static final String NEW_PASSWORD_PARAM = 'newPassword';
    @testVisible private static final String VERIFY_PASSWORD_PARAM = 'verifyPassword';
    @testVisible private static final String CURRENT_PORTAL_PARAM = 'currentPortal';

    @testVisible private static final String START_URL_PARAM = 'startURL';
    @testVisible private static final String FAMILY_DASHBOARD_PAGE = '/FamilyDashboard';
    @testVisible private static final String FAMILY_LANDING_PAGE = '/FamilyPortalLandingPage';

    @testVisible private static Boolean mockIsPasswordExpired;

    private SiteService() { }

    /**
     * @description Used to specify the portal that a password reset or login will be performed in.
     */
    public enum PortalType {
        FAMILY,
        SCHOOL
    }

    /**
     * @description This method is used to handle logging in. For all users, we check if the start url parameter is set. If so, the user will be directed to that page upon logging in instead of the standard landing page for the community being accessed. Family portal users will be directed to the FamilyPortalLanding page despite having the starturl param set if they do not have early access and today's date is within the Landing Page start and end dates for the family portal settings.
     * @param request The request containing login information.
     * @return The page a user should be directed to after logging in.
     * @throws ArgumentNullException if request is null.
     */
    public PageReference login(Request request) {
        ArgumentNullException.throwIfNull(request, REQUEST_PARAM);

        String postLoginUrl = getPostLoginUrl(request.Portal);

        return Site.login(request.Username, request.Password, postLoginUrl);
    }

    /**
     * @description Changes the current user's password by calling the Site.changePassword() method. If the user is a
     *              family portal user then we determine where the user should be directed to after changing their
     *              password based on early access and whether or not the family portal landing page should be
     *              displayed.
     *
     *              If a user's password has NOT expired we just return Site.changePassword() since this would indicate
     *              a user trying to change password without the use of the password reset email.
     * @param request The request containing new password information as well as information about the current portal.
     * @return The page that users should be directed to after attempting to change their password.
     */
    public PageReference changePassword(Request request) {
        ArgumentNullException.throwIfNull(request, REQUEST_PARAM);

        // Just change the password if the user's password is not expired which would indicate that they
        // have chosen to change their password without receiving a reset password email.
        if (!isPasswordExpired()) {
            return Site.changePassword(request.NewPassword, request.VerifyPassword, request.Password);
        }

        // School portal users don't require any special handling, just change password.
        if (request.Portal == SiteService.PortalType.SCHOOL) {
            return Site.changePassword(request.NewPassword, request.VerifyPassword, request.Password);
        }

        // Begin special behavior for family portal users.
        // Change password and check page reference. If null, return early since something probably went wrong.
        PageReference postLoginPage = Site.changePassword(request.NewPassword, request.VerifyPassword, request.Password);
        if (postLoginPage == null) {
            return postLoginPage;
        }

        String postLoginUrl = getPostLoginUrl(request.Portal);
        if (String.isNotBlank(postLoginUrl)) {
            postLoginPage = new PageReference(postLoginUrl);
            postLoginPage.setRedirect(true);
            return postLoginPage;
        }

        return postLoginPage;
    }

    /**
     * @description This method allows us to determine where the user should be redirected too after completing a login
     *              or change password operation. This method is test visible since we Site.cls methods return null
     *              during unit tests.
     * @return The url that the user should be directed to after logging in or changing passwords.
     */
    @testVisible
    private String getPostLoginUrl(SiteService.PortalType currentPortal) {
        String startUrl = System.currentPageReference().getParameters().get(START_URL_PARAM);

        // Return early for the school portal since there isn't special handling required.
        if (currentPortal == SiteService.PortalType.SCHOOL) {
            return startUrl;
        }

        // Begin special handling for family portal users.
        startUrl = (startUrl == null) ? FAMILY_DASHBOARD_PAGE : startUrl;

        // Checking the early access cookie and checking the user record for early access needs to be separate because we can't access cookies in triggers.
        Boolean userHasEarlyAccess = AccessService.Instance.isEarlyAccessCookieSet() || CurrentUser.hasEarlyAccess();
        Boolean isEarlyAccessOpen = AccessService.Instance.isEarlyAccessOpen();
        Boolean shouldShowLandingPage = AcademicYearService.Instance.showLandingPage();

        // Handle directing the user to the Family Portal Landing Page.
        // Only direct users to the landing page if before the overlap period and the user does not have early access.
        Boolean showFamilyPortalLandingPage = (!isEarlyAccessOpen || !userHasEarlyAccess) && shouldShowLandingPage;
        startUrl = (showFamilyPortalLandingPage) ? FAMILY_LANDING_PAGE : startUrl;

        return startUrl;
    }

    private static Boolean isPasswordExpired() {
        if (!Test.isRunningTest()) {
            return Site.isPasswordExpired();
        }

        return mockIsPasswordExpired == null ? Site.isPasswordExpired() : mockIsPasswordExpired;
    }

    /**
     * @description Creates a new instance of the SiteService.
     * @return An instance of SiteService.
     */
    public static SiteService newInstance() {
        return new SiteService();
    }

    /**
     * @description Use this property to access the SiteService singleton.
     */
    public static SiteService Instance {
        get {
            if (Instance == null) {
                Instance = newInstance();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Request class used to pass parameters to the SiteService.
     */
    public class Request {

        /**
         * @description Constructor for a request used to log in to the specified portal.
         * @param currentPortal The portal where the operation will done.
         * @param usernameParam The username of the person attempting to login.
         * @param passwordParam The password of the person attempting to login.
         * @throws ArgumentNullException if any param is null.
         */
        public Request(SiteService.PortalType currentPortal, String usernameParam, String passwordParam) {
            ArgumentNullException.throwIfNull(currentPortal, CURRENT_PORTAL_PARAM);
            ArgumentNullException.throwIfNull(usernameParam, USERNAME_PARAM);
            ArgumentNullException.throwIfNull(passwordParam, PASSWORD_PARAM);

            this.Username = usernameParam;
            this.Password = passwordParam;
            this.Portal = currentPortal;
        }

        /**
         * @description Constructs a request for changing a password in the specified portal.
         * @param currentPortal The portal where the operation will done.
         * @param newPasswordParam The new password a user would like to use.
         * @param verifyPasswordParam The verification value used to ensure the user correctly entered their new password.
         * @param oldPassword The user's current password that will be checked to make sure they can change the password.
         *                    This is an optional parameter.
         * @throws ArgumentNullException if currentPortal, newPasswordParam, or verifyPasswordParam are null.
         */
        public Request(SiteService.PortalType currentPortal, String newPasswordParam, String verifyPasswordParam, String oldPassword) {
            ArgumentNullException.throwIfNull(currentPortal, CURRENT_PORTAL_PARAM);
            ArgumentNullException.throwIfNull(newPasswordParam, NEW_PASSWORD_PARAM);
            ArgumentNullException.throwIfNull(verifyPasswordParam, VERIFY_PASSWORD_PARAM);

            this.Portal = currentPortal;
            this.NewPassword = newPasswordParam;
            this.VerifyPassword = verifyPasswordParam;
            this.Password = oldPassword;
        }

        /**
         * @description The username to perform a login with.
         */
        public String Username { get; private set; }

        /**
         * @description The current password to use for logging in and password changes.
         */
        public String Password { get; private set; }

        /**
         * @description The value to set as the new password.
         */
        public String NewPassword { get; private set; }

        /**
         * @description The verification value to use for comparing a new password value.
         */
        public String VerifyPassword { get; private set; }

        /**
         * @description Specifies which portal a requested operation is being performed in.
         */
        public SiteService.PortalType Portal { get; private set; }
    }
}