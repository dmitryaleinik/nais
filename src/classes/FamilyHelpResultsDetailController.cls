/**
 * FamilyHelpResultsDetailController.cls
 *
 * @description Controller class for FamilyHelpResultsDetail page
 *
 * @author Mike Havrilla @ Presence PG
 */

public class FamilyHelpResultsDetailController {
    public FamilyTemplateController controller { get; set; }
    public Knowledge__kav knowledgeArticle { get; set; }

    public FamilyHelpResultsDetailController( FamilyTemplateController controller) {
        this.controller = controller;

        if(this.controller.pfsRecord == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error: No PFS Record Found for current user.'));
            this.controller.pfsRecord = new PFS__c();
        }

        Id knowledgeArticleId = (Id) ApexPages.currentPage().getParameters().get('articleId');

        if( String.isNotEmpty(knowledgeArticleId)) {
            knowledgeArticle = new KnowledgeDataAccessService().getKnowledgeArticleByArticleId( knowledgeArticleId);
        }

    }

    public static FamilyTemplateController.config loadPFS(String pfsId, String academicYearId) {
        PFS__c pfsRecord;
        User currentUser = [Select Id, ContactId, ProfileId from User where Id = :UserInfo.getUserId()];
        Academic_Year__c academicYear = [select Id, Name from Academic_Year__c where Id = :academicYearId];
        
        if (GlobalVariables.isSysAdminUser(currentUser) || GlobalVariables.isCallCenterUser(currentUser)){
            pfsRecord = ApplicationUtils.queryPFSRecord(pfsId, null, null);
            // if this not a sys admin or call center user, get record based on contact id and academic year
        } else {
            pfsRecord = ApplicationUtils.queryPFSRecord(null, currentUser.ContactId, academicYearId);
        }
        
        return new FamilyTemplateController.config(pfsRecord, null);
    }//End:loadPFS

}