/**
 * MassEmailSendTriggerHandlerTest.cls
 *
 * @description: Test class for MassEmailSendTriggerHandler using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
private class MassEmailSendTriggerHandlerTest {

    /* test data setup */
    @testSetup static void setupTestData() {
        MassEmailSendTestDataFactory.createEmailTemplate();

        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {

            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
        }
    }

    /* negative test cases */

    // Attempt afterUpdate with non 'Ready to Send' status, no send
    @isTest static void afterUpdate_withNonReadyStatus_noSend() {

        // Arrange
        Mass_Email_Send__c massES = [select Status__c from Mass_Email_Send__c where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];

        // Act
        Test.startTest();
            massES.Status__c = 'Delivered';
            update massES;
        Test.stopTest();

        // Assert
        massES = [select Date_Sent__c from Mass_Email_Send__c where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
        System.assertEquals( null, massES.Date_Sent__c);
    }


    /* postive test cases */

    // NOTE: (as of 7/2016)
    // Due to a bug introduced in Spring '15, Test.start/stopTest() commits are not getting cleared when a future method with the callout annotation
    // is called, meaning any DML before future callout is treated as one transaction, resulting in "You have uncommitted work pending" exception
    // see links:
    // http://www.joe-ferraro.com/2014/04/apex-unit-testing-test-data-and-test-setmock/
    // https://patlatus.wordpress.com/2015/02/12/bugs-in-salesforce-spring-15-causing-system-calloutexception-you-have-uncommitted-work-pending-please-commit-or-rollback-before-calling-out/
    // https://success.salesforce.com/issues_view?id=a1p300000008XHBAA2
    // Because a Mass_Email_Send__c record must exist before we can make callout, there is no way to trigger this and
    // not receive an exception - exception is caught and gracefully handled, these calls are still necessary for test coverage.

    // Attempt afterUpdate with 'Ready to Send' status, send triggered
    @isTest static void afterUpdate_withReadyStatus_send() {

        // Arrange
        Mass_Email_Send__c massES = [select Status__c from Mass_Email_Send__c where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];

        // Act
        Test.startTest();

            // Create Academic Year, Applicant, PFS && sPFS Assignment
            MassEmailSendTestDataFactory.createRequiredPFSData();
            Test.setMock( HttpCalloutMock.class, new SendgridMockHttpResponseGenerator());
            massES.Status__c = 'Ready to Send';
            update massES;
        Test.stopTest();

        // No asserts for now, Mass_Email_Send__c record will be moved to "Error" status but if this bug
        // is fixed in the future then record will be correctly updated and assert on "Error" will cause test to fail
    }

    // Delete a record to get coverage in TriggerFactory delete contexts
    @isTest static void deleteContextMethods_coverage_invoke() {

        // Arrange
        Mass_Email_Send__c massES = [select Status__c from Mass_Email_Send__c where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL2_NAME];

        // Act
        Test.startTest();
            delete massES;
        Test.stopTest();

        // Assert
        List<Mass_Email_Send__c> massESList = [select Status__c from Mass_Email_Send__c where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL2_NAME];
        System.assert( massESList.isEmpty() == true);
    }

}