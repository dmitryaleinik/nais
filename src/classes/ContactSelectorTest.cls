@isTest
private class ContactSelectorTest
{
    
    @isTest
    private static void selectByRecordTypeId_contactExists_expectRecordReturned()
    {
        Contact contact = ContactTestData.Instance
            .forRecordTypeId(RecordTypes.schoolStaffContactTypeId).DefaultContact;

        Test.startTest();
            List<Contact> contactRecords = ContactSelector.Instance
                .selectByRecordTypeId(RecordTypes.schoolStaffContactTypeId);
        Test.stopTest();

        System.assert(!contactRecords.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(1, contactRecords.size(), 'Expected there to be one contact record returned.');
        System.assertEquals(contact.Id, contactRecords[0].Id, 'Expected the Ids of the contact records to match.');
    }

    @isTest
    private static void selectByRecordTypeId_nullSet_expectArgumentNullException()
    {
        Contact contact = ContactTestData.Instance
            .forRecordTypeId(RecordTypes.schoolStaffContactTypeId).DefaultContact;

        Test.startTest();
            try {
                ContactSelector.Instance.selectByRecordTypeId(null);

                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, ContactSelector.RECORDTYPE_ID);
            }
        Test.stopTest();
    }

    @isTest
    private static void selectBySchoolIdAndEmail_nullParameters()
    {
        Account school = AccountTestData.Instance.asSchool().DefaultAccount;
        Contact contact = ContactTestData.Instance.forAccount(school.Id).DefaultContact;

        Test.startTest();
            try 
            {
                ContactSelector.newInstance().selectBySchoolIdAndEmail(null, 'test');

                TestHelpers.expectedArgumentNullException();
            } 
            catch (Exception e) 
            {
                TestHelpers.assertArgumentNullException(e, ContactSelector.SCHOOL_ID);
            }

            try 
            {
                ContactSelector.newInstance().selectBySchoolIdAndEmail(school.Id, null);

                TestHelpers.expectedArgumentNullException();
            } 
            catch (Exception e) 
            {
                TestHelpers.assertArgumentNullException(e, ContactSelector.CONTACT_EMAIL);
            }
        Test.stopTest();
    }

    @isTest
    private static void selectBySchoolIdAndEmail_expectRecordsReturned()
    {
        String testEmail = 'newschooluseremail@test.com';
        Account school = AccountTestData.Instance.asSchool().DefaultAccount;
        Contact contact = ContactTestData.Instance
            .forEmail(testEmail)
            .forAccount(school.Id).DefaultContact;

        Test.startTest();
            List<Contact> contacts = ContactSelector.newInstance().selectBySchoolIdAndEmail(school.Id, testEmail);
        Test.stopTest();
        
        System.assertEquals(1, contacts.size());
        System.assertEquals(school.Id, contacts[0].AccountId);
        System.assertEquals(testEmail, contacts[0].Email);
    } 
}