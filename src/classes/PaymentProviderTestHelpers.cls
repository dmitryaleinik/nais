/**
 * @description Test Helpers for Payment Providers.
 **/
@isTest
public class PaymentProviderTestHelpers {
    @testVisible private static final String INVALID_CREDIT_CARD_NUMBER = 'Invalid Credit Card number.';
    @testVisible private static final Integer ERROR_CODE = 1;

    public PaymentResult Result { get; set; }

    /**
     * @description Accept in a PaymentInformation object and return a PaymentResult
     *              response with a successful or failed outcome.
     */
    public PaymentProviderTestHelpers(PaymentRequest request) {
        ArgumentNullException.throwIfNull(request, 'request');
        ArgumentNullException.throwIfNull(request.PaymentInformation, 'paymentInformation');

        if (request.PaymentInformation instanceof CreditCardInformation) {
            validateCreditCard((CreditCardInformation)request.PaymentInformation);
        } else if (request.PaymentInformation instanceof ECheckInformation) {
            validateCheck((ECheckInformation)request.PaymentInformation);
        }

        Result.TransactionResultCode = (Result.isSuccess) ? 0 : ERROR_CODE;

        Result.Request = request;
    }

    /**
     * @description Accept in a RefundRequest object and return a successful
     *              PaymentResult.
     */
    public PaymentProviderTestHelpers(RefundRequest request) {
        ArgumentNullException.throwIfNull(request, 'request');

        Result = new PaymentResult();
        Result.Request = request;
        Result.isSuccess = true;
    }

    private void validateCreditCard(CreditCardInformation ccInfo) {
        Result = new PaymentResult();
        validateBasePaymentInformation(ccInfo);

        // For unit test purposes we are only testing against visa and mastercard at this time.
        Boolean validVisa = (ccInfo.CardType == 'Visa') && (ccInfo.CardNumber == '4111111111111111' || ccInfo.CardNumber == '4242424242424242' || ccInfo.CardNumber == '4012888888881881' || ccInfo.CardNumber == '4457010000000009');
        Boolean validMC = (ccInfo.CardType == 'MasterCard' || ccInfo.CardType == 'MC Eurocard') && (ccInfo.CardNumber == '5555555555554444' || ccInfo.CardNumber == '5200828282828210' || ccInfo.CardNumber == '5105105105105100');

        // AVS Checking that determines if the card postal code is valid or not
        Boolean validPostalCode = ccInfo.BillingPostalCode != '99999';

        Result.isSuccess &= validPostalCode;
        Result.isSuccess &= validVisa || validMC;
        Result.isSuccess &= ccInfo.NameOnCard != null;
        Result.isSuccess &= ccInfo.Cvv2 != null && (ccInfo.Cvv2.length() != 3 || ccInfo.Cvv2.length() != 4);

        Result.ErrorOccurred = !Result.isSuccess;

        if (!Result.isSuccess) {
            if (!validVisa && !validMC) {
                Result.ErrorMessage = INVALID_CREDIT_CARD_NUMBER;
                Result.ErrorData = INVALID_CREDIT_CARD_NUMBER;
                Result.AdditionalInfo = INVALID_CREDIT_CARD_NUMBER;
            } else if (!validPostalCode) {
                Result.ErrorMessage = Label.ZipAndAddressDoNotMatch;
                Result.ErrorData = Label.ZipAndAddressDoNotMatch;
                Result.AdditionalInfo = Label.ZipAndAddressDoNotMatch;
            } else {
                Result.ErrorMessage = PaymentProcessor.UNEXPECTED_ERROR;
                Result.ErrorData = PaymentProcessor.UNEXPECTED_ERROR;
                Result.AdditionalInfo = PaymentProcessor.UNEXPECTED_ERROR;
            }
        }
    }

    private void validateCheck(ECheckInformation ecInfo) {
        Result = new PaymentResult();
        validateBasePaymentInformation(ecInfo);

        Result.isSuccess &= ecInfo.AccountNumber != null;
        Result.isSuccess &= ecInfo.AccountNumber != '0987654321';
        Result.isSuccess &= ecInfo.AccountType != null;
        Result.isSuccess &= ecInfo.BankName != null;
        Result.isSuccess &= ecInfo.RoutingNumber != null;

        if (!Result.isSuccess) {
            Result.ErrorMessage = PaymentProcessor.UNEXPECTED_ERROR;
            Result.ErrorData = PaymentProcessor.UNEXPECTED_ERROR;
            Result.AdditionalInfo = PaymentProcessor.UNEXPECTED_ERROR;
        }
    }

    private void validateBasePaymentInformation(PaymentInformation paymentInformation) {
        Result.isSuccess = true;
        Result.isSuccess &= paymentInformation.BillingEmail != null;
        Result.isSuccess &= paymentInformation.BillingStreet1 != null;
        Result.isSuccess &= paymentInformation.BillingCity != null;
        Result.isSuccess &= paymentInformation.BillingState != null;
        Result.isSuccess &= paymentInformation.BillingCountryCode != null;
        Result.isSuccess &= paymentInformation.BillingFirstName != null;
        Result.isSuccess &= paymentInformation.BillingLastName != null;
        Result.isSuccess &= paymentInformation.BillingPostalCode != null;
    }
}