/**
 * @description Query selector for Applicant__c records.
 */
public class ApplicantSelector extends fflib_SObjectSelector {

    @testVisible private static final String PFS_ID  = 'pfsIds';
    @testVisible private static final String STUDENT_ID  = 'studentIds';

    /**
     * @description Selects Applicant__c records by PFS__c and Student__c ids.
     * @param pfsIds The Ids of the Pfs to query for Applicant__c records.
     * @param studentIds The Ids of the Student to query for Applicant__c records.
     * @return A list of Applicant__c records.
     * @throws An ArgumentNullException if pfsIds is null.
     * @throws An ArgumentNullException if studentIds is null.
     */
    public List<Applicant__c> selectByPFSandStudent(Set<Id> pfsIds, Set<Id> studentIds) {
        ArgumentNullException.throwIfNull(pfsIds, PFS_ID);
        ArgumentNullException.throwIfNull(studentIds, STUDENT_ID);

        assertIsAccessible();

        String query = newQueryFactory(true)
                .setCondition('Pfs__c IN :pfsIds AND Contact__c IN :studentIds')
                .toSOQL();

        return Database.query(query);
    }

    /**
     * @description Selects All Applicant records with only Id field.
     * @return A list of Applicant records.
     */
    public List<Applicant__c> selectAll()
    {
        assertIsAccessible();

        return Database.query(newQueryFactory().toSOQL());
    }

    private Schema.SObjectType getSObjectType() {
        return Applicant__c.getSObjectType();
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                Applicant__c.Name,
                Applicant__c.Id,
                Applicant__c.Pfs__c,
                Applicant__c.Contact__c
        };
    }

    /**
     * @description Creates a new instance of the ApplicantSelector.
     * @return An instance of ApplicantSelector.
     */
    public static ApplicantSelector newInstance() {
        return new ApplicantSelector();
    }
    
    /**
     * @description Singleton property ensuring external sources do not
     *              instantiate this directly.
     */
    public static ApplicantSelector Instance {
        get {
            if (Instance == null) {
                Instance = new ApplicantSelector();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to enforce singleton access
     *              via the Instance property.
     */
    private ApplicantSelector() {}
}