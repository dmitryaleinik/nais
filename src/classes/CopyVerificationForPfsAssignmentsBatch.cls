/**
 * @description This class is simply used to distinguish between batches created to process SPAs from annual
 *              setting records and batches created for flagged SPA records. The other batch class,
 *              AutoSPAValueAssignBatch has the ability to process records from Annual Setting records. This class lets
 *              us identify the batches that were created to process PFS Assignments with the Verification Processing
 *              Status picklist set to Processing Required. We need the ability to distinguish so we can avoid
 *              scheduling another CopyVerificationForPfsAssignments batch when there is already one in the queue.
 */
public class CopyVerificationForPfsAssignmentsBatch extends AutoSPAValueAssignBatch implements Schedulable {

    public CopyVerificationForPfsAssignmentsBatch() {
        super();
    }

    public void execute(SchedulableContext sc) {
        Database.executeBatch(new CopyVerificationForPfsAssignmentsBatch(), 75);
    }
}