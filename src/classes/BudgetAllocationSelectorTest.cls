@isTest
private class BudgetAllocationSelectorTest {
    
    @isTest
    private static void selectAll_TwoBudgetAllocations_AllBudgetAllocations()
    {
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
        insert currentAcademicYear;
        
        Account school = AccountTestData.Instance.asSchool().DefaultAccount;
        
        Budget_Group__c budgetGroup1 = BudgetGroupTestData.Instance
            .forAcademicYearId(currentAcademicYear.Id)
            .forSchoolId(school.Id)
            .forTotalFunds(50000).create();
            
        Budget_Group__c budgetGroup2 = BudgetGroupTestData.Instance
            .forAcademicYearId(currentAcademicYear.Id)
            .forSchoolId(school.Id)
            .forTotalFunds(50000).create();
            
        insert new List<Budget_Group__c>{budgetGroup1, budgetGroup2};
        
        Contact student = ContactTestData.Instance.asStudent().create();
        insert student;
        
        Student_Folder__c folder = StudentFolderTestData.Instance.DefaultStudentFolder;
        
        Budget_Allocation__c allocation1 = BudgetAllocationTestData.Instance
            .forBudgetGroupId(budgetGroup1.Id)
            .forAmountAllocated(7000)
            .forStudentFolder(folder.Id).create();
        Budget_Allocation__c allocation2 = BudgetAllocationTestData.Instance
            .forBudgetGroupId(budgetGroup2.Id)
            .forAmountAllocated(3000)
            .forStudentFolder(folder.Id).create();
        insert new List<Budget_Allocation__c>{allocation1, allocation2};
     
	    Test.startTest();
	        System.assertEquals(2, (BudgetAllocationSelector.newInstance().selectById(new Set<Id>{allocation1.Id, allocation2.Id})).size());
	    Test.stopTest();
    }
}