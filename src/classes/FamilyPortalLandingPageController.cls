public with sharing class FamilyPortalLandingPageController {
    public string currentAcademicYear{ get; set; }
    public string nextAcademicYear{ get; set; }

    public FamilyPortalLandingPageController() {
        this.currentAcademicYear = GlobalVariables.getCurrentYearString();
        this.nextAcademicYear = String.ValueOf(Integer.ValueOf(this.currentAcademicYear.left(4))+1)
                                    +'-'+String.ValueOf(Integer.ValueOf(this.currentAcademicYear.right(4))+1);
    }

    /**
     * @description Gets the message that will be displayed to family portal users who end up on the landing page. The
     *              message is formatted using the FAM_Landing_Page_Message custom label with the current academic
     *              year, next academic year, and overlap period start date.
     * @return The formatted landing page message.
     */
    public String getMessage() {
        String overlapPeriodStart = AcademicYearService.Instance.getOverlapStartDate();

        return String.format(Label.FAM_Landing_Page_Message,
                new List<String> {
                    this.nextAcademicYear,
                    overlapPeriodStart,
                    this.currentAcademicYear });
    }

    public pageReference goToDashboard() {
        PageReference dashboard = Page.FamilyDashboard;
        dashboard.setRedirect(true);
        return dashboard;

    }
}