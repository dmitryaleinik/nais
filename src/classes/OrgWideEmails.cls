/**
 * @description This class is used to interact with the Org_Wide_Emails__c hierarchical setting.
 */
 public class OrgWideEmails {
     
     private Org_Wide_Emails__c record;
     
     private OrgWideEmails() {
         
         record = Org_Wide_Emails__c.getInstance();
         
         if (record == null || Test.isRunningTest()) {
            record = getDefault();
        }
     }
     
     private Org_Wide_Emails__c getDefault() {
         return (Org_Wide_Emails__c)Org_Wide_Emails__c.SObjectType.newSObject(null, true);
     }
    
    public String Do_Not_Reply_Address {
        get {
            
            if (Do_Not_Reply_Address == null) {
                Do_Not_Reply_Address = record.Do_Not_Reply_Address__c;
            }
            
            return Do_Not_Reply_Address;
        }
        set;
    }
    
    public String SSS_Main_Address {
        get {
            
            if (SSS_Main_Address == null) {
                SSS_Main_Address = record.SSS_Main_Address__c;
            }
            
            return SSS_Main_Address;
        }
        set;
    }
    
    /**
     * @description Singleton instance of the OrgWideEmails.
     */
    public static OrgWideEmails Instance {
        get {
            if (Instance == null) {
                Instance = new OrgWideEmails();
            }
            return Instance;
        }
        private set;
    }
}