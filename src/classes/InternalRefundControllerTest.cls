@isTest
private class InternalRefundControllerTest {
    private static void createAppFeeSetting(String appFeeSettingName) {
        Integer appFeeSettingAmount = 50;
        Integer appFeeSettingDiscountedAmount = 40;
        String appFeeSettingProductCode = 'Test Product Code';
        String appFeeSettingDiscountedProductCode = 'KS Test Product Code';
        Application_Fee_Settings__c appFeeSettings = ApplicationFeeSettingsTestData.Instance
                .forName(appFeeSettingName)
                .forAmount(appFeeSettingAmount)
                .forDiscountedAmount(appFeeSettingDiscountedAmount)
                .forProductCode(appFeeSettingProductCode)
                .forDiscountedProductCode(appFeeSettingDiscountedProductCode).DefaultApplicationFeeSettings;
    }

    private static List<Transaction_Line_Item__c> getTransactionLineItems(Id opportunityId) {
        return [SELECT Id, Name, Opportunity__c, Transaction_ID__c, Transaction_Date__c, Amount__c, CC_Last_4_Digits__c,
                VaultGuid__c, Tender_type__c, Account_Label__c, RecordTypeId, Transaction_Type__c, Transaction_Status__c
        FROM Transaction_Line_Item__c
        WHERE Opportunity__c = :opportunityId];
    }

    private static void createTestData(Boolean createOppAndTLIToo) {
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        createAppFeeSetting(currentAcademicYear.Name);
        Application_Fee_Waiver__c afw1 = ApplicationFeeWaiverTestData.Instance.DefaultApplicationFeeWaiver;

        // create the default test data
        String parentACountry = 'United States';
        String parentAAdress = 'Line 1 Address\nLine 2 Address';
        PFS__c pfs = PfsTestData.Instance
            .asSubmitted()
            .forParentAAddress(parentAAdress)
            .forParentACountry(parentACountry)
            .forAcademicYearPicklist(currentAcademicYear.Name).DefaultPfs;

        if (createOppAndTLIToo){
            FinanceAction.createOppAndTransaction(new Set<Id>{pfs.Id});
        }
    }

    @isTest
    private static void testInternalRefundController() {
        createTestData(true);
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        Test.startTest();
            //System.debug(LoggingLevel.ERROR, 'TESTING limits' + Limits.getLimitQueries());
            // get the opportunity
            Opportunity theOpportunity = [SELECT Id FROM Opportunity WHERE PFS__c = :pfs.Id];
            System.assertNotEquals(null, theOpportunity);

            // set up payment TLI
            PaymentResult result = new PaymentResult();
            result.transactionNumber = '1928308120938';
            result.transactionResultCode = 0;
            result.VaultGUID = 'GUID-GUID-GUID-GUID-GUID';
            Id paymentTliId = PaymentUtils.insertCreditCardTransactionLineItem(
                theOpportunity.Id, 50.00, FamilyPaymentData.CC_TYPE_VISA, '1234', result);

            List<Transaction_Line_Item__c> tlis = getTransactionLineItems(theOpportunity.Id);
            // There should now be 2 TLIs (Sale, Payment)
            System.assertEquals(2, tlis.size());
            //System.debug(LoggingLevel.warn, 'Tlis:' + tlis);

            // move to refund page
            Test.setCurrentPage(new PageReference(Page.InternalRefund.getUrl() + '?id=' + theOpportunity.id));
            InternalRefundController refundController = new InternalRefundController();
            refundController.init();
            System.assertNotEquals(null, refundController.opp);
            System.assertNotEquals(null, refundController.tliList);
            //System.debug(LoggingLevel.Warn, refundController.opp);
            //System.debug(LoggingLevel.warn, 'TLI List: ' + tlis);
            System.assertEquals(true, refundController.showTLI);
            System.assertEquals(1, refundController.tliList.size(), 'Expected only One Transaction Line Item.');

            Id targetTliId = refundController.tliList[0].Id;

            // set the TLI ID and select it in the controller
            ApexPages.currentPage().getParameters().put('selectedTLI', targetTliId);

            refundController.showTLISection();
            System.assertEquals(targetTliId, refundController.tlItem.id);

            refundController.selectedTliId = targetTliId;

            // This is expected - there is no 'test' for refunds of non-existing transactions [C&P support - jB]
            PageReference refundPrResult = refundController.processRefund();

            tlis = getTransactionLineItems(theOpportunity.Id);

            // a refund TLI should have been created
            System.assertEquals(3, tlis.size());

            // a refund TLI should have been created
            System.assertNotEquals(null, [select Id, Name, Opportunity__c, Transaction_ID__c, Transaction_Date__c, Amount__c,
                                          CC_Last_4_Digits__c, VaultGuid__c, Tender_type__c, Account_Label__c, RecordTypeId, Transaction_Type__c, Transaction_Status__c
                                          from Transaction_Line_Item__c
                                          where Opportunity__c = :theOpportunity.Id AND RecordTypeId = :RecordTypes.refundTransactionTypeId]);

            PageReference cancelPr = refundController.cancel();
            System.assertNotEquals(null, cancelPr);
        Test.stopTest();
    }

    /**
     * @description Test various bad data initalizations
     */
    @isTest
    private static void testInternalRefundControllerNoOpp() {
        createTestData(true);
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        Test.startTest();
            Test.setCurrentPage(Page.InternalRefund);
            InternalRefundController refundController = new InternalRefundController();
            refundController.init();
            System.assertEquals(true, refundController.hasError);

            Test.setCurrentPage(new PageReference(Page.InternalRefund.getUrl() + '?id=garbage'));
            refundController = new InternalRefundController();
            refundController.init();
            System.assertEquals(true, refundController.hasError);

            // this opp won't have TLIs that are payments yet - just sale
            Opportunity theOpportunity = [SELECT Id FROM Opportunity WHERE PFS__c = :pfs.Id];

            Test.setCurrentPage(new PageReference(Page.InternalRefund.getUrl() + '?id=' + theOpportunity.id));
            refundController = new InternalRefundController();
            refundController.init();
            System.assertEquals(true, refundController.hasError);

            // no TLI found
            PageReference selectPageReference = refundController.showTLISection();
            System.assertEquals(null, selectPageReference);

            PageReference processPageReference = refundController.processRefund();
            System.assertEquals(null, processPageReference);
        Test.stopTest();
    }

    @isTest
    private static void constructor_eCheckPaymentPresent_expectNoErrors() {
        Opportunity opportunity = OpportunityTestData.Instance.insertAsPaid();
        InternalRefundController controller;

        Id paymentTransactionRecordTypeId =
            Schema.SObjectType.Transaction_Line_Item__c.getRecordTypeInfosByName().get('Payment').getRecordTypeId();
        Id expectedTliId = [
            SELECT Id
            FROM Transaction_Line_Item__c
            WHERE RecordTypeId = :paymentTransactionRecordTypeId][0].Id;

        Test.startTest();
            Test.setCurrentPage(new PageReference(Page.InternalRefund.getUrl() + '?id=' + opportunity.Id));
            controller = new InternalRefundController();
            controller.init();
        Test.stopTest();

        System.assert(!controller.hasError, 'Expected no error.');
        System.assertEquals(expectedTliId, controller.tlItem.Id, 'Expected the refundable Transaction Line Item.');
    }
}