@isTest
private class DocumentUploadSettingsTest {
    
    @isTest
    private static void isEnabled_EnforcePageLimit_expectTrue() {
        DocumentUploadSettings.setSetting('Enforce_Page_Limit__c', true);

        System.assertEquals(true, DocumentUploadSettings.isEnabled('Enforce_Page_Limit__c'),
                'Expected the setting to be enabled.');
    }
    
    @isTest
    private static void isEnabled_EnforcePageLimit_expectFalse() {
        DocumentUploadSettings.setSetting('Enforce_Page_Limit__c', true);

        System.assertEquals(true, DocumentUploadSettings.isEnabled('Enforce_Page_Limit__c'),
                'Expected the setting to be false.');
    }

    @isTest
    private static void getNumber_PageLimit_expect900() {
        DocumentUploadSettings.setSetting('Page_Limit__c', 900);

        System.assertEquals(Integer.ValueOf(900), DocumentUploadSettings.getValue('Page_Limit__c'),
                'Expected the setting to be 900.');
    }
}