/**
 * @description This action is meant to run at the end of the ravenna family integration to update contact records with
 *              ravenna Ids and ethnicity information.
 *              This must run after IntegrationFinalize.
 */
public class RavennaUpdateContacts implements IntegrationApiModel.IAction {

    public void invoke(IntegrationApiModel.ApiState apiState) {
        RavennaIntegrationUtils.syncContacts(apiState);
    }
}