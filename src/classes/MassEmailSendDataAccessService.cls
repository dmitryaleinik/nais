/**
 * MassEmailSendDataAccessService.cls
 *
 * @description Data access service class for Mass_Email_Send__c
 *
 * @author Chase Logan @ Presence PG
 */
public class MassEmailSendDataAccessService extends DataAccessService {
    
    private final String MASS_EMAIL_READY_STATUS = 'Ready to Send';

    public static final String FILTER_DELIVERED = 'Delivered';
    public static final String FILTER_ERROR = 'Error';
    public static final String FILTER_DRAFT = 'Draft';

    /**
     * @description: Queries a Mass_Email_Send__c record by ID, queries all fields by default because of the direct query of
     * a singe record by ID limited to 1
     *
     * @param massEmailId - The ID of the Mass_Email_Send__c record
     *
     * @return A Mass_Email_Send__c record
     */
    public Mass_Email_Send__c getMassEmailSendById( Id massEmailId) {
        Mass_Email_Send__c returnVal;

        if ( massEmailId != null) {

            try {

                returnVal = Database.query( 'select ' + String.join( super.getSObjectFieldNames( 'Mass_Email_Send__c'), ',') + ',' +
                                                  ' Sent_By__r.Name' +
                                             ' from Mass_Email_Send__c' + 
                                            ' where Id = :massEmailId' + 
                                            ' limit 1');
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in MassEmailSendDataAccessService.getMassEmailSendById' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }

        return returnVal;
    }

    /**
     * @description: Queries Mass_Email_Send__c records by ID for Recipients field
     *
     * @param massEmailIdSet - The Mass_Email_Send__c ID set
     *
     * @return A List of Mass_Email_Send__c records
     */
    public List<Mass_Email_Send__c> getMassEmailSendByIdSet( Set<String> massEmailIdSet) {
        
        return this.getMassEmailSendByIdSet( massEmailIdSet, false);
    }

    /**
     * @description: Queries Mass_Email_Send__c records by ID for Recipients field
     *
     * @param massEmailIdSet - The Mass_Email_Send__c ID set
     * @param lockRecords - Flag to set whether or not records should be locked for update
     *
     * @return A List of Mass_Email_Send__c records
     */
    public List<Mass_Email_Send__c> getMassEmailSendByIdSet( Set<String> massEmailIdSet, Boolean lockRecords) {
        List<Mass_Email_Send__c> returnList;

        if ( massEmailIdSet != null) {

            try {

                returnList = Database.query( 'select Id, Name, Recipients__c, School__c, Date_Sent__c, Sent_By__r.Name,' +
                                            ' Opened__c, Bounced__c, Unsubscribed__c, Send_Email_For_Each_Applicant__c, Is_Award_Letter__c, Send_Copy_To_School__c' +
                                            ' from Mass_Email_Send__c' + 
                                            ' where Id = :massEmailIdSet' + 
                                            ( lockRecords != null && lockRecords == true ? ' for update' : ''));
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in MassEmailSendDataAccessService.getMassEmailSendByIdSet' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }

        return returnList;
    }

    /**
     * @description: Queries Mass_Email_Send__c records by School ID
     *
     * @param schoolId - The School ID of the Mass_Email_Send__c record(s)
     *
     * @return A List of Mass_Email_Send__c records
     */
    public List<Mass_Email_Send__c> getMassEmailSendBySchoolId( Id schoolId) {

        return getMassEmailSendBySchoolId( schoolId, null);
    }

    /**
     * @description: Queries Mass_Email_Send__c records by School ID
     *
     * @param schoolId - The School ID of the Mass_Email_Send__c record(s)
     *
     * @param filterBy - (optional) Filter by Status
     *
     * @return A List of Mass_Email_Send__c records
     */
    public List<Mass_Email_Send__c> getMassEmailSendBySchoolId( Id schoolId, String filterBy) {
        List<Mass_Email_Send__c> returnList;

        if ( schoolId != null) {

            try {

                returnList = Database.query( 'select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c, Date_Sent__c,' +
                                            ' School_PFS_Assignments__c, Subject__c, Text_Body__c, Html_Body__c, Footer__c, Reply_To_Address__c,' +
                                            ' Failed_to_Send__c, Send_Email_For_Each_Applicant__c, Is_Award_Letter__c, Send_Copy_To_School__c' +
                                            ' from Mass_Email_Send__c' + 
                                            ' where School__c = :schoolId' + 
                                            ( String.isNotEmpty( filterBy) && 
                                             ( filterBy == MassEmailSendDataAccessService.FILTER_DELIVERED ||
                                              filterBy == MassEmailSendDataAccessService.FILTER_DRAFT ||
                                              filterBy == MassEmailSendDataAccessService.FILTER_ERROR) ? 
                                            ' and Status__c = :filterBy' : '') + 
                                            ' order by CreatedDate desc' +
                                            ' limit 100');
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in MassEmailSendDataAccessService.getMassEmailSendBySchoolId' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }

        return returnList;
    }

    /**
     * @description: Queries the Id of the Mass Email Send VF template by name
     *
     * @param templateName - The name of the Mass Email Send VF template
     *
     * @return the Id of the Mass Email Send VF Template
     */
    public Id getMassEmailSendTemplateId( String templateName) {
        Id returnVal;

        if ( !String.isEmpty( templateName)) {

            try {

                returnVal = [select Id
                               from EmailTemplate
                              where DeveloperName = :templateName
                              limit 1].Id;
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in MassEmailSendDataAccessService.getMassEmailSendTemplateId' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }

        return returnVal;
    }

    /**
     * @description: Attempts trigger a resend of a Mass Email Send record by setting record back to MASS_EMAIL_READY_STATUS
     *
     * @param massEmailId - The ID of the Mass_Email_Send__c record
     */
    public void triggerMassEmailResend( Id massEmailId) {

        if ( String.isNotEmpty( massEmailId)) {

            try {

                Mass_Email_Send__c massES = [select Id, Status__c
                                               from Mass_Email_Send__c
                                              where Id = :massEmailId];

                massES.Status__c = this.MASS_EMAIL_READY_STATUS;
                update massES;
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in MassEmailSendDataAccessService.triggerMassEmailResend' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }
    }

    /**
     * @description: Attempts to clone a Mass Email Send record by ID
     *
     * @param massEmailId - The ID of the Mass_Email_Send__c record to be cloned
     *
     * @return the Id of the cloned Mass Email Send Record
     */
    public Id cloneMassEmailRecord( Id massEmailId) {
        Id returnVal;

        if ( String.isNotEmpty( massEmailId)) {

            try {

                Mass_Email_Send__c oldMassEs = this.getMassEmailSendById( massEmailId);
                Mass_Email_Send__c newMassES = oldMassEs.clone( false, true, false, false);
                newMassES.Date_Sent__c = null;
                newMassES.Status__c = EmailService.EMAIL_DRAFT_STATUS;

                insert newMassES;

                returnVal = newMassES.Id;
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in MassEmailSendDataAccessService.cloneMassEmailRecord' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }

        return returnVal;
    }

    /**
     * @description: Attempts to delete a Mass Email Send record by ID
     *
     * @param massEmailId - The ID of the Mass_Email_Send__c record to be deleted
     */
    public void deleteMassEmailRecord( Id massEmailId) {

        if ( String.isNotEmpty( massEmailId)) {

            try {

                Mass_Email_Send__c massES = this.getMassEmailSendById( massEmailId);

                delete massES;
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in MassEmailSendDataAccessService.deleteMassEmailRecord' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }
    }

}