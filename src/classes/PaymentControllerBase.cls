/*
 * Base Class for Payment page controllers (used for NAIS-488 and NAIS-873)
 *
 * SL, Exponent Partners, 2013
 */
public virtual class PaymentControllerBase {
    private static final String EMPTY_STRING = '';
    /* CONSTANTS */

    public static final String MESSAGE_PAGE_VALIDATION_ERROR = 'Please review and correct your data below.';

    public static final String MESSAGE_FIELD_VALUE_MISSING = 'Error: You must enter a value';
    public static final String MESSAGE_INVALID_EMAIL = 'Error: Please enter a valid email';
    public static final String MESSAGE_INVALID_CC_NUMBER = 'Error: Please enter a valid credit card number';
    public static final String MESSAGE_INVALID_CC_SECURITY_CODE = 'Error: Please enter a valid security code';
    public static final String MESSAGE_INVALID_CHECK_NUMBER = 'Error: Please enter a valid check number';
    public static final String MESSAGE_INVALID_CHECK_ROUTING_NUMBER = 'Error: Please enter a valid routing number';
    public static final String MESSAGE_INVALID_CHECK_ROUTING_NUMBER_LENGTH = 'Error: Routing number must be 9 digits';
    public static final String MESSAGE_INVALID_CHECK_ACCOUNT_NUMBER = 'Error: Please enter a valid account number';
    public static final String MESSAGE_EXPIRED_CARD = 'Error: Please enter a valid expiration date';

    private static final String MASTER_CARD = 'Master Card';
    private static final String VISA = 'Visa';
    private static final String AMEX = 'Amex';
    private static final String DISCOVER = 'Discover';

    public class FieldErrors {
        public String paymentType { get; set; }
        public String paymentAmount { get; set; }
        public String paymentDescription { get; set; }
        public String billingFirstName { get; set; }
        public String billingLastName { get; set; }
        public String billingStreet1 { get; set; }
        public String billingStreet2 { get; set; }
        public String billingCity { get; set; }
        public String billingState { get; set; }
        public String billingPostalCode { get; set; }
        public String billingCountry { get; set; }
        public String billingEmail { get; set; }
        public String billingEmailConfirm { get; set; }
        public String ccNameOnCard { get; set; }
        public String ccCardType { get; set; }
        public String ccCardNumber { get; set; }
        public String ccExpiration { get; set; }
        public String ccSecurityCode { get; set; }
        public String checkBankName { get; set; }
        public String checkAccountType { get; set; }
        public String checkAccountNumber { get; set; }
        public String checkAccountNumberConfirm { get; set; }
        public String checkNameOnCheck { get; set; }
        public String checkRoutingNumber { get; set; }
        public String checkRoutingNumberConfirm { get; set; }
        public String checkCheckType { get; set; }
        public String checkCheckNumber { get; set; }
        public String paymentTracker { get; set; }
    }

    public FamilyPaymentData paymentData { get; set; }
    public FieldErrors fieldErrors { get; set; }
    public Boolean PageError { get; set; }
    public String ErrorMessage { get; set; }

    public PaymentControllerBase() {
        paymentData = new FamilyPaymentData();
        fieldErrors = new FieldErrors();
    }

    public boolean getIsUnitedStatesSelected() {
        boolean isSelected = false;
        if (paymentData.billingCountry == PaymentProcessor.getDefaultCountryCode()) {
            isSelected = true;
        }
        return isSelected;
    }

    public List<SelectOption> getBillingCountrySelectOptions() {
        return PaymentProcessor.getBillingCountrySelectOptions();
    }

    public List<SelectOption> getBillingStateUSSelectOptions() {
        return PaymentProcessor.getBillingStateUSSelectOptions();
    }

    public List<SelectOption> getCreditCardTypeSelectOptions() {
        // [CH] NAIS-1547 Adding support for a custom setting to activate/deactivate credit card types
        List<SelectOption> options;

        Family_Portal_Settings__c familySettings = Family_Portal_Settings__c.getInstance('Family');
        if (familySettings != null) {
             options = new List<SelectOption> {};
             if (familySettings.CC_Visa__c) {
                 options.add(new SelectOption(FamilyPaymentData.CC_TYPE_VISA, VISA));
             }
             if (familySettings.CC_MasterCard__c) {
                 options.add(new SelectOption(FamilyPaymentData.CC_TYPE_MASTERCARD, MASTER_CARD));
             }
             if (familySettings.CC_Amex__c) {
                options.add(new SelectOption(FamilyPaymentData.CC_TYPE_AMEX, AMEX));
             }
             if (familySettings.CC_Discover__c) {
                 options.add(new SelectOption(FamilyPaymentData.CC_TYPE_DISCOVER, DISCOVER));
             }
        } else {
            options = new List<SelectOption> {
                new SelectOption(FamilyPaymentData.CC_TYPE_VISA, VISA),
                new SelectOption(FamilyPaymentData.CC_TYPE_MASTERCARD, MASTER_CARD),
                new SelectOption(FamilyPaymentData.CC_TYPE_AMEX, AMEX),
                new SelectOption(FamilyPaymentData.CC_TYPE_DISCOVER, DISCOVER)
            };
        }

        return options;
    }

    public List<SelectOption> getExpireMonthSelectOptions() {
        return PaymentProcessor.getExpireMonthSelectOptions();
    }

    public List<SelectOption> getExpireYearSelectOptions() {
        return PaymentProcessor.getExpireYearSelectOptions();
    }

    public List<SelectOption> getAccountTypeSelectOptions() {
        return PaymentProcessor.getAccountTypeSelectOptions();
    }

    public String getSelectedCountryName() {
        return PaymentProcessor.getCountryCode(paymentData.billingCountry);
    }

    public String getSelectedAccountType() {
        return PaymentProcessor.getAccountTypeLabel(paymentData.checkAccountType);
    }

    /**
     * @description Process the content of the current payment page. This
     *              wraps the processPayment() call in a try catch to ensure
     *              that only what should get rolled back does.
     * @return A page reference.
     */
    public PageReference process() {
        try {
            return processPayment();
        } catch (Exception e) {
            // Something happened. Let's go ahead and attempt to gracefully
            // back out while logging the issue if necessary.
            CustomDebugService.Instance.logException(e);

            // Show an error on the page letting the user know to call SSS
            handlePaymentError();
        }

        return null;
    }

    /**
     * @description A default implementation of handleError() that
     *              will be overrideen in the payment controllers.
     * @return A null page reference.
     */
    protected virtual PageReference handlePaymentError() {
        return null;
    }

    /**
     * @description A default implementation of processPayment() that
     *              will be overridden in the payment controllers.
     * @return A null page reference.
     */
    protected virtual PageReference processPayment() {
        return null;
    }

    protected boolean validateCreditCardFields() {
        boolean isValid = true;

        if (String.isBlank(paymentData.ccNameOnCard)) {
           isValid = false;
           fieldErrors.ccNameOnCard = MESSAGE_FIELD_VALUE_MISSING;
        }

        if (String.isBlank(paymentData.ccCardType) || (paymentData.ccCardType == 'NONE')) {
           isValid = false;
           fieldErrors.ccCardType = MESSAGE_FIELD_VALUE_MISSING;
        }

        // Validate that the card number is not blank, is a series of digits,
        // and has a proper length for the card type provided.
        if (!isValidCreditCardNumber(paymentData)) {
            isValid = false;
            fieldErrors.ccCardNumber = MESSAGE_INVALID_CC_NUMBER;
        }

        if (String.isBlank(paymentData.ccExpirationMM) || (paymentData.ccExpirationMM == '--Select--')
                || String.isBlank(paymentData.ccExpirationYY) || (paymentData.ccExpirationYY == 'NONE')) {
            isValid = false;
            fieldErrors.ccExpiration = MESSAGE_FIELD_VALUE_MISSING;
        } else if (!isValidExpiration(paymentData)) {
            isValid = false;
            fieldErrors.ccExpiration = MESSAGE_EXPIRED_CARD;
        }

        if (String.isBlank(paymentData.ccSecurityCode)) {
           isValid = false;
           fieldErrors.ccSecurityCode = MESSAGE_FIELD_VALUE_MISSING;
        } else if (!paymentData.ccSecurityCode.isNumeric()) {
            isValid = false;
            fieldErrors.ccSecurityCode = MESSAGE_INVALID_CC_SECURITY_CODE;
        } else if ((paymentData.ccSecurityCode.length() != 3 &&
                   (paymentData.ccCardType == MASTER_CARD ||
                    paymentData.ccCardType == VISA ||
                    paymentData.ccCardType == DISCOVER)) ||
                   (paymentData.ccSecurityCode.length() != 4 &&
                    paymentData.ccCardType == AMEX)) {
            isValid = false;
            fieldErrors.ccSecurityCode = MESSAGE_INVALID_CC_SECURITY_CODE;
        }

        return isValid;
    }

    protected boolean validateECheckFields() {
        boolean isValid = true;

        // Validate the account type
        if (String.isBlank(paymentData.checkAccountType) || (paymentData.checkAccountType == 'NONE')) {
           isValid = false;
           fieldErrors.checkAccountType = MESSAGE_FIELD_VALUE_MISSING;
        }

        // Validate the Bank Name
        if (String.isBlank(paymentData.checkBankName)) {
            isValid = false;
            fieldErrors.checkBankName = MESSAGE_FIELD_VALUE_MISSING;
        }

        // Validate the Name on Check field
        if (paymentData.checkAccountType == 'Business Checking' && String.isBlank(paymentData.checkNameOnCheck)) {
            isValid = false;
            fieldErrors.checkNameOnCheck = MESSAGE_FIELD_VALUE_MISSING;
        }

        // Validate the routing number
        if (String.isBlank(paymentData.checkRoutingNumber)) {
           isValid = false;
           fieldErrors.checkRoutingNumber = MESSAGE_FIELD_VALUE_MISSING;
        } else if (!paymentData.checkRoutingNumber.isNumeric()) {
            isValid = false;
            fieldErrors.checkRoutingNumber = MESSAGE_INVALID_CHECK_ROUTING_NUMBER;
        } else if (paymentData.checkRoutingNumber.length() != 9) {
            isValid = false;
            fieldErrors.checkRoutingNumber = MESSAGE_INVALID_CHECK_ROUTING_NUMBER_LENGTH;
        }

        // Validate the account number
        if (String.isBlank(paymentData.checkAccountNumber)) {
           isValid = false;
           fieldErrors.checkAccountNumber = MESSAGE_FIELD_VALUE_MISSING;
        } else if (!paymentData.checkAccountNumber.isNumericSpace()) {
            isValid = false;
            fieldErrors.checkAccountNumber = MESSAGE_INVALID_CHECK_ACCOUNT_NUMBER;
        }

        return isValid;
    }

    protected boolean validateBillingFields() {
        boolean isValid = true;

        if (String.isBlank(paymentData.billingFirstName)) {
            isValid = false;
            fieldErrors.billingFirstName = MESSAGE_FIELD_VALUE_MISSING;
        }

        if (String.isBlank(paymentData.billingLastName)) {
           isValid = false;
           fieldErrors.billingLastName = MESSAGE_FIELD_VALUE_MISSING;
        }

        if (String.isBlank(paymentData.billingEmail)) {
            isValid = false;
            fieldErrors.billingEmail = MESSAGE_FIELD_VALUE_MISSING;
        } else if (!paymentData.billingEmail.contains('@') || !paymentData.billingEmail.contains('.')) {
            isValid = false;
            fieldErrors.billingEmail = MESSAGE_INVALID_EMAIL;
        }

        if (String.isBlank(paymentData.billingCountry) || (paymentData.billingCountry == 'NONE')) {
           isValid = false;
           fieldErrors.billingCountry = MESSAGE_FIELD_VALUE_MISSING;
        }

        if (String.isBlank(paymentData.billingStreet1)) {
           isValid = false;
           fieldErrors.billingStreet1 = MESSAGE_FIELD_VALUE_MISSING;
        }

        if (String.isBlank(paymentData.billingCity)) {
           isValid = false;
           fieldErrors.billingCity = MESSAGE_FIELD_VALUE_MISSING;
        }

        // NAIS-2317 [DP] 03.05.2015 only require state if this is a United States transaction
        if (getIsUnitedStatesSelected() && (String.isBlank(paymentData.billingState) || (paymentData.billingState == 'NONE'))) {
           isValid = false;
           fieldErrors.billingState = MESSAGE_FIELD_VALUE_MISSING;
        }

        if (String.isBlank(paymentData.billingPostalCode)) {
           isValid = false;
           fieldErrors.billingPostalCode = MESSAGE_FIELD_VALUE_MISSING;
        }

        return isValid;
    }

    protected Opportunity getOpportunity() {
        // Gather the Id and if null exit early
        Id opportunityId = UrlService.Instance.getIdByType(getOpportunityIdParameter(), Opportunity.getSObjectType());
        if (opportunityId == null) {
            return null;
        }

        return getOpportunity(opportunityId);
    }

    protected Opportunity getOpportunity(Id opportunityId) {
        Opportunity opportunityRecord;
        List<Opportunity> opportunities = OpportunitySelector.Instance.selectByIdWithPfs(new Set<Id> { opportunityId });
        if (!opportunities.isEmpty()) {
            opportunityRecord = opportunities[0];
        }

        return opportunityRecord;
    }

    protected PFS__c getPfs() {
        // Gather the Id and if null exit early
        Id pfsId = UrlService.Instance.getIdByType(getPfsIdParameter(), PFS__c.getSObjectType());
        if (pfsId == null) {
            return null;
        }

        PFS__c pfsRecord;
        List<PFS__c> pfsRecords = PfsSelector.Instance.selectByIdWithOpportunities(new Set<Id> { pfsId });
        if (!pfsRecords.isEmpty()) {
            pfsRecord = pfsRecords[0];
        }

        return pfsRecord;
    }

    /**
     * @description Default placeholder for the opportunity Id query parameter.
     * @returns An empty string.
     */
    protected virtual String getOpportunityIdParameter() {
        return EMPTY_STRING;
    }

    /**
     * @description Default placeholder for the PFS Id query parameter.
     * @returns An empty string.
     */
    protected virtual String getPfsIdParameter() {
        return EMPTY_STRING;
    }

    private Boolean isValidCreditCardNumber(FamilyPaymentData paymentData) {
        return !String.isBlank(paymentData.ccCardNumber) &&
                paymentData.ccCardNumber.isNumericSpace() &&
               (isValidVisa(paymentData) ||
                isValidAmex(paymentData) ||
                isValidDiscover(paymentData) ||
                isValidMasterCard(paymentData));
    }

    private Boolean isValidVisa(FamilyPaymentData paymentData) {
        return paymentData.ccCardType == VISA &&
              (paymentData.ccCardNumber.length() == 13 ||
               paymentData.ccCardNumber.length() == 16 ||
               paymentData.ccCardNumber.length() == 19);
    }

    private Boolean isValidAmex(FamilyPaymentData paymentData) {
        return paymentData.ccCardType == AMEX &&
               paymentData.ccCardNumber.length() == 15;
    }

    private Boolean isValidDiscover(FamilyPaymentData paymentData) {
        return paymentData.ccCardType == DISCOVER &&
              (paymentData.ccCardNumber.length() == 16 ||
               paymentData.ccCardNumber.length() == 19);
    }

    private Boolean isValidMasterCard(FamilyPaymentData paymentData) {
        return paymentData.ccCardType == MASTER_CARD &&
               paymentData.ccCardNumber.length() == 16;
    }

    private Boolean isValidExpiration(FamilyPaymentData paymentData) {
        Integer year = Integer.valueOf(paymentData.ccExpirationYY);
        Integer currentYear = System.today().year();
        Integer month = Integer.valueOf(paymentData.ccExpirationMM);
        Integer currentMonth = System.today().month();

        return ((year > currentYear) || (year == currentYear && month >= currentMonth));
    }
}