/**
 * @description This class is used to send notifications for batch job operations. By using this class, we can ensure notification are sent to the entire team instead of just the user who initiated the job.
 */
public class BatchNotificationService {

    private static final String SYSTEM_USER_NAME = 'System User';
    private static final String FAILED_STATUS = 'Failed';
    private static final String SUBJECT_FORMAT = '{0} Status: {1}';
    private static final String BODY_FORMAT = 'The {0} job processed {1} batches with {2} failures. Extended Status: {3}';
    private static final String EXCEPTION_FORMAT = 'Exceptions: {0}';
    
    private BatchNotificationService() { }
    
    public void notifyFailures(Id jobId, String jobName) {
        notifyFailures(jobId, jobName, null);
    }
    
    /**
     * @description Checks the status of the specified AsyncApexJob and sends an email alert to sssoperations@nais.org
     *              if the job has failed.
     * @param jobId The Id of the job to check.
     * @param jobName The name of the job that should be used in the email. This is used to provide more context than
     *                the name of the batch class itself.
     * @param exceptions The list of exceptions to be sent in the emai. This is used to provide more details about the failures.
     */
    public void notifyFailures(Id jobId, String jobName, String exceptions) {
        // Instead of throwing null argument exception, we will just do nothing since jobs are not given Ids in unit tests.
        if (jobId == null || String.isBlank(jobName)) {
            return;
        }

        // Get job by Id
        JobWrapper job = getJob(jobId);

        // If we don't find the job, return early.
        if (job == null) {
            return;
        }

        // If the job is failed, send an email.
        if (FAILED_STATUS.equalsIgnoreCase(job.Status) || exceptions != null) {
            sendApexJobSummaryEmail(job, jobName, exceptions);
        }
    }

    private JobWrapper getJob(Id apexJobId) {
        List<AsyncApexJob> jobs = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                TotalJobItems, ExtendedStatus FROM AsyncApexJob WHERE Id = :apexJobId];

        return jobs.isEmpty() ? null : new JobWrapper(jobs[0]);
    }

    @testVisible
    private List<Messaging.SendEmailResult> sendApexJobSummaryEmail(JobWrapper apexJob, String apexJobName, String exceptions) {
        // Email the Batch Job results that the Job is finished with errors.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        String subject = String.format(SUBJECT_FORMAT, new List<String> { apexJobName, apexJob.Status});
        mail.setSubject(subject);

        Id recipientId = getRecipientId();
        mail.setTargetObjectId(recipientId);

        // Save As Activity flag must be false when sending emails to users.
        mail.setSaveAsActivity(false);

        String body = String.format(BODY_FORMAT, new List<String> {
                    apexJobName,
                    String.valueOf(apexJob.TotalBatches),
                    String.valueOf(apexJob.NumberOfErrors),
                    apexJob.ExtendedStatus });
        
        // Concatenates the exception messages catched during the batch execution
        body += exceptions == null ? '' :
                String.format(EXCEPTION_FORMAT, new List<String> {
                exceptions});
        
        mail.setPlainTextBody(body);
        
        return Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

    /**
     * @description Gets the Id of the user that should error notification emails. This user should be the user with the name System User.
     * @return The Id of the System User or the current user's Id.
     */
    private Id getRecipientId() {
        Id profileId = GlobalVariables.sysAdminProfileId;
        List<User> users = [SELECT Id FROM User WHERE IsActive = true AND Name = :SYSTEM_USER_NAME AND ProfileId = :profileId];

        return users.isEmpty() ? UserInfo.getUserId() : users[0].Id;
    }

    /**
     * @description This class simply acts as a wrapper for the AsyncApexJob class which allows us to mock scenarios in unit tests.
     */
    public class JobWrapper {

        /**
         * @description The status of the batch job. (e.g. Completed, Failed, Preparing)
         */
        public String Status { get; private set; }

        /**
         * @description A more detailed status of the batch job that will include the first error encountered by the batch.
         */
        public String ExtendedStatus { get; private set; }

        /**
         * @description The total number of batches.
         */
        public Integer TotalBatches { get; private set; }

        /**
         * @description The number of failed batches.
         */
        public Integer NumberOfErrors { get; private set; }

        public JobWrapper(String status, String extStatus, Integer numberOfBatches, Integer numberOfErrors) {
            this.Status = status;
            this.ExtendedStatus = extStatus;
            this.TotalBatches = numberOfBatches;
            this.NumberOfErrors = numberOfErrors;
        }

        public JobWrapper(AsyncApexJob apexJob) {
            this(apexJob.Status, apexJob.ExtendedStatus, apexJob.TotalJobItems, apexJob.NumberOfErrors);
        }
    }

    /**
     * @description Singleton instance of this service.
     */
    public static BatchNotificationService Instance {
        get {
            if (Instance == null) {
                Instance = new BatchNotificationService();
            }
            return Instance;
        }
        private set;
    }
}