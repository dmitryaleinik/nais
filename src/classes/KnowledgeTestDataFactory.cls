/**
 * KnowledgeTestDataFactory.cls
 *
 * @description: Test data factory class for Knowledge generates necessary test data for unit tests
 *
 * @author: Mike Havrilla @ Presence PG
 */
@isTest
public class KnowledgeTestDataFactory {
    /**
    * @description: creates a list of knowledge articles based on an integer.
    * @param numberOfArticles to return
    * @return A List of Knowledge__kav records
    */
    public static List<Knowledge__kav> createKnowledgeArticleVersions( Integer numberOfArticles) {
        return createKnowledgeArticleVersions( numberOfArticles, null);
    }

    /**
    * @description: creates a list of knowledge articles based on an integer.
    * @param numberOfArticles to return
    * @param valueToAppendToBody appends a value to the body for search testing.
    * @return A List of Knowledge__kav records
    */
    public static List<Knowledge__kav> createKnowledgeArticleVersions( Integer numberOfArticles, String valueToAppendToBody) {
        List<Knowledge__kav> articles = new List<Knowledge__kav>();

        for( Integer i = 0; i < numberOfArticles; i++) {
            Integer randomNumber = (Integer) Math.floor(Math.random() * 100000); // creates a number between 0 and 999,999;
            String randomValue = String.valueOf( randomNumber);

            Knowledge__kav article = new Knowledge__kav();
            article.ValidationStatus = 'Validated';
            article.Title = 'Test Knowledge Article ' + randomValue;
            article.UrlName = 'test-knowledge-article-' + randomValue;
            article.Summary = 'This is a Test Knowledge Article created for Unit Tests';
            article.Sequence__c = i;
            article.Article_Body__c = 'Test';
            article.Language = 'en_US';

            if( String.isNotEmpty( valueToAppendToBody)) {
                article.Article_Body__c += valueToAppendToBody;
            }
            articles.add( article);
        }

        return articles;
    }

    /**
    * @description: Assigns a list of knowledge articles to a category for viewing.
    * @param articlesToAssign a list of articles thtat will get assigned
    * @param defaultCategory default category to assign them too.
    * @return A List of Knowledge__DataCategorySelection records.
    */
    public static List<Knowledge__DataCategorySelection> assignKnowledgeArticleToCategory( List<Knowledge__kav> articlesToAssign, String defaultCategory) {
        List<Knowledge__DataCategorySelection> knowledgeCategories = new List<Knowledge__DataCategorySelection>();

        if( defaultCategory == null) {
            defaultCategory = 'Documents';
        }

        for( Knowledge__kav article : articlesToAssign) {

            Knowledge__DataCategorySelection knowledgeCategory = new Knowledge__DataCategorySelection();
            knowledgeCategory.DataCategoryGroupName = 'Audience';
            knowledgeCategory.DataCategoryName = defaultCategory;
            knowledgeCategory.ParentId = article.Id;
            knowledgeCategories.add( knowledgeCategory);
        }

        return knowledgeCategories;
    }

    /**
    * @description: Publishes a list of articles.
    * @param a list of Knowledge__kav objects to publish.
    */
    public static void publishArticles( List<Knowledge__kav> articlesToPublish) {
        for( Knowledge__kav article : articlesToPublish) {
            KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
        }
    }
}