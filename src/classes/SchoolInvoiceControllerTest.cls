@isTest
private class SchoolInvoiceControllerTest {
    private static Opportunity testOpp {get;set;}
    private static PFS__c testPFS;
    private static User testUser;
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

    /**
     * @description Insert a Portal User who then creates a PFS
     *              and Opportunity to use in the Test Methods
     */
    private static void init() {
        Account testAccount = testUtils.createAccount('Test Account', RecordTypes.schoolAccountTypeId, 1, false);
        testAccount.BillingStreet = 'Test Street';
        testAccount.BillingCity = 'Test City';
        testAccount.BillingCountry = 'United States';
        testAccount.BillingState = 'Arizona';
        testAccount.BillingPostalCode = '00000';
        testAccount.Phone = '5555555555';
        testAccount.SSS_School_Code__c = 'Test Code';
        insert testAccount;

        Contact testContact = testUtils.createContact('Test Contact', testAccount.Id, RecordTypes.schoolStaffContactTypeId, false);
        testContact.Email = 'testcontact@test.com';
        insert testContact;

        Academic_Year__c testAcademicYear = testUtils.createAcademicYear('2020-2021', true);

        Subscription_Settings__c sSettings = new Subscription_Settings__c();
        sSettings.Name = 'Subscription Settings';
        sSettings.Start_Month__c = 10;
        sSettings.Start_Day__c = 1;
        sSettings.End_Month__c = 9;
        sSettings.End_Day__c = 30;
        sSettings.Number_of_Months__c = 12;
        sSettings.Number_of_Months_Extended__c = 36;
        insert sSettings;

        //Insert the Portal User as the current system administrator to avoid Mixed DML Errors
        system.runAs(thisUser) {
            testUser = testUtils.createPortalUser('Test Portal User', 'port@l.user', 'test', testContact.Id, GlobalVariables.schoolPortalAdminProfileId, true, true);
        }

        //Insert the PFS and Opportunity as the Portal User so that it is owned by that user.
        system.runAs(testUser) {
            testPFS = testUtils.createPFS('Test PFS', testAcademicYear.Id, testContact.Id, true);

            //Insert the Opportunity with a 100 dollar discount
            testOpp = testUtils.createOpportunity('Test Opp', RecordTypes.opportunitySubscriptionFeeTypeId, testPFS.Id, testAcademicYear.Name, false);
            testOpp.AccountId = testAccount.Id;
            testOpp.Amount = 500;
            testOpp.CloseDate = date.newInstance(2015, 1, 1);
            testOpp.Subscription_Discount__c = 100;
            insert testOpp;
        }

        //Create TLIs on the Opportunity to emulate a Sale of $500 and a payment of $400 dollars
        List<Transaction_Line_Item__c> insertTLIs = new List<Transaction_Line_Item__c>();

        Transaction_Line_Item__c testSaleTLI = testUtils.createTransactionLineItem(testOpp.Id, RecordTypes.saleTransactionTypeId, false);
        testSaleTLI.Amount__c = 500;
        testSaleTLI.Product_Code__c = 'Test Product Code';
        testSaleTLI.Transaction_Type__c = 'School Subscription';
        insertTLIs.add(testSaleTLI);

        Transaction_Line_Item__c testPaymentTLI = testUtils.createTransactionLineItem(testOpp.Id, RecordTypes.paymentTransactionTypeId, false);
        testPaymentTLI.Amount__c = 400;
        testPaymentTLI.Tender_Type__c = 'Visa';
        testPaymentTLI.CC_Last_4_Digits__c = '5555';
        testPaymentTLI.Transaction_Type__c = 'Credit/Debit Card';
        insertTLIs.add(testPaymentTLI);

        Transaction_Line_Item__c testRefundTLI = testUtils.createTransactionLineItem(testOpp.Id, RecordTypes.refundTransactionTypeId, false);
        testRefundTLI.Amount__c = -500;
        testRefundTLI.Product_Code__c = 'Test Product Code';
        testRefundTLI.Transaction_Type__c = 'Refund-CC';
        insertTLIs.add(testrefundTLI);

        Transaction_Line_Item__c testDiscountTLI = testUtils.createTransactionLineItem(testOpp.Id, RecordTypes.schoolDiscountTransactionTypeId, false);
        testDiscountTLI.Amount__c = -500;
        testDiscountTLI.Product_Code__c = 'Test Product Code';
        testDiscountTLI.Transaction_Type__c = 'School Discount';
        insertTLIs.add(testdiscountTLI);

        Transaction_Line_Item__c testPaymentTLI2 = testUtils.createTransactionLineItem(testOpp.Id, RecordTypes.paymentTransactionTypeId, false);
        testPaymentTLI2.Amount__c = 400;
        testPaymentTLI2.Tender_Type__c = 'Visa';
        testPaymentTLI2.CC_Last_4_Digits__c = '5555';
        testPaymentTLI2.Transaction_Type__c = 'Credit/Debit Card';
        insertTLIs.add(testPaymentTLI2);

        Transaction_Line_Item__c testRefundTLI2 = testUtils.createTransactionLineItem(testOpp.Id, RecordTypes.refundTransactionTypeId, false);
        testRefundTLI2.Amount__c = -500;
        testRefundTLI2.Product_Code__c = 'Test Product Code';
        testRefundTLI2.Transaction_Type__c = 'Refund-CC';
        insertTLIs.add(testrefundTLI2);

        Transaction_Line_Item__c testDiscountTLI2 = testUtils.createTransactionLineItem(testOpp.Id, RecordTypes.schoolDiscountTransactionTypeId, false);
        testDiscountTLI2.Amount__c = -500;
        testDiscountTLI2.Product_Code__c = 'Test Product Code';
        testDiscountTLI2.Transaction_Type__c = 'School Discount';
        insertTLIs.add(testdiscountTLI2);

        insert insertTLIs;
    }

    private static void assertInvoiceGeneratedDateTime(SchoolInvoiceController controller) {
        System.assertNotEquals(null, controller.opp.DateTime_Invoice_Generated__c,
                'Expected there to be a date time set.');

        // Because the time could be different, we'll just validate the date
        DateTime invoiceDateTime = controller.opp.DateTime_Invoice_Generated__c;
        Date stampedDate = date.newInstance(invoiceDateTime.year(), invoiceDateTime.month(), invoiceDateTime.day());

        System.assertEquals(System.today(), stampedDate, 'Expected the dates to match.');
    }

    /**
     * @description Tests the data that is saved in all of the variables
     *              that populate SchoolReceipt and SchoolInvoice pages.
     */
    @isTest
    private static void testSchoolInvoiceController() {
        init();

        // [DP] 06.06.2016 requery opp to get Name for description assertion
        testOpp = [Select Id, Name, CloseDate FROM Opportunity WHERE Id = :testOpp.Id];

        //Run as the portal user who created the Opp
        system.runAs(testUser) {
            test.StartTest();

                ApexPages.currentPage().getParameters().put('id',testOpp.Id);
                SchoolInvoiceController controller = new SchoolInvoiceController();

            test.StopTest();

            system.assertEquals('Test Account', controller.toSchoolName);
            system.assertEquals('Test Street', controller.toStreet);
            system.assertEquals('Test City', controller.toCity);
            system.assertEquals('Arizona', controller.toState);
            system.assertEquals('00000', controller.toPostalCode);
            system.assertEquals('5555555555', controller.toPhone);
            system.assertEquals('2020-2021', controller.academicYear);
            system.assertEquals(testOpp.CloseDate.format(), controller.currentDate); // NAIS-2376
            system.assertEquals('Visa ending in 5555', controller.paymentMethod);
            system.assertEquals(null, controller.checkNumber);
            system.assertEquals('Test Code', controller.schoolCode);
            system.assertEquals(1, controller.qty);
            system.assertEquals('Test Product Code', controller.productCode);
            system.assertEquals(800, controller.totalPaid);
            system.assertEquals(500, controller.totalAmount);
            system.assertEquals(testOpp.Name, controller.description);
            system.assertEquals(date.newInstance(2015, 1, 1).format(), controller.oppDate);
            system.assertEquals(100, controller.subDiscount);
            system.assertEquals(400, controller.netDueInvoice);
            system.assertEquals(false, controller.credit);
        }
    }

    @isTest
    private static void applyInvoiceDateTime_noOpportunity_expectNoError() {
        SchoolInvoiceController controller = new SchoolInvoiceController();

        try {
            Test.startTest();
            controller.applyInvoiceDateTime();

            System.assertEquals(null, controller.opp.DateTime_Invoice_Generated__c,
                    'Expected the DateTime_Invoice_Generated__c field to be null.');
        } catch (Exception e) {
            System.assert(false, 'Expected there to be no errors. Caught: ' + e);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void applyInvoiceDateTime_opportunityHasNoDate_expectDateSet() {
        init();

        ApexPages.currentPage().getParameters().put('id',testOpp.Id);
        SchoolInvoiceController controller = new SchoolInvoiceController();

        Test.startTest();
        controller.applyInvoiceDateTime();
        Test.stopTest();

        assertInvoiceGeneratedDateTime(controller);
    }

    @isTest
    private static void applyInvoiceDateTime_opportunityHasDate_expectDateNotUpdated() {
        init();

        ApexPages.currentPage().getParameters().put('id',testOpp.Id);
        SchoolInvoiceController controller = new SchoolInvoiceController();
        controller.applyInvoiceDateTime();

        assertInvoiceGeneratedDateTime(controller);

        DateTime invoiceDateTime = controller.opp.DateTime_Invoice_Generated__c;

        Test.startTest();
        controller.applyInvoiceDateTime();
        Test.stopTest();

        System.assertEquals(invoiceDateTime, controller.opp.DateTime_Invoice_Generated__c,
                'Expected the DateTime_Invoice_Generated__c to not be updated on the opportunity.');
    }
}