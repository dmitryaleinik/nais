@isTest
private with sharing class FamilyAppFamilyExpEducationContTest {

    private static Account school1;
    private static Student_Folder__c studentFolder11;
    private static PFS__c pfsA;
    private static Applicant__c applicant1A;
    private static Dependents__c dependents;
    private static Contact parentA;
    private static Contact student1;

    static void SetupData()
    {
        //setup PFS
        // create test data
        TestUtils.createAcademicYears();
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        
        parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, true);
        student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, true);
        
        studentFolder11 = TestUtils.createStudentFolder('Student Folder 1.1', academicYearId, student1.Id, true);
        
        school1 = TestUtils.createAccount('Test School 1', RecordTypes.schoolAccountTypeId, 3, true);
        
        pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, true);
        applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, true);
        dependents = new Dependents__c(PFS__c = pfsa.Id);
        
        try {
            insert dependents;
        } catch (DMLException ex) {
            System.assert(false); // shouldn't get here
        }
        
        String pfsSql = getPFSqueryString();
        
        //reload pfs
         pfsA = Database.query(pfsSql);
    }
    
    private static String getPFSqueryString()
    {
        return 'SELECT ' + String.Join(ExpCore.GetAllFieldNames('PFS__c'), ',') + ', (SELECT ' + String.Join(ExpCore.GetAllFieldNames('Applicant__c'), ',') + ' FROM Applicants__r) ' +  ', (SELECT ' + String.Join(ExpCore.GetAllFieldNames('Dependents__c'), ',') + ' FROM Dependents__r) FROM PFS__c LIMIT 1';
    }

    @isTest
    private static void TestUpdateDelegates()
    {
        FamilyAppFamilyExpEducationContTest.SetupData();
        
        FamilyAppFamilyExpEducationCont controller = new FamilyAppFamilyExpEducationCont();
        controller.pfs = pfsA;
        controller.currentApplicantId = pfsA.Applicants__r[0].Id;
        controller.haveEducationExpense = 'Yes';
        
        System.assert(controller.UpdateDelegates() == null);
    }
    
    @isTest
    private static void TestUpdateOtherSources()
    {
        FamilyAppFamilyExpEducationContTest.SetupData();
        
        FamilyAppFamilyExpEducationCont controller = new FamilyAppFamilyExpEducationCont();
        controller.pfs = pfsA;
        controller.currentApplicantId = pfsA.Applicants__r[0].Id;
        
        controller.otherSourcesCurrent = '100.0';
        controller.otherSourcesEst = '20.0';
        
        System.assertEquals(controller.UpdateOtherSources(), null);
        
        Map<Id, Applicant__c> applicantMap = new Map<Id, Applicant__c>(controller.pfs.Applicants__r);
        Applicant__c applicant = applicantMap.get(controller.currentApplicantId);
        System.assertEquals(applicant.Other_Source_Sources_Current__c, Decimal.valueOf(controller.otherSourcesCurrent));
        System.assertEquals(applicant.Other_Source_Sources_Est__c, Decimal.valueOf(controller.otherSourcesEst));
    }
    
    @isTest
    private static void TestUpdateDependentOtherSources() {
        FamilyAppFamilyExpEducationContTest.SetupData();
        
        FamilyAppFamilyExpEducationCont controller = new FamilyAppFamilyExpEducationCont();
        controller.pfs = pfsA;
        
        controller.otherSourcesCurrent = '10.0';
        controller.otherSourcesEst = '20.0';
        controller.dependentId = '1';
        
        System.assertEquals(controller.updateDependentOtherSources(), null);
        
        Dependents__c theDependents = controller.pfs.Dependents__r[0];
        System.assertEquals(theDependents.Dependent_1_Other_Source_Sources_Current__c, Decimal.valueOf(controller.otherSourcesCurrent));
        System.assertEquals(theDependents.Dependent_1_Other_Source_Sources_Est__c, Decimal.valueOf(controller.otherSourcesEst));
        
        controller.otherSourcesCurrent = '30.0';
        controller.otherSourcesEst = '40.0';
        controller.dependentId = '2';
        
        System.assertEquals(controller.updateDependentOtherSources(), null);
        
        theDependents = controller.pfs.Dependents__r[0];
        System.assertEquals(theDependents.Dependent_2_Other_Source_Sources_Current__c, Decimal.valueOf(controller.otherSourcesCurrent));
        System.assertEquals(theDependents.Dependent_2_Other_Source_Sources_Est__c, Decimal.valueOf(controller.otherSourcesEst));
        
        controller.otherSourcesCurrent = '50.0';
        controller.otherSourcesEst = '60.0';
        controller.dependentId = '3';
        
        System.assertEquals(controller.updateDependentOtherSources(), null);
        
        theDependents = controller.pfs.Dependents__r[0];
        System.assertEquals(theDependents.Dependent_3_Other_Source_Sources_Current__c, Decimal.valueOf(controller.otherSourcesCurrent));
        System.assertEquals(theDependents.Dependent_3_Other_Source_Sources_Est__c, Decimal.valueOf(controller.otherSourcesEst));
        
        controller.otherSourcesCurrent = '70.0';
        controller.otherSourcesEst = '80.0';
        controller.dependentId = '4';
        
        System.assertEquals(controller.updateDependentOtherSources(), null);
        
        theDependents = controller.pfs.Dependents__r[0];
        System.assertEquals(theDependents.Dependent_4_Other_Source_Sources_Current__c, Decimal.valueOf(controller.otherSourcesCurrent));
        System.assertEquals(theDependents.Dependent_4_Other_Source_Sources_Est__c, Decimal.valueOf(controller.otherSourcesEst));
        
        controller.otherSourcesCurrent = '90.0';
        controller.otherSourcesEst = '100.0';
        controller.dependentId = '5';
        
        System.assertEquals(controller.updateDependentOtherSources(), null);
        
        theDependents = controller.pfs.Dependents__r[0];
        System.assertEquals(theDependents.Dependent_5_Other_Source_Sources_Current__c, Decimal.valueOf(controller.otherSourcesCurrent));
        System.assertEquals(theDependents.Dependent_5_Other_Source_Sources_Est__c, Decimal.valueOf(controller.otherSourcesEst));
        
        controller.otherSourcesCurrent = '110.0';
        controller.otherSourcesEst = '120.0';
        controller.dependentId = '6';
        
        System.assertEquals(controller.updateDependentOtherSources(), null);
        
        theDependents = controller.pfs.Dependents__r[0];
        System.assertEquals(theDependents.Dependent_6_Other_Source_Sources_Current__c, Decimal.valueOf(controller.otherSourcesCurrent));
        System.assertEquals(theDependents.Dependent_6_Other_Source_Sources_Est__c, Decimal.valueOf(controller.otherSourcesEst));
        
        // code coverage
        // bad values
        controller.otherSourcesCurrent = 'aaa';
        controller.otherSourcesEst = 'bbb';
        controller.dependentId = '1';
        
        System.assertEquals(controller.updateDependentOtherSources(), null);   
        System.assertEquals(theDependents.Dependent_1_Other_Source_Sources_Current__c, 10);
        System.assertEquals(theDependents.Dependent_1_Other_Source_Sources_Est__c,20);   
        
        controller.otherSourcesCurrent = '10';
        controller.otherSourcesEst = '20';
        controller.dependentId = '7';
        
        System.assertEquals(controller.updateDependentOtherSources(), null);                                                         
    }
    
    private static FamilyAppFamilyExpEducationCont setupForClearDependenFields()
    {
        School_PFS_Assignment__c spaRecord = SchoolPfsAssignmentTestData.Instance.DefaultSchoolPfsAssignment;
        
        spaRecord = [SELECT Id, Applicant__c, Applicant__r.PFS__c FROM School_PFS_Assignment__c WHERE Id =: spaRecord.Id LIMIT 1];
        
        System.AssertEquals(true, spaRecord.Applicant__r.PFS__c != null);
        System.AssertEquals(true, spaRecord.Applicant__c != null);
        
        Dependents__c dependent = DependentsTestData.Instance.forPfsId(spaRecord.Applicant__r.PFS__c).DefaultDependents;
        
        PFS__c pfsRecord = Database.query(getPFSqueryString());
        
        Test.StartTest();
            
            FamilyAppFamilyExpEducationCont controller = new FamilyAppFamilyExpEducationCont();
            controller.pfs = pfsRecord;
            controller.currentApplicantId = spaRecord.Applicant__c;
        Test.StopTest();
        
        return controller;
    }
    
    @isTest 
    private static void clearDependenTuitionProgramNextYearFields_answerChangesFromYesToNoAndDependents1To6WithValues_expectValuesForDependents1To6AreClearOut()
    {
        FamilyAppFamilyExpEducationCont controller = setupForClearDependenFields();
            
        //1. When 13.(1-6)h is changed from yes to no, clear out all 13.(1-6)i through 13.(1-6)q for that dependent.
        controller.pfs.Dependents__r[0].Dependent_1_Tuition_Program_Next_Year__c = 'Yes';
        controller.pfs.Dependents__r[0].Dependent_2_Tuition_Program_Next_Year__c = 'Yes';
        controller.pfs.Dependents__r[0].Dependent_3_Tuition_Program_Next_Year__c = 'Yes';
        controller.pfs.Dependents__r[0].Dependent_4_Tuition_Program_Next_Year__c = 'Yes';
        controller.pfs.Dependents__r[0].Dependent_5_Tuition_Program_Next_Year__c = 'Yes';
        controller.pfs.Dependents__r[0].Dependent_6_Tuition_Program_Next_Year__c = 'Yes';
        
        controller.pfs.Dependents__r[0].Dependent_1_Tuition_Cost_Est__c = 11;
        controller.pfs.Dependents__r[0].Dependent_2_Tuition_Cost_Est__c = 11;
        controller.pfs.Dependents__r[0].Dependent_3_Tuition_Cost_Est__c = 11;
        controller.pfs.Dependents__r[0].Dependent_4_Tuition_Cost_Est__c = 11;
        controller.pfs.Dependents__r[0].Dependent_5_Tuition_Cost_Est__c = 11;
        controller.pfs.Dependents__r[0].Dependent_6_Tuition_Cost_Est__c = 11;
        
        controller.pfs.Dependents__r[0].Dependent_1_Parent_Sources_Est__c = 22;
        controller.pfs.Dependents__r[0].Dependent_2_Parent_Sources_Est__c = 22;
        controller.pfs.Dependents__r[0].Dependent_3_Parent_Sources_Est__c = 22;
        controller.pfs.Dependents__r[0].Dependent_4_Parent_Sources_Est__c = 22;
        controller.pfs.Dependents__r[0].Dependent_5_Parent_Sources_Est__c = 22;
        controller.pfs.Dependents__r[0].Dependent_6_Parent_Sources_Est__c = 22;
        
        controller.clearDependenTuitionProgramNextYearFields1();
        controller.clearDependenTuitionProgramNextYearFields2();
        controller.clearDependenTuitionProgramNextYearFields3();
        controller.clearDependenTuitionProgramNextYearFields4();
        controller.clearDependenTuitionProgramNextYearFields5();
        controller.clearDependenTuitionProgramNextYearFields6();
        
        System.assertEquals(11, controller.pfs.Dependents__r[0].Dependent_1_Tuition_Cost_Est__c);
        System.assertEquals(11, controller.pfs.Dependents__r[0].Dependent_2_Tuition_Cost_Est__c);
        System.assertEquals(11, controller.pfs.Dependents__r[0].Dependent_3_Tuition_Cost_Est__c);
        System.assertEquals(11, controller.pfs.Dependents__r[0].Dependent_4_Tuition_Cost_Est__c);
        System.assertEquals(11, controller.pfs.Dependents__r[0].Dependent_5_Tuition_Cost_Est__c);
        System.assertEquals(11, controller.pfs.Dependents__r[0].Dependent_6_Tuition_Cost_Est__c);
        System.assertEquals(22, controller.pfs.Dependents__r[0].Dependent_1_Parent_Sources_Est__c);
        System.assertEquals(22, controller.pfs.Dependents__r[0].Dependent_2_Parent_Sources_Est__c);
        System.assertEquals(22, controller.pfs.Dependents__r[0].Dependent_3_Parent_Sources_Est__c);
        System.assertEquals(22, controller.pfs.Dependents__r[0].Dependent_4_Parent_Sources_Est__c);
        System.assertEquals(22, controller.pfs.Dependents__r[0].Dependent_5_Parent_Sources_Est__c);
        System.assertEquals(22, controller.pfs.Dependents__r[0].Dependent_6_Parent_Sources_Est__c);
        
        //1.1 Answer changes from yes to no
        controller.pfs.Dependents__r[0].Dependent_1_Tuition_Program_Next_Year__c = 'No';
        controller.pfs.Dependents__r[0].Dependent_2_Tuition_Program_Next_Year__c = 'No';
        controller.pfs.Dependents__r[0].Dependent_3_Tuition_Program_Next_Year__c = 'No';
        controller.pfs.Dependents__r[0].Dependent_4_Tuition_Program_Next_Year__c = 'No';
        controller.pfs.Dependents__r[0].Dependent_5_Tuition_Program_Next_Year__c = 'No';
        controller.pfs.Dependents__r[0].Dependent_6_Tuition_Program_Next_Year__c = 'No';
        
        controller.clearDependenTuitionProgramNextYearFields1();
        controller.clearDependenTuitionProgramNextYearFields2();
        controller.clearDependenTuitionProgramNextYearFields3();
        controller.clearDependenTuitionProgramNextYearFields4();
        controller.clearDependenTuitionProgramNextYearFields5();
        controller.clearDependenTuitionProgramNextYearFields6();
        
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_1_Tuition_Cost_Est__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_2_Tuition_Cost_Est__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_3_Tuition_Cost_Est__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_4_Tuition_Cost_Est__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_5_Tuition_Cost_Est__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_6_Tuition_Cost_Est__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_1_Parent_Sources_Est__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_2_Parent_Sources_Est__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_3_Parent_Sources_Est__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_4_Parent_Sources_Est__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_5_Parent_Sources_Est__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_6_Parent_Sources_Est__c);
    }
    
    @isTest 
    private static void clearDependenHaveEducationExpensesFields_answerChangesFromYesToNoAndDependents1To6WithValues_expectValuesForDependents1To6AreClearOut()
    {
        FamilyAppFamilyExpEducationCont controller = setupForClearDependenFields();
            
        //1. When 13.(1-6)a is changed from yes to no, clear out all 13.(1-6)b through 13.(1-6)h for that dependent.
        controller.pfs.Dependents__r[0].Dependent_1_Have_Education_Expenses__c = 'Yes';
        controller.pfs.Dependents__r[0].Dependent_2_Have_Education_Expenses__c = 'Yes';
        controller.pfs.Dependents__r[0].Dependent_3_Have_Education_Expenses__c = 'Yes';
        controller.pfs.Dependents__r[0].Dependent_4_Have_Education_Expenses__c = 'Yes';
        controller.pfs.Dependents__r[0].Dependent_5_Have_Education_Expenses__c = 'Yes';
        controller.pfs.Dependents__r[0].Dependent_6_Have_Education_Expenses__c = 'Yes';
        
        controller.pfs.Dependents__r[0].Dependent_1_Tuition_Cost_Current__c = 11;
        controller.pfs.Dependents__r[0].Dependent_2_Tuition_Cost_Current__c = 11;
        controller.pfs.Dependents__r[0].Dependent_3_Tuition_Cost_Current__c = 11;
        controller.pfs.Dependents__r[0].Dependent_4_Tuition_Cost_Current__c = 11;
        controller.pfs.Dependents__r[0].Dependent_5_Tuition_Cost_Current__c = 11;
        controller.pfs.Dependents__r[0].Dependent_6_Tuition_Cost_Current__c = 11;
        
        controller.pfs.Dependents__r[0].Dependent_1_Parent_Sources_Current__c = 22;
        controller.pfs.Dependents__r[0].Dependent_2_Parent_Sources_Current__c = 22;
        controller.pfs.Dependents__r[0].Dependent_3_Parent_Sources_Current__c = 22;
        controller.pfs.Dependents__r[0].Dependent_4_Parent_Sources_Current__c = 22;
        controller.pfs.Dependents__r[0].Dependent_5_Parent_Sources_Current__c = 22;
        controller.pfs.Dependents__r[0].Dependent_6_Parent_Sources_Current__c = 22;
        
        controller.clearDependenHaveEducationExpensesFields1();
        controller.clearDependenHaveEducationExpensesFields2();
        controller.clearDependenHaveEducationExpensesFields3();
        controller.clearDependenHaveEducationExpensesFields4();
        controller.clearDependenHaveEducationExpensesFields5();
        controller.clearDependenHaveEducationExpensesFields6();
        
        System.assertEquals(11, controller.pfs.Dependents__r[0].Dependent_1_Tuition_Cost_Current__c);
        System.assertEquals(11, controller.pfs.Dependents__r[0].Dependent_2_Tuition_Cost_Current__c);
        System.assertEquals(11, controller.pfs.Dependents__r[0].Dependent_3_Tuition_Cost_Current__c);
        System.assertEquals(11, controller.pfs.Dependents__r[0].Dependent_4_Tuition_Cost_Current__c);
        System.assertEquals(11, controller.pfs.Dependents__r[0].Dependent_5_Tuition_Cost_Current__c);
        System.assertEquals(11, controller.pfs.Dependents__r[0].Dependent_6_Tuition_Cost_Current__c);
        System.assertEquals(22, controller.pfs.Dependents__r[0].Dependent_1_Parent_Sources_Current__c);
        System.assertEquals(22, controller.pfs.Dependents__r[0].Dependent_2_Parent_Sources_Current__c);
        System.assertEquals(22, controller.pfs.Dependents__r[0].Dependent_3_Parent_Sources_Current__c);
        System.assertEquals(22, controller.pfs.Dependents__r[0].Dependent_4_Parent_Sources_Current__c);
        System.assertEquals(22, controller.pfs.Dependents__r[0].Dependent_5_Parent_Sources_Current__c);
        System.assertEquals(22, controller.pfs.Dependents__r[0].Dependent_6_Parent_Sources_Current__c);
        
        //1.1 Answer changes from yes to no
        controller.pfs.Dependents__r[0].Dependent_1_Have_Education_Expenses__c = 'No';
        controller.pfs.Dependents__r[0].Dependent_2_Have_Education_Expenses__c = 'No';
        controller.pfs.Dependents__r[0].Dependent_3_Have_Education_Expenses__c = 'No';
        controller.pfs.Dependents__r[0].Dependent_4_Have_Education_Expenses__c = 'No';
        controller.pfs.Dependents__r[0].Dependent_5_Have_Education_Expenses__c = 'No';
        controller.pfs.Dependents__r[0].Dependent_6_Have_Education_Expenses__c = 'No';
        
        controller.clearDependenHaveEducationExpensesFields1();
        controller.clearDependenHaveEducationExpensesFields2();
        controller.clearDependenHaveEducationExpensesFields3();
        controller.clearDependenHaveEducationExpensesFields4();
        controller.clearDependenHaveEducationExpensesFields5();
        controller.clearDependenHaveEducationExpensesFields6();
        
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_1_Tuition_Cost_Current__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_2_Tuition_Cost_Current__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_3_Tuition_Cost_Current__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_4_Tuition_Cost_Current__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_5_Tuition_Cost_Current__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_6_Tuition_Cost_Current__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_1_Parent_Sources_Current__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_2_Parent_Sources_Current__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_3_Parent_Sources_Current__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_4_Parent_Sources_Current__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_5_Parent_Sources_Current__c);
        System.assertEquals(0, controller.pfs.Dependents__r[0].Dependent_6_Parent_Sources_Current__c);
    }
}