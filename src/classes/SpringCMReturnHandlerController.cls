public with sharing class SpringCMReturnHandlerController {
    
    // PROPERTIES
    public String docIdParam {get; set;}
    public String successParam {get; set;}
    public String destParam {get; set;}


    // ACTION METHODS
    
    public PageReference pageLoadAction() {
        docIdParam = ApexPages.currentPage().getParameters().get('docid');
        successParam = ApexPages.currentPage().getParameters().get('success');
        destParam = ApexPages.currentPage().getParameters().get('dest');
        
        System.debug('SpringCMReturnHandler>>> docid='+docIdParam+'; success='+successParam+'; dest='+ destParam);

        // JAC - 1/29/15 - NAIS-2214 - Change redirect to autoclose to include a message. 
        String message = null;
        String messageBgColor = null;
        String messageForeColor = null;
        // do post-upload processing
        if (successParam == '1') {
            processSuccess();
            //NAIS-2290 Change upload successful message
            message = Label.Family_Document_Upload_Successful;
            messageBgColor = 'green';
            messageForeColor = 'white';
        }
        else if (successParam == '0') {
            processCancel();
            message = 'Upload failed.';
            messageBgColor = 'red';
            messageForeColor = 'white';
        }
        
        PageReference autoClose = Page.SystemAutoClose;
        Map<string, string> parms = autoClose.getParameters();
        
        parms.put('autoclose', 'false');
        parms.put('msg', message);
        parms.put('bgcolor', messageBgColor);
        parms.put('forecolor', messageForeColor);
        
        return autoClose;

        // redirect to the destination url
        //PageReference destPR;
        //if (String.isNotBlank(destParam)) {
        //  destPR = new PageReference(EncodingUtil.urlDecode(destParam,'UTF-8'));
        //}
        //else {
        //  destPR = new PageReference('/');
        //}
        
        //return destPR.setRedirect(true);
        //return Page.SystemAutoClose; // [DP] 10.27.14 returning to auto-close page
    }
    
    // HELPER METHODS
    
    public void processSuccess() {
        // if the return page is loaded before the upload is processed by spring CM, then mark the status Upload Pending
        Family_Document__c familyDocument = null;
        
        if (String.isNotBlank(docIdParam)) {
            for (Family_Document__c fd : [SELECT Id, Document_Status__c 
                                                FROM Family_Document__c 
                                                WHERE Id = :docIdParam 
                                                AND Document_Status__c = 'Not Received'
                                                AND Request_to_Waive_Requirement__c != 'Yes'
                                                LIMIT 1
                                                FOR UPDATE  // Lock the record to avoid a race condition with the Spring CM update logic
                                                ]) {
                                                // Removed these criteria so we can handle docs that are being Replaced
                                                //AND Import_Id__c = null
                                                //AND Filename__c = null

                familyDocument = fd;
            }
            
            if (familyDocument != null) {
                try {
                    if (familyDocument.Document_Status__c == 'Not Received'){
                        familyDocument.Document_Status__c = 'Upload Pending';
                        update familyDocument;
                    }
                }
                catch (DmlException e) {
                    String errmsg = 'ERROR: Unable to update family document ' + familyDocument.Id + ': ' + e.getMessage() + ' ' + e.getStackTraceString();
                    System.debug(LoggingLevel.Error, errmsg);
                    insert new Custom_Debug__c(Message__c = errmsg);
                }
            }
        }
    }
    
    public void processCancel() {
        if (String.isNotBlank(docIdParam)) {
            Family_Document__c familyDocument = null;
            
            // retrieve and delete the family document, but only if it hasn't been received
            for (Family_Document__c fd : [SELECT Id, (select Id from School_Document_Assignments__r where Required_Document__c = null and PFS_Specific__c != true)
                                            FROM Family_Document__c 
                                            WHERE Id = :docIdParam 
                                            AND Document_Status__c = 'Not Received'
                                            AND Import_Id__c = null
                                            AND Filename__c = null
                                            AND Request_to_Waive_Requirement__c != 'Yes'
                                            LIMIT 1]) {
                familyDocument = fd;
            }
                        
            // delete the family document record
            if (familyDocument != null) {
                
                // delete the family document SDA records that are from an Other/Misc Document upload
                if (familyDocument.School_Document_Assignments__r != null) {
                    try {
                        delete familyDocument.School_Document_Assignments__r;
                    }
                    catch (DmlException e) {
                        System.debug(LoggingLevel.Error, 'ERROR: Unable to delete School Document Assignments ' + familyDocument.Id + ': ' + e.getMessage() + ' ' + e.getStackTraceString());
                    }
                }
                
                try {
                    delete familyDocument;
                }
                catch (DmlException e) {
                    System.debug(LoggingLevel.Error, 'ERROR: Unable to delete family document ' + familyDocument.Id + ': ' + e.getMessage() + ' ' + e.getStackTraceString());
                }
            }
        }
    }
    
    
}