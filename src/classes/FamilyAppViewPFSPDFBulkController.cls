public with sharing class FamilyAppViewPFSPDFBulkController {

    private static final String STRING_YES = 'Yes';
    public Map<Id, List<School_PFS_Assignment__c>> bulkApplicationSchoolAssignmentMap {get; private set;}
    public List<PFS__c> pfsList {get; private set;}
    public Boolean renderPDF { get; private set; }
    public Set<Id> pfsIds { get; private set; }
    private static final Integer MAX_PDF_COUNT = 50; // [bc] - set max # of PDFs to 100, but limit could be set up to 300 PDFs (if necessary)
    public MIEWrapper MIE { get; private set; }
    public BusinessWrapper Business  { get; private set; }
    private  Boolean isSingleRecord;
    public Map<String, Academic_Year__c> mapAcademicYears;
    public Map<String, Annual_Setting__c> annualSettingBySchoolAndAcademicYear;
    
    /****** Business Wrapper *********/
    public class BusinessWrapper {
        
        public Map<Id, List<Business_farm__c>> businessFarmMap { get; private set; }
        public Map<Id,List<FamilyAppPrintPFSPDFController.BizFarmGroupWrapper>> groupedBizFarmMap { get; private set; }
        
        public BusinessWrapper() {
            
            businessFarmMap = new Map<Id, List<Business_farm__c>>();
            groupedBizFarmMap = new Map<Id,List<FamilyAppPrintPFSPDFController.BizFarmGroupWrapper>>();
        }
        
        public void add(Id pfsId){
            if( !businessFarmMap.containsKey(pfsId) ) {
                businessFarmMap.put(pfsId, new List<Business_farm__c>() );
            }
        }
        
        public void run() {
            List<String> fieldNames = new List<String>(Schema.SObjectType.Business_Farm__c.fields.getMap().keySet());
            Set<Id> pfsIds = businessFarmMap.keyset();
            List<Business_farm__c> farms;
            WithoutSharingQueryClass withoutSharingQueryHandler = new WithoutSharingQueryClass();
            
            for(Business_farm__c f : withoutSharingQueryHandler.queryBusinessFarm(fieldNames, pfsIds) ) {
                                             
                farms = businessFarmMap.containsKey(f.PFS__c) ? businessFarmMap.get(f.PFS__c) : new List<Business_farm__c>();
                farms.add(f);
                businessFarmMap.put(f.PFS__c, farms);
            }
            
            getBizOrFarmGroupWrappersMap();
        }
        
        private void getBizOrFarmGroupWrappersMap(){
            
            List<FamilyAppPrintPFSPDFController.BizFarmGroupWrapper> groupedBizFarmList;
            List<Business_Farm__c> bFarmList;
            Integer i;
            
            for(ID id : businessFarmMap.keySet()) {
                
                i=0;
                bFarmList = businessFarmMap.get(id);
                groupedBizFarmList = new List<FamilyAppPrintPFSPDFController.BizFarmGroupWrapper>();
                groupedBizFarmList.add(getRelevantBizFarmList(new Map<String, String>{'Business_or_Farm__c' => 'Business', 'Business_Entity_Type__c' => 'Sole Proprietorship'},bFarmList , 'Businesses - Sole Proprietorship', null, i++));
                groupedBizFarmList.add(getRelevantBizFarmList(new Map<String, String>{'Business_or_Farm__c' => 'Business', 'Business_Entity_Type__c' => 'Partnership'},bFarmList , 'Businesses - Partnership', null, i++));
                groupedBizFarmList.add(getRelevantBizFarmList(new Map<String, String>{'Business_or_Farm__c' => 'Business', 'Business_Entity_Type__c' => 'Corporation'},bFarmList , 'Business - Corporation', null, i++));
                groupedBizFarmList.add(getRelevantBizFarmList(new Map<String, String>{'Business_or_Farm__c' => 'Farm', 'Business_Entity_Type__c' => 'Sole Proprietorship'},bFarmList, 'Farms - Sole Proprietorship', null, i++));
                groupedBizFarmList.add(getRelevantBizFarmList(new Map<String, String>{'Business_or_Farm__c' => 'Farm', 'Business_Entity_Type__c' => 'Partnership'},bFarmList , 'Farms - Partnership', null, i++));
                groupedBizFarmList.add(getRelevantBizFarmList(new Map<String, String>{'Business_or_Farm__c' => 'Farm', 'Business_Entity_Type__c' => 'Corporation'},bFarmList, 'Farm - Corporation', null, i++));
                
                groupedBizFarmMap.put(id, groupedBizFarmList);
            }
        }
        
        private FamilyAppPrintPFSPDFController.BizFarmGroupWrapper getRelevantBizFarmList(Map<String, String> fieldNameToFieldValue, 
                                                           List<Business_Farm__c> bFarmList, 
                                                           String header1, 
                                                           String header2, 
                                                           Integer iParam) {
            
            List<Business_Farm__c> localList = new List<Business_Farm__c>();
            
            if(bFarmList != null) {
                
                for (Business_Farm__c bf :bFarmList) {
                    Boolean addBizFarm = true;
                    
                    for (String fieldName : fieldNameToFieldValue.keySet()) {
                        
                        if (bf.get(fieldName) != fieldNameToFieldValue.get(fieldName)) {
                            addBizFarm = false;
                        }
                    }
                    if (addBizFarm){
                        localList.add(bf);
                    }
        
                }
            }
    
            return new FamilyAppPrintPFSPDFController.BizFarmGroupWrapper(localList, header1, header2, iParam);
        }
    }//End-BusinessWrapper
    
    /****** MIE Wrapper *********/
    public class MIEWrapper {
        
        private MonthlyIncomeAndExpensesService.MIEResponseBulk response { get; set; }
        
        public MIEWrapper(Set<Id> folderIds) {
            
            MonthlyIncomeAndExpensesService.MIERequestBulk request = 
                new MonthlyIncomeAndExpensesService.MIERequestBulk(folderIds);
            
            response = new MonthlyIncomeAndExpensesService.MIEResponseBulk(request, false);
        }
        
        /**
        * @description Returns true if the MIE was requested for the given PFS. 
        *              This method can be used to show/hide MIE menu.
        * @param pfs The PFS record for which we want to determine if MIE was already requested.
        * @return Returns true if the MIE was requested for the given PFS. Otherwise, false.
        */
        public Boolean isMIEActive(PFS__c pfs) {
            
            return response.pfsAlreadyCompletedIds.contains(pfs.Id) || response.pfsRequestedAndIncompletedIds.contains(pfs.Id);
        }
    }//End-MIEWrapper
    
    private void retrieveAcademicYears() {
        if(mapAcademicYears == null) {
            mapAcademicYears = new Map<String, Academic_Year__c>();
            for(Academic_Year__c a : GlobalVariables.getAllAcademicYears()) {
                mapAcademicYears.put(a.Name, a);
            }
        }
    }
    
    public FamilyAppViewPFSPDFBulkController(ApexPages.StandardSetController controller){

        if (!hasItemsSelected(controller)) { return; }
        
        isSingleRecord = false;
        Set<Id> folderIds = new Set<Id>();
        pfsIds = new Set<Id>();
        renderPDF = false;
        Business = new BusinessWrapper();
        bulkApplicationSchoolAssignmentMap = new Map<Id, List<School_PFS_Assignment__c>>();
        Set<String> academicYears = new Set<String>();
        Set<Id> schoolIds = new Set<Id>();
        
        for ( Student_Folder__c f : (Student_Folder__c[])controller.getSelected() ){
            
            folderIds.add(f.Id);
        }
        
        //MIE Logic
        MIE = new MIEWrapper(folderIds);
        
        Set<Id> folderIdsForMIE = new Set<Id>();
        set<Id> spaAlreadyQueried = new set<Id>();
        WithoutSharingQueryClass withoutSharingQueryHandler = new WithoutSharingQueryClass();
        for (School_PFS_Assignment__c spa : withoutSharingQueryHandler.queryByFolderIds(folderIds)) {
            
            retrieveSchoolAssignments(spa);
            pfsIds.add(spa.Applicant__r.PFS__c);
            Business.add(spa.Applicant__r.PFS__c);
            spaAlreadyQueried.add(spa.Id);
            
            //Get key to query annualSetting records.
            schoolIds.add(spa.School__c);
            academicYears.add(spa.Academic_Year_Picklist__c);
        }
        
        
        retrieveAnnualSettings(schoolIds, academicYears);
        retrieveRelatedSchoolAssignments(spaAlreadyQueried);
        initializer();
    }
    
    private Boolean hasItemsSelected(ApexPages.StandardSetController controller) {
        try {
            //Since the "ids" parameter only contains the id of the first folder selected.
            //When there are no selected folders from advanced list the "ids" parameter is "ids=6".
            //So we need to verify if the "ids" parameter is a valid Id. Otherwise, We'll show a message to the user.
            String ids = ApexPages.currentPage().getParameters().get('ids');
            Id fid = Id.ValueOf(ids);
        } catch(Exception e) {
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Print_PFS_Must_Select_Records));
            
            return false;
        }
        return true;
    }
    
    private void retrieveAnnualSettings(Set<Id> schoolIds, Set<String> academicYears) {
        
        annualSettingBySchoolAndAcademicYear = new Map<String, Annual_Setting__c>();
        Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Annual_Setting__c.fields.getMap();
        List<String> fieldsToquery = new List<String>();
        fieldsToquery.addAll(fieldMap.keySet());
        
        for (Annual_Setting__c asTemp : AnnualSettingsSelector.Instance
            .selectBySchoolAndAcademicYear(schoolIds, academicYears, fieldsToquery)) {
                
                annualSettingBySchoolAndAcademicYear.put(
                    asTemp.School__c + ':' + asTemp.Academic_Year_Name__c, asTemp);
        }
    }
    
    private void retrieveRelatedSchoolAssignments(set<Id> spaAlreadyQueried) {
        
        //This method retrieve all the missing spas for "Subscriber Schools Selected" section.
        List<School_PFS_Assignment__c> tmpSpas = new List<School_PFS_Assignment__c>();
        WithoutSharingQueryClass withoutSharingQueryHandler = new WithoutSharingQueryClass();
        
        for (School_PFS_Assignment__c spa : withoutSharingQueryHandler.queryByPfsIds(pfsIds, spaAlreadyQueried)) {

            tmpSpas = bulkApplicationSchoolAssignmentMap.containsKey(spa.Applicant__c)
                ? bulkApplicationSchoolAssignmentMap.get(spa.Applicant__c) : new List<School_PFS_Assignment__c>();
            tmpSpas.add(spa);
            bulkApplicationSchoolAssignmentMap.put(spa.Applicant__c, tmpSpas);
        }
    }
    
    public FamilyAppViewPFSPDFBulkController() {
        
        Id pfsId = Id.valueOf(ApexPages.currentPage().getParameters().get('id'));
        isSingleRecord = true;
        pfsIds = new Set<Id>();
        renderPDF = false;
        Business = new BusinessWrapper();
        bulkApplicationSchoolAssignmentMap = new Map<Id, List<School_PFS_Assignment__c>>();
        WithoutSharingQueryClass withoutSharingQueryHandler = new WithoutSharingQueryClass();
        
        pfsIds.add(pfsId);
        Business.add(pfsId);//To avoid exception when the only spa is removed and Own_Business_or_Farm__c == 'Yes'
        
        Set<Id> folderIds = new Set<Id>();
        for (School_PFS_Assignment__c spa : withoutSharingQueryHandler.queryByPfsId(pfsId)){
            
            retrieveSchoolAssignments(spa);
            pfsIds.add(spa.Applicant__r.PFS__c);
            folderIds.add(spa.Student_Folder__c);
        }
        
        //MIE Login
        MIE = new MIEWrapper(folderIds);
        
        initializer();
    }
    
    private void retrieveSchoolAssignments(School_PFS_Assignment__c spa) {
        
        List<School_PFS_Assignment__c> tmpSpas = new List<School_PFS_Assignment__c>();
            
        tmpSpas = bulkApplicationSchoolAssignmentMap.containsKey(spa.Applicant__c)
            ? bulkApplicationSchoolAssignmentMap.get(spa.Applicant__c) : new List<School_PFS_Assignment__c>();
        tmpSpas.add(spa);
        bulkApplicationSchoolAssignmentMap.put(spa.Applicant__c, tmpSpas);
    }
    
    public void initializer(){
        
        retrieveAcademicYears();
        Business.run();
        
        if ( pfsIds.size() > MAX_PDF_COUNT){
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You may only print ' + MAX_PDF_COUNT + ' PFSs at a time, but selected ' + pfsIds.size() + ' PFSs.'));
        } else {
            if( isSingleRecord ) {
                List<Id> ids = new List<Id>{};
                ids.addAll(pfsIds);
                FamilyAppViewPFSPDFController pfsHandler = new FamilyAppViewPFSPDFController(ids[0]);
                pfsList = new List<PFS__c>{pfsHandler.pfs};
            } else {
                queryPFSList();
            }
        }        
    }
    
    public FamilyAppViewPFSPDFBulkController getFamilyAppViewPFSPDFBulkController() { return this; }
    
    public void queryPFSList(){
        
        String queryString = ApplicationUtils.writeQueryString('PFSView');
        queryString += ' FROM PFS__c where Id in :pfsIds order by Parent_A__r.LastName';
        
        try {
            this.sortQueryByStudentLastName(Database.query(queryString));
            renderPDF = true;
        } catch (Exception ex) {
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
    }
    
    private void sortQueryByStudentLastName(list<PFS__c> pfss)
    {       
        map<Id,PFS__c> allPfsById = new map<Id,PFS__c>();
        map<String, set<Id>> allPfsByIdByStudentLastName = new map<String, set<Id>>();
        set<Id> tmpListId;
        list<PFS__c> pfsWithoutStudents = new list<PFS__c>();
        
        for(PFS__c iPfs:pfss)
        {
            allPfsById.put(iPfs.Id, iPfs);
            if(iPfs.Applicants__r!=null && iPfs.Applicants__r.size()>0)
            {
                if(allPfsByIdByStudentLastName.containsKey(iPfs.Applicants__r[0].Last_Name__c))
                {
                    tmpListId=allPfsByIdByStudentLastName.get(iPfs.Applicants__r[0].Last_Name__c);
                }else{
                    tmpListId = new set<Id>();
                }
                tmpListId.add(iPfs.Id);
                allPfsByIdByStudentLastName.put(iPfs.Applicants__r[0].Last_Name__c, tmpListId);
            }else{
                pfsWithoutStudents.add(iPfs);
            }
        }
        
        list<String> studentsLastNames = new list<String>();
        studentsLastNames.addAll(allPfsByIdByStudentLastName.keySet());
        studentsLastNames.sort();
        pfsList = new list<PFS__c>();
        for(String lastName:studentsLastNames)
        {
            tmpListId = allPfsByIdByStudentLastName.get(lastName);
            for(Id pfsId:tmpListId)
            {
                pfsList.add(allPfsById.get(pfsId));
            }
        }
        if(pfsWithoutStudents.size()>0)pfsList.addAll(pfsWithoutStudents);
    }//End:sortQueryByStudentLastName
    
    public without sharing class WithoutSharingQueryClass{
        
        public WithoutSharingQueryClass() {}
        
        public List<School_PFS_Assignment__c> queryByFolderIds(Set<Id> folderIds) {
            
            return new List<School_PFS_Assignment__c>([
                SELECT Id, Student_Folder__c, Academic_Year_Name__c, Withdrawn__c, 
                    School__r.Name, Applicant__c, Applicant__r.PFS__C, School__c, SSS_Subscriber_Status__c,
                    School__r.School_Type__c, School__r.SSS_School_Code__c, Academic_Year_Picklist__c 
                FROM School_PFS_Assignment__c 
                WHERE Student_Folder__c IN :folderIds 
                    AND Withdrawn__c != :STRING_YES]);
        }
        
        public List<School_PFS_Assignment__c> queryByPfsIds(Set<Id> pfsIds, set<Id> spaAlreadyQueried) {
            
            return new List<School_PFS_Assignment__c>([
                SELECT Id, Student_Folder__c, Academic_Year_Name__c, Withdrawn__c, 
                    School__r.Name, Applicant__c, Applicant__r.PFS__C, School__c, SSS_Subscriber_Status__c,
                    School__r.School_Type__c, School__r.SSS_School_Code__c 
                FROM School_PFS_Assignment__c 
                WHERE Applicant__r.PFS__c IN :pfsIds 
                    AND Id NOT IN:spaAlreadyQueried 
                    AND Withdrawn__c != :STRING_YES]);
        }
        
        public List<School_PFS_Assignment__c> queryByPfsId(Id pfsId) {
            
            return new List<School_PFS_Assignment__c>([
                SELECT Id, Student_Folder__c, Academic_Year_Name__c, Withdrawn__c, School__r.Name, Applicant__c,
                    Applicant__r.PFS__C, School__c, SSS_Subscriber_Status__c, School__r.School_Type__c,
                    School__r.SSS_School_Code__c, Applicant__r.Suffix__c 
                FROM School_PFS_Assignment__c 
                WHERE Applicant__r.PFS__c = :pfsId
                    AND Withdrawn__c != :STRING_YES]);
        }
        
        public List<Annual_Setting__c> queryAnnualSettingByAcademicYear(List<String> academicYears, List<Id> schoolIds) {
            
            return new List<Annual_Setting__c>([
                SELECT Id, Collect_MIE__c, School__c, Academic_Year__r.Name 
                FROM Annual_Setting__c
                WHERE School__c IN: schoolIds 
                    AND Academic_Year__r.Name IN: academicYears 
                    AND Collect_MIE__c = :STRING_YES 
                  LIMIT 1]);
        }
        
        public List<Business_farm__c> queryBusinessFarm(List<String> fieldNames, Set<Id> pfsIds) {
            
            return Database.query('SELECT '+ String.Join(fieldNames, ',')
                                         + ' FROM Business_farm__c '
                                         + ' WHERE PFS__c in :pfsIds '
                                         + ' ORDER BY createdDate ASC');
        }
    }

    public List<String> applicantNames {
        get{
            if( applicantNames == null && pfsList != null) {
                
                applicantNames = new List<String>();
                String names;
                for (PFS__c ipfs : pfsList) {
                    
                    names = '';
                    for (Applicant__c applicant : ipfs.Applicants__r){
                        names += (names != '' ? ', ' : '') + applicant.First_Name__c + ' ' + applicant.Last_Name__c;
                    }                   
                    applicantnames.add(names);
                }
            }
            return applicantNames;
        }
        private set;
    }
}