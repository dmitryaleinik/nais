/**
* @description Class implemented to change the Contact's account to a new
* one created specially for each family portal contact. To execute the:
* ID batchprocessid = Database.executeBatch(new batchUpdateAccountModel());
*/
global class batchUpdateAccountModel implements Database.Batchable<sObject>
{
    global final String Query;
    
    global batchUpdateAccountModel()
    {
        Query='Select Id, FirstName, LastName, AccountId, Household__c, RecordTypeId from Contact where '+
                            '(RecordTypeId=\''+RecordTypes.studentContactTypeId+'\' OR RecordTypeId=\''+RecordTypes.parentContactTypeId+'\') '+
                            'and Account.RecordTypeId=\''+RecordTypes.individualAccountTypeId+'\' '+
                            'and Account.Name = \'Individual\'';   
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(Query);
    }
    
    global void finish(Database.BatchableContext BC)
    {
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> batch)
    {
        List<Contact> contacts = (List<Contact>)batch;
        ContactAction.isFirstRun = true;
        ContactAction.createBucketAccountForFamilyContact(contacts, true);
        update contacts;
    }
}