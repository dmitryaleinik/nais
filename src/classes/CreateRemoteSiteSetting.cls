public with sharing class CreateRemoteSiteSetting {
    
    public static void createRemoteSiteSettings(String fullName, String url){   
        RemoteSiteSetting_MetadataService.MetadataPort service = createService();
        RemoteSiteSetting_MetadataService.RemoteSiteSetting remoteSiteSettings = new RemoteSiteSetting_MetadataService.RemoteSiteSetting();
        remoteSiteSettings.fullName = fullName;
        remoteSiteSettings.url = url;
        remoteSiteSettings.description = '';
        remoteSiteSettings.isActive=true;
        remoteSiteSettings.disableProtocolSecurity=false;
        RemoteSiteSetting_MetadataService.AsyncResult[] results = service.create(new List<RemoteSiteSetting_MetadataService.Metadata> { remoteSiteSettings });
        
        if(!Test.isRunningTest()) {
            RemoteSiteSetting_MetadataService.AsyncResult[] checkResults = service.checkStatus(new List<string> {string.ValueOf(results[0].Id)});
        } else {
            
            RemoteSiteSetting_MetadataService.AsyncResult[] checkResults = service.checkStatus(null);
            
            RemoteSiteSetting_MetadataService.CreateResponse_Element cRespElement = new RemoteSiteSetting_MetadataService.CreateResponse_Element();
            RemoteSiteSetting_MetadataService.AsyncResult asy = new RemoteSiteSetting_MetadataService.AsyncResult();
            RemoteSiteSetting_MetadataService.CheckStatus_Element statusEle = new RemoteSiteSetting_MetadataService.CheckStatus_Element();
            RemoteSiteSetting_MetadataService.CheckStatusResponse_Element respElement = new RemoteSiteSetting_MetadataService.CheckStatusResponse_Element();
            RemoteSiteSetting_MetadataService.PackageTypeMembers pkgMember = new RemoteSiteSetting_MetadataService.PackageTypeMembers();
            RemoteSiteSetting_MetadataService.DebuggingHeader_Element headerElement = new RemoteSiteSetting_MetadataService.DebuggingHeader_element();
            RemoteSiteSetting_MetadataService.LogInfo logInfo = new RemoteSiteSetting_MetadataService.LogInfo();
            RemoteSiteSetting_MetadataService.Metadata mData = new RemoteSiteSetting_MetadataService.Metadata();
            RemoteSiteSetting_MetadataService.SessionHeader_Element sElement = new RemoteSiteSetting_MetadataService.SessionHeader_Element();
            RemoteSiteSetting_MetadataService.RetrieveResponse_Element retrieveElement = new RemoteSiteSetting_MetadataService.RetrieveResponse_Element();
            RemoteSiteSetting_MetadataService.DebuggingInfo_Element debugElement = new RemoteSiteSetting_MetadataService.DebuggingInfo_Element();
            RemoteSiteSetting_MetadataService.CallOptions_Element calloptionElement = new RemoteSiteSetting_MetadataService.CallOptions_Element();
            RemoteSiteSetting_MetadataService.CheckDeployStatus_Element deploElement = new RemoteSiteSetting_MetadataService.CheckDeployStatus_Element();
            RemoteSiteSetting_MetadataService.Create_Element createElement = new RemoteSiteSetting_MetadataService.Create_Element();
        }
    }
    
    public static RemoteSiteSetting_MetadataService.MetadataPort createService(){
        RemoteSiteSetting_MetadataService.MetadataPort service = new RemoteSiteSetting_MetadataService.MetadataPort();
        service.SessionHeader = new RemoteSiteSetting_MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = UserInfo.getSessionId();
        return service;
    }
}