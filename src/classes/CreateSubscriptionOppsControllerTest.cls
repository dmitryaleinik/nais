/**
* @CreateSubscriptionOppsControllerTest
* @date 3/1/2014
* @author  CH for Exponent Partners
* @description  Functional tests for the CreateSubscriptionOppsController 
*/
@isTest
private class CreateSubscriptionOppsControllerTest
{

    // Functional test for the Auto-creation page
    @isTest
    private static void testLoadPageAndSave() {
        Academic_Year__c newAcademicYear = new Academic_Year__c();
        //newAcademicYear.Name = '2014-2015';
        newAcademicYear.Name = GlobalVariables.getCurrentYearString();
        insert newAcademicYear;

        Transaction_Settings__c setting = new Transaction_Settings__c();
        setting.Name = 'School Subscription';
        setting.Account_Code__c = '5100';
        setting.Transaction_Code__c = '1210';
        setting.Account_Label__c = 'Subscription Revenue';
        insert setting;
        
        Transaction_Annual_Settings__c transAnnualSetting = new Transaction_Annual_Settings__c();
        transAnnualSetting.Product_Amount__c = 200.00;
        transAnnualSetting.Product_Code__c = 'Test Product Code';
        transAnnualSetting.Year__c = String.valueOf(Date.today().addYears(1).year());
        transAnnualSetting.Name = 'Sample Name';
        transAnnualSetting.Subscription_Type__c = 'Full';
        transAnnualSetting.Year__c = newAcademicYear.Name.left(4);
        insert transAnnualSetting;
        
        // Create Accounts
        List<Account> accountsToInsert = new List<Account>{};
        for(Integer i=0; i<10; i++){
            Account newAccount = TestUtils.createAccount('Test' + i, RecordTypes.accessOrgAccountTypeId, 1, false);
            newAccount.SSS_Subscription_Type_Current__c = 'Full';
            accountsToInsert.add(newAccount);
        }
        insert accountsToInsert;
        
        // Create Subscriptions
        List<Subscription__c> subsToInsert = new List<Subscription__c>{};
        Integer currentYear = Date.today().year();
        Date startDate = Date.newInstance(currentYear, 1, 1);
        Date endDate = Date.newInstance(currentYear, 12, 31);
        // Add some subscriptions expiring this year
        for(Integer i=0; i<5; i++){
            subsToInsert.add(TestUtils.createSubscription(accountsToInsert[i].Id, startDate, endDate, 'No', 'Full', false));
        }
        // Add some subscriptions expiring next year
        for(Integer i=5; i<10; i++){
            subsToInsert.add(TestUtils.createSubscription(accountsToInsert[i].Id, startDate.addYears(1), endDate.addYears(1), 'No', 'Full', false));
        }
        insert subsToInsert;
        
        // Create an existing Opportunity
        Opportunity existingOpp = TestUtils.createOpportunity('Test Opp', RecordTypes.opportunitySubscriptionFeeTypeId, null, null, false);
        existingOpp.AccountId = accountsToInsert[0].Id;
        existingOpp.CloseDate = startDate.addDays(14);
        existingOpp.StageName = 'Prospecting';
        existingOpp.Subscription_Type__c = 'Full';
        existingOpp.Academic_Year_Picklist__c = newAcademicYear.Name;
        insert existingOpp;
        
        Test.startTest();
            CreateSubscriptionOppsController controller = new CreateSubscriptionOppsController();
            System.assertEquals(5, controller.numExpirations);
            System.assertEquals(4, controller.numMissing);
            
            controller.createSubscriptions();
            
            // Check to see if the right number of Opportunites were created
            //  There should be 4 plus the existing one
            List<Opportunity> oppCheckList = [select Id, RecordTypeId, Subscription_Type__c, 
                                                Name, PFS__c, StageName, Amount, Parent_A__c, Academic_Year_Picklist__c, 
                                                PFS_Number__c, Paid_Status__c, Net_Amount_Due__c, SSS_School_Code__c,
                                                RecordType.Id, RecordType.Name, Total_Purchased_Waivers__c, 
                                                Product_Quantity__c,
                                                
                                                // PFS Fields
                                                PFS__r.Id, PFS__r.Parent_A_First_Name__c, PFS__r.Parent_A_Last_Name__c,
                                                PFS__r.Parent_A_Address__c, PFS__r.Parent_A_City__c, PFS__r.Parent_A_State__c,
                                                PFS__r.Parent_A_ZIP_Postal_Code__c, PFS__r.Parent_A_Country__c, PFS__r.Parent_A_Email__c,
                                                PFS__r.PFS_Number__c

            from Opportunity];
            System.assertEquals(5, oppCheckList.size());
            for(Opportunity oppRecord : oppCheckList){
                System.assertEquals(RecordTypes.opportunitySubscriptionFeeTypeId, oppRecord.RecordTypeId);
                System.assertEquals('Full', oppRecord.Subscription_Type__c);
            }
            
            // [DP] 07.29.2015 NAIS-2469 now TLIs are only created if explicitly called
            PaymentUtils.insertTransactionLineItemsFromOpportunities(oppCheckList);

            List<Transaction_Line_Item__c> transCheckList = [select Id, Transaction_Date__c, Transaction_Code__c, Account_Code__c, Account_Label__c, Product_Amount__c, Product_Code__c 
                                                                from Transaction_Line_Item__c];
            System.assertEquals(5, transCheckList.size());
            for(Transaction_Line_Item__c transRecord : transCheckList){
                System.assertEquals(Date.Today(), transRecord.Transaction_Date__c);
                System.assertEquals(setting.Transaction_Code__c, transRecord.Transaction_Code__c);
                System.assertEquals(setting.Account_Code__c, transRecord.Account_Code__c);
                System.assertEquals(setting.Account_Label__c, transRecord.Account_Label__c);
                
               // System.assertEquals(String.valueOf(transAnnualSetting.Product_Amount__c), transRecord.Product_Amount__c);
                //System.assertEquals(transAnnualSetting.Product_Code__c, transRecord.Product_Code__c);
            }
        Test.stopTest();
    }
}