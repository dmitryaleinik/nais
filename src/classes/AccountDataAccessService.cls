/**
 * AccountDataAccessService.cls
 *
 * @description Data access service class for Account (School)
 *
 * @author Chase Logan @ Presence PG
 */
public class AccountDataAccessService extends DataAccessService {
    
    /**
     * @description: Queries an Account record by ID, queries all fields by default because of the direct query of
     * a singe record by ID limited to 1
     *
     * @param acctId - The ID of the Account record
     *
     * @return An Account record
     */
    public Account getAccountById( Id acctId) {
        Account returnVal;

        if ( acctId != null) {

            try {

                returnVal = Database.query( 'select ' + String.join( super.getSObjectFieldNames( 'Account'), ',') +
                                             ' from Account' + 
                                            ' where Id = :acctId' + 
                                            ' limit 1');
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in AccountDataAccessService.getAccountById' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }

        return returnVal;
    }

    public List<Account> getAccountBySchool(Set<String> schoolIds, String schoolIdMap) {
        List<Account> returnVal;

        try {

            returnVal = Database.query( 'select ' + schoolIdMap + String.join( super.getSObjectFieldNames( 'Account'), ',') +
                                         ' from Account' + 
                                        ' where ' + schoolIdMap + ' in :schoolIds' + 
                                        ' and SSS_Subscriber_Status__c = \'Current\' ');
        } catch ( Exception e) {

            // log it and bubble to caller for handling
            System.debug( 'DEBUG:::exception in AccountDataAccessService.getAccountBySchool' +
                e.getMessage());
            throw new DataAccessServiceException( e);
        }

        return returnVal;
    }
}