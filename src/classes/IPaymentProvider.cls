/**
 * @description The Payment Provider interface, defining standard
 *              operations for payment processing.
 **/
public interface IPaymentProvider {
    /**
     * @description Prepare the payment for processing. We are
     *              unable to do web callouts after DML has been
     *              done, so we are separating DML into this
     *              method and the web callout into the process
     *              method.
     * @param request The request that contains the Payment or
     *        refund to process.
     */
    PaymentResult processBefore(PaymentProcessorRequest request);

    /**
     * @description Process the actual transaction and parse
     *              any success or failure responses provided
     *              back from the result.
     * @param request The request that contains the Payment or
     *        refund to process.
     */
    PaymentResult process(PaymentProcessorRequest request);

    /**
     * @description Process the payment data after a payment has
     *              been made. In most cases this entails the
     *              generation of Transaction_Line_Items.
     * @param objectToProcess A generic object that will need to
     *        be processed. We do not specify the type of object
     *        because we are allowing future Payment Providers to
     *        call this method from a trigger, requiring various
     *        SObjects to be accepted.
     */
    PaymentResult processAfter(Object objectToProcess);

    /**
     * @description Gather the Billing Countries to provide to
     *              the payment form for the drop down select list.
     */
    List<SelectOption> getBillingCountrySelectOptions();

    /**
     * @description
     */
    List<SelectOption> getBillingStateUSSelectOptions();

    /**
     * @description
     */
    List<SelectOption> getExpireMonthSelectOptions();

    /**
     * @description
     */
    List<SelectOption> getExpireYearSelectOptions();

    /**
     * @description
     */
    List<SelectOption> getAccountTypeSelectOptions();

    /**
     * @description
     */
    String getCountryCode(String countryName);

    /**
     * @description
     */
    String getDefaultCountryCode();

    /**
     * @description
     */
    String getCountryName(String countryCode);

    /**
     * @description
     */
    String getAccountTypeLabel(String accountTypeValue);

    /**
     * @description Determines if the error code returned from the transaction is an
     *              Internal error or not.
     */
    Boolean isInternalError(Integer transactionResultCode);

    /**
     * @description Determines if the error code returned from the transaction is a
     *              Data error or not.
     */
    Boolean isDataError(Integer transactionResultCode);

    /**
     * @description Determines if the error code returned from the transaction is a
     *              Request error or not.
     */
    Boolean isRequestError(Integer transactionResultCode);

    /**
     * @description Determines if the error code returned from the transaction is a
     *              Processing error or not.
     */
    Boolean isProcessingError(Integer transactionResultCode);

    /**
     * @description Determines if the error code returned from the transaction is a
     *              Decline error or not.
     */
    Boolean isDeclineError(Integer transactionResultCode);

    /**
     * @description Gets the page printable error message from the transaction.
     */
    String getErrorMessage(PaymentResult result);
}