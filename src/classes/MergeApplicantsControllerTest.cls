@isTest
private class MergeApplicantsControllerTest
{
    
    private static Academic_Year__c currentAcademicYear;
    private static Academic_Year__c previousAcademicYear;
    private static Account testIndivAccount;
    private static Account testSchool;

    private static void generateBaseRecords(){
        currentAcademicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), false);
        previousAcademicYear = TestUtils.createAcademicYear(GlobalVariables.getPreviousYearString(), false);
        Database.insert(new List<Academic_Year__c>{currentAcademicYear, previousAcademicYear});
        
        testIndivAccount = TestUtils.createAccount('Test Individual', RecordTypes.individualAccountTypeId, 1, false);
        testSchool = TestUtils.createAccount('Test School', RecordTypes.individualAccountTypeId, 1, false);
        Database.insert(new List<Account>{testIndivAccount, testSchool});
    }

    @isTest
    private static void testConditionA() {
        generateBaseRecords();    
        
        Contact studentContact1A = TestUtils.createContact('LastName', null, RecordTypes.studentContactTypeId, false);
        studentContact1A.FirstName = 'FirstName';
        Contact studentContact1B = TestUtils.createContact('LastName', null, RecordTypes.studentContactTypeId, false);
        studentContact1B.FirstName = 'FirstName';
        Database.insert(new List<Contact>{studentContact1A, studentContact1B});
        
        Student_Folder__c folder1A = TestUtils.createStudentFolder('FirstName LastName', currentAcademicYear.Id, studentContact1A.Id, false);
        folder1A.School__c = testSchool.Id;
        Student_Folder__c folder1B = TestUtils.createStudentFolder('FirstName LastName', currentAcademicYear.Id, studentContact1B.Id, false);
        folder1B.School__c = testSchool.Id;
        Database.insert(new List<Student_Folder__c>{folder1A, folder1B});
        
        MergeApplicantsController controller = new MergeApplicantsController();
        // Perform a search of the record structure
        controller.searchContact.FirstName = 'FirstName';
        controller.searchContact.LastName = 'LastName';
        controller.searchFolder.School__c = testSchool.Id;
        controller.searchForContacts();
        
        System.assertEquals(1, controller.searchResults.size());
        System.assertEquals(2, controller.searchResults[0].folderGroup.size());
        
        Test.startTest();
            controller.searchResults[0].folderGroup[0].folderSelected = true;
            controller.mergeSelected();
            
            List<Contact> checkContacts = [select Id from Contact];
            System.assertEquals(1, checkContacts.size());
            
            List<Student_Folder__c> checkFolders = [select Id from Student_Folder__c];
            System.assertEquals(1, checkFolders.size());
        Test.stopTest();
    }
    
    @isTest
    private static void testConditionB() {
        generateBaseRecords();
        
        Contact studentContact1A = TestUtils.createContact('LastName', null, RecordTypes.studentContactTypeId, false);
        studentContact1A.FirstName = 'FirstName';
        Database.insert(new List<Contact>{studentContact1A});
        
        Student_Folder__c folder1A = TestUtils.createStudentFolder('FirstName LastName', currentAcademicYear.Id, studentContact1A.Id, false);
        folder1A.School__c = testSchool.Id;
        Student_Folder__c folder1B = TestUtils.createStudentFolder('FirstName LastName', currentAcademicYear.Id, studentContact1A.Id, false);
        folder1B.School__c = testSchool.Id;
        Database.insert(new List<Student_Folder__c>{folder1A, folder1B});
        
        MergeApplicantsController controller = new MergeApplicantsController();
        // Perform a search of the record structure
        controller.searchContact.FirstName = 'FirstName';
        controller.searchContact.LastName = 'LastName';
        controller.searchFolder.School__c = testSchool.Id;
        controller.searchForContacts();
        
        System.assertEquals(1, controller.searchResults.size());
        System.assertEquals(2, controller.searchResults[0].folderGroup.size());
        
        Test.startTest();
            controller.searchResults[0].folderGroup[0].folderSelected = true;
            controller.mergeSelected();
            
            List<Contact> checkContacts = [select Id from Contact];
            System.assertEquals(1, checkContacts.size());
            
            List<Student_Folder__c> checkFolders = [select Id from Student_Folder__c];
            System.assertEquals(1, checkFolders.size());
        Test.stopTest();
    }
    
    @isTest
    private static void testConditionC() {
        generateBaseRecords();
        
        Contact studentContact1A = TestUtils.createContact('LastName', null, RecordTypes.studentContactTypeId, false);
        studentContact1A.FirstName = 'FirstName';
        Database.insert(new List<Contact>{studentContact1A});
        
        Student_Folder__c folder1A = TestUtils.createStudentFolder('FirstName LastName', currentAcademicYear.Id, studentContact1A.Id, false);
        folder1A.School__c = testSchool.Id;
        Student_Folder__c folder1B = TestUtils.createStudentFolder('FirstName LastName', previousAcademicYear.Id, studentContact1A.Id, false);
        folder1B.School__c = testSchool.Id;
        Database.insert(new List<Student_Folder__c>{folder1A, folder1B});
        
        MergeApplicantsController controller = new MergeApplicantsController();
        // Perform a search of the record structure
        controller.searchContact.FirstName = 'FirstName';
        controller.searchContact.LastName = 'LastName';
        controller.searchFolder.School__c = testSchool.Id;
        controller.searchForContacts();
        
        System.assertEquals(1, controller.searchResults.size());
        
        Test.startTest();
            controller.searchResults[0].folderGroup[0].folderSelected = true;
            controller.mergeSelected();
            
            List<Contact> checkContacts = [select Id from Contact];
            System.assertEquals(1, checkContacts.size());
            
            List<Student_Folder__c> checkFolders = [select Id from Student_Folder__c];
            System.assertEquals(2, checkFolders.size());
        Test.stopTest();
    }
    
    @isTest
    private static void testConditionD() {
        generateBaseRecords();
        
        Contact studentContact1A = TestUtils.createContact('LastName', null, RecordTypes.studentContactTypeId, false);
        studentContact1A.FirstName = 'FirstName';
        Contact studentContact1B = TestUtils.createContact('LastName', null, RecordTypes.studentContactTypeId, false);
        studentContact1B.FirstName = 'FirstName';
        Database.insert(new List<Contact>{studentContact1A, studentContact1B});
        
        Student_Folder__c folder1A = TestUtils.createStudentFolder('FirstName LastName', currentAcademicYear.Id, studentContact1A.Id, false);
        folder1A.School__c = testSchool.Id;
        Student_Folder__c folder1B = TestUtils.createStudentFolder('FirstName LastName', previousAcademicYear.Id, studentContact1B.Id, false);
        folder1B.School__c = testSchool.Id;
        Database.insert(new List<Student_Folder__c>{folder1A, folder1B});
        
        MergeApplicantsController controller = new MergeApplicantsController();
        // Perform a search of the record structure
        controller.searchContact.FirstName = 'FirstName';
        controller.searchContact.LastName = 'LastName';
        controller.searchFolder.School__c = testSchool.Id;
        controller.searchForContacts();
        
        System.assertEquals(1, controller.searchResults.size());
        System.assertEquals(2, controller.searchResults[0].folderGroup.size());
        
        Test.startTest();
            controller.searchResults[0].folderGroup[0].folderSelected = true;
            controller.mergeSelected();
            
            List<Contact> checkContacts = [select Id from Contact];
            System.assertEquals(1, checkContacts.size());
            
            List<Student_Folder__c> checkFolders = [select Id from Student_Folder__c];
            System.assertEquals(2, checkFolders.size());
        Test.stopTest();
    }
    
    @isTest
    private static void testConditionA2() {
        generateBaseRecords();    
        
        Contact studentContact1A = TestUtils.createContact('LastName', null, RecordTypes.studentContactTypeId, false);
        studentContact1A.FirstName = 'FirstName';
        Contact studentContact1B = TestUtils.createContact('LastName', null, RecordTypes.studentContactTypeId, false);
        studentContact1B.FirstName = 'FirstName';
        Database.insert(new List<Contact>{studentContact1A, studentContact1B});
        
        Student_Folder__c folder1A = TestUtils.createStudentFolder('FirstName LastName', currentAcademicYear.Id, studentContact1A.Id, false);
        folder1A.School__c = testSchool.Id;
        Student_Folder__c folder1B = TestUtils.createStudentFolder('FirstName LastName', currentAcademicYear.Id, studentContact1B.Id, false);
        folder1B.School__c = testSchool.Id;
        Database.insert(new List<Student_Folder__c>{folder1A, folder1B});
        
        MergeApplicantsController controller = new MergeApplicantsController();
        // Perform a search of the record structure
        controller.masterFolder.Student__c = studentContact1A.Id;
        controller.childFolder.Student__c = studentContact1B.Id;
        
        Test.startTest();
            controller.mergeContacts();
            
            List<Contact> checkContacts = [select Id from Contact];
            System.assertEquals(1, checkContacts.size());
            
            List<Student_Folder__c> checkFolders = [select Id from Student_Folder__c];
            System.assertEquals(1, checkFolders.size());
        Test.stopTest();
    }
    
    @isTest
    private static void testConditionB2() {
        generateBaseRecords();
        
        Contact studentContact1A = TestUtils.createContact('LastName', null, RecordTypes.studentContactTypeId, false);
        studentContact1A.FirstName = 'FirstName';
        Database.insert(new List<Contact>{studentContact1A});
        
        Student_Folder__c folder1A = TestUtils.createStudentFolder('FirstName LastName', currentAcademicYear.Id, studentContact1A.Id, false);
        folder1A.School__c = testSchool.Id;
        Student_Folder__c folder1B = TestUtils.createStudentFolder('FirstName LastName', currentAcademicYear.Id, studentContact1A.Id, false);
        folder1B.School__c = testSchool.Id;
        Database.insert(new List<Student_Folder__c>{folder1A, folder1B});
        
        MergeApplicantsController controller = new MergeApplicantsController();
        // Perform a search of the record structure
        controller.masterFolder.Student__c = studentContact1A.Id;
        controller.childFolder.Student__c = studentContact1A.Id;
        
        Test.startTest();
            controller.mergeContacts();
            
            List<Contact> checkContacts = [select Id from Contact];
            System.assertEquals(1, checkContacts.size());
            
            List<Student_Folder__c> checkFolders = [select Id from Student_Folder__c];
            System.assertEquals(1, checkFolders.size());
        Test.stopTest();
    }
    
    @isTest
    private static void testConditionC2() {
        generateBaseRecords();
        
        Contact studentContact1A = TestUtils.createContact('LastName', null, RecordTypes.studentContactTypeId, false);
        studentContact1A.FirstName = 'FirstName';
        Database.insert(new List<Contact>{studentContact1A});
        
        Student_Folder__c folder1A = TestUtils.createStudentFolder('FirstName LastName', currentAcademicYear.Id, studentContact1A.Id, false);
        folder1A.School__c = testSchool.Id;
        Student_Folder__c folder1B = TestUtils.createStudentFolder('FirstName LastName', previousAcademicYear.Id, studentContact1A.Id, false);
        folder1B.School__c = testSchool.Id;
        Database.insert(new List<Student_Folder__c>{folder1A, folder1B});
        
        MergeApplicantsController controller = new MergeApplicantsController();
        // Perform a search of the record structure
        controller.masterFolder.Student__c = studentContact1A.Id;
        controller.childFolder.Student__c = studentContact1A.Id;
        
        Test.startTest();
            controller.mergeContacts();
            
            List<Contact> checkContacts = [select Id from Contact];
            System.assertEquals(1, checkContacts.size());
            
            List<Student_Folder__c> checkFolders = [select Id from Student_Folder__c];
            System.assertEquals(2, checkFolders.size());
        Test.stopTest();
    }
    
    @isTest
    private static void testConditionD2() {
        generateBaseRecords();
        
        Contact studentContact1A = TestUtils.createContact('LastName', null, RecordTypes.studentContactTypeId, false);
        studentContact1A.FirstName = 'FirstName';
        Contact studentContact1B = TestUtils.createContact('LastName', null, RecordTypes.studentContactTypeId, false);
        studentContact1B.FirstName = 'FirstName';
        Database.insert(new List<Contact>{studentContact1A, studentContact1B});
        
        Student_Folder__c folder1A = TestUtils.createStudentFolder('FirstName LastName', currentAcademicYear.Id, studentContact1A.Id, false);
        folder1A.School__c = testSchool.Id;
        Student_Folder__c folder1B = TestUtils.createStudentFolder('FirstName LastName', previousAcademicYear.Id, studentContact1B.Id, false);
        folder1B.School__c = testSchool.Id;
        Database.insert(new List<Student_Folder__c>{folder1A, folder1B});
        
        MergeApplicantsController controller = new MergeApplicantsController();
        // Perform a search of the record structure
        controller.masterFolder.Student__c = studentContact1A.Id;
        controller.childFolder.Student__c = studentContact1B.Id;
        
        Test.startTest();
            controller.mergeContacts();
            
            List<Contact> checkContacts = [select Id from Contact];
            System.assertEquals(1, checkContacts.size());
            
            List<Student_Folder__c> checkFolders = [select Id from Student_Folder__c];
            System.assertEquals(2, checkFolders.size());
        Test.stopTest();
    }
}