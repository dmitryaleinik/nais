/*
 * NAIS-488 Payment Processing - Application Fee; Req# R-054, R-100, R-230, R-232, R-331, R-283
 *    Have an initial screen to ask how they are paying and allow them to say they are expecting a fee credit waiver,
 *    and then pass them to either the CC or ACH flow depending on what they select.
 *    If they are expecting a fee waiver, there would be a required field added which would be a dynamically
 *    generated list of the schools specified on the PFS.
 *
 * SL, Exponent Partners, 2013
 */
public without sharing class FamilyPaymentController extends PaymentControllerBase {
    public static final String PFS_PARAM_NAME = 'id';
    public static final String ACADEMIC_YEAR_PARAM_NAME = 'academicyearid';
    public static final String TRANSACTION_LINE_ITEM_PARAM_NAME = 'tid';

    public static final String PAYMENT_OPP_STAGENAME_OPEN = 'Open';
    public static final String PAYMENT_OPP_STAGENAME_WRITTEN_OFF = 'Written Off';
    public static final String SCHOOL_PORTAL_SETTINGS_INSTANCE = 'School';

    public static final String MESSAGE_WAIVER_SCHOOL_VALIDATION_ERROR = 'Please select the school expected to provide a waiver';
    public static final String MESSAGE_WAIVER_CONTACT_VALIDATION_ERROR = 'Please enter a contact name for the selected school';

    public static final String MESSAGE_EMAIL_CONFIRM_MISMATCH = 'Error: The email addresses do not match';
    public static final String MESSAGE_CHECK_ACCOUNT_NUMBER_CONFIRM_MISMATCH = 'Error: The account numbers do not match';
    public static final String MESSAGE_CHECK_ROUTING_NUMBER_CONFIRM_MISMATCH = 'Error: The routing numbers do not match';

    private static final String MESSAGE_MUST_BE_ACCESSED_FROM_FAMILY_PORTAL = 'This page must be accessed from the ' +
            'Family Portal. Please use the Payment Terminal to process payments if you are not a portal user.';
    private static final String DEFAULT_ERROR_MESSAGE =
            '<b>TRANSACTION RESULT:</b> [{0}] {1}<br/><b>ADDITIONAL INFO:</b> {2}';

    public static final String MESSAGE_PAYMENT_ERROR_TEMPORARY = LABEL.PaymentErrorTemp;
    public static final String MESSAGE_PAYMENT_ERROR_DECLINE = LABEL.PaymentErrorDecline;
    public static final String MESSAGE_PAYMENT_ERROR_BAD_DATA = LABEL.PaymentErrorBadData;
    public static final String MESSAGE_PAYMENT_ERROR_GENERAL = LABEL.PaymentErrorGeneral;
    public static final String MESSAGE_UNSUCCESSFUL_PAYMENT_ATTEMPTS = LABEL.TLI_Unsuccessful_Payment_Attempts;

    public boolean isAdminTransaction { get; set; }
    public boolean isDatabankTransaction { get; set; }
    public String adminMessage { get; set; }
    public Boolean showContactPanel { get; set; }
    private FamilyTemplateController controller { get; set; }

    public PFS__c thePFS { get; set; }

    public String academicYearId {
        get {
            if (academicYearId == null) {
                academicYearId = ApexPages.currentPage().getParameters().get(ACADEMIC_YEAR_PARAM_NAME);
                academicYearId = (String.isBlank(academicYearId)) ?
                        GlobalVariables.getCurrentAcademicYear().Id : academicYearId;
            }
            return academicYearId;
        }
        set;
    }
    public Opportunity theOpportunity { get; set; }

    public String paymentStep { get; set; }

    /**
     * @description Whether or not there has been an error during the processBefore()
     *              method. This will make sure that the processPayment() method does
     *              not continue.
     */
    public Boolean hasProcessBeforeErrors {
        get {
            if (hasProcessBeforeErrors == null) {
                return false;
            }
            return hasProcessBeforeErrors;
        }
        set;
    }

    public Boolean isTestMode {
        get {
            return PaymentProcessor.InTestMode;
        }
    }

    public String billingEmailConfirm { get; set; }
    public String checkAccountNumberConfirm { get; set; }
    public String checkRoutingNumberConfirm { get; set; }

    // waiver fields
    public String waiverSchool { get; set; }
    public String waiverContactName { get; set; }

    // NAIS-907 filter the schools in the waiver dropdown
    //public List<School_PFS_Assignment__c> schoolPfsAssignments { get; set; }
    public List<Account> filteredSchoolsApplied { get; set; }

    public String getCreditCardStatementDescriptor() {
        return PaymentProcessor.CreditCardStatementDescriptor;
    }

    public Boolean ksOnly;

    protected override String getPfsIdParameter() {
        return PFS_PARAM_NAME;
    }

    /**
     * @description Constructor that generates the default state
     *              of the Family Payment page.
     */
    public FamilyPaymentController(FamilyTemplateController controller) {
        // call the base class constructor
        super();
        this.controller = controller;
        PageError = false;
        this.showContactPanel = false;

        User currentUser = GlobalVariables.getCurrentUser();

        // NAIS-1688 [dp] For Databank Paper Entry check if user is a databank user
        isDatabankTransaction = GlobalVariables.isDatabankUser(currentUser);

        // check if the user is NOT databank and not a portal user -- throw error
        isAdminTransaction = false;
        if (!isDatabankTransaction && currentUser.ContactId == null) {
            isAdminTransaction = true;
            adminMessage = MESSAGE_MUST_BE_ACCESSED_FROM_FAMILY_PORTAL;
        }

        if (isAdminTransaction || isDatabankTransaction) {
            // user is an admin -- get the PFS using the id param
            String pfsId = ApexPages.currentPage().getParameters().get(PFS_PARAM_NAME);
            if ((pfsId == null) || (pfsId.length() == 0)) {
                PageError = true;
                ErrorMessage = 'Missing PFS Id parameter';
                return;
            }
        }

        thePfs = this.controller.pfsRecord;

        if (thePFS == null) {
            PageError = true;
            ErrorMessage = 'PFS record not found';
            return;
        }

        if (thePFS.PFS_Status__c != 'Application Submitted') {
            pageError = true;
            ErrorMessage = Label.UnsubmittedPfsPaymentErrorNoPrior;

            PFS__c potentialPfs = getSubmittedUnpaidPfs(new Set<Id> { currentUser.ContactId });
            if (potentialPfs != null && potentialPfs.Academic_Year_Picklist__c != null) {
                Academic_Year__c priorAcademicYear = AcademicYearService.Instance.getPreviousAcademicYear();
                if (priorAcademicYear == null) {
                    return;
                }

                // Only allow them to pay for the prior academic year PFS
                if (potentialPfs.Academic_Year_Picklist__c == priorAcademicYear.Name) {
                    ErrorMessage = String.format(Label.UnsubmittedPfsPaymentError, new List<String>{
                            priorAcademicYear.Name,
                            priorAcademicYear.Id,
                            priorAcademicYear.Name
                    });
                }
            }
            return;
        }

        // Return early if this is the already paid page.
        if (ApexPages.currentPage().getURL().toLowerCase().contains('familypaymentprevpaid')) {
            return;
        }

        // set up the payment data structure
        paymentData.paymentType = FamilyPaymentData.PAYMENT_TYPE_CC;  // default to credit card payment
        paymentData.billingCountry = PaymentProcessor.getDefaultCountryCode();

        if (thePFS.Opportunities__r.isEmpty()) {
            // [DP] 07.06.2015 NAIS-1933 now getting a "dummy" opp to use that is never inserted (only need to do this if fee hasn't been waived)
            if (thePFS.Fee_Waived__c != 'Yes') {
                theOpportunity = FinanceAction.createOppAndTransaction(new Set<Id>{thePFS.ID}, true)[0];
            }
        } else if (hasClosedOpportunity(thePFS)) {
            // There is a closed Opportunity on this Pfs. Let the user know to call in.
            pageError = true;
            ErrorMessage = Label.PfsHasClosedOpportunity;
        } else {
            theOpportunity = thePFS.Opportunities__r[0];
        }

        // get the list of schools applied to


        /* NAIS-907: BEGIN Filter Schools Expected to Provide Waiver */

        filteredSchoolsApplied = new List<Account>();
        Set<Id> appliedSchoolIds = new Set<Id>();
        Set<Id> appliedSchoolIdsNew = new Set<Id>();
        Set<Id> appliedSchoolIdsReturning = new Set<Id>();

        Id ksSchoolId = SchoolPortalSettings.KS_School_Account_Id;

        List<School_PFS_Assignment__c> spaList = [SELECT Id, School__c, School__r.Name, Name,
                                                        Student_Folder__r.New_Returning__c
                                                FROM School_PFS_Assignment__c
                                                WHERE Applicant__r.PFS__c = :thePfs.Id
                                                AND Withdrawn__c != 'Yes'];
        // get list of SPAs, excluding those that are withdrawn
        for (School_PFS_Assignment__c spa : spaList) {
            if (spa.School__c != null) {
                appliedSchoolIds.add(spa.School__c);
                if (spa.Student_Folder__r.New_Returning__c == 'New') {
                    appliedSchoolIdsNew.add(spa.School__c);
                } else {
                    appliedSchoolIdsReturning.add(spa.School__c);
                }
            }
        }

        ksOnly = FinanceAction.isKSOnlyPFS(spaList); // [DP] 07.07.2015 NAIS-1933 need to determine if this is KS only when calculating fee

        // [CH] NAIS-2180 Get the set of Application Fee Waivers that have been declined
        //         for this school and Parent so the schools can be excluded from the avilable list
        Set<Id> declinedFeeWaivers = new Set<Id>();
        for (Application_Fee_Waiver__c appWaiver : [select Id, Contact__c, Account__c from Application_Fee_Waiver__c
                                                    where Contact__c = :thePFS.Parent_A__c
                                                    and Academic_Year__c = :this.controller.academicYearId ]) {
            declinedFeeWaivers.add(appWaiver.Account__c);
        }

        // loop through the schools (but only if the subscriber status is current)
        for (Account appliedSchool : [SELECT Id, Name, SSS_Subscriber_Status__c,
                                            (SELECT Id, PFS_Deadline_Type__c, PFS_New__c, PFS_Returning__c
                                                FROM Annual_Settings__r
                                                 WHERE Academic_Year__c = :this.controller.academicYearId
                                                 ORDER BY CreatedDate DESC LIMIT 1)
                                        FROM Account
                                        WHERE Id IN :appliedSchoolIds
                                        AND SSS_Subscriber_Status__c = 'Current'
                                        ORDER BY Name]) {

            // check for Schools that have indicated in annual settings that they have a hard deadline for PFS submission and that deadline has passed
            Date deadlineDate = null;
            if (!appliedSchool.Annual_Settings__r.isEmpty()) {
                Annual_Setting__c settings = appliedSchool.Annual_Settings__r[0];
                if (settings.PFS_Deadline_Type__c == 'Hard Deadline') {
                    if (appliedSchoolIdsNew.contains(appliedSchool.Id)) {
                        deadlineDate = settings.PFS_New__c;
                    }
                    if (appliedSchoolIdsReturning.contains(appliedSchool.Id)
                            && ((deadlineDate == null) || (settings.PFS_Returning__c > deadlineDate))) {
                        deadlineDate = settings.PFS_Returning__c;
                    }
                }
            }

            // [CH] NAIS-2180 Added logic to exclude schools that have already declined a fee waiver
            if ( ((deadlineDate == null) || (deadlineDate >= Date.today())) && !declinedFeeWaivers.contains(appliedSchool.Id)) {
                this.filteredSchoolsApplied.add(appliedSchool);
            }
        }
        /* NAIS-907: END Filter Schools Expected to Provide Waiver */


        // prepopulate the billing information
        paymentData.billingFirstName = thePFS.Parent_A_First_Name__c;
        paymentData.billingLastName = thePFS.Parent_A_Last_Name__c;
        paymentData.billingCity = thePFS.Parent_A_City__c;
        paymentData.billingPostalCode = thePFS.Parent_A_ZIP_Postal_Code__c;
        paymentData.billingEmail = thePFS.Parent_A_Email__c;

        // split the address into individual lines
        if (thePFS.Parent_A_Address__c != null) {
            List<String> addressLines = thePFS.Parent_A_Address__c.trim().split('\n');
            if (addressLines.size() > 0) {
                paymentData.billingStreet1 = addressLines[0];
            }
            if (addressLines.size() > 1) {
                paymentData.billingStreet2 = addressLines[1];
            }
        }

        // try to look up the country code
        if (thePFS.Parent_A_Country__c != null) {
            paymentData.billingCountry = PaymentProcessor.getCountryCode(thePFS.Parent_A_Country__c);
        }

        if (paymentData.billingCountry == PaymentProcessor.getDefaultCountryCode()) {
            paymentData.billingState = thePFS.Parent_A_State__c;
        }

        // get the application fee
        Decimal balanceDue = this.getNetAmountDue();
        if (balanceDue == null) {
            PageError = true;
            ErrorMessage = 'Unable to calculate application amount due.';
            return;
        }
        if (balanceDue <= 0 ) {
            PageError = true;
            ErrorMessage = 'You have no balance due at this time.';
            return;
        }
        paymentData.paymentAmount = balanceDue;

        paymentStep = 'PAYMENT_SELECT';

        // [DP] NAIS-1585 Show Decline Error
        String errParam = System.currentPagereference().getParameters().get('paymenterror');
        if (errParam != null && errParam == 'true') {
            PageError = true;
            String showContactPanelParam = System.currentPagereference().getParameters().get('showContactPanel');
            this.showContactPanel = ( String.isNotEmpty( showContactPanelParam) && showContactPanelParam == 'true');
            if (ErrorMessage == null) {
                ErrorMessage = System.currentPagereference().getParameters().get('errormessage');
            }
        }
    }

    public FamilyPaymentController getMe() {
        return this;
    }

    public static FamilyTemplateController.config loadPFS(String pfsId, String academicYearId) {
        PFS__c pfsRecord;
        User currentUser = GlobalVariables.getCurrentUser();

        if(pfsId!=null && ((!GlobalVariables.isDatabankUser(currentUser) && currentUser.ContactId == null) || GlobalVariables.isDatabankUser(currentUser))) {
            pfsRecord = getPfsRecord(pfsId);
        } else {
            // user isn't an admin -- get the PFS using the current user and academic year
            pfsRecord = getPfsRecord(currentUser.contactId, academicYearId);
        }
        return new FamilyTemplateController.config(pfsRecord, null);
    }//End:loadPFS

    // [DP] NAIS-1583 redirects to dashboard if fee is already waived
    public PageReference initMethod() {
        if (isDatabankTransaction) {
            return databankWaiverOnInit();
        } else if (thePFS.Fee_Waived__c == 'Yes') {
            return actionCancel();
        } else  {
            return null;
        }
    }

    public PageReference actionCancel() {
        PageReference pr = Page.FamilyDashboard;
        if (academicYearId != null) {
            pr.getParameters().put('academicyearid', academicYearId);
        }
        return pr.setRedirect(true);
    }

    public PageReference actionPaymentSelect() {
        clearPageError();
        paymentStep = 'PAYMENT_SELECT';

        return Page.FamilyPayment;
    }

    /**
     * @description
     * @return A PageReference that will navigate the user to the
     */
    public PageReference actionPaymentForm() {
        PageReference pr = null;
        clearPageError();

        String paymentType = ApexPages.currentPage().getParameters().get('paymentTypeParam');
        if (paymentType != null) {
            paymentData.paymentType = paymentType;
        }

        if (paymentData.paymentType == FamilyPaymentData.PAYMENT_TYPE_WAIVER) {
            // for waivers, instead of displaying the form, process the waiver
            if (validateWaiverFields() == true) {
                pr = actionWaiverSubmit();
            }
        } else {
            paymentStep = 'PAYMENT_FORM';
        }

        return pr;
    }

    /**
     * @description
     * @return A PageReference that will navigate the user to the
     */
    public PageReference actionPaymentConfirm() {
        clearPageError();
        paymentData.trimStrings();

        boolean isValid = false;
        if (paymentData.paymentType == FamilyPaymentData.PAYMENT_TYPE_CC) {
            isValid = validateCreditCardForm();
        } else if (paymentData.paymentType == FamilyPaymentData.PAYMENT_TYPE_ECHECK) {
            isValid = validateECheckForm();
        }

        if (isValid) {
            PaymentService.Request request = new PaymentService.Request(theOpportunity, thePFS, paymentData, false);
            PaymentService.Response response = PaymentService.Instance.processBefore(request);

            if (!response.Result.isSuccess) {
                hasProcessBeforeErrors = true;
                String message = (response.Result.ErrorMessage == null) ?
                        PaymentProcessor.UNEXPECTED_ERROR : response.Result.ErrorMessage;
                setPageError(message);
                return null;
            }
        }

        // We want to make sure that we check the results of the process before
        if (!isValid) {
            setPageError(MESSAGE_PAGE_VALIDATION_ERROR);
        }

        if (this.PageError != true && !hasProcessBeforeErrors) {
           paymentStep = 'PAYMENT_CONFIRM';
        }

        return null;
    }

    /**
     * @description Send the user to the Family Payment Submit page.
     * @returns A PageReference pointing to the Family Payment Submit page.
     */
    public PageReference actionPaymentSubmit() {
        clearPageError();
        PageReference pr = Page.FamilyPaymentSubmit;
        return pr;
    }

    /**
     * @description If a PFS exists, and there is a school that has provided a waiver,
     *              create a waiver and assign it to the family.
     * @returns A PageReference sending the user to the Family Payment Waiver Complete page.
     */
    public PageReference actionWaiverSubmit() {
        clearPageError();

        // create an application fee waiver
        if ((thePFS != null) && (waiverSchool != null)) {
            // NAIS-1310: only create a new waiver request if one doesn't already exists
            if ([SELECT count()
                    FROM Application_Fee_Waiver__c
                    WHERE Contact__c = :thePFS.Parent_A__c
                    AND Academic_Year__c = :this.controller.academicYearId
                    AND Account__c = :this.waiverSchool
                    AND Status__c IN ('Pending Approval', 'Assigned') ] == 0) {

                Application_Fee_Waiver__c waiver = new Application_Fee_Waiver__c();
                waiver.Academic_Year__c = this.controller.academicYearId;
                waiver.Contact__c = thePFS.Parent_A__c;
                waiver.Account__c = this.waiverSchool;
                waiver.Person_Who_Offered_Waiver__c = this.waiverContactName;
                waiver.Status__c = 'Pending Approval';

                Contact mainSSSCon;
                // loop through so we don't have to worry about no records being found
                for (Contact c:  [Select Id, Email from Contact where AccountId = :this.waiverSchool AND SSS_Main_Contact__c = true order by LastModifiedDate desc limit 1]) {
                    mainSSSCon = c;
                }
                waiver.SSS_Contact_Email__c = mainSSSCon == null ? null : mainSSSCon.Email;

                insert waiver;
            }
        }

        PageReference pr = Page.FamilyPaymentWaiverComplete;
        if (isAdminTransaction) {
            pr.getParameters().put(PFS_PARAM_NAME, thePfs.Id);
        } else {
            pr.getParameters().put(ACADEMIC_YEAR_PARAM_NAME, academicYearId);
        }
        pr.setRedirect(true); // force a GET to flush the page state and prevent a re-post on page reload
        return pr;
    }

    /**
     * @description Handle either creating a new or updating an existing waiver
     *              with assigned for the given PFS.
     * @returns A PageReference pointing the user to the Family Payment Waiver Complete
     *          page.
     */
    public PageReference databankWaiverOnInit() {
        clearPageError();

        // create a application fee waiver
        if (thePFS != null) {
            // NAIS-1310: only create a new waiver request if one doesn't already exists
            List<Application_Fee_Waiver__c> existingAFWs =  [SELECT Id, Academic_Year__c, Status__c
                    FROM Application_Fee_Waiver__c
                    WHERE Contact__c = :thePFS.Parent_A__c
                    AND Academic_Year__c = :this.controller.academicYearId
                    AND Account__c = :SchoolPortalSettings.KS_School_Account_Id
                    order by CreatedDate desc
                    LIMIT 1];

            if (existingAFWs.isEmpty()) {
                Application_Fee_Waiver__c waiver = new Application_Fee_Waiver__c();
                waiver.Academic_Year__c = this.controller.academicYearId;
                waiver.Contact__c = thePFS.Parent_A__c;
                waiver.Account__c = SchoolPortalSettings.KS_School_Account_Id;
                waiver.Person_Who_Offered_Waiver__c = 'Databank Data Entry';
                waiver.Status__c = 'Assigned';

                Contact mainSSSCon;
                // loop through so we don't have to worry about no records being found
                for (Contact c:  [Select Id, Email from Contact where AccountId = :this.waiverSchool AND SSS_Main_Contact__c = true order by LastModifiedDate desc limit 1]) {
                    mainSSSCon = c;
                }
                waiver.SSS_Contact_Email__c = mainSSSCon == null ? null : mainSSSCon.Email;

                insert waiver;
            } else if (existingAFWs[0].Status__c != 'Assigned') {
                existingAFWs[0].Status__c = 'Assigned';
                update existingAFWs[0];
            }
        }

        PageReference pr = Page.FamilyPaymentWaiverComplete;
        pr.getParameters().put(PFS_PARAM_NAME, thePfs.Id);
        pr.getParameters().put('dbank', '1');
        pr.setRedirect(true); // force a GET to flush the page state and prevent a re-post on page reload
        return pr;
    }

    /**
     * @description Handle any transaction error, setting the errorMessage with the
     *              relevant message.
     */
    @testVisible
    private void handleTransactionError(PaymentResult result, Boolean errorLimit) {
        this.PageError = true;

        if (errorLimit) {
            this.ErrorMessage = MESSAGE_UNSUCCESSFUL_PAYMENT_ATTEMPTS;
            this.showContactPanel = true;
        } else {
            this.ErrorMessage = result.getErrorMessage();
        }
    }

    /**
     * @description Process the payment for the PFS, allowing it to be submitted
     *              to the applied schools. If the payment is successful then set
     *              the PageReference to send the user to the Family Payment Complete
     *              page, otherwise send the user to the Family Payment Error page with
     *              the relevant error message loaded.
     * @returns A PageReference pointing to the Family Payment Complete page if the payment
     *          is successful, otherwise the Family Payment Error page.
     */
    public override PageReference processPayment() {
        clearPageError();

        paymentData.paymentDescription = String.format(PaymentProcessor.PfsApplicationFeeItemDescription, new List<String>{thePfs.PFS_Number__c});
        paymentData.paymentTracker = String.format(PaymentProcessor.PfsApplicationFeeTracker,
                                            new List<String>{thePfs.PFS_Number__c});

        if ((paymentData.paymentAmount == null) || (paymentData.paymentAmount <= 0)) {
            this.PageError = true;
            this.ErrorMessage = 'Unable to process payment amount: ' + paymentData.paymentAmount;
            return null;
        }

        // NAIS-2049 [DP] 12.10.2014
        // SFP-200 [DP] 12.14.2015 ALWAYS check for paid Opp before continuing
        List<Opportunity> requeriedOppList = [Select Id, Stagename, Paid_Status__c from Opportunity where PFS__c = :thePFS.Id
                                                AND RecordTypeId = :RecordTypes.pfsApplicationFeeOpportunityTypeId]; // [DP] 05.18.2016 SFP-510 only care about PFS App Fees
        for (Opportunity requeriedOpp : requeriedOppList) {
            if (requeriedOpp.Paid_Status__c == 'Paid') {
                apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Your payment has already been received.  Please return to the dashboard.'));
                PageReference prAlreadyPaid = Page.FamilyPaymentPrevPaid.setRedirect(true);
                prAlreadyPaid.getParameters().put(PFS_PARAM_NAME, thePfs.Id);
                prAlreadyPaid.getParameters().put(ACADEMIC_YEAR_PARAM_NAME, academicYearId);
                return prAlreadyPaid;
            }

            // [DP] 05.18.2016 SFP-510 adding ability to set theOpportunity variable here in case an errored payment created an opp and then the user hit "back"
            if (requeriedOpp.StageName == PAYMENT_OPP_STAGENAME_OPEN || requeriedOpp.StageName == PAYMENT_OPP_STAGENAME_WRITTEN_OFF) { // [SL] NAIS-1235: allow Written Off
                theOpportunity = requeriedOpp;
            }
        }

        PaymentService.Request request = new PaymentService.Request(theOpportunity, thePFS, paymentData, false);
        PaymentService.Response response = PaymentService.Instance.processPayment(request);

        if (response.ErrorOccurred) {
            this.PageError = true;
            this.ErrorMessage = response.ErrorMessage;
            handleTransactionError(response.Result, response.ErrorMessage == MESSAGE_UNSUCCESSFUL_PAYMENT_ATTEMPTS);
            return handlePaymentError();
        }

        // Show success or error page
        PageReference pr;

        if (response.Result != null && response.Result.isSuccess) {
            if (thePfs.Parent_A__c != null) {
                // Attempt to send an email receipt, debugging if it fails
                try {
                    PaymentUtils.emailPaymentReceipt(thePfs.Parent_A__c, response.TransactionLineItemId) ;
                } catch(Exception e) {
                    CustomDebugService.Instance.logException(e);
                }
            }

            pr = Page.FamilyPaymentComplete;
            pr.setRedirect(true);

            if (isAdminTransaction) {
                pr.getParameters().put(PFS_PARAM_NAME, thePfs.Id);
            } else {
                pr.getParameters().put(ACADEMIC_YEAR_PARAM_NAME, academicYearId);
            }

            // NAIS-1092
            if (response.TransactionLineItemId != null) {
                pr.getParameters().put(TRANSACTION_LINE_ITEM_PARAM_NAME, response.TransactionLineItemId);
            }
        } else {
            handleTransactionError(response.Result, response.ErrorMessage == MESSAGE_UNSUCCESSFUL_PAYMENT_ATTEMPTS);

            pr = handlePaymentError();
        }

        return pr;
    }

    /**
     * @description Handle an error on the Family Payment page, reloading it with
     *              the error populated.
     * @returns A PageReference to the Family Payment page with the error message
     *          and academic year loaded into the page parameters.
     */
    public override PageReference handlePaymentError() {
        PageReference pr = Page.FamilyPayment;

        pr.getParameters().put('paymenterror', 'true');
        pr.getParameters().put('errormessage', this.ErrorMessage);
        pr.getParameters().put('showContactPanel', String.valueOf( this.showContactPanel));
        pr.getParameters().put(ACADEMIC_YEAR_PARAM_NAME, this.academicYearId);

        return pr;
    }

    /**
     * @description A list of Select Options comprised of the schools that
     *              the family has applied to.
     * @returns A list of Select Options made up of the schools that the
     *          family has applied to.
     */
    public List<SelectOption> getSchoolsAppliedSelectOptions() {
        List<SelectOption> options = new List<SelectOption>();

        if (this.filteredSchoolsApplied != null) {
            for (Account school : this.filteredSchoolsApplied) {
                options.add(new SelectOption(school.Id, school.Name));
            }
        }
        return options;
    }

    // returns the PFS for a parent contact and academic year
    public static PFS__c getPfsRecord(Id parentAContactId, Id academicYearId) {
        PFS__c pfsRecord = null;

        List<PFS__c> pfsRecords = PfsSelector.Instance.selectWithOpportunitiesByParentAAndAcademicYears(
                new Set<Id> { parentAContactId },
                new Set<String> { GlobalVariables.getAcademicYear(academicYearId).Name }
        );
        if (!pfsRecords.isEmpty()) {
            pfsRecord = pfsRecords[0];
        }

        return pfsRecord;
    }

    // returns the PFS for the PFS Id
    public static PFS__c getPfsRecord(Id pfsId) {
        PFS__c pfsRecord = null;

        List<PFS__c> pfsRecords = PfsSelector.Instance.selectByIdWithOpportunities(new Set<Id> { pfsId });
        if (!pfsRecords.isEmpty()) {
            pfsRecord = pfsRecords[0];
        }

        return pfsRecord;
    }

    private static Boolean hasClosedOpportunity(PFS__c pfs) {
        if (pfs == null || pfs.Opportunities__r == null || pfs.Opportunities__r.size() == 0) {
            return false;
        }

        List<Opportunity> opportunities = pfs.Opportunities__r;
        for (Opportunity opportunity : opportunities) {
            if (opportunity.StageName.contains('Closed')) {
                return true;
            }
        }

        return false;
    }

    private PFS__c getSubmittedUnpaidPfs(Set<Id> contactIds) {
        List<PFS__c> unpaidPfsRecords = PfsSelector.Instance.selectByContactIdSubmittedUnpaid(contactIds);

        // Because the PFS records are sorted by Academic Year descending we are
        // expecting the first record, if any, to be the most recent unpaid
        // submitted PFS.
        if (!unpaidPfsRecords.isEmpty()) {
            return unpaidPfsRecords[0];
        }

        return null;
    }

    private boolean validateECheckAccountConfirm() {
        boolean isValid = true;
        if (String.isBlank(checkAccountNumberConfirm)) {
            isValid = false;
            fieldErrors.checkAccountNumberConfirm = MESSAGE_FIELD_VALUE_MISSING;
        } else if (checkAccountNumberConfirm.trim() != paymentData.checkAccountNumber) {
            isValid = false;
            fieldErrors.checkAccountNumberConfirm = MESSAGE_CHECK_ACCOUNT_NUMBER_CONFIRM_MISMATCH;
        }

        if (String.isBlank(checkRoutingNumberConfirm)) {
            isValid = false;
            fieldErrors.checkRoutingNumberConfirm = MESSAGE_FIELD_VALUE_MISSING;
        } else if (checkRoutingNumberConfirm.trim() != paymentData.checkRoutingNumber) {
            isValid = false;
            fieldErrors.checkRoutingNumberConfirm = MESSAGE_CHECK_ROUTING_NUMBER_CONFIRM_MISMATCH;
        }

        return isValid;
    }

    @testVisible
    private boolean validateCreditCardForm() {
        boolean isValid = true;

        boolean isValidBillingFields = validateBillingFields();
        boolean isValidCreditCardFields = validateCreditCardFields();

        isValid = isValidBillingFields && isValidCreditCardFields;

        return isValid;
    }

    private boolean validateECheckForm() {
        boolean isValid = true;

        boolean isValidBillingFields = validateBillingFields();
        boolean isValidECheckFields = validateECheckFields();
        boolean isValidECheckAccountConfirm = validateECheckAccountConfirm();

        isValid = isValidBillingFields && isValidECheckFields && isValidECheckAccountConfirm;

        return isValid;
    }

    private boolean validateWaiverFields() {
        boolean isValid = true;

        if (String.isBlank(waiverSchool) || (waiverSchool == 'NONE')) {
            isValid = false;
            this.setPageError(MESSAGE_WAIVER_SCHOOL_VALIDATION_ERROR);
        }
        else if (String.isBlank(waiverContactName)) {
            isValid = false;
            this.setPageError(MESSAGE_WAIVER_CONTACT_VALIDATION_ERROR);
        }

        return isValid;
    }

    private Decimal getNetAmountDue() {
        Decimal netAmountDue = null;
        // [DP] 07.06.2015 NAIS-1933 use generic method that doesn't require an opportunity to determine the amount due.
        netAmountDue = FinanceAction.getAmountDue(theOpportunity, thePFS.Academic_Year_Picklist__c, ksOnly);
        if (theOpportunity != null && theOpportunity.Id != null) {
            netAmountDue = netAmountDue + theOpportunity.Total_Write_Offs__c;
        }

        return netAmountDue;
    }

    private void clearPageError() {
        this.PageError = false;
        this.ErrorMessage = null;
        this.fieldErrors = new FieldErrors();
    }

    private void setPageError(String message) {
        this.PageError = true;
        this.ErrorMessage = message;
    }

    /**
     * @description Custom Exception that is thrown when there is a Family Payment Exception.
     *              NOTE: Not currently used.
     **/
    public class familyPaymentException extends Exception {}
}