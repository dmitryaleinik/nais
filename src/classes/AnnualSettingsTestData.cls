/**
 * @description This class is used to create Annual Setting records for unit tests.
 */
@isTest
public class AnnualSettingsTestData extends SObjectTestData {
    /**
     * @description Get the default values for the Annual_Setting__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Annual_Setting__c.Academic_Year__c => AcademicYearTestData.Instance.DefaultAcademicYear.Id,
                Annual_Setting__c.School__c => AccountTestData.Instance.asSchool().DefaultAccount.Id
        };
    }

    public AnnualSettingsTestData withTuitionScheduleType(String scheduleType) {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Tuition_Schedule_Type__c, scheduleType);
    }

    /**
     * @description Set the Use Additional Questions field to Yes.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData usingAdditionalQuestions() {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Use_Additional_Questions__c, 'Yes');
    }

    /**
     * @description Set the School__c field on the current Annual Setting record.
     * @param accountId The School Id to set on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forSchoolId(Id accountId) {
        return (AnnualSettingsTestData) with(Annual_Setting__c.School__c, accountId);
    }

    /**
     * @description Set the Academic_Year__c field on the current Annual Setting record.
     * @param academicYearId The Academic Year Id to set on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forAcademicYearId(Id academicYearId) {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Academic_Year__c, academicYearId);
    }

    /**
    * @description Set the Display_Next_Year_Tax_Estimates__c field on the current Annual Setting record.
    * @param displayNextYearTaxEstimates The boolean value to set on the Annual Setting.
    * @return The current working instance of AnnualSettingsTestData.
    */
    public AnnualSettingsTestData forDisplayNextYearTaxEstimates(Boolean displayNextYearTaxEstimates)
    {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Display_Next_Year_Tax_Estimates__c, displayNextYearTaxEstimates);
    }

    /**
     * @description Set the Total_Waivers_Override_Default__c field on the current Annual Setting record.
     * @param totalWaivers The Total Waivers to set on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forTotalWaivers(Integer totalWaivers) {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Total_Waivers_Override_Default__c, totalWaivers);
    }

    /**
     * @description Set the Waivers_Assigned__c field on the current Annual Setting record.
     * @param waiversAssigned The Waivers Assigned to set on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forWaiversAssigned(Integer waiversAssigned) {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Waivers_Assigned__c, waiversAssigned);
    }

    /**
     * @description Set the PFS_Reminder_Date__c field on the current Annual Setting record.
     * @param pfsReminderDate The PFS Reminder Date to set on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forPFSReminderDate(Date pfsReminderDate) {
        return (AnnualSettingsTestData) with(Annual_Setting__c.PFS_Reminder_Date__c, pfsReminderDate);
    }

    /**
     * @description Set the Document_Reminder_Date__c field on the current Annual Setting record.
     * @param documentReminderDate The Prior Year Tax Form Reminder New to set on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forPriorYearTaxFormReminderNew(Date documentReminderDate) {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Document_Reminder_Date__c, documentReminderDate);
    }

    /**
     * @description Set the Curr_Year_Tax_Docs_Reminder_Date_New__c field on the current Annual Setting record.
     * @param currYearTaxDocsReminderDateNew The Curr Year Tax Docs Reminder Date New to set on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forCurrYearTaxDocsReminderDateNew(Date currYearTaxDocsReminderDateNew) {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Curr_Year_Tax_Docs_Reminder_Date_New__c, currYearTaxDocsReminderDateNew);
    }

    /**
     * @description Set the PFS_Reminder_Date_Returning__c field on the current Annual Setting record.
     * @param pfsReminderDateReturning The PFS Reminder Date Returning to set on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forPFSReminderDateReturning(Date pfsReminderDateReturning) {
        return (AnnualSettingsTestData) with(Annual_Setting__c.PFS_Reminder_Date_Returning__c, pfsReminderDateReturning);
    }

    /**
     * @description Set the Document_Reminder_Date_Returning__c field on the current Annual Setting record.
     * @param documentReminderDateReturning The Prior Year Tax Form Reminder Returning to set on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forPriorYearTaxFormReminderReturning(Date documentReminderDateReturning) {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Document_Reminder_Date_Returning__c, documentReminderDateReturning);
    }

    /**
     * @description Set the Curr_Yr_Tax_Docs_Remind_Date_Returning__c field on the current Annual Setting record.
     * @param currYrTaxDocsRemindDateReturning The Curr Yr Tax Docs Remind Date Returning to set on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forCurrYrTaxDocsRemindDateReturning(Date currYrTaxDocsRemindDateReturning) {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Curr_Yr_Tax_Docs_Remind_Date_Returning__c, currYrTaxDocsRemindDateReturning);
    }

    /**
     * @description Set the Apply_Minimum_Income__c field on the current Annual Setting record.
     * @param applyMinimumIncome The string value to set Apply Minimum Income field on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forApplyMinimumIncome(String applyMinimumIncome) {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Apply_Minimum_Income__c, applyMinimumIncome);
    }

    /**
     * @description Set the Add_Depreciation_Home_Business_Expense__c field on the current Annual Setting record.
     * @param addDeprecationHomeBusinessExpense The string value to set Add Deprecation Home Business Expense field on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forAddDeprecationHomeBusinessExpense(String addDeprecationHomeBusinessExpense) {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Add_Depreciation_Home_Business_Expense__c, addDeprecationHomeBusinessExpense);
    }

    /**
     * @description Set the Collect_MIE__c field on the current Annual Setting record.
     * @param collectMIE The string value to set Monthly Income and Expenses field on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forCollectMIE(String collectMIE) {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Collect_MIE__c, collectMIE);
    }
    
    /**
     * @description Set the Opt_Out_Of_PFS_EZ__c field on the current Annual Setting record.
     * @param optOutEz The Opt Out PFS EZ to set on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forOptOutPfsEz(Boolean optOutEz) {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Opt_Out_Of_PFS_EZ__c, optOutEz);
    }

    /**
     * @description Set the PFS_New__c field on the current Annual Setting record.
     * @param pfsNew The Deadline Date for new Pfss to set on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forPfsNew(Date pfsNew)
    {
        return (AnnualSettingsTestData) with(Annual_Setting__c.PFS_New__c, pfsNew);
    }

    /**
     * @description Set the PFS_Returning__c field on the current Annual Setting record.
     * @param pfsReturning The Deadline Date for Returning Pfss to set on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forPfsReturning(Date pfsReturning)
    {
        return (AnnualSettingsTestData) with(Annual_Setting__c.PFS_Returning__c, pfsReturning);
    }

    /**
     * @description Set the Current_Year_Tax_Documents_New__c field on the current Annual Setting record.
     * @param currentYearTaxDocsNew The Deadline Date for New Current Year Tax Docs to set on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forCurrentYearTaxDocsNew(Date currentYearTaxDocsNew)
    {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Current_Year_Tax_Documents_New__c, currentYearTaxDocsNew);
    }

    /**
     * @description Set the Current_Year_Tax_Documents_Returning__c field on the current Annual Setting record.
     * @param currentYearTaxDocsReturning The Deadline Date for Returning Current Year Tax Docs to set on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forCurrentYearTaxDocsReturning(Date currentYearTaxDocsReturning)
    {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Current_Year_Tax_Documents_Returning__c, currentYearTaxDocsReturning);
    }

    /**
     * @description Set the Prior_Year_Tax_Documents_New__c field on the current Annual Setting record.
     * @param priorYearTaxDocsNew The Deadline Date for New Prior Year Tax Docs to set on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forPriorYearTaxDocsNew(Date priorYearTaxDocsNew)
    {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Prior_Year_Tax_Documents_New__c, priorYearTaxDocsNew);
    }

    /**
     * @description Set the Prior_Year_Tax_Documents_Returning__c field on the current Annual Setting record.
     * @param priorYearTaxDocsReturning The Deadline Date for Returning Prior Year Tax Docs to set on the Annual Setting.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData forPriorYearTaxDocsReturning(Date priorYearTaxDocsReturning)
    {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Prior_Year_Tax_Documents_Returning__c, priorYearTaxDocsReturning);
    }

    /**
     * @description Set the Don_t_collect_current_year_document_New__c field on the current Annual Setting record to true.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData asCurrentYearTaxDocsNewNotRequired()
    {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Don_t_collect_current_year_document_New__c, true);
    }

    /**
     * @description Set the Don_t_collect_current_year_document_Ret__c field on the current Annual Setting record to true.
     * @return The current working instance of AnnualSettingsTestData.
     */
    public AnnualSettingsTestData asCurrentYearTaxDocsRetNotRequired()
    {
        return (AnnualSettingsTestData) with(Annual_Setting__c.Don_t_collect_current_year_document_Ret__c, true);
    }

    /**
     * @description Sets the Email_Reminders_to_Parents__c field.
     * @param reminders The reminders that would be sent to families.
     * @return The current instance.
     */
    public AnnualSettingsTestData withReminders(String reminders) {
        return (AnnualSettingsTestData)with(Annual_Setting__c.Email_Reminders_to_Parents__c, reminders);
    }

    /**
     * @description Sets all reminder date fields used for new families.
     * @param reminderDate The date that reminders would be sent.
     * @return The current instance.
     */
    public AnnualSettingsTestData withNewFamilyReminderDates(Date reminderDate) {
        return forCurrYearTaxDocsReminderDateNew(reminderDate).forPFSReminderDate(reminderDate).forPriorYearTaxFormReminderNew(reminderDate);
    }

    /**
     * @description Sets all reminder date fields used for returning families.
     * @param reminderDate The date that reminders would be sent.
     * @return The current instance.
     */
    public AnnualSettingsTestData withReturningFamilyReminderDates(Date reminderDate) {
        return forCurrYrTaxDocsRemindDateReturning(reminderDate).forPFSReminderDateReturning(reminderDate).forPriorYearTaxFormReminderReturning(reminderDate);
    }

    /**
     * @description Insert the current working Annual_Setting__c record.
     * @return The currently operated upon Annual_Setting__c record.
     */
    public Annual_Setting__c insertAnnualSettings() {
        return (Annual_Setting__c)insertRecord();
    }

    /**
     * @description Create the current working Annual Setting record without resetting
     *             the stored values in this instance of AnnualSettingsTestData.
     * @return A non-inserted Annual_Setting__c record using the currently stored field
     *             values.
     */
    public Annual_Setting__c createAnnualSettingsWithoutReset() {
        return (Annual_Setting__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Annual_Setting__c record.
     * @return The currently operated upon Annual_Setting__c record.
     */
    public Annual_Setting__c create() {
        return (Annual_Setting__c)super.buildWithReset();
    }

    /**
     * @description The default Annual_Setting__c record.
     */
    public Annual_Setting__c DefaultAnnualSettings {
        get {
            if (DefaultAnnualSettings == null) {
                DefaultAnnualSettings = createAnnualSettingsWithoutReset();
                insert DefaultAnnualSettings;
            }
            return DefaultAnnualSettings;
        }
        private set;
    }

    /**
     * @description Get the Annual_Setting__c SObjectType.
     * @return The Annual_Setting__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Annual_Setting__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static AnnualSettingsTestData Instance {
        get {
            if (Instance == null) {
                Instance = new AnnualSettingsTestData();
            }
            return Instance;
        }
        private set;
    }

    private AnnualSettingsTestData() { }
}