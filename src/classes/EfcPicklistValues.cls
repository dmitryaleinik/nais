public class EfcPicklistValues {
    public static final String EFC_CALC_STATUS_CURRENT = 'Current';
    public static final String EFC_CALC_STATUS_RECALCULATE = 'Recalculate';
    public static final String EFC_CALC_STATUS_RECALCULATE_BATCH = 'Recalculate Batch';
    public static final String EFC_CALC_STATUS_ERROR = 'Error';
    //public static final String EFC_CALC_STATUS_RECALCULATE_FUTURE = 'Recalculate Future';
    
    public static final String BUSINESS_FARM_OWNER_PARENT_A = 'Parent A';
    public static final String BUSINESS_FARM_OWNER_PARENT_B = 'Parent B';
    public static final String BUSINESS_FARM_OWNER_BOTH = 'Both';
    
    public static final String FILING_TYPE_INDIVIDUAL_SINGLE = 'Individual Single';
    public static final String FILING_TYPE_MARRIED_JOINT = 'Married Joint/Surviving Spouse';
    public static final String FILING_TYPE_MARRIED_SEPARATE = 'Married Filing Separately';
    public static final String FILING_TYPE_INDIVIDUAL_HEAD_OF_HOUSEHOLD = 'Individual Head of Household';
    
    public static final String FILING_STATUS_SINGLE = 'Single';
    public static final String FILING_STATUS_MARRIED_JOINT = 'Married, Filing Jointly';
    public static final String FILING_STATUS_MARRIED_SEPARATE = 'Married, Filing Separately';
    public static final String FILING_STATUS_HEAD_OF_HOUSEHOLD = 'Head of Household';
    public static final String FILING_STATUS_DID_NOT_FILE = 'Did not File';
    public static final String FILING_STATUS_QUALIFYING_WINDOW_WITH_CHILD = 'Qualifying Widow(er) with Dependent Child';//SFP-425
    
    public static final String FAMILY_STATUS_1_PARENT = '1 Parent';
    public static final String FAMILY_STATUS_2_PARENT = '2 Parents';
    
    public static final String GRADE_PRESCHOOL = 'Preschool';
    public static final String GRADE_PRE_KINDERGARTEN = 'Pre-Kindergarten';
    public static final String GRADE_JUNIOR_KINDERGARTEN = 'Junior Kindergarten';
    public static final String GRADE_KINDERGARTEN = 'Kindergarten';
    public static final String GRADE_PRE_FIRST = 'Pre-First';
    public static final String GRADE_1 = '1';
    public static final String GRADE_2 = '2';
    public static final String GRADE_3 = '3';
    public static final String GRADE_4 = '4';
    public static final String GRADE_5 = '5';
    public static final String GRADE_6 = '6';
    public static final String GRADE_7 = '7';
    public static final String GRADE_8 = '8';
    public static final String GRADE_9 = '9';
    public static final String GRADE_10 = '10';
    public static final String GRADE_11 = '11';
    public static final String GRADE_12 = '12';
    public static final String GRADE_POST_GRADUATION = 'Post Graduation';
    
    public static final String DAYBOARDING_DAY = 'Day';
    public static final String DAYBOARDING_BOARDING = 'Boarding';
    
    public static final String PFS_STATUS_SUBMITTED = 'Application Submitted';
    
    public static final String PAYMENT_STATUS_PAID_IN_FULL = 'Paid in Full';
    public static final String PAYMENT_STATUS_WRITTEN_OFF = 'Written Off';
}