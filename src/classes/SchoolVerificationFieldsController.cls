/*
* NAIS-357 
* Separate Visualforce page for the verification fields
*
* Nathan, Exponent Partners, 2013
*/
public with sharing class SchoolVerificationFieldsController implements SchoolAcademicYearSelectorStudInterface{

    /*Properties*/
    
    // input
    public String schoolPFSAssignmentId {get; private set;}
    
    // output
    public School_PFS_Assignment__c schoolPFSAssignment {get; private set;} 
    public School_PFS_Assignment__c pyschoolPFSAssignment {get; private set;} 
    public String academicYearId {get; set;} 
    public Student_Folder__c folder {get; set;} 

    public Verification__c verification {get; private set;}
    public Verification__c pyverification {get; private set;}

    public List <School_Document_Assignment__c> schoolDocumentAssignmentList {get; private set;}
    public List <School_Document_Assignment__c> sdaListSorted {get; private set;}
    public List <School_Document_Assignment__c> sdaListW2 {get; private set;}
    public List <School_Document_Assignment__c> sdaListScheduleC {get; private set;}
    public List <School_Document_Assignment__c> sdaListScheduleE {get; private set;}
    public List <School_Document_Assignment__c> sdaListScheduleF {get; private set;}
    
    /*End Properties*/
    
    /*Initialization*/
    
    // constructor
    public SchoolVerificationFieldsController() {
        // init
        schoolPFSAssignment = new School_PFS_Assignment__c();
        verification = new Verification__c();
        schoolDocumentAssignmentList = new List <School_Document_Assignment__c>();
        sdaListSorted = new List <School_Document_Assignment__c>(); 
        sdaListW2 = new List <School_Document_Assignment__c>(); 
        sdaListScheduleC = new List <School_Document_Assignment__c>(); 
        sdaListScheduleE = new List <School_Document_Assignment__c>(); 
        sdaListScheduleF = new List <School_Document_Assignment__c>(); 
        
        loadSchoolPFSAssignment();
        loadVerification(); 
        loadSchoolDocumentAssignment();
        sortSDAList();       
    }

    /*End Initialization*/
     
    /*Helper Methods*/
     
    private void loadSchoolPFSAssignment() {
    
        schoolPFSAssignmentId = ApexPages.currentPage().getParameters().get('schoolPFSAssignmentId');
        if (schoolPFSAssignmentId == null) {
            return;
        }
        
        List <School_PFS_Assignment__c> spfsaList = [select Id, Name, 
            Academic_Year_Picklist__c,
            School__c,
            Applicant__c,
            Applicant__r.PFS__c,
            Applicant__r.PFS__r.Verification__c,
            Applicant__r.PFS__r.Parent_a__r.Name,
            Applicant__r.PFS__r.Parent_A_Email__c,
            Applicant__r.PFS__r.Academic_Year_Picklist__c, 
            Student_Folder__c 
            from School_PFS_Assignment__c where Id = :schoolPFSAssignmentId];
        if (spfsaList.size() > 0) {
            schoolPFSAssignment = spfsaList[0];
            this.academicYearId = GlobalVariables.getAcademicYearByName(this.schoolPFSAssignment.Academic_Year_Picklist__c).Id;
            this.folder = GlobalVariables.getFolderAndSchoolPFSAssignments(this.schoolPFSAssignment.Student_Folder__c);
        }     

       if(getpreviousAcademicYear() != null && schoolPFSAssignment.School__c != null && schoolPFSAssignment.Applicant__r.PFS__r.Parent_A_Email__c != null){
           list<School_PFS_Assignment__c> pyPfs = [SELECT Applicant__r.PFS__r.Verification__c FROM School_PFS_Assignment__c WHERE School__c =:schoolPFSAssignment.School__c AND Applicant__r.PFS__r.Parent_A_Email__c =:schoolPFSAssignment.Applicant__r.PFS__r.Parent_A_Email__c AND Academic_Year_Picklist__c =:getpreviousAcademicYear().Name];
           if(!pyPFS.isEmpty()){
                pyschoolPFSAssignment = pyPfs[0];
                this.loadPYVerification();
           }
        }
    }

    private Academic_Year__c previousAcademicYear;
    public Academic_Year__c getPreviousAcademicYear(){
        if( previousAcademicYear == null){      
            List <Academic_Year__c> ayList = GlobalVariables.getAllAcademicYears();
            id academicYearId = GlobalVariables.getAcademicYearByName(schoolPFSAssignment.Applicant__r.PFS__r.Academic_Year_Picklist__c).Id;
            for (Integer i=0; i<ayList.size(); i++) {
                if (academicYearId != null && ayList[i].Id == academicYearId && i+1 < ayList.size()) {
                    previousAcademicYear = ayList[i+1];
                    break;
                }
            }

             if(previousAcademicYear == null && academicYearId != null){
                Integer firstYear = Integer.valueOf(GlobalVariables.getAcademicYearByName(schoolPFSAssignment.Applicant__r.PFS__r.Academic_Year_Picklist__c).Name.left(4))-1;
                Integer secondYear = Integer.valueOf(GlobalVariables.getAcademicYearByName(schoolPFSAssignment.Applicant__r.PFS__r.Academic_Year_Picklist__c).Name.right(4))-1;
                String yearString = String.valueOf(firstYear) + '-' + String.valueOf(secondYear); 
                previousAcademicYear = new Academic_Year__c(Name=yearString);
            }
        }
        return previousAcademicYear;        
    }
    
    private void loadVerification() {
    
        if (schoolPFSAssignment.Applicant__r.PFS__r.Verification__c == null) {
            return;
        }
    
        String selectStr = getVerificationQuery();
        String queryStr = selectStr + ' where Id = \'' + schoolPFSAssignment.Applicant__r.PFS__r.Verification__c + '\''; 
        List <Verification__c> verList = Database.Query(queryStr);

        if (verList.size() > 0) {                
            verification = verList[0];
        }
    }

    private void loadPYVerification() {
    
        if (pyschoolPFSAssignment.Applicant__r.PFS__r.Verification__c == null) {
            return;
        }

        String selectStr = getVerificationQuery();
        String queryStr = selectStr + ' where Id = \'' + pyschoolPFSAssignment.Applicant__r.PFS__r.Verification__c + '\''; 
        List <Verification__c> verList = Database.Query(queryStr);

        if (verList.size() > 0) {                
            pyverification = verList[0];
        }
    }

    private void loadSchoolDocumentAssignment() {
    
        if (schoolPFSAssignmentId == null) {
            return;
        }
    
        schoolDocumentAssignmentList = [select Id, Name,
            
            Document__r.Birthdate__c,
            Document__r.Document_Type__c,
            Document__r.First_Name__c,
            Document__r.Last_Name__c,
            Document__r.Sch_C_Depreciation_Sect179__c,
            Document__r.Sch_C_Exp_Bus_Uses_Home__c,
            Document__r.Sch_C_Gross_Income__c,
            Document__r.Sch_E_Depreciation_Total__c,
            Document__r.Sch_E_Rents_Income_Total__c,
            Document__r.Sch_E_Royalties_Income_Total__c,
            Document__r.Sch_F_Depreciation_Sec179_Exp__c,
            Document__r.Sch_F_Gross_Income__c,
            Document__r.W2_Deferred_Untaxed_1_A__c,
            Document__r.W2_Deferred_Untaxed_2_B__c,
            Document__r.W2_Deferred_Untaxed_3_C__c,
            Document__r.W2_Deferred_Untaxed_4_D__c,
            Document__r.W2_Deferred_Untaxed_5_E__c,
            Document__r.W2_Deferred_Untaxed_6_F__c,
            Document__r.W2_Deferred_Untaxed_7_G__c,
            Document__r.W2_Deferred_Untaxed_8_H__c,
            Document__r.W2_Employee_ID__c,
            Document__r.W2_Employer_ID__c,
            Document__r.W2_Medicare__c,
            Document__r.W2_SS_Tax__c,
            Document__r.W2_Wages__c,
            Document__r.Document_Year__c,
            Document__r.Filename__c
            
            from School_Document_Assignment__c
            where School_PFS_Assignment__c = :schoolPFSAssignmentId and
                Deleted__c = false and
                Document__c != null and
                Document__r.Deleted__c = false and
                Document__r.Document_Type__c in ('W2', 'Tax Schedule C', 'Tax Schedule E', 'Tax Schedule F')];
                    
    }

    private void sortSDAList() {
    
        if (schoolDocumentAssignmentList == null || schoolDocumentAssignmentList.size() == 0) {
            return;
        }
        
        List <String> docTypeList = new List <String>{'W2', 'Tax Schedule C', 'Tax Schedule E', 'Tax Schedule F'};
        Map <String, List <School_Document_Assignment__c>> docType_sdaList_map = new Map <String, List <School_Document_Assignment__c>>();
        
        for (School_Document_Assignment__c sda : schoolDocumentAssignmentList) {
            List <School_Document_Assignment__c> sdaList = new List <School_Document_Assignment__c>();
            if (docType_sdaList_map.containsKey(sda.Document__r.Document_Type__c)) {
                sdaList = docType_sdaList_map.get(sda.Document__r.Document_Type__c);
            }
            sdaList.add(sda);
            docType_sdaList_map.put(sda.Document__r.Document_Type__c, sdaList);
        } 
        
        // sort
        sdaListSorted = new List <School_Document_Assignment__c>(); 
        sdaListW2 = new List <School_Document_Assignment__c>(); 
        sdaListScheduleC = new List <School_Document_Assignment__c>(); 
        sdaListScheduleE = new List <School_Document_Assignment__c>(); 
        sdaListScheduleF = new List <School_Document_Assignment__c>(); 
        for (String docType : docTypeList) {
            if (docType_sdaList_map.containsKey(docType)) {
                sdaListSorted.addAll(docType_sdaList_map.get(docType));
                if (docType == 'W2') {
                    sdaListW2 = docType_sdaList_map.get(docType);
                } else if (docType == 'Tax Schedule C') {
                    sdaListScheduleC = docType_sdaList_map.get(docType);
                } else if (docType == 'Tax Schedule E') {
                    sdaListScheduleE = docType_sdaList_map.get(docType);
                } else if (docType == 'Tax Schedule F') {
                    sdaListScheduleF = docType_sdaList_map.get(docType);
                }
            }
        }               
    }
        
    private String getVerificationQuery() {
    
        String fieldStr = '';
        Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Verification__c.fields.getMap();
        for (Schema.SObjectField field : fieldMap.values()) {
            Schema.DescribeFieldResult dfr = field.getDescribe();
            if (dfr.isAccessible() == true) {
                if (fieldStr == '') {
                    fieldStr = dfr.getName();
                } else {
                    fieldStr += ', ' + dfr.getName();
                }
            }
        }
        
        String query = 'select ' + fieldStr + ' from Verification__c ';
        
        return query;
    }
    
    /*End Helper Methods*/
    
    /* Interface Implementation */
    public SchoolVerificationFieldsController Me {
        get { return this; }
    }

    public String getAcademicYearId() {
        return this.academicYearId;
    }

    public void setAcademicYearId(String academicYearIdParam) {
        this.academicYearId = academicYearIdParam;
    }

    public PageReference SchoolAcademicYearSelector_OnChange(Boolean saveRecord) {
        return null;
    }

    // on change of academic year, reload page with new folder
    public PageReference SchoolAcademicYearSelectorStudent_OnChange(PageReference newPage) {
        return newPage;
    }
    /* End Interface Implementation */
}