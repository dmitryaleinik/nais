public without sharing class UnusualConditionsWrapper {

    /*Initialization*/
    public UnusualConditionsWrapper(string title, string descField, string titleField, string sourceField, string actionField) {
        this.title = title;
        this.descField = descField;
        this.sourceField = sourceField;
        this.actionField = actionField;
        this.titleField = titleField;
        exist = false;
    } 

    /*End Initialization*/
   
    /*Properties*/
    //public Static Map<String, UnusualConditionsWrapper> pfs_UC_map {get; set;}
    
    public string title {get; set;}
    public string codeField {get; set;}
    public string descField {get; set;}
    public string noteField {get; set;}
    public string sourceField {get; set;}
    public string actionField {get; set;}
    public string titleField {get; set;}
    public Object codeValue {get; set;}
    public Object descValue {get; set;}
    public Object noteValue {get; set;}
    public Object sourceValue {get; set;}
    public Object actionValue {get; set;}
    public Object titleValue {get; set;}
    public Boolean exist {get; set;}
    
    /*End Properties*/
       
    /*Action Methods*/
   
    /*End Action Methods*/
   
    /*Helper Methods*/
    public static String getUnusualConditionsQuery() {
    
        String fieldStr = '';
        Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Unusual_Conditions__c.fields.getMap();
        for (Schema.SObjectField field : fieldMap.values()) {
            Schema.DescribeFieldResult dfr = field.getDescribe();
            if (dfr.isAccessible() == true) {
                if (fieldStr == '') {
                    fieldStr = dfr.getName();
                } else {
                    fieldStr += ', ' + dfr.getName();
                }
            }
        }
        
        String query = 'select Academic_Year__r.Name, ' + fieldStr + ' from Unusual_Conditions__c ';
        
        return query;
    }                
    
    // returns a new map of all UC PFS Field Names to a UnusualConditionsWrapper
    public static Map<String, UnusualConditionsWrapper> getNewPFSFieldNameToUCFieldNamesMap(){
        //if (pfs_UC_map == null || pfs_UC_map.isEmpty()){
            Map<String, UnusualConditionsWrapper> pfs_UC_map = new Map <String, UnusualConditionsWrapper>();    
            
            pfs_UC_map.put('Business_Not_Indicated__c', 
                new UnusualConditionsWrapper(
                    'Business Not Indicated', 
                    'Business_Not_Indicated_Description__c',
                    'Business_Not_Indicated_Title__c',
                    'Business_Not_Indicated_Source__c', 
                    'Business_Not_Indicated_Possible_Actions__c'));
            pfs_UC_map.put('High_Debt__c', 
                new UnusualConditionsWrapper(
                    'High Debt', 
                    'High_Debt_Description__c',
                    'High_Debt_Title__c',
                    'High_Debt_Source__c',
                    'High_Debt_Possible_Actions__c'));
            pfs_UC_map.put('High_Dividends_Interest__c', 
                new UnusualConditionsWrapper(
                    'High Dividends Interest', 
                    'High_Dividends_Interest_Description__c',
                    'High_Dividends_Interest_Title__c',
                    'High_Dividends_Interest_Source__c',
                    'High_Dividends_Interest_Possible_Actions__c'));
            pfs_UC_map.put('High_Income_Adjustments__c', 
                new UnusualConditionsWrapper(
                    'High Income Adjustments', 
                    'High_Income_Adjustments_Description__c',
                    'High_Income_Adjustments_Title__c',
                    'High_Income_Adjustments_Source__c',
                    'High_Income_Adjustments_Possible_Actions__c'));
            pfs_UC_map.put('High_Medical_Expense__c', 
                new UnusualConditionsWrapper(
                    'High Medical Expense', 
                    'High_Medical_Expense_Description__c',
                    'High_Medical_Expense_Title__c',
                    'High_Medical_Expense_Source__c',
                    'High_Medical_Expense_Possible_Actions__c'));
            pfs_UC_map.put('High_Student_Assets__c', 
                new UnusualConditionsWrapper(
                    'Student Assets', 
                    'Student_Assets_Description__c',
                    'Student_Assets_Title__c',
                    'Student_Assets_Source__c',
                    'Student_Assets_Possible_Actions__c'));
            pfs_UC_map.put('High_Unusual_Expenses__c', 
                new UnusualConditionsWrapper(
                    'High Unusual Expenses', 
                    'High_Unusual_Expenses_Description__c',
                    'High_Unusual_Expenses_Title__c',
                    'High_Unusual_Expenses_Source__c',
                    'High_Unusual_Expenses_Possible_Actions__c'));
            pfs_UC_map.put('Home_Equity_Capped__c', 
                new UnusualConditionsWrapper(
                    'Home Equity Capped', 
                    'Home_Equity_Capped_Description__c',
                    'Home_Equity_Capped_Title__c',
                    'Home_Equity_Capped_Source__c',
                    'Home_Equity_Capped_Possible_Actions__c'));
            pfs_UC_map.put('Income_Tax_High__c', 
                new UnusualConditionsWrapper(
                    'Income Tax High', 
                    'Income_Tax_High_Description__c',
                    'Income_Tax_High_Title__c',
                    'Income_Tax_High_Source__c',
                    'Income_Tax_High_Possible_Actions__c'));
            pfs_UC_map.put('Income_Tax_Low__c', 
                new UnusualConditionsWrapper(
                    'Income Tax Low', 
                    'Income_Tax_Low_Description__c',
                    'Income_Tax_Low_Title__c',
                    'Income_Tax_Low_Source__c',
                    'Income_Tax_Low_Possible_Actions__c'));
            pfs_UC_map.put('Income_Zero__c', 
                new UnusualConditionsWrapper(
                    'Income Zero', 
                    'Income_Zero_Description__c',
                    'Income_Zero_Title__c',
                    'Income_Zero_Source__c',
                    'Income_Zero_Possible_Actions__c'));
            pfs_UC_map.put('Large_Income_Change__c', 
                new UnusualConditionsWrapper(
                    'Large Income Change', 
                    'Large_Income_Change_Description__c',
                    'Large_Income_Change_Title__c',
                    'Large_Income_Change_Source__c',
                    'Large_Income_Change_Possible_Actions__c'));
            pfs_UC_map.put('Low_Dividends_Interest__c', 
                new UnusualConditionsWrapper(
                    'Low Dividends Interest', 
                    'Low_Dividends_Interest_Description__c',
                    'Low_Dividends_Interest_Title__c',
                    'Low_Dividends_Interest_Source__c',
                    'Low_Dividends_Interest_Possible_Actions__c'));
            pfs_UC_map.put('Multiple_Tuitions__c', 
                new UnusualConditionsWrapper(
                    'Multiple Tuitions', 
                    'Multiple_Tuitions_Description__c',
                    'Multiple_Tuitions_Title__c',
                    'Multiple_Tuitions_Source__c',
                    'Multiple_Tuitions_Possible_Actions__c'));
            pfs_UC_map.put('No_Income_Tax__c', 
                new UnusualConditionsWrapper(
                    'No Income_Tax', 
                    'No_Income_Tax_Description__c',
                    'No_Income_Tax_Title__c',
                    'No_Income_Tax_Source__c',
                    'No_Income_Tax_Possible_Actions__c'));
            pfs_UC_map.put('Other_Taxable_Income_Negative__c', 
                new UnusualConditionsWrapper(
                    'Other Taxable Income Negative', 
                    'Other_Taxable_Income_Negative_Descrip__c',
                    'Other_Taxable_Income_Negative_Title__c',
                    'Other_Taxable_Income_Negative_Source__c',
                    'Other_Taxable_Inc_Neg_Possible_Actions__c'));
            //}
        return pfs_UC_map;
    }
    /*End Helper Methods*/


}