@isTest
private class FinancialAidApplications_v1_REST_Test {

    private static final String REQUEST_URI = '/financialaid/v1/applications/';

    private static Annual_Setting__c annualSetting;
    private static Academic_Year__c currentAcademicYear;
    private static User schoolPortalUser;
    private static Student_Folder__c studentFolder;
    private static Student_Folder__c studentFolderTwo;
    private static String schoolId;
    private static String secondSchoolId;

    private static void assertApplicationHasLinks(FinancialAidApplications_v1_REST.Application applicationToCheck) {
        System.assert(!applicationToCheck.links.isEmpty(), 'Expected the application to have related links in the links property.');
        System.assertEquals(FinancialAidApplications_v1_REST.LINK_REL_FINANCIAL_AID, applicationToCheck.links[0].rel, 'Expected the first link to be for the financial aid page.');
        System.assertNotEquals(null, applicationToCheck.links[0].href, 'Expected the first link to actually have a url that is not null.');
    }

    @isTest
    static void getApplications_withSingleSchoolUser_withNoParams_applicationsFound() {
        
        setupTestData('2017-2018', false, true);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = REQUEST_URI;
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        System.runAs(schoolPortalUser) {
            FinancialAidApplications_v1_REST.getFinancialApplications();
            FinancialAidApplications_v1_REST.GET_Response response = (FinancialAidApplications_v1_REST.GET_Response)JSON.deserialize(RestContext.response.responseBody.toString(), FinancialAidApplications_v1_REST.GET_Response.class);
            List<FinancialAidApplications_v1_REST.Application> applications = response.applications;
            List<FinancialAidApplications_v1_REST.School> schools = response.schools;

            System.assertEquals(FinancialAidApplications_v1_REST.SUCCESSFUL_REQUEST_APPLICATIONS, response.message, 'API Message wasnt successful');
            System.assertEquals(schoolId, schools[0].id, 'School of user wasnt returned from API');
            System.assertEquals(studentFolder.id, applications[0].id, 'Student folder wasnt returned from API');
            System.assertEquals(200, RestContext.response.statusCode, 'API status should be successful 200');
        }

        Test.stopTest();
    }

    @isTest
    static void getApplications_withSingleSchoolUser_withAcademicYear_applicationsFound() {
        setupTestData('2016-2017', false);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = REQUEST_URI;
        req.httpMethod = 'GET';
        req.addParameter(FinancialAidApplications_v1_REST.ACADEMIC_YEAR_REQUEST_PARAM, '2016-2017');

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        System.runAs(schoolPortalUser) {
            FinancialAidApplications_v1_REST.getFinancialApplications();
            FinancialAidApplications_v1_REST.GET_Response response = (FinancialAidApplications_v1_REST.GET_Response)JSON.deserialize(RestContext.response.responseBody.toString(), FinancialAidApplications_v1_REST.GET_Response.class);
            List<FinancialAidApplications_v1_REST.Application> applications = response.applications;
            List<FinancialAidApplications_v1_REST.School> schools = response.schools;

            System.assertEquals(FinancialAidApplications_v1_REST.SUCCESSFUL_REQUEST_APPLICATIONS, response.message,'API Message wasnt successful');
            System.assertEquals(schoolId, schools[0].id, 'School of user wasnt returned from API');
            System.assertEquals(studentFolder.id, applications[0].id, 'Student folder wasnt returned from API');
            System.assertEquals(200, RestContext.response.statusCode, 'API status should be successful 200');

        }

        Test.stopTest();
    }

    @isTest
    static void getApplication_withSingleSchoolUser_withSchoolId_applicationsFound() {
        
        setupTestData('2017-2018', false, true);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = REQUEST_URI;
        req.httpMethod = 'GET';
        req.addParameter(FinancialAidApplications_v1_REST.SCHOOL_ID_REQUEST_PARAM, schoolId);

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        System.runAs(schoolPortalUser) {
            FinancialAidApplications_v1_REST.getFinancialApplications();
            FinancialAidApplications_v1_REST.GET_Response response = (FinancialAidApplications_v1_REST.GET_Response)JSON.deserialize(RestContext.response.responseBody.toString(), FinancialAidApplications_v1_REST.GET_Response.class);
            List<FinancialAidApplications_v1_REST.Application> applications = response.applications;
            List<FinancialAidApplications_v1_REST.School> schools = response.schools;

            System.assertEquals(FinancialAidApplications_v1_REST.SUCCESSFUL_REQUEST_APPLICATIONS, response.message, 'API Message wasnt successful');
            System.assertEquals(studentFolder.id, applications[0].id, 'Student folder wasnt returned from API');
            System.assertEquals(200, RestContext.response.statusCode, 'API status should be successful 200');
        }

        Test.stopTest();
    }

    @isTest
    static void getApplication_withSingleSchoolUser_withNonAccessSchoolId_noApplicationFound() {
        setupTestData('2016-2017', false);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = REQUEST_URI;
        req.httpMethod = 'GET';
        req.addParameter(FinancialAidApplications_v1_REST.SCHOOL_ID_REQUEST_PARAM, secondSchoolId);

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        System.runAs(schoolPortalUser) {
            FinancialAidApplications_v1_REST.getFinancialApplications();
            FinancialAidApplications_v1_REST.GET_Response response = (FinancialAidApplications_v1_REST.GET_Response)JSON.deserialize(RestContext.response.responseBody.toString(), FinancialAidApplications_v1_REST.GET_Response.class);
            List<FinancialAidApplications_v1_REST.Application> applications = response.applications;
            List<FinancialAidApplications_v1_REST.School> schools = response.schools;

            System.assertEquals(FinancialAidApplications_v1_REST.USER_ACCESS_ERROR, response.message, 'API Message wasnt showing access error.');
            System.assertEquals(403, RestContext.response.statusCode, 'API status code should be 403 when user can not access the specified school.');

        }

        Test.stopTest();
    }

    @isTest
    static void getApplication_withSingleSchoolUser_withApplicationId_singleApplicationFound() {
        setupTestData('2016-2017', false);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = REQUEST_URI;
        req.httpMethod = 'GET';
        req.addParameter(FinancialAidApplications_v1_REST.APPLICATION_ID_REQUEST_PARAM, studentFolder.id);

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        System.runAs(schoolPortalUser) {
            FinancialAidApplications_v1_REST.getFinancialApplications();
            FinancialAidApplications_v1_REST.GET_Response response = (FinancialAidApplications_v1_REST.GET_Response)JSON.deserialize(RestContext.response.responseBody.toString(), FinancialAidApplications_v1_REST.GET_Response.class);
            List<FinancialAidApplications_v1_REST.Application> applications = response.applications;
            List<FinancialAidApplications_v1_REST.School> schools = response.schools;

            System.assertEquals(FinancialAidApplications_v1_REST.SUCCESSFUL_REQUEST_APPLICATIONS, response.message, 'API Message should be successfully retrieved applications');
            System.assertEquals(studentFolder.id, applications[0].id, 'Student folder wasnt returned from API');
            System.assertEquals(200, RestContext.response.statusCode, 'API status should be successful 200');
        }

        Test.stopTest();
    }

    @isTest
    static void getApplication_withSingleSchoolUser_withInvalidApplicationId_noApplicationFound() {
        setupTestData('2017-2018', true);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = REQUEST_URI;
        req.httpMethod = 'GET';
        req.addParameter(FinancialAidApplications_v1_REST.APPLICATION_ID_REQUEST_PARAM, studentFolderTwo.Id);

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        System.runAs(schoolPortalUser) {
            FinancialAidApplications_v1_REST.getFinancialApplications();
            FinancialAidApplications_v1_REST.GET_Response response = (FinancialAidApplications_v1_REST.GET_Response)JSON.deserialize(RestContext.response.responseBody.toString(), FinancialAidApplications_v1_REST.GET_Response.class);
            List<FinancialAidApplications_v1_REST.Application> applications = response.applications;
            List<FinancialAidApplications_v1_REST.School> schools = response.schools;

            System.assertEquals(FinancialAidApplications_v1_REST.INVALID_REQUEST_ERROR, response.message, 'API Message wasnt showing invalid params');
            System.assertEquals(400, RestContext.response.statusCode, 'API status code should be 400 for invalid params');
        }

        Test.stopTest();
    }

    @isTest
    static void getApplication_withMultiSchoolUser_withoutParamsSpecified_returnMustProvideParams() {
        setupTestData('2017-2018', true);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = REQUEST_URI;
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        System.runAs(schoolPortalUser) {
            FinancialAidApplications_v1_REST.getFinancialApplications();
            FinancialAidApplications_v1_REST.GET_Response response = (FinancialAidApplications_v1_REST.GET_Response)JSON.deserialize(RestContext.response.responseBody.toString(), FinancialAidApplications_v1_REST.GET_Response.class);
            List<FinancialAidApplications_v1_REST.Application> applications = response.applications;
            List<FinancialAidApplications_v1_REST.School> schools = response.schools;

            System.debug(response);
            System.assertEquals(FinancialAidApplications_v1_REST.MULTIPLE_SCHOOLS_FOUND, response.message, 'API Message wasnt showing multiple schools found');
            System.assertEquals(300, RestContext.response.statusCode, 'API status code should be 300 showing multiple accounts found');
        }

        Test.stopTest();

    }

    @isTest
    static void getApplication_withMultiSchoolUser_invalidSchoolIdProvided_returnInvalidParams() {
        setupTestData('2016-2017', true);

        Account schoolThree = AccountTestData.Instance
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .insertAccount();


        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = REQUEST_URI;
        req.httpMethod = 'GET';
        req.addParameter(FinancialAidApplications_v1_REST.SCHOOL_ID_REQUEST_PARAM, schoolThree.Id);

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        System.runAs(schoolPortalUser) {
            FinancialAidApplications_v1_REST.getFinancialApplications();
            FinancialAidApplications_v1_REST.GET_Response response = (FinancialAidApplications_v1_REST.GET_Response)JSON.deserialize(RestContext.response.responseBody.toString(), FinancialAidApplications_v1_REST.GET_Response.class);
            List<FinancialAidApplications_v1_REST.Application> applications = response.applications;
            List<FinancialAidApplications_v1_REST.School> schools = response.schools;

            System.assertEquals(FinancialAidApplications_v1_REST.USER_ACCESS_ERROR, response.message, 'API Message wasnt showing access error.');
            System.assertEquals(403, RestContext.response.statusCode, 'API status code should be 403 when a user can not access the specified school.');
            System.debug('Why Errors: >>> '+ response.errors);
        }
        Test.stopTest();
    }

    @isTest
    static void getApplication_withMultiSchoolUser_withValidSchoolId_applicantsReturned() {
        
        setupTestData('2017-2018', true, true);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = REQUEST_URI;
        req.httpMethod = 'GET';
        req.addParameter(FinancialAidApplications_v1_REST.SCHOOL_ID_REQUEST_PARAM, schoolId);

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        System.runAs(schoolPortalUser) {
            FinancialAidApplications_v1_REST.getFinancialApplications();
            System.debug('Request: >>> ' + RestContext.request);
            System.debug('Response: >>> ' + RestContext.response.responseBody.toString());

            FinancialAidApplications_v1_REST.GET_Response response = (FinancialAidApplications_v1_REST.GET_Response)JSON.deserialize(RestContext.response.responseBody.toString(), FinancialAidApplications_v1_REST.GET_Response.class);
            List<FinancialAidApplications_v1_REST.Application> applications = response.applications;
            List<FinancialAidApplications_v1_REST.School> schools = response.schools;

            System.assertEquals(FinancialAidApplications_v1_REST.SUCCESSFUL_REQUEST_APPLICATIONS, response.message);
            System.assertEquals(studentFolder.id, applications[0].id);
            assertApplicationHasLinks(applications[0]);
            System.assertEquals(200, RestContext.response.statusCode);
        }

        Test.stopTest();
    }

    static void getApplication_withMultiSchoolUser_withValidSchoolIdAndAcademicYear_applicantsReturned() {
        setupTestData('2016-2017', false);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = REQUEST_URI;
        req.httpMethod = 'GET';
        req.addParameter(FinancialAidApplications_v1_REST.SCHOOL_ID_REQUEST_PARAM, schoolId);
        req.addParameter(FinancialAidApplications_v1_REST.ACADEMIC_YEAR_REQUEST_PARAM, '2016-2017');

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        System.runAs(schoolPortalUser) {
            FinancialAidApplications_v1_REST.getFinancialApplications();
            FinancialAidApplications_v1_REST.GET_Response response = (FinancialAidApplications_v1_REST.GET_Response)JSON.deserialize(RestContext.response.responseBody.toString(), FinancialAidApplications_v1_REST.GET_Response.class);
            List<FinancialAidApplications_v1_REST.Application> applications = response.applications;
            List<FinancialAidApplications_v1_REST.School> schools = response.schools;

            System.assertEquals(FinancialAidApplications_v1_REST.SUCCESSFUL_REQUEST_APPLICATIONS, response.message, 'API Message should be successfully retrieved applications');
            System.assertEquals(studentFolder.id, applications[0].id, 'Student folder wasnt returned from API');
            System.assertEquals(200, RestContext.response.statusCode, 'API Status should be successful 200');
        }

        Test.stopTest();

    }

    @isTest
    static void putApplications_withSingleSchoolUser_withValidApplications_applicationsUpdated () {

        setupTestData('2017-2018', false);
        studentFolder.Admission_Enrollment_Status__c = 'Accepted';
        List<FinancialAidApplications_v1_REST.Application> applications = new List<FinancialAidApplications_v1_REST.Application>();
        applications.add(new FinancialAidApplications_v1_REST.Application(studentFolder));

        FinancialAidApplications_v1_REST.PUT_Request request = new FinancialAidApplications_v1_REST.PUT_Request();
        request.applications = applications;

        RestRequest req = new RestRequest();
        req.requestBody = Blob.valueOf(JSON.serialize(request));
        req.requestURI = REQUEST_URI;
        req.httpMethod = 'PUT';

        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        FinancialAidApplications_v1_REST.putFinancialApplicationStatus();

        System.assertEquals(200, RestContext.response.statusCode, 'Should return 200 for successful Student Folder update');

        List<Student_Folder__c> updatedFolders = getStudentFoldersById(new Set<String>{studentFolder.Id});

        for (Student_Folder__c folder : updatedFolders) {
            System.assertEquals('Accepted', folder.Admission_Enrollment_Status__c, 'Each Application should have enrollment status of accepted');
        }
    }

    @isTest
    static void putApplications_withSingleSchoolURser_withInvalidApplicationStatus_applicationsNotUpdated () {

        setupTestData('2017-2018', false);
        studentFolder.Admission_Enrollment_Status__c = 'Applied';
        List<FinancialAidApplications_v1_REST.Application> applications = new List<FinancialAidApplications_v1_REST.Application>();
        applications.add(new FinancialAidApplications_v1_REST.Application(studentFolder));

        FinancialAidApplications_v1_REST.PUT_Request request = new FinancialAidApplications_v1_REST.PUT_Request();
        request.applications = applications;

        RestRequest req = new RestRequest();
        req.requestBody = Blob.valueOf(JSON.serialize(request));
        req.requestURI = REQUEST_URI;
        req.httpMethod = 'PUT';

        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        FinancialAidApplications_v1_REST.putFinancialApplicationStatus();

        System.assertEquals(400, RestContext.response.statusCode, 'Should return 400 for unsuccessful Student Folder update');
    }

    @isTest
    static void getApplications_withSingleSchoolUser_integrationLogsCreated() {
        setupTestData('2016-2017', false);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = REQUEST_URI;
        req.httpMethod = 'GET';
        req.addParameter(FinancialAidApplications_v1_REST.APPLICATION_ID_REQUEST_PARAM, studentFolder.id);

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        System.runAs(schoolPortalUser) {
            FinancialAidApplications_v1_REST.getFinancialApplications();
            FinancialAidApplications_v1_REST.GET_Response response = (FinancialAidApplications_v1_REST.GET_Response)JSON.deserialize(RestContext.response.responseBody.toString(), FinancialAidApplications_v1_REST.GET_Response.class);
            List<FinancialAidApplications_v1_REST.Application> applications = response.applications;
            List<FinancialAidApplications_v1_REST.School> schools = response.schools;

            List<IntegrationLog__c> logs = getIntegrationLogs();
            System.assertEquals('GET', logs[0].Http_Method__c, 'Integration Log Http Method incorrect');
            System.assertEquals('Financial Aid API', logs[0].Source__c, 'Integration Log source incorrect');
            System.assertEquals('/financialaid/v1/applications/', logs[0].Endpoint__c, 'Integration log not showing correct endpoint');
        }

        Test.stopTest();
    }

    @isTest
    static void putApplications_withSingleSchoolURser_withInvalidApplicationStatus_integrationLogShowsStatus () {

        setupTestData('2017-2018', false);
        studentFolder.Admission_Enrollment_Status__c = 'Applied';
        List<FinancialAidApplications_v1_REST.Application> applications = new List<FinancialAidApplications_v1_REST.Application>();
        applications.add(new FinancialAidApplications_v1_REST.Application(studentFolder));

        FinancialAidApplications_v1_REST.PUT_Request request = new FinancialAidApplications_v1_REST.PUT_Request();
        request.applications = applications;

        RestRequest req = new RestRequest();
        req.requestBody = Blob.valueOf(JSON.serialize(request));
        req.requestURI = REQUEST_URI;
        req.httpMethod = 'PUT';

        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        System.runAs(schoolPortalUser) {
            FinancialAidApplications_v1_REST.putFinancialApplicationStatus();

            List<IntegrationLog__c> logs = getIntegrationLogs();
            System.assertEquals('PUT', logs[0].Http_Method__c, 'Integration Log Http Method incorrect');
            System.assertEquals('Financial Aid API', logs[0].Source__c, 'Integration Log source incorrect');
            System.assertEquals('/financialaid/v1/applications/', logs[0].Endpoint__c, 'Integration log not showing correct endpoint');
            System.assertEquals(400, logs[0].Response_Code__c, 'Integration Log response code is incorrect');
        }
        Test.stopTest();
    }

    static List<IntegrationLog__c> getIntegrationLogs() {
        return [Select Id,
                Http_Method__c,
                Source__c,
                Endpoint__c,
                Response_Code__c
        FROM IntegrationLog__c];
    }

    @isTest
    static void putApplication_withSingleSchoolUser_integrationLogsCreated() {
        setupTestData('2017-2018', false);
        studentFolder.Admission_Enrollment_Status__c = 'Accepted';
        List<FinancialAidApplications_v1_REST.Application> applications = new List<FinancialAidApplications_v1_REST.Application>();
        applications.add(new FinancialAidApplications_v1_REST.Application(studentFolder));

        FinancialAidApplications_v1_REST.PUT_Request request = new FinancialAidApplications_v1_REST.PUT_Request();
        request.applications = applications;

        RestRequest req = new RestRequest();
        req.requestBody = Blob.valueOf(JSON.serialize(request));
        req.requestURI = REQUEST_URI;
        req.httpMethod = 'PUT';

        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        FinancialAidApplications_v1_REST.putFinancialApplicationStatus();

        System.assertEquals(200, RestContext.response.statusCode, 'Should return 200 for successful Student Folder update');

        List<Student_Folder__c> updatedFolders = getStudentFoldersById(new Set<String>{studentFolder.Id});

        for (Student_Folder__c folder : updatedFolders) {
            System.assertEquals('Accepted', folder.Admission_Enrollment_Status__c, 'Each Application should have enrollment status of accepted');
        }
    }

    static List<Student_Folder__c> getStudentFoldersById(Set<String> ids) {
        return [Select Id,
                Admission_Enrollment_Status__c
        FROM Student_Folder__c
        WHERE Id in :ids];
    }

    static void setupTestData(String customAcademicYear, Boolean multiSchool) {
        setupTestData(customAcademicYear, multiSchool, false);
    }
    
    static void setupTestData(String customAcademicYear, Boolean multiSchool, Boolean customAcademicYearIsCurrent) {

        Id academicYearId;
        Annual_Setting__c pastAnnualSetting;

        // First school
        Account school = AccountTestData.Instance
                .asSchool()
                .DefaultAccount;

        schoolId = school.Id;


        // Staff contact
        Contact schoolStuff = ContactTestData.Instance
                .forAccount(school.Id)
                .asSchoolStaff()
                .DefaultContact;

        // Create staff user
        System.runAs(CurrentUser.getCurrentUser()) {
            schoolPortalUser = UserTestData.createSchoolPortalUser();
            schoolPortalUser.In_Focus_School__c = school.Id;
            insert schoolPortalUser;
        }

        // Sharing account with user
        insertAccountShare(school.Id, schoolPortalUser.Id);

        // Second school, used as possible invalid Id or for multiple school cases
        Account schoolTwo = AccountTestData.Instance
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .insertAccount();

        secondSchoolId = schoolTwo.Id;

        // Create Affiliation and second application when testing multiple school cases
        if (multiSchool) {

            studentFolderTwo = StudentFolderTestData.Instance
                    .forSchoolId(secondSchoolId)
                    .forAcademicYearPicklist(customAcademicYear)
                    .insertStudentFolder();

            Affiliation__c affiliation = AffiliationTestData.Instance
                    .forType('Additional School User')
                    .forStatus('Current')
                    .forOrganizationId(schoolTwo.Id)
                    .createAffiliationWithoutReset();

            insert affiliation;
        }


        List<Academic_Year__c> accYears = AcademicYearTestData.Instance.insertAcademicYears(5);
        
        if (customAcademicYearIsCurrent) {
            
            Academic_Year__c currentAcademicYearVar = GlobalVariables.getAcademicYearByName(customAcademicYear);
            
            String previousAcademicYearName = GlobalVariables.getPreviousAcademicYearStr(currentAcademicYearVar.Name);
        
            Academic_Year__c previousAcademicYearVar = GlobalVariables.getAcademicYearByName(previousAcademicYearName);
        
            AcademicYearService.Instance.testCurrentAcademicYear = currentAcademicYearVar;
            
            AcademicYearService.Instance.testPreviousAcademicYear = previousAcademicYearVar;
        
        }
        
        Academic_Year__c currentAcademicYear = GlobalVariables.getCurrentAcademicYear();
        academicYearId = currentAcademicYear.Id;
        String academicYearName = currentAcademicYear.Name;

        // Create student for application
        Contact student = ContactTestData.Instance
                .forRecordTypeId(RecordTypes.studentContactTypeId)
                .create();
        insert student;

        // Create applicants/pfs for Student Folder
        System.runAs(schoolPortalUser) {
            PFS__c pfs = PfsTestData.Instance
                    .forParentA(schoolStuff.Id)
                    .createPfsWithoutReset();
            Dml.WithoutSharing.insertObjects(new List<PFS__c>{ pfs });

            List<Applicant__c> applicants = new List<Applicant__c> {
                    ApplicantTestData.Instance.createApplicantWithoutReset(),
                    ApplicantTestData.Instance.createApplicantWithoutReset(),
                    ApplicantTestData.Instance.createApplicantWithoutReset()
            };

            Dml.WithoutSharing.insertObjects(applicants);

            studentFolder = StudentFolderTestData.Instance
                    .forStudentId(student.Id)
                    .forSchoolId(school.Id)
                    .forAcademicYearPicklist(customAcademicYear)
                    .createStudentFolderWithoutReset();

            studentFolder.RavennaId__c = '123456789';
            studentFolder.Loan__c = 90.00;
            studentFolder.Grant_Awarded__c = 75.00;
            studentFolder.Admission_Enrollment_Status__c = 'Never Applied';

            Dml.WithoutSharing.insertObjects(new List<Student_Folder__c>{ studentFolder });

            Id academicYearPastId = GlobalVariables.getPreviousAcademicYear().Id;
            academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

            List<Annual_Setting__c> annualSettings = new List<Annual_Setting__c> {
                    AnnualSettingsTestData.Instance
                            .forSchoolId(school.Id)
                            .forAcademicYearId(academicYearPastId)
                            .createAnnualSettingsWithoutReset(),
                    AnnualSettingsTestData.Instance
                            .forSchoolId(school.Id)
                            .forAcademicYearId(academicYearId)
                            .createAnnualSettingsWithoutReset()
            };
            Dml.WithoutSharing.insertObjects(annualSettings);

            pastAnnualSetting = annualSettings[0];

            List<School_PFS_Assignment__c> pfsAssignments = new List<School_PFS_Assignment__c>();

            for (Integer i = 0; i < 3; i++) {
                pfsAssignments.add(
                        SchoolPfsAssignmentTestData.Instance
                                .forSchoolId(school.Id)
                                .forStudentFolderId(studentFolder.Id)
                                .forAcademicYearPicklist(academicYearName)
                                .forApplicantId(applicants[i].Id)
                                .createSchoolPfsAssignmentWithoutReset()
                );
            }

            Dml.WithoutSharing.insertObjects(pfsAssignments);
        }

        // Share pfs records with User
        List<School_PFS_Assignment__c> pfsAssignmentsForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(pfsAssignmentsForSharing, null);

    }

    private static void insertAccountShare(Id accountId, Id userId) {
        insert new AccountShare(
                AccountId = AccountId,
                CaseAccessLevel = 'Edit',
                OpportunityAccessLevel = 'Edit',
                AccountAccessLevel = 'Edit',
                UserOrGroupId = userId
        );
    }

}