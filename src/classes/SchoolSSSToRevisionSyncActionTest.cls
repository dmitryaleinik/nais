@isTest
private class SchoolSSSToRevisionSyncActionTest {
    private static final String[] schoolPFSAssignmentFieldNames = ExpCore.GetAllFieldNames('School_PFS_Assignment__c');
    
    private static School_PFS_Assignment__c querySPA(Id spaId) {
        School_PFS_Assignment__c theSPA = null;
        String queryString = 'SELECT ' + String.join(schoolPFSAssignmentFieldNames, ',')
            + ' FROM School_PFS_Assignment__c WHERE Id=:spaId LIMIT 1';
        theSPA = Database.query(queryString);
        return theSPA;
    }

    // generic method to query inserted/updated SBFAs
    private static List<School_Biz_Farm_Assignment__c> querySchoolBizFarms(){
        return [
            SELECT Id, Business_Entity_Type__c, Business_Farm__c, School_PFS_Assignment__c, Business_Farm_Assets__c, Business_Farm_Debts__c,
                Business_Farm_Owner__c, Business_Farm_Ownership_Percent__c, Business_Farm_Share__c,
                Orig_Business_Entity_Type__c, Orig_Business_Farm_Assets__c, Orig_Business_Farm_Debts__c, Orig_Business_Farm_Owner__c,
                Orig_Business_Farm_Ownership_Percent__c, Orig_Business_Farm_Share__c,
                School_PFS_Assignment__r.Family_May_Submit_Updates__c, Orig_Net_Profit_Loss_Business_Farm__c,
                Net_Profit_Loss_Business_Farm__c, Business_Farm__r.Business_Farm_Assets__c,
                Business_Farm__r.Business_Farm_Debts__c, Business_Farm__r.Business_Farm_Owner__c,
                Business_Farm__r.Business_Farm_Ownership_Percent__c, Business_Farm__r.Business_Net_Profit_Share_Current__c
            FROM School_Biz_Farm_Assignment__c];
    }

    @isTest
    private static void testSyncOrigToRevisionFields() {
        // set up test data
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        Account family = TestUtils.createAccount('Individual Account', RecordTypes.individualAccountTypeId, 5, true);
        Contact parent = TestUtils.createContact('Test Parent', family.Id, RecordTypes.parentContactTypeId, true);
        Contact student = TestUtils.createContact('Test Student', family.Id, RecordTypes.studentContactTypeId, true);
        PFS__c pfs = TestUtils.createPFS('Test PFS', academicYear.Id, parent.Id, true);
        Applicant__c applicant = TestUtils.createApplicant(student.Id, pfs.Id, true);
        Account school = TestUtils.createAccount('Test School', RecordTypes.schoolAccountTypeId, 5, true);
        Student_Folder__c studentFolder = TestUtils.createStudentFolder('Test Student Folder', academicYear.Id, student.Id, true);
        School_PFS_Assignment__c spa = new School_PFS_Assignment__c();
        spa.Student_Folder__c = studentFolder.Id;
        spa.School__c = school.Id;
        spa.Academic_Year_Picklist__c = academicYear.Name;
        spa.Applicant__c = applicant.Id;
        
        // set values for orig fields
        spa.Orig_Salary_Wages_Parent_A__c = 111;
        spa.Orig_Salary_Wages_Parent_B__c = 111;
        spa.Orig_Interest_Income__c = 111;
        spa.Orig_Dividend_Income__c = 111;
        spa.Orig_Alimony_Received__c = 111;
        //spa.Orig_Net_Profit_Loss_Business_Farm__c = 111;
        spa.Orig_Taxable_Refunds__c = 111;
        spa.Orig_Capital_Gain_Loss__c = 111;
        spa.Orig_Other_Gain_Loss__c = 111;
        spa.Orig_IRA_Distribution__c = 111;
        spa.Orig_Pensions_and_Annuities__c = 111;
        spa.Orig_Rental_Real_Estate_Trusts_etc__c = 111;
        spa.Orig_Unemployment_Compensation__c = 111;
        spa.Orig_Social_Security_Benefits_Taxable__c = 111;
        spa.Orig_Other_Income__c = 111;
        spa.Orig_Deductible_Part_Self_Employment_Tax__c = 111;
        spa.Orig_Keogh_Plan_Payment__c = 111;
        spa.Orig_Untaxed_IRA_Plan_Payment__c = 111;
        spa.Orig_Child_Support_Received__c = 111;
        spa.Orig_Social_Security_Benefits__c = 111;
        spa.Orig_Pre_Tax_Payment_to_Retirement_Plans__c = 111;
        spa.Orig_Pre_Tax_Fringe_Benefit_Contribution__c = 111;
        spa.Orig_Cash_Support_Gift_Income__c = 111;
        spa.Orig_Other_Support_from_Divorced_Spouse__c = 111;
        spa.Orig_Food_Housing_Other_Allowance__c = 111;
        spa.Orig_Earned_Income_Credits__c = 111;
        spa.Orig_Welfare_Veterans_and_Workers_Comp__c = 111;
        spa.Orig_Tax_Exempt_Investment_Income__c = 111;
        spa.Orig_Income_Earned_Abroad__c = 111;
        spa.Orig_Other_Untaxed_Income__c = 111;
        spa.Orig_Current_Tax_Return_Status__c = 'Completed';
        spa.Orig_Filing_Status__c = EfcPicklistValues.FILING_STATUS_SINGLE;
        spa.Orig_Income_Tax_Exemptions__c = 1;
        spa.Orig_Itemized_Deductions__c = 1;
        spa.Orig_Total_SE_Tax_Paid__c = 111;
        spa.Orig_Parent_State__c = 'California';
        spa.Orig_Medical_Dental_Expense__c = 111;
        spa.Orig_Unusual_Expense__c = 111;
        spa.Orig_Home_Purchase_Year__c = '2001';
        spa.Orig_Home_Purchase_Price__c = 111;
        spa.Orig_Home_Market_Value__c = 111;
        spa.Orig_Unpaid_Principal_1st_Mortgage__c = 111;
        spa.Orig_Unpaid_Principal_2nd_Mortgage__c = 111;
        spa.Orig_Other_Real_Estate_Market_Value__c = 111;
        spa.Orig_Other_Real_Estate_Unpaid_Principal__c = 111;
        spa.Orig_Business_Farm_Owner__c = EfcPicklistValues.BUSINESS_FARM_OWNER_PARENT_A;
        //spa.Orig_Business_Farm_Ownership_Percent__c = 111;
        //spa.Orig_Business_Farm_Assets__c = 111;
        //spa.Orig_Business_Farm_Debts__c = 111;
        spa.Orig_Bank_Accounts__c = 111;
        spa.Orig_Investments__c = 111;
        spa.Orig_Total_Debts__c = 111;
        spa.Orig_Dependent_Children__c = 1;
        spa.Orig_Family_Size__c = 5;
        spa.Orig_Num_Children_in_Tuition_Schools__c = 1;
        spa.Orig_Student_Assets__c = 111;
        spa.Orig_Grade_Applying__c = '1';
        
        // insert the record
        insert spa;
        
        // reload the SPA record
        spa = querySPA(spa.Id);
        
        // check that the revision fields are set
        System.assertEquals(111, spa.Salary_Wages_Parent_A__c);
        System.assertEquals(111, spa.Salary_Wages_Parent_B__c);
        System.assertEquals(111, spa.Interest_Income__c);
        System.assertEquals(111, spa.Dividend_Income__c);
        System.assertEquals(111, spa.Alimony_Received__c);
        //System.assertEquals(111, spa.Net_Profit_Loss_Business_Farm__c);
        System.assertEquals(111, spa.Taxable_Refunds__c);
        System.assertEquals(111, spa.Capital_Gain_Loss__c);
        System.assertEquals(111, spa.Other_Gain_Loss__c);
        System.assertEquals(111, spa.IRA_Distribution__c);
        System.assertEquals(111, spa.Pensions_and_Annuities__c);
        System.assertEquals(111, spa.Rental_Real_Estate_Trusts_etc__c);
        System.assertEquals(111, spa.Unemployment_Compensation__c);
        System.assertEquals(111, spa.Social_Security_Benefits_Taxable__c);
        System.assertEquals(111, spa.Other_Income__c);
        System.assertEquals(111, spa.Deductible_Part_of_Self_Employment_Tax__c);
        System.assertEquals(111, spa.Keogh_Plan_Payment__c);
        System.assertEquals(111, spa.Untaxed_IRA_Plan_Payment__c);
        System.assertEquals(111, spa.Child_Support_Received__c);
        System.assertEquals(111, spa.Social_Security_Benefits__c);
        System.assertEquals(111, spa.Pre_Tax_Payments_to_Retirement_Plans__c);
        System.assertEquals(111, spa.Pre_Tax_Fringe_Benefit_Contribution__c);
        System.assertEquals(111, spa.Cash_Support_Gift_Income__c);
        System.assertEquals(111, spa.Other_Support_from_Divorced_Spouse__c);
        System.assertEquals(111, spa.Food_Housing_Other_Allowance__c);
        System.assertEquals(111, spa.Earned_Income_Credits__c);
        System.assertEquals(111, spa.Welfare_Veterans_and_Workers_Comp__c);
        System.assertEquals(111, spa.Tax_Exempt_Investment_Income__c);
        System.assertEquals(111, spa.Income_Earned_Abroad__c);
        System.assertEquals(111, spa.Other_Untaxed_Income__c);
        System.assertEquals('Completed', spa.Current_Tax_Return_Status__c);
        System.assertEquals(EfcPicklistValues.FILING_STATUS_SINGLE, spa.Filing_Status__c);
        System.assertEquals(1, spa.Income_Tax_Exemptions__c);
        System.assertEquals(1, spa.Itemized_Deductions__c);
        System.assertEquals(111, spa.Total_SE_Tax_Paid__c);
        System.assertEquals('California', spa.Parent_State__c);
        System.assertEquals(111, spa.Medical_Dental_Expense__c);
        System.assertEquals(111, spa.Unusual_Expense__c);
        System.assertEquals('2001', spa.Home_Purchase_Year__c);
        System.assertEquals(111, spa.Home_Purchase_Price__c);
        System.assertEquals(111, spa.Home_Market_Value__c);
        System.assertEquals(111, spa.Unpaid_Principal_1st_Mortgage__c);
        System.assertEquals(111, spa.Unpaid_Principal_2nd_Mortgage__c);
        System.assertEquals(111, spa.Other_Real_Estate_Market_Value__c);
        System.assertEquals(111, spa.Other_Real_Estate_Unpaid_Principal__c);
        System.assertEquals(EfcPicklistValues.BUSINESS_FARM_OWNER_PARENT_A, spa.Business_Farm_Owner__c);
        //System.assertEquals(111, spa.Business_Farm_Ownership_Percent__c);
        //System.assertEquals(111, spa.Business_Farm_Assets__c);
        //System.assertEquals(111, spa.Business_Farm_Debts__c);
        System.assertEquals(111, spa.Bank_Accounts__c);
        System.assertEquals(111, spa.Investments__c);
        System.assertEquals(111, spa.Total_Debts__c);
        System.assertEquals(1, spa.Dependent_Children__c);
        System.assertEquals(5, spa.Family_Size__c);
        System.assertEquals(1, spa.Num_Children_in_Tuition_Schools__c);
        System.assertEquals(111, spa.Student_Assets__c);
        System.assertEquals('1', spa.Grade_Applying__c);
        
        // test updating SSS fields
        spa.Orig_Salary_Wages_Parent_A__c = 222;
        spa.Orig_Salary_Wages_Parent_B__c = 222;
        spa.Orig_Interest_Income__c = 222;
        spa.Orig_Dividend_Income__c = 222;
        spa.Orig_Alimony_Received__c = 222;
        //spa.Orig_Net_Profit_Loss_Business_Farm__c = 222;
        spa.Orig_Taxable_Refunds__c = 222;
        spa.Orig_Capital_Gain_Loss__c = 222;
        spa.Orig_Other_Gain_Loss__c = 222;
        spa.Orig_IRA_Distribution__c = 222;
        spa.Orig_Pensions_and_Annuities__c = 222;
        spa.Orig_Rental_Real_Estate_Trusts_etc__c = 222;
        spa.Orig_Unemployment_Compensation__c = 222;
        spa.Orig_Social_Security_Benefits_Taxable__c = 222;
        spa.Orig_Other_Income__c = 222;
        spa.Orig_Deductible_Part_Self_Employment_Tax__c = 222;
        spa.Orig_Keogh_Plan_Payment__c = 222;
        spa.Orig_Untaxed_IRA_Plan_Payment__c = 222;
        spa.Orig_Child_Support_Received__c = 222;
        spa.Orig_Social_Security_Benefits__c = 222;
        spa.Orig_Pre_Tax_Payment_to_Retirement_Plans__c = 222;
        spa.Orig_Pre_Tax_Fringe_Benefit_Contribution__c = 222;
        spa.Orig_Cash_Support_Gift_Income__c = 222;
        spa.Orig_Other_Support_from_Divorced_Spouse__c = 222;
        spa.Orig_Food_Housing_Other_Allowance__c = 222;
        spa.Orig_Earned_Income_Credits__c = 222;
        spa.Orig_Welfare_Veterans_and_Workers_Comp__c = 222;
        spa.Orig_Tax_Exempt_Investment_Income__c = 222;
        spa.Orig_Income_Earned_Abroad__c = 222;
        spa.Orig_Other_Untaxed_Income__c = 222;
        spa.Orig_Current_Tax_Return_Status__c = 'Estimated';
        spa.Orig_Filing_Status__c = EfcPicklistValues.FILING_STATUS_MARRIED_JOINT;
        spa.Orig_Income_Tax_Exemptions__c = 2;
        spa.Orig_Itemized_Deductions__c = 2;
        spa.Orig_Total_SE_Tax_Paid__c = 222;
        spa.Orig_Parent_State__c = 'Alaska';
        spa.Orig_Medical_Dental_Expense__c = 222;
        spa.Orig_Unusual_Expense__c = 222;
        spa.Orig_Home_Purchase_Year__c = '2002';
        spa.Orig_Home_Purchase_Price__c = 222;
        spa.Orig_Home_Market_Value__c = 222;
        spa.Orig_Unpaid_Principal_1st_Mortgage__c = 222;
        spa.Orig_Unpaid_Principal_2nd_Mortgage__c = 222;
        spa.Orig_Other_Real_Estate_Market_Value__c = 222;
        spa.Orig_Other_Real_Estate_Unpaid_Principal__c = 222;
        spa.Orig_Business_Farm_Owner__c = EfcPicklistValues.BUSINESS_FARM_OWNER_PARENT_B;
        //spa.Orig_Business_Farm_Ownership_Percent__c = 222;
        //spa.Orig_Business_Farm_Assets__c = 222;
        //spa.Orig_Business_Farm_Debts__c = 222;
        spa.Orig_Bank_Accounts__c = 222;
        spa.Orig_Investments__c = 222;
        spa.Orig_Total_Debts__c = 222;
        spa.Orig_Dependent_Children__c = 2;
        spa.Orig_Family_Size__c = 7;
        spa.Orig_Num_Children_in_Tuition_Schools__c = 2;
        spa.Orig_Student_Assets__c = 222;
        spa.Orig_Grade_Applying__c = '2';
        update spa;
        
        // check that the revision fields are set
        spa = querySPA(spa.Id);
        System.assertEquals(222, spa.Salary_Wages_Parent_A__c);
        System.assertEquals(222, spa.Salary_Wages_Parent_B__c);
        System.assertEquals(222, spa.Interest_Income__c);
        System.assertEquals(222, spa.Dividend_Income__c);
        System.assertEquals(222, spa.Alimony_Received__c);
        //System.assertEquals(222, spa.Net_Profit_Loss_Business_Farm__c);
        System.assertEquals(222, spa.Taxable_Refunds__c);
        System.assertEquals(222, spa.Capital_Gain_Loss__c);
        System.assertEquals(222, spa.Other_Gain_Loss__c);
        System.assertEquals(222, spa.IRA_Distribution__c);
        System.assertEquals(222, spa.Pensions_and_Annuities__c);
        System.assertEquals(222, spa.Rental_Real_Estate_Trusts_etc__c);
        System.assertEquals(222, spa.Unemployment_Compensation__c);
        System.assertEquals(222, spa.Social_Security_Benefits_Taxable__c);
        System.assertEquals(222, spa.Other_Income__c);
        System.assertEquals(222, spa.Deductible_Part_of_Self_Employment_Tax__c);
        System.assertEquals(222, spa.Keogh_Plan_Payment__c);
        System.assertEquals(222, spa.Untaxed_IRA_Plan_Payment__c);
        System.assertEquals(222, spa.Child_Support_Received__c);
        System.assertEquals(222, spa.Social_Security_Benefits__c);
        System.assertEquals(222, spa.Pre_Tax_Payments_to_Retirement_Plans__c);
        System.assertEquals(222, spa.Pre_Tax_Fringe_Benefit_Contribution__c);
        System.assertEquals(222, spa.Cash_Support_Gift_Income__c);
        System.assertEquals(222, spa.Other_Support_from_Divorced_Spouse__c);
        System.assertEquals(222, spa.Food_Housing_Other_Allowance__c);
        System.assertEquals(222, spa.Earned_Income_Credits__c);
        System.assertEquals(222, spa.Welfare_Veterans_and_Workers_Comp__c);
        System.assertEquals(222, spa.Tax_Exempt_Investment_Income__c);
        System.assertEquals(222, spa.Income_Earned_Abroad__c);
        System.assertEquals(222, spa.Other_Untaxed_Income__c);
        System.assertEquals('Estimated', spa.Current_Tax_Return_Status__c);
        System.assertEquals(EfcPicklistValues.FILING_STATUS_MARRIED_JOINT, spa.Filing_Status__c);
        System.assertEquals(2, spa.Income_Tax_Exemptions__c);
        System.assertEquals(2, spa.Itemized_Deductions__c);
        System.assertEquals(222, spa.Total_SE_Tax_Paid__c);
        System.assertEquals('Alaska', spa.Parent_State__c);
        System.assertEquals(222, spa.Medical_Dental_Expense__c);
        System.assertEquals(222, spa.Unusual_Expense__c);
        System.assertEquals('2002', spa.Home_Purchase_Year__c);
        System.assertEquals(222, spa.Home_Purchase_Price__c);
        System.assertEquals(222, spa.Home_Market_Value__c);
        System.assertEquals(222, spa.Unpaid_Principal_1st_Mortgage__c);
        System.assertEquals(222, spa.Unpaid_Principal_2nd_Mortgage__c);
        System.assertEquals(222, spa.Other_Real_Estate_Market_Value__c);
        System.assertEquals(222, spa.Other_Real_Estate_Unpaid_Principal__c);
        System.assertEquals(EfcPicklistValues.BUSINESS_FARM_OWNER_PARENT_B, spa.Business_Farm_Owner__c);
        //System.assertEquals(222, spa.Business_Farm_Ownership_Percent__c);
        //System.assertEquals(222, spa.Business_Farm_Assets__c);
        //System.assertEquals(222, spa.Business_Farm_Debts__c);
        System.assertEquals(222, spa.Bank_Accounts__c);
        System.assertEquals(222, spa.Investments__c);
        System.assertEquals(222, spa.Total_Debts__c);
        System.assertEquals(2, spa.Dependent_Children__c);
        System.assertEquals(7, spa.Family_Size__c);
        System.assertEquals(2, spa.Num_Children_in_Tuition_Schools__c);
        System.assertEquals(222, spa.Student_Assets__c);
        System.assertEquals('2', spa.Grade_Applying__c);
        
        // set the revision fields to something else
        spa.Salary_Wages_Parent_A__c = 333;
        spa.Salary_Wages_Parent_B__c = 333;
        update spa;
        
        // now try setting the SSS fields
        spa.Orig_Salary_Wages_Parent_A__c = 444;
        spa.Orig_Salary_Wages_Parent_B__c = 444;
        update spa;
        
        // verify that the SSS fields did not overwrite the Revision
        spa = querySPA(spa.Id);
        System.assertEquals(333, spa.Salary_Wages_Parent_A__c);
        System.assertEquals(333, spa.Salary_Wages_Parent_B__c);
        
    }

    @isTest
    private static void testDuplicateSPAPreventionSequential(){
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        Account family = TestUtils.createAccount('Individual Account', RecordTypes.individualAccountTypeId, 5, true);
        Contact parent = TestUtils.createContact('Test Parent', family.Id, RecordTypes.parentContactTypeId, true);
        Contact student = TestUtils.createContact('Test Student', family.Id, RecordTypes.studentContactTypeId, true);
        PFS__c pfs = TestUtils.createPFS('Test PFS', academicYear.Id, parent.Id, true);
        Applicant__c applicant = TestUtils.createApplicant(student.Id, pfs.Id, true);
        Account school = TestUtils.createAccount('Test School', RecordTypes.schoolAccountTypeId, 5, true);
        Account school2 = TestUtils.createAccount('Test School2', RecordTypes.schoolAccountTypeId, 6, true);
        Student_Folder__c studentFolder = TestUtils.createStudentFolder('Test Student Folder', academicYear.Id, student.Id, true);
        School_PFS_Assignment__c spa = new School_PFS_Assignment__c();
        spa.Student_Folder__c = studentFolder.Id;
        spa.School__c = school.Id;
        spa.Academic_Year_Picklist__c = academicYear.Name;
        spa.Applicant__c = applicant.Id;
        
        insert spa;

        School_PFS_Assignment__c spa2 = spa.clone(false, true);
        //spa2.School__c = school2.Id; // this line added for manual test failure
        Boolean insertprevented = false;
        try {
            insert spa2;
        } catch (Exception e){
            insertprevented = true;
        }

        System.assertEquals(true, insertprevented);
       
    }


    @isTest
    private static void testDuplicateSPAPreventionConcurrent(){
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        Account family = TestUtils.createAccount('Individual Account', RecordTypes.individualAccountTypeId, 5, true);
        Contact parent = TestUtils.createContact('Test Parent', family.Id, RecordTypes.parentContactTypeId, true);
        Contact student = TestUtils.createContact('Test Student', family.Id, RecordTypes.studentContactTypeId, true);
        PFS__c pfs = TestUtils.createPFS('Test PFS', academicYear.Id, parent.Id, true);
        Applicant__c applicant = TestUtils.createApplicant(student.Id, pfs.Id, true);
        Account school = TestUtils.createAccount('Test School', RecordTypes.schoolAccountTypeId, 5, true);
        Account school2 = TestUtils.createAccount('Test School2', RecordTypes.schoolAccountTypeId, 6, true);
        Student_Folder__c studentFolder = TestUtils.createStudentFolder('Test Student Folder', academicYear.Id, student.Id, true);
        School_PFS_Assignment__c spa = new School_PFS_Assignment__c();
        spa.Student_Folder__c = studentFolder.Id;
        spa.School__c = school.Id;
        spa.Academic_Year_Picklist__c = academicYear.Name;
        spa.Applicant__c = applicant.Id;
        

        School_PFS_Assignment__c spa2 = spa.clone(false, true);
        //spa2.School__c = school2.Id; // this line added for manual test failure
        Boolean insertprevented = false;
        try {
            insert new List<School_PFS_Assignment__c>{spa, spa2};
        } catch (Exception e){
            insertprevented = true;
        }

        System.assertEquals(true, insertprevented);
       
    }

    // [DP] 08.10.2015 NAIS-2529 test syncing SBFA fields
    @isTest
    private static void testSyncOrigToRevisionFieldsSBFA() {
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;
        Business_Farm__c bizFarm = TestUtils.createBusinessFarm(pfs.Id, 'MyBiz1', 'Business', 'Partnership', false);

        bizFarm.Business_Assets_Current__c = 1; 
        bizFarm.Business_Bldg_Mortgage_Current__c = 2; 
        bizFarm.Business_Farm_Owner__c = 'Parent A';
        bizFarm.Business_Farm_Ownership_Percent__c = 4;
        bizFarm.Business_Net_Profit_Share_Current__c = 5;
        bizFarm.Business_Farm_Share__c = 6;

        Test.startTest();
        insert bizFarm;

        List<School_Biz_Farm_Assignment__c> sbfaList = querySchoolBizFarms();

        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){
            // verify orig fields are set
            System.assertEquals(1, sbfa.Orig_Business_Farm_Assets__c);
            System.assertEquals(2, sbfa.Orig_Business_Farm_Debts__c); 
            System.assertEquals('Parent A', sbfa.Orig_Business_Farm_Owner__c);
            System.assertEquals(4, sbfa.Orig_Business_Farm_Ownership_Percent__c);
            System.assertEquals(5, sbfa.Orig_Net_Profit_Loss_Business_Farm__c);

            // verify revision fields are set
            System.assertEquals(sbfa.Orig_Business_Farm_Assets__c, sbfa.Business_Farm_Assets__c);
            System.assertEquals(sbfa.Orig_Business_Farm_Debts__c, sbfa.Business_Farm_Debts__c); 
            System.assertEquals(sbfa.Orig_Business_Farm_Owner__c, sbfa.Business_Farm_Owner__c);
            System.assertEquals(sbfa.Orig_Business_Farm_Ownership_Percent__c, sbfa.Business_Farm_Ownership_Percent__c);
            System.assertEquals(sbfa.Orig_Business_Farm_Share__c, sbfa.Business_Farm_Share__c);
            System.assertEquals(sbfa.Orig_Net_Profit_Loss_Business_Farm__c, sbfa.Net_Profit_Loss_Business_Farm__c);
        }
        update sbfaList;

        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){
            // modify the orig fields
            sbfa.Orig_Business_Farm_Assets__c = 10;
            sbfa.Orig_Business_Farm_Debts__c = 20; 
            sbfa.Orig_Business_Farm_Owner__c = 'Parent B';
            sbfa.Orig_Business_Farm_Ownership_Percent__c = 40;
            sbfa.Orig_Business_Farm_Share__c = 50;
        }
        update sbfaList;

        sbfaList = querySchoolBizFarms();

        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){
            // verify revision fields are set again
            System.assertEquals(sbfa.Orig_Business_Farm_Assets__c, sbfa.Business_Farm_Assets__c);
            System.assertEquals(sbfa.Orig_Business_Farm_Debts__c, sbfa.Business_Farm_Debts__c); 
            System.assertEquals(sbfa.Orig_Business_Farm_Owner__c, sbfa.Business_Farm_Owner__c);
            System.assertEquals(sbfa.Orig_Business_Farm_Ownership_Percent__c, sbfa.Business_Farm_Ownership_Percent__c);
            System.assertEquals(sbfa.Orig_Business_Farm_Share__c, sbfa.Business_Farm_Share__c);

        }
    }

    @isTest
    private static void syncOrigToRevisionFieldsSBFA_revisionFieldsSameAsOrigFields_2018_2019_excludeSCorpAndPartnershipsEnabled_expectRevisionFieldsConsistentWithOrigFields() {
        FeatureToggles.setToggle('Exclude_S_Corps_Partnerships_From_Sums__c', true);

        Academic_Year__c academicYear = AcademicYearTestData.Instance.forName('2018-2019').DefaultAcademicYear;
        PFS__c pfs = PfsTestData.Instance.forAcademicYearPicklist(academicYear.Name).DefaultPfs;

        String originalBusinessType = EfcDataManager.BUSINESS_ENTITY_TYPE_CORPORATION;
        String updatedBusinessType = EfcDataManager.BUSINESS_ENTITY_TYPE_PARTNERSHIP;

        Business_Farm__c bizFarm = TestUtils.createBusinessFarm(pfs.Id, 'MyBiz1', 'Business', originalBusinessType, false);

        bizFarm.Business_Assets_Current__c = 1;
        bizFarm.Business_Bldg_Mortgage_Current__c = 2;
        bizFarm.Business_Farm_Owner__c = 'Parent A';
        bizFarm.Business_Farm_Ownership_Percent__c = 4;
        bizFarm.Business_Net_Profit_Share_Current__c = 5;
        bizFarm.Business_Farm_Share__c = 6;

        Test.startTest();
        insert bizFarm;

        List<School_Biz_Farm_Assignment__c> sbfaList = querySchoolBizFarms();

        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){
            // verify orig fields are set
            System.assertEquals(1, sbfa.Orig_Business_Farm_Assets__c);
            System.assertEquals(2, sbfa.Orig_Business_Farm_Debts__c);
            System.assertEquals('Parent A', sbfa.Orig_Business_Farm_Owner__c);
            System.assertEquals(4, sbfa.Orig_Business_Farm_Ownership_Percent__c);
            System.assertEquals(5, sbfa.Orig_Net_Profit_Loss_Business_Farm__c);
            System.assertEquals(originalBusinessType, sbfa.Orig_Business_Entity_Type__c, 'Expected the original business type to be the same as the business farm record.');

            // verify revision fields are set
            System.assertEquals(sbfa.Orig_Business_Farm_Assets__c, sbfa.Business_Farm_Assets__c);
            System.assertEquals(sbfa.Orig_Business_Farm_Debts__c, sbfa.Business_Farm_Debts__c);
            System.assertEquals(sbfa.Orig_Business_Farm_Owner__c, sbfa.Business_Farm_Owner__c);
            System.assertEquals(sbfa.Orig_Business_Farm_Ownership_Percent__c, sbfa.Business_Farm_Ownership_Percent__c);
            System.assertEquals(sbfa.Orig_Business_Farm_Share__c, sbfa.Business_Farm_Share__c);
            System.assertEquals(sbfa.Orig_Net_Profit_Loss_Business_Farm__c, sbfa.Net_Profit_Loss_Business_Farm__c);
            System.assertEquals(originalBusinessType, sbfa.Business_Entity_Type__c, 'Expected the revision business type to be the same as the business farm record.');
        }

        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){
            // modify the orig fields
            sbfa.Orig_Business_Farm_Assets__c = 10;
            sbfa.Orig_Business_Farm_Debts__c = 20;
            sbfa.Orig_Business_Farm_Owner__c = 'Parent B';
            sbfa.Orig_Business_Farm_Ownership_Percent__c = 40;
            sbfa.Orig_Business_Farm_Share__c = 50;
            sbfa.Orig_Business_Entity_Type__c = updatedBusinessType;
        }
        update sbfaList;

        sbfaList = querySchoolBizFarms();

        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){
            // verify revision fields are set again
            System.assertEquals(sbfa.Orig_Business_Farm_Assets__c, sbfa.Business_Farm_Assets__c);
            System.assertEquals(sbfa.Orig_Business_Farm_Debts__c, sbfa.Business_Farm_Debts__c);
            System.assertEquals(sbfa.Orig_Business_Farm_Owner__c, sbfa.Business_Farm_Owner__c);
            System.assertEquals(sbfa.Orig_Business_Farm_Ownership_Percent__c, sbfa.Business_Farm_Ownership_Percent__c);
            System.assertEquals(sbfa.Orig_Business_Farm_Share__c, sbfa.Business_Farm_Share__c);
            System.assertEquals(sbfa.Orig_Business_Entity_Type__c, sbfa.Business_Entity_Type__c,
                    'If the revision field was the same as the orig field, the revision field should be remain consistent with the orig field after the orig field is updated.');
        }
    }

    // [DP] 08.10.2015 NAIS-2529 test syncing SBFA fields
    @isTest
    private static void testSyncOrigToRevisionFieldsSBFA2() {
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;
        Business_Farm__c bizFarm = TestUtils.createBusinessFarm(pfs.Id, 'MyBiz1', 'Business', 'Sole Proprietorship', false);

        bizFarm.Business_Assets_Current__c = 1; 
        bizFarm.Business_Bldg_Mortgage_Current__c = 2; 
        bizFarm.Business_Farm_Owner__c = 'Parent A';
        bizFarm.Business_Farm_Ownership_Percent__c = 4;
        bizFarm.Business_Net_Profit_Share_Current__c = 5;

        Test.startTest();
        insert bizFarm;

        List<School_Biz_Farm_Assignment__c> sbfaList = querySchoolBizFarms();

        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){

            // set the revision fields
            sbfa.Business_Farm_Assets__c = 9;
            sbfa.Business_Farm_Debts__c = 99; 
            sbfa.Business_Farm_Owner__c = 'Parent 9';
            sbfa.Business_Farm_Ownership_Percent__c = 999;
            sbfa.Business_Farm_Share__c = 9999;

        }
        update sbfaList;

        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){
            // modify the orig fields 
            sbfa.Orig_Business_Farm_Assets__c = 100;
            sbfa.Orig_Business_Farm_Debts__c = 200; 
            sbfa.Orig_Business_Farm_Owner__c = 'Parent C';
            sbfa.Orig_Business_Farm_Ownership_Percent__c = 400;
            sbfa.Orig_Business_Farm_Share__c = 500;
        }
        update sbfaList;

        sbfaList = querySchoolBizFarms();

        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){
            // verify revision fields have not been updated
            System.assertNotEquals(sbfa.Orig_Business_Farm_Assets__c, sbfa.Business_Farm_Assets__c);
            System.assertNotEquals(sbfa.Orig_Business_Farm_Debts__c, sbfa.Business_Farm_Debts__c); 
            System.assertNotEquals(sbfa.Orig_Business_Farm_Owner__c, sbfa.Business_Farm_Owner__c);
            System.assertNotEquals(sbfa.Orig_Business_Farm_Ownership_Percent__c, sbfa.Business_Farm_Ownership_Percent__c);
            System.assertNotEquals(sbfa.Orig_Business_Farm_Share__c, sbfa.Business_Farm_Share__c);

            System.assertEquals(100, sbfa.Orig_Business_Farm_Assets__c);
            System.assertEquals(200, sbfa.Orig_Business_Farm_Debts__c); 
            System.assertEquals('Parent C', sbfa.Orig_Business_Farm_Owner__c);
            System.assertEquals(400, sbfa.Orig_Business_Farm_Ownership_Percent__c);
            System.assertEquals(500, sbfa.Orig_Business_Farm_Share__c);

            System.assertEquals(9, sbfa.Business_Farm_Assets__c);
            System.assertEquals(99, sbfa.Business_Farm_Debts__c); 
            System.assertEquals('Parent 9', sbfa.Business_Farm_Owner__c);
            System.assertEquals(999, sbfa.Business_Farm_Ownership_Percent__c);
            System.assertEquals(9999, sbfa.Business_Farm_Share__c);
        }
    }

    @isTest
    private static void syncOrigToRevisionFieldsSBFA_revisionFieldsChangedThenOrigFieldsChanged_2018_2019_excludeSCorpAndPartnershipsEnabled_expectRevisionFieldsDifferentThanOrig() {
        FeatureToggles.setToggle('Exclude_S_Corps_Partnerships_From_Sums__c', true);

        Academic_Year__c academicYear = AcademicYearTestData.Instance.forName('2018-2019').DefaultAcademicYear;
        PFS__c pfs = PfsTestData.Instance.forAcademicYearPicklist(academicYear.Name).DefaultPfs;

        String originalBusinessType = 'Sole Proprietorship';
        String revisionBusinessType = EfcDataManager.BUSINESS_ENTITY_TYPE_PARTNERSHIP;
        String updatedOrigBusinessType = EfcDataManager.BUSINESS_ENTITY_TYPE_CORPORATION;

        Business_Farm__c bizFarm = TestUtils.createBusinessFarm(pfs.Id, 'MyBiz1', 'Business', originalBusinessType, false);

        bizFarm.Business_Assets_Current__c = 1;
        bizFarm.Business_Bldg_Mortgage_Current__c = 2;
        bizFarm.Business_Farm_Owner__c = 'Parent A';
        bizFarm.Business_Farm_Ownership_Percent__c = 4;
        bizFarm.Business_Net_Profit_Share_Current__c = 5;

        Test.startTest();
        insert bizFarm;

        List<School_Biz_Farm_Assignment__c> sbfaList = querySchoolBizFarms();

        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){

            // set the revision fields
            sbfa.Business_Farm_Assets__c = 9;
            sbfa.Business_Farm_Debts__c = 99;
            sbfa.Business_Farm_Owner__c = 'Parent 9';
            sbfa.Business_Farm_Ownership_Percent__c = 999;
            sbfa.Business_Farm_Share__c = 9999;
            sbfa.Business_Entity_Type__c = revisionBusinessType;
        }
        update sbfaList;

        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){
            // modify the orig fields
            sbfa.Orig_Business_Farm_Assets__c = 100;
            sbfa.Orig_Business_Farm_Debts__c = 200;
            sbfa.Orig_Business_Farm_Owner__c = 'Parent C';
            sbfa.Orig_Business_Farm_Ownership_Percent__c = 400;
            sbfa.Orig_Business_Farm_Share__c = 500;
            sbfa.Orig_Business_Entity_Type__c = updatedOrigBusinessType;
        }
        update sbfaList;

        sbfaList = querySchoolBizFarms();

        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){
            // verify revision fields have not been updated
            System.assertNotEquals(sbfa.Orig_Business_Farm_Assets__c, sbfa.Business_Farm_Assets__c);
            System.assertNotEquals(sbfa.Orig_Business_Farm_Debts__c, sbfa.Business_Farm_Debts__c);
            System.assertNotEquals(sbfa.Orig_Business_Farm_Owner__c, sbfa.Business_Farm_Owner__c);
            System.assertNotEquals(sbfa.Orig_Business_Farm_Ownership_Percent__c, sbfa.Business_Farm_Ownership_Percent__c);
            System.assertNotEquals(sbfa.Orig_Business_Farm_Share__c, sbfa.Business_Farm_Share__c);
            System.assertNotEquals(sbfa.Orig_Business_Entity_Type__c, sbfa.Business_Entity_Type__c,
                    'Expected the revision field to not be updated after changing the orig field.');

            System.assertEquals(100, sbfa.Orig_Business_Farm_Assets__c);
            System.assertEquals(200, sbfa.Orig_Business_Farm_Debts__c);
            System.assertEquals('Parent C', sbfa.Orig_Business_Farm_Owner__c);
            System.assertEquals(400, sbfa.Orig_Business_Farm_Ownership_Percent__c);
            System.assertEquals(500, sbfa.Orig_Business_Farm_Share__c);
            System.assertEquals(updatedOrigBusinessType, sbfa.Orig_Business_Entity_Type__c,
                    'Expected the original business entity type to have the updated value.');

            System.assertEquals(9, sbfa.Business_Farm_Assets__c);
            System.assertEquals(99, sbfa.Business_Farm_Debts__c);
            System.assertEquals('Parent 9', sbfa.Business_Farm_Owner__c);
            System.assertEquals(999, sbfa.Business_Farm_Ownership_Percent__c);
            System.assertEquals(9999, sbfa.Business_Farm_Share__c);
            System.assertEquals(revisionBusinessType, sbfa.Business_Entity_Type__c,
                    'Expected the revision business entity type to be unchanged after updating the orig field.');
        }
    }
}