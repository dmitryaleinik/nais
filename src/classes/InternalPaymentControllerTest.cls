@isTest
private class InternalPaymentControllerTest {
    private static void createAppFeeSetting(String appFeeSettingName) {
        Integer appFeeSettingAmount = 50;
        Integer appFeeSettingDiscountedAmount = 40;
        String appFeeSettingProductCode = 'Test Product Code';
        String appFeeSettingDiscountedProductCode = 'KS Test Product Code';
        Application_Fee_Settings__c appFeeSettings = ApplicationFeeSettingsTestData.Instance
                .forName(appFeeSettingName)
                .forAmount(appFeeSettingAmount)
                .forDiscountedAmount(appFeeSettingDiscountedAmount)
                .forProductCode(appFeeSettingProductCode)
                .forDiscountedProductCode(appFeeSettingDiscountedProductCode).DefaultApplicationFeeSettings;
    }

    private static void createTestData(Boolean createOppAndTLIToo) {
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        createAppFeeSetting(currentAcademicYear.Name);
        Application_Fee_Waiver__c afw1 = ApplicationFeeWaiverTestData.Instance.DefaultApplicationFeeWaiver;

        // create the default test data
        String parentACountry = 'United States';
        String parentAAdress = 'Line 1 Address\nLine 2 Address';
        PFS__c pfs = PfsTestData.Instance
            .asSubmitted()
            .forParentAAddress(parentAAdress)
            .forParentACountry(parentACountry)
            .forAcademicYearPicklist(currentAcademicYear.Name).DefaultPfs;

        if (createOppAndTLIToo){
            FinanceAction.createOppAndTransaction(new Set<Id>{pfs.Id});
        }
    }

    @isTest
    private static void testInternalPaymentControllerInit() {
        createTestData(true);
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        Test.startTest();
            // get the opportunity
            Opportunity theOpportunity = [SELECT Id FROM Opportunity WHERE PFS__c = :pfs.Id];

            Test.setCurrentPage(Page.InternalPayment);

            // test instantiate without id param
            InternalPaymentController controller = new InternalPaymentController();
            controller.init();
            System.assertEquals(false, controller.isValidPage);

            // test instantiate with bad id param
            ApexPages.currentPage().getParameters().put('id', '000000000000');
            controller = new InternalPaymentController();
            controller.init();
            System.assertEquals(false, controller.isValidPage);

            // test instantiate with valid id param
            ApexPages.currentPage().getParameters().put('id', theOpportunity.Id);
            controller = new InternalPaymentController();
            controller.init();
        Test.stopTest();

        System.assertEquals(true, controller.isValidPage);
        // check picklist values
        System.assertEquals(2, controller.getPaymentTypeOptions().size());
    }

    @isTest
    private static void testCancelAction() {
        createTestData(true);
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        Opportunity theOpportunity = [SELECT Id FROM Opportunity WHERE PFS__c = :pfs.Id];
        Test.setCurrentPage(Page.InternalPayment);
        ApexPages.currentPage().getParameters().put('id', theOpportunity.Id);

        InternalPaymentController controller = new InternalPaymentController();
        controller.init();
        System.assertEquals(true, controller.isValidPage);

        Test.startTest();
            // do cancel
            PageReference pr = controller.actionCancel();
        Test.stopTest();

        // verify return to opportunity
        System.assertEquals('/'+theOpportunity.Id, pr.getUrl());
    }

    @isTest
    private static void testProcessPaymentActionValidation() {
        createTestData(true);
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        Test.startTest();
            Opportunity theOpportunity = [SELECT Id FROM Opportunity WHERE PFS__c = :pfs.Id];
            Test.setCurrentPage(Page.InternalPayment);
            ApexPages.currentPage().getParameters().put('id', theOpportunity.Id);

            InternalPaymentController controller = new InternalPaymentController();
            controller.init();
            System.assertEquals(true, controller.isValidPage);

            // try submitting without required fields
            controller.paymentData.paymentAmount = null;
            controller.paymentData.paymentDescription = null;
            controller.paymentData.paymentTracker = null;

            // process payment
            PageReference pr = controller.processPayment();

            // verify validation errors
            System.assertNotEquals(null, controller.fieldErrors.paymentAmount);
            System.assertNotEquals(null, controller.fieldErrors.paymentDescription);
            System.assertNotEquals(null, controller.fieldErrors.paymentTracker);

            // test bad payment amount
            controller.paymentData.paymentAmount = -123;
            pr = controller.processPayment();
        Test.stopTest();

        System.assertNotEquals(null, controller.fieldErrors.paymentAmount);
    }

    @isTest
    private static void testProcessPaymentActionCreditCard() {
        createTestData(true);
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        Opportunity theOpportunity = [SELECT Id FROM Opportunity WHERE PFS__c = :pfs.Id];
        Test.setCurrentPage(Page.InternalPayment);
        ApexPages.currentPage().getParameters().put('id', theOpportunity.Id);

        InternalPaymentController controller = new InternalPaymentController();
        controller.init();
        System.assertEquals(true, controller.isValidPage);

        // enter form data
        controller.paymentData.paymentType = FamilyPaymentData.PAYMENT_TYPE_CC;
        controller.paymentData.billingFirstName = 'Test First Name';
        controller.paymentData.billingLastName = 'Test Last Name';
        controller.paymentData.billingStreet1 = 'Test Street 1';
        controller.paymentData.billingStreet2 = 'Test Street 2';
        controller.paymentData.billingCity = 'Test City';
        controller.paymentData.billingState = 'Test State';
        controller.paymentData.billingPostalCode = '11111';
        controller.paymentData.billingCountry = '123';
        controller.paymentData.billingEmail = 'test@test.com';
        controller.paymentData.ccNameOnCard = 'Test Name';
        controller.paymentData.ccCardType = 'Visa';
        controller.paymentData.ccCardNumber = '4111111111111111';
        controller.paymentData.ccExpirationMM = String.valueOf(System.today().month());
        controller.paymentData.ccExpirationYY = String.valueOf(System.today().addYears(1).year());
        controller.paymentData.ccSecurityCode = '123';

        Test.startTest();
            //System.debug(LoggingLevel.ERROR, 'TESTING limits' + Limits.getLimitQueries());

            // process payment
            PageReference pr = controller.processPayment();

            // verify no error
            System.assertNotEquals(null, pr);

            // verify transaction line item
            Transaction_Line_Item__c tli = [
                SELECT Id
                FROM Transaction_Line_Item__c
                WHERE Opportunity__c = :theOpportunity.Id
                    AND RecordTypeId=:RecordTypes.paymentAutoTransactionTypeId];
        Test.stopTest();

        System.assertEquals('/'+tli.Id, pr.getUrl());
    }

    @isTest
    private static void testProcessPaymentActionECheck() {
        // set up the test data
        createTestData(true);
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        Opportunity theOpportunity = [SELECT Id FROM Opportunity WHERE PFS__c = :pfs.Id];
        Test.setCurrentPage(Page.InternalPayment);
        ApexPages.currentPage().getParameters().put('id', theOpportunity.Id);

        InternalPaymentController controller = new InternalPaymentController();
        controller.init();
        System.assertEquals(true, controller.isValidPage);

        // enter form data
        controller.paymentData.paymentType = FamilyPaymentData.PAYMENT_TYPE_ECHECK;
        controller.paymentData.billingFirstName = 'Test First Name';
        controller.paymentData.billingLastName = 'Test Last Name';
        controller.paymentData.billingStreet1 = 'Test Street 1';
        controller.paymentData.billingStreet2 = 'Test Street 2';
        controller.paymentData.billingCity = 'Test City';
        controller.paymentData.billingState = 'Test State';
        controller.paymentData.billingPostalCode = '99999';
        controller.paymentData.billingCountry = '123';
        controller.paymentData.billingEmail = 'test@test.com';
        controller.paymentData.checkBankName = 'BankOfAmerica';
        controller.paymentData.checkAccountType = 'CheckingAccount';
        controller.paymentData.checkAccountNumber = '1234567890';
        controller.paymentData.checkRoutingNumber = '111111111';
        controller.paymentData.checkCheckType = 'Personal';
        controller.paymentData.checkCheckNumber = '123';

        Test.startTest();
            // process payment
            PageReference pr = controller.processPayment();
        Test.stopTest();

        // verify no error
        System.assertNotEquals(null, pr);

        // verify transaction line item
        Transaction_Line_Item__c tli = [
            SELECT Id
            FROM Transaction_Line_Item__c
            WHERE Opportunity__c = :theOpportunity.Id
                AND RecordTypeId=:RecordTypes.paymentAutoTransactionTypeId];
        System.assertEquals('/'+tli.Id, pr.getUrl());
    }

    @isTest
    private static void testProcessPaymentActionDecline() {
        createTestData(true);
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;
        Opportunity theOpportunity = [SELECT Id FROM Opportunity WHERE PFS__c = :pfs.Id];

        Test.setCurrentPage(Page.InternalPayment);
        ApexPages.currentPage().getParameters().put('id', theOpportunity.Id);

        InternalPaymentController controller = new InternalPaymentController();
        controller.init();
        System.assertEquals(true, controller.isValidPage);

        // enter form data
        controller.paymentData.paymentType = FamilyPaymentData.PAYMENT_TYPE_CC;
        controller.paymentData.billingFirstName = 'Test First Name';
        controller.paymentData.billingLastName = 'Test Last Name';
        controller.paymentData.billingStreet1 = 'Test Street 1';
        controller.paymentData.billingStreet2 = 'Test Street 2';
        controller.paymentData.billingCity = 'Test City';
        controller.paymentData.billingState = 'Test State';
        controller.paymentData.billingPostalCode = '99999';
        controller.paymentData.billingCountry = '123';
        controller.paymentData.billingEmail = 'test@test.com';
        controller.paymentData.ccNameOnCard = 'Test Name';
        controller.paymentData.ccCardType = 'Visa';
        controller.paymentData.ccCardNumber = '1234567890';
        controller.paymentData.ccExpirationMM = '05';
        controller.paymentData.ccExpirationYY = '10';
        controller.paymentData.ccSecurityCode = '123';

        Test.startTest();
            PageReference pr = controller.processPayment();
        Test.stopTest();

        // verify an error
        System.assertEquals(null, pr);
    }

    @isTest
    private static void testNoPreexistingSaleTLI(){
        Account school = AccountTestData.Instance.DefaultAccount;
        createTestData(false);

        Test.startTest();
            Opportunity theOpportunity = new Opportunity(
                                        Name = 'subscrptiontest',
                                        StageName = 'Open',
                                        CloseDate = Date.today(),
                                        Academic_Year_Picklist__c = GlobalVariables.getCurrentYearString(),
                                        RecordTypeId = RecordTypes.opportunitySubscriptionFeeTypeId,
                                        AccountId = school.Id);
            insert theOpportunity;

            System.assertEquals(0, [Select count() FROM Transaction_Line_Item__c]);

            Test.setCurrentPage(Page.InternalPayment);

            ApexPages.currentPage().getParameters().put('id', theOpportunity.Id);
            InternalPaymentController controller = new InternalPaymentController();
            controller.init();
        Test.stopTest();

        System.assertEquals(true, controller.isValidPage);
        System.assertEquals(1, [Select count() FROM Transaction_Line_Item__c]);
    }
}