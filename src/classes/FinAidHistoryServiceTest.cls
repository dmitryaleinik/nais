@isTest
private class FinAidHistoryServiceTest {
    
    private static Academic_Year__c currentAcademicYear;
    private static Academic_Year__c previousAcademicYear;
    private static Contact student1;
    private static Account school;
    private static Budget_Group__c budgetPrevious;
    private static Budget_Group__c budgetCurrent;
    
    private static void setup() {
        
        //1. Create current and previous academic year.
        currentAcademicYear = AcademicYearTestData.Instance.create();
        previousAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, previousAcademicYear};
        
        //2. Create a student.
        student1 = ContactTestData.Instance.asStudent().create();
        insert student1;
        
        //3. Create a School.
        school = AccountTestData.Instance.asSchool().DefaultAccount;        
        
        //4. Create one Budget Group for the School for Previous academic year.
        //   And one Budget Group for the current academic year.
        budgetPrevious = BudgetGroupTestData.Instance
            .forAcademicYearId(previousAcademicYear.Id)
            .forSchoolId(school.Id)
            .forTotalFunds(50000).create();
        budgetPrevious.Name = 'Group 1';
        
        budgetCurrent = BudgetGroupTestData.Instance
            .forAcademicYearId(currentAcademicYear.Id)
            .forSchoolId(school.Id)
            .forTotalFunds(60000).create();  
        budgetCurrent.Name = 'Group 2';

        insert new List<Budget_Group__c>{budgetPrevious, budgetCurrent};
    }
    
    
    //SFP-1243
    @isTest
    private static void getPreviousAwards_studentWithPreviousBudgetAllocations_folderWithBudgetAllocationsForPreviousAcademicYear() {
        
        //1. Setup test data.
        setup();
        
        //2. Create student folder for each Academic Year.
        Student_Folder__c studentFolderPrevious = StudentFolderTestData.Instance
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(previousAcademicYear.Name)
            .forStudentId(student1.Id).create();
        
        Student_Folder__c studentFolderCurrent = StudentFolderTestData.Instance
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forStudentId(student1.Id).create();

        insert new List<Student_Folder__c>{studentFolderPrevious, studentFolderCurrent};
        
        //3. Award for each StudentFolders.
        Budget_Allocation__c allocationPreviousYear = BudgetAllocationTestData.Instance
            .forBudgetGroupId(budgetPrevious.Id)
            .forAmountAllocated(1200)
            .forStudentFolder(studentFolderPrevious.Id).create();

        Budget_Allocation__c allocationCurrentYear = BudgetAllocationTestData.Instance
            .forBudgetGroupId(budgetCurrent.Id)
            .forAmountAllocated(3000)
            .forStudentFolder(studentFolderCurrent.Id).create();

        insert new List<Budget_Allocation__c>{allocationPreviousYear, allocationCurrentYear};
        
        Test.startTest();
        
            FinAidHistoryService.FinAidInfo previousFolderResult = 
                FinAidHistoryService.Instance.getPreviousAwards(student1.Id, school.Id, previousAcademicYear.Name);
            
            System.assertEquals(true, previousFolderResult.getHasBudgetAllocations());
            System.assertEquals(1200, previousFolderResult.getTotalAward());
            
            FinAidHistoryService.FinAidInfo currentFolderResult = 
                FinAidHistoryService.Instance.getPreviousAwards(student1.Id, school.Id, currentAcademicYear.Name);
            
            System.assertEquals(true, currentFolderResult.getHasBudgetAllocations());
            System.assertEquals(3000, currentFolderResult.getTotalAward());
            
        Test.stopTest();
    }
    
    //SFP-1243
    @isTest
    private static void getPreviousAwards_studentFolderWithoutPreviousBudgetAllocations_noRecordsForPreviousAwards() {
        
        //1. Setup test data.
        setup();
        
        //2. Create student folder for each Academic Year.
        Student_Folder__c studentFolderPrevious = StudentFolderTestData.Instance
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(previousAcademicYear.Name)
            .forStudentId(student1.Id).create();
        
        Student_Folder__c studentFolderCurrent = StudentFolderTestData.Instance
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forStudentId(student1.Id).create();
            
        Test.startTest();
        
            FinAidHistoryService.FinAidInfo previousFolderResult = 
                FinAidHistoryService.Instance.getPreviousAwards(student1.Id, school.Id, previousAcademicYear.Name);
            
            System.assertEquals(false, previousFolderResult.getHasBudgetAllocations());
            System.assertEquals(0, previousFolderResult.getTotalAward());
            
            FinAidHistoryService.FinAidInfo currentFolderResult = 
                FinAidHistoryService.Instance.getPreviousAwards(student1.Id, school.Id, currentAcademicYear.Name);
            
            System.assertEquals(false, currentFolderResult.getHasBudgetAllocations());
            System.assertEquals(0, currentFolderResult.getTotalAward());
        Test.stopTest();
    }
    
    //SFP-1243
    @isTest
    private static void getPreviousAwards_studentWithoutFolders_noRecordsForPreviousAwards() {
        
        //1. Setup test data.
        setup();
            
        Test.startTest();
        
            FinAidHistoryService.FinAidInfo previousFolderResult = 
                FinAidHistoryService.Instance.getPreviousAwards(student1.Id, school.Id, previousAcademicYear.Name);
            
            System.assertEquals(false, previousFolderResult.getHasBudgetAllocations());
            System.assertEquals(0, previousFolderResult.getTotalAward());
        
            FinAidHistoryService.FinAidInfo currentFolderResult = 
                FinAidHistoryService.Instance.getPreviousAwards(student1.Id, school.Id, currentAcademicYear.Name);
            
            System.assertEquals(false, currentFolderResult.getHasBudgetAllocations());
            System.assertEquals(0, currentFolderResult.getTotalAward());
        Test.stopTest();
    }
}