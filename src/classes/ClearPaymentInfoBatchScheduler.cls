/**
* Scheduler for the batch that daily clears payment fields of Chargent Order and Transaction records. 
*/
global class ClearPaymentInfoBatchScheduler implements Schedulable {

    global void execute(SchedulableContext sc) {
        Database.executebatch(new ClearPaymentInfoBatch());
    }
}