/**
 * ContactDataAccessServiceTest.cls
 *
 * @description: Test class for ContactDataAccessService using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
private class ContactDataAccessServiceTest {
    
    /* test data setup */
    @testSetup static void setupTestData() {
        
        MassEmailSendTestDataFactory.createEmailTemplate();
        
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
        }
    }
    
    /* negative test cases */

    // Attempt getContactById with null param, expect null object ref returned
    @isTest static void getContactById_withNull_nullContact() {
        
        // Arrange
        Contact con = new Contact();

        // Act
        con = new ContactDataAccessService().getContactById( null);

        // Assert
        System.assertEquals( null, con);
    }

    // Attempt getContactEmailCSVStringByIdSet with null param, expect null object ref returned
    @isTest static void getContactEmailCSVStringByIdSet_withNull_nullList() {
        
        // Arrange
        List<String> resultList = new List<String>();

        // Act
        resultList = new ContactDataAccessService().getContactEmailCSVStringByIdSet( null);

        // Assert
        System.assertEquals( null, resultList);
    }

    // Attempt getContactEmailByIdSet with null param, expect null object ref returned
    @isTest static void getContactEmailByIdSet_withNull_nullList() {
        
        // Arrange
        List<Contact> resultList = new List<Contact>();

        // Act
        resultList = new ContactDataAccessService().getContactEmailByIdSet( null);

        // Assert
        System.assertEquals( null, resultList);
    }



    /* postive test cases */

    // Attempt getContactById with valid param, expect Contact returned
    @isTest static void getContactById_withValidId_contactRecord() {

        // Arrange
        Contact con = [select Id from Contact limit 1];

        // Act
        con = new ContactDataAccessService().getContactById( con.Id);

        // Assert
        System.assertNotEquals( null, con);
    }

    // Attempt getContactEmailCSVStringByIdSet with valid param, expect List<String> returned
    @isTest static void getContactEmailCSVStringByIdSet_withValidParam_listString() {
        
        // Arrange
        List<String> resultList = new List<String>();
        List<Contact> contactList = [select Id from Contact];
        Set<String> contactIdSet = new Set<String>();
        for ( Contact c : contactList) {
            contactIdSet.add( c.Id);
        }

        // Act
        resultList = new ContactDataAccessService().getContactEmailCSVStringByIdSet( contactIdSet);

        // Assert
        System.assertNotEquals( null, resultList);
    }

    // Attempt getContactEmailByIdSet with valid param, expect List<String> returned
    @isTest static void getContactEmailByIdSet_withValidParam_listContact() {
        
        // Arrange
        List<Contact> resultList = new List<Contact>();
        List<Contact> contactList = [select Id from Contact];
        Set<String> contactIdSet = new Set<String>();
        for ( Contact c : contactList) {
            contactIdSet.add( c.Id);
        }

        // Act
        resultList = new ContactDataAccessService().getContactEmailByIdSet( contactIdSet);

        // Assert
        System.assertNotEquals( null, resultList);
    }

}