/**
 * KnowledgeDataAccessServiceTest.cls
 *
 * @description: Test class for KnowledgeDataAccessService using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Mike Havrilla @ Presence PG
 */
@isTest (seeAllData = false)
private class KnowledgeDataAccessServiceTest {

    static User knowledgeUser;
    static {
        knowledgeUser = CurrentUser.getCurrentUser();
        knowledgeUser.UserPermissionsKnowledgeUser = true;

        update knowledgeUser;
    }

    private static List<Knowledge__kav> setupArticles() {
        List<Knowledge__kav> articles;

        System.runAs(knowledgeUser) {
            articles = KnowledgeAVTestData.Instance.insertKnowledgeAVs( 14);

            // Create articles with the default category.
            List<Knowledge__DataCategorySelection> categories = new List<Knowledge__DataCategorySelection>();

            for (Knowledge__kav article : articles) {
                categories.add(KnowledgeDCSTestData.Instance.forParentId(article.Id).create());
            }
            insert categories;

            articles = [SELECT Id, KnowledgeArticleId FROM Knowledge__kav WHERE Id in :articles];

            KnowledgeTestHelper.publishArticles( articles);

        }

        return articles;
    }

    // Validate that the correct amount of knowledgeArticles were returned in the correct sequence, not limiting to just 'Documents'
    @isTest
    static void getTopFamilyKnowledgeArticles_withTen_tenKnowledgeArticles() {
        System.runAs(knowledgeUser) {
            // Arrange
            List<Knowledge__kav> articles = setupArticles();
            List<Knowledge__kav> knowledgeArticles = new List<Knowledge__kav>();

            Test.startTest();
                knowledgeArticles = new KnowledgeDataAccessService().getTopFamilyKnowledgeArticles( 10);
            Test.stopTest();

            // Assert
            // Test that 10 come back
            System.assertEquals( 10, knowledgeArticles.size(), 'The correct number of records did not come back from the query');
            for( Integer i = 0; i < 10; i++) {
                System.assertEquals( i, knowledgeArticles.get(i).Sequence__c, 'The article was not retrieved in the right order');
            }
        }
    }

    // Validate that the correct amount of knowledgeArticles were returned in the correct sequence, limiting to just 'Documents'
    @isTest
    static void getTopFamilyKnowledgeArticles_withFiveLimitDocuments_fiveDocumentsKnowledgeArticles() {
        System.runAs(knowledgeUser) {
            Test.startTest();
                // Arrange
                List<Knowledge__kav> articles = setupArticles();
                List<Knowledge__kav> knowledgeArticles = new List<Knowledge__kav>();
                knowledgeArticles = new KnowledgeDataAccessService().getTopFamilyKnowledgeArticles(5, true);
            Test.stopTest();
            // Assert
            System.assertEquals( 5, knowledgeArticles.size(), 'The correct number of records did not come back from the query');
            for( Integer i = 0; i < 5; i++) {
                System.assertEquals( i, knowledgeArticles.get(i).Sequence__c, 'The article was notretrieved in the right order');
            }
        }
    }

    // Validate that the correct amount of knowledgeArticles were returned in the correct sequence, limiting to just 'Documents'
    @isTest
    static void getTopFamilyKnowledgeArticlesAbbreviated_withFiveLimitDocuments_fiveDocumentsKnowledgeArticlesNotAbbreviated() {
        System.runAs(knowledgeUser) {
            Test.startTest();
                // Arrange
                List<Knowledge__kav> articles = setupArticles();
                List<KnowledgeDataAccessService.KnowledgeWrapper> knowledgeArticles = new List<KnowledgeDataAccessService.KnowledgeWrapper>();
                knowledgeArticles = new KnowledgeDataAccessService().getTopFamilyKnowledgeArticlesAbbreviated(5, true, false);
            Test.stopTest();

            // Assert
            System.assertEquals( 5, knowledgeArticles.size(), 'The correct number of records did not come back from the query');
            for( Integer i = 0; i < 5; i++) {
                System.assertEquals( true, knowledgeArticles.get(i).hasAbbreviatedBody, 'The article was abbrivated when we expected it to not be');
            }
        }
    }

    // Validate that the correct amount of knowledgeArticles were returned in the correct sequence, limiting to just 'Documents'
    @isTest
    static void getTopFamilyKnowledgeArticlesAbbreviated_withFiveLimitDocuments_fiveDocumentsKnowledgeArticlesAbbreviated() {
        System.runAs(knowledgeUser) {
            Knowledge__kav article = KnowledgeAVTestData.Instance.forSequence(1).insertKnowledgeAV();

            // Arrange
            List<KnowledgeDataAccessService.KnowledgeWrapper> knowledgeArticles = new List<KnowledgeDataAccessService.KnowledgeWrapper>();

            // Create articles with the default category.
            Knowledge__DataCategorySelection categories = KnowledgeDCSTestData.Instance.forParentId(article.Id).insertKnowledgeDCS();

            List<Knowledge__kav> articles = [SELECT Id, KnowledgeArticleId FROM Knowledge__kav WHERE Id = :article.Id];

            KnowledgeTestHelper.publishArticles( articles);

            // Act
            Test.startTest();
                knowledgeArticles = new KnowledgeDataAccessService().getTopFamilyKnowledgeArticlesAbbreviated(5, true, false);
            Test.stopTest();
            // Assert
            System.assertEquals( 1, knowledgeArticles.size(), 'The correct number of records did not come back from the query');
            for( Integer i = 0; i < 1; i++) {
                if( knowledgeArticles.get(i).knowledgeArticle.Id == article.Id) {
                    System.assertEquals( true, knowledgeArticles.get(i).hasAbbreviatedBody, 'The article was not abbrivated when we expected it to be ');
                } else {
                    System.assertEquals( true, knowledgeArticles.get(i).hasAbbreviatedBody, 'The article was abbrivated when we didn\'t expect it to be ');
                }
            }
        }
    }

    // Validate that the correct amount of knowledgeArticles where every body is greater than 200 but, doesn't contain any spaces.
    @isTest
    static void getTopFamilyKnowledgeArticlesAbbreviated_withFiveLimitDocumentsAndNoSpacesInBody_fiveDocumentsKnowledgeArticlesAbbreviatedWithNoSpacesInBody() {
        System.runAs(knowledgeUser) {
            // Arrange
            List<KnowledgeDataAccessService.KnowledgeWrapper> knowledgeArticles = new List<KnowledgeDataAccessService.KnowledgeWrapper>();
            // 229 character string that we can test no spaces.
            String articleBody = 'hHeXdFcBGvKQI5IosArL0TlErN783Z0vybtJX9MSuxYnpRE4RP9cFSRrCndLezolNIrRTD0C6DPz0dZQbrusCaFXv6DucFdKZgpzpmnmGcHz4XkgrigQIMNYVlxXCF4rlonsfdcaHlyMWRzvm3NEEYiQfljlNCUw5I6eJEnbqMKMLW3SrH6ASa729hwY1DuW7bzZMRjjUYA0VrBglqDVsxpRRcGN';

            Knowledge__kav article = KnowledgeAVTestData.Instance.forSequence(1).forArticleBody(articleBody).insertKnowledgeAV();

            // Create articles with the default category.
            Knowledge__DataCategorySelection categories = KnowledgeDCSTestData.Instance.forParentId(article.Id).insertKnowledgeDCS();

            List<Knowledge__kav> articles = [SELECT Id, KnowledgeArticleId FROM Knowledge__kav WHERE Id = :article.Id];

            KnowledgeTestHelper.publishArticles( articles);

            // Act
            Test.startTest();
                knowledgeArticles = new KnowledgeDataAccessService().getTopFamilyKnowledgeArticlesAbbreviated(5, true, false);
            Test.stopTest();
            // Assert
            System.assertEquals( 1, knowledgeArticles.size(), 'The correct number of records did not come back from the query');
            System.assertEquals( true, knowledgeArticles.get( 0).hasAbbreviatedBody, 'The article was abbrivated when we didn\'t expect it to be ');
            System.assertEquals( articleBody + '...', knowledgeArticles.get( 0).abbreviatedBody, 'The article was abbrivated when we didn\'t expect it to be ');
        }
    }

    @isTest
    static void getTopFamilyKnowledgeArticlesAbbreviated_withFiveLimitDocuments_fiveDocumentsKnowledgeArticlesAbbreviatedWithSpaceAtTheEnd() {
        System.runAs(knowledgeUser) {
            // Arrange
            List<KnowledgeDataAccessService.KnowledgeWrapper> knowledgeArticles = new List<KnowledgeDataAccessService.KnowledgeWrapper>();
            // 229 character string that we can test no spaces.
            String articleBody = 'hHeXdFcBGvKQI5IosArL0TlErN783Z0vybtJX9MSuxYnpRE4RP9cFSRrCndLezolNIrRTD0C6DPz0dZQbrusCaFXv6DucFdKZgpzpmnmGcHz4XkgrigQIMNYVlxXCF4rlonsfdcaHlyMWRzvm3NEEYiQfljlNCUw5I6eJEnbqMKMLW3SrH6ASa729hwY1DuW7bzZMRjjUYA0VrBglqDVsxpRRcGN ';
            Knowledge__kav article = KnowledgeAVTestData.Instance.forSequence(1).forArticleBody(articleBody).insertKnowledgeAV();

            // Create articles with the default category.
            Knowledge__DataCategorySelection categories = KnowledgeDCSTestData.Instance.forParentId(article.Id).insertKnowledgeDCS();

            List<Knowledge__kav> articles = [SELECT Id, KnowledgeArticleId FROM Knowledge__kav WHERE Id = :article.Id];

            KnowledgeTestHelper.publishArticles( articles);

            // Act
            Test.startTest();
                knowledgeArticles = new KnowledgeDataAccessService().getTopFamilyKnowledgeArticlesAbbreviated(5, true, false);
            Test.stopTest();
            // Assert
            String expectedResult = articleBody.replace(' ', '');
            System.assertEquals( 1, knowledgeArticles.size(), 'The correct number of records did not come back from the query');
            System.assertEquals( true, knowledgeArticles.get( 0).hasAbbreviatedBody, 'The article was abbrivated when we didn\'t expect it to be ');
            System.assertEquals( expectedResult + '...', knowledgeArticles.get( 0).abbreviatedBody, 'The article was abbrivated when we didn\'t expect it to be ');
        }
    }

    // Validate that the correct amount of knowledgeArticles were returned with a valid Keyword
    @isTest
    static void searchKnowledgeArticlesByKeyword_withKeyword_articlesReturned() {
        System.runAs(knowledgeUser) {
            // Arrange
            List<Knowledge__kav> articles = setupArticles();
            List<Search.SearchResult> knowledgeArticles = new List<Search.SearchResult>();

            Id[] fixedSearchResults = new Id[]{};
            for( Integer i = 0; i < articles.size(); i++) {
                fixedSearchResults.add( articles[i].Id);
            }
            Test.setFixedSearchResults( fixedSearchResults);

            Test.startTest();
                Search.SearchResults searchResults = new KnowledgeDataAccessService().searchKnowledgeArticlesByKeyword( 'Test', 0, 200);
                knowledgeArticles = searchResults.get( 'Knowledge__kav');
            Test.stopTest();

            // Assert
            System.assertEquals( articles.size(), knowledgeArticles.size(), 'The correct number of records did not come back from the query');
        }
    }

    // Validate that the correct amount of knowledgeArticles were returned based on a keyword.
    @isTest
    static void searchKnowledgeArticlesByKeyword_invalidSearch_noArticlesReturned() {
        System.runAs(knowledgeUser) {
            // Arrange
            List<Knowledge__kav> articles = setupArticles();
            List<Search.SearchResult> knowledgeArticles = new List<Search.SearchResult>();
            Test.startTest();
                Search.SearchResults searchResults = new KnowledgeDataAccessService().searchKnowledgeArticlesByKeyword( 'noValues', 0, 200);
                knowledgeArticles = searchResults.get( 'Knowledge__kav');
            Test.stopTest();

            // Assert
            System.assertEquals( 0, knowledgeArticles.size(), 'The correct number of records did not come back from the query');
        }
    }

    // Validate that the correct amount of knowledgeArticles were returned using SOSL
    @isTest
    static void getKnowledgeArticleByKeywordCount_withKeyword_countReturnedSuccess() {
        System.runAs(knowledgeUser) {
            // Arrange
            List<Knowledge__kav> articles = setupArticles();
            Integer searchResultsReturned;

            Id[] fixedSearchResults = new Id[]{};
            for( Integer i = 0; i < articles.size(); i++) {
                fixedSearchResults.add( articles[i].Id);
            }
            Test.setFixedSearchResults( fixedSearchResults);

            Test.startTest();
                searchResultsReturned = new KnowledgeDataAccessService().getKnowledgeArticleByKeywordCount( 'Test');
            Test.stopTest();

            // Assert
            System.assertEquals(articles.size(), searchResultsReturned,  'The correct number of records did not come back from the query');
        }
    }

    // Validate that the correct knowledge article was returned based on the Pusblished article KnowledgeArticleId
    @isTest
    static void getKnowledgeArticleByArticleId_withId_articleByIdReturned() {
        System.runAs(knowledgeUser) {
            // Arrange
            List<Knowledge__kav> articles = setupArticles();
            Knowledge__kav knowledgeArticleReturned;

            Test.startTest();
                knowledgeArticleReturned = new KnowledgeDataAccessService().getKnowledgeArticleByArticleId( articles[0].KnowledgeArticleId);
            Test.stopTest();

            // Assert
            System.assertEquals( articles[0].Id, knowledgeArticleReturned.Id,  'The correct knowledge article was not returned');
        }
    }

    // Validate that an error is thrown when there are no records to return.
    @isTest
    static void getKnowledgeArticleByArticleId_withId_noArticlesReturned() {
        System.runAs(knowledgeUser) {
            // Arrange
            List<Knowledge__kav> articles = setupArticles();
            String errorMessage;
            Knowledge__kav knowledgeArticleReturned;

            Test.startTest();
                try {
                    knowledgeArticleReturned = new KnowledgeDataAccessService().getKnowledgeArticleByArticleId( articles[0].Id);
                } catch (Exception e) {
                    errorMessage = e.getMessage();
                }
            Test.stopTest();

            // Assert
            System.assertEquals('Script-thrown exception', errorMessage, 'The error message did not return the correct error' );
        }
    }


    // Validate that the correct amount of knowledgeArticles were returned based on category FAQ
    @isTest
    static void getTopFamilyKnowledgeArticlesFaq_byNumberOfArticles_articleReturned() {
        System.runAs(knowledgeUser) {
            // Arrange
            List<Knowledge__kav> articles = setupArticles();
            // Arrange
            List<Knowledge__kav> knowledgeArticleReturned;

            // Setup article creation.
            List<Knowledge__kav> faqArticles = KnowledgeAVTestData.Instance.insertKnowledgeAVs( 10);

            // Create articles with the default category.
            List<Knowledge__DataCategorySelection> categories = new List<Knowledge__DataCategorySelection>();

            for (Knowledge__kav faqArticle : faqArticles) {
                categories.add(KnowledgeDCSTestData.Instance
                    .forParentId(faqArticle.Id)
                    .forDataCategoryName('Pre_Payment_FAQ')
                    .create());
            }

            insert categories;

            faqArticles = [SELECT Id, KnowledgeArticleId FROM Knowledge__kav WHERE Id in :faqArticles];

            KnowledgeTestHelper.publishArticles( faqArticles);

            // Act
            Test.startTest();
                knowledgeArticleReturned = new KnowledgeDataAccessService().getTopFamilyKnowledgeArticlesFaq( faqArticles.size());
            Test.stopTest();
            // Assert
            System.assertNotEquals( faqArticles.size(), knowledgeArticleReturned.size(),  'The correct number knowledge articles were not returned');
        }
    }

    // Validate that the correct amount of knowledgeArticles were returned based on category FAQ
    @isTest
    static void getKnowledgeArticleByCategoryCount_byNumberOfArticles_articleCountReturned() {
        System.runAs(knowledgeUser) {
            // Arrange
            List<Knowledge__kav> articles = setupArticles();
            Integer searchResultsReturned;

            Test.startTest();
                searchResultsReturned = new KnowledgeDataAccessService().getKnowledgeArticleByCategoryCount( 'Documents__c');
            Test.stopTest();

            // Assert
            System.assertEquals(articles.size(), searchResultsReturned,  'The correct number of records did not come back from the query');
        }
    }

    // Validate that the correct amount of knowledgeArticles were returned based on category FAQ
    @isTest
    static void getKnowledgeArticleByCategory_byNumberOfArticles_articleCountReturned() {
        System.runAs(knowledgeUser) {
            // Arrange
            List<Knowledge__kav> articles = setupArticles();
            List<Knowledge__kav> knowledgeArticleReturned;

            Test.startTest();
                knowledgeArticleReturned = new KnowledgeDataAccessService().getKnowledgeArticleByCategory( 'Documents__c', 0, articles.size());
            Test.stopTest();

            // Assert
            System.assertEquals(articles.size(), knowledgeArticleReturned.size(),  'The correct number of records did not come back from the query');
        }
    }
}