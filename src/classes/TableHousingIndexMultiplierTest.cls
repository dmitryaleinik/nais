@IsTest
private class TableHousingIndexMultiplierTest
{

    @isTest
    private static void testTableHousingIndexMultiplier()
    {
        // create the academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        // create the table
        TableTestData.populateHousingIndexMultiplierData(academicYear.Id);

        // test old year, not in table
        System.assertEquals(8.2, TableHousingIndexMultiplier.getHousingIndexMultiplier(academicYear.Id, '1900'));

        // test year in table
        System.assertEquals(1.31, TableHousingIndexMultiplier.getHousingIndexMultiplier(academicYear.Id, '2000'));
    }
}