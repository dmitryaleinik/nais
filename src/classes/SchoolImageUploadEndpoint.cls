/**
 * SchoolImageUploadEndpoint.cls
 *
 * @description Apex REST class that exposes RESTful endpoint for async image uploads via HTTP POST
 * Built to support and work with the CKEditor Upload Image API:
 * http://docs.ckeditor.com/#!/guide/dev_file_upload
 *
 * @author Chase Logan @ Presence PG
 */
@RestResource( urlMapping='/SchoolImageUpload/*')
global class SchoolImageUploadEndpoint {
    
    /* class constants */
    private static final String FILE_NAME_PREFIX = 'imgupload_';
    private static final String FILE_NAME_SUFFIX = '.png';
    private static final String DEF_IMG_FOLDER = 'DefaultDocumentUploadFolder';
    private static final String X_DETAIL_HEADER = 'X-Detail-Id';
    private static final String INVALID_REQ_RESP_MESG = 'An error has occurred while adding image, please try again.';
    private static final String INVALID_REQ_EXC_MSG = 'Invalid request or failed auth.';
    private static final String MISSING_FOLDER_EXC_MSG = 'No value found in Mass_Email_Setting__mdt.DefaultDocumentUploadFolder';

    global class UploadException extends Exception {}

    /**
     * @description Exposed HTTP POST operation for handling incoming image uploads, creating Document(s) and returning URL
     *
     * @author Chase Logan @ Presence PG
     */
    @HttpPost
    global static ImageResponse handleImageUpload() {
        ImageResponse returnResponse;

        try {

            // early return on invalid data, request must be for a valid MES record
            if ( RestContext.request == null || RestContext.request.requestBody == null) {

                throw new UploadException( INVALID_REQ_EXC_MSG);
            } else if ( RestContext.request != null) {

                Map<String,String> headerMap = RestContext.request.headers;
                if ( headerMap != null && headerMap.get( X_DETAIL_HEADER) != null) {

                    if ( new MassEmailSendDataAccessService().getMassEmailSendById( 
                            headerMap.get( X_DETAIL_HEADER)) == null) {

                        throw new UploadException( INVALID_REQ_EXC_MSG);
                    }
                } else {

                    throw new UploadException( INVALID_REQ_EXC_MSG);
                }
            }
        } catch ( Exception e) {

            returnResponse = new ImageResponse( 0, INVALID_REQ_RESP_MESG);
            System.debug( 'DEBUG:::exception occurred in SchoolImageUploadEndpoint.handleImageUpload(), message:' + 
                e.getMessage() + ' - ' + e.getStackTraceString());
        }

        if ( returnResponse == null) {

            // images must be retrieveable by public URL in order to render correctly, must store
            // as public Document, return response includes file name and URL.
            try {

                Mass_Email_Setting__mdt massEmailMdt = [select Value__c 
                                                          from Mass_Email_Setting__mdt 
                                                         where DeveloperName = :DEF_IMG_FOLDER];

                if ( massEmailMdt == null || massEmailMdt.Value__c == null) {

                    throw new UploadException( MISSING_FOLDER_EXC_MSG);
                }
                
                String fileName = FILE_NAME_PREFIX + String.valueOf( Datetime.now().getTime()) + FILE_NAME_SUFFIX;

                Document d = new Document();
                d.Name = fileName;
                d.Body = RestContext.request.requestBody;
                d.IsPublic = true;
                d.FolderId = massEmailMdt.Value__c;

                insert d;

                String imgURL = new MassEmailController().constructLogoURL( fileName);
                returnResponse = new ImageResponse( 1, fileName, imgURL);
            } catch ( Exception e) {

                returnResponse = new ImageResponse( 0, INVALID_REQ_RESP_MESG);
                System.debug( 'DEBUG:::exception occurred in SchoolImageUploadEndpoint.handleImageUpload(), message:' + 
                    e.getMessage() + ' - ' + e.getStackTraceString());
            }
        }

        return returnResponse;
    }

    // wrapper response class with properties expected by API
    global class ImageResponse {

        public Integer uploaded { get; set; }
        public String fileName { get; set; }
        public String url { get; set; }
        public Map<String,String> error { get; set; }


        public ImageResponse( Integer uploaded, String fileName, String url) {

            this.uploaded = uploaded;
            this.fileName = fileName;
            this.url = url;
        }

        public ImageResponse( Integer uploaded, String errorMessage) {

            this.uploaded = uploaded;
            this.error = new Map<String,String>();
            this.error.put( 'message', errorMessage);
        }

    }

}