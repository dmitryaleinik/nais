@isTest 
private class SchoolAcademicYearSelectorStudentCtrTest {
    
    @isTest
    private static void OnChange_InOverlapPeriodOrMultipleYears_redictionWithIdParameter() {
        Contact student = ContactTestData.Instance.asStudent().insertContact();
        Account school = AccountTestData.Instance.asSchool().DefaultAccount;

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
        Academic_Year__c previousAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, previousAcademicYear};

        PFS__c pfsCurrent = PfsTestData.Instance
            .forAcademicYearPicklist(currentAcademicYear.Name).create();
        PFS__c pfsPrevious = PfsTestData.Instance
            .forAcademicYearPicklist(previousAcademicYear.Name).create();
        insert new List<PFS__c>{pfsCurrent, pfsPrevious};

        Applicant__c applicantCurrent = ApplicantTestData.Instance
            .forPfsId(pfsCurrent.Id)
            .forContactId(student.Id).create();
        Applicant__c applicantPrevious = ApplicantTestData.Instance
            .forPfsId(pfsPrevious.Id)
            .forContactId(student.Id).create();
        insert new List<Applicant__c>{applicantCurrent, applicantPrevious};

        Student_Folder__c studentFolder = StudentFolderTestData.Instance
            .forStudentId(student.Id)
            .forAcademicYearPicklist(currentAcademicYear.Name).create();
        Student_Folder__c studentFolderPast = StudentFolderTestData.Instance
            .forStudentId(student.Id)
            .forAcademicYearPicklist(previousAcademicYear.Name).create();
        insert new List<Student_Folder__c>{studentFolder, studentFolderPast};
        
        School_PFS_Assignment__c spfsaCurrent = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicantCurrent.Id)
            .forStudentFolderId(studentFolder.Id)
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forSchoolId(school.Id).create();
        School_PFS_Assignment__c spfsaPrevious = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicantPrevious.Id)
            .forStudentFolderId(studentFolderPast.Id)
            .forAcademicYearPicklist(previousAcademicYear.Name)
            .forSchoolId(school.Id).create();
        insert new List<School_PFS_Assignment__c>{spfsaCurrent, spfsaPrevious};

        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(studentFolder);
            SchoolFolderSummaryController consumer = new SchoolFolderSummaryController(sc);
    
            SchoolAcademicYearSelectorStudentControl testCont = new SchoolAcademicYearSelectorStudentControl();
    
            testCont.folder = studentFolder;
            testCont.Consumer = consumer;
            testCont.loadMaps();
        Test.stopTest();        
    
        System.assertEquals(2, testCont.acadYearNameToAcadYear.size());
        System.assertEquals(2, testCont.acadYearNameToFolder.values().size());
        System.assertEquals(2, testCont.AcademicYearOptions.size());
        System.assertEquals(currentAcademicYear.Name, testCont.CurrentAcademicYearName);
        System.assertEquals(currentAcademicYear.Id, testCont.CurrentAcademicYearId);
        System.assertEquals(true, testCont.InOverlapPeriodOrMultipleYears);

        testCont.NewAcademicYearId = previousAcademicYear.Id;
        PageReference newPR = testCont.SchoolAcademicYearSelectorStudent_OnChange();
        System.assert(newPR.getURL().contains(studentFolderPast.Id));
    }
}