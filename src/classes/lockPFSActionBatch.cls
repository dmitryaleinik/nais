/*
 * NAIS-2411
 *
 * From the School Portal Annual Settings page (under Setup), have two buttons: "Lock All PFSs". 
 * These buttons should change the "Family May Submit Updates" fields on all SPAs for this school/academic year. 
 * If the user is a multi-school user, it should only lock the SPAs for the current school that is "In Focus".
 *
 * Leydi, 2015
 */
global class lockPFSActionBatch implements Database.Batchable<sObject> {
    global final String query = 
        'select Id, Family_May_Submit_Updates__c '+
        'from School_PFS_Assignment__c '+
        'where School__c=\''+GlobalVariables.getCurrentSchoolId()+'\' '+
        'and Academic_Year_Picklist__c=\''+SchoolAnnualSettingsController.getSelectedAcademicYear()+'\' '+
        ' and Family_May_Submit_Updates__c!=\'No\'';
    
    global lockPFSActionBatch() {
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        list<School_PFS_Assignment__c> pfsa = new list<School_PFS_Assignment__c>();
        School_PFS_Assignment__c pfsaTmp;
        for (sObject iPfsa : scope) {
            pfsaTmp = (School_PFS_Assignment__c) iPfsa;
            pfsaTmp.Family_May_Submit_Updates__c = 'No';
            pfsa.add(pfsaTmp);
        }
        if (pfsa.size() > 0) {
            update pfsa;
        }
    }
    
    global void finish(Database.BatchableContext bc) {
    }
}