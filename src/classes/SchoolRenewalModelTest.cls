@isTest
private class SchoolRenewalModelTest
{

    private static Account testAccount;
    private static Opportunity testOpportunity;
    private static Transaction_Annual_Settings__c testTransactionAnnualSetting;
    private static Subscription_Settings__c testSubscriptionSetting;
    
    /***Test with a Basic Subscription (on the existing Opportunity)***/
    @isTest
    private static void SchoolRenewalModel_Test_Constructor_Subscription_Basic() {
        init();
        
        test.startTest();
        SchoolRenewalModel model = new SchoolRenewalModel (testAccount,testOpportunity,true);
        test.stopTest(); 
        
        system.assertEquals(testAccount.Id,model.currentSchool.Id);
        system.assertEquals(testAccount.Paid_Through__c,model.subscriptionExpiration);
        system.assertEquals(testAccount.SSS_Subscription_Type_Current__c,model.subscriptionType);
        system.assertEquals('Basic',model.newSubscriptionType);
        system.assertEquals('Basic',model.subscriptionType);
        system.assertEquals(4,model.subscriptionTypeOptions.size());
        system.assertEquals(system.today().year(),model.newSubscriptionStartDate.year());
        system.assertEquals(10,model.newSubscriptionStartDate.month());
        system.assertEquals(1,model.newSubscriptionStartDate.day());
        system.assertEquals(system.today().addYears(1).year(),model.newSubscriptionEndDate.year());
        system.assertEquals(9,model.newSubscriptionEndDate.month());
        system.assertEquals(30,model.newSubscriptionEndDate.day());
        system.assertEquals(string.valueOf(system.today().month()) + '/' + string.valueOf(system.today().day()) + '/' + string.valueOf(testAccount.Paid_Through__c.year()),model.getSubscriptionExpiration());
        system.assertEquals('10/1/' + string.valueOf(testAccount.Paid_Through__c.year()),model.getNewSubscriptionStartDate());
        system.assertEquals('9/30/' + string.valueOf(testAccount.Paid_Through__c.addYears(1).year()),model.getNewSubscriptionEndDate()); 
    }
    
    /***Test with a Full Subscription (on the existing Opportunity)***/
    @isTest
    private static void SchoolRenewalModel_Test_Constructor_Subscription_Full() {
        init();
        testOpportunity.Subscription_Type__c = 'Full';
        update testOpportunity;
        testAccount.SSS_Subscription_Type_Current__c = 'Full';
        update testAccount;
        
        test.startTest();
        SchoolRenewalModel model = new SchoolRenewalModel (testAccount,testOpportunity,true);
        test.stopTest(); 
        
        system.assertEquals(testAccount.Id,model.currentSchool.Id);
        system.assertEquals(testAccount.Paid_Through__c,model.subscriptionExpiration);
        system.assertEquals(testAccount.SSS_Subscription_Type_Current__c,model.subscriptionType);
        system.assertEquals('Full',model.newSubscriptionType);
        system.assertEquals('Full',model.subscriptionType);
        system.assertEquals(3,model.subscriptionTypeOptions.size());
        system.assertEquals(system.today().year(),model.newSubscriptionStartDate.year());
        system.assertEquals(10,model.newSubscriptionStartDate.month());
        system.assertEquals(1,model.newSubscriptionStartDate.day());
        system.assertEquals(system.today().addYears(1).year(),model.newSubscriptionEndDate.year());
        system.assertEquals(9,model.newSubscriptionEndDate.month());
        system.assertEquals(30,model.newSubscriptionEndDate.day());
        system.assertEquals(string.valueOf(system.today().month()) + '/' + string.valueOf(system.today().day()) + '/' + string.valueOf(testAccount.Paid_Through__c.year()),model.getSubscriptionExpiration());
        system.assertEquals('10/1/' + string.valueOf(testAccount.Paid_Through__c.year()),model.getNewSubscriptionStartDate());
        system.assertEquals('9/30/' + string.valueOf(testAccount.Paid_Through__c.addYears(1).year()),model.getNewSubscriptionEndDate());
    }
    
    /***Test with a Blank Subscription (on the existing Opportunity)***/
    @isTest
    private static void SchoolRenewalModel_Test_Constructor_Subscription_OppSubscription_Blank() {
        init();
        testOpportunity.Subscription_Type__c = '';
        update testOpportunity;
        
        test.startTest();
        SchoolRenewalModel model = new SchoolRenewalModel (testAccount,testOpportunity,true);
        test.stopTest(); 
        
        system.assertEquals(testAccount.Id,model.currentSchool.Id);
        system.assertEquals(testAccount.Paid_Through__c,model.subscriptionExpiration);
        system.assertEquals(testAccount.SSS_Subscription_Type_Current__c,model.subscriptionType);
        system.assertEquals('',model.newSubscriptionType);
        system.assertEquals('Basic',model.subscriptionType);
        system.assertEquals(4,model.subscriptionTypeOptions.size());
        system.assertEquals(system.today().year(),model.newSubscriptionStartDate.year());
        system.assertEquals(10,model.newSubscriptionStartDate.month());
        system.assertEquals(1,model.newSubscriptionStartDate.day());
        system.assertEquals(system.today().addYears(1).year(),model.newSubscriptionEndDate.year());
        system.assertEquals(9,model.newSubscriptionEndDate.month());
        system.assertEquals(30,model.newSubscriptionEndDate.day());
        system.assertEquals(string.valueOf(system.today().month()) + '/' + string.valueOf(system.today().day()) + '/' + string.valueOf(testAccount.Paid_Through__c.year()),model.getSubscriptionExpiration());
        system.assertEquals('10/1/' + string.valueOf(testAccount.Paid_Through__c.year()),model.getNewSubscriptionStartDate());
        system.assertEquals('9/30/' + string.valueOf(testAccount.Paid_Through__c.addYears(1).year()),model.getNewSubscriptionEndDate());
    }
    
    /***Test for when isNewSubscription is False***/
    @isTest
    private static void SchoolRenewalModel_Test_Constructor_isNewSubscription_False() {
        init();
        
        test.startTest();
        SchoolRenewalModel model = new SchoolRenewalModel (testAccount,testOpportunity,false);
        test.stopTest(); 
        
        system.assertEquals(testAccount.Id,model.currentSchool.Id);
        system.assertEquals(testAccount.Paid_Through__c,model.subscriptionExpiration);
        system.assertEquals(testAccount.SSS_Subscription_Type_Current__c,model.subscriptionType);
        system.assertEquals('Basic',model.newSubscriptionType);
        system.assertEquals('Basic',model.subscriptionType);
        system.assertEquals(4,model.subscriptionTypeOptions.size());
        system.assertEquals(system.today().addyears(-1).year(),model.newSubscriptionStartDate.year());
        system.assertEquals(10,model.newSubscriptionStartDate.month());
        system.assertEquals(1,model.newSubscriptionStartDate.day());
        system.assertEquals(system.today().year(),model.newSubscriptionEndDate.year());
        system.assertEquals(9,model.newSubscriptionEndDate.month());
        system.assertEquals(30,model.newSubscriptionEndDate.day());
        system.assertEquals(string.valueOf(system.today().month()) + '/' + string.valueOf(system.today().day()) + '/' + string.valueOf(testAccount.Paid_Through__c.year()),model.getSubscriptionExpiration());
        system.assertEquals('10/1/' + string.valueOf(testAccount.Paid_Through__c.addyears(-1).year()),model.getNewSubscriptionStartDate());
        system.assertEquals('9/30/' + string.valueOf(testAccount.Paid_Through__c.year()),model.getNewSubscriptionEndDate());
    }
    
    /***Test the Try/Catch block for when the Subscription Setting is null (Catch bloack will throw an error message)***/
    @isTest
    private static void SchoolRenewalModel_Test_Constructor_DateError() {
        init();
        delete testSubscriptionSetting;
        
        test.startTest();
        SchoolRenewalModel model = new SchoolRenewalModel (testAccount,testOpportunity,true);
        
        test.stopTest(); 
        
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean b = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('There was an error retrieving the subscription start and end dates.')) {
                b = true;
            }
        }
        system.assert(b);
    }
    
    /***Test the calculateSubscriptionFee for when there should be no discount***/
    @isTest
    private static void SchoolRenewalModel_Test_calculateSubscriptionFee_NoDiscount() {
        init();
        testTransactionAnnualSetting.Subscription_Type__c = 'Basic';
        update testTransactionAnnualSetting;
        
        test.startTest();
        SchoolRenewalModel model = new SchoolRenewalModel (testAccount,testOpportunity,true);
        test.stopTest(); 
        
        system.assertEquals(testTransactionAnnualSetting.Year__c,string.valueof(model.newSubscriptionEndDate.year()));
        
        system.assertEquals('Basic',model.newSubscriptionType);
        system.assertEquals(0,model.discount);
        system.assertEquals(100,model.subscriptionFee);
        system.assertEquals(100,model.discountedFee);
    }
    
    /***Test the calculateSubscriptionFee for when there should a discount***/
    @isTest
    private static void SchoolRenewalModel_Test_calculateSubscriptionFee_Discount() {
        init();
//        testAccount.Subscription_Discount__c = 10;
//        update testAccount;
        testOpportunity.Subscription_Type__c = 'Full';
        update testOpportunity;
        
        test.startTest();
        SchoolRenewalModel model = new SchoolRenewalModel (testAccount,testOpportunity,true);
        test.stopTest(); 
        
        system.assertEquals(testTransactionAnnualSetting.Year__c,string.valueof(model.newSubscriptionEndDate.year()));
        
        system.assertEquals('Full',model.newSubscriptionType);
        system.assertEquals(10,model.discount);
        system.assertEquals(100,model.subscriptionFee);
        system.assertEquals(90,model.discountedFee);
    }
    
    /***Create test data to use throughout this test class***/
    private static void init()
    {
        testAccount = testUtils.createAccount('Test Account', RecordTypes.schoolAccountTypeId, 1, false);
            testAccount.SSS_Subscription_Type_Current__c = 'Basic';
            testAccount.SSS_School_Code__c = 'Test';
            //testAccount.Paid_Through__c = system.today()    //not writable - new subscription created instead, account query to get updated value.
        insert testAccount;

        Subscription__c testSubscription = testUtils.createSubscription(testAccount.Id, system.today()-14, system.today(), 'No', 'Basic', true);
        
        testAccount = [
            SELECT
                Name, 
                SSS_School_Code__c,
                SSS_Subscription_Type_Current__c,
                Paid_Through__c //,
//                Subscription_Discount__c 
            FROM Account
            WHERE Id =: testAccount.Id    
        ];
        
        testTransactionAnnualSetting = new Transaction_Annual_Settings__c ( //Not included in testUtils class.
            Name = 'Full',
            Quantity__c = 1,
            Year__c = String.valueOf(System.today().addYears(1).Year()),
            Product_Amount__c = 100,
            Subscription_Type__c = 'Full'
        );
        Transaction_Annual_Settings__c testTransactionAnnualSetting2 = new Transaction_Annual_Settings__c ( //Not included in testUtils class.
            Name = 'Early Bird',
            Quantity__c = 1,
            Year__c = String.valueOf(System.today().addYears(1).Year()),
            Product_Amount__c = 10,
            Subscription_Type__c = 'Full',
            Discount_Type__c = 'Early Bird'
        );
        insert new List<Transaction_Annual_Settings__c> { testTransactionAnnualSetting, testTransactionAnnualSetting2 };

        Academic_Year__c testAcademicYear = testUtils.createAcademicYear( //Needed to create a new Opportunity
            testAccount.Paid_Through__c.addYears(1).year() + '-' + testAccount.Paid_Through__c.addYears(2).year(),true
        );
        
        Contact testContact = testUtils.createContact('Test Contact', testAccount.Id, RecordTypes.schoolStaffContactTypeId, true); //Needed to create a new Opportunity
        
        PFS__c testPFS = testUtils.createPFS('Test PFS', testAcademicYear.Id, testContact.Id, true); //Needed to create a new Opportunity
            
        testOpportunity = testUtils.createOpportunity('Test Opp', RecordTypes.opportunitySubscriptionFeeTypeId, testPFS.Id, testAcademicYear.Name, false);
        testOpportunity.AccountId = testAccount.Id;
        testOpportunity.New_Renewal__c = 'Renewal';
        testOpportunity.StageName = 'Open';
        testOpportunity.Subscription_Type__c = 'Basic';
        testOpportunity.Applications_Anticipated__c = 10;
        insert testOpportunity;

        testSubscriptionSetting = new Subscription_Settings__c ( //Not included in testUtils class.
            Name = 'Subscription Settings',
            End_Day__c = 30,
            End_Month__c = 9,
            Number_of_Months__c = 12,
            Start_Day__c = 1,
            Start_Month__c = 10
        );
        insert testSubscriptionSetting;
        
        Renewal_Annual_Settings__c rs = new Renewal_Annual_Settings__c(Name = String.valueOf(System.today().year()));
        rs.Renewal_Season_Start_Date__c = System.today();
        rs.Renewal_Season_End_Date__c = System.today().addDays(10);
        rs.Early_Bird_End_Date__c = System.today().addDays(5);
        insert rs;
    }
}