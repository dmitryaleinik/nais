@isTest
private class SchoolMessageListControllerTest {
    
    private static void CreateData() {
        list<Case> lstCases= new list<Case>();
        Integer totalCases = Integer.valueOf(SchoolPortalSettings.Record.Fee_Waivers_Table_Page_Size__c);

        for (integer i = 0; i < totalCases; i++) {
            Case c = new Case(subject = 'Test'+i,
                    description = 'Test'+i,
                    RecordTypeId = RecordTypes.schoolPortalCaseTypeId);

            lstCases.add(c);
        }

        insert lstCases;
    }

    private static void CreateData(Id PFSId, Id ContactId) {
        list<Case> lstCases= new list<Case>();
        Integer totalCases = Integer.valueOf(SchoolPortalSettings.Record.Fee_Waivers_Table_Page_Size__c);

        for(integer i = 0; i < totalCases; i++) {
            Case c = new Case(subject = 'Test'+i,
                    description = 'Test'+i,
                    contactId = ContactId,
                    RecordTypeId = RecordTypes.schoolParentCommunicationCaseTypeId,
                    PFS__c = PFSId);

            lstCases.add(c);
        }

        insert lstCases;
    }

    @isTest
    private static void pagination_expectSuccess() {
        Test.startTest();
        System.runAs(UserTestData.insertSchoolPortalUser()) {
            SchoolMessageListControllerTest.CreateData();
            PageReference pageRef = Page.SchoolSupportTicket;
            Test.setCurrentPage(pageRef);

            SchoolMessageListController controller = new SchoolMessageListController();
            controller.isSupportTicket=true;
            controller.IsFamilyPortal = false;
            system.assert(controller.Cases.size() > 0);

            if (controller.getPagination()) {
                controller.next();
                system.assertEquals(controller.pageNumber, 2);
                controller.previous();
                system.assertEquals(controller.pageNumber, 1);
                controller.LastPage();
                system.assertEquals(controller.pageNumber, 2);
                controller.FirstPage();
                system.assertEquals(controller.pageNumber, 1);
                controller.pageNumber=2;
                controller.goTo();
            }
        }
        Test.stopTest();
    }

    @isTest
    private static void SchoolListMessageTest() {
        User schoolPortalUser = UserTestData.insertSchoolPortalUser();

        Test.startTest();
        Integer numberOfCases = Integer.valueOf(SchoolPortalSettings.Record.Fee_Waivers_Table_Page_Size__c);
        Integer numberOfRecordsPerPage = 5;
        Integer numberOfPages = 4;

        system.runAs(schoolPortalUser) {
            PFS__c pfs1 = PfsTestData.Instance.insertPfs();
            PFS__c pfs2 = PfsTestData.Instance.insertPfs();

            SchoolMessageListControllerTest.CreateData(pfs1.Id, pfs1.Parent_A__c);
            SchoolMessageListControllerTest.CreateData(pfs2.Id, pfs2.Parent_A__c);

            PageReference pageRef = Page.SchoolListMessage;
            Test.setCurrentPage(pageRef);

            SchoolMessageListController controller = new SchoolMessageListController();
            controller.isSupportTicket=false;
            controller.IsFamilyPortal = false;
            system.assert(controller.Cases.size() > 0);

            // Update limit size to be the number of records per page.
            controller.totalRecs = numberOfCases;
            controller.limitSize = numberOfRecordsPerPage;
            controller.totalPages = numberOfPages;
            System.assert(controller.getPagination(), 'Expected pagination to be used.');

            if (controller.getPagination()) {
                controller.next();
                system.assertEquals(2, controller.pageNumber);
                controller.previous();
                system.assertEquals(1, controller.pageNumber);
                controller.LastPage();
                system.assertEquals(numberOfPages, controller.pageNumber);
                controller.FirstPage();
                system.assertEquals(1, controller.pageNumber);
                controller.pageNumber=2;
                controller.goTo();
                controller.NewAction();
                pageRef.getParameters().put('column','CreatedBy.name');
                controller.sort();
                controller.sort();
                controller.sort();
            }

            // Test Academic Year Selector
            SchoolMessageListController c = controller.me;
            controller.loadAcademicYear();
            controller.getAcademicYearId();
            controller.SchoolAcademicYearSelector_OnChange(true);

        }
        Test.stopTest();
    }
}