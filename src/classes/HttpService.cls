/** 
 * HttpService.cls
 * 
 * @description: Service class for constructing and sending HTTP requests
 *
 * @author: Chase Logan @ Presence PG
 */
public class HttpService {

    /* static constants for status and http method */
    public static final String HTTP_GET = 'GET';
    public static final String HTTP_POST = 'POST';
    public static final String HTTP_PUT = 'PUT';
    public static final Integer HTTP_OK = 200;

    // Extend base exception for custom throws
    public class HttpServiceException extends Exception {}


    /** 
     * @description Overloaded option constructs and sends HttpRequest based on provided input endpoint and method, 
     * if no method is provided, default to GET_METHOD
     *
     * @param httpEndpoint - the URL to send the request
     * @param httpMethod - the http request method (IE GET, POST, etc) 
     *
     * @return String - the raw HttpResponse message body String
     */
    public static String sendHttpRequest( String httpEndpoint, String httpMethod) {

        return HttpService.sendHttpRequest( httpEndpoint, null, null, httpMethod);
    }

    /** 
     * @description Overloaded option constructs and sends HttpRequest based on provided input endpoint, body (optional), 
     * and method, if no method is provided, default to GET_METHOD
     *
     * @param httpEndpoint - the URL to send the request
     * @param httpBody - the optional body of the http request
     * @param httpMethod - the http request method (IE GET, POST, etc) 
     *
     * @return String - the raw HttpResponse message body String
     */
    public static String sendHttpRequest( String httpEndpoint, String httpBody, String httpMethod) {

        return HttpService.sendHttpRequest( httpEndpoint, null, httpBody, httpMethod);
    }
    
    /** 
     * @description Constructs and sends HttpRequest based on provided input endpoint, headers (optional), body (optional), 
     * and method, if no method is provided, default to GET_METHOD
     *
     * @param httpEndpoint - the URL to send the request
     * @param httpHeaders - the map of custom http headers to apply to the request, in header name (key), header val (value) format
     * @param httpBody - the optional body of the http request
     * @param httpMethod - the http request method (IE GET, POST, etc) 
     *
     * @return String - the raw HttpResponse message body String
     */
    public static String sendHttpRequest( String httpEndpoint, Map<String, String> httpHeaders, String httpBody, String httpMethod) {
        String returnVal = '';
        
        if ( !String.isEmpty( httpEndpoint)) {
            
            try {
                
                // set request endpoint and method
                HttpRequest httpReq = new HttpRequest();
                httpReq.setEndpoint( httpEndpoint);

                if( !String.isEmpty( httpBody)) {
                    httpReq.setBody( httpBody);
                }

                // handle any headers present
                if ( httpHeaders != null && httpHeaders.size() > 0) {
                    for ( String s : httpHeaders.keySet()) httpReq.setHeader( s, httpHeaders.get( s));
                }
                httpReq.setMethod( 
                    ( !String.isEmpty( httpMethod) ? httpMethod : HttpService.HTTP_GET));
                
                // execute request and return response body
                Http http = new Http();
                HttpResponse httpResp = http.send( httpReq);
                
                returnVal = httpResp.getBody();
            } catch ( Exception e) {
                
                // log exception but re-throw to caller for handling
                System.debug( 'DEBUG:::Exception in HttpService.sendHttpRequest, message: ' 
                    + e.getMessage());
                throw new HttpServiceException( e);
            }
        }
        
        return returnVal;
    }

}