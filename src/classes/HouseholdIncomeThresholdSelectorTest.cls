@isTest
private class HouseholdIncomeThresholdSelectorTest {

    private static void assertFamilySizeDescendingOrder(List<Household_Income_Threshold__c> records) {
        System.assertNotEquals(null, records, 'Expected the records to not be null.');

        Decimal familySize = -1;
        for (Household_Income_Threshold__c record : records) {
            System.assertNotEquals(null, record.Family_Size__c, 'The family size should not be null for this assertion.');

            // For the first record, the previous date will be null. Set it, and continue to the next record.
            if (familySize == -1) {
                familySize = record.Family_Size__c;
                continue;
            }

            System.assert(familySize >= record.Family_Size__c, 
                 String.format('Expected the previous familySize to be greater or equal to our after familySize. ' +
                'Previous familySize: {0} Next familySize: {1}', new List<String> { String.valueOf(familySize), String.valueOf( record.Family_Size__c) }));
        }
    }

    private static Map<Id, Household_Income_Threshold__c> insertRecords() {
        return new Map<Id, Household_Income_Threshold__c>(HouseholdIncomeThresholdTestData.Instance.insertIncomeThresholds());
    }

    @isTest
    private static void selectById_emptySetOfIds_expectEmptyList() {
        Map<Id, Household_Income_Threshold__c> recordsById = insertRecords();

        Set<Id> recordIds = new Set<Id>();

        List<Household_Income_Threshold__c> queriedRecords = HouseholdIncomeThresholdSelector.newInstance().selectById(recordIds);

        System.assertNotEquals(null, queriedRecords, 'Expected the queried records to not be null.');
        System.assertEquals(0, queriedRecords.size(), 'Expected zero records to be queried.');
    }

    @isTest
    private static void selectById_setWithRecordIds_expectRecordsQueriedInCorrectOrder() {
        Map<Id, Household_Income_Threshold__c> recordsById = insertRecords();

        Set<Id> recordIds = recordsById.keySet();

        List<Household_Income_Threshold__c> queriedRecords = HouseholdIncomeThresholdSelector.newInstance().selectById(recordIds);

        System.assertNotEquals(null, queriedRecords, 'Expected the queried records to not be null.');
        System.assert(!queriedRecords.isEmpty(), 'Expected records to have actually been queried.');
        System.assertEquals(recordsById.size(), queriedRecords.size(), 'Expected all inserted records to be queried.');
        assertFamilySizeDescendingOrder(queriedRecords);
    }
}