/**
 * @description This class is used to create State Tax Table records for unit tests.
 */
@isTest
public class StateTaxTableTestData extends SObjectTestData {
    @testVisible private static final Decimal INCOME_LOW = 20;
    @testVisible private static final Decimal PERCENT_OF_TOTAL_INCOME = 1.5;
    @testVisible private static final String STATE = 'New York';

    /**
     * @description Get the default values for the State_Tax_Table__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                State_Tax_Table__c.Academic_Year__c => AcademicYearTestData.Instance.DefaultAcademicYear.Id,
                State_Tax_Table__c.Income_Low__c => INCOME_LOW,
                State_Tax_Table__c.Percent_of_Total_Income__c => PERCENT_OF_TOTAL_INCOME,
                State_Tax_Table__c.State__c => STATE
        };
    }

    /**
     * @description Set the Academic Year for the current State Tax Table record.
     * @param academicYearId The Id of the AcademicYear to set on the current
     *             State Tax Table record.
     * @return The current instance of StateTaxTableTestData.
     */
    public StateTaxTableTestData forAcademicYearId(Id academicYearId) {
        return (StateTaxTableTestData) with(State_Tax_Table__c.Academic_Year__c, academicYearId);
    }

    /**
     * @description Set the State for the current State Tax Table record.
     * @param state The State Name to set on the current State Tax Table record.
     * @return The current instance of StateTaxTableTestData.
     */
    public StateTaxTableTestData forState(String state) {
        return (StateTaxTableTestData) with(State_Tax_Table__c.State__c, state);
    }

    /**
     * @description Set the Income Low for the current State Tax Table record.
     * @param incomeLow The Income Low to set on the current State Tax Table record.
     * @return The current instance of StateTaxTableTestData.
     */
    public StateTaxTableTestData forIncomeLow(Decimal incomeLow) {
        return (StateTaxTableTestData) with(State_Tax_Table__c.Income_Low__c, incomeLow);
    }

    /**
     * @description Insert the current working State_Tax_Table__c record.
     * @return The currently operated upon State_Tax_Table__c record.
     */
    public State_Tax_Table__c insertStateTaxTable() {
        return (State_Tax_Table__c)insertRecord();
    }

    /**
     * @description Create the current working State Tax Table record without resetting
     *             the stored values in this instance of StateTaxTableTestData.
     * @return A non-inserted State_Tax_Table__c record using the currently stored field
     *             values.
     */
    public State_Tax_Table__c createStateTaxTableWithoutReset() {
        return (State_Tax_Table__c)buildWithoutReset();
    }

    /**
     * @description Create the current working State_Tax_Table__c record.
     * @return The currently operated upon State_Tax_Table__c record.
     */
    public State_Tax_Table__c create() {
        return (State_Tax_Table__c)super.buildWithReset();
    }

    /**
     * @description The default Federal_Tax_Table__c record.
     */
    public State_Tax_Table__c DefaultStateTaxTable {
        get {
            if (DefaultStateTaxTable == null) {
                DefaultStateTaxTable = createStateTaxTableWithoutReset();
                insert DefaultStateTaxTable;
            }
            return DefaultStateTaxTable;
        }
        private set;
    }

    /**
     * @description Get the State_Tax_Table__c SObjectType.
     * @return The State_Tax_Table__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return State_Tax_Table__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static StateTaxTableTestData Instance {
        get {
            if (Instance == null) {
                Instance = new StateTaxTableTestData();
            }
            return Instance;
        }
        private set;
    }

    private StateTaxTableTestData() { }
}