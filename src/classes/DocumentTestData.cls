/**
 * @description This class is used to create Document records for unit tests.
 */
@isTest
public class DocumentTestData extends SObjectTestData {

    @testVisible private static final String DOCUMENT_DEFAULT_NAME = 'Document Name';

    /**
     * @description Get the default values for the Document object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Document.Name => DOCUMENT_DEFAULT_NAME,
                Document.FolderId => UserInfo.getUserId()
        };
    }

    /**
     * @description Set the Name field on the current Document record.
     * @param docName The Document Name to set on the Document.
     * @return The current working instance of DocumentTestData.
     */
    public DocumentTestData forDocumentName(String docName) {
        return (DocumentTestData) with(Document.Name, docName);
    }

    /**
     * @description Set the DeveloperName field on the current Document record.
     * @param docDeveloperName The Document DeveloperName to set on the Document.
     * @return The current working instance of DocumentTestData.
     */
    public DocumentTestData forDocumentDeveloperName(String docDeveloperName) {
        return (DocumentTestData) with(Document.DeveloperName, docDeveloperName);
    }

    /**
     * @description Insert the current working Document record.
     * @return The currently operated upon Document record.
     */
    public Document insertDocument() {
        Document document;
        try {
            document = (Document)buildWithReset();
            insert document;
        } catch (System.DmlException e) {
            // If there is a duplicate developer name, try again
            if (e.getMessage().contains('DUPLICATE_DEVELOPER_NAME')) {
                document.DeveloperName = generateRandomString();
                insert document;
            } else {
                System.assert(false, e.getMessage() + ' ||| ' + e.getStackTraceString());
            }
        }

        return document;
    }

    /**
     * @description Create the current working Document record without resetting
     *          the stored values in this instance of DocumentTestData.
     * @return A non-inserted Document record using the currently stored field
     *          values.
     */
    public Document createDocumentWithoutReset() {
        return (Document)buildWithoutReset();
    }

    /**
     * @description Create the current working Document record.
     * @return The currently operated upon Document record.
     */
    public Document create() {
        return (Document)super.buildWithReset();
    }

    /**
     * @description The default Document record.
     */
    public Document DefaultDocument {
        get {
            if (DefaultDocument == null) {
                DefaultDocument = createDocumentWithoutReset();
                insert DefaultDocument;
            }
            return DefaultDocument;
        }
        private set;
    }

    /**
     * @description Get the Document SObjectType.
     * @return The Document SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Document.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static DocumentTestData Instance {
        get {
            if (Instance == null) {
                Instance = new DocumentTestData();
            }
            return Instance;
        }
        private set;
    }

    private DocumentTestData() { }
}