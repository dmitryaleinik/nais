public class EfcCalculatorBatchScheduler implements Schedulable {
    
    public void execute(SchedulableContext SC) {
        boolean runSSS = !(EfcUtil.getEfcSettingDisableSssAutoEfcCalc());
        boolean runRevision = !(EfcUtil.getEfcSettingDisableSssAutoEfcCalc());
        
        if (runSSS && runRevision) {
            EfcCalculatorBatchProcessor.doExecuteBatch();
        }
        else if (runSSS) {
            EfcCalculatorBatchProcessor.doExecuteBatch(EfcCalculatorBatchProcessor.CalcType.SSS);
        }
        else if (runRevision) {
            EfcCalculatorBatchProcessor.doExecuteBatch(EfcCalculatorBatchProcessor.CalcType.Revision);
        }
    }
}