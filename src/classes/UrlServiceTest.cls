@isTest
private class UrlServiceTest {
    private static final String DUMMY_PARAM = 'dummyParam';

    private static void setup() {
        setup(UrlService.ID_PARAM);
    }

    private static void setup(String param) {
        PFS__c pfs = new PFS__c();
        insert pfs;
        System.assertNotEquals(null, pfs.Id, 'Expected the PFS to be inserted.');

        ApexPages.CurrentPage().getParameters().put(param, pfs.Id);
    }

    @isTest
    private static void getIdByType_idPresentCorrectType_expectIdReturned() {
        setup();
        Id pfsId = ApexPages.CurrentPage().getParameters().get(UrlService.ID_PARAM);

        Test.startTest();
        Id retrievedPfsId = UrlService.Instance.getIdByType(PFS__c.getSObjectType());
        Test.stopTest();

        System.assertNotEquals(null, retrievedPfsId, 'Expected an Id to be returned.');
        System.assertEquals(pfsId, retrievedPfsId, 'Expected ' + pfsId + ' to be returned.');
    }

    @isTest
    private static void getIdByType_idPresentWrongType_expectNullReturned() {
        setup();
        Id pfsId = ApexPages.CurrentPage().getParameters().get(UrlService.ID_PARAM);

        Test.startTest();
        Id retrievedPfsId = UrlService.Instance.getIdByType(FamilyAppSettings__c.getSObjectType());
        Test.stopTest();

        System.assertEquals(null, retrievedPfsId, 'Expected null to be returned.');
    }

    @isTest
    private static void getIdByType_noIdPresent_expectNullReturned() {
        Test.startTest();
        Id retrievedPfsId = UrlService.Instance.getIdByType(PFS__c.getSObjectType());
        Test.stopTest();

        System.assertEquals(null, retrievedPfsId, 'Expected null to be returned.');
    }

    @isTest
    private static void getIdByType_nullTypeRequested_expectArgumentNullException() {
        try {
            Test.startTest();
            UrlService.Instance.getIdByType(null);

            System.assert(false, 'Expected an ArgumentNullException thrown.');
        } catch (Exception e) {
            System.assertEquals(ArgumentNullException.class.getName(), e.getTypeName(),
                    'Expected an ArgumentNullException.');

            String expectedMessage = String.format(ArgumentNullException.MESSAGE,
                    new List<String> { UrlService.DESIRED_TYPE_PARAM });
            System.assertEquals(expectedMessage, e.getMessage(), 'Expected the expectedMessage returned.');
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void getIdByType_nullParam_expectArgumentNullException() {
        try {
            Test.startTest();
            UrlService.Instance.getIdByType(null, null);

            System.assert(false, 'Expected an ArgumentNullException thrown.');
        } catch (Exception e) {
            System.assertEquals(ArgumentNullException.class.getName(), e.getTypeName(),
                    'Expected an ArgumentNullException.');

            String expectedMessage = String.format(ArgumentNullException.MESSAGE,
                    new List<String> { UrlService.QUERY_PARAM_PARAM });
            System.assertEquals(expectedMessage, e.getMessage(), 'Expected the expectedMessage returned.');
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void getIdByType_noParamPresent_expectNullReturned() {
        Test.startTest();
        Id retrievedPfsId = UrlService.Instance.getIdByType(DUMMY_PARAM, PFS__c.getSObjectType());
        Test.stopTest();

        System.assertEquals(null, retrievedPfsId, 'Expected null to be returned.');
    }

    @isTest
    private static void getIdByType_paramPopulatedDifferentParamRequested_expectNull() {
        setup();

        Test.startTest();
        Id retrievedPfsId = UrlService.Instance.getIdByType(DUMMY_PARAM, PFS__c.getSObjectType());
        Test.stopTest();

        System.assertEquals(null, retrievedPfsId, 'Expected null to be returned.');
    }

    @isTest
    private static void getIdByType_paramPopulated_expectId() {
        setup(DUMMY_PARAM);
        Id pfsId = ApexPages.CurrentPage().getParameters().get(DUMMY_PARAM);

        Test.startTest();
        Id retrievedPfsId = UrlService.Instance.getIdByType(DUMMY_PARAM, PFS__c.getSObjectType());
        Test.stopTest();

        System.assertEquals(pfsId, retrievedPfsId, 'Expected the PFS Id to be returned.');
    }
}