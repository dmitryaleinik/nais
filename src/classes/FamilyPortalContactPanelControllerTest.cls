/**
 * FamilyPortalContactPanelControllerTest.cls
 *
 * @description: Test class for FamilyPortalContactPanelController.cls No Real data to setup, just make sure the Metadata is not empty. 
 *
 * @author: Mike havrilla @ Presence PG
 */
@isTest
public class FamilyPortalContactPanelControllerTest {

    @isTest
    private static void loadLiveAgentConfiguration_getLiveAgentInformation_liveAgentConfigPopulated() {
        // Arrange
        // No Action
        
        // Act 
        Test.startTest();
            LiveAgentDeploymentService.LiveAgentConfigurationWrapper wrapper = FamilyPortalContactPanelController.getLiveAgentInformation('FamilyDocuments');
        Test.stopTest();
        
        //Assert
        System.assert( String.isNotEmpty(wrapper.liveAgentButtonId));
        System.assert( String.isNotEmpty(wrapper.liveAgentDeploymentUrl));
        System.assert( String.isNotEmpty(wrapper.liveAgentDeploymentId));
        System.assert( String.isNotEmpty(wrapper.liveAgentChatUrl));
    }
}