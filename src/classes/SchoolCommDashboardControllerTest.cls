/**
 * SchoolCommDashboardControllerTest.cls
 *
 * @description: Test class for SchoolCommDashboardController using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
private class SchoolCommDashboardControllerTest {

    /* test data setup */
    @testSetup 
    static void setupTestData() {
        MassEmailSendTestDataFactory.createEmailTemplate();
        
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
            
             MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
        }
    }

    /* postive test cases */

    // test controller init w/ header/academic year selector
    @isTest 
    static void constructor_validUser_validInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            Test.startTest();
                
                PageReference commPRef = Page.SchoolCommDashboard;
                Test.setCurrentPage( commPRef);
                SchoolCommDashboardController commDashboardController = new SchoolCommDashboardController();
                commDashboardController.loadAcademicYear();
                commDashboardController.SchoolAcademicYearSelector_OnChange( null);
            Test.stopTest();

            // Assert
            System.assertEquals( commDashboardController.Me, commDashboardController);
            System.assertEquals( commDashboardController.getAcademicYearId(), GlobalVariables.getCurrentAcademicYear().Id);
        }
    }

    // test controller init w/ header/academic year selector
    @isTest 
    static void launchMassEmail_validUser_validPageRef() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            Test.startTest();
                
                PageReference commPRef = Page.SchoolCommDashboard;
                Test.setCurrentPage( commPRef);
                SchoolCommDashboardController commDashboardController = new SchoolCommDashboardController();
                Boolean enabled = commDashboardController.massEmailEnabled;
                PageReference pRef = commDashboardController.launchMassEmail();
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, pRef);
        }
    }

}