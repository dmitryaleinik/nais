/**
 * @description This class is used to create Chargent Order records for unit tests.
 */
@isTest
public class ChargentOrderTestData extends SObjectTestData {

    /**
     * @description Get the default values for the ChargentOrders__ChargentOrder__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
        };
    }

    /**
     * @description Insert the current working ChargentOrders__ChargentOrder__c record.
     * @return The currently operated upon ChargentOrders__ChargentOrder__c record.
     */
    public ChargentOrders__ChargentOrder__c insertChargentOrder() {
        return (ChargentOrders__ChargentOrder__c)insertRecord();
    }

    /**
     * @description Inserts a list of ChargentOrders__ChargentOrder__c records.
     * @param numberOfRecords Number of records to insert.
     * @return The currently operated upon ChargentOrders__ChargentOrder__c record.
     */
    public List<ChargentOrders__ChargentOrder__c> insertChargentOrders(Integer numberOfRecords) {
        return (List<ChargentOrders__ChargentOrder__c>)insertRecords(numberOfRecords);
    }

    /**
     * @description Create the current working Chargent Order record without resetting
     *          the stored values in this instance of ChargentOrderTestData.
     * @return A non-inserted ChargentOrders__ChargentOrder__c record using the currently stored field
     *          values.
     */
    public ChargentOrders__ChargentOrder__c createChargentOrderWithoutReset() {
        return (ChargentOrders__ChargentOrder__c)buildWithoutReset();
    }

    /**
     * @description Create the current working ChargentOrders__ChargentOrder__c record.
     * @return The currently operated upon ChargentOrders__ChargentOrder__c record.
     */
    public ChargentOrders__ChargentOrder__c create() {
        return (ChargentOrders__ChargentOrder__c)super.buildWithReset();
    }

    /**
     * @description The default ChargentOrders__ChargentOrder__c record.
     */
    public ChargentOrders__ChargentOrder__c DefaultChargentOrder {
        get {
            if (DefaultChargentOrder == null) {
                DefaultChargentOrder = createChargentOrderWithoutReset();
                insert DefaultChargentOrder;
            }
            return DefaultChargentOrder;
        }
        private set;
    }

    /**
     * @description Get the ChargentOrders__ChargentOrder__c SObjectType.
     * @return The ChargentOrders__ChargentOrder__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return ChargentOrders__ChargentOrder__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static ChargentOrderTestData Instance {
        get {
            if (Instance == null) {
                Instance = new ChargentOrderTestData();
            }
            return Instance;
        }
        private set;
    }

    private ChargentOrderTestData() { }
}