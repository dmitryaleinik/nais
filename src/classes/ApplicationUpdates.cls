/**
 * Created by Nate on 7/19/17.
 */

public class ApplicationUpdates {


    @testVisible private static final String APPLICATION_UPDATES_PARAM = 'ApplicationUpdates';


    @testvisible private static List<Application_Update__e> newApplicationUpdates;

    private ApplicationUpdates(List<Application_Update__e> applicationUpdates) {
        newApplicationUpdates = applicationUpdates;
    }

    @testVisible
    private static List<Application_Update__e> getEvents() {
        return newApplicationUpdates == null ? new List<Application_Update__e>() : newApplicationUpdates;
    }

    public static ApplicationUpdates newInstance(List<Application_Update__e> applicationUpdates) {
        ArgumentNullException.throwIfNull(applicationUpdates, APPLICATION_UPDATEs_PARAM);
        return new ApplicationUpdates(applicationUpdates);
    }

}