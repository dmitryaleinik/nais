/**
 * @description This class is used to create Question Labels records for unit tests.
 */
@isTest
public class QuestionLabelsTestData extends SObjectTestData {
    @testVisible private static final String NAME = '2016-2017 English';

    /**
     * @description Get the default values for the Question_Labels__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Question_Labels__c.Name => NAME
        };
    }

    /**
     * @description Set the Academic_Year__c field on the current Question Labels record.
     * @param academicYearId The Academic Year Id to set on the Question Labels.
     * @return The current working instance of QuestionLabelsTestData.
     */
    public QuestionLabelsTestData forAcademicYearId(Id academicYearId) {
        return (QuestionLabelsTestData) with(Question_Labels__c.Academic_Year__c, academicYearId);
    }

    /**
     * @description Set the Language__c field on the current Question Labels record.
     * @param language The Language to set on the Question Labels.
     * @return The current working instance of QuestionLabelsTestData.
     */
    public QuestionLabelsTestData forLanguage(String language) {
        return (QuestionLabelsTestData) with(Question_Labels__c.Language__c, language);
    }

    /**
     * @description Set the Additional_Businesses_Farms__c field on the current Question Labels record.
     * @param additionalBusinessesFarms The Additional Businesses Farms to set on the Question Labels.
     * @return The current working instance of QuestionLabelsTestData.
     */
    public QuestionLabelsTestData forAdditionalBusinessesFarms(String additionalBusinessesFarms) {
        return (QuestionLabelsTestData) with(Question_Labels__c.Additional_Businesses_Farms__c, additionalBusinessesFarms);
    }

    /**
     * @description Insert the current working Question_Labels__c record.
     * @return The currently operated upon Question_Labels__c record.
     */
    public Question_Labels__c insertQuestionLabels() {
        return (Question_Labels__c)insertRecord();
    }

    /**
     * @description Create the current working Question Labels record without resetting
     *             the stored values in this instance of QuestionLabelsTestData.
     * @return A non-inserted Question_Labels__c record using the currently stored field
     *             values.
     */
    public Question_Labels__c createQuestionLabelsWithoutReset() {
        return (Question_Labels__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Question_Labels__c record.
     * @return The currently operated upon Question_Labels__c record.
     */
    public Question_Labels__c create() {
        return (Question_Labels__c)super.buildWithReset();
    }

    /**
     * @description The default Question_Labels__c record.
     */
    public Question_Labels__c DefaultQuestionLabels {
        get {
            if (DefaultQuestionLabels == null) {
                DefaultQuestionLabels = createQuestionLabelsWithoutReset();
                insert DefaultQuestionLabels;
            }
            return DefaultQuestionLabels;
        }
        private set;
    }

    /**
     * @description Get the Question_Labels__c SObjectType.
     * @return The Question_Labels__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Question_Labels__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static QuestionLabelsTestData Instance {
        get {
            if (Instance == null) {
                Instance = new QuestionLabelsTestData();
            }
            return Instance;
        }
        private set;
    }

    private QuestionLabelsTestData() { }
}