/**
 * StudentFolderDataAccessService.cls
 *
 * @description Data access service class for Student_Folder__c
 *
 * @author Chase Logan @ Presence PG
 */
public class StudentFolderDataAccessService extends DataAccessService {
    
    /**
     * @description: Queries a Student_Folder__c record by ID, queries all fields by default because of the direct query of
     * a singe record by ID limited to 1
     *
     * @param studentFolderId - The ID of the Student_Folder__c record
     *
     * @return A Student_Folder__c record
     */
    public Student_Folder__c getStudentFolderById( Id studentFolderId) {
        Student_Folder__c returnVal;

        if ( studentFolderId != null) {

            try {

                returnVal = Database.query( 'select ' + String.join( super.getSObjectFieldNames( 'Student_Folder__c'), ',') +
                                             ' from Student_Folder__c' + 
                                            ' where Id = :studentFolderId' + 
                                            ' limit 1');
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in StudentFolderDataAccessService.getStudentFolderById' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }

        return returnVal;
    }

    /**
     * @description: Queries Student_Folder__c.Mass_Email_Events__c by Id set
     *
     * @param studentFolderIdSet - The Set of Student_Folder__c ID's
     *
     * @param lockRecords - a boolean indicating whether or not to apply record lock on returned records
     *
     * @return A list of Student_Folder__c records
     */
    public List<Student_Folder__c> getStudentFolderEmailEventByIdSet( Set<String> studentFolderIdSet, Boolean lockRecords) {
        List<Student_Folder__c> returnList;

        if ( studentFolderIdSet != null) {

            try {

                returnList = Database.query( 'select Mass_Email_Events__c, Name, Primary_Parent__c' +
                                               ' from Student_Folder__c' + 
                                                ' where Id in :studentFolderIdSet' +
                                                ' limit 10000' + ( lockRecords != null && lockRecords == true ? 
                                                  ' for update' : ''));
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in StudentFolderDataAccessService.getStudentFolderEmailEventByIdSet' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }

        return returnList;
    }

    /**
     * @description: Queries School_PFS_Assignment__c by Student Folder ID set
     *
     * @param studentFolderIdSet - The Set of Student_Folder__c ID's
     *
     * @return A list of School_PFS_Assignment__c records
     */
    public List<School_PFS_Assignment__c> getSchoolPFSByStudentFolderIdSet( Set<String> studentFolderIdSet) {
        List<School_PFS_Assignment__c> returnList;

        if ( studentFolderIdSet != null) {

            try {

                returnList = Database.query( 'select Student_Folder__c, Student_Folder__r.Academic_Year_Picklist__c, Applicant_First_Name__c, Applicant_Last_Name__c, PFS_Number__c,'+ 
                                                   ' PFS_Status__c, Parent_A__c, Parent_A_Email_Migrated__c, Grade_Applying__c,' +
                                                   ' Applicant__r.PFS__r.Parent_A__c' +
                                               ' from School_PFS_Assignment__c' + 
                                                ' where Student_Folder__c in :studentFolderIdSet');
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in StudentFolderDataAccessService.getSchoolPFSByStudentFolderIdSet' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }

        return returnList;
    }

    /**
     * @description: Queries School_PFS_Assignment__c by Student Folder Parent ID set
     *
     * @param parentIdSet - The Set of Parent (Contact) ID's
     *
     * @return A list of School_PFS_Assignment__c records
     */
    public List<School_PFS_Assignment__c> getSchoolPFSByParentIdSet( Set<String> parentIdSet) {
        List<School_PFS_Assignment__c> returnList;

        if ( parentIdSet != null) {

            try {

                returnList = Database.query( 'select Student_Folder__c, Student_Folder__r.Mass_Email_Events__c, Student_Folder__r.Academic_Year_Picklist__c, Applicant_First_Name__c,' +
                                                   ' Applicant_Last_Name__c, PFS_Number__c, PFS_Status__c, Parent_A__c, Parent_A_Email_Migrated__c, Grade_Applying__c,' + 
                                                   ' Applicant__r.PFS__r.Parent_A__c' +
                                               ' from School_PFS_Assignment__c' + 
                                                ' where Applicant__r.PFS__r.Parent_A__c in :parentIdSet');
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in StudentFolderDataAccessService.getSchoolPFSByParentIdSet' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }

        return returnList;
    }

}