@isTest
private class CourtesyFeeWaiversExtensionTest
{

    @isTest
    private static void testInitializeAndSave()
    {
        Account accessOrg = AccountTestData.Instance.asAccessOrganisation().forSSSSubscriberStatus('Current').DefaultAccount;

        Academic_Year__c testAcademicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        Annual_Setting__c testAnnualSettings = TestUtils.createAnnualSetting(accessOrg.Id, testAcademicYear.Id, true);

        String yearValue = GlobalVariables.getCurrentAcademicYear().Name.split('-')[0];

        TransactionAnnualSettingsTestData.Instance
            .forName('1 Pack - ' + yearValue)
            .forQuantity(1.0)
            .forYear(yearValue)
            .forProductCode('123-100 Extra Fee Waivers - 1 Pack - ' + yearValue)
            .forProductAmount(41.00)
            .insertTransactionAnnualSettings();

        // Verify loading the page
        ApexPages.StandardController stdController = new ApexPages.StandardController(accessOrg);
        CourtesyFeeWaiversExtension extension = new CourtesyFeeWaiversExtension(stdController);

        // Simulate making a selection
        extension.selectedQuantity = '4';
        extension.save();

        // Verify that the fee waiver records negate each other
        Opportunity testOpportunity = [select Id, Amount, Net_Amount_Due__c, Total_Purchased_Single_Waivers__c, Total_Purchased_Waiver_Packs__c,
                                        (select Transaction_Type__c, Product_Code__c, Waiver_Quantity__c from Transaction_Line_Items__r)
                                            from Opportunity];

        System.assertEquals(164.00, testOpportunity.Amount);
        System.assertEquals(0, testOpportunity.Net_Amount_Due__c);

        List<Transaction_Line_Item__c> createdSaleLineItems = [select Id from Transaction_Line_Item__c where RecordTypeId = :RecordTypes.saleTransactionTypeId];
        System.assertEquals(1, createdSaleLineItems.size());

        List<Transaction_Line_Item__c> createdAdjustmentLineItems = [select Id from Transaction_Line_Item__c where RecordTypeId = :RecordTypes.adjustmentTransactionTypeId];
        System.assertEquals(1, createdAdjustmentLineItems.size());

        testAnnualSettings = [select Id, Total_Waivers_Override_Default__c from Annual_Setting__c where Id = :testAnnualSettings.Id limit 1];
        System.assertEquals(4, testAnnualSettings.Total_Waivers_Override_Default__c);
    }

    @isTest
    private static void testInitializeAndSave_notCurrentSubscriber()
    {
        Account accessOrg = AccountTestData.Instance.asAccessOrganisation().forSSSSubscriberStatus('Former').DefaultAccount;

        Academic_Year__c testAcademicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        Annual_Setting__c testAnnualSettings = TestUtils.createAnnualSetting(accessOrg.Id, testAcademicYear.Id, true);

        String yearValue = GlobalVariables.getCurrentAcademicYear().Name.split('-')[0];

        TransactionAnnualSettingsTestData.Instance
            .forName('1 Pack - ' + yearValue)
            .forQuantity(1.0)
            .forYear(yearValue)
            .forProductCode('123-100 Extra Fee Waivers - 1 Pack - ' + yearValue)
            .forProductAmount(41.00)
            .insertTransactionAnnualSettings();

        // Verify loading the page
        ApexPages.StandardController stdController = new ApexPages.StandardController(accessOrg);
        CourtesyFeeWaiversExtension extension = new CourtesyFeeWaiversExtension(stdController);

        // Simulate making a selection
        extension.selectedQuantity = '4';

        Test.startTest();
            extension.save();

            List<Opportunity> testOpportunities = [
                SELECT Id, Amount, Net_Amount_Due__c, Total_Purchased_Single_Waivers__c, Total_Purchased_Waiver_Packs__c,
                        (SELECT Transaction_Type__c, Product_Code__c, Waiver_Quantity__c FROM Transaction_Line_Items__r)
                FROM Opportunity];

            System.assertEquals(0, testOpportunities.size());
        Test.stopTest();
    }

    @isTest
    private static void testLimitedVisibility()
    {
        Profile_Type_Grouping__c ptg3 = new Profile_Type_Grouping__c();
        ptg3.Name = 'TCN Agent - Family';
        ptg3.Profile_Name__c = 'TCN Agent - Family';
        ptg3.Is_Call_Center_Profile__c = true;

        Profile_Type_Grouping__c ptg2 = new Profile_Type_Grouping__c();
        ptg2.Name = 'TCN Agent - Universal';
        ptg2.Profile_Name__c = 'TCN Agent - Universal';
        ptg2.Is_Call_Center_Profile__c = true;

        insert new List<Profile_Type_Grouping__c> {ptg2, ptg3};

        Account accessOrg = AccountTestData.Instance.asAccessOrganisation().forSSSSubscriberStatus('Current').DefaultAccount;
        Academic_Year__c testAcademicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        User callCenterUser = new User();
        callCenterUser.LastName = 'call center';
        callCenterUser.Username = 'callcenter@call.call';
        callCenterUser.Email = 'callcenter@call.call';
        callCenterUser.Alias = 'call';
        callCenterUser.TimeZoneSidKey = 'America/New_York';
        callCenterUser.LocaleSidKey = 'en_US';
        callCenterUser.EmailEncodingKey = 'ISO-8859-1';
        callCenterUser.ProfileId = GlobalVariables.getCallCenterProfileIds()[0];
        callCenterUser.LanguageLocaleKey = 'en_US';
        callCenterUser.isActive = true;
        callCenterUser.Can_Apply_Courtesy_Waivers__c = false;
        insert callCenterUser;

        System.runAs(callCenterUser){
            // Verify loading the page
            ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(accessOrg);
            CourtesyFeeWaiversExtension extension = new CourtesyFeeWaiversExtension(stdController);

            System.assertEquals(false, extension.displayInterface);
        }
    }

    @isTest
    private static void testFullVisibility()
    {
        Profile_Type_Grouping__c ptg1 = new Profile_Type_Grouping__c();
        ptg1.Name = 'NAIS Power User';
        ptg1.Profile_Name__c = 'NAIS Power User';
        ptg1.Is_Sys_Admin__c = true;
        ptg1.Is_Call_Center_Profile__c = true;
        insert new List<Profile_Type_Grouping__c> {ptg1};

        Account accessOrg = AccountTestData.Instance.asAccessOrganisation().forSSSSubscriberStatus('Current').DefaultAccount;
        Academic_Year__c testAcademicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        User powerUser = new User();
        powerUser.LastName = 'power user';
        powerUser.Username = 'poweruser@call.call';
        powerUser.Email = 'poweruser@call.call';
        powerUser.Alias = 'powe';
        powerUser.TimeZoneSidKey = 'America/New_York';
        powerUser.LocaleSidKey = 'en_US';
        powerUser.EmailEncodingKey = 'ISO-8859-1';
        powerUser.ProfileId = [select Id from Profile where Name = 'NAIS Power User' limit 1].Id;
        powerUser.LanguageLocaleKey = 'en_US';
        powerUser.isActive = true;
        powerUser.Can_Apply_Courtesy_Waivers__c = true;
        insert powerUser;

        System.runAs(powerUser){
            // Verify loading the page
            ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(accessOrg);
            CourtesyFeeWaiversExtension extension = new CourtesyFeeWaiversExtension(stdController);

            System.assertEquals(true, extension.displayInterface);
        }
    }

    @isTest
    private static void testGetAcademicYears()
    {
        Account accessOrg = AccountTestData.Instance
            .forRecordTypeId(RecordTypes.accessOrgAccountTypeId).DefaultAccount;

        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(accessOrg);
        CourtesyFeeWaiversExtension extension = new CourtesyFeeWaiversExtension(stdController);

        Test.startTest();
            System.assertEquals(2, extension.getAcademicYears().size());
        Test.stopTest();
    }

    @isTest
    private static void testSaveWithSwitchingAcademicYear()
    {
        Account accessOrg = AccountTestData.Instance.asAccessOrganisation().forSSSSubscriberStatus('Current').DefaultAccount;
        String yearValue = GlobalVariables.getPreviousAcademicYear().Name.split('-')[0];

        TransactionAnnualSettingsTestData.Instance
            .forName('1 Pack - ' + yearValue)
            .forQuantity(1)
            .forYear(yearValue)
            .forProductCode('123-100 Extra Fee Waivers - 1 Pack - ' + yearValue)
            .forProductAmount(41.00)
            .insertTransactionAnnualSettings();

        ApexPages.StandardController stdController = new ApexPages.StandardController(accessOrg);
        CourtesyFeeWaiversExtension extension = new CourtesyFeeWaiversExtension(stdController);

        String academicYearA = extension.getAcademicYears()[0].getValue();
        String academicYearB = extension.getAcademicYears()[1].getValue();

        Test.startTest();
            extension.selectedAcademicYearName = academicYearA;
            extension.selectedQuantity = '1';
            extension.save();

            List<Opportunity> allOpportunities = OpportunitySelector.Instance.selectAll();
            System.assertEquals(1, allOpportunities.size());
            Id firstOpportunityId = allOpportunities[0].Id;
            System.assertEquals(academicYearA, allOpportunities[0].Academic_Year_Picklist__c);

            extension.selectedAcademicYearName = academicYearB;
            extension.selectedQuantity = '1';
            extension.save();

            allOpportunities = OpportunitySelector.Instance.selectAll();
            System.assertEquals(2, allOpportunities.size());

            if (allOpportunities[0].Id != firstOpportunityId)
            {
                System.assertEquals(academicYearB, allOpportunities[0].Academic_Year_Picklist__c,
                    'The second opportunity has academicYearB in Academic_Year_Picklist__c field');
                System.assertEquals(academicYearA, allOpportunities[1].Academic_Year_Picklist__c,
                    'The first inserted opportunity still has academicYearA in Academic_Year_Picklist__c field');
            }
            else
            {
                System.assertEquals(academicYearB, allOpportunities[1].Academic_Year_Picklist__c);
                System.assertEquals(academicYearA, allOpportunities[0].Academic_Year_Picklist__c);
            }
        Test.stopTest();
    }
}