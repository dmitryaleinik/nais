/**
 * @description This class is used to wrap School Document Assignment records and handle logic for setting document
 *              verification details.
 */
public class SchoolDocumentAssignments extends fflib_SObjectDomain {

    @testVisible private static final String SCHOOL_DOC_ASSIGNMENT_PARAM = 'schoolDocAssignment';
    @testVisible private static final String SCHOOL_DOC_ASSIGNMENTS_PARAM = 'schoolDocAssignments';
    @testVisible private static final String SETTINGS_DOMAIN_PARAM = 'settingsDomain';

    // Verification_Request_Status__c picklist values.
    public static final String VERIFICATION_REQUEST_INCOMPLETE = 'Incomplete';
    public static final String VERIFICATION_REQUEST_COMPLETE = 'Complete';
    public static final String VERIFICATION_REQUEST_ERROR = 'Error';

    private static final String BLANK_VERIFICATION_FIELDS_ERROR =
            'The SDA record is missing information necessary for setting the Verify Data field. ' +
            'The SDA record must have Document Year populated. The related SPA record with Id {0} must ' +
            'have the academic year picklist populated.';

    @testVisible private Set<String> academicYearNames;
    @testVisible private Set<Id> schoolIds;

    /**
     * @description Default constructor which will store academic year names and school Ids from the provided records.
     */
    private SchoolDocumentAssignments(List<School_Document_Assignment__c> schoolDocAssignments) {
        super(schoolDocAssignments);
        populateAcademicYearNamesAndSchoolIds((List<School_Document_Assignment__c>)this.Records);
    }

    /**
     * @description Handles logic that should be done before SDAs are inserted. New SDA records will have the Verify
     *              Data field set based on the verification mode specified on corresponding annual settings.
     * @throws DomainException if the SDA records are missing information necessary for setting Verify Data.
     */
    public override void onBeforeInsert() {
        validateFields();
        
        // Query annual setting records that apply to the SDAs, then set verify data checkbox accordingly.
        setVerificationDetails();

        // Mark records where document verification should be requested.
        flagProcessedDocsForVerification();
    }

    /**
     * @description Handles logic that should be done after SDAs are inserted. After inserting SDA records that were
     *              flagged for verification, we will queue a job to send the callouts to a third party. This only happens
     *              if the Verify_Existing_Documents__c feature toggle is enabled.
     */
    public override void onAfterInsert() {
        // Check for records that were flagged for verification and queue job to make callouts.
        queueVerificationRequests();
    }

    /**
     * @description Handles logic that should be done before SDAs are updated. This will flag records that need to be
     *              queued for verification requests in the onAfterUpdate method.
     * @param existingRecords The old records before any updates were made.
     */
    public override void onBeforeUpdate(Map<Id, SObject> existingRecords) {
        validateFields();
        // SFP-1150: Flag processed docs for verification if track verification request date is enabled. We check this toggle
        // instead since the original Verify_Existing_Documents__c toggle didn't include this functionality.
        flagProcessedDocsForVerification((Map<Id, School_Document_Assignment__c>)existingRecords);
    }

    /**
     * @description Handles logic that should be done after SDAs are updated. This will queue a job to request document
     *              verification from a third party if any records were marked for verification.
     * @param existingRecords The old records before any updates were made.
     */
    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        // Check for records that were flagged for verification and queue job to make callouts.
        queueVerificationRequests((Map<Id, School_Document_Assignment__c>)existingRecords);
    }

    /**
     * @description Queries annual setting records that apply to the SDA records then sets the Verify Data field based
     *              on the Document Verification mode specified on the corresponding annual setting.
     * @throws DomainException if the SDA records are missing information necessary for setting Verify Data.
     */
    public void setVerificationDetails() {
        throwDomainExceptionIfRecordsInvalid();

        List<Annual_Setting__c> annualSettingRecords =
                AnnualSettingsSelector.newInstance().selectBySchoolAndAcademicYear(this.schoolIds, this.academicYearNames);
        AnnualSettings settingsDomain = AnnualSettings.newInstance(annualSettingRecords);

        setVerificationDetails(settingsDomain);
    }

    /**
     * @description Sets the Verify Data field based on the Document Verification mode specified on the corresponding
     *              annual setting. The annual settings are provided to the domain class in this case. If an annual
     *              setting can not be found for an SDA's academic year and school, then no change will be made to the
     *              SDA record.
     * @param settingsDomain The annual settings to use for updating the Verify Data field.
     * @throws DomainException if the SDA records are missing information necessary for setting Verify Data.
     * @throws ArgumenNullException if settingsDomain is null.
     */
    public void setVerificationDetails(AnnualSettings settingsDomain) {
        ArgumentNullException.throwIfNull(settingsDomain, SETTINGS_DOMAIN_PARAM);

        throwDomainExceptionIfRecordsInvalid();

        for (School_Document_Assignment__c docAssignment : (List<School_Document_Assignment__c>)this.Records) {
            String verificationMode = settingsDomain.getDocVerificationMode(docAssignment.Academic_Year__c, docAssignment.School__c);
            Wrapper docAssignmentWrapper = new Wrapper(docAssignment);
            docAssignmentWrapper.setVerifyData(verificationMode);
        }
    }

    /**
     * @description Iterates over each record and determines if we need to request document verification from a third party.
     *              If a verification request is necessary, Verification_Request_Status__c is set to 'Incomplete'. This
     *              value allows scheduleable/queueable jobs to identify the records to send requests for.
     *              We need to request verification if:
     *                  - Verify Data is true
     *                  - Document lookup is populated
     *                  - Verification Requested Date is null (Only if the Track Verification Requested Date feature is enabled.)
     *                  - Document Type is one that we verify (e.g. W2, 1040, Schedule A)
     *                  - Document Status is processed.
     *                  - Verify_Existing_Documents__c feature toggle is enabled.
     */
    @testVisible
    private void flagProcessedDocsForVerification() {
        if (!FeatureToggles.isEnabled('Verify_Existing_Documents__c')) {
            return;
        }

        for (School_Document_Assignment__c docAssignment : (List<School_Document_Assignment__c>)this.Records) {
            // Check SDA to see if we may need to request verification for a document that has been processed in previous years.
            // If the record does not meet the criteria, we set Verification Request Status to null to prevent records
            // from being caught in queueable/schedulable jobs.
            if (shouldFlagForVerificationRequest(docAssignment)) {
                docAssignment.Verification_Request_Status__c = VERIFICATION_REQUEST_INCOMPLETE;
            } else {
                docAssignment.Verification_Request_Status__c = null;
            }
        }
    }

    /**
     * @description Iterates over each record and determines if we need to request document verification from a third
     *              party based on changes to the related family document. If a verification request is necessary,
     *              Verification_Request_Status__c is set to 'Incomplete'. This value allows scheduleable/queueable jobs
     *              to identify the records to send requests for. We need to request verification if:
     *                  - Verify Data is true
     *                  - Document lookup is populated and has changed from the previous value.
     *                  - Verification Requested Date is null (Only if the Track Verification Requested Date feature is enabled.)
     *                  - Document Type is one that we verify (e.g. W2, 1040, Schedule A)
     *                  - Document Status is processed.
     *                  - Track_Verification_Request_Date__c feature toggle is enabled.
     */
    private void flagProcessedDocsForVerification(Map<Id, School_Document_Assignment__c> existingRecordsById) {
        if (!FeatureToggles.isEnabled('Track_Verification_Request_Date__c')) {
            return;
        }

        for (School_Document_Assignment__c docAssignment : (List<School_Document_Assignment__c>)this.Records) {
            School_Document_Assignment__c oldRecord = existingRecordsById.get(docAssignment.Id);

            if (oldRecord == null) {
                continue;
            }

            // If the family document lookup has not changed, continue to the next record.
            if (oldRecord.Document__c == docAssignment.Document__c) {
                continue;
            }

            // If the family document has changed, check to see if this record meets the other criteria for
            // requesting verification for an existing processed document.
            if (shouldFlagForVerificationRequest(docAssignment)) {
                docAssignment.Verification_Request_Status__c = VERIFICATION_REQUEST_INCOMPLETE;
            }
        }
    }

    private Boolean shouldFlagForVerificationRequest(School_Document_Assignment__c documentAssignment) {
        // If we are tracking Verification Request Date and it is null, return false since that document has already been verified.
        if (FeatureToggles.isEnabled('Track_Verification_Request_Date__c') &&
                documentAssignment.Verification_Requested_Date__c != null) {
            return false;
        }

        return documentAssignment.Verify_Data__c && documentAssignment.Document__c != null &&
                VerificationAction.PROCESSED.equals(documentAssignment.Document_Status__c) &&
                VerificationAction.isDocTypeVerifiable(documentAssignment.Document_Type__c);
    }

    /**
     * @description Checks for any records that have been flagged for verification and queues a job to make the callouts
     *              to a third party. We return early without queueing any jobs if the Verify_Existing_Documents__c
     *              feature toggle is not enabled.
     */
    private void queueVerificationRequests() {
        if (!FeatureToggles.isEnabled('Verify_Existing_Documents__c')) {
            return;
        }

        Set<Id> recordIdsToProcess = new Set<Id>();
        for (School_Document_Assignment__c docAssignment : (List<School_Document_Assignment__c>)this.Records) {
            if (docAssignment.Verification_Request_Status__c == VERIFICATION_REQUEST_INCOMPLETE) {
                recordIdsToProcess.add(docAssignment.Id);
            }
        }

        DocumentVerificationQueue.queueVerificationRequests(recordIdsToProcess);
    }

    /**
     * @description Checks for any records that have been flagged for verification as part of an update by comparing the
     *              updated value to the previous value.
     * @param existingRecordsById The old records from an update trigger context.
     */
    private void queueVerificationRequests(Map<Id, School_Document_Assignment__c> existingRecordsById) {
        if (!FeatureToggles.isEnabled('Verify_Existing_Documents__c')) {
            return;
        }

        Set<Id> recordIdsToProcess = new Set<Id>();
        for (School_Document_Assignment__c docAssignment : (List<School_Document_Assignment__c>)this.Records) {
            School_Document_Assignment__c oldRecord = existingRecordsById.get(docAssignment.Id);

            if (oldRecord == null) {
                continue;
            }

            String oldStatus = oldRecord.Verification_Request_Status__c;
            String newStatus = docAssignment.Verification_Request_Status__c;
            if (oldStatus != newStatus && newStatus == VERIFICATION_REQUEST_INCOMPLETE) {
                recordIdsToProcess.add(docAssignment.Id);
            }
        }

        DocumentVerificationQueue.queueVerificationRequests(recordIdsToProcess);
    }

    private void populateAcademicYearNamesAndSchoolIds(List<School_Document_Assignment__c> docAssignmentRecords) {
        academicYearNames = new Set<String>();
        schoolIds = new Set<Id>();

        for (School_Document_Assignment__c schoolDocAssignment : docAssignmentRecords) {
            academicYearNames.add(schoolDocAssignment.Academic_Year__c);
            schoolIds.add(schoolDocAssignment.School_Id__c);
        }
    }

    private String createSettingKey(School_Document_Assignment__c schoolDocAssignment) {
        return schoolDocAssignment.Academic_Year__c + '-' + schoolDocAssignment.School__c;
    }

    @testVisible private static String formatBlankVerificationFieldErrorError(Id relatedPFSRecordId) {
        return String.format(BLANK_VERIFICATION_FIELDS_ERROR, new List<String> { (String)relatedPFSRecordId });
    }

    /**
     * @description Checks an SDA record to see if all fields required for properly marking an SDA record for
     *              verification are populated. The necessary fields are: School__c, School_Id__c, Academic_Year__c,
     *              and Document_Year__c.
     * @param docAssignment The SDA record to check.
     * @return True if the record has the information necessary to be marked for verification.
     */
    private Boolean requiredVerificationFieldsNotBlank(School_Document_Assignment__c docAssignment) {
        // We check the PFS formulas instead of checking the PFS assignment lookup because that is a required field.
        return String.isNotBlank(docAssignment.Document_Year__c) &&
                String.isNotBlank(docAssignment.Academic_Year__c) &&
                String.isNotBlank(docAssignment.School__c) &&
                String.isNotBlank(docAssignment.School_Id__c);
    }

    private List<String> getErrorsForUnpopulatedFormulaFields() {
        // Iterate over the records and add an error for any record that does not have all fields necessary
        // for setting the verify data field populated.
        List<String> errors = new List<String>();

        for (School_Document_Assignment__c docAssignment : (List<School_Document_Assignment__c>)this.Records) {
            if (requiredVerificationFieldsNotBlank(docAssignment)) {
                continue;
            } else {
                errors.add(formatBlankVerificationFieldErrorError(docAssignment.School_PFS_Assignment__c));
            }
        }

        return errors;
    }

    private void throwDomainExceptionIfRecordsInvalid() {
        List<String> errors = getErrorsForUnpopulatedFormulaFields();

        if (errors.isEmpty()) {
            return;
        }

        String exceptionMessage = String.join(errors, '\n');

        throw new DomainException(exceptionMessage);
    }

    /**
     * @description Creates a new instance of the SchoolDocumentAssignments domain class.
     * @param schoolDocAssignments The records to create the domain class for.
     * @return An instance of SchoolDocumentAssignments.
     * @throws ArgumentNullException if schoolDocAssignments is null.
     */
    public static SchoolDocumentAssignments newInstance(List<School_Document_Assignment__c>  schoolDocAssignments) {
        ArgumentNullException.throwIfNull(schoolDocAssignments, SCHOOL_DOC_ASSIGNMENTS_PARAM);
        return new SchoolDocumentAssignments(schoolDocAssignments);
    }

    /**
     * @description Used to wrap a single record.
     */
    private class Wrapper {

        private School_Document_Assignment__c record;

        /**
         * @description Constructor for an SDA wrapper that will handle setting the Verify Data field.
         * @param schoolDocAssignment The record to wrap.
         * @throws ArgumentNullException if schoolDocAssignment is null.
         */
        public Wrapper(School_Document_Assignment__c schoolDocAssignment) {
            ArgumentNullException.throwIfNull(schoolDocAssignment, SCHOOL_DOC_ASSIGNMENT_PARAM);
            this.record = schoolDocAssignment;
        }

        /**
         * @description Sets the verify data field based on the specified verification mode. If the specified
         *              verification mode is blank, no changes will be made. Otherwise, the Verify Data field is updated
         *              based on the SDA records document year and academic year values.
         *                  If no verification is required, verify data is set to false.
         *                  For Prior Year verification, verify data will be true if the beginning of the academic year is 2 years before the document year.
         *                  For Current Year verification, verify data will be true if the beginning of the academic year is 1 years before the document year.
         *                  For Current And Prior Year verification, verify data will be true if the beginning of the academic year is 1 or 2 years before the document year.
         * @param verificationMode The verification mode corresponding to an Annual_Setting__c.Document_Verification__c value.
         * @see AnnualSettings.cls
         */
        public void setVerifyData(String verificationMode) {
            if (String.isBlank(verificationMode)) {
                return;
            }

            if (verificationMode.equals(AnnualSettings.NO_VERIFICATION)) {
                this.record.Verify_Data__c = false;
            } else if (verificationMode.equals(AnnualSettings.PY_VERIFICATION)) {
                this.record.Verify_Data__c = isPrior();
            } else if (verificationMode.equals(AnnualSettings.CY_VERIFICATION)) {
                this.record.Verify_Data__c = isCurrent();
            } else if (verificationMode.equals(AnnualSettings.CYANDPY_VERIFICATION)) {
                this.record.Verify_Data__c = isCurrent() || isPrior();
            }
        }

        private Boolean isCurrent() {
            return getAcademicYearStart() - getDocumentYear() == 1;
        }

        private Boolean isPrior() {
            return getAcademicYearStart() - getDocumentYear() == 2;
        }

        private Integer getDocumentYear() {
            return Integer.valueOf(this.record.Document_Year__c);
        }

        private Integer getAcademicYearStart() {
            String academicYearStart = this.record.Academic_Year__c.left(4);

            return Integer.valueOf(academicYearStart);
        }
    }
    
    private void validateFields() {
        for (School_Document_Assignment__c sda : (List<School_Document_Assignment__c>)this.Records) {
            if(sda.School_PFS_Assignment__c == null) {
                sda.School_PFS_Assignment__c.addError('A school PFS Assignment is required.');
            }
        }
    }
}