/**
 * BatchSendEmailsViaProviderTest.cls
 *
 * @description: Test class for BatchSendEmailsViaProvider using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
private class BatchSendEmailsViaProviderTest {
    
    /* test data setup */
    @testSetup 
    static void setupTestData() {
        MassEmailSendTestDataFactory.createEmailTemplate();
        
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
        }
    }

    /* postive test cases */

    // NOTE: (as of 7/2016)
    // Due to a bug introduced in Spring '15, Test.start/stopTest() commits are not getting cleared when a future method with the callout annotation
    // is called, meaning any DML before future callout is treated as one transaction, resulting in "You have uncommitted work pending" exception
    // see links: 
    // http://www.joe-ferraro.com/2014/04/apex-unit-testing-test-data-and-test-setmock/
    // https://patlatus.wordpress.com/2015/02/12/bugs-in-salesforce-spring-15-causing-system-calloutexception-you-have-uncommitted-work-pending-please-commit-or-rollback-before-calling-out/
    // https://success.salesforce.com/issues_view?id=a1p300000008XHBAA2
    // Because a Mass_Email_Send__c record must exist before we can make callout, there is no way to trigger this and
    // not receive an exception - exception is caught and gracefully handled, these calls are still necessary for test coverage.

    // Trigger batch execution with update of MES record to valid status
    @isTest 
    static void execute_validRecord_sendExecuted() {
        
        // Arrange
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {

            // Create Academic Year, Applicant, PFS && sPFS Assignment
            MassEmailSendTestDataFactory.createRequiredPFSData();
            Mass_Email_Send__c massES = [select Status__c from Mass_Email_Send__c where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];

            // Act
            Test.startTest();

                Test.setMock( HttpCalloutMock.class, new SendgridMockHttpResponseGenerator());
                massES.Status__c = 'Ready to Send';
                update massES;
            Test.stopTest();
        }

        // Assert
        // Can't assert
    }
    
}