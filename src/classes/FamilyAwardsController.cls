public class FamilyAwardsController
{
    // TODO SFP-1750 We should make this with sharing.
    // Issue was not resolved.
    // Now if we make Award and Awards controllers *with sharing* we are not getting any results in loadPFS method
    // A Family user then doesn't see any student folders, pfss, spas and so on.
    // Maybe some specific security setup for user profile is required

    public ApplicationUtils appUtils {get; set;}
    public FamilyTemplateController controller {get; set;}
    
    private Map<Id, Student_Folder__c> foldersById;
    
    public FamilyAwardsController(FamilyTemplateController thisController)
    {
        // TODO SFP-1750 What happens on this page when the user selects a different academic year?
         /*If a user has PFS for this selected year - awards for this selected year will be shown
         (because we use pfs Id in our query in the getApplicantsWithAwards method and select awards using it,
         so different academic year -> different pfs -> different awards*/

        controller = thisController;
        
        if(controller.pfsRecord == null)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error: No PFS Record Found for current user.'));
            controller.pfsRecord = new PFS__c();
        }
        appUtils = new ApplicationUtils(UserInfo.getLanguage(), this.controller.pfsRecord);

    }
    
    public List<FamilyAwardsService.ApplicantsWithAwardsWrapper> applicantsInfo
    {
        get
        {
            // TODO+ SFP-1750 pass the request into the service instead of running the logic in the response constructor.
            //Done

            applicantsInfo = FamilyAwardsService.Instance.getApplicantsWithAwards(controller.pfsRecord.Id, null);
            foldersById = FamilyAwardsService.Instance.foldersMap;

            return applicantsInfo;
        }
        private set;
    }
    
    /**
    * @description Method invoked after the user clicks "Accept" button.
    */
    public PageReference acceptAction()
    {
        String folderId = ApexPages.currentPage().getParameters().get('folderId');
        Student_Folder__c folder = foldersById.get(folderId);
        FamilyAwardsService.Instance.acceptAward(folder);

        return null;

    }
    
    /**
    * @description Method invoked after the user clicks "Deny" button.
    */
    public PageReference denyAction()
    {
        String folderId = ApexPages.currentPage().getParameters().get('folderId');
        Student_Folder__c folder = foldersById.get(folderId);
        FamilyAwardsService.Instance.denyAward(folder);

        return null;

    }
}