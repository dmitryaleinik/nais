/*
 * Controller for SchoolSubtabs component referenced in School Setup pages
 *
 *    NAIS-2270 Add logic to control visibility of subscription renewal tab
 *
 */
public with sharing class SchoolSubtabsController {
    
    public String selectedValue {get; set;} 
    public SchoolSubtabsController(){}
    public SchoolAcademicYearSelectorInterface Consumer { get; set; }
    
    //CSG: Added on 4-13-15 for Renewal tab
    private Account school {
        private get{
            if(school == null) {
                school = new Account();
                Account[] schoolList = [
                    SELECT Paid_Through__c 
                    FROM Account 
                    WHERE Id = :GlobalVariables.getCurrentSchoolId()
                ];
                if(schoolList.size() > 0) {
                    school = schoolList[0];
                }
            }
            return school;
        }private set;
    }
    public Boolean getIsEligibleForRenew() {
        if(school.Paid_Through__c == null) { 
            return true; 
        }else if(school.Paid_Through__c.year() < System.today().year()) { 
            return true; 
        }else if(school.Paid_Through__c.year() == System.today().year() && GlobalVariables.isRenewalSeason()) {
            return true;
        }
        return false;
    }
    public Boolean getSubscriptionExpired() {
        if(school.Paid_Through__c == null || (school.Paid_Through__c != null && school.Paid_Through__c < System.today())) {
            return true;
        }
        return false;
    }
    //END CSG Add

    public Boolean getMassEmailEnabled() {

        // Handle app enabled/disabled
        return MassEmailUtil.isMassEmailGloballyEnabledByAccount( 
                    GlobalVariables.getCurrentSchoolId(), UserInfo.getProfileId());
    }
}