/**
 * EmailService.cls
 *
 * @description: Provider agnostic service for creating email sends for providers to consume
 *
 * @author: Chase Logan @ Presence PG
 */
public class EmailService {
    
    /* resuable constants */
    public static final String EMAIL_NEW_STATUS = 'New';
    public static final String EMAIL_DRAFT_STATUS = 'Draft';
    public static final String EMAIL_SEND_STATUS = 'Ready to Send';
    public static final String EMAIL_DELIVERED_STATUS = 'Delivered';
    public static final String EMAIL_ERR_STATUS = 'Error';
    public static final String EMAIL_INVALID_ERR_MSG = 'Recipients, Reply to Address, Subject, and Text or HTML body are required to send email.';
    public static final String EMAIL_FATAL_ERR_MSG = 'Unrecoverable error attempting to send email, message not sent, please check data and try again.';

    /* custom exception for throws */
    public class SendException extends Exception {}

    /* Implementation of lazy-loaded Singleton pattern */
    private static EmailService instance = null;

    // private constructor means getInstance() is the only way to get an instance of EmailService
    private EmailService() {}
    
    // returns a static instance of EmailService, prevents multiple instantiations of the same object in a single transaction
    public static EmailService getInstance() { 
        if (instance == null) {
            instance = new EmailService();
        }
        return instance;
    }

    /* public methods */

    /**
     * @description: Send email associated with a single Mass_Email_Send__c record via email provider
     *
     * @param massEmailSendId - The ID of the Mass_Email_Send__c record
     */
    public void sendEmails( Id massEmailSendId) {

        if ( massEmailSendId == null) return;

        // pass off to overloaded list version
        List<Id> massEmailSendIdList = new List<Id>();
        massEmailSendIdList.add( massEmailSendId);
        this.sendEmails( massEmailSendIdList);
    }

    /**
     * @description: Send email associated with muliple Mass_Email_Send__c record(s) via email provider
     *
     * @param massEmailSendIdList - The List of ID's of the Mass_Email_Send__c record(s)
     */
    public void sendEmails( List<Id> massEmailSendIdList) {

        if ( massEmailSendIdList == null) return;

        for ( Id i : massEmailSendIdList) {

            this.sendEmailsViaProvider( i);
        }
    }

    /**
     * @description: Return the active email provider instance
     *
     * @return A valid IEmailProvider instance
     */
    public IEmailProvider getProvider() {

        // this is a simple implementation, could determine which provider is active
        // in a number of ways (custom metadata, etc) and return active provider
        Type t = Type.forName( new SendgridEmailProvider().getType());
        return ( SendgridEmailProvider)t.newInstance();
    }

    /**
     * @description: Responsible for updating Mass_Email_Send__c records with status after send
     *
     * @param validIdSet - The Set of ID's of the Mass_Email_Send__c records that were successfully sent
     *
     * @param failedIdMap - the Map of ID => List recipient Id of failed individual sends
     */
    public void updateRecords( Set<Id> validIdSet, Map<Id,List<String>> failedIdMap) {

        if ( validIdSet == null || failedIdMap == null) return;

        final String ALL_RECIPS_FAILED_ERR = 'All recipients failed to send, see Failed_to_Send__c field for recipients list.';
        List<Mass_Email_Send__c> massEmailSendList = new List<Mass_Email_Send__c>();
        for ( Id i : validIdSet) {

            // shallow clone saves a query statement and references original object
            Mass_Email_Send__c massES = new Mass_Email_Send__c( Id = i).clone( true, false, false, false);
            massES.Date_Sent__c = Datetime.now();
            massES.Status__c = EmailService.EMAIL_DELIVERED_STATUS;
            massEmailSendList.add( massES);
        }

        for ( Id i : failedIdMap.keySet()) {

            // shallow clone saves a query statement and references original object
            Mass_Email_Send__c massES = new Mass_Email_Send__c( Id = i).clone( true, false, false, false);

            // if there have been successful records on this send, do not change massES record status or error message,
            // instead store individual failed recipient id's on Failed_to_Send__c for later reporting
            if ( validIdSet != null && validIdSet.size() > 0) {

                massES.Failed_to_Send__c = String.join( failedIdMap.get( i), ',');
            } else {

                massES.Status__c = EmailService.EMAIL_ERR_STATUS;
                massES.Error_Message__c = ALL_RECIPS_FAILED_ERR;
                massES.Failed_to_Send__c = String.join( failedIdMap.get( i), ',');
            }
            
            massEmailSendList.add( massES);
        }

        // upsert records, bubble any exception to caller
        try {

            upsert massEmailSendList;    
        } catch ( Exception e) {

            // log it, bubble to caller
            System.debug( 'DEBUG:::exception in EmailService.updateRecords' + e.getMessage());
            throw new SendException( e);
        }
    }
    
    /**
     * @description: @future asynchronous method for making the callout, used for test email sends
     *
     * @param massEmailSendId - The Id of the Mass_Email_Send__c record - required
     *
     * @param toAddress - The email address the test send should be delivered to - required
     */
    @future(callout=true) 
    public static void sendTestEmailAsync( Id massEmailSendId, String toAddress) {

        try {

            // params required, early return on empty
            if ( String.isBlank( massEmailSendId) || String.isBlank( toAddress)) return;

            // query the MES record
            Mass_Email_Send__c massES = new MassEmailSendDataAccessService().getMassEmailSendById( massEmailSendId);
            // get first SPA Id off massES record and query, is used for merge field preview during test send
            List<String> spaIdList = 
                new List<String>( massES.School_PFS_Assignments__c.split( SchoolMassEmailSendModel.SPLIT_CHAR));
            School_PFS_Assignment__c firstSPA = 
                new SchoolPFSAssignmentDataAccessService().getSchoolPFSAssignmentById( ( Id)spaIdList[0]);

            Email singleMessage = new Email();
            singleMessage.toAddresses.add( toAddress);
            singleMessage.fromAddresses.add( massES.Reply_To_Address__c);
            
            // replace any merge fields at this point, verification happens prior
            Map<String,String> mergeDataMap = MassEmailUtil.createMergeMap( firstSPA);
            String htmlEmailBody = MassEmailUtil.getEmailTemplateBody(massES);
            String msgBody = MassEmailUtil.replaceMergeFields(htmlEmailBody, mergeDataMap);
            singleMessage.messageBody = msgBody;
            singleMessage.messageSubject = massES.Subject__c;

            // send test message via active provider
            SendgridEmailProvider sgProvider = ( SendgridEmailProvider)EmailService.getInstance().getProvider();
            List<Email> emailList = new List<Email>();
            emailList.add( singleMessage);
            sgProvider.sendEmails( emailList);
        } catch ( Exception e) {
            
            // log it, eat it, future context means caller has lost scope
            System.debug( 'DEBUG:::exception in EmailService.sendTestEmailAsync' + e.getMessage());
        }
    }
    
    /* private methods */

    /**
     * @description: @future asynchronous method for making the callout
     *
     * @param massEmailSendId - The ID of the Mass_Email_Send__c record
     */
    private void sendEmailsViaProvider( Id massEmailSendId) {

        if ( massEmailSendId == null) return;

        BatchSendEmailsViaProvider.startBatch(massEmailSendId);
    }

}