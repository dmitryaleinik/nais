/**
 * @description Domain class for the IntegrationLog__c object. Contains enums for status values.
 */
public class IntegrationLogs {

    /**
     * @description Used to specify the status of different integration logs.
     */
    public enum Status {
        FATAL,
        ERROR,
        SUCCESS,
        WARN,
        INFO
    }
}