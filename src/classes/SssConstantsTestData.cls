/**
 * @description This class is used to create Sss Constants records for unit tests.
 */
@isTest
public class SssConstantsTestData extends SObjectTestData {
    @testVisible private static final Decimal EXEMPTION_ALLOWANCE = 20;
    @testVisible private static final Decimal NEGATIVE_CONTRIBUTION_CAP_CONSTANT = 25;
    @testVisible private static final Decimal BOARDING_SCHOOL_FOOD_ALLOWANCE = 30;
    @testVisible private static final Decimal MEDICAL_DENTAL_ALLOWANCE_PERCENT = 1.5;
    @testVisible private static final Decimal PERCENTAGE_FOR_IMPUTING_ASSETS = 35.5;
    @testVisible private static final Decimal DEFAULT_COLA_VALUE = 2.5;
    @testVisible private static final Decimal EMPLOYMENT_ALLOWANCE_MAXIMUM = 40;
    @testVisible private static final Decimal IPA_HOUSING_FOR_EACH_ADDITIONAL = 45;
    @testVisible private static final Decimal IPA_OTHER_FOR_EACH_ADDITIONAL = 50;
    @testVisible private static final Decimal STD_DEDUCTION_SINGLE_FILING_SEPARATELY = 55;
    @testVisible private static final Decimal STD_DEDUCTION_JOINT_SURVIVING = 60;
    @testVisible private static final Decimal STD_DEDUCTION_HEAD_OF_HOUSEHOLD = 65;
    @testVisible private static final Decimal MEDICARE_TAX_RATE = 3.5;
    @testVisible private static final Decimal IPA_HOUSING_MULTIPLIER_FOR_ADDITIONAL = 4.5;

    /**
     * @description Get the default values for the SSS_Constants__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                SSS_Constants__c.Academic_Year__c => AcademicYearTestData.Instance.DefaultAcademicYear.Id,
                SSS_Constants__c.Exemption_Allowance__c => EXEMPTION_ALLOWANCE,
                SSS_Constants__c.Negative_Contribution_Cap_Constant__c => NEGATIVE_CONTRIBUTION_CAP_CONSTANT,
                SSS_Constants__c.Boarding_School_Food_Allowance__c => BOARDING_SCHOOL_FOOD_ALLOWANCE,
                SSS_Constants__c.Medical_Dental_Allowance_Percent__c => MEDICAL_DENTAL_ALLOWANCE_PERCENT,
                SSS_Constants__c.Percentage_for_Imputing_Assets__c => PERCENTAGE_FOR_IMPUTING_ASSETS,
                SSS_Constants__c.Default_COLA_Value__c => DEFAULT_COLA_VALUE,
                SSS_Constants__c.Employment_Allowance_Maximum__c => EMPLOYMENT_ALLOWANCE_MAXIMUM,
                SSS_Constants__c.IPA_Housing_For_Each_Additional__c => IPA_HOUSING_FOR_EACH_ADDITIONAL,
                SSS_Constants__c.IPA_Other_For_Each_Additional__c => IPA_OTHER_FOR_EACH_ADDITIONAL,
                SSS_Constants__c.Std_Deduction_Single_Filing_Separately__c => STD_DEDUCTION_SINGLE_FILING_SEPARATELY,
                SSS_Constants__c.Std_Deduction_Joint_Surviving__c => STD_DEDUCTION_JOINT_SURVIVING,
                SSS_Constants__c.Std_Deduction_Head_of_Household__c => STD_DEDUCTION_HEAD_OF_HOUSEHOLD,
                SSS_Constants__c.Medicare_Tax_Rate__c => MEDICARE_TAX_RATE,
                SSS_Constants__c.IPA_Housing_Multiplier_for_Additional__c => IPA_HOUSING_MULTIPLIER_FOR_ADDITIONAL
        };
    }

    /**
     * @description Set the Academic Year on the current SSS Constants record.
     * @param academicYearId The Id of the Academic Year to set on the current
     *             SSS Constants record.
     * @return The current working instance of SssConstantsTestData.
     */
    public SssConstantsTestData forAcademicYearId(Id academicYearId) {
        return (SssConstantsTestData) with(SSS_Constants__c.Academic_Year__c, academicYearId);
    }

    /**
     * @description Insert the current working SSS_Constants__c record.
     * @return The currently operated upon SSS_Constants__c record.
     */
    public SSS_Constants__c insertSssConstants() {
        return (SSS_Constants__c)insertRecord();
    }

    /**
     * @description Create the current working Sss Constants record without resetting
     *             the stored values in this instance of SssConstantsTestData.
     * @return A non-inserted SSS_Constants__c record using the currently stored field
     *             values.
     */
    public SSS_Constants__c createSssConstantsWithoutReset() {
        return (SSS_Constants__c)buildWithoutReset();
    }

    /**
     * @description Create the current working SSS_Constants__c record.
     * @return The currently operated upon SSS_Constants__c record.
     */
    public SSS_Constants__c create() {
        return (SSS_Constants__c)super.buildWithReset();
    }

    /**
     * @description The default SSS_Constants__c record.
     */
    public SSS_Constants__c DefaultSssConstants {
        get {
            if (DefaultSssConstants == null) {
                DefaultSssConstants = createSssConstantsWithoutReset();
                insert DefaultSssConstants;
            }
            return DefaultSssConstants;
        }
        private set;
    }

    /**
     * @description Get the SSS_Constants__c SObjectType.
     * @return The SSS_Constants__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return SSS_Constants__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static SssConstantsTestData Instance {
        get {
            if (Instance == null) {
                Instance = new SssConstantsTestData();
            }
            return Instance;
        }
        private set;
    }

    private SssConstantsTestData() { }
}