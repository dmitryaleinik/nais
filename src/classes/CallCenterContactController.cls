//fill out case on edit
// 2 fill out contact recordtype
//work fone column header-  exp sandbox
public with sharing class CallCenterContactController
{

    public String ani    {get;set;}
    public String dnis    {get;set;}
    public String callID {get;set;}
    public String username {get;set;}
    public String searchTerm {get;set;}
    public boolean initialMatchFound {get;set;}
    public boolean renderSearchResults {get;set;}

    /*move this to recordtype class when it gets checked into GIT for now leave it here*/

    public List <Contact> contacts {get;set;}
    public String contactUrl {get;set;}
    public String newCaseURL {get;set;}
    public String url {get;set;} // popup url
    
    private ID contactRecordTypeID = RecordTypes.parentContactTypeId;

    public CallCenterContactController() {
        initialMatchFound = false;
    }
    
    public void doSearch()
    {
        if (this.searchTerm!=null && searchTerm.length()>0) {
            
            List<List<SObject>> searchList = [FIND :searchTerm IN ALL FIELDS RETURNING Contact ( Account.name, id, name,HomePhone, MobilePhone, Phone,MailingCity, MailingCountry, email,MailingState, MailingPostalCode WHERE recordtypeid = :contactRecordTypeID limit 21)  ];
            this.contacts = (List<Contact>)searchList[0];
            if (contacts.size()>20) {
                
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.info,'More than 20 results found. Try searching again with more specific criteria.');
                ApexPages.addMessage(myMsg);
            }
            else if    (contacts.size() == 0 ) {
                contacts = null;
            }
            renderSearchResults = true;
        }
    }

    private String getCallCenterParam(String nameOfParameter) {
        // Check for CTI parameter in url.
        String ctiParameter = ApexPages.currentPage().getParameters().get(nameOfParameter);

        // If CTI parameter was null or blank, check cookies.
        if (String.isBlank(ctiParameter)) {
            ctiParameter = getCookieValue(nameOfParameter);
        }

        return ctiParameter;
    }

    private String getCookieValue(String cookieName) {
        Cookie targetCookie = ApexPages.currentPage().getCookies().get(cookieName);

        return targetCookie == null ? null : targetCookie.getValue();
    }

    public PageReference findContacts() {

        // Get call center params by first checking for url params and falling back on cookie values.
        this.ani = getCallCenterParam('ani');
        this.dnis = getCallCenterParam('dnis');
        this.callID = getCallCenterParam('callID');
        this.username = getCallCenterParam('username');
 
        List<Contact> theContacts =null;
        if (ani != null && ani!='') {
            
            if (PhoneHelper.isValidPhone(ani)) { //if the phone is something we understand
                String sfFormatted = PhoneHelper.getSFPhone(ani);
                String sfSearchable = PhoneHelper.getSFSearchablePhone(ani);
                
                system.debug('ani '+ ani +' sfFormatted ' +sfFormatted+ ' '+ ' searchable ' + sfSearchable );
                theContacts = [select id, name,HomePhone, MobilePhone, Phone,MailingCity, MailingCountry, email,MailingState, MailingPostalCode from contact where 
                    recordtypeid = :contactRecordTypeID and (
                    HomePhone=:ani or MobilePhone=:ani or Phone=:ani or 
                    homePhone like :sfSearchable or MobilePhone like :sfSearchable or Phone like:sfSearchable
                )];
                system.debug(theContacts.size() );
                
            
            } else {
        
                theContacts = [select id, name,HomePhone, MobilePhone, Phone,MailingCity, MailingCountry, email,MailingState from contact where 
                recordtypeid = :contactRecordTypeID and (
                HomePhone=:ani or MobilePhone=:ani or Phone=:ani
                )];
            }
            
        }
        else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.info,'Phone Number Unavailable');
            ApexPages.addMessage(myMsg);
        }
        this.contactUrl = '/003/e?RecordType='+this.contactRecordTypeID;
        if (ani!=null && ani.length()>0) {
            contactUrl+= '&con13='+ani;    
        }
        // NAIS-1392 set retURL
        contactUrl += '&retURL=' + EncodingUtil.urlEncode(ApexPages.currentPage().getUrl(), 'UTF-8');

        this.newCaseURL = CallCenterContactRedirectController.getCaseURL(CallCenterContactRedirectController.parentSupportCaseTypeId, null, null);
        // NAIS-1392 set retURL
        this.newCaseURL += '&retURL=' + EncodingUtil.urlEncode(ApexPages.currentPage().getUrl(), 'UTF-8');

        
        if ( theContacts==null || theContacts.size() == 0 ) {
            renderSearchResults = false;
            
            //this.caseUrl=getCaseUrl(this.ani,this.dnis);
            if (ani!=null && ani.length()>0) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.info,'No Contact Found for ' + PhoneHelper.getSFPhone(ani));
                ApexPages.addMessage(myMsg);
            }
        
            return null;
        }
        else {
            renderSearchResults = true;
            initialMatchFound = true;
        }
    
        this.contacts = theContacts;
        
        if (theContacts.size()==1) {
            
            this.url = '/' + theContacts[0].id;
            
            return null;
        }
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.info,'Multiple Contacts Found for ' + PhoneHelper.getSFPhone(ani));
        ApexPages.addMessage(myMsg);
        return null;
    }
}