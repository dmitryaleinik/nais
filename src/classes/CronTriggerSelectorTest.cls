@isTest
private class CronTriggerSelectorTest {

    @isTest static void testSelectById() {
        Test.startTest();
            try {
                CronTriggerSelector.Instance.selectById(null);
                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, CronTriggerSelector.CRON_TRIGGER_IDS_PARAM);
            }
        Test.stopTest();
    }

    @isTest static void testSelectByCronJobDetailName() {
        Test.startTest();
            try {
                CronTriggerSelector.Instance.selectByCronJobDetailName(null);
                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, CronTriggerSelector.CRON_JOB_DETAIL_NAMES_PARAM);
            }
        Test.stopTest();
    }
}