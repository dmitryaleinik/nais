/**
 * @description Service to be invoked to process CaseArticles records.
 */
public without sharing class CaseDispositionService {

    public final static Set<String> EXEMPTED_TYPES = new Set<String>{
            'Misdirected Contact'
    };

    final static Set<String> CASE_TYPES = new Set<String>{
            RecordTypes.parentCallCaseTypeId,
            RecordTypes.familyEmailCaseTypeId,
            RecordTypes.familyLiveAgentCaseTypeId,
            RecordTypes.familyPortalCaseTypeId
    };

    @testVisible
    private CaseDispositionService() {
    }
    
    /**
    * @description Takes a list of Cases, and determines which ones match with the following record types: 
    *              - Parent Call
    *              - Email - Family
    *              - Parent Live Chat
    *              - Family Portal Case for NAIS
    *              If the selected case has a related CaseArticle. Then, we search an Article Disposition Mapping 
    *              for that CaseArticle, based in the CaseArticle.KnowledgeArticle.ArticleNumber. If we find a 
    *              Article Disposition Mapping record, we update the case with the type, disposition, and sub-category.
    * @param cases The list of record to be processed.
    * @return A list of cases, ready to be updated.
    */
    public List<Case> setDispositionBasedOnArticles(List<Case> cases) {

        CaseArticle caseArticleTmp;
        Article_Disposition_Mapping__c articleDispositionTmp;
        List<Case> toUpdate = new List<Case>();
        Map<Id, Case> mapCases = new Map<Id, Case>(cases);
        String articleNumber;

        // Get the map of Article_Disposition_Mapping__c
        Map<String, Article_Disposition_Mapping__c> articleDispositionMap =
                ArticleDispositionMappingSelector.Instance.getMapByArticleNumber();

        // Get the related CaseArticle records and map those records by CaseId.
        Map<Id, CaseArticle> articlesByCaseId = mapLatestCaseArticlesByCaseId(mapCases.keySet(), articleDispositionMap.keySet());

        // For each case, determine if it requires a knowledge article to be closed. A case requires a knowledge article if:
        // - record type is family call, family portal case, family email case, or family live agent case
        // - the Case.Type is NOT Misdirected Contact.
        // Once we know if it requires a knowledge article to be closed, we will get the latest knowledge article then the
        // corresponding Article Disposition custom metadata type by article number.
        // We then use the metadata type to populate the case type, disposition, and sub category.
        // If there is no custom metadata type, we will leave the fields as is.
        // If the case requires a knowledge article but there aren't any for that case, we add an error to the case to prevent the case from being updated by the triggers.
        for (Case currentCase : cases) {
            if (requiresKnowledgeArticleToClose(currentCase)) {

                caseArticleTmp = articlesByCaseId.get(currentCase.Id);

                // If the case does not have a case article, add an error so TCN agents will know to add one.
                if (caseArticleTmp == null) {
                    currentCase.addError(Label.Case_Requires_Knowledge_Article);
                } else {
                    articleNumber = caseArticleTmp != null ? caseArticleTmp.KnowledgeArticle.ArticleNumber : null;
                    articleDispositionTmp = articleDispositionMap.get(articleNumber);

                    if (articleDispositionTmp != null) {
                        
                        if (!String.isBlank(articleDispositionTmp.Type__c)) {
                            currentCase.Type = articleDispositionTmp.Type__c;
                        }
                        
                        if (!String.isBlank(articleDispositionTmp.Disposition__c)) {
                            currentCase.Disposition__c = articleDispositionTmp.Disposition__c;
                        }
                        
                        if (!String.isBlank(articleDispositionTmp.Sub_category__c)) {
                            currentCase.Sub_category__c = articleDispositionTmp.Sub_category__c;
                        }
                    }
                }
            }

            toUpdate.add(currentCase);
        }

        return toUpdate;

    }

    /**
    * @description Creates a map of CaseArticle records, related to a list of Case Ids. 
    *              The key of the map is the CaseArticle.KnowledgeArticle.ArticleNumber.
    * @param caseIds The set of case ids for which we will query de CaseArticle.
    * @param articleNumbers The set of valid Knowledge Article numbers.
    * @return The map of CaseArticle records.
    */
    private Map<Id, CaseArticle> mapLatestCaseArticlesByCaseId(Set<Id> caseIds, Set<String> articleNumbers) {

        Map<Id, CaseArticle> result = new Map<Id, CaseArticle>();

        String queryStr = 'SELECT Id, KnowledgeArticleId, CaseId, KnowledgeArticle.ArticleNumber ' +
                'FROM CaseArticle WHERE CaseId IN : caseIds ' +
                ' AND KnowledgeArticle.ArticleNumber =: articleNumbers ' +
                'ORDER BY CreatedDate DESC';

        for (CaseArticle article : Database.query(queryStr)) {
            // Check the map to see if we already have an article for this case. The first article will be the one that
            // was last added to the Case so we should only take the first one for each case.
            if (result.get(article.CaseId) == null) {
                result.put(article.CaseId, article);
            }
        }

        return result;
    }
    
    /**
    * @description Returns true if a given Case requires a knowledge article to be closed. A case requires a knowledge article if the Case.Type is NOT Misdirected Contact and the recordType, is:
    *              - Parent Call
    *              - Email - Family
    *              - Parent Live Chat
    *              - Family Portal Case for NAIS
    * @param c the Case record to check.
    * @return True, if the given case's recordTypeId and Type require a knowledge article to be closed. Otherwise, false.
    */
    private Boolean requiresKnowledgeArticleToClose(Case caseToCheck) {

        return CASE_TYPES.contains(caseToCheck.recordTypeId) && !EXEMPTED_TYPES.contains(caseToCheck.Type);
    }

    /**
     * @description Singleton instance of the CaseDispositionService.
     */
    public static CaseDispositionService Instance {
        get {
            if (Instance == null) {
                Instance = new CaseDispositionService();
            }
            return Instance;
        }
        private set;
    }
}