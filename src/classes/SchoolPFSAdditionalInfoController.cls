/*
* SPEC-121
*
* The school should be able to see all answers provided by the families on the main application screens
*
* Nathan, Exponent Partners, 2013
*/
public with sharing class SchoolPFSAdditionalInfoController implements SchoolAcademicYearSelectorStudInterface {

    // input
    String schoolPFSAssignmentId;

    /*Properties*/

    // to pass to component academic year selector
    public Student_Folder__c folder {get; set;}
    public SchoolPFSAdditionalInfoController thisController {get {return this;}}

    // output
    public School_PFS_Assignment__c schoolPFSAssignment {get; private set;}
    public PFS__c pfs {get; private set;}

    // NAIS-2498 START
    public List<Business_Farm__c> bfList {get; private set;}
//  public String businessAddress {get; private set;}
    public Map<Id, String> businessAddressByBFId {get; private set;}
    // NAIS-2498 END

    public String errorMessage {get; private set;}    // to verify in test classes

    // [CH] NAIS-756
    public Boolean isEditableYear {
        get{
            return GlobalVariables.isEditableYear(schoolPFSAssignment.Academic_Year_Picklist__c);
        }
        set;
    }

    // wrapper class for other real estate
    public class OtherRealEstate {
        public String name {get; private set;}
        public String typeStr {get; private set;}    // type is future reserved word. making it as typeStr
        public PFS__c pfs {get; private set;}
        public OtherRealEstate() {
          pfs = new PFS__c();
        }
        public OtherRealEstate(String name, String typeStr, PFS__c pfs) {
            this.name = name;
            this.typeStr = typeStr;
            this.pfs = pfs;
        }
    }
    public List <OtherRealEstate> otherRealEstateList {get; private set;}

    // wrapper class for vehicle
    public class Vehicle {
        public String name {get; private set;}
        public String typeStr {get; private set;}    // type is future reserved word. making it as typeStr
        public PFS__c pfs {get; private set;}
        public Boolean existData {get; private set;}
        public Vehicle() {
          pfs = new PFS__c();
          existData = false;
        }
    }
    public List <Vehicle> vehicleList {get; private set;}
    public List <Vehicle> vehicleListDisplay {get; private set;}

    // wrapper class for applicant
    public class Applicant {
        public Applicant__c applicant {get; private set;}
        public School_PFS_Assignment__c schoolPFSAssignment {get; private set;}
        public Decimal expense {get; private set;}
        public Decimal income {get; private set;}
        public Applicant() {
            applicant = new Applicant__c();
            schoolPFSAssignment = new School_PFS_Assignment__c();
            expense = 0;
            income = 0;
        }
    }
    public List <Applicant> applicantList {get; private set;}
    public List <Applicant__c> otherSourceList {get; private set;}
    public List <Applicant> otherSourceListApplicant {get; private set;}

    // wrapper class for dependent
    public class Dependent {
        public Dependents__c dependents {get; private set;}
        public String name {get; private set;}
        public Decimal expense {get; private set;}
        public Decimal income {get; private set;}
        public String otherSourceDesc {get; private set;}

        public Dependent() {
            dependents = new Dependents__c();
            expense = 0;
            income = 0;
        }
    }
    public List <Dependent> dependentList {get; private set;}
    public List <Dependent> otherSourceListDependent {get; private set;}
    static final Integer dependentsPerRecord = 6;

    // wrapper class for educational expenses and income
    public class FinancialItem {
        public String typeStr {get; private set;}    // type is future reserved word. making it as typeStr
        public List <Applicant> applicantList {get; private set;}
        public List <Dependent> dependentList {get; private set;}

        public FinancialItem() {
            applicantList = new List <Applicant>();
            dependentList = new List <Dependent>();
        }
    }
    public List <FinancialItem> educationalExpenseList {get; private set;}
    public List <FinancialItem> applicantIncomeList {get; private set;}

    // SFP-605
    public Boolean displayAdditionalQuestions { get {
        return AnnualSettingHelper.getCustomQuestionsEnabledForSchoolIds(new Set<String> { schoolPFSAssignment.School__c },
                    GlobalVariables.getAcademicYearByName(schoolPFSAssignment.Academic_Year_Picklist__c).Id);
    }}

    /*End Properties*/

    /*Initialization*/

    List <String> expenseTypeList = new List <String>();
    List <String> incomeTypeList = new List <String>();
    Map <String, FinancialItem> type_EduExp_map = new Map <String, FinancialItem>();
    Map <String, FinancialItem> type_appInc_map = new Map <String, FinancialItem>();

    // constructor
    public SchoolPFSAdditionalInfoController() {

        // init
        schoolPFSAssignment = new School_PFS_Assignment__c();
        pfs = new PFS__c();
        otherRealEstateList = new List <OtherRealEstate>();
        vehicleList = new List <Vehicle>();
        vehicleListDisplay = new List <Vehicle>();
        applicantList = new List <Applicant>();
        dependentList = new List <Dependent>();
        otherSourceList = new List <Applicant__c>();
        otherSourceListApplicant = new List <Applicant>();
        otherSourceListDependent = new List <Dependent>();
        educationalExpenseList = new List <FinancialItem>();
        applicantIncomeList = new List <FinancialItem>();

        schoolPFSAssignmentId = ApexPages.currentPage().getParameters().get('schoolPFSAssignmentId');
        loadSchoolPFSAssignment();
        loadLeftNav();
        loadPFS();
    }

    /*End Initialization*/

    /*Helper Methods*/

    private void loadSchoolPFSAssignment() {

        // init

        if (schoolPFSAssignmentId != null) {
            List <School_PFS_Assignment__c> spfsaList = [select Id, Name,
                Academic_Year_Picklist__c,
                Applicant__r.PFS__c,
                Student_Folder__c,
                School__c
            from School_PFS_Assignment__c
            where Id = :schoolPFSAssignmentId];

            if (spfsaList.size() > 0) {
                schoolPFSAssignment = spfsaList[0];
            }
        }
    }

    private void loadLeftNav() {

        if (schoolPFSAssignment != null && schoolPFSAssignment.Student_Folder__c != null) {
            folder = GlobalVariables.getFolderAndSchoolPFSAssignments(schoolPFSAssignment.Student_Folder__c);
        }
    }

    private void loadPFS() {

        if (schoolPFSAssignment != null && schoolPFSAssignment.Applicant__r.PFS__c != null) {
            List<String> pfsFieldNames = new List<String>(Schema.SObjectType.PFS__c.fields.getMap().keySet());
            Id tempPFSId = schoolPFSAssignment.Applicant__r.PFS__c;

            String pfsQueryString = 'SELECT ' + String.Join(pfsFieldNames, ',') + ', Parent_A__r.MailingState from PFS__c where Id = :tempPFSId';

            System.debug('TESTING172 ' + pfsQueryString);
            List <PFS__c> pfsList = Database.query(pfsQueryString);

            if (pfsList.size() > 0) {
                pfs = pfsList[0];

                loadOtherRealEstate(pfs);
                loadVehicle(pfs);
                loadBoatRV(pfs);
                // NAIS-2498 START
                //loadBusinessAddress(pfs);        replaced by LoadBusinessAddressMap()
                loadBusinessFarmList(pfs.Id);
                LoadBusinessAddressMap();
                // NAIS-2498 END
                loadApplicants();
                loadDependents();
                loadExpenseIncomeList();
            }
        }
    }

    private void loadOtherRealEstate(PFS__c pfs) {

        // init
        otherRealEstateList = new List <OtherRealEstate>();

        otherRealEstateList.add(new OtherRealEstate('Property Address', 'Property Address', pfs));
        otherRealEstateList.add(new OtherRealEstate('Purpose/Use of Property', 'Purpose/Use of Property', pfs));
        otherRealEstateList.add(new OtherRealEstate('Purchase Price', 'Purchase Price', pfs));
        otherRealEstateList.add(new OtherRealEstate('Current Market Value', 'Current Market Value', pfs));
        otherRealEstateList.add(new OtherRealEstate('Unpaid Principal on all Mortgages', 'Unpaid Principal on all Mortgages', pfs));
    }

    private void loadVehicle(PFS__c pfs) {

        // init
        vehicleList = new List <Vehicle>();
        vehicleListDisplay = new List <Vehicle>();

        // load cars
        for (Integer i=1; i<=5; i++) {
            Vehicle veh = new Vehicle();
            veh.name = 'Vehicle ' + i;
            veh.typeStr = 'Vehicle';
            veh.pfs.put('Car_1_Annual_Lease_Cost__c', pfs.get('Car_' + i + '_Annual_Lease_Cost__c'));
            veh.pfs.put('Car_1_Current_Debt__c', pfs.get('Car_' + i + '_Current_Debt__c'));
            veh.pfs.put('Car_1_Make__c', pfs.get('Car_' + i + '_Make__c'));
            veh.pfs.put('Car_1_Model__c', pfs.get('Car_' + i + '_Model__c'));
            veh.pfs.put('Car_1_Notes__c', pfs.get('Car_' + i + '_Notes__c'));
            veh.pfs.put('Car_1_Ownership_Status__c', pfs.get('Car_' + i + '_Ownership_Status__c'));
            veh.pfs.put('Car_1_Year__c', pfs.get('Car_' + i + '_Year__c'));
            veh.pfs.put('Car_1_Year__c', pfs.get('Car_' + i + '_Year__c'));
            veh.pfs.put('Vehicle_1_Type__c', pfs.get('Vehicle_'+i+'_Type__c'));

            vehicleList.add(veh);

            // determine if there is data in vehicle fields
            if (veh.pfs.Car_1_Annual_Lease_Cost__c != null ||
                veh.pfs.Car_1_Current_Debt__c != null ||
                veh.pfs.Car_1_Make__c != null ||
                veh.pfs.Car_1_Model__c != null ||
                veh.pfs.Car_1_Notes__c != null ||
                veh.pfs.Car_1_Ownership_Status__c != null ||
                veh.pfs.Car_1_Year__c != null ||
                veh.pfs.Vehicle_1_Type__c != null) {

                veh.existData = true;
                vehicleListDisplay.add(veh);
            }
        }
    }

    private void loadBoatRV(PFS__c pfs) {

        for (Integer i=1; i<=2; i++) {
            Vehicle veh = new Vehicle();
            veh.name = 'Boat/RV ' + i;
            veh.typeStr = 'Boat/RV';
            /*
            veh.pfs.put('Boat_Recreational_Vehicle_1_Ownership__c', pfs.get('Boat_Recreational_Vehicle_' + i + '_Ownership__c'));
            veh.pfs.put('Boat_Rec_Vehicle_1_Current_Debt__c', pfs.get('Boat_Rec_Vehicle_' + i + '_Current_Debt__c'));
            veh.pfs.put('Boat_Rec_Vehicle_1_Lease_Cost__c', pfs.get('Boat_Rec_Vehicle_' + i + '_Lease_Cost__c'));
            veh.pfs.put('Boat_Rec_Vehicle_1_Make__c', pfs.get('Boat_Rec_Vehicle_' + i + '_Make__c'));
            veh.pfs.put('Boat_Rec_Vehicle_1_Notes__c', pfs.get('Boat_Rec_Vehicle_' + i + '_Notes__c'));
            veh.pfs.put('Boat_Rec_Vehicle_1_Model__c', pfs.get('Boat_Rec_Vehicle_' + i + '_Model__c'));
            veh.pfs.put('Boat_Rec_Vehicle_1_Year__c', pfs.get('Boat_Rec_Vehicle_' + i + '_Year__c'));

            vehicleList.add(veh);

            // determine if there is data in vehicle fields
            if (veh.pfs.Boat_Recreational_Vehicle_1_Ownership__c != null ||
                veh.pfs.Boat_Rec_Vehicle_1_Current_Debt__c != null ||
                veh.pfs.Boat_Rec_Vehicle_2_Lease_Cost__c != null ||
                veh.pfs.Boat_Rec_Vehicle_1_Make__c != null ||
                veh.pfs.Boat_Rec_Vehicle_1_Notes__c != null ||
                veh.pfs.Boat_Rec_Vehicle_1_Model__c != null ||
                veh.pfs.Boat_Rec_Vehicle_1_Year__c != null) {

                veh.existData = true;
                vehicleListDisplay.add(veh);
            }
            */
        }

    }

    // NAIS-2498 START
    private void loadBusinessFarmList(Id pfsId) {
        List<String> bfFieldNames = new List<String>(Schema.SObjectType.Business_Farm__c.fields.getMap().keySet());
        String bfQueryString = 'select ' + String.Join(bfFieldNames, ',') + ' from Business_Farm__c where PFS__c = :pfsId order by Sequence_Number__c asc';
        bfList = Database.query(bfQueryString);
    }

//  private void loadBusinessAddress(PFS__c pfs) {
//      String address = '';
//      if (pfs.Business_Street__c != null) {
//          address += pfs.Business_Street__c;
//      }
//      if (pfs.Business_City__c != null) {
//          address += ', ' + pfs.Business_City__c;
//      }
//      if (pfs.Business_State_Province__c != null) {
//          address += ', ' + pfs.Business_State_Province__c;
//      }
//      if (pfs.Business_Zip_Postal_Code__c != null) {
//          address += ' ' + pfs.Business_Zip_Postal_Code__c;
//      }
//      businessAddress = address;
//  }

    private void LoadBusinessAddressMap() {
        businessAddressByBFId = new Map<Id, String>();
        for (Business_Farm__c bf : bfList) {
            String address = '';
            if (bf.Business_Street__c != null) {
                address += bf.Business_Street__c;
            }
            if (bf.Business_City__c != null) {
                address += (String.isNotBlank(address) ? ', ' : '') + bf.Business_City__c;
            }
            if (bf.Business_State_Province__c != null) {
                address += (String.isNotBlank(address) ? ', ' : '') + bf.Business_State_Province__c;
            }
            if (bf.Business_Zip_Postal_Code__c != null) {
                address += (String.isNotBlank(address) ? ' ' : '') + bf.Business_Zip_Postal_Code__c;
            }
            businessAddressByBFId.put(bf.Id, address);
        }
    }
    // NAIS-2498 END

    private void loadApplicants() {

        // init
        applicantList = new List <Applicant>();
        educationalExpenseList = new List <FinancialItem>();
        applicantIncomeList = new List <FinancialItem>();

        expenseTypeList = new List <String>();
        expenseTypeList.add('Cost of child care, preschool, school, or college tuition');
        expenseTypeList.add('Source: Parent/Guardian');
        expenseTypeList.add('Source: Student\'s Assets and Earnings');
        expenseTypeList.add('Source: Financial Aid');
        expenseTypeList.add('Source: Loans');
        expenseTypeList.add('Source: Other');

        incomeTypeList = new List <String>();
        incomeTypeList.add('Applicant income earned');

        if (pfs != null && pfs.Id != null) {
            List <Applicant__c> appList = [select Id, Name,
                Assets_Total_Value__c,
                Contact__c,
                Contact__r.Name,
                Federal_Tax_Return_Tax_Status__c,
                Financial_Aid_Sources_Current__c,
                First_Name__c,
                Income_Earned_Current__c,
                Last_Name__c,
                Loan_Sources_Current__c,
                //Loan_Source_Description__c,
                Middle_Initial__c,
                Other_Source_Sources_Current__c,
                Other_Source_Description__c,
                Parent_Sources_Current__c,
                Student_Sources_Current__c,
                Tuition_Cost_Current__c,
                (select Id, Name from School_PFS_Assignments__r
                // added following line to only get School PFS Assignments for this School [dp] 7.12.13
                where School__c = :schoolPFSAssignment.School__c
                order by CreatedDate desc)
            from Applicant__c
            where PFS__c = :pfs.Id];

            type_EduExp_map = new Map <String, FinancialItem>();
            type_appInc_map = new Map <String, FinancialItem>();

            // expense
            for (String typeStr : expenseTypeList) {
                FinancialItem eduExp;
                if (type_EduExp_map.containsKey(typeStr)) {
                    eduExp = type_EduExp_map.get(typeStr);
                } else {
                    eduExp = new FinancialItem();
                    eduExp.typeStr = typeStr;
                }

                for (Applicant__c app : appList) {
                    Applicant applicant = new Applicant();
                    applicant.applicant = app;
                    if (app.School_PFS_Assignments__r.size() > 0) {
                        applicant.schoolPFSAssignment = app.School_PFS_Assignments__r[0];
                    }
                    // cost of child care, preschool, school or college tuition
                    Decimal costChildCare = 0;
                    if (app.Tuition_Cost_Current__c != null) {
                        costChildCare += app.Tuition_Cost_Current__c;
                    }
                    if (typeStr == 'Cost of child care, preschool, school, or college tuition') {
                        applicant.expense = costChildCare;
                    }
                    if (app.Parent_Sources_Current__c != null && typeStr == 'Source: Parent/Guardian') {
                        applicant.expense = app.Parent_Sources_Current__c;
                    }
                    if (app.Student_Sources_Current__c != null && typeStr == 'Source: Student\'s Assets and Earnings') {
                        applicant.expense = app.Student_Sources_Current__c;
                    }
                    if (app.Financial_Aid_Sources_Current__c != null && typeStr == 'Source: Financial Aid') {
                        applicant.expense = app.Financial_Aid_Sources_Current__c;
                    }
                    if (app.Loan_Sources_Current__c != null && typeStr == 'Source: Loans') {
                        applicant.expense = app.Loan_Sources_Current__c;
                    }
                    if (app.Other_Source_Sources_Current__c != null && typeStr == 'Source: Other') {
                        applicant.expense = app.Other_Source_Sources_Current__c;
                    }

                    eduExp.applicantList.add(applicant);
                }
                type_EduExp_map.put(typeStr, eduExp);
            }

            // income
            for (String typeStr : incomeTypeList) {
                FinancialItem appInc;
                if (type_appInc_map.containsKey(typeStr)) {
                    appInc = type_appInc_map.get(typeStr);
                } else {
                    appInc = new FinancialItem();
                    appInc.typeStr = typeStr;
                }

                for (Applicant__c app : appList) {
                    Applicant applicant = new Applicant();
                    applicant.applicant = app;
                    if (app.Income_Earned_Current__c != null && typeStr == 'Applicant income earned') {
                        applicant.income = app.Income_Earned_Current__c;
                    }
                    appInc.applicantList.add(applicant);
                }
                type_appInc_map.put(typeStr, appInc);
            }

            // this is used to display
            otherSourceList = new List <Applicant__c>();
            otherSourceListApplicant = new List <Applicant>();
            for (Applicant__c app : appList) {
                if (app.Other_Source_Description__c != null) {
                    Applicant applicant = new Applicant();
                    applicant.applicant = app;
                    if (app.School_PFS_Assignments__r.size() > 0) {
                        applicant.schoolPFSAssignment = app.School_PFS_Assignments__r[0];
                    }
                    otherSourceListApplicant.add(applicant);
                }
            }

        }
    }

    private void loadDependents() {

        if (pfs != null && pfs.Id != null) {
            String selectStr = getDependentsQuery();
            String queryStr = selectStr + ' where PFS__c = \'' + pfs.Id + '\'';

            List <Dependents__c> dependentsList = Database.Query(queryStr);

            // expense
            for (String typeStr : expenseTypeList) {
                FinancialItem eduExp;
                if (type_EduExp_map.containsKey(typeStr)) {
                    eduExp = type_EduExp_map.get(typeStr);
                } else {
                    eduExp = new FinancialItem();
                    eduExp.typeStr = typeStr;
                }

                for (Dependents__c dep : dependentsList) {
                    for (Integer i=1; i<=dependentsPerRecord; i++) {
                        String nameField = 'Dependent_' + i + '_Full_Name__c';
                        if (dep.get(nameField) != null) {

                            Dependent dependent = new Dependent();
                            dependent.dependents = dep;
                            dependent.name = (String)dep.get(nameField);

                            if (dep.get('Dependent_' + i + '_Tuition_Cost_Current__c') != null && typeStr == 'Cost of child care, preschool, school, or college tuition') {
                                dependent.expense = (Decimal)dep.get('Dependent_' + i + '_Tuition_Cost_Current__c');
                            }
                            if (dep.get('Dependent_' + i + '_Parent_Sources_Current__c') != null && typeStr == 'Source: Parent/Guardian') {
                                dependent.expense = (Decimal)dep.get('Dependent_' + i + '_Parent_Sources_Current__c');
                            }
                            if (dep.get('Dependent_' + i + '_Student_Sources_Current__c') != null && typeStr == 'Source: Student\'s Assets and Earnings') {
                                dependent.expense = (Decimal)dep.get('Dependent_' + i + '_Student_Sources_Current__c');
                            }
                            if (dep.get('Dependent_' + i + '_Fin_Aid_Sources_Current__c') != null && typeStr == 'Source: Financial Aid') {
                                dependent.expense = (Decimal)dep.get('Dependent_' + i + '_Fin_Aid_Sources_Current__c');
                            }
                            if (dep.get('Dependent_' + i + '_Loan_Sources_Current__c') != null && typeStr == 'Source: Loans') {
                                dependent.expense = (Decimal)dep.get('Dependent_' + i + '_Loan_Sources_Current__c');
                            }
                            if (dep.get('Dependent_' + i + '_Other_Source_Sources_Current__c') != null && typeStr == 'Source: Other') {
                                dependent.expense = (Decimal)dep.get('Dependent_' + i + '_Other_Source_Sources_Current__c');
                            }

                            eduExp.dependentList.add(dependent);
                        }
                    }
                }
                type_EduExp_map.put(typeStr, eduExp);
            }

            // other sources list for dependents
            otherSourceListDependent = new List <Dependent>();

            for (Dependents__c dep : dependentsList) {
                for (Integer i=1; i<=dependentsPerRecord; i++) {
                    String nameField = 'Dependent_' + i + '_Full_Name__c';
                    if (dep.get(nameField) != null) {
                        Dependent depedent = new Dependent();
                        depedent.name = (String)dep.get(nameField);
                        if (dep.get('Dependent_' + i + '_Other_Source_Description__c') != null) {
                            depedent.otherSourceDesc = (String)dep.get('Dependent_' + i + '_Other_Source_Description__c');
                            otherSourceListDependent.add(depedent);
                        }
                    }
                }
            }
        }
    }

    private void loadExpenseIncomeList() {

        for (String typeStr : expenseTypeList) {
            if (type_EduExp_map.containsKey(typeStr)) {
                educationalExpenseList.add(type_EduExp_map.get(typeStr));
            }
        }
        for (String typeStr : incomeTypeList) {
            if (type_appInc_map.containsKey(typeStr)) {
                applicantIncomeList.add(type_appInc_map.get(typeStr));
            }
        }

        // this is always same list
        if (educationalExpenseList.size() > 0) {
            applicantList = educationalExpenseList[0].applicantList;
            dependentList = educationalExpenseList[0].dependentList;
        }

    }

    private String getDependentsQuery() {

        String fieldStr = '';
        Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Dependents__c.fields.getMap();
        for (Schema.SObjectField field : fieldMap.values()) {
            Schema.DescribeFieldResult dfr = field.getDescribe();
            if (dfr.isAccessible() == true) {
                if (fieldStr == '') {
                    fieldStr = dfr.getName();
                } else {
                    fieldStr += ', ' + dfr.getName();
                }
            }
        }

        String query = 'select ' + fieldStr + ' from Dependents__c ';

        return query;
    }

    /*End Helper Methods*/

    /*SchoolAcademicYearSelectorStudInterface methods*/

    public PageReference SchoolAcademicYearSelectorStudent_OnChange(PageReference newPR) {

        return newPR;
    }

    /*End SchoolAcademicYearSelectorStudInterface methods*/

}