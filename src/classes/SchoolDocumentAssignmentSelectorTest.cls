@isTest
private class SchoolDocumentAssignmentSelectorTest
{
    
    @isTest
    private static void selectAll_expectRecordReturned()
    {
        Academic_Year__c currentYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        School_PFS_Assignment__c spa = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(currentYear.Name).DefaultSchoolPfsAssignment;
        School_Document_Assignment__c sda = SchoolDocumentAssignmentTestData.Instance
            .forDocumentYear(currentYear.Name)
            .forPfsAssignment(spa.Id).DefaultSchoolDocumentAssignment;

        Test.startTest();
            List<School_Document_Assignment__c> records = SchoolDocumentAssignmentSelector.newInstance().selectAll();
            
            System.assert(!records.isEmpty(), 'Expected the returned list to not be empty.');
            System.assertEquals(1, records.size(), 'Expected there to be one sda record returned.');
            System.assertEquals(sda.Id, records[0].Id, 'Expected the Ids of the sda records to match.');
        Test.stopTest();
    }
}