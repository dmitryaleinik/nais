/**
 * IntegrationMappingDataAccessService.cls
 *
 * @description: 
 *
 * @author: Mike Havrila @ Presence PG
 */

public without sharing class IntegrationMappingDataAccessService extends DataAccessService {

    public List<IntegrationMapping__mdt> getMappingsByIntegration( String integration) {
        List<IntegrationMapping__mdt> returnval;

        if( integration == null) return returnval;

        try {
            returnVal = Database.query( 'select ' + String.join( super.getSObjectFieldNames( 'IntegrationMapping__mdt'), ',') +
                                             ' from IntegrationMapping__mdt' + 
                                            ' where Source__c = :integration');
        } catch( Exception e) {

            System.debug( 'DEBUG:::IntegrationMappingModel.getMappingsByIntegration:Error:' + e.getMessage());
        }

        return returnval;
    }
}