public class RecordTypes {
    
    private static final Map<String, Schema.RecordTypeInfo> accountTypeMap = Account.SObjectType.getDescribe().getRecordTypeInfosByName();
    public static final Id accessOrgAccountTypeId   = accountTypeMap.get('Access Organization').RecordTypeId;   
    public static final Id individualAccountTypeId  = accountTypeMap.get('Individual').RecordTypeId;
    public static final Id partnerAccountTypeId = accountTypeMap.get('Partner/Vendor').RecordTypeId;    
    public static final Id schoolAccountTypeId  = accountTypeMap.get('School').RecordTypeId;
    public static final Id meansBasedFeeWaiverAccountTypeId = accountTypeMap.get(ApplicationFeeSettingsService.MEANS_BASED_FEE_WAIVER_RECORDTYPE).RecordTypeId;
    
    private static final Map<String, Schema.RecordTypeInfo> contactTypeMap = Contact.SObjectType.getDescribe().getRecordTypeInfosByName();
    public static final Id parentContactTypeId      = contactTypeMap.get('Parent/Guardian').RecordTypeId;   
    public static final Id schoolStaffContactTypeId = contactTypeMap.get('School Staff').RecordTypeId;
    public static final Id studentContactTypeId     = contactTypeMap.get('Student').RecordTypeId;
    
    private static final Map<String, Schema.RecordTypeInfo> caseTypeMap = Case.SObjectType.getDescribe().getRecordTypeInfosByName();
    public static final Id schoolParentCommunicationCaseTypeId = caseTypeMap.get('School/Parent Communication').RecordTypeId;
    public static final Id familyPortalCaseTypeId = caseTypeMap.get('Family Portal Case for NAIS').RecordTypeId;
    public static final Id familyPortalCaseSchoolTypeId = caseTypeMap.get('Family Portal Case for School').RecordTypeId;
    public static final Id parentCallCaseTypeId = caseTypeMap.get('Parent Call').RecordTypeId;
    public static final Id schoolCallCaseTypeId = caseTypeMap.get('School Call').RecordTypeId;
    public static final Id sssInternalCaseTypeId = caseTypeMap.get('SSS Internal Case').RecordTypeId;
    public static final Id expClientCaseTypeId = caseTypeMap.get('ExP Client Case').RecordTypeId;
    public static final Id schoolPortalCaseTypeId = caseTypeMap.get('School Portal Case').RecordTypeId; //NAIS-1973
    public static final Id schoolEmailCaseTypeId = caseTypeMap.get('Email - School').RecordTypeId; //NAIS-2480
    public static final Id familyEmailCaseTypeId = caseTypeMap.get('Email - Family').RecordTypeId; //NAIS-2480
    public static final Id familyLiveAgentCaseTypeId = caseTypeMap.get('Parent Live Chat').RecordTypeId;
    
    private static final Map<String, Schema.RecordTypeInfo> opportunityTypeMap = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName();
    public static final Id pfsApplicationFeeOpportunityTypeId = opportunityTypeMap.get('PFS Application Fee').RecordTypeId;
    public static final Id opportunityFeeWaiverPurchaseTypeId = opportunityTypeMap.get('Fee Waiver Purchase').RecordTypeId;
    public static final Id opportunitySubscriptionFeeTypeId = opportunityTypeMap.get('Subscription Fee').RecordTypeId;
    public static final Id opportunityUnmatchedCheckTypeId = opportunityTypeMap.get('Unmatched Check').RecordTypeId;
    
    private static final Map<String, Schema.RecordTypeInfo> transactionTypeMap = Transaction_Line_Item__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    public static final Id adjustmentTransactionTypeId = transactionTypeMap.get('Adjustment').RecordTypeId;
    public static final Id pfsFeeWaiverTransactionTypeId = transactionTypeMap.get('PFS Fee Waiver').RecordTypeId;
    public static final Id paymentTransactionTypeId = transactionTypeMap.get('Payment').RecordTypeId;
    public static final Id paymentAutoTransactionTypeId = transactionTypeMap.get('Payment - Auto').RecordTypeId;
    public static final Id paymentReturnTransactionTypeId = transactionTypeMap.get('Payment Return').RecordTypeId;
    public static final Id saleTransactionTypeId = transactionTypeMap.get('Sale').RecordTypeId;
    public static final Id schoolDiscountTransactionTypeId = transactionTypeMap.get('School Discount').RecordTypeId;
    public static final Id refundTransactionTypeId = transactionTypeMap.get('Refund').RecordTypeId;
    public static final Id refund = refundTransactionTypeId; // [DP] 06.06.2016 hack to get unit tests to pass
    
}