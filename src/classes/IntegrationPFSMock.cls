public with sharing class IntegrationPFSMock implements HttpCalloutMock{
    public IntegrationPFSMock() {
        
    }

    public HTTPResponse respond(HttpRequest req){

        HttpResponse res = new HttpResponse();
        res.setStatusCode(200);

        if(req.getEndpoint().contains('ravenna') && req.getEndpoint().endsWith('students/'))
            res.setBody('[ { "id": 90915, "parent_id": "89426", "created": null, "modified": "2016-07-18 15:12:59", "name_first": "Tester", "name_familiar": "", "name_middle": "", "name_last": "Student", "gender": "M", "birthdate": "1975-01-22", "apply_grade_default": "4", "current_grade": "3", "current_school_id": null, "current_school_name": null, "grades_attended": null, "show_to_school": "0", "country_of_birth": "US", "citizenship": null, "languages_at_home": "English, German", "ethnicity": "African American, Asian American", "hide_from_overview": "0", "no_current_school_reason": "Declined to answer" } ]');
        else if(req.getEndpoint().contains('ravenna') && req.getEndpoint().contains('applications'))
            res.setBody('[ { "id": "151355", "created": "2016-07-21 16:05:06", "modified": "2016-07-21 16:05:06", "admission_year": "2017-2018", "school_id": "234", "school_name": "Sheridan School", "school_logo_url": "https://s3-us-west-2.amazonaws.com/ravenna-generalpurpose-storage/staging/school_logos/42507-a5301d7e12c36dd4b316344c17a14922.png", "dateset_id": 1584, "is_international": false, "apply_grade": "K", "application_status": "started", "core_submittable": false, "supplemental_submittable": false, "fee_due": "60.00", "decision_notice": null, "this_app_is_duplicate": false, "supplemental_submitted": false }, { "id": "151354", "created": "2016-07-21 16:03:41", "modified": "2016-08-16 20:16:08", "admission_year": "2016-2017", "school_id": "123", "school_name": "Bishop Dunne Catholic School", "school_logo_url": "https://s3-us-west-2.amazonaws.com/ravenna-generalpurpose-storage/staging/school_logos/59371-d29a79093b70f4e392c6f2145136b819.png", "dateset_id": 1921, "is_international": false, "apply_grade": "8", "application_status": "applied", "core_submittable": true, "supplemental_submittable": false, "fee_due": 0, "decision_notice": null, "this_app_is_duplicate": false, "supplemental_submitted": false } ]');
        else if(req.getEndpoint().contains('ravenna') && req.getEndpoint().contains('schools') && req.getEndpoint().endsWith('234'))
            res.setBody('{"id":234,"nces_id":"42507"}');
        else if(req.getEndpoint().contains('ravenna') && req.getEndpoint().contains('schools') && req.getEndpoint().endsWith('123'))
            res.setBody('{"id":123,"nces_id":"59371"}');    
        else
            res.setBody('');

        return res;
    }

}