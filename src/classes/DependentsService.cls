public without sharing class DependentsService 
{

    private DependentsService() { }

    public static DependentsService Instance {
        get {
            if (Instance == null) {
                Instance = new DependentsService();
            }
            return Instance;
        }
        private set;
    }


    public void copyDependentsPreviousToNew(Dependents__c prevDependents, Dependents__c newDependents) {
        newDependents.Dependent_1_Full_Name__c = prevDependents.Dependent_1_Full_Name__c;
        newDependents.Dependent_1_Gender__c = prevDependents.Dependent_1_Gender__c;
        newDependents.Dependent_1_Birthdate__c = prevDependents.Dependent_1_Birthdate__c;
        newDependents.Dependent_1_Current_Grade__c = prevDependents.Dependent_1_Current_Grade__c;
        
        newDependents.Dependent_2_Full_Name__c = prevDependents.Dependent_2_Full_Name__c;
        newDependents.Dependent_2_Gender__c = prevDependents.Dependent_2_Gender__c;
        newDependents.Dependent_2_Birthdate__c = prevDependents.Dependent_2_Birthdate__c;
        newDependents.Dependent_2_Current_Grade__c = prevDependents.Dependent_2_Current_Grade__c;
        
        newDependents.Dependent_3_Full_Name__c = prevDependents.Dependent_3_Full_Name__c;
        newDependents.Dependent_3_Gender__c = prevDependents.Dependent_3_Gender__c;
        newDependents.Dependent_3_Birthdate__c = prevDependents.Dependent_3_Birthdate__c;
        newDependents.Dependent_3_Current_Grade__c = prevDependents.Dependent_3_Current_Grade__c;
        
        newDependents.Dependent_4_Full_Name__c = prevDependents.Dependent_4_Full_Name__c;
        newDependents.Dependent_4_Gender__c = prevDependents.Dependent_4_Gender__c;
        newDependents.Dependent_4_Birthdate__c = prevDependents.Dependent_4_Birthdate__c;
        newDependents.Dependent_4_Current_Grade__c = prevDependents.Dependent_4_Current_Grade__c;
        
        newDependents.Dependent_5_Full_Name__c = prevDependents.Dependent_5_Full_Name__c;
        newDependents.Dependent_5_Gender__c = prevDependents.Dependent_5_Gender__c;
        newDependents.Dependent_5_Birthdate__c = prevDependents.Dependent_5_Birthdate__c;
        newDependents.Dependent_5_Current_Grade__c = prevDependents.Dependent_5_Current_Grade__c;
        
        newDependents.Dependent_6_Full_Name__c = prevDependents.Dependent_6_Full_Name__c;
        newDependents.Dependent_6_Gender__c = prevDependents.Dependent_6_Gender__c;
        newDependents.Dependent_6_Birthdate__c = prevDependents.Dependent_6_Birthdate__c;
        newDependents.Dependent_6_Current_Grade__c = prevDependents.Dependent_6_Current_Grade__c;

        newDependents.Additional_Dependents__c = prevDependents.Additional_Dependents__c;
    }

    /**
     * @description Shifts dependent data so there are no gaps. For example, instead of having Dependent 1 and Dependent
     *              3 fields populated, this method will shift Dependent 3 data to the Dependent 2 fields.
     *
     *              SFP-1511: We added the isCallFromTrigger param to avoid clearing 'empty' dependent data when we
     *              aren't doing DML. This allows us to make changes to the record in view state so users can add/delete
     *              dependents without needing to commit each change to the database. Now we can alter viewstate and
     *              save all the changes at once.
     */
    public void moveDependents(Dependents__c dep, Boolean isCallFromTrigger)
    {
        // [CH] NAIS-2276 Refactoring to cover rearranging dependents to fill in blank spots
        // Create a list of Dependents with no gaps
        List<Dependents__c> condensedList = packDependentsIntoList(dep, isCallFromTrigger);
        // Check to see the number of dependents have changed
        if  ( (dep.Number_of_Dependents__c == 7 && condensedList.size() != 6) ||
                (dep.Number_of_Dependents__c != 7 && (dep.Number_of_Dependents__c != condensedList.size()))
            )
        {
            // Clear the current Dependents record values
            for (Integer i = 1; i <= 6; i++)
            {
                removeDependentData(dep, i);
            }

            // Copy the values back out of the dummy records in the condensed array
            for (Integer i = 0; i < condensedList.size(); i++) 
            {
                copyFieldsFromDummyRecord(condensedList[i], dep, i + 1);
            }
            
            if (String.isNotBlank(dep.Additional_Dependents__c))
            {
                dep.Number_of_Dependents__c = condensedList.size() + 1;
            } 
            else
            {
                dep.Number_of_Dependents__c = condensedList.size();
            }
        }
    }

    private List<Dependents__c> packDependentsIntoList(Dependents__c depRecord, Boolean isCallFromTrigger) 
    {
        List<Dependents__c> returnList = new List<Dependents__c>();

        for (Integer i = 1; i <= 6; i++)
        {
            if (isCallFromTrigger)
            {
                if (String.isNotBlank((String)depRecord.get('Dependent_' + String.valueOf(i) + '_Full_Name__c')) &&
                    depRecord.get('Dependent_' + String.valueOf(i) + '_birthdate__c') != null &&
                    depRecord.get('Dependent_' + String.valueOf(i) + '_Gender__c') != null &&
                    depRecord.get('Dependent_' + String.valueOf(i) + '_Current_Grade__c') != null)
                {
                    returnList.add(copyFieldsToDummyRecord(depRecord, i));
                }
            }
            else
            {
                if (String.isNotBlank((String)depRecord.get('Dependent_' + String.valueOf(i) + '_Full_Name__c')) ||
                    depRecord.get('Dependent_' + String.valueOf(i) + '_birthdate__c') != null ||
                    depRecord.get('Dependent_' + String.valueOf(i) + '_Gender__c') != null ||
                    depRecord.get('Dependent_' + String.valueOf(i) + '_Current_Grade__c') != null ||
                    String.isNotBlank((String)depRecord.get('Dependent_' + String.valueOf(i) + '_Current_School__c')))
                {
                    returnList.add(copyFieldsToDummyRecord(depRecord, i));
                }
            }
        }

        return returnList;
    }

    private Dependents__c copyFieldsToDummyRecord(Dependents__c sourceRecord, Integer i) {
        Dependents__c dummyRecord = new Dependents__c();

        dummyRecord.put('Dependent_1_Full_Name__c',            sourceRecord.get('Dependent_' + String.valueOf(i) + '_Full_Name__c'));
        dummyRecord.put('Dependent_1_birthdate__c',            sourceRecord.get('Dependent_' + String.valueOf(i) + '_birthdate__c'));
        dummyRecord.put('Dependent_1_Gender__c',            sourceRecord.get('Dependent_' + String.valueOf(i) + '_Gender__c'));
        dummyRecord.put('Dependent_1_Current_School__c',        sourceRecord.get('Dependent_' + String.valueOf(i) + '_Current_School__c'));
        dummyRecord.put('Dependent_1_Current_Grade__c',          sourceRecord.get('Dependent_' + String.valueOf(i) + '_Current_Grade__c'));
        dummyRecord.put('Dependent_1_School_Next_Year__c',        sourceRecord.get('Dependent_' + String.valueOf(i) + '_School_Next_Year__c'));
        dummyRecord.put('Dependent_1_School_Next_Year_Type__c',      sourceRecord.get('Dependent_' + String.valueOf(i) + '_School_Next_Year_Type__c'));
        dummyRecord.put('Dependent_1_Grade_Next_Year__c',        sourceRecord.get('Dependent_' + String.valueOf(i) + '_Grade_Next_Year__c'));
        dummyRecord.put('Dependent_1_Have_Education_Expenses__c',    sourceRecord.get('Dependent_' + String.valueOf(i) + '_Have_Education_Expenses__c'));
        dummyRecord.put('Dependent_1_Tuition_Cost_Est__c',        sourceRecord.get('Dependent_' + String.valueOf(i) + '_Tuition_Cost_Est__c'));
        dummyRecord.put('Dependent_1_Tuition_Cost_Current__c',      sourceRecord.get('Dependent_' + String.valueOf(i) + '_Tuition_Cost_Current__c'));
        dummyRecord.put('Dependent_1_Parent_Sources_Current__c',    sourceRecord.get('Dependent_' + String.valueOf(i) + '_Parent_Sources_Current__c'));
        dummyRecord.put('Dependent_1_Fin_Aid_Sources_Current__c',    sourceRecord.get('Dependent_' + String.valueOf(i) + '_Fin_Aid_Sources_Current__c'));
        dummyRecord.put('Dependent_1_Student_Sources_Current__c',    sourceRecord.get('Dependent_' + String.valueOf(i) + '_Student_Sources_Current__c'));
        dummyRecord.put('Dependent_1_Student_Sources_Est__c',      sourceRecord.get('Dependent_' + String.valueOf(i) + '_Student_Sources_Est__c'));
        dummyRecord.put('Dependent_1_Other_Source_Sources_Current__c',  sourceRecord.get('Dependent_' + String.valueOf(i) + '_Other_Source_Sources_Current__c'));
        dummyRecord.put('Dependent_1_Other_Source_Description__c',    sourceRecord.get('Dependent_' + String.valueOf(i) + '_Other_Source_Description__c'));
        dummyRecord.put('Dependent_1_Loan_Sources_Current__c',      sourceRecord.get('Dependent_' + String.valueOf(i) + '_Loan_Sources_Current__c'));
        dummyRecord.put('Dependent_1_Loan_Sources_Est__c',        sourceRecord.get('Dependent_' + String.valueOf(i) + '_Loan_Sources_Est__c'));
        dummyRecord.put('Dependent_1_Other_Source_Sources_Est__c',    sourceRecord.get('Dependent_' + String.valueOf(i) + '_Other_Source_Sources_Est__c'));
        dummyRecord.put('Dependent_1_Parent_Sources_Est__c',      sourceRecord.get('Dependent_' + String.valueOf(i) + '_Parent_Sources_Est__c'));
        dummyRecord.put('Dependent_1_Fin_Aid_Sources_Est__c',      sourceRecord.get('Dependent_' + String.valueOf(i) + '_Fin_Aid_Sources_Est__c'));
        dummyRecord.put('Dependent_1_Tuition_Program_Next_Year__c',    sourceRecord.get('Dependent_' + String.valueOf(i) + '_Tuition_Program_Next_Year__c'));
        return dummyRecord;
    }


    private Dependents__c copyFieldsFromDummyRecord(Dependents__c dummyRecord, Dependents__c targetRecord, Integer i) {
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Full_Name__c',          dummyRecord.get('Dependent_1_Full_Name__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_birthdate__c',          dummyRecord.get('Dependent_1_birthdate__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Gender__c',            dummyRecord.get('Dependent_1_Gender__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Current_School__c',        dummyRecord.get('Dependent_1_Current_School__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Current_Grade__c',        dummyRecord.get('Dependent_1_Current_Grade__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_School_Next_Year__c',        dummyRecord.get('Dependent_1_School_Next_Year__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_School_Next_Year_Type__c',    dummyRecord.get('Dependent_1_School_Next_Year_Type__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Grade_Next_Year__c',        dummyRecord.get('Dependent_1_Grade_Next_Year__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Have_Education_Expenses__c',    dummyRecord.get('Dependent_1_Have_Education_Expenses__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Tuition_Cost_Est__c',        dummyRecord.get('Dependent_1_Tuition_Cost_Est__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Tuition_Cost_Current__c',      dummyRecord.get('Dependent_1_Tuition_Cost_Current__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Parent_Sources_Current__c',    dummyRecord.get('Dependent_1_Parent_Sources_Current__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Fin_Aid_Sources_Current__c',    dummyRecord.get('Dependent_1_Fin_Aid_Sources_Current__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Student_Sources_Current__c',    dummyRecord.get('Dependent_1_Student_Sources_Current__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Student_Sources_Est__c',      dummyRecord.get('Dependent_1_Student_Sources_Est__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Other_Source_Sources_Current__c',  dummyRecord.get('Dependent_1_Other_Source_Sources_Current__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Other_Source_Description__c',    dummyRecord.get('Dependent_1_Other_Source_Description__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Loan_Sources_Current__c',      dummyRecord.get('Dependent_1_Loan_Sources_Current__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Loan_Sources_Est__c',        dummyRecord.get('Dependent_1_Loan_Sources_Est__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Other_Source_Sources_Est__c',    dummyRecord.get('Dependent_1_Other_Source_Sources_Est__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Parent_Sources_Est__c',      dummyRecord.get('Dependent_1_Parent_Sources_Est__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Fin_Aid_Sources_Est__c',      dummyRecord.get('Dependent_1_Fin_Aid_Sources_Est__c'));
        targetRecord.put('Dependent_' + String.valueOf(i) + '_Tuition_Program_Next_Year__c',  dummyRecord.get('Dependent_1_Tuition_Program_Next_Year__c'));

        return targetRecord;
    }


    public Dependents__c removeDependentData(Dependents__c depRecord, Integer i) {

        depRecord.put('Dependent_' + String.valueOf(i) + '_Full_Name__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_birthdate__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_Gender__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_Current_School__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_Current_Grade__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_School_Next_Year__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_School_Next_Year_Type__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_Grade_Next_Year__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_Have_Education_Expenses__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_Tuition_Cost_Est__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_Tuition_Cost_Current__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_Parent_Sources_Current__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_Fin_Aid_Sources_Current__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_Student_Sources_Current__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_Student_Sources_Est__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_Other_Source_Sources_Current__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_Other_Source_Description__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_Loan_Sources_Current__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_Loan_Sources_Est__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_Other_Source_Sources_Est__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_Parent_Sources_Est__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_Fin_Aid_Sources_Est__c', null);
        depRecord.put('Dependent_' + String.valueOf(i) + '_Tuition_Program_Next_Year__c', null);

        return depRecord;
    }
}