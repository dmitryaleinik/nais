/**
 * @description This class is responsible for querying Annual Setting records.
 */
public class AnnualSettingsSelector extends fflib_SObjectSelector {

    @testVisible private static final String ID_SET_PARAM = 'idSet';
    @testVisible private static final String SCHOOL_IDS_PARAM = 'schoolIds';
    @testVisible private static final String ACADEMIC_YEAR_NAMES_PARAM = 'academicYearNames';
    @testVisible private static final String ANN_SET_FIELDS_TO_SELECT_PARAM = 'fieldsToSelect';

    /**
     * @description Default constructor for an instance of the AnnualSettingsSelector that will not include field sets while
     *              enforcing FLS and CRUD.
     */
    public AnnualSettingsSelector() { }

    /**
     * @description Queries Annual Setting records by Id.
     * @param idSet The Ids of the Annual Setting records to query.
     * @return A list of Annual Setting records.
     * @throws ArgumentNullException if idSet is null.
     */
    public List<Annual_Setting__c> selectById(Set<Id> idSet) {
        ArgumentNullException.throwIfNull(idSet, ID_SET_PARAM);

        return (List<Annual_Setting__c>)super.selectSObjectsById(idSet);
    }
    
    /**
     * @description Queries Annual Setting records by school id and academic year names.
     * @param schoolIds The ids of the schools to get annual settings for.
     * @param academicYearNames The names of academic years (e.g. 2016-2017).
     * @return A list of Annual Setting records.
     * @throws ArgumentNullException if any parameter is null.
     */
    public List<Annual_Setting__c> selectBySchoolAndAcademicYear(Set<Id> schoolIds, Set<String> academicYearNames) {
        
        return selectBySchoolAndAcademicYear(schoolIds, academicYearNames, null);
    }
    
    /**
     * @description Queries Annual Setting records by school id and academic year names.
     * @param schoolIds The ids of the schools to get annual settings for.
     * @param academicYearNames The names of academic years (e.g. 2016-2017).
     * @param fieldsToquery The set of fields to retrieve.
     * @return A list of Annual Setting records.
     * @throws ArgumentNullException if any parameter is null.
     */
    public List<Annual_Setting__c> selectBySchoolAndAcademicYear(Set<Id> schoolIds, Set<String> academicYearNames, List<String> fieldsToquery) {
        ArgumentNullException.throwIfNull(schoolIds, SCHOOL_IDS_PARAM);
        ArgumentNullException.throwIfNull(academicYearNames, ACADEMIC_YEAR_NAMES_PARAM);

        String condition = 'School__c IN :schoolIds AND Academic_Year__r.Name IN :academicYearNames';
        String query;
        
        if (fieldsToquery != null) {
            
            query = newQueryFactory().selectFields(fieldsToquery).setCondition(condition).toSOQL();
        } else {
            
            query = newQueryFactory().setCondition(condition).toSOQL();
        }
        
        return Database.Query(query);
    }
    
    /**
     * @description Queries Annual Setting records by "School_Academic_Year_Picklist__c (SchoolId:AcademicYearPicklist)".
     * @param schoolAcademicYear The set of SchoolId:AcademicYearPicklist to be queried.
     * @param fieldsToquery The set of fields to retrieve.
     * @return A list of Annual Setting records.
     * @throws ArgumentNullException if any parameter is null.
     */
    public List<Annual_Setting__c> selectBySchoolIdAcademicYearName(Set<String> schoolAcademicYear, List<String> fieldsToquery) {
        ArgumentNullException.throwIfNull(schoolAcademicYear, 'schoolAcademicYear');

        String condition = 'School_Academic_Year_Picklist__c IN :schoolAcademicYear';
        String query;
        
        if (fieldsToquery != null) {
            
            query = newQueryFactory().selectFields(fieldsToquery).setCondition(condition).toSOQL();
        } else {
            
            query = newQueryFactory().setCondition(condition).toSOQL();
        }
        
        return Database.Query(query);
    }
    
    public List<Annual_Setting__c> selectWithCustomFieldListById(Set<Id> idSet, List<String> fieldsToSelect) {
        ArgumentNullException.throwIfNull(idSet, ID_SET_PARAM);
        ArgumentNullException.throwIfNull(fieldsToSelect, ANN_SET_FIELDS_TO_SELECT_PARAM);

        assertIsAccessible();

        String annualSettingQuery = newQueryFactory(false)
                .selectFields(fieldsToSelect)
                .setCondition('Id IN :idSet')
                .toSOQL();

        return Database.query(annualSettingQuery);
    }

    public List<Annual_Setting__c> selectWithCustomFieldList(List<String> fieldsToSelect) {
        ArgumentNullException.throwIfNull(fieldsToSelect, ANN_SET_FIELDS_TO_SELECT_PARAM);

        assertIsAccessible();

        String annualSettingQuery = newQueryFactory(false)
                .selectFields(fieldsToSelect)
                .toSOQL();

        return Database.query(annualSettingQuery);
    }

    public List<Annual_Setting__c> selectAllAnnualSettings() {
        assertIsAccessible();

        String annualSettingQuery = newQueryFactory()
                .toSOQL();

        return Database.query(annualSettingQuery);
    }
    
    private Schema.SObjectType getSObjectType() {
        return Annual_Setting__c.SObjectType;
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>{
                Annual_Setting__c.Name,
                Annual_Setting__c.Document_Verification__c,
                Annual_Setting__c.School__c,
                Annual_Setting__c.School_Name__c,
                Annual_Setting__c.Academic_Year_Name__c,
                Annual_Setting__c.Print_RFC_On_Two_Pages__c,
                Annual_Setting__c.Display_Next_Year_Tax_Estimates__c,
                Annual_Setting__c.Display_Prior_Year_Verification_Values__c
        };
    }
    
    private String mOrderBy;
    
    /**
     * @description Controls the default ordering of records returned by the base queries,
     *              defaults to the 'Name' field of the object or 'CreatedDate' if there is none.
     **/
    public override String getOrderBy() {
        
        if (mOrderBy == null) {
            return super.getOrderBy();
        }
        return mOrderBy;
    }
    
    /**
     * @description Use this method to override the default sorting provided by fflib_SObjectSelector.
     * @param orderBy You can use the following formatt: 'Academic_Year__r.Start_Date__c DESC, Name ASC'
     */
    public void setOrderBy(String orderBy) { 
        mOrderBy = orderBy;
    }

    /**
     * @description Creates a new instance of the AnnualSettingsSelector.
     * @return An instance of AnnualSettingsSelector.
     */
    public static AnnualSettingsSelector newInstance() {
        return new AnnualSettingsSelector();
    }
    
    /**
     * @description Singleton instance of the selector to be reused at your leisure.
     */
    public static AnnualSettingsSelector Instance {
        get {
            if (Instance == null) {
                Instance = newInstance();
            }
            return Instance;
        }
        private set;
    }
}