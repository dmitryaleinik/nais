public with sharing class SchoolFCWTable {
    
    @testvisible private static final String SFCW_TABLE_TELL_ME_MORE_LINK_DOCINFO_NAME = 'SchoolFCWTable_Tell_Me_More_Link';
    
    public SchoolFCWController fcwController {get;set;}

    public String tellMeMoreDocId {
        get {
            if (tellMeMoreDocId == null) {
                if (!Test.isRunningTest()) {
                    tellMeMoreDocId = 
                        DocumentLinkMappingService.getDocumentRecordId(SFCW_TABLE_TELL_ME_MORE_LINK_DOCINFO_NAME);
                } else {
                    tellMeMoreDocId = DocumentLinkMappingService.getDocumentRecordId(
                        DocumentLinkMappingService.TEST_DOCUMENT_LINK_MAPPING_RECORD_NAME);           
                }
            }
            
            return tellMeMoreDocId;
        } 
        private set;
    }
}