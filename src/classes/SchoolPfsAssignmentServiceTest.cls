@isTest
private class SchoolPfsAssignmentServiceTest {
    @isTest
    private static void copyPersonalJudgementSettings_expectValuesReturned() {
        School_PFS_Assignment__c spa = new School_PFS_Assignment__c();
        Annual_Setting__c annualSetting = new Annual_Setting__c(
                Add_Depreciation_Home_Business_Expense__c = 'Yes',
                Use_Home_Equity__c = '',
                Use_Home_Equity_Cap__c = '',
                Use_Housing_Index_Multiplier__c = '',
                Use_Dividend_Interest_to_Impute_Assets__c = '',
                Impute_Rate__c = 2.0,
                Apply_Minimum_Income__c = '',
                Minimum_Income_Amount__c = 20000,
                Use_Cost_of_Living_Adjustment__c = '',
                Override_Default_COLA_Value__c = 5000,
                Adjust_Housing_Portion_of_IPA__c = 'Yes',
                IPA_Housing_Family_of_4__c = 20000
        );

        Test.startTest();
        School_PFS_Assignment__c returnedSpa = SchoolPfsAssignmentService.Instance
                .copyPersonalJudgementSettings(spa, annualSetting);
        Test.stopTest();

        System.assertEquals(annualSetting.Add_Depreciation_Home_Business_Expense__c, spa.Add_Deprec_Home_Bus_Expense__c);
        System.assertEquals(annualSetting.Use_Home_Equity__c, spa.Use_Home_Equity__c);
        System.assertEquals(annualSetting.Use_Home_Equity_Cap__c, spa.Use_Home_Equity_Cap__c);
        System.assertEquals(annualSetting.Use_Housing_Index_Multiplier__c, spa.Use_Housing_Index_Multiplier__c);
        System.assertEquals(annualSetting.Use_Dividend_Interest_to_Impute_Assets__c, spa.Use_Div_Int_to_Impute_Assets__c);
        System.assertEquals(annualSetting.Impute_Rate__c, spa.Impute__c);
        System.assertEquals(annualSetting.Apply_Minimum_Income__c, spa.Apply_Minimum_Income__c);
        System.assertEquals(annualSetting.Minimum_Income_Amount__c, spa.Minimum_Income_Amount__c);
        System.assertEquals(annualSetting.Use_Cost_of_Living_Adjustment__c, spa.Use_COLA__c);
        System.assertEquals(annualSetting.Override_Default_COLA_Value__c, spa.Override_Default_COLA_Value__c);
        System.assertEquals(annualSetting.Adjust_Housing_Portion_of_IPA__c, spa.Adjust_Housing_Portion_of_IPA__c);
        System.assertEquals(annualSetting.IPA_Housing_Family_of_4__c, spa.IPA_Housing_Family_of_4__c);
    }

    @isTest
    private static void copyPersonalJudgementSettings_nullSpa_expectArgumentNullException() {
        try {
            Test.startTest();
            SchoolPfsAssignmentService.Instance.copyPersonalJudgementSettings(null, null);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, SchoolPfsAssignmentService.SPA_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void copyPersonalJudgementSettings_nullAnnualSetting_expectArgumentNullException() {
        try {
            Test.startTest();
            SchoolPfsAssignmentService.Instance.copyPersonalJudgementSettings(new School_PFS_Assignment__c(), null);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, SchoolPfsAssignmentService.ANNUAL_SETTING_PARAM);
        } finally {
            Test.stopTest();
        }
    }
}