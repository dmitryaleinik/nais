public with sharing class SchoolAnnualSettingsController implements SchoolAcademicYearSelectorInterface
{

    @testVisible private static final String DB_BOARDING = 'Boarding';
    @testVisible private static final String DB_DAY = 'Day';
    /*Initialization*/

    public SchoolAnnualSettingsController()
    {
        currentUser = GlobalVariables.getCurrentUser();
        isNonAdministrator = (currentUser.Profile.Name != ProfileSettings.SchoolAdminProfileName) ? true : false;

        List<AsyncApexJob> sasBatchesInQueue = [
            SELECT  Status, ApexClass.Name
            FROM AsyncApexJob
            WHERE ApexClass.Name = 'SchoolAnnualSettingsController'];

        if (!sasBatchesInQueue.isEmpty())
        {
            messageName = Label.Annual_Setting_Async_Update;
            errorLevel = 'INFO';
            showMessage();
            sasBatchInProgress = true;
        }
    }
    
    /*End Initialization*/
    
    /*Properties*/
    public Boolean sasBatchInProgress{get; private set;}
    public Boolean isNonAdministrator {get; set;}

    public SchoolAnnualSettingsController Me {
        get { return this; }
    }
    public String PP_SelectedYear{
       get {
           String currentYear = getSelectedAcademicYear();
           return String.ValueOf(Integer.ValueOf(currentYear.left(4))-3); }
    }
    public String priorSelectedYear{
        get { 
            String currentYear = getSelectedAcademicYear();
            return String.ValueOf(Integer.ValueOf(currentYear.left(4))-2); }
    }	
    public String currentSelectedYear{
        get { 
            String currentYear = getSelectedAcademicYear();
            return String.ValueOf(Integer.ValueOf(currentYear.left(4))-1); }
    }
    public static String getSelectedAcademicYear()
    {
        String academicyearidParam = ApexPages.currentPage().getParameters().get('academicyearid');
        String academicYearTmp = GlobalVariables.getCurrentAcademicYear().Name;
        
        try
        {
            academicYearTmp = [SELECT Id, Name FROM Academic_Year__c WHERE ID = :academicyearidParam limit 1].Name;
        } catch(Exception e){}

        return academicYearTmp; 
    }
    
    
    private Map<String, List<Required_Document__c>> requiredDocuments 
    {
        get 
        {
            if (requiredDocuments == null) 
            {
                Id schoolId = GlobalVariables.getCurrentSchoolId();
                        
                requiredDocuments = RequiredDocumentSelector.newInstance()
                    .mapBySchoolAcademicYear(new Set<Id>{schoolId}, new Set<String>{Model.Academic_Year_Name__c});
            }
            
            return requiredDocuments;
        }
        set;
    }

    @testVisible public Boolean hasPreviousAcademicYear 
    {
        get 
        {
            if (hasPreviousAcademicYear == null) 
            {
                String previousAYStr = GlobalVariables.getPreviousAcademicYearStr(getSelectedAcademicYear());
                
                List<Annual_Setting__c> priorAS = new List<Annual_Setting__c>([
                    SELECT Id 
                    FROM Annual_Setting__c 
                    WHERE Academic_Year_Name__c = :previousAYStr 
                    AND School__c = :GlobalVariables.getCurrentSchoolId() 
                    LIMIT 1]);
                    
                hasPreviousAcademicYear = !priorAS.isEmpty();
            }
            
            return hasPreviousAcademicYear;
        }
        
        private set;
    }

    @testVisible public Boolean hasPPAcademicYear 
    {
        get 
        {
            if (hasPPAcademicYear == null) 
            {
                String pp_AYStr = GlobalVariables.getPPAcademicYearStr(getSelectedAcademicYear());
                
                List<Annual_Setting__c> ppAS = new List<Annual_Setting__c>([
                    SELECT Id 
                    FROM Annual_Setting__c 
                    WHERE Academic_Year_Name__c = :pp_AYStr 
                    AND School__c = :GlobalVariables.getCurrentSchoolId() 
                    LIMIT 1]);
                    
                hasPPAcademicYear = !ppAS.isEmpty();
            }
            
            return hasPPAcademicYear;
        }
        
        private set;
    }

    // SFP-1810 : boolean to Show Prior Prior YearSection
    public Boolean showPPYearTaxDocsSection {
        get {
            if (showPPYearTaxDocsSection == null) 
            {
                showPPYearTaxDocsSection = showTaxDocsPPYSection(false);
            }
            
            return showPPYearTaxDocsSection;
        }
        private set;
    }
    
    public Boolean showPriorYearTaxDocsSection 
    {
        get 
        {
            if (showPriorYearTaxDocsSection == null) 
            {
                showPriorYearTaxDocsSection = showTaxDocsSection(false);
            }
            
            return showPriorYearTaxDocsSection;
        }
        
        private set;
    }
    
    public Boolean showCurrentYearTaxDocsSection 
    {
        get 
        {
            if (showCurrentYearTaxDocsSection == null) 
            {
                showCurrentYearTaxDocsSection = showTaxDocsSection(true);
            }
            
            return showCurrentYearTaxDocsSection;
        }
        
        private set;
    }
    // SFP-1810 : Previous Previous Year(PPY) section should only be available for schools with the Premium Subscription type.
    @testVisible private Boolean showTaxDocsPPYSection(Boolean checkPPYTaxDocument) 
    {
        if (!checkPPYTaxDocument) {
			Subscription__c premiumSubscriptionRec = GlobalVariables.getMostRecentSubscription();
			if (premiumSubscriptionRec.Subscription_Type__c == 'Premium') {
				return true;
			} else {
				return false;
			}
		}
        return false;
    }
 
    @testVisible private Boolean showTaxDocsSection(Boolean checkCurrentTaxDocument) 
    {
        if (hasPreviousAcademicYear) 
        {
            String taxYear;
            
            if (checkCurrentTaxDocument) 
            {
                taxYear = GlobalVariables.getDocYearStringFromAcadYearName(Model.Academic_Year_Name__c);
            } else 
            {
                taxYear = GlobalVariables.getPrevDocYearStringFromAcadYearName(Model.Academic_Year_Name__c);
            }
            
            Id schoolId = GlobalVariables.getCurrentSchoolId();
            
            String keySearch = RequiredDocumentSelector.newInstance().createMapKey(schoolId, Model.Academic_Year_Name__c, taxYear);
            return requiredDocuments.containsKey(keySearch);
        }
		
        return true;//If there's no previous annualSettings for the school. Then, deadline dates are not tied to required documents.
    }
    
    public Id batjobId;
    public Boolean bDay { get; private set; }
    public Boolean bBoarding { get; private set; }
    public String errorLevel { get; set; } //NAIS-1775
    public String messageName { get; set; } //NAIS-1775

    /**
     * @description Determines whether or not there is a valid annual setting. This will return true if the Annual
     *              Setting record is not null and the Annual Settings Updated field is not null.
     */
    public Boolean hasAnnualSetting 
    {
        get 
        {
            return this.Model==null || this.Model.Annual_Settings_Updated__c == null ? false : true;
        }
    }
    
    public Boolean ShowValidationMessage { get; set; }

    public List<Grade> Grades { get; private set; }
    @testVisible public Annual_Setting__c Model { get; private set; }
    public User currentUser { get; set; }
    public Academic_Year__c accYear { get; set; }
    public String accYearStart; //NAIS-2110

    public String deadLineType {get{ return ('No Fixed Deadlines');}} //NAIS-1099
    public Boolean bOneSchedulePerGrade { 
        get { return (this.Model.Tuition_Schedule_Type__c == 'One Schedule per Grade'); }
    }
    
    // Methods for Academic Years -- TO DO clean this up and pull into a helper Class
    // making this list public so we can optionally pass in a list of academic years
    public List<Academic_Year__c> academicYears_x { get; set; }
    public List<Academic_Year__c> getAcademicYears() {
        if(this.academicYears_x == null) {
            this.academicYears_x = GlobalVariables.getCurrentAcademicYears();
        }
        
        return this.academicYears_x;
    }
    
    private Map<Id, Academic_Year__c> academicYearMap_x {get; set;}
    public Map<Id, Academic_Year__c> getAcademicYearMap() {
        if(this.academicYearMap_x == null) {    
            this.academicYearMap_x = new Map<Id, Academic_Year__c>(this.getAcademicYears());
        }
        
        return this.academicYearMap_x;
    }
    
    private List<SelectOption> academicYearOptions_x {get; set;}
    public List<SelectOption> getAcademicYearOptions() {
        if(this.academicYearOptions_x == null) {
            this.academicYearOptions_x = new List<SelectOption>();
            
            for(Academic_Year__c year : this.getAcademicYears()) {
                this.academicYearOptions_x.add(new SelectOption(year.Id, year.Name));
            }
        }
        
        return this.academicYearOptions_x;
    }
    
    public Boolean getIsMIEPilotActive(){
        return SchoolPortalSettings.MIE_Pilot;
    }
           
    public Map<String, String> FieldLabels {
        get {
            return ExpCore.GetAllFieldLabels('Annual_Setting__c');
        }
    }
    
    public Decimal additionalQuestionLimit {
        get {
            if (additionalQuestionLimit == null) {
                additionalQuestionLimit = SchoolPortalSettings.Additional_Questions_Limit;
            }
            
            return additionalQuestionLimit;
        }
        
        set;
    } 
    
    public String MIELinkToPDF {
        get {
            return SchoolPortalSettings.MIE_Link_on_Annual_Settings_page;
        }
    }
    
    // Tuition and Fees Properties
    static public List<String> GradeNames = new List<String> {
        'Preschool', 'Pre K', 'Jr K', 'K', 'Pre-First', 'Grade 1', 'Grade 2', 'Grade 3', 'Grade 4', 'Grade 5',
        'Grade 6', 'Grade 7', 'Grade 8', 'Grade 9', 'Grade 10', 'Grade 11', 'Grade 12', 'Post Grad'
    };
    // This must match Annual_Settings__c. Shown in the page table.
    static public List<String> GradeFieldNames = new List<String> {
        'Preschool', 'Pre_K', 'Jr_K', 'K', 'Pre_First', 'Grade_1', 'Grade_2', 'Grade_3', 'Grade_4', 'Grade_5',
        'Grade_6', 'Grade_7', 'Grade_8', 'Grade_9', 'Grade_10', 'Grade_11', 'Grade_12' , 'Post_Grad'
    };
    // Used for mapping the picklist from SPA to our grade array.
    Map<String, Integer> gradeMap = new Map<String, Integer> {
        'Preschool' => 0,
        'Pre-Kindergarten' => 1,
        'Junior Kindergarten' => 2,
        'Kindergarten' => 3,
        'Pre-First' => 4,
        '1' => 5,
        '2' => 6,
        '3' => 7,
        '4' => 8,
        '5' => 9,
        '6' => 10,
        '7' => 11,
        '8' => 12,
        '9' => 13,
        '10' => 14,
        '11' => 15,
        '12' => 16, 
        'Post Graduation' => 17
    };
    private Annual_Setting__c previousModel {get;set;} //NAIS-2435
    public boolean isAvailableCopyTuitionCost {get;set;}

    /*End Properties*/
        
    /*Action Methods*/
    
    public void LoadAnnualSetting() 
    {
        PageReference page = ApexPages.currentPage();
        Map<String, String> parameters = page.getParameters();
        
        String settingsId = null;

        if(parameters.containsKey('id'))
        {
            settingsId = parameters.get('id');
        } 
        //[CH] NAIS-1359 Added default to current year is parameter is not provided
        String academicYearId = null;

        if(parameters.containsKey('academicyearid'))
        {
            academicYearId = parameters.get('academicyearid');
        } else
        {
            academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        }
        
         /*NAIS-2435*/
        isAvailableCopyTuitionCost=false;
        list<Annual_Setting__c> lstAnnualSetting = new list<Annual_Setting__c>();
        Academic_Year__c previousAcademicY =  GlobalVariables.getPreviousAcademicYear(academicYearId);

        if(previousAcademicY != null)
        {
            Id previousAcademicYearId =previousAcademicY.Id; 
            lstAnnualSetting = GlobalVariables.getCurrentAnnualSettings(false, previousAcademicYearId);  
        }  
        /*End NAIS-2435*/
        
        //NAIS-1775 Start   
        accYear = [SELECT Id, Name FROM Academic_Year__c WHERE ID = :academicYearId];
        accYearStart = accYear.Name.split('-', 2)[0].trim(); //NAIS-2110
        //NAIS-1775 End
        if(settingsId != null && settingsId != '') {
            List<Annual_Setting__c> annualSettings = GlobalVariables.getCurrentAnnualSettings(true, datetime.now().date());
        
            if(settingsId != '' && settingsId != null) {
                Map<Id, Annual_Setting__c> annualSettingsMap = new Map<Id, Annual_Setting__c>(annualSettings);
                this.Model = annualSettingsMap.get(settingsId);
            }
        } else if(academicYearId != null && academicYearId != '') {
            if(academicYearId != '' && academicYearId != null) {
                List<Annual_Setting__c> annualSettings = GlobalVariables.getCurrentAnnualSettings(true, academicYearId);
                
                for(Annual_Setting__c setting : annualSettings) {

                    if(setting.Academic_Year__c == academicYearId) {
                        this.Model = setting;
                    }
                    
                }
            }
        } else {
            List<Annual_Setting__c> annualSettings = GlobalVariables.getCurrentAnnualSettings(true, datetime.now().date());
            this.Model = annualSettings[0];
        }       
        // The rest of this is for Tuition&Fees.
        if (this.Model.Tuition_Schedule_Type__c == null) {
            this.Model.Tuition_Schedule_Type__c = 'One Schedule per Grade';
        }
        
        // Need to do again for SchoolAcademicYearSelector_OnChange().
        // myAnnualSettings.Tuition_Schedule_Type__c should be set before calling getGradesList().
        getGradesList(this.Model); //NAIS-2435
        
        this.bBoarding = (this.Model.School__r.School_Type__c == DB_BOARDING) || (this.Model.School__r.School_Type__c == 'Day and Boarding');
        this.bDay = (this.Model.School__r.School_Type__c == DB_DAY) || (this.Model.School__r.School_Type__c == 'Day and Boarding');
        if ((this.bBoarding == false) && (this.bDay == false))
        {
            // If Model.School__r.School_Type__c is set to None, then show everything.
            bBoarding = bDay = true;
        }
        
        if (GlobalVariables.isLimitedSchoolView()){
            this.Model.Accept_Documents_Online__c = 'No';
        } else {
            this.Model.Accept_Documents_Online__c = 'Yes';
        }
        if(lstAnnualSetting != null && lstAnnualSetting.size()>0)
        {
            previousModel = lstAnnualSetting.get(0);
            isAvailableCopyTuitionCost = getAvailabletoCopyTuitionCost(); 
        }
        else
        {
            previousModel=null;
        }

        if(this.Model.Copy_Verification_Values_to_Revisions__c == null){
            this.Model.Copy_Verification_Values_to_Revisions__c = 'No';
        }
    }
    private boolean getAvailabletoCopyTuitionCost(){
        boolean isAvailableToCopy=false;
        if(this.Model.Tuition_Schedule_Type__c == previousModel.Tuition_Schedule_Type__c)
            isAvailableToCopy= true;
        return isAvailableToCopy;
    }
    public void copyTuitionFees()
    {
        getGradesList(previousModel);
    }

    public PageReference Submit()
    {
        try
        {
            Boolean skipPPYDeadlines = !showPPYearTaxDocsSection;
            Boolean skipPriorYearDeadlines = !showPriorYearTaxDocsSection;
            Boolean skipCurrentYearDeadlines = !showCurrentYearTaxDocsSection;

            if (AnnualSettingHelper.IsValid(this.Model, skipPPYDeadlines, skipPriorYearDeadlines, skipCurrentYearDeadlines))
			{
                writeNullsBasedOnPreferences();
                this.Model.Annual_Settings_Updated__c = System.today();
                
                Boolean datesValid = validateDate();
                if (!datesValid)
                {
                    errorLevel = 'WARNING';
                    String accYearStart = accYear.Name.split('-', 2)[0].trim();
                    messageName = 'Reminder dates cannot be after 12/31/' + accYearStart;
                    showMessage();
                    return null;
                }

                Decimal annSettingTreshold;
                School_Portal_Settings_2__mdt portalSettings = SchoolPortalSettings.Record;
                if (portalSettings != null)
                {
                    annSettingTreshold = SchoolPortalSettings.Annual_Setting_Async_Update_Threshold;
                }

                List<School_PFS_Assignment__c> spfsas = SchoolPfsAssignmentsSelector.Instance.selectBySchoolAndAcademicYear(
                    new Set<Id>{this.Model.School__c}, this.Model.Academic_Year_Name__c);
                
                if (portalSettings!= null && annSettingTreshold != null && annSettingTreshold < spfsas.size())
                {
                    AnnualSettingsUpdateBatch asuBatch = new AnnualSettingsUpdateBatch(new List<Annual_Setting__c>{this.Model});
                    Id batchProcessId = Database.executeBatch(asuBatch);

                    errorLevel = 'INFO';
                    messageName = Label.Annual_Setting_Async_Update;
                    sasBatchInProgress = true;
                }
                else
                {
                    upsert this.Model;

                    errorLevel = 'CONFIRM';
                    messageName = 'Saved';
                }

                this.ShowValidationMessage = false;
                showMessage();
            }
            else
            {
                this.ShowValidationMessage = true;
            }
        }
        catch(DmlException ex)
        {
            this.ShowValidationMessage = true;
        }
        catch(Exception ex)
        {
            ApexPages.addMessages(ex);
            this.ShowValidationMessage = false;
        }
        
        return null;
    }
    
    public void showMessage()
    {
        ApexPages.Message msg;
        
        if (errorLevel == 'WARNING')
        {
            msg = new ApexPages.Message(ApexPages.Severity.WARNING, messageName);
        }

        if (errorLevel == 'CONFIRM')
        {
            msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, messageName);
        }

        if (errorLevel == 'INFO')
        {
            msg = new ApexPages.Message(ApexPages.Severity.INFO, messageName);
        }

        ApexPages.addMessage(msg);
    }

    public boolean validateDate()
    {
        Integer intDate;
        List<String> dateFieldNames;
        Integer intAccYearStart = Integer.valueOf(accYearStart);
        
        Map<String, List<String>> checkboxAndDependentDateFields = new Map<String, List<String>>();
        
        //Section: PFS
        checkboxAndDependentDateFields.put('Send_PFS_Reminders__c', 
            new List<String>{'PFS_Reminder_Date__c','PFS_Reminder_Date_Returning__c'});
        
        //Section: Prior Prior Year(PPY) Docs
        checkboxAndDependentDateFields.put('Send_PP_Year_Document_Reminders__c', 
            new List<String>{'PPY_Document_Reminder_Date__c','PPY_Document_Reminder_Date_Returning__c'});
			
        //Section: Prior Year Docs
        checkboxAndDependentDateFields.put('Send_Prior_Year_Document_Reminders__c', 
            new List<String>{'Document_Reminder_Date__c','Document_Reminder_Date_Returning__c'});
        
        //Section: Current Year Docs
        checkboxAndDependentDateFields.put('Send_Current_Year_Document_Reminders__c', 
            new List<String>{'Curr_Year_Tax_Docs_Reminder_Date_New__c','Curr_Yr_Tax_Docs_Remind_Date_Returning__c'});
        
        
        for (String sendReminderCheckbox : checkboxAndDependentDateFields.keyset()) {
            
            if (this.Model.get(sendReminderCheckbox) == true) {
                
                dateFieldNames = checkboxAndDependentDateFields.get(sendReminderCheckbox);
                
                for (String fieldName : dateFieldNames) {
                    
                    if (this.Model.get(fieldName) != null) {
                    
                        intDate = Integer.valueOf(((Date) this.Model.get(fieldName)).year());
                        system.debug(intAccYearStart + ' (intAccYearStart) < ' + intDate);
                        if (intAccYearStart < intDate) {
                            return false;
                        }
                    }
                    
                }
            }
        }
        return true;
    }

    public PageReference Cancel() 
    {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Cancelled.'));
        this.LoadAnnualSetting();
        
        return null;
    }
    
    public PageReference CopyPKSchools() 
    {        
        return null;        
    }

    public PageReference tuitionSchedule_onchange() 
    {
        getGradesList(this.Model);//NAIS-2435
        isAvailableCopyTuitionCost = getAvailabletoCopyTuitionCost(); 
        return null;
    }
    
    public PageReference applyWNoTuition() {
        ApplyToStudents(true);
        return null;
    }

    public PageReference applyAll() {
        ApplyToStudents(false);
        return null;
    }

    private void ApplyToStudents(Boolean bNoTuition) {
        
        // Update Annual Settings.
        putGradesList();
        this.Model.Tuition_and_Fees_Updated__c = System.today();
        update this.Model;
        
        // Apply to Student_Folders?
        //Id schoolId = GlobalVariables.getCurrentUser().Contact.AccountId;
        Id schoolId = GlobalVariables.getCurrentSchoolId();
        Map<Id, Student_Folder__c> folders = new Map<Id, Student_Folder__c>();
        List<School_PFS_Assignment__c> spaRecords;
        
        if (bNoTuition)
        {
            spaRecords = [
                SELECT Id, Student_Folder__c, Day_Boarding__c, Grade_Applying__c
                FROM School_PFS_Assignment__c
                WHERE School__c = :schoolId
                    AND Academic_Year_Picklist__c = :accYear.Name 
                    AND (Student_Folder__r.Student_Tuition__c = null 
                    OR Student_Folder__r.Student_Tuition__c = 0)];
        }
        else
        {
            spaRecords = [
                SELECT Id, Student_Folder__c, Day_Boarding__c, Grade_Applying__c
                FROM School_PFS_Assignment__c
                WHERE School__c = :schoolId
                    AND Academic_Year_Picklist__c = :accYear.Name];
        }
        
        for (School_PFS_Assignment__c spa : spaRecords) {

            // Select the right grade.
            Integer iGrade = 0;
            
            if (spa.Grade_Applying__c != null && bOneSchedulePerGrade)
                iGrade = gradeMap.get(spa.Grade_Applying__c);
            
            if (iGrade == null) { continue; }//To avoid exception: Null list index 

            Student_Folder__c folder;
            if (bDay && ((spa.Day_Boarding__c == DB_DAY) || (spa.Day_Boarding__c == null)))
            {
                folder = new Student_Folder__c(
                    Id = spa.Student_Folder__c,
                    Student_Tuition__c = grades[iGrade].Day_Tuition,
                    Travel_Expenses__c = grades[iGrade].Day_Travel_Expense,
                    Student_Fees__c = grades[iGrade].Day_Fees,
                    New_Student_Expenses__c = grades[iGrade].Day_New_Student_Expense,
                    Returning_Student_Expenses__c = grades[iGrade].Day_Returning_Student_Expense
                );
                
                folders.put(spa.Student_Folder__c, folder);
            }
            else if (bBoarding && spa.Day_Boarding__c == DB_BOARDING)
            {
                folder = new Student_Folder__c(
                    Id = spa.Student_Folder__c,
                    Student_Tuition__c = grades[iGrade].Boarding_Tuition,
                    Travel_Expenses__c = grades[iGrade].Boarding_Travel_Expense,
                    Student_Fees__c = grades[iGrade].Boarding_Fees,
                    New_Student_Expenses__c = grades[iGrade].Boarding_New_Student_Expense,
                    Returning_Student_Expenses__c = grades[iGrade].Boarding_Returning_Student_Expense
                );
                
                folders.put(spa.Student_Folder__c, folder);

            }
        }

        update folders.values();
        
        if(!ApexPages.hasMessages(ApexPages.Severity.Error)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Saved.'));
        }
    }

    public Decimal defaultForNull(Decimal value)
    {
        if (value == null)
        {
            return 0;
        }
        
        return value;
    }
    
    public List<Grade> getGradesList(Annual_Setting__c annualSetting) { //NAIS-2435
        
        grades = new List<Grade>();
        
        if(bOneSchedulePerGrade){
            for (Integer i = 0; i < GradeFieldNames.size(); i++)
            {
                Grade myGrade = new Grade();
                myGrade.Name = GradeNames[i];
                
                myGrade.Day_Fees = defaultForNull((Decimal)annualSetting.get(GradeFieldNames[i] + '_Day_Fees__c'));
                myGrade.Day_New_Student_Expense = defaultForNull((Decimal)annualSetting.get(GradeFieldNames[i] + '_Day_New_Student_Expense__c'));
                myGrade.Day_Returning_Student_Expense = defaultForNull((Decimal)annualSetting.get(GradeFieldNames[i] + '_Day_Ret_Student_Expense__c'));
                myGrade.Day_Travel_Expense = defaultForNull((Decimal)annualSetting.get(GradeFieldNames[i] + '_Day_Travel_Expense__c'));
                myGrade.Day_Tuition = defaultForNull((Decimal)annualSetting.get(GradeFieldNames[i] + '_Day_Tuition__c'));
                myGrade.Boarding_Fees = defaultForNull((Decimal)annualSetting.get(GradeFieldNames[i] + '_Boarding_Fees__c'));
                myGrade.Boarding_New_Student_Expense = defaultForNull((Decimal)annualSetting.get(GradeFieldNames[i] + '_Boarding_New_Student_Expense__c'));
                myGrade.Boarding_Returning_Student_Expense = defaultForNull((Decimal)annualSetting.get(GradeFieldNames[i] + '_Boarding_Ret_Student_Expense__c'));
                myGrade.Boarding_Travel_Expense = defaultForNull((Decimal)annualSetting.get(GradeFieldNames[i] + '_Boarding_Travel_Expense__c'));
                myGrade.Boarding_Tuition = defaultForNull((Decimal)annualSetting.get(GradeFieldNames[i] + '_Boarding_Tuition__c'));
            
                grades.add(myGrade);
            }   
        }
        else{
            
            Grade myGrade = new Grade();
            myGrade.Name = 'One Schedule';

            myGrade.Boarding_Fees = defaultForNull((Decimal)annualSetting.One_Schedule_Boarding_Fees__c);
            myGrade.Boarding_New_Student_Expense = defaultForNull((Decimal)annualSetting.One_Sched_Boarding_New_Student_Expense__c);
            myGrade.Boarding_Returning_Student_Expense = defaultForNull((Decimal)annualSetting.One_Sched_Boarding_Ret_Student_Expense__c);
            myGrade.Boarding_Travel_Expense = defaultForNull((Decimal)annualSetting.One_Schedule_Boarding_Travel_Expense__c);
            myGrade.Boarding_Tuition = defaultForNull((Decimal)annualSetting.One_Schedule_Boarding_Tuition__c);
            myGrade.Day_Fees = defaultForNull((Decimal)annualSetting.One_Schedule_Day_Fees__c);
            myGrade.Day_New_Student_Expense = defaultForNull((Decimal)annualSetting.One_Schedule_Day_New_Student_Expense__c);
            myGrade.Day_Returning_Student_Expense = defaultForNull((Decimal)annualSetting.One_Schedule_Day_Ret_Student_Expense__c);
            myGrade.Day_Travel_Expense = defaultForNull((Decimal)annualSetting.One_Schedule_Day_Travel_Expense__c);
            myGrade.Day_Tuition = defaultForNull((Decimal)annualSetting.One_Schedule_Day_Tuition__c);
            
            grades.add(myGrade);
        }
        
        return grades;
    }
    
    private void putGradesList()
    {
        // If there is one row per grade
        if (bOneSchedulePerGrade) 
        {
            for (Integer i = 0; i < GradeFieldNames.size(); i++)
            {
                if (bDay)
                {
                    this.Model.put(GradeFieldNames[i] + '_Day_Fees__c', grades[i].Day_Fees.setScale(0));
                    this.Model.put(GradeFieldNames[i] + '_Day_New_Student_Expense__c', grades[i].Day_New_Student_Expense.setScale(0));
                    this.Model.put(GradeFieldNames[i] + '_Day_Ret_Student_Expense__c', grades[i].Day_Returning_Student_Expense.setScale(0));
                    this.Model.put(GradeFieldNames[i] + '_Day_Travel_Expense__c', grades[i].Day_Travel_Expense.setScale(0));
                    this.Model.put(GradeFieldNames[i] + '_Day_Tuition__c', grades[i].Day_Tuition.setScale(0));
                }
                    
                if (bBoarding)
                {
                    this.Model.put(GradeFieldNames[i] + '_Boarding_Fees__c', grades[i].Boarding_Fees.setScale(0));
                    this.Model.put(GradeFieldNames[i] + '_Boarding_New_Student_Expense__c', grades[i].Boarding_New_Student_Expense.setScale(0));
                    this.Model.put(GradeFieldNames[i] + '_Boarding_Ret_Student_Expense__c', grades[i].Boarding_Returning_Student_Expense.setScale(0));
                    this.Model.put(GradeFieldNames[i] + '_Boarding_Travel_Expense__c', grades[i].Boarding_Travel_Expense.setScale(0));
                    this.Model.put(GradeFieldNames[i] + '_Boarding_Tuition__c', grades[i].Boarding_Tuition.setScale(0));
                }
            }
        } 
        else // If there is only one row that covers all of the grades 
        {
            // Copy the first row into AnnualSettings and to all the others in our own list.
            for (Integer i = 0; i < GradeFieldNames.size(); i++)
            {               
                if (bDay)
                {
                    this.Model.One_Schedule_Day_Fees__c = grades[0].Day_Fees;
                    this.Model.One_Schedule_Day_New_Student_Expense__c = grades[0].Day_New_Student_Expense;
                    this.Model.One_Schedule_Day_Ret_Student_Expense__c = grades[0].Day_Returning_Student_Expense;
                    this.Model.One_Schedule_Day_Travel_Expense__c = grades[0].Day_Travel_Expense;
                    this.Model.One_Schedule_Day_Tuition__c = grades[0].Day_Tuition;
                    
                    // First one won't do anything. We could move setScale outside the loop but code will change.
                    grades[0].Day_Fees = grades[0].Day_Fees.setScale(0);
                    grades[0].Day_New_Student_Expense = grades[0].Day_New_Student_Expense.setScale(0);
                    grades[0].Day_Returning_Student_Expense = grades[0].Day_Returning_Student_Expense.setScale(0);
                    grades[0].Day_Travel_Expense = grades[0].Day_Travel_Expense.setScale(0);
                    grades[0].Day_Tuition = grades[0].Day_Tuition.setScale(0);
                }

                if (bBoarding)
                {
                    this.Model.One_Schedule_Boarding_Fees__c = grades[0].Boarding_Fees;
                    this.Model.One_Sched_Boarding_New_Student_Expense__c = grades[0].Boarding_New_Student_Expense;
                    this.Model.One_Sched_Boarding_Ret_Student_Expense__c = grades[0].Boarding_New_Student_Expense;
                    this.Model.One_Schedule_Boarding_Travel_Expense__c = grades[0].Boarding_Travel_Expense;
                    this.Model.One_Schedule_Boarding_Tuition__c = grades[0].Boarding_Tuition;
                    
                    grades[0].Boarding_Fees = grades[0].Boarding_Fees;
                    grades[0].Boarding_New_Student_Expense = grades[0].Boarding_New_Student_Expense.setScale(0);
                    grades[0].Boarding_Returning_Student_Expense = grades[0].Boarding_Returning_Student_Expense.setScale(0);
                    grades[0].Boarding_Travel_Expense = grades[0].Boarding_Travel_Expense.setScale(0);
                    grades[0].Boarding_Tuition = grades[0].Boarding_Tuition.setScale(0);
                }
            }   
        }
    }

    @testVisible private void writeNullsBasedOnPreferences()
    {
        if (this.Model.Send_PFS_Reminders__c != true) {
            
            if (this.Model.Send_PFS_Reminders__c == null) {
                this.Model.Send_PFS_Reminders__c = false;
            }
            this.Model.PFS_Reminder_Date__c = null;
            this.Model.PFS_Reminder_Date_Returning__c = null;
        }
        
        if (this.Model.Send_PP_Year_Document_Reminders__c != true) {
            
            if (this.Model.Send_PP_Year_Document_Reminders__c == null) {
                this.Model.Send_PP_Year_Document_Reminders__c = false;
            }
            
            this.Model.PPY_Document_Reminder_Date__c = null;
            this.Model.PPY_Document_Reminder_Date_Returning__c = null;
        }
        
        if (this.Model.Send_Prior_Year_Document_Reminders__c != true) {
            
            if (this.Model.Send_Prior_Year_Document_Reminders__c == null) {
                this.Model.Send_Prior_Year_Document_Reminders__c = false;
            }
            
            this.Model.Document_Reminder_Date__c = null;
            this.Model.Document_Reminder_Date_Returning__c = null;
        }
        
        if (this.Model.Send_Current_Year_Document_Reminders__c != true) {
            
            if (this.Model.Send_Current_Year_Document_Reminders__c == null) {
                this.Model.Send_Current_Year_Document_Reminders__c = false;
            }
            
            this.Model.Curr_Year_Tax_Docs_Reminder_Date_New__c = null;
            this.Model.Curr_Yr_Tax_Docs_Remind_Date_Returning__c = null; 
        }
    }
    /*End Helper Methods*/
    
    /* Internal Class */
    public class Grade {
        
        public String Name { get; set; }
        
        public Decimal Boarding_Fees { get; set; }
        public Decimal Boarding_New_Student_Expense { get; set; }
        public Decimal Boarding_Returning_Student_Expense { get; set; }
        public Decimal Boarding_Travel_Expense { get; set; }
        public Decimal Boarding_Tuition { get; set; }
        
        public Decimal Day_Fees { get; set; }
        public Decimal Day_New_Student_Expense { get; set; }
        public Decimal Day_Returning_Student_Expense { get; set; }
        public Decimal Day_Travel_Expense { get; set; }
        public Decimal Day_Tuition { get; set; }
    }
    /* End Internal Class */
    
    /*SchoolAcademicYearSelectorInterface Implementation*/
    
    public String newAcademicYearId;
    
    public String getAcademicYearId() {
        return this.Model.Academic_Year__c;
    }
    
    public void setAcademicYearId(String academicYearId) {
        newAcademicYearId = academicYearId;
    } 
    
    public void VerificationCopyValues() {   
        String verificationValue = ApexPages.currentPage().getParameters().get('verificationToCompare');
        String fieldToClear = ApexPages.currentPage().getParameters().get('fieldToClear');   
        if (Model.get(verificationValue) == 'No'){
            Model.put(fieldToClear, null);
        }
        if (Model.get(verificationValue) == 'Yes' && Model.get(fieldToClear) != 'Current Year'){
            Model.put(fieldToClear, 'Current Year');
        }
    }
    
    public PageReference SchoolAcademicYearSelector_OnChange(Boolean saveRecord) {
        
        String pageName = '/apex/'+SchoolPagesUtils.getPageName(ApexPages.currentPage());
        PageReference newPage = new PageReference(pageName);
        
        newPage.getParameters().put('academicyearid', newAcademicYearId);
        newPage.setRedirect(true);
        
        return newPage;
    }
    
    /*End SchoolAcademicYearSelectorInterface Implementation*/

    /**
     * @description Determines whether or not certain fields on the annual setting should be read only based on whether
     *              or not the family portal has opened. If the family portal has opened for the annual setting's
     *              academic year, then document verification details will be read only.
     * @return True if the family portal has opened.
     */
    public Boolean getShouldLockAnnualSettingUpdates() {
        Id annualSettingAcademicYearId = this.Model.Academic_Year__c;

        return AcademicYearService.Instance.hasFamilyPortalOpened(annualSettingAcademicYearId);
    }
    
    
    public PageReference setVisitedCookie() {
        Cookie isVisitedAnnualSettings = ApexPages.currentPage().getCookies().get('AnnualSettingsVisited');
        
        if (isVisitedAnnualSettings == null) {
            isVisitedAnnualSettings = new Cookie('AnnualSettingsVisited', 'true', null, -1, false);
            
            ApexPages.currentPage().setCookies(new Cookie[]{ isVisitedAnnualSettings });
        }
        
        
        PageReference pr = Page.SchoolDashboard;
        pr.setRedirect(true);
        
        return pr;
    }
    
    public PageReference clearReminderDatesIfMasterCheckboxIsFalse() {
        
        String fieldToCompare = ApexPages.currentPage().getParameters().get('fieldToCompare');
        String fieldToClear1 = ApexPages.currentPage().getParameters().get('fieldToClear1');
        String fieldToClear2 = ApexPages.currentPage().getParameters().get('fieldToClear2');
        
        if (Model.get(fieldToCompare) == false) {
            Model.put(fieldToClear1, null);
            Model.put(fieldToClear2, null);
        }
        
        return null;
    }
    
    public PageReference resetCheckbox() {
        
        String fieldToClear = ApexPages.currentPage().getParameters().get('fieldToClear');
        String fieldToCompare = ApexPages.currentPage().getParameters().get('fieldToCompare');
        
        if (Model.get(fieldToCompare) != 'Hard Deadline' && Model.get(fieldToCompare) != 'Soft Deadline') {
            Model.put(fieldToClear, false);
        }
        
        return null;
    }
		
}