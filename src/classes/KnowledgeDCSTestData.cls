/**
 * @description This class is used to create KnowledgeDataCategorySelection records for unit tests.
 */
@isTest
public class KnowledgeDCSTestData extends SObjectTestData {

    private static final String KDCS_DC_GROUP_NAME_AUDIENCE = 'Audience';
    private static final String KDCS_DC_NAME_DOCUMENTS = 'Documents';
    
    /**
     * @description Get the default values for the KnowledgeDataCategorySelection object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
            Knowledge__DataCategorySelection.DataCategoryGroupName => KDCS_DC_GROUP_NAME_AUDIENCE,
            Knowledge__DataCategorySelection.DataCategoryName => KDCS_DC_NAME_DOCUMENTS
        };
    }

    /**
     * @description Set the DataCategoryName of the KnowledgeDataCategorySelection record.
     * @param dataCategoryName The DataCategoryName to set the current KnowledgeDataCategorySelection record to.
     * @return The current instance of KnowledgeDCSTestData.
     */
    public KnowledgeDCSTestData forDataCategoryName(String dataCategoryName) {
        return (KnowledgeDCSTestData) with(Knowledge__DataCategorySelection.DataCategoryName, dataCategoryName);
    }

    /**
     * @description Set the ParentId of the KnowledgeDataCategorySelection record.
     * @param parentId The ParentId to set the current KnowledgeDataCategorySelection record to.
     * @return The current instance of KnowledgeDCSTestData.
     */
    public KnowledgeDCSTestData forParentId(String parentId) {
        return (KnowledgeDCSTestData) with(Knowledge__DataCategorySelection.ParentId, parentId);
    }
    
    /**
     * @description Insert the current working KnowledgeDataCategorySelection record.
     * @return The currently operated upon KnowledgeDataCategorySelection record.
     */
    public Knowledge__DataCategorySelection insertKnowledgeDCS() {
        return (Knowledge__DataCategorySelection)insertRecord();
    }

    /**
     * @description Inserts a list of Knowledge__DataCategorySelection and ties into the before and after hooks.
     * @param numToInsert the number of Knowledge__DataCategorySelection to insert.
     * @return The inserted SObjects.
     */
    public List<Knowledge__DataCategorySelection> insertKnowledgeDCSs(Integer numToInsert) {
        return (List<Knowledge__DataCategorySelection>)insertRecords(numToInsert);
    }

    /**
     * @description Create the current working KnowledgeDataCategorySelection record without resetting
     *             the stored values in this instance of KnowledgeDCSTestData.
     * @return A non-inserted KnowledgeDataCategorySelection record using the currently stored field
     *             values.
     */
    public Knowledge__DataCategorySelection createKnowledgeDCSWithoutReset() {
        return (Knowledge__DataCategorySelection)buildWithoutReset();
    }

    /**
     * @description Create the current working KnowledgeDataCategorySelection record.
     * @return The currently operated upon KnowledgeDataCategorySelection record.
     */
    public Knowledge__DataCategorySelection create() {
        return (Knowledge__DataCategorySelection)super.buildWithReset();
    }

    /**
     * @description The default KnowledgeDataCategorySelection record.
     */
    public Knowledge__DataCategorySelection DefaultKnowledgeDCS {
        get {
            if (DefaultKnowledgeDCS == null) {
                DefaultKnowledgeDCS = createKnowledgeDCSWithoutReset();
                insert DefaultKnowledgeDCS;
            }
            return DefaultKnowledgeDCS;
        }
        private set;
    }

    /**
     * @description Get the Knowledge__DataCategorySelection SObjectType.
     * @return The Knowledge__DataCategorySelection SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Knowledge__DataCategorySelection.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static KnowledgeDCSTestData Instance {
        get {
            if (Instance == null) {
                Instance = new KnowledgeDCSTestData();
            }
            return Instance;
        }
        private set;
    }

    private KnowledgeDCSTestData() { }
}