public class FamilyAppDependentsCont extends FamilyAppCompController
{

    public Integer dependentNumber {get; set;}

    public void addDependent()
    {
        if (pfs.Number_of_Dependent_Children__c == null)
        {
            pfs.Number_of_Dependent_Children__c = 1;
        }
        else
        {
            pfs.Number_of_Dependent_Children__c += 1;
        }

        dependentItem.Number_of_Dependents__c = pfs.Number_of_Dependent_Children__c;
    }

    public void deleteDependent() 
    {
        if (dependentNumber < 7)
        {
            if (dependentItem.Number_of_Dependents__c == 7 && String.isBlank(dependentItem.Additional_Dependents__c))
            {
                pfs.Number_of_Dependent_Children__c -= 1;
                dependentItem.Number_of_Dependents__c -= 1;
                dependentItem.Additional_Dependents__c = null;
            }
            
            dependentItem = ApplicationUtils.removeDependentData(dependentItem, dependentNumber);
            ApplicationUtils.moveDependents(dependentItem, false);
            dependentItem.Number_of_Dependents__c = pfs.Number_of_Dependent_Children__c;
        }
        else
        {
            dependentItem.Additional_Dependents__c = null;    
        }
        
        pfs.Number_of_Dependent_Children__c -= 1;
        dependentItem.Number_of_Dependents__c -= 1;
    }
}