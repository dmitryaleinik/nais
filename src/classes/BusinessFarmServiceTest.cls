@isTest
private class BusinessFarmServiceTest {
    @isTest
    private static void fixNullSequenceNumbers_nullParam_expectArgumentNullException() {
        try {
            BusinessFarmService.Instance.fixNullSequenceNumbers((Business_Farm__c)null);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, BusinessFarmService.BUSINESS_FARM_PARAM);
        }

        try {
            BusinessFarmService.Instance.fixNullSequenceNumbers((List<Business_Farm__c>)null);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, BusinessFarmService.BUSINESS_FARMS_PARAM);
        }
    }

    @isTest
    private static void fixNullSequenceNumbers_nullSequenceNumber_expectSequenceUpdated() {
        Business_Farm__c businessFarm = BusinessFarmTestData.Instance.forSequenceNumber(null).insertBusinessFarm();
        System.assertEquals(null, businessFarm.Sequence_Number__c, 'Expected the Sequence Number to be null.');

        Test.startTest();
        BusinessFarmService.Instance.fixNullSequenceNumbers(businessFarm);
        Test.stopTest();

        Business_Farm__c updatedBusinessFarm =
                [SELECT Id, Sequence_Number__c FROM Business_Farm__c WHERE Id = :businessFarm.Id];

        System.assertEquals(1, updatedBusinessFarm.Sequence_Number__c, 'Expected the Sequence Number to be updated.');
    }

    private class BusinessSummaryExpectations {

        public Integer numberOfBusinesses;
        public Integer numberOfFarms;
        public Integer numberOfPartnershipsAndCorps;
        public Decimal origBusinessProfits;
        public Decimal revisedBusinessProfits;
        public Decimal origFarmProfits;
        public Decimal revisedFarmProfits;
        public Decimal origPartnershipProfits;
        public Decimal revisedPartnershipProfits;
    }

    private static void test_getBusinessSummary(String academicYearToTest, Decimal individualBusinessNetProfit, BusinessSummaryExpectations expectations) {
        Academic_Year__c academicYear = AcademicYearTestData.Instance.forName(academicYearToTest).DefaultAcademicYear;

        PFS__c pfs = PfsTestData.Instance.forAcademicYearPicklist(academicYearToTest).DefaultPfs;

        School_PFS_Assignment__c pfsAssignment = SchoolPfsAssignmentTestData.Instance.forAcademicYearPicklist(academicYearToTest).DefaultSchoolPfsAssignment;

        List<String> businessEntityTypes = new List<String> { 'Corporation', 'Sole Proprietorship', 'Partnership' };

        // Create businesses
        List<Business_Farm__c> businessesAndFarms = new List<Business_Farm__c>();
        for (String entityType : businessEntityTypes) {
            businessesAndFarms.add(BusinessFarmTestData.Instance.forBusinessOrFarm('Business').forBusinessEntityType(entityType).withGrossSales(individualBusinessNetProfit).withNetProfits(individualBusinessNetProfit).create());
        }

        // Create farms
        for (String entityType : businessEntityTypes) {
            businessesAndFarms.add(BusinessFarmTestData.Instance.forBusinessOrFarm('Farm').forBusinessEntityType(entityType).withGrossSales(individualBusinessNetProfit).withNetProfits(individualBusinessNetProfit).create());
        }

        insert businessesAndFarms;

        List<School_Biz_Farm_Assignment__c> businessFarmAssignments = [SELECT Id, School_PFS_Assignment__c FROM School_Biz_Farm_Assignment__c WHERE School_PFS_Assignment__c = :pfsAssignment.Id];
        System.assertEquals(businessesAndFarms.size(), businessFarmAssignments.size(), 'Expected all businesses and farms to be linked to the pfs assignment.');

        for (School_Biz_Farm_Assignment__c sbfa : businessFarmAssignments) {
            System.assertEquals(pfsAssignment.Id, sbfa.School_PFS_Assignment__c,
                    'All the business farm assignments should have the same PFS Assignment.');
        }

        pfsAssignment = [SELECT Id, Academic_Year_Picklist__c, (SELECT Id, Business_Entity_Type__c, Orig_Business_Entity_Type__c, Academic_Year__c, Net_Profit_Loss_Business_Farm__c, Orig_Net_Profit_Loss_Business_Farm__c, Business_Farm__c, Business_Farm__r.Business_or_Farm__c, Business_Farm__r.Business_Entity_Type__c FROM School_Biz_Farm_Assignments__r) FROM School_PFS_Assignment__c WHERE Id = :pfsAssignment.Id LIMIT 1];

        BusinessFarmService.BusinessSummary summary = BusinessFarmService.Instance.getBusinessSummary(pfsAssignment);

        System.assertEquals(expectations.numberOfBusinesses, summary.Businesses.size(), 'Expected the correct number of SBFAs categorized as businesses.');
        System.assertEquals(expectations.origBusinessProfits, summary.BusinessNetProfit_Original, 'Expected the correct sum of original profits for businesses.');
        System.assertEquals(expectations.revisedBusinessProfits, summary.BusinessNetProfit_Revised, 'Expected the correct sum of revised profits for businesses.');

        System.assertEquals(expectations.numberOfFarms, summary.Farms.size(), 'Expected the correct number of SBFAs categorized as farms.');
        System.assertEquals(expectations.origFarmProfits, summary.FarmNetProfit_Original, 'Expected the correct sum of original profits for farms.');
        System.assertEquals(expectations.revisedFarmProfits, summary.FarmNetProfit_Revised, 'Expected the correct sum of revised profits for farms.');

        System.assertEquals(expectations.numberOfPartnershipsAndCorps, summary.PartnershipsAndSCorporations.size(), 'Expected the correct number of SBFAs categorized as partnerships/corporations.');
        System.assertEquals(expectations.origPartnershipProfits, summary.SCorpPartnershipNetProfit_Original, 'Expected the correct sum of original profits for S-Corporations/Partnerships.');
        System.assertEquals(expectations.revisedPartnershipProfits, summary.SCorpPartnershipNetProfit_Revised, 'Expected the correct sum of revised profits for S-Corporations/Partnerships.');
    }

    @isTest
    private static void getBusinessSummary_pfsAssignmentHas3BusinessesAnd3Farms_oneOfEachBusinessEntityType_excludePartnershipsDisabled_expectPartnershipsNotSeparated() {
        FeatureToggles.setToggle('Exclude_S_Corps_Partnerships_From_Sums__c', false);

        String academicYear = '2018-2019';
        Decimal netProfit = 25000;

        BusinessSummaryExpectations expectations = new BusinessSummaryExpectations();
        expectations.numberOfBusinesses = 3;
        expectations.numberOfFarms = 3;
        expectations.numberOfPartnershipsAndCorps = 0;
        expectations.origBusinessProfits = netProfit * expectations.numberOfBusinesses;
        expectations.revisedBusinessProfits = netProfit * expectations.numberOfBusinesses;
        expectations.origFarmProfits = netProfit * expectations.numberOfFarms;
        expectations.revisedFarmProfits = netProfit * expectations.numberOfFarms;
        expectations.origPartnershipProfits = netProfit * expectations.numberOfPartnershipsAndCorps;
        expectations.revisedPartnershipProfits = netProfit * expectations.numberOfPartnershipsAndCorps;

        test_getBusinessSummary(academicYear, netProfit, expectations);
    }

    @isTest
    private static void getBusinessSummary_pfsAssignmentHas3BusinessesAnd3Farms_oneOfEachBusinessEntityType_academicYearBefore2018_2019_expectPartnershipsNotSeparated() {
        FeatureToggles.setToggle('Exclude_S_Corps_Partnerships_From_Sums__c', true);

        String academicYear = '2017-2018';
        Decimal netProfit = 25000;

        BusinessSummaryExpectations expectations = new BusinessSummaryExpectations();
        expectations.numberOfBusinesses = 3;
        expectations.numberOfFarms = 3;
        expectations.numberOfPartnershipsAndCorps = 0;
        expectations.origBusinessProfits = netProfit * expectations.numberOfBusinesses;
        expectations.revisedBusinessProfits = netProfit * expectations.numberOfBusinesses;
        expectations.origFarmProfits = netProfit * expectations.numberOfFarms;
        expectations.revisedFarmProfits = netProfit * expectations.numberOfFarms;
        expectations.origPartnershipProfits = netProfit * expectations.numberOfPartnershipsAndCorps;
        expectations.revisedPartnershipProfits = netProfit * expectations.numberOfPartnershipsAndCorps;

        test_getBusinessSummary(academicYear, netProfit, expectations);
    }

    @isTest
    private static void getBusinessSummary_pfsAssignmentHas3BusinessesAnd3Farms_oneOfEachBusinessEntityType_excludePartnershipsEnabled_expectPartnershipsAndSCorpSeparated() {
        FeatureToggles.setToggle('Exclude_S_Corps_Partnerships_From_Sums__c', true);

        String academicYear = '2018-2019';
        Decimal netProfit = 25000;

        BusinessSummaryExpectations expectations = new BusinessSummaryExpectations();
        expectations.numberOfBusinesses = 1;
        expectations.numberOfFarms = 1;
        expectations.numberOfPartnershipsAndCorps = 4;
        expectations.origBusinessProfits = netProfit * expectations.numberOfBusinesses;
        expectations.revisedBusinessProfits = netProfit * expectations.numberOfBusinesses;
        expectations.origFarmProfits = netProfit * expectations.numberOfFarms;
        expectations.revisedFarmProfits = netProfit * expectations.numberOfFarms;
        expectations.origPartnershipProfits = netProfit * expectations.numberOfPartnershipsAndCorps;
        expectations.revisedPartnershipProfits = netProfit * expectations.numberOfPartnershipsAndCorps;

        test_getBusinessSummary(academicYear, netProfit, expectations);
    }
}