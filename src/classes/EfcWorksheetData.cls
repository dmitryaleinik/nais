/**
 * Spec-017 EFC Calculation
 *
 * Data structure used to pass around EFC inputs and calculated values
 *
 * SL, Exponent Partners, 2013
 **/
public class EfcWorksheetData {
    /* Inner class to hold Applicant Data */
    public class ApplicantData {
        public Id sourceApplicantId { get; set; }
        public String applicantName { get; set; }
        public String gradeInEntryYear { get; set; }
        public Decimal assetsTotalValue { get; set; }
        public Decimal studentAssetContribution { get; set; }
        public Decimal efcDay { get; set; }
        public Decimal efcBoarding { get; set; }
        public String dayOrBoarding { get; set; }
        public Decimal estimatedFamilyContribution { get; set; }
    }

    /* Worksheet Data */
    // Source records
    public Id sourcePfsId { get; set; }
    public Id sourceSchoolPfsAssignmentId { get; set; }
    public List<Id> sourceBizFarmIds { get; set; } // NAIS-2500
    public List<Id> sourceSBFAIds { get; set; } // NAIS-2500

    /**
     * @description The Id of the applicant related to the School_PFS_Assignment__c record specified by the
     *              sourceSchoolPfsAssignmentId property.
     */
    public Id MainApplicantId { get; set; }

    // [CH] NAIS-1514 Error handling support
    public Boolean processingError { get; set; }
    // NAIS-2371 [CH] Capturing full error message
    public String processingErrorText { get; set; }

    // Academic Year
    public Id academicYearId { get; set; }

    // Applicant Data
    public List<ApplicantData> applicants { get; set; }

    // Taxable Income
    public Decimal salaryWagesParentA { get; set; }
    public Decimal salaryWagesParentB { get; set; }
    public Decimal dividendIncome { get; set; }
    public Decimal interestIncome { get; set; }
    public Decimal alimonyReceived { get; set; }
    public Decimal netProfitLossBusinessFarm { get; set; }

    public Decimal taxableRefunds { get; set; }
    public Decimal capitalGainLoss { get; set; }
    public Decimal otherGainsLosses { get; set; }
    public Decimal iraDistributions { get; set; }
    public Decimal pensionsAndAnnuities { get; set; }
    public Decimal rentalRealEstateTrustsEtc { get; set; }
    public Decimal unemploymentCompensation { get; set; }
    public Decimal socialSecurityBenefitsTaxable { get; set; }
    public Decimal otherIncome { get; set; }
    public Decimal deductiblePartOfSelfEmploymentTax { get; set; }
    public Decimal sepSimpleQualifiedPlans { get; set; }
    public Decimal untaxedIraPlanPayment { get; set; }
    public Decimal otherAdjustmentsToIncome { get; set; }

    // Non-Taxable Income
    public Decimal childSupportReceived { get; set; }
    public Decimal socialSecurityBenefits { get; set; }
    public Decimal preTaxPaymentsToRetirementPlans { get; set; }
    public Decimal preTaxFringeBenefitContribution { get; set; }
    public Decimal cashSupportGiftIncome { get; set; }
    public Decimal otherSupportFromDivorcedSpouse { get; set; }
    public Decimal foodHousingOtherAllowance { get; set; }
    public Decimal earnedIncomeCredits { get; set; }
    public Decimal welfareVeteransAndWorkersComp { get; set; }
    public Decimal taxExemptInvestmentIncome { get; set; }
    public Decimal incomeEarnedAbroad { get; set; }
    public Decimal otherUntaxedIncome { get; set; }
    public boolean pjApplyMinIncomeToNonTaxableIncome { get; set; }
    public Decimal pjMinimumIncomeAmount { get; set; }
    public Decimal pjMinimumIncomeImputed { get; set; }
    public boolean pjAddDeprecHomeBusExpense { get; set; }
    public Decimal depreciationSec179ExpSchC { get; set; }
    public Decimal depreciationScheduleE { get; set; }
    public Decimal depreciationSec179ExpSchF { get; set; }
    public Decimal depreciationSec179Exp4562 { get; set; }
    public Decimal homeBusinessExpenseScheduleC { get; set; }
    public Decimal totalNonTaxableIncome { get; set; }

    // [DP] 08.11.2016 SFP-149 now we always use the depreationHomeBusExpense, since it is overwriteable and pre-filled from tax forms
    //// NAIS-510
    //public Decimal depreciationHomeBusinessExpenseFromTaxForms { get; set; }

    // Total Income
    public Decimal totalIncome { get; set; }

    // Allowances
    public boolean useRevisedFederalTax { get; set; }
    public boolean useRevisedStateOtherTax { get; set; }
    public boolean useRevisedMedicareTax { get; set; }
    public boolean useRevisedSocialSecTax { get; set; }
    public String filingStatus { get; set; }
    public String filingStatusParentB { get; set; } //LR:SFP-352
    public Decimal incomeTaxExemptions { get; set; }
    public Decimal itemizedDeductions { get; set; }
    public Decimal federalIncomeTaxRevised { get; set; }
    public Decimal federalIncomeTaxCalculated { get; set; }
    public Decimal socialSecurityTaxAllowanceCalculated { get; set; }
    public Decimal socialSecurityTaxAllowanceRevised { get; set; } // [CH] NAIS-1860
    public Decimal medicareTaxAllowanceCalculated { get; set; }
    public Decimal medicareTaxAllowanceRevised { get; set; } // [CH] NAIS-1860
    public Decimal selfEmploymentTaxPaid { get; set; }
    public Decimal selfEmploymentTaxCalculated { get; set; } // [CH] NAIS-1850
    public Decimal selfEmploymentTaxAllowanceRemainder { get; set; }
    public String parentState { get; set; }
    public String parentCountry { get; set; } // [CH] NAIS-1464
    public Decimal stateOtherTaxAllowanceRevised { get; set; }
    public Decimal stateOtherTaxAllowanceCalculated { get; set; }
    public Decimal employmentAllowance { get; set; }
    public Decimal medicalDentalExpenses { get; set; }
    public Decimal medicalAllowance { get; set; }
    public Decimal unusualExpenses { get; set; }
    public Decimal totalAllowances { get; set; }

    // Effective Income
    public Decimal effectiveIncome { get; set; }

    // Income Supplement
    public boolean pjUseHomeEquity { get; set; }
    public boolean pjUseHousingIndexMultiplier { get; set; }
    public boolean pjUseHomeEquityCap { get; set; }
    public String homePurchaseYear { get; set; }
    public Decimal homePurchasePrice { get; set; }
    public Decimal homeMarketValue { get; set; }
    public Decimal unpaidPrincipal1stMortgage { get; set; }
    public Decimal unpaidPrincipal2ndMortgage { get; set; }
    public Decimal housingIndexMultiplier { get; set; }
    public Decimal housingIndexMultiplierHomeValue { get; set; }
    public Decimal homeEquityCap { get; set; }
    public Decimal homeEquityUncapped { get; set; }
    public Decimal homeEquity { get; set; }
    public Decimal otherRealEstateUnpaidMortgagePrincipal { get; set; }
    public Decimal otherRealEstateMarketValue { get; set; }
    public Decimal otherRealEstateEquity { get; set; }
    public String businessFarmOwner { get; set; }
    // NAIS-2500 [DP] 09.23.2015 now using ordered lists to account for multiple Biz/Farms
    //public Decimal businessFarmDebts { get; set; }
    //public Decimal businessFarmAssets { get; set; }
    //public Decimal businessFarmPercentOwnership { get; set; }
    public Decimal valueOfBusinessFarm { get; set; }
    public Decimal businessFarmShare { get; set; }
    public boolean pjUseDividendInterestIncomeToImputeAssets { get; set; }
    public Decimal pjPercentageForImputingAssets { get; set; }
    public Decimal imputedAssets { get; set; }
    public Decimal bankAccounts { get; set; }
    public Decimal otherInvestments { get; set; }
    public Decimal totalDebts { get; set; }
    public Decimal totalAssets { get; set; }
    public Decimal netWorth { get; set; }
    public boolean ownHome { get; set; } // [SL] NAIS-1016

    // NAIS-2500 one ordered entry for each business
    public List<Decimal> businessFarmDebtsList { get; set; } // NAIS-2500 [DP] 09.23.2015
    public List<Decimal> businessFarmAssetsList { get; set; } // NAIS-2500 [DP] 09.23.2015
    public List<Decimal> businessFarmPercentOwnershipList { get; set; } // NAIS-2500 [DP] 09.23.2015
    public List<Decimal> businessFarmValueList { get; set; } // NAIS-2500 [DP] 09.23.2015 for EFC output to BizFarm of SBFA
    public List<Decimal> businessFarmShareList { get; set; } // NAIS-2500 [DP] 09.23.2015 for EFC output to BizFarm of SBFA

    // Discretionary Income
    public String familyStatus { get; set; }
    public Integer ageParentA { get; set; }
    public Integer ageParentB { get; set; }
    public Decimal conversionCoefficient { get; set; }
    public Decimal retirementAllowance { get; set; }
    public Decimal discretionaryNetWorth { get; set; }
    public Decimal incomeSupplement { get; set; }
    public Decimal adjustedEffectiveIncome { get; set; }
    public boolean pjUseCostOfLivingAdjustment { get; set; }
    public Decimal pjOverrideDefaultColaValue { get; set; } // NAIS-523: change from boolean to actual cola value
    public Decimal pjColaValue { get; set; }
    public Decimal revisedAdjustedEffectiveIncome { get; set; }
    public Decimal revisedEffectiveIncome { get; set; }
    public Decimal familySize { get; set; }
    public String adjustHousingPortion { get; set; } // NAIS-1885: For calculating split value
    public Decimal ipaHousingFamilyOf4 { get; set; } // NAIS-1885: For calculating split value
    public Decimal incomeProtectionAllowanceHousingOrig { get; set; } // NAIS-1885: For calculating split value
    public Decimal incomeProtectionAllowanceHousing { get; set; } // NAIS-1885: For calculating split value
    public Decimal incomeProtectionAllowanceHousingCalc { get; set; } // NAIS-1885: For calculating split value
    public Decimal incomeProtectionAllowanceOther { get; set; } // NAIS-1885: For calculating split value
    public Decimal incomeProtectionAllowance { get; set; }
    public Decimal discretionaryIncome { get; set; }
    public Decimal discretionaryIncomeNoIncomeSupplement { get; set; }

    // contribution
    public Decimal estimatedParentalContribution { get; set; }
    public Decimal estimatedParentalContributionBoarding { get; set; }
    public Decimal numChildrenInTuitionSchools { get; set; }
    public Decimal estimatedParentalContributionPerChild { get; set; }
    public Decimal estimatedParentalContributionPerChildBoarding { get; set; }
    public Decimal contribPercentTotalIncome { get; set; }
    public Decimal contribFromIncome { get; set; }
    public Decimal contribPercentDiscretionaryIncome { get; set; }
    public Decimal contribFromAssets { get; set; }

    // calculated values
    public Decimal dividendInterestIncome { get; set; }
    public Decimal otherTaxableIncome { get; set; }
    public Decimal totalTaxableIncome { get; set; }
    public Decimal otherNonTaxableIncome { get; set; }
    public Decimal untaxedIraSepSimplePayments { get; set; }
    public Decimal businessFarmDepreciation { get; set; }
    public Decimal totalDepreciation { get; set; }
    public Decimal depreciationHomeBusinessExpense { get; set; }
    public Decimal adjustmentsToIncome { get; set; }

    // if true, then recalculate the value, otherwise just use the input value
    public boolean calculateAdjustmentsToIncome { get; set; }

    // [SL] NAIS-1007
    public boolean calculateFamilySize { get; set; } // if true, then recalculate the value, otherwise just use the input value
    public Decimal numDependentChildren { get; set; }

    // [SL] NAIS-1011
    public boolean calculateNetProfitLossBusinessFarm { get; set; }
    public Decimal businessTotalIncome { get; set; }
    public Decimal businessTotalExpenses { get; set; }

    // convenience methods for accessing applicant data (these map to the first applicantData object)
    public String dayOrBoarding {
        get {
            if ((applicants != null) && (!applicants.isEmpty())) {
                return applicants[0].dayOrBoarding;
            }
            else {
                return null;
            }
        }
        set {
            if ((applicants != null) && (!applicants.isEmpty())) {
                applicants[0].dayOrBoarding = value;
            }
        }
    }
    public String gradeApplying {
        get {
            if ((applicants != null) && (!applicants.isEmpty())) {
                return applicants[0].gradeInEntryYear;
            }
            else {
                return null;
            }
        }
        set {
            if ((applicants != null) && (!applicants.isEmpty())) {
                applicants[0].gradeInEntryYear = value;
            }
        }
    }

    public Decimal studentAssets {
        get {
            if ((applicants != null) && (!applicants.isEmpty())) {
                return applicants[0].assetsTotalValue;
            }
            else {
                return null;
            }
        }
        set {
            if ((applicants != null) && (!applicants.isEmpty())) {
                applicants[0].assetsTotalValue = value;
            }
        }
    }

    public Decimal studentAssetContribution {
        get {
            if ((applicants != null) && (!applicants.isEmpty())) {
                return applicants[0].studentAssetContribution;
            }
            else {
                return null;
            }
        }
        set {
            if ((applicants != null) && (!applicants.isEmpty())) {
                applicants[0].studentAssetContribution = value;
            }
        }
    }

    public Decimal estimatedFamilyContributionDay {
        get {
            if ((applicants != null) && (!applicants.isEmpty())) {
                return applicants[0].efcDay;
            }
            else {
                return null;
            }
        }
        set {
            if ((applicants != null) && (!applicants.isEmpty())) {
                applicants[0].efcDay = value;
            }
        }
    }

    public Decimal estimatedFamilyContributionBoarding {
        get {
            if ((applicants != null) && (!applicants.isEmpty())) {
                return applicants[0].efcBoarding;
            }
            else {
                return null;
            }
        }
        set {
            if ((applicants != null) && (!applicants.isEmpty())) {
                applicants[0].efcBoarding = value;
            }
        }
    }

    public Decimal estimatedFamilyContribution {
        get {
            if ((applicants != null) && (!applicants.isEmpty())) {
                return applicants[0].estimatedFamilyContribution;
            }
            else {
                return null;
            }
        }
        set {
            if ((applicants != null) && (!applicants.isEmpty())) {
                applicants[0].estimatedFamilyContribution = value;
            }
        }
    }

    /**
     * @description Gets the main applicant from the list of all applicants related to the EfcWorksheetData. The main
     *              applicant is determined by the MainApplicantId property which is the Id of the applicant specified
     *              on the SPA.
     * @return ApplicantData instance representing the main applicant or null if none could be found.
     */
    private ApplicantData getMainApplicant() {
        if (MainApplicantId == null || applicants == null || applicants.isEmpty()) {
            return null;
        }

        for (ApplicantData applicant : applicants) {
            if (applicant.sourceApplicantId == MainApplicantId) {
                return applicant;
            }
        }

        return null;
    }

    /**
     * @description Determines whether or not the calculations are primarily based off of day school. To figure this
     *              out, we first look for the main applicant. The main applicant is determined by the School PFS
     *              Assignment. After getting the main applicant, we return true if the are attending day school.
     *
     *              If we are unable to find the main applicant, we will default to the first applicant. If the first
     *              applicant does not have a dayOrBoarding value, we will default to true. This happens in cases where
     *              the EfcWorksheetData is not created from a School PFS Assignment. Examples of this include:
     *                  - Creating EfcWorksheetData from a PFS after a family first submits their PFS.
     *                  - Creating EfcWorksheetData for the EFC Simulator.
     * @return True if the calculations are based off of day school.
     */
    public Boolean isDaySchool() {
        ApplicantData mainApplicant = getMainApplicant();

        if (mainApplicant != null) {
            return String.isNotBlank(mainApplicant.dayOrBoarding) ? EfcPicklistValues.DAYBOARDING_DAY.equalsIgnoreCase(mainApplicant.dayOrBoarding) : true;
        }

        if (applicants == null || applicants.isEmpty()) {
            return true;
        }

        ApplicantData firstApplicant = applicants[0];
        return String.isNotBlank(firstApplicant.dayOrBoarding) ? EfcPicklistValues.DAYBOARDING_DAY.equalsIgnoreCase(firstApplicant.dayOrBoarding) : true;
    }

    /* Debug values (not persisted) */
    public Decimal debugEmploymentAllowanceTargetIncome { get; set; }
    public Decimal debugMedicalAllowanceThreshold { get; set; }
    public Decimal debugImputePercentageUsed { get; set; }

    /* Constructor - initialize default values */
    public EfcWorksheetData() {
        // [CH] NAIS-1514 - Default processing error to false
        processingError = false;
        // NAIS-2371 [CH] Default to empty
        processingErrorText = '';

        // set an initial applicant
        applicants = new List<ApplicantData> {new ApplicantData()};

        // set default professional judgement values (defaults are based on SSS calculation)
        pjApplyMinIncomeToNonTaxableIncome = false;
        pjAddDeprecHomeBusExpense = false;
        pjUseHomeEquity = true;
        pjUseHousingIndexMultiplier = false;
        pjUseHomeEquityCap = true;

        // by default don't calculate adjustments to income
        calculateAdjustmentsToIncome = false;

        // by default calculate tax
        useRevisedFederalTax = false;
        useRevisedStateOtherTax = false;
        useRevisedMedicareTax = false;
        useRevisedSocialSecTax = false;

        // [SL] NAIS-1016
        ownHome = false;

        // [SL] NAIS-1007: by default calculate family size
        calculateFamilySize = true;

        // [SL] NAIS-1011: by default calculate net profit/loss - business
        calculateNetProfitLossBusinessFarm = true;
    }
}