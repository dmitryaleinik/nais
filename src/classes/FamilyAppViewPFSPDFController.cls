/**
 * Class: FamilyAppViewPFSPDFController
 *
 * Copyright (C) 2013 NAIS
 *
 * Purpose: Retrieves the appropriate PFS information for display in a PDF document
 * 
 * Where Referenced:
 *   Family Portal - FamilyApplicationMain Page
 *
 * Change History:
 *
 * Developer           Date        JIRA Ref     Description
 * ---------------------------------------------------------------------------------------
 * Brian Clift         09/11/13    NAIS-526     Initial development
 *
 * exponent partners
 * 901 Mission Street, Suite 105
 * San Francisco, CA  94103
 * +1 (800) 918-2917
 * 
 */

public without sharing class FamilyAppViewPFSPDFController {
    public PFS__c pfs { get; set; }
    
    public FamilyAppViewPFSPDFController() {
        String pfsId = ApexPages.currentPage().getParameters().get('id');
        
        if(pfsId != null) {
            initializer(Id.valueOf(pfsId));
        }
    }
    
    public FamilyAppViewPFSPDFController(Id pfsId) {
        initializer(pfsId);
    }   
    
    private void initializer(String pfsId) {
        
        if(pfsId != null && pfs == null){
            //pfs = ApplicationUtils.queryPFSRecord(pfsId, null, null, 'PFSView');
            String queryString = ApplicationUtils.writeQueryString('PFSView');
            queryString += ' FROM PFS__c where Id = :pfsId';
            
            // [CH] NAIS-1483 Adding filter to lock down PFS access in Family Portal
            //It ensure that FP users do not have access to not allowed PFS.
            User u = GlobalVariables.getCurrentUser();
            if(GlobalVariables.isFamilyPortalProfile(u.ProfileId)){
                queryString += ' AND Parent_A__c = \'' + u.ContactId + '\'';
            }
            
            queryString += ' limit 1';
            
            // [CH] NAIS-1482 When we get ready to run the query we should see if 
            //  sharing needs to be enforced
            Boolean isSchoolPortalUser = GlobalVariables.isSchoolPortalProfile(UserInfo.getProfileId());
            
            try{
                // If this is a school portal user
                if(isSchoolPortalUser){
                    // Run the query with Sharing
                    WithSharingQueryClass queryClass = new WithSharingQueryClass();
                    pfs = queryClass.queryPFS(queryString, pfsId);
                }
                // This is not a school portal user
                else{
                    // Run the query as-is
                    List<PFS__c> pfsResults = Database.query(queryString);
                    pfs = pfsResults.isEmpty() ? null : pfsResults[0];
                }
            }
            catch(Exception ex){
                throw new PFSRetrievalException('Error retrieving PFS record, or requested record does not exist.  Please contact your administrator for assistance.');
                System.debug('PFS Retrieval Error: ' + ex.getMessage() + '\n\n' + ex.getStackTraceString());
            }
            
            // Present an error message if the PFS record doesn't come back
            if(pfs == null){
                throw new PFSRetrievalException('Error retrieving PFS record, or requested record does not exist.  Please contact your administrator for assistance.');
                System.debug('PFS Retrieval Error: ');
            }
        }  
    }  
    
    // class that lets us query with sharing
    public with sharing class WithSharingQueryClass{
        String pfsId;
        public PFS__c queryPFS(String queryString, String pfsId){
            PFS__c thePFS;
            this.pfsId = pfsId; 
            for (PFS__c pfs : Database.query(queryString)){
                thePFS = pfs;
            }
            return thePFS;
        }
    } 
    
    public class PFSRetrievalException extends Exception {} 
}