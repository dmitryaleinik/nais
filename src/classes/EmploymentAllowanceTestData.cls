/**
 * @description This class is used to create Employment Allowance records for unit tests.
 */
@isTest
public class EmploymentAllowanceTestData extends SObjectTestData {
    @testVisible private static final Decimal INCOME_LOW = 20;
    @testVisible private static final Decimal ALLOWANCE_BASE_AMOUNT = 100;
    @testVisible private static final Decimal ALLOWANCE_THRESHOLD = 50;

    /**
     * @description Get the default values for the Employment_Allowance__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Employment_Allowance__c.Academic_Year__c => AcademicYearTestData.Instance.DefaultAcademicYear.Id,
                Employment_Allowance__c.Income_Low__c => INCOME_LOW,
                Employment_Allowance__c.Allowance_Base_Amount__c => ALLOWANCE_BASE_AMOUNT,
                Employment_Allowance__c.Allowance_Threshold__c => ALLOWANCE_THRESHOLD
        };
    }

    /**
     * @description Set the Academic Year for the current Employment Allowance
     *             record.
     * @param academicYearId The AcademicYear to set on the current Employment
     *             Allowance record.
     * @return The current working instance of EmploymentAllowanceTestData.
     */
    public EmploymentAllowanceTestData forAcademicYearId(Id academicYearId) {
        return (EmploymentAllowanceTestData) with(Employment_Allowance__c.Academic_Year__c, academicYearId);
    }

    /**
     * @description Insert the current working Employment_Allowance__c record.
     * @return The currently operated upon Employment_Allowance__c record.
     */
    public Employment_Allowance__c insertEmploymentAllowance() {
        return (Employment_Allowance__c)insertRecord();
    }

    /**
     * @description Create the current working Employment Allowance record without resetting
     *             the stored values in this instance of EmploymentAllowanceTestData.
     * @return A non-inserted Employment_Allowance__c record using the currently stored field
     *             values.
     */
    public Employment_Allowance__c createEmploymentAllowanceWithoutReset() {
        return (Employment_Allowance__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Employment_Allowance__c record.
     * @return The currently operated upon Employment_Allowance__c record.
     */
    public Employment_Allowance__c create() {
        return (Employment_Allowance__c)super.buildWithReset();
    }

    /**
     * @description The default Employment_Allowance__c record.
     */
    public Employment_Allowance__c DefaultEmploymentAllowance {
        get {
            if (DefaultEmploymentAllowance == null) {
                DefaultEmploymentAllowance = createEmploymentAllowanceWithoutReset();
                insert DefaultEmploymentAllowance;
            }
            return DefaultEmploymentAllowance;
        }
        private set;
    }

    /**
     * @description Get the Employment_Allowance__c SObjectType.
     * @return The Employment_Allowance__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Employment_Allowance__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static EmploymentAllowanceTestData Instance {
        get {
            if (Instance == null) {
                Instance = new EmploymentAllowanceTestData();
            }
            return Instance;
        }
        private set;
    }

    private EmploymentAllowanceTestData() { }
}