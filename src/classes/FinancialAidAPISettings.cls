/**
 * Created by Nate on 7/17/17.
 */

public with sharing class FinancialAidAPISettings {

    @testVisible private static final String API_SETTING_NAME_PARAM = 'APISettingName';
    @testVisible private static final String FIELD_TO_SET_PARAM = 'fieldToSet';
    @testVisible private static final String FIELD_VALUE_PARAM = 'fieldValue';

    private static Financial_Aid_API_Settings__c record;
    private static Set<String> fieldNames;

    private static Financial_Aid_API_Settings__c getInstance() {
        if (record != null) {
            return record;
        }

        // Why have this?
        record = Financial_Aid_API_Settings__c.getInstance();

        // If nothing is defined, not even org defaults, create a new instance using the default values.
        // If we are in a unit test, use the default values.
        if (record == null || Test.isRunningTest()) {
            record = getDefault();
        }

        return record;
    }

    public static Boolean isEnabled(String apiSettingName) {
        ArgumentNullException.throwIfNull(apiSettingName, API_SETTING_NAME_PARAM);

        // If the field exists in the metadata check the value, otherwise default to false.
        Boolean isFeatureEnabled = hasField(apiSettingName) ? (Boolean)getInstance().get(apiSettingName) : false;

        // If the value of the feature toggle is null, default to false.
        return isFeatureEnabled == null ? false : isFeatureEnabled;
    }

    @testVisible
    private static void setToggle(String featureToggleName, Boolean fieldValue) {
        ArgumentNullException.throwIfNull(featureToggleName, API_SETTING_NAME_PARAM);
        ArgumentNullException.throwIfNull(fieldValue, FIELD_VALUE_PARAM);

        // If the field is a valid feature toggle, populate the value.
        if (hasField(featureToggleName)) {
            getInstance().put(featureToggleName, fieldValue);
        }
    }

    private static Financial_Aid_API_Settings__c getDefault() {
        return (Financial_Aid_API_Settings__c)Financial_Aid_API_Settings__c.SObjectType.newSObject(null, true);
    }

    private static Set<String> getFieldNames() {
        if (fieldNames != null) {
            return fieldNames;
        }

        fieldNames = new Set<String>();

        for (String fieldName : Schema.SObjectType.Financial_Aid_API_Settings__c.fields.getMap().keySet()) {
            fieldNames.add(fieldName.toLowerCase());
        }

        return fieldNames;
    }

    private static Boolean hasField(String fieldName) {
        return String.isNotBlank(fieldName) && getFieldNames().contains(fieldName.toLowerCase());
    }

}