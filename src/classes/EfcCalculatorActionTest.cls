@isTest
private class EfcCalculatorActionTest
{

    @isTest
    private static void testIsDisabledSssAutoCalc()
    {
        EFC_Settings__c efcSettings = insertEfcSettings();

        Test.startTest();
            System.assertEquals(EfcCalculatorAction.isDisabledRevisionAutoCalc, false);
        Test.stopTest();
    }

    @isTest
    private static void testsetIsDisabledRevisionAutoCalc()
    {
        EFC_Settings__c efcSettings = insertEfcSettings();

        Test.startTest();
            EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
            System.assertEquals(true, EfcCalculatorAction.isDisabledRevisionAutoCalc);
        Test.stopTest();
    }

    @isTest
    private static void testGetIsDisabledRevisionAutoCalc()
    {
        EFC_Settings__c efcSettings = insertEfcSettings();

        Test.startTest();
            System.assertEquals(false, EfcCalculatorAction.getIsDisabledRevisionAutoCalc());

            EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
            System.assertEquals(true, EfcCalculatorAction.getIsDisabledRevisionAutoCalc());
        Test.stopTest();
    }

    @isTest
    private static void testSetIsDisabledSssAutoCalc()
    {
        EFC_Settings__c efcSettings = insertEfcSettings();

        Test.startTest();
            System.assertEquals(false, EfcCalculatorAction.isDisabledSssAutoCalc);

            EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
            System.assertEquals(true, EfcCalculatorAction.isDisabledSssAutoCalc);
        Test.stopTest();
    }

    @isTest
    private static void testGetIsDisabledSssAutoCalc()
    {
        EFC_Settings__c efcSettings = insertEfcSettings();

        Test.startTest();
            System.assertEquals(false, EfcCalculatorAction.getIsDisabledSssAutoCalc());

            EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
            System.assertEquals(true, EfcCalculatorAction.getIsDisabledSssAutoCalc());
        Test.stopTest();
    }

    @isTest
    private static void testCalculateRevisionEfc_singleSpfsaId()
    {
        EFC_Settings__c efcSettings = insertEfcSettings();
        List<EfcWorksheetData> worksheets;

        School_Pfs_Assignment__c spfsa = createSpfsaRecords(1)[0];
        insert spfsa;
        Set<Id> schoolPfsAssignmentIds = new Set<Id>{spfsa.Id};

        Test.startTest();
            worksheets = EfcCalculatorAction.calculateRevisionEfc(schoolPfsAssignmentIds, true);
        Test.stopTest();
            
        System.assertEquals(1, worksheets.size());
        List<School_Pfs_Assignment__c> spfsaRecords = SchoolPfsAssignmentsSelector.newInstance().selectByIdWithCustomFieldList(
            schoolPfsAssignmentIds, new List<String>{'Revision_EFC_Calc_Status__c'});
        System.assertEquals(1, spfsaRecords.size());
        System.assertNotEquals(EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE_BATCH, spfsaRecords[0].Revision_EFC_Calc_Status__c);
    }

    @isTest
    private static void testCalculateRevisionEfc_BulkProcessing()
    {
        EFC_Settings__c efcSettings = insertEfcSettings();
        List<EfcWorksheetData> worksheets;

        List<School_Pfs_Assignment__c> spfsaRecords = createSpfsaRecords(11);
        insert spfsaRecords;
        Set<Id> schoolPfsAssignmentIds = new Map<Id, School_Pfs_Assignment__c>(spfsaRecords).keySet();

        Test.startTest();
            worksheets = EfcCalculatorAction.calculateRevisionEfc(schoolPfsAssignmentIds, true);
        Test.stopTest();
        
        System.assertEquals(null, worksheets);
        
        spfsaRecords = SchoolPfsAssignmentsSelector.newInstance().selectByIdWithCustomFieldList(
            schoolPfsAssignmentIds, new List<String>{'Revision_EFC_Calc_Status__c'});
        System.assertEquals(11, spfsaRecords.size());
        
        Boolean isProcessedByBatch = true;
        String recalcBatchStatus = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE_BATCH;
        for(School_Pfs_Assignment__c singleSpfsa : spfsaRecords)
        {
            if (singleSpfsa.Revision_EFC_Calc_Status__c != recalcBatchStatus)
            {
                isProcessedByBatch = false;
            }
        }
        System.assert(isProcessedByBatch);
    }

    @isTest
    private static void testIsEfcRecalcNeeded_spfsa()
    {
        insertEfcSettings();
        Pfs__c submittedPfs = PfsTestData.Instance
            .asSubmitted()
            .asPaid()
            .forAcademicYearPicklist(AcademicYearTestData.Instance.DefaultAcademicYear.Name)
            .forVisibleToSchoolsUntil(Date.Today().addDays(30)).DefaultPfs;
        System.assert(EfcCalculatorAction.isStatusRecalculable(
            submittedPfs.PFS_Status__c, submittedPfs.Payment_Status__c, submittedPfs.Visible_to_Schools_Until__c));

        List<School_Pfs_Assignment__c> spfsaRecords = createSpfsaRecords(2);
        spfsaRecords[0].Override_Default_COLA_Value__c = 1;
        spfsaRecords[0].Est_Family_Contribution__c = 10000;
        spfsaRecords[0].PFS_Status__c = submittedPfs.PFS_Status__c;
        spfsaRecords[1].Override_Default_COLA_Value__c = 2;
        spfsaRecords[1].Est_Family_Contribution__c = 20000;
        spfsaRecords[1].PFS_Status__c = submittedPfs.PFS_Status__c;
        insert spfsaRecords;

        spfsaRecords = getSPFSAsById(new Map<Id, School_Pfs_Assignment__c>(spfsaRecords).keySet());
        System.assertEquals(2, spfsaRecords.size());
        School_PFS_Assignment__c newSchoolPfsAssign = spfsaRecords[0];
        School_PFS_Assignment__c oldSchoolPfsAssign = spfsaRecords[1];

        Test.startTest();
            System.assert(!EfcCalculatorAction.isEfcRecalcNeeded(new School_PFS_Assignment__c(), new School_PFS_Assignment__c()));
            System.assert(EfcCalculatorAction.isEfcRecalcNeeded(newSchoolPfsAssign, null));
            System.assert(EfcCalculatorAction.isEfcRecalcNeeded(newSchoolPfsAssign, oldSchoolPfsAssign));
        Test.stopTest();
    }

    @isTest
    private static void testIsEfcRecalcNeeded_pfs()
    {
        insertEfcSettings();
        Pfs__c oldPfs = PfsTestData.Instance.create();
        Pfs__c newPfs = PfsTestData.Instance
            .asSubmitted()
            .asPaid()
            .forAcademicYearPicklist(AcademicYearTestData.Instance.DefaultAcademicYear.Name)
            .forVisibleToSchoolsUntil(Date.Today().addDays(30)).create();
        insert new List<Pfs__c>{oldPfs, newPfs};

        Test.startTest();
            System.assert(EfcCalculatorAction.isEfcRecalcNeeded(newPfs, oldPfs));

            oldPfs.Number_of_Dependent_Children__c = 1;
            oldPfs.PFS_Status__c = EfcPicklistValues.PFS_STATUS_SUBMITTED;
            oldPfs.Payment_Status__c = EfcPicklistValues.PAYMENT_STATUS_PAID_IN_FULL;
            oldPfs.Visible_to_Schools_Until__c = Date.Today().addDays(30);
            newPfs.Number_of_Dependent_Children__c = 2;
            update new List<Pfs__c>{oldPfs, newPfs};

            System.assert(EfcCalculatorAction.isEfcRecalcNeeded(newPfs, oldPfs));
            System.assert(EfcCalculatorAction.isEfcRecalcNeeded(newPfs, null));
        Test.stopTest();      
    }

    @isTest
    private static void testIsStatusRecalculable()
    {
        EFC_Settings__c efcSettings = insertEfcSettings();

        Pfs__c submittedPfs = PfsTestData.Instance
            .asSubmitted()
            .forVisibleToSchoolsUntil(Date.Today().addDays(30)).DefaultPfs;

        Test.startTest();
            System.assert(EfcCalculatorAction.isStatusRecalculable(
                submittedPfs.PFS_Status__c, submittedPfs.Payment_Status__c, submittedPfs.Visible_to_Schools_Until__c));
        
            efcSettings.Do_Not_Require_Payment_For_EFC_Run__c = true;
            update efcSettings;

            System.assert(EfcCalculatorAction.isStatusRecalculable(
                submittedPfs.PFS_Status__c, submittedPfs.Payment_Status__c, submittedPfs.Visible_to_Schools_Until__c));
        Test.stopTest();
    }

    @isTest
    private static void testIsEfcRecalcNeeded_applicant()
    {
        Applicant__c newApplicant = ApplicantTestData.Instance
            .forGradeInEntryYear('1').create();
        Applicant__c oldApplicant = ApplicantTestData.Instance
            .forGradeInEntryYear('2').create();
        insert new List<Applicant__c>{newApplicant, oldApplicant};

        Test.startTest();
            System.assert(EfcCalculatorAction.isEfcRecalcNeeded(newApplicant, oldApplicant));
        Test.stopTest();
    }

    @isTest
    private static void testCalculateSssEfc()
    {
        List<Pfs__c> pfsRecords = new List<Pfs__c>();

        for (Integer i=0; i<11; i++)
        {
            Pfs__c pfsRecord = PfsTestData.Instance.create();

            pfsRecords.add(pfsRecord);
        }

        insert pfsRecords;

        Test.startTest();
            EfcCalculatorAction.calculateSssEfc(new Map<Id, Pfs__c>(pfsRecords).keySet(), true);

            pfsRecords = PfsSelector.newInstance().selectWithCustomFieldListById(
                new Map<Id, Pfs__c>(pfsRecords).keySet(), new List<String>{'SSS_EFC_Calc_Status__c'});
            System.assertEquals(EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE_BATCH, pfsRecords[0].SSS_EFC_Calc_Status__c);
            System.assertEquals(EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE_BATCH, pfsRecords[10].SSS_EFC_Calc_Status__c);
        Test.stopTest();
    }

    @isTest
    private static void testApplicantHasUpdatedEfcFields()
    {
        Applicant__c newApplicant = ApplicantTestData.Instance
            .forDoesApplicantOwnAssets('Yes')
            .forAssetsTotalValue(10000).create();
        Applicant__c oldApplicant = ApplicantTestData.Instance
            .forDoesApplicantOwnAssets('Yes')
            .forAssetsTotalValue(20000).create();
        insert new List<Applicant__c>{newApplicant, oldApplicant};

        Test.startTest();
            System.assert(EfcCalculatorAction.applicantHasUpdatedEfcFields(newApplicant, oldApplicant));
        Test.stopTest();
    }

    @isTest
    private static void testBusinessFarmHasUpdatedEfcFields()
    {
        Academic_Year__c nextAcademicYear = AcademicYearTestData.Instance.asNextYear().DefaultAcademicYear;
        Pfs__c pfs = PfsTestData.Instance.forAcademicYearPicklist(nextAcademicYear.Name).DefaultPfs;

        Business_Farm__c newBizFarm = BusinessFarmTestData.Instance
            .forPfsId(pfs.Id)
            .forBusinessEntityType('Sole Proprietorship').create();
        Business_Farm__c oldBizFarm = BusinessFarmTestData.Instance
            .forPfsId(pfs.Id)
            .forBusinessEntityType('Partnership').create();
        insert new List<Business_Farm__c>{newBizFarm, oldBizFarm};

        Test.startTest();
            System.assert(EfcCalculatorAction.businessFarmHasUpdatedEfcFields(newBizFarm, null));
            System.assert(!EfcCalculatorAction.businessFarmHasUpdatedEfcFields(newBizFarm, oldBizFarm));
        Test.stopTest();
    }

    private static Map<Id, Business_Farm__c> selectBusinessFarmsById(Set<Id> recordIds) {
        List<String> fieldsToQuery = new List<String>(Business_Farm__c.SObjectType.getDescribe().fields.getMap().keySet());

        String query = String.format('SELECT {0} FROM Business_Farm__c WHERE Id IN :recordIds', new List<String> { String.join(fieldsToQuery, ',') });
        List<Business_Farm__c> records = Database.query(query);

        return new Map<Id, Business_Farm__c>(records);
    }

    private static Map<Id, School_Biz_Farm_Assignment__c> selectBusinessAssignmentsById(Set<Id> recordIds) {
        List<String> fieldsToQuery = new List<String>(School_Biz_Farm_Assignment__c.SObjectType.getDescribe().fields.getMap().keySet());

        String query = String.format('SELECT {0} FROM School_Biz_Farm_Assignment__c WHERE Id IN :recordIds', new List<String> { String.join(fieldsToQuery, ',') });
        List<School_Biz_Farm_Assignment__c> records = Database.query(query);

        return new Map<Id, School_Biz_Farm_Assignment__c>(records);
    }

    @isTest
    private static void businessFarmHasUpdatedEfcFields_2018_2019_excludeCorporationsAndPartnerships_businessTypeChanged_expectTrue()
    {
        FeatureToggles.setToggle('Exclude_S_Corps_Partnerships_From_Sums__c', true);
        Academic_Year__c nextAcademicYear = AcademicYearTestData.Instance.forName('2018-2019').DefaultAcademicYear;
        Pfs__c pfs = PfsTestData.Instance.forAcademicYearPicklist(nextAcademicYear.Name).DefaultPfs;

        Business_Farm__c newBizFarm = BusinessFarmTestData.Instance
                .forPfsId(pfs.Id)
                .forBusinessEntityType('Sole Proprietorship').create();
        Business_Farm__c oldBizFarm = BusinessFarmTestData.Instance
                .forPfsId(pfs.Id)
                .forBusinessEntityType('Partnership').create();
        insert new List<Business_Farm__c>{newBizFarm, oldBizFarm};

        Set<Id> recordIds = new Set<Id> { newBizFarm.Id, oldBizFarm.Id };
        Map<Id, Business_Farm__c> businessesById = selectBusinessFarmsById(recordIds);

        Test.startTest();
        System.assert(EfcCalculatorAction.businessFarmHasUpdatedEfcFields(businessesById.get(newBizFarm.Id), businessesById.get(oldBizFarm.Id)), 'Expected EFC updates to be needed after changing the business entity type.');
        Test.stopTest();
    }

    @isTest
    private static void businessFarmHasUpdatedEfcFields_2018_2019_excludeCorporationsAndPartnerships_businessTypeNotChanged_expectFalse()
    {
        FeatureToggles.setToggle('Exclude_S_Corps_Partnerships_From_Sums__c', true);
        Academic_Year__c nextAcademicYear = AcademicYearTestData.Instance.forName('2018-2019').DefaultAcademicYear;
        Pfs__c pfs = PfsTestData.Instance.forAcademicYearPicklist(nextAcademicYear.Name).DefaultPfs;

        Business_Farm__c newBizFarm = BusinessFarmTestData.Instance
                .forPfsId(pfs.Id)
                .forBusinessEntityType('Sole Proprietorship').create();
        Business_Farm__c oldBizFarm = BusinessFarmTestData.Instance
                .forPfsId(pfs.Id)
                .forBusinessEntityType('Sole Proprietorship').create();
        insert new List<Business_Farm__c>{newBizFarm, oldBizFarm};

        Set<Id> recordIds = new Set<Id> { newBizFarm.Id, oldBizFarm.Id };
        Map<Id, Business_Farm__c> businessesById = selectBusinessFarmsById(recordIds);
        
        Test.startTest();
        System.assert(!EfcCalculatorAction.businessFarmHasUpdatedEfcFields(businessesById.get(newBizFarm.Id), businessesById.get(oldBizFarm.Id)), 'Expected EFC updates to not be needed when the business entity type does not change.');
        Test.stopTest();
    }

    @isTest
    private static void testSchoolBizFarmAssignmentHasUpdatedEfcFields()
    {
        School_Biz_Farm_Assignment__c oldSBFA = SchoolBusinessFarmAssignmentTestData.Instance
            .forOrigBusinessFarmAssets(10000).create();
        School_Biz_Farm_Assignment__c newSBFA = SchoolBusinessFarmAssignmentTestData.Instance
            .forOrigBusinessFarmAssets(20000).create();
        insert new List<School_Biz_Farm_Assignment__c>{oldSBFA, newSBFA};

        Map<Id, School_Biz_Farm_Assignment__c> sbfasById = selectBusinessAssignmentsById(new Set<Id> { oldSBFA.Id, newSBFA.Id });
        newSBFA = sbfasById.get(newSBFA.Id);
        oldSBFA = sbfasById.get(oldSBFA.Id);

        Test.startTest();
        System.assert(EfcCalculatorAction.schoolBizFarmAssignmentHasUpdatedEfcFields(newSBFA, null));
        System.assert(EfcCalculatorAction.schoolBizFarmAssignmentHasUpdatedEfcFields(newSBFA, oldSBFA));
        Test.stopTest();
    }

    @isTest
    private static void schoolBizFarmAssignmentHasUpdatedEfcFields_2018_2019_excludeCorporationsAndPartnerships_businessTypeChanged_expectTrue()
    {
        FeatureToggles.setToggle('Exclude_S_Corps_Partnerships_From_Sums__c', true);
        Academic_Year__c nextAcademicYear = AcademicYearTestData.Instance.forName('2018-2019').DefaultAcademicYear;
        PFS__c pfs = PfsTestData.Instance.forAcademicYearPicklist(nextAcademicYear.Name).DefaultPfs;

        // Create the SBFAs with the same business type.
        School_Biz_Farm_Assignment__c oldSBFA = SchoolBusinessFarmAssignmentTestData.Instance
                .forOrigBusinessFarmAssets(10000).withBusinessType(EfcDataManager.BUSINESS_ENTITY_TYPE_PARTNERSHIP).create();
        School_Biz_Farm_Assignment__c newSBFA = SchoolBusinessFarmAssignmentTestData.Instance
                .forOrigBusinessFarmAssets(10000).withBusinessType(EfcDataManager.BUSINESS_ENTITY_TYPE_PARTNERSHIP).create();
        insert new List<School_Biz_Farm_Assignment__c>{ oldSBFA, newSBFA };

        Map<Id, School_Biz_Farm_Assignment__c> sbfasById = selectBusinessAssignmentsById(new Set<Id> { oldSBFA.Id, newSBFA.Id });
        newSBFA = sbfasById.get(newSBFA.Id);
        oldSBFA = sbfasById.get(oldSBFA.Id);

        // Now change the business type revision field on the new SBFA.
        newSBFA.Business_Entity_Type__c = EfcDataManager.BUSINESS_ENTITY_TYPE_CORPORATION;

        Test.startTest();
        System.assert(EfcCalculatorAction.schoolBizFarmAssignmentHasUpdatedEfcFields(newSBFA, null), 'Expected EFC updates to be necessary if the Business Type has changed.');
        System.assert(EfcCalculatorAction.schoolBizFarmAssignmentHasUpdatedEfcFields(newSBFA, oldSBFA), 'Expected EFC updates to be necessary if the Business Type has changed.');
        Test.stopTest();
    }

    @isTest
    private static void schoolBizFarmAssignmentHasUpdatedEfcFields_2018_2019_excludeCorporationsAndPartnerships_origBusinessTypeChanged_expectTrue()
    {
        FeatureToggles.setToggle('Exclude_S_Corps_Partnerships_From_Sums__c', true);
        Academic_Year__c nextAcademicYear = AcademicYearTestData.Instance.forName('2018-2019').DefaultAcademicYear;
        PFS__c pfs = PfsTestData.Instance.forAcademicYearPicklist(nextAcademicYear.Name).DefaultPfs;

        // Create the SBFAs with the same business type.
        School_Biz_Farm_Assignment__c oldSBFA = SchoolBusinessFarmAssignmentTestData.Instance
                .forOrigBusinessFarmAssets(10000).withBusinessType(EfcDataManager.BUSINESS_ENTITY_TYPE_PARTNERSHIP).create();
        School_Biz_Farm_Assignment__c newSBFA = SchoolBusinessFarmAssignmentTestData.Instance
                .forOrigBusinessFarmAssets(10000).withBusinessType(EfcDataManager.BUSINESS_ENTITY_TYPE_PARTNERSHIP).create();
        insert new List<School_Biz_Farm_Assignment__c>{ oldSBFA, newSBFA };

        Map<Id, School_Biz_Farm_Assignment__c> sbfasById = selectBusinessAssignmentsById(new Set<Id> { oldSBFA.Id, newSBFA.Id });
        newSBFA = sbfasById.get(newSBFA.Id);
        oldSBFA = sbfasById.get(oldSBFA.Id);

        // Now change the business type orig field on the new SBFA.
        newSBFA.Orig_Business_Entity_Type__c = EfcDataManager.BUSINESS_ENTITY_TYPE_CORPORATION;

        Test.startTest();
        System.assert(EfcCalculatorAction.schoolBizFarmAssignmentHasUpdatedEfcFields(newSBFA, null), 'Expected EFC updates to be necessary if the Business Type has changed.');
        System.assert(EfcCalculatorAction.schoolBizFarmAssignmentHasUpdatedEfcFields(newSBFA, oldSBFA), 'Expected EFC updates to be necessary if the Business Type has changed.');
        Test.stopTest();
    }

    @isTest
    private static void schoolBizFarmAssignmentHasUpdatedEfcFields_2018_2019_excludeCorporationsAndPartnerships_businessTypesNotChanged_expectFalse()
    {
        FeatureToggles.setToggle('Exclude_S_Corps_Partnerships_From_Sums__c', true);
        Academic_Year__c nextAcademicYear = AcademicYearTestData.Instance.forName('2018-2019').DefaultAcademicYear;
        PFS__c pfs = PfsTestData.Instance.forAcademicYearPicklist(nextAcademicYear.Name).DefaultPfs;

        // Create the SBFAs with the same business type.
        School_Biz_Farm_Assignment__c oldSBFA = SchoolBusinessFarmAssignmentTestData.Instance
                .forOrigBusinessFarmAssets(10000).withBusinessType(EfcDataManager.BUSINESS_ENTITY_TYPE_PARTNERSHIP).create();
        School_Biz_Farm_Assignment__c newSBFA = SchoolBusinessFarmAssignmentTestData.Instance
                .forOrigBusinessFarmAssets(10000).withBusinessType(EfcDataManager.BUSINESS_ENTITY_TYPE_PARTNERSHIP).create();
        insert new List<School_Biz_Farm_Assignment__c>{ oldSBFA, newSBFA };

        Map<Id, School_Biz_Farm_Assignment__c> sbfasById = selectBusinessAssignmentsById(new Set<Id> { oldSBFA.Id, newSBFA.Id });
        newSBFA = sbfasById.get(newSBFA.Id);
        oldSBFA = sbfasById.get(oldSBFA.Id);

        Test.startTest();
        System.assert(!EfcCalculatorAction.schoolBizFarmAssignmentHasUpdatedEfcFields(newSBFA, oldSBFA), 'Expected EFC updates to not be necessary if the Business Type has not changed.');
        Test.stopTest();
    }

    private static EFC_Settings__c insertEfcSettings()
    {
        EFC_Settings__c efcSettings = createEfcSettings();
        insert efcSettings;

        return efcSettings;
    }

    private static EFC_Settings__c createEfcSettings()
    {
        return new EFC_Settings__c (
            Name = 'EFC',
            Batch_Calculation_Threshold__c = 10,
            Disable_Automatic_Revision_EFC_Calc__c = false,
            Disable_Automatic_SSS_EFC_Calc__c = false,
            Do_Not_Require_Payment_For_EFC_Run__c = false,
            Initial_EFC_Calc_Method__c = 'Batch');
    }

    private static List<School_Pfs_Assignment__c> createSpfsaRecords(Integer spfsaNumber)
    {
        List<School_Pfs_Assignment__c> spfsaRecords = new List<School_Pfs_Assignment__c>();
        String academicYearName = AcademicYearTestData.Instance.DefaultAcademicYear.Name;

        List<Contact> students = createStudentRecords(spfsaNumber);
        insert students;

        List<Applicant__c> applicants = createApplicantRecords(new Map<Id, Contact>(students).keySet());
        insert applicants;

        for (Applicant__c applicant : applicants)
        {
            School_Pfs_Assignment__c spfsa = SchoolPfsAssignmentTestData.Instance
                .forAcademicYearPicklist(academicYearName)
                .forApplicantId(applicant.Id).create();

            spfsaRecords.add(spfsa);
        }

        return spfsaRecords;
    }

    private static List<Contact> createStudentRecords(Integer studentsNumber)
    {
        List<Contact> students = new List<Contact>();

        for (Integer i = 0; i < studentsNumber; i++)
        {
            Contact student = ContactTestData.Instance.asStudent().create();

            students.add(student);
        }

        return students;
    }

    private static List<Applicant__c> createApplicantRecords(Set<Id> studentIds)
    {
        List<Applicant__c> applicants = new List<Applicant__c>();

        for (Id studentId : studentIds)
        {
            Applicant__c applicant = ApplicantTestData.Instance
                .forContactId(studentId).create();

            applicants.add(applicant);
        }

        return applicants;
    }

    private static List<School_PFS_Assignment__c> getSPFSAsById(Set<Id> spfsaIds)
    {
        List<String> pfsAssignmentFields = new List<String>(Schema.SObjectType.School_PFS_Assignment__c.fields.getMap().keySet());

        return SchoolPfsAssignmentsSelector.newInstance().selectByIdWithCustomFieldList(spfsaIds, pfsAssignmentFields);
    }
}