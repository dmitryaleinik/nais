/**
 * @description This class is responsible for querying Knowledge__kav records.
 */
public class KnowledgeAVSelector extends fflib_SObjectSelector
{

    @testVisible private static final String ID_SET_PARAM = 'idSet';

    /**
     * @description Queries Knowledge__kav records by Id.
     * @param idSet The Ids of the Knowledge__kav records to query.
     * @return A list of Knowledge__kav records.
     * @throws ArgumentNullException if idSet is null.
     */
    public List<Knowledge__kav> selectById(Set<Id> idSet)
    {
        ArgumentNullException.throwIfNull(idSet, ID_SET_PARAM);

        return (List<Knowledge__kav>)super.selectSObjectsById(idSet);
    }

    private Schema.SObjectType getSObjectType()
    {
        return Knowledge__kav.SObjectType;
    }

    private List<Schema.SObjectField> getSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
            Knowledge__kav.ArticleNumber,
            Knowledge__kav.Title,
            Knowledge__kav.UrlName,
            Knowledge__kav.ValidationStatus,
            Knowledge__kav.Summary,
            Knowledge__kav.Language,
            Knowledge__kav.Article_Body__c,
            Knowledge__kav.Sequence__c
        };
    }

    /**
     * @description Creates a new instance of the KnowledgeAVSelector.
     * @return An instance of KnowledgeAVSelector.
     */
    public static KnowledgeAVSelector newInstance()
    {
        return new KnowledgeAVSelector();
    }

    /**
     * @description Singleton instance property.
     */
    public static KnowledgeAVSelector Instance
    {
        get
        {
            if (Instance == null)
            {
                Instance = newInstance();
            }
            return Instance;
        }
        private set;
    }

    public KnowledgeAVSelector() {}
}