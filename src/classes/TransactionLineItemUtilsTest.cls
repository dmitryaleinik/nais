@isTest
private class TransactionLineItemUtilsTest {
    private static final Decimal PRODUCT_AMOUNT = 200.00;
    private static final Decimal ALT_PRODUCT_AMOUNT = 150.00;
    private static final Decimal EARLY_BIRD_PRODUCT_AMOUNT = 100.00;

    private static final String ACCOUNT_NAME = 'Account Name';
    private static final String OPPORTUNITY_NAME = 'Opportunity Name';
    private static final String PRODUCT_CODE = 'Full Product Code';
    private static final String EARLY_BIRD_PRODUCT_CODE = 'Early Bird Product Code';
    private static final String ACCOUNT_CODE = '5100';
    private static final String TRANSACTION_CODE = '3230';
    private static final String SCHOOL_CODE = '123456';
    private static final String ACCOUNT_LABEL = 'Subscription Revenue';
    private static final String SCHOOL_SUBSCRIPTION = 'School Subscription';
    private static final String ALT_SCHOOL_SUBSCRIPTION = 'Alt School Subscription';
    private static final String RENEWAL = 'Renewal';
    private static final String PROSPECTING = 'Prospecting';
    private static final String POSTED = 'Posted';
    private static final String EARLY_BIRD = 'Early Bird';
    private static final String EARLY_BIRD_SUBSCRIPTION_DISCOUNT = 'Early Bird Subscription Discount';
    private static final String ACADEMIC_YEAR = '2016-2017';

    @testSetup
    private static void setup() {
        insert new Academic_Year__c(Name = ACADEMIC_YEAR);

        insert new List<Transaction_Settings__c> {
                new Transaction_Settings__c(
                        Name = SCHOOL_SUBSCRIPTION,
                        Account_Code__c = ACCOUNT_CODE,
                        Transaction_Code__c = TRANSACTION_CODE,
                        Account_Label__c = ACCOUNT_LABEL),
                new Transaction_Settings__c(
                        Name = EARLY_BIRD_SUBSCRIPTION_DISCOUNT,
                        Account_Code__c = ACCOUNT_CODE,
                        Transaction_Code__c = TRANSACTION_CODE,
                        Account_Label__c = ACCOUNT_LABEL)
        };

        insert new List<Transaction_Annual_Settings__c> {
                new Transaction_Annual_Settings__c(
                        Name = SCHOOL_SUBSCRIPTION,
                        Product_Amount__c = PRODUCT_AMOUNT,
                        Product_Code__c = PRODUCT_CODE,
                        Year__c = ACADEMIC_YEAR.left(4),
                        Subscription_Type__c = SubscriptionTypes.SUBSCRIPTION_FULL_TYPE),
                new Transaction_Annual_Settings__c(
                        Name = EARLY_BIRD,
                        Product_Amount__c = EARLY_BIRD_PRODUCT_AMOUNT,
                        Product_Code__c = EARLY_BIRD_PRODUCT_CODE,
                        Year__c = ACADEMIC_YEAR.left(4),
                        Discount_Type__c = EARLY_BIRD,
                        Subscription_Type__c = SubscriptionTypes.SUBSCRIPTION_FULL_TYPE),
                new Transaction_Annual_Settings__c(
                        Name = EARLY_BIRD + SCHOOL_SUBSCRIPTION,
                        Product_Amount__c = ALT_PRODUCT_AMOUNT,
                        Product_Code__c = EARLY_BIRD_PRODUCT_CODE,
                        Year__c = ACADEMIC_YEAR.left(4),
                        Discount_Type__c = EARLY_BIRD + SCHOOL_SUBSCRIPTION,
                        Subscription_Type__c = SubscriptionTypes.BASIC_TYPE),
                new Transaction_Annual_Settings__c(
                        Name = ALT_SCHOOL_SUBSCRIPTION,
                        Product_Amount__c = ALT_PRODUCT_AMOUNT,
                        Product_Code__c = PRODUCT_CODE,
                        Year__c = ACADEMIC_YEAR.left(4),
                        Subscription_Type__c = SubscriptionTypes.BASIC_TYPE)
        };

        Account schoolAccount = TestUtils.createAccount(ACCOUNT_NAME, RecordTypes.schoolAccountTypeId, 5, true);

        Opportunity opportunityRecord = new Opportunity(
                Name = OPPORTUNITY_NAME,
                AccountId = schoolAccount.Id,
                RecordTypeId = RecordTypes.opportunitySubscriptionFeeTypeId,
                Academic_Year_Picklist__c = ACADEMIC_YEAR,
                New_Renewal__c = RENEWAL,
                StageName = PROSPECTING,
                CloseDate = Date.today(),
                Subscription_Type__c = SubscriptionTypes.SUBSCRIPTION_FULL_TYPE,
                SYSTEM_Suppress_Auto_Sale_TLI_Insert__c = true);

        insert opportunityRecord;
    }

    private static Opportunity getOpportunity() {
        List<Opportunity> opportunities = [SELECT
                                                  Id,
                                                  Name,
                                                  AccountId,
                                                  RecordTypeId,
                                                  Academic_Year_Picklist__c,
                                                  New_Renewal__c,
                                                  StageName,
                                                  CloseDate,
                                                  Subscription_Type__c,
                                                  SYSTEM_Suppress_Auto_Sale_TLI_Insert__c
                                             FROM Opportunity
                                            LIMIT 1];

        if (!opportunities.isEmpty()) {
            return opportunities[0];
        }
        return null;
    }

    private static List<Transaction_Line_Item__c> getTransactionLineItems(Id opportunityId) {
        return [SELECT
                       Id,
                       RecordTypeId,
                       Transaction_Status__c,
                       Transaction_Date__c,
                       Transaction_Type__c,
                       Account_Code__c,
                       Account_Label__c,
                       Transaction_Code__c,
                       Amount__c,
                       Product_Amount__c,
                       Product_Code__c
                  FROM Transaction_Line_Item__c
                 WHERE Opportunity__c = :opportunityId];
    }

    @isTest
    private static void testCreateSchoolSubscriptionSaleTLI() {
        Opportunity newOpportunity = getOpportunity();

        List<Transaction_Line_Item__c> transactions = getTransactionLineItems(newOpportunity.Id);

        System.assertEquals(0, transactions.size());

        Test.startTest();
        TransactionLineItemUtils.createSaleTLI(newOpportunity, true);
        Test.stopTest();

        transactions = getTransactionLineItems(newOpportunity.Id);

        System.assertEquals(1, transactions.size());

        Transaction_Line_Item__c lineItem = transactions[0];
        System.assertEquals(RecordTypes.saleTransactionTypeId, lineItem.RecordTypeId);

        System.assertEquals(POSTED, lineItem.Transaction_Status__c);
        System.assertEquals(Date.today(), lineItem.Transaction_Date__c);
        System.assertEquals(SCHOOL_SUBSCRIPTION, lineItem.Transaction_Type__c);

        System.assertEquals(ACCOUNT_CODE, lineItem.Account_Code__c);
        System.assertEquals(ACCOUNT_LABEL, lineItem.Account_Label__c);
        System.assertEquals(TRANSACTION_CODE, lineItem.Transaction_Code__c);

        System.assertEquals(PRODUCT_AMOUNT, lineItem.Amount__c);
        System.assertEquals(String.valueOf(PRODUCT_AMOUNT), lineItem.Product_Amount__c);
        System.assertEquals(PRODUCT_CODE, lineItem.Product_Code__c);
    }

    @isTest
    private static void testCreateSchoolEarlyBirdDiscountTLI() {
        Opportunity newOpportunity = getOpportunity();

        List<Transaction_Line_Item__c> transactions = getTransactionLineItems(newOpportunity.Id);
        System.assertEquals(0, transactions.size());

        Test.startTest();
        TransactionLineItemUtils.createSchoolEarlyBirdDiscountTLI(newOpportunity, 50.00, true);

        // We call this a second time to ensure that we aren't duplicating the
        // creation of Early Bird Transaction Line Items.
        TransactionLineItemUtils.createSchoolEarlyBirdDiscountTLI(newOpportunity, 50.00, true);
        Test.stopTest();

        transactions = getTransactionLineItems(newOpportunity.Id);
        System.assertEquals(1, transactions.size());

        Transaction_Line_Item__c lineItem = transactions[0];
        System.assertEquals(RecordTypes.schoolDiscountTransactionTypeId, lineItem.RecordTypeId);

        System.assertEquals(POSTED, lineItem.Transaction_Status__c);
        System.assertEquals(Date.today(), lineItem.Transaction_Date__c);
        System.assertEquals(EARLY_BIRD_SUBSCRIPTION_DISCOUNT, lineItem.Transaction_Type__c);

        System.assertEquals(ACCOUNT_CODE, lineItem.Account_Code__c);
        System.assertEquals(ACCOUNT_LABEL, lineItem.Account_Label__c);
        System.assertEquals(TRANSACTION_CODE, lineItem.Transaction_Code__c);

        System.assertEquals(EARLY_BIRD_PRODUCT_AMOUNT, lineItem.Amount__c);
        System.assertEquals(String.valueOf(EARLY_BIRD_PRODUCT_AMOUNT), lineItem.Product_Amount__c);
        System.assertEquals(EARLY_BIRD_PRODUCT_CODE, lineItem.Product_Code__c);
    }

    @isTest
    private static void testGetSchoolSubscriptionSaleTLIAmount() {
        String year = ACADEMIC_YEAR.left(4);
        String invalidYear = ACADEMIC_YEAR.right(4);

        Test.startTest();
        System.assertEquals(PRODUCT_AMOUNT, TransactionLineItemUtils.getSchoolSubscriptionSaleTLIAmount(
                SubscriptionTypes.SUBSCRIPTION_FULL_TYPE, year));
        System.assertEquals(ALT_PRODUCT_AMOUNT, TransactionLineItemUtils.getSchoolSubscriptionSaleTLIAmount(
                SubscriptionTypes.BASIC_TYPE, year));
        System.assertEquals(0, TransactionLineItemUtils.getSchoolSubscriptionSaleTLIAmount(
                SubscriptionTypes.SUBSCRIPTION_FULL_TYPE, invalidYear));
        System.assertEquals(0, TransactionLineItemUtils.getSchoolSubscriptionSaleTLIAmount(
                ALT_SCHOOL_SUBSCRIPTION, year));
        Test.stopTest();
    }

    @isTest
    private static void testGetSchoolSubscriptionDiscountTLIAmount() {
        String year = ACADEMIC_YEAR.left(4);
        String invalidYear = ACADEMIC_YEAR.right(4);
        String earlyBirdSchoolSubscription = EARLY_BIRD + SCHOOL_SUBSCRIPTION;

        Test.startTest();
        System.assertEquals(ALT_PRODUCT_AMOUNT, TransactionLineItemUtils.getSchoolSubscriptionDiscountTLIAmount(
                SubscriptionTypes.BASIC_TYPE, earlyBirdSchoolSubscription, year));
        System.assertEquals(EARLY_BIRD_PRODUCT_AMOUNT, TransactionLineItemUtils.getSchoolSubscriptionDiscountTLIAmount(
                SubscriptionTypes.SUBSCRIPTION_FULL_TYPE, EARLY_BIRD, year));
        System.assertEquals(0, TransactionLineItemUtils.getSchoolSubscriptionDiscountTLIAmount(
                SubscriptionTypes.SUBSCRIPTION_FULL_TYPE, EARLY_BIRD, invalidYear));
        System.assertEquals(0, TransactionLineItemUtils.getSchoolSubscriptionDiscountTLIAmount(
                ALT_SCHOOL_SUBSCRIPTION, EARLY_BIRD, year));
        Test.stopTest();
    }
    
    //SFP-1389
    @isTest
    private static void createSchoolEarlyBirdDiscountTLI_BasicOpportunity_NoEarlyBirdDiscountTLI() {
        Opportunity newOpportunity = getOpportunity();
        newOpportunity.Subscription_Type__c = SubscriptionTypes.BASIC_TYPE;
        update newOpportunity;
        
        Transaction_Line_Item__c earlyBirdTransaction = TransactionLineItemUtils.createSchoolEarlyBirdDiscountTLI(
                    newOpportunity, 100, false);

        System.assertEquals(null, earlyBirdTransaction, 'There should be NO early bird TLI for basic opps.');
    }
}