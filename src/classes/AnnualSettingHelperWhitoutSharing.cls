public class AnnualSettingHelperWhitoutSharing {
    // [DP] 05.01.2015 COMMENTING OUT and moving back into AnnualSettingHelper. AnnualSettingHelper.cls is now "without sharing"
    //public static void updateAccountPFSCounters(List<Annual_Setting__c> newTrigger,
    //                                            List<Annual_Setting__c> oldTrigger)//NAIS-2305
    //{
    //    Map<Id, Account> accountsToUpdate = new Map<Id, Account>{};
    //    Academic_Year__c currentYear = GlobalVariables.getCurrentAcademicYear();
    //    Academic_Year__c priorYear = GlobalVariables.getPreviousAcademicYear();
    //    Account tmpAccount = null;
        
    //    for(Integer i=0; i<newTrigger.size(); i++)
    //    {
    //        Annual_Setting__c annualSetting = newTrigger[i];
    //        //NAIS-2305: On trigger update
    //        if((oldTrigger[i].wPFS_Count__c != annualSetting.wPFS_Count__c  || oldTrigger[i].Total_PFS_Count__c != annualSetting.Total_PFS_Count__c )
    //        && (annualSetting.Academic_Year__c == currentYear.Id || annualSetting.Academic_Year__c == priorYear.Id) )//NAIS-2305
    //        {
    //            tmpAccount = (accountsToUpdate.containsKey(annualSetting.School__c)
    //                        ?accountsToUpdate.get(annualSetting.School__c):new Account(Id=annualSetting.School__c));
                
    //            if(annualSetting.Academic_Year__c == currentYear.Id){
    //                tmpAccount.Most_Recent_wPFS_Count__c =  annualSetting.wPFS_Count__c;
    //                tmpAccount.Most_Recent_PFS_Count__c =  annualSetting.Total_PFS_Count__c;
    //            }else if(annualSetting.Academic_Year__c == priorYear.Id){
    //                tmpAccount.Prior_Year_wPFS_Count__c  =  annualSetting.wPFS_Count__c;
    //                tmpAccount.Prior_Year_PFS_Count__c  =  annualSetting.Total_PFS_Count__c;
    //            }
    //            accountsToUpdate.put(annualSetting.School__c, tmpAccount);
    //        }
    //    }
    //    Database.update(accountsToUpdate.values());
    //}
}