/*
 * NAIS-1092: Payment receipt
 *
 * SL, Exponent Partners, 2013
 */
@isTest
private class FamilyPaymentReceiptControllerTest {
    @isTest
    private static void testPaymentReceipt() {
        // set up test data
        Application_Fee_Settings__c feeSettings = new Application_Fee_Settings__c();
        feeSettings.Name = GlobalVariables.getCurrentYearString();
        feeSettings.Amount__c = 50;
        feeSettings.Discounted_Amount__c = 40;
        feeSettings.Product_Code__c = 'Test Product Code'; // [SL] NAIS-1031: new required field
        feeSettings.Discounted_Product_Code__c = 'KS Test Product Code'; // [SL] NAIS-1031: new required field
        insert feeSettings;

        AcademicYearTestData.Instance.insertAcademicYears(6);
        PFS__c pfs = PfsTestData.Instance
            .forAcademicYearPicklist(GlobalVariables.getCurrentYearString())
            .forParentAFirstName('notme')
            .forParentALastName('whosonfirst').DefaultPfs;

        FinanceAction.createOppAndTransaction(new Set<Id>{pfs.Id}); // [DP] 07.08.2015 NAIS-1933 -- need to call this explicitly now

        Opportunity opp = [SELECT Id, Paid_Status__c FROM Opportunity WHERE PFS__c = :pfs.Id];

        // add a transaction line item
        Transaction_Line_Item__c tli1 = TestUtils.createTransactionLineItem(opp.Id, RecordTypes.paymentAutoTransactionTypeId, false);
        tli1.Transaction_Type__c = 'Credit/Debit Card';
        tli1.Transaction_Status__c = 'Posted';
        tli1.Amount__c = 50;
        tli1.Transaction_Date__c = Date.newInstance(2013, 1, 1);
        tli1.Transaction_ID__c = '12345';
        tli1.Tender_Type__c = 'Visa';
        tli1.CC_Last_4_Digits__c = '1111';

        Test.startTest();
            insert tli1;

            // verify receipt values when instantiating as a page controller
            Test.setCurrentPage(Page.FamilyPaymentReceipt);
            ApexPages.currentPage().getParameters().put('id', tli1.Id);
            FamilyPaymentReceiptController controller = new FamilyPaymentReceiptController();
        Test.stopTest();

        pfs = PfsSelector.Instance.selectWithCustomFieldListById(
            new Set<Id>{pfs.Id}, new List<String>{'Academic_Year_Picklist__c'})[0];
        System.assertEquals(50, controller.receiptValues.amount);
        System.assertEquals(
            [SELECT Name FROM Academic_Year__c WHERE Name=:pfs.Academic_Year_Picklist__c].Name,
            controller.receiptValues.academicYear);
        System.assertEquals(
            [SELECT PFS_Number__c FROM PFS__c WHERE Id=:pfs.Id].PFS_Number__c,
            controller.receiptValues.pfsNumber);
        System.assertEquals(Date.newInstance(2013, 1, 1), controller.receiptValues.paymentDate);
        System.assertEquals('Credit/Debit Card', controller.receiptValues.transactionType);
        System.assertEquals('12345', controller.receiptValues.referenceId);
        System.assertEquals(true, controller.receiptValues.isAutoPayment);
        System.assertEquals(true, controller.receiptValues.isCreditDebit);
        System.assertEquals('Visa', controller.receiptValues.cardType);
        System.assertEquals('1111', controller.receiptValues.cardNumber);
        System.assertEquals('notme', controller.receiptValues.contactFirstName);
        System.assertEquals('notme whosonfirst', controller.receiptValues.contactFullName);

        String expectedDescription = PaymentProcessor.CreditCardStatementDescriptor;
        System.assertEquals(expectedDescription, controller.receiptValues.description);

        // verify HTML/Text content is returned when invoking statically
        System.assertNotEquals(null, FamilyPaymentReceiptController.getHtmlContent(tli1.Id));
        System.assertNotEquals(null, FamilyPaymentReceiptController.getPlainTextContent(tli1.Id));
    }

    @isTest
    private static void testLogoLinkProperty() {
        FamilyPaymentReceiptController controller = new FamilyPaymentReceiptController();

        System.assert(String.isBlank(controller.familyPaymentReceiptLogoLinkUrl));

        Document doc = DocumentTestData.Instance
            .forDocumentDeveloperName(DocumentLinkMappingService.documentInfo.Document_Developer_Name__c).insertDocument();

        Test.startTest();
            controller = new FamilyPaymentReceiptController();
        Test.stopTest();

        System.assert(String.isNotBlank(controller.familyPaymentReceiptLogoLinkUrl));
    }

}