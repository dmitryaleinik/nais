@isTest
private with sharing class FamilyAppEZProcessControllerTest
{
    
    private static PFS__c pfsRecord;
    private static ApplicationUtils appUtils;
    private static String currentScreen;
    private static Id academicYearId;
    
    private static void generateBasicPFSData()
    {
        insertFamilyAppSettings();
        List<Household_Income_Threshold__c> result = HouseholdIncomeThresholdTestData.Instance.insertIncomeThresholds();
        academicYearId = result[0].Academic_Year__c;
        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, true);
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, true);
        
        User userRecord = [select Id, Name, ContactId, Contact.LastName, Contact.Name, Contact.MailingStreet, Contact.MailingCity, Contact.MailingState,  
                        Contact.MailingPostalCode, Contact.MailingCountry, Contact.Gender__c, Contact.Birthdate, 
                        Contact.Email, Contact.HomePhone, Contact.MobilePhone, Contact.Preferred_Phone__c,
                        Contact.FirstName, Contact.Middle_Name__c, Contact.Suffix__c, Contact.Salutation, Contact.Phone,
                        LanguageLocaleKey
                        from User 
                        where Id = :UserInfo.getUserId()];
                        
        pfsRecord = ApplicationUtils.generateBlankPFS(academicYearId, userRecord);
        
        Id pfsId = [select Id from PFS__c limit 1].Id;
        pfsRecord = ApplicationUtils.queryPFSRecord(pfsId);
        
        if (currentScreen != null){
            appUtils = new ApplicationUtils(UserInfo.getLanguage(), pfsRecord, currentScreen);
        } else {
            appUtils = new ApplicationUtils(UserInfo.getLanguage(), pfsRecord);
        }
    }
    
    private static PFS__c requeryPFSToUpdateFormulaFields(Id pfsId) {
        
        Set<String> pfsFieldsSet = Schema.SObjectType.PFS__c.fields.getMap().keySet();
        List<String> pfsFieldsList = new List<String>();
        pfsFieldsList.addAll(pfsFieldsSet);
        String pfsFieldsStr = String.join(pfsFieldsList, ',');            
        List<PFS__c> pfsRecords = Database.query('SELECT '+pfsFieldsStr.removeEnd(',')+' FROM PFS__c WHERE Id=\''+pfsId+'\' LIMIT 1');            
            
        return pfsRecords[0];
    }
    
    private static void insertFamilyAppSettings()
    {
        List<FamilyAppSettings__c> familyAppSettings = new List<FamilyAppSettings__c>
        {
            FamilyAppSettingsTestData.Instance
                    .forName('HouseholdInformation')
                    .forMainLabelField('HouseholdInformation__c')
                    .forNextPage('ParentsGuardians')
                    .forStatusField('statHouseholdInformationBoolean__c')
                    .forSubLabelField('statHouseholdInformation__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset(),
                    
            FamilyAppSettingsTestData.Instance
                    .forName('ParentsGuardians')
                    .forMainLabelField('SchoolSelection__c')
                    .forNextPage('FamilyIncome')
                    .forStatusField('statSelectSchools__c')
                    .forSubLabelField('SelectSchools__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset(),

            FamilyAppSettingsTestData.Instance
                    .forName('SelectSchools')
                    .forMainLabelField('SchoolSelection__c')
                    .forNextPage('FamilyIncome')
                    .forStatusField('statSelectSchools__c')
                    .forSubLabelField('SelectSchools__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset(),

            FamilyAppSettingsTestData.Instance
                    .forName('ApplicantInformation')
                    .forMainLabelField('SchoolSelection__c')
                    .forNextPage('FamilyIncome')
                    .forStatusField('statSelectSchools__c')
                    .forSubLabelField('SelectSchools__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset(),

            FamilyAppSettingsTestData.Instance
                    .forName('BusinessFarm')
                    .forMainLabelField('BusinessFarm__c')
                    .forNextPage('BusinessInformation')
                    .forIsSpeedbumpPage(true)
                    .forStatusField('statBusinessFarmBoolean__c')
                    .createFamilyAppSettingsWithoutReset(),

            FamilyAppSettingsTestData.Instance
                    .forName('BusinessInformation')
                    .forMainLabelField('BusinessFarm__c')
                    .forNextPage('BusinessIncome')
                    .forStatusField('statBusinessInformation__c')
                    .forSubLabelField('BusinessInformation__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset(),

            FamilyAppSettingsTestData.Instance
                    .forName('BusinessAssets')
                    .forMainLabelField('BusinessFarm__c')
                    .forNextPage('OtherInformation')
                    .forLoopBackPage('BusinessInformation')
                    .forStatusField('statBusinessAssets__c')
                    .forSubLabelField('BusinessAssets__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset(),

            FamilyAppSettingsTestData.Instance
                    .forName('ForKamehamehaProgram')
                    .forMainLabelField('ForKamehameha__c')
                    .forNextPage('OtherInformation')
                    .forLoopBackPage('ForKamehamehaApplicant')
                    .forStatusField('statForKamehamehaProgram__c')
                    .forSubLabelField('ForKamehameha__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset(),

            FamilyAppSettingsTestData.Instance
                    .forName('EducationalExpenses')
                    .forMainLabelField('SchoolSelection__c')
                    .forNextPage('OtherInformation')
                    .forStatusField('statSelectSchools__c')
                    .forSubLabelField('SelectSchools__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset(),
            

            FamilyAppSettingsTestData.Instance
                    .forName('FamilyIncome')
                    .forMainLabelField('SchoolSelection__c')
                    .forNextPage('OtherInformation')
                    .forStatusField('statSelectSchools__c')
                    .forSubLabelField('SelectSchools__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset()
        };

        insert familyAppSettings;
    }
    
    @isTest
    private static void allGetters_isEZPFZ_expectTrue()
    {
        generateBasicPFSData();
        System.assertEquals(true, pfsRecord != null);
        System.assertEquals(true, pfsRecord.Id != null);
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .forDefaultEZHouseholdAssets(30000).DefaultFamilyPortalSettings;
        pfsRecord.Family_Size__c = 4;
        pfsRecord.Parent_A_Country__c = 'United States';
        pfsRecord.statParentsGuardians__c  = true;
        pfsRecord.statApplicantInformation__c   = true;
        pfsRecord.statDependentInformation__c  = true;
        pfsRecord.statHouseholdSummary__c   = true;
        pfsRecord.statSelectSchools__c   = true;
        pfsRecord.Submission_Process__c = null;
        
        update pfsRecord;
        
        Test.startTest();      
            pfsRecord = requeryPFSToUpdateFormulaFields(pfsRecord.Id);
            FamilyPortalSettings.Default_EZ_Household_Assets = 30000;
            
            PageReference mainPage = Page.FamilyApplicationMain;
            mainPage.getParameters().put('id', pfsRecord.Id);
            mainPage.getParameters().put('screen', 'BusinessFarm');
            Test.setCurrentPage(mainPage);
            
            FamilyAppEZProcessController controller = new FamilyAppEZProcessController();
            controller.pfs = pfsRecord;
            controller.appUtils = appUtils;
            controller.pfs.Household_Income_Under_Threshold__c = 'Yes';
            controller.pfs.Household_Assets_Under_Threshold__c = 'Yes';
            controller.pfs.Household_Has_Received_Benefits__c = 'Yes';
            controller.pfs.Guardian_Owns_Business__c = 'Yes';
            System.assertEquals(true, controller.saveFormEZ() == null);
            
            System.assertEquals(4,  controller.pfs.Family_Size__c);
            System.assertEquals(70000, controller.getHouseholdIncomeUnderThreshold_X());
            System.assertEquals(30000, controller.getHouseholdAssetsUnderThreshold_X());
            System.assertEquals(true, pfsRecord.statParentsGuardians__c);
            System.assertEquals(true, pfsRecord.statApplicantInformation__c);
            System.assertEquals(true, pfsRecord.statDependentInformation__c);
            System.assertEquals(true, pfsRecord.statHouseholdSummary__c);
            System.assertEquals('Complete', controller.pfs.statHouseholdInformation__c);
            System.assertEquals('Complete', controller.pfs.statSchoolSelection__c);
            System.assertEquals(true, controller.basicSectionsCompleted);
            System.assertEquals(true, controller.showPopup);
            
            
            controller.pfs.Guardian_Self_Employed_Freelancer__c = 'Yes';
            System.assertEquals(true, controller.saveFormEZ() != null);
            
            System.assertEquals(true, controller.cancelFormEZ() != null);
            System.assertEquals(true, controller.selectedCountryApply);
        Test.stopTest();
    }
    
    @isTest
    private static void incompleteSectionAction_ezSuccessTrue_expectParentsGuardiansScreen()
    {
        generateBasicPFSData();
        System.assertEquals(true, pfsRecord != null);
        System.assertEquals(true, pfsRecord.Id != null);
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .forDefaultEZHouseholdAssets(30000).DefaultFamilyPortalSettings;
        
        pfsRecord.statHouseholdInformationBoolean__c = true;
        update pfsRecord;
        
        Test.startTest();      
            pfsRecord = requeryPFSToUpdateFormulaFields(pfsRecord.Id);
            
            PageReference mainPage = Page.FamilyApplicationMain;
            mainPage.getParameters().put('id', pfsRecord.Id);
            mainPage.getParameters().put('screen', 'HouseholdInformation');
            Test.setCurrentPage(mainPage);
            
            FamilyAppEZProcessController controller = new FamilyAppEZProcessController();
            controller.pfs = pfsRecord;
            controller.appUtils = appUtils;
            
            PageReference p = controller.incompleteSectionAction(true);
            System.assertEquals(true, p.getParameters().get('ezsuccess') != null);
            System.assertEquals('parentsguardians', p.getParameters().get('screen').toLowerCase());
        Test.stopTest();
    }
}