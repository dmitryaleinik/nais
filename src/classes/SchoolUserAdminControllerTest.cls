// NAIS-82 School Portal - User Administration
// BR, Exponent Partners, 2013

@isTest
private class SchoolUserAdminControllerTest {

    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

    @isTest
    private static void testUserAdmin()
    {
        List<Profile_Type_Grouping__c> ptgSettings = new List<Profile_Type_Grouping__c>();
        String profName = [Select Name from Profile where Id = :GlobalVariables.schoolPortalAdminProfileId][0].Name;
        ptgSettings.add(new Profile_Type_Grouping__c(Name = profName, Is_School_Admin_Profile__c = true, Is_School_Profile__c = true, Profile_Name__c = profName));
        insert ptgSettings;

        Account schoolA = TestUtils.createAccount('School A', RecordTypes.schoolAccountTypeId, 3, false);
        Account schoolB = TestUtils.createAccount('School B', RecordTypes.schoolAccountTypeId, 3, false);
        insert new List<Account> { schoolA, schoolB };
        
        Contact staffA1 = TestUtils.createContact('Staff A1', schoolA.Id, RecordTypes.schoolStaffContactTypeId, false);
        Contact staffA2 = TestUtils.createContact('Staff A2', schoolA.Id, RecordTypes.schoolStaffContactTypeId, false);
        Contact staffA3 = TestUtils.createContact('Staff A3', schoolA.Id, RecordTypes.schoolStaffContactTypeId, false);
        Contact staffB1 = TestUtils.createContact('Staff B1', schoolB.Id, RecordTypes.schoolStaffContactTypeId, false);
        insert new List<Contact> { staffA1, staffA2, staffA3, staffB1 };
        
        User portalUser1 = TestUtils.createPortalUser('Staff A1', 'sa1@test.org', 'sa1', staffA1.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);
        User portalUser2 = TestUtils.createPortalUser('Staff A2', 'sa2@test.org', 'sa2', staffA2.Id, GlobalVariables.schoolPortalAdminProfileId, false, false);
        
        System.runAs(thisUser){
            insert new List<User> { portalUser1, portalUser2 };
        }
        
        // fake new context
        GlobalVariables.u = null;
        
        System.runAs(portalUser1) {
            Test.setCurrentPage(Page.SchoolUserAdmin);
            
            // Constructor/Load
            SchoolUserAdminController testCon = new SchoolUserAdminController();
            
            system.debug(testCon.contactList);
            
            System.assertEquals(3, testCon.contactList.size());
            System.assertEquals(staffA1.Id, testCon.ContactList[0].c.Id);
            System.assertEquals(true, testCon.ContactList[0].portalAccess);
            System.assertEquals(staffA2.Id, testCon.ContactList[1].c.Id);
            System.assertEquals(false, testCon.ContactList[1].portalAccess);        // inactive user
            //System.assertEquals(staffA3.Id, testCon.ContactList[2].c.Id);
            //System.assertEquals(false, testCon.ContactList[2].portalAccess);        // no user
            
            // nsmelink
            testCon.selectedContactId = staffA2.Id;
            PageReference nameLinkRef = testCon.namelink();
            System.assert(nameLinkRef.getURL().contains('id=' + staffA2.Id));//NAIS-2013
            //System.assert(nameLinkRef.getURL().contains('retURL='));
            
            // cannot remove user's own portal access
            testCon.selectedContactId = staffA1.Id;
            PageReference removeAccessRef = testCon.removePortalAccess();
            System.assertEquals('You may not remove your own access to the portal.', ApexPages.getMessages()[0].getSummary());
            System.assertEquals(null, removeAccessRef);
            
            // remove portal access
            portalUser2.isActive = true;
            update portalUser2;
            System.assertEquals(true, [select IsActive from User where ContactId =:staffA2.Id].isActive);
            testCon.selectedContactId = staffA2.Id;
            removeAccessRef = testCon.removePortalAccess();
            System.assertEquals(false, [select IsActive from User where ContactId =:staffA2.Id].isActive);
            System.assert(removeAccessRef.getUrl().contains(Page.SchoolUserAdmin.getUrl()));
            
            // Don't compare with ? and & in case the order of parameters changes.
            PageReference newContactRef = testCon.newcontact();
            System.assertEquals(newContactRef.getUrl(), Page.SchoolUserAdminProfile.getUrl());//NAIS-2013
        }
    }
}