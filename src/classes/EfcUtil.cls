public class EfcUtil {
    private static Map<Id, String> academicYearIdToNameMap = new Map<Id, String>();
    
    public static Decimal nullToZero(Decimal value) {
        return (value != null) ? value : 0;
    }
    
    public static Decimal negativeToZero(Decimal value) {
        return (value < 0) ? 0 : value;
    }
    
    public static Integer ageFromBirthdate(Date birthdate, DateTime submissionDate) {
        Integer age = null;
        // NAIS-1654 [dp] changing age method to use submission date
        Date dateToUse = submissionDate != null ? submissionDate.Date() : System.today();
        if (birthdate != null) {
            // difference between birth year and current year
            age = dateToUse.year() - birthdate.year();
            
            // subtract 1 if birthday hasn't occurred this year
            if (Date.newInstance(dateToUse.year(), birthdate.month(), birthdate.day()) > dateToUse) {
                age = age - 1;
            }
        }
        return age;
    }
    
    public static String getAcademicYearName(Id academicYearId) {
        String academicYearName = academicYearIdToNameMap.get(academicYearId);
        if (academicYearName == null) {
            for (Academic_Year__c theAcademicYear : [SELECT Name FROM Academic_Year__c WHERE Id = :academicYearId ORDER BY CreatedDate DESC LIMIT 1]) {
                academicYearName = theAcademicYear.Name;
            }
            if (academicYearName != null) {
                academicYearIdToNameMap.put(academicYearId, academicYearName);
            }
        }
        return academicYearName;
    }
    
    public static EFC_Settings__c getEfcSettings() {
        EFC_Settings__c efcSettings = EFC_Settings__c.getValues('EFC');
        return efcSettings;
    }
    
    public static Decimal getEfcSettingBatchCalculationThreshold() {
        EFC_Settings__c efcSettings = getEfcSettings();
        return (efcSettings != null) ? efcSettings.Batch_Calculation_Threshold__c : 10; // default to 10
    }
    
    public static boolean getEfcSettingDisableRevisionAutoEfcCalc() {
        EFC_Settings__c efcSettings = getEfcSettings();
        return (efcSettings != null) ? efcSettings.Disable_Automatic_Revision_EFC_Calc__c : false; // defaut to false
    }
    
    public static boolean getEfcSettingDisableSssAutoEfcCalc() {
        EFC_Settings__c efcSettings = getEfcSettings();
        return (efcSettings != null) ? efcSettings.Disable_Automatic_SSS_EFC_Calc__c : false; // defaut to false
    }
}