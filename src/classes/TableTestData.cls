@isTest
public class TableTestData {
    /* Populate all lookup tables with test data */
    public static void populateAllEfcTables(Id academicYearId) {
        populateFederalTaxTableData(academicYearId);
        populateStateTaxTableData(academicYearId, 'Alabama');
        populateStateTaxTableData(academicYearId, 'Colorado');
        populateStateTaxTableData(academicYearId, 'Mexico');
        populateStateTaxTableData(academicYearId, 'Not Reported');
        populateEmploymentAllowanceData(academicYearId);
        populateHousingIndexMultiplierData(academicYearId);
        populateBusinessFarmShareData(academicYearId);
        populateRetirementAllowanceData(academicYearId);
        populateAssetProgressivityData(academicYearId);
        populateIncomeProtectionAllowanceData(academicYearId);
        populateEstimatedContributionRateData(academicYearId);
    }

    /* Federal Income Tax */
    public static Federal_Tax_Table__c createFederalTaxTable(Id academicYearId, String filingType, Decimal taxableIncomeLow, Decimal taxableIncomeHigh,
                                                                Decimal usIncomeTaxBase, Decimal usIncomeTaxRate, Decimal usIncomeTaxRateThreshold,
                                                                boolean doInsert) {
        Federal_Tax_Table__c federalTaxTable = new Federal_Tax_Table__c(Academic_Year__c = academicYearId, Filing_Type__c = filingType, Taxable_Income_Low__c = taxableIncomeLow,
                                                                            Taxable_Income_High__c = taxableIncomeHigh, US_Income_Tax_Base__c = usIncomeTaxBase,
                                                                            US_Income_Tax_Rate__c = usIncomeTaxRate, US_Income_Tax_Rate_Threshold__c = usIncomeTaxRateThreshold);
        if (doInsert == true) insert federalTaxTable;
        return federalTaxTable;
    }

    public static void populateFederalTaxTableData(Id academicYearId) {
        insert new List<Federal_Tax_Table__c> {
            // academicYearId, filingType, taxableIncomeLow, taxableIncomeHigh, usIncomeTaxBase, usIncomeTaxRate, usIncomeTaxRateThreshold, doInsert
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_MARRIED_JOINT, 0, 17400, 0.00, 0.1, 0, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_MARRIED_JOINT, 17401, 70700, 1740.00, 0.15, 17400, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_MARRIED_JOINT, 70701, 142700, 9735.00, 0.25, 70700, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_MARRIED_JOINT, 142701, 217450, 27735.00, 0.28, 142700, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_MARRIED_JOINT, 217451, 388350, 48665.00, 0.33, 217450, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_MARRIED_JOINT, 388351, null, 105062.00, 0.35, 388350, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_HEAD_OF_HOUSEHOLD, 0, 12400, 0.00, 0.1, 0, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_HEAD_OF_HOUSEHOLD, 12401, 47350, 1240.00, 0.15, 12400, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_HEAD_OF_HOUSEHOLD, 47351, 122300, 6482.50, 0.25, 47350, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_HEAD_OF_HOUSEHOLD, 122301, 198050, 25220.00, 0.28, 122300, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_HEAD_OF_HOUSEHOLD, 198051, 388350, 46430.00, 0.33, 198050, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_HEAD_OF_HOUSEHOLD, 388351, null, 109229.00, 0.35, 388350, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_SINGLE, 0, 8700, 0.00, 0.1, 0, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_SINGLE, 8701, 35350, 870.00, 0.15, 8700, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_SINGLE, 35351, 85650, 4867.50, 0.25, 35350, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_SINGLE, 85651, 178650, 17442.50, 0.28, 85650, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_SINGLE, 178651, 388350, 43482.50, 0.33, 178650, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_SINGLE, 388351, null, 112683.50, 0.35, 388350, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_MARRIED_SEPARATE, 0, 8700, 0.00, 0.1, 0, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_MARRIED_SEPARATE, 8701, 35350, 870.00, 0.15, 8700, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_MARRIED_SEPARATE, 35351, 71350, 4867.50, 0.25, 35350, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_MARRIED_SEPARATE, 71351, 108725, 13867.50, 0.28, 71350, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_MARRIED_SEPARATE, 108726, 194175, 24332.50, 0.33, 108725, false),
            createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_MARRIED_SEPARATE, 194176, null, 52531.00, 0.35, 194175, false)
        };
    }

    /* State Income Tax */
    public static State_Tax_Table__c createStateTaxTable(Id academicYearId, String state, Decimal incomeLow, Decimal incomeHigh, Decimal percentTotalIncome, boolean doInsert) {
        State_Tax_Table__c stateTaxTable = new State_Tax_Table__c(Academic_Year__c=academicYearId, State__c=state, Income_Low__c=incomeLow,
                                                                    Income_High__c=incomeHigh, Percent_of_Total_Income__c=percentTotalIncome);
        if (doInsert == true) insert stateTaxTable;
        return stateTaxTable;
    }

    public static void populateStateTaxTableData(Id academicYearId, String state) {
        insert new List<State_Tax_Table__c> {
            createStateTaxTable(academicYearId, state, 0, 50000, 0.095, false),
            createStateTaxTable(academicYearId, state, 50001, 60000, 0.085, false),
            createStateTaxTable(academicYearId, state, 60001, 70000, 0.08, false),
            createStateTaxTable(academicYearId, state, 70001, 80000, 0.075, false),
            createStateTaxTable(academicYearId, state, 80001, 90000, 0.075, false),
            createStateTaxTable(academicYearId, state, 90001, 100000, 0.07, false),
            createStateTaxTable(academicYearId, state, 100001, 110000, 0.065, false),
            createStateTaxTable(academicYearId, state, 110001, 120000, 0.065, false),
            createStateTaxTable(academicYearId, state, 120001, 130000, 0.065, false),
            createStateTaxTable(academicYearId, state, 130001, null, 0.06, false)
        };
    }

    /* Employment Allowance */
    public static Employment_Allowance__c createEmploymentAllowance(Id academicYearId, Decimal incomeLow, Decimal incomeHigh, Decimal allowanceBaseAmount,
                                                                        Decimal allowanceRate, Decimal allowanceThreshold, boolean hasMaximumAllowance,
                                                                        boolean doInsert) {
        Employment_Allowance__c employmentAllowance = new Employment_Allowance__c(Academic_Year__c=academicYearId, Income_Low__c=incomeLow,
                                                                                    Income_High__c=incomeHigh, Allowance_Base_Amount__c=allowanceBaseAmount,
                                                                                    Allowance_Rate__c=allowanceRate, Allowance_Threshold__c=allowanceThreshold,
                                                                                    Has_Maximum_Allowance__c=hasMaximumAllowance);
        if (doInsert == true) insert employmentAllowance;
        return employmentAllowance;
    }

    public static void populateEmploymentAllowanceData(Id academicYearId) {
        insert new List<Employment_Allowance__c> {
            createEmploymentAllowance(academicYearId, 0, 10069, 0, 0.50, 0, true, false),
            createEmploymentAllowance(academicYearId, 10070, null, 5035, 0.25, 10069, true, false)
        };
    }

    /* Housing Index Multiplier */
    public static Housing_Index_Multiplier__c createHousingIndexMultiplier(Id academicYearId, String homePurchaseYear, Decimal multiplier,
                                                                                boolean doInsert) {
        Housing_Index_Multiplier__c housingIndexMultiplier = new Housing_Index_Multiplier__c(Academic_Year__c=academicYearId, Home_Purchase_Year__c=homePurchaseYear,
                                                                                                Housing_Index_Multiplier__c=multiplier);
        if (doInsert == true) insert housingIndexMultiplier;
        return housingIndexMultiplier;
    }

    public static void populateHousingIndexMultiplierData(Id academicYearId) {
        insert new List<Housing_Index_Multiplier__c> {
            createHousingIndexMultiplier(academicYearId, '1958', 8.2, false),
            createHousingIndexMultiplier(academicYearId, '1959', 8.2, false),
            createHousingIndexMultiplier(academicYearId, '1960', 8.14, false),
            createHousingIndexMultiplier(academicYearId, '1961', 8.14, false),
            createHousingIndexMultiplier(academicYearId, '1962', 8.09, false),
            createHousingIndexMultiplier(academicYearId, '1963', 8.17, false),
            createHousingIndexMultiplier(academicYearId, '1964', 8.12, false),
            createHousingIndexMultiplier(academicYearId, '1965', 7.85, false),
            createHousingIndexMultiplier(academicYearId, '1966', 7.52, false),
            createHousingIndexMultiplier(academicYearId, '1967', 7.26, false),
            createHousingIndexMultiplier(academicYearId, '1968', 6.88, false),
            createHousingIndexMultiplier(academicYearId, '1969', 6.47, false),
            createHousingIndexMultiplier(academicYearId, '1970', 6.32, false),
            createHousingIndexMultiplier(academicYearId, '1971', 5.96, false),
            createHousingIndexMultiplier(academicYearId, '1972', 5.56, false),
            createHousingIndexMultiplier(academicYearId, '1973', 5.08, false),
            createHousingIndexMultiplier(academicYearId, '1974', 4.62, false),
            createHousingIndexMultiplier(academicYearId, '1975', 4.23, false),
            createHousingIndexMultiplier(academicYearId, '1976', 3.98, false),
            createHousingIndexMultiplier(academicYearId, '1977', 3.6, false),
            createHousingIndexMultiplier(academicYearId, '1978', 3.18, false),
            createHousingIndexMultiplier(academicYearId, '1979', 2.84, false),
            createHousingIndexMultiplier(academicYearId, '1980', 2.55, false),
            createHousingIndexMultiplier(academicYearId, '1981', 2.36, false),
            createHousingIndexMultiplier(academicYearId, '1982', 2.27, false),
            createHousingIndexMultiplier(academicYearId, '1983', 2.25, false),
            createHousingIndexMultiplier(academicYearId, '1984', 2.19, false),
            createHousingIndexMultiplier(academicYearId, '1985', 2.15, false),
            createHousingIndexMultiplier(academicYearId, '1986', 2.07, false),
            createHousingIndexMultiplier(academicYearId, '1987', 1.98, false),
            createHousingIndexMultiplier(academicYearId, '1988', 1.91, false),
            createHousingIndexMultiplier(academicYearId, '1989', 1.84, false),
            createHousingIndexMultiplier(academicYearId, '1990', 1.8, false),
            createHousingIndexMultiplier(academicYearId, '1991', 1.79, false),
            createHousingIndexMultiplier(academicYearId, '1992', 1.77, false),
            createHousingIndexMultiplier(academicYearId, '1993', 1.69, false),
            createHousingIndexMultiplier(academicYearId, '1994', 1.61, false),
            createHousingIndexMultiplier(academicYearId, '1995', 1.55, false),
            createHousingIndexMultiplier(academicYearId, '1996', 1.52, false),
            createHousingIndexMultiplier(academicYearId, '1997', 1.48, false),
            createHousingIndexMultiplier(academicYearId, '1998', 1.44, false),
            createHousingIndexMultiplier(academicYearId, '1999', 1.38, false),
            createHousingIndexMultiplier(academicYearId, '2000', 1.31, false),
            createHousingIndexMultiplier(academicYearId, '2001', 1.25, false),
            createHousingIndexMultiplier(academicYearId, '2002', 1.22, false),
            createHousingIndexMultiplier(academicYearId, '2003', 1.15, false),
            createHousingIndexMultiplier(academicYearId, '2004', 1.06, false),
            createHousingIndexMultiplier(academicYearId, '2005', 0.99, false),
            createHousingIndexMultiplier(academicYearId, '2006', 0.93, false),
            createHousingIndexMultiplier(academicYearId, '2007', 0.92, false),
            createHousingIndexMultiplier(academicYearId, '2008', 0.95, false),
            createHousingIndexMultiplier(academicYearId, '2009', 0.99, false),
            createHousingIndexMultiplier(academicYearId, '2010', 1.01, false),
            createHousingIndexMultiplier(academicYearId, '2011', 1, false)
        };
    }

    /* Business Farm Share */
    public static Net_Worth_of_Business_Farm__c createNetWorthBusinessFarm(Id academicYearId, Decimal netWorthBusinessLow, Decimal netWorthBusinessHigh,
                                                                            Decimal expectedContributionBase, Decimal expectedContributionRate,
                                                                            Decimal thresholdToApplyRate, boolean doInsert) {
        Net_Worth_of_Business_Farm__c netWorthBusinessFarm = new Net_Worth_of_Business_Farm__c(Academic_Year__c=academicYearId, Net_Worth_of_Business_Low__c=netWorthBusinessLow,
                                                                                                Net_Worth_of_Business_High__c=netWorthBusinessHigh, Expected_Contribution_Base__c=expectedContributionBase,
                                                                                                Expected_Contribution_Rate__c=expectedContributionRate, Threshold_To_Apply_Rate__c=thresholdToApplyRate);
        if (doInsert == true) insert netWorthBusinessFarm;
        return netWorthBusinessFarm;
    }

    public static void populateBusinessFarmShareData(Id academicYearId) {
        insert new List<Net_Worth_of_Business_Farm__c> {
            createNetWorthBusinessFarm(academicYearId, 0, 46505, 0, 0.4000, 0, false),
            createNetWorthBusinessFarm(academicYearId, 46506, 139517, 18603, 0.5000, 46505, false),
            createNetWorthBusinessFarm(academicYearId, 139518, 232528, 65110, 0.6000, 139517, false),
            createNetWorthBusinessFarm(academicYearId, 232529, null, 120917, 0.7000, 232528, false)
        };
    }

    /* Retirement Allowance */
    public static Retirement_Allowance__c createRetirementAllowance(Id academicYearId, Decimal ageLow, Decimal ageHigh,
                                                                            Decimal conversionCoefficientTwoParent, Decimal allowanceTwoParent,
                                                                            Decimal conversionCoefficientOneParent, Decimal allowanceOneParent,
                                                                            boolean doInsert) {
        Retirement_Allowance__c retirementAllowance = new Retirement_Allowance__c(Academic_Year__c=academicYearId, Age_Low__c=ageLow, Age_High__c=ageHigh,
                                                                                    Conversion_Coefficient_One_Parent_Fam__c=conversionCoefficientOneParent, Conversion_Coefficient_Two_Parent_Fam__c=conversionCoefficientTwoParent,
                                                                                    Allowance_One_Parent_Family__c=allowanceOneParent, Allowance_Two_Parent_Family__c=allowanceTwoParent);
        if (doInsert == true) insert retirementAllowance;
        return retirementAllowance;
    }
    public static void populateRetirementAllowanceData(Id academicYearId) {
        insert new List<Retirement_Allowance__c> {
            createRetirementAllowance(academicYearId, 0, 40, 0.07, 32100, 0.04, 9500, false),
            createRetirementAllowance(academicYearId, 41, 41, 0.06, 32900, 0.03, 9700, false),
            createRetirementAllowance(academicYearId, 42, 42, 0.06, 33700, 0.03, 9900, false),
            createRetirementAllowance(academicYearId, 43, 43, 0.06, 34500, 0.03, 10100, false),
            createRetirementAllowance(academicYearId, 44, 44, 0.06, 35400, 0.03, 10300, false),
            createRetirementAllowance(academicYearId, 45, 45, 0.05, 36200, 0.03, 10600, false),
            createRetirementAllowance(academicYearId, 46, 46, 0.05, 37100, 0.03, 10800, false),
            createRetirementAllowance(academicYearId, 47, 47, 0.05, 38000, 0.03, 11100, false),
            createRetirementAllowance(academicYearId, 48, 48, 0.05, 39000, 0.03, 11300, false),
            createRetirementAllowance(academicYearId, 49, 49, 0.05, 39900, 0.03, 11600, false),
            createRetirementAllowance(academicYearId, 50, 50, 0.04, 40900, 0.03, 11900, false),
            createRetirementAllowance(academicYearId, 51, 51, 0.04, 42100, 0.03, 12200, false),
            createRetirementAllowance(academicYearId, 52, 52, 0.04, 43100, 0.03, 12500, false),
            createRetirementAllowance(academicYearId, 53, 53, 0.04, 44200, 0.03, 12800, false),
            createRetirementAllowance(academicYearId, 54, 54, 0.04, 45500, 0.03, 13100, false),
            createRetirementAllowance(academicYearId, 55, 55, 0.03, 46800, 0.03, 13400, false),
            createRetirementAllowance(academicYearId, 56, 56, 0.03, 47900, 0.03, 13700, false),
            createRetirementAllowance(academicYearId, 57, 57, 0.03, 49300, 0.03, 14100, false),
            createRetirementAllowance(academicYearId, 58, 58, 0.03, 50800, 0.03, 14400, false),
            createRetirementAllowance(academicYearId, 59, 59, 0.03, 52200, 0.03, 14800, false),
            createRetirementAllowance(academicYearId, 60, 60, 0.02, 53500, 0.02, 15100, false),
            createRetirementAllowance(academicYearId, 61, 61, 0.02, 55000, 0.02, 15600, false),
            createRetirementAllowance(academicYearId, 62, 62, 0.02, 56900, 0.02, 16000, false),
            createRetirementAllowance(academicYearId, 63, 63, 0.02, 58500, 0.02, 16400, false),
            createRetirementAllowance(academicYearId, 64, 64, 0.02, 60100, 0.02, 16900, false),
            createRetirementAllowance(academicYearId, 65, null, 0.02, 61800, 0.02, 17400, false)
        };
    }

    /* Asset Progressivity */
    public static Asset_Progressivity__c createAssetProgressivity(Id academicYearId, Decimal discretionaryNetWorthLow, Decimal discretionaryNetWorthHigh,
                                                                                Decimal index, boolean doInsert) {
        Asset_Progressivity__c assetProgressivity = new Asset_Progressivity__c(Academic_Year__c=academicYearId, Discretionary_Net_Worth_High__c=discretionaryNetWorthHigh,
                                                                                    Discretionary_Net_Worth_Low__c=discretionaryNetWorthLow, Index__c=index);
        if (doInsert == true) insert assetProgressivity;
        return assetProgressivity;
    }

    public static void populateAssetProgressivityData(Id academicYearId) {
        insert new List<Asset_Progressivity__c> {
            createAssetProgressivity(academicYearId, 1, 23256, 0.50, false),
            createAssetProgressivity(academicYearId, 23257, 46514, 0.75, false),
            createAssetProgressivity(academicYearId, 46515, 69771, 1.05, false),
            createAssetProgressivity(academicYearId, 69772, 93028, 1.40, false),
            createAssetProgressivity(academicYearId, 93029, null, 1.80, false)
        };
    }

    /* Income Protection Allowance */
    // [CH] NAIS-1885 Updating to support splitting out housing allowance
    public static Income_Protection_Allowance__c createIncomeProtectionAllowance(Id academicYearId, Decimal familySize, Decimal allowance, Decimal allowanceHousing, Decimal allowanceOther, Decimal familyMultiplier, boolean doInsert) {
        Income_Protection_Allowance__c incomeProtectionAllowance = new Income_Protection_Allowance__c(
                                                                        Academic_Year__c=academicYearId,
                                                                        Family_Size__c=familySize,
                                                                        Allowance__c=allowance,
                                                                        Housing_Allowance__c=allowanceHousing,
                                                                        Other_Allowance__c=allowanceOther,
                                                                        Family_Size_Multiplier__c=familyMultiplier);
        if (doInsert == true) insert incomeProtectionAllowance;
        return incomeProtectionAllowance;
    }

    // [CH] NAIS-1885 Updating to support splitting out housing allowance
    public static void populateIncomeProtectionAllowanceData(Id academicYearId) {
        insert new List<Income_Protection_Allowance__c> {
            createIncomeProtectionAllowance(academicYearId, 2, 22766, 7942, 16053, 0.67, false),
            createIncomeProtectionAllowance(academicYearId, 3, 27183, 9483, 19168, 0.8, false),
            createIncomeProtectionAllowance(academicYearId, 4, 33979, 11854, 23960, 1.0, false),
            createIncomeProtectionAllowance(academicYearId, 5, 40775, 14225, 28751, 1.2, false),
            createIncomeProtectionAllowance(academicYearId, 6, 47231, 16478, 33303, 1.39, false)
        };
    }

    /* Estimated Contribution Rate */
    public static Expected_Contribution_Rate__c createExpectedContributionRate(Id academicYearId, Decimal discretionaryIncomeLow, Decimal discretionaryIncomeHigh,
                                                                                Decimal expectedContribution, Decimal expectedContributionPercentage,
                                                                                Decimal threshold, boolean doInsert) {
        Expected_Contribution_Rate__c expectedContributionRate = new Expected_Contribution_Rate__c(Academic_Year__c=academicYearId, Discretionary_Income_Low__c=discretionaryIncomeLow,
                                                                                Discretionary_Income_High__c=discretionaryIncomeHigh, Expected_Contribution__c=expectedContribution,
                                                                                Expected_Contribution_Percentage__c=expectedContributionPercentage, Threshold__c=threshold);
        if (doInsert == true) insert expectedContributionRate;
        return expectedContributionRate;
    }

    public static void populateEstimatedContributionRateData(Id academicYearId) {
        insert new List<Expected_Contribution_Rate__c> {
            createExpectedContributionRate(academicYearId, 0, 5815,    0, 0.2200, 0, false),
            createExpectedContributionRate(academicYearId, 5816, 11631, 1280, 0.2500, 5815, false),
            createExpectedContributionRate(academicYearId, 11632, 17448, 2735, 0.2900, 11631, false),
            createExpectedContributionRate(academicYearId, 17449, 23264, 4423, 0.3400, 17448, false),
            createExpectedContributionRate(academicYearId, 23265, 29081, 6402, 0.4000, 23264, false),
            createExpectedContributionRate(academicYearId, 29082, null,    8729, 0.4500, 29081, false)
        };
    }
}