/**
 * @description Used to access the Profile_Settings__c hierarchical setting. The class and setting tell the system which
 *              profiles are used to identify certain types of users such as school administrators and family users.
 *              This class also allows us to avoid DML for this setting in unit tests. DML on hierarchical settings
 *              should be avoided in unit tests because it prohibits the ability to run unit tests async.
 */
public class ProfileSettings {

    @testVisible private static final String FIELD_TO_SET_PARAM = 'fieldToSet';

    // These default values are used when our configuration is blank. These values correspond to the original profiles used
    // for Family/School users. Legacy family profile used the deprecated Overage High Volume Customer Portal license.
    // Legacy school profiles used the deprecated Gold Partner license.
    @testVisible private static final String FAMILY_PROFILE_LEGACY = 'Family Portal User';
    @testVisible private static final String SCHOOL_ADMIN_PROFILE_LEGACY = 'School Portal Administrator';
    @testVisible private static final String SCHOOL_BASIC_ADMIN_PROFILE_LEGACY = 'School Portal Basic Administrator';
    @testVisible private static final String SCHOOL_USER_PROFILE_LEGACY = 'School Portal User';
    @testVisible private static final String SCHOOL_POWER_USER_PROFILE_LEGACY = 'School Portal Power User';
    @testVisible private static final String PARTNER_PROFILE_LEGACY = 'Gold Partner User';

    private static Profile_Settings__c record;

    /**
     * @description The name of the profile used by Families in the Family Portal. If not specified on the Portal
     *              Settings, the name of the old profile is used: Family Portal User
     */
    public static String FamilyProfileName {
        get {
            // If the family profile to use is not specified, return the name of the old profile.
            if (String.isBlank(getInstance().Family_Profile_Name__c)) {
                return FAMILY_PROFILE_LEGACY;
            }

            return getInstance().Family_Profile_Name__c;
        }
    }

    /**
     * @description The name of the profile for standard partner users. If the value is blank, we default to the legacy
     *              profile name: Gold Partner User. This is necessary because some of our unit tests relied on using a
     *              partner profile that isn't a school user.
     */
    public static String PartnerProfileName {
        get {
            if (String.isBlank(getInstance().Partner_Profile_Name__c)) {
                return PARTNER_PROFILE_LEGACY;
            }

            return getInstance().Partner_Profile_Name__c;
        }
    }

    /**
     * @description The name of the profile used by School Admins. If the value is blank, we default to the legacy
     *              profile name: School Portal Administrator.
     */
    public static String SchoolAdminProfileName {
        get {
            if (String.isBlank(getInstance().School_Admin_Profile_Name__c)) {
                return SCHOOL_ADMIN_PROFILE_LEGACY;
            }

            return getInstance().School_Admin_Profile_Name__c;
        }
    }

    /**
     * @description The name of the profile used by basic School users and admins. If the value is blank, we default to the legacy
     *              profile name: School Portal Basic Administrator.
     */
    public static String SchoolBasicAdminProfileName {
        get {
            if (String.isBlank(getInstance().School_Basic_Admin_Profile_Name__c)) {
                return SCHOOL_BASIC_ADMIN_PROFILE_LEGACY;
            }

            return getInstance().School_Basic_Admin_Profile_Name__c;
        }
    }

    /**
     * @description The name of the profile used by School users. If the value is blank, we default to the legacy
     *              profile name: School Portal User.
     */
    public static String SchoolUserProfileName {
        get {
            if (String.isBlank(getInstance().School_User_Profile_Name__c)) {
                return SCHOOL_USER_PROFILE_LEGACY;
            }

            return getInstance().School_User_Profile_Name__c;
        }
    }

    /**
     * @description The name of the profile used by School Power users. If the value is blank, we default to the legacy
     *              profile name: School Portal Power User.
     */
    public static String SchoolPowerUserProfileName {
        get {
            if (String.isBlank(getInstance().School_Power_User_Profile_Name__c)) {
                return SCHOOL_POWER_USER_PROFILE_LEGACY;
            }

            return getInstance().School_Power_User_Profile_Name__c;
        }
    }

    /**
     * @description Sets a value on the internal record. This method permits unit testing without doing DML on
     *              hierarchical settings. This method is only meant to be used in unit tests and should be left private
     *              while using the testVisible annotation.
     * @param fieldToSet The SObject field to populate.
     * @param fieldValue The value to set. This can be null.
     * @throws ArgumentNullException if fieldToSet is null.
     */
    @testVisible
    private static void setField(Schema.SObjectField fieldToSet, Object fieldValue) {
        ArgumentNullException.throwIfNull(fieldToSet, FIELD_TO_SET_PARAM);

        getInstance().put(fieldToSet, fieldValue);
    }

    private static Profile_Settings__c getInstance() {
        if (record != null) {
            return record;
        }

        record = Profile_Settings__c.getInstance();

        // If nothing is defined, not even org defaults, create a new instance using the default values.
        // If we are in a unit test, use the default values.
        if (record == null || Test.isRunningTest()) {
            record = getDefault();
        }

        return record;
    }

    private static Profile_Settings__c getDefault() {
        return (Profile_Settings__c)Profile_Settings__c.SObjectType.newSObject(null, true);
    }
}