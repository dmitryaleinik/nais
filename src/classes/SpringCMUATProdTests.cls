@isTest
private without sharing class SpringCMUATProdTests
{

    @isTest
    private static void testProd()
    {
        SpringCMPRod.BPMInitiateWorkflow_element one = new SpringCMProd.BPMInitiateWorkflow_element();
        SpringCMPRod.BPMInitiateWorkflowResponse_element two = new SpringCMProd.BPMInitiateWorkflowResponse_element();
        SpringCMPRod.AuthenticateSSO_element three = new SpringCMProd.AuthenticateSSO_element();
        SpringCMPRod.AuthenticateSSOResponse_element four = new SpringCMProd.AuthenticateSSOResponse_element();
        SpringCMProd.SpringCMServiceSoap five = new SpringCMProd.SpringCMServiceSoap();

        new SpringCMProd.DocumentGetByIdResponse_element();
        new SpringCMProd.ArrayOfSCMMetadata();
        new SpringCMProd.SCMMetadata();
        new SpringCMProd.DocumentDownloadPreviewImageResponse_element();
        new SpringCMProd.DocumentGetById_element();
        new SpringCMProd.DocumentDownloadPreviewImage_element();

        new SpringCMProd.DocumentPublish_element();
        new SpringCMProd.DocumentPublishResponse_element();

        new SpringCMProd.GetDocumentRevisions_element();
        new SpringCMProd.GetDocumentRevisionsResponse_element();

        new SpringCMProd.ArrayOfSCMDocument();
        new SpringCMProd.SCMDocument();
    }

    @isTest
    private static void testUAT()
    {
        SpringCMUAT.BPMInitiateWorkflow_element one = new SpringCMUAT.BPMInitiateWorkflow_element();
        SpringCMUAT.BPMInitiateWorkflowResponse_element two = new SpringCMUAT.BPMInitiateWorkflowResponse_element();
        SpringCMUAT.AuthenticateSSO_element three = new SpringCMUAT.AuthenticateSSO_element();
        SpringCMUAT.AuthenticateSSOResponse_element four = new SpringCMUAT.AuthenticateSSOResponse_element();
        SpringCMUAT.SpringCMServiceSoap five = new SpringCMUAT.SpringCMServiceSoap();

        new SpringCMUAT.DocumentGetByIdResponse_element();
        new SpringCMUAT.ArrayOfSCMMetadata();
        new SpringCMUAT.SCMMetadata();
        new SpringCMUAT.DocumentDownloadPreviewImageResponse_element();
        new SpringCMUAT.DocumentGetById_element();
        new SpringCMUAT.DocumentDownloadPreviewImage_element();

        new SpringCMUAT.DocumentPublish_element();
        new SpringCMUAT.DocumentPublishResponse_element();

        new SpringCMUAT.GetDocumentRevisions_element();
        new SpringCMUAT.GetDocumentRevisionsResponse_element();

        new SpringCMUAT.ArrayOfSCMDocument();
        new SpringCMUAT.SCMDocument();
    }
}