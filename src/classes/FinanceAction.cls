/*
 * NAIS-499, SPEC-047 Finance - PFS Sale, R-246
 *  When an application is submitted for the first time, if an opportunity of type Application Fee does not already exist on the PFS record,
 *  it needs to be created, and a transaction line item for the cost of the Application needs to be created as well. This would be a
 *  transaction line item of type PFS Sale.
 *
 * WH, Exponent Partners, 2013
 */
public class FinanceAction {

    public static void createOppAndTransaction(Set<ID> pfsIds) {
        createOppAndTransaction(pfsIds, false);
    }

    // [DP] 07.07.2015 NAIS-1933 newly submitted PFSs only get Opps and TLIs if they have a pre-existing Fee Waiver
    public static List<Application_Fee_Waiver__c> preexistingAFWList;
    public static void createOffAndTransactionOnlyIfWaived(List<PFS__c> submittedPFSs) {
        Set<String> academicYearNames = new Set<String>(); // NAIS-2417 [DP] 05.13.2015
        Set<Id> parentAIds = new Set<Id>();
        Map<String, PFS__c> parentAIdAndAcadYearKeyToPFS = new Map<String, PFS__c>(); // NAIS-2417 [DP] 05.13.2015

        // loop through newly submitted PFSs, gather academic years and parent ids, store custom key made from both
        for (PFS__c pfs : submittedPFSs) {
            String key = pfs.Parent_A__c + '-' + pfs.Academic_Year_Picklist__c;
            academicYearNames.add(pfs.Academic_Year_Picklist__c);
            parentAIds.add(pfs.Parent_A__c);
            parentAIdAndAcadYearKeyToPFS.put(key, pfs);
        }
        // store the pre-existing fee waivers here, they will be re-used in the createOppAndTransaction method
        preexistingAFWList = new List<Application_Fee_Waiver__c>();

        // loop through application fee waivers, create the same style custom key.
        // if the key is in the map, the PFS is pre-waived and we should create an opp and TLI for it
        Set<Id> preWaivedPFSIds = new Set<Id>();
        for (Application_Fee_Waiver__c afw : [Select Id, Contact__c, Academic_Year__c, Academic_Year__r.Name, Status__c, Account__c FROM Application_Fee_Waiver__c
                                              where Contact__c in :parentAIds
                                              AND Academic_Year__r.Name in :academicYearNames
                                              AND Status__c = 'Assigned']) {
            String key = afw.Contact__c + '-' + afw.Academic_Year__r.Name; // NAIS-2417 [DP] 05.13.2015
            
            if (parentAIdAndAcadYearKeyToPFS.keySet().contains(key)) {
                preWaivedPFSIds.add(parentAIdAndAcadYearKeyToPFS.get(key).Id);
                preexistingAFWList.add(afw);
            }
        }

        if (!preWaivedPFSIds.isEmpty()) {
            createOppAndTransaction(preWaivedPFSIds, false);
        }
    }

    /*
     * NAIS-499, SPEC-047 Finance - PFS Sale, R-246
     */
    // [DP] 07.07.2015 NAIS-1933 refactor to allow for preventing insert when we just need a dummy record in code
    public static List<Opportunity> createOppAndTransaction(Set<ID> pfsIds, Boolean preventInsert) { // NAIS-1031: refactor - change param from List to Set
        // create PFS Application Fee Opportunities
        List<Opportunity> oppsToCreate = new List<Opportunity>();

        List<Pfs__c> pfsRecords = PfsSelector.Instance.selectByIdWithOpportunities(pfsIds);
        for (PFS__c pfs : pfsRecords) {
            if (pfs.Opportunities__r == null || pfs.Opportunities__r.size() == 0) {
                // owner cannot be a family portal user because there is a sharing rule on Opp
                Id oppOwnerId = GlobalVariables.isFamilyPortalProfile(UserInfo.getProfileId()) ?
                        GlobalVariables.getIndyOwner() : UserInfo.getUserId();

                oppsToCreate.add(new Opportunity(
                                     Name = pfs.Name + ' - Application Fee',
                                     StageName = 'Open',
                                     CloseDate = Date.today(),
                                     PFS__c = pfs.Id,
                                     Academic_Year_Picklist__c = pfs.Academic_Year_Picklist__c,
                                     Parent_A__c = pfs.Parent_A__c,
                                     OwnerId = oppOwnerId,
                                     RecordTypeId = RecordTypes.pfsApplicationFeeOpportunityTypeId));
            }
        }

        // NAIS-1933 only actually insert opp and create TLIs if boolean isn't set to prevent it
        if (!preventInsert && !oppsToCreate.isEmpty()) {
            insert oppsToCreate;
            FinanceAction.updateSaleTransactionForPFSIds(pfsIds, true);
        }

        return oppsToCreate;
    }

    // NAIS-1933 overloaded method to get the TOTAL amount from the opp if it exists, and from custom settings if it does not
    public static Decimal getAmountTotal(Opportunity opp, String acadYearNameParam, Boolean isKS) {
        return getAmountOrAmountDueFromOpp(opp, acadYearNameParam, isKS, false);
    }

    // NAIS-1933 overloaded method to get the DUE amount from the opp if it exists, and from custom settings if it does not
    public static Decimal getAmountDue(Opportunity opp, String acadYearNameParam, Boolean isKS) {
        return getAmountOrAmountDueFromOpp(opp, acadYearNameParam, isKS, true);
    }

    // [DP] 07.06.2015 new method that determines the Net Amount Due OR Total Amoun from the Opp if it exists, and from custom settings if it does not
    public static Decimal getAmountOrAmountDueFromOpp(Opportunity opp, String acadYearNameParam, Boolean isKS, Boolean getAmountDue) {
        if (opp != null && opp.Id != null) {
            if (getAmountDue) {
                return opp.Net_Amount_Due__c == null ? 0 : opp.Net_Amount_Due__c;
            } else {
                return opp.Amount == null ? 0 : opp.Amount;
            }
        } else {
            Decimal applicationFee;
            String acadYearName = (opp != null && opp.Academic_Year_Picklist__c != null) ? opp.Academic_Year_Picklist__c : acadYearNameParam;
            Application_Fee_Settings__c applicationFeeSettings = Application_Fee_Settings__c.getInstance(acadYearName); // NAIS-2417 [DP] 05.13.2015

            // if only KS schools exist, then it should be discounted
            if (isKS) {
                applicationFee = applicationFeeSettings == null ? 42 : applicationFeeSettings.Discounted_Amount__c.setScale(2);
            }

            // otherwise it's the regular amount
            else {
                applicationFee = applicationFeeSettings == null ? 42 : applicationFeeSettings.Amount__c.setScale(2);
            }
            return applicationFee;
        }
    }

    /*
     * [SL] NAIS-1031 - recalc application fee amount and update the sale transaction line item if it has changed. If
     *      createTransactionLineItem is set to true, then create the transaction line item record first if necessary.
     */
    public static void updateSaleTransactionForPFSIds(Set<Id> pfsIds, boolean createTransactionLineItem) {
        // get the KS school id
        Id ksSchoolId = SchoolPortalSettings.KS_School_Account_Id;

        // keep track of which PFSs have a KS school and which have non-KS schools; also keep track of academic years
        Set<Id> pfsIdsWithKS = new Set<Id>();
        Set<Id> pfsIdsWithNonKS = new Set<Id>();
        for (School_PFS_Assignment__c spa : [SELECT Id, School__c, Applicant__r.PFS__c
                                             FROM School_PFS_Assignment__c
                                             WHERE Applicant__r.PFS__c IN :pfsIds
                                             AND Withdrawn__c != 'Yes']) {
            if ((ksSchoolId != null) && (spa.School__c == ksSchoolId)) {
                pfsIdsWithKS.add(spa.Applicant__r.PFS__c);
            } else {
                pfsIdsWithNonKS.add(spa.Applicant__r.PFS__c);
            }
        }

        //[DP] 2.13.14 these three sets are part of  code to handle Fee Waivers assigned before PFS submission
        //used to look for fee waivers
        //Set<Id> academicYearIds = new Set<Id>();
        Set<String> academicYearNames = new Set<String>(); // NAIS-2417 [DP] 05.13.2015
        Set<Id> parentAIds = new Set<Id>();
        Set<String> parentAIdAcadYearNameKeySet = new Set<String>(); // NAIS-2417 [DP] 05.13.2015


        // iterate through the Opportunities corresponding to the PFS records, and update the opp/sale amount if necessary
        List<Opportunity> opportunitiesToUpdate = new List<Opportunity>();
        List<Transaction_Line_Item__c> transactionLineItemsToUpdate = new List<Transaction_Line_Item__c>();
        for (Opportunity opp : [SELECT Id, Amount, Academic_Year_Picklist__c, PFS__c, PFS__r.Academic_Year_Picklist__c, PFS__r.Parent_A__c,
                                (SELECT Id, Amount__c, Product_Amount__c, Product_Code__c
                                 FROM Transaction_Line_Items__r
                                 WHERE RecordTypeId = :RecordTypes.saleTransactionTypeId
                                         ORDER BY CreatedDate DESC LIMIT 1) // there should only be one sale TLI, but if not make sure to get the latest
                                FROM Opportunity
                                WHERE PFS__c IN :pfsIds
                                AND RecordTypeId = :RecordTypes.pfsApplicationFeeOpportunityTypeId
                                        // AND PFS__r.PFS_Status__c = 'Application Submitted'  // [CH] NAIS-2102 This filter was blocking TLI creation for PFSs not yet submitted
                                        AND PFS__r.Payment_Status__c != 'Paid in Full'
                               ]) {

            //[DP] 2.13.14 these three sets are part of  code to handle Fee Waivers assigned before PFS submission
            //used to look for fee waivers
            //academicYearIds.add(opp.Academic_Year__c);
            academicYearNames.add(opp.Academic_Year_Picklist__c); // NAIS-2417 [DP] 05.13.2015
            parentAIds.add(opp.PFS__r.Parent_A__c);
            parentAIdAcadYearNameKeySet.add(opp.PFS__r.Parent_A__c + '-' + opp.Academic_Year_Picklist__c); // NAIS-2417 [DP] 05.13.2015

            // get the application fee from the fee settings
            Application_Fee_Settings__c applicationFeeSettings = Application_Fee_Settings__c.getInstance(opp.Academic_Year_Picklist__c); // NAIS-2417 [DP] 05.13.2015

            if (applicationFeeSettings != null) {
                Decimal applicationFee;
                String productCode;

                //---------

                // if only KS schools exist, then it should be discounted
                if ((pfsIdsWithKS.contains(opp.PFS__c)) && (!pfsIdsWithNonKS.contains(opp.PFS__c))) {
                    applicationFee = applicationFeeSettings.Discounted_Amount__c.setScale(2);
                    productCode = applicationFeeSettings.Discounted_Product_Code__c;
                }

                // otherwise it's the regular amount
                else {
                    applicationFee = applicationFeeSettings.Amount__c.setScale(2);
                    productCode = applicationFeeSettings.Product_Code__c;
                }


                // ----------


                /*

                // if an application only contains the KS school, then it should be discounted
                if ((pfsIdsWithKS.contains(opp.PFS__c)) && (!pfsIdsWithNonKS.contains(opp.PFS__c))) {
                    applicationFee = applicationFeeSettings.Discounted_Amount__c.setScale(2);
                    productCode = applicationFeeSettings.Discounted_Product_Code__c;
                }
                else {
                    applicationFee = applicationFeeSettings.Amount__c.setScale(2);
                    productCode = applicationFeeSettings.Product_Code__c;
                }
                */

                /*
                // update the opportunity and TLI if the calculated amount is different
                if (opp.Amount != applicationFee) {
                    opp.Amount = applicationFee;
                    opportunitiesToUpdate.add(opp);
                }
                */

                // create a new TLI if the flag is set and a TLI doesn't already exist
                if (opp.Transaction_Line_Items__r.isEmpty()) {
                    if (createTransactionLineItem == true) {
                        Transaction_Line_Item__c tli = new Transaction_Line_Item__c();
                        tli.RecordTypeId = RecordTypes.saleTransactionTypeId;
                        tli.Transaction_Type__c = 'PFS Sale';
                        tli.Transaction_Date__c = System.Today(); // [DP] added per Sara's urgent request 2.6.14, and Kathy's on 4.4.14

                        // [CH] NAIS-1489 Adding auto-population of codes from custom setting
                        Transaction_Settings__c settingRecord = Transaction_Settings__c.getValues(tli.Transaction_Type__c);

                        if (settingRecord != null) {
                            tli.Transaction_Code__c = settingRecord.Transaction_Code__c;
                            tli.Account_Code__c = settingRecord.Account_Code__c;
                            tli.Account_Label__c = settingRecord.Account_Label__c;
                        }

                        tli.Opportunity__c = opp.Id;
                        tli.Product_Amount__c = String.valueOf(applicationFee);
                        tli.Product_Code__c = productCode;
                        tli.Amount__c = applicationFee; // the wf rule will copy this value, but also setting it explicitly in case it's needed before wf rule kicks in
                        transactionLineItemsToUpdate.add(tli);
                    }
                }

                // otherwise just update the existing TLI
                else {
                    Boolean isUpdated = false;
                    Transaction_Line_Item__c tli = opp.Transaction_Line_Items__r[0];
                    if (tli.Product_Amount__c != String.valueOf(applicationFee)) {
                        tli.Product_Amount__c = String.valueOf(applicationFee);
                        tli.Amount__c = applicationFee; // the wf rule will copy this value, but also setting it explicitly in case it's needed before wf rule kicks in
                        isUpdated = true;
                    }
                    if (tli.Product_Code__c != productCode) {
                        tli.Product_Code__c = productCode;
                        isUpdated = true;
                    }
                    if (isUpdated) {
                        transactionLineItemsToUpdate.add(tli);
                    }
                }
            }
        }

        /*
        if (!opportunitiesToUpdate.isEmpty()) {
            update opportunitiesToUpdate;
        }
        */

        if (!transactionLineItemsToUpdate.isEmpty()) {
            upsert transactionLineItemsToUpdate;
        }

        System.debug(LoggingLevel.ERROR, 'TESTINGDrewpreexistingwaivers2 ' + preexistingAFWList);

        //[DP] 2.13.14 START code to handle Fee Waivers assigned before PFS submission
        //used to look for fee waivers
        if (createTransactionLineItem == true) {
            if (preexistingAFWList == null) {
                // list of preexisting fee waivers that go with these new opportunities
                preexistingAFWList = new List<Application_Fee_Waiver__c>();

                for (Application_Fee_Waiver__c afw : [Select Id, Contact__c, Academic_Year__c, Academic_Year__r.Name, Status__c, Account__c FROM Application_Fee_Waiver__c
                                                      where Contact__c in :parentAIds
                                                      AND Academic_Year__r.Name in :academicYearNames
                                                      AND Status__c = 'Assigned']) {
                    String key = afw.Contact__c + '-' + afw.Academic_Year__r.Name; // NAIS-2417 [DP] 05.13.2015
                    if (parentAIdAcadYearNameKeySet.contains(key)) {
                        preexistingAFWList.add(afw);
                    }
                }
            }

            if (!preexistingAFWList.isEmpty()) {
                // this will insert any new TLI fee waivers that need to be created.
                // need to insert them after TLIs upserted above so we can make use of the amounts in the PFS Sale TLI
                ApplicationFeeWaiverAction.createFeeWaiverTransactionLineItems(preexistingAFWList);
            }
        }
        //[DP] 2.13.14 END code to handle Fee Waivers assigned before PFS submission
    }

    public static void updateSaleTransactionForApplicantIds(Set<Id> applicantIds, boolean createTransactionLineItem) {
        Set<Id> pfsIds = new Set<Id>();
        for (Applicant__c applicant : [SELECT Id, PFS__c FROM Applicant__c WHERE Id IN :applicantIds]) {
            if (applicant.PFS__c != null) {
                pfsIds.add(applicant.PFS__c);
            }
        }
        if (!pfsIds.isEmpty()) {
            FinanceAction.updateSaleTransactionForPFSIds(pfsIds, createTransactionLineItem);
        }
    }

    // [DP] 07.07.2015 NAIS-1933 three overloaded methods to determine if this PFS is a KS-Only PFS
    public static Boolean isKSOnlyPFS(PFS__c thePFS) {
        return isKSOnlyPFS([SELECT Id, School__c, School__r.Name, Name, Student_Folder__r.New_Returning__c FROM School_PFS_Assignment__c WHERE Applicant__r.PFS__c = :thePfs.Id AND Withdrawn__c != 'Yes']);
    }

    public static Boolean isKSOnlyPFS(Set<Id> schoolIds) {
        List<School_PFS_Assignment__c> spaList = new List<School_PFS_Assignment__c>();
        for (Id schoolId : schoolIds) {
            spaList.add(new School_PFS_Assignment__c(School__c = schoolId));
        }
        return isKSOnlyPFS(spaList);
    }

    public static Boolean isKSOnlyPFS(List<School_PFS_Assignment__c> spaList) {
        Id ksSchoolId = SchoolPortalSettings.KS_School_Account_Id;

        Boolean hasKS = false;
        Boolean hasNonKS = false;

        // get list of SPAs, excluding those that are withdrawn
        for (School_PFS_Assignment__c spa : spaList) {
            if (spa.School__c != null && spa.School__c == ksSchoolId) {
                hasKS = true;
            } else {
                hasNonKS = true;
            }
        }

        return hasKS && !hasNonKS;
    }
}