public with sharing class SchoolFWPagerController {
    
    public SchoolFWPagerController() {
        
    }

    public SchoolFeeWaiversController SchoolFeeCtrl { get; set; }

    /* Interface for paging. */

    // Boolean to check if there are more records after the present displaying records
    public Boolean hasNext { get { return SchoolFeeCtrl.AssignedSetCtrl.getHasNext(); } set; }
    // Boolean to check if there are more records before the present displaying records
    public Boolean hasPrevious { get { return SchoolFeeCtrl.AssignedSetCtrl.getHasPrevious(); } set; }
    //Returns the previous page of records
    public void previous() { SchoolFeeCtrl.previous(); }
    //Returns the next page of records
    public void next() { SchoolFeeCtrl.next(); }
    // Page number of the current displaying records
    public Integer pageNumber { get { return SchoolFeeCtrl.AssignedSetCtrl.getPageNumber(); } set; }
    
    public Integer pageSize {
        get { return SchoolFeeCtrl.AssignedSetCtrl.getPageSize(); }
        set { SchoolFeeCtrl.AssignedSetCtrl.setPageSize(value); }
    }
}