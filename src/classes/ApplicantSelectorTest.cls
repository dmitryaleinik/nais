@isTest
private class ApplicantSelectorTest {
    
    @isTest
    private static void selectByPFSandStudent_expectRecordReturned() {
        Applicant__c record = ApplicantTestData.Instance.DefaultApplicant;

        Test.startTest();
            List<Applicant__c> applicants = ApplicantSelector.newInstance().selectByPFSandStudent(
                new Set<Id>{PfsTestData.Instance.DefaultPfs.Id}, new Set<Id>{ContactTestData.Instance.DefaultContact.Id});
        Test.stopTest();

        System.assertEquals(1, applicants.size());
        System.assertEquals(PfsTestData.Instance.DefaultPfs.Id, applicants[0].Pfs__c);
        System.assertEquals(ContactTestData.Instance.DefaultContact.Id, applicants[0].Contact__c);
    }
    
    @isTest
    private static void selectByPFSandStudent_expectException() {
        Applicant__c record = ApplicantTestData.Instance.DefaultApplicant;

        Test.startTest();
            try {
                ApplicantSelector.newInstance().selectByPFSandStudent(null, new Set<Id>{ContactTestData.Instance.DefaultContact.Id});

                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, ApplicantSelector.PFS_ID);
            }

            try {
                ApplicantSelector.newInstance().selectByPFSandStudent(new Set<Id>{PfsTestData.Instance.DefaultPfs.Id}, null);

                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, ApplicantSelector.STUDENT_ID);
            }
        Test.stopTest();
    }

    @isTest
    private static void selectAll_expectRecordReturned()
    {
        Applicant__c applicant = ApplicantTestData.Instance.DefaultApplicant;

        Test.startTest();
            List<Applicant__c> records = ApplicantSelector.newInstance().selectAll();
            
            System.assert(!records.isEmpty(), 'Expected the returned list to not be empty.');
            System.assertEquals(1, records.size(), 'Expected there to be one applicant record returned.');
            System.assertEquals(applicant.Id, records[0].Id, 'Expected the Ids of the applicant records to match.');
        Test.stopTest();
    }
}