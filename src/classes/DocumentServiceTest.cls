@isTest
private class DocumentServiceTest {

    private static final String DOC_TYPE_1040 = '1040';
    private static final String DOC_TYPE_W2 = 'W2';

    private static String currentDocumentYear;
    private static String previousDocumentYear;

    private static String getCurrentDocumentYear() {
        if (currentDocumentYear != null) {
            return currentDocumentYear;
        }

        currentDocumentYear = GlobalVariables.getDocYearStringFromAcadYearName(GlobalVariables.getCurrentYearString());

        return currentDocumentYear;
    }

    private static String getPreviousDocumentYear() {
        if (previousDocumentYear != null) {
            return previousDocumentYear;
        }

        previousDocumentYear = GlobalVariables.getPrevDocYearStringFromAcadYearName(GlobalVariables.getCurrentYearString());

        return previousDocumentYear;
    }

    public class TestDataLocal {
        Id academicYearId;
        String academicYearName;
        Account school1, school2;
        Contact staff1, parentA, parentB, student1, student2;
        Applicant__c applicant1A;
        PFS__c pfs1;
        Student_Folder__c studentFolder1, studentFolder2;
        School_PFS_Assignment__c spfsa1, spfsa2;
        Household__c hh;

        public TestDataLocal() {
            // academic year
            TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
            academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
            academicYearName = GlobalVariables.getCurrentAcademicYear().Name;

            // school
            school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, 1, false);
            school1.SSS_School_Code__c = '1234';
            school2 = TestUtils.createAccount('school2', RecordTypes.schoolAccountTypeId, 1, false);
            school2.SSS_School_Code__c = '5678';
            insert new List<Account> { school1, school2 };

            // household
            hh = TestUtils.createHousehold('Test Household', true);

            // parents and students
            parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
            parentA.Household__c = hh.Id;
            parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
            student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
            student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
            insert new List<Contact> {parentA, parentB, student1, student2};

            // psf
            pfs1 = TestUtils.createPFS('PFS A', academicYearName, parentA.Id, false);
            insert new List<PFS__c> {pfs1};

            // applicant
            applicant1A = TestUtils.createApplicant(student1.Id, pfs1.Id, false);
            insert new List<Applicant__c> {applicant1A};

            // student folder
            studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearName, student1.Id, false);
            studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', academicYearName, student1.Id, false);
            insert new List<Student_Folder__c> { studentFolder1, studentFolder2};

            // school pfs assignment
            spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearName, applicant1A.Id, school1.Id, studentFolder1.Id, false);
            spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearName, applicant1A.Id, school2.Id, studentFolder2.Id, false);
            insert new List<School_PFS_Assignment__c> { spfsa1, spfsa2 };

            // create pertains to mapping
            SpringCMActionTest.setupMappings();

            insert new Global_Message__c(Portal__c='School Portal; School Login; Spring CM - Maintenance Window', Display_End_Date_Time__c = system.now().addDays(1), Display_Start_Date_Time__c = system.now().addDays(-1));
        }
    }

    private static List<School_Document_Assignment__c> createSDAsForSchool(Id schoolPfsAssignmentId, String docType, Integer numberToCreate) {
        List<School_Document_Assignment__c> newSDAs = new List<School_Document_Assignment__c>();

        for (Integer i = 0; i < numberToCreate; i++) {
            School_Document_Assignment__c newSDA = TestUtils.createSchoolDocumentAssignment('SDA Name ' + i, null, null, schoolPfsAssignmentId, false);
            newSDA.Document_Type__c = docType;
            newSDAs.add(newSDA);
        }

        return newSDAs;
    }

    private static List<School_Document_Assignment__c> insertSDAsForSchool(Id schoolPfsAssignmentId, String docType, Integer numberToInsert) {
        List<School_Document_Assignment__c> newSDAs = createSDAsForSchool(schoolPfsAssignmentId, docType, numberToInsert);

        DML.WithoutSharing.insertObjects(newSDAs);

        return newSDAs;
    }

    private static List<School_Document_Assignment__c> querySDAs(List<School_Document_Assignment__c> sdasToQuery) {
        Map<Id, School_Document_Assignment__c> sdasById = new Map<Id, School_Document_Assignment__c>(sdasToQuery);

        Set<Id> sdaIds = sdasById.keySet();

        return [SELECT Id, Document__c FROM School_Document_Assignment__c WHERE Id IN :sdaIds];
    }

    private static void assertSDAsHaveFamilyDocument(List<School_Document_Assignment__c> sdasToCheck, Id expectedDocumentId) {
        for (School_Document_Assignment__c sda : sdasToCheck) {
            System.assertEquals(expectedDocumentId, sda.Document__c,
                    'Expected the School Document Assignment to have the specified family document Id.');
        }
    }

    @isTest
    private static void updateDocumentAssignments_noSdasForSchools_1040Inserted_expectNoSDAsUpdated() {
        TestDataLocal testData = new TestDataLocal();

        String docType = DOC_TYPE_1040;
        String docYear = getCurrentDocumentYear();

        Family_Document__c newDocument = TestUtils.createFamilyDocument('Test Document', testData.academicYearName, docType, testData.hh.Id, 'Parent A', null, false);
        newDocument.Document_Year__c = docYear;
        insert newDocument;

        DocumentService.SDAUpdateRequest request =
                new DocumentService.SDAUpdateRequest(testData.pfs1.Id, newDocument.Id, docType, docYear, null, null, null);

        DocumentService.Instance.updateDocumentAssignments(request);

        List<School_Document_Assignment__c> sdasToCheck = [SELECT Id, Document__c FROM School_Document_Assignment__c WHERE Document__c = :newDocument.Id];
        System.assert(sdasToCheck.isEmpty(), 'Expected no SDAs to have been updated for the new family document.');
    }

    @isTest
    private static void updateDocumentAssignments_sdaForOneSchoolExpectingW2_w2FamilyDocInserted_expectSDAUpdated() {
        TestDataLocal testData = new TestDataLocal();

        String docType = DOC_TYPE_W2;
        String docYear = getCurrentDocumentYear();

        // Insert SDAs so that one school will be expecting a W2.
        List<School_Document_Assignment__c> docAssignments = insertSDAsForSchool(testData.spfsa1.Id, docType, 1);

        // Insert new W2.
        Family_Document__c newDocument = TestUtils.createFamilyDocument('Test Document', testData.academicYearName, docType, testData.hh.Id, 'Parent A', null, false);
        newDocument.Document_Year__c = docYear;
        insert newDocument;

        DocumentService.SDAUpdateRequest request =
                new DocumentService.SDAUpdateRequest(testData.pfs1.Id, newDocument.Id, docType, docYear, null, null, null);

        DocumentService.Instance.updateDocumentAssignments(request);

        // Query SDAs to see if they were updated.
        docAssignments = querySDAs(docAssignments);

        assertSDAsHaveFamilyDocument(docAssignments, newDocument.Id);
    }

    @isTest
    private static void updateDocumentAssignments_sdaForTwoSchoolsExpectingW2_w2FamilyDocInserted_expectSDAsUpdated() {
        TestDataLocal testData = new TestDataLocal();

        String docType = DOC_TYPE_W2;
        String docYear = getCurrentDocumentYear();

        // Insert SDAs so that two schools will be expecting a W2.
        List<School_Document_Assignment__c> docAssignments = new List<School_Document_Assignment__c>();
        docAssignments.addAll(createSDAsForSchool(testData.spfsa1.Id, docType, 1));
        docAssignments.addAll(createSDAsForSchool(testData.spfsa2.Id, docType, 1));
        insert docAssignments;

        // Insert new W2.
        Family_Document__c newDocument = TestUtils.createFamilyDocument('Test Document', testData.academicYearName, docType, testData.hh.Id, 'Parent A', null, false);
        newDocument.Document_Year__c = docYear;
        insert newDocument;

        DocumentService.SDAUpdateRequest request =
                new DocumentService.SDAUpdateRequest(testData.pfs1.Id, newDocument.Id, docType, docYear, null, null, null);

        DocumentService.Instance.updateDocumentAssignments(request);

        // Query SDAs to see if they were updated.
        docAssignments = querySDAs(docAssignments);

        assertSDAsHaveFamilyDocument(docAssignments, newDocument.Id);
    }

    @isTest
    private static void updateDocumentAssignments_sdasForTwoSchoolsExpectingW2_1040FamilyDocInserted_expectSDAsNotUpdated() {
        TestDataLocal testData = new TestDataLocal();

        String sdaDocType = DOC_TYPE_W2;
        String familyDocType = DOC_TYPE_1040;
        String docYear = getCurrentDocumentYear();

        // Insert SDAs so that two schools will be expecting a W2.
        List<School_Document_Assignment__c> docAssignments = new List<School_Document_Assignment__c>();
        docAssignments.addAll(createSDAsForSchool(testData.spfsa1.Id, sdaDocType, 1));
        docAssignments.addAll(createSDAsForSchool(testData.spfsa2.Id, sdaDocType, 1));
        insert docAssignments;

        // Insert new 1040.
        Family_Document__c newDocument = TestUtils.createFamilyDocument('Test Document', testData.academicYearName, familyDocType, testData.hh.Id, 'Parent A', null, false);
        newDocument.Document_Year__c = docYear;
        insert newDocument;

        DocumentService.SDAUpdateRequest request =
                new DocumentService.SDAUpdateRequest(testData.pfs1.Id, newDocument.Id, familyDocType, docYear, null, null, null);

        DocumentService.Instance.updateDocumentAssignments(request);

        // Query SDAs to see if they were updated.
        docAssignments = querySDAs(docAssignments);

        // Make sure none of the SDAs were updated by asserting against null.
        assertSDAsHaveFamilyDocument(docAssignments, null);
    }

    @isTest
    private static void updateDocumentAssignments_sdasForTwoSchoolsExpecting1040_1040FamilyDocInserted_expectSDAsUpdated() {
        TestDataLocal testData = new TestDataLocal();

        String sdaDocType = DOC_TYPE_1040;
        String familyDocType = DOC_TYPE_1040;
        String docYear = getCurrentDocumentYear();

        // Insert SDAs so that two schools will be expecting a 1040.
        List<School_Document_Assignment__c> docAssignments = new List<School_Document_Assignment__c>();
        docAssignments.addAll(createSDAsForSchool(testData.spfsa1.Id, sdaDocType, 1));
        docAssignments.addAll(createSDAsForSchool(testData.spfsa2.Id, sdaDocType, 1));
        insert docAssignments;

        // Insert new W2.
        Family_Document__c newDocument = TestUtils.createFamilyDocument('Test Document', testData.academicYearName, familyDocType, testData.hh.Id, 'Parent A', null, false);
        newDocument.Document_Year__c = docYear;
        insert newDocument;

        DocumentService.SDAUpdateRequest request =
                new DocumentService.SDAUpdateRequest(testData.pfs1.Id, newDocument.Id, familyDocType, docYear, null, null, null);

        DocumentService.Instance.updateDocumentAssignments(request);

        // Query SDAs to see if they were updated.
        docAssignments = querySDAs(docAssignments);

        // Make sure none of the SDAs were updated by asserting against null.
        assertSDAsHaveFamilyDocument(docAssignments, newDocument.Id);
    }

    @isTest
    private static void updateDocumentAssignments_oneSchoolExpectingTwo1040s_marriedFilingSeparately_1040FamilyDocInserted_expectOneSDAUpdated() {
        TestDataLocal testData = new TestDataLocal();

        String sdaDocType = DOC_TYPE_1040;
        String familyDocType = DOC_TYPE_1040;
        String docYear = getCurrentDocumentYear();

        // Insert SDAs so that one school will be expecting two 1040s for the current document year.
        // This mimics the scenario that we see when parents are married but file taxes separately.
        List<School_Document_Assignment__c> docAssignments = insertSDAsForSchool(testData.spfsa1.Id, sdaDocType, 2);

        // Insert new 1040.
        Family_Document__c newDocument = TestUtils.createFamilyDocument('Test Document', testData.academicYearName, familyDocType, testData.hh.Id, 'Parent A', null, false);
        newDocument.Document_Year__c = docYear;
        insert newDocument;

        DocumentService.SDAUpdateRequest request =
                new DocumentService.SDAUpdateRequest(testData.pfs1.Id, newDocument.Id, familyDocType, docYear, null, null, null);

        DocumentService.Instance.updateDocumentAssignments(request);

        // Query SDAs to see if they were updated.
        docAssignments = querySDAs(docAssignments);

        List<School_Document_Assignment__c> updatedSDAs = new List<School_Document_Assignment__c>();
        List<School_Document_Assignment__c> unchangedSDAs = new List<School_Document_Assignment__c>();
        for (School_Document_Assignment__c sda : docAssignments) {
            if (sda.Document__c == newDocument.Id) {
                updatedSDAs.add(sda);
            } else if (sda.Document__c == null) {
                unchangedSDAs.add(sda);
            }
        }

        System.assertEquals(1, updatedSDAs.size(), 'Expected one SDA to be updated.');
        System.assertEquals(1, unchangedSDAs.size(), 'Expected one SDA to be unchanged.');
    }

    @isTest
    private static void updateDocumentAssignments_schoolWithSpecificDocType_insertSpecificDoc_expectSDAUpdated() {
        TestDataLocal testData = new TestDataLocal();

        String sdaDocType = ApplicationUtils.schoolSpecificDocType();
        String familyDocType = ApplicationUtils.schoolSpecificDocType();
        String docYear = getCurrentDocumentYear();

        String schoolSpecificDocumentLabel = 'ThisIsAVerySpecificTypeOfDocument';

        // Insert a required document since school specific documents rely on these records.
        Required_Document__c docRequirement = TestUtils.createRequiredDocument(testData.academicYearId, testData.school1.Id, false);
        docRequirement.Label_for_School_Specific_Document__c = schoolSpecificDocumentLabel;
        docRequirement.Document_Year__c = docYear;
        docRequirement.Document_Type__c = ApplicationUtils.schoolSpecificDocType();
        insert docRequirement;

        List<School_Document_Assignment__c> docAssignments = [SELECT Id, Document__c FROM School_Document_Assignment__c WHERE Required_Document__c = :docRequirement.Id];
        System.assert(!docAssignments.isEmpty(), 'Expected SDAs to be auto generated by inserting a document requirement.');

        // Insert new document for the school specific type.
        Family_Document__c newDocument = TestUtils.createFamilyDocument('Test Document', testData.academicYearName, familyDocType, testData.hh.Id, 'Parent A', null, false);
        newDocument.Document_Year__c = docYear;
        insert newDocument;

        // This time create a request where we specify the school code and specific document type name
        DocumentService.SDAUpdateRequest request =
                new DocumentService.SDAUpdateRequest(
                    testData.pfs1.Id,
                    newDocument.Id,
                    familyDocType,
                    docYear,
                    testData.school1.SSS_School_Code__c,
                    schoolSpecificDocumentLabel,
                    null);

        DocumentService.Instance.updateDocumentAssignments(request);

        docAssignments = querySDAs(docAssignments);

        assertSDAsHaveFamilyDocument(docAssignments, newDocument.Id);
    }

    @isTest
    private static void updateDocumentAssignments_twoApplicantsToSameSchool_parentsFilingJointly_expectAll1040SDAsUpdated() {
        TestDataLocal testData = new TestDataLocal();

        testData.pfs1.Filing_Status__c = 'Married, Filing Jointly';
        update testData.pfs1;

        // Insert a second applicant and an SPA for the same school as applicant 1.
        Applicant__c applicant2 = TestUtils.createApplicant(testData.student2.Id, testData.pfs1.Id, true);

        Student_Folder__c folder =
                TestUtils.createStudentFolder('Student Folder 3', testData.academicYearName, testData.student2.Id, true);

        School_PFS_Assignment__c spaForApplicant2 =
                TestUtils.createSchoolPFSAssignment(testData.academicYearName, applicant2.Id, testData.school1.Id, folder.Id, true);

        String sdaDocType = DOC_TYPE_1040;
        String familyDocType = DOC_TYPE_1040;
        String docYear = getCurrentDocumentYear();

        // Create two SDAs for 1040s for the same school since that is what we will expect for scenarios where there are two applicants going to the same school.
        List<School_Document_Assignment__c> docAssignments = createSDAsForSchool(testData.spfsa1.Id, sdaDocType, 1);
        docAssignments.addAll(createSDAsForSchool(spaForApplicant2.Id, sdaDocType, 1));
        insert docAssignments;

        // Insert new 1040.
        Family_Document__c newDocument = TestUtils.createFamilyDocument('Test Document', testData.academicYearName, familyDocType, testData.hh.Id, 'Parent A', null, false);
        newDocument.Document_Year__c = docYear;
        insert newDocument;

        DocumentService.SDAUpdateRequest request =
                new DocumentService.SDAUpdateRequest(testData.pfs1.Id, newDocument.Id, familyDocType, docYear, null, null, null);

        DocumentService.Instance.updateDocumentAssignments(request);

        // Query SDAs to see if they were updated.
        docAssignments = querySDAs(docAssignments);

        List<School_Document_Assignment__c> updatedSDAs = new List<School_Document_Assignment__c>();
        List<School_Document_Assignment__c> unchangedSDAs = new List<School_Document_Assignment__c>();
        for (School_Document_Assignment__c sda : docAssignments) {
            if (sda.Document__c == newDocument.Id) {
                updatedSDAs.add(sda);
            } else if (sda.Document__c == null) {
                unchangedSDAs.add(sda);
            }
        }

        System.assertEquals(2, updatedSDAs.size(), 'Expected both SDAs to be updated.');
        System.assertEquals(0, unchangedSDAs.size(), 'Expected zero SDAs to be unchanged.');
    }

    @isTest
    private static void updateDocumentAssignments_twoApplicantsToSameSchool_parentsFilingSeparately_expectHalfOf1040SDAsUpdated() {
        TestDataLocal testData = new TestDataLocal();

        testData.pfs1.Filing_Status__c = 'Married, Filing Separately';
        update testData.pfs1;

        // Insert a second applicant and an SPA for the same school as applicant 1.
        Applicant__c applicant2 = TestUtils.createApplicant(testData.student2.Id, testData.pfs1.Id, true);

        Student_Folder__c folder =
                TestUtils.createStudentFolder('Student Folder 3', testData.academicYearName, testData.student2.Id, true);

        School_PFS_Assignment__c spaForApplicant2 =
                TestUtils.createSchoolPFSAssignment(testData.academicYearName, applicant2.Id, testData.school1.Id, folder.Id, true);

        String sdaDocType = DOC_TYPE_1040;
        String familyDocType = DOC_TYPE_1040;
        String docYear = getCurrentDocumentYear();

        // Create two SDAs for 1040s for the same school since that is what we will expect for scenarios where there
        // are two applicants going to the same school and their parents file separately.
        List<School_Document_Assignment__c> docAssignments = createSDAsForSchool(testData.spfsa1.Id, sdaDocType, 2);
        docAssignments.addAll(createSDAsForSchool(spaForApplicant2.Id, sdaDocType, 2));
        insert docAssignments;

        // Insert new 1040.
        Family_Document__c newDocument = TestUtils.createFamilyDocument('Test Document', testData.academicYearName, familyDocType, testData.hh.Id, 'Parent A', null, false);
        newDocument.Document_Year__c = docYear;
        insert newDocument;

        DocumentService.SDAUpdateRequest request =
                new DocumentService.SDAUpdateRequest(testData.pfs1.Id, newDocument.Id, familyDocType, docYear, null, null, null);

        DocumentService.Instance.updateDocumentAssignments(request);

        // Query SDAs to see if they were updated.
        docAssignments = querySDAs(docAssignments);

        List<School_Document_Assignment__c> updatedSDAs = new List<School_Document_Assignment__c>();
        List<School_Document_Assignment__c> unchangedSDAs = new List<School_Document_Assignment__c>();
        for (School_Document_Assignment__c sda : docAssignments) {
            if (sda.Document__c == newDocument.Id) {
                updatedSDAs.add(sda);
            } else if (sda.Document__c == null) {
                unchangedSDAs.add(sda);
            }
        }

        // We only expect 2 to be updated since we update 1 SDA per applicant per school.
        System.assertEquals(2, updatedSDAs.size(), 'Expected two SDAs to be updated.');
        System.assertEquals(2, unchangedSDAs.size(), 'Expected two SDAs to be unchanged.');
    }
}