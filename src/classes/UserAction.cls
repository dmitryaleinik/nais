/*
 * Spec-074 School Portal - Delegated User Administration; Req# R-109, R-343
 *    Allow school Admins to utilize the delegated user administration feature, but cap the number of licenses they can delegate based on the
 *    Max_Number_of_Users__c field for the license count on the Account, and a custom setting. The custom setting has the default that can be
 *    overwritten on the Account.
 *
 * WH, Exponent Partners, 2013
 */
public class UserAction {
    private static final String GOLD_PARTNER_LICENSE_DEFINITION_KEY = 'PID_STRATEGIC_PRM';

    // NAIS-2476
    private static Set<String> acctPfsActAlerts = new Set<String>{'Daily digest of new applications', 'Weekly digest of new applications'};

    // [CH] NAIS-1766 Action for adding Users to Groups
    public static void addUsersToGroups(List<Id> usersIdList){
    //public static void addUsersToGroups(List<User> usersToAdd){
        List<String> groupNameList = new List<String>();

        List<User> usersToAdd = [select Id, accountId, contactId from User where Id in :usersIdList];
        List<Id> contactIds = new List<Id>();

        // Collect the list of AccountIds to use for querying Groups
        //  and contactIds for querying affiliations
        for(User userRecord : usersToAdd){
            groupNameList.add('X' + userRecord.AccountId);
            contactIds.add(userRecord.contactId);
        }

        // Look for related Affiliation records and add their
        //  Organizations' groups to the list for querying
        Map<Id, Contact> contactsWithAffiliations = new Map<Id, Contact>(
                                                        [select Id,
                                                            (select Id, Organization__c, Contact__c
                                                                from Affiliations__r
                                                                where Status__c = 'Current'
                                                                and Type__c != 'Relationship')
                                                            from Contact
                                                            where Id in :contactIds]);

        for(Contact contactRecord : contactsWithAffiliations.values()){
            for(Affiliation__c affiliation : contactRecord.Affiliations__r){
                groupNameList.add('X' + affiliation.Organization__c);
            }
        }

        // Query groups based on DeveloperName and add to a Map
        Map<String, Id> groupsMap = new Map<String, Id>();
        for(Group groupRecord : [select Id, Name, DeveloperName from Group where DeveloperName in :groupNameList]){
            groupsMap.put(groupRecord.DeveloperName, groupRecord.Id);
        }

        // Match users up to groups and create new GroupMembers to be inserted
        List<GroupMember> groupMembersToInsert = new List<GroupMember>();
        for(User user : usersToAdd){
            Id matchingGroupId = groupsMap.get('X' + user.AccountId);
            if(matchingGroupId != null){
                groupMembersToInsert.add(new GroupMember(GroupId = matchingGroupId, UserOrGroupId = user.Id));
            }

            // Query for groups to be added based on Contact's affiliations
            Contact relatedContact = contactsWithAffiliations.get(user.contactId);
            if(relatedContact != null){
                for(Affiliation__c affiliation : relatedContact.Affiliations__r){
                    Id affiliationGroupId = groupsMap.get('X' + affiliation.Organization__c);
                    if(affiliationGroupId != null){
                        groupMembersToInsert.add(new GroupMember(GroupId = affiliationGroupId, UserOrGroupId = user.Id));
                    }
                }
            }
        }

        // If there are GroupMembers to insert
        Database.insert(groupMembersToInsert);
    }

    // [CH] NAIS-1766 Action for adding Users to Groups
    public static void removeUsersFromGroups(List<User> usersToRemove){

        // Collect the list of Account and User ids to use for querying GroupMembers
        List<String> groupNameList = new List<String>();
        List<Id> userList = new List<Id>();
        List<Id> contactIds = new List<Id>();

        for(User user : usersToRemove){
            groupNameList.add('X' + user.AccountId);
            userList.add(user.Id);
            contactIds.add(user.contactId);
        }

        // Look for related Affiliation records and add their
        //  Organizations' groups to the list for querying
        Map<Id, Contact> contactsWithAffiliations = new Map<Id, Contact>(
                                                        [select Id,
                                                            (select Id, Organization__c, Contact__c
                                                                from Affiliations__r
                                                                where Status__c = 'Current'
                                                                and Type__c != 'Relationship')
                                                            from Contact
                                                            where Id in :contactIds]);

        for(Contact contactRecord : contactsWithAffiliations.values()){
            for(Affiliation__c affiliation : contactRecord.Affiliations__r){
                groupNameList.add('X' + affiliation.Organization__c);
            }
        }

        Map<Id, GroupMember> groupMembersToRemove = new Map<Id, GroupMember>([select Id from GroupMember where Group.DeveloperName in :groupNameList and UserOrGroupId in :userList]);
        // Deleting records
        Database.delete(new List<Id>(groupMembersToRemove.keySet()));
    }

    /*
     * Spec-074 School Portal - Delegated User Administration; Req# R-109, R-343
     */
    public static void checkSchoolPortalUserLimit(List<User> users, Set<Id> contactIds) {
        // get global default limit from custom settings
        Integer defaultMax = SchoolPortalSettings.Max_Number_of_Users;

        // find the set of accounts from the contactId on the users bcoz AccountId on User is not set before insert
        Map<Id, Id> accountIdByContactId = new Map<Id, Id>();
        Set<Id> accountIds = new Set<Id>();
        for (Contact c : [select Id, AccountId from Contact where Id in :contactIds]) {
            accountIdByContactId.put(c.Id, c.AccountId);
            accountIds.add(c.AccountId);
        }

        // find max # of partner portal users allowed for each school, value on account if any overrides default in custom settings
        Map<Id, Integer> maxBySchoolId = new Map<Id, Integer>();
        for (Account a : [select Id, Max_Number_of_Users__c from Account
                            where Id in :accountIds and (RecordTypeId = :RecordTypes.accessOrgAccountTypeId or RecordTypeId = :RecordTypes.schoolAccountTypeId)]) {
            maxBySchoolId.put(a.Id, (a.Max_Number_of_Users__c != null) ? a.Max_Number_of_Users__c.intValue() : defaultMax);
        }

        System.debug('>>>>>>>>>> maxBySchoolId: ' + maxBySchoolId);

        // find # of portal user licenses remaining for each school beyond existing active users
        for(AggregateResult ar: [select AccountId, count(Id) numUsers from User
                                    where AccountId in :accountIds and IsPortalEnabled = true and IsActive = true
                                    group by AccountId]) {
            Id acctId = (Id) ar.get('AccountId');
            if (maxBySchoolId.containsKey(acctId)) {
                maxBySchoolId.put(acctId, maxBySchoolId.get(acctId) - (Integer) ar.get('numUsers'));
            }
        }

        System.debug('>>>>>>>>>> maxBySchoolId: ' + maxBySchoolId);

        // loop through new portal users to be inserted and determine if limit would be exceeded for the related school
        Integer remainingCount;
        for (User u : users) {
            if (u.ProfileId != GlobalVariables.familyPortalProfileId){
                System.debug('TESTING55 + throwing error?');
                Id accountId = accountIdByContactId.get(u.ContactId);
                if (maxBySchoolId.containsKey(accountId)) {
                    remainingCount = maxBySchoolId.get(accountId) - 1;
                    maxBySchoolId.put(accountId, remainingCount);
                    if (remainingCount < 0)    // limit exceeded
                        u.AddError('Maximum number of portal users exceeded for this school account');
                } // else, not a school portal account subject to these limits
            }
        }

    }

    /**
     * SPEC-108: School Portal - Feature Version, R-160, R-159, 60 hours [Drew]
     * For schools which have the full feature view, it is as speced. For schools which have the limited feature view,
     * they will need to have a different nav so they won't be able to see most of the admin functions.
     * The NAIS Administrator should be able to set at a Global level whether their school sees the full or basic version.
     *
     * DP Exponent Partners, 2013
     */
    public static void updateUserProfiles(Set<Id> accountIdsNewlyBasic, Set<Id> accountIdsNewlyFull) {
        if (System.isBatch() || System.isFuture() || System.isQueueable()) {
            updateUserProfilesSync(accountIdsNewlyBasic, accountIdsNewlyFull);
        } else {
            updateUserProfilesFuture(accountIdsNewlyBasic, accountIdsNewlyFull);
        }
    }

    @future
    private static void updateUserProfilesFuture(Set<Id> accountIdsNewlyBasic, Set<Id> accountIdsNewlyFull) {
        updateUserProfilesSync(accountIdsNewlyBasic, accountIdsNewlyFull);
    }

    private static void updateUserProfilesSync(Set<Id> accountIdsNewlyBasic, Set<Id> accountIdsNewlyFull) {
        List<User> usersToUpdate = new List<User>();

        accountIdsNewlyBasic = accountIdsNewlyBasic != null ? accountIdsNewlyBasic : new Set<Id>();
        accountIdsNewlyFull = accountIdsNewlyFull != null ? accountIdsNewlyFull : new Set<Id>();

        Set<Id> accountIds = new Set<Id>();
        accountIds.addAll(accountIdsNewlyFull);
        accountIds.addAll(accountIdsNewlyBasic);

        // This is needed, otherwise we get a field integrity exception when updating the current user's profile id
        Set<Id> invalidUserIds = new Set<Id>();
        if (System.isBatch() || System.isFuture() || System.isQueueable()) {
            invalidUserIds.add(UserInfo.getUserId());
        }

        for (User u : [SELECT Id, ProfileId, ContactId, Contact.AccountId FROM User WHERE Id NOT IN :invalidUserIds AND Contact.AccountId IN :accountIds AND Profile.UserLicense.LicenseDefinitionKey = :getSchoolPortalLicenseKey()]){
            if (accountIdsNewlyBasic.contains(u.Contact.AccountId) && u.ProfileId != GlobalVariables.schoolPortalBasicAdminProfileId && u.ProfileId != GlobalVariables.schoolPortalBasicUserProfileId) {
                // if currently an admin, set to admin. else, set to user
                // [DP] custom settings refactor - NAIS-1498 2.5.14
                //u.ProfileId = u.ProfileId == GlobalVariables.schoolPortalAdminProfileId ? GlobalVariables.schoolPortalBasicAdminProfileId : GlobalVariables.schoolPortalBasicUserProfileId;
                u.ProfileId = GlobalVariables.isSchoolPortalAdminProfile(u) ? GlobalVariables.schoolPortalBasicAdminProfileId : GlobalVariables.schoolPortalBasicUserProfileId;
                usersToUpdate.add(u);
            } else if (accountIdsNewlyFull.contains(u.Contact.AccountId) && u.ProfileId != GlobalVariables.schoolPortalAdminProfileId && u.ProfileId != GlobalVariables.schoolPortalUserProfileId) {
                // if currently an admin, set to admin. else, set to user
                // [DP] custom settings refactor - NAIS-1498 2.5.14
                //u.ProfileId = u.ProfileId == GlobalVariables.schoolPortalBasicAdminProfileId ? GlobalVariables.schoolPortalAdminProfileId : GlobalVariables.schoolPortalUserProfileId;
                u.ProfileId = GlobalVariables.isSchoolPortalAdminProfile(u) ? GlobalVariables.schoolPortalAdminProfileId : GlobalVariables.schoolPortalUserProfileId;
                usersToUpdate.add(u);
            }
        }

        if (!usersToUpdate.isEmpty()){
            update usersToUpdate;
        }
    }

/*
 * NAISNAIS-766
 * SpringCM: Keep user email in synch
 *
 * Update user email in Spring via API when email changes in SF on the User record (trigger on User).
 *
 * DP, Exponent Partners, 2013
 */
     public static String workflowResult;

    // future method to send xml with email updates to SpringCM
    @Future (callout=true)
    public static void sendEmailUpdateXMLToSpring(Map<String, String> newEmailToOldemailMap, String sessionId){
        sendEmailUpdateXMLToSpringSYNCHRONOUS(newEmailToOldemailMap, sessionId);
    }

    public static void sendEmailUpdateXMLToSpringSYNCHRONOUS(Map<String, String> newEmailToOldemailMap, String sessionId){
        String xmlString = writeEmailXML(newEmailToOldemailMap);
        String workflowName = 'SF_UpdateUserEmail';
        workflowResult = SpringCMAction.sparkSpringCMWorkflow(xmlString, workflowName, sessionId);
    }

    // takes a map of new email to old email, writes the xml
    public static String writeEmailXML(Map<String, String> newEmailToOldemailMap){
        XmlStreamWriter w = new XmlStreamWriter();
        w.writeStartDocument(null, '1.0');

        for (String newEmail : newEmailToOldemailMap.keySet()){
            String oldEmail = newEmailToOldemailMap.get(newEmail);
            w.writeStartElement(null, 'SFData', null); //start sfdata

                w.writeStartElement(null, 'OldUserEmail', null); // start old email
                    w.writeCharacters(oldEmail);
                w.writeEndElement(); //end old email
                w.writeStartElement(null, 'NewUserEmail', null); // start new email
                    w.writeCharacters(newEmail);
                w.writeEndElement(); //end new email

            w.writeEndElement(); //end SFData
        }
        w.writeEndDocument();

        String xmlOutput = w.getXmlString();
        w.close();
        return xmlOutput;
    }

    private static String schoolPortalLicenseKey;
    /**
     * @description This method is used to determine the license key we should use to identify users that need to have
     *              profiles updated. We used to always assume it was the Gold Partner license key. We now need to use
     *              the Partner Community license key. However, instead of making it a hardcoded constant, we will grab
     *              it from the school portal admin profile. If we can't find the profile, we default to the gold partner
     *              license key.
     * @return The name of the School Portal License Key.
     */
    private static String getSchoolPortalLicenseKey() {
        // If we already know what the license key is for school portal profiles, return it.
        if (String.isNotBlank(schoolPortalLicenseKey)) {
            return schoolPortalLicenseKey;
        }

        // Otherwise, we have to get the license key by checking one of the school portal profiles.
        // We use the School Admin profile which is specified on the Portal_Settings__c.

        Profile schoolAdminProfile = [SELECT Id, UserLicense.LicenseDefinitionKey FROM Profile WHERE Name = :ProfileSettings.SchoolAdminProfileName LIMIT 1];

        if (schoolAdminProfile != null) {
            schoolPortalLicenseKey = schoolAdminProfile.UserLicense.LicenseDefinitionKey;
        } else {
            // If we can't find the school admin profile for some reason, default to the original license key for Gold Partner licenses.
            schoolPortalLicenseKey = GOLD_PARTNER_LICENSE_DEFINITION_KEY;
        }

        return schoolPortalLicenseKey;
    }

	/**
	 *    NAIS-2476
	 *  If the school preference is set, it should automatically set the main contact preference as well. [jB]
	 */
	 public static void setPfsNotificationFrequencyMainContact(Set<Id> accountIds){
	     List<Contact> contactsToUpdate = new List<Contact>();
	     for(Contact mainContact : [SELECT PFS_Alert_Preferences__c, Account.Alert_Frequency__c
	                                 FROM Contact WHERE AccountId IN :accountIds AND SSS_Main_Contact__c = true]){
	         // set main contacts preference to recieve alerts based on Account preference (Weekly or Daily - turns on)
	         if(acctPfsActAlerts.contains(mainContact.Account.Alert_Frequency__c)){
	             mainContact.PFS_Alert_Preferences__c = 'Receive Alerts';
	         } else {
	             mainContact.PFS_Alert_Preferences__c = 'Do Not Receive Alerts';
	         }
	         contactsToUpdate.add(mainContact);
	     }
	     // update the main contacts
	     update contactsToUpdate;
	 }
}