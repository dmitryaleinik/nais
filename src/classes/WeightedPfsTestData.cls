/**
 * @description This class is used to create Weighted Pfs records for unit tests.
 */
@isTest
public class WeightedPfsTestData extends SObjectTestData {
    /**
     * @description Get the default values for the Weighted_PFS__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Weighted_PFS__c.Annual_Setting__c => AnnualSettingsTestData.Instance.DefaultAnnualSettings.Id,
                Weighted_PFS__c.PFS__c => PfsTestData.Instance.DefaultPfs.Id
        };
    }

    /**
     * @description Set the Annual_Setting__c field on the current Weighted Pfs record.
     * @param annualSettingId The Annual Setting Id to set on the Weighted Pfs.
     * @return The current working instance of WeightedPfsTestData.
     */
    public WeightedPfsTestData forAnnualSettingId(Id annualSettingId) {
        return (WeightedPfsTestData) with(Weighted_PFS__c.Annual_Setting__c, annualSettingId);
    }

    /**
     * @description Set the PFS__c field on the current Weighted Pfs record.
     * @param pfsId The Pfs Id to set on the Weighted Pfs.
     * @return The current working instance of WeightedPfsTestData.
     */
    public WeightedPfsTestData forPfsId(Id pfsId) {
        return (WeightedPfsTestData) with(Weighted_PFS__c.PFS__c, pfsId);
    }

    /**
     * @description Set the School_Count__c field on the current Weighted Pfs record.
     * @param schoolCount The School Count to set on the Weighted Pfs.
     * @return The current working instance of WeightedPfsTestData.
     */
    public WeightedPfsTestData forSchoolCount(Integer schoolCount) {
        return (WeightedPfsTestData) with(Weighted_PFS__c.School_Count__c, schoolCount);
    }

    /**
     * @description Insert the current working Weighted_PFS__c record.
     * @return The currently operated upon Weighted_PFS__c record.
     */
    public Weighted_PFS__c insertWeightedPfs() {
        return (Weighted_PFS__c)insertRecord();
    }

    /**
     * @description Create the current working Weighted Pfs record without resetting
     *             the stored values in this instance of WeightedPfsTestData.
     * @return A non-inserted Weighted_PFS__c record using the currently stored field
     *             values.
     */
    public Weighted_PFS__c createWeightedPfsWithoutReset() {
        return (Weighted_PFS__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Weighted_PFS__c record.
     * @return The currently operated upon Weighted_PFS__c record.
     */
    public Weighted_PFS__c create() {
        return (Weighted_PFS__c)super.buildWithReset();
    }

    /**
     * @description The default Weighted_PFS__c record.
     */
    public Weighted_PFS__c DefaultWeightedPfs {
        get {
            if (DefaultWeightedPfs == null) {
                DefaultWeightedPfs = createWeightedPfsWithoutReset();
                insert DefaultWeightedPfs;
            }
            return DefaultWeightedPfs;
        }
        private set;
    }

    /**
     * @description Get the Weighted_PFS__c SObjectType.
     * @return The Weighted_PFS__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Weighted_PFS__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static WeightedPfsTestData Instance {
        get {
            if (Instance == null) {
                Instance = new WeightedPfsTestData();
            }
            return Instance;
        }
        private set;
    }

    private WeightedPfsTestData() { }
}