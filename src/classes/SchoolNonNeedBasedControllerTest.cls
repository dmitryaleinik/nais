/**
* @ SchoolNonNeedBasedControllerTest.cls
* @date 2/11/2015
* @author  Exponent Partners
* @description Unit test for new School-Initiated (Non Need Based Folders) page controller
*/
@isTest
private with sharing class SchoolNonNeedBasedControllerTest {

    private static User schoolPortalUser;
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    private static Contact student1;

    private static void createTestData() {
        Account family = AccountTestData.Instance.asFamily().create();
        Account school = AccountTestData.Instance.asSchool().create();
        insert new List<Account>{family, school};

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;

        Contact parentA = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(school.Id).create();
        String studentFirstName = 'TestFirstName';
        String studentLastName = 'TestLastName';
        Date studentBirthDate = Date.today().addYears(-10);
        student1 = ContactTestData.Instance
            .asStudent()
            .forAccount(family.Id)
            .forFirstName(studentFirstName)
            .forLastName(studentLastName)
            .forBirthdate(studentBirthDate).create();
        insert new List<Contact> {schoolStaff, parentA, student1};

        System.runAs(thisUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);
        String pfsStatusUnpaid = 'Unpaid';

        System.runAs(schoolPortalUser){
            PFS__c pfs1 = PfsTestData.Instance
                .asSubmitted()
                .forPaymentStatus(pfsStatusUnpaid)
                .forParentA(parentA.Id)
                .forOriginalSubmissionDate(System.today().addDays(-3))
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<PFS__c> {pfs1});

            Applicant__c applicant1A = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forStudentFirstName(student1.FirstName)
                .forStudentLastName(student1.LastName)
                .forStudentBirthdate(student1.Birthdate)
                .forContactId(student1.Id).create();
            Dml.WithoutSharing.insertObjects(new List<Applicant__c> {applicant1A});

            Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
                .forStudentId(student1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            studentFolder1.First_Name__c = studentFirstName;
            studentFolder1.Last_Name__c = studentLastName;
            studentFolder1.Birthdate__c = studentBirthDate;            
            Dml.WithoutSharing.insertObjects(new List<Student_Folder__c> {studentFolder1});

            String fifthGrade = '5';
            School_PFS_Assignment__c spfsa1 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1A.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            spfsa1.Birthdate__c = studentBirthDate; 
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> {spfsa1});
        }

        // Share records to school staff
        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);
    }

    @isTest
    private static void testSearch() {
        createTestData();

        Test.startTest();
            System.runAs(schoolPortalUser) {
                SchoolNonNeedBasedController controller = new SchoolNonNeedBasedController();
                controller.searchContact.FirstName = student1.FirstName;
                controller.searchContact.LastName = student1.LastName;
                controller.searchContact.Birthdate = student1.Birthdate;

                controller.searchContacts();
                
                System.AssertEquals(1, controller.searchResults.size());
                
                controller.searchContact.FirstName = 'Test';
                controller.searchContact.LastName = 'Applicant';
                controller.searchContact.Birthdate = Date.today().addDays(-100);
                controller.searchContacts();
                
                System.AssertEquals(0, controller.searchResults.size());
                
                controller.startAgain();
                
                System.AssertEquals(null, controller.searchContact.FirstName);
                System.AssertEquals(null, controller.searchContact.LastName);
                System.AssertEquals(null, controller.searchContact.Birthdate);
                System.AssertEquals(null, controller.searchResults);
            }
        Test.stopTest();
    }
}