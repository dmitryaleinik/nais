/**
 * @description This batch class is used to run the VerificationAction.cls in a batch context.
 */
public class VerificationStatusBatch implements Database.Batchable<SObject> {

    public Set<Id> HouseholdIds { get; private set; }

    public VerificationStatusBatch(Set<Id> householdIdsToProcess) {
        HouseholdIds = householdIdsToProcess;
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT Id FROM Household__c WHERE Id IN :HouseholdIds]);
    }

    public void execute(Database.BatchableContext bc, List<SObject> scope) {
        Set<Id> householdIdsToProcess = new Set<Id>();

        for (SObject record : scope) {
            householdIdsToProcess.add(record.Id);
        }


        if (!householdIdsToProcess.isEmpty()) {
            VerificationAction.setVerficationStatusValues(null, householdIdsToProcess);
        }
    }

    public void finish(Database.BatchableContext bc) {}
}