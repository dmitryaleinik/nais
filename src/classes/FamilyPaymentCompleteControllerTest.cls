@isTest
private class FamilyPaymentCompleteControllerTest {
    private static User createTestSysAdminUser() {
        User sysAdminUser = new User();
        sysAdminUser.Alias = 'TestSyAd';
        sysAdminUser.LanguageLocaleKey = 'en_US';
        sysAdminUser.LocaleSidKey = 'en_US';
        sysAdminUser.EmailEncodingKey = 'ISO-8859-1';
        sysAdminUser.FirstName = 'Sys';
        sysAdminUser.LastName = 'admin';
        sysAdminUser.Username = 'sysadmin@sys.adm';
        sysAdminUser.ProfileId = GlobalVariables.sysAdminProfileId;
        sysAdminUser.Email = sysAdminUser.Username;

        sysAdminUser.CommunityNickName = sysAdminUser.Email.substring(0, sysAdminUser.Email.indexOf('@')) +'_'+Math.round((Math.random() * 100000));

        sysAdminUser.TimeZoneSidKey = 'America/Los_Angeles';
        insert sysAdminUser;
        return sysAdminUser;
    }

    private static TestDataFamily createTestData() {
        List<Academic_Year__c> academicYears = TestUtils.createAcademicYears();

        List<Federal_Tax_Table__c> fedTaxTables = new List<Federal_Tax_Table__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            fedTaxTables.add(TestUtils.createFederalTaxTable(academicYears[i].Id, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_SINGLE, false));
        }
        Database.insert(fedTaxTables);

        List<State_Tax_Table__c> stateTaxTables = new List<State_Tax_Table__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            stateTaxTables.add(TestUtils.createStateTaxTable(academicYears[i].Id, false));
        }
        Database.insert(stateTaxTables);

        List<Employment_Allowance__c> emplAllowances = new List<Employment_Allowance__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            emplAllowances.add(TestUtils.createEmploymentAllowance(academicYears[i].Id, false));
        }
        Database.insert(emplAllowances);

        List<Net_Worth_of_Business_Farm__c> busFarm = new List<Net_Worth_of_Business_Farm__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            busFarm.add(TestUtils.createBusinessFarm(academicYears[i].Id, false));
        }
        Database.insert(busFarm);

        List<Retirement_Allowance__c> retAllowances = new List<Retirement_Allowance__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            retAllowances.add(TestUtils.createRetirementAllowance(academicYears[i].Id, false));
        }
        Database.insert(retAllowances);

        List<Asset_Progressivity__c> assetProgs = new List<Asset_Progressivity__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            assetProgs.add(TestUtils.createAssetProgressivity(academicYears[i].Id, false));
        }
        Database.insert(assetProgs);

        List<Income_Protection_Allowance__c> incProAllowances = new List<Income_Protection_Allowance__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            incProAllowances.add(TestUtils.createIncomeProtectionAllowance(academicYears[i].Id, false));
        }
        Database.insert(incProAllowances);

        List<Expected_Contribution_Rate__c> expContRates = new List<Expected_Contribution_Rate__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            expContRates.add(TestUtils.createExpectedContributionRate(academicYears[i].Id, false));
        }
        Database.insert(expContRates);

        List<Housing_Index_Multiplier__c> houseIndexes = new List<Housing_Index_Multiplier__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            houseIndexes.add(TestUtils.createHousingIndexMultiplier(academicYears[i].Id, false));
        }
        Database.insert(houseIndexes);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        SSS_Constants__c constants = new SSS_Constants__c(
           Percentage_for_Imputing_Assets__c = 1,
           Default_COLA_Value__c = 1,
           Academic_Year__c = academicYearId,
           Exemption_Allowance__c = 0,
           Std_Deduction_Joint_Surviving__c = 0,
           Std_Deduction_Head_of_Household__c = 0,
           Std_Deduction_Single_Filing_Separately__c = 0,
           Employment_Allowance_Maximum__c = 0,
           Medicare_Tax_Rate__c = 0,
           Medical_Dental_Allowance_Percent__c = 0,
           Negative_Contribution_Cap_Constant__c = 0,
           Boarding_School_Food_Allowance__c = 0,
           Social_Security_Tax_Threshold__c = 0,
           Social_Security_Tax_Rate__c = 0.25,
           IPA_Housing_For_Each_Additional__c = 1000,
           IPA_Other_For_Each_Additional__c = 1000);
        insert constants;

        // init the custom settings
        Application_Fee_Settings__c feeSettings = new Application_Fee_Settings__c();
        feeSettings.Name = GlobalVariables.getCurrentYearString();
        feeSettings.Amount__c = 50;
        feeSettings.Discounted_Amount__c = 40;
        feeSettings.Product_Code__c = 'Test Product Code'; // [SL] NAIS-1031: new required field
        feeSettings.Discounted_Product_Code__c = 'KS Test Product Code'; // [SL] NAIS-1031: new required field
        insert feeSettings;

        // create the default test data
        TestDataFamily testData = new TestDataFamily();

        // set to submitted, so the opportunity gets created
        testData.pfsA.Parent_A_Address__c = 'Line 1 Address\nLine 2 Address';
        testData.pfsA.PFS_Status__c = 'Application Submitted';
        update testData.pfsA;

        return testData;
    }

    @isTest
    private static void testPaymentCompletePageWithPfsIdParam() {
        TestDataFamily testData = createTestData();
        User sysAdmin = createTestSysAdminUser();

        System.runAs(sysAdmin) {
            PageReference pr = Page.FamilyPaymentComplete;
            pr.getParameters().put('id', testData.pfsA.Id);
            Test.setCurrentPage(pr);
            FamilyTemplateController template = new FamilyTemplateController();
            FamilyPaymentCompleteController controller = new FamilyPaymentCompleteController(template);
            System.assertEquals(testData.pfsA.Id, controller.thePFS.Id);
        }
    }

    @isTest
    private static void testPaymentCompletePageWithNoParam() {
        TestDataFamily testData = createTestData();
        User sysAdmin = createTestSysAdminUser();

        System.runAs(testData.familyPortalUser) {
            PageReference pr = Page.FamilyPaymentComplete;
            Test.setCurrentPage(pr);

            FamilyTemplateController template = new FamilyTemplateController();
            FamilyPaymentCompleteController controller = new FamilyPaymentCompleteController(template);
            System.assertEquals(testData.pfsA.Id, controller.thePFS.Id);
        }
    }

    @isTest
    private static void testPaymentCompletePageWithAcademicYearIdParam() {
        TestDataFamily testData = createTestData();
        User sysAdmin = createTestSysAdminUser();
        Id academicYearId = GlobalVariables.getAcademicYearByName(testData.pfsA.Academic_Year_Picklist__c).Id;

        System.runAs(testData.familyPortalUser) {
            PageReference pr = Page.FamilyPaymentComplete;
            pr.getParameters().put('academicyearid', academicYearId);
            Test.setCurrentPage(pr);

            FamilyTemplateController template = new FamilyTemplateController();
            FamilyPaymentCompleteController controller = new FamilyPaymentCompleteController(template);
            System.assertEquals(testData.pfsA.Id, controller.thePFS.Id);
        }
    }

    @isTest
    private static void testReturnToDashboard() {
        TestDataFamily testData = createTestData();
        User sysAdmin = createTestSysAdminUser();
        Id academicYearId = GlobalVariables.getAcademicYearByName(testData.pfsA.Academic_Year_Picklist__c).Id;

        System.runAs(testData.familyPortalUser) {
            PageReference pr = Page.FamilyPaymentComplete;
            pr.getParameters().put('academicyearid', academicYearId);
            Test.setCurrentPage(pr);

            FamilyTemplateController template = new FamilyTemplateController();
            FamilyPaymentCompleteController controller = new FamilyPaymentCompleteController(template);
            PageReference newpage = FamilyPaymentCompleteController.returnToDashboard();
            System.assertEquals(Page.FamilyDashboard.getUrl() + '?academicyearid=' + academicYearId, newpage.getUrl());
        }
    }

    @isTest
    private static void testTopNavInterface() {
        TestDataFamily testData = createTestData();
        User sysAdmin = createTestSysAdminUser();
        Id academicYearId = GlobalVariables.getAcademicYearByName(testData.pfsA.Academic_Year_Picklist__c).Id;
        Academic_Year__c newAcademicYear = GlobalVariables.getPreviousAcademicYear();

        System.runAs(testData.familyPortalUser) {
            PageReference pr = Page.FamilyPaymentComplete;
            pr.getParameters().put('academicyearid', academicYearId);
            Test.setCurrentPage(pr);

            FamilyTemplateController template = new FamilyTemplateController();
            FamilyPaymentCompleteController controller = new FamilyPaymentCompleteController(template);
            System.assertEquals(GlobalVariables.getAcademicYearByName(controller.thePFS.Academic_Year_Picklist__c).Id, template.academicYearId);
            PageReference newpage = template.YearSelector_OnChange(newAcademicYear.Id);
            System.assertEquals(Page.FamilyDashboard.getUrl() + '?academicyearid=' + academicYearId, newpage.getUrl());
        }
    }
}