/**
 * @description Query selector for Transaction Line Item records.
 **/
public without sharing class TransactionLineItemSelector extends fflib_SObjectSelector {
    private static final String POSTED = 'Posted';
    private static final String CREDIT_DEBIT_CARD = 'Credit/Debit Card';
    private static final String PAYMENT = 'Payment';
    private static final String AUTO_PAYMENT = 'Payment - Auto';

    @testVisible private static final String TRANSACTION_LINE_ITEM_IDS_PARAM = 'transactionLineItemIds';
    @testVisible private static final String OPPORTUNITY_IDS_PARAM = 'opportunityIds';
    @testVisible private static final String RECORD_TYPE_IDS_PARAM = 'recordTypeIds';

    // Relations
    private static final String OPPORTUNITY_RELATION = 'Opportunity__r';

    /**
     * @description Select Transaction Line Items by their Ids.
     * @param transactionLineItemIds The Ids to select Transaction Line Items by.
     * @returns A list of Transaction Line Items associated with the requested Ids.
     * @throws An ArgumentNullException of transactionLineItemIds is null.
     */
    public List<Transaction_Line_Item__c> selectById(Set<Id> transactionLineItemIds) {
        ArgumentNullException.throwIfNull(transactionLineItemIds, TRANSACTION_LINE_ITEM_IDS_PARAM);

        assertIsAccessible();
        OpportunitySelector.Instance.assertIsAccessible();

        return Database.query(String.format('SELECT {0}, {1} FROM {2} WHERE Id IN' +
                ':transactionLineItemIds ORDER BY {3} ASC', new List<String> {
                getFieldListString(),
                OpportunitySelector.Instance.getRelatedFieldListString(OPPORTUNITY_RELATION),
                getSObjectName(),
                getOrderBy()
        }));
    }

    /**
     * @description Select Transaction Line Items by Opportunity and Record Type Ids.
     * @param opportunityIds The Opportunity Ids to select Transaction Line Items by.
     * @param recordTypeIds The Record Type Ids to select Transaction Line Items by.
     * @returns A list of Transaction Line Items associated with the Opportunity Ids
     *          and Record Type Ids provided.
     * @throws An ArgumentNullException if either of the parameters are null.
     */
    public List<Transaction_Line_Item__c> selectByOpportunityAndRecordType(Set<Id> opportunityIds, Set<Id> recordTypeIds) {
        ArgumentNullException.throwIfNull(opportunityIds, OPPORTUNITY_IDS_PARAM);
        ArgumentNullException.throwIfNull(recordTypeIds, RECORD_TYPE_IDS_PARAM);

        assertIsAccessible();

        return Database.query(String.format('SELECT {0} FROM Transaction_Line_Item__c WHERE Opportunity__c IN ' +
                ':opportunityIds AND RecordTypeId IN :recordTypeIds ORDER BY {1} ASC',
                new List<String> { getFieldListString(), getOrderBy() }));
    }

    /**
     * @description Select refundable Transaction Line Items by Opportunity and Record
     *              Type Ids. Refundable Transaction Line Items must be posted transactions
     *              that have a vault guid defined.
     * @param opportunityIds The Opportunity Ids to select Transaction Line Items by.
     * @returns A list of Transaction Line Items associated with the Opportunity Ids
     *          and Record Type Ids provided.
     * @throws An ArgumentNullException if either of the parameters are null.
     */
    public List<Transaction_Line_Item__c> selectRefundableTransactionLineItems(Set<Id> opportunityIds) {
        ArgumentNullException.throwIfNull(opportunityIds, OPPORTUNITY_IDS_PARAM);

        assertIsAccessible();
        OpportunitySelector.Instance.assertIsAccessible();

        // Prep the required record type Ids
        Set<Id> paymentRecordTypeIds = new Set<Id>{
                Schema.SObjectType.Transaction_Line_Item__c.getRecordTypeInfosByName().get(PAYMENT).getRecordTypeId(),
                Schema.SObjectType.Transaction_Line_Item__c.getRecordTypeInfosByName().get(AUTO_PAYMENT).getRecordTypeId()
        };
        Id refundedRecordTypeId = RecordTypes.refundTransactionTypeId;

        String query = 'SELECT {0}, {1} FROM {2} WHERE Opportunity__c IN :opportunityIds ' +
                'AND ((RecordTypeId IN :PaymentRecordTypeIds AND Transaction_Status__c = :POSTED AND ' +
                'VaultGuid__c != null) OR (RecordTypeId = :refundedRecordTypeId)) ORDER BY {3} ASC';

        return Database.query(String.format(query,
                new List<String> {
                        getFieldListString(),
                        OpportunitySelector.Instance.getRelatedFieldListString(OPPORTUNITY_RELATION),
                        getSObjectName(),
                        getOrderBy()
                }));
    }

    private Schema.SObjectType getSObjectType() {
        return Transaction_Line_Item__c.getSObjectType();
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                Transaction_Line_Item__c.Account_Code__c,
                Transaction_Line_Item__c.Account_Label__c,
                Transaction_Line_Item__c.Amount__c,
                Transaction_Line_Item__c.CC_Last_4_Digits__c,
                Transaction_Line_Item__c.Discount_Reason__c,
                Transaction_Line_Item__c.Id,
                Transaction_Line_Item__c.Name,
                Transaction_Line_Item__c.Opportunity__c,
                Transaction_Line_Item__c.Product_Amount__c,
                Transaction_Line_Item__c.Product_Code__c,
                Transaction_Line_Item__c.RecordTypeId,
                Transaction_Line_Item__c.Source__c,
                Transaction_Line_Item__c.Tender_Type__c,
                Transaction_Line_Item__c.Transaction__c,
                Transaction_Line_Item__c.Transaction_Code__c,
                Transaction_Line_Item__c.Transaction_Date__c,
                Transaction_Line_Item__c.Transaction_ID__c,
                Transaction_Line_Item__c.Transaction_Result_Code__c,
                Transaction_Line_Item__c.Transaction_Status__c,
                Transaction_Line_Item__c.Transaction_Type__c,
                Transaction_Line_Item__c.VaultGUID__c,
                Transaction_Line_Item__c.Waiver_Quantity__c
        };
    }

    /**
     * @description Singleton property ensuring external sources do not
     *              instantiate this directly.
     */
    public static TransactionLineItemSelector Instance {
        get {
            if (Instance == null) {
                Instance = new TransactionLineItemSelector();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to enforce singleton access
     *              via the Instance property.
     */
    private TransactionLineItemSelector() {
        super(false, false, false);
    }
}