/*
 * Spec-037 School Portal - Fee Waivers; R-082, R-083
 * BR, Exponent Partners, 2013
 */

/* [CH] NAIS-1666 Converting page to accept a parameter for one academic year, and only display
 *      Waivers for that year (no prior year records)
 *      - Removing the myAcademicYear variable because it's never used for anything but its id value
 *              which is already available in the academicYearId variable
 */

public with sharing class SchoolFeeWaiversController implements SchoolAcademicYearSelectorInterface {

    private static final String PFS_PAYMENT_STATUS_UNPAID = 'Unpaid';
    private static final String SSS_FEE_WAIVER_GUIDELINES_LINK_DOCINFO_NAME = 'SSS_Fee_Waiver_Guidelines_Link';
    private static final String OPPORTUNITY_ID_PARAM = 'Id';

    /* Initialization */
    /**
     * @description Constructor that by default sets isAssignedWaiverPage to false.
     */
    public SchoolFeeWaiversController() {
        isAssignedWaiverPage = false; // SFP-449 [DP] 04.18.2016 default to false
        showFeeWaiverConfirm = false;
    }

    /**
     * @description Initialized from page action of the SchoolFeeWaivers page. Loads default values for page properties.
     */
    public void initFeeWaiversPage() {
        initCommon();
        loadAcademicYear();
        loadAnnualSettings(getAcademicYearId());

        updateAssignedTable();
    }

    /**
     * @description Initialized from page action of the SchoolFeeWaiversAssigned page. Loads default values for page properties.
     * Sets isAssignedWaiver to true.
     */
    public void initFeeWaiversAssignedTabPage() {
        isAssignedWaiverPage = true; // called from vf page tag
        initCommon();
        System.debug(getAcademicYearId());
        loadAnnualSettings(getAcademicYearId());
        updateAssignedTable();
    }

    /**
     * @description Loads table size from school settings and sets school id.
     */
    public void initCommon() {

        String schoolIdParam = System.currentPagereference().getParameters().get('theschoolid');

        if (!String.isBlank(schoolIdParam)){
            mySchoolId = schoolIdParam;
        } else{
            mySchoolId = GlobalVariables.getCurrentSchoolId();
        }
        //mySchoolId = GlobalVariables.getCurrentUser().Contact.AccountId;

        // Get the table page size from the school portal settings.
        tablePageSize = SchoolPortalSettings.Fee_Waivers_Table_Page_Size;

        loadAcademicYear();

    }

    /**
     * @description Loads academic year from the academicyearid url param otherwise defaults to the current year.
     */
    public void loadAcademicYear() {

        // Due to the selector_onchange not refreshing properly, we redirect to the same page with academicyearid parameter.
        Map<String, String> parameters = ApexPages.currentPage().getParameters();
        String academicYearId = null;
        if (parameters.containsKey('academicyearid'))
            setAcademicYearId(parameters.get('academicyearid'));
        else {
            // [CH] NAIS-1666
            setAcademicYearId(GlobalVariables.getCurrentAcademicYear().Id);
        }
    }

    /**
     * @description Loads annual settings record based on the academic year id.
     */
    public Annual_Setting__c loadAnnualSettings(Id academicYearId) {

        List<Annual_Setting__c> annualSettings = GlobalVariables.getCurrentAnnualSettings(true, academicYearId);
        for (Annual_Setting__c setting : annualSettings) {
            if (setting.Academic_Year__c == academicYearId) {
                myAnnualSettings = setting;
            }
        }

        // Check Waivers_Assigned__c and Total_Waivers_Override_Default__c.
        if (myAnnualSettings.Waivers_Assigned__c == null) {
            myAnnualSettings.Waivers_Assigned__c = 0;
        }
        if (myAnnualSettings.Total_Waivers_Override_Default__c == null) {
            myAnnualSettings.Total_Waivers_Override_Default__c = 0;
        }

        return myAnnualSettings;
    }

    /* Properties */
    public SchoolFeeWaiversController Me { get { return this; } }

    // [CH] NAIS-1678
    public Academic_Year__c currentAcademicYear { get; private set; }
    public Annual_Setting__c myAnnualSettings { get; set; }

    public Id mySchoolId { get; private set; }
    public Id selectedPFS { get; set; }
    public String selectedPFSNumber { get; set; }
    public Integer tablePageSize;
    public Integer iAssignedCount { get; set; } // Property mainly for tests, only valid after assignwaivers().
    public Boolean showFeeWaiverConfirm {get; set;} // used to display popup confirmation
    public Boolean applicantsHaveFeeWaiver {get; set;}
    public List<PFSCheckWrapper> applicantsWithAssignedFeeWaivers {get; set;}
    
    /**
     *  @description Setting for conditional rendering of "Thanks" message for fee waivers purchasing.
    */
    public Boolean showThanksForFWPurchasingMessage {
        get {
            if(showThanksForFWPurchasingMessage == null) {
                Id opportunityId = UrlService.Instance.getIdByType(OPPORTUNITY_ID_PARAM, Opportunity.getSObjectType());

                if (opportunityId != null && String.isNotBlank(opportunityId)) {
                    showThanksForFWPurchasingMessage = true;
                } 
            }
            return showThanksForFWPurchasingMessage;
        } set;}

    public Boolean isAssignedWaiverPage {get; set;} // [DP] 04.18.2016 SFP-449 set to true when on the assigned waiver page

    /** @description Set controller used for pagination and storing queried records. */
    public ApexPages.StandardSetController assignedSetCtrl { get; set; }

    /**
     * @description List of waivers on the page. Pagination resets this list.
     */
    public List<PFSCheckWrapper> assignedList {
        get {
            if (assignedList == null) {
                assignedList = feeWaiversInstance.wrapPFSSetCtrlRecords(assignedSetCtrl, selectedApplicantsByPfsId, getAcademicYearId());
            }
            return assignedList;
        }
        set;
    }

    /**
     * @description Instantiates the correct FeeWaivers instance based on if the page is SchoolFeeWaivers or SchoolFeeWaiversAssigned.
     */
    private SchoolFeeWaiversController.FeeWaivers feeWaiversInstance {
        get {
            if(feeWaiversInstance == null) {
                feeWaiversInstance = isAssignedWaiverPage ? (FeeWaivers)new AssignedFeeWaivers() : (FeeWaivers)new UnassignedFeeWaivers();
            }
            return feeWaiversInstance;
        }
        set;
    }

    public String sssFeeWaiverGuidelinesId {
        get {
            if (sssFeeWaiverGuidelinesId == null) {
                sssFeeWaiverGuidelinesId = 
                    DocumentLinkMappingService.getDocumentRecordId(SSS_FEE_WAIVER_GUIDELINES_LINK_DOCINFO_NAME);
            }
            return sssFeeWaiverGuidelinesId;
        } 
        private set;
    }

    /**
     * @description Number of waivers selected that will be assigned.
     * @return Number of waivers selected.
     */
    public Integer getNumOfNewWaivers() {
        return getApplicantsToWaive().size();
    }

    //Used to maintain list of selected PFSs while paging
    @testVisible private Map<String, PFSCheckWrapper> selectedApplicantsByPfsId = new Map<String, PFSCheckWrapper>();

    /**
     * @description Gets PFSCheckWrappers that have been selected by the user for fee waivers from the map of selected
     *              applicants by PFS Id.
     * @return A list of PFSCheckWrapper.
     */
    public List<PFSCheckWrapper> getApplicantsToWaive() {
        return selectedApplicantsByPfsId == null ? new List<PFSCheckWrapper>() : selectedApplicantsByPfsId.values();
    }

    /* Action Methods */
    // [DP] ADDED two methods for NAIS-1555
    public void doInitFeeWaiverConfirm() {
        // Update the map of selected applicants to assign fee waivers to from the current list displayed to the user.
        updateListOfSelectedApplicants(assignedList);
        showFeeWaiverConfirm = true;
    }

    public void doCancelFeeWaiverConfirm() {
        showFeeWaiverConfirm = false;
    }

    public void doCancelApplicantsHaveFeeWaiverError() {
        applicantsHaveFeeWaiver = false;
    }
    
    /**
     *  [JB] NAIS-1877 Redirect to 'Purchase Waivers' Page
     */
    public PageReference purchaseAdditionalWaivers() {
        return Page.SchoolFeeWaiversMBWInfo;//SFP-189, [G.S]
    }

    /**
     * @description Navigate to the student folder from the list of pfs's. The command link sets the selectedPFSNumber parameter.
     * @return The formatted link to the SchoolPfsSummary page.
     */
    public PageReference pfslink() {

        // Need to get the SPA Id. :()
        List<School_PFS_Assignment__c> spas = [SELECT Id FROM School_PFS_Assignment__c
        WHERE PFS_Number__c = :selectedPFSNumber
        AND School__c = :mySchoolId
        LIMIT 1];
        if (spas.size() > 0) {
            PageReference pageRef = Page.SchoolPFSSummary;
            pageRef.getParameters().put('SchoolPFSAssignmentId', spas[0].Id);
            pageRef.setRedirect(true);
            return pageRef;
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                    'Cannot find School PFS Assignment for PFS ID ' + selectedPFSNumber + '.'));
        }

        return null;
    }

    /* AssignWaiverWrapper */
    /**
     * @description Wrapper class used for displaying information on the waivers page.
     */
    public class PFSCheckWrapper {
        // Wraps an sObject adding a checkbox.
        public Boolean isChecked { get; set; }
        public Boolean isDisabled { get; set; }
        public Boolean isMeansBased {get; set;}
        public Integer applicantsSize {get;set;}
        public String feeWaived {get; set;}

        // We need to contact grades and names together because of the delimiters. :(
        public String sStudentGrades { get; set; }
        public String sStudentNames { get; set; }

        // SFP-692 new properties to conserve view state
        public Id PFSId { get; set; }
        public String PFSNumber { get; set; }
        public Id ParentId { get; set; }
        public String ParentFirstName { get; set; }
        public String ParentLastName { get; set; }
        public String ParentEmail { get; set; }
        public String PreferredPhoneNumber { get; set; }
        public String PFSStatus { get; set; }
        public Decimal TotalIncome { get; set; }

        public PFSCheckWrapper() {
            isChecked = false;
            isDisabled = false;
            isMeansBased = false;
        }
    }

    /* Helper Methods */

    /**
     * @description Initializes the page with the PFS's for the school.
     */
    public void updateAssignedTable() {

        // if this is the assigned fee waiver page, get fewer fields to minimize viewstate
        assignedSetCtrl = new ApexPages.StandardSetController(
                feeWaiversInstance.getPfsLocator(getAcademicYearId(), mySchoolId));
        // Reset map for when waivers have been assigned.
        selectedApplicantsByPfsId = new Map<String, PFSCheckWrapper>();
        assignedSetCtrl.setPageSize(tablePageSize);
    }

    /* SchoolAcademicYearSelectorInterface Implementation */

    private Id academicYearId;
    /**
     * @description Returns the academic year set by the page.
     * @return Academic Year Id
     */
    public String getAcademicYearId() {
        return academicYearId;
    }
    /**
     * @description Sets the academic year id and retrieves the mean based waiver account id for that year.
     * @param idValue Academic Year Id.
     */
    public void setAcademicYearId(String idValue) {
        // [CH] NAIS-1666 and NAIS-1678
        academicYearId = idValue;
        currentAcademicYear = GlobalVariables.getAcademicYear(idValue);
        feeWaiversInstance.MeansBasedId = ApplicationFeeSettingsService.Instance.getMeansBasedFeeWaiverAccountId();
    }

    public PageReference SchoolAcademicYearSelector_OnChange(Boolean saveRecord) {
        PageReference newPage = new PageReference(ApexPages.currentPage().getUrl());
        newPage.getParameters().put('academicyearid', getAcademicYearId());
        newPage.setRedirect(true);
        return newPage;
    }

    /**
     * @description Encapsulates all of the logic for querying Pfs's and building the PFSWrappers.
     * @return Academic Year Id
     */
    public abstract class FeeWaivers {
        protected List<Id> pfsIds;
        protected Set<Id> applicantIds;

        private Id academicYear;
        private Id mySchoolId;

        /** @description Account Id of the means based account. Used for querying for means based waivers. */
        public Id MeansBasedId {get; set;}
        /**
         * @description This method must be called to set the pfsIds and applicantIds that are used for
         * querying and building the PFSWrappers.
         * @param academicYear Academic year to be used in the query.
         * @param mySchoolId School Id to be used in query.
         */
        protected abstract void setPfsAndApplicantIds(Id academicYear, Id mySchoolId);
        /**
         * @description Returns the query locator to initialze the set controller for the page. This method should cal
         * @param academicYear Academic year to filter PFS's.
         * @param mySchoolId Only return pfs's which are tied to the user's school.
         * @return Locator for initializing the set controller.
         */
        public abstract Database.QueryLocator getPfsLocator(Id academicYear, Id mySchoolId);

        protected Set<Id> getApplicantIds() {
            return applicantIds;
        }

        protected List<Id> getCurrentYearPfsIds() {
            return pfsIds;
        }

        /**
         * @description Build list of wrappers. This method can call buildWrapper to use base logic shared by all instances.
         * @param setCtrl Set Controller to get current records to wrap.
         * @param selectedApplicantsByPfsId Map of currently selected applicants. Wrappers should be checked if they are in this map.
         * @param academicYear Can be used to query additional records.
         * @return List of wrapped records.
         */
        public abstract List<PFSCheckWrapper> wrapPFSSetCtrlRecords(ApexPages.StandardSetController setCtrl, Map<String, PFSCheckWrapper> selectedApplicantsByPfsId, Id academicYear);

        /**
         * @description Used for shared logic to initialize a wrapper.
         * @param pfs The pfs to initialize the wrapper from.
         * @return The wrapper class.
         */
        protected PFSCheckWrapper buildWrapper(PFS__c pfs) {
            Set<Id> applicantIds = getApplicantIds();
            PFSCheckWrapper wrapper = new PFSCheckWrapper();
            wrapper.feeWaived = pfs.Fee_Waived__c;
            wrapper.applicantsSize = pfs.Applicants__r.size();

            wrapper.PFSId = pfs.Id;
            wrapper.PFSNumber = pfs.PFS_Number__c;
            wrapper.ParentId = pfs.Parent_A__c;
            wrapper.ParentFirstName = pfs.Parent_A__r.FirstName;
            wrapper.ParentLastName = pfs.Parent_A__r.LastName;
            wrapper.ParentEmail= pfs.Parent_A__r.Email;
            wrapper.PreferredPhoneNumber = pfs.Parent_A__r.Preferred_Phone_Number__c;
            wrapper.PFSStatus = pfs.PFS_Status__c;
            wrapper.TotalIncome = pfs.Total_Income_Current__c;

            // We have to concat the Student Grades and Names ourselves instead of using apex:repeat because of the delimiter.
            // Student Grades & Names Concat
            if (applicantIds != null) {
                for (Applicant__c apl : pfs.Applicants__r) {
                    // [CH] NAIS-1837 this filter has been moved into queries
                    if (applicantIds.contains(apl.Id)) {
                        if (wrapper.sStudentGrades == null) {
                            wrapper.sStudentGrades = apl.Grade_in_Entry_Year__c;
                        } else {
                            wrapper.sStudentGrades += ', ' + apl.Grade_in_Entry_Year__c;
                        }
                        if (wrapper.sStudentNames == null) {
                                 wrapper.sStudentNames = apl.First_Name__c + ' ' + apl.Last_Name__c;
                        } else {
                            wrapper.sStudentNames += ', ' + apl.First_Name__c + ' ' + apl.Last_Name__c;
                        }
                    }
                }
            }
            return wrapper;
        }
    }

    /**
     * @description Logic for the SchoolFeeWaivers page to display unassigned PFSs.
     */
    private class UnassignedFeeWaivers extends FeeWaivers {
        protected override void setPfsAndApplicantIds(Id academicYear, Id mySchoolId) {
            pfsIds = new List<Id>();
            applicantIds = new Set<Id>();
            for (School_PFS_Assignment__c spa : [SELECT Applicant__c, Applicant__r.PFS__c, Applicant__r.PFS__r.Parent_A__c FROM School_PFS_Assignment__c
            WHERE School__c = :mySchoolId
            AND Academic_Year_Picklist__c = :GlobalVariables.getAcademicYear(academicYear).Name
            AND Withdrawn__c != 'Yes' ]) {
                pfsIds.add(spa.Applicant__r.PFS__c);
                applicantIds.add(spa.Applicant__c);
            }
        }

        public override Database.QueryLocator getPfsLocator(Id academicYear, Id mySchoolId) {
            setPfsAndApplicantIds(academicYear, mySchoolId);
            List<Id> currentYearPFSIds = getCurrentYearPfsIds();
            Database.QueryLocator recordQuerier = Database.getQueryLocator([
                SELECT PFS_Number__c, Payment_Status__c, PFS_Status__c, Fee_Waived__c, Parent_A__r.FirstName, Parent_A__r.LastName,
                    Parent_A__r.Email, Parent_A__r.Preferred_Phone_Number__c, Total_Income_Current__c, Parent_A__c,
                    (SELECT First_Name__c, Last_Name__c, Grade_in_Entry_Year__c FROM Applicants__r order by Contact__r.Name)
            FROM PFS__c WHERE
            Parent_A__c NOT IN
            (
                    SELECT Contact__c
                    FROM Application_Fee_Waiver__c
                    WHERE Account__c = :mySchoolId
                    AND Academic_Year__c = :academicYear
                    AND Status__c = 'Pending Qualification'
            )
            AND Academic_Year_Picklist__c = :GlobalVariables.getAcademicYear(academicYear).Name
            AND Id IN :currentYearPFSIds                   // Filter for school or affiliation with access org and year
            AND Payment_Status__c = :PFS_PAYMENT_STATUS_UNPAID
            ORDER BY Parent_A__r.LastName
            ]);

            // Clear PFS Ids because we don't need them after creating our query locator.
            // We want to avoid storing them unnecessarily because they take up a large amount of view state.
            // For KS school portal users the PFS Ids could take up about 45 KB of view state alone.
            this.pfsIds = new List<Id>();

            return recordQuerier;
        }

        public override List<PFSCheckWrapper> wrapPFSSetCtrlRecords(ApexPages.StandardSetController setCtrl,
                Map<String, PFSCheckWrapper> selectedApplicantsByPfsId, Id academicYear) {
            List<PFS__c> pfsList = (List<PFS__c>) setCtrl.getRecords();
            List<PFSCheckWrapper> wrappers = new List<PFSCheckWrapper>();
            for (PFS__c pfs : pfsList ) {
                PFSCheckWrapper wrapper = buildWrapper(pfs);
                wrapper.isChecked = (pfs.Fee_Waived__c == 'Yes') || selectedApplicantsByPfsId.containsKey(pfs.Id);
                wrapper.isDisabled = (pfs.Fee_Waived__c == 'Yes') || (pfs.Payment_Status__c == 'Paid in Full');
                wrappers.add(wrapper);
            }

            return wrappers;
        }
    }

    /**
     * @description Logic for the SchoolFeeWaiversAssigned page to display assigned fee waivers.
     */
    private class AssignedFeeWaivers extends SchoolFeeWaiversController.FeeWaivers{

        protected override void setPfsAndApplicantIds(Id academicYear, Id mySchoolId) {
            pfsIds = new List<Id>();
            applicantIds = new Set<Id>();
            for (School_PFS_Assignment__c spa : [SELECT Applicant__c, Applicant__r.PFS__c, Applicant__r.PFS__r.Parent_A__c, Applicant__r.PFS__r.Parent_A__r.Name FROM School_PFS_Assignment__c
            WHERE School__c = :mySchoolId
            AND Academic_Year_Picklist__c = :GlobalVariables.getAcademicYear(academicYear).Name ]) {
                pfsIds.add(spa.Applicant__r.PFS__c);
                applicantIds.add(spa.Applicant__c);
            }
        }

        public override Database.QueryLocator getPfsLocator(Id academicYear, Id mySchoolId) {
            setPfsAndApplicantIds(academicYear, mySchoolId);
            List<Id> currentYearPFSIds = getCurrentYearPfsIds();
            Database.QueryLocator recordQuerier = Database.getQueryLocator([SELECT
                    PFS_Number__c, Payment_Status__c, PFS_Status__c, Fee_Waived__c, Parent_A__r.AccountId,
                    Parent_A__r.FirstName, Parent_A__r.LastName, Parent_A__r.Email, Parent_A__r.Preferred_Phone_Number__c, Total_Income_Current__c, Total_Salary_and_Wages__c,
            (SELECT First_Name__c, Last_Name__c, Grade_in_Entry_Year__c FROM Applicants__r order by Contact__r.Name)
            FROM PFS__c
            WHERE Parent_A__c IN
            (SELECT Contact__c
            FROM Application_Fee_Waiver__c
            WHERE (Account__c = :mySchoolId OR Account__c = :meansBasedId)
            AND Academic_Year__c = :academicYear
            AND (Status__c = 'Assigned' OR Status__c = 'Pending Qualification'))

            AND Id IN :currentYearPFSIds

            ORDER BY Parent_A__r.LastName
            ]);

            // Clear PFS Ids because we don't need them after creating our query locator.
            // We want to avoid storing them unnecessarily because they take up a large amount of view state.
            // For KS school portal users the PFS Ids could take up about 45 KB of view state alone.
            this.pfsIds = new List<Id>();

            return recordQuerier;
        }

        public override List<PFSCheckWrapper> WrapPFSSetCtrlRecords(ApexPages.StandardSetController setCtrl,
                Map<String, PFSCheckWrapper> selectedApplicantsByPfsId, Id academicYear) {

            List<PFS__c> pfsList = (List<PFS__c>) setCtrl.getRecords();
            //SFP-131, [G.S]
            Set<Id> parentSet = new Set<Id>();
            for (PFS__c pfs : pfsList ) {
                if (String.isNotBlank(pfs.Parent_A__c)) {
                    parentSet.add(pfs.Parent_A__c);
                }
            }
            Set<Id> meansBasedSet = new Set<Id>();
            for (Application_Fee_Waiver__c afw : [SELECT Account__c, Contact__c FROM Application_Fee_Waiver__c
            WHERE Contact__c IN :parentSet AND Academic_Year__c = :academicYear
            AND Status__c = 'Assigned' AND Account__c = :meansBasedId
            ORDER BY CreatedDate DESC]) {
                meansBasedSet.add(afw.Contact__c);
            }

            List<PFSCheckWrapper> wrappers = new List<PFSCheckWrapper>();
            for (PFS__c pfs : pfsList ) {
                PFSCheckWrapper wrapper = buildWrapper(pfs);
                //SFP-131, [G.S]
                if (String.isNotBlank(pfs.Parent_A__c) && meansBasedSet.contains(pfs.Parent_A__c)) {
                    wrapper.isMeansBased = true;
                }
                wrappers.add(wrapper);
            }
            return wrappers;
        }
    }

    /**
     * @description Called by the SchoolFWPager. Pages records and adds any selected rows to the selected map.
     */
    public void previous() {
        assignedSetCtrl.previous();
        updateListOfSelectedApplicants(assignedList);
        assignedList = feeWaiversInstance.wrapPFSSetCtrlRecords(assignedSetCtrl, selectedApplicantsByPfsId, getAcademicYearId());
    }

    /**
     * @description Called by the SchoolFWPager. Pages records and adds any selected rows to the selected map.
     */
    public void next() {
        assignedSetCtrl.next();
        updateListOfSelectedApplicants(assignedList);
        assignedList = feeWaiversInstance.wrapPFSSetCtrlRecords(assignedSetCtrl, selectedApplicantsByPfsId, getAcademicYearId());
    }

    private void updateListOfSelectedApplicants(List<PFSCheckWrapper> currentList) {
        for(PFSCheckWrapper wrapper : currentList) {
            if(wrapper.isChecked == true) {
                selectedApplicantsByPfsId.put(wrapper.PFSId, wrapper);
            } else {
                selectedApplicantsByPfsId.remove(wrapper.PFSId);
            }
        }
    }

    /**
     * @description Assign fee waivers to all of the selected PFS's. A school must have available waivers to assign
     * otherwise an error message is displayed.
     */
    public void assignWaivers() {
        Set<Id> pfsIdsToLock = new Set<Id>{};
        applicantsWithAssignedFeeWaivers = new List<PFSCheckWrapper>();

        for (PFSCheckWrapper singlePfsCheckWrapper : selectedApplicantsByPfsId.values()) {
            if ((singlePfsCheckWrapper != null) && (singlePfsCheckWrapper.isChecked == true) 
                && ((singlePfsCheckWrapper.feeWaived == null) || (singlePfsCheckWrapper.feeWaived == 'No'))) {

                pfsIdsToLock.add(singlePfsCheckWrapper.PFSId);
            }
        }

        if (pfsIdsToLock.size() > 0) {
            List<PFS__c> pfsToLock = new List<PFS__c>();

            try {
                pfsToLock = PfsSelector.Instance.selectByIdForUpdate(pfsIdsToLock);
            } catch (Exception ex) {
                if (pfsIdsToLock.size() == 1) {
                    applicantsWithAssignedFeeWaivers.add(selectedApplicantsByPfsId.get(new List<Id>(pfsIdsToLock)[0]));
                }

                showFeeWaiverConfirm = false;
                applicantsHaveFeeWaiver = true;

                return;
            }
            
            if (pfsIdsToLock.size() > pfsToLock.size()) {
                showFeeWaiverConfirm = false;
                applicantsHaveFeeWaiver = true;

                return;
            }

            for (PFS__c pfs : pfsToLock) {
                if (pfs.Payment_Status__c != PFS_PAYMENT_STATUS_UNPAID || pfs.Fee_Waived__c == 'Yes') {
                    applicantsWithAssignedFeeWaivers.add(selectedApplicantsByPfsId.get(pfs.Id));
                }
            }

            if (applicantsWithAssignedFeeWaivers.size() != 0) {
                applicantsHaveFeeWaiver = true;

                return;
            }
        }

        List<Application_Fee_Waiver__c> insertAfwlList = new List<Application_Fee_Waiver__c>();
        List<Id> waivedContactIds = new List<Id>();
        Set<Id> pfsIdsForOpps = new Set<Id>(); // [CH] NAIS-2102 List of PFSs to get Sale Opportunities and TLIs

        iAssignedCount = 0;

        //Make sure we add any selected on the current page to the map of selected
        updateListOfSelectedApplicants(assignedList);
        // Current List
        for (PFSCheckWrapper w : selectedApplicantsByPfsId.values()) {
            if ((w != null) && (w.isChecked == true) && ((w.feeWaived == null) || (w.feeWaived == 'No'))) {
                // [CH] NAIS-2102 Collect list of PFSs to get Sale Opportunities and TLIs
                pfsIdsForOpps.add(w.PFSId);

                // Applicaiton Fee Waiver
                Application_Fee_Waiver__c afw = new Application_Fee_Waiver__c();
                afw.Academic_Year__c = getAcademicYearId();
                // [CH] NAIS-1666 afw.Academic_Year__c = myAcademicYear.Id;
                afw.Account__c = mySchoolId;
                afw.Contact__c = w.ParentId;
                afw.Person_Who_Offered_Waiver__c = GlobalVariables.getCurrentUser().Contact.Name;

                // SFP-190 - if application is submitted assign directly
                afw.Status__c = (w.PFSStatus == 'Application Submitted' ? 'Assigned' : 'Pending Qualification');

                insertAfwlList.add(afw);

                // PFS
                w.feeWaived = 'Yes';

                // [SL] NAIS-1032: fee waived flag is now set by the Opportunity trigger
                // [SL] NAIS-1032: only update the Fee_Waived__c field, to avoid overwriting other fields with old values

                iAssignedCount++;
            }
        }

        // if iAssignedCount <= how many we have
        if (iAssignedCount <= myAnnualSettings.Waiver_Credit_Balance_Formula__c) {
            // [CH] NAIS-2102 Insert Opportunity and TLI records for each PFS that needs it
            // Note: Logic to check for existing Sales type Opportunities is already in
            //          the FinanceAction method

            // [DP] 07.15.2015 NAIS-1933 no longer creating the Opp and Sale TLI until the PFS is submitted
            //FinanceAction.createOppAndTransaction(pfsIdsForOpps);

            insert insertAfwlList; // New App Fee Waivers.

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, iAssignedCount + ' Fee Waivers Assigned.'));

        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                    'Not enough Fee Waiver Credits available. Please remove '
                            + (iAssignedCount - myAnnualSettings.Waiver_Credit_Balance_Formula__c) + ' selections. No Fee Waivers granted.'));
            iAssignedCount = 0;
        }
    }

    /**
     * @description Returns the pagereference to the receipt of Fee Waivers purchasing.
     */
    public PageReference generateReceipt (){
        Id opportunityId = UrlService.Instance.getIdByType(OPPORTUNITY_ID_PARAM, Opportunity.getSObjectType());
        Pagereference receiptPage = null;

        if (opportunityId != null && String.isNotBlank(opportunityId)) {
            receiptPage = Page.SchoolReceipt;
            receiptPage.getParameters().put('Id', opportunityId);
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was a problem processing your request.'));
        }
        
        return receiptPage;
    }
}