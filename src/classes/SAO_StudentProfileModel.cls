/**
 * @description Used to represent the student profiles that we receive from the SAO Integration with their Profiles API.
 *              We request a parent Profile from the SAO API and from that, we are able to gather the related student
 *              profiles. These student profiles contain the Submitted SAO Applications.
 */
public class SAO_StudentProfileModel {

    // Represents the SAO Profile Response property that contains all the other profiles within the parent profile.
    // This will include the "child profiles" which are the children managed by the parent in the SAO System.
    private static final String RELATIONS = 'relations';

    // The relationship property on the SAO response that we use to identify that parent's children.
    private static final String RELATIONSHIP = 'relationship';

    // The SAO property that tells us the relationship type.
    private static final String RELATIONSHIP_TYPE = 'relationshipType';

    // The SAO relationship type that identifies the students/children.
    private static final String STUDENT_PROFILE_TYPE = 'Child';

    // The SAO property that represents a single related profile. A related profile
    // represents a student and will contain the SAO Applications.
    private static final String RELATED_PROFILE = 'relatedProfile';

    // The SAO property that contains additional information for the related profiles (students).
    // For our purposes, this will contain the SAO Application data.
    private static final String EXTENSIONS = 'extensions';

    // The SAO property where the applications are found.
    private static final String SAO_APPLICATIONS = 'submittedSaoApplications';

    private static final String STUDENT_ID = 'folioId';
    private static final String APPLICATION_YEAR = 'year';
    private static final String SCHOOL = 'school';
    private static final String SCHOOL_CODE = 'ncesCode';

    private List<Student> students;

    private SAO_StudentProfileModel(List<Student> studentModels) {
        students = studentModels;
    }

    /**
     * @description Gets the individual students.
     */
    public List<Student> getStudents() {
        return students;
    }

    /**
     * @description Filters the raw data from SAO to give us a model containing the students that have SAO applications
     *              for the specified academic year.
     */
    public static SAO_StudentProfileModel createModelForYear(Map<String, Object> rawParentProfileResponse, String academicYear) {
        List<Student> studentsForYear = getStudentsForYear(rawParentProfileResponse, academicYear);

        return new SAO_StudentProfileModel(studentsForYear);
    }

    /**
     * @description Contains the data necessary for pulling a single student and their applications into our system
     *              for a particular academic year.
     */
    public class Student {

        private List<String> schoolCodes;
        private Map<String, Object> rawData;
        private List<Map<String, Object>> filteredApplications;

        private Student(Map<String, Object> rawStudentData, List<String> schoolCodesFromSao) {
            rawData = rawStudentData;
            schoolCodes = schoolCodesFromSao;
        }

        private Student(Map<String, Object> rawStudentData, List<Map<String, Object>> filteredApplicationData, List<String> schoolCodesFromSao) {
            rawData = rawStudentData;
            schoolCodes = schoolCodesFromSao;
            filteredApplications = filteredApplicationData;
        }

        /**
         * @description Gets the school codes for this students applications.
         */
        public List<String> getSchoolCodes() {
            return this.schoolCodes == null ? new List<String>() : this.schoolCodes;
        }

        /**
         * @description The raw data received from SAO that represents a student profile.
         */
        public Map<String, Object> getRawData() {
            return this.rawData == null ? new Map<String, Object>() : this.rawData;
        }

        public List<Map<String, Object>> getApplicationData() {
            return this.filteredApplications == null ? new List<Map<String, Object>>() : this.filteredApplications;
        }

        /**
         * @description Gets the unique identifier for this student used in the SAO system.
         */
        public String getExternalId() {
            return String.ValueOf(getRawData().get(STUDENT_ID));
        }
    }

    private static List<Student> getStudentsForYear(Map<String, Object> rawParentProfileResponse, String academicYear) {
        List<Student> studentsForYear = new List<Student>();

        List<Map<String, Object>> rawStudentProfiles = getRawStudentProfiles(rawParentProfileResponse);

        // TODO SFP-1551 Confirm with SAO/SSS that this is correct. We might actually have to use the right 4 digits.
        // Get the left four digits from the academic year (e.g. 2018-2019) and use them to identify the applications to import.
        // In SAO, applicants apply for Fall 2018, Fall 2019, etc. Which corresponds to the upcoming school year.
        // Fall 2018 == 2018-2019 academic year in our system.
        String saoYear = academicYear.left(4);

        List<String> schools;
        List<Map<String, Object>> applicationsForYear;

        // Iterate over the raw student data and look for students with applications for the specified year.
        // We must do this ourselves because SAO doesn't filter the data for us.
        for (Map<String, Object> rawStudentData : rawStudentProfiles) {
            schools = new List<String>();
            applicationsForYear = new List<Map<String, Object>>();

            // First get the extensions from the student profile which contains the SAO applications.
            // Skip this student if the necessary data is missing.
            Map<String, Object> saoExtensions = (Map<String, Object>)rawStudentData.get(EXTENSIONS);
            if (saoExtensions == null || saoExtensions.isEmpty()) {
                continue;
            }

            // Now pull the applications from the extensions.
            // Skip this student if the necessary data is missing.
            List<Object> submittedSaoApplications = (List<Object>)saoExtensions.get(SAO_APPLICATIONS);
            if (submittedSaoApplications == null || submittedSaoApplications.isEmpty()) {
                continue;
            }

            for (Object rawApplication : submittedSaoApplications) {
                Map<String, Object> application = (Map<String, Object>)rawApplication;
                if (application == null || application.isEmpty()) {
                    continue;
                }

                String applicationYear = parseStr(application.get(APPLICATION_YEAR));
                String schoolCode = getSchoolCodeFromApplication(application);

                if (saoYear.equalsIgnoreCase(applicationYear) && String.isNotBlank(schoolCode)) {
                    applicationsForYear.add(application);
                    schools.add(schoolCode);
                }
            }

            // If we have found applications with school codes for the specified academic year,
            // update the extensions.submittedSaoApplications property of the raw application and include it in our results.
            // We update the raw data because SAO includes all of the SAO applications for the student so we have to filter ourselves.
            // The raw data will later be processed by our integration mapping framework.
            if (!applicationsForYear.isEmpty()) {
                saoExtensions.put(SAO_APPLICATIONS, applicationsForYear);
                rawStudentData.put(EXTENSIONS, saoExtensions);
                studentsForYear.add(new Student(rawStudentData, applicationsForYear, schools));
            }
        }

        return studentsForYear;
    }

    private static String getSchoolCodeFromApplication(Map<String, Object> saoApplication) {
        Map<String, Object> school = (Map<String, Object>)saoApplication.get(SCHOOL);

        if (school == null || school.isEmpty()) {
            return null;
        }

        return parseStr(school.get(SCHOOL_CODE));
    }

    /**
     * @description Takes a map representing the raw parent profile response from SAO and returns a model which only contains the related child profiles.
     */
    private static List<Map<String, Object>> getRawStudentProfiles(Map<String, Object> rawParentProfileResponse) {
        List<Object> parentProfileRelations = (List<Object>)rawParentProfileResponse.get(RELATIONS);

        List<Map<String, Object>> studentProfiles = new List<Map<String, Object>>();

        for (Object parentRelation : parentProfileRelations) {
            Map<String, Object> possibleChildRelation = (Map<String, Object>)parentRelation;

            // Get the relationship info so we can determine if this is for a student.
            Map<String, Object> relationship = (Map<String, Object>)possibleChildRelation.get(RELATIONSHIP);

            // If the relationship is null or empty, skip it since we can't tell if it is a student or not.
            if (relationship == null || relationship.isEmpty()) {
                continue;
            }

            String relationshipType = (String)relationship.get(RELATIONSHIP_TYPE);

            // If the relationship type is blank or is not a student, skip it.
            if (String.isBlank(relationshipType) || !STUDENT_PROFILE_TYPE.equalsIgnoreCase(relationshipType)) {
                continue;
            }

            Map<String, Object> studentProfile = (Map<String, Object>)possibleChildRelation.get(RELATED_PROFILE);

            // If we don't actually have any information on the student, skip it since there won't be anything to add to our system.
            if (studentProfile == null || studentProfile.isEmpty()) {
                continue;
            }

            studentProfiles.add(studentProfile);
        }

        return studentProfiles;
    }

    private static String parseStr(Object s) {

        return s == null ? '' : (String.ValueOf(s)).trim();
    }
}