@isTest
private class CopyVerifForPfsaBatchSchedulerTest {
    
    @isTest
    private static void testSchedule() {
        Test.startTest();
            ScheduleSharingJobsController.deleteJobsRemotely();
            
            Id job0Id = System.schedule('Test Schedule', '0  0 * * * ?', new CopyVerifForPfsaBatchScheduler());
        Test.stopTest();

        CronTrigger ct = [SELECT Id, CronExpression FROM CronTrigger WHERE Id = :job0Id];
        System.assertEquals('0  0 * * * ?', ct.CronExpression);
    }
}