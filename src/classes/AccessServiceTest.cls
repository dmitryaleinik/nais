@isTest
private class AccessServiceTest {

    private static void setAccessCookie() {
        Cookie earlyAccessCookie = new Cookie(AccessService.EARLY_ACCESS_COOKIE, 'true', '/', 300, false);
        ApexPages.currentPage().setCookies(new List<Cookie> { earlyAccessCookie });
    }

    private static Cookie getAccessCookie() {
        return ApexPages.currentPage().getCookies().get(AccessService.EARLY_ACCESS_COOKIE);
    }

    private static void assertAccessCookieSet() {
        Cookie earlyAccessCookie = getAccessCookie();

        System.assertNotEquals(null, earlyAccessCookie, 'Expected the access cookie to not be null.');
    }

    private static void assertAccessCookieNotSet() {
        Cookie earlyAccessCookie = getAccessCookie();

        System.assertEquals(null, earlyAccessCookie, 'Expected the access cookie to be null.');
    }

    private static void setAccessUrlParam() {
        ApexPages.currentPage().getParameters().put(AccessService.EARLY_ACCESS_URL_PARAM, 'true');
    }

    private static User getUserById(Id userId) {
        Set<Id> userIds = new Set<Id> { userId };

        List<User> userRecords = UserSelector.newInstance().selectById(userIds);

        return userRecords.isEmpty() ? null : userRecords[0];
    }

    @isTest
    private static void newInstance_expectInstanceReturned() {
        System.assertNotEquals(null, AccessService.newInstance(), 'Expected the instance to not be null.');
    }

    @isTest
    private static void grantEarlyAccess_earlyAccessCookieNotSet_expectUserNotUpdated() {
        User familyUser = UserTestData.insertFamilyPortalUser();

        System.runAs(familyUser) {
            AccessService.Instance.grantEarlyAccess();
        }

        familyUser = getUserById(familyUser.Id);

        System.assert(!familyUser.Early_Access__c,
                'Expected the user to not have early access when the cookie was not set.');
    }

    @isTest
    private static void grantEarlyAccess_earlyAccessCookieSet_expectUserUpdated() {
        setAccessCookie();

        User familyUser = UserTestData.insertFamilyPortalUser();

        System.runAs(familyUser) {
            Test.startTest();
            AccessService.Instance.grantEarlyAccess();
            Test.stopTest();
        }

        familyUser = getUserById(familyUser.Id);

        System.assert(familyUser.Early_Access__c, 'Expected the user to have early access when the cookie is set.');
    }

    @isTest
    private static void grantEarlyAccess_userAlreadyHasEarlyAccess_expectUserNotUpdatedAgain() {
        setAccessCookie();

        User familyUser = UserTestData.createFamilyPortalUser();
        familyUser.Early_Access__c = true;
        insert familyUser;

        System.runAs(familyUser) {
            Test.startTest();
            AccessService.Instance.grantEarlyAccess();
            System.assertEquals(0, Limits.getDmlStatements());
            Test.stopTest();
        }
    }

    @isTest
    private static void isEarlyAccessOpen_earlyAccessStartDateNull_expectFalse() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .forEarlyAccessStartDate(null).DefaultFamilyPortalSettings;

        System.assert(!AccessService.Instance.isEarlyAccessOpen(),
                'Expected early access to not be open when the start date is null.');
    }

    @isTest
    private static void isEarlyAccessOpen_earlyAccessStartDateTomorrow_expectFalse() {
        DateTime startDate = System.now().addDays(1);
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .forEarlyAccessStartDate(startDate).DefaultFamilyPortalSettings;

        System.assert(!AccessService.Instance.isEarlyAccessOpen(),
                'Expected early access to not be open when the start date is yesterday.');
    }

    @isTest
    private static void isEarlyAccessOpen_earlyAccessStartDateToday_expectTrue() {
        DateTime startDate = System.now();
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .forEarlyAccessStartDate(startDate).DefaultFamilyPortalSettings;

        System.assert(AccessService.Instance.isEarlyAccessOpen(),
                'Expected early access to be open when the start date is today.');
    }

    @isTest
    private static void isEarlyAccessOpen_earlyAccessStartDateYesterday_expectTrue() {
        DateTime startDate = System.now().addDays(-1);
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .forEarlyAccessStartDate(startDate).DefaultFamilyPortalSettings;

        System.assert(AccessService.Instance.isEarlyAccessOpen(),
                'Expected early access to be open when the start date is yesterday.');
    }

    @isTest
    private static void setEarlyAccessCookie_earlyAccessClosedNoUrlParam_expectNoCookieSet() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        AccessService.Instance.setEarlyAccessCookie();

        assertAccessCookieNotSet();
    }

    @isTest
    private static void setEarlyAccessCookie_earlyAccessClosedUrlParamSet_expectNoCookieSet() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        setAccessUrlParam();

        AccessService.Instance.setEarlyAccessCookie();

        assertAccessCookieNotSet();
    }

    @isTest
    private static void setEarlyAccessCookie_earlyAccessOpenUrlParamNotSet_expectNoCookieSet() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessOpened().DefaultFamilyPortalSettings;

        AccessService.Instance.setEarlyAccessCookie();

        assertAccessCookieNotSet();
    }

    @isTest
    private static void setEarlyAccessCookie_earlyAccessOpenUrlParamSet_expectCookieSet() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessOpened().DefaultFamilyPortalSettings;

        setAccessUrlParam();

        AccessService.Instance.setEarlyAccessCookie();

        assertAccessCookieSet();
    }

    @isTest
    private static void isEarlyAccessCookieSet_cookieNotSet_expectFalse() {
        System.assert(!AccessService.Instance.isEarlyAccessCookieSet(), 'Expected the cookie to not be set.');
    }

    @isTest
    private static void isEarlyAccessCookieSet_cookieIsSet_expectTrue() {
        setAccessCookie();
        System.assert(AccessService.Instance.isEarlyAccessCookieSet(), 'Expected the cookie to be set.');
    }
}