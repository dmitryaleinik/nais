/**
 * IntegrationApiModel.cls
 *
 * @description: Integration Api Model, is a model class that holds Integration Api
 * State and ApiState class, to pass around while handling integration mapping.
 *
 * @author: Mike Havrila @ Presence PG
 */

public class IntegrationApiModel {

    public String apiBaseUrl { get; set; }
    public String apiEndpoint { get; set; }
    public String apiName { get; set; }
    public String nextApiName { get; set; }
    public String nextApiParameters { get; set; }
    public Boolean isActive { get; set; }
    public Boolean isEntryApi { get; set; }
    public List<String> preProcessClasses { get; set; }
    public List<String> postProcessClasses { get; set; }

    public IntegrationApiModel( Integration_API_Setting__mdt mdt, String apiBaseUrl) {

        this.apiBaseUrl = apiBaseUrl;
        this.apiEndpoint = mdt.Api_Endpoint__c;
        this.apiName = mdt.ApiName__c;
        this.nextApiName = mdt.Next_Api_Name__c;
        this.nextApiParameters = mdt.Next_Api_Parameters__c;
        this.isActive = mdt.IsActive__c;
        this.isEntryApi = mdt.IsFirstApi__c;

        this.preProcessClasses = String.isNotBlank( mdt.PreProcessClass__c) ?
                mdt.PreProcessClass__c.split( ',') : new List<String>();

        this.postProcessClasses = String.isNotBlank( mdt.PostProcessClass__c) ?
                mdt.PostProcessClass__c.split( ',') : new List<String>();
    }

    public void preProcess( IntegrationApiModel.ApiState apiState) {

        if ( this.preProcessClasses.size() > 0) {

            for( String preProcessClass : this.preProcessClasses) {

                Type t = Type.forName( preProcessClass.trim());
                IAction action = ( IAction) t.newInstance();
                action.invoke( apiState);
            }
        }
    }

    public void postProcess( IntegrationApiModel.ApiState apiState) {

        if ( this.postProcessClasses.size() > 0) {

            for( String postProcessClass : postProcessClasses) {

                Type t = Type.forName( postProcessClass.trim());
                IAction action = ( IAction) t.newInstance();
                action.invoke( apiState);
            }
        }
    }

    public class ApiState {

        public Map<String, List<Object>> httpResponseByApi { get; set; }
        public Map<String, List<String>> httpParamsByApi { get; set; }
        public Map<String, List<Object>> httpResponseByParamMap { get; set; }
        public Map<String, IntegrationApiModel> integrationModelByNameMap { get; set; }
        public String apiSource { get; set; }
        public String apiDisplayName { get; set; }

        // CALLOUT STATE;
        public String authToken { get; set; }

        // FAMILY SPECIFICS
        public FamilyIntegrationModel familyModel { get; set; }

        public String currentApi { get; set; }
        public Pfs__c pfs { get; set; }

        public IntegrationApiModel.ApiState( String source, PFS__c pfs, Map<String, IntegrationApiModel> integrationModelByNameMap  ) {

            this.httpResponseByApi = new Map<String, List<Object>>();
            this.httpResponseByParamMap = new Map<String, List<Object>>();
            this.httpParamsByApi = new Map<String, List<String>>();
            this.pfs = pfs;
            this.familyModel = new FamilyIntegrationModel( source);
            this.integrationModelByNameMap = integrationModelByNameMap;
            this.apiSource = source;
        }
    }

    public interface IAction {

        void invoke( IntegrationApiModel.ApiState apiState);
    }
}