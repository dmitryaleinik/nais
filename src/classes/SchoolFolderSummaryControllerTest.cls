/*
*   SPEC-092 Student Folder & Aid Allocation (Folder Summary), R-167, R-180, R-125, R-161, R-117 20-24 hours [Drew]
*   The school folder is a VF page, with some folder level items and statuses, and links to the one or two PFS' within. 
*   The main folder page is also currently where the Aid Allocation is done. There are a number of editable fields which display the inputs to the aid decision and some can be revised as well. 
*   Need to populate the Tuition fields (4 of them) based on grade and the enrollment status, and day vs. boarding." 
*   - School user should be able to easily navigate between sibling folders
*   - The UI should clearly differentiate folder information (which is relevant to all PFS's) versus single PFS information
*   - School user should be able to obviously see when a folder has two PFS's in it
*   - School should be able to adjust the tuition & fees at the folder level which will allow them to apply their own discounts for siblings, etc.
*   - A folder should have a visual indicator if the student is new or returning
*   - A school aid officer should be able to allocate financial aid from any number of budgets they have specified.
*/
@isTest
private class SchoolFolderSummaryControllerTest {

    private static final String PFS_STATUS_UNPAID = 'Unpaid';

    private static Account school1;
    private static Contact student1;
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    private static User schoolPortalUser;
    private static Academic_Year__c currentAcademicYear, previousAcademicYear;
    private static Contact parentA, parentB, parentC;

    private static void createTestData() {
        Account family = AccountTestData.Instance.asFamily().create();

        String activeStatus = GlobalVariables.getActiveSSSStatusForTest();
        school1 = AccountTestData.Instance.asSchool()
            .forSSSSubscriberStatus(activeStatus)
            .forPortalVersion(SubscriptionTypes.BASIC_TYPE)
            .asSchool().create();
        Account school2 = AccountTestData.Instance
            .forSSSSubscriberStatus(activeStatus)
            .forPortalVersion(SubscriptionTypes.BASIC_TYPE)
            .asSchool().create();
        insert new List<Account>{family, school1, school2};

        Subscription__c subscription1 = SubscriptionTestData.Instance
            .forStartDate(system.today().addDays(-14))
            .forEndDate(system.today().addYears(1))
            .forAccount(school1.Id).create();
        Subscription__c subscription2 = SubscriptionTestData.Instance
            .forStartDate(system.today().addDays(-14))
            .forEndDate(system.today().addYears(1))
            .forAccount(school2.Id).create();
        insert new List<Subscription__c> {subscription1, subscription2};

        currentAcademicYear = AcademicYearTestData.Instance.create();
        previousAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, previousAcademicYear};

        parentA = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        parentB = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        parentC = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .forAccount(school1.Id)
            .asSchoolStaff().create();
        student1 = ContactTestData.Instance.asStudent().create();
        Contact student2 = ContactTestData.Instance.asStudent().create();
        Contact student3 = ContactTestData.Instance.asStudent().create();
        insert new List<Contact>{parentA, parentB, parentC, schoolStaff, student1, student2, student3};

        System.runAs(thisUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        // SFP-672 Create Share between schoolPortalUser and its school because criteria based sharing rules do not run in
        // unit tests.
        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);

        PFS__c pfs1, pfs2, pfs1Past, pfs2Past;
        School_PFS_Assignment__c spfsa1, spfsa2, spfsa3, spfsa1Past, spfsa2Past, spfsa1School2;

        System.runAs(schoolPortalUser){
            String isPfsFeeWaived = 'Yes';
            
            pfs1 = PfsTestData.Instance
                .asSubmitted()
                .forPaymentStatus(PFS_STATUS_UNPAID)
                .forParentA(parentA.Id)
                .forOriginalSubmissionDate(System.today().addDays(-3))
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            pfs2 = PfsTestData.Instance
                .asSubmitted()
                .forPaymentStatus(PFS_STATUS_UNPAID)
                .forParentA(parentB.Id)
                .forOriginalSubmissionDate(System.today().addDays(-5))
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            pfs1Past = PfsTestData.Instance
                .asSubmitted()
                .forParentA(parentA.Id)
                .forOriginalSubmissionDate(Date.newInstance(System.today().addYears(-5).year(), 12, 12))
                .forAcademicYearPicklist(previousAcademicYear.Name).create();
            pfs2Past = PfsTestData.Instance
                .asSubmitted()
                .forParentA(parentC.Id)
                .forOriginalSubmissionDate(Date.newInstance(System.today().addYears(-5).year(), 12, 12))
                .forFeeWaived(isPfsFeeWaived)
                .forAcademicYearPicklist(previousAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<PFS__c> {pfs1, pfs2, pfs1Past, pfs2Past});

            Applicant__c applicant1A = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student1.Id).create();
            Applicant__c applicant1B = ApplicantTestData.Instance
                .forPfsId(pfs2.Id)
                .forContactId(student1.Id).create();
            Applicant__c applicant2A = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student2.Id).create();
            Applicant__c applicant1School2 = ApplicantTestData.Instance
                .forPfsId(pfs1Past.Id)
                .forContactId(student1.Id).create();
            Applicant__c applicant1Past = ApplicantTestData.Instance
                .forPfsId(pfs1Past.Id)
                .forContactId(student1.Id).create();
            Applicant__c applicant2Past  = ApplicantTestData.Instance
                .forPfsId(pfs2Past.Id)
                .forContactId(student3.Id).create();
            Dml.WithoutSharing.insertObjects(
                new List<Applicant__c> {applicant1A, applicant1B, applicant2A, applicant1Past, applicant2Past, applicant1School2});

            Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
                .forStudentId(student1.Id)
                .forSchoolId(school1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Student_Folder__c studentFolder2 = StudentFolderTestData.Instance
                .forStudentId(student2.Id)
                .forSchoolId(school1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Student_Folder__c studentFolder1Past = StudentFolderTestData.Instance
                .forStudentId(student1.Id)
                .forSchoolId(school1.Id)
                .forAcademicYearPicklist(previousAcademicYear.Name).create();
            Student_Folder__c studentFolder2Past = StudentFolderTestData.Instance
                .forStudentId(student3.Id)
                .forSchoolId(school1.Id)
                .forAcademicYearPicklist(previousAcademicYear.Name).create();
            Student_Folder__c studentFolder1School2 = StudentFolderTestData.Instance
                .forStudentId(student1.Id)
                .forSchoolId(school2.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<Student_Folder__c> {
                studentFolder1, studentFolder2, studentFolder1Past, studentFolder2Past, studentFolder1School2});

            String fifthGrade = '5';
            spfsa1 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1A.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school1.Id).create();
            spfsa2 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1B.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school1.Id).create();
            spfsa3 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant2A.Id)
                .forStudentFolderId(studentFolder2.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school1.Id).create();
            spfsa1Past = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1Past.Id)
                .forStudentFolderId(studentFolder1Past.Id)
                .forAcademicYearPicklist(previousAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school1.Id).create();
            spfsa2Past = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant2Past.Id)
                .forStudentFolderId(studentFolder2Past.Id)
                .forAcademicYearPicklist(previousAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school1.Id).create();
            spfsa1School2 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1School2.Id)
                .forStudentFolderId(studentFolder1School2.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school2.Id).create();

            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> {
                spfsa1, spfsa2, spfsa3, spfsa1Past, spfsa2Past, spfsa1School2});
        }

        // Share records to school staff
        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);

        createDependents(new Set<Id>{pfs1.Id, pfs2.Id, pfs1Past.Id});
    }

    @isTest
    private static void testJustFolderInit(){
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
        
        createTestData();

        List<Student_Folder__c> studentFolders = StudentFolderSelector.newInstance().selectByStudentId_schoolId_academicYearName(
            new Set<Id>{student1.Id}, new Set<Id>{school1.Id}, new Set<String>{currentAcademicYear.Name});
        System.assertEquals(1, studentFolders.size());
        Student_Folder__c studentFolder1 = studentFolders[0];
        
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(studentFolder1);
            SchoolFolderSummaryController testCont = new SchoolFolderSummaryController(sc);
            
            PageReference pageRef = Page.SchoolFolderSummary;
            pageRef.getParameters().put('id', String.valueOf(studentFolder1.Id));
            Test.setCurrentPage(pageRef);
            
            System.assertEquals(1, testCont.getSiblings().size());
            System.assertEquals(1, testCont.getOtherApps().size());
            System.assertEquals(2, testCont.getDependents().size());
            
            testCont.setAcademicYearId(previousAcademicYear.Id);
            PageReference pr = testCont.SchoolAcademicYearSelectorStudent_OnChange(new PageReference('string'));
            
            testCont.spfs1.PFS_Document_Status__c = 'Missing Docs'; // fake status
            testCont.spfs2.PFS_Document_Status__c = 'Required Documents Complete';
            testCont.folder.Folder_Status__c = 'In progress';
            testCont.calculateColorAndMore();
            // docs missing to start, yellow
            System.assertEquals('yellow', testCont.color);
            
            testCont.spfs1.PFS_Document_Status__c = 'Required Documents Complete'; // fake status
            testCont.spfs2.PFS_Document_Status__c = 'Required Documents Complete';
            testCont.docsCompleted = true;
            testCont.folder.Folder_Status__c = 'Verified';
            testCont.calculateColorAndMore();
            // docs there, purple
            System.assertEquals('purple', testCont.color);

            testCont.spfs1.PFS_Document_Status__c = 'Required Documents Complete'; // fake status
            testCont.spfs2.PFS_Document_Status__c = 'Required Documents Complete';
            testCont.folder.Folder_Status__c = 'Award Proposed';
            testCont.calculateColorAndMore();
            // docs there, purple
            System.assertEquals('purple', testCont.color);

            testCont.spfs1.PFS_Document_Status__c = 'Required Documents Complete'; // fake status
            testCont.spfs2.PFS_Document_Status__c = 'Required Documents Complete';
            testCont.folder.Folder_Status__c = 'Submitted';
            testCont.calculateColorAndMore();
            // docs there, purple
            System.assertEquals('purple', testCont.color);

            testCont.spfs1.PFS_Document_Status__c = 'Required Documents Complete'; // fake status
            testCont.spfs2.PFS_Document_Status__c = 'Required Documents Complete';
            testCont.folder.Folder_Status__c = 'Award Approved';
            testCont.calculateColorAndMore();
            // docs there, purple
            System.assertEquals('green', testCont.color);

            testCont.spfs1.PFS_Document_Status__c = 'Required Documents Complete'; // fake status
            testCont.spfs2.PFS_Document_Status__c = 'Required Documents Complete';
            testCont.folder.Folder_Status__c = 'Complete, Award Accepted';
            testCont.calculateColorAndMore();
            // docs there, purple
            System.assertEquals('green', testCont.color);
            
            testCont.spfs1.PFS_Document_Status__c = 'Required Documents Complete'; // fake status
            testCont.spfs2.PFS_Document_Status__c = 'Required Documents Complete';
            testCont.folder.Folder_Status__c = 'Wait Pooled';
            testCont.calculateColorAndMore();
            // docs there, purple
            System.assertEquals('yellow', testCont.color);

            testCont.spfs1.PFS_Document_Status__c = 'Required Documents Complete'; // fake status
            testCont.spfs2.PFS_Document_Status__c = 'Required Documents Complete';
            testCont.folder.Folder_Status__c = 'Waiting on Family';
            testCont.calculateColorAndMore();
            // docs there, purple
            System.assertEquals('yellow', testCont.color);

            testCont.spfs1.PFS_Document_Status__c = 'Required Documents Complete'; // fake status
            testCont.spfs2.PFS_Document_Status__c = 'Required Documents Complete';
            testCont.folder.Folder_Status__c = 'Appealed';
            testCont.calculateColorAndMore();
            // docs there, purple
            System.assertEquals('yellow', testCont.color);

            testCont.spfs1.PFS_Document_Status__c = 'Required Documents Complete'; // fake status
            testCont.spfs2.PFS_Document_Status__c = 'Required Documents Complete';
            testCont.folder.Folder_Status__c = 'Award Denied';
            testCont.calculateColorAndMore();
            // docs there, purple
            System.assertEquals('grey', testCont.color);

            testCont.spfs1.PFS_Document_Status__c = 'Required Documents Complete'; // fake status
            testCont.spfs2.PFS_Document_Status__c = 'Required Documents Complete';
            testCont.folder.Folder_Status__c = 'Complete, Award Refused';
            testCont.calculateColorAndMore();
            // docs there, purple
            System.assertEquals('grey', testCont.color);

            testCont.spfs1.PFS_Document_Status__c = 'Required Documents Outstanding'; // fake status
            testCont.spfs2.PFS_Document_Status__c = 'Required Documents Complete';
            testCont.folder.Folder_Status__c = 'Complete, Award Refused';
            testCont.calculateColorAndMore();
            // docs there, purple
            System.assertEquals('yellow', testCont.color);

            // test incomplete sections
            testCont.getPFS1Missing();
            testCont.getPFS2Missing();
            testCont.getIsMissingSections();
            testCont.getMissingSectionsMap();
            system.assert(testCont.getPFS1Missing(),true);
            system.assert(testCont.getPFS2Missing(),true);
            system.assert(testCont.getIsMissingSections(),true);
        Test.stopTest();
    }

    @isTest
    private static void testOutstandingDocLists(){
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);

        Account family = AccountTestData.Instance.asFamily().create();

        String activeStatus = GlobalVariables.getActiveSSSStatusForTest();
        school1 = AccountTestData.Instance.asSchool()
            .forSSSSubscriberStatus(activeStatus)
            .forPortalVersion(SubscriptionTypes.BASIC_TYPE)
            .asSchool().create();
        insert new List<Account>{family, school1};

        Subscription__c subscription1 = SubscriptionTestData.Instance
            .forStartDate(system.today().addDays(-14))
            .forEndDate(system.today().addYears(1))
            .forAccount(school1.Id).create();
        insert new List<Subscription__c> {subscription1};

        currentAcademicYear = AcademicYearTestData.Instance.create();
        insert new List<Academic_Year__c>{currentAcademicYear};

        parentA = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .forAccount(school1.Id)
            .asSchoolStaff().create();
        student1 = ContactTestData.Instance.asStudent().create();
        insert new List<Contact>{parentA, schoolStaff, student1};

        System.runAs(thisUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        // SFP-672 Create Share between schoolPortalUser and its school because criteria based sharing rules do not run in
        // unit tests.
        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);

        PFS__c pfs1;
        School_PFS_Assignment__c spfsa1;
        Student_Folder__c studentFolder1;

        System.runAs(schoolPortalUser){
            pfs1 = PfsTestData.Instance
                .asSubmitted()
                .forPaymentStatus(PFS_STATUS_UNPAID)
                .forParentA(parentA.Id)
                .forOriginalSubmissionDate(System.today().addDays(-3))
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<PFS__c> {pfs1});

            Applicant__c applicant1A = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student1.Id).create();
            Dml.WithoutSharing.insertObjects(new List<Applicant__c> {applicant1A});

            studentFolder1 = StudentFolderTestData.Instance
                .forStudentId(student1.Id)
                .forSchoolId(school1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<Student_Folder__c> {studentFolder1});

            String fifthGrade = '5';
            spfsa1 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1A.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school1.Id).create();
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> {spfsa1});
        }

        // Share records to school staff
        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);

        createSchoolDocumentAssignment(spfsa1.Id);
        
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(studentFolder1);
            SchoolFolderSummaryController testCont = new SchoolFolderSummaryController(sc);
            System.assertEquals('Required Documents Outstanding', testCont.documentStatus);

            Integer outstandingDocSize = [Select count() from School_Document_Assignment__c where School_PFS_Assignment__c = :spfsa1.Id AND Document_Status__c = 'Not Received'];
        Test.stopTest();

        System.assertEquals(outstandingDocSize, testCont.pfs1OutstandingWrapper.sdaList.size());
    }

    @isTest
    private static void testGetOtherApps() {
        Academic_Year__c academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        Integer numberOfSchools = 3;
        List<Account> schools = createSchools(numberOfSchools);

        Contact student = ContactTestData.Instance.asStudent().insertContact();
        Applicant__c applicant = ApplicantTestData.Instance
            .forContactId(student.Id).insertApplicant();

        Student_Folder__c defaultStudentFolder = StudentFolderTestData.Instance
            .forStudentId(student.Id)
            .forAcademicYearPicklist(academicYear.Name).DefaultStudentFolder;
        List<Student_Folder__c> studentFolders = createStudentFoldersForSchools(schools, student.Id, academicYear.Name);

        School_PFS_Assignment__c spfsa1 = SchoolPfsAssignmentTestData.Instance
            .forStudentFolderId(studentFolders[0].Id)
            .forApplicantId(applicant.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .forSchoolId(schools[0].Id).create();
        School_PFS_Assignment__c spfsa2 = SchoolPfsAssignmentTestData.Instance
            .forStudentFolderId(studentFolders[1].Id)
            .forApplicantId(applicant.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .forSchoolId(schools[1].Id).create();
        School_PFS_Assignment__c spfsa3 = SchoolPfsAssignmentTestData.Instance
            .forStudentFolderId(studentFolders[2].Id)
            .forApplicantId(applicant.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .forSchoolId(schools[2].Id)
            .asWithdrawn().create();
        insert new List<School_PFS_Assignment__c>{spfsa1, spfsa2, spfsa3};

        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(defaultStudentFolder);
            SchoolFolderSummaryController schoolController = new SchoolFolderSummaryController(sc);
            List<SchoolFolderSummaryController.OtherApp> otherApps = schoolController.getOtherApps();
        Test.stopTest();
        
        System.assertEquals(2, otherApps.size(), 'Expect that only not withdrawn schools are shown on the page');
    }

    private static List<Account> createSchools(Integer numberOfSchools) {
        String activeSubscrStatus = GlobalVariables.getActiveSSSStatusForTest();
        List<Account> schools = new List<Account>();

        for (Integer i=0; i<numberOfschools; i++){
            Account newSchool = AccountTestData.Instance
                .asSchool()
                .forSSSSubscriberStatus(activeSubscrStatus).create();
            
            schools.add(newSchool);
        }
        insert schools;

        return schools;
    } 

    private static List<Student_Folder__c> createStudentFoldersForSchools(List<Account> schools, Id studentId, String academicYear) {
        List<Student_Folder__c> studentFolders = new List<Student_Folder__c>();

        for (Account singleSchool : schools){
            Student_Folder__c studentFolder = StudentFolderTestData.Instance
                .forSchoolId(singleSchool.Id)
                .forStudentId(studentId)
                .forAcademicYearPicklist(academicYear).create();
            
            studentFolders.add(studentFolder);
        }
        insert studentFolders;

        return studentFolders;
    } 
    
    private static void createDependents(Set<Id> pfsIds) {
        List<Dependents__c> dependents = new List<Dependents__c>();
        String schoolNextYear = 'schoolnext';
        String currentSchool = 'TestSchool';
        String gender = 'female';
        String fullName1 = 'full name1';
        String fullName2 = 'full name2';
        Date birthDate = System.today().addYears(-10);
        String currentGrade = '5';

        for(Id pfsId : pfsIds) {
            Dependents__c dependent = DependentsTestData.Instance
                .forPfsId(pfsId)
                .forDependent1SchoolNextYear(schoolNextYear)
                .forDependent2SchoolNextYear(schoolNextYear)
                .forDependent1Gender(gender)
                .forDependent2Gender(gender)
                .forDependent1FullName(fullName1)
                .forDependent2FullName(fullName2)
                .forDependent1Birthdate(birthDate)
                .forDependent2Birthdate(birthDate)
                .forDependent1CurrentSchool(currentSchool)
                .forDependent2CurrentSchool(currentSchool)
                .forDependent1CurrentGrade(currentGrade)
                .forDependent2CurrentGrade(currentGrade).create();

            dependents.add(dependent);
        }

        insert dependents;
    }

    private static void createSchoolDocumentAssignment(Id spfsaId) {
        Required_Document__c requiredDocument = RequiredDocumentTestData.Instance
            .forSchoolId(school1.Id)
            .forAcademicYearId(currentAcademicYear.Id).DefaultRequiredDocument;
        Family_Document__c familyDocument = FamilyDocumentTestData.Instance
            .forAcademicYearPicklist(currentAcademicYear.Name).isProcessed().DefaultFamilyDocument;
        School_Document_Assignment__c sda = SchoolDocumentAssignmentTestData.Instance
            .forPfsAssignment(spfsaId)
            .forRequirement(requiredDocument.Id)
            .forDocumentYear(currentAcademicYear.Name)
            .forDocument(familyDocument.Id).DefaultSchoolDocumentAssignment;
    }
}