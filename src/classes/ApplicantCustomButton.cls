public with sharing class ApplicantCustomButton {
    private Applicant__c applicant;
    public String resultMessage { get; set; }
    
    public ApplicantCustomButton(ApexPages.StandardController controller) {
        
        applicant = (Applicant__c) controller.getRecord();
    }//End-Constructor
    
    /**
    * @description Method implemeted to be called from a custom button in the Applicant's layout.
    *              For very special limited people they need to be able to delete an Applicant. 
    *              We would want a custom button on the Applicant record for Delete the applicant, 
    *              SPAs and SDAs and Student Folders would all be deleted for that applicant.
    *              The SDAs deletion occurs in SchoolPFSAssignmentAfter.trigger.
    */
    public PageReference DeleteApplicantFromCustomButton(){
        
        resultMessage = '';
        
        //1. Retrieve the applicant record and its related SPAs.
        List<Applicant__c> applicant = new List<Applicant__c>([
            SELECT Id,  
            (SELECT Id, Student_Folder__c 
                FROM School_PFS_Assignments__r) 
            FROM Applicant__c 
            WHERE Id = :applicant.Id 
            LIMIT 1]);
        
        
        if( !applicant.isEmpty() ) {
            
            //2. Retrieve the folder ids related to the Appliant's SPAs.
            Set<Id> folderIds = new Set<Id>();
            Set<Id> spaIds = new Set<Id>();
            List<School_PFS_Assignment__c> spas = applicant[0].School_PFS_Assignments__r;            
            for(School_PFS_Assignment__c spa : spas) {
                
                folderIds.add(spa.Student_Folder__c);
                spaIds.add(spa.Id);
            }
            
            //3. Retrieve the Student Folder records for the selected Applicant.
            List<Student_Folder__c> sf = new List<Student_Folder__c>([
                SELECT Id 
                FROM Student_Folder__c 
                WHERE Id =: folderIds]);
                
            //4. Retrieve the School Document Assignment records for the selected Applicant.
            List<School_Document_Assignment__c> sdas = new List<School_Document_Assignment__c>([
                    SELECT Id 
                    FROM School_Document_Assignment__c 
                    WHERE School_PFS_Assignment__c IN: spaIds]);
            
            System.savepoint savepoint = Database.setSavepoint();
            
            try{
	            //5. Delete related SPA records. It will automatically delete the related StudentFolder records.
	            if(!spas.isEmpty()) {
	                delete spas;
	            }
	            
	            //6. Delete applicant record.
	            if(!applicant.isEmpty()) {
	                delete applicant;
	            }
	            
	            //7. Delete related School Document Assignments: 
	            //This logic was moved to SchoolPFSAssignmentAfter.trigger as part of SFP-1149.
            }catch(Exception e){
                Database.rollback(savepoint);
                resultMessage = e.getMessage();
            }
            
            //8. Set successful message.
            if( !applicant.isEmpty() ) {
                
                resultMessage = String.format(Label.Delete_Applicant_Process_Completed, 
                                new List<String>{
                                String.ValueOf(spas.size()),
                                String.ValueOf(sf.size()),
                                String.ValueOf(sdas.size())});
            }
        } else {
            
            //9. Set failure message.
            resultMessage = 'The selected applicant no longer exists. Another user may have deleted the record.';
        }
        
        return null;
    }//End:purgeApplicant
    
    /**
    * @description Method implemented to dinamicaly get the Applicants tab url.
    */
    public String getSuccessURL() {
        
        sObjectType objectType = Applicant__c.sObjectType;
		Schema.DescribeSObjectResult objectSchema = objectType.getDescribe();
		String threeCharacterPrefix = objectSchema.getKeyPrefix();
		
		return '/'+threeCharacterPrefix;
    }//End:getSuccessURL
}