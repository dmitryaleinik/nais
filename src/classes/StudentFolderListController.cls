public class StudentFolderListController {

    public StudentFolderListController() {
        boolean hasGroups = this.schoolHasBudgetGroups();
        for(Student_Folder_List__c sfl: Student_Folder_List__c.getAll().values()){
            if(sfl.Name == 'Status/Demographics'){
                statDemo = sfl;
            }else if(sfl.Name == 'Updates/Awards' && hasGroups){
                updateAward = sfl;
            }else if(sfl.Name == 'Updates/Awards without Budget Group' && !hasGroups){
                updateAward = sfl;
            }else if(sfl.Name == 'Financials/Analysis'){
                finAnalysis = sfl;
            }else if(sfl.Name == 'Business/Farm Owners'){
                busFarm = sfl;
            }else if(sfl.Name == 'Multiple Campus'){
                multCamp = sfl;
            }else if(sfl.Name == 'Basic View'){
                basicView = sfl; // NAIS-2037 [DP] 12.11.2014
            }else if(sfl.Name == 'Non Need'){
                nonNeed = sfl; // [CH] NAIS-1866
            }
        }

        /*
        system.debug(ApexPages.currentPage().getCookies());
        system.debug(userId);

        if(ApexPages.currentPage().getCookies().containsKey('userId')){
            userIdCookie = ApexPages.currentPage().getCookies().get('userId');
            system.debug(userIdCookie.getValue());
        }
        if(ApexPages.currentPage().getCookies().containsKey('sessionid')){
            sessionIdCookie = ApexPages.currentPage().getCookies().get('sessionid');
            system.debug(sessionIdCookie.getValue());
        }
        */
    }//End-Constructor

    private boolean schoolHasBudgetGroups()
    {
        return (new list<Budget_Group__c>([Select Id from Budget_Group__c
                                                where School__c=:GlobalVariables.getCurrentSchoolId()
                                                and School__c<>null
                                                limit 1])).size()>0?true:false;
    }//End:schoolHasBudgetGroups

    public Student_Folder_List__c statDemo { get; set; }
    public Student_Folder_List__c updateAward { get; set; }
    public Student_Folder_List__c finAnalysis { get; set; }
    public Student_Folder_List__c busFarm { get; set; }
    public Student_Folder_List__c multCamp { get; set; }
    public Student_Folder_List__c basicView { get; set; }
    public Student_Folder_List__c nonNeed { get; set; } // [CH] NAIS-1866

    // [CH] NAIS-2359 Esacping apostrophes for names like O'Brien
    public String searchTerm {
        get{
            return ApexPages.currentPage().getParameters().get('q');
        }
        set;
    }

    /*public Cookie userIdCookie {get; set;}
    public Cookie sessionIdCookie {get; set;}

    public static String userId {    get{ return UserInfo.getUserId(); } }*/

    //NAIS-1824 Start
    public Boolean multiCampus {
        get{
            Boolean isMultiCampus;
            User currentUser = GlobalVariables.getCurrentUser();
            List<Affiliation__c> affiliations = [SELECT
                                                    Id,
                                                    Status__c,
                                                    Type__c
                                                FROM Affiliation__c
                                                WHERE Contact__c = :currentUser.ContactId
                                                AND Status__c = 'Current'
                                                AND Type__c = 'Additional School User'
                                                ];
            if(affiliations.size() > 0) {
                isMultiCampus = true;
            } else {
                isMultiCampus = false;
            }
            //for(Affiliation__c a : affiliations) {
                //if(a.Status__c == 'Current' && a.Type__c == 'Additional School User'){
                //    isMultiCampus = true;
                //}
            //}
            return isMultiCampus;
        }
        set;
    }
    //NAIS-1824 End

    // NAIS-2037 [DP] 12.11.2014
    public static Boolean getIsLimitedSchoolView(){
        return GlobalVariables.isLimitedSchoolView();
    }



}