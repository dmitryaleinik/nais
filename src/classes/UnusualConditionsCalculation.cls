/*
*    SPEC-065 Unusual Conditions Req# R-174, R-386, R-172 [Drew]
*    - The system should automatically run the Unusual Condition validation on the specified fields when an application is submitted.
*    - The system should be able to store Unusual Condition validations (hard-coded) 
*    - NAIS staff should be able to set-up ""Unusual Conditions"" in the system which are a set of validation rules around specific fields 
*    When the PFS is submitted, or when an update is Submitted, the Unusual Condition validation needs to run. There are approximately 20 unusual conditions, which are individual validations run on a given field. If the field value is so far out of the expected values, then it would be considered an unusual condition. 
*    The flags for the unusual conditions would be fields on the PFS as well. The flag fields would be set if the corresponding rule being evaluated is true.
*    There will be one unusual conditions object and one record per year, total,  which contains the rules for each one, predominantly hard-coded, but with some fields as variables, where percentages or limits (max, min) are set.
*/

public without sharing class UnusualConditionsCalculation {

    /*Initialization*/   
    /*End Initialization*/
   
    /*Properties*/
    
    public static List<PFS__c> pfsList;
    
    public static Boolean isFirstRun = true; 
    public static Integer debugI = 1;
    public static Integer debugN = 1;
    
    /*End Properties*/
       
    /*Action Methods*/
    
    //init method, called from VF Page in platform
    public pageReference calcAndReturn(){
        Id pfsId = System.currentPagereference().getParameters().get('id');
        Set<Id> pfsIdSet = new Set<Id>();
        pfsIdSet.add(pfsId);
        calculateConditions(pfsIdSet, true);
        return new PageReference('/' + pfsId);
    }
   
    // future method, takes PFS Id set
    //@future
    //public static void calculateConditionsFuture(Set<ID> pfsIdSet, Boolean save){
    //    calculateConditions(pfsIdSet, save);
    //}

    public static List<String> getUCFieldNames(){
        List<String> ucFieldNames = new List<String>{    'Id', 'Academic_Year_Picklist__c', 'Total_Taxable_Income_Current__c', 'Total_Income_Current__c', 'Unusual_Expenses_Current__c', 
                                                        'Total_Assets_Current__c', 'Total_Debts__c',
                                                        'Medical_Dental_Exp_Current__c', 'Other_Taxable_Income_Current__c', 'Own_Business_or_Farm__c',
                                                        'Num_Children_in_Tuition_Schools__c', 'Salary_Wages_Parent_A__c', 'Salary_Wages_Parent_A_Est__c', 
                                                        'Salary_Wages_Parent_B__c', 'Salary_Wages_Parent_B_Est__c', 
                                                        'IRS_Adjustments_to_Income_Current__c', 'Bank_Account_Value__c', 'Investments_Net_Value__c', 'Dividend_Income_Current__c', 'Interest_Income_Current__c',
                                                        'Federal_Income_Tax_Reported__c', 'Federal_Income_Tax_Calculated__c', 'Home_Equity_Uncapped__c'
                                                        //, 'Home_Equity__c', 'X1040_Business_Income_Loss__c', 'X1040_Business_Income_Loss_B__c'
        };
        
        return ucFieldNames;
    }

    // takes PFS id set
    public static void calculateConditions(Set<ID> pfsIdSet, Boolean save){
        System.debug('TESTING59 ' + debugN++);
        
        List<String> ucFieldNames = getUCFieldNames();
        String queryString = 'Select ';
        
        for (String s : ucFieldNames){
            queryString += s + ', ';
        }
        
        queryString += 'Verification__r.X1040_Business_Income_Loss__c, Verification__r.X1040_Business_Income_Loss_B__c, (Select Id, Assets_Total_Value__c from Applicants__r) from PFS__c where Id in :pfsIdSet ';
        
        List<PFS__c> pfsListParam = Database.query(queryString);
        
        /*
        List<PFS__c> pfsListParam = [Select Id, Academic_Year__c, Total_Taxable_Income_Current__c, Total_Income_Current__c, Unusual_Expenses_Current__c, Total_Assets_Current__c, Total_Debts__c,
                                                Medical_Dental_Exp_Current__c, Other_Taxable_Income_Current__c, Own_Business_or_Farm__c,
                                                Num_Children_in_Tuition_Schools__c, Salary_Wages_Parent_A__c, Salary_Wages_Parent_A_Est__c, Salary_Wages_Parent_B__c, Salary_Wages_Parent_B_Est__c, 
                                                IRS_Adjustments_to_Income_Current__c, Bank_Account_Value__c, Investments_Net_Value__c, Dividend_Income_Current__c, Interest_Income_Current__c,
                                                Federal_Income_Tax_Reported__c, Federal_Income_Tax_Calculated__c, Home_Equity__c, Home_Equity_Uncapped__c, 
                                                //X1040_Business_Income_Loss__c, X1040_Business_Income_Loss_B__c,
                                                Verification__r.X1040_Business_Income_Loss__c, Verification__r.X1040_Business_Income_Loss_B__c,
                                                (Select Id, Assets_Total_Value__c from Applicants__r) 
                                                from PFS__c where Id in :pfsIdSet ];
        */
        calculateConditions(pfsListParam, save);
    }

    // takes PFS List
    public static void calculateConditions(List<PFS__c> pfsListParam, Boolean save){
        pfsList = pfsListParam;

        Set<String> acadYearNames = new Set<String>(); // NAIS-2417 [DP] 05.19.2015
        
        // populate academic year list
        for (PFS__c pfs : pfsList){    
            acadYearNames.add(pfs.Academic_Year_Picklist__c);
        }
        
        // map of academic year name to unusual condition
        Map<String, Unusual_Conditions__c> acadYearNameToUnusualConditions = new Map<String, Unusual_Conditions__c>();
        
        for (Unusual_Conditions__c uc : [Select Id, Academic_Year__c, Academic_Year__r.Name, Student_Assets_Variable__c, No_Income_Tax_Variable__c, Low_Dividends_Interest_Variable_2__c, Low_Dividends_Interest_Variable_1__c, Large_Income_Change_Variable__c, Income_Tax_Low_Variable__c,
                                            Income_Tax_High_Variable__c, Home_Equity_Capped_Variable__c, High_Unusual_Expenses_Variable__c, High_Medical_Expense_Variable__c, High_Income_Adjustments_Variable__c, High_Dividends_Interest_Variable_2__c, High_Dividends_Interest_Variable_1__c
                                            FROM Unusual_Conditions__c where Academic_Year__r.Name in :acadYearNames]){
            
            acadYearNameToUnusualConditions.put(uc.Academic_Year__r.Name, uc);                    
        }
        
        // loop through each PFS and calculate the UCs, using the appropriate AY
        for (PFS__c pfs : pfsList){    
            
            Unusual_Conditions__c uc = acadYearNameToUnusualConditions.get(pfs.Academic_Year_Picklist__c);
            
            if (uc != null){
                pfs.High_Medical_Expense__c = calcHighMedExpense(pfs, uc);        
                pfs.High_Unusual_Expenses__c = calcHighUnusualExpense(pfs, uc);
                pfs.High_Debt__c = calcHighDebt(pfs, uc);
                pfs.Home_Equity_Capped__c = calcHomeEquityCapped(pfs, uc);
                pfs.High_Student_Assets__c = calcHighStudentAssets(pfs, uc);
                pfs.Other_Taxable_Income_Negative__c = calcOtherTaxIncomeNegative(pfs, uc);
                pfs.Business_Not_Indicated__c = calcBusinessNotIndicated(pfs, uc);
                pfs.Multiple_Tuitions__c = calcMultipleTuitions(pfs, uc);
                pfs.Income_Zero__c = calcIncomeZero(pfs, uc);
                pfs.Large_Income_Change__c = calcLargeIncomeChange(pfs, uc);
                pfs.High_Income_Adjustments__c = calcHighIncomeAdjustments(pfs, uc);
                pfs.High_Dividends_Interest__c = calcHighDividendsInterest(pfs, uc);
                pfs.Low_Dividends_Interest__c = calcLowDividendsInterest(pfs, uc);
                pfs.No_Income_Tax__c = calcNoIncomeTax(pfs, uc);
                pfs.Income_Tax_Low__c = calcLowIncomeTax(pfs, uc);
                pfs.Income_Tax_High__c = calcHighIncomeTax(pfs, uc);
            }
        }
        
        if (save){
            update pfsList;
        }
    }
   
    /*End Action Methods*/
   
    /*Helper Methods*/
   
    // Multiply C*AO Total Income by .05.  If the result is less than the Medical Expense figure (PFS Section E, 22), 
    // set this condition code.
    public static String calcHighMedExpense(PFS__c pfs, Unusual_Conditions__c uc){
        Double variable = uc.High_Medical_Expense_Variable__c; //0.05;

        String s = 'No';
    
        try {
            if (pfs.Total_Income_Current__c != null && pfs.Medical_Dental_Exp_Current__c != null){
                if ((pfs.Total_Income_Current__c * (variable / 100)) < pfs.Medical_Dental_Exp_Current__c){
                    s = 'Yes';
                }
            }
        } catch (Exception e) {
            System.debug('ERROR High Medical Expense ' + e);
            s = null;
        }
        
        return s;
    }    

    //If C*AO Total Taxable Income is not zero, divide  Unusual Expenses  from PFSO Section E, 24  by Total Income.  
    // If the result is greater than .15, set this condition code.
    public static String calcHighUnusualExpense(PFS__c pfs, Unusual_Conditions__c uc){
        Double variable = uc.High_Unusual_Expenses_Variable__c; //0.15
        
        String s = 'No';
        try {
            if (pfs.Total_Taxable_Income_Current__c != null && pfs.Total_Taxable_Income_Current__c != 0){
                Double num = pfs.Unusual_Expenses_Current__c != null ? pfs.Unusual_Expenses_Current__c : 0;
                Double denom = pfs.Total_Income_Current__c != null ? pfs.Total_Income_Current__c : 0;
                
                Double quotient = denom == 0 ? 0 : num / denom;
                
                if (quotient > (variable / 100)){
                    s = 'Yes';
                }
            }
        } catch (Exception e) {
            System.debug('ERROR High Unusual Expense ' + e);
            s = null;
        }
        
        return s;
    } 
    
    //If C*AO Home Equity is greater than zero calculate the Equity Cap by mulitplying C*AO Total Income times 3.
    //If the result is less than the Home Equity amount, set this condition code.  
    public static String calcHomeEquityCapped(PFS__c pfs, Unusual_Conditions__c uc){
        System.debug('TESTING uc.Home_Equity_Capped_Variable__c ' + uc.Home_Equity_Capped_Variable__c);
        System.debug('TESTING uc.Total_Income_Current__c ' + pfs.Total_Income_Current__c);
        System.debug('TESTING uc.Home_Equity_Uncapped__c ' + pfs.Home_Equity_Uncapped__c);
        Double variable = uc.Home_Equity_Capped_Variable__c;  //3
        
        String s = 'No';
        try {
            if (pfs.Home_Equity_Uncapped__c != null && pfs.Home_Equity_Uncapped__c > 0){
                Double calculatedHomeCap = pfs.Total_Income_Current__c == null ? 0 : pfs.Total_Income_Current__c * variable;
                if (calculatedHomeCap < pfs.Home_Equity_Uncapped__c){
                    s = 'Yes';
                }
            }
        } catch (Exception e) {
            System.debug('ERROR Equity Capped ' + e);
            s = null;
        }

        return s;
    }
    
    //Subtract Debts from PFSO Section D, 17 from Total Assets.  If the result is less than zero, set this condition code.
    public static String calcHighDebt(PFS__c pfs, Unusual_Conditions__c uc){
        Double variable = 0; //No variable for this one
        
        String s = 'No';
        try {
            
            Double debts = pfs.Total_Debts__c == null ? 0 : pfs.Total_Debts__c;
            Double assets = pfs.Total_Assets_Current__c == null ? 0 : pfs.Total_Assets_Current__c;
            
            if ((assets - debts) < variable){
                    s = 'Yes';
            }
        } catch (Exception e) {
            System.debug('ERROR High Debt ' + e);
            s = null;
        }
        
        return s;
    } 
    
    //At least one applicant has Student Assets (PFSO Section C, 9C) greater than $2,000.
    public static String calcHighStudentAssets(PFS__c pfs, Unusual_Conditions__c uc){
        Double variable = uc.Student_Assets_Variable__c; //2000;
        
        String s = 'No';
        try {
            for (Applicant__c a : pfs.Applicants__r){
                if (a.Assets_Total_Value__c > variable){
                        s = 'Yes';
                }
            }
        } catch (Exception e) {
            System.debug('ERROR High Student Assets ' + e);
            s = null;
        }
        
        return s;
    } 

    //If Other Taxable Income is negative, set this condition code.
    public static String calcOtherTaxIncomeNegative(PFS__c pfs, Unusual_Conditions__c uc){
        Double variable = 0; //no variable;
        
        System.debug('TESTING Other_Taxable_Income_Current__c ' + pfs.Other_Taxable_Income_Current__c);
        System.debug('TESTING variable ' + variable);
        
        String s = 'No';
        try {
            if (pfs.Other_Taxable_Income_Current__c != null && pfs.Other_Taxable_Income_Current__c < variable){
                s = 'Yes';
            }
        } catch (Exception e) {
            System.debug('ERROR Other Tax Negative ' + e);
            s = null;
        }
        
        return s;
    } 

    // If the verification amount for Business/Farm Profit/Loss is not null or zero, 
    // AND the Business/Farm Indicator is null, set this condition code.
    public static String calcBusinessNotIndicated(PFS__c pfs, Unusual_Conditions__c uc){
        //Double calcBusinessNotIndicatedVariable = 0; //no variable;
        
        String s = 'No';
        try {
            if (pfs.Own_Business_or_Farm__c == null || pfs.Own_Business_or_Farm__c == 'No'){
                
                if ((pfs.Verification__r.X1040_Business_Income_Loss__c != null && pfs.Verification__r.X1040_Business_Income_Loss__c != 0) 
                    || 
                    (pfs.Verification__r.X1040_Business_Income_Loss_B__c != null && pfs.Verification__r.X1040_Business_Income_Loss_B__c != 0)){
                    s = 'Yes';
                }
            }
        } catch (Exception e) {
            System.debug('ERROR BusinessNotIndicated ' + e);
            s = null;
        }
        
        return s;
    } 

    // If the Number of Children in Tuition Schools (PFSO Section E,  19A) is greater than one, set this condition code.
    public static String calcMultipleTuitions(PFS__c pfs, Unusual_Conditions__c uc){
        Double variable = 1; //no variable;
        
        String s = 'No';
        try {
            if (pfs.Num_Children_in_Tuition_Schools__c != null && pfs.Num_Children_in_Tuition_Schools__c > variable){
                s = 'Yes';
            }
        } catch (Exception e) {
            System.debug('ERROR Mult Tuitions Assets ' + e);
            s = null;
        }
        
        return s;
    } 

    // C*AO Total Taxable Income (summed from all PFS taxable income amounts) = 0
    public static String calcIncomeZero(PFS__c pfs, Unusual_Conditions__c uc){
        Double variable = 0; //no variable;
        
        String s = 'No';        
        try {
            if (pfs.Total_Taxable_Income_Current__c != null && pfs.Total_Taxable_Income_Current__c == variable){
                s = 'Yes';
            }
        } catch (Exception e) {
            System.debug('ERROR income zero ' + e);
            s = null;
        }
        
        return s;
    } 

    // Using values from PFS Section C, 7A, divide projected year Salary/Wages Parent A by current year Salary/Wages Parent A.  
    //If result is greater than 1.15 or less than .85, set condition.  
    // Repeat for Salary/Wages Parent B, if present using PFS Section C, 7B.
    // If either calculation exceeds the range, set the condition code.  
    public static String calcLargeIncomeChange(PFS__c pfs, Unusual_Conditions__c uc){
        Double variable = uc.Large_Income_Change_Variable__c; //15;
        
        String s = 'No';        
        try {
            if (pfs.Salary_Wages_Parent_A__c != null && pfs.Salary_Wages_Parent_A_Est__c != null){
                Double percentChange = Math.abs(100 * ((pfs.Salary_Wages_Parent_A_Est__c / pfs.Salary_Wages_Parent_A__c) - 1));                
                if (percentChange > variable){
                    s = 'Yes';
                }
            }
            
            if (pfs.Salary_Wages_Parent_B__c != null && pfs.Salary_Wages_Parent_B_Est__c != null){
                Double percentChange = Math.abs(100 * ((pfs.Salary_Wages_Parent_B_Est__c / pfs.Salary_Wages_Parent_B__c) - 1));
                if (percentChange > variable){
                    s = 'Yes';
                }
            }
            
        } catch (Exception e) {
            System.debug('ERROR large income ' + e);
            s = null;
        }
        
        return s;
    } 
    
    // If C*AO Total Taxable Income is not zero, divide  Adjustments to Income  by Total Taxable Income.  
    // If the result is greater than .25, set this condition code.  
    // [Since a negative number increases income, only a high positive amount is considered here.]    
    public static String calcHighIncomeAdjustments(PFS__c pfs, Unusual_Conditions__c uc){
        Double variable = uc.High_Income_Adjustments_Variable__c; //25;
        
        String s = 'No';        
        try {
            if (pfs.Total_Taxable_Income_Current__c != null && pfs.Total_Taxable_Income_Current__c != 0 && pfs.IRS_Adjustments_to_Income_Current__c != null){
                
                Double percentChange = 100 * ((pfs.IRS_Adjustments_to_Income_Current__c / pfs.Total_Taxable_Income_Current__c));
                
                System.debug('TESTING262 ' + pfs.IRS_Adjustments_to_Income_Current__c + ';' + pfs.Total_Taxable_Income_Current__c);
                System.debug('TESTING263 ' + percentChange + ';' + variable);
                
                if (percentChange > variable){
                    s = 'Yes';
                }
                
                // if total taxable income is negative and IRS Adjustments is positive, also set condition
                if (pfs.Total_Taxable_Income_Current__c < 0 && pfs.IRS_Adjustments_to_Income_Current__c > 0){
                    s = 'Yes';
                }            
            }
        } catch (Exception e) {
            System.debug('ERROR high income adjustments ' + e);
            s = null;
        }
        
        return s;
    } 
    
    //If the PFSO Section C, 7C Dividend/Interest Income amount is greater than $250, multiply that amount by 50.  
    // If the result is greater than the sum of Bank Accounts and Other Investments, set this condtion code.
    public static String calcHighDividendsInterest(PFS__c pfs, Unusual_Conditions__c uc){
        //Double variable1 = uc.Low_Dividends_Interest_Variable_1__c; //5000;
        //Double variable2 = uc.Low_Dividends_Interest_Variable_2__c; //2%;
        Double variable3 = uc.High_Dividends_Interest_Variable_1__c; //250;
        Double variable4 = uc.High_Dividends_Interest_Variable_2__c; //50
        
        String s = 'No';        
        try {
            Double acctsAndInvests = 0;
            acctsAndInvests = pfs.Bank_Account_Value__c == null ? acctsAndInvests : acctsAndInvests + pfs.Bank_Account_Value__c;
            acctsAndInvests = pfs.Investments_Net_Value__c == null ? acctsAndInvests : acctsAndInvests + pfs.Investments_Net_Value__c;
            
            Double dividendIncome = 0;
            dividendIncome = pfs.Dividend_Income_Current__c == null ? dividendIncome : dividendIncome + pfs.Dividend_Income_Current__c;
            dividendIncome = pfs.Interest_Income_Current__c == null ? dividendIncome : dividendIncome + pfs.Interest_Income_Current__c;
            
            if (dividendIncome > variable3){
                // USING 2% number instead of 50% number
                //Double inversed = dividendIncome * variable4;
                Double inversed = dividendIncome * (100 / variable4);
                System.debug('TESTING307 ' + inversed + '; ' + acctsAndInvests);
                if (inversed > acctsAndInvests){
                    s = 'Yes';
                }
            }
        } catch (Exception e) {
            System.debug('ERROR high dividends Assets ' + e);
            s = null;
        }
        
        return s;
    } 

    //If the total of Bank Accounts(PFSO Section D, 14) and Other Investments (PFSO Section D, 15) is greater than $5000, 
    // multiply that total by .02.  If the result is larger than Dividend/Interest Income, set this condition code.
    public static String calcLowDividendsInterest(PFS__c pfs, Unusual_Conditions__c uc){
        Double variable1 = uc.Low_Dividends_Interest_Variable_1__c; //5000;
        Double variable2 = uc.Low_Dividends_Interest_Variable_2__c; //2%;
        //Double variable3 = uc.High_Dividends_Interest_Variable_1__c; //250;
        //Double variable4 = uc.High_Dividends_Interest_Variable_2__c; //2%;
        
        String s = 'No';        
        try {
            Double acctsAndInvests = 0;
            acctsAndInvests = pfs.Bank_Account_Value__c == null ? acctsAndInvests : acctsAndInvests + pfs.Bank_Account_Value__c;
            acctsAndInvests = pfs.Investments_Net_Value__c == null ? acctsAndInvests : acctsAndInvests + pfs.Investments_Net_Value__c;
            
            Double dividendIncome = 0;
            dividendIncome = pfs.Dividend_Income_Current__c == null ? dividendIncome : dividendIncome + pfs.Dividend_Income_Current__c;
            dividendIncome = pfs.Interest_Income_Current__c == null ? dividendIncome : dividendIncome + pfs.Interest_Income_Current__c;

            if (acctsAndInvests > variable1){
                Double percentage = acctsAndInvests * (variable2 / 100);
                if (percentage > dividendIncome){
                    s = 'Yes';
                }
            }
        } catch (Exception e) {
            System.debug('ERROR low dividends Assets ' + e);
            s = null;
        }
        
        return s;
    } 

    // Federal Income Tax Reported (PFS C. Family Income, 6E "what did you pay in total for Federal Taxes?") = 0, 
    // and C*AO Total Taxable Income is greater than $20,000.
    public static String calcNoIncomeTax(PFS__c pfs, Unusual_Conditions__c uc){
        Double variable = uc.No_Income_Tax_Variable__c; //20000;
        
        String s = 'No';        
        try {
            if (pfs.Federal_Income_Tax_Reported__c == null || pfs.Federal_Income_Tax_Reported__c == 0){
                if (pfs.Total_Taxable_Income_Current__c != null && pfs.Total_Taxable_Income_Current__c > variable){
                    s = 'Yes';
                }
            }
        } catch (Exception e) {
            System.debug('ERROR no income ' + e);
            s = null;
        }
        
        return s;
    } 

    // C*AO Federal Income Tax (Calculated) times .8.
    // Compare results to Federal Income Tax Reported (PFS C. Family Income, 6E "what did you pay in total for Federal Taxes?")
    // If calculated amount greater than the PFS figure, set this condition code.
    public static String calcLowIncomeTax(PFS__c pfs, Unusual_Conditions__c uc){
        Double variable = uc.Income_Tax_Low_Variable__c; //80;
        
        String s = 'No';        
        try {
            if (pfs.Federal_Income_Tax_Reported__c != null && pfs.Federal_Income_Tax_Calculated__c != null){
                if (pfs.Federal_Income_Tax_Calculated__c * (variable / 100) > pfs.Federal_Income_Tax_Reported__c){                    
                    s = 'Yes';
                }
            }
        } catch (Exception e) {
            System.debug('ERROR low income ' + e);
            s = null;
        }
        
        return s;
    } 

    // C*AO Federal Income Tax (Calculated) times 1 + .20.
    // Compare results to C*AO Federal Income Tax (PFS C. Family Income, 6E "what did you pay in total for Federal Taxes?")
    // If the PFS figure is greater than the calculated amount , set this condition code.
    public static String calcHighIncomeTax(PFS__c pfs, Unusual_Conditions__c uc){
        System.debug('TESTING500 ' + debugI++);
        Double variable = uc.Income_Tax_High_Variable__c; //20;
        
        String s = 'No';        
        try {
            if (pfs.Federal_Income_Tax_Reported__c != null && pfs.Federal_Income_Tax_Calculated__c != null){
                if (pfs.Federal_Income_Tax_Calculated__c * (1 + (variable / 100)) < pfs.Federal_Income_Tax_Reported__c){                    
                    s = 'Yes';
                }
            }
            System.debug('TESTING513 ' + s);
        } catch (Exception e) {
            System.debug('ERROR no income ' + e);
            s = null;
        }
        
        return s;
    } 

    /*End Helper Methods*/


}