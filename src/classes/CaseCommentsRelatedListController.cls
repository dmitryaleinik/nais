public without sharing class CaseCommentsRelatedListController {
    
    public Id caseId {get; set;}
    
    public Id pfsId {get; set;}
    
    public Id academicYearId {get; set;}
    
    public boolean isSchoolPortal {get;set;} //NAIS-2472
    
    public Boolean canCreateComment {get; set;}

    // NAIS-2489 Add comment support to parent messages
    public Boolean isSupport {get; set;}
    
    private transient List<cComment> cComments_x = null;
    public  List<cComment> comments{
        get{
            if(this.cComments_x == null) {
                this.cComments_x = new List<cComment>();
                for(CaseComment comment : [Select
                                                LastModifiedDate,
                                                LastModifiedBy.Id,
                                                LastModifiedBy.Name,
                                                IsPublished,
                                                CreatedDate,
                                                CreatedBy.Id,
                                                CreatedBy.Name,
                                                CommentBody
                                            FROM
                                                CaseComment c
                                            WHERE
                                                ParentId = :caseId
                                                AND IsPublished = true
                                            ORDER BY
                                                c.LastModifiedDate DESC])
                {
                    cComment c = new cComment();
                    c.comment = comment;
                    
                    // Build String to display.
                    c.commentText = 'Created By: ' + comment.CreatedBy.Name + ' (' + comment.CreatedDate.format() + ') ';
                    c.commentText += comment.CommentBody;
                    
                    //Add to list
                    this.cComments_x.add(c); 
                }
            }
            return this.cComments_x;
        }
    }
        
    public PageReference NewComment()
    {
        //NAIS-2472
        string returnUrl='';
        if(isSchoolPortal !=null && isSchoolPortal==true)
        {
            PageReference schoolMessage= Page.SchoolMessage;
            schoolMessage.getParameters().put('id', this.caseId);
            schoolMessage.getParameters().put('mode', '1');
            // NAIS-2489 only redirect to 'isSupport' if commenting on a support case [jB]
            if(isSupport){
                schoolMessage.getParameters().put('isSupport', '1');
            }
            returnUrl =  EncodingUtil.urlEncode(schoolMessage.getUrl(), 'UTF-8');
        }
        else //End NAIS-2472
        {
            PageReference messagesEdit = Page.FamilyMessagesEdit;
            messagesEdit.getParameters().put('id', this.caseId);
            messagesEdit.getParameters().put('academicYearId', this.academicYearId);
            messagesEdit.getParameters().put('pfsId', this.pfsId);
            
            returnUrl= EncodingUtil.urlEncode(messagesEdit.getUrl(), 'UTF-8');
        }
        
        PageReference pr = new PageReference('/00a/e?parent_id='+ caseId + '&retURL=' +returnUrl ); //NAIS-2472
        pr.setRedirect(true);
        return pr;
    }
    
    /*
    public PageReference deleteComment() {
        Id commentId = ApexPages.currentPage().getParameters().get('CommentId_d');
        
        for(cComment comment : this.comments) {
            
            if(comment.comment.Id == commentId) {
                Database.delete(comment.comment);
                break;
            }
        }
        
        PageReference pg = new PageReference('/?id=' + caseId);
        pg.setRedirect(true);
        return pg;
    }
    */
    
    public class cComment {
    
        public CaseComment comment {get; set;}
        public String commentText {get; set;}
    }
}