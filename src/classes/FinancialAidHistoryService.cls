/**
 * @description Created during Idea day for IDEA-3. This class will get student folders and SPAs so that we can plot various historical data.
 */
public class FinancialAidHistoryService {

    private FinancialAidHistoryService() { }

    /**
     * @description Simply returns a sample set of history without querying any real data.
     */
    public History getSampleHistory() {
        History sampleHistory = new History();

        // Family Contribution sample data.
        sampleHistory.addSummary(new FamilyContributionSummary(10000, 8000, 7500, '2013-2014'));
        sampleHistory.addSummary(new FamilyContributionSummary(10750, 8500, 6500, '2014-2015'));
        sampleHistory.addSummary(new FamilyContributionSummary(11000, 7750, 8500, '2015-2016'));
        sampleHistory.addSummary(new FamilyContributionSummary(11500, 9750, 10000, '2016-2017'));
        sampleHistory.addSummary(new FamilyContributionSummary(12050, 11000, 9750, '2017-2018'));

        // Verification Summary sample data.
        sampleHistory.VerificationData.add(new VerificationSummary(57500, 65000, '2013-2014'));
        sampleHistory.VerificationData.add(new VerificationSummary(58000, 67500, '2014-2015'));
        sampleHistory.VerificationData.add(new VerificationSummary(60000, 68500, '2015-2016'));
        sampleHistory.VerificationData.add(new VerificationSummary(62000, 70000, '2016-2017'));
        sampleHistory.VerificationData.add(new VerificationSummary(65000, 75000, '2017-2018'));

        // Financial Aid sample data.
        FinancialAidSummary summary1 = new FinancialAidSummary(15000, 1000, 20000, 15000, 850, 4000, '2013-2014');
        summary1.TravelExpenses = 1000;
        summary1.MiscExpenses = 1000;
        summary1.StudentExpenses = 2000;
        sampleHistory.addSummary(summary1);

        FinancialAidSummary summary2 = new FinancialAidSummary(17000, 1000, 23000, 15500, 1000, 7000, '2014-2015');
        summary2.TravelExpenses = 1500;
        summary2.MiscExpenses = 1500;
        summary2.StudentExpenses = 2000;
        sampleHistory.addSummary(summary2);

        FinancialAidSummary summary3 = new FinancialAidSummary(19000, 1000, 25500, 16750, 1100, 8250, '2015-2016');
        summary3.TravelExpenses = 1500;
        summary3.MiscExpenses = 1500;
        summary3.StudentExpenses = 2500;
        sampleHistory.addSummary(summary3);

        FinancialAidSummary summary4 = new FinancialAidSummary(21000, 1500, 27500, 16500, 1050, 9250, '2016-2017');
        summary4.TravelExpenses = 1500;
        summary4.MiscExpenses = 1500;
        summary4.StudentExpenses = 2000;
        sampleHistory.addSummary(summary4);

        FinancialAidSummary summary5 = new FinancialAidSummary(22000, 1000, 29000, 16000, 1100, 10000, '2017-2018');
        summary5.TravelExpenses = 2000;
        summary5.MiscExpenses = 1500;
        summary5.StudentExpenses = 2500;
        sampleHistory.addSummary(summary5);

        return sampleHistory;
    }

    public static FinancialAidHistoryService Instance {
        get {
            if (Instance == null) {
                Instance = new FinancialAidHistoryService();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description A summary of estimated family contributions for a single academic year.
     */
    public class FamilyContributionSummary implements Comparable {

        public FamilyContributionSummary() { }

        public FamilyContributionSummary(Decimal amountAwarded, Decimal efcAmount, Decimal offerFromParent, String academicTerm) {
            this.AidAwarded = amountAwarded;
            this.EstimatedFamilyContribution = efcAmount;
            this.ParentOffer = offerFromParent;
            this.Term = academicTerm;
        }

        /**
         * @description The amount of financial aid awarded to the applicant.
         */
        public Decimal AidAwarded { get; set; }

        /**
         * @description The amount that the parent should pay based on our calculations.
         */
        public Decimal EstimatedFamilyContribution { get; set; }

        /**
         * @description The amount that the parent offered to pay.
         */
        public Decimal ParentOffer { get; set; }

        /**
         * @description The academic term that this data represents. (e.g. 2016-2017, 2017-2018)
         */
        public String Term { get; set; }

        public Integer compareTo(Object objToCompare) {
            FamilyContributionSummary summaryToCompare = (FamilyContributionSummary)objToCompare;
            Integer thisYear = getAcademicTermStartYear(this.Term);
            Integer compareToYear = getAcademicTermStartYear(summaryToCompare.Term);

            if (thisYear == compareToYear) return 0;
            if (thisYear > compareToYear) return 1;
            return -1;
        }

        private Integer getAcademicTermStartYear(String academicTerm) {
            return Integer.valueOf(academicTerm.left(4));
        }
    }

    /**
     * @description A summary of reported income/asset and verification values.
     */
    public class VerificationSummary implements Comparable {

        public VerificationSummary(Decimal reportedIncomeVal, Decimal verifiedIncomeVal, String academicTerm) {
            this.ReportedIncome = reportedIncomeVal;
            this.VerifiedIncome = verifiedIncomeVal;
            this.Term = academicTerm;
        }

        /**
         * @description The income reported by a family.
         */
        public Decimal ReportedIncome { get; set; }

        /**
         * @description The income as verified by tax documents.
         */
        public Decimal VerifiedIncome { get; set; }

        /**
         * @description The academic term which this data is for. (e.g. 2016-2017, 2017-2018)
         */
        public String Term { get; set; }

        public Integer compareTo(Object objToCompare) {
            VerificationSummary summaryToCompare = (VerificationSummary)objToCompare;
            Integer thisYear = getAcademicTermStartYear(this.Term);
            Integer compareToYear = getAcademicTermStartYear(summaryToCompare.Term);

            if (thisYear == compareToYear) return 0;
            if (thisYear > compareToYear) return 1;
            return -1;
        }

        private Integer getAcademicTermStartYear(String academicTerm) {
            return Integer.valueOf(academicTerm.left(4));
        }
    }

    /**
     * @description A summary of the financial aid a family received and the costs for attending school.
     */
    public class FinancialAidSummary implements Comparable {

        public FinancialAidSummary(Decimal tuitionCost, Decimal fees, Decimal need, Decimal aidGrant, Decimal tuitionRemission, Decimal otherAidFunds, String academicTerm) {
            this.Tuition = tuitionCost;
            this.StudentFees = fees;
            this.FinancialNeed = need;
            this.GrantAwarded = aidGrant;
            this.Remission = tuitionRemission;
            this.OtherAid = otherAidFunds;
            this.TotalAid = aidGrant + tuitionRemission + otherAidFunds;
            this.Term = academicTerm;
        }

        public Decimal FinancialNeed { get; set; }

        public Decimal Tuition { get; set; }

        public Decimal StudentFees { get; set; }

        public Decimal TravelExpenses { get; set; }

        public Decimal StudentExpenses { get; set; }

        public Decimal MiscExpenses { get; set; }

        /**
         * @description This should be a sum of grant awarded, remission, and other aid.
         */
        public Decimal TotalAid {
            get;
            set;
        }

        public Decimal GrantAwarded { get; set; }

        public Decimal Remission { get; set; }

        public Decimal OtherAid { get; set; }

        /**
         * @description The academic term which this data is for. (e.g. 2016-2017, 2017-2018)
         */
        public String Term { get; set; }

        public List<PieData> getNeedBreakdown() {
            List<PieData> needBreakdown = new List<FinancialAidHistoryService.PieData>();

            needBreakdown.add(new FinancialAidHistoryService.PieData('Tuition', this.Tuition));
            needBreakdown.add(new FinancialAidHistoryService.PieData('Student Fees', this.StudentFees));
            needBreakdown.add(new FinancialAidHistoryService.PieData('Student Expenses', this.StudentExpenses));
            needBreakdown.add(new FinancialAidHistoryService.PieData('Travel Expenses', this.TravelExpenses));
            needBreakdown.add(new FinancialAidHistoryService.PieData('Misc Expenses', this.MiscExpenses));

            return needBreakdown;
        }

        public List<PieData> getAidBreakdown() {
            List<PieData> aidBreakdown = new List<FinancialAidHistoryService.PieData>();

            aidBreakdown.add(new FinancialAidHistoryService.PieData('Grant Awarded', this.GrantAwarded));
            aidBreakdown.add(new FinancialAidHistoryService.PieData('Tuition Remission', this.Remission));
            aidBreakdown.add(new FinancialAidHistoryService.PieData('Other Aid', this.OtherAid));

            return aidBreakdown;
        }

        public Integer compareTo(Object objToCompare) {
            FinancialAidSummary summaryToCompare = (FinancialAidSummary)objToCompare;
            Integer thisYear = getAcademicTermStartYear(this.Term);
            Integer compareToYear = getAcademicTermStartYear(summaryToCompare.Term);

            if (thisYear == compareToYear) return 0;
            if (thisYear > compareToYear) return 1;
            return -1;
        }

        private Integer getAcademicTermStartYear(String academicTerm) {
            return Integer.valueOf(academicTerm.left(4));
        }
    }

    public class PieData {

        public PieData(String nameVal, Decimal dataValue) {
            this.Name = nameVal;
            this.Value = dataValue;
        }

        public String Name { get; set; }

        public Decimal Value { get; set; }
    }

    /**
     * @description A collection of financial aid summaries.
     */
    public class History {

        public List<FamilyContributionSummary> EfcData {
            get {
                if (EfcData == null) {
                    EfcData = new List<FamilyContributionSummary>();
                }
                return EfcData;
            }
            private set;
        }

        public List<VerificationSummary> VerificationData {
            get {
                if (VerificationData == null) {
                    VerificationData = new List<VerificationSummary>();
                }
                return VerificationData;
            }
            private set;
        }

        public List<FinancialAidSummary> FinancialAidData {
            get {
                if (FinancialAidData == null) {
                    FinancialAidData = new List<FinancialAidSummary>();
                }
                return FinancialAidData;
            }
            private set;
        }

        public void addSummary(Object newSummary) {

            if (newSummary instanceof FinancialAidHistoryService.FamilyContributionSummary) {
                this.EfcData.add((FamilyContributionSummary)newSummary);
            } else if (newSummary instanceof FinancialAidHistoryService.VerificationSummary) {
                this.VerificationData.add((VerificationSummary)newSummary);
            } else if (newSummary instanceof FinancialAidHistoryService.FinancialAidSummary) {
                this.FinancialAidData.add((FinancialAidSummary)newSummary);
            }
            sortData();
        }

        private void sortData() {
            this.EfcData.sort();
            this.VerificationData.sort();
            this.FinancialAidData.sort();
        }

    }
}