/**
* @ FamilyAppCompControllerTest.cls
* @date 6/28/2013
* @author  Charles Howard for Exponent Partners
* @description Unit test methods for FamilyAppCompController
*/
@isTest
private with sharing class FamilyAppCompControllerTest
{
    
    private static final String FAMILY_APPLICATION_MAIN_PAGE = 'FamilyApplicationMain';
    private static PFS__c pfsRecord;
    private static ApplicationUtils appUtils;
    private static String currentScreen;

    @isTest
    private static void testControllerProperties()
    {
        Business_Farm__c businessFarm = BusinessFarmTestData.Instance.DefaultBusinessFarm;
        User familyPortalUser = UserTestData.Instance.insertUser();
        Pfs__c pfs = PfsTestData.Instance.DefaultPfs;
        String parentDelete = 'testString';
        String taxableWarningMessage = 'testString';

        FamilyAppCompController controller = new FamilyAppCompController();

        Test.startTest();
            controller.bf = businessFarm;
            System.assertEquals(businessFarm.Id, controller.bf.Id);

            controller.loggedUser = familyPortalUser;
            System.assertEquals(familyPortalUser.Id, controller.loggedUser.Id);

            controller.pfs = pfs;
            System.assertEquals(pfs.Id, controller.pfs.Id);

            controller.parentDelete = parentDelete;
            System.assertEquals(parentDelete, controller.parentDelete);

            controller.taxableWarningMessage = taxableWarningMessage;
            System.assertEquals(taxableWarningMessage, controller.taxableWarningMessage);
            
            controller.showPopup = true;
            System.assertEquals(true, controller.showPopup);
        Test.stopTest();
    }

    @isTest
    private static void testGetCurrentAndPrevious_AcademicYear_Pfs()
    {
        Academic_Year__c currentYear = AcademicYearTestData.Instance.create();
        Academic_Year__c priorPFSYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentYear, priorPFSYear};
 
        Pfs__c pfsForCurrentYear = PfsTestData.Instance.forAcademicYearPicklist(currentYear.Name).create();
        Pfs__c pfsForPriorYear = PfsTestData.Instance.forAcademicYearPicklist(priorPFSYear.Name).create();
        insert new List<Pfs__c>{pfsForCurrentYear, pfsForPriorYear};

        FamilyAppCompController controller = new FamilyAppCompController();
        controller.pfs = pfsForCurrentYear;

        Test.startTest();
            System.assertEquals(currentYear.Id, controller.currentPFSYear.Id);
            System.assertEquals(priorPFSYear.Id, controller.previousPFSYear.Id);
            System.assertEquals(pfsForPriorYear.Id, controller.priorYearPFS.Id);
        Test.stopTest();
    }

    @isTest
    private static void testCurrent_Future_TaxYearLabel()
    {
        Academic_Year__c currentYear = AcademicYearTestData.Instance.create();
        Academic_Year__c priorPFSYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentYear, priorPFSYear};
 
        Pfs__c pfsWithEmptyYearField = PfsTestData.Instance.create();
        Pfs__c pfs = PfsTestData.Instance.forAcademicYearPicklist(currentYear.Name).create();
        insert new List<Pfs__c>{pfsWithEmptyYearField, pfs};

        FamilyAppCompController controller = new FamilyAppCompController();
        controller.pfs = pfsWithEmptyYearField;

        Test.startTest();
            System.assertEquals('', controller.currentTaxYearLabel);
            System.assertEquals('', controller.futureTaxYearLabel);

            controller.pfs = pfs;
            controller.currentTaxYearLabel = null;
            controller.futureTaxYearLabel = null;

            System.assertEquals(String.valueOf(Integer.valueOf(currentYear.Name.left(4)) - 1), controller.currentTaxYearLabel);
            System.assertEquals(currentYear.Name.left(4), controller.futureTaxYearLabel);
        Test.stopTest();
    }

    @isTest
    private static void testApplicantsPageUrl()
    {
        Pfs__c pfs = PfsTestData.Instance.DefaultPfs;

        FamilyAppCompController controller = new FamilyAppCompController();
        controller.pfs = pfs;

        Test.startTest();
            String urlString = controller.applicantsPageUrl;
            System.assert(urlString.containsIgnoreCase('/apex/' + FAMILY_APPLICATION_MAIN_PAGE)
                && urlString.containsIgnoreCase('screen=' + FamilyAppCompController.APPLICANT_INFORMATION_SCREEN) 
                && urlString.containsIgnoreCase('id=' + pfs.Id));
        Test.stopTest();
    }

    @isTest
    private static void testGetLanguages()
    {
        FamilyAppCompController controller = new FamilyAppCompController();

        Test.startTest();
            List<SelectOption> soList = controller.getLanguages();

            System.assertEquals(2, soList.size());
            for(SelectOption so : soList)
            {
                if (so.getValue() == 'en_US')
                {
                    System.assertEquals('English', so.getLabel());
                } else {
                    System.assertEquals('Spanish', so.getLabel());
                }
            }
        Test.stopTest();
    }

    @isTest
    private static void testNoWarningBasicTax()
    {
        FamilyAppCompController controller = new FamilyAppCompController();
        controller.showPopup = true;

        Test.startTest();
            System.assertEquals(null, controller.noWarningBasicTax());
            System.assertEquals(false, controller.showPopup);
        Test.stopTest();
    }

    @isTest
    private static void testGetPFSIncome()
    {
        Academic_Year__c currentYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        Pfs__c pfs = PfsTestData.Instance.forAcademicYearPicklist(currentYear.Name).DefaultPfs;

        FamilyAppCompController controller = new FamilyAppCompController();
        controller.pfs = pfs;

        Test.startTest();
            System.assertEquals(FamilyTotalIncomeCalculator.calculateTotalIncome(pfs.Id), controller.getPFSIncome());
        Test.stopTest();
    }

    @isTest
    private static void testGetReqs()
    {
        Integer testAmountValue = 1000;

        Pfs__c pfsWithEmptyReqFields = PfsTestData.Instance.create();
        Pfs__c pfs1 = PfsTestData.Instance
            .forMedicalDentalExpCurrent(testAmountValue)
            .forUnusualExpensesCurrent(testAmountValue)
            .forAnnualClubFees(testAmountValue)
            .forCampsLessonsAnnualCost(testAmountValue).create();
        Pfs__c pfs2 = PfsTestData.Instance
            .forMedicalDentalExpEst(testAmountValue)
            .forUnusualExpensesEst(testAmountValue).create();
        insert new List<Pfs__c>{pfsWithEmptyReqFields, pfs1, pfs2};

        FamilyAppCompController controller = new FamilyAppCompController();
        controller.pfs = pfsWithEmptyReqFields;

        Test.startTest();
            System.assert(!controller.getMedDentReq());
            System.assert(!controller.getUnusualReq());
            System.assert(!controller.getClubReq());
            System.assert(!controller.getLessCampReq());

            controller.pfs = pfs1;
            System.assert(controller.getMedDentReq());
            System.assert(controller.getUnusualReq());
            System.assert(controller.getClubReq());
            System.assert(controller.getLessCampReq());

            controller.pfs = pfs2;

            System.assert(controller.getMedDentReq());
            System.assert(controller.getUnusualReq());
        Test.stopTest();
    }

    @isTest
    private static void testGettingLists()
    {
        generateBasicPFSData();

        pfsRecord.Parent_A_First_Name__c = 'Test Parent';
        pfsRecord.Parent_B_First_Name__c = 'Test Parent B';
        pfsRecord.Addl_Parent_First_Name__c = 'Other Parent';
        pfsRecord.Number_of_Dependent_Children__c = 6;
        
        pfsRecord.Dependents__r[0].Dependent_1_Full_Name__c = 'Test Dep 1';
        pfsRecord.Dependents__r[0].Dependent_2_Full_Name__c = 'Test Dep 2';
        pfsRecord.Dependents__r[0].Dependent_3_Full_Name__c = 'Test Dep 3';
        pfsRecord.Dependents__r[0].Dependent_4_Full_Name__c = 'Test Dep 4';
        pfsRecord.Dependents__r[0].Dependent_5_Full_Name__c = 'Test Dep 5';
        pfsRecord.Dependents__r[0].Dependent_6_Full_Name__c = 'Test Dep 6';
        
        FamilyAppCompController compController = new FamilyAppCompController();
        compController.pfs = pfsRecord;
        compController.appUtils = appUtils;
        
        System.assertEquals(3, compController.parentsList.size());
        System.assertEquals(6, compController.dependentsList.size());
        System.assertEquals(null, compController.setParentValue());
    }
    
    @isTest
    private static void testDeleteParent()
    {
        generateBasicPFSData();
        
        pfsRecord.Parent_A_First_Name__c = 'Test Parent';
        pfsRecord.Parent_B_First_Name__c = 'Test Parent B';
        pfsRecord.Addl_Parent_First_Name__c = 'Other Parent';

        FamilyAppHouseSumCont compController = new FamilyAppHouseSumCont();
        compController.pfs = pfsRecord;
        compController.appUtils = appUtils;
        
        compController.parentDelete = 'Parent A';
        compController.deleteParent();
        pfsRecord = ApplicationUtils.queryPFSRecord(pfsRecord.Id);
        System.assertEquals(pfsRecord.Parent_A_First_Name__c, null);
        System.assertNotEquals(pfsRecord.Parent_B_First_Name__c, null);
        System.assertNotEquals(pfsRecord.Addl_Parent_First_Name__c, null);        
        
        compController.parentDelete = 'Parent B';
        compController.deleteParent();
        pfsRecord = ApplicationUtils.queryPFSRecord(pfsRecord.Id);
        System.assertEquals(pfsRecord.Parent_B_First_Name__c, null);
        System.assertNotEquals(pfsRecord.Addl_Parent_First_Name__c, null);
        
        compController.parentDelete = 'Other Parent';
        compController.deleteParent();        
        pfsRecord = ApplicationUtils.queryPFSRecord(pfsRecord.Id);
        System.assertEquals(pfsRecord.Addl_Parent_First_Name__c, null);
        
    }    
    
    @isTest
    private static void testNewDependent()
    {
        generateBasicPFSData();
        
        pfsRecord.Number_of_Dependent_Children__c = 4;
        
        pfsRecord.Dependents__r[0].Dependent_1_Full_Name__c = 'Test Dep 1';
        pfsRecord.Dependents__r[0].Dependent_2_Full_Name__c = 'Test Dep 2';
        pfsRecord.Dependents__r[0].Dependent_3_Full_Name__c = 'Test Dep 3';
        pfsRecord.Dependents__r[0].Dependent_4_Full_Name__c = 'Test Dep 4';
        
        FamilyAppHouseSumCont compController = new FamilyAppHouseSumCont();
        compController.pfs = pfsRecord;
        compController.appUtils = appUtils;
        
        compController.newDependent();
        
        pfsRecord = ApplicationUtils.queryPFSRecord(pfsRecord.Id);
        System.assertEquals(pfsRecord.Number_of_Dependent_Children__c, 5);
    } 
    
    @isTest
    private static void testTotalTaxable()
    {
        generateBasicPFSData();
        pfsRecord.Salary_Wages_Parent_A__c = 500;
        pfsRecord.Salary_Wages_Parent_A_Est__c = 5000;
        pfsRecord.Salary_Wages_Parent_B__c = 5000;
        pfsRecord.Salary_Wages_Parent_B_Est__c = 5000;
        pfsRecord.IRS_Adjustments_to_Income_Current__c = 1000;
        pfsRecord.IRS_Adjustments_to_Income_Est__c = 1000;
        pfsRecord.IRS_Adjustments_to_Income_Descrip__c = 'Test';
        pfsRecord.Interest_Income_Current__c = 100;
        pfsRecord.Dividend_Income_Current__c = 100;
        pfsRecord.Alimony_Current__c = 0;
        FamilyAppSettings__c appSettings = new FamilyAppSettings__c();
        appSettings.Name = 'TotalTaxable';
        appSettings.Status_Field__c = 'statTotalTaxable__c';
        FamilyAppSettings__c appSettings1 = new FamilyAppSettings__c();
        appSettings1.Name = 'HouseholdInformation';
        appSettings1.Status_Field__c = 'statTotalTaxable__c';
        insert new List<FamilyAppSettings__c>{appSettings, appSettings1};
        
        Family_Portal_Settings__c testFPSettings = new Family_Portal_Settings__c(Name = 'Family');
        testFPSettings.Adjusment_High_Percentage__c = 0.75;
        testFPSettings.Parent_A_Total_Salary__c = 600;
        testFPSettings.Parent_B_Total_Salary__c = 600;
        insert testFPSettings;
        
        FamilyAppCompController compController = new FamilyAppCompController();
        compController.pfs = pfsRecord;
        appUtils.currentScreen = 'TotalTaxable';
        compController.appUtils = appUtils;
        System.assertEquals(null,compController.showPopup);
        
        compController.saveAndExitTotalTaxable();
        System.assertEquals(true,compController.showPopup);
        
        pfsRecord.Salary_Wages_Parent_A__c = 5000;
        compController.pfs = pfsRecord;
        compController.saveAndExitTotalTaxable();
        System.assertEquals(false,compController.showPopup);
    }

    @isTest
    private static void testDeleteApplicantAndUpdateRelatedPFS()
    {
        Academic_Year__c currentYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        Pfs__c pfs = PfsTestData.Instance.asCompleted().DefaultPfs;
        Applicant__c applicant = ApplicantTestData.Instance.DefaultApplicant;
        School_PFS_Assignment__c spa = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(currentYear.Name).DefaultSchoolPfsAssignment;
        School_Document_Assignment__c sda = SchoolDocumentAssignmentTestData.Instance
            .forDocumentYear(currentYear.Name)
            .forPfsAssignment(spa.Id).DefaultSchoolDocumentAssignment;
        School_Biz_Farm_Assignment__c sbfa = SchoolBusinessFarmAssignmentTestData.Instance.DefaultSchoolBusinessFarmAssignment;

        FamilyAppCompController compController = new FamilyAppCompController();
        compController.pfs = pfs;

        Test.startTest();
            compController.deleteApplicantAndUpdateRelatedPFS(applicant.Id);
        Test.stopTest();

        System.assert(!pfs.statEducationalExpenses__c);
        System.assert(!pfs.statStudentIncome__c);
        System.assert(!pfs.statForKamehamehaApplicant__c);
        System.assert(!pfs.statForKamehamehaProgram__c);
        System.assert(!pfs.statSelectSchools__c);
        System.assert(!pfs.statHouseholdSummary__c);
        System.assert(!pfs.statApplicantInformation__c);

        System.assertEquals(0, ApplicantSelector.newInstance().selectAll().size());
        System.assertEquals(0, SchoolPfsAssignmentsSelector.newInstance().selectAll().size());
        System.assertEquals(0, SchoolDocumentAssignmentSelector.newInstance().selectAll().size());
        System.assertEquals(0, SchoolBizFarmAssignmentSelector.newInstance().selectAll().size());
    }

    @isTest
    private static void testShowPriorColumn()
    {
        Academic_Year__c currentYear = AcademicYearTestData.Instance.create();
        Academic_Year__c priorPFSYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentYear, priorPFSYear};
 
        Pfs__c pfsForCurrentYear = PfsTestData.Instance.forAcademicYearPicklist(currentYear.Name).DefaultPfs;

        FamilyAppCompController compController = new FamilyAppCompController();
        compController.pfs = pfsForCurrentYear;

        Test.startTest();
            System.assert(!compController.showPriorColumn);

            compController.showPriorColumn = null;
            Pfs__c pfsForPriorYear = PfsTestData.Instance.forAcademicYearPicklist(priorPFSYear.Name).insertPfs();
            System.assert(compController.showPriorColumn);
        Test.stopTest();
    }
    
    @isTest
    private static void testShowReceivedDate()
    {
        Contact parent = ContactTestData.Instance.asParent().insertContact();
        User familyPortalUser = UserTestData.Instance
            .forUsername(UserTestData.generateTestEmail())
            .forContactId(parent.Id)
            .forProfileId(GlobalVariables.familyPortalProfileId).create();
        User databankUser = UserTestData.Instance
            .forUsername(UserTestData.generateTestEmail())
            .forProfileId(GlobalVariables.dataBankProfileId).create();
        insert new List<User>{databankUser, familyPortalUser};

        Profile_Type_Grouping__c ptgDatabank = ProfileTypeGroupingTestData.Instance
            .asDatabankUser().DefaultProfileTypeGrouping;

        FamilyAppCompController compController;

        Test.startTest();
            System.runAs(familyPortalUser)
            {
                compController = new FamilyAppCompController();
                System.assert(!compController.showReceivedDate);
            }

            GlobalVariables.databankProfileIds = null;
            System.runAs(databankUser)
            {
                compController = new FamilyAppCompController();
                System.assert(compController.showReceivedDate);
            }
        Test.stopTest();
    }

    @isTest
    private static void testCancelTotalTaxable()
    {
        FamilyAppCompController compController = new FamilyAppCompController();

        Test.startTest();
            System.assert(compController.cancelTotalTaxable().getUrl().containsIgnoreCase(FAMILY_APPLICATION_MAIN_PAGE));
        Test.stopTest();
    }

    @isTest
    private static void testSaveAndContinueTotalTaxable_ExpectValue()
    {
        List<FamilyAppSettings__c> familyAppSettings = FamilyNavigationServiceTestHelper.insertFamilyAppSettings();
        pfsRecord = PfsTestData.Instance.DefaultPfs;

        appUtils = new ApplicationUtils(UserInfo.getLanguage(), pfsRecord);
        appUtils.currentScreen = FamilyNavigationServiceTestHelper.FAP_TOTAL_TAXABLE;
        
        FamilyAppCompController compController = new FamilyAppCompController();
        compController.pfs = pfsRecord;
        compController.appUtils = appUtils;
       
        Test.startTest();
            String referenceUrl = compController.saveAndContinueTotalTaxable().getUrl();
            System.assert(referenceUrl.containsIgnoreCase(FamilyNavigationServiceTestHelper.FAP_TOTAL_NON));
        Test.stopTest();
    }

    @isTest
    private static void testSaveAndContinueTotalTaxable_ExpectItemizeAdjustmentsError()
    {
        List<FamilyAppSettings__c> familyAppSettings = FamilyNavigationServiceTestHelper.insertFamilyAppSettings();

        Integer parentATotalSalary = 600;
        Integer parentBTotalSalary = 600;
        Decimal adjusmentHighPercentage = 5.00;
        Family_Portal_Settings__c familyPortalSettings = FamilyPortalSettingsTestData.Instance
            .forAdjusmentHighPercentage(adjusmentHighPercentage)
            .forParentATotalSalary(parentATotalSalary)
            .forParentBTotalSalary(parentBTotalSalary).DefaultFamilyPortalSettings;
            
        Integer salaryWagesParentA = 500;
        Integer salaryWagesParentB = 500;
        Integer otherIncomeCurrent = 100;
        Integer deductiblePartSETaxCurrent = 100;
        Integer iRSAdjustmentsToIncomeCurrent = 7000;
        PFS__c pfsRecord = PfsTestData.Instance
            .forIRSAdjustmentsToIncomeCurrent(iRSAdjustmentsToIncomeCurrent)
            .forDeductiblePartSETaxCurrent(deductiblePartSETaxCurrent)
            .forOtherIncomeCurrent(otherIncomeCurrent)
            .withOtherIncomeDescription('Description of other taxable income.')
            .forSalaryWagesParentA(salaryWagesParentA)
            .forSalaryWagesParentB(salaryWagesParentB).DefaultPfs;
        
        appUtils = new ApplicationUtils(UserInfo.getLanguage(), pfsRecord);
        appUtils.currentScreen = FamilyNavigationServiceTestHelper.FAP_TOTAL_TAXABLE;
        
        FamilyAppCompController compController = new FamilyAppCompController();
        compController.pfs = pfsRecord;
        compController.appUtils = appUtils;
       
        Test.startTest();
            System.assertEquals(null, compController.saveAndContinueTotalTaxable());
            List<Apexpages.Message> pageMessages = ApexPages.getMessages();
            System.assertEquals(1, pageMessages.size());
            System.assert(pageMessages[0].getDetail().contains(System.Label.Itemize_Adjustments_to_Income_Section),
                    'Expected page messages to contain errors about itemizing adjustments. Page messages: ' + pageMessages[0].getDetail());
        Test.stopTest();
    }

    @isTest
    private static void testSaveAndContinueTotalTaxable_ExpectEnterValueErrorAndNull()
    {
        List<FamilyAppSettings__c> familyAppSettings = FamilyNavigationServiceTestHelper.insertFamilyAppSettings();

        Integer parentATotalSalary = 600;
        Integer parentBTotalSalary = 600;
        Decimal adjusmentHighPercentage = 5.00;
        Family_Portal_Settings__c familyPortalSettings = FamilyPortalSettingsTestData.Instance
            .forAdjusmentHighPercentage(adjusmentHighPercentage)
            .forParentATotalSalary(parentATotalSalary)
            .forParentBTotalSalary(parentBTotalSalary).DefaultFamilyPortalSettings;
            
        Integer salaryWagesParentA = 500;
        Integer salaryWagesParentB = 500;
        Integer otherIncomeCurrent = 100;
        Integer deductiblePartSETaxCurrent = 100;
        Integer iRSAdjustmentsToIncomeCurrent = 7000;
        
        PFS__c pfsRecord = PfsTestData.Instance
            .forOtherIncomeCurrent(otherIncomeCurrent)
            .forSalaryWagesParentA(salaryWagesParentA)
            .forSalaryWagesParentB(salaryWagesParentB).create();
        pfsRecord.IRS_Adjustments_to_Income_Descrip__c = 'Test';
        insert new List<Pfs__c>{pfsRecord};
        
        appUtils = new ApplicationUtils(UserInfo.getLanguage(), pfsRecord);
        appUtils.currentScreen = FamilyNavigationServiceTestHelper.FAP_TOTAL_TAXABLE;
        
        FamilyAppCompController compController = new FamilyAppCompController();
        compController.pfs = pfsRecord;
        compController.appUtils = appUtils;
       
        Test.startTest();
            compController.formHiddenFields = new FamilyPortalHiddenFields();
            FamilyPortalHiddenFields.HiddenFieldWrapper wrapper = new FamilyPortalHiddenFields.HiddenFieldWrapper();
            wrapper.hiddenFields = 'Other_Taxable_Income_Descrip__c';
            compController.formHiddenFields.hiddenFieldsByScreen = new Map<String, FamilyPortalHiddenFields.HiddenFieldWrapper>{'TotalTaxable' => wrapper};
            System.assertEquals(null, compController.saveAndContinueTotalTaxable());
            List<Apexpages.Message> pageMessages = ApexPages.getMessages();
            System.assertEquals(1, pageMessages.size());
            System.assert(pageMessages[0].getDetail().contains(System.Label.You_must_enter_a_value));

            compController.formHiddenFields = null;
            System.assertEquals(null, compController.saveAndContinueTotalTaxable());
        Test.stopTest();
    }
    
    @isTest
    private static void testNoWarning()
    {
        FamilyAppCompController compController = new FamilyAppCompController();

        Test.startTest();
            System.assertEquals(null, compController.noWarning());
            System.assert(!compController.showPopup);
        Test.stopTest();
    }

    @isTest
    private static void testSaveAndExitBasicTax_ExpectValue()
    {
        List<FamilyAppSettings__c> familyAppSettings = FamilyNavigationServiceTestHelper.insertFamilyAppSettings();
        pfsRecord = PfsTestData.Instance
            .forOwnBusinessOrFarm('Yes').DefaultPfs;
        appUtils = new ApplicationUtils(UserInfo.getLanguage(), pfsRecord);
        appUtils.currentScreen = FamilyNavigationServiceTestHelper.FAP_BASIC_TAX;
        
        FamilyAppCompController compController = new FamilyAppCompController();
        compController.pfs = pfsRecord;
        compController.appUtils = appUtils;
       
        Test.startTest();
            String referenceUrl = compController.saveAndExitBasicTax().getUrl();
            System.assert(referenceUrl.containsIgnoreCase('FamilyDashboard'));
        Test.stopTest();
    }

    @isTest
    private static void testGetIsEZProcessActive()
    {
        FamilyAppCompController compController = new FamilyAppCompController();

        Test.startTest();
            System.assertEquals(PfsProcessService.Instance.isEZProcessActive, compController.getIsEZProcessActive());
        Test.stopTest();
    }

    @isTest
    private static void testSaveAndContinueBasicTax()
    {
       List<FamilyAppSettings__c> familyAppSettings = FamilyNavigationServiceTestHelper.insertFamilyAppSettings();
        pfsRecord = PfsTestData.Instance.DefaultPfs;
        appUtils = new ApplicationUtils(UserInfo.getLanguage(), pfsRecord);
        appUtils.currentScreen = FamilyNavigationServiceTestHelper.FAP_BASIC_TAX;
        
        FamilyAppCompController compController = new FamilyAppCompController();
        compController.pfs = pfsRecord;
        compController.appUtils = appUtils;
       
        Test.startTest();
            String referenceUrl = compController.saveAndContinueBasicTax().getUrl();
            System.assert(referenceUrl.containsIgnoreCase(FamilyNavigationServiceTestHelper.FAP_TOTAL_TAXABLE));
        Test.stopTest();
    }
    
    @isTest 
    private static void reCalculateSubmissionProcess_EZPFS_expectedFullPfs()
    {
        currentScreen = 'TotalTaxable';        
        generateBasicPFSData();
        
        List<FamilyAppSettings__c> familyAppSettings = new List<FamilyAppSettings__c>
        {
            FamilyAppSettingsTestData.Instance
                    .forName('TotalTaxable')
                    .forMainLabelField('FamilyIncome__c')
                    .forNextPage('TotalNon')
                    .forStatusField('statTotalTaxable__c')
                    .forSubLabelField('TotalTaxable__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset(),
            FamilyAppSettingsTestData.Instance
                    .forName('TotalNon')
                    .forMainLabelField('FamilyIncome__c')
                    .forNextPage('StudentIncome')
                    .forStatusField('statTotalNon__c')
                    .forSubLabelField('TotalNon__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset()
        };
        insert familyAppSettings;
        
        //0.- We enable EZ logic
        FeatureToggles.setToggle('PFS_EZ_Enabled__c', true);
        
        //1.- Define the Income Threshold        
        pfsRecord.Family_Size__c = 3;//Default Income Threshold is 60,000 for a family of 3.
        
        //2.- The Parent answer that he has an income lower than 60,000 
        pfsRecord.Household_Income_Under_Threshold__c = 'Yes';
        
        //3.- The current PFS is EZ
        pfsRecord.Submission_Process__c = 'EZ';
        
        //4.- We set the values that will provoke a totalTaxableIncome higher than 60,000
        pfsRecord.Salary_Wages_Parent_A__c = 15000000;
        pfsRecord.Salary_Wages_Parent_A_Est__c = 15000000;
        pfsRecord.Salary_Wages_Parent_B__c = 80000000;
        pfsRecord.Salary_Wages_Parent_B_Est__c = 80000000;
        
        pfsRecord.IRS_Adjustments_to_Income_Current__c = 1000;
        pfsRecord.IRS_Adjustments_to_Income_Est__c = 1000;
        pfsRecord.IRS_Adjustments_to_Income_Descrip__c = 'Test';
        pfsRecord.Interest_Income_Current__c = 100;
        pfsRecord.Dividend_Income_Current__c = 100;
        pfsRecord.Alimony_Current__c = 0;
        
        update pfsRecord;
        
        Test.startTest();
            FamilyAppCompController controller = new FamilyAppCompController();
            controller.pfs = pfsRecord;
            controller.appUtils = appUtils;
            
            system.assertEquals(true, controller.EZNeedsToBeUpdatedToFull(controller.pfs, true, 'TotalTaxable').hasChanged);
            controller.yesWarning();
        Test.stopTest();
        
        system.assertEquals('Full', [Select Id, Submission_Process__c FROM PFS__c WHERE Id=:pfsRecord.Id LIMIT 1].Submission_Process__c);
    }//End:reCalculateSubmissionProcess_EZPFS_expectedFullPfs
     
    @isTest
    private static void reCalculateSubmissionProcessSection6_EZPFS_expectedFullPfs()
    {
        currentScreen = 'BasicTax';        
        generateBasicPFSData();
        
        List<FamilyAppSettings__c> familyAppSettings = new List<FamilyAppSettings__c>
        {
            FamilyAppSettingsTestData.Instance
                    .forName('BasicTax')
                    .forMainLabelField('FamilyIncome__c')
                    .forNextPage('TotalTaxable')
                    .forStatusField('statBasicTax__c')
                    .forSubLabelField('FamilyIncome__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset(),
            FamilyAppSettingsTestData.Instance
                    .forName('TotalNon')
                    .forMainLabelField('FamilyIncome__c')
                    .forNextPage('StudentIncome')
                    .forStatusField('statTotalNon__c')
                    .forSubLabelField('TotalNon__c')
                    .forIsSpeedbumpPage(false)
                    .createFamilyAppSettingsWithoutReset()
        };

        insert familyAppSettings;
        
        //0.- We enable EZ logic
        FeatureToggles.setToggle('PFS_EZ_Enabled__c', true);
        
        //1.- Owns Business
        pfsRecord.Own_Business_or_Farm__c = 'Yes';
        
        //2.- Amount of Business
        pfsRecord.Number_of_Businesses__c = '1';
        
        //3.- The current PFS is EZ
        pfsRecord.Submission_Process__c = 'EZ';
        
        update pfsRecord;
        
        Test.startTest();
            FamilyAppCompController controller = new FamilyAppCompController();
            controller.pfs = pfsRecord;
            controller.appUtils = appUtils;
            
            system.assertEquals(true, controller.appUtils!=null);
            controller.saveAndContinueBasicTax();
            system.assertEquals(true, controller.EZNeedsToBeUpdatedToFull(controller.pfs, false, 'BasicTax').hasChanged);
            controller.yesWarningBasicTax();  
        Test.stopTest();
        
        system.assertEquals('Full', [Select Id, Submission_Process__c FROM PFS__c WHERE Id=:pfsRecord.Id LIMIT 1].Submission_Process__c);
    }//End:reCalculateSubmissionProcessSection6_EZPFS_expectedFullPfs

    @isTest
    private static void testDependentItem()
    {
        String userName = 'Bob Brown';
        FamilyAppCompController controller = new FamilyAppCompController();

        Test.startTest();
            controller.dependentItem = new Dependents__c(Dependent_1_Full_Name__c = userName);
        Test.stopTest();

        System.assertEquals(userName, controller.dependentItem.Dependent_1_Full_Name__c);
    }

    private static void generateBasicPFSData()
    {
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, true);
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, true);
        
        // Look up the current user record's related Contact record
        User userRecord = [select Id, Name, ContactId, Contact.LastName, Contact.Name, Contact.MailingStreet, Contact.MailingCity, Contact.MailingState,  
                        Contact.MailingPostalCode, Contact.MailingCountry, Contact.Gender__c, Contact.Birthdate, 
                        Contact.Email, Contact.HomePhone, Contact.MobilePhone, Contact.Preferred_Phone__c,
                        Contact.FirstName, Contact.Middle_Name__c, Contact.Suffix__c, Contact.Salutation, Contact.Phone,
                        LanguageLocaleKey
                        from User 
                        where Id = :UserInfo.getUserId()];
                        
        pfsRecord = ApplicationUtils.generateBlankPFS(academicYearId, userRecord);
        
        Id pfsId = [select Id from PFS__c limit 1].Id;
        pfsRecord = ApplicationUtils.queryPFSRecord(pfsId);
        
        if (currentScreen != null){
            appUtils = new ApplicationUtils(UserInfo.getLanguage(), pfsRecord, currentScreen);
        } else {
            appUtils = new ApplicationUtils(UserInfo.getLanguage(), pfsRecord);
        }
    }
}