/**
* Helper class for Tests developed for testing functionality 
* that clears payment fields of Chargent Order and Transaction records. 
*/
@isTest
public class ClearPaymentInfoTestHelper {
    
    /**
     * @description Creates and inserts particular number of test Chargent Order records
     *              with populated Payment fields.
     * @param numberOfOrders Number of Chargent Order records to insert.
     */
    @testVisible
    private List<ChargentOrders__ChargentOrder__c> createChargentOrderWithPaymentData(Integer numberOfOrders) {
        List<ChargentOrders__ChargentOrder__c> chargentOrders = new List<ChargentOrders__ChargentOrder__c>();
        
        for (Integer i=0; i<numberOfOrders; i++) {
            ChargentOrders__ChargentOrder__c singleOrder = ChargentOrderTestData.Instance.create();
            singleOrder.ChargentOrders__Card_Number__c = '123';
            singleOrder.ChargentOrders__Card_Last_4__c = '1234';
            singleOrder.ChargentOrders__Card_Expiration_Month__c = '10';
            singleOrder.ChargentOrders__Card_Expiration_Year__c = String.valueOf(Date.Today().Year() + 1);
            singleOrder.ChargentOrders__Card_Security_Code__c = '123';
            singleOrder.ChargentOrders__Card_Type__c = 'Visa';
            singleOrder.ChargentOrders__Bank_Name__c = 'test';
            singleOrder.ChargentOrders__Bank_Account_Name__c = 'test';
            singleOrder.ChargentOrders__Bank_Account_Number__c = '123';
            singleOrder.ChargentOrders__Bank_Routing_Number__c = '123';
            singleOrder.ChargentOrders__Bank_Account_Type__c = 'Savings';
            singleOrder.PaymentInfoIsCleared__c = false;

            chargentOrders.add(singleOrder);
        }
        insert chargentOrders;

        return chargentOrders;
    }

    /**
     * @description Creates and inserts test Transaction records with populated Payment fields
     *              for the list of Chargent Orders.
     * @param chargentOrders Chargent Order list to be used for transaction records creating.
     */
    @testVisible
    private List<ChargentOrders__Transaction__c> createTransactionsWithPaymentData(
        List<ChargentOrders__ChargentOrder__c> chargentOrders) {

        List<ChargentOrders__Transaction__c> transactions = new List<ChargentOrders__Transaction__c>();
        
        for (ChargentOrders__ChargentOrder__c chargentOrder : chargentOrders) {
            ChargentOrders__Transaction__c singleTransaction = 
                TransactionTestData.Instance.forChargentOrder(chargentOrder.Id).create();
            singleTransaction.ChargentOrders__Credit_Card_Name__c = 'test';
            singleTransaction.ChargentOrders__Credit_Card_Type__c = 'Visa';
            singleTransaction.ChargentOrders__Credit_Card_Number__c = '123';
            singleTransaction.ChargentOrders__Card_Last_4__c = '1234';
            singleTransaction.ChargentOrders__Bank_Account_Number__c = '123';
            singleTransaction.ChargentOrders__Bank_Account_Name__c = 'test';
            singleTransaction.ChargentOrders__Bank_Routing_Number__c = '123';
            singleTransaction.ChargentOrders__Bank_Account_Type__c = 'Savings';
            singleTransaction.ChargentOrders__Bank_Name__c = 'test';
            singleTransaction.PaymentInfoIsCleared__c = false;

            transactions.add(singleTransaction);
        }

        insert transactions;

        return transactions;
    }

    /**
     * @description Selects Transaction records by Ids from the parameter list.
     * @param transactionIds Transaction Ids for using in the query.
     */
    @testVisible
    private List<ChargentOrders__Transaction__c> getTransactionsByIds(Set<Id> transactionIds) {
        return [
            SELECT ChargentOrders__Credit_Card_Name__c, ChargentOrders__Credit_Card_Type__c, ChargentOrders__Credit_Card_Number__c,
                ChargentOrders__Card_Last_4__c, ChargentOrders__Bank_Account_Number__c, ChargentOrders__Bank_Account_Name__c,
                ChargentOrders__Bank_Routing_Number__c, ChargentOrders__Bank_Account_Type__c, ChargentOrders__Bank_Name__c, 
                PaymentInfoIsCleared__c
            FROM ChargentOrders__Transaction__c
            WHERE Id IN :transactionIds];
    }

    /**
     * @description Selects Chargent Order records by Ids from the parameter list.
     * @param chargentOrderIds Chargent Order Ids for using in the query.
     */
    @testVisible
    private List<ChargentOrders__ChargentOrder__c> getChargentOrdersByIds(Set<Id> chargentOrderIds) {
        return [
            SELECT ChargentOrders__Card_Number__c, ChargentOrders__Card_Last_4__c, ChargentOrders__Card_Expiration_Month__c,
                ChargentOrders__Card_Expiration_Year__c, ChargentOrders__Card_Security_Code__c, ChargentOrders__Card_Type__c,
                ChargentOrders__Bank_Name__c, ChargentOrders__Bank_Account_Name__c, ChargentOrders__Bank_Account_Number__c,
                ChargentOrders__Bank_Routing_Number__c, ChargentOrders__Bank_Account_Type__c, PaymentInfoIsCleared__c
            FROM ChargentOrders__ChargentOrder__c
            WHERE Id IN :chargentOrderIds];
    }

    /**
     * @description Validates if the clearing payment info functionality works correctly for Transactions.
     *              Expectation is that all payment fields are empty and PaymentInfoIsCleared__c is set to true.
     * @param transactionsToCheck List of transaction records to validate.
     */
    @testVisible
    private Boolean transactionsPaymentInfoIsCleared(List<ChargentOrders__Transaction__c> transactionsToCheck) {
        for (ChargentOrders__Transaction__c singleTransaction : transactionsToCheck) {
            System.assertEquals(null, singleTransaction.ChargentOrders__Credit_Card_Name__c);
            System.assertEquals(null, singleTransaction.ChargentOrders__Credit_Card_Type__c);
            System.assertEquals(null, singleTransaction.ChargentOrders__Credit_Card_Number__c);
            System.assertEquals(null, singleTransaction.ChargentOrders__Card_Last_4__c);
            System.assertEquals(null, singleTransaction.ChargentOrders__Bank_Account_Number__c);
            System.assertEquals(null, singleTransaction.ChargentOrders__Bank_Account_Name__c);
            System.assertEquals(null, singleTransaction.ChargentOrders__Bank_Routing_Number__c);
            System.assertEquals(null, singleTransaction.ChargentOrders__Bank_Account_Type__c);
            System.assertEquals(null, singleTransaction.ChargentOrders__Bank_Name__c);
            System.assertEquals(true, singleTransaction.PaymentInfoIsCleared__c);
        }

        return true;
    }

    /**
     * @description Validates if the clearing payment info functionality works correctly for Chargent Orders.
     *              Expectation is that all payment fields are empty and PaymentInfoIsCleared__c is set to true.
     * @param ordersToCheck List of chargent order records to validate.
     */
    @testVisible
    private Boolean ordersPaymentInfoIsCleared(List<ChargentOrders__ChargentOrder__c> ordersToCheck) {
        for (ChargentOrders__ChargentOrder__c singleOrder : ordersToCheck) {
            System.assertEquals(null, singleOrder.ChargentOrders__Card_Number__c);
            System.assertEquals(null, singleOrder.ChargentOrders__Card_Last_4__c);
            System.assertEquals(null, singleOrder.ChargentOrders__Card_Expiration_Month__c);
            System.assertEquals(null, singleOrder.ChargentOrders__Card_Expiration_Year__c);
            System.assertEquals(null, singleOrder.ChargentOrders__Card_Security_Code__c);
            System.assertEquals(null, singleOrder.ChargentOrders__Card_Type__c);
            System.assertEquals(null, singleOrder.ChargentOrders__Bank_Name__c);
            System.assertEquals(null, singleOrder.ChargentOrders__Bank_Account_Name__c);
            System.assertEquals(null, singleOrder.ChargentOrders__Bank_Account_Number__c);
            System.assertEquals(null, singleOrder.ChargentOrders__Bank_Routing_Number__c);
            System.assertEquals(null, singleOrder.ChargentOrders__Bank_Account_Type__c);
            System.assertEquals(true, singleOrder.PaymentInfoIsCleared__c);
        }

        return true;
    }

    /**
     * @description An instance property to store the singleton
     *              instance of the ClearPaymentInfoTestHelper.
     */
    public static ClearPaymentInfoTestHelper Instance {
        get {
            if (Instance == null) {
                Instance = new ClearPaymentInfoTestHelper();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to keep this from being
     *              instantiated externally.
     */
    private ClearPaymentInfoTestHelper() {}
}