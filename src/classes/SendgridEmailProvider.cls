/**
 * SendgridEmailProvider.cls
 *
 * @description: Handles sending emails to Sendgrid and Sendgrid specific API parsing
 *
 * @author: Chase Logan @ Presence PG
 */
public class SendgridEmailProvider implements IEmailProvider {

    /* class constants */
    private final String TYPE_NAME = 'SendgridEmailProvider';
    private final String ENCODING_TYPE = 'UTF-8';
    // API URL related
    private final String SG_CRED_BASE_PATH = 'callout:.__Sendgrid/';
    private final String SG_CRED_BASE_PATH_TEST = 'callout:.__Sendgrid_test/';
    private final String SG_MAIL_SEND_PATH = 'mail.send.json?';
    private final String SG_CREDS_BODY = 'api_user={!$Credential.Username}&api_key={!$Credential.Password}';
    // API params
    private final String SG_PARAM_SUBJECT = 'subject=';
    private final String SG_PARAM_TO_SINGLE = '&to=';
    private final String SG_PARAM_TO_MULTIPLE = '&to[]=';
    private final String SG_PARAM_TONAME = '&toname=';
    private final String SG_PARAM_HTML = '&html=';
    private final String SG_PARAM_FROM_SINGLE = '&from=';
    private final String SG_PARAM_FROM_MULTIPLE = '&from[]=';
    private final String SG_PARAM_SMTP_API = '&x-smtpapi=';
    // API params for unique args
    private final String SG_PARAM_UNIQ_ARGS = 'unique_args';
    private final String SG_PARAM_UNIQ_ARGS_SCHOOL_ID = 'school_id';
    private final String SG_PARAM_UNIQ_ARGS_SENDER_ID = 'sender_id';
    private final String SG_PARAM_UNIQ_ARGS_RECIP_ID = 'recipient_id';
    private final String SG_PARAM_UNIQ_ARGS_SPA_ID = 'spa_id';
    private final String SG_PARAM_UNIQ_ARGS_MASS_EMAIL_ID = 'mass_email_send_id';
    private final String SG_PARAM_UNIQ_ARGS_ACADEMIC_YEAR = 'academic_year';
    // API response keys
    private final String SG_RESP_MESSAGE = 'message';
    private final String SG_RESP_SUCCESS = 'success';
    private final String SG_RESP_ERROR = 'error';
    private final String SG_RESP_ERR_ARRAY = 'errors';

    @testVisible private static final String SOURCE_SEND_GRID = 'Send Grid';

    /* properties, lazy loaded where applicable */
    public Email singleEmailMessage { 
        get { 
            return ( this.singleEmailMessage == null ? this.singleEmailMessage = new Email() : this.singleEmailMessage);
        }
        set; 
     }
    public List<Email> emailMessageList { 
        get {
            return ( this.emailMessageList == null ? this.emailMessageList = new List<Email>() : this.emailMessageList);
        }
        set;
    }
    
    // Extend base exception for custom throws
    public class SendgridEmailProviderException extends Exception {}

    /**
     * @description: sends list of Emails to Sendgrid, exceptions are bubbled to caller for handling
     *
     * @param emails - List of Email objects
     *
     * @return List of EmailResult objects
     */
    public List<EmailResult> sendEmails( List<Email> emails) 
    {
        List<EmailResult> returnList = new List<EmailResult>();

        if ( emails != null && emails.size() > 0) 
        {
            Boolean isSandbox = [select IsSandbox from Organization].IsSandbox;
            // HttpService can be used for small list sends - proving API connectivity, NOT a batch safe implementation.
            for ( Email e : emails)
            {
                String httpEndpoint, httpResult, httpBody;

                try 
                {
                    httpEndpoint = ( isSandbox ? this.SG_CRED_BASE_PATH_TEST : this.SG_CRED_BASE_PATH) + this.SG_MAIL_SEND_PATH;

                    // SFP-1612: Create the body of the request with the named credentials and all info used to actually build and send the email.
                    // We used to include everything the query string part of the request in the actual endpoint however
                    // that severely limited the size of the emails that people were able to send. We should keep as much info as possible in the body of the request.
                    httpBody = this.SG_CREDS_BODY + '&' + this.SG_PARAM_SUBJECT + e.messageSubject + this.toAddressesToString( e.toAddresses) +
                                     this.SG_PARAM_HTML + e.messageBody + this.fromAddressesToString( e.fromAddresses);

                    // create unique args JSON object
                    String uniqueArgs = this.generateUniqueArgsJSONString( e);
                    httpBody += this.SG_PARAM_SMTP_API + uniqueArgs;

                    // make callout, parse response to EmailResult object
                    httpResult = HttpService.sendHttpRequest( httpEndpoint, httpBody, HttpService.HTTP_POST);
                    EmailResult er = this.parseAPIResponseToEmailResult( httpResult);
                    returnList.add( er);
                }
                catch ( Exception ex) 
                {
                    // SFP-1484: Log the exception so the team can review the results and handle appropriately.
                    // We re-throw the exception as a SendgridException simply because that is how it was before and a lot of classes calling this method are catching exceptions.
                    // Changing the code to not re-throw the exception seemed to be riskier.
                    insert new IntegrationLog__c (
                        Endpoint__c = httpEndpoint,
                        Raw_Request__c = httpBody,
                        Errors__c = ex.getMessage(),
                        Status__c = IntegrationLogs.Status.ERROR.name(),
                        Raw_Response__c = httpResult,
                        Source__c = SOURCE_SEND_GRID,
                        Http_Method__c = HttpService.HTTP_POST);

                    throw new SendgridEmailProviderException(ex);
                }    
            }
        }

        return returnList;
    }

    /**
     * @description: Implementation of required getType for providing instance type name
     *
     * @return String representing type name of this class
     */
    public String getType() {

        return this.TYPE_NAME;
    }

    /* private instance methods for String/JSON conversion and formatting specifc to Sendgrid API */
    
    // convert toAddresses list to API format
    private String toAddressesToString( List<String> addresses) {
        String returnVal = '';

        if ( addresses != null && addresses.size() == 1) {

            for ( String s : addresses) {

                returnVal += this.SG_PARAM_TO_SINGLE + 
                    EncodingUtil.urlEncode( s, this.ENCODING_TYPE);
            }
        } else if ( addresses != null && addresses.size() > 1) {

            for ( String s : addresses) {

                returnVal += this.SG_PARAM_TO_MULTIPLE + 
                    EncodingUtil.urlEncode( s, this.ENCODING_TYPE);
            }
        }

        return returnVal;
    }

    // convert fromAddresses list to API format
    private String fromAddressesToString( List<String> addresses) {
        String returnVal = '';

        if ( addresses != null && addresses.size() == 1) {

            for ( String s : addresses) {

                returnVal += this.SG_PARAM_FROM_SINGLE + 
                    EncodingUtil.urlEncode( s, this.ENCODING_TYPE);
            }
        } else if ( addresses != null && addresses.size() > 1) {

            for ( String s : addresses) {

                returnVal += this.SG_PARAM_FROM_MULTIPLE + 
                    EncodingUtil.urlEncode( s, this.ENCODING_TYPE);
            }
        }

        return returnVal;
    }

    // convert Email object unique arg fields into URL encoded JSON
    private String generateUniqueArgsJSONString( Email e) {
        String returnVal = '';

        if ( e != null) {

            // Create a JSONGenerator instance, disable pretty print
            JSONGenerator jGen = JSON.createGenerator( false);
            try {

                jGen.writeStartObject();
                jGen.writeFieldName( this.SG_PARAM_UNIQ_ARGS);
                jGen.writeStartObject();
                if ( e.schoolId != null) {
                    jGen.writeFieldName( this.SG_PARAM_UNIQ_ARGS_SCHOOL_ID);
                    jGen.writeString( e.schoolId);
                }
                if ( e.senderId != null) {
                    jGen.writeFieldName( this.SG_PARAM_UNIQ_ARGS_SENDER_ID);
                    jGen.writeString( e.senderId);
                }
                if ( e.recipientId != null) {
                    jGen.writeFieldName( this.SG_PARAM_UNIQ_ARGS_RECIP_ID);
                    jGen.writeString( e.recipientId);
                }
                if ( e.spaId != null) {
                    jGen.writeFieldName( this.SG_PARAM_UNIQ_ARGS_SPA_ID);
                    jGen.writeString( e.spaId);
                }
                if ( e.massEmailSendId != null) {
                    jGen.writeFieldName( this.SG_PARAM_UNIQ_ARGS_MASS_EMAIL_ID);
                    jGen.writeString( e.massEmailSendId);
                }
                if ( e.academicYear != null) {
                    jGen.writeFieldName( this.SG_PARAM_UNIQ_ARGS_ACADEMIC_YEAR);
                    jGen.writeString( e.academicYear);
                }
                jGen.writeEndObject();
                jGen.writeEndObject();

                returnVal = EncodingUtil.urlEncode( jGen.getAsString(), this.ENCODING_TYPE);
            } catch ( Exception ex) {

                // log exception and throw to caller for handling
                System.debug( 'DEBUG:::exception in SendgridEmailProvider.generateUniqueArgsJSONString' + 
                    ex.getMessage());
                throw ex;
            } finally {

                // close JSONGenerator regardless
                jGen.close();
            }
        }    

        return returnVal;
    }

    // parse Sendgrid API response JSON to EmailResult object
    private EmailResult parseAPIResponseToEmailResult( String jsonString) {
        EmailResult returnVal = new EmailResult();

        if ( !String.isEmpty( jsonString)) {

            try {

                Map<String,Object> topLevelMap = 
                    ( Map<String,Object>)JSON.deserializeUntyped( jsonString);
                    System.debug( 'DEBUG:::' + topLevelMap);

                returnVal.isSuccess = ( topLevelMap.get( this.SG_RESP_MESSAGE) == this.SG_RESP_SUCCESS ? true : false);
                List<Object> objList = 
                    ( !returnVal.isSuccess && topLevelMap.containsKey( this.SG_RESP_ERR_ARRAY) ? 
                        ( List<Object>)topLevelMap.get( this.SG_RESP_ERR_ARRAY) : returnVal.errorList);

                for ( Object o : objList) returnVal.errorList.add( String.valueOf( o));
            } catch ( Exception e) {

                // log exception and throw to caller for handling
                System.debug( 'DEBUG:::exception in SendgridEmailProvider.parseAPIResponseToEmailResult' + 
                    e.getMessage());
                throw e;
            }
        }

        return returnVal;
    }

}