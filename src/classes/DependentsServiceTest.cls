@isTest
private class DependentsServiceTest {

    private static Dependents__c createDependents() {
        return new Dependents__c(
                Dependent_1_Full_Name__c = 'Full Name 1',
                Dependent_1_Gender__c = 'Male',
                Dependent_1_Birthdate__c = Date.today(),
                Dependent_1_Current_School__c = 'Current School',
                Dependent_1_Current_Grade__c = 'Preschool',
                Dependent_1_School_Next_Year__c = 'School Next Year',
                Dependent_1_School_Next_Year_Type__c = 'Public School',
                Dependent_1_Grade_Next_Year__c = 'Preschool',
                Dependent_1_Have_Education_Expenses__c = 'Yes',
                Dependent_1_Tuition_Cost_Est__c = 1,
                Dependent_1_Tuition_Cost_Current__c = 1,
                Dependent_1_Parent_Sources_Current__c = 1,
                Dependent_1_Fin_Aid_Sources_Current__c = 1,
                Dependent_1_Student_Sources_Current__c = 1,
                Dependent_1_Student_Sources_Est__c = 1,
                Dependent_1_Other_Source_Sources_Current__c = 1,
                Dependent_1_Other_Source_Description__c = 'Other Source Description',
                Dependent_1_Loan_Sources_Current__c = 1,
                Dependent_1_Loan_Sources_Est__c = 1,
                Dependent_1_Other_Source_Sources_Est__c = 1,
                Dependent_1_Parent_Sources_Est__c = 1,
                Dependent_1_Fin_Aid_Sources_Est__c = 1,
                Dependent_1_Tuition_Program_Next_Year__c = 'No',

                Dependent_2_Full_Name__c = 'Full Name 2',
                Dependent_2_Gender__c = 'Male',
                Dependent_2_Birthdate__c = Date.today(),
                Dependent_2_Current_School__c = 'Current School',
                Dependent_2_Current_Grade__c = 'Preschool',
                Dependent_2_School_Next_Year__c = 'School Next Year',
                Dependent_2_School_Next_Year_Type__c = 'Public School',
                Dependent_2_Grade_Next_Year__c = 'Preschool',
                Dependent_2_Have_Education_Expenses__c = 'Yes',
                Dependent_2_Tuition_Cost_Est__c = 2,
                Dependent_2_Tuition_Cost_Current__c = 2,
                Dependent_2_Parent_Sources_Current__c = 2,
                Dependent_2_Fin_Aid_Sources_Current__c = 2,
                Dependent_2_Student_Sources_Current__c = 2,
                Dependent_2_Student_Sources_Est__c = 2,
                Dependent_2_Other_Source_Sources_Current__c = 2,
                Dependent_2_Other_Source_Description__c = 'Other Source Description',
                Dependent_2_Loan_Sources_Current__c = 2,
                Dependent_2_Loan_Sources_Est__c = 2,
                Dependent_2_Other_Source_Sources_Est__c = 2,
                Dependent_2_Parent_Sources_Est__c = 2,
                Dependent_2_Fin_Aid_Sources_Est__c = 2,
                Dependent_2_Tuition_Program_Next_Year__c = 'No',

                Dependent_3_Full_Name__c = 'Full Name 3',
                Dependent_3_Gender__c = 'Male',
                Dependent_3_Birthdate__c = Date.today(),
                Dependent_3_Current_School__c = 'Current School',
                Dependent_3_Current_Grade__c = 'Preschool',
                Dependent_3_School_Next_Year__c = 'School Next Year',
                Dependent_3_School_Next_Year_Type__c = 'Public School',
                Dependent_3_Grade_Next_Year__c = 'Preschool',
                Dependent_3_Have_Education_Expenses__c = 'Yes',
                Dependent_3_Tuition_Cost_Est__c = 3,
                Dependent_3_Tuition_Cost_Current__c = 3,
                Dependent_3_Parent_Sources_Current__c = 3,
                Dependent_3_Fin_Aid_Sources_Current__c = 3,
                Dependent_3_Student_Sources_Current__c = 3,
                Dependent_3_Student_Sources_Est__c = 3,
                Dependent_3_Other_Source_Sources_Current__c = 3,
                Dependent_3_Other_Source_Description__c = 'Other Source Description',
                Dependent_3_Loan_Sources_Current__c = 3,
                Dependent_3_Loan_Sources_Est__c = 3,
                Dependent_3_Other_Source_Sources_Est__c = 3,
                Dependent_3_Parent_Sources_Est__c = 3,
                Dependent_3_Fin_Aid_Sources_Est__c = 3,
                Dependent_3_Tuition_Program_Next_Year__c = 'No',

                Dependent_4_Full_Name__c = 'Full Name 4',
                Dependent_4_Gender__c = 'Male',
                Dependent_4_Birthdate__c = Date.today(),
                Dependent_4_Current_School__c = 'Current School',
                Dependent_4_Current_Grade__c = 'Preschool',
                Dependent_4_School_Next_Year__c = 'School Next Year',
                Dependent_4_School_Next_Year_Type__c = 'Public School',
                Dependent_4_Grade_Next_Year__c = 'Preschool',
                Dependent_4_Have_Education_Expenses__c = 'Yes',
                Dependent_4_Tuition_Cost_Est__c = 4,
                Dependent_4_Tuition_Cost_Current__c = 4,
                Dependent_4_Parent_Sources_Current__c = 4,
                Dependent_4_Fin_Aid_Sources_Current__c = 4,
                Dependent_4_Student_Sources_Current__c = 4,
                Dependent_4_Student_Sources_Est__c = 4,
                Dependent_4_Other_Source_Sources_Current__c = 4,
                Dependent_4_Other_Source_Description__c = 'Other Source Description',
                Dependent_4_Loan_Sources_Current__c = 4,
                Dependent_4_Loan_Sources_Est__c = 4,
                Dependent_4_Other_Source_Sources_Est__c = 4,
                Dependent_4_Parent_Sources_Est__c = 4,
                Dependent_4_Fin_Aid_Sources_Est__c = 4,
                Dependent_4_Tuition_Program_Next_Year__c = 'No',

                Dependent_5_Full_Name__c = 'Full Name 5',
                Dependent_5_Gender__c = 'Male',
                Dependent_5_Birthdate__c = Date.today(),
                Dependent_5_Current_School__c = 'Current School',
                Dependent_5_Current_Grade__c = 'Preschool',
                Dependent_5_School_Next_Year__c = 'School Next Year',
                Dependent_5_School_Next_Year_Type__c = 'Public School',
                Dependent_5_Grade_Next_Year__c = 'Preschool',
                Dependent_5_Have_Education_Expenses__c = 'Yes',
                Dependent_5_Tuition_Cost_Est__c = 5,
                Dependent_5_Tuition_Cost_Current__c = 5,
                Dependent_5_Parent_Sources_Current__c = 5,
                Dependent_5_Fin_Aid_Sources_Current__c = 5,
                Dependent_5_Student_Sources_Current__c = 5,
                Dependent_5_Student_Sources_Est__c = 5,
                Dependent_5_Other_Source_Sources_Current__c = 5,
                Dependent_5_Other_Source_Description__c = 'Other Source Description',
                Dependent_5_Loan_Sources_Current__c = 5,
                Dependent_5_Loan_Sources_Est__c = 5,
                Dependent_5_Other_Source_Sources_Est__c = 5,
                Dependent_5_Parent_Sources_Est__c = 5,
                Dependent_5_Fin_Aid_Sources_Est__c = 5,
                Dependent_5_Tuition_Program_Next_Year__c = 'No',

                Dependent_6_Full_Name__c = 'Full Name 6',
                Dependent_6_Gender__c = 'Male',
                Dependent_6_Birthdate__c = Date.today(),
                Dependent_6_Current_School__c = 'Current School',
                Dependent_6_Current_Grade__c = 'Preschool',
                Dependent_6_School_Next_Year__c = 'School Next Year',
                Dependent_6_School_Next_Year_Type__c = 'Public School',
                Dependent_6_Grade_Next_Year__c = 'Preschool',
                Dependent_6_Have_Education_Expenses__c = 'Yes',
                Dependent_6_Tuition_Cost_Est__c = 6,
                Dependent_6_Tuition_Cost_Current__c = 6,
                Dependent_6_Parent_Sources_Current__c = 6,
                Dependent_6_Fin_Aid_Sources_Current__c = 6,
                Dependent_6_Student_Sources_Current__c = 6,
                Dependent_6_Student_Sources_Est__c = 6,
                Dependent_6_Other_Source_Sources_Current__c = 6,
                Dependent_6_Other_Source_Description__c = 'Other Source Description',
                Dependent_6_Loan_Sources_Current__c = 6,
                Dependent_6_Loan_Sources_Est__c = 6,
                Dependent_6_Other_Source_Sources_Est__c = 6,
                Dependent_6_Parent_Sources_Est__c = 6,
                Dependent_6_Fin_Aid_Sources_Est__c = 6,
                Dependent_6_Tuition_Program_Next_Year__c = 'No'
        );
    }


    @isTest private static void copyDependentsPreviousToNew_sObjectsNotNull_fieldsPopulated() {
        Dependents__c prevDependents = createDependents();
        Dependents__c newDependents = new Dependents__c();

        DependentsService.Instance.copyDependentsPreviousToNew(prevDependents, newDependents);

        System.assert(newDependents.Dependent_1_Full_Name__c == prevDependents.Dependent_1_Full_Name__c);
        System.assert(newDependents.Dependent_1_Gender__c == prevDependents.Dependent_1_Gender__c);
        System.assert(newDependents.Dependent_1_Birthdate__c == prevDependents.Dependent_1_Birthdate__c);
        System.assert(newDependents.Dependent_1_Current_Grade__c == prevDependents.Dependent_1_Current_Grade__c);
        System.assert(newDependents.Dependent_2_Full_Name__c == prevDependents.Dependent_2_Full_Name__c);
        System.assert(newDependents.Dependent_2_Gender__c == prevDependents.Dependent_2_Gender__c);
        System.assert(newDependents.Dependent_2_Birthdate__c == prevDependents.Dependent_2_Birthdate__c);
        System.assert(newDependents.Dependent_2_Current_Grade__c == prevDependents.Dependent_2_Current_Grade__c);
        System.assert(newDependents.Dependent_3_Full_Name__c == prevDependents.Dependent_3_Full_Name__c);
        System.assert(newDependents.Dependent_3_Gender__c == prevDependents.Dependent_3_Gender__c);
        System.assert(newDependents.Dependent_3_Birthdate__c == prevDependents.Dependent_3_Birthdate__c);
        System.assert(newDependents.Dependent_3_Current_Grade__c == prevDependents.Dependent_3_Current_Grade__c);
        System.assert(newDependents.Dependent_4_Full_Name__c == prevDependents.Dependent_4_Full_Name__c);
        System.assert(newDependents.Dependent_4_Gender__c == prevDependents.Dependent_4_Gender__c);
        System.assert(newDependents.Dependent_4_Birthdate__c == prevDependents.Dependent_4_Birthdate__c);
        System.assert(newDependents.Dependent_4_Current_Grade__c == prevDependents.Dependent_4_Current_Grade__c);
        System.assert(newDependents.Dependent_5_Full_Name__c == prevDependents.Dependent_5_Full_Name__c);
        System.assert(newDependents.Dependent_5_Gender__c == prevDependents.Dependent_5_Gender__c);
        System.assert(newDependents.Dependent_5_Birthdate__c == prevDependents.Dependent_5_Birthdate__c);
        System.assert(newDependents.Dependent_5_Current_Grade__c == prevDependents.Dependent_5_Current_Grade__c);
        System.assert(newDependents.Dependent_6_Full_Name__c == prevDependents.Dependent_6_Full_Name__c);
        System.assert(newDependents.Dependent_6_Gender__c == prevDependents.Dependent_6_Gender__c);
        System.assert(newDependents.Dependent_6_Birthdate__c == prevDependents.Dependent_6_Birthdate__c);
        System.assert(newDependents.Dependent_6_Current_Grade__c == prevDependents.Dependent_6_Current_Grade__c);
        System.assert(newDependents.Additional_Dependents__c == prevDependents.Additional_Dependents__c);
    }


    @isTest private static void moveDependents_dependentsNotNull_dependentsUpdated() 
    {
        Dependents__c dep = createDependents();

        DependentsService.Instance.moveDependents(dep, true);
        System.assert(dep.Number_of_Dependents__c == 6);

        dep = createDependents();
        dep.Additional_Dependents__c = 'Additional Dependents';
        DependentsService.Instance.moveDependents(dep, true);
        System.assert(dep.Number_of_Dependents__c == 7);
    }


    @isTest private static void removeDependentData_dependentsNotNull_dependentsUpdated() {
        Dependents__c dep = createDependents();

        Dependents__c resultDep = DependentsService.Instance.removeDependentData(dep, 1);

        System.assert(resultDep.Dependent_1_Full_Name__c == null);
        System.assert(resultDep.Dependent_1_Gender__c == null);
        System.assert(resultDep.Dependent_1_Birthdate__c == null);
        System.assert(resultDep.Dependent_1_Current_School__c == null);
        System.assert(resultDep.Dependent_1_Current_Grade__c == null);
        System.assert(resultDep.Dependent_1_School_Next_Year__c == null);
        System.assert(resultDep.Dependent_1_School_Next_Year_Type__c == null);
        System.assert(resultDep.Dependent_1_Grade_Next_Year__c == null);
        System.assert(resultDep.Dependent_1_Have_Education_Expenses__c == null);
        System.assert(resultDep.Dependent_1_Tuition_Cost_Est__c == null);
        System.assert(resultDep.Dependent_1_Tuition_Cost_Current__c == null);
        System.assert(resultDep.Dependent_1_Parent_Sources_Current__c == null);
        System.assert(resultDep.Dependent_1_Fin_Aid_Sources_Current__c == null);
        System.assert(resultDep.Dependent_1_Student_Sources_Current__c == null);
        System.assert(resultDep.Dependent_1_Student_Sources_Est__c == null);
        System.assert(resultDep.Dependent_1_Other_Source_Sources_Current__c == null);
        System.assert(resultDep.Dependent_1_Other_Source_Description__c == null);
        System.assert(resultDep.Dependent_1_Loan_Sources_Current__c == null);
        System.assert(resultDep.Dependent_1_Loan_Sources_Est__c == null);
        System.assert(resultDep.Dependent_1_Other_Source_Sources_Est__c == null);
        System.assert(resultDep.Dependent_1_Parent_Sources_Est__c == null);
        System.assert(resultDep.Dependent_1_Fin_Aid_Sources_Est__c == null);
        System.assert(resultDep.Dependent_1_Tuition_Program_Next_Year__c == null);
    }
}