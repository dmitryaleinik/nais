/**
 * @description This class is used to create Transaction records for unit tests.
 */
@isTest
public class TransactionTestData extends SObjectTestData {

    private final static String TRANSACTION_RESPONSE_STATUS_SUCCESS = 'Success';
    private final static Integer DEFAULT_TRANSACTION_AMOUNT = 100;
    private final static String DEFAULT_TRANSACTION_TYPE = 'Charge';

    /**
     * @description Get the default values for the ChargentOrders__Transaction__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
            ChargentOrders__Transaction__c.ChargentOrders__Order__c => ChargentOrderTestData.Instance.DefaultChargentOrder,
            ChargentOrders__Transaction__c.ChargentOrders__Response_Status__c => TRANSACTION_RESPONSE_STATUS_SUCCESS,
            ChargentOrders__Transaction__c.ChargentOrders__Amount__c => DEFAULT_TRANSACTION_AMOUNT,
            ChargentOrders__Transaction__c.ChargentOrders__Type__c => DEFAULT_TRANSACTION_TYPE
        };
    }

    /**
     * @description Assigns Transaction to the desired Chargent Order.
     * @param chargentOrderId The value to set the ChargentOrders__Order__c field to.
     * @return The current TransactionTestData instance.
     */
    public TransactionTestData forChargentOrder(Id chargentOrderId) {
        return (TransactionTestData) with(ChargentOrders__Transaction__c.ChargentOrders__Order__c, chargentOrderId);
    }

    /**
     * @description Insert the current working ChargentOrders__Transaction__c record.
     * @return The currently operated upon ChargentOrders__Transaction__c record.
     */
    public ChargentOrders__Transaction__c insertTransaction() {
        return (ChargentOrders__Transaction__c)insertRecord();
    }

    /**
     * @description Create the current working Transaction record without resetting
     *          the stored values in this instance of TransactionTestData.
     * @return A non-inserted ChargentOrders__Transaction__c record using the currently stored field
     *          values.
     */
    public ChargentOrders__Transaction__c createTransactionWithoutReset() {
        return (ChargentOrders__Transaction__c)buildWithoutReset();
    }

    /**
     * @description Create the current working ChargentOrders__Transaction__c record.
     * @return The currently operated upon ChargentOrders__Transaction__c record.
     */
    public ChargentOrders__Transaction__c create() {
        return (ChargentOrders__Transaction__c)super.buildWithReset();
    }

    /**
     * @description The default ChargentOrders__Transaction__c record.
     */
    public ChargentOrders__Transaction__c DefaultTransaction {
        get {
            if (DefaultTransaction == null) {
                DefaultTransaction = createTransactionWithoutReset();
                insert DefaultTransaction;
            }
            return DefaultTransaction;
        }
        private set;
    }

    /**
     * @description Get the ChargentOrders__Transaction__c SObjectType.
     * @return The ChargentOrders__Transaction__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return ChargentOrders__Transaction__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static TransactionTestData Instance {
        get {
            if (Instance == null) {
                Instance = new TransactionTestData();
            }
            return Instance;
        }
        private set;
    }

    private TransactionTestData() { }
}