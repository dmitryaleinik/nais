@isTest
private class TableStateIncomeTaxTest
{
    
    @isTest
    private static void testTableStateIncomeTax()
    {
        // create the academic year
        Academic_Year__c academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        
        // create the state tax table
        TableTestData.populateStateTaxTableData(academicYear.Id, 'Alabama');

        // test lowest bracket
        System.assertEquals(950, TableStateIncomeTax.calculateTax(academicYear.Id, 'Alabama', null, 10000));
        
        // test middle bracket
        System.assertEquals(7000, TableStateIncomeTax.calculateTax(academicYear.Id, 'Alabama', null, 100000));
        
        // test highest bracket
        System.assertEquals(60000, TableStateIncomeTax.calculateTax(academicYear.Id, 'Alabama', null, 1000000));
    }   
}