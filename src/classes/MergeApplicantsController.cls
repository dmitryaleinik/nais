public with sharing class MergeApplicantsController {
    
    /* Variables */
    public Student_Folder__c searchFolder {get; set;}
    public Student_Folder__c masterFolder {get; set;}
    public Student_Folder__c childFolder {get; set;}
    public Contact searchContact {get; set;}
    public List<selectableFolderGroup> searchResults {get; set;}
    public Boolean useFirstFuzzySearch {get; set;}
    public Boolean useLastFuzzySearch {get; set;}
    
    public Integer contactConfirmation { get; set; }
    public Integer folderConfirmation { get; set; }
    public Integer spaConfirmation { get; set; }
    public Integer baConfirmation { get; set; }
    
    public Boolean ranASearch { get; set; }
    
    public Boolean areConfirmations{
        get{
            return (contactConfirmation != null || folderConfirmation != null || spaConfirmation != null || baConfirmation != null);
        } 
        set;
    }
    
    /* Initialization */
    public MergeApplicantsController(){
        searchFolder = new Student_Folder__c();
        masterFolder = new Student_Folder__c();
        childFolder = new Student_Folder__c();
        searchContact = new Contact();
        
        useFirstFuzzySearch = false;
        useLastFuzzySearch = false;
        
        ranASearch = false;
    }
    
    /* Action Methods */
    public void searchForContacts(){
        String queryString = 'select Id, Student__c, Student__r.FirstName, Student__r.LastName, Student__r.Birthdate, Academic_Year_Picklist__c, School__r.Name, Grade_Applying__c, ';
        queryString += '(select Id, School__c from School_PFS_Assignments__r) ';
        queryString += 'from Student_Folder__c ';
        if(useFirstFuzzySearch){
            queryString += ' where Student__r.FirstName LIKE \'%' + searchContact.FirstName + '%\'';
        }
        else{
            queryString += ' where Student__r.FirstName = \'' + searchContact.FirstName + '\'';
        }
        if(useLastFuzzySearch){
            queryString += ' and Student__r.LastName LIKE \'%' + searchContact.LastName + '%\'';
        }
        else{
            queryString += ' and Student__r.LastName = \'' + searchContact.LastName + '\'';    
        }
        
        if(searchContact.Birthdate != null){
            Date filterDate = searchContact.Birthdate;
            queryString += ' and Student__r.Birthdate = :filterDate';    
        }
        if(searchFolder.Grade_Applying__c != null){
            queryString += ' and Grade_Applying__c = \'' + searchFolder.Grade_Applying__c + '\'';    
        }

        // debugList = new List<Student_Folder__c>{}; // This is for seeing the raw query results and should be removed 
        searchResults = new List<selectableFolderGroup>{};
        
        Map<String, Map<Id, Student_Folder__c>> folderGroupMap = new Map<String, Map<Id, Student_Folder__c>>{};
        
        ranASearch = true;
        
        for(Student_Folder__c result : Database.query(queryString)){
            // debugList.add(result); // This is for seeing the raw query results and should be removed 
            // String resultKey = result.Student__r.FirstName.toLowerCase() + '__' + result.Student__r.LastName.toLowerCase() + '__' + result.Academic_Year__c;
            // String resultKey = result.Student__r.FirstName.toLowerCase() + '__' + result.Student__r.LastName.toLowerCase();
            
            // Because the Student Folder School__c field is only used for school-initiated folders 
            Id resultKey;
            if(result.School__c != null){
                resultKey = result.School__c;
            }
            else if(result.School_PFS_Assignments__r != null && result.School_PFS_Assignments__r.size() > 0 ){
                resultKey = result.School_PFS_Assignments__r[0].School__c;
            }
            
            if(resultKey != null && resultKey == searchFolder.School__c){
                // Look for an existing list of records for the given school
                Map<Id, Student_Folder__c> existingMap = folderGroupMap.get(resultKey);
                // If there is not already a list of records for the first and last name 
                if(existingMap == null){
                    existingMap = new Map<Id, Student_Folder__c>{};
                }
                
                // Add the current result to the map for the first and last name
                // existingMap.put(result.Student__r.Id, result);
                existingMap.put(result.Id, result);
                
                // Add the map for first and last name to the map of groups
                folderGroupMap.put(resultKey, existingMap);
            }
        }
        
        for(String keyValue : folderGroupMap.keySet()){
            Map<Id, Student_Folder__c> currentMap = folderGroupMap.get(keyValue);
            
            // If there is more than one Applicant with the same First and Last Name
            if(currentMap.values().size() > 1){
                selectableFolderGroup currentGroup = new selectableFolderGroup();  
                for(Student_Folder__c folder : currentMap.values()){
                    currentGroup.folderGroup.add(new selectableFolder(folder, keyValue));
                }
                searchResults.add(currentGroup);
            }
        }
    }
    
    public PageReference mergeSelected(){
        
        Map<Id, Student_Folder__c> allFoldersMap = new Map<Id, Student_Folder__c>{};
        Map<Id, Set<Id>> survivorFolderMap = new Map<Id, Set<Id>>{};
        Map<Id, Set<Id>> survivorContactMap = new Map<Id, Set<Id>>{};
        
        // Iterate through the various groups of search results
        for(selectableFolderGroup recordGroup : searchResults){
            // In each group see which record is selected and which are not            
            String survivorFolderId = null;
            String survivorContactId = null;
            Set<Id> deletionFolderSet = new Set<Id>{};
            Set<Id> deletionContactSet = new Set<Id>{};
            for(selectableFolder folderRecord : recordGroup.folderGroup){
                // Identify each folder as one to remove or one to keep
                if(folderRecord.folderSelected){
                    survivorFolderId = folderRecord.folder.Id;
                    survivorContactId = folderRecord.folder.Student__c;
                }
                else {
                    deletionFolderSet.add(folderRecord.folder.Id);
                    deletionContactSet.add(folderRecord.folder.Student__c);
                }
                allFoldersMap.put(folderRecord.folder.Id, folderRecord.folder);
            }
            
            // Make sure that in the case of one Contact and multiple Folders
            //  we don't try to merge the one Contact
            deletionContactSet.remove(survivorContactId);
            
            // If the group has a record selected as the survivor
            if(survivorFolderId != null){
                survivorFolderMap.put(survivorFolderId, deletionFolderSet);
                survivorContactMap.put(survivorContactId, deletionContactSet);
            }
        }
        
        // Iterate through the Map of Contacts and merge duplicates
        for(Id contactId : survivorContactMap.keySet()){
            // If there is at least one duplicate
            Set<Id> deletionContactSet = survivorContactMap.get(contactId);
            
            if(deletionContactSet != null && deletionContactSet.size() > 0){
                // Filter merge set down to no more than two children to merge
                Set<Id> mergeContactSet = new Set<Id>{};
                Integer idsAdded = 0;
                for(Id mergeContactId : deletionContactSet){
                    mergeContactSet.add(mergeContactId);
                    idsAdded++;
                    if(idsAdded > 1) break;
                } 
                // Merge the duplicate Contacts with the one selected to keep
                Database.merge(new Contact(Id = contactId), new List<Id>(mergeContactSet), true);
            }
        }

        // Iterate through the Sets of duplicate folder records to compile a list for querying
        //  and to get an inverted Map of duplicate -> survivor for re-parenting        
        Set<Id> allDuplicateFolderIds = new Set<Id>{};
        List<Student_Folder__c> allDuplicateFolders = new List<Student_Folder__c>{};
        Map<Id, Id> dupeToSurvivorMap = new Map<Id, Id>{};
        // For each surviving record that's selected
        for(Id folderId : survivorFolderMap.keySet()){
            // Get the master record to be kept
            Student_Folder__c masterFolder = allFoldersMap.get(folderId);
            // Get the records that are likely duplicates
            Set<Id> deletionFolderSet = survivorFolderMap.get(folderId);
            if(deletionFolderSet != null && deletionFolderSet.size() > 0){                
                for(Id dupeFolderId : deletionFolderSet){
                    // Check to make sure that we're only going to remove folders that are in the same
                    //  Academic Year as the one selected to survive the merge
                    Student_Folder__c dupeFolder = allFoldersMap.get(dupeFolderId);
                    if(dupeFolder.Academic_Year_Picklist__c == masterFolder.Academic_Year_Picklist__c){
                        allDuplicateFolderIds.addAll(deletionFolderSet); // Ids for querying related Child records
                        allDuplicateFolders.add(new Student_Folder__c(Id=dupeFolderId)); // Full records for deleting 
                        dupeToSurvivorMap.put(dupeFolderId, folderId);
                    }
                }
            }
        }
        
        // Query for SPFS records to be re-parented
        List<School_PFS_Assignment__c> spfsRecordsToReparent = [select Id, Student_Folder__c from School_PFS_Assignment__c where Student_Folder__c in :allDuplicateFolderIds]; 
        
        // Query for Budget Allocation records to be re-parented
        List<Budget_Allocation__c> budgetAllocationsToReparent = [select Id, Student_Folder__c from Budget_Allocation__c where Student_Folder__c in :allDuplicateFolderIds];
        
        // Iterate through SPFS records
        for(School_PFS_Assignment__c spfsRecord : spfsRecordsToReparent){
            // Look up what the new Parent record Id should be for the SPFS
            Id newFolderId = dupeToSurvivorMap.get(spfsRecord.Student_Folder__c);
            if(newFolderId != null){
                spfsRecord.Student_Folder__c = newFolderId;
            }
        }
        
        // Iterate through Budget Allocation records
        for(Budget_Allocation__c budgetRecord : budgetAllocationsToReparent){
            // Look up what the new Parent record Id should be for the Budget Allocation
            Id newFolderId = dupeToSurvivorMap.get(budgetRecord.Student_Folder__c);
            if(newFolderId != null){
                budgetRecord.Student_Folder__c = newFolderId;
            }
        }
        
        // Update SPFS records
        if(spfsRecordsToReparent != null && spfsRecordsToReparent.size() > 0){
            update spfsRecordsToReparent;
        }
        
        // Update Budget Allocation records
        if(budgetAllocationsToReparent != null && budgetAllocationsToReparent.size() > 0){
            update budgetAllocationsToReparent;
        }
        
        // Delete the duplicate folders
        if(allDuplicateFolders != null && allDuplicateFolders.size() > 0){
            delete allDuplicateFolders;
        }
        
        searchForContacts();
        
        PageReference reloadPage = new PageReference('/apex/MergeApplicants'); 
        return reloadPage;
    }
    
    public PageReference mergeContacts(){
        // If the user has not specified the same Contact record
        if(masterFolder.Student__c != childFolder.Student__c){
            // Merge contacts
            Database.merge(new Contact(Id = masterFolder.Student__c), childFolder.Student__c, true);
            contactConfirmation = 1;
        }
        else{
            contactConfirmation = 0;
        }
        
        // Query for the set of Folders related to the master Contact, including their
        //     related School PFS Assignment and Budget Allocation records
        List<Student_Folder__c> folderTrees = [select Id, Academic_Year_Picklist__c, School__c, (select Id, School__c from School_PFS_Assignments__r), (select Id from Budget_Allocations__r) from Student_Folder__c where Student__c = :masterFolder.Student__c];

        // Iterate through the folders and map them together by Academic Year and School
        Map<String, List<Student_Folder__c>> sfTreeMap = new Map<String, List<Student_Folder__c>>{};
        for(Student_Folder__c folderRecord : folderTrees){
            
            Id schoolIdValue;
            if(folderRecord.School__c != null){
                schoolIdValue = folderRecord.School__c;
            }                
            else if(folderRecord.School_PFS_Assignments__r != null && folderRecord.School_PFS_Assignments__r.size() > 0 ){
                schoolIdValue = folderRecord.School_PFS_Assignments__r[0].School__c; 
            }

            // Just in case, somehow, there is a folder that is not a manually created folder
            //  and doesn't have any SPA records attached
            if(schoolIdValue != null){
                // Determine the key value for the current folder
                String folderKey = folderRecord.Academic_Year_Picklist__c + '_' + schoolIdValue; // NAIS-2417 [DP] 05.14.2015
                // Check to see if it's already in the map and create a new list if needed
                List<Student_Folder__c> mappedFolderList = sfTreeMap.get(folderKey);
                if(mappedFolderList == null){
                    mappedFolderList = new List<Student_Folder__c>();
                }
                // Add the current folder to a list and put the list back into the map
                mappedFolderList.add(folderRecord);
                sfTreeMap.put(folderKey, mappedFolderList);
            }
        }
        
        // Look through the map for keys that have more than one Folder in their value list
        List<School_PFS_Assignment__c> spaRecordsToUpdate = new List<School_PFS_Assignment__c>{};
        List<Budget_Allocation__c> budgetAllocationsToUpdate = new List<Budget_Allocation__c>{};
        List<Student_Folder__c> foldersToDelete = new List<Student_Folder__c>{};
        
        for(String keyValue : sfTreeMap.keySet()){
            List<Student_Folder__c> foldersList = sfTreeMap.get(keyValue);
            // Look through the list starting with index 1 (this will skip the list if size is 1)
            for(Integer i=1; i<foldersList.size(); i++){
                // Re-point any SPA records
                for(School_PFS_Assignment__c spaChild : foldersList[i].School_PFS_Assignments__r){
                    spaChild.Student_Folder__c = foldersList[0].Id;
                    spaRecordsToUpdate.add(spaChild);
                }
                
                // Re-point any Budget Allocation records
                for(Budget_Allocation__c baChild : foldersList[i].Budget_Allocations__r){
                    baChild.Student_Folder__c = foldersList[0].Id;
                    budgetAllocationsToUpdate.add(baChild);    
                }
                
                // Add folder to list of deletion
                foldersToDelete.add(foldersList[i]);
            }
        }
        
        // Reparent SPA records
        spaConfirmation = spaRecordsToUpdate.size();
        if(spaRecordsToUpdate.size() > 0){
            update spaRecordsToUpdate;    
        }
        
        // Reparent BA records
        baConfirmation = budgetAllocationsToUpdate.size();
        if(budgetAllocationsToUpdate.size() > 0){
            update budgetAllocationsToUpdate;
        }
        
        // Remove extra folders
        folderConfirmation = foldersToDelete.size();
        if(foldersToDelete.size() > 0){
            Dml.WithoutSharing.deleteObjects(foldersToDelete);
        }
        
        return null;
        
    }

    /* Inner Classes */
    public class selectableFolder{
        public Boolean folderSelected {get; set;}
        public Boolean contactSelected {get; set;}
        public String keyValue {get; set;}
        public Student_Folder__c folder {get; set;}
        
        public selectableFolder(Student_Folder__c folder, String keyValue){
            folderSelected = false;
            contactSelected = false;
            
            this.keyValue = keyValue;
            this.folder = folder;
        }        
    }
    
    public class selectableFolderGroup{
        public List<selectableFolder> folderGroup {get; set;}
        
        public selectableFolderGroup(){
            folderGroup = new List<selectableFolder>{};
        }
    }

}