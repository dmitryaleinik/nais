/**
 * @description Handle logging to the Custom_Debug__c object for any exception.
 */
public class CustomDebugService {
    private String customMessage;

    /**
     * @description Logs an exception into the Custom Debug
     *              object if the toggle has been turned on
     *              in the active PaymentProvider.
     * @param e The exception to log.
     * @return A shiny new Custom_Debug__c record with the
     *         exception information in it.
     */
    public Custom_Debug__c logException(Exception e) {
        // Normally we would throw an ArgumentNullException here if e was
        // null, but we don't want to cause an unintended rollback of the
        // current operation if we do.
        if (e == null || !PaymentProcessor.CustomDebug) {
            return null;
        }

        Custom_Debug__c customDebug;

        String message = e.getMessage();
        String exceptionType = e.getTypeName();
        String stackTrace = e.getStackTraceString();

        // Create and insert the Custom Debug record.
        customDebug = new Custom_Debug__c(
            Message__c = message,
            Custom_Message__c = this.customMessage,
            Type__c = exceptionType,
            Stack_Trace__c = stackTrace);
        insert customDebug;

        return customDebug;
    }

    /**
     * @description Set a custom message for the logged exception.
     * @param customMessage The custom message to log with the exception.
     * @return The current working instance of the Custom Debug Service.
     */
    public CustomDebugService withCustomMessage(String customMessage) {
        if (String.isNotBlank(customMessage)) {
            this.customMessage = customMessage;
        }

        return this;
    }

    /**
     * @description Singleton instance of the CustomDebugService.
     */
    public static CustomDebugService Instance {
        get {
            if (Instance == null) {
                Instance = new CustomDebugService();
            }
            return Instance;
        }
        private set;
    }

    private CustomDebugService() {}
}