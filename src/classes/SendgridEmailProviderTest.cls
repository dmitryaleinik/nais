/**
 * SendgridEmailProviderTest.cls
 *
 * @description: Test class for SendgridEmail using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest
private class SendgridEmailProviderTest {

    /* test data setup */
    @testSetup static void setupTestData() {

        // any necessary data setup
    }
    
    /* negative test cases */

    // Call send emails with empty Email list
    @isTest static void sendEmails_noEmailList_emptyResultList() {
        
        // Arrange
        Test.setMock( HttpCalloutMock.class, new SendgridMockHttpResponseGenerator());
        List<EmailResult> eResult = new List<EmailResult>();
        SendgridEmailProvider sGridEmail = new SendgridEmailProvider();
        
        // Act
        eResult = sGridEmail.sendEmails( null);

        // Assert
        System.debug( 'The expected size is 0 and the actual size is ' + eResult.size());
        System.assertEquals( 0, eResult.size());
    }


    /* postive test cases */

    // Call send emails with populated Email list, single to address
    @isTest static void sendEmails_singleAddress_validResultList() {

        // Arrange
        Test.setMock( HttpCalloutMock.class, new SendgridMockHttpResponseGenerator());
        List<EmailResult> eResult = new List<EmailResult>();
        SendgridEmailProvider sGridEmail = new SendgridEmailProvider();
        Id schoolId = '003J0000011yJcHIAU';
        Id senderId = '003J0000011yJcHIAU';
        Id recipId = '003J0000011yJcHIAU';
        Id spaId = '003J0000011yJcHIAU';
        Id massEmailId = '003J0000011yJcHIAU';

        // Act
        sGridEmail.emailMessageList.add( 
            new Email( new List<String> {'testemail@test.com'}, new List<String> {'info@test.com'}, 'test with space', 'more testing', schoolId,
                       senderId, recipId, spaId, massEmailId, '2016-2017'));
        eResult = sGridEmail.sendEmails( sGridEmail.emailMessageList);

        // Assert
        System.debug( 'The expected size is 1 and the actual size is ' + eResult.size());
        System.assertEquals( 1, eResult.size());
    }

    // Call send emails with populated Email list, multiple to and from addresses
    @isTest static void sendEmails_multipleAddresses_validResultList() {

        // Arrange
        Test.setMock( HttpCalloutMock.class, new SendgridMockHttpResponseGenerator());
        List<EmailResult> eResult = new List<EmailResult>();
        SendgridEmailProvider sGridEmail = new SendgridEmailProvider();
        Id schoolId = '003J0000011yJcHIAU';
        Id senderId = '003J0000011yJcHIAU';
        Id recipId = '003J0000011yJcHIAU';
        Id spaId = '003J0000011yJcHIAU';
        Id massEmailId = '003J0000011yJcHIAU';

        // Act
        sGridEmail.emailMessageList.add( 
            new Email( new List<String> {'testemail@test.com', 'testemail2@test.com'}, new List<String> {'info@test.com', 'info2@test.com'}, 
                       'test with space', 'more testing', schoolId, senderId, recipId, spaId, massEmailId, '2016-2017'));
        eResult = sGridEmail.sendEmails( sGridEmail.emailMessageList);

        // Assert
        System.debug( 'The expected size is 1 and the actual size is ' + eResult.size());
        System.assertEquals( 1, eResult.size());
    }
    
}