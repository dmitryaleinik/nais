public class FamilyAppSubmitController extends FamilyAppCompController
{

    public FamilyAppSubmitController() {}

    public Help_Configuration__c currentHelpConfig
    {
        get
        {
            Help_Configuration__c tempHelpConfig = GlobalVariables.getCurrentTermsAndConditionsHelpRecord();
            if (tempHelpConfig != null)
            {
                return tempHelpConfig;
            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Family_App_Submit_No_Terms_and_Conditions));
                return null;
            }
        }
        set;
    }

    public Boolean areSubscribedSchools
    {
        get
        {
            if (areSubscribedSchools == null)
            {
                areSubscribedSchools = ApplicationUtils.areSubscribedSchools(pfs.Id);
            }
            return areSubscribedSchools;
        }
        set;
    }

    public Boolean areOpenDeadlines 
    {
        get
        {
            if (areOpenDeadlines == null) 
            {
                areOpenDeadlines = ApplicationUtils.areAvailableSchools(pfs);
            }
            return areOpenDeadlines;
        }
        set;
    }
    //NAIS-2032 Start
    public Boolean areSchoolSetupComplete 
    {
        get
        {
            if (areSchoolSetupComplete == null) 
            {
                areSchoolSetupComplete = ApplicationUtils.areSchoolSetupComplete(pfs);
            }
            return areSchoolSetupComplete;
        }
        set;
    }

    public PageReference submitAndPay() 
    {
        Savepoint sp = Database.setSavepoint();
        try {
            PageReference pr;
            Boolean waiverApplied = false;

            waiverApplied = ApplicationFeeWaiverService.Instance.applyAvailableWaivers(pfs);

            System.debug('Waiver Applied >>>> ' + waiverApplied);

            // check if the user is an admin
            Boolean isAdminOrDatabankTransaction = false;
            User u = GlobalVariables.getCurrentUser();
            // NAIS-1688 [dp] For Databank Paper Entry
            if ((GlobalVariables.isSysAdminUser(u)) || GlobalVariables.isCallCenterUser(u) || GlobalVariables.isDatabankUser(u)) {
                isAdminOrDatabankTransaction = true;
            }

            pfs.PFS_Status__c = 'Application Submitted';
            update pfs;

            if (waiverApplied) {
                pr = Page.FamilyPaymentFeeWaiverApplied;
            } else {
                pr = Page.FamilyPayment;
                // [SL] NAIS-1078 - pass in academic year instead of id
                pr.getParameters().put('academicyearid', GlobalVariables.getAcademicYearByName(pfs.Academic_Year_Picklist__c).Id);

                if (isAdminOrDatabankTransaction) {
                    pr.getParameters().put('id', pfs.Id);
                }
            }
            return pr;

        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.PfsSubmitErrorGeneral));
            Database.rollback(sp);
            return null;
        }
    }

    public PageReference cancel() 
    {
        return Page.FamilyDashboard;
    }
}