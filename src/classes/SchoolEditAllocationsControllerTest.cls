@isTest
private class SchoolEditAllocationsControllerTest {

    private static User currentUser = UserSelector.newInstance().selectById(new Set<Id>{UserInfo.getUserId()})[0];
    private static User schoolPortalUser;
    private static Academic_Year__c currentAcademicYear;
    private static Contact student1;
    private static Account school;

    @isTest
    private static void case1() {
        initTestData();

        Student_Folder__c tmpFolder;
        List<Budget_Allocation__c> tmpBudgetGroup;
        
        List<Student_Folder__c> studentFolders = StudentFolderSelector.newInstance().selectByStudentId_schoolId_academicYearName(
            new Set<Id>{student1.Id}, new Set<Id>{school.Id}, new Set<String>{currentAcademicYear.Name});
        System.assert(!studentFolders.isEmpty());
        System.assertEquals(1, studentFolders.size());
        Student_Folder__c studentFolder = studentFolders[0];

        Test.startTest();
            String year = [Select Id from Academic_Year__c where Name=:studentFolder.Academic_Year_Picklist__c limit 1].Id;
            Budget_Group__c budgetGroup1 = new Budget_Group__c(
                Name=school.Name+' - 01', Academic_Year__c=year, School__c=studentFolder.School__c);
            Budget_Group__c budgetGroup2 = new Budget_Group__c(
                Name=school.Name+' - 02', Academic_Year__c=year, School__c=studentFolder.School__c);
            insert new list<Budget_Group__c>{budgetGroup1, budgetGroup2};

            System.runAs(schoolPortalUser) {
                PageReference pageRef = Page.SchoolEditAllocations;
                pageRef.getParameters().put('id', String.ValueOf(studentFolder.Id));
                Test.setCurrentPage(pageRef);

                System.assertEquals(budgetGroup1.School__c,studentFolder.School__c);
                SchoolEditAllocationsController controller = new SchoolEditAllocationsController();
                controller.getUseBudgetOptions();
                controller.getBudgetGroupOptions();
                controller.useBudgets = 'No';
                controller.folder.Grant_Awarded__c = 8;
                controller.folder.Other_Aid__c = 10;
                controller.folder.Tuition_Remission__c = 5;
                controller.saveAction();
                tmpFolder = [Select Use_Budget_Groups__c, Other_Aid__c, Tuition_Remission__c, Grant_Awarded__c
                            from Student_Folder__c where Id=:studentFolder.Id limit 1];
                System.assertEquals(8, tmpFolder.Grant_Awarded__c);
                System.assertEquals(10, tmpFolder.Other_Aid__c);
                System.assertEquals(5, tmpFolder.Tuition_Remission__c);

                controller.useBudgets = 'Yes';
                controller.newBudgetInit();
                controller.newBudgetAlloc.Amount_Allocated__c = 3;
                controller.newBudgetAlloc.Budget_Group__c = budgetGroup1.Id;
                controller.addBudgetAlloc();
                controller.newBudgetInit();
                controller.newBudgetAlloc.Amount_Allocated__c = 2;
                controller.newBudgetAlloc.Budget_Group__c = budgetGroup1.Id;
                controller.addBudgetAlloc();
                controller.saveAction();
                tmpFolder = [Select Use_Budget_Groups__c, Other_Aid__c, Tuition_Remission__c, Grant_Awarded__c
                            from Student_Folder__c where Id=:studentFolder.Id limit 1];
                System.assertEquals(5, tmpFolder.Grant_Awarded__c);
                tmpBudgetGroup = [SELECT Id FROM Budget_Allocation__c where Student_Folder__c=:studentFolder.Id];
                System.assertEquals(2, tmpBudgetGroup.size());
            }
        Test.stopTest();
    }

    @isTest
    private static void case2() {
        initTestData();

        Student_Folder__c tmpFolder;
        List<Budget_Allocation__c> tmpBudgetGroup;
        
        List<Student_Folder__c> studentFolders = StudentFolderSelector.newInstance().selectByStudentId_schoolId_academicYearName(
            new Set<Id>{student1.Id}, new Set<Id>{school.Id}, new Set<String>{currentAcademicYear.Name});
        System.assert(!studentFolders.isEmpty());
        System.assertEquals(1, studentFolders.size());
        Student_Folder__c studentFolder = studentFolders[0];

        Test.startTest();
            String year = [Select Id from Academic_Year__c where Name=:studentFolder.Academic_Year_Picklist__c limit 1].Id;
            Budget_Group__c budgetGroup1 = new Budget_Group__c(Name=school.Name+' - 01', Academic_Year__c=year, School__c=studentFolder.School__c);
            Budget_Group__c budgetGroup2 = new Budget_Group__c(Name=school.Name+' - 02', Academic_Year__c=year, School__c=studentFolder.School__c);
            insert new list<Budget_Group__c>{budgetGroup1, budgetGroup2};

            System.runAs(schoolPortalUser) {
                PageReference pageRef = Page.SchoolEditAllocations;
                pageRef.getParameters().put('id', String.ValueOf(studentFolder.Id));
                Test.setCurrentPage(pageRef);

                SchoolEditAllocationsController controller = new SchoolEditAllocationsController();
                controller.getUseBudgetOptions();
                controller.getBudgetGroupOptions();
                controller.useBudgets = 'No';
                controller.folder.Grant_Awarded__c = 8;
                controller.folder.Other_Aid__c = 10;
                controller.folder.Tuition_Remission__c = 5;
                controller.saveAction();
                tmpFolder = [Select Use_Budget_Groups__c, Other_Aid__c, Tuition_Remission__c, Grant_Awarded__c
                            from Student_Folder__c where Id=:studentFolder.Id limit 1];
                System.assertEquals(8, tmpFolder.Grant_Awarded__c);
                System.assertEquals(10, tmpFolder.Other_Aid__c);
                System.assertEquals(5, tmpFolder.Tuition_Remission__c);

                controller.useBudgets = 'Yes';
                controller.newBudgetInit();
                controller.newBudgetAlloc.Amount_Allocated__c = null;
                controller.addBudgetAlloc();
                System.assertEquals(true, controller.amountErrorMsg);

                controller.useBudgets = 'No';
                controller.folder.Grant_Awarded__c = 5;
                controller.saveAction();
                System.assertEquals(5, [Select Grant_Awarded__c from Student_Folder__c
                                        where Id=:controller.folder.Id limit 1].Grant_Awarded__c);
                list<Budget_Allocation__c> allocations = [Select Id from Budget_Allocation__c
                                        where Student_Folder__c=:controller.folder.Id];
                System.assertEquals(allocations.size(), 0);
            }
        Test.stopTest();
    }

    @isTest
    private static void saveAction_amountAwardedStillPopulated_expectError() {
        initTestData();

        Student_Folder__c tmpFolder;
        List<Budget_Allocation__c> tmpBudgetGroup;
        
        List<Student_Folder__c> studentFolders = StudentFolderSelector.newInstance().selectByStudentId_schoolId_academicYearName(
            new Set<Id>{student1.Id}, new Set<Id>{school.Id}, new Set<String>{currentAcademicYear.Name});
        System.assert(!studentFolders.isEmpty());
        System.assertEquals(1, studentFolders.size());
        Student_Folder__c studentFolder = studentFolders[0];

        Test.startTest();
            String year = [Select Id from Academic_Year__c where Name=:studentFolder.Academic_Year_Picklist__c limit 1].Id;
            Budget_Group__c budgetGroup1 = new Budget_Group__c(
                Name=school.Name+' - 01', Academic_Year__c=year, School__c=studentFolder.School__c);
            Budget_Group__c budgetGroup2 = new Budget_Group__c(
                Name=school.Name+' - 02', Academic_Year__c=year, School__c=studentFolder.School__c);
            insert new list<Budget_Group__c>{budgetGroup1, budgetGroup2};

            System.runAs(schoolPortalUser) {
                PageReference pageRef = Page.SchoolEditAllocations;
                pageRef.getParameters().put('id', String.ValueOf(studentFolder.Id));
                Test.setCurrentPage(pageRef);

                // new context
                SchoolEditAllocationsController controller = new SchoolEditAllocationsController();

                // Test the validation of the budget allocation in case the user forgot to add the aid award.
                controller.newBudgetAlloc.Amount_Allocated__c = 10;
                controller.saveAction();
                System.assert(controller.amountErrorMsg, 'Expected there to be an error.');
                System.assertEquals(Label.AwardAmountNotSaved, controller.AmountErrorMessage,
                        'Expected the given error message.');
            }
        Test.stopTest();
    }

    private static void initTestData() {
        Account family = AccountTestData.Instance.asFamily().create();
        school = AccountTestData.Instance.asSchool().create();
        insert new List<Account>{family, school};

        Contact parentA = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .forAccount(school.Id)
            .asSchoolStaff().create();
        student1 = ContactTestData.Instance
            .asStudent().create();
        insert new List<Contact>{parentA, schoolStaff, student1};
        
        System.runAs(currentUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);

        currentAcademicYear = AcademicYearTestData.Instance.insertAcademicYear();

        System.runAs(schoolPortalUser) {
            PFS__c pfs1 = PfsTestData.Instance
                .asSubmitted()
                .forParentA(parentA.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<PFS__c> {pfs1});

            Applicant__c applicant1 = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student1.Id).create();
            Dml.WithoutSharing.insertObjects(new List<Applicant__c> {applicant1});

            String dayBoarding = 'Day';
            String folderSourceSchoolInitiated = 'School-Initiated';
            String folderStatusAwardApproved = 'Award Approved';
            String useBudgetGroups = 'No';
            String newReturning = 'New';
            Integer grantAwarded3000 = 3000;
            Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
                .forSchoolId(school.Id)
                .forNewReturning(newReturning)
                .forStudentId(student1.Id)
                .forDayBoarding(dayBoarding)
                .forFolderSource(folderSourceSchoolInitiated)
                .forFolderStatus(folderStatusAwardApproved)
                .forUseBudgetGroups(useBudgetGroups)
                .forGrantAwarded(grantAwarded3000)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<Student_Folder__c> {studentFolder1});

            String fifthGrade = '5';
            Decimal salaryWagesParentA = 60000;
            School_PFS_Assignment__c spfsa1A = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSalaryWagesParentA(salaryWagesParentA)
                .forSchoolId(school.Id).create();
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> {spfsa1A});
        }

        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);
    }
}