public class VerificationAction
{

    public static final String PROCESSED = 'Processed';
    @TestVisible
    private static final String PENDING = 'Pending';
    @TestVisible
    private static final String COMPLETE_NO_MATCH = 'Complete - No Match';
    @TestVisible
    private static final String COMPLETE_MATCH = 'Complete - Matched';

    public static final List<String> X1040_DOCUMENT_TYPES = new List<String>{
        '1040 with all filed schedules and attachments', '1040', '1040A', '1040EZ'
    };

    @TestVisible
    private static final String DOCUMENT_TYPE_W2 = 'W2';
    @TestVisible
    private static final String FAMILY_DOC_W2_FIELD = 'W2_WAGES__c';
    @TestVisible
    private static final String VERIFICATION_X1040_WAGES_FIELD = 'X1040_Wages__c';
    @TestVisible
    private static final String VERIFICATION_X1040_WAGES_FIELD_B = 'X1040_Wages_B__c';
    @TestVisible
    private static final String VERIFICATION_X1040_WAGES_STATUS_FIELD = 'X1040_Wages_Status__c';
    @testVisible
    private static final String PY_PREFIX = 'PY_';

    private static final String DOCUMENT_TYPE_SCH_A = 'Tax Schedule A';
    @TestVisible
    private static final String DOCUMENT_TYPE_SCH_C = 'Tax Schedule C';
    @TestVisible
    private static final String FAMILY_DOC_SCH_C_FIELD = 'Sch_C_Business_Income__c';
    @TestVisible
    private static final String VERIFICATION_X1040_BUSINESS_INCOME_LOSS_FIELD = 'X1040_Business_Income_Loss__c';
    @TestVisible
    private static final String VERIFICATION_X1040_BUSINESS_INCOME_LOSS_B_FIELD = 'X1040_Business_Income_Loss_B__c';
    @TestVisible
    private static final String VERIFICATION_X1040_BUSINESS_INCOME_LOSS_STATUS_FIELD = 'X1040_Business_Income_Loss_Status__c';

    @TestVisible
    private static final String DOCUMENT_TYPE_SCH_E = 'Tax Schedule E';
    @TestVisible
    private static final String FAMILY_DOC_SCH_E_FIELD = 'Sch_E_Total_Rent_Restate_Royalty_Income__c';
    @TestVisible
    private static final String VERIFICATION_RENTAL_ESTATE_TRUSTS_FIELD = 'Rental_Real_Estate_Trusts_etc__c';
    @TestVisible
    private static final String VERIFICATION_RENTAL_ESTATE_TRUSTS_FIELD_B = 'Rental_Real_Estate_Trusts_etc_B__c';
    @TestVisible
    private static final String VERIFICATION_NET_PROFIT_LOSS_RENTAL_REAL_ESTATE_TRUSTS_ETC_STATUS_FIELD = 'Rental_Real_Estate_Trusts_Etc_Status__c';

    @TestVisible
    private static final String DOCUMENT_TYPE_SCH_F = 'Tax Schedule F';
    @TestVisible
    private static final String FAMILY_DOC_SCH_F_FIELD = 'Sch_F_Farm_Income__c';
    @TestVisible
    private static final String VERIFICATION_NET_PROFIT_LOSS_FARM_FIELD = 'Net_Profit_Loss_Farm__c';
    private static final String VERIFICATION_NET_PROFIT_LOSS_FARM_FIELD_B = 'Net_Profit_Loss_Farm_B__c';
    private static final String VERIFICATION_NET_PROFIT_LOSS_FARM_STATUS_FIELD = 'Net_Profit_Loss_Farm_Status__c';

    @TestVisible
    private static final String VERIFICATION_PARENT_A_VERIFIED = 'Parent_A_1040_Verified__c';
    @TestVisible
    private static final String VERIFICATION_PARENT_B_VERIFIED = 'Parent_B_1040_Verified__c';

    private static final String CALC_VERIFICATION_STATUS_FOR_OPEN_ACADEMIC_YEAR = 'Calc_Verification_Status_For_Open_AY__c';
    public static final String EXPECTED_NUMBER_OF_1040S = 'Expected_Number_of_1040s__c';

    public static final List<String> DOCUMENT_TYPES_TO_PROCESS =
        new List<String>{DOCUMENT_TYPE_W2, DOCUMENT_TYPE_SCH_C, DOCUMENT_TYPE_SCH_E, DOCUMENT_TYPE_SCH_F};

    public static final Map<String, String> familyDocumentFieldsByDocumentType = new Map<String, String>{
        DOCUMENT_TYPE_W2 => FAMILY_DOC_W2_FIELD,
        DOCUMENT_TYPE_SCH_C => FAMILY_DOC_SCH_C_FIELD,
        DOCUMENT_TYPE_SCH_E => FAMILY_DOC_SCH_E_FIELD,
        DOCUMENT_TYPE_SCH_F => FAMILY_DOC_SCH_F_FIELD
    };

    private static final Map<String, String> statusFieldByDocumentType = new Map<String, String>{
        DOCUMENT_TYPE_W2 => VERIFICATION_X1040_WAGES_STATUS_FIELD,
        DOCUMENT_TYPE_SCH_C => VERIFICATION_X1040_BUSINESS_INCOME_LOSS_STATUS_FIELD,
        DOCUMENT_TYPE_SCH_F => VERIFICATION_NET_PROFIT_LOSS_FARM_STATUS_FIELD,
        DOCUMENT_TYPE_SCH_E => VERIFICATION_NET_PROFIT_LOSS_RENTAL_REAL_ESTATE_TRUSTS_ETC_STATUS_FIELD
    };

    private static final Map<String, String> pyStatusFieldByDocumentType = new Map<String, String>{
        DOCUMENT_TYPE_W2 => PY_PREFIX + VERIFICATION_X1040_WAGES_STATUS_FIELD,
        DOCUMENT_TYPE_SCH_C => PY_PREFIX + VERIFICATION_X1040_BUSINESS_INCOME_LOSS_STATUS_FIELD,
        DOCUMENT_TYPE_SCH_F => PY_PREFIX + VERIFICATION_NET_PROFIT_LOSS_FARM_STATUS_FIELD,
        DOCUMENT_TYPE_SCH_E => PY_PREFIX + VERIFICATION_NET_PROFIT_LOSS_RENTAL_REAL_ESTATE_TRUSTS_ETC_STATUS_FIELD
    };

    private static final Map<String, List<String>> verificationFieldsByDocumentType = new Map<String, List<String>>{
        DOCUMENT_TYPE_W2 => new List<String>{VERIFICATION_X1040_WAGES_FIELD, VERIFICATION_X1040_WAGES_FIELD_B},
        DOCUMENT_TYPE_SCH_C => new List<String>{VERIFICATION_X1040_BUSINESS_INCOME_LOSS_FIELD, VERIFICATION_X1040_BUSINESS_INCOME_LOSS_B_FIELD},
        DOCUMENT_TYPE_SCH_E => new List<String>{VERIFICATION_RENTAL_ESTATE_TRUSTS_FIELD, VERIFICATION_RENTAL_ESTATE_TRUSTS_FIELD_B},
        DOCUMENT_TYPE_SCH_F => new List<String>{VERIFICATION_NET_PROFIT_LOSS_FARM_FIELD, VERIFICATION_NET_PROFIT_LOSS_FARM_FIELD_B}
    };

    private static final Map<String, List<String>> pyVerificationFieldsByDocumentType = new Map<String, List<String>>{
        DOCUMENT_TYPE_W2 => new List<String>{PY_PREFIX + VERIFICATION_X1040_WAGES_FIELD, PY_PREFIX + VERIFICATION_X1040_WAGES_FIELD_B},
        DOCUMENT_TYPE_SCH_C => new List<String>{PY_PREFIX + VERIFICATION_X1040_BUSINESS_INCOME_LOSS_FIELD, PY_PREFIX + VERIFICATION_X1040_BUSINESS_INCOME_LOSS_B_FIELD},
        DOCUMENT_TYPE_SCH_E => new List<String>{PY_PREFIX + VERIFICATION_RENTAL_ESTATE_TRUSTS_FIELD, PY_PREFIX + VERIFICATION_RENTAL_ESTATE_TRUSTS_FIELD_B},
        DOCUMENT_TYPE_SCH_F => new List<String>{PY_PREFIX + VERIFICATION_NET_PROFIT_LOSS_FARM_FIELD, PY_PREFIX + VERIFICATION_NET_PROFIT_LOSS_FARM_FIELD_B}
    };

    private static final List<String> verificationFieldsToSelect = new List<String>{
        VERIFICATION_X1040_WAGES_FIELD, VERIFICATION_X1040_WAGES_FIELD_B, VERIFICATION_X1040_WAGES_STATUS_FIELD,
        VERIFICATION_X1040_BUSINESS_INCOME_LOSS_FIELD, VERIFICATION_X1040_BUSINESS_INCOME_LOSS_B_FIELD,
        VERIFICATION_X1040_BUSINESS_INCOME_LOSS_STATUS_FIELD,
        VERIFICATION_RENTAL_ESTATE_TRUSTS_FIELD, VERIFICATION_RENTAL_ESTATE_TRUSTS_FIELD_B,
        VERIFICATION_NET_PROFIT_LOSS_RENTAL_REAL_ESTATE_TRUSTS_ETC_STATUS_FIELD,
        VERIFICATION_NET_PROFIT_LOSS_FARM_FIELD, VERIFICATION_NET_PROFIT_LOSS_FARM_FIELD_B,
        VERIFICATION_NET_PROFIT_LOSS_FARM_STATUS_FIELD,
        VERIFICATION_PARENT_A_VERIFIED, VERIFICATION_PARENT_B_VERIFIED,
        EXPECTED_NUMBER_OF_1040S,

        PY_PREFIX + VERIFICATION_X1040_WAGES_FIELD, PY_PREFIX + VERIFICATION_X1040_WAGES_FIELD_B,
        PY_PREFIX + VERIFICATION_X1040_BUSINESS_INCOME_LOSS_FIELD, PY_PREFIX + VERIFICATION_X1040_BUSINESS_INCOME_LOSS_B_FIELD,
        PY_PREFIX + VERIFICATION_RENTAL_ESTATE_TRUSTS_FIELD, PY_PREFIX + VERIFICATION_RENTAL_ESTATE_TRUSTS_FIELD_B,
        PY_PREFIX + VERIFICATION_NET_PROFIT_LOSS_FARM_FIELD, PY_PREFIX + VERIFICATION_NET_PROFIT_LOSS_FARM_FIELD_B,
        PY_PREFIX + EXPECTED_NUMBER_OF_1040S, PY_PREFIX + VERIFICATION_PARENT_B_VERIFIED, PY_PREFIX + VERIFICATION_PARENT_A_VERIFIED,
        PY_PREFIX + VERIFICATION_X1040_WAGES_STATUS_FIELD,
        PY_PREFIX + VERIFICATION_X1040_BUSINESS_INCOME_LOSS_STATUS_FIELD,
        PY_PREFIX + VERIFICATION_NET_PROFIT_LOSS_FARM_STATUS_FIELD,
        PY_PREFIX + VERIFICATION_NET_PROFIT_LOSS_RENTAL_REAL_ESTATE_TRUSTS_ETC_STATUS_FIELD
    };

    @TestVisible
    private static Map<String, Set<Family_Document__c>> familyDocumentsByHouseholdId;
    @TestVisible
    private static Map<String, Verification__c> verificationByHouseholdId;
    @TestVisible
    private static Map<String, List<Verification__c>> allVerificationByHouseholdId;
    @TestVisible
    private static List<Verification__c> verificationsWithModification = new List<Verification__c>();

    /* [SL] NAIS-1652: recalc EFC if depreciation fields change  */
    public static void recalculateEFCs(Set<Id> verificationIds) {
        // get the associated PFS Ids
        Set<Id> pfsIds = new Set<Id>();
        for (PFS__c pfs : [SELECT Id FROM PFS__c WHERE Verification__c IN :verificationIds]) {
            pfsIds.add(pfs.Id);
        }

        // update the school PFS Assignments
        List<School_PFS_Assignment__c> spasToUpdate = new List<School_PFS_Assignment__c>();
        for (School_PFS_Assignment__c spa : [
                SELECT Id, Revision_EFC_Calc_Status__c, PFS_Status__c, Payment_Status__c, Visible_to_Schools_Until__c
                FROM School_PFS_Assignment__c
                WHERE Applicant__r.PFS__c IN :pfsIds
                AND Add_Deprec_Home_Bus_Expense__c = 'Yes'
                AND Family_May_Submit_Updates__c = 'Yes'
        ]) {
            if (EfcCalculatorAction.isStatusRecalculable(spa.PFS_Status__c, spa.Payment_Status__c, spa.Visible_to_Schools_Until__c)) {
                spa.Revision_EFC_Calc_Status__c = EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE;
                spasToUpdate.add(spa);
            }
        }
        if (!spasToUpdate.isEmpty()) {
            update spasToUpdate;
        }
    }
    
    public static void setVerficationStatusValues(Set<Family_Document__c> familyDocuments, Set<Id> householdIds)
    {
        if (FeatureToggles.isEnabled(CALC_VERIFICATION_STATUS_FOR_OPEN_ACADEMIC_YEAR))
        {
            setVerficationStatusValuesProcessNew(familyDocuments, householdIds);
        }
        else
        {
            setVerficationStatusValuesProcessOld(familyDocuments, householdIds);
        }
    }
    
    /**
     *  SFP-640 [8.30.16] Post Processing actions on Verification records determines the status value [jB]
     *          Some fields used in this calculation are assigned from SpringCM to Fam. Documents
     *          Some assigned to verification, so this procedure needs to run after both
     *
     *          Fields: PENDING - COMPLETE NO MATCH - COMPLETE MATCH
     *          W2, X1040_Wages_Status__c, SUM(FamilyDocument__c.W2_WAGES__c) == Verification__c.X1040_Wages__c + Verification.X1040_Wages_B__c
     *          Sch. C., X1040_Business_Income_Loss_Status__c, SUM(FamilyDocument__c.Sch_C_Business_Income__c) == Verification__c.X1040_Business_Income__c + Verification__c.X1040_Business_Income_B__c
     *          Sch. E., Rental_Real_Estate_Trusts_Etc_Status__c, SUM(FamilyDocument__c.Sch_E_Total_Rent_Restate_Royalty_Income__c) == Verification__c.Rental_Real_Estate_Trusts_etc__c + Verification__c.Rental_Real_Estate_Trusts_etc_B__c
     *          Sch. F., Net_Profit_Loss_Farm_Status__c, SUM(FamilyDocument__c.Sch_F_Farm_Income__c) == Verification__c.Net_Profit_Loss_Farm__c + Verification__c.Net_Profit_Loss_Farm_B__c
     *
     *          Triggers: Verification__c, After update
     *                    FamilyDocument__c, After update, After Delete
     */
    private static void setVerficationStatusValuesProcessOld(Set<Family_Document__c> familyDocuments, Set<Id> householdIds)
    {
        System.debug('setting Verification kickoff: ' + familyDocuments);
        try
        {
            Map<String, PFS__c> pfsByVerificationId = new Map<String, PFS__c>();
            Map<String, String> householdByVerificationId = new Map<String, String>();
            verificationByHouseholdId = new Map<String, Verification__c>();
            familyDocumentsByHouseholdId = new Map<String, Set<Family_Document__c>>();
            Set<Id> verificationIds = new Set<Id>();
            // Maybe triggered from document delete, in which case processed directly on Household ids
            if (familyDocuments != null && !familyDocuments.isEmpty()) {
                householdIds = getHouseholdIdsFromFamilyDocuments(familyDocuments);
            }

            if (householdIds == null || householdIds.isEmpty()) {
                throw new VerificationException('No household ids returned for family documents');
            }

            // get all document ids for later processing
            Map<String, Family_Document__c> documentsByAcademicYear = new Map<String, Family_Document__c>();

            Set<String> familyDocumentTypes = new Set<String>(familyDocumentFieldsByDocumentType.keyset());
            familyDocumentTypes.addAll(X1040_DOCUMENT_TYPES);

            Set<String> academicYearPicklistvalues = new Set<String>();
            for (Family_Document__c fd : FamilyDocumentsSelector.newInstance().selectByHouseholdId_docType_docStatus(
                                            householdIds, familyDocumentTypes, PROCESSED))
            {
                if (familyDocumentsByHouseholdId.get(fd.Household__c) == null)
                {
                    familyDocumentsByHouseholdId.put(fd.Household__c, new Set<Family_Document__c>());
                }

                familyDocumentsByHouseholdId.get(fd.Household__c).add(fd);
                documentsByAcademicYear.put(fd.Document_Year__c, fd);
                
                if (!academicYearPicklistvalues.contains(fd.Academic_Year_Picklist__c))
                {
                    academicYearPicklistvalues.add(fd.Academic_Year_Picklist__c);
                }
            }

            // Get contacts by households
            Set<String> householdContactIds = new Set<String>();
            Map<String, String> contactHouseholdIds = new Map<String, String>();

            for (Household__c household : [SELECT Id, (SELECT Id FROM Contacts__r) FROM Household__c WHERE Id IN :householdIds])
            {
                List<Contact> householdContacts = new List<Contact>(household.Contacts__r);
                for (Contact c : householdContacts)
                {
                    householdContactIds.add(c.Id);
                    contactHouseholdIds.put(c.Id, household.Id);
                }
            }

            academicYearPicklistvalues.addAll(documentsByAcademicYear.keySet());
            // select PFS
            List<String> verificationFields = new List<String>();
            for (String verField : verificationFieldsToSelect) {
                verificationFields.add('Verification__r.' + verField);
            }

            // TODO SFP-1185 Before querying, remove any Academic Year Picklist values that aren't still open.
            String pfsQuery = 'SELECT Verification__c, Parent_A__c, Parent_B__c, Academic_Year_Picklist__c, ' +
                    String.join(verificationFields, ',') +
                    ' FROM PFS__c WHERE Academic_Year_Picklist__c IN :academicYearPicklistvalues ' +
                    'AND (Parent_A__c IN :householdContactIds OR Parent_B__c IN :householdContactIds) ORDER BY Academic_Year_Picklist__c ASC';
            for (PFS__c pfs : Database.query(pfsQuery))
            {
                verificationIds.add(pfs.Verification__c);
                pfsByVerificationId.put(pfs.Verification__c, pfs);
                // TODO SFP-1185 Be able to store multiple verification records per household so we can process all verification records for PFSs that are for an open academic year.
                verificationByHouseholdId.put(contactHouseHoldIds.get(pfs.Parent_A__c), (Verification__c) pfs.getSobject('Verification__r'));
            }
            // filter out previous year docs
            for (String householdId : verificationByHouseholdId.keySet())
            {
                Set<Family_Document__c> allDocs = familyDocumentsByHouseholdId.get(householdId);

                // SFP-1109: Gracefully handle null sets of docs and log the occurrence of this scenario.
                // First, check to see if we are logging the households that have null family docs in this context.
                // If we are and the set of docs is null, record the household Id and move on to the next household.
                // The households without docs will be logged in a new Custom Debug record at the end of the loop.
                // If this toggle is not enabled, we will simply allow the null pointer exception to fire as it was before.
                // SFP-1185: We no longer log this in the custom debug records. Now we just continue on to the next household
                if (shouldLogNullFamilyDocsForHouseholds(allDocs))
                {
                    continue;
                }

                for (Family_Document__c fd : allDocs)
                {
                    // Sometimes we will see null pointer exceptions when a household has old family documents with null Academic_Year_Picklist__c values.
                    // To resolve these errors, just find the household and populate the Academic_Year_Picklist__c field based on the PFS Number.
                    // The correct value can be determined using the first two digits of the PFS Number.
                    // The first two digits correspond to the second year of the academic year that we should use.
                    // For example, for PFS Number 18-1234567890, we want to select 2017-2018 for the Academic_Year_Picklist__c.
                    if (GlobalVariables.getDocYearStringFromAcadYearName(fd.Academic_Year_Picklist__c) != fd.Document_Year__c)
                    {
                        familyDocumentsByHouseholdId.get(householdId).remove(fd);
                    }
                }
            }
            
            for (String householdId : verificationByHouseholdId.keySet()) {
                String docYear = GlobalVariables.getDocYearStringFromAcadYearName(pfsByVerificationId.get(verificationByHouseholdId.get(householdId).Id).Academic_Year_Picklist__c);

                // Grab the family docs for this household. If we are logging cases where the docs are null for this
                // household, skip to the next household to avoid an invalid scenario that has already been logged in the previous loop.
                Set<Family_Document__c> allDocs = familyDocumentsByHouseholdId.get(householdId);
                if (shouldLogNullFamilyDocsForHouseholds(allDocs)) {
                    continue;
                }
                checkDocumentCompleteness(allDocs, verificationByHouseholdId.get(householdId), docYear);
            }

            // update verification records
            if (!verificationsWithModification.isEmpty())
            {
                update verificationsWithModification;
                verificationsWithModification.clear();
            }
        }
        catch (VerificationException ve)
        {
            throw(ve);
            return;
        }
    }

    private static void setVerficationStatusValuesProcessNew(Set<Family_Document__c> familyDocuments, Set<Id> householdIds)
    {
        try
        {
            //1. Variables to be used.
            Map<String, PFS__c> pfsByVerificationId = new Map<String, PFS__c>();
            Map<Id, String> householdByVerificationId = new Map<Id, String>();
            familyDocumentsByHouseholdId = new Map<String, Set<Family_Document__c>>();
            Map<Id, String> contactHouseholdIds = new Map<Id, String>();
            Set<String> householdContactIds = new Set<String>();
            Set<Id> verificationIds = new Set<Id>();
            Id tmpHouseholdId;
            
            //2. We get the household ids for the given family_Document records.
            if (familyDocuments != null && !familyDocuments.isEmpty()) {
                
                //2.1 Maybe triggered from document delete, in which case processed directly on Household ids.
                householdIds = getHouseholdIdsFromFamilyDocuments(familyDocuments);
            }
            
            if (householdIds == null || householdIds.isEmpty()) {
                throw new VerificationException('No household ids returned for family documents');
            }

            //3. Get all document ids for later processing.
            Set<String> familyDocumentTypes = new Set<String>(familyDocumentFieldsByDocumentType.keyset());
            familyDocumentTypes.addAll(X1040_DOCUMENT_TYPES);
            
            //4. Ge all the open academic years available to the user instead of just the latest one.
            List<Academic_Year__c> availableAcademicYears = AcademicYearService.Instance.getAvailableAcademicYears();
            List<String> allOpenAcademicYear = new List<String>();
            for(Academic_Year__c a : availableAcademicYears) {
                allOpenAcademicYear.add(a.Name);
            }

            for (Family_Document__c fd : FamilyDocumentsSelector.newInstance().selectByHouseholdId_docType_docStatus(
                                            householdIds, familyDocumentTypes, PROCESSED)) {
                if (familyDocumentsByHouseholdId.get(fd.Household__c) == null) {
                    familyDocumentsByHouseholdId.put(fd.Household__c, new Set<Family_Document__c>());
                }

                familyDocumentsByHouseholdId.get(fd.Household__c).add(fd);

            }

            //5. Get contacts by households.
            for (Household__c household : [SELECT Id, (SELECT Id FROM Contacts__r) FROM Household__c WHERE Id IN :householdIds]) {
                
                List<Contact> householdContacts = new List<Contact>(household.Contacts__r);
                for (Contact c : householdContacts) {
                    householdContactIds.add(c.Id);
                    contactHouseholdIds.put(c.Id, household.Id);
                }
            }

            //6. Get the fields to be query from Verification__c object.
            List<String> verificationFields = new List<String>();
            for (String verField : verificationFieldsToSelect) {
                verificationFields.add('Verification__r.' + verField);
            }
            
            //7. Query the PFSs and its verification records.            
            allVerificationByHouseholdId = new Map<String, List<Verification__c>>();
            List<Verification__c> tmpAllVerification = new List<Verification__c>();
            
            String pfsQuery = 'SELECT Verification__c, Parent_A__c, Parent_B__c, Academic_Year_Picklist__c, ' +
                              String.join(verificationFields, ',') + ' ' +
                              'FROM PFS__c ' +
                              'WHERE Academic_Year_Picklist__c IN :allOpenAcademicYear ' +
                              'AND (Parent_A__c IN :householdContactIds OR Parent_B__c IN :householdContactIds) ' +
                              'ORDER BY Academic_Year_Picklist__c ASC';

            for (PFS__c pfs : Database.query(pfsQuery)) {
                //7.1 Store multiple verification records per household so we can process all verification records for PFSs 
                //that are for an open academic year.
                if(contactHouseHoldIds.containsKey(pfs.Parent_A__c) || contactHouseHoldIds.containsKey(pfs.Parent_B__c)) {
                    
                    tmpHouseholdId = contactHouseHoldIds.containsKey(pfs.Parent_A__c) ? 
                                     contactHouseHoldIds.get(pfs.Parent_A__c)
                                   : contactHouseHoldIds.get(pfs.Parent_B__c);
                   
                    verificationIds.add(pfs.Verification__c);
                    pfsByVerificationId.put(pfs.Verification__c, pfs);
                    
                    tmpAllVerification = ( !allVerificationByHouseholdId.containsKey(tmpHouseholdId) 
                                       ? tmpAllVerification = new List<Verification__c>()
                                       : allVerificationByHouseholdId.get(tmpHouseholdId) );
                    
                    tmpAllVerification.add((Verification__c) pfs.getSobject('Verification__r'));
                    
                    allVerificationByHouseholdId.put(tmpHouseholdId, tmpAllVerification);
                }
            }
            //8. Filter out previous year Family Documents.
            for (String householdId : allVerificationByHouseholdId.keySet()) {
                Set<Family_Document__c> allDocs = familyDocumentsByHouseholdId.get(householdId);

                // SFP-1109: Gracefully handle null sets of docs and log the occurrence of this scenario.
                // First, check to see if we are logging the households that have null family docs in this context.
                // If we are and the set of docs is null, record the household Id and move on to the next household.
                // The households without docs will be logged in a new Custom Debug record at the end of the loop.
                // If this toggle is not enabled, we will simply allow the null pointer exception to fire as it was before.
                // SFP-1185: We no longer log this in the custom debug records. Now we just continue on to the next household
                if (shouldLogNullFamilyDocsForHouseholds(allDocs)) {
                    continue;
                }

                 for (Family_Document__c fd : allDocs)
                 {
                        // Sometimes we will see null pointer exceptions when a household has old family documents with null Academic_Year_Picklist__c values.
                        // To resolve these errors, just find the household and populate the Academic_Year_Picklist__c field based on the PFS Number.
                        // The correct value can be determined using the first two digits of the PFS Number.
                        // The first two digits correspond to the second year of the academic year that we should use.
                        // For example, for PFS Number 18-1234567890, we want to select 2017-2018 for the Academic_Year_Picklist__c.
                        if (GlobalVariables.getDocYearStringFromAcadYearName(fd.Academic_Year_Picklist__c) != fd.Document_Year__c)
                        {
                            familyDocumentsByHouseholdId.get(householdId).remove(fd);
                        }
                 }
            }
            
            //9. Process all verification records
            for (String householdId : allVerificationByHouseholdId.keySet()) {
                
                for(Verification__c v : allVerificationByHouseholdId.get(householdId)) {
                    
                    String docYear = GlobalVariables.getDocYearStringFromAcadYearName(pfsByVerificationId.get(v.Id).Academic_Year_Picklist__c);
                    String pyDocYear = GlobalVariables.getPrevDocYearStringFromAcadYearName(pfsByVerificationId.get(v.Id).Academic_Year_Picklist__c);
    
                    // Grab the family docs for this household. If we are logging cases where the docs are null for this
                    // household, skip to the next household to avoid an invalid scenario that has already been logged in the previous loop.
                    Set<Family_Document__c> allDocs = familyDocumentsByHouseholdId.get(householdId);
                    if (shouldLogNullFamilyDocsForHouseholds(allDocs)) {
                        continue;
                    }

                    checkDocumentCompleteness(allDocs, v, docYear);
                }
            }
            if (!verificationsWithModification.isEmpty()) {
                update verificationsWithModification;
                verificationsWithModification.clear();
            }
        } catch (VerificationException ve) {
            throw(ve);
            return;
        }
    }

    public static Set<Id> getHouseholdIdsFromFamilyDocuments(Set<Family_Document__c> familyDocuments) {
        Set<Id> retVal = new Set<Id>();
        for (Family_Document__c fd : familyDocuments) {
            if (String.isEmpty(fd.Household__c)) {
                throw new VerificationException('Family Document without household id');
            }
            retVal.add(fd.Household__c);
        }
        return retVal;
    }

    /**
     * Scan Family Documents for completeness
     */
    @testVisible
    private static void checkDocumentCompleteness(Set<Family_Document__c> familyDocuments, Verification__c verification, String academicYear)
    {
        String priorAcademicYear = String.valueOf(Integer.valueOf(academicYear.left(4)) - 1);

        List<String> academicYears = new List <String>{priorAcademicYear, academicYear};
        Map<String, Map<String, Set<Family_Document__c>>> familyDocumentsByAcademicYear = sortFamilyDocumentsByAcademicYearAndType(familyDocuments);
        Boolean verificationModified = false;
        String pyPrefix;
        Map<String, List<String>> verificationFieldsByDocType;
        Map<String, String> statusFieldByDocType;

        for (String academicYearValue : academicYears)
        {
            if (academicYearValue == priorAcademicYear)
            {
                pyPrefix = PY_PREFIX;
                verificationFieldsByDocType = pyVerificationFieldsByDocumentType;
                statusFieldByDocType = pyStatusFieldByDocumentType;
            }
            else
            {
                pyPrefix = '';
                verificationFieldsByDocType = verificationFieldsByDocumentType;
                statusFieldByDocType = statusFieldByDocumentType;
            }

            // process documents
            if (familyDocumentsByAcademicYear.get(academicYearValue) == null)
            {
                continue;
            }

            Map<String, Set<Family_Document__c>> processingAcademicYearDocs = familyDocumentsByAcademicYear.get(academicYearValue);
            // X1040_Wages_Status__c
            for (String dt : DOCUMENT_TYPES_TO_PROCESS)
            {
                if (processingAcademicYearDocs.get(dt) != null && verification.get(pyPrefix + EXPECTED_NUMBER_OF_1040S) != null)
                {
                    Boolean match = false;
                    Decimal matchingValue = 0.0;
                    if ((Boolean.valueOf(verification.get(pyPrefix + VERIFICATION_PARENT_A_VERIFIED)) || Boolean.valueOf(verification.get(pyPrefix + VERIFICATION_PARENT_B_VERIFIED))) &&
                            Integer.valueOf(verification.get(pyPrefix + EXPECTED_NUMBER_OF_1040S)) == 1)
                    {
                        Integer parentWith1040Verified = Boolean.valueOf(verification.get(pyPrefix + VERIFICATION_PARENT_A_VERIFIED)) ? 0 : 1;//SFP-1216: Since it may be verified by ParentA or ParentB.
                        matchingValue = (Decimal) (verification.get(verificationFieldsByDocType.get(dt).get(parentWith1040Verified)) != null ? verification.get(verificationFieldsByDocType.get(dt).get(parentWith1040Verified)) : 0.0);
                        match = checkVerificationVerificationValuesMatch(
                            matchingValue, // the verification field
                            processingAcademicYearDocs.get(dt), // family documents
                            familyDocumentFieldsByDocumentType.get(dt)); // field processing on family docs

                        verification.put(statusFieldByDocType.get(dt), (match ? COMPLETE_MATCH : COMPLETE_NO_MATCH));
                        verificationModified = true;
                    } else if (Integer.valueOf(verification.get(pyPrefix + EXPECTED_NUMBER_OF_1040S)) == 2
                        && Boolean.valueOf(verification.get(pyPrefix + VERIFICATION_PARENT_A_VERIFIED))
                        && Boolean.valueOf(verification.get(pyPrefix + VERIFICATION_PARENT_B_VERIFIED)))
                    {
                        // check if the verification fields
                        Decimal parentAValue = (Decimal) (verification.get(verificationFieldsByDocType.get(dt).get(0)) != null ? verification.get(verificationFieldsByDocType.get(dt).get(0)) : 0.0);
                        Decimal parentBValue = (Decimal) (verification.get(verificationFieldsByDocType.get(dt).get(1)) != null ? verification.get(verificationFieldsByDocType.get(dt).get(1)) : 0.0);

                        matchingValue = parentAValue + parentBValue;
                        match = checkVerificationVerificationValuesMatch(
                                // add both verification fields together and compare against the total for all fields
                                matchingValue,
                                processingAcademicYearDocs.get(dt), // family documents
                                familyDocumentFieldsByDocumentType.get(dt)); // field processing on family docs

                        verification.put(statusFieldByDocType.get(dt), (match ? COMPLETE_MATCH : COMPLETE_NO_MATCH));
                        verificationModified = true;
                    }
                }
                else
                {
                    // Revert values if no documents exist for this type and the status field is not 'PENDING'
                    if (verification.get(statusFieldByDocType.get(dt)) != PENDING)
                    {
                        verification.put(statusFieldByDocType.get(dt), PENDING);
                        verificationModified = true;
                    }
                }
            }
        }
        if (verificationModified)
        {
            verificationsWithModification.add(verification);
        }
    }

    @TestVisible
    private static Map<String, Map<String, Set<Family_Document__c>>> sortFamilyDocumentsByAcademicYearAndType(Set<Family_Document__c> familyDocuments) {
        Map<String, Map<String, Set<Family_Document__c>>> familyDocumentsByAcademicYear = new Map<String, Map<String, Set<Family_Document__c>>>();
        // Sort documents by type and year
        for (Family_Document__c fd : familyDocuments) {
            if (familyDocumentsByAcademicYear.get(fd.Document_Year__c) == null) {
                familyDocumentsByAcademicYear.put(fd.Document_Year__c, new Map<String, Set<Family_Document__c>>());
            }
            if (familyDocumentsByAcademicYear.get(fd.Document_Year__c).get(fd.Document_Type__c) == null) {
                familyDocumentsByAcademicYear.get(fd.Document_Year__c).put(fd.Document_Type__c, new Set<Family_Document__c>());
            }
            familyDocumentsByAcademicYear.get(fd.Document_Year__c).get(fd.Document_Type__c).add(fd);
        }
        return familyDocumentsByAcademicYear;
    }

    /**
     *  Check the specific field specified on all existing family documents and check if it matches expected value
     */
    private static Boolean checkVerificationVerificationValuesMatch(Decimal matchingValue, Set<Family_Document__c> familyDocuments, String sumField)
    {
        Decimal fieldSumFromAllDocuments = 0.00;
        for (Family_Document__c fd : familyDocuments)
        {
            fieldSumFromAllDocuments += (Decimal) (fd.get(sumField) != null ? fd.get(sumField) : 0.00);
        }

        if(fieldSumFromAllDocuments != matchingValue && SchoolPortalSettings.Verification_Match_Tolerance > 0)
        {
            
            Decimal difference = fieldSumFromAllDocuments - matchingValue;
            return Math.abs(difference) <= SchoolPortalSettings.Verification_Match_Tolerance;
        }

        return fieldSumFromAllDocuments == matchingValue;
    }

    /**
     *  SFP-640 [9-13-16] Triggered when Filing_Status__c changes to set the number of expected 1040s
     */
    public static void setVerificationExpected1040BasedOnFilingStatus(Map<Id, PFS__c> newPFSRecords, Map<Id, PFS__c> oldPFSRecords) {
        // Select the verification records
        Map<String, PFS__c> pfsByVerificationId = new Map<String, PFS__c>();
        for (Id newPfsId : newPFSRecords.keyset()) {
            PFS__c newPFS = newPFSRecords.get(newPfsId);
            PFS__c oldPFS = oldPFSRecords.get(newPfsId);
            if (EfcConstants.isFilingMarriedSeparated(newPFS.Filing_Status__c, newPFS.Parent_B_Filing_Status__c) !=
                    EfcConstants.isFilingMarriedSeparated(oldPFS.Filing_Status__c, oldPFS.Parent_B_Filing_Status__c)) {
                pfsByVerificationId.put(newPFS.Verification__c, newPFS);
            }
        }

        if (pfsByVerificationId.isEmpty()) {
            return;
        }
        List<Verification__c> verifications = [
                SELECT Id, Expected_Number_of_1040s__c
                FROM Verification__c
                WHERE Id IN :pfsByVerificationId.keyset()
        ];

        for (Verification__c verification : verifications) {
            PFS__c pfs = pfsByVerificationId.get(verification.id);
            verification.Expected_Number_of_1040s__c = EfcConstants.isFilingMarriedSeparated(pfs.Filing_Status__c, pfs.Parent_B_Filing_Status__c) ? 2 : 1;
        }
        update verifications;
    }

    private static Set<String> verifiableDocTypes;
    /**
     * @description Gets the document types that can be verified by the system. These document types are primarily different types of tax documents such as 1040s and W2s.
     * @return A set containing document types.
     */
    public static Set<String> getVerifiableDocTypes() {
        if (verifiableDocTypes != null) {
            return verifiableDocTypes;
        }

        verifiableDocTypes = new Set<String>();
        verifiableDocTypes.addAll(X1040_DOCUMENT_TYPES);
        verifiableDocTypes.addAll(familyDocumentFieldsByDocumentType.keyset());
        verifiableDocTypes.add(DOCUMENT_TYPE_SCH_A);

        return verifiableDocTypes;
    }

    /**
     * @description Checks a document type to see if it is one that can be verified.
     * @param docTypeToCheck The type to check.
     * @return True if the specified document type is verifiable.
     */
    public static Boolean isDocTypeVerifiable(String docTypeToCheck) {
        return getVerifiableDocTypes().contains(docTypeToCheck);
    }

    @testVisible
    private static Boolean shouldLogNullFamilyDocsForHouseholds(Set<Family_Document__c> famDocsToCheck) {
        if (!FeatureToggles.isEnabled('Log_Null_Family_Docs_For_Verification__c')) {
            return false;
        }

        return famDocsToCheck == null;
    }

    private class VerificationException extends Exception {}
}