public without sharing class SchoolPFSLockSiblings {

    /*Initialization*/
   
    /*End Initialization*/
   
    /*Properties*/
   public static Boolean isFirstRun = true; 
    /*End Properties*/
       
    /*Action Methods*/

    // method that locks or unlocks sibling School PFS Assignments of School PFS Assignments that were just locked or unlocked
    public static void lockOrUnlockSiblingSPFSs(List<School_PFS_Assignment__c> updatedSPFSs){
        // id sets for sibling query
        Set<Id> pfsIds = new Set<Id>();
        Set<Id> schoolIds = new Set<Id>();      
        // map of custom key (schoolId + pfsId) to just-updated School PFS Assignment
        Map<String, School_PFS_Assignment__c> customKeyToSPFS = new Map<String, School_PFS_Assignment__c>();
        
        // requery to get parent fields
        updatedSPFSs = [Select Id, Family_May_Submit_Updates__c, School__c, Applicant__r.PFS__c from School_PFS_Assignment__c where Id in :updatedSPFSs];
        
        // populate sets and map
        for (School_PFS_Assignment__c spfs : updatedSPFSs){
            pfsIds.add(spfs.Applicant__r.PFS__c);
            schoolIds.add(spfs.School__c);
            String customKey = String.valueOf(spfs.School__c) + String.valueOf(spfs.Applicant__r.PFS__c);
            customKeyToSPFS.put(customKey, spfs);
        }
        
        // list we will eventually update
        List<School_PFS_Assignment__c> siblingsToUpdate = new List<School_PFS_Assignment__c>();
        
        // query for sibling SPFSs
        List<School_PFS_Assignment__c> possibleSiblings = [Select Id, Family_May_Submit_Updates__c, School__c, Applicant__r.PFS__c
                                                            from School_PFS_Assignment__c 
                                                            where Applicant__r.PFS__c in :pfsIds 
                                                            AND School__c in :schoolIds
                                                            AND Id not in :updatedSPFSs];
        
        // loop through sibling SPFSs, update the locked field if it differs   
        for (School_PFS_Assignment__c siblingSPFS : possibleSiblings){
            String customKey = String.valueOf(siblingSPFS.School__c) + String.valueOf(siblingSPFS.Applicant__r.PFS__c);
            School_PFS_Assignment__c updatedSPFS = customKeyToSPFS.get(customKey);
            if (updatedSPFS != null && updatedSPFS.Family_May_Submit_Updates__c != null && siblingSPFS.Family_May_Submit_Updates__c != updatedSPFS.Family_May_Submit_Updates__c){
                siblingSPFS.Family_May_Submit_Updates__c = updatedSPFS.Family_May_Submit_Updates__c;
                siblingsToUpdate.add(siblingSPFS);
            }
        }
        
        if (!siblingsToUpdate.isEmpty()){
            update siblingsToUpdate;
        }
    }
    
   
    /*End Action Methods*/
   
    /*Helper Methods*/
   
    /*End Helper Methods*/
}