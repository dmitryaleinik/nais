@isTest
private class DocumentVerificationServiceTest {

    private static final String PROCESSING_REQUIRED = 'Processing Required';
    private static final String PROCESSING_COMPLETE = 'Processing Complete';

    private static PFS__c pfs;
    private static School_PFS_Assignment__c pfsAssignment;
    private static Annual_Setting__c annualSetting;
    private static Verification__c verification;
    private static void setup(Boolean shouldCopyVerification){
        // create test data
        List<Academic_Year__c> yearList = AcademicYearTestData.Instance.insertAcademicYears(5);
        Academic_Year__c academicYear = yearList[yearList.size() - 1];
        TestUtils.createSSSConstants(academicYear.Id, true);

        TableTestData.populateAllEfcTables(academicYear.Id);

        Account school = AccountTestData.Instance.forName('School1')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccount();

        Account family = AccountTestData.Instance.forName('The individual')
                .forRecordTypeId(RecordTypes.individualAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccount();

        Contact student = ContactTestData.Instance.forLastName('Student')
                .forAccount(family.Id)
                .forRecordTypeId(RecordTypes.studentContactTypeId)
                .insertContact();

        Contact parent = ContactTestData.Instance.forLastName('Parent')
                .forAccount(family.Id)
                .forRecordTypeId(RecordTypes.parentContactTypeId)
                .insertContact();

        Student_Folder__c folder = StudentFolderTestData.Instance.forName('Folder')
                .forAcademicYearPicklist(academicYear.Name)
                .forStudentId(student.Id)
                .insertStudentFolder();

        annualSetting = AnnualSettingsTestData.Instance.forSchoolId(school.Id).forAcademicYearId(academicYear.Id).create();
        annualSetting.Add_Depreciation_Home_Business_Expense__c = 'Yes';
        annualSetting.Copy_Verification_Values_to_Revisions__c = shouldCopyVerification ? 'Yes' : 'No';
        insert annualSetting;

        verification = TestUtils.createVerification(true);

        pfs = PfsTestData.Instance.forAcademicYearPicklist(academicYear.Name).forParentA(parent.Id).create();
        pfs.Verification__c = verification.Id;
        insert pfs;

        Applicant__c applicant = ApplicantTestData.Instance.forContactId(student.Id).forPfsId(pfs.Id).insertApplicant();

        // Create a school PFS Assignment        
        pfsAssignment = SchoolPfsAssignmentTestData.Instance.forAcademicYearPicklist(academicYear.Name)
                .forApplicantId(applicant.Id)
                .forSchoolId(school.Id)
                .forStudentFolderId(folder.Id)
                .insertSchoolPfsAssignment();
    }

    private static School_PFS_Assignment__c querySpa(Id recordId) {
        return [SELECT Salary_Wages_Parent_A__c, Salary_Wages_Parent_B__c, Social_Security_Tax_Allowance_Calc_Rev__c,
                Medicare_Tax_Allowance_Calc_Revision__c, Pre_Tax_Payments_to_Retirement_Plans__c, Verification_Processing_Status__c
        FROM School_PFS_Assignment__c WHERE Id = :recordId];
    }

    private static School_Document_Assignment__c queryDocumentAssignment(Id recordId) {
        return [SELECT Id, Verification_Request_Status__c, Verification_Request_Error__c FROM School_Document_Assignment__c WHERE Id = :recordId];
    }

    private static List<School_Document_Assignment__c> queryDocumentAssignments(Set<Id> recordIds) {
        return [SELECT Id, Verification_Request_Status__c, Verification_Request_Error__c FROM School_Document_Assignment__c WHERE Id IN :recordIds];
    }

    private static List<School_PFS_Assignment__c> insertPfsAssignmentsForSchools(List<Account> schools) {
        List<School_PFS_Assignment__c> pfsAssignments = new List<School_PFS_Assignment__c>();

        for (Account school : schools) {
            School_PFS_Assignment__c newRecord = SchoolPfsAssignmentTestData.Instance
                    .forSchoolId(school.Id)
                    .forAcademicYearPicklist('2017-2018')
                    .createSchoolPfsAssignmentWithoutReset();
            pfsAssignments.add(newRecord);
        }

        if (!pfsAssignments.isEmpty()) {
            DML.WithoutSharing.insertObjects(pfsAssignments);
        }

        return pfsAssignments;
    }

    private static List<School_Document_Assignment__c> insertDocAssignmentsForSchools(SchoolDocumentAssignmentTestData recordTestData, List<School_PFS_Assignment__c> pfsAssignments) {
        List<School_Document_Assignment__c> documentAssignments = new List<School_Document_Assignment__c>();

        for (School_PFS_Assignment__c spa : pfsAssignments) {
            School_Document_Assignment__c newRecord =
                    recordTestData.forPfsAssignment(spa.Id).createSchoolDocumentAssignmentWithoutReset();
            documentAssignments.add(newRecord);
        }

        if (!documentAssignments.isEmpty()) {
            DML.WithoutSharing.insertObjects(documentAssignments);
        }

        return documentAssignments;
    }

    private static void assertSdaRequestValues(List<School_Document_Assignment__c> documentAssignments, String expectedStatus, String expectedError) {
        for (School_Document_Assignment__c documentAssignment : documentAssignments) {
            System.assertEquals(expectedStatus, documentAssignment.Verification_Request_Status__c, 'Expected the correct request status.');
            System.assertEquals(expectedError, documentAssignment.Verification_Request_Error__c, 'Expected the correct request errors.');
        }
    }

    @isTest
    private static void queueVerificationUpdates_emptySetOfIds_expectNoBatchScheduled() {
        Id jobId = DocumentVerificationService.Instance.queueVerificationUpdates(new Set<Id>());

        System.assertEquals(null, jobId, 'Expected no job to be scheduled.');
    }

    @isTest
    private static void queueVerificationUpdates_verificationCompleteAndMatched_expectBatchUpdatesSpa() {
        Boolean shouldCopyVerification = true;
        setup(shouldCopyVerification);

        // set verification X1040_Wages__c
        verification.X1040_Wages__c = 100000;
        verification.X1040_Wages_Status__c = VerificationAction.COMPLETE_MATCH;
        verification.Parent_A_1040_Verified__c = true; // not necessary
        verification.X1040_Wages_B__c = 500000;
        verification.Parent_B_1040_Verified__c = true; // not necessary
        update verification;

        pfsAssignment.Sum_W2_Wages_Parent_A__c = 100000;
        pfsAssignment.Sum_W2_Wages_Parent_B__c = 500000;
        pfsAssignment.Sum_W2_SS_Tax__c = 10000;
        pfsAssignment.Sum_W2_Medicare__c = 10001;
        pfsAssignment.Sum_Pre_Tax_Payments_To_Retirement_Plans__c = 11001;
        update pfsAssignment;

        Test.startTest();
        Id jobId = DocumentVerificationService.Instance.queueVerificationUpdates(new Set<Id> { pfsAssignment.Id });
        Test.stopTest();

        School_PFS_Assignment__c updatedRecord = querySpa(pfsAssignment.Id);

        System.assertEquals(100000, updatedRecord.Salary_Wages_Parent_A__c);
        System.assertEquals(500000, updatedRecord.Salary_Wages_Parent_B__c);
        System.assertEquals(10000, updatedRecord.Social_Security_Tax_Allowance_Calc_Rev__c);
        System.assertEquals(10001, updatedRecord.Medicare_Tax_Allowance_Calc_Revision__c);
        System.assertEquals(11001, updatedRecord.Pre_Tax_Payments_to_Retirement_Plans__c);
        System.assertEquals(PROCESSING_COMPLETE, updatedRecord.Verification_Processing_Status__c, 'Expected the SPA to be processed.');
    }

    @isTest
    private static void queueVerificationUpdates_verificationCompleteNotMatched_expectBatchDoesNotUpdateVerification() {
        Boolean shouldCopyVerification = true;
        setup(shouldCopyVerification);

        // set verification X1040_Wages__c
        verification.X1040_Wages__c = 100000;
        verification.X1040_Wages_Status__c = VerificationAction.COMPLETE_NO_MATCH;
        verification.Parent_A_1040_Verified__c = true; // not necessary
        verification.X1040_Wages_B__c = 500000;
        verification.Parent_B_1040_Verified__c = true; // not necessary
        update verification;

        pfsAssignment.Sum_W2_Wages_Parent_A__c = 100000;
        pfsAssignment.Sum_W2_Wages_Parent_B__c = 500000;
        pfsAssignment.Sum_W2_SS_Tax__c = 10000;
        pfsAssignment.Sum_W2_Medicare__c = 10001;
        pfsAssignment.Sum_Pre_Tax_Payments_To_Retirement_Plans__c = 11001;
        update pfsAssignment;

        Test.startTest();
        Id jobId = DocumentVerificationService.Instance.queueVerificationUpdates(new Set<Id> { pfsAssignment.Id });
        Test.stopTest();

        School_PFS_Assignment__c updatedRecord = querySpa(pfsAssignment.Id);

        System.assertEquals(null, updatedRecord.Salary_Wages_Parent_A__c);
        System.assertEquals(null, updatedRecord.Salary_Wages_Parent_B__c);
        System.assertEquals(null, updatedRecord.Social_Security_Tax_Allowance_Calc_Rev__c);
        System.assertEquals(null, updatedRecord.Medicare_Tax_Allowance_Calc_Revision__c);
        System.assertEquals(null, updatedRecord.Pre_Tax_Payments_to_Retirement_Plans__c);
        System.assertEquals(PROCESSING_COMPLETE, updatedRecord.Verification_Processing_Status__c, 'Expected the SPA to be processed.');
    }

    @isTest
    private static void queueVerificationUpdates_verificationCompleteAndMatched_spaAlreadyFlagged_expectNoBatchScheduled() {
        Boolean shouldCopyVerification = true;
        setup(shouldCopyVerification);

        // set verification X1040_Wages__c
        verification.X1040_Wages__c = 100000;
        verification.X1040_Wages_Status__c = VerificationAction.COMPLETE_MATCH;
        verification.Parent_A_1040_Verified__c = true; // not necessary
        verification.X1040_Wages_B__c = 500000;
        verification.Parent_B_1040_Verified__c = true; // not necessary
        update verification;

        pfsAssignment.Sum_W2_Wages_Parent_A__c = 100000;
        pfsAssignment.Sum_W2_Wages_Parent_B__c = 500000;
        pfsAssignment.Sum_W2_SS_Tax__c = 10000;
        pfsAssignment.Sum_W2_Medicare__c = 10001;
        pfsAssignment.Sum_Pre_Tax_Payments_To_Retirement_Plans__c = 11001;
        pfsAssignment.Verification_Processing_Status__c = PROCESSING_REQUIRED;
        update pfsAssignment;

        Test.startTest();
        Id jobId = DocumentVerificationService.Instance.queueVerificationUpdates(new Set<Id> { pfsAssignment.Id });
        Test.stopTest();

        System.assertEquals(null, jobId, 'Expected no batch jobs to be scheduled.');

        School_PFS_Assignment__c updatedRecord = querySpa(pfsAssignment.Id);

        System.assertEquals(null, updatedRecord.Salary_Wages_Parent_A__c);
        System.assertEquals(null, updatedRecord.Salary_Wages_Parent_B__c);
        System.assertEquals(null, updatedRecord.Social_Security_Tax_Allowance_Calc_Rev__c);
        System.assertEquals(null, updatedRecord.Medicare_Tax_Allowance_Calc_Revision__c);
        System.assertEquals(null, updatedRecord.Pre_Tax_Payments_to_Retirement_Plans__c);
        System.assertNotEquals(PROCESSING_COMPLETE, updatedRecord.Verification_Processing_Status__c,
                'Expected the SPA to not be processed.');
    }

    @isTest
    private static void queueVerificationUpdates_verificationComplete_annualSettingsDontCopyValues_expectBatchScheduledButNoUpdates() {
        Boolean shouldCopyVerification = false;
        setup(shouldCopyVerification);

        // set verification X1040_Wages__c
        verification.X1040_Wages__c = 100000;
        verification.X1040_Wages_Status__c = VerificationAction.COMPLETE_MATCH;
        verification.Parent_A_1040_Verified__c = true; // not necessary
        verification.X1040_Wages_B__c = 500000;
        verification.Parent_B_1040_Verified__c = true; // not necessary
        update verification;

        pfsAssignment.Sum_W2_Wages_Parent_A__c = 100000;
        pfsAssignment.Sum_W2_Wages_Parent_B__c = 500000;
        pfsAssignment.Sum_W2_SS_Tax__c = 10000;
        pfsAssignment.Sum_W2_Medicare__c = 10001;
        pfsAssignment.Sum_Pre_Tax_Payments_To_Retirement_Plans__c = 11001;
        update pfsAssignment;

        Test.startTest();
        Id jobId = DocumentVerificationService.Instance.queueVerificationUpdates(new Set<Id> { pfsAssignment.Id });
        Test.stopTest();

        School_PFS_Assignment__c updatedRecord = querySpa(pfsAssignment.Id);

        System.assertEquals(null, updatedRecord.Salary_Wages_Parent_A__c);
        System.assertEquals(null, updatedRecord.Salary_Wages_Parent_B__c);
        System.assertEquals(null, updatedRecord.Social_Security_Tax_Allowance_Calc_Rev__c);
        System.assertEquals(null, updatedRecord.Medicare_Tax_Allowance_Calc_Revision__c);
        System.assertEquals(null, updatedRecord.Pre_Tax_Payments_to_Retirement_Plans__c);
        System.assertEquals(PROCESSING_COMPLETE, updatedRecord.Verification_Processing_Status__c, 'Expected the SPA to be processed.');
    }

    @isTest
    private static void requestVerificationForProcessedDocs_featureDisabled_expectFlaggedRecordsNotUpdated() {
        Account school = AccountTestData.Instance.forName('School1')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccount();

        School_PFS_Assignment__c schoolPfsAssignment = SchoolPfsAssignmentTestData.Instance
                .forSchoolId(school.Id)
                .forAcademicYearPicklist('2017-2018')
                .insertSchoolPfsAssignment();

        Family_Document__c document = FamilyDocumentTestData.Instance
                .forDocumentType('W2')
                .withImportId('1234-1234-1234-1234-1234')
                .insertFamilyDocument();

        String requestStatus = SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE;
        School_Document_Assignment__c documentAssignment = SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(requestStatus)
                .forPfsAssignment(schoolPfsAssignment.Id)
                .asType('W2')
                .forDocumentYear('2017')
                .insertSchoolDocumentAssignment();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(requestStatus, documentAssignment.Verification_Request_Status__c, 'Expected the request status to be set.');

        Test.startTest();
        DocumentVerificationService.Instance.requestVerificationForProcessedDocs(new Set<Id> { documentAssignment.Id });
        Test.stopTest();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(requestStatus, documentAssignment.Verification_Request_Status__c, 'Expected the request status to be set.');
    }

    @isTest
    private static void requestVerificationForProcessedDocs_featureEnabled_springCMDownForMaintenance_expectFlaggedRecordsNotUpdated() {
        Account school = AccountTestData.Instance.forName('School1')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccount();

        School_PFS_Assignment__c schoolPfsAssignment = SchoolPfsAssignmentTestData.Instance
                .forSchoolId(school.Id)
                .forAcademicYearPicklist('2017-2018')
                .insertSchoolPfsAssignment();

        Family_Document__c document = FamilyDocumentTestData.Instance
                .forDocumentType('W2')
                .withImportId('1234-1234-1234-1234-1234')
                .insertFamilyDocument();

        String requestStatus = SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE;
        School_Document_Assignment__c documentAssignment = SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(requestStatus)
                .forPfsAssignment(schoolPfsAssignment.Id)
                .asType('W2')
                .forDocumentYear('2017')
                .insertSchoolDocumentAssignment();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(requestStatus, documentAssignment.Verification_Request_Status__c, 'Expected the request status to be set.');

        DateTime startTime = System.now().addDays(-4);
        DateTime endTime = System.now().addDays(4);

        // Insert global message for spring CM maintenance window.
        Global_Message__c springCMDownMessage = new Global_Message__c(
                Portal__c = 'Spring CM - Maintenance Window',
                Display_Start_Date_Time__c = startTime,
                Display_End_Date_Time__c = endTime);
        insert springCMDownMessage;

        Test.startTest();
        FeatureToggles.setToggle('Verify_Existing_Documents__c', true);
        DocumentVerificationService.Instance.requestVerificationForProcessedDocs(new Set<Id> { documentAssignment.Id });
        Test.stopTest();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(requestStatus, documentAssignment.Verification_Request_Status__c,
                'Expected the request status to be unchanged when springCM is down for maintenance.');
    }

    @isTest
    private static void requestVerificationForProcessedDocs_recordsFlaggedForVerification_expectRequestStatusComplete() {
        Account school = AccountTestData.Instance.forName('School1')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccount();

        School_PFS_Assignment__c schoolPfsAssignment = SchoolPfsAssignmentTestData.Instance
                .forSchoolId(school.Id)
                .forAcademicYearPicklist('2017-2018')
                .insertSchoolPfsAssignment();

        Family_Document__c document = FamilyDocumentTestData.Instance
                .forDocumentType('W2')
                .withImportId('1234-1234-1234-1234-1234')
                .insertFamilyDocument();

        String requestStatus = SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE;
        School_Document_Assignment__c documentAssignment = SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(requestStatus)
                .forPfsAssignment(schoolPfsAssignment.Id)
                .asType('W2')
                .forDocumentYear('2017')
                .insertSchoolDocumentAssignment();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(requestStatus, documentAssignment.Verification_Request_Status__c, 'Expected the request status to be correct.');

        Test.startTest();
        FeatureToggles.setToggle('Verify_Existing_Documents__c', true);
        DocumentVerificationService.Instance.requestVerificationForProcessedDocs(new Set<Id> { documentAssignment.Id });
        Test.stopTest();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(SchoolDocumentAssignments.VERIFICATION_REQUEST_COMPLETE, documentAssignment.Verification_Request_Status__c, 'Expected the request status to be correct.');
    }

    @isTest
    private static void requestVerificationForProcessedDocs_docForMultipleSchools_recordsFlaggedForVerification_expectRequestStatusComplete() {
        Integer numberOfSchools = 3;
        List<Account> schools = AccountTestData.Instance.forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccounts(numberOfSchools);

        List<School_PFS_Assignment__c> pfsAssignments = insertPfsAssignmentsForSchools(schools);

        Family_Document__c document = FamilyDocumentTestData.Instance
                .forDocumentType('W2')
                .withImportId('1234-1234-1234-1234-1234')
                .insertFamilyDocument();

        String requestStatus = SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE;
        SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(requestStatus)
                .asType('W2')
                .forDocumentYear('2017');

        List<School_Document_Assignment__c> documentAssignments =
                insertDocAssignmentsForSchools(SchoolDocumentAssignmentTestData.Instance, pfsAssignments);
        Map<Id, School_Document_Assignment__c> documentAssignmentsByIds =
                new Map<Id, School_Document_Assignment__c>(documentAssignments);

        documentAssignments = queryDocumentAssignments(documentAssignmentsByIds.keySet());
        assertSdaRequestValues(documentAssignments, requestStatus, null);

        Test.startTest();
        FeatureToggles.setToggle('Verify_Existing_Documents__c', true);
        DocumentVerificationService.Instance.requestVerificationForProcessedDocs(documentAssignmentsByIds.keySet());
        Test.stopTest();

        documentAssignments = queryDocumentAssignments(documentAssignmentsByIds.keySet());
        assertSdaRequestValues(documentAssignments, SchoolDocumentAssignments.VERIFICATION_REQUEST_COMPLETE, null);
    }

    @isTest
    private static void requestVerificationForProcessedDocs_recordsFlaggedForVerification_previousSdaHasVerifyDataTrue_expectRequestStatusNull() {
        Integer numberOfSchools = 3;
        List<Account> schools = AccountTestData.Instance.forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccounts(numberOfSchools);

        List<School_PFS_Assignment__c> pfsAssignments = insertPfsAssignmentsForSchools(schools);

        Family_Document__c document = FamilyDocumentTestData.Instance
                .forDocumentType('W2')
                .withImportId('1234-1234-1234-1234-1234')
                .insertFamilyDocument();

        String requestStatus = SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE;
        SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(requestStatus)
                .asType('W2')
                .forDocumentYear('2017');

        List<School_Document_Assignment__c> documentAssignments =
                insertDocAssignmentsForSchools(SchoolDocumentAssignmentTestData.Instance, pfsAssignments);

        // Update SDAs to have verify data set to true.
        for (School_Document_Assignment__c documentAssignment : documentAssignments) {
            documentAssignment.Verify_Data__c = true;
        }

        update documentAssignments;

        Map<Id, School_Document_Assignment__c> documentAssignmentsByIds =
                new Map<Id, School_Document_Assignment__c>(documentAssignments);

        documentAssignments = queryDocumentAssignments(documentAssignmentsByIds.keySet());
        assertSdaRequestValues(documentAssignments, requestStatus, null);

        Test.startTest();
        FeatureToggles.setToggle('Verify_Existing_Documents__c', true);
        // Only request verification for first SDA.
        DocumentVerificationService.Instance.requestVerificationForProcessedDocs(new Set<Id> { documentAssignments[0].Id });
        Test.stopTest();

        List<School_Document_Assignment__c> processedAssignments = queryDocumentAssignments(new Set<Id> { documentAssignments[0].Id });
        assertSdaRequestValues(processedAssignments, null, null);
    }

    @isTest
    private static void requestVerificationForProcessedDocs_recordsFlaggedForVerification_recordsDoNotHaveRequiredInfo_expectRequestStatusNull() {
        // Create a school without a school code to include in missing information.
        Account school = AccountTestData.Instance.forName('School1')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .create();
        school.SSS_School_Code__c = '';
        insert school;

        School_PFS_Assignment__c schoolPfsAssignment = SchoolPfsAssignmentTestData.Instance
                .forSchoolId(school.Id)
                .forAcademicYearPicklist('2017-2018')
                .insertSchoolPfsAssignment();

        // Insert a family document without an import Id.
        Family_Document__c document = FamilyDocumentTestData.Instance.forDocumentType('W2').insertFamilyDocument();

        String requestStatus = SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE;
        School_Document_Assignment__c documentAssignment = SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(requestStatus)
                .forPfsAssignment(schoolPfsAssignment.Id)
                .forDocumentYear('2017')
                .insertSchoolDocumentAssignment();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(requestStatus, documentAssignment.Verification_Request_Status__c, 'Expected the request status to be correct.');

        Test.startTest();
        FeatureToggles.setToggle('Verify_Existing_Documents__c', true);
        DocumentVerificationService.Instance.requestVerificationForProcessedDocs(new Set<Id> { documentAssignment.Id });
        Test.stopTest();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(SchoolDocumentAssignments.VERIFICATION_REQUEST_ERROR, documentAssignment.Verification_Request_Status__c,
                'Expected the request status to be null if the required information is missing.');
        System.assert(String.isNotBlank(documentAssignment.Verification_Request_Error__c),
                'Expected the errors to not be blank.');
    }

    @isTest
    private static void requestVerificationForProcessedDocs_recordsFlaggedForVerification_mockException_expectErrorStatus() {
        Account school = AccountTestData.Instance.forName('School1')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccount();

        School_PFS_Assignment__c schoolPfsAssignment = SchoolPfsAssignmentTestData.Instance
                .forSchoolId(school.Id)
                .forAcademicYearPicklist('2017-2018')
                .insertSchoolPfsAssignment();

        Family_Document__c document = FamilyDocumentTestData.Instance
                .forDocumentType('W2')
                .withImportId('1234-1234-1234-1234-1234')
                .insertFamilyDocument();

        String requestStatus = SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE;
        School_Document_Assignment__c documentAssignment = SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(requestStatus)
                .forPfsAssignment(schoolPfsAssignment.Id)
                .asType('W2')
                .forDocumentYear('2017')
                .insertSchoolDocumentAssignment();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(requestStatus, documentAssignment.Verification_Request_Status__c, 'Expected the request status to be correct.');

        // Create mock exception
        String expectedError = 'Failed to request verification from spring CM';
        SpringCMProd.springCMException mockException = new SpringCMProd.springCMException(expectedError);
        SpringCMAction.mockException = mockException;

        Test.startTest();
        FeatureToggles.setToggle('Verify_Existing_Documents__c', true);
        DocumentVerificationService.Instance.requestVerificationForProcessedDocs(new Set<Id> { documentAssignment.Id });
        Test.stopTest();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(SchoolDocumentAssignments.VERIFICATION_REQUEST_ERROR, documentAssignment.Verification_Request_Status__c,
                'Expected the request status to be error when the callout fails.');
        System.assertEquals(expectedError, documentAssignment.Verification_Request_Error__c,
                'Expected the request error to have the exception message.');
    }

    @isTest
    private static void requestVerificationForProcessedDocs_oneDocForMultipleSchools_recordsFlaggedForVerification_mockException_expectErrorStatus() {
        Integer numberOfSchools = 3;
        List<Account> schools = AccountTestData.Instance.forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccounts(numberOfSchools);

        List<School_PFS_Assignment__c> pfsAssignments = insertPfsAssignmentsForSchools(schools);

        Family_Document__c document = FamilyDocumentTestData.Instance
                .forDocumentType('W2')
                .withImportId('1234-1234-1234-1234-1234')
                .insertFamilyDocument();

        String requestStatus = SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE;
        SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(requestStatus)
                .asType('W2')
                .forDocumentYear('2017');

        List<School_Document_Assignment__c> documentAssignments =
                insertDocAssignmentsForSchools(SchoolDocumentAssignmentTestData.Instance, pfsAssignments);
        Map<Id, School_Document_Assignment__c> documentAssignmentsByIds =
                new Map<Id, School_Document_Assignment__c>(documentAssignments);

        documentAssignments = queryDocumentAssignments(documentAssignmentsByIds.keySet());
        assertSdaRequestValues(documentAssignments, requestStatus, null);

        // Create mock exception
        String expectedError = 'Failed to request verification from spring CM';
        SpringCMProd.springCMException mockException = new SpringCMProd.springCMException(expectedError);
        SpringCMAction.mockException = mockException;

        Test.startTest();
        FeatureToggles.setToggle('Verify_Existing_Documents__c', true);
        DocumentVerificationService.Instance.requestVerificationForProcessedDocs(documentAssignmentsByIds.keySet());
        Test.stopTest();

        documentAssignments = queryDocumentAssignments(documentAssignmentsByIds.keySet());
        assertSdaRequestValues(documentAssignments, SchoolDocumentAssignments.VERIFICATION_REQUEST_ERROR, expectedError);
    }

    // BEGIN tests with Track Verification Requested Date feature toggle.
    // These tests were introduced as part of SFP-1150

    @isTest
    private static void requestVerificationForProcessedDocs_docForMultipleSchools_verificationRequestDateNull_expectRequestStatusCompleteAndRequestDatePopulated() {
        Integer numberOfSchools = 3;
        List<Account> schools = AccountTestData.Instance.forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccounts(numberOfSchools);

        List<School_PFS_Assignment__c> pfsAssignments = insertPfsAssignmentsForSchools(schools);

        Family_Document__c document = FamilyDocumentTestData.Instance
                .forDocumentType('W2')
                .withImportId('1234-1234-1234-1234-1234')
                .verificationRequestedOn(null)
                .insertFamilyDocument();

        String requestStatus = SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE;
        SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(requestStatus)
                .asType('W2')
                .forDocumentYear('2017');

        List<School_Document_Assignment__c> documentAssignments =
                insertDocAssignmentsForSchools(SchoolDocumentAssignmentTestData.Instance, pfsAssignments);
        Map<Id, School_Document_Assignment__c> documentAssignmentsByIds =
                new Map<Id, School_Document_Assignment__c>(documentAssignments);

        documentAssignments = queryDocumentAssignments(documentAssignmentsByIds.keySet());
        assertSdaRequestValues(documentAssignments, requestStatus, null);

        Test.startTest();
        FeatureToggles.setToggle('Verify_Existing_Documents__c', true);
        FeatureToggles.setToggle('Track_Verification_Request_Date__c', true);
        DocumentVerificationService.Instance.requestVerificationForProcessedDocs(documentAssignmentsByIds.keySet());
        Test.stopTest();

        documentAssignments = queryDocumentAssignments(documentAssignmentsByIds.keySet());
        assertSdaRequestValues(documentAssignments, SchoolDocumentAssignments.VERIFICATION_REQUEST_COMPLETE, null);

        document = [SELECT Id, Verification_Requested_Date__c FROM Family_Document__c WHERE Id = :document.Id LIMIT 1];
        System.assertNotEquals(null, document.Verification_Requested_Date__c,
                'Expected the verification requested date to not be null.');
    }

    @isTest
    private static void requestVerificationForProcessedDocs_verificationRequestDatePopulated_expectRequestStatusNullAndRequestDateUnchanged() {
        Integer numberOfSchools = 3;
        List<Account> schools = AccountTestData.Instance.forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccounts(numberOfSchools);

        List<School_PFS_Assignment__c> pfsAssignments = insertPfsAssignmentsForSchools(schools);

        DateTime verificationRequestedOn = System.now();
        Family_Document__c document = FamilyDocumentTestData.Instance
                .forDocumentType('W2')
                .withImportId('1234-1234-1234-1234-1234')
                .verificationRequestedOn(verificationRequestedOn)
                .insertFamilyDocument();

        String requestStatus = SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE;
        SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(requestStatus)
                .asType('W2')
                .forDocumentYear('2017');

        List<School_Document_Assignment__c> documentAssignments =
                insertDocAssignmentsForSchools(SchoolDocumentAssignmentTestData.Instance, pfsAssignments);

        // Update SDAs to have verify data set to true.
        for (School_Document_Assignment__c documentAssignment : documentAssignments) {
            documentAssignment.Verify_Data__c = true;
        }

        update documentAssignments;

        Map<Id, School_Document_Assignment__c> documentAssignmentsByIds =
                new Map<Id, School_Document_Assignment__c>(documentAssignments);

        documentAssignments = queryDocumentAssignments(documentAssignmentsByIds.keySet());
        assertSdaRequestValues(documentAssignments, requestStatus, null);

        Test.startTest();
        FeatureToggles.setToggle('Verify_Existing_Documents__c', true);
        FeatureToggles.setToggle('Track_Verification_Request_Date__c', true);
        // Only request verification for first SDA.
        DocumentVerificationService.Instance.requestVerificationForProcessedDocs(new Set<Id> { documentAssignments[0].Id });
        Test.stopTest();

        List<School_Document_Assignment__c> processedAssignments = queryDocumentAssignments(new Set<Id> { documentAssignments[0].Id });
        assertSdaRequestValues(processedAssignments, null, null);

        document = [SELECT Id, Verification_Requested_Date__c FROM Family_Document__c WHERE Id = :document.Id LIMIT 1];
        System.assertEquals(verificationRequestedOn, document.Verification_Requested_Date__c,
                'Expected the verification requested date to be unchanged.');
    }

    @isTest
    private static void requestVerificationForProcessedDocs_verificationRequestDateNull_mockException_expectErrorStatusRequestDateNull() {
        Account school = AccountTestData.Instance.forName('School1')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccount();

        School_PFS_Assignment__c schoolPfsAssignment = SchoolPfsAssignmentTestData.Instance
                .forSchoolId(school.Id)
                .forAcademicYearPicklist('2017-2018')
                .insertSchoolPfsAssignment();

        Family_Document__c document = FamilyDocumentTestData.Instance
                .forDocumentType('W2')
                .withImportId('1234-1234-1234-1234-1234')
                .verificationRequestedOn(null)
                .insertFamilyDocument();

        String requestStatus = SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE;
        School_Document_Assignment__c documentAssignment = SchoolDocumentAssignmentTestData.Instance
                .forDocument(document.Id)
                .withVerificationRequestStatus(requestStatus)
                .forPfsAssignment(schoolPfsAssignment.Id)
                .asType('W2')
                .forDocumentYear('2017')
                .insertSchoolDocumentAssignment();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(requestStatus, documentAssignment.Verification_Request_Status__c, 'Expected the request status to be correct.');

        // Create mock exception
        String expectedError = 'Failed to request verification from spring CM';
        SpringCMProd.springCMException mockException = new SpringCMProd.springCMException(expectedError);
        SpringCMAction.mockException = mockException;

        Test.startTest();
        FeatureToggles.setToggle('Verify_Existing_Documents__c', true);
        FeatureToggles.setToggle('Track_Verification_Request_Date__c', true);
        DocumentVerificationService.Instance.requestVerificationForProcessedDocs(new Set<Id> { documentAssignment.Id });
        Test.stopTest();

        documentAssignment = queryDocumentAssignment(documentAssignment.Id);
        System.assertEquals(SchoolDocumentAssignments.VERIFICATION_REQUEST_ERROR, documentAssignment.Verification_Request_Status__c,
                'Expected the request status to be error when the callout fails.');
        System.assertEquals(expectedError, documentAssignment.Verification_Request_Error__c,
                'Expected the request error to have the exception message.');

        document = [SELECT Id, Verification_Requested_Date__c FROM Family_Document__c WHERE Id = :document.Id LIMIT 1];
        System.assertEquals(null, document.Verification_Requested_Date__c,
                'Expected the verification requested date to still be null.');
    }
    // END tests with Track Verification Requested Date feature toggle.
}