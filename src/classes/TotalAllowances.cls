/* 4. Calculate Total Allowances */
public virtual class TotalAllowances implements EfcCalculationSteps.Step {

    public void doCalculation(List<EfcWorksheetData> efcDataList) {
        for (EfcWorksheetData efcData : efcDataList) {
            doCalculation(efcData);
        }
    }

    public void doCalculation(EfcWorksheetData efcData) {

        // Federal Income Tax
        Decimal federalIncomeTax = 0;
        if (efcData.useRevisedFederalTax == true) {
            federalIncomeTax = efcData.federalIncomeTaxRevised;
        }
        else {
            // NAIS-514 do not calculate federal tax if filing status is Did not File
            if (efcData.filingStatus != EfcPicklistValues.FILING_STATUS_DID_NOT_FILE) {
                federalIncomeTax = calculateFederalTax(efcData);
            }
            // update the worksheet
            efcData.federalIncomeTaxCalculated = federalIncomeTax;
        }

        // [CH] NAIS-1860 Updated to allow revision
        // Social Security Tax Allowance
        Decimal socialSecurityTaxAllowance = 0;
        if(efcData.useRevisedSocialSecTax){
            socialSecurityTaxAllowance = efcData.socialSecurityTaxAllowanceRevised;
        }
        else{
            socialSecurityTaxAllowance = calculateSocialSecurityTaxAllowance(efcData);
            efcData.socialSecurityTaxAllowanceCalculated = socialSecurityTaxAllowance;
        }

        // [CH] NAIS-1860 Updated to allow revision
        // Medicare Tax Allowance
        Decimal medicareTaxAllowance = 0;
        if(efcData.useRevisedMedicareTax){
            medicareTaxAllowance = efcData.medicareTaxAllowanceRevised;
        }
        else{
            medicareTaxAllowance = calculateMedicareTaxAllowance(efcData);
            // update the worksheet
            efcData.medicareTaxAllowanceCalculated = medicareTaxAllowance;
        }

        // [CH] NAIS-1850 Put the calculated self employment tax value into efcData
        efcData.selfEmploymentTaxCalculated = calculateSelfEmploymentTaxCalc(efcData);

        // SE Tax Allowance
        Decimal selfEmploymentTaxAllowanceRemainder;
        selfEmploymentTaxAllowanceRemainder = calculateSelfEmploymentTaxAllowance(efcData);
        System.debug(LoggingLevel.ERROR, 'CH: selfEmploymentTaxAllowanceRemainder ' + selfEmploymentTaxAllowanceRemainder);

        // State/Other Tax Allowance
        Decimal stateOtherTaxAllowance = 0;
        if (efcData.useRevisedStateOtherTax == true) {
            stateOtherTaxAllowance = efcData.stateOtherTaxAllowanceRevised;
        }
        else {
            stateOtherTaxAllowance = calculateStateOtherTaxAllowance(efcData);
            // update the worksheet
            efcData.stateOtherTaxAllowanceCalculated = stateOtherTaxAllowance;
        }

        // Employment Allowance
        Decimal employmentAllowance;
        employmentAllowance = calculateEmploymentAllowance(efcData);

        // Medical Allowance
        Decimal medicalAllowance;
        medicalAllowance = calculateMedicalAllowance(efcData);

        // Unusual Expense
        Decimal unusualExpense = 0;
        if (efcData.unusualExpenses != null) {
            unusualExpense = efcData.unusualExpenses;
        }

        // Total Allowances
        Decimal totalAllowances = federalIncomeTax + stateOtherTaxAllowance
                + socialSecurityTaxAllowance + medicareTaxAllowance + selfEmploymentTaxAllowanceRemainder
                + employmentAllowance + medicalAllowance + unusualExpense;
        // update the worksheet
        // efcData.socialSecurityTaxAllowanceCalculated = socialSecurityTaxAllowance;
        // efcData.medicareTaxAllowanceCalculated = medicareTaxAllowance;
        efcData.selfEmploymentTaxAllowanceRemainder = selfEmploymentTaxAllowanceRemainder;
        efcData.employmentAllowance = employmentAllowance;
        efcData.medicalAllowance = medicalAllowance;
        efcData.totalAllowances = EfcUtil.negativeToZero(totalAllowances);
    }

    public Decimal calculateFederalTax(EfcWorksheetData efcData) {

        // check for required fields
        if (efcData.academicYearId == null) {throw new EfcException('The academic year was not specified.');}
        if (efcData.filingStatus == null) {throw new EfcException('The filing status was not specified.');}

        // SSS Constants
        Decimal exemptionAllowance = efcConstants.getExemptionAllowance(efcData.academicYearId);
        Decimal standardDeduction = efcConstants.getStandardDeductionForFilingStatus(efcData.academicYearId, efcData.filingStatus, efcData.filingStatusParentB);

        // exemptions
        Decimal exemptionValue = 0;
        if (efcData.incomeTaxExemptions != null) {
            exemptionValue = exemptionAllowance * efcData.incomeTaxExemptions;
        }

        // deductions
        Decimal deductionValue = 0;
        if ((efcData.itemizedDeductions != null) && (efcData.itemizedDeductions > standardDeduction)) {
            deductionValue = efcData.itemizedDeductions;
        }
        else {
            deductionValue = standardDeduction;
        }

        // taxable income
        Decimal taxableIncome = 0;
        if (efcData.totalTaxableIncome != null) {
            taxableIncome = efcData.totalTaxableIncome - exemptionValue - deductionValue;
            if (taxableIncome < 0) {
                taxableIncome = 0;
            }
        }

        // federal tax
        Decimal federalIncomeTax = TableFederalIncomeTax.calculateTax(efcData.academicYearId, efcData.filingStatus,
                taxableIncome, efcData.filingStatusParentB);
        return federalIncomeTax;
    }

    public Decimal calculateSocialSecurityTaxAllowance(EfcWorksheetData efcData) {

        // check for required fields
        if (efcData.academicYearId == null) {throw new EfcException('The academic year was not specified.');}

        // SSS Constants
        Decimal socialSecurityTaxThreshold = efcConstants.getSocialSecurityTaxThreshold(efcData.academicYearId);
        Decimal socialSecurityTaxRate = efcConstants.getSocialSecurityTaxRate(efcData.academicYearId);

        Decimal socialSecurityTaxAllowance = 0;

        // Parent A
        if ((efcData.salaryWagesParentA != null) && (efcData.salaryWagesParentA > 0)){

            // apply SS rate for value under cap
            if (efcData.salaryWagesParentA <= socialSecurityTaxThreshold) {
                socialSecurityTaxAllowance = socialSecurityTaxAllowance + (efcData.salaryWagesParentA * socialSecurityTaxRate).round(RoundingMode.HALF_UP);
            }
            else {
                socialSecurityTaxAllowance = socialSecurityTaxAllowance + (socialSecurityTaxThreshold * socialSecurityTaxRate).round(RoundingMode.HALF_UP);
            }
        }


        // Parent B
        if (efcData.familyStatus == EfcPicklistValues.FAMILY_STATUS_2_PARENT) {
            if ((efcData.salaryWagesParentB != null) && (efcData.salaryWagesParentB > 0)){

                // apply SS rate for value under cap
                if (efcData.salaryWagesParentB <= socialSecurityTaxThreshold) {
                    socialSecurityTaxAllowance = socialSecurityTaxAllowance + (efcData.salaryWagesParentB * socialSecurityTaxRate).round(RoundingMode.HALF_UP);
                }
                else {
                    socialSecurityTaxAllowance = socialSecurityTaxAllowance + (socialSecurityTaxThreshold * socialSecurityTaxRate).round(RoundingMode.HALF_UP);
                }
            }
        }
        return socialSecurityTaxAllowance;
    }


//  NAIS-1202 - update Medicare logic with high income threshold and rate
/*
        public Decimal calculateMedicareTaxAllowance(EfcWorksheetData efcData) {

            // check for required fields
            if (efcData.academicYearId == null) {throw new EfcException('The academic year was not specified.');}

            // SSS Constants
            Decimal medicareTaxRate = efcConstants.getMedicareTaxRate(efcData.academicYearId);

            Decimal medicareTaxAllowance = 0;

            // Parent A
            if ((efcData.salaryWagesParentA != null) && (efcData.salaryWagesParentA > 0)){
                medicareTaxAllowance = medicareTaxAllowance + (efcData.salaryWagesParentA * medicareTaxRate);
            }

            // Parent B
            if (efcData.familyStatus == EfcPicklistValues.FAMILY_STATUS_2_PARENT) {
                if ((efcData.salaryWagesParentB != null) && (efcData.salaryWagesParentB > 0)){
                    medicareTaxAllowance = medicareTaxAllowance + (efcData.salaryWagesParentB * medicareTaxRate);
                }
            }

            // NAIS-563: round the allowance value after applying rate to both parents
            medicareTaxAllowance = medicareTaxAllowance.round(RoundingMode.HALF_UP);


            // negative values should be treated as 0
            medicareTaxAllowance = EfcUtil.negativeToZero(medicareTaxAllowance);

            return medicareTaxAllowance;
        }
*/

    public Decimal calculateMedicareTaxAllowance(EfcWorksheetData efcData) {

        Decimal medicareTaxAllowance = 0;

        // check for required fields
        if (efcData.academicYearId == null) {throw new EfcException('The academic year was not specified.');}
        if (efcData.filingStatus == null) {throw new EfcException('The filing status was not specified.');}

        // get the rates from SSS Constants
        Decimal rate = efcConstants.getMedicareRateForFilingStatus(efcData.academicYearId, efcData.filingStatus, efcData.filingStatusParentB);
        Decimal threshold = efcConstants.getMedicareThresholdForFilingStatus(efcData.academicYearId, efcData.filingStatus, efcData.filingStatusParentB);
        Decimal thresholdRate = null;
        if (threshold != null) {
            thresholdRate = efcConstants.getMedicareThresholdRateForFilingStatus(efcData.academicYearId, efcData.filingStatus, efcData.filingStatusParentB);
        }

        // use combined salary if Married Joint filing
        if (efcData.filingStatus == EfcPicklistValues.FILING_STATUS_MARRIED_JOINT
                || efcData.filingStatus== EfcPicklistValues.FILING_STATUS_QUALIFYING_WINDOW_WITH_CHILD) {
            Decimal totalSalaryWages = 0;
            if ((efcData.salaryWagesParentA != null) && (efcData.salaryWagesParentA > 0)){
                totalSalaryWages = totalSalaryWages + efcData.salaryWagesParentA;
            }
            if (efcData.familyStatus == EfcPicklistValues.FAMILY_STATUS_2_PARENT) {
                if ((efcData.salaryWagesParentB != null) && (efcData.salaryWagesParentB > 0)){
                    totalSalaryWages = totalSalaryWages + efcData.salaryWagesParentB;
                }
            }

            medicareTaxAllowance = getMedicareTax(totalSalaryWages, rate, threshold, thresholdRate);
        }

        // if not filing jointly, then calculate Parent A and Parent B separately
        else {
            // Parent A
            if ((efcData.salaryWagesParentA != null) && (efcData.salaryWagesParentA > 0)){
                medicareTaxAllowance = medicareTaxAllowance + getMedicareTax(efcData.salaryWagesParentA, rate, threshold, thresholdRate);
            }

            // Parent B
            if (efcData.familyStatus == EfcPicklistValues.FAMILY_STATUS_2_PARENT) {
                if ((efcData.salaryWagesParentB != null) && (efcData.salaryWagesParentB > 0)){
                    medicareTaxAllowance = medicareTaxAllowance + getMedicareTax(efcData.salaryWagesParentB, rate, threshold, thresholdRate);
                }
            }
        }

        // NAIS-563: round the allowance value after applying rate to both parents
        medicareTaxAllowance = medicareTaxAllowance.round(RoundingMode.HALF_UP);

        // negative values should be treated as 0
        medicareTaxAllowance = EfcUtil.negativeToZero(medicareTaxAllowance);

        return medicareTaxAllowance;
    }

    public Decimal getMedicareTax(Decimal salary, Decimal rate, Decimal threshold, Decimal thresholdRate) {
        Decimal medicareTax = 0;
        if (threshold != null) {
            if (salary <= threshold) {
                medicareTax = salary * rate;
            }
            else {
                Decimal salaryAboveThreshold = salary - threshold;
                medicareTax = (threshold * rate) + (salaryAboveThreshold * thresholdRate);
            }
        }
        else {
            medicareTax = (salary * rate);
        }
        return medicareTax;
    }

    // [CH] NAIS-1850 Perform calculation for self employment tax based on configurable rate
    public Decimal calculateSelfEmploymentTaxCalc(EfcWorksheetData efcData){

        Decimal selfEmploymentTaxRate = EfcConstants.getSelfEmploymentTaxRate(efcData.academicYearId);

        Decimal businessFarmProfit = EfcUtil.nullToZero(efcData.netProfitLossBusinessFarm);
        Decimal selfEmploymentTaxCalculated = businessFarmProfit * (selfEmploymentTaxRate / 100);
        if(selfEmploymentTaxCalculated < 0) selfEmploymentTaxCalculated = 0; // Avoid negative self employment tax values
        return selfEmploymentTaxCalculated;
    }

    public Decimal calculateSelfEmploymentTaxAllowance(EfcWorksheetData efcData) {

        // [CH] NAIS-1850 Adding logic for calculation of line 26 on the FCW
        Decimal selfEmploymentTaxPaid = efcData.selfEmploymentTaxPaid; // NAIS-2414 [CH] refactored to maintain a null value instead of replacing with 0
        // Decimal selfEmploymentTaxPaid = EfcUtil.nullToZero(efcData.selfEmploymentTaxPaid);
        Decimal selfEmploymentTaxCalc = EfcUtil.nullToZero(efcData.selfEmploymentTaxCalculated);

        // If there is no value for selfEmploymentTaxPaid then use the calculated value selfEmploymentTaxCalc
        Decimal selfEmploymentTaxToUse = selfEmploymentTaxPaid != null ? selfEmploymentTaxPaid : selfEmploymentTaxCalc; // NAIS-2414 [CH] refactored to use null instead of 0
        Decimal deductiblePartOfSETax = EfcUtil.nullToZero(efcData.deductiblePartOfSelfEmploymentTax);

        // SE Tax Allowance Remainder
        Decimal selfEmploymentTaxAllowanceRemainder = selfEmploymentTaxToUse - deductiblePartOfSETax;

        if (selfEmploymentTaxAllowanceRemainder < 0) {
            selfEmploymentTaxAllowanceRemainder = 0;
        }
        return selfEmploymentTaxAllowanceRemainder;

    }

    public Decimal calculateStateOtherTaxAllowance(EfcWorksheetData efcData) {

        // check for required fields
        if (efcData.academicYearId == null) {throw new EfcException('The academic year was not specified.');}

            /* NAIS-1464 - The case where the State value is null is now handled below
            // NAIS-980 - if the state is not found, then return 0 instead of throwing an exception
            //throw new EfcException('The parent state was not specified.');
            if (efcData.parentState == null) {
                return 0;
            }
            */

        // [CH] NAIS-1464 - Updating to look up the 'Not Reported' tax entry where the country
        //                   is outside of the US, or the state field is not populated

        // [CH] NAIS-1659 - Adjusting logic to handle correct defaulting for countries other than US

        // state tax
        Decimal stateIncomeTax = 0;
        // If there is an income value to look up
        if (efcData.totalIncome != null) {

            stateIncomeTax = TableStateIncomeTax.calculateTax(efcData.academicYearId, efcData.parentState, efcData.parentCountry, efcData.totalIncome);

                /*
                // If it's outside the US, it doesn't matter what the state values is
                if(efcData.parentCountry == 'United States' || efcData.parentCountry == '' || efcData.parentCountry == null){
                    stateIncomeTax = TableStateIncomeTax.calculateTax(efcData.academicYearId, 'Not Reported', efcData.totalIncome);
                }
                // If it's in the US and a state was provided
                else if(efcData.parentCountry == 'United States' && efcData.parentState != null && efcData.parentState != ''){
                    stateIncomeTax = TableStateIncomeTax.calculateTax(efcData.academicYearId, efcData.parentState, efcData.totalIncome);
                }
                // If it's in the US and a state was not provided then just return 0
                */
        }

        // negative values should be treated as 0
        if (stateIncomeTax < 0) {
            stateIncomeTax = 0;
        }

        return stateIncomeTax;
    }

    // NAIS-569 - Replace original employment allowance calc with simplified version
        /*
            Code Removed 4/20/2015
        */

    public Decimal calculateEmploymentAllowance(EfcWorksheetData efcData) {

        // check for required fields
        if (efcData.academicYearId == null) {throw new EfcException('The academic year was not specified.');}
        if (efcData.familyStatus == null) {throw new EfcException('The Family Status was not specified');}

        // input values
        Decimal salaryWagesA = EfcUtil.nullToZero(efcData.salaryWagesParentA);
        Decimal salaryWagesB = EfcUtil.nullToZero(efcData.salaryWagesParentB);

        Decimal employmentAllowance = 0;

        //1. Single parent
        if (efcData.familyStatus == EfcPicklistValues.FAMILY_STATUS_1_PARENT) {
            //a. No business (FCW line 45 Business/Farm Owner = None), apply current rule based on Parent A salary
            if (String.isBlank(efcData.businessFarmOwner) && (salaryWagesA != null && salaryWagesA > 0)) {
                employmentAllowance = TableEmploymentAllowance.calculateAllowance(efcData.academicYearId, salaryWagesA);
                efcData.debugEmploymentAllowanceTargetIncome = salaryWagesA;
            }
            //b. If Bus/Farm Owner = Parent A, apply maximum EA, regardless of salary or net business income.
            else if (efcData.businessFarmOwner == EfcPicklistValues.BUSINESS_FARM_OWNER_PARENT_A) {
                employmentAllowance = EfcConstants.getEmploymentAllowanceMaximum(efcData.academicYearId);
            }
            //c. If not business owner, and no wages, zero EA
            else{
                employmentAllowance = 0;
            }
        }

        // [CH] NAIS-1463 Updating Employment Allowance calculation
        //2. 2 parents
        else if (efcData.familyStatus == EfcPicklistValues.FAMILY_STATUS_2_PARENT) {

            Boolean parentABusinessOwner = (efcData.businessFarmOwner==EfcPicklistValues.BUSINESS_FARM_OWNER_PARENT_A ||
                    efcData.businessFarmOwner==EfcPicklistValues.BUSINESS_FARM_OWNER_BOTH);

            Boolean parentAParticipating = (parentABusinessOwner || (salaryWagesA != null && salaryWagesA > 0));

            Boolean parentBBusinessOwner = (efcData.businessFarmOwner==EfcPicklistValues.BUSINESS_FARM_OWNER_PARENT_B ||
                    efcData.businessFarmOwner==EfcPicklistValues.BUSINESS_FARM_OWNER_BOTH);

            Boolean parentBParticipating = (parentBBusinessOwner || (salaryWagesB != null && salaryWagesB > 0));

            Integer totalParticipating = ((parentAParticipating ? 1 : 0) + (parentBParticipating ? 1 : 0));

            // If only one parent (or none) is engaged then NO ALLOWANCE
            if(totalParticipating == 1 || totalParticipating == 0){
                employmentAllowance = 0;
            }
            // If both parents are engaged, and either of them are business owners then MAX ALLOWANCE
            else if(totalParticipating == 2){
                if(parentABusinessOwner || parentBBusinessOwner){
                    employmentAllowance = EfcConstants.getEmploymentAllowanceMaximum(efcData.academicYearId);
                }
                // If both parents are engaged and neither is a business owner then calculate based on the greater Wage value
                else{
                    Decimal largerWageValue = (salaryWagesA >= salaryWagesB) ? salaryWagesA : salaryWagesB;
                    employmentAllowance = TableEmploymentAllowance.calculateAllowance(efcData.academicYearId, largerWageValue);
                }
            }

                /*  REMOVE THIS ONCE THIS IS APPROVED
                //a. No business
                if (efcData.businessFarmOwner == null) {
                    // If both Parents are working
                    if( salaryWagesA != null && salaryWagesA > 0 && salaryWagesB != null && salaryWagesB > 0 ){
                        Decimal allowanceIncome = (salaryWagesA >= salaryWagesB) ? salaryWagesA : salaryWagesB;
                        employmentAllowance = TableEmploymentAllowance.calculateAllowance(efcData.academicYearId, allowanceIncome);
                        efcData.debugEmploymentAllowanceTargetIncome = allowanceIncome;
                    }
                    // If one or both parents are not working
                    else{
                        employmentAllowance = 0;
                    }
                }

                //b. If Bus/Farm Owner = anything except None, apply maximum EA, regardless of salary or net business income.
                else {
                    // NAIS-2390 [CH] Updating to be more specific about only applying an allowance if the business owner does not have a W2 position
                    if(
                        (efcData.businessFarmOwner == EfcPicklistValues.BUSINESS_FARM_OWNER_PARENT_A && (salaryWagesA == null || salaryWagesA == 0)) ||
                        (efcData.businessFarmOwner == EfcPicklistValues.BUSINESS_FARM_OWNER_PARENT_B && (salaryWagesB == null || salaryWagesB == 0)) ||
                        (efcData.businessFarmOwner == EfcPicklistValues.BUSINESS_FARM_OWNER_BOTH && (salaryWagesA == null || salaryWagesA == 0 || salaryWagesB == null || salaryWagesB == 0))
                    ){
                        employmentAllowance = EfcConstants.getEmploymentAllowanceMaximum(efcData.academicYearId);
                    }
                    else{
                        employmentAllowance = 0;
                    }
                }
                */
        }

        return employmentAllowance;
    }



    public Decimal calculateMedicalAllowance(EfcWorksheetData efcData) {
        // check for required fields
        if (efcData.academicYearId == null) {throw new EfcException('The academic year was not specified.');}

        // inputs
        Decimal totalIncome = EfcUtil.nullToZero(efcData.totalIncome).round(RoundingMode.HALF_UP);
        Decimal medicalExpenses = EfcUtil.nullToZero(efcData.medicalDentalExpenses).round(RoundingMode.HALF_UP);

        // get the medical allowance threshold
        Decimal medicalAllowancePercent = EfcConstants.getMedicalDentalAllowancePercent(efcData.academicYearId);
        Decimal medicalAllowanceThreshold = (totalIncome * medicalAllowancePercent).round(RoundingMode.HALF_UP);

        // store the threshold for debugging
        efcData.debugMedicalAllowanceThreshold = medicalAllowanceThreshold;

        // calculate the medical allowance
        Decimal medicalAllowance = 0;
        if (medicalExpenses > medicalAllowanceThreshold) {
            medicalAllowance = medicalExpenses - medicalAllowanceThreshold;
        }

        // negative values should be treated as 0
        if (medicalAllowance < 0) {
            medicalAllowance = 0;
        }

        return medicalAllowance;
    }
}