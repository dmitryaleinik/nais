@isTest
private class TableFederalIncomeTaxTest
{

	@isTest
    private static void testTableFederalIncomeTax()
    {
        // create the academic year
        Academic_Year__c academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        
        // create the federal tax table
        TableTestData.populateFederalTaxTableData(academicYear.Id);
        
        // test lowest bracket
        System.assertEquals(1000, TableFederalIncomeTax.calculateTax(academicYear.Id, EfcPicklistValues.FILING_STATUS_MARRIED_JOINT, 10000, null));
        
        // test middle bracket
        System.assertEquals(19645, TableFederalIncomeTax.calculateTax(academicYear.Id, EfcPicklistValues.FILING_STATUS_HEAD_OF_HOUSEHOLD, 100000, null));
        
        // test highest bracket
        System.assertEquals(326761, TableFederalIncomeTax.calculateTax(academicYear.Id, EfcPicklistValues.FILING_STATUS_SINGLE, 1000000, null));
    }
}