/**
 * @description SPEC-092 Student Folder & Aid Allocation (Folder Summary),
 *              R-167, R-180, R-125, R-161, R-117 20-24 hours [Drew]
 *              The school folder is a VF page, with some folder level items and statuses,
 *              and links to the one or two PFS' within. The main folder page is also currently
 *              where the Aid Allocation is done. There are a number of editable fields which
 *              display the inputs to the aid decision and some can be revised as well.
 *              Need to populate the Tuition fields (4 of them) based on grade and the enrollment
 *              status, and day vs. boarding."
 *                  - School user should be able to easily navigate between sibling folders
 *                  - The UI should clearly differentiate folder information (which is relevant
 *                    to all PFS's) versus single PFS information
 *                  - School user should be able to obviously see when a folder has two PFS's in it
 *                  - School should be able to adjust the tuition & fees at the folder level which
 *                    will allow them to apply their own discounts for siblings, etc.
 *                  - A folder should have a visual indicator if the student is new or returning
 *                  - A school aid officer should be able to allocate financial aid from any number
 *                    of budgets they have specified.
 **/
public without sharing class SchoolFinancialAidController extends financialAidEditAllocationsBase implements SchoolAcademicYearSelectorStudInterface 
{

    /*  Initialization  */
    public SchoolFinancialAidController(ApexPages.StandardController stdController) {
        this();
    }//End:SchoolFinancialAidController

    public SchoolFinancialAidController() {
        // Load URL parameters
        folderId = System.currentPagereference().getParameters().get('id');
        seedFolderId = System.currentPagereference().getParameters().get('seedid'); // NAIS-1866
        firstName = System.currentPagereference().getParameters().get('firstname'); // NAIS-1866
        lastName = System.currentPagereference().getParameters().get('lastname'); // NAIS-1866
        birthDate = System.currentPagereference().getParameters().get('birthdate'); // NAIS-1866

        loadFolder();
        loadBudgets();
        calculateAmounts();
        newBudgetInit();

        loadBudgetGroups();

        if (useBudgets == null) {
            budgetAllocations = (budgetAllocations == null) ? new List<Budget_Allocation__c>() : budgetAllocations;
            budgetGroups = (budgetGroups == null) ? new List<Budget_Group__c>() : budgetGroups;

            // NEW LOGIC to handle case when grant has been awarded from the MyApplicants page - [dp] 6.17.14
            // default to use budgets if 1. there are budget groups and -- either 2. budget allocations exist or 3. grant awarded is null
            useBudgets = (budgetGroups.size() > 0 && (budgetAllocations.size() > 0 || folder.Grant_Awarded__c == null)) ? 'Yes' : 'No';

        }

        // allow the budget choice if there are budget groups
        allowBudgetChoice = budgetGroups != null && budgetGroups.size() > 0 ? true : false;
        budgetChange = false;

        String saveString = System.currentPagereference().getParameters().get('save');
        if (saveString != null && saveString == '1') {
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Save successful.'));
        }
    }//End:SchoolFinancialAidController
    /*End Initialization*/

    /*Properties*/
    public String folderId {get; set;}
    public String seedFolderId {get; set;}
    public String firstName {get; set;}
    public String lastName {get; set;}
    public String birthDate {get; set;}
    public School_PFS_Assignment__c spfs1 {get; set;}
    public School_PFS_Assignment__c spfs2 {get; set;}
    public Boolean currentYearAvailable {get; set;}
    public String currentYearName {get; set;}
    public String previousYearName {get; set;}
    public String existingFolderId {get; set;}
    public Contact applicantContact {get; set;} // [CH] NAIS-1866
    public Contact parentContact {get; set;} // [CH] NAIS-1866
    public Id selectedBudgetGroupId {get; set;}
    public integer seventyFiveAmount {get; set;}
    public integer fiftyAmount {get; set;}
    public integer twentyFiveAmount {get; set;}
    public integer customPercentage {get; set;}
    public integer customAmount {get; set;}

    private Account currentSchool 
    {
        get 
        {
            if (currentSchool == null) 
            {
                currentSchool = GlobalVariables.getCurrentSchool();
            }

            return currentSchool;
        }
        set;
    }//End:currentSchool

    // [CH] NAIS-1866
    public List<SelectOption> availableAcademicYears 
    {
        get 
        {
            if (availableAcademicYears == null) 
            {
                List<SelectOption> academicYearsForSorting = new List<SelectOption>{};
                availableAcademicYears = new List<SelectOption>{};
                String currentAcademicYearName;

                // NAIS-2417 [DP] 05.14.2015
                Map<String, Academic_Year__c> academicYearsMap = new Map<String, Academic_Year__c>();

                Map<String, Academic_Year__c> priorAcademicYears = new Map<String, Academic_Year__c>();
                
                for (Academic_Year__c yearRecord : GlobalVariables.getAllAcademicYears()) 
                {
                    if (yearRecord.Non_PFS_Folder_Open__c <= Date.today() && yearRecord.Non_PFS_Folder_Close__c >= Date.today()) 
                    {
                        if (currentAcademicYearName == null) 
                        {
                            currentAcademicYearName = yearRecord.Name;
                        }

                        if (currentYearName == null) 
                        { // If a current Year name has not been populated yet
                            currentYearName = yearRecord.Name;
                        }
                        else if (previousYearName == null)
                        { // If a current Year name has already been found
                            // Then use the next one in the ordered results as the previous year
                            previousYearName = yearRecord.Name;
                        }

                        academicYearsMap.put(yearRecord.Name, yearRecord);
                    }
                    
                    if (yearRecord.Name < currentYearName)
                    {
                        priorAcademicYears.put(yearRecord.Name, yearRecord);
                    }
                }

                if (priorAcademicYears.keySet().size() > 0)
                {
                    List<Annual_Setting__c> existingAnnualSettings = AnnualSettingsSelector.newInstance()
                        .selectBySchoolAndAcademicYear(new Set<Id>{currentSchool.Id}, priorAcademicYears.keySet());
                    
                    if (existingAnnualSettings != null && !existingAnnualSettings.isEmpty())
                    {

                        for (Annual_Setting__c annualSetting : existingAnnualSettings)
                        {
                            academicYearsMap.put(
                                annualSetting.Academic_Year_Name__c, priorAcademicYears.get(annualSetting.Academic_Year_Name__c));
                        }
                    }
                }

                if (academicYearsMap != null && academicYearsMap.values() != null && academicYearsMap.values().size() > 0) 
                {
                    if (folder != null)
                    {
                        for (Student_Folder__c folderRecord : [select Id, Academic_Year_Picklist__c from Student_Folder__c
                                                                where Student__c = :folder.Student__c
                                                                and School__c = :folder.School__c]) {
                            academicYearsMap.remove(folderRecord.Academic_Year_Picklist__c);
                        }
                    }

                    // If the current academic year is still available in the list
                    if (academicYearsMap.keySet().contains(currentAcademicYearName)) 
                    {
                        currentYearAvailable = true;
                    }


                    // First add everything to a list of Select Options with the Name in the
                    //  value spot so we can sort the list by Name
                    for (Academic_Year__c year : academicYearsMap.values()) 
                    {
                        academicYearsForSorting.add(new SelectOption(year.Name, year.Name));
                    }

                    if (folder != null && folder.Id != null) 
                    {
                        academicYearsForSorting.add(new SelectOption(folder.Academic_Year_Picklist__c, folder.Academic_Year_Picklist__c));
                    }

                    academicYearsForSorting.sort();

                    // Add rows to the final list, flipping value and label as we go
                    for (integer i=academicYearsForSorting.size()-1; i>=0; i--) 
                    {
                        SelectOption sortedOption = academicYearsForSorting[i];
                        //availableAcademicYears.add(new SelectOption(sortedOption.getLabel(), sortedOption.getValue()));
                        availableAcademicYears.add(sortedOption); // NAIS-2417 [DP] 05.13.2015 These are now the same, so no need to flip
                    }

                }
            }

            return availableAcademicYears;
        }
        set;
    }//End:availableAcademicYears

    // [CH] NAIS-1866
    public Boolean isPFSFolder{
        get {
            return (folder.Id != null && (folder.Folder_Source__c == null || folder.Folder_Source__c == 'PFS'));
        }
        set;
    }//End:isPFSFolder

    public Boolean isEditableYear {
        get {
            return GlobalVariables.isEditableYear(folder.Academic_Year_Picklist__c);
        }
        set;
    }//End:isEditableYear
    
    /**
    * @description Returns previous academic year folder along with the child budget allocation records and related budget group information.
    */
    public FinAidHistoryService.FinAidInfo finAidHistoryInfo {
        get {
            if(finAidHistoryInfo == null){
            
                finAidHistoryInfo =  FinAidHistoryService.Instance.getPreviousAwards(
                    folder.Student__c, folder.School__c, previousAcademicYearStr);
            }
            
            return finAidHistoryInfo;
        }
        
        private set;
    }
    
    public String previousAcademicYearStr {
        get {
            if(previousAcademicYearStr == null) {
                previousAcademicYearStr = GlobalVariables.getPreviousAcademicYearStr(folder.Academic_Year_Picklist__c);
            }
            return previousAcademicYearStr;
        }
        
        private set;
    }

    public double getCombinedContribution() {
        double d1 = 0;
        double d2 = 0;
        double dCombined = 0;
        if (spfs1 == null || spfs2 == null) {
            dCombined = 0;
        } else {
            // use Total Contribution unless it is null
            d1 = spfs1.Total_Contribution__c == null ? d1 : spfs1.Total_Contribution__c;

            // use Total Contribution unless it is null
            d2 = spfs2.Total_Contribution__c == null ? d2 : spfs2.Total_Contribution__c;

            dCombined = d1 + d2;
        }

        return dCombined;
    }//End:getCombinedContribution

    // Quick page indicates that this was loaded from the streamlined lite/quick page and requires slightly different functionality
    public boolean IsQuickPage {
        get {
            if (IsQuickPage == null) {
                IsQuickPage = ApexPages.currentPage().getParameters().get('page')=='quick';
            }
            return IsQuickPage;
        }
        set;
    }
    /*  End Properties  */


    /*Action Methods*/
    public pageReference cancel() {
        // [CH] NAIS-1866 Action method for School-Initied folder buttons
        if (folder.Id != null) {
            pageReference pr = Page.SchoolFolderSummary;
            pr.getParameters().put('id', folder.Id);
            return pr;
        }
        else{
            return new PageReference('/apex/SchoolNonNeedBased');
        }
    }//End:cancel
    public boolean isError { get; set; }

    public pageReference saveAndRecalc() {
        isError = false;
        amountErrorMsg = false;
        AmountErrorMessage = null;

        if (newBudgetAlloc.Amount_Allocated__c != null) {
            isError = true;
            amountErrorMsg = true;
            AmountErrorMessage = Label.AwardAmountNotSaved;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, Label.AwardAmountNotSaved));
            return null;
        }

        Savepoint sp = Database.setSavepoint();

        try {
            update applicantContact; // Adding to allow filling in Ethnicity value
            folder.Use_Budget_Groups__c = useBudgets;
            // SFP-1289: Make sure the ethnicity from the applicant contact is transfered to the folder. Typically, this is done by a future job executed during the contact trigger but somestimes the future job fails.
            folder.Ethnicity__c = applicantContact.Ethnicity__c;
            update folder; // NAIS-2233 [DP] 01.22.2015 moved up from line 188 so that it won't overwrite the Grant Awarded calculation that is calculated by Budget Allocation trigger

            if (useBudgets == 'Yes') {
                handleBudgetUpdates();
            }

            List<School_PFS_Assignment__c> spfsToUpdate = new List<School_PFS_Assignment__c>();
            spfsToUpdate = this.copyValuesToOriginalSPA(new List<School_PFS_Assignment__c>{spfs1, spfs2}, spfsToUpdate);

            if (!spfsToUpdate.isEmpty()) {
                update spfsToUpdate;
            }

            loadFolder();
            loadBudgets();
            calculateAmounts();
            budgetChange = false;

        } catch (Exception e) {
            
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an error saving: ' + e.getMessage()));
            Database.rollback(sp);
            isError = true;
            return null;
        }

        PageReference pr;
        if (IsQuickPage)
            pr = Page.AllocateAwardLite;
        else
             pr = Page.SchoolFinancialAid;

        pr.getParameters().put('id', folder.Id);
        pr.getParameters().put('save', '1');
        if (IsQuickPage)
            pr.getParameters().put('page','quick');
        pr.setRedirect(true);
        return pr;
    }//End:saveAndRecalc

    // [CH] NAIS-1866 Action method for School-Initied folder buttons
    public PageReference submit() {
        existingFolderId = null;

        if (applicantContact.Id == null) {
            // Check to see if there is already a Contact for the Student
            List<Contact> checkContacts = [select Id from Contact
                                                where FirstName = :applicantContact.FirstName
                                                and LastName = :applicantContact.LastName
                                                and Birthdate = :applicantContact.Birthdate 
                                                and RecordTypeId =: RecordTypes.studentContactTypeId];

            // If there is an existing Student Contact
            if (checkContacts != null && checkContacts.size() > 0) {
                // Update the existing Contact
                applicantContact.Id = checkContacts[0].Id;
            }
        }

        if (parentContact.Id == null) {
            // Check to see if there is already a Contact for the Parent
            List<Contact> checkContacts = [select Id from Contact
                                                    where FirstName = :parentContact.FirstName
                                                    and LastName = :parentContact.LastName
                                                    and Email = :parentContact.Email 
                                                    and RecordTypeId =: RecordTypes.parentContactTypeId];

            // If there is an existing Parent Contact
            if (checkContacts != null && checkContacts.size() > 0) {
                // Update the existing Contact
                parentContact.Id = checkContacts[0].Id;
            }
        }

        if (folder.Id == null) {
            // Check to see if there is an existing folder that matches the one they're about to insert
            List<Student_Folder__c> matchingFolders = [select Id from Student_Folder__c
                                                                where Student__c = :applicantContact.Id
                                                                and Academic_Year_Picklist__c = :folder.Academic_Year_Picklist__c
                                                                and School__c = :folder.School__c];

            // If a folder already exists
            if (matchingFolders != null && matchingFolders.size() > 0) {
                // display an error
                existingFolderId = matchingFolders[0].Id;
                return null;
            }
        }
        
        upsert new List<Contact>{applicantContact,parentContact};
        
        folder.Student__c = applicantContact.Id;
        folder.Primary_Parent__c = parentContact.Id;
        folder.Gender__c = applicantContact.Gender__c;
        folder.Birthdate__c = applicantContact.Birthdate;
        // SFP-1289: Make sure the ethnicity from the applicant contact is transfered to the folder. Typically, this is done by a future job executed during the contact trigger but somestimes the future job fails.
        folder.Ethnicity__c = applicantContact.Ethnicity__c;
        folder.Use_Budget_Groups__c = useBudgets;
        folder.Name = applicantContact.FirstName + ' ' + applicantContact.LastName + ' - ' + currentSchool.Name + ' Folder';
        folder.School__c = currentSchool.Id;

        upsert folder;

        if (useBudgets == 'Yes') {
            handleBudgetUpdates();
        }

        // Share the school folder with the school users
        Map<Id, Set<Id>> PFSsBySchoolId = new Map<Id, Set<Id>>();

        Map<Id, Set<Id>> studentFoldersBySchoolId = new Map<Id, Set<Id>>();
        studentFoldersBySchoolId.put(currentSchool.Id, new Set<Id> { folder.Id });

        Map<Id, Set<Id>> parentAccountsBySchoolId = new Map<Id, Set<Id>>();
        if (parentContact.AccountId == null) {
            List<Contact> parentContacts = [SELECT AccountId FROM Contact WHERE Id = :parentContact.Id AND AccountId != null];
            if (!parentContacts.isEmpty()) {
                parentAccountsBySchoolId.put(currentSchool.Id, new Set<Id>{ parentContacts[0].AccountId });
            }
        } else {
            parentAccountsBySchoolId.put(currentSchool.Id, new Set<Id>{ parentContact.AccountId });
        }

        SchoolStaffShareAction.shareRecordsWithSchools(PFSsBySchoolId, studentFoldersBySchoolId, parentAccountsBySchoolId, true);

        return new PageReference('/apex/SchoolFolderSummary?id=' + folder.Id);
    }//End:submit

    // [CH] NAIS-1866 Action method for School-Initied folder buttons
    public PageReference submitAndAdd() {
        submit();
        return new PageReference('/apex/SchoolNonNeedBased');
    }//End:submitAndAdd
    /*End Action Methods*/

    /* Interface Implementation */
    // used for the acad year selector
    public SchoolFinancialAidController Me {
        get { return this; }
    }

    // on change of academic year, reload page with new folder
    public PageReference SchoolAcademicYearSelectorStudent_OnChange(PageReference newPage) {
        return newPage;
    }
    /* End Interface Implementation */

    /*Helper Methods*/

    public void calculateAmounts() {
        Integer financialAid = folder.Financial_Need__c == null ? 0 : Integer.valueOf(folder.Financial_Need__c);

        seventyFiveAmount = financialAid * 75 / 100;
        fiftyAmount = financialAid * 50 / 100;
        twentyFiveAmount = financialAid * 25 / 100;
        customAmount = null;
        customAmount = customPercentage == null ? null : (financialAid * customPercentage) / 100;
    }

    // [CH] NAIS-1866 Adding support for creating a school-initiated folder
    public void loadFolder() {
        if (folderId != null) { // Normal PFS folder path
            folder = queryStudentFolder(folderId);

            applicantContact = createApplicantContact(folder);
            // NAIS-1866
            if (folder.Folder_Source__c == 'School-Initiated') {
                parentContact = queryParentContact();
            }

            spfs1 = folder.School_PFS_Assignments__r.size() > 0 ? folder.School_PFS_Assignments__r[0] : null;
            spfs2 = folder.School_PFS_Assignments__r.size() > 1 ? folder.School_PFS_Assignments__r[1] : null;
        } else if (seedFolderId != null) { // [CH] NAIS-1866 Accept a selected existing folder as a seed record
            Student_Folder__c existingFolder = queryStudentFolder(seedFolderId);

            folder = existingFolder.clone(false, true, false, false);
            // folder = new Student_Folder__c();

            folder.Folder_Source__c = 'School-Initiated';
            folder.Folder_Status__c = 'Submitted';
            folder.Grade_Applying__c = null;

            // Blanking out Status section
            folder.New_Returning__c = null;
            folder.Admission_Enrollment_Status__c = null;
            folder.Admission_Status_Date__c = null;
            folder.Day_Boarding__c = null;

            // Blanking out Financial Aid section
            folder.Grant_Awarded__c = null;
            folder.Other_Aid__c = null;
            folder.Other_Aid_Provider__c = null;
            folder.Other_Aid_Type__c = null;
            folder.Tuition_and_Expense_Notes_Long__c = null;
            folder.Use_Budget_Groups__c = 'No';

            if (availableAcademicYears != null && availableAcademicYears.size() > 0) {
                folder.Academic_Year_Picklist__c = availableAcademicYears[0].getValue();
            }

            spfs1 = existingFolder.School_PFS_Assignments__r.size() > 0 ? existingFolder.School_PFS_Assignments__r[0] : null;
            spfs2 = existingFolder.School_PFS_Assignments__r.size() > 1 ? existingFolder.School_PFS_Assignments__r[1] : null;

            applicantContact = createApplicantContact(existingFolder);
            parentContact = queryParentContact();
        } else { // [CH] NAIS-1866 If an existing folder is not found

            String academicYearName = null;
            if (availableAcademicYears != null && availableAcademicYears.size() > 0) {
                academicYearName = availableAcademicYears[0].getValue();
            }

            folder = new Student_Folder__c(
                Folder_Source__c = 'School-Initiated',
                School__c = GlobalVariables.getCurrentSchoolId(),
                Academic_Year_Picklist__c = academicYearName,
                Folder_Status__c = 'Submitted',
                Use_Budget_Groups__c = 'No'
            );

            Date birthdateDate = null;
            if (birthDate != null) {
                birthdateDate = Date.valueOf(birthDate);
            }

            applicantContact = new Contact(
                                    FirstName = firstName,
                                    LastName = lastName,
                                    Birthdate = birthdateDate,
                                    RecordTypeId = RecordTypes.studentContactTypeId
            );
            parentContact = new Contact(RecordTypeId = RecordTypes.parentContactTypeId);
        }

        useBudgets = folder.Use_Budget_Groups__c; // [CH] NAIS-1866 Consolidating from constructor
    }

    // [CH] NAIS-1866 Separating into new method to support re-calling on Academic Year change
    public void loadBudgetGroups() {
        // [CH] NAIS-1866 Adjusting to run for non need-based type folder
        Id AccountId;
        if (spfs1 != null) {
            AccountId = spfs1.School__c;
        } else {
            AccountId = folder.School__c;
        }

        budgetGroups = [Select Id, Name from Budget_Group__c where School__c = :AccountId AND Academic_Year__r.Name = :folder.Academic_Year_Picklist__c order by Name asc];
        budgetGroupIdToBudgetGroupMap = new Map<Id, Budget_Group__c>(budgetGroups);
    }

    // [CH] NAIS-1866
    private Contact createApplicantContact(Student_Folder__c sourceFolder) {
            return new Contact(
                Id = sourceFolder.Student__c,
                FirstName = sourceFolder.Student__r.FirstName,
                LastName = sourceFolder.Student__r.LastName,
                Birthdate = Date.valueOf(sourceFolder.Student__r.Birthdate),
                Gender__c = sourceFolder.Student__r.Gender__c,
                Ethnicity__c = sourceFolder.Student__r.Ethnicity__c
            );


    }

    // [CH] NAIS-1866 Gets the primary parent record for the folder
    private Contact queryParentContact() {

        Contact returnContact = new Contact();

        // If there's a specified Primary Parent then
        if (folder.Primary_Parent__c != null) {
            returnContact = new Contact(
                Id = folder.Primary_Parent__c,
                AccountId = folder.Primary_Parent__r.AccountId,
                FirstName = folder.Primary_Parent__r.FirstName,
                LastName = folder.Primary_Parent__r.LastName,
                MailingStreet = folder.Primary_Parent__r.MailingStreet,
                MailingCity = folder.Primary_Parent__r.MailingCity,
                MailingStateCode = folder.Primary_Parent__r.MailingStateCode,
                MailingPostalCode = folder.Primary_Parent__r.MailingPostalCode,
                MailingCountryCode = folder.Primary_Parent__r.MailingCountryCode,
                HomePhone = folder.Primary_Parent__r.HomePhone,
                Email = folder.Primary_Parent__r.Email
            );
        } else { // If there's no Primary Parent specified then look for the Parent A on the most recent PFS record
            List<Applicant__c> applicantRecords = [select Id, PFS__r.Parent_A__c, PFS__r.Parent_A__r.FirstName, PFS__r.Parent_A__r.LastName,
                                                        PFS__r.Parent_A__r.MailingStreet, PFS__r.Parent_A__r.MailingCity, PFS__r.Parent_A__r.MailingStateCode,
                                                        PFS__r.Parent_A__r.MailingPostalCode, PFS__r.Parent_A__r.MailingCountryCode,
                                                        PFS__r.Parent_A__r.Phone, PFS__r.Parent_A__r.HomePhone, PFS__r.Parent_A__r.Email, PFS__r.Parent_A__r.AccountId
                                                    from Applicant__c
                                                    where Contact__c = :folder.Student__c
                                                    order by PFS__r.CreatedDate desc];

            if (applicantRecords != null && applicantRecords.size() > 0) {
                returnContact = new Contact(
                    Id = applicantRecords[0].PFS__r.Parent_A__c,
                    AccountId = applicantRecords[0].PFS__r.Parent_A__r.AccountId,
                    FirstName = applicantRecords[0].PFS__r.Parent_A__r.FirstName,
                    LastName = applicantRecords[0].PFS__r.Parent_A__r.LastName,
                    MailingStreet = applicantRecords[0].PFS__r.Parent_A__r.MailingStreet,
                    MailingCity = applicantRecords[0].PFS__r.Parent_A__r.MailingCity,
                    MailingStateCode = applicantRecords[0].PFS__r.Parent_A__r.MailingStateCode,
                    MailingPostalCode = applicantRecords[0].PFS__r.Parent_A__r.MailingPostalCode,
                    MailingCountryCode = applicantRecords[0].PFS__r.Parent_A__r.MailingCountryCode,
                    Phone = applicantRecords[0].PFS__r.Parent_A__r.Phone,
                    HomePhone = applicantRecords[0].PFS__r.Parent_A__r.HomePhone,
                    Email = applicantRecords[0].PFS__r.Parent_A__r.Email
                );
            }
        }
        
        return returnContact;
    }

    // [CH] NAIS-1866 Moved query into a method to support querying based on different parameters
    private Student_Folder__c queryStudentFolder(Id folderIdToQuery) {

        // NAIS-1782 Added Day_Boarding__c, Orig_Est_Family_Contribution_Boarding__c, and Est_Family_Contribution_Boarding__c fields to query
        return [Select Admission_Status_Sub_Category__c, Aid_Award_Appealed__c, Date_Award_Letter_Sent__c, Award_Letter_Sent__c,Folder_Status__c, New_Returning__c, Admission_Enrollment_Status__c, Admission_Status_Date__c, Award_Decision_Date__c,
                        Tax_Return_Reviewed__c, Tuition_and_Expense_Notes_Long__c, Misc_Discounts__c, Other_Aid_Notes__c,
                        Student_Tuition__c, Travel_Expenses__c, Misc_Expenses_1__c, Misc_Expenses_2__c, Total_Tuition_and_Expenses__c,
                        Financial_Need__c, Day_Boarding__c, Grant_Awarded__c, Folder_Status_Date__c,
                        Other_Aid__c, Other_Aid_Type__c, Other_Aid_Provider__c, Tuition_Remission__c,
                        Total_Aid__c, Unmet_Financial_Need__c, Student__c,
                        Grade_Applying__c,Faculty_Child__c,Folder_Source__c, School__c, // [CH] NAIS-1866
                        Aid_of_Tuition__c, Aid_of_Total_Expenses__c, Aid_of_Financial_Need__c,
                        Student__r.FirstName, Student__r.LastName, Student__r.Birthdate, Student__r.Gender__c, Student__r.Ethnicity__c, // [CH] NAIS-1866
                        Primary_Parent__c, Primary_Parent__r.FirstName, Primary_Parent__r.LastName, Primary_Parent__r.MailingStreet, Primary_Parent__r.MailingCity,// [CH] NAIS-1866
                        Primary_Parent__r.MailingState, Primary_Parent__r.MailingPostalCode, Primary_Parent__r.MailingCountry, Primary_Parent__r.HomePhone,
                        Primary_Parent__r.MailingStateCode, Primary_Parent__r.MailingCountryCode, Primary_Parent__r.Email, // [CH] NAIS-1866
                        Primary_Parent__r.AccountId, Award_Accepted_Date__c, Award_Accepted_By__c, Award_Denied_Date__c, Award_Denied_By__c, 
                        Academic_Year_Picklist__c, Use_Budget_Groups__c, New_Student_Expenses__c, Returning_Student_Expenses__c, Student_Fees__c, Proposed_Award__c,
                            (Select Family_Contribution__c, Other_Contribution__c, Total_Contribution__c, Salary_Wages_Parent_A__c,
                                Applicant__r.PFS__r.Tuition_Able_to_Pay_All_Children__c, Orig_Parent_Can_Afford_to_Pay__c,
                                Est_Family_Contribution__c, Est_Family_Contribution_Boarding__c, Orig_Est_Family_Contribution__c,
                                Orig_Est_Family_Contribution_Boarding__c, School__c, Day_Boarding__c, // [CH] NAIS-1866  Day_Boarding_Form__c
                                Applicant__r.PFS__r.Parent_A__r.AccountId
                                from School_PFS_Assignments__r
                                order by Applicant__r.PFS__r.CreatedDate asc,
                                    Applicant__r.PFS__r.PFS_Number__c asc)
                        FROM Student_Folder__c
                        where Id = :folderIdToQuery
                        limit 1];
    }

    /**
    * @description Method implemented to copy values from VF Page to SPA record in database.
    * This method was implemented to avoid the case where a school has two tabs open, one for
    * financial aid and one with the FCW and they make changes in the FCW and then edit the
    * Financial Aid page in the other tab, it overwrites the EFC values (doesn't do a recalc).
    * @param records Copy of the records initialized when this controller was loaded.
    * @param spfsToUpdate List of spas to update.
    *
    * @return The record to be save to the database.
    */
    private List<School_PFS_Assignment__c> copyValuesToOriginalSPA(List<School_PFS_Assignment__c> records,
                                                            List<School_PFS_Assignment__c> spfsToUpdate) {
        for (School_PFS_Assignment__c record:records) {
            if (record!=null && record.Id!=null) {
                /*IMPORTANT: Every SPA's record added to this page as an input should be included in this query*/
                School_PFS_Assignment__c result = [Select Family_Contribution__c, Other_Contribution__c
                                                        from School_PFS_Assignment__c where Id=:record.Id limit 1];
                if (record.Family_Contribution__c != result.Family_Contribution__c
                || record.Other_Contribution__c != result.Other_Contribution__c) {
                    result.Family_Contribution__c = record.Family_Contribution__c;
                    result.Other_Contribution__c = record.Other_Contribution__c;
                    spfsToUpdate.add(result);
                }
            }
        }
        return spfsToUpdate;
    }//End:copyValuesToOriginalSPA
    
    public PageReference onChangeAwardLetterSent() {
        
        if (folder.Award_Letter_Sent__c!='Yes') {
            folder.Date_Award_Letter_Sent__c = null;
        }
        
        return null;
    }
    /*End Helper Methods*/
}