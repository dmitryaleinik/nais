/**
 * @description This class is used to create Business Farm records for unit tests.
 */
@isTest
public class BusinessFarmTestData extends SObjectTestData {
    @testVisible private static final String BF_NAME = 'Business Farm Name';

    /**
     * @description Get the default values for the Business_Farm__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Business_Farm__c.Name => BF_NAME,
                Business_Farm__c.PFS__c => PfsTestData.Instance.DefaultPfs.Id
        };
    }

    /**
     * @description Set the Business_Net_Profit_Share_Current__c field on the current Business Farm record.
     * @param netProfits The net profits to set on the Business Farm.
     * @return The current working instance of BusinessFarmTestData.
     */
    public BusinessFarmTestData withNetProfits(Decimal netProfits) {
        return (BusinessFarmTestData)with(Business_Farm__c.Business_Net_Profit_Share_Current__c, netProfits);
    }

    /**
     * @description Set the Business_Gross_Sales_Current__c field on the current Business Farm record.
     * @param grossSales The gross sales to set on the Business Farm.
     * @return The current working instance of BusinessFarmTestData.
     */
    public BusinessFarmTestData withGrossSales(Decimal grossSales) {
        return (BusinessFarmTestData)with(Business_Farm__c.Business_Gross_Sales_Current__c, grossSales);
    }

    /**
     * @description Set the Name field on the current Business Farm record.
     * @param name The Name to set on the Business Farm.
     * @return The current working instance of BusinessFarmTestData.
     */
    public BusinessFarmTestData forName(String name) {
        return (BusinessFarmTestData) with(Business_Farm__c.Name, name);
    }

    /**
     * @description Set the PFS__c field on the current Business Farm record.
     * @param pfsId The Pfs Id to set on the Business Farm.
     * @return The current working instance of BusinessFarmTestData.
     */
    public BusinessFarmTestData forPfsId(Id pfsId) {
        return (BusinessFarmTestData) with(Business_Farm__c.PFS__c, pfsId);
    }

    /**
     * @description Set the Sequence_Number__c field on the current Business Farm record.
     * @param sequenceNumber The Sequence Number to set on the Business Farm.
     * @return The current working instance of BusinessFarmTestData.
     */
    public BusinessFarmTestData forSequenceNumber(Integer sequenceNumber) {
        return (BusinessFarmTestData) with(Business_Farm__c.Sequence_Number__c, sequenceNumber);
    }

    /**
     * @description Set the Business_or_Farm__c field on the current Business Farm record.
     * @param businessOrFarm The Business or Farm to set on the Business Farm.
     * @return The current working instance of BusinessFarmTestData.
     */
    public BusinessFarmTestData forBusinessOrFarm(String businessOrFarm) {
        return (BusinessFarmTestData) with(Business_Farm__c.Business_or_Farm__c, businessOrFarm);
    }

    /**
     * @description Set the Business_Entity_Type__c field on the current Business Farm record.
     * @param businessEntityType The Business Entity Type to set on the Business Farm.
     * @return The current working instance of BusinessFarmTestData.
     */
    public BusinessFarmTestData forBusinessEntityType(String businessEntityType) {
        return (BusinessFarmTestData) with(Business_Farm__c.Business_Entity_Type__c, businessEntityType);
    }

    /**
     * @description Set the Business_Assets_Current__c field on the current Business Farm record.
     * @param businessAssetsCurrent The current business assets to set on the Business Farm.
     * @return The current working instance of BusinessFarmTestData.
     */
    public BusinessFarmTestData forBusinessAssetsCurrent(Decimal businessAssetsCurrent)
    {
        return (BusinessFarmTestData) with(Business_Farm__c.Business_Assets_Current__c, businessAssetsCurrent);
    }

    /**
     * @description Set the Business_Bldg_Mortgage_Current__c field on the current Business Farm record.
     * @param businessBldgMortgageCurrent The current business bldg mortgage to set on the Business Farm.
     * @return The current working instance of BusinessFarmTestData.
     */
    public BusinessFarmTestData forBusinessBldgMortgageCurrent(Decimal businessBldgMortgageCurrent)
    {
        return (BusinessFarmTestData) with(Business_Farm__c.Business_Bldg_Mortgage_Current__c, businessBldgMortgageCurrent);
    }

    /**
     * @description Set section status fields of the Business Farm record to complete(true) status.
     * @return The current instance of BusinessFarmTestData.
     */
    public BusinessFarmTestData asCompleted()
    {
        return (BusinessFarmTestData)  
             with(Business_Farm__c.statBusinessInformation__c, true)
            .with(Business_Farm__c.statBusinessIncome__c, true)
            .with(Business_Farm__c.statBusinessExpenses__c, true)
            .with(Business_Farm__c.statBusinessAssets__c, true);
    }

    /**
     * @description Insert the current working Business_Farm__c record.
     * @return The currently operated upon Business_Farm__c record.
     */
    public Business_Farm__c insertBusinessFarm() {
        return (Business_Farm__c)insertRecord();
    }

    /**
     * @description Create the current working Business Farm record without resetting
     *             the stored values in this instance of BusinessFarmTestData.
     * @return A non-inserted Business_Farm__c record using the currently stored field
     *             values.
     */
    public Business_Farm__c createBusinessFarmWithoutReset() {
        return (Business_Farm__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Business_Farm__c record.
     * @return The currently operated upon Business_Farm__c record.
     */
    public Business_Farm__c create() {
        return (Business_Farm__c)super.buildWithReset();
    }

    /**
     * @description The default Business_Farm__c record.
     */
    public Business_Farm__c DefaultBusinessFarm {
        get {
            if (DefaultBusinessFarm == null) {
                DefaultBusinessFarm = createBusinessFarmWithoutReset();
                insert DefaultBusinessFarm;
            }
            return DefaultBusinessFarm;
        }
        private set;
    }

    /**
     * @description Get the Business_Farm__c SObjectType.
     * @return The Business_Farm__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Business_Farm__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static BusinessFarmTestData Instance {
        get {
            if (Instance == null) {
                Instance = new BusinessFarmTestData();
            }
            return Instance;
        }
        private set;
    }

    private BusinessFarmTestData() { }
}