/**
 * Created by hansen on 7/15/16.
 */

public class Dml {

    public static DmlWithoutSharing WithoutSharing {
        get {
            if(WithoutSharing == null) {
                WithoutSharing = new DmlWithoutSharing();
            }
            return WithoutSharing;
        }
        private set;
    }

    public without sharing class DmlWithoutSharing {

        public void insertObjects(SObject obj) {
            insertObjects(new List<SObject> { obj });
        }

        public void insertObjects(List<SObject> objects) {
            insert objects;
        }

        public void updateObjects(List<SObject> objects) {
            update objects;
        }

        public void updateObjects(SObject obj) {
            updateObjects(new List<SObject>{obj});
        }

        public void upsertObjects(list<sObject> objects){
            upsert objects;
        }
        
        public void upsertObjects(list<sObject> objects, Schema.SObjectField f){
            Database.upsert(objects,f);
        }
        
        public void deleteObjects(List<SObject> obj) {
            delete obj;
        }
    }

}