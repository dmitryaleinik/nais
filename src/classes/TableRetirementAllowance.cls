public class TableRetirementAllowance
{
    
    private static Map<Id, List<Retirement_Allowance__c>> retirementAllowanceDataByYear = new Map<Id, List<Retirement_Allowance__c>>();
    
    public class AllowanceData {
        public Decimal allowance;
        public Decimal conversionCoefficient;
    }
    
    private static List<Retirement_Allowance__c> getRetirementAllowanceData(Id academicYearId) {
        List<Retirement_Allowance__c> retirementAllowanceData = retirementAllowanceDataByYear.get(academicYearId);
        if (retirementAllowanceData == null) {
            retirementAllowanceData = [SELECT Id, Age_High__c, Age_Low__c, Allowance_One_Parent_Family__c,
                                                Allowance_Two_Parent_Family__c, Conversion_Coefficient_One_Parent_Fam__c,
                                                Conversion_Coefficient_Two_Parent_Fam__c 
                                        FROM Retirement_Allowance__c 
                                        WHERE Academic_Year__c = :academicYearId
                                        ORDER BY Age_Low__c ASC];
            retirementAllowanceDataByYear.put(academicYearId, retirementAllowanceData);
        }    
        return retirementAllowanceData;
    } 
    
    public static AllowanceData getRetirementAllowance(Id academicYearId, String familyStatus, Integer age) {
        AllowanceData allowanceData = new AllowanceData();
        List<Retirement_Allowance__c> retirementAllowanceData = getRetirementAllowanceData(academicYearId);
        if (age != null) {
            Retirement_Allowance__c matchingRetirementAllowance = null;
            for (Retirement_Allowance__c retirementAllowance : retirementAllowanceData) {
                if (age >= retirementAllowance.Age_Low__c) {
                    if ((retirementAllowance.Age_High__c == null) || (age <= retirementAllowance.Age_High__c)) {
                        matchingRetirementAllowance = retirementAllowance;
                        break;
                    }
                }
            }
            if (matchingRetirementAllowance != null) {
                if (familyStatus == EfcPicklistValues.FAMILY_STATUS_1_PARENT) {
                    allowanceData.allowance = matchingRetirementAllowance.Allowance_One_Parent_Family__c;
                    allowanceData.conversionCoefficient = matchingRetirementAllowance.Conversion_Coefficient_One_Parent_Fam__c;
                }
                else if (familyStatus == EfcPicklistValues.FAMILY_STATUS_2_PARENT) {
                    allowanceData.allowance = matchingRetirementAllowance.Allowance_Two_Parent_Family__c;
                    allowanceData.conversionCoefficient = matchingRetirementAllowance.Conversion_Coefficient_Two_Parent_Fam__c;
                }
            }
        }
        
        if (allowanceData.allowance == null) {
            throw new EfcException.EfcMissingDataException('No Retirement Allowance found for academic year \'' + EfcUtil.getAcademicYearName(academicYearId) + '\' for family status = ' + familyStatus + ' and age = ' + age);
        }
        if (allowanceData.conversionCoefficient == null) {
            throw new EfcException.EfcMissingDataException('No Retirement Allowance Conversion Coefficient found for academic year \'' + EfcUtil.getAcademicYearName(academicYearId) + '\' for family status = ' + familyStatus + 'and age = ' + age);
        } 
        
        return allowanceData;
    }
}