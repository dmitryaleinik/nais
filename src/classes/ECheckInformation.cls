/**
 * @description E Check payment information.
 */
public class ECheckInformation extends PaymentInformation {
    public String AccountType { get; set; }
    public String AccountNumber { get; set; }
    public String BankName { get; set; }
    public String CheckNumber { get; set; }
    public String CheckType { get; set; }
    public String NameOnCheck { get; set; }
    public String RoutingNumber { get; set; }
}