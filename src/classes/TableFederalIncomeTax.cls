public with sharing class TableFederalIncomeTax
{
    
    private static List<Federal_Tax_Table__c> getFederalTaxTableData(Id academicYearId, String filingType)
    {
        List<Federal_Tax_Table__c> federalTaxTableData = FederalTaxTableCache.getTaxTables(academicYearId);

        List<Federal_Tax_Table__c> federalTaxTableDataFilteredByType = filterTablesByType(federalTaxTableData, filingType);

        return federalTaxTableDataFilteredByType;
    }

    public static Decimal calculateTax(Id academicYearId, String filingStatus, Decimal taxableIncome, String filingStatusParentB)
    {    
        // map from filing status to filing type
        String filingType = null;
        filingStatus = (EfcConstants.isFilingMarriedSeparated(filingStatus, filingStatusParentB)
                        ?EfcPicklistValues.FILING_STATUS_MARRIED_SEPARATE
                        :filingStatus);
                        
        if (filingStatus == EfcPicklistValues.FILING_STATUS_SINGLE)
        {
            filingType = EfcPicklistValues.FILING_TYPE_INDIVIDUAL_SINGLE;
        }
        else if (filingStatus == EfcPicklistValues.FILING_STATUS_MARRIED_JOINT
        || filingStatus == EfcPicklistValues.FILING_STATUS_QUALIFYING_WINDOW_WITH_CHILD)
        {
            filingType = EfcPicklistValues.FILING_TYPE_MARRIED_JOINT;
        }
        else if (filingStatus == EfcPicklistValues.FILING_STATUS_MARRIED_SEPARATE)
        {
            filingType = EfcPicklistValues.FILING_TYPE_MARRIED_SEPARATE;
        }
        else if (filingStatus == EfcPicklistValues.FILING_STATUS_HEAD_OF_HOUSEHOLD)
        {
            filingType = EfcPicklistValues.FILING_TYPE_INDIVIDUAL_HEAD_OF_HOUSEHOLD;
        }
        
        Decimal calculatedTax = null;
        if (taxableIncome != null)
        {            
            // make sure the taxable income is rounded to the nearest whole number
            Decimal roundedTaxableIncome = taxableIncome.round(RoundingMode.HALF_UP);
            
            List<Federal_Tax_Table__c> federalTaxTableData = getFederalTaxTableData(academicYearId, filingType);
            
            if (federalTaxTableData != null && !federalTaxTableData.isEmpty())
            {            
                Federal_Tax_Table__c matchingBracket = null;
                // assume the tax data is sorted in ascending order by the Taxable_Income_Low__c field
                for (Federal_Tax_Table__c federalTaxTable : federalTaxTableData)
                {            
                    if (roundedTaxableIncome >= federalTaxTable.Taxable_Income_Low__c)
                    {            
                        if ((federalTaxTable.Taxable_Income_High__c == null) || 
                            (roundedTaxableIncome <= federalTaxTable.Taxable_Income_High__c))
                        {                                            
                            matchingBracket = federalTaxTable;
                            break;
                        }
                    }
                }
                
                if (matchingBracket != null)
                {                    
                    Decimal taxableIncomeAboveThreshold = 0;
                    if ((matchingBracket.US_Income_Tax_Rate_Threshold__c != null) 
                                    && (roundedTaxableIncome > matchingBracket.US_Income_Tax_Rate_Threshold__c))
                    {
                        taxableIncomeAboveThreshold = roundedTaxableIncome - matchingBracket.US_Income_Tax_Rate_Threshold__c;
                    }
                    calculatedTax = (matchingBracket.US_Income_Tax_Base__c + (taxableIncomeAboveThreshold * matchingBracket.US_Income_Tax_Rate__c)).round(RoundingMode.HALF_UP);
                }
            }
        }

        if (calculatedTax == null)
        {
            throw new EfcException.EfcMissingDataException(
                'No tax table record found for academic year \'' + EfcUtil.getAcademicYearName(academicYearId) 
                + '\' for filing status = ' + filingStatus + ' and taxable income = ' + taxableIncome);
        }
        
        return calculatedTax;
    }

    private static List<Federal_Tax_Table__c> filterTablesByType(List<Federal_Tax_Table__c> federalTaxTables, String filingType)
    {
        List<Federal_Tax_Table__c> filteredTables = new List<Federal_Tax_Table__c>();

        for (Federal_Tax_Table__c federalTaxTable : federalTaxTables)
        {
            if (federalTaxTable.Filing_Type__c == filingType)
            {
                filteredTables.add(federalTaxTable);
            }
        }

        return filteredTables;
    }
}