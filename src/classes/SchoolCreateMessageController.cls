/**
 * SchoolCreateMessageController.cls
 *
 * @description: Server side controller for Mass Email create/edit message page, handles interactions w/ data model and view state manipulation.
 *
 * @author: Chase Logan @ Presence PG
 */
public class SchoolCreateMessageController {

    /* member vars and properties */
    public SchoolMassEmailSendModel massESModel { get; set; }

    public MassEmailUtil.EnablementSettings Settings {
        get {
            if (Settings == null) {
                Settings = MassEmailUtil.getFeatureSettings(GlobalVariables.getCurrentSchoolId(), UserInfo.getProfileId());
            }
            return Settings;
        }
        private set;
    }

    public Boolean massEmailEnabled {
        get {
            return Settings.IsEnabled;
        }
    }

    public Boolean SendEmailsPerApplicantEnabled {
        get {
            return Settings.IsSendEmailsPerApplicantEnabled;
        }
    }
    
    public Boolean massEmailAwardLetterIsEnabled {
        get {
            return FamilyAwardsService.Instance.isAwardLinkEnabledForCurrentSchool();
        }
        
        private set;
    }

    public Boolean renderPreview {
        get {
            if (this.renderPreview == null) {
                this.renderPreview = false;
            }
            return this.renderPreview;
        }
        set;
    }
    public Boolean discardDraft {
        get {
            if (this.discardDraft == null) {
                this.discardDraft = false;
            }
            return this.discardDraft;
        }
        set;
    }
    public Boolean advancedEditor {
        get {
            // Handle advanced RT editor or standard RT editor
            return FeatureToggles.isEnabled( this.EDITOR_FEATURE_TOGGLE_NAME);
        }
    }
    public String testToAddress { get; set; }
    public Boolean mergeSuccess { get; private set; } 
    public Set<String> mergeErrMessageSet { get; private set; }

    private Mass_Email_Send__c massES;
    private static Boolean initHasRun = false;

    /* class constants */
    public static final String QUERY_STRING_MESSAGE_DETAIL = 'messagedetailid';
    public static final String QUERY_STRING_PREVIEW_MSG = 'preview';
    
    private final String NAME_DRAFT = 'New Mass Email';
    private final String PREVIEW_TRUE = '1';
    private final String PREVIEW_FALSE = '0';
    private final String READY_STATUS = 'Ready to Send';
    private final String EDITOR_FEATURE_TOGGLE_NAME = 'Advanced_Mass_Email_Editor__c';

    private final String UNABLE_SEND_EXC_MSG = 'Unable to send email, please try again.';
    private final String TEST_SEND_EXC_MSG = 'Test email send failed, please try again.';
    
    // default ctor
    public SchoolCreateMessageController() {

        if ( !initHasRun) {

            // initialize view
            this.init();
            initHasRun = true;
        }
    }

    // return to dashboard
    public PageReference navigateBackToDashboard() {

        PageReference pRef = Page.SchoolMassEmailDashboard;
        pRef.setRedirect( true);

        return pRef;
    }

    // return to selected recips
    public PageReference navigateBackToRecips() {

        this.save();

        PageReference pRef = Page.SchoolSelectedRecipients;
        pRef.getParameters().put( SchoolSelectedRecipientsExtension.QUERY_STRING_MESSAGE_DETAIL, this.massESModel.id);
        pRef.setRedirect( true);

        return pRef;
    }

    // save record as draft
    public PageReference saveAsDraft() {

        if ( !this.discardDraft) {

            this.save();
        }
        
        return null;
    }

    // save message content and redirect to show preview modal
    // work around for no support of rerender on rich text areas
    public PageReference executePreview() {

        this.save();

        PageReference pRef = Page.SchoolCreateMessage;
        pRef.getParameters().put( SchoolCreateMessageController.QUERY_STRING_MESSAGE_DETAIL, this.massESModel.id);
        pRef.getParameters().put( SchoolCreateMessageController.QUERY_STRING_PREVIEW_MSG, this.PREVIEW_TRUE);
        pRef.setRedirect( true);

        return pRef;
    }

    // hide preview modal
    public PageReference hidePreview() {

        this.renderPreview = false;

        PageReference pRef = Page.SchoolCreateMessage;
        pRef.getParameters().put( SchoolCreateMessageController.QUERY_STRING_MESSAGE_DETAIL, this.massESModel.id);
        pRef.getParameters().put( SchoolCreateMessageController.QUERY_STRING_PREVIEW_MSG, this.PREVIEW_FALSE);
        pRef.setRedirect( true);

        return pRef;
    }

    // trigger test email send via email service, purely for seeing actual email content
    // this does not modify MES record status or populate any necessary webhook fields
    public PageReference sendTestEmail() {

        if ( this.massESModel != null) {

        	// validate any merge fields before save
        	this.mergeErrMessageSet = MassEmailUtil.verifyMergeFields( this.massESModel.htmlBody);
        	if ( this.mergeErrMessageSet.isEmpty()) {

        		// save any in-progress edits to message, 
	            // insert default logo and footer in its own transaction if necessary
	            this.mergeSuccess = true;
	            this.save();
	            MassEmailController meCon = new MassEmailController();
	            meCon.massEmailSendId = this.massESModel.id;

	            try {

	                EmailService.sendTestEmailAsync( this.massESModel.id, this.testToAddress);
	            } catch ( Exception e) {

	                System.debug( 'DEBUG:::exception occurred in SchoolCreateMessageController.sendTestEmail(), message: ' +
	                    e.getMessage() + ' ' + e.getStackTraceString());
	                ApexPages.addMessage( 
	                    new ApexPages.Message( ApexPages.Severity.ERROR, this.TEST_SEND_EXC_MSG));
	            }
        	} else {

        		// display merge error messages
        		this.mergeSuccess = false;
        		for (String msg : mergeErrMessageSet) {
        		    
        		    ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.ERROR, msg));
        		}
        	}
        }

        return null;
    }

    // trigger email send by updating status
    public PageReference sendEmail() {
        PageReference returnRef = null;

        try {

            if ( this.massESModel != null) {

            	// validate any merge fields before save
	        	this.mergeErrMessageSet = MassEmailUtil.verifyMergeFields( this.massESModel.htmlBody);
	        	if ( this.mergeErrMessageSet.isEmpty()) {

	        		this.mergeSuccess = true;
	        		this.massESModel.status = this.READY_STATUS;
                	this.save();
                	returnRef = this.navigateBackToDashboard();
	        	} else {

	        		// display merge error messages
	        		this.mergeSuccess = false;
	        	}
            } else {

                ApexPages.addMessage( 
                    new ApexPages.Message( ApexPages.Severity.ERROR, this.UNABLE_SEND_EXC_MSG));
            }

        } catch ( Exception e) {

            System.debug( 'DEBUG:::exception occurred in SchoolCreateMessageController.sendEmail(), message: ' +
                e.getMessage() + ' ' + e.getStackTraceString());
            ApexPages.addMessage( 
                    new ApexPages.Message( ApexPages.Severity.ERROR, this.UNABLE_SEND_EXC_MSG));
        }

        return returnRef;
    }

    // returns the currently configured image upload endpoint URL
    @RemoteAction
    public static RemoteResponseModel getAdvancedEditorDetails() {
        RemoteResponseModel returnResonse = new RemoteResponseModel();

        returnResonse.imgUploadURL = MassEmailUtil.getImageUploadURL();
        returnResonse.selectOptions = new Map<String,String>();

        Map<String, Mass_Email_Merge_Setting__c> mergeSettingMap = Mass_Email_Merge_Setting__c.getAll();
        if ( !mergeSettingMap.isEmpty()) {

        	List<Mass_Email_Merge_Setting__c> sortedList = mergeSettingMap.values();
        	sortedList.sort();
        	for ( Mass_Email_Merge_Setting__c m : sortedList) {

        		returnResonse.selectOptions.put( m.Display_Name__c, m.Merge_Name__c);
        	}	
        }

        return returnResonse;
    }

    /* private instance methods */

    // handles page initilization
    private void init() {

        // grab query string param(s) and attempt lookup of corresponding massES record
        String currentDetailId = 
            ApexPages.currentPage().getParameters().get( SchoolCreateMessageController.QUERY_STRING_MESSAGE_DETAIL);
        String displayPreview = 
            ApexPages.currentPage().getParameters().get( SchoolCreateMessageController.QUERY_STRING_PREVIEW_MSG);

        if ( String.isNotEmpty( currentDetailId)) {

            this.massES = new MassEmailSendDataAccessService().getMassEmailSendById( currentDetailId);
            this.massESModel = SchoolMassEmailSendModel.getModel( this.massES);
            this.mergeSuccess = true;
            this.mergeErrMessageSet = new Set<String>();

            // render preview if this a preview redirect
            if ( String.isNotEmpty( displayPreview) && displayPreview == this.PREVIEW_TRUE) {

                this.renderPreview = true;
            }
        }
    }

    // handles saving changes to local massES record
    private void save() {

        if ( this.massESModel != null) {
            
            if (this.massES.Visualforce_Template__c != defaultEmailTemplate) {
                
                 this.massES.Visualforce_Template__c = defaultEmailTemplate;
            }
            
            this.massESModel.save();
        }
    }
    
    private String defaultEmailTemplate {
        
        get {
            if (defaultEmailTemplate == null) {
                
                Mass_Email_Setting__mdt setting = [select Value__c from Mass_Email_Setting__mdt where DeveloperName = 'EmailTemplate'];
                defaultEmailTemplate = setting.Value__c;
            }
            
            return defaultEmailTemplate;
        }
        set;
    }


    /* Model wrapper classes */

    // Wrapper model for JSON response via remoting call
    public class RemoteResponseModel {

        public String imgUploadURL { get; set; }
        public Map<String,String> selectOptions { get; set; }

        RemoteResponseModel() {}
    }

}