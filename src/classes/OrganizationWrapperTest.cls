/**
 * Created by hansen on 3/30/16.
 */

@IsTest
private class OrganizationWrapperTest {
    @isTest
    private static void isSandbox_expectNotNull() {
        System.assertNotEquals(null, OrganizationWrapper.isSandbox(),
                'Expected isSandbox to return a non null Boolean.');
    }
}