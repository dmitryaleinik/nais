public without sharing class CongaPageController {

    public String partnerURL;
    public Contact dummyContact1 {get; set;}
    public Contact dummyContact2 {get; set;}

    /*Initialization*/
    public CongaPageController(){
        // stealing the partner server url from custom settings
        Databank_Settings__c databankSettings = Databank_Settings__c.getInstance('Databank');
        partnerURL = databankSettings.API_Partner_Server_URL__c;

        dummyContact1 = new Contact();
        dummyContact2 = new Contact();
        dynamicCongaURL = '';
    }
   
    /*End Initialization*/
   
    /*Properties*/
    public String getCongaURL(){
        
        String congaURL = 'https://www.appextremes.com/apps/Conga/PointMerge.aspx?sessionId='
                    + userInfo.getSessionId() +
                    '&serverUrl='
                    + partnerURL
                    //+ '%2Fservices%2FSoap%2Fu%2F8.0%2F' 
                    + UserInfo.getOrganizationId();
        
        return congaURL;
    } 

    public String dynamicCongaURL {get; set;}

    public PageReference launchCongaWithDates(){
        if (dummyContact1.Birthdate == null || dummyContact2.Birthdate == null){
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Both dates are required.'));
            return null;
        }

        String startDateYear = String.valueOf(dummyContact1.Birthdate.year());
        String startDateMonth = String.valueOf(dummyContact1.Birthdate.Month());
        String startDateDay = String.valueOf(dummyContact1.Birthdate.Day());
        startDateMonth = startDateMonth.length() == 1 ? '0' + startDateMonth : startDateMonth;
        startDateDay = startDateDay.length() == 1 ? '0' + startDateDay : startDateDay;
        String startDate = startDateYear + '-' + startDateMonth + '-' + startDateDay;

        String endDateYear = String.valueOf(dummyContact2.Birthdate.year());
        String endDateMonth = String.valueOf(dummyContact2.Birthdate.Month());
        String endDateDay = String.valueOf(dummyContact2.Birthdate.Day());
        endDateMonth = endDateMonth.length() == 1 ? '0' + endDateMonth : endDateMonth;
        endDateDay = endDateDay.length() == 1 ? '0' + endDateDay : endDateDay;
        String endDate = endDateYear + '-' + endDateMonth + '-' + endDateDay;

        String todayDateYear = String.valueOf(System.today().year());
        String todayDateMonth = String.valueOf(System.today().Month());
        String todayDateDay = String.valueOf(System.today().Day());
        todayDateMonth = todayDateMonth.length() == 1 ? '0' + todayDateMonth : todayDateMonth;
        todayDateDay = todayDateDay.length() == 1 ? '0' + todayDateDay : todayDateDay;
        String todayDate = todayDateYear + '-' + todayDateMonth + '-' + todayDateDay;


        String myURL = getCongaURL();
        myURL += dynamicCongaURL;
        myURL += '&dv0=' + startDate + '&dv1=' + endDate + '&dv2=' + todayDate;
        String pvDateParams = 'pv0=' + startDate + '~pv1=' + endDate;
        myURL = myURL.replace('pvTARGET', pvDateParams);
        /*
        // add master id and template id
        myUrl += '&Id=005d0000001L6FWAA0&TemplateId=a0yd0000000wQEy';
        // add query id with pv params
        myURL += '&QueryId=a0Td000000Jm3UZ?pv0=' + startDate + '~pv1=' + endDate;
        // add dvs
        myURL += '&dv0=' + startDate + '&dv1=' + endDate + '&dv2=' + todayDate;
        // add misc params
        myURL += '&OFN={Template.Label}&OCNR=1&DS7=3';
        */
        PageReference pr = new PageReference(myUrl);
        return pr;
    }
    
    

/*
    public String dynamicCongaURL1 {get; set;}
    public String dynamicCongaURL2 {get; set;}
    public String dynamicCongaURL3 {get; set;}
    public String dynamicCongaURL4 {get; set;}

    public pageReference conga1 {get; set;}
    public pageReference conga2 {get; set;}
    public pageReference conga3 {get; set;}
    public pageReference conga4 {get; set;}
    public Boolean displayCongaLinks {get; set;}

    public void generateCongaLinks(){
        if (dummyContact1.Birthdate == null || dummyContact2.Birthdate == null){
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Both dates are required.'));
            return;
        }

        String startDateYear = String.valueOf(dummyContact1.Birthdate.year());
        String startDateMonth = String.valueOf(dummyContact1.Birthdate.Month());
        String startDateDay = String.valueOf(dummyContact1.Birthdate.Day());
        startDateMonth = startDateMonth.length() == 1 ? '0' + startDateMonth : startDateMonth;
        startDateDay = startDateDay.length() == 1 ? '0' + startDateDay : startDateDay;
        String startDate = startDateYear + '-' + startDateMonth + '-' + startDateDay;

        String endDateYear = String.valueOf(dummyContact2.Birthdate.year());
        String endDateMonth = String.valueOf(dummyContact2.Birthdate.Month());
        String endDateDay = String.valueOf(dummyContact2.Birthdate.Day());
        endDateMonth = endDateMonth.length() == 1 ? '0' + endDateMonth : endDateMonth;
        endDateDay = endDateDay.length() == 1 ? '0' + endDateDay : endDateDay;
        String endDate = endDateYear + '-' + endDateMonth + '-' + endDateDay;

        String todayDateYear = String.valueOf(System.today().year());
        String todayDateMonth = String.valueOf(System.today().Month());
        String todayDateDay = String.valueOf(System.today().Day());
        todayDateMonth = todayDateMonth.length() == 1 ? '0' + todayDateMonth : todayDateMonth;
        todayDateDay = todayDateDay.length() == 1 ? '0' + todayDateDay : todayDateDay;
        String todayDate = todayDateYear + '-' + todayDateMonth + '-' + todayDateDay;


        String myURL = getCongaURL();
        myURL += '&dv0=' + startDate + '&dv1=' + endDate + '&dv2=' + todayDate;
        String conga1Sring = myURL += dynamicCongaURL1; 
        String conga2Sring = myURL += dynamicCongaURL2; 
        String conga3Sring = myURL += dynamicCongaURL3; 
        String conga4Sring = myURL += dynamicCongaURL4; 


        String pvDateParams = 'pv0=' + startDate + '~pv1=' + endDate;
        conga1Sring = conga1Sring.replace('pvTARGET', pvDateParams);
        conga2Sring = conga2Sring.replace('pvTARGET', pvDateParams);
        conga3Sring = conga3Sring.replace('pvTARGET', pvDateParams);
        conga4Sring = conga4Sring.replace('pvTARGET', pvDateParams);


        conga1 = new PageReference(conga1Sring);
        conga2 = new PageReference(conga2Sring);
        conga3 = new PageReference(conga3Sring);
        conga4 = new PageReference(conga4Sring);

        displayCongaLinks = true;
        //return null;
    }

*/

    /*End Properties*/
       
    /*Action Methods*/
   
    /*End Action Methods*/
   
    /*Helper Methods*/
   
    /*End Helper Methods*/

}