/* [CH] NAIS-1696 Converting page to accept a parameter for one academic year, and only display */

public with sharing class SchoolCurrentApplicantsController implements SchoolAcademicYearSelectorInterface {
    /*Initialization*/

	public SchoolCurrentApplicantsController() {}

	public void initCurrentYearApplicants() {
		mySchoolId = GlobalVariables.getCurrentSchoolId();

		// Get the table page size from the school portal settings.
		tablePageSize = SchoolPortalSettings.Fee_Waivers_Table_Page_Size;

		loadAcademicYear();
		loadAnnualSettings(getAcademicYearId());

		updateCurrentTable();
		showFeeWaiverConfirm = false;
	}

	public void loadAcademicYear() {

		// Due to the selector_onchange not refreshing properly, we redirect to the same page with academicyearid parameter.
		Map<String, String> parameters = ApexPages.currentPage().getParameters();
		if (parameters.containsKey('academicyearid'))
			setAcademicYearId(parameters.get('academicyearid'));
		else
				// [CH] NAIS-1666
				setAcademicYearId(GlobalVariables.getCurrentAcademicYear().Id);
		// myAcademicYear = GlobalVariables.getCurrentAcademicYear();
	}

	public Annual_Setting__c loadAnnualSettings(Id academicYearId) {
		List<Annual_Setting__c> annualSettings = GlobalVariables.getCurrentAnnualSettings(true, academicYearId);
		for (Annual_Setting__c setting : annualSettings) {
			if (setting.Academic_Year__c == academicYearId) {
				myAnnualSettings = setting;
			}
		}

		// Check Waivers_Assigned__c and Total_Waivers_Override_Default__c.
		if (myAnnualSettings.Waivers_Assigned__c == null) {
			myAnnualSettings.Waivers_Assigned__c = 0;
			//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Annual Settings.Waivers Assigned is empty.'));
		}
		if (myAnnualSettings.Total_Waivers_Override_Default__c == null) {
			myAnnualSettings.Total_Waivers_Override_Default__c = 0;
			//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Annual Settings.Waiver Credit Total is empty.'));
		}

		return myAnnualSettings;
	}

    /*End Initialization*/

    /*Properties*/
	public Academic_Year__c currentAcademicYear { get; private set; }
	public Annual_Setting__c myAnnualSettings { get; set; }
	public Id mySchoolId { get; private set; }
	public Integer tablePageSize;
	public SchoolCurrentApplicantsController Me { get { return this; } }
	public ApexPages.StandardSetController currentSetCtrl { get; set; }

	public Boolean showFeeWaiverConfirm {get; set;} // used to display popup confirmation

	public Integer getCurrentCurrentPageTopIndex() {
		return (currentSetCtrl.getPageNumber() - 1) * currentSetCtrl.getPageSize();
	}

	public List<PFSCheckWrapper> currentList {
		get {
			if(currentList == null) {
				currentList = WrapPFSSetCtrlRecords(currentSetCtrl, true);
			}
			return currentList;
		}
		set;
	}

	public String selectedPFSNumber { get; set; }
	public Integer iAssignedCount { get; set; } // Property mainly for tests, only valid after assignwaivers().
	//private Set<Id> currentYearFWRecipientIds;

	public Integer getNumOfNewWaivers() {
		Integer i = 0;
		if (currentList != null) {
			for (PFSCheckWrapper w : currentList) {
				if (w != null && w.isChecked && !w.isDisabled) { i++; } // NAIS-2116 [DP] 02.27.2015 need to check for null since list is only populated with one page's worth
			}
		}

		return i;
	}
    /*End Properties*/

	private Set<String> selectedApplicants = new Set<String>();
	private Map<String, PFSCheckWrapper> selectedApplicantsByPfsId = new Map<String, PFSCheckWrapper>();

    /*Action Methods*/
	// [CH] NAIS-2058 Copying in methods for Assigning Waivers
	public void doInitFeeWaiverConfirm() {
		showFeeWaiverConfirm = true;
	}

	public void doCancelFeeWaiverConfirm() {
		showFeeWaiverConfirm = false;
	}

	public PageReference assignwaivers() {

		List<Application_Fee_Waiver__c> insertAfwlList = new List<Application_Fee_Waiver__c>();
		List<Id> waivedContactIds = new List<Id>();
		Set<Id> pfsIdsForOpps = new Set<Id>(); // [CH] NAIS-2102 List of PFSs to get Sale Opportunities and TLIs

		iAssignedCount = 0;

		//Make sure we add any selected on the current page to the map of selected
		updateListOfSelectedApplicants(currentList);

		// Current List
		for (PFSCheckWrapper w : selectedApplicantsByPfsId.values()) {
			if ((w != null)
					&& (w.isChecked == true)
					&& ((w.feeWaived == null) || (w.feeWaived == 'No'))) {
				// [CH] NAIS-2102 Collect list of PFSs to get Sale Opportunities and TLIs
				pfsIdsForOpps.add(w.pfsId);

				// Applicaiton Fee Waiver
				Application_Fee_Waiver__c afw = new Application_Fee_Waiver__c();
				afw.Academic_Year__c = getAcademicYearId();
				// [CH] NAIS-1666 afw.Academic_Year__c = myAcademicYear.Id;
				afw.Account__c = mySchoolId;
				afw.Contact__c = w.parentA;
				afw.Person_Who_Offered_Waiver__c = GlobalVariables.getCurrentUser().Contact.Name;

				// SFP-190 - if application is submitted assign directly
				afw.Status__c = (w.pfsStatus == 'Application Submitted' ? 'Assigned' : 'Pending Qualification');

				insertAfwlList.add(afw);

				// PFS
				w.feeWaived = 'Yes';

				// [SL] NAIS-1032: fee waived flag is now set by the Opportunity trigger
				// [SL] NAIS-1032: only update the Fee_Waived__c field, to avoid overwriting other fields with old values

				iAssignedCount++;
			}
		}

		// if iAssignedCount <= how many we have
		if (iAssignedCount <= myAnnualSettings.Waiver_Credit_Balance_Formula__c) {
			// [CH] NAIS-2102 Insert Opportunity and TLI records for each PFS that needs it
			// Note: Logic to check for existing Sales type Opportunities is already in
			//          the FinanceAction method

			// [DP] 07.15.2015 NAIS-1933 no longer creating the Opp and Sale TLI until the PFS is submitted
			//FinanceAction.createOppAndTransaction(pfsIdsForOpps);

			insert insertAfwlList; // New App Fee Waivers.

			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, iAssignedCount + ' Fee Waivers Assigned.'));

			//resetTables();
			updateCurrentTable();
		} else {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
					'Not enough Fee Waiver Credits available. Please remove '
							+ (iAssignedCount - myAnnualSettings.Waiver_Credit_Balance_Formula__c) + ' selections. No Fee Waivers granted.'));
			iAssignedCount = 0;
			return null;
		}

		// Redraw the whole page.
		PageReference pageRef = new PageReference(ApexPages.currentPage().getURL());
		pageRef.getParameters().put('academicyearid', getAcademicYearId());
		pageRef.setRedirect(true);
		return pageRef;
	}

	public PageReference pfslink() {

		// Need to get the SPA Id. :()
		List<School_PFS_Assignment__c> spas = [SELECT Id FROM School_PFS_Assignment__c
		WHERE PFS_Number__c = :selectedPFSNumber
		AND School__c = :mySchoolId
		LIMIT 1];
		if (spas.size() > 0) {
			PageReference pageRef = Page.SchoolPFSSummary;
			pageRef.getParameters().put('SchoolPFSAssignmentId', spas[0].Id);
			pageRef.setRedirect(true);
			return pageRef;
		} else {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
					'Cannot find School PFS Assignment for PFS ID ' + selectedPFSNumber + '.'));
		}

		return null;
	}
    /*End Action Methods*/

    /*Helper Methods*/
	public List<PFSCheckWrapper> WrapPFSSetCtrlRecords(ApexPages.StandardSetController setCtrl, Boolean bChecks) {

		Set<Id> pfsIdsSet = new Set<Id>();
		List<PFS__c> pfsList = (List<PFS__c>) setCtrl.getRecords();
		Set<Id> applicantIds = new Set<Id>();
		Map<Id, PFS__c> pfsMap;
		for (School_PFS_Assignment__c spa : [SELECT Applicant__c, Applicant__r.PFS__c, Applicant__r.PFS__r.Parent_A__c FROM School_PFS_Assignment__c
		WHERE School__c = :mySchoolId
		AND Academic_Year_Picklist__c = :GlobalVariables.getAcademicYear(getAcademicYearId()).Name
		AND Withdrawn__c != 'Yes' ]) {
			applicantIds.add(spa.Applicant__c);
		}
		//(SELECT Contact__r.Name, Grade_in_Entry_Year__c FROM Applicants__r order by Contact__r.Name)

		for (PFS__c lPFS : pfsList ) {
			pfsIdsSet.add(lPFS.Id);
		}
		pfsMap = new Map<Id, PFS__c> ([select Id,PFS_Number__c, Payment_Status__c, PFS_Status__c, Fee_Waived__c, Parent_A__r.FirstName, Parent_A__r.LastName,
				Parent_A__r.Email, Parent_A__r.Preferred_Phone_Number__c, Total_Income_Current__c, Parent_A__c,
		(SELECT Contact__r.Name, Grade_in_Entry_Year__c FROM Applicants__r order by Contact__r.Name)
		from PFS__c where Id in :pfsIdsSet]);

		List<PFSCheckWrapper> wrappers = new List<PFSCheckWrapper>();
		for (PFS__c pfs : pfsList ) {
			PFS__c localPFS = pfsMap.get(pfs.Id);
			PFSCheckWrapper wrapper = new PFSCheckWrapper();
			//wrapper.pfs = pfs;
			if (bChecks == true) {
				wrapper.isChecked = (localPFS.Fee_Waived__c == 'Yes') || selectedApplicantsByPfsId.containsKey(localPFS.Id);
				wrapper.isDisabled = (localPFS.Fee_Waived__c == 'Yes') || (localPFS.Payment_Status__c == 'Paid in Full');
			}

			// We have to concat the Student Grades and Names ourselves instead of using apex:repeat because of the delimiter.
			// Student Grades & Names Concat

			wrapper.pfsId = localPFS.Id;
			wrapper.pfsNumber = localPFS.PFS_Number__c;
			wrapper.paymentStatus = localPFS.Payment_Status__c;
			wrapper.pfsStatus = localPFS.PFS_Status__c;
			wrapper.feeWaived = localPFS.Fee_Waived__c;
			//wrapper.parentA = localPFS.PFS_Number__c;
			wrapper.parentA = localPFS.Parent_A__c; //SFP-360, [G.S]
			wrapper.parentAFirstName = localPFS.Parent_A__r.FirstName;
			wrapper.parentALastName = localPFS.Parent_A__r.LastName;
			wrapper.parentAEmail = localPFS.Parent_A__r.Email;
			wrapper.parentApreferredPhone = localPFS.Parent_A__r.Preferred_Phone_Number__c;
			wrapper.totalIncomeCurrent = localPFS.Total_Income_Current__c;
			wrapper.applicantsSize = (localPFS.Applicants__r != null)?localPFS.Applicants__r.size():0;
			for (Applicant__c apl : localPFS.Applicants__r) {
				// [CH] NAIS-1837 this filter has been moved into queries
				if (applicantIds.contains(apl.Id)) {
					if (wrapper.sStudentGrades == null) {
						wrapper.sStudentGrades = apl.Grade_in_Entry_Year__c;
					} else {
						wrapper.sStudentGrades += ', ' + apl.Grade_in_Entry_Year__c;
					}
					if (wrapper.sStudentNames == null) {
						wrapper.sStudentNames = apl.Contact__r.Name;
					} else {
						wrapper.sStudentNames += ', ' + apl.Contact__r.Name;
					}
				}
			}
			wrappers.add(wrapper);
		}
		return wrappers;
	}

	public void updateCurrentTable() {

		// Don't show anything from the requesting list in the current list.
		// Filter by Academic Year, School and Reuqesting list. Sort by Parents Last Name.
		List<Id> currentYearPFSIds = new List<Id>();
		for (School_PFS_Assignment__c spa : [SELECT Applicant__c, Applicant__r.PFS__c, Applicant__r.PFS__r.Parent_A__c FROM School_PFS_Assignment__c
		WHERE School__c = :mySchoolId
		AND Academic_Year_Picklist__c = :GlobalVariables.getAcademicYear(getAcademicYearId()).Name
		AND Withdrawn__c != 'Yes' ]) {
			currentYearPFSIds.add(spa.Applicant__r.PFS__c);
		}

		currentSetCtrl = new ApexPages.StandardSetController(Database.getQueryLocator(
		[SELECT Id FROM PFS__c WHERE
		Parent_A__c NOT IN
		(
				SELECT Contact__c
				FROM Application_Fee_Waiver__c
				WHERE Account__c = :mySchoolId
				AND Academic_Year__c = :getAcademicYearId()
				AND Status__c = 'Pending Qualification'
		)
		AND Academic_Year_Picklist__c = :currentAcademicYear.Name
		AND Id IN :currentYearPFSIds                   // Filter for school or affiliation with access org and year
		AND Payment_Status__c = 'Unpaid'
		ORDER BY Parent_A__r.LastName
		]));

		// Reset map for when waivers have been assigned.
		selectedApplicantsByPfsId = new Map<String, PFSCheckWrapper>();

		currentSetCtrl.setPageSize(tablePageSize);
	}


    /*End Helper Methods*/

    /* SchoolAcademicYearSelectorInterface Implementation */
	private Id academicYearId;

	public String getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(String idValue) {
		// [CH] NAIS-1666 and NAIS-1678
		academicYearId = idValue;
		currentAcademicYear = GlobalVariables.getAcademicYear(idValue);
	}

	public PageReference SchoolAcademicYearSelector_OnChange(Boolean saveRecord) {
		PageReference newPage = new PageReference(ApexPages.currentPage().getUrl());
		newPage.getParameters().put('academicyearid', getAcademicYearId());
		newPage.setRedirect(true);
		return newPage;
	}

	public class PFSCheckWrapper {
		// Wraps an sObject adding a checkbox.
		//public PFS__c pfs { get; set; }
		public String pfsId {get;set;}
		public String pfsNumber {get;set;}
		public String pfsStatus {get;set;}
		public String paymentStatus {get;set;}
		public String feeWaived {get;set;}
		public String parentA {get;set;}
		public String parentAFirstName {get;set;}
		public String parentALastName {get;set;}
		public String parentAEmail {get;set;}
		public String parentApreferredPhone {get;set;}
		public Decimal totalIncomeCurrent {get;set;}

		public Boolean isChecked { get; set; }
		public Boolean isDisabled { get; set; }

		// We need to contact grades and names together because of the delimiters. :(
		public String sStudentGrades { get; set; }
		public String sStudentNames { get; set; }

		public Integer applicantsSize {get;set;}

		public PFSCheckWrapper() {
			//pfs = null;
			isChecked = false;
			isDisabled = false;
		}
	}

	public PageReference purchaseAdditionalWaivers() {
		return Page.SchoolFeeWaiversMBWInfo;//SFP-189, [G.S]
	}
	public void previous() {
		currentSetCtrl.previous();
		updateListOfSelectedApplicants(currentList);
		currentList = WrapPFSSetCtrlRecords(currentSetCtrl, true);
	}

	public void next() {
		currentSetCtrl.next();
		updateListOfSelectedApplicants(currentList);
		currentList = WrapPFSSetCtrlRecords(currentSetCtrl, true);
	}

	private void updateListOfSelectedApplicants(List<PFSCheckWrapper> currentList) {
		for(PFSCheckWrapper wrapper : currentList) {
			if(wrapper.isChecked == true) {
				selectedApplicantsByPfsId.put(wrapper.pfsId, wrapper);
			} else {
				selectedApplicantsByPfsId.remove(wrapper.pfsId);
			}
		}
	}
}