public with sharing class TableBusinessFarmShare
{
    
    private static Map<Id, List<Net_Worth_of_Business_Farm__c>> businessFarmShareDataByYear = new Map<Id, List<Net_Worth_of_Business_Farm__c>>();
    
    public static List<Net_Worth_of_Business_Farm__c> getBusinessFarmShareData(Id academicYearId) {
        List<Net_Worth_of_Business_Farm__c> businessFarmShareData = businessFarmShareDataByYear.get(academicYearId);
        if (businessFarmShareData == null) {
            businessFarmShareData = [SELECT Id, Expected_Contribution_Base__c, Expected_Contribution_Rate__c, 
                                            Net_Worth_of_Business_High__c, Net_Worth_of_Business_Low__c,
                                            Threshold_to_Apply_Rate__c
                                        FROM Net_Worth_of_Business_Farm__c
                                        WHERE Academic_Year__c = :academicYearId
                                        ORDER BY Net_Worth_of_Business_Low__c ASC];
            businessFarmShareDataByYear.put(academicYearId, businessFarmShareData);
        }
        return businessFarmShareData;
    }
    
    public static Decimal calculateBusinessFarmShare(Id academicYearId, Decimal valueOfBusinessFarm)
    {
        Decimal calculatedBusinessFarmShare = null;
        if (valueOfBusinessFarm != null) {
            // make sure the value is rounded to the nearest whole number
            Decimal roundedValueOfBusinessFarm = valueOfBusinessFarm.round(RoundingMode.HALF_UP);
            
            List<Net_Worth_of_Business_Farm__c> businessFarmShareData = getBusinessFarmShareData(academicYearId);
            if (businessFarmShareData != null) {
                Net_Worth_of_Business_Farm__c matchingBracket = null;
                // assume the table data is sorted in ascending order by Net_Worth_of_Business_Low__c
                for (Net_Worth_of_Business_Farm__c businessFarmShare : businessFarmShareData) {
                    if (roundedValueOfBusinessFarm >= businessFarmShare.Net_Worth_of_Business_Low__c) {
                        if ((businessFarmShare.Net_Worth_of_Business_High__c == null) 
                                || (roundedValueOfBusinessFarm <= businessFarmShare.Net_Worth_of_Business_High__c)) {
                            matchingBracket = businessFarmShare;
                            break;            
                        }
                    }
                }
                
                if (matchingBracket != null) {
                    Decimal valueOfBusinessFarmAboveThreshold = 0;
                    if ((matchingBracket.Threshold_to_Apply_Rate__c != null) 
                            && (roundedValueOfBusinessFarm > matchingBracket.Threshold_to_Apply_Rate__c)) {
                        valueOfBusinessFarmAboveThreshold = roundedValueOfBusinessFarm - matchingBracket.Threshold_to_Apply_Rate__c;
                    }
                    calculatedBusinessFarmShare = (matchingBracket.Expected_Contribution_Base__c 
                                                    + (valueOfBusinessFarmAboveThreshold * matchingBracket.Expected_Contribution_Rate__c)).round(RoundingMode.HALF_UP);
                }
            }
        }
        if (calculatedBusinessFarmShare == null) {
            throw new EfcException.EfcMissingDataException('No Business Farm Share found for academic year \'' + EfcUtil.getAcademicYearName(academicYearId) + '\' for value = ' + valueOfBusinessFarm);
        }
        
        return calculatedBusinessFarmShare;
    }
}