/**
 * @description This class is used to create Budget Allocation records for unit tests.
 */
@isTest
public class BudgetAllocationTestData extends SObjectTestData {
    @testVisible private static final Decimal AMOUNT = 1500;

    /**
     * @description Get the default values for the Budget_Allocation__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Budget_Allocation__c.Amount_Allocated__c => AMOUNT,
                Budget_Allocation__c.Budget_Group__c =>  BudgetGroupTestData.Instance.DefaultBudgetGroup.Id,
                Budget_Allocation__c.Student_Folder__c => StudentFolderTestData.Instance.DefaultStudentFolder.Id
        };
    }

    /**
     * @description Set the Budget_Group__c field on the current Budget Allocation record.
     * @param budgetGroupId The Budget Group Id to set on the Budget Allocation.
     * @return The current working instance of BudgetAllocationTestData.
     */
    public BudgetAllocationTestData forBudgetGroupId(Id budgetGroupId) {
        return (BudgetAllocationTestData) with(Budget_Allocation__c.Budget_Group__c, budgetGroupId);
    }

    /**
     * @description Set the Amount_Allocated__c field on the current Budget Allocation record.
     * @param amountAllocated The Amount to set on the Budget Allocation.
     * @return The current working instance of BudgetAllocationTestData.
     */
    public BudgetAllocationTestData forAmountAllocated(Decimal amountAllocated) {
        return (BudgetAllocationTestData) with(Budget_Allocation__c.Amount_Allocated__c, amountAllocated);
    }

    /**
     * @description Set the Student_Folder__c field on the current Budget Allocation record.
     * @param studentFolder The folder to set on the Budget Allocation.
     * @return The current working instance of BudgetAllocationTestData.
     */
    public BudgetAllocationTestData forStudentFolder(Id studentFolderId) {
        return (BudgetAllocationTestData) with(Budget_Allocation__c.Student_Folder__c, studentFolderId);
    }
    
    /**
     * @description Insert the current working Budget_Allocation__c record.
     * @return The currently operated upon Budget_Allocation__c record.
     */
    public Budget_Allocation__c insertBudgetGroup() {
        return (Budget_Allocation__c)insertRecord();
    }

    /**
     * @description Create the current working Budget Allocation record without resetting
     *             the stored values in this instance of BudgetAllocationTestData.
     * @return A non-inserted Budget_Allocation__c record using the currently stored field
     *             values.
     */
    public Budget_Allocation__c createBudgetAllocationWithoutReset() {
        return (Budget_Allocation__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Budget_Allocation__c record.
     * @return The currently operated upon Budget_Allocation__c record.
     */
    public Budget_Allocation__c create() {
        return (Budget_Allocation__c)super.buildWithReset();
    }

    /**
     * @description The default Budget_Allocation__c record.
     */
    public Budget_Allocation__c DefaultBudgetAllocation {
        get {
            if (DefaultBudgetAllocation == null) {
                DefaultBudgetAllocation = createBudgetAllocationWithoutReset();
                insert DefaultBudgetAllocation;
            }
            return DefaultBudgetAllocation;
        }
        private set;
    }

    /**
     * @description Get the Budget_Allocation__c SObjectType.
     * @return The Budget_Allocation__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Budget_Allocation__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static BudgetAllocationTestData Instance {
        get {
            if (Instance == null) {
                Instance = new BudgetAllocationTestData();
            }
            return Instance;
        }
        private set;
    }

    private BudgetAllocationTestData() { }
}