/**
 * SchoolImageUploadEndpointTest.cls
 *
 * @description: Test class for SchoolImageUploadEndpoint using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
private class SchoolImageUploadEndpointTest {
    
    /* negative test cases */

    // Test an empty call to handleImageUpload endpoint
    @isTest 
    static void handleImageUpload_emptyRequest_noUpload() {
        
        // Arrange
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'services/apexrest/SchoolImageUpload';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        String reqString = '[ {} ]';
        Blob myBlob = Blob.valueOf( reqString);

        // Act
        Test.startTest();
            req.requestBody = myBlob;
            SchoolImageUploadEndpoint.handleImageUpload();
        Test.stopTest();

        // Assert
        List<Document> dList = [select Id from Document limit 1];
        System.assert( dList.isEmpty());
    }

    // Test a call to handleImageUpload without valid auth key
    @isTest 
    static void handleImageUpload_invalidRequest_noUpload() {

        // Arrange
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'services/apexrest/SchoolImageUpload';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        req.addHeader( 'X-Detail-Id', '123456');
        RestContext.response = res;
        String reqString = 'garbage test request';
        Blob myBlob = Blob.valueOf( reqString);

        // Act
        Test.startTest();
            req.requestBody = myBlob;
            SchoolImageUploadEndpoint.handleImageUpload();
        Test.stopTest();

        // Assert
        List<Document> dList = [select Id from Document limit 1];
        System.assert( dList.isEmpty());
    }

    /* positive test cases */
    
    // Test a valid call to handleImageUpload
    @isTest 
    static void handleImageUpload_validRequest_validUpload() {

        // Arrange
        MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
        Mass_Email_Send__c massES = [select Id from Mass_Email_Send__c limit 1];

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'services/apexrest/SchoolImageUpload';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        req.addHeader( 'X-Detail-Id', massES.Id);
        RestContext.response = res;
        String reqString = 'garbage test request';
        Blob myBlob = Blob.valueOf( reqString);

        // Act
        Test.startTest();
            req.requestBody = myBlob;
            SchoolImageUploadEndpoint.handleImageUpload();
        Test.stopTest();

        // Assert
        List<Document> dList = [select Id from Document limit 1];
        System.assert( !dList.isEmpty());
    }

}