public class FamilyNavigationBarController {

    /*Initialization*/
    
    public FamilyNavigationBarController() {
        this.DisableHouseholdSummary = false;
    }
   
    /*End Initialization*/
   
    /*Properties*/
    
    public transient Boolean isKSSchoolUser_x = null;
    public Boolean IsKSSchoolUser { get { return getappUtils().IsKSSchoolUser;} }
    public Boolean isMIERequired { get { return getappUtils().isMIERequired;} }
    
    public PFS__c PFS { get; set; }
    public Id bfId { get; set; }
    public String Screen { get; set; }
    
    private transient ApplicationUtils appUtils_x = null;
    
    public void setappUtils(ApplicationUtils value) { this.appUtils_x = value; }
    
    public ApplicationUtils getappUtils() {
        if(this.appUtils_x == null){
            this.appUtils_x = new ApplicationUtils(UserInfo.getLanguage(), this.PFS);    
        }
        return this.appUtils_x;
    }
    
    public Boolean DisableHouseholdSummary { get; set; }
    
    public Boolean HasApplicants { get { return !this.PFS.Applicants__r.isEmpty(); } }
    
    public Boolean OwnsBusinessOrFarm { get { return this.PFS.Own_Business_or_Farm__c == 'Yes'; } }
    
    public String bfLinkIconSpacing {
        get{
            if(bfLinkIconSpacing == null){
                bfLinkIconSpacing = pfs.Business_Farms__r.size() > 2 ? '38px 0px' : '20px 0px'; 
            }
            return bfLinkIconSpacing;
        }
        set;
    }
    public String bfLinkSpacing {
        get{
            if(bfLinkSpacing == null){
                bfLinkSpacing = pfs.Business_Farms__r.size() > 2 ? '56px' : '38px'; 
            }
            return bfLinkSpacing;
        }
        set;
    }

    // SFP-607
    public Boolean renderAdditionalQuestionsSelection{
        get { 
            return appUtils_x.getRequiredAdditionalQuestions();
        } set;
    }

    /*End Properties*/
       
    /*Action Methods*/
    
    /*End Action Methods*/
   
    /*Helper Methods*/
    
    /*End Helper Methods*/

}