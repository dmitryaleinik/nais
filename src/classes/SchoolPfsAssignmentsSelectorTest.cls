@isTest
private class SchoolPfsAssignmentsSelectorTest {
    
    private static PFS__c pfs;
    private static Student_Folder__c folder1, folder2;
    
    @isTest
    private static void selectById_emptySet_expectNoRecords() {
        SchoolPfsAssignmentTestData.Instance.insertSchoolPfsAssignment();

        Set<Id> recordIds = new Set<Id>();

        List<School_PFS_Assignment__c> records = SchoolPfsAssignmentsSelector.Instance.selectById(recordIds);
        System.assertNotEquals(null, records, 'Did not expect the list of records to be null.');
        System.assert(records.isEmpty(), 'Expected the list of records to be empty.');
    }

    @isTest
    private static void selectById_setContainsRecordIds_expectRecords() {
        School_PFS_Assignment__c record = SchoolPfsAssignmentTestData.Instance.insertSchoolPfsAssignment();

        Set<Id> recordIds = new Set<Id> { record.Id };

        List<School_PFS_Assignment__c> records = SchoolPfsAssignmentsSelector.Instance.selectById(recordIds);
        System.assertNotEquals(null, records, 'Did not expect the list of records to be null.');
        System.assertEquals(1, records.size(), 'Expected one record to be returned.');
        System.assertEquals(record.Id, records[0].Id, 'Expected the specified record to be returned.');
    }
    
    @isTest
    private static void selectForFcwById_setContainsRecordIds_expectRecords() {
        School_PFS_Assignment__c record = SchoolPfsAssignmentTestData.Instance.insertSchoolPfsAssignment();

        Set<Id> recordIds = new Set<Id> { record.Id };

        List<School_PFS_Assignment__c> records = SchoolPfsAssignmentsSelector.Instance.selectForFcwById(recordIds);
        System.assertNotEquals(null, records, 'Did not expect the list of records to be null.');
        System.assertEquals(1, records.size(), 'Expected one record to be returned.');
        System.assertEquals(record.Id, records[0].Id, 'Expected the specified record to be returned.');
    }

    @isTest
    private static void selectByName_expectRecordReturned() {
        String spfsaName = 'Spfsa';
        School_PFS_Assignment__c record = SchoolPfsAssignmentTestData.Instance
            .forName(spfsaName).insertSchoolPfsAssignment();

        Test.startTest();
            List<School_PFS_Assignment__c> spfsaRecords = 
                SchoolPfsAssignmentsSelector.newInstance().selectByName(new Set<String>{spfsaName+'0'});
        Test.stopTest();

        System.assert(!spfsaRecords.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(1, spfsaRecords.size(), 'Expected there to be 1 record returned.');
    }

    @isTest
    private static void selectByName_expectException() {
        String spfsaName = 'Spfsa';
        School_PFS_Assignment__c record = SchoolPfsAssignmentTestData.Instance
            .forName(spfsaName).insertSchoolPfsAssignment();

        Test.startTest();
            try {
                SchoolPfsAssignmentsSelector.newInstance().selectByName(null);

                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, SchoolPfsAssignmentsSelector.SPFSA_NAME);
            }
        Test.stopTest();
    }

    @isTest
    private static void selectAll_expectRecordReturned() {
        School_PFS_Assignment__c spfsa1 = SchoolPfsAssignmentTestData.Instance.DefaultSchoolPfsAssignment;

        Test.startTest();
            List<School_PFS_Assignment__c> spfsaRecords = SchoolPfsAssignmentsSelector.newInstance().selectAll();
        Test.stopTest();

        System.assert(!spfsaRecords.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(1, spfsaRecords.size(), 'Expected there to be 1 record returned.');
    }

    @isTest
    private static void testSelectByStudentAndSchoolForAcademicYear_expectExceptions()
    {
        Account school = AccountTestData.Instance.asSchool().DefaultAccount;
        Contact student = ContactTestData.Instance.asStudent().insertContact();
        Student_Folder__c studentFolder = StudentFolderTestData.Instance
            .forStudentId(student.Id).DefaultStudentFolder;
        Applicant__c applicant = ApplicantTestData.Instance
            .forContactId(student.Id).DefaultApplicant;

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        Academic_Year__c previousAcademicYear = AcademicYearTestData.Instance.asPreviousYear().insertAcademicYear();

        School_PFS_Assignment__c spfsa = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(currentAcademicYear.Name).DefaultSchoolPfsAssignment;

        Test.startTest();
            try 
            {
                SchoolPfsAssignmentsSelector.newInstance().selectByStudentAndSchoolForAcademicYear(
                    null, new Set<Id>{school.Id}, previousAcademicYear.Name);

                TestHelpers.expectedArgumentNullException();
            } 
            catch (Exception e) 
            {
                TestHelpers.assertArgumentNullException(e, SchoolPfsAssignmentsSelector.SPFSA_APPLICANT_CONTACT);
            }

            try 
            {
                SchoolPfsAssignmentsSelector.newInstance().selectByStudentAndSchoolForAcademicYear(
                    new Set<Id>{student.Id}, null, previousAcademicYear.Name);

                TestHelpers.expectedArgumentNullException();
            } 
            catch (Exception e) 
            {
                TestHelpers.assertArgumentNullException(e, SchoolPfsAssignmentsSelector.SCHOOL_IDS);
            }

            try
            {
                SchoolPfsAssignmentsSelector.newInstance().selectByStudentAndSchoolForAcademicYear(
                    new Set<Id>{student.Id}, new Set<Id>{school.Id}, null);

                TestHelpers.expectedArgumentNullException();
            }
            catch (Exception e)
            {
                TestHelpers.assertArgumentNullException(e, SchoolPfsAssignmentsSelector.SPFSA_ACADEMIC_YEAR);
            }
        Test.stopTest();
    }

    @isTest
    private static void testSelectByStudentAndSchoolForAcademicYear_expectRecordReturned()
    {
        Account schoolA = AccountTestData.Instance.asSchool().DefaultAccount;
        Account schoolB = AccountTestData.Instance.asSchool().insertAccount();

        Contact student1 = ContactTestData.Instance.asStudent().create();
        Contact student2 = ContactTestData.Instance.asStudent().create();
        insert new List<Contact>{student1, student2};

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
        Academic_Year__c previousAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, previousAcademicYear};

        Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forStudentId(student1.Id).create();
        Student_Folder__c studentFolder1Prev = StudentFolderTestData.Instance
            .forAcademicYearPicklist(previousAcademicYear.Name)
            .forStudentId(student1.Id).create();
        Student_Folder__c studentFolder2 = StudentFolderTestData.Instance
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forStudentId(student2.Id).create();
        Student_Folder__c studentFolder2Prev = StudentFolderTestData.Instance
            .forAcademicYearPicklist(previousAcademicYear.Name)
            .forStudentId(student2.Id).create();
        insert new List<Student_Folder__c>{studentFolder1, studentFolder1Prev, studentFolder2, studentFolder2Prev};

        Applicant__c applicant1 = ApplicantTestData.Instance.forContactId(student1.Id).create();
        Applicant__c applicant1Prev = ApplicantTestData.Instance.forContactId(student1.Id).create();
        Applicant__c applicant2 = ApplicantTestData.Instance.forContactId(student2.Id).create();
        Applicant__c applicant2Prev = ApplicantTestData.Instance.forContactId(student2.Id).create();
        insert new List<Applicant__c>{applicant1, applicant1Prev, applicant2, applicant2Prev};

        School_PFS_Assignment__c spfsa1 = SchoolPfsAssignmentTestData.Instance
            .forStudentFolderId(studentFolder1.Id)
            .forApplicantId(applicant1.Id)
            .forAcademicYearPicklist(currentAcademicYear.Name).create();
        School_PFS_Assignment__c spfsa1Previous = SchoolPfsAssignmentTestData.Instance
            .forStudentFolderId(studentFolder1Prev.Id)
            .forApplicantId(applicant1Prev.Id)
            .forAcademicYearPicklist(previousAcademicYear.Name).create();
        School_PFS_Assignment__c spfsa1PreviousSchoolB = SchoolPfsAssignmentTestData.Instance
            .forStudentFolderId(studentFolder1.Id)
            .forApplicantId(applicant1.Id)
            .forSchoolId(schoolB.Id)
            .forAcademicYearPicklist(previousAcademicYear.Name).create();
        School_PFS_Assignment__c spfsa2 = SchoolPfsAssignmentTestData.Instance
            .forStudentFolderId(studentFolder2.Id)
            .forApplicantId(applicant2.Id)
            .forAcademicYearPicklist(currentAcademicYear.Name).create();
        School_PFS_Assignment__c spfsa2Previous = SchoolPfsAssignmentTestData.Instance
            .forStudentFolderId(studentFolder2Prev.Id)
            .forApplicantId(applicant2Prev.Id)
            .forAcademicYearPicklist(previousAcademicYear.Name).create();
        insert new List<School_PFS_Assignment__c>{spfsa1, spfsa1Previous, spfsa1PreviousSchoolB, spfsa2, spfsa2Previous};

        Test.startTest();
            List<School_PFS_Assignment__c> spfsaRecords = SchoolPfsAssignmentsSelector.newInstance()
                .selectByStudentAndSchoolForAcademicYear(new Set<Id>{student1.Id}, new Set<Id>{schoolA.Id}, previousAcademicYear.Name);
        Test.stopTest();

        System.assert(!spfsaRecords.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(1, spfsaRecords.size(), 'Expected there to be 1 record returned.');
        System.assertEquals(spfsa1Previous.Id, spfsaRecords[0].Id);
    }

    @isTest
    private static void selectByIdWithCustomFieldList_expectRecordReturned() {
        School_PFS_Assignment__c spfsa = SchoolPfsAssignmentTestData.Instance.DefaultSchoolPfsAssignment;

        Test.startTest();
            List<School_PFS_Assignment__c> spfsaRecords = SchoolPfsAssignmentsSelector.newInstance()
                .selectByIdWithCustomFieldList(new Set<Id>{spfsa.Id}, new List<String>{'Name'});
        Test.stopTest();

        System.assert(!spfsaRecords.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(1, spfsaRecords.size(), 'Expected there to be one record returned.');
        System.assertEquals(spfsa.Id, spfsaRecords[0].Id, 'Expected the Ids of the records to match.');
    }

    @isTest
    private static void selectByIdWithCustomFieldListd_expectArgumentNullException() {
        School_PFS_Assignment__c spfsa = SchoolPfsAssignmentTestData.Instance.DefaultSchoolPfsAssignment;

        Test.startTest();
            try {
                SchoolPfsAssignmentsSelector.newInstance().selectByIdWithCustomFieldList(new Set<Id>{spfsa.Id}, null);

                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, SchoolPfsAssignmentsSelector.SPFSA_FIELDS_TO_SELECT_PARAM);
            }

            try {
                SchoolPfsAssignmentsSelector.newInstance().selectByIdWithCustomFieldList(null, new List<String>{'Name'});

                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, SchoolPfsAssignmentsSelector.ID_SET_PARAM);
            }
        Test.stopTest();
    }

    @isTest
    private static void selectAllWithCustomFieldList_spfsaExists_expectRecordReturned() {
        School_PFS_Assignment__c spfsa = SchoolPFSAssignmentTestData.Instance.DefaultSchoolPfsAssignment;

        Test.startTest();
            List<School_PFS_Assignment__c> spfsaRecords = SchoolPFSAssignmentsSelector.Instance.selectAllWithCustomFieldList(
                new List<String>{'Name'});
        Test.stopTest();

        System.assert(!spfsaRecords.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(1, spfsaRecords.size(), 'Expected there to be one spfsa record returned.');
        System.assertEquals(spfsa.Id, spfsaRecords[0].Id, 'Expected the Ids of the spfsa records to match.');
    }

    @isTest
    private static void selectAllWithCustomFieldList_nullSet_expectArgumentNullException() {
        School_PFS_Assignment__c spfsa = SchoolPFSAssignmentTestData.Instance.DefaultSchoolPfsAssignment;

        Test.startTest();
            try {
                SchoolPFSAssignmentsSelector.Instance.selectAllWithCustomFieldList(null);

                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, SchoolPFSAssignmentsSelector.SPFSA_FIELDS_TO_SELECT_PARAM);
            }
        Test.stopTest();
    }
    
    private static void createTestData() {
        
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        
        //1. Create a school.
        Account school = AccountTestData.Instance.asSchool().DefaultAccount;
        
        //2. Create contacts for Parent and its students.
        Contact student1 = ContactTestData.Instance
                .forFirstName('Jane')
                .forLastName('Doe')
                .forRecordTypeId(RecordTypes.studentContactTypeId)
                .create();
                
        Contact student2 = ContactTestData.Instance
                .forFirstName('Jhon')
                .forLastName('Doe')
                .forRecordTypeId(RecordTypes.studentContactTypeId)
                .create();

        Contact parent = ContactTestData.Instance.forLastName('Parent')
                .forRecordTypeId(RecordTypes.parentContactTypeId)
                .create();
                
        insert new List<Contact>{student1, student2, parent};

        //3. Create 2 folders, one for each applicant.
        folder1 = StudentFolderTestData.Instance.forName('Folder1')
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forStudentId(student1.Id)
                .create();
                
        folder2 = StudentFolderTestData.Instance.forName('Folder1')
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forStudentId(student1.Id)
                .create();
                
        insert new List<Student_Folder__c>{folder1, folder2};
        
        //4. Create the PFS record.
        pfs = PfsTestData.Instance.forAcademicYearPicklist(currentAcademicYear.Name).forParentA(parent.Id).create();
        insert pfs;
        
        //5. Create 2 applicants, one for each student.
        Applicant__c applicant1 = ApplicantTestData.Instance.forContactId(student1.Id).forPfsId(pfs.Id).create();
        Applicant__c applicant2 = ApplicantTestData.Instance.forContactId(student2.Id).forPfsId(pfs.Id).create();
        
        insert new List<Applicant__c>{applicant1, applicant2};
        
        //6. Create SPAS for each applicant
        School_PFS_Assignment__c spa1 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forApplicantId(applicant1.Id)
            .forSchoolId(school.Id)
            .forStudentFolderId(folder1.Id)
            .create();
            
        School_PFS_Assignment__c spa2 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forApplicantId(applicant2.Id)
            .forSchoolId(school.Id)
            .forStudentFolderId(folder2.Id)
            .create();
            
        insert new List<School_PFS_Assignment__c>{spa1, spa2};
        
    }
    
    @isTest
    private static void selectByStudentFolder_folderWithSpas_expectListOfSpas() {
        
        createTestData();
        
        Test.startTest();
        
            List<School_PFS_Assignment__c> spas = SchoolPFSAssignmentsSelector.Instance
                .selectByStudentFolder( new Set<Id>{folder1.Id, folder2.Id} );
            
            System.assertEquals(2, spas.size(), 'Selector must retrieve all spa records related to the give folder ids.');
            
        Test.stopTest();
        
    }//End:selectByStudentFolder_folderWithSpas_expectListOfSpas
    
    @isTest
    private static void selectByPFSId_pfsWithSiblings_expectListOfSiblingsSpas() {
        
        createTestData();
        
        Test.startTest();
        
            List<School_PFS_Assignment__c> spas = SchoolPFSAssignmentsSelector.Instance.selectByPFSId( new Set<Id>{pfs.Id} );
            
            System.assertEquals(2, spas.size(), 'Selector must retrieve all spa records related to the give pfs.');
            
        Test.stopTest();
        
    }//End:selectByPFSId_pfsWithSiblings_expectListOfSiblingsSpas
    
    
    @isTest
    private static void selectByStudentAndSchoolForAcademicYear_expectListOfPreviousSPAs() {
        
        //Create current and previous academic years.
		Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
		Academic_Year__c priorAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
		insert new List<Academic_Year__c>{currentAcademicYear, priorAcademicYear};
		
		//Create a School.
		Account school = AccountTestData.Instance.asSchool().insertAccount();
		
		//Create Annual Setting.
		Annual_Setting__c annualSetting = AnnualSettingsTestData.Instance
		    .forSchoolId(school.Id)
		    .forAcademicYearId(currentAcademicYear.Id)
		    .insertAnnualSettings();
		
		//Create contacts for parents and students.
		Contact parentA = ContactTestData.Instance.asParent().create();
		Contact student = ContactTestData.Instance.asStudent().create();
		insert new List<Contact> { parentA, student };
		
		//Create PFS records.
		PFS__c priorPFS = PfsTestData.Instance
		    .forParentA(parentA.Id)
		    .forAcademicYearPicklist(priorAcademicYear.Name)
		    .asPaid()
		    .create();
		PFS__c currentPFS = PfsTestData.Instance
		    .forParentA(parentA.Id)
		    .forAcademicYearPicklist(currentAcademicYear.Name)
		    .asPaid()
		    .create();
		insert new List<PFS__c>{priorPFS, currentPFS};
		
		//Create applicants.
		Applicant__c priorApplicant = ApplicantTestData.Instance
		    .forContactId(student.Id)
		    .forPfsId(priorPFS.Id)
		    .create();
		Applicant__c currentApplicant = ApplicantTestData.Instance
		    .forContactId(student.Id)
		    .forPfsId(currentPFS.Id)
		    .create();
		insert new List<Applicant__c>{priorApplicant, currentApplicant};
		   
		//Create Folders
		Student_Folder__c folderCurrent = StudentFolderTestData.Instance.forName('Folder1')
		    .forAcademicYearPicklist(currentAcademicYear.Name)
		    .forStudentId(student.Id)
		    .forSchoolId(school.Id)
		    .create();
		    
	    Student_Folder__c folderPrevious = StudentFolderTestData.Instance.forName('Folder2')
	        .forAcademicYearPicklist(priorAcademicYear.Name)
	        .forStudentId(student.Id)
	        .forSchoolId(school.Id)
	        .create();
		
		insert new List<Student_Folder__c>{folderCurrent, folderPrevious};
		
		//Create SPAs.
		School_PFS_Assignment__c spa1 = SchoolPfsAssignmentTestData.Instance
		    .forAcademicYearPicklist(currentAcademicYear.Name)
		    .forApplicantId(currentApplicant.Id)
		    .forSchoolId(school.Id)
		    .forStudentFolderId(folderCurrent.Id)
		    .create();
		    
	    School_PFS_Assignment__c spa2 = SchoolPfsAssignmentTestData.Instance
	        .forAcademicYearPicklist(priorAcademicYear.Name)
	        .forApplicantId(priorApplicant.Id)
	        .forSchoolId(school.Id)
	        .forStudentFolderId(folderPrevious.Id)
	        .create();
        
        insert new List<School_PFS_Assignment__c>{spa1, spa2};
        
        Test.startTest();
            
            School_PFS_Assignment__c spaCurrent = [
                SELECT Id, school__c, Contact_ID__c, Academic_Year_Name__c 
                FROM School_PFS_Assignment__c 
                WHERE Id =: spa1.Id];
            
            String currentSPAKey = Id.ValueOf(spaCurrent.school__c) + ':' 
                + Id.ValueOf(spaCurrent.Contact_ID__c) + ':' 
                + GlobalVariables.getPreviousAcademicYearStr(spaCurrent.Academic_Year_Name__c);
            
            List<School_PFS_Assignment__c> result = SchoolPfsAssignmentsSelector.newInstance()
                .selectByStudentAndSchoolForAcademicYear(new Set<String>{currentSPAKey});
            
            System.AssertEquals(1, result.size());
            System.AssertEquals(spa2.Id, result[0].Id);
            
        Test.stopTest();
    }

    @isTest
    private static void testSelectBySchoolAndAcademicYear_expectExceptions()
    {
        Account school = AccountTestData.Instance.asSchool().DefaultAccount;
        Contact student = ContactTestData.Instance.asStudent().insertContact();
        Student_Folder__c studentFolder = StudentFolderTestData.Instance
            .forStudentId(student.Id).DefaultStudentFolder;
        Applicant__c applicant = ApplicantTestData.Instance
            .forContactId(student.Id).DefaultApplicant;

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        Academic_Year__c previousAcademicYear = AcademicYearTestData.Instance.asPreviousYear().insertAcademicYear();

        School_PFS_Assignment__c spfsa = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(currentAcademicYear.Name).DefaultSchoolPfsAssignment;

        Test.startTest();
        try
        {
            SchoolPfsAssignmentsSelector.newInstance().selectBySchoolAndAcademicYear(null, previousAcademicYear.Name);

            TestHelpers.expectedArgumentNullException();
        }
        catch (Exception e)
        {
            TestHelpers.assertArgumentNullException(e, SchoolPfsAssignmentsSelector.SCHOOL_IDS);
        }

        try
        {
            SchoolPfsAssignmentsSelector.newInstance().selectBySchoolAndAcademicYear(new Set<Id>{school.Id}, null);

            TestHelpers.expectedArgumentNullException();
        }
        catch (Exception e)
        {
            TestHelpers.assertArgumentNullException(e, SchoolPfsAssignmentsSelector.SPFSA_ACADEMIC_YEAR);
        }
        Test.stopTest();
    }

    @isTest
    private static void testSelectBySchoolAndAcademicYear_expectRecordReturned()
    {
        Account schoolA = AccountTestData.Instance.asSchool().DefaultAccount;
        Account schoolB = AccountTestData.Instance.asSchool().insertAccount();

        Contact student1 = ContactTestData.Instance.asStudent().create();
        Contact student2 = ContactTestData.Instance.asStudent().create();
        insert new List<Contact>{student1, student2};

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
        Academic_Year__c previousAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, previousAcademicYear};

        Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forStudentId(student1.Id).create();
        Student_Folder__c studentFolder1Prev = StudentFolderTestData.Instance
            .forAcademicYearPicklist(previousAcademicYear.Name)
            .forStudentId(student1.Id).create();
        Student_Folder__c studentFolder2 = StudentFolderTestData.Instance
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forStudentId(student2.Id).create();
        Student_Folder__c studentFolder2Prev = StudentFolderTestData.Instance
            .forAcademicYearPicklist(previousAcademicYear.Name)
            .forStudentId(student2.Id).create();
        insert new List<Student_Folder__c>{studentFolder1, studentFolder1Prev, studentFolder2, studentFolder2Prev};

        Applicant__c applicant1 = ApplicantTestData.Instance.forContactId(student1.Id).create();
        Applicant__c applicant1Prev = ApplicantTestData.Instance.forContactId(student1.Id).create();
        Applicant__c applicant2 = ApplicantTestData.Instance.forContactId(student2.Id).create();
        Applicant__c applicant2Prev = ApplicantTestData.Instance.forContactId(student2.Id).create();
        insert new List<Applicant__c>{applicant1, applicant1Prev, applicant2, applicant2Prev};

        School_PFS_Assignment__c spfsa1 = SchoolPfsAssignmentTestData.Instance
            .forStudentFolderId(studentFolder1.Id)
            .forApplicantId(applicant1.Id)
            .forAcademicYearPicklist(currentAcademicYear.Name).create();
        School_PFS_Assignment__c spfsa1Previous = SchoolPfsAssignmentTestData.Instance
            .forStudentFolderId(studentFolder1Prev.Id)
            .forApplicantId(applicant1Prev.Id)
            .forAcademicYearPicklist(previousAcademicYear.Name).create();
        School_PFS_Assignment__c spfsa1PreviousSchoolB = SchoolPfsAssignmentTestData.Instance
            .forStudentFolderId(studentFolder1.Id)
            .forApplicantId(applicant1.Id)
            .forSchoolId(schoolB.Id)
            .forAcademicYearPicklist(previousAcademicYear.Name).create();
        School_PFS_Assignment__c spfsa2 = SchoolPfsAssignmentTestData.Instance
            .forStudentFolderId(studentFolder2.Id)
            .forApplicantId(applicant2.Id)
            .forAcademicYearPicklist(currentAcademicYear.Name).create();
        School_PFS_Assignment__c spfsa2Previous = SchoolPfsAssignmentTestData.Instance
            .forStudentFolderId(studentFolder2Prev.Id)
            .forApplicantId(applicant2Prev.Id)
            .forAcademicYearPicklist(previousAcademicYear.Name).create();
        insert new List<School_PFS_Assignment__c>{spfsa1, spfsa1Previous, spfsa1PreviousSchoolB, spfsa2, spfsa2Previous};

        Test.startTest();
            List<School_PFS_Assignment__c> spfsaRecords = SchoolPfsAssignmentsSelector.newInstance()
                .selectBySchoolAndAcademicYear(new Set<Id>{schoolA.Id}, previousAcademicYear.Name);
        Test.stopTest();

        System.assert(!spfsaRecords.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(2, spfsaRecords.size(), 'Expected there to be 2 records returned.');
        System.assert((new Map<Id, School_PFS_Assignment__c>(spfsaRecords).keySet())
            .containsAll(new Set<Id>{spfsa1Previous.Id, spfsa2Previous.Id}));
    }
}