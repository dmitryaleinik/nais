public with sharing class EfcDebugToolController {
    
    private Long timerStart;
    
    public EfcWorksheetData worksheet {get; set;}
    public String pfsId {get; set;}
    public String assignmentId {get; set;}
    public Applicant__c dummyPfsLookup {get; set;} // used just for the PFS lookup
    public School_Document_Assignment__c dummySchoolPfsAssignmentLookup {get; set;} // used just for the School PFS Assignment lookup
    public PFS__c dummyPfs {get; set;}
    List<SelectOption> academicYearOptions;
    public String debugLog {get; set;}
    public String jsonWorksheet {get; set;}
    
    
    // table lookup input fields
    public List<SelectOption> getTableAcademicYearOptions() {
        return academicYearOptions;
    }
    public String tableAcademicYear {get; set;}
    public String tableHimHomePurchaseYear {get; set;}
    public Decimal tableValueOfBusinessFarm {get; set;}
    public String tableFamilyStatus {get; set;}
    public Integer tableAge {get; set;}
    public String tableFilingStatus {get; set;}
    public String tablefilingStatusParentB {get; set;}
    public Decimal tableFederalIncome {get; set;}
    public String tableState {get; set;}
    public String tableCountry {get; set;}
    public Decimal tableStateIncome {get; set;}
    public Decimal tableEmploymentAllowanceIncome {get; set;}
    public Decimal tableDiscretionaryNetWorth {get; set;}
    public Decimal tableConversionCoefficient {get; set;}
    public Decimal tableFamilySize {get; set;}
    public Decimal tableContributionDiscretionaryIncome {get; set;}
    
    public EfcDebugToolController() {
        clearLog();
        dummyPfsLookup = new Applicant__c();
        dummySchoolPfsAssignmentLookup = new School_Document_Assignment__c();
        dummyPfs = new PFS__c();
        worksheet = new EfcWorksheetData();
        
        academicYearOptions = new List<SelectOption>();
        for (Academic_Year__c academicYear : [SELECT Id, Name FROM Academic_Year__c ORDER BY End_Date__c DESC]) {
            academicYearOptions.add(new SelectOption(academicYear.Id, academicYear.Name));
        }
    }
    
    public List<SelectOption> getAcademicYearOptions() {
        return academicYearOptions;
    }
    
    public PageReference loadFromPfs() {
        timerStart();
        pfsId = dummyPfsLookup.PFS__c;
        if (pfsId != null) {
            worksheet = EfcDataManager.createEfcWorksheetDataForPFS(pfsId);
        }
        Long endTime = timerStop();
        debugLog('LoadPFS', 'Loaded from PFS Id: ' + pfsId + ' (elapsed time = ' + endTime + ' ms)');
        return null;
    }    
    
    public PageReference loadFromSchoolPfsAssignment() {
        timerStart();
        assignmentId = dummySchoolPfsAssignmentLookup.School_PFS_Assignment__c;
        if (assignmentId != null) {
            worksheet = EfcDataManager.createEfcWorksheetDataForSchoolPFSAssignment(assignmentId);
        }
        Long endTime = timerStop();
        debugLog('LoadPFS', 'Loaded from School PFS Assignment Id: ' + assignmentId + ' (elapsed time = ' + endTime + ' ms)');
        return null;
    }
    
    public PageReference serializeWorksheet() {
        try {
            jsonWorksheet = System.Json.serializePretty(worksheet);
        }
        catch (Exception e) {
            handleException('Serialize', e);
        }    
        return null;
    }
    
    public PageReference deserializeWorksheet() {
        try {
            worksheet = (EfcWorksheetData) System.Json.deserialize(jsonWorksheet, EfcWorksheetData.class);
        }
        catch (Exception e) {
            handleException('Deserialize', e);
        }
        return null;
    }
    
    
    public PageReference calcTaxableIncome() {
        try {
            timerStart();
            EfcCalculationSteps.TaxableIncome calcStep = new EfcCalculationSteps.TaxableIncome();
            calcStep.doCalculation(worksheet);
            Long endTime = timerStop();
            debugLog('TaxableIncome', 'Calculated taxable income = ' + worksheet.totalTaxableIncome + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('TaxableIncome', e);
        }    
        return null;
    }
    
    public PageReference calcNonTaxableIncome() {
        try {
            timerStart();
            EfcCalculationSteps.NonTaxableIncome calcStep = new EfcCalculationSteps.NonTaxableIncome();
            calcStep.doCalculation(worksheet);
            Long endTime = timerStop();
            debugLog('NonTaxableIncome', 'Calculated nontaxable income = ' + worksheet.totalNonTaxableIncome + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('NonTaxableIncome', e);
        }    
        return null;
    }
    
    public PageReference calcTotalIncome() {
        try {
            timerStart();
            EfcCalculationSteps.TotalIncome calcStep = new EfcCalculationSteps.TotalIncome();
            calcStep.doCalculation(worksheet);
            Long endTime = timerStop();
            debugLog('TotalIncome', 'Calculated total income = ' + worksheet.totalNonTaxableIncome + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('TotalIncome', e);
        }    
        return null;
    }
    
    public PageReference calcTotalAllowances() {
        try {    
            timerStart();
            EfcCalculationSteps.TotalAllowances calcStep = new EfcCalculationSteps.TotalAllowances();
            calcStep.doCalculation(worksheet);
            Long endTime = timerStop();
            debugLog('TotalAllowances', 'Calculated total allowances = ' + worksheet.totalAllowances + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('TotalAllowances', e);
        }        
        return null;
    }
    
    public PageReference calcEffectiveIncome() {
        try {
            timerStart();
            EfcCalculationSteps.EffectiveIncome calcStep = new EfcCalculationSteps.EffectiveIncome();
            calcStep.doCalculation(worksheet);
            Long endTime = timerStop();
            debugLog('EffectiveIncome', 'Calculated effective income = ' + worksheet.effectiveIncome + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('EffectiveIncome', e);
        }    
        return null;
    }
    
    public PageReference calcHomeEquity() {
        try {
            timerStart();
            EfcCalculationSteps.HomeEquity calcStep = new EfcCalculationSteps.HomeEquity();
            calcStep.doCalculation(worksheet);
            Long endTime = timerStop();
            debugLog('HomeEquity', 'Calculated home equity = ' + worksheet.homeEquity + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('HomeEquity', e);
        }    
        return null;
    }
    
    public PageReference calcNetWorth() {
        try {
            timerStart();
            EfcCalculationSteps.NetWorth calcStep = new EfcCalculationSteps.NetWorth();
            calcStep.doCalculation(worksheet);
            Long endTime = timerStop();
            debugLog('NetWorth', 'Calculated net worth = ' + worksheet.netWorth + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('NetWorth', e);
        }    
        return null;
    }
    
    public PageReference calcDiscretionaryNetWorth() {
        try {
            timerStart();
            EfcCalculationSteps.DiscretionaryNetWorth calcStep = new EfcCalculationSteps.DiscretionaryNetWorth();
            calcStep.doCalculation(worksheet);
            Long endTime = timerStop();
            debugLog('DiscretionaryNetWorth', 'Calculated discretionary net worth = ' + worksheet.discretionaryNetWorth + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('DiscretionaryNetWorth', e);
        }    
        return null;
    }
    
    public PageReference calcIncomeSupplement() {
        try {
            timerStart();
            EfcCalculationSteps.IncomeSupplement calcStep = new EfcCalculationSteps.IncomeSupplement();
            calcStep.doCalculation(worksheet);
            Long endTime = timerStop();
            debugLog('IncomeSupplement', 'Calculated income supplement = ' + worksheet.incomeSupplement + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('IncomeSupplement', e);
        }    
        return null;
    }
    
    public PageReference calcAdjustedEffectiveIncome() {
        try {
            timerStart();
            EfcCalculationSteps.AdjustedEffectiveIncome calcStep = new EfcCalculationSteps.AdjustedEffectiveIncome();
            calcStep.doCalculation(worksheet);
            Long endTime = timerStop();
            debugLog('AdjustedEffectiveIncome', 'Calculated adjusted effective income  = ' + worksheet.adjustedEffectiveIncome + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('AdjustedEffectiveIncome', e);
        }    
        return null;
    }
    
    public PageReference calcRevisedAdjustedEffectiveIncome() {
        try {
            timerStart();
            EfcCalculationSteps.RevisedAdjustedEffectiveIncome calcStep = new EfcCalculationSteps.RevisedAdjustedEffectiveIncome();
            calcStep.doCalculation(worksheet);
            Long endTime = timerStop();
            debugLog('RevisedAdjustedEffectiveIncome', 'Calculated revised adjusted effective income  = ' + worksheet.revisedAdjustedEffectiveIncome + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('RevisedAdjustedEffectiveIncome', e);
        }    
        return null;
    }
    
    public PageReference calcIncomeProtectionAllowance() {
        try {
            timerStart();
            EfcCalculationSteps.IncomeProtectionAllowance calcStep = new EfcCalculationSteps.IncomeProtectionAllowance();
            calcStep.doCalculation(worksheet);
            Long endTime = timerStop();
            debugLog('IncomeProtectionAllowance', 'Calculated income protection allowance  = ' + worksheet.incomeProtectionAllowance + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('IncomeProtectionAllowance', e);
        }    
        return null;
    }
    
    public PageReference calcDiscretionaryIncome() {
        try {
            timerStart();
            EfcCalculationSteps.DiscretionaryIncome calcStep = new EfcCalculationSteps.DiscretionaryIncome();
            calcStep.doCalculation(worksheet);
            Long endTime = timerStop();
            debugLog('DiscretionaryIncome', 'Calculated discretionary income  = ' + worksheet.discretionaryIncome + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('DiscretionaryIncome', e);
        }    
        return null;
    }
    
    public PageReference calcEstimatedParentalContribution() {
        try {
            timerStart();
            EfcCalculationSteps.EstimatedParentalContribution calcStep = new EfcCalculationSteps.EstimatedParentalContribution();
            calcStep.doCalculation(worksheet);
            Long endTime = timerStop();
            debugLog('EstimatedParentalContribution', 'Calculated estimated parental contribution  = ' + worksheet.estimatedParentalContribution + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('EstimatedParentalContribution', e);
        }    
        return null;
    }
    
    public PageReference calcEstimatedParentalContributionPerChild() {
        try {
            timerStart();
            EfcCalculationSteps.EstimatedParentalContributionPerChild calcStep = new EfcCalculationSteps.EstimatedParentalContributionPerChild();
            calcStep.doCalculation(worksheet);
            Long endTime = timerStop();
            debugLog('EstimatedParentalContributionPerChild', 'Calculated estimated parental contribution per child Day/Boarding = ' + worksheet.estimatedParentalContributionPerChild + '/' + worksheet.estimatedParentalContributionPerChildBoarding + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('EstimatedParentalContributionPerChild', e);
        }    
        return null;
    }
    
    public PageReference calcStudentAssetContribution() {
        try {
            timerStart();
            EfcCalculationSteps.StudentAssetContribution calcStep = new EfcCalculationSteps.StudentAssetContribution();
            calcStep.doCalculation(worksheet);
            Long endTime = timerStop();
            debugLog('StudentAssetContribution', 'Calculated student asset contribution  = ' + worksheet.studentAssetContribution + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('StudentAssetContribution', e);
        }    
        return null;
    }
    
    public PageReference calcEstimatedFamilyContribution() {
        try {
            timerStart();
            EfcCalculationSteps.EstimatedFamilyContribution calcStep = new EfcCalculationSteps.EstimatedFamilyContribution();
            calcStep.doCalculation(worksheet);
            Long endTime = timerStop();
            debugLog('EstimatedFamilyContributionDay', 'Calculated EFC Day/Boarding  = ' + worksheet.estimatedFamilyContributionDay + '/' + worksheet.estimatedFamilyContributionBoarding + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('EstimatedFamilyContributionDay', e);
        }    
        return null;
    }
    
    
    
    public PageReference calcAll() {
        try {
            timerStart();
            EfcCalculator calculator= new EfcCalculator();
            
            calculator.calculateEfc(worksheet);
            Long endTime = timerStop();
            if (worksheet.estimatedFamilyContribution != null) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Estimated Family Contribution = ' + worksheet.estimatedFamilyContribution));
            }
            else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Estimated Family Contribution Day / Boarding = ' + worksheet.estimatedFamilyContributionDay + ' / ' + worksheet.estimatedFamilyContributionBoarding));
            }
            debugLog('CalcAll', 'Elapsed time = ' + endTime + ' ms');
        }
        catch (EfcException e) {
            handleException('CalcAll', e);
        }
        
        return null;
    }
    
    public PageReference lookupFederalTax() {
        try {
            timerStart();
            Decimal federalIncomeTax = TableFederalIncomeTax.calculateTax(tableAcademicYear, 
                                                                            tableFilingStatus,
                                                                            tableFederalIncome,
                                                                            tablefilingStatusParentB);
            Long endTime = timerStop();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Federal Income Tax = ' + federalIncomeTax));
            debugLog('FederalTaxTable', 'Calculated federal tax = ' + federalIncomeTax + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('FederalTaxTable', e);
        }        
        return null;
    }
    
    public PageReference lookupStateTax() {
        try {
            timerStart();
            Decimal stateIncomeTax = TableStateIncomeTax.calculateTax(tableAcademicYear, 
                                                                            tableState,
                                                                            tableCountry,
                                                                            tableStateIncome);
            Long endTime = timerStop();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'State Income Tax = ' + stateIncomeTax));
            debugLog('StateTaxTable', 'Calculated state tax = ' + stateIncomeTax + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('CalcAllSSS', e);
        }
        
        return null;
    }
    
    public PageReference lookupHousingIndexMultiplier() {
        try {
            timerStart();
            Decimal himValue = TableHousingIndexMultiplier.getHousingIndexMultiplier(tableAcademicYear, tableHimHomePurchaseYear);
            Long endTime = timerStop();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Housing Index Multiplier = ' + himValue));
            debugLog('HousingIndexMultiplier', 'Calculated housing index multiplier = ' + himValue + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('HousingIndexMultiplier', e);
        }
        
        return null;
    }
    
    public PageReference lookupBusinessFarmShare() {
        try {
            timerStart();
            Decimal businessFarmShare = TableBusinessFarmShare.calculateBusinessFarmShare(tableAcademicYear, tableValueOfBusinessFarm);
            Long endTime = timerStop();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Business/Farm share = ' + businessFarmShare));
            debugLog('BusinessFarmShare', 'Calculated business/farm share = ' + businessFarmShare + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('BusinessFarmShare', e);
        }
        
        return null;
    }
    
    public PageReference lookupRetirementAllowance() {
        try {
            timerStart();
            TableRetirementAllowance.AllowanceData retirementAllowance = TableRetirementAllowance.getRetirementAllowance(tableAcademicYear, tableFamilyStatus, tableAge);
            Long endTime = timerStop();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Retirement Allowance = ' + retirementAllowance.allowance + '; conversion coefficient = ' + retirementAllowance.conversionCoefficient));
            debugLog('RetirementAllowance', 'Retirement allowance = ' + retirementAllowance.allowance + ' / ' + retirementAllowance.conversionCoefficient + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('RetirementAllowance', e);
        }
        
        return null;
    }
    
    
    
    public PageReference lookupEmploymentAllowance() {
        try {
            timerStart();
            Decimal employmentAllowance = TableEmploymentAllowance.calculateAllowance(tableAcademicYear, tableEmploymentAllowanceIncome);
            Long endTime = timerStop();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Employment Allowance = ' + employmentAllowance));
            debugLog('EmploymentAllowance', 'Calculated Employment Allowance = ' + employmentAllowance + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('EmploymentAllowance', e);
        }
        
        return null;
    }
    
    public PageReference lookupAssetProgressivity() {
        try {
            timerStart();
            Decimal incomeSupplement = TableAssetProgressivity.calculateIncomeSupplement(tableAcademicYear, tableDiscretionaryNetWorth, tableConversionCoefficient);
            Long endTime = timerStop();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Income Supplement = ' + incomeSupplement));
            debugLog('AssetProgressivity', 'Calculated income supplement = ' + incomeSupplement + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('AssetProgressivity', e);
        }
        
        return null;
    }
    
    public PageReference lookupIncomeProtectionAllowance() {
        try {
            timerStart();
            worksheet.familySize = tableFamilySize;
            worksheet.academicYearId = tableAcademicYear;
            Decimal incomeProtectionAllowance = TableIncomeProtectionAllowance.getIncomeProtectionAllowance(worksheet)[2];
            //Decimal incomeProtectionAllowance = TableIncomeProtectionAllowance.getIncomeProtectionAllowance(tableAcademicYear, tableFamilySize);
            
            Long endTime = timerStop();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'income protection allowance = ' + incomeProtectionAllowance));
            debugLog('IncomeProtectionAllowance', 'Calculated income protection allowance = ' + incomeProtectionAllowance + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('IncomeProtectionAllowance', e);
        }
        
        return null;
    }
    
    public PageReference lookupEstimatedContributionRate() {
        try {
            timerStart();
            Decimal estimatedContributionRate = TableEstimatedContributionRate.getEstimatedContributionRate(tableAcademicYear, tableContributionDiscretionaryIncome);
            Long endTime = timerStop();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'estimated contribution rate = ' + estimatedContributionRate));
            debugLog('EstimatedContributionRate', 'Calculated estimated contribution rate = ' + estimatedContributionRate + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('EstimatedContributionRate', e);
        }
        
        return null;
    }
    
    
    
    public PageReference lookupSssConstants() {
        try {
            timerStart();
            SSS_Constants__c sssConstants = EfcConstants.getSssConstantsForYear(tableAcademicYear);
            Long endTime = timerStop();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, String.valueOf(sssConstants)));
            debugLog('SssConstants', 'SSS Constants = ' + sssConstants + ' (elapsed time = ' + endTime + ' ms)');
        }
        catch (EfcException e) {
            handleException('CalcAllSSS', e);
        }
        return null;
    }
    
    public void debugLog(String logCategory, String logMessage) {
        debugLog += System.now().format('HH:mm:ss:SSS') + ' [' + logCategory + ']' + ' ' + logMessage + '\n';
    }
    
    public void clearLog() {
        debugLog = '';
    }
    
    public void timerStart() {
        timerStart = System.now().getTime();
    }
    
    public Long timerStop() {
        Long timerStop = System.now().getTime();
        return (timerStop - timerStart);
    }
    
    public void handleException(String logCategory, Exception e) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        debugLog(logCategory, 'Exception: ' + e.getMessage());
        debugLog(logCategory, 'Stack Trace: ' + e.getStackTraceString());
    }
    
    
    public list<SelectOption> getFilingStatusOptions(){
        list<SelectOption> options = new list<SelectOption>{new SelectOption('','')};
        options.addAll(applicationUtils.getPicklistValues(Schema.sObjectType.School_PFS_Assignment__c.fields.Filing_Status__c));
        return options;
    }
     
    public list<SelectOption> getParentBFilingStatusOptions(){
         list<SelectOption> options = new list<SelectOption>{new SelectOption('','')};
         options.addAll(applicationUtils.getPicklistValues(Schema.sObjectType.School_PFS_Assignment__c.fields.Parent_B_Filing_Status__c));
         return options;
     }    
}