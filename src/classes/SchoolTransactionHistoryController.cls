/*
 * NAIS-2376 Transaction History Page in School Portal
 *
 * WH, Exponent Partners, 2015
 */
public without sharing class SchoolTransactionHistoryController implements SchoolAcademicYearSelectorInterface {
    
    /* CONSTANTS */
    
    /* END CONSTANTS */
    
    
    /* PROPERTIES */
    public SchoolTransactionHistoryController Me { get { return this; } }
        
    public List<SchoolTransaction> transactions { get; private set; }
    /* END PROPERTIES */
    
    
    /* INITIALIZATION */
    public SchoolTransactionHistoryController() {
        Id schoolId = GlobalVariables.getCurrentSchoolId();
        
        transactions = new List<SchoolTransaction>();
        
        for (Opportunity o : [select Id, RecordType.Name, Academic_Year_Picklist__c, CloseDate, Subscription_Type__c, Total_Payments__c 
                                from Opportunity 
                                where AccountId = :schoolId 
                                    and IsClosed = true 
                                    and RecordTypeId in (:RecordTypes.opportunityFeeWaiverPurchaseTypeId, :RecordTypes.opportunitySubscriptionFeeTypeId)
                                order by CloseDate desc]) {
            String transactionName = o.CloseDate.format() + ' - ' + o.RecordType.Name;
            transactionName += (o.RecordTypeId == RecordTypes.opportunitySubscriptionFeeTypeId) ? (' - ' + o.Subscription_Type__c) : '';
            transactions.add( new SchoolTransaction(o.Id, transactionName, o.Total_Payments__c, o.Academic_Year_Picklist__c) );
        }
    }
    /* END INITIALIZATION */
    
    
    /* ACTION METHODS */
    
    /* END ACTION METHODS */
    
    
    /* HELPER METHODS */
    
    /* END HELPER METHODS */
    
    
    /* INNER CLASS */
    public class SchoolTransaction {
        
        /* PROPERTIES */
        public String oppId { get; private set; }
        public String name { get; private set; }
        public Decimal amount { get; private set; }
        public String academicYear { get; private set; }
        /* END PROPERTIES */
        
        /* INITIALIZATION */
        public SchoolTransaction(Id oppId, String name, Decimal amount, String year) {
            this.oppId = oppId;
            this.name = name;
            this.amount = amount;
            this.academicYear = year;
        }
        /* END INITIALIZATION */
        
    }
    
    /* END INNER CLASS */
    
    
    /* SchoolAcademicYearSelectorInterface Implementation */
    private Id academicYearId;
    
    public String getAcademicYearId() {
        return academicYearId;
    }
    
    public void setAcademicYearId(String idValue) {
        academicYearId = idValue; 
    }

     public PageReference SchoolAcademicYearSelector_OnChange(Boolean saveRecord) {
        return null;
    }
    
}