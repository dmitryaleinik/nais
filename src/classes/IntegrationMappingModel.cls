/**
 * IntegrationMappingModel.cls
 *
 * @description: 
 *
 * @author: Mike Havrila @ Presence PG
 */

public class IntegrationMappingModel {

    /* properties */
    public Boolean allowOverwrite { get; set; }
    public String dataType { get; set; }
    public String source { get; set; }
    public String sourceField { get; set; }
    public String sourceObject { get; set; }
    public String targetField { get; set; }
    public String targetObject { get; set; }
    public Boolean isSourceComponded { get; set; }

    public IntegrationMappingModel( IntegrationMapping__mdt integrationMapping) {
        this.allowOverwrite = integrationMapping.AllowOverwrite__c;
        this.dataType = integrationMapping.DataType__c;
        this.source = integrationMapping.Source__C;
        this.sourceField = integrationMapping.SourceField__c;
        this.sourceObject = integrationMapping.SourceObject__c;
        this.targetField = integrationMapping.TargetField__c;
        this.targetObject = integrationMapping.TargetObject__c;
        this.isSourceComponded = this.isCompoundValue( this.sourceField, ',');
    }

    public Boolean isCompoundValue( String value, String delimiter) {
        return value.contains( delimiter);
    }
}