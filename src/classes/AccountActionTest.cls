/*
 * NAIS-967 Roll Up Subscription Status and Type to Account
 *
 * 1) set Account.SSS_Subscriber_Status__c
 *    no subscription records for the account --> Never
 *    no current subscription records and at least one expired or cancelled --> Former
 *    most recent subscription record is current (start date today or in the past, end date in the future and not cancelled) --> Current
 *
 * 2) set Account.SSS_Subscription_Type_Current__c
 *    most recent subscription is current --> Subscription.Subscription_Type__c
 *    most recent subscription is not current --> null
 *
 * WH, Exponent Partners, 2013
 */
@isTest
private class AccountActionTest
{

    // [CH] NAIS-1766, [GH] NAIS-2094
    @isTest
    private static void testInsertPublicGroups()
    {
        // Verify that no public groups exist yet
        // List<Group> existingGroups = [select Id from Group];
        DateTime checkpoint = DateTime.now();
        Decimal existingGroupsCount = [select Count() from Group where createdDate >= :checkpoint];

        Account account1 = TestUtils.createAccount('Account 1', RecordTypes.schoolAccountTypeId, 1, false);
        Account account2 = TestUtils.createAccount('Account 2', RecordTypes.schoolAccountTypeId, 1, false);
        Account account3 = TestUtils.createAccount('Account 3', RecordTypes.schoolAccountTypeId, 1, false);
        account3.SSS_School_code__c = '123456';
        Account account4 = TestUtils.createAccount('Account 4 1234567890 1234567890 12345', RecordTypes.schoolAccountTypeId, 1, false);
        account4.SSS_School_code__c = '12345';
        Account account5 = TestUtils.createAccount('Account 5 1234567890 1234567890 1234567890', RecordTypes.schoolAccountTypeId, 1, false);
        account5.SSS_School_code__c = '1234';

        Database.insert(new List<Account>{account1, account2, account3, account4, account5});

        DateTime currentDay = system.now();
        List<Group> allGroups = [select Id, name from Group WHERE CreatedDate = :currentDay];
        for(Group g : allGroups){
            if(g.Name.startsWith('Account 3')){
                system.assertEquals(account3.Name.length() + account3.SSS_School_Code__c.length() + 3, g.Name.length());
            }else if(g.Name.startsWith('Account 4') || g.Name.startsWith('Account 5')){
                system.assertEquals(40, g.Name.length());
            }
        }
        Decimal newGroupsCount = [select Count() from Group where createdDate >= :checkpoint];
        System.assertEquals(existingGroupsCount + 5, newGroupsCount);

        account3.name = 'Updated Account 3 12345 12345 12345 12345';
        Database.update(account3);
        List<Group> updatedGroups = [select Id, name from Group WHERE CreatedDate = :currentDay];
        for(Group g : updatedGroups){
            if(g.Name.startsWith('Updated Account 3')){
                system.assertEquals(40, g.Name.length());
            }
        }
    }

    @isTest
    private static void subscriptionStatusRolledUpToAccount() {
        Account a1 = TestUtils.createAccount('School 1', RecordTypes.schoolAccountTypeId, 2, false);
        Account a2 = TestUtils.createAccount('School 2', RecordTypes.schoolAccountTypeId, 2, false);
        Account a3 = TestUtils.createAccount('Access Org', RecordTypes.accessOrgAccountTypeId, 2, false);
        List<Account> accounts = new List<Account> { a1, a2, a3 };
        insert accounts;

        // account a1 - 1 'Not Started Yet', 1 'Current' subscription
        Subscription__c s11 = TestUtils.createSubscription(a1.Id, Date.today().addDays(3), Date.today().addYears(1), 'No', 'Extended', false);
        Subscription__c s12 = TestUtils.createSubscription(a1.Id, Date.today().addDays(-3), Date.today().addYears(1), 'No', 'Full', false);

        // account a2 - 1 'Expired', 1 'Cancelled' subscription
        Subscription__c s21 = TestUtils.createSubscription(a2.Id, Date.today().addYears(-1), Date.today().addDays(-1), 'No', 'Waiver Distributor', false);
        Subscription__c s22 = TestUtils.createSubscription(a2.Id, Date.today().addDays(-3), Date.today().addYears(1), 'Yes', 'Basic', false);

        // account s3 - 1 'Not Started Yet' subscription
        Subscription__c s31 = TestUtils.createSubscription(a3.Id, Date.today().addDays(3), Date.today().addYears(1), 'No', 'Extended', false);

        Test.startTest();
        insert new List<Subscription__c> { s11, s12, s21, s22, s31 };

        Map<Id, Account> updatedAccounts = new Map<Id, Account>(
                [select Id, SSS_Subscriber_Status__c, SSS_Subscription_Type_Current__c from Account where Id in :accounts] );

        System.assertEquals('Current',    updatedAccounts.get(a1.Id).SSS_Subscriber_Status__c);
        System.assertEquals('Full',        updatedAccounts.get(a1.Id).SSS_Subscription_Type_Current__c);
        System.assertEquals('Former',    updatedAccounts.get(a2.Id).SSS_Subscriber_Status__c);
        System.assertEquals(null,        updatedAccounts.get(a2.Id).SSS_Subscription_Type_Current__c);
        System.assertEquals('Never',    updatedAccounts.get(a3.Id).SSS_Subscriber_Status__c);
        System.assertEquals(null,        updatedAccounts.get(a3.Id).SSS_Subscription_Type_Current__c);

        // updates to subscription status

        s12.Cancelled__c = 'Yes';
        s12.Cancellation_Date__c = Date.today();
        s12.Reason_Cancelled__c = 'Test reason';
        s12.Charge_Cancellation_Fee__c = 'No';

        s22.Cancelled__c = 'No';
        s31.Start_Date__c = Date.today();

        update new List<Subscription__c> { s12, s22, s31 };

        updatedAccounts = new Map<Id, Account>(
                [select Id, SSS_Subscriber_Status__c, SSS_Subscription_Type_Current__c from Account where Id in :accounts] );

        System.assertEquals('Former',    updatedAccounts.get(a1.Id).SSS_Subscriber_Status__c);
        // NAIS-2373 maintain prior subscription type after expiration
        //System.assertEquals(null,        updatedAccounts.get(a1.Id).SSS_Subscription_Type_Current__c);
        System.assertEquals('Full',        updatedAccounts.get(a1.Id).SSS_Subscription_Type_Current__c);
        System.assertEquals('Current',    updatedAccounts.get(a2.Id).SSS_Subscriber_Status__c);
        System.assertEquals('Basic',    updatedAccounts.get(a2.Id).SSS_Subscription_Type_Current__c);
        System.assertEquals('Current',    updatedAccounts.get(a3.Id).SSS_Subscriber_Status__c);
        System.assertEquals('Extended',    updatedAccounts.get(a3.Id).SSS_Subscription_Type_Current__c);

        // updates to subscription type

        s12.Subscription_Type__c = 'Basic';
        s31.Subscription_Type__c = 'Full';

        update new List<Subscription__c> { s12, s31 };

        updatedAccounts = new Map<Id, Account>(
                [select Id, SSS_Subscriber_Status__c, SSS_Subscription_Type_Current__c from Account where Id in :accounts] );

        System.assertEquals('Former',    updatedAccounts.get(a1.Id).SSS_Subscriber_Status__c);
        // NAIS-2373 maintain prior subscription type after expiration
        //System.assertEquals(null,        updatedAccounts.get(a1.Id).SSS_Subscription_Type_Current__c);
        System.assertEquals('Full',        updatedAccounts.get(a1.Id).SSS_Subscription_Type_Current__c);
        System.assertEquals('Current',    updatedAccounts.get(a2.Id).SSS_Subscriber_Status__c);
        System.assertEquals('Basic',    updatedAccounts.get(a2.Id).SSS_Subscription_Type_Current__c);
        System.assertEquals('Current',    updatedAccounts.get(a3.Id).SSS_Subscriber_Status__c);
        System.assertEquals('Full',        updatedAccounts.get(a3.Id).SSS_Subscription_Type_Current__c);

        // delete
        delete new List<Subscription__c> { s12, s22, s31 };

        updatedAccounts = new Map<Id, Account>(
                [select Id, SSS_Subscriber_Status__c, SSS_Subscription_Type_Current__c from Account where Id in :accounts] );

        System.assertEquals('Never',    updatedAccounts.get(a1.Id).SSS_Subscriber_Status__c);
        System.assertEquals(null,        updatedAccounts.get(a1.Id).SSS_Subscription_Type_Current__c);
        System.assertEquals('Former',    updatedAccounts.get(a2.Id).SSS_Subscriber_Status__c);
        // NAIS-2373 maintain prior subscription type after expiration
        //System.assertEquals(null,        updatedAccounts.get(a2.Id).SSS_Subscription_Type_Current__c);
        System.assertEquals('Basic',    updatedAccounts.get(a2.Id).SSS_Subscription_Type_Current__c);
        System.assertEquals('Never',    updatedAccounts.get(a3.Id).SSS_Subscriber_Status__c);
        System.assertEquals(null,        updatedAccounts.get(a3.Id).SSS_Subscription_Type_Current__c);

        // undelete
        undelete s31;

        updatedAccounts = new Map<Id, Account>(
                [select Id, SSS_Subscriber_Status__c, SSS_Subscription_Type_Current__c from Account where Id in :accounts] );

        System.assertEquals('Never',    updatedAccounts.get(a1.Id).SSS_Subscriber_Status__c);
        System.assertEquals(null,        updatedAccounts.get(a1.Id).SSS_Subscription_Type_Current__c);
        System.assertEquals('Former',    updatedAccounts.get(a2.Id).SSS_Subscriber_Status__c);
        // NAIS-2373 maintain prior subscription type after expiration
        //System.assertEquals(null,        updatedAccounts.get(a2.Id).SSS_Subscription_Type_Current__c);
        System.assertEquals('Basic',    updatedAccounts.get(a2.Id).SSS_Subscription_Type_Current__c);
        System.assertEquals('Current',    updatedAccounts.get(a3.Id).SSS_Subscriber_Status__c);
        System.assertEquals('Full',        updatedAccounts.get(a3.Id).SSS_Subscription_Type_Current__c);

        Test.stopTest();
    }

    @isTest
    private static void testAccountInsert_LongGroupName()
    {
        Integer randomNumber = Integer.valueOf(Math.random());
        String schoolName = 'Chrispion' + randomNumber + ' School of Appointed Bird';
        String sssSchoolCode = '200001';
        Account school = AccountTestData.Instance
            .asSchool()
            .forName(schoolName)
            .forSSSSchoolCode(sssSchoolCode).create();

        Test.startTest();
            insert school;
        Test.stopTest();

        String expectedGroupName = schoolName.substring(0, 37 - sssSchoolCode.length()) + ' - ' + sssSchoolCode;
        List<Group> publicGroups = [
            SELECT Id
            FROM Group
            WHERE Name =:expectedGroupName];
        System.assertEquals(1, publicGroups.size());
    }
}