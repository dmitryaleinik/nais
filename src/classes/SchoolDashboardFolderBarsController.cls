public with sharing class SchoolDashboardFolderBarsController {//In the prior version the stats (loadPFSStats) where calculated without sharing, that caused that the report was inaccurated in some cases
    
    private static final String PFS_PAYMENT_STATUS_PAID_IN_FULL = 'Paid in Full';
    
    /*Initialization*/
    public SchoolDashboardFolderBarsController(){

    }
    /*End Initialization*/
    
    public String acadYearName {get; set;}
    
    public String getReportId(){
        return SchoolPortalSettings.Folder_Status_Report_Id;
    }
    
    @testVisible 
    private static FolderBarsWrapper loadAwardStats(FolderBarsWrapper result, Set<Id> folderIds)
    {
        Set<String> approvedStatuses = new Set<String> {'Award Approved', 'Complete, Award Accepted'};

        List<AggregateResult> awardResults = new List<AggregateResult>([
            SELECT count(ID) awdCount, sum(Grant_Awarded__c) amt, avg(Grant_Awarded__c) avgAmt 
            FROM Student_Folder__c
            WHERE Id in :folderIds
                AND Folder_Status__c in :approvedStatuses]);

        if( !awardResults.isEmpty() ){
            result.awardsGiven = Integer.valueOf(awardResults[0].get('awdCount'));
            result.avgAward = Integer.valueOf(awardResults[0].get('avgAmt'));
        }
        return result;
    }
    /**
    * @description Method implemente to retrieve the folder's ids in the current
    * school and the given academic year.
    */
    @testVisible
    private static Map<Id, Student_Folder__c> getFolderIds(String varAcademicYearName)
    {
        return new Map<Id, Student_Folder__c>([SELECT Id, (SELECT Id, Applicant__r.PFS__c FROM School_PFS_Assignments__r) 
                                                FROM Student_Folder__c 
                                                WHERE School__c = : GlobalVariables.getCurrentSchoolId()  
                                                    AND Academic_Year_Picklist__c = :varAcademicYearName]);
    }//End:getFolderIds
    
    @testVisible 
    private static Set<Id> getPFSIds(Map<Id, Student_Folder__c> folders)
    {
        Set<Id> pfsIds = new Set<Id>();
        for(Student_Folder__c f:folders.values())
        {
            if( !f.School_PFS_Assignments__r.isEmpty() )
            {
                for(School_PFS_Assignment__c spa : f.School_PFS_Assignments__r)
                {
                    if( !pfsIds.contains(spa.Applicant__r.PFS__c) )
                    {
                        pfsIds.add( spa.Applicant__r.PFS__c );
                    }
                }
            }
        }
        return pfsIds;  
    }//End:getPFS
    
    @testVisible 
    private static FolderBarsWrapper loadPFSStats(FolderBarsWrapper result, Set<Id> PFSIds, String varAcademicYearName)
    {
        Id schoolId = GlobalVariables.getCurrentSchoolId();
        Date pastWeek = system.today().addDays(-7);
        Date pastDay = System.today().addDays(-1);

        List<Id> uniquePFSIdsPastDay = new List<Id>();
        List<Id> uniquePFSIdsPastWeek = new List<Id>();
        
        for(School_PFS_Assignment__c spa : [SELECT Id, Applicant__r.PFS__c, Applicant__r.PFS__r.Original_Submission_Date__c, 
                                                Applicant__r.PFS__r.PFS_Status__c, CreatedDate, Applicant__r.PFS__r.Payment_Status__c
                                            FROM School_PFS_Assignment__c 
                                            WHERE Applicant__r.PFS__r.Academic_Year_Picklist__c = :varAcademicYearName 
                                                AND School__c =: schoolId
                                                AND Withdrawn__c = 'No'
                                                AND PFS_Received_by_School__c >= :pastWeek
                                                AND Applicant__r.PFS__r.Payment_Status__c = :PFS_PAYMENT_STATUS_PAID_IN_FULL])
        {
            if(spa.Applicant__r.PFS__r.Original_Submission_Date__c >= pastDay 
            || (spa.Applicant__r.PFS__r.Payment_Status__c == PFS_PAYMENT_STATUS_PAID_IN_FULL && spa.CreatedDate >= pastDay)){
                uniquePFSIdsPastDay.add(spa.Id);
            }
            uniquePFSIdsPastWeek.add(spa.Id);
        }

        result.pfsPastDay = uniquePFSIdsPastDay.size();
        result.pfsPastWeek = uniquePFSIdsPastWeek.size();
        result.pfsUpdatedPastDay = [
            SELECT Count()
            FROM School_PFS_Assignment__c 
            WHERE Applicant__r.PFS__c IN :PFSIds 
                AND Application_Last_Modified_by_Family__c > :pastDay 
                AND School__c =: schoolId
                AND Withdrawn__c = 'No'
                AND Applicant__r.PFS__r.Payment_Status__c = :PFS_PAYMENT_STATUS_PAID_IN_FULL];

        return result;
    }
    
    /**
    * @description Method that sets the values to be set in the chart
    * in to varMap.
    * @param ids The set of folder ids to be retrieved.
    */
    @testVisible 
    private static Integer mappingFolders(Map<String, Integer> varMap, Set<Id> ids)
    {
        Integer counter = 0;
        for (AggregateResult ar : [SELECT Folder_Status__c status, Count(Id) counter 
                                    FROM Student_Folder__c 
                                    WHERE Id IN : ids    
                                        AND Folder_Status__c != null 
                                      GROUP BY Folder_Status__c])
        {
            varMap.put(String.valueOf(ar.get('status')), Integer.valueOf(ar.get('counter')));
            counter += Integer.valueOf(ar.get('counter'));
        }
        return counter;
    }//End:mappingFolders
    
    @testVisible 
    private static List<VFChartData> setChartValues(Map<String, Integer> dataResult)
    {
        List<VFChartData> result = new List<VFChartData>();
        String status;
        VFChartData chart;
        for(Schema.PicklistEntry p : Student_Folder__c.Folder_Status__c.getDescribe().getPicklistValues()){
            status = p.getValue();
            chart = new VFChartData(status, 
                                        dataResult.get(status) == null 
                                            ? 0 
                                            : dataResult.get(status) );
            chart.Name = ( chart.Name.contains('Complete, ') 
                                ? chart.Name.replace('Complete, ', '') 
                                : chart.Name);
            result.add(chart);
        }
        return result;
    }//End:setChartValues
    
    @RemoteAction
    public static FolderBarsWrapper getSchoolDashboardChartData(String varAcademicYearName)
    {
        FolderBarsWrapper resultWrapper = new FolderBarsWrapper();
        List<VFChartData> result = new List<VFChartData>();
        
        Map<Id, Student_Folder__c> folderIdsMap = getFolderIds(varAcademicYearName);
        Set<Id> pfsIds = getPFSIds(folderIdsMap);
        
        if( !folderIdsMap.isEmpty() ){
            Map<String, Integer> dataResult = new Map<String, Integer>();
            resultWrapper.counter = mappingFolders(dataResult, folderIdsMap.keyset());//Pass dataResult by reference 
            resultWrapper.result = setChartValues(dataResult);
            loadPFSStats(resultWrapper, pfsIds, varAcademicYearName);
            loadAwardStats(resultWrapper, folderIdsMap.keyset());
        }
        
        return resultWrapper;
    }//End:getSchoolDashboardChartData
    
    public class FolderBarsWrapper{
        public List<VFChartData> result{get;set;}
        public Integer counter{get;set;}
        public Integer pfsPastWeek {get; set;}
        public Integer pfsPastDay {get; set;}
        public Integer pfsUpdatedPastDay {get; set;}
        public Integer awardsGiven {get; set;}
        public Double avgAward {get; set;}
    
        public FolderBarsWrapper(){
            result = new List<VFChartData>();
            result = setChartValues(new Map<String, Integer>() );
            counter = 0;
            pfsPastWeek = 0;
            pfsPastDay = 0;
            pfsUpdatedPastDay = 0;
            awardsGiven = 0;
            avgAward = 0;
        }
    }//End-Class:FolderBarsWrapper
}//End-Class