public without sharing class SchoolDashboardBudgetPiesController {

    /*Initialization*/
    public SchoolDashboardBudgetPiesController(){
    }
    /*End Initialization*/
   
    /*Properties*/
    public Id schoolId {get; set;}
    public Id academicYearId {get; set;}
    public static Id varSchoolId {get; set;}
    public static Id varAcademicYearId {get; set;}
    
    public static BudgetPieWrapper pieWrapperResult
    {
        get
        {
            if (pieWrapperResult == null)
            {
                pieWrapperResult = new BudgetPieWrapper();
                for (Budget_Group__c bg : [Select Id, Name, Total_Allocated__c, Total_Funds__c, Total_Remaining__c 
                                                       FROM Budget_Group__c 
                                                           WHERE School__c = :varSchoolId
                                                               AND Academic_Year__c = :varAcademicYearId
                                                               ORDER BY Total_Funds__c desc])
                {
                    pieWrapperResult.totalBudget += bg.Total_Funds__c == null ? 0 : Integer.valueOf(bg.Total_Funds__c);
                    
                    pieWrapperResult.totalRemaining += bg.Total_Remaining__c == null ? 0 : Integer.valueOf(bg.Total_Remaining__c);
                    
                    pieWrapperResult.allocatedBudget += bg.Total_Allocated__c == null ? 0 : Integer.valueOf(bg.Total_Allocated__c);
                    
                    pieWrapperResult.budgetPies.add(new SchoolDashboardBudgetTinyPie(bg));
                }
                if( pieWrapperResult.totalRemaining>0 || pieWrapperResult.allocatedBudget>0 ){
                    pieWrapperResult.result = new List<VFChartData>{
                                                new VFChartData('Unallocated', pieWrapperResult.totalRemaining),
                                                new VFChartData('Total Allocated', pieWrapperResult.allocatedBudget)
                                                };
                }
            }
            return pieWrapperResult;
        }
        private set;
    }
    /*End Properties*/
       
    /*Action Methods*/
    @RemoteAction
    public static BudgetPieWrapper getSchoolBudgetPieData(Id varSchoolId1, Id varAcademicYearId1)
    {
        varSchoolId = varSchoolId1;
        varAcademicYearId = varAcademicYearId1;
        return pieWrapperResult;
    }
    /*End Action Methods*/
   
    /*Helper Methods*/
    /*End Helper Methods*/
    public List<String> getChartNames(){
        return new List<String>{
                        'tinyPie0',
                        'tinyPie1',
                        'tinyPie2',
                        'tinyPie3',
                        'tinyPie4',
                        'tinyPie5',
                        'tinyPie6',
                        'tinyPie7',
                        'tinyPie8',
                        'tinyPie9'};
    }
    /* Internal Class */
    public class BudgetPieWrapper{
        public List<VFChartData> result {get;set;}
        public List<SchoolDashboardBudgetTinyPie> budgetPies{get; set;}
        public Integer totalBudget {get; set;}
        public Integer totalRemaining {get; set;}
        public Integer allocatedBudget {get; set;}
    
        public BudgetPieWrapper(){
            result = new List<VFChartData>{
                                        new VFChartData('Unallocated', 0),
                                        new VFChartData('Total Allocated', 0)
                                        };
            budgetPies = new List<SchoolDashboardBudgetTinyPie>();
            totalBudget = 0;
            totalRemaining = 0;
            allocatedBudget = 0;
        }
    }//End-Class:BudgetPieWrapper
    /* End Internal Class */
}