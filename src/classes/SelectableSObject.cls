/**
* @ SelectableSObject.cls
* @date 2/21/2015
* @author Charles Howard for Exponent Partners
* @description Utility class for creating lists of Sobject rows with a boolean 
*   to handle being selected 
*/

public with sharing class SelectableSObject {
        
    public Boolean selected { get; set; }
    public String stringValue1 { get; set; }
    public String stringValue2 { get; set; }
    public String stringValue3 { get; set; }
    public String stringValue4 { get; set; }
    public SObject record { get; set; }
    
    // Getter methods that convert the record into a particular SObject type
    public Contact contact { get{ return (Contact)record; } set; }
    public Student_Folder__c folder { get{ return (Student_Folder__c)record; } set; }
    
    public SelectableSObject(SObject objectRecord){
        record = objectRecord;
        selected = false;
    }
    
    public static List<SelectableSObject> createList(List<SObject> sobjectList){
        List<SelectableSObject> returnList = new List<SelectableSObject>();
        for(SObject r : sobjectList){
            returnList.add(new SelectableSObject(r));
        }
        return returnList;
    }

}