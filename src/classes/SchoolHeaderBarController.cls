public without sharing class SchoolHeaderBarController {

    private Map<Id, Account> currentSchoolsForUserMap {get; set;}
    public Account currentSchool {get; set;}
    public String pageTitle{get; set;}
    public Boolean hasAnnualSetting {get; set;}
    public string academicyearid{get; set;}
    
    public Boolean isAllowAccessWithoutAnnualSettings {get; set;}

    public SchoolHeaderBarController(){
        currentSchoolsForUserMap = new Map<Id, Account>(GlobalVariables.getAllSchoolsForUser());
        currentSchool = currentSchoolsForUserMap.get(GlobalVariables.getCurrentSchoolId());
        pageTitle = SchoolPagesUtils.getSchoolPageTitle(ApexPages.currentPage());//NAIS-2369:School Portal Tab Naming
        academicyearid = System.currentPagereference().getParameters().get('academicyearid');
        academicyearid = (academicyearid==null?GlobalVariables.getCurrentAcademicYear().Id:academicyearid);
        List<Annual_Setting__c> annSets = [Select Id, Annual_Settings_Updated__c
                    from Annual_Setting__c
                    where School__c = :currentSchool.Id
                    AND Academic_Year__c = :academicyearid limit 1];

        Annual_Setting__c annSet = annSets.size() > 0 ? annSets[0] : null;
        //SFP-623: If there's no annualSetting created, or if the existing one do not have the required fields empty.
        String thisPage = (ApexPages.currentPage().getURL()).toLowerCase();

        // TODO SFP-711 hasAnnualSetting should be true for basic school users.
        if (GlobalVariables.isLimitedSchoolView()) {
            this.hasAnnualSetting = true;
        } else {
            this.hasAnnualSetting = ((annSet==null || annSet.Annual_Settings_Updated__c==null)
                    && !thisPage.contains('schoolannualsettings')
                    && !thisPage.contains('schoolrenewal')
                    && !thisPage.contains('logout')?false:true);
        }
        
        
        isAllowAccessWithoutAnnualSettings = false;
        
        if (!hasAnnualSetting) {
            User currentUser = GlobalVariables.getCurrentUser();
            
            if (currentUser.Profile.Name != ProfileSettings.SchoolAdminProfileName) {
                Cookie isVisitedAnnualSettings = ApexPages.currentPage().getCookies().get('AnnualSettingsVisited');
                
                if (isVisitedAnnualSettings != null) {
                    isAllowAccessWithoutAnnualSettings = true;
                }
            }
        }
    }

    /* [CH] Deactivating for migration to prod
    public String walkmeSRC {get; set;}
    */
}