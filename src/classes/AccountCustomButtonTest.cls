@isTest
private class AccountCustomButtonTest {
    
    private static  Account school, school2;
    private static PFS__c recordPFS;
    private static Verification__c verification;
    private static School_PFS_Assignment__c pfsAssignment;
    private static String pfsNumber;
    
    private static void setup(Decimal wages1040, Decimal wagesW2){
        
        String type1040 = VerificationAction.X1040_DOCUMENT_TYPES.get(1);
        String typeW2 = VerificationAction.DOCUMENT_TYPE_W2;
        
        //0. Create the household that will be related to test parents.
        Household__c household = HouseholdTestData.Instance.DefaultHousehold;
        System.AssertNotEquals(null, household.Id);
        
        //1. Setup Databank custom settings.
        SpringCMActionTest.setupCustomSettings();
        SpringCMActionTest.setupMappings();
        
        //2. Create test data records.
        AcademicYearTestData.Instance.insertAcademicYears(5);
        
        String currentAcademicYearStr = GlobalVariables.getCurrentYearString();
        String currentYearStr = GlobalVariables.getDocYearStringFromAcadYearName(currentAcademicYearStr);
        
        school = AccountTestData.Instance.forName('School1')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .create();
                
        school2 = AccountTestData.Instance.forName('School2')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .create();
        insert new List<Account>{school, school2};

        pfsAssignment = SchoolPfsAssignmentTestData.Instance
                .forSchoolId(school.Id)
                .forAcademicYearPicklist(currentAcademicYearStr)
                .create();
        insert pfsAssignment;
        
        Family_Document__c documentW2 = FamilyDocumentTestData.Instance
                .forDocumentType(typeW2)
                .forAcademicYearPicklist(currentAcademicYearStr)
                .forDocYear(currentYearStr)
                .withStatus('Processed')
                .create();
        documentW2.Household__c = household.Id;
        documentW2.Document_Pertains_to__c = 'Parent A';     
        Family_Document__c documen1040 = FamilyDocumentTestData.Instance
                .forDocumentType(type1040)
                .forAcademicYearPicklist(currentAcademicYearStr)
                .forDocYear(currentYearStr)
                .withStatus('Processed')
                .create();
        documen1040.Household__c = household.Id;
        documen1040.Document_Pertains_to__c = 'Parent A';
        insert new List<Family_Document__c>{documentW2, documen1040};

        School_Document_Assignment__c documentAssignmentW2 = 
                SchoolDocumentAssignmentTestData.Instance
                .forDocument(documentW2.Id)
                .forPfsAssignment(pfsAssignment.Id)
                .asType(typeW2)
                .forDocumentYear(currentYearStr)
                .create();
        School_Document_Assignment__c documentAssignment1040 = 
                SchoolDocumentAssignmentTestData.Instance
                .forDocument(documen1040.Id)
                .forPfsAssignment(pfsAssignment.Id)
                .asType(type1040)
                .forDocumentYear(currentYearStr)
                .create();
        insert new List<School_Document_Assignment__c>{documentAssignmentW2, documentAssignment1040};
        
        
        //3. Associate the test parent record to the newly created household
        School_PFS_Assignment__c recordSPA = [
            SELECT Id, Applicant__r.PFS__c, Applicant__r.PFS__r.Parent_A__c, Applicant__c 
            FROM School_PFS_Assignment__c 
            WHERE Id= :pfsAssignment.Id 
            LIMIT 1];
        recordPFS = [
            SELECT Id, Verification__c, Filing_Status__c, PFS_Status__c  
            FROM PFS__c 
            WHERE Id = :recordSPA.Applicant__r.PFS__c 
            LIMIT 1];
        Contact recordParentA = [
            SELECT Id, Household__c 
            FROM Contact  
            WHERE Id = :recordSPA.Applicant__r.PFS__r.Parent_A__c 
            LIMIT 1];
        recordParentA.Household__c = household.Id;
        update recordParentA;
        
        //4. Create the verification record
        verification = verificationTestData.Instance.create();
        verification.Parent_A_1040_Verified__c = true;
        insert verification;
        
        //5. Link the test PFS with the newly created verification
        recordPFS.Verification__c = verification.Id;
        recordPFS.Filing_Status__c = EfcPicklistValues.FILING_STATUS_SINGLE;
        recordPFS.PFS_Status__c = 'Application Submitted';
        recordPFS.Academic_Year_Picklist__c = currentAcademicYearStr;               
        recordPFS.Payment_Status__c = 'Paid in Full';
        update recordPFS;
        
        //6. Create the SpringCM records
        SpringCMAction.isTest = true;
        
        recordPFS = [
            SELECT Id, PFS_Number__c, Academic_Year_Picklist__c, Parent_A__c, Verification__c 
            FROM PFS__c 
            WHERE Id = :recordPFS.Id 
            LIMIT 1];
            
        pfsNumber = recordPFS.PFS_Number__c;
        System.AssertNotEquals(null, pfsNumber);
        
        List<SpringCM_Document__c> springCMDocuments = new List<SpringCM_Document__c>{
            new SpringCM_Document__c(
                Import_Id__c = '1111-2222-3333-4444-5555', 
                Document_Year__c = currentYearStr, 
                Date_Verified__c = System.today(), 
                PFS_Number__c = pfsNumber, 
                Document_Type__c = type1040, 
                Document_Status__c = VerificationAction.PROCESSED, 
                X1040_Wages__c = wages1040, 
                Document_Pertains_to__c = 'A'),
            new SpringCM_Document__c(
                Import_Id__c = '1234-1234-1234-1234-1234', 
                Document_Year__c = currentYearStr, 
                Date_Verified__c = System.today(), 
                PFS_Number__c = pfsNumber, 
                Document_Type__c = typeW2, 
                Document_Status__c = VerificationAction.PROCESSED, 
                W2_WAGES__c = wagesW2, 
                Document_Pertains_to__c = 'A')
        };
        insert springCMDocuments;
    }
    
    private static Family_Document__c updateFamilyDocument( Family_Document__c fd,
                                                            Id householdId,
                                                            String academicYearStr,
                                                            String documentType,
                                                            String documentYear,
                                                            String importId,
                                                            String pertainsTo) {
                                                                
        System.AssertNotEquals(null, academicYearStr);
        System.AssertNotEquals(null, GlobalVariables.getDocYearStringFromAcadYearName(academicYearStr));
        
        fd.Household__c = householdId;
        fd.Academic_Year_Picklist__c = academicYearStr;
        fd.Document_Type__c = documentType;
        fd.Document_Year__c = documentYear;
        fd.Import_Id__c = importId;
        fd.Document_Pertains_to__c = pertainsTo;
        
        return fd;
    }//End:updateFamilyDocument
    
    //SFP-1224
    @isTest static void recalculateVerificationStatuses_school1_verificationDoMatches()
    {
        SchoolPortalSettings.Verification_Match_Tolerance = 2;
        setup(133000, 133001);
        
        pfsAssignment.Verification_Processing_Status__c = 'Processing Complete';
        update pfsAssignment;
        
        Test.startTest();
            AccountCustomButton.recalculateVerificationStatuses(school.Id);
        Test.stopTest();
        
        System.assertEquals(VerificationAction.COMPLETE_MATCH, [SELECT X1040_Wages_Status__c FROM Verification__c WHERE id = :verification.Id].X1040_Wages_Status__c);
    
    }//End:recalculateVerificationStatuses_verificationMatchToleranceIsSet_verificationDoMatches

    @isTest static void recalculateVerificationStatuses_school2_verificationNotDoMatches()
    {
        
        
        SchoolPortalSettings.Verification_Match_Tolerance = 2;
        setup(133000, 133001);
        
        pfsAssignment.Verification_Processing_Status__c = 'Processing Complete';
        update pfsAssignment;
        
        String OldStatus = [SELECT X1040_Wages_Status__c FROM Verification__c WHERE id = :verification.Id].X1040_Wages_Status__c;
        
        Test.startTest();
            AccountCustomButton.recalculateVerificationStatuses(school2.Id);
        Test.stopTest();
        
        System.assertNotEquals(OldStatus, VerificationAction.COMPLETE_MATCH);
        System.assertEquals(OldStatus, [SELECT X1040_Wages_Status__c FROM Verification__c WHERE id = :verification.Id].X1040_Wages_Status__c);
    }
}