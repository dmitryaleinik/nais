/**
 * @description Class used to insert Family_Portal_Settings__c records in a test context.
 */
@isTest
public with sharing class FamilyPortalSettingsTestData extends SObjectTestData
{

    private static final String DEFAULT_NAME = 'Family';

    /**
     * @description Get the default values for the Family_Portal_Settings__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap()
    {
        return new Map<Schema.SObjectField,Object>
            {
                Family_Portal_Settings__c.Name => DEFAULT_NAME
            };
    }

    /**
     * @description Set the Early_Access_Start_Date__c field to +7 days from today for the
     *              current Family Portal Settings record.
     * @return The current instance of FamilyPortalSettingsTestData.
     */
    public FamilyPortalSettingsTestData asEarlyAccessClosed()
    {
        return (FamilyPortalSettingsTestData) with(Family_Portal_Settings__c.Early_Access_Start_Date__c, System.now().addDays(7));
    }

    /**
     * @description Set the Early_Access_Start_Date__c field to -7 days from today for the
     *              current Family Portal Settings record.
     * @return The current instance of FamilyPortalSettingsTestData.
     */
    public FamilyPortalSettingsTestData asEarlyAccessOpened()
    {
        return (FamilyPortalSettingsTestData) with(Family_Portal_Settings__c.Early_Access_Start_Date__c, System.now().addDays(-7));
    }

    /**
     * @description Sets the default EZ household assets to the specified value.
     * @param defaultEZHouseholdAssets The value to set Default_EZ_Household_Assets__c field.
     * @return FamilyPortalSettingsTestData instance.
     */
    public FamilyPortalSettingsTestData forDefaultEZHouseholdAssets(Integer defaultEZHouseholdAssets) 
    {
        return (FamilyPortalSettingsTestData) with(Family_Portal_Settings__c.Default_EZ_Household_Assets__c, defaultEZHouseholdAssets);
    }

    /**
     * @description Sets the early access start date to the specified value.
     * @param earlyAccessStartDate The value to set for the early access start date.
     * @return FamilyPortalSettingsTestData instance.
     */
    public FamilyPortalSettingsTestData forEarlyAccessStartDate(DateTime earlyAccessStartDate) 
    {
        return (FamilyPortalSettingsTestData) with(Family_Portal_Settings__c.Early_Access_Start_Date__c, earlyAccessStartDate);
    }

    /**
     * @description Sets the parent A total salary to the specified value.
     * @param parentATotalSalary The value to set Parent_A_Total_Salary__c field.
     * @return FamilyPortalSettingsTestData instance.
     */
    public FamilyPortalSettingsTestData forParentATotalSalary(Integer parentATotalSalary) 
    {
        return (FamilyPortalSettingsTestData) with(Family_Portal_Settings__c.Parent_A_Total_Salary__c, parentATotalSalary);
    }

    /**
     * @description Sets the parent B total salary to the specified value.
     * @param parentBTotalSalary The value to set Parent_B_Total_Salary__c field.
     * @return FamilyPortalSettingsTestData instance.
     */
    public FamilyPortalSettingsTestData forParentBTotalSalary(Integer parentBTotalSalary) 
    {
        return (FamilyPortalSettingsTestData) with(Family_Portal_Settings__c.Parent_B_Total_Salary__c, parentBTotalSalary);
    }

    /**
     * @description Sets the Adjusment High Percentage field to the specified value.
     * @param adjusmentHighPercentage The value to set Adjusment_High_Percentage__c field.
     * @return FamilyPortalSettingsTestData instance.
     */
    public FamilyPortalSettingsTestData forAdjusmentHighPercentage(Decimal adjusmentHighPercentage) 
    {
        return (FamilyPortalSettingsTestData) with(Family_Portal_Settings__c.Adjusment_High_Percentage__c, adjusmentHighPercentage);
    }
    
    /**
     * @description Insert the current working Family_Portal_Settings__c record.
     * @return The currently operated upon Family_Portal_Settings__c record.
     */
    public Family_Portal_Settings__c insertFamilyPortalSettings()
    {
        return (Family_Portal_Settings__c)insertRecord();
    }

    /**
     * @description Create the current working Family Portal Settings record without resetting
     *              the stored values in this instance of FamilyPortalSettingsTestData.
     * @return A non-inserted Family Portal Settings record using the currently stored field values.
     */
    public Family_Portal_Settings__c createFamilyPortalSettingsWithoutReset() 
    {
        return (Family_Portal_Settings__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Family_Portal_Settings__c record.
     * @return The currently operated upon Family_Portal_Settings__c record.
     */
    public Family_Portal_Settings__c create()
    {
        return (Family_Portal_Settings__c)super.buildWithReset();
    }

    /**
     * @description Singleton instance of FamilyPortalSettingsTestData.
     */
    public static FamilyPortalSettingsTestData Instance
    {
        get
        {
            if (Instance == null)
            {
                Instance = new FamilyPortalSettingsTestData();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description The default Family_Portal_Settings__c record.
     */
    public Family_Portal_Settings__c DefaultFamilyPortalSettings
    {
        get
        {
            if (DefaultFamilyPortalSettings == null)
            {
                DefaultFamilyPortalSettings = createFamilyPortalSettingsWithoutReset();
                insert DefaultFamilyPortalSettings;
            }
            return DefaultFamilyPortalSettings;
        }
        private set;
    }

    /**
     * @description Get the Family_Portal_Settings__c SObjectType.
     * @return The Family_Portal_Settings__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType()
    {
        return Family_Portal_Settings__c.SObjectType;
    }

    private FamilyPortalSettingsTestData() {}
}