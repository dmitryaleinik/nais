public with sharing class SchoolRenewalController implements SchoolAcademicYearSelectorInterface
{

    @testVisible
    private static final String SCHOOL_SUBSCRIPTION_INFORMATION = 'School Subscription Information';
    @testVisible
    private static final String FILE_LINK_URL = 'servlet/servlet.FileDownload?file=';

    public SchoolRenewalController thisController {get {return this;}}

    public Account currentSchool { get; set; }
    public Opportunity currentOpp { get; set;}
    public Academic_Year__c academicYear { get; private set;}
    public String academicYearString;

    public SchoolRenewalModel model { get; set; }

    //Interface methods
    @TestVisible private String academicYearId;
    public String getAcademicYearId(){
        return academicYearId;
    }
    public void setAcademicYearId(String academicYearId) {
        this.academicYearId = academicYearId;
    }
    public PageReference SchoolAcademicYearSelector_OnChange(Boolean saveRecord) {
        return null;
    }//End Interface methods

    public Boolean isShowSelectSchool {
        get {
            if (isShowSelectSchool == null) {
                if (GlobalVariables.getAllSchoolsForUser().size() > 1) {
                    isShowSelectSchool = true;
                } else {
                    isShowSelectSchool = false;
                }
            }

            return isShowSelectSchool;
        }
        set;
    }

    /**
     * @description If the current school has a closed won subscription fee opportunity
     *              for the current academic year but is missing a subscription then we
     *              want to have them call into the school to get this resolved before
     *              paying through the portal.
     */
    public Boolean IsMissingSubscription {
        get {
            return !CurrentUser.hasCurrentSubscription() &&
                    CurrentUser.hasSubscriptionOpportunityForCurrentAcademicYear();
        }
    }

    //Constructor
    public SchoolRenewalController() {
        Id schoolId = GlobalVariables.getCurrentSchoolId();
        currentSchool = getCurrentSchool(schoolId);

        if (IsMissingSubscription) {
            return;
        }

        if (ApexPages.currentPage().getParameters().containsKey('id')) {
            try {
                currentOpp = [
                    SELECT
                        Amount,
                        Subscription_Type__c,
                        Academic_Year_Picklist__c,
                        stageName,
                        RecordTypeId
                    FROM Opportunity
                    WHERE Id = :ApexPages.currentPage().getParameters().get('Id')
                ];
                model = new SchoolRenewalModel(currentSchool, currentOpp, false);

            }catch(Exception e) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an error retrieving the opportunity.'));
                return;
            }
        }
        if (currentOpp == null) {
            if (currentSchool.Paid_Through__c == null) {
                // all current schools should have at least 1 subscription record; but just in case
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There is no subscription record for this school. Please contact ' 
                + Label.Company_Name_Full + '.'));
                return;
            }
            academicYearString = currentSchool.Paid_Through__c.addYears(1).year() + '-' + currentSchool.Paid_Through__c.addYears(2).year();

            currentOpp = new Opportunity();
            Opportunity[] existingOpps = [
                SELECT
                    Amount,
                    Subscription_Type__c,
                    Academic_Year_Picklist__c,
                    stageName,
                    RecordTypeId
                FROM Opportunity
                WHERE AccountId = :currentSchool.Id
                AND RecordTypeId = :RecordTypes.opportunitySubscriptionFeeTypeId
                AND IsClosed = false
                AND Academic_Year_Picklist__c = :academicYearString
                ORDER BY CreatedDate DESC
                LIMIT 1
            ];

            if (existingOpps.size() > 0) {
                currentOpp = existingOpps[0];
            }
            model = new SchoolRenewalModel(currentSchool, currentOpp, true);
        }

    }//End Constructor

    public Account getCurrentSchool(Id schoolId) {
        try {
            return [
                SELECT
                    Name,
                    Paid_Through__c,
                    SSS_Subscription_Type_Current__c
                FROM Account
                WHERE Id = :schoolId
                LIMIT 1
            ];
        } catch (Exception e) {
            system.debug('Exception generated in SchoolRenewalController.getCurrentSchool() : No School found for Id '+schoolId);
        }
        return null;
    }
    public PageReference upsertOpportunity() {
        if (model.newSubscriptionType == null || model.subscriptionFee == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was a problem processing your request : subscription type and fee cannot be blank'));
            return null;
        }

        //Id currentYearId = GlobalVariables.getCurrentAcademicYear().Id;

        if (currentOpp.Id == null) {
            currentOpp.Name = currentSchool.Name + ' - ' + model.newSubscriptionType + ' Subscription ' + academicYearString;
            currentOpp.AccountId = currentSchool.Id;
            currentOpp.RecordTypeId = RecordTypes.opportunitySubscriptionFeeTypeId;
            currentOpp.Academic_Year_Picklist__c = academicYearString;
            currentOpp.New_Renewal__c = 'Renewal';
            currentOpp.StageName = 'Prospecting';
            currentOpp.CloseDate = System.today();
            currentOpp.SYSTEM_Suppress_Auto_Sale_TLI_Insert__c = true;
        }
        currentOpp.Amount = model.subscriptionFee;
        currentOpp.Subscription_Discount__c = model.discount;
        currentOpp.Subscription_Type__c = model.newSubscriptionType;
        currentOpp.Subscription_Period_Start_Year__c = String.valueOf(System.today().year());

        try {
            upsert currentOpp;

        } catch (DMLException dmle) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was a problem processing your request : '+dmle.getMessage()));
            //return new Opportunity();
            return null;
        }
        //return currentOpp;

        if (model.newSubscriptionType == 'Basic') {
            return null;
        }

        //PageReference schoolPayment = SchoolPayment.Page;
        PageReference schoolPayment = Page.SchoolPayment;
        schoolPayment.getParameters().put('Id',currentOpp.Id);
        return schoolPayment.setRedirect(true);
    }

    // NAIS-2373 Login behavior for expired subscriptions: use server side redirect to re-enable portal tabs after subscription renewal
    public PageReference renewSubscription() {
        Transaction_Line_Item__c tli = TransactionLineItemUtils.createSaleTLI(currentOpp,true);

        currentOpp.stageName = 'Closed Won';
        try {
            upsert currentOpp;
        } catch (DMLException dmle) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was a problem processing your request : '+ dmle.getMessage()));
            return null;
        }
        return updateThankYouPageURL();
    }

    public PageReference cancelRedirect (){
        model.newSubscriptionType = '';
        return Apexpages.currentpage().setredirect(true);
    }

    public PageReference generateReceipt (){
        if (currentOpp.Id != null) {
            Pagereference receiptPage = Page.SchoolReceipt;
            receiptPage.getParameters().put('Id',currentOpp.Id);
            return receiptPage;
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was a problem processing your request.'));
            return null;
        }
    }

    public PageReference updateThankYouPageURL () {
        PageReference pageRef = Page.SchoolRenewal;
        pageRef.getParameters().put('Id',currentOpp.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }

    //SFP-126.[G.S]
    public Boolean getIsEligibleForRenew() {
        if (currentSchool.Paid_Through__c == null) {
            return true;
        } else if (currentSchool.Paid_Through__c.year() < System.today().year()) {
            return true;
        } else if (currentSchool.Paid_Through__c.year() == System.today().year() && GlobalVariables.isRenewalSeason()) {
            return true;
        }
        return false;
    }

    public PageReference selectSchool() {
        PageReference pr = Page.SchoolPicker;
        pr.setRedirect(true);

        return pr;
    }

    public String subscriptionHelpInfoDocLink
    {
        get
        {
            if (subscriptionHelpInfoDocLink == null)
            {
                List<Document> subscriptionDocs = [
                    SELECT Id
                    FROM Document
                    WHERE Name = :SCHOOL_SUBSCRIPTION_INFORMATION];

                if (!subscriptionDocs.isEmpty())
                {
                    subscriptionHelpInfoDocLink = FILE_LINK_URL + subscriptionDocs[0].Id;
                }
            }

            return subscriptionHelpInfoDocLink;
        }
        private set;
    }
}