@isTest 
private class StudentFolderActionTest{

    private static Student_Folder__c studentFolder;

    private static Student_Folder__c selectFolderById(Id folderId) {
        return [SELECT Id, Faculty_Child__c, Student_ID__c FROM Student_Folder__c WHERE Id = :folderId LIMIT 1];
    }

    private static List<Academic_Year__c> insertAcademicYears() {
        List<Academic_Year__c> academicYears = new List<Academic_Year__c>();

        Date startDate = Date.newInstance(2017, 6, 1);

        academicYears.add(AcademicYearTestData.Instance.forName('2018-2019').forStartDate(startDate.addMonths(3)).create());
        academicYears.add(AcademicYearTestData.Instance.forName('2017-2018').forStartDate(startDate.addMonths(2)).create());
        academicYears.add(AcademicYearTestData.Instance.forName('2016-2017').forStartDate(startDate.addMonths(1)).create());
        academicYears.add(AcademicYearTestData.Instance.forName('2015-2016').forStartDate(startDate).create());

        insert academicYears;

        return academicYears;
    }

    private static void initTestData(String currentAcademicYear)
    {
        Contact student = ContactTestData.Instance.forRecordTypeId(RecordTypes.studentContactTypeId).DefaultContact;
        studentFolder = StudentFolderTestData.Instance
            .forStudentId(student.Id)
            .forAcademicYearPicklist(currentAcademicYear).DefaultStudentFolder;
        List<Academic_Year__c> academicYears = AcademicYearService.Instance.getAllAcademicYears();

        List<Student_Folder__c> folders = new List<Student_Folder__c>();
        Student_Folder__c tmpFolder;

        for(Academic_Year__c a : academicYears)
        {
            if(a.Name == currentAcademicYear){
                studentFolder.New_Returning__c = 'New';
                studentFolder.Student_ID__c = null;
                folders.add(studentFolder);
            }else{
                tmpFolder = TestUtils.createStudentFolder('Student Folder 1', a.Id, student.Id, false);
                tmpFolder.New_Returning__c = 'New';
                tmpFolder.Student_ID__c = 'tocopy_' + a.Name;
                folders.add(tmpFolder);
            }
        }
        
        if(!folders.isEmpty()) {
            upsert folders;
        } 
    }

    @isTest
    private static void copyDataFromPreviousYear_studentWithPreviousYearFolder_studentIdPopulatedOnPreviousYear_expectNewFolderHasId() {
        insertAcademicYears();

        String previousAcademicYear = '2017-2018';
        String nextAcademicYear = '2018-2019';

        String newOrReturning = 'Returning';

        String expectedStudentId = 'testStudentId';
        String expectedFacultyChild = 'Yes';

        Account school = AccountTestData.Instance.asSchool().insertAccount();

        Student_Folder__c previousFolder = StudentFolderTestData.Instance
                .forAcademicYearPicklist(previousAcademicYear)
                .withStudentIdString(expectedStudentId)
                .withFacultyChild(expectedFacultyChild)
                .forSchoolId(school.Id)
                .insertStudentFolder();

        Student_Folder__c nextFolder = StudentFolderTestData.Instance
                .forAcademicYearPicklist(nextAcademicYear)
                .forSchoolId(school.Id)
                .forNewReturning(newOrReturning)
                .insertStudentFolder();

        nextFolder = selectFolderById(nextFolder.Id);

        System.assertEquals(expectedStudentId, nextFolder.Student_ID__c,
                'Expected the student Id to be carried over from the previous year.');
        System.assertEquals(expectedFacultyChild, nextFolder.Faculty_Child__c,
                'Expected the Faculty Child value to be carried over from the previous year.');
    }

    @isTest
    private static void copyDataFromPreviousYear_studentWithPreviousYearFolder_studentIdNullOnPreviousYear_expectNewFolderHasFacultyChildValue() {
        insertAcademicYears();

        String previousAcademicYear = '2017-2018';
        String nextAcademicYear = '2018-2019';

        String newOrReturning = 'Returning';

        String expectedStudentId = null;
        String expectedFacultyChild = 'Yes';

        Account school = AccountTestData.Instance.asSchool().insertAccount();

        Student_Folder__c previousFolder = StudentFolderTestData.Instance
                .forAcademicYearPicklist(previousAcademicYear)
                .withStudentIdString(expectedStudentId)
                .withFacultyChild(expectedFacultyChild)
                .forSchoolId(school.Id)
                .insertStudentFolder();

        Student_Folder__c nextFolder = StudentFolderTestData.Instance
                .forAcademicYearPicklist(nextAcademicYear)
                .forSchoolId(school.Id)
                .forNewReturning(newOrReturning)
                .insertStudentFolder();

        nextFolder = selectFolderById(nextFolder.Id);

        System.assertEquals(expectedStudentId, nextFolder.Student_ID__c,
                'Expected the student Id to be carried over from the previous year.');
        System.assertEquals(expectedFacultyChild, nextFolder.Faculty_Child__c,
                'Expected the Faculty Child value to be carried over from the previous year.');
    }

    @isTest
    private static void copyDataFromPreviousYear_studentWithPreviousYearFoldersForMultipleSchools_studentIdPopulatedOnPreviousYear_expectNewFolderHasId() {
        insertAcademicYears();

        String previousAcademicYear = '2017-2018';
        String nextAcademicYear = '2018-2019';

        String newOrReturning = 'Returning';

        String expectedStudentId = 'testStudentId';
        String expectedFacultyChild = 'Yes';
        String otherSchoolStudentId = 'unexpectedStudentId';
        String otherSchoolFacultyChild = 'No';

        Account school = AccountTestData.Instance.asSchool().insertAccount();
        Account otherSchool = AccountTestData.Instance.asSchool().insertAccount();

        Student_Folder__c previousFolder = StudentFolderTestData.Instance
                .forAcademicYearPicklist(previousAcademicYear)
                .withStudentIdString(expectedStudentId)
                .withFacultyChild(expectedFacultyChild)
                .forSchoolId(school.Id)
                .insertStudentFolder();

        Student_Folder__c previousFolderForOtherSchool = StudentFolderTestData.Instance
                .forAcademicYearPicklist(previousAcademicYear)
                .withStudentIdString(otherSchoolStudentId)
                .withFacultyChild(otherSchoolFacultyChild)
                .forSchoolId(otherSchool.Id)
                .insertStudentFolder();

        Student_Folder__c nextFolder = StudentFolderTestData.Instance
                .forAcademicYearPicklist(nextAcademicYear)
                .forSchoolId(school.Id)
                .forNewReturning(newOrReturning)
                .insertStudentFolder();

        nextFolder = selectFolderById(nextFolder.Id);

        System.assertEquals(expectedStudentId, nextFolder.Student_ID__c,
                'Expected the student Id to be carried over from the previous year.');
        System.assertEquals(expectedFacultyChild, nextFolder.Faculty_Child__c,
                'Expected the Faculty Child value to be carried over from the previous year.');
    }

    @isTest
    private static void copyDataFromPreviousYear_studentWith2PreviousFolders_studentIdPopulatedOnFirstFolderOnly_expectNewFolderHasId() {
        insertAcademicYears();

        String firstAcademicYear = '2015-2016';
        String previousAcademicYear = '2017-2018';
        String nextAcademicYear = '2018-2019';

        String newOrReturning = 'Returning';

        String expectedStudentId = 'testStudentId';
        String expectedFacultyChild = 'Yes';

        Account school = AccountTestData.Instance.asSchool().insertAccount();

        Student_Folder__c firstFolder = StudentFolderTestData.Instance
                .forAcademicYearPicklist(firstAcademicYear)
                .withStudentIdString(expectedStudentId)
                .withFacultyChild(expectedFacultyChild)
                .forSchoolId(school.Id)
                .insertStudentFolder();

        Student_Folder__c previousFolder = StudentFolderTestData.Instance
                .forAcademicYearPicklist(previousAcademicYear)
                .forSchoolId(school.Id)
                .forNewReturning(newOrReturning)
                .insertStudentFolder();

        previousFolder = selectFolderById(previousFolder.Id);
        System.assertEquals(expectedStudentId, previousFolder.Student_ID__c,
                'Expected the student Id to be carried over from the first year.');
        System.assertEquals(expectedFacultyChild, previousFolder.Faculty_Child__c,
                'Expected the Faculty Child value to be carried over from the first year.');

        // Now we will update the previous folder so that the student Id is null to see if the
        // next folder will inherit the value from the first folder.
        previousFolder.Student_ID__c = null;
        previousFolder.Faculty_Child__c = null;
        update previousFolder;

        previousFolder = selectFolderById(previousFolder.Id);
        System.assertEquals(null, previousFolder.Student_ID__c,
                'Expected the student Id to stay null after update.');
        System.assertEquals(null, previousFolder.Faculty_Child__c,
                'Expected the faculty child value to stay null after update.');

        Student_Folder__c nextFolder = StudentFolderTestData.Instance
                .forAcademicYearPicklist(nextAcademicYear)
                .forSchoolId(school.Id)
                .forNewReturning(newOrReturning)
                .insertStudentFolder();

        nextFolder = selectFolderById(nextFolder.Id);

        System.assertEquals(expectedStudentId, nextFolder.Student_ID__c,
                'Expected the student Id to be carried over from the previous year.');
        System.assertEquals(expectedFacultyChild, nextFolder.Faculty_Child__c,
                'Expected the Faculty Child value to be carried over from the previous year.');
    }

    @isTest
    private static void copyDataFromPreviousYear_studentWith2PreviousFolders_studentIdPopulatedOnFirstFolderOnly_facultyChildYesOnPreviousFolder_expectNewFolderHasIdAndYes() {
        insertAcademicYears();

        String firstAcademicYear = '2015-2016';
        String previousAcademicYear = '2017-2018';
        String nextAcademicYear = '2018-2019';

        String newOrReturning = 'Returning';

        String expectedStudentId = 'testStudentId';
        String firstFacultyChildValue = 'No';
        String expectedFacultyChild = 'Yes';

        Account school = AccountTestData.Instance.asSchool().insertAccount();

        Student_Folder__c firstFolder = StudentFolderTestData.Instance
                .forAcademicYearPicklist(firstAcademicYear)
                .withStudentIdString(expectedStudentId)
                .withFacultyChild(firstFacultyChildValue)
                .forSchoolId(school.Id)
                .insertStudentFolder();

        Student_Folder__c previousFolder = StudentFolderTestData.Instance
                .forAcademicYearPicklist(previousAcademicYear)
                .forSchoolId(school.Id)
                .forNewReturning(newOrReturning)
                .insertStudentFolder();

        previousFolder = selectFolderById(previousFolder.Id);
        System.assertEquals(expectedStudentId, previousFolder.Student_ID__c,
                'Expected the student Id to be carried over from the first year.');
        System.assertEquals(firstFacultyChildValue, previousFolder.Faculty_Child__c,
                'Expected the Faculty Child value to be carried over from the first year.');

        // Now we will update the previous folder so that the student Id is null to see if the
        // next folder will inherit the value from the first folder.
        previousFolder.Student_ID__c = null;
        // Change the faculty child to Yes so we can make sure the new folder can inherit student Id and faculty child
        // from different academic years.
        previousFolder.Faculty_Child__c = expectedFacultyChild;
        update previousFolder;

        previousFolder = selectFolderById(previousFolder.Id);
        System.assertEquals(null, previousFolder.Student_ID__c,
                'Expected the student Id to stay null after update.');
        System.assertEquals(expectedFacultyChild, previousFolder.Faculty_Child__c,
                'Expected the faculty child value to stay null after update.');

        Student_Folder__c nextFolder = StudentFolderTestData.Instance
                .forAcademicYearPicklist(nextAcademicYear)
                .forSchoolId(school.Id)
                .forNewReturning(newOrReturning)
                .insertStudentFolder();

        nextFolder = selectFolderById(nextFolder.Id);

        System.assertEquals(expectedStudentId, nextFolder.Student_ID__c,
                'Expected the student Id to be carried over from the previous year.');
        System.assertEquals(expectedFacultyChild, nextFolder.Faculty_Child__c,
                'Expected the Faculty Child value to be carried over from the previous year.');
    }

    @isTest
    private static void copyDataFromPreviousYear_studentWith2PreviousFolders_studentIdOnFirstFolderDifferent_facultyChildYesOnPreviousFolder_expectNewFolderHasPreviousStudentIdAndYes() {
        insertAcademicYears();

        String firstAcademicYear = '2015-2016';
        String previousAcademicYear = '2017-2018';
        String nextAcademicYear = '2018-2019';

        String newOrReturning = 'Returning';

        String firstStudentId = 'firstStudentIdValue';
        String expectedStudentId = 'testStudentId';
        String firstFacultyChildValue = 'No';
        String expectedFacultyChild = 'Yes';

        Account school = AccountTestData.Instance.asSchool().insertAccount();

        Student_Folder__c firstFolder = StudentFolderTestData.Instance
                .forAcademicYearPicklist(firstAcademicYear)
                .withStudentIdString(firstStudentId)
                .withFacultyChild(firstFacultyChildValue)
                .forSchoolId(school.Id)
                .insertStudentFolder();

        Student_Folder__c previousFolder = StudentFolderTestData.Instance
                .forAcademicYearPicklist(previousAcademicYear)
                .forSchoolId(school.Id)
                .forNewReturning(newOrReturning)
                .insertStudentFolder();

        previousFolder = selectFolderById(previousFolder.Id);
        System.assertEquals(firstStudentId, previousFolder.Student_ID__c,
                'Expected the student Id to be carried over from the first year.');
        System.assertEquals(firstFacultyChildValue, previousFolder.Faculty_Child__c,
                'Expected the Faculty Child value to be carried over from the first year.');

        // Now we will update the previous folder so that the student Id is different than the Id on the first folder.
        // This is the value that we expect on the newest folder.
        previousFolder.Student_ID__c = expectedStudentId;
        // Change the faculty child to Yes so we can make sure the new folder can inherit student Id and faculty child
        // from different academic years.
        previousFolder.Faculty_Child__c = expectedFacultyChild;
        update previousFolder;

        previousFolder = selectFolderById(previousFolder.Id);
        System.assertEquals(expectedStudentId, previousFolder.Student_ID__c,
                'Expected the student Id to remain the same after update.');
        System.assertEquals(expectedFacultyChild, previousFolder.Faculty_Child__c,
                'Expected the faculty child value to remain the same after update.');

        Student_Folder__c nextFolder = StudentFolderTestData.Instance
                .forAcademicYearPicklist(nextAcademicYear)
                .forSchoolId(school.Id)
                .forNewReturning(newOrReturning)
                .insertStudentFolder();

        nextFolder = selectFolderById(nextFolder.Id);

        System.assertEquals(expectedStudentId, nextFolder.Student_ID__c,
                'Expected the student Id to be carried over from the previous year.');
        System.assertEquals(expectedFacultyChild, nextFolder.Faculty_Child__c,
                'Expected the Faculty Child value to be carried over from the previous year.');
    }

    @isTest 
    private static void checkStudentIdForReturningApplicants() {
        insertAcademicYears();

        String academicYearName = '2018-2019';

        initTestData(academicYearName);
        
        Test.startTest();
            studentFolder.New_Returning__c = 'Returning';
            update studentFolder;        
        Test.stopTest();
        
        String previousAcademicYear = GlobalVariables.getPreviousAcademicYearStr(studentFolder.Academic_Year_Picklist__c);
        
        System.assertEquals('tocopy_'+previousAcademicYear, StudentFolderSelector.newInstance()
            .selectByIdWithCustomFieldList(new Set<Id>{studentFolder.Id}, new List<String>{'Student_ID__c'})[0].Student_ID__c);
    }

    private static List<Academic_Year__c> filterFutureYears(List<Academic_Year__c> records) {
        List<Academic_Year__c> years = new List<Academic_Year__c>();

        for (Academic_Year__c record : records) {
            if (record.Start_Date__c <= System.today()) {
                years.add(record);
            }
        }

        return years;
    }
}