@isTest
private class FamilyPortalTopBarControllerTest
{

    @isTest
    private static void getAcademicYears_outsideOverlapPeriod_earlyAccessClosed_userWithoutEarlyAccess_expectCurrentAcademicYearOnly()
    {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        // Insert academic years that do not overlap.
        Integer numberOfYears = 5;
        AcademicYearTestData.Instance.insertAcademicYears(numberOfYears);

        Boolean shouldHaveEarlyAccess = false;
        User familyUser = insertFamilyPortalUser(shouldHaveEarlyAccess);

        Integer expectedNumberOfYears = 1;

        List<Academic_Year__c> academicYears;
        System.runAs(familyUser) {
            FamilyPortalTopBarController controller = new FamilyPortalTopBarController();
            academicYears = controller.getAcademicYears();
        }

        assertExpectedNumberOfAcademicYears(academicYears, expectedNumberOfYears);
    }

    @isTest
    private static void getAcademicYears_outsideOverlapPeriod_earlyAccessOpen_userWithoutEarlyAccess_expectCurrentAcademicYearOnly()
    {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessOpened().DefaultFamilyPortalSettings;

        // Insert academic years that do not overlap.
        Integer numberOfYears = 5;
        AcademicYearTestData.Instance.insertAcademicYears(numberOfYears);

        Boolean shouldHaveEarlyAccess = false;
        User familyUser = insertFamilyPortalUser(shouldHaveEarlyAccess);

        Integer expectedNumberOfYears = 1;

        List<Academic_Year__c> academicYears;
        System.runAs(familyUser) {
            FamilyPortalTopBarController controller = new FamilyPortalTopBarController();
            academicYears = controller.getAcademicYears();
        }

        assertExpectedNumberOfAcademicYears(academicYears, expectedNumberOfYears);
    }

    @isTest
    private static void getAcademicYears_outsideOverlapPeriod_earlyAccessOpen_userWithEarlyAccess_expectCurrentAndPreviousAcademicYears()
    {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessOpened().DefaultFamilyPortalSettings;

        // Insert academic years that do not overlap.
        Integer numberOfYears = 5;
        AcademicYearTestData.Instance.insertAcademicYears(numberOfYears);

        Boolean shouldHaveEarlyAccess = true;
        User familyUser = insertFamilyPortalUser(shouldHaveEarlyAccess);

        Integer expectedNumberOfYears = 2;

        List<Academic_Year__c> academicYears;
        System.runAs(familyUser) {
            FamilyPortalTopBarController controller = new FamilyPortalTopBarController();
            academicYears = controller.getAcademicYears();
        }

        assertExpectedNumberOfAcademicYears(academicYears, expectedNumberOfYears);
    }

    @isTest
    private static void getAcademicYears_withinOverlapPeriod_userWithoutEarlyAccess_expectCurrentAndPreviousAcademicYears()
    {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessOpened().DefaultFamilyPortalSettings;

        // Insert overlapping academic years.
        AcademicYearTestData.Instance.insertOverlappingYears();

        Boolean shouldHaveEarlyAccess = false;
        User familyUser = insertFamilyPortalUser(shouldHaveEarlyAccess);

        Integer expectedNumberOfYears = 2;

        List<Academic_Year__c> academicYears;
        System.runAs(familyUser) {
            FamilyPortalTopBarController controller = new FamilyPortalTopBarController();
            academicYears = controller.getAcademicYears();
        }

        assertExpectedNumberOfAcademicYears(academicYears, expectedNumberOfYears);
    }

    @isTest
    private static void getAcademicYears_withinOverlapPeriod_userWithEarlyAccess_expectOverlappingAcademicYearsAndNextAcademicYear()
    {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessOpened().DefaultFamilyPortalSettings;

        // Insert 2 current overlapping academic years and one in the future.
        // We expect the early access user to get access to the future year
        // which has not officially been opened to all families yet.
        AcademicYearTestData.Instance.insertOverlappingYears();

        Boolean shouldHaveEarlyAccess = true;
        User familyUser = insertFamilyPortalUser(shouldHaveEarlyAccess);

        Integer expectedNumberOfYears = 3;

        List<Academic_Year__c> academicYears;
        System.runAs(familyUser) {
            FamilyPortalTopBarController controller = new FamilyPortalTopBarController();
            academicYears = controller.getAcademicYears();
        }

        assertExpectedNumberOfAcademicYears(academicYears, expectedNumberOfYears);
    }

    @isTest
    private static void testPfsSubmittedAndPaid()
    {   
        Pfs__c pfs1 = PfsTestData.Instance.create();
        Pfs__c pfs2 = PfsTestData.Instance.asSubmitted().create();
        Pfs__c pfs3 = PfsTestData.Instance.asPaid().create();
        Pfs__c pfs4 = PfsTestData.Instance.asSubmitted().asPaid().create();
        insert new List<Pfs__c>{pfs1, pfs2, pfs3, pfs4};

        Test.startTest();
            FamilyPortalTopBarController controller = new FamilyPortalTopBarController();
            System.assert(!controller.pfsSubmittedAndPaid, 'currentPFSRecord is null -> false');

            controller.currentPFSRecord = pfs1;
            System.assert(!controller.pfsSubmittedAndPaid);

            controller.currentPFSRecord = pfs2;
            System.assert(!controller.pfsSubmittedAndPaid);

            controller.currentPFSRecord = pfs3;
            System.assert(!controller.pfsSubmittedAndPaid);

            controller.currentPFSRecord = pfs4;
            System.assert(controller.pfsSubmittedAndPaid);
        Test.stopTest();
    }

    @isTest
    private static void testDocumentPageUrl()
    { 
        Academic_Year__c academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        Pfs__c pfs = PfsTestData.Instance.DefaultPfs;

        FamilyPortalTopBarController controller = new FamilyPortalTopBarController();
        controller.currentPFSId = pfs.Id;
        controller.academicYearId = academicYear.Id;

        Test.startTest();
            String selectedTab = controller.selectedTab;
            System.assert(String.isBlank(selectedTab));
            String documentPageUrl = controller.documentPageUrl;
            System.assert(documentPageUrl.containsIgnoreCase('/apex/FamilyDocuments?')
                && documentPageUrl.containsIgnoreCase('academicyearid=' + academicYear.Id) 
                && documentPageUrl.containsIgnoreCase('id=' + pfs.Id));
        Test.stopTest();
    }

    private static final String INTERFACE_TEST_VALUE = 'interface test value'; 

    @isTest
    private static void testConsumer()
    {
        Test.startTest();
            FamilyPortalTopBarController controller = new FamilyPortalTopBarController();
            System.assertEquals(null, controller.consumer);

            controller.Consumer = new ConsumerTest();
            System.assertEquals(INTERFACE_TEST_VALUE, controller.Consumer.getAcademicYearId());
        Test.stopTest();
    }

    @isTest
    private static void testAcademicYearOptions()
    {
        User familyUser = UserTestData.insertFamilyPortalUser();
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
        Academic_Year__c overlappingAcademicYear1 = AcademicYearTestData.Instance.create();
        Academic_Year__c overlappingAcademicYear2 = AcademicYearTestData.Instance.create();
        List<Academic_Year__c> academicYears = 
            new List<Academic_Year__c>{currentAcademicYear, overlappingAcademicYear1, overlappingAcademicYear2};

        System.runAs(familyUser)
        {
            FamilyPortalTopBarController controller = new FamilyPortalTopBarController();

            Test.startTest();
                System.assertEquals(1, controller.academicYearOptions.size());
                System.assert(!controller.inOverlapPeriodOrMultipleYears);

                insert academicYears;
                AcademicYearService.Instance.allAcademicYears = academicYears;
                controller = new FamilyPortalTopBarController();
                System.assertEquals(3, controller.academicYearOptions.size());
                System.assert(controller.inOverlapPeriodOrMultipleYears);
            Test.stopTest();
        }
    }

    @isTest
    private static void testGetcanCreatePFS()
    {
        User familyUser = UserTestData.insertFamilyPortalUser();
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
        Academic_Year__c overlappingAcademicYear1 = AcademicYearTestData.Instance.create();
        Academic_Year__c overlappingAcademicYear2 = AcademicYearTestData.Instance.create();
        List<Academic_Year__c> academicYears = 
            new List<Academic_Year__c>{currentAcademicYear, overlappingAcademicYear1, overlappingAcademicYear2};

        System.runAs(familyUser)
        {
            FamilyPortalTopBarController controller = new FamilyPortalTopBarController();

            Test.startTest();
                System.assert(!controller.getcanCreatePFS());
                
                insert academicYears;
                AcademicYearService.Instance.allAcademicYears = academicYears;
                controller = new FamilyPortalTopBarController();
                System.assert(controller.getcanCreatePFS());
            Test.stopTest();
        }
    }

    @isTest
    private static void testGetIsFamilyPortalUser()
    {
        Contact parent = ContactTestData.Instance.asParent().DefaultContact;
        User familyUser = UserTestData.Instance
            .forProfileId(GlobalVariables.familyPortalProfileId)
            .forContactId(parent.Id).create();
        User systemAdmin = UserTestData.Instance
            .forUsername(UserTestData.generateTestEmail())
            .forProfileId(GlobalVariables.sysAdminProfileId).create();
        Insert new List<User>{familyUser, systemAdmin};

        FamilyPortalTopBarController controller;

        Test.startTest();
            System.runAs(familyUser)
            {
                controller = new FamilyPortalTopBarController();
                System.assert(controller.getIsFamilyPortalUser());
            }

            System.runAs(systemAdmin)
            {
                controller = new FamilyPortalTopBarController();
                System.assert(!controller.getIsFamilyPortalUser());
            }
        Test.stopTest();
    }

    @isTest
    private static void testControllerAppUtilsFunctions()
    {
        Academic_Year__c academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        Pfs__c pfs = PfsTestData.Instance
            .forAcademicYearPicklist(academicYear.Name).DefaultPfs;

        pfs = PfsSelector.newInstance().selectWithCustomFieldListById(
            new Set<Id>{pfs.Id}, new List<String>{'Academic_Year_Picklist__c', 'PFS_Number__c', 'Name', 'PFS_Status__c'})[0];
    
        Test.startTest();
            FamilyPortalTopBarController controller = new FamilyPortalTopBarController();
            controller.currentPFSRecord = pfs;

            System.assertEquals(
                'PFS ' + pfs.Academic_Year_Picklist__c + '  |  PFSID#: ' + pfs.PFS_Number__c, controller.getPFSName());
            System.assertEquals(ApplicationUtils.DEFAULT_PFS_STATUS, controller.getPFSStatus());
            System.assertEquals(ApplicationUtils.DEFAULT_PFS_STATUS, controller.getPFSStatusCSS());
        Test.stopTest();
    }

    @isTest
    private static void testAcademicYearSelectorOnChange()
    {
        Academic_Year__c academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        Pfs__c pfs = PfsTestData.Instance.forAcademicYearPicklist(academicYear.Name).DefaultPfs;

        FamilyPortalTopBarController controller = new FamilyPortalTopBarController();
        controller.currentPFSId = pfs.Id;
        controller.currentPFSRecord = pfs;
        controller.academicYearId = academicYear.Id;
        controller.newAcademicYearId = academicYear.Id;
        controller.Consumer = new ConsumerTest();

        Test.startTest();
            String pageRefUrl = controller.AcademicYearSelectorOnChange().getUrl();
            System.assert(pageRefUrl.containsIgnoreCase('/apex/FamilyDashboard') 
                && pageRefUrl.containsIgnoreCase('id='+pfs.Id) 
                && pageRefUrl.containsIgnoreCase('academicyearid='+academicYear.Id));

            Academic_Year__c nextAcademicYear = AcademicYearTestData.Instance.asNextYear().insertAcademicYear();
            controller.newAcademicYearId = nextAcademicYear.Id;
            pageRefUrl = controller.AcademicYearSelectorOnChange().getUrl();
            System.assert(pageRefUrl.containsIgnoreCase('/apex/FamilyDashboard') 
                && !pageRefUrl.containsIgnoreCase('id='+pfs.Id)
                && pageRefUrl.containsIgnoreCase('academicyearid='+nextAcademicYear.Id));
        Test.stopTest();
    }

    private class ConsumerTest implements FamilyPortalTopBarInterface
    {
        private ConsumerTest(){}

        public String getAcademicYearId()
        {
            return INTERFACE_TEST_VALUE;
        }

        public PageReference YearSelector_OnChange(Id newAcademicYearId)
        {
            return null;
        }
    }

    private static void assertExpectedNumberOfAcademicYears(
        List<Academic_Year__c> yearsToAssert, Integer expectedNumberOfYears)
    {
        System.assertNotEquals(null, yearsToAssert, 'Expected the academic years to not be null.');
        System.assert(!yearsToAssert.isEmpty(), 'Expected the academic years to not be empty.');
        System.assertEquals(expectedNumberOfYears, yearsToAssert.size(),
                'Expected the correct number of academic years. Academic_Year__c records: ' + yearsToAssert);

        Set<String> academicYearNames = new Set<String>();
        for (Academic_Year__c year : yearsToAssert) {
            academicYearNames.add(year.Name);
        }

        System.assert(expectedNumberOfYears == academicYearNames.size(),
                'Expected the academic years to be unique: ' + academicYearNames);
    }

    private static User insertFamilyPortalUser(Boolean shouldHaveEarlyAccess)
    {
        User familyPortalUser = UserTestData.createFamilyPortalUser();

        familyPortalUser.Early_Access__c = shouldHaveEarlyAccess;

        insert familyPortalUser;

        return familyPortalUser;
    }
}