public class SchoolPFSAction {

    public Map <Id, School_PFS_Assignment__c> oldSpfsaMap = new Map <Id, School_PFS_Assignment__c>();
    public Map <Id, School_PFS_Assignment__c> newSpfsaMap = new Map <Id, School_PFS_Assignment__c>();

    private Set<ID> schoolPFSIDs = new Set<ID>();
    private Set<ID> folderIDS = new Set<ID>();
    
    public SchoolPFSAction(Map<Id, School_PFS_Assignment__c> triggerNew, Map <Id, School_PFS_Assignment__c> triggerOld) {
        oldSpfsaMap = triggerOld;
        newSpfsaMap = triggerNew;
    }
    
    public SchoolPFSAction(Map<Id, School_PFS_Assignment__c> triggerNew) {
        
        newSpfsaMap = triggerNew;
    }
    public SchoolPFSAction(){} // [DP] 08.14.2015 NAIS-2528
    
    public Boolean buildIDMapFromInsert() {
        folderIDS =  new Set<ID>();
        schoolPFSIDs = new Set<ID>();
        Boolean modified= false;

        //are we assuming schoolpfsassignments are not allowed to change folders?
        for (Id pfsID  : newSpfsaMap.keyset()) {
            School_PFS_Assignment__c newpfs = newSpfsaMap.get(pfsID);
        
        
        
            if (
                newpfs.Application_Last_Modified_by_Family__c != null ||
                
                newpfs.Age__c !=null ||
                newpfs.Birthdate__c != null ||
                newpfs.Grade_Applying__c != null || 
                
                newpfs.Kamehameha_ID__C != null || 
                newpfs.Parent_A__c != null || 
                newpfs.Parent_B__c != null ||
                newPfs.School__r.SSS_School_Code__c != null || //NAIS-2103
                newpfs.PFS_Received__c != null || 
                newpfs.LastModifiedBy != null || 
                
                newpfs.Total_Salary_and_Wages__c != null || 
                (newpfs.Total_Salary_and_Wages__c ==null && (newpfs.Orig_Total_Salary_and_Wages__c != null)) ||  // oldPfs.Business_Farm_Owner__c ==null too if we've gotten to this point
                
                //NAIS-2103 START
                newpfs.Effective_Income_Currency__c != null || 
                (newpfs.Effective_Income_Currency__c ==null && (newpfs.Orig_Effective_Income_Currency__c != null)) ||
                
                newpfs.Income_Supplement__c != null || 
                (newpfs.Income_Supplement__c ==null && (newpfs.Orig_Income_Supplement__c != null)) ||
                
                newpfs.Update_Student_Folder__c == true
                )
            {
                    modified = true;
                    folderIDS.add(newpfs.Student_Folder__c);
                    schoolPFSIDs.add(pfsID);
            }
            
            
        }
        return modified;
    }
    public Boolean buildIDMapFromUpdate() {
        folderIDS =  new Set<ID>();
        schoolPFSIDs = new Set<ID>();
        Boolean modified= false;

        //are we assuming schoolpfsassignments are not allowed to change folders?
        for (Id pfsID  : oldSpfsaMap.keyset()) {
            School_PFS_Assignment__c oldPfs= oldSpfsaMap.get(pfsID);
            School_PFS_Assignment__c newpfs = newSpfsaMap.get(pfsID);

            if (
            
                oldPfs.Application_Last_Modified_by_Family__c != newpfs.Application_Last_Modified_by_Family__c ||
        
                oldPfs.Kamehameha_ID__c != newpfs.Kamehameha_ID__c ||
                oldPfs.Age__c != newpfs.Age__c ||
                oldPfs.Birthdate__c != newpfs.Birthdate__c ||
                oldPfs.Grade_Applying__c != newpfs.Grade_Applying__c || 
                oldPfs.Parent_A__c != newpfs.Parent_A__c || 
                oldPfs.Parent_B__c != newpfs.Parent_B__c ||
                oldPfs.PFS_Received__c != newpfs.PFS_Received__c || 
                oldPfs.LastModifiedBy != newpfs.LastModifiedBy || 
                oldPfs.Total_Salary_and_Wages__c != newpfs.Total_Salary_and_Wages__c || 
                (newpfs.Total_Salary_and_Wages__c ==null && (oldPfs.Orig_Total_Salary_and_Wages__c != newpfs.Orig_Total_Salary_and_Wages__c)) ||  // oldPfs.Business_Farm_Owner__c ==null too if we've gotten to this point
                
                //NAIS-2103 START
                oldPfs.Effective_Income_Currency__c != newpfs.Effective_Income_Currency__c || 
                (newpfs.Effective_Income_Currency__c ==null && (oldPfs.Orig_Effective_Income_Currency__c != newpfs.Orig_Effective_Income_Currency__c)) ||  
                
                oldPfs.Income_Supplement__c != newpfs.Income_Supplement__c || 
                (newpfs.Income_Supplement__c ==null && (oldPfs.Orig_Income_Supplement__c != newpfs.Orig_Income_Supplement__c)) || 
                
                (oldPfs.Update_Student_Folder__c == false && newPfs.Update_Student_Folder__c == true)
                )
            {
                modified = true;
                folderIDS.add(newpfs.Student_Folder__c);
                schoolPFSIDs.add(pfsID);
            }
            
            
        }
        return modified;
    }
    
    public void clearFolderRollupsAndRemoveEmptyStudentFolders(Set<Id> studentFolderIds) {
        ArgumentNullException.throwIfNull(studentFolderIds, 'studentFolderIds');
        
        Set<Id> folderIdsToClear = new Set<Id>();
        Set<Id> spaIdsToClear = new Set<Id>();
        List<Student_Folder__c> studentFoldersToDelete = new List<Student_Folder__c>();
        
        for (Student_Folder__c folder : [select Id, (select Id from School_PFS_Assignments__r) from Student_Folder__c where Id in :studentFolderIds]) {
            
            if (folder.School_PFS_Assignments__r == null || folder.School_PFS_Assignments__r.size() == 0) {
                
                studentFoldersToDelete.add(folder);
            } else {
                
                for(School_PFS_Assignment__c spa : folder.School_PFS_Assignments__r) {
                    spaIdsToClear.add(spa.Id);
                }
                folderIdsToClear.add(folder.Id);
            }
        }
        
        //Remove empty student folders
        if (studentFoldersToDelete.size() > 0) {
            delete studentFoldersToDelete;
        }
        
        //Clear folder rollups
        if (!folderIdsToClear.isEmpty()) {
            clearFolderRollupsForDeletedPfsAssignments(folderIdsToClear, spaIdsToClear);
        }
    }
    
    public void clearFolderRollupsForDeletedPfsAssignments(Set<Id> folderIds, Set<Id> spaIdsToClear) {
        ArgumentNullException.throwIfNull(folderIds, 'folderIds');
        ArgumentNullException.throwIfNull(spaIdsToClear, 'spaIdsToClear');
        
        List<School_PFS_Assignment__c> allSPAs = getAllrelevantPFSassignments(folderIds);
        Map<ID,Student_Folder__c> mapFolders = getMapFoldersById(folderIds);
        Map<Id, Student_Folder__c> foldersToUpdate = new Map<Id, Student_Folder__c>();
        Student_Folder__c tmpFolder;
        Id folderId;
        
        for (School_PFS_Assignment__c spa : allSPAs ) {
            
            folderId = spa.Student_Folder__c;
            tmpFolder = foldersToUpdate.containsKey(folderId) ? foldersToUpdate.get(folderId) : mapFolders.get(folderId);
            
            if( spaIdsToClear.contains(spa.Id) ) {
                
                //Clear PFS2 Info and make sure PFS1 rollups are populated with the remain SPA's info.
                setPFS2Info(tmpFolder, new School_PFS_Assignment__c());
                setPFS1Info(tmpFolder, spa);
            }
            
            foldersToUpdate.put(folderId, tmpFolder);
        }
        
        if( !foldersToUpdate.isEmpty() ) {
            update foldersToUpdate.Values();
        }
    }
    
    
    private List<School_PFS_Assignment__c> getAllrelevantPFSassignments(Set<Id> tmpFolderIDS) {
        return new List<School_PFS_Assignment__c> ([SELECT 
            Application_Last_Modified_by_Family__c,
            name,
            Kamehameha_ID__c,
            Age__c, 
            Birthdate__c,
            Gender__c, 
            Grade_Applying__c,
            Parent_A__c,
            Parent_B__c,
            Student_Folder__r.name,
            Student_Folder__c,
            LastModifiedDate,
            Business_Farm_Owner__c,
            Orig_Business_Farm_Owner__c,
            Total_Salary_and_Wages__c,
            Orig_Total_Salary_and_Wages__c,
            Effective_Income_Currency__c,
            Orig_Effective_Income_Currency__c,
            Income_Supplement__c,
            Orig_Income_Supplement__c,
            Business_Farm_Share__c,
            School__r.SSS_School_Code__c,
            PFS_number_on_Student_Folder__c,
            Num_Children_in_Tuition_Schools__c, 
            PFS_Received__c,
                (Select Id, 
                Business_Farm_Share__c, 
                Business_Farm_Owner__c, 
                Business_Farm_Assets__c,
                Net_Profit_Loss_Business_Farm__c,
                Orig_Business_Farm_Share__c,
                Orig_Business_Farm_Owner__c,
                Orig_Business_Farm_Assets__c,
                Orig_Net_Profit_Loss_Business_Farm__c
                FROM School_Biz_Farm_Assignments__r)
            from School_PFS_Assignment__c 
            where Student_Folder__c in :tmpFolderIDS 
            order by Student_Folder__c, createdDate asc, Applicant__r.PFS__r.PFS_Number__c asc]);
    }
    
    private Map<ID,Student_Folder__c> getMapFoldersById(Set<Id> tmpFolderIDS) {
        return new Map<ID,Student_Folder__c> ([SELECT 
            s.Grade_Applying__c,
            s.PFS2_Total_Salary_and_Wages__c, 
            s.PFS2_Parent_B__c, 
            s.PFS2_Parent_A__c, 
            s.PFS2_Original_Submission_Date__c, 
            s.PFS2_Business_Farm_Owner__c, 
            s.PFS2_Business_Farm_Assets__c, 
            s.PFS2_Application_Last_Modified_by_Family__c,
            s.PFS2_Effective_Income_Revised__c,
            s.PFS2_Income_Supplement_Revised__c,
            s.PFS2_Net_Profit_Loss_Business_Farm__c,
            s.PFS2_Business_Farm_Share__c,
            s.PFS1_Total_Salary_and_Wages__c, 
            s.PFS1_Parent_B__c, 
            s.PFS1_Parent_A__c, 
            s.PFS1_Original_Submission_Date__c, 
            s.PFS1_Business_Farm_Owner__c, 
            s.PFS1_Business_Farm_Assets__c,  
            s.PFS1_Application_Last_Modified_by_Family__c,
            s.PFS1_Effective_Income_Revised__c,
            s.PFS1_Income_Supplement_Revised__c,
            s.PFS1_Net_Profit_Loss_Business_Farm__c,
            s.PFS1_Business_Farm_Share__c,
            s.Age__c, s.Birthdate__c, s.SSS_Code__c, 
            s.PFS1_Children_in_Tuition_Charging_School__c, //SFP-1290
            s.PFS2_Children_in_Tuition_Charging_School__c //SFP-1290
            From Student_Folder__c s where s.id in :tmpFolderIDS ]);
    }
    public  void updateFolder() {
        
        List<School_PFS_Assignment__c> allrelevantPFSassignments = getAllrelevantPFSassignments(folderIDS);
        
        Map<ID,Student_Folder__c> foldersToUpdate = getMapFoldersById(folderIDS);
        
        String previousFolderID = null;
        School_PFS_Assignment__c pfs1 = null;
        School_PFS_Assignment__c pfs2 = null;
        
        for (School_PFS_Assignment__c assignment : allrelevantPFSassignments ) {
            pfs1 = null;
            pfs2 = null;
            Student_Folder__c folder= foldersToUpdate.get(assignment.Student_Folder__c);
            if (!schoolPFSIDs.contains(assignment.id)) {
                System.debug('continued' + assignment.name + '  ' + assignment.Student_Folder__r.name);
                previousFolderID = assignment.Student_Folder__c;
                continue;
            }
            if (previousFolderID==assignment.Student_Folder__c) {
                
                pfs2 = assignment;
                setPFS2Info(folder,assignment);
            }
            else {
                
                pfs1 = assignment;
                setPFS1Info(folder,assignment);
            }
            
            previousFolderID = assignment.Student_Folder__c;
            
            
        }
        update foldersToUpdate.values();
    }
    
    public void setLastModifiedByFamilyDate(List<School_PFS_Assignment__c> spfs) {

        Set<Id> applicantIds = new Set<Id>();

        for(School_PFS_Assignment__c spf : spfs) {
            applicantIds.add(spf.Applicant__c);
        }

        Map<Id,Applicant__c> applicantMap = new Map<Id,Applicant__c>([SELECT
        Id,
        PFS__r.Id,
        PFS__r.Application_Last_Modified_by_Family__c 
        FROM Applicant__c
        WHERE Id IN : applicantIds]);
        for (School_PFS_Assignment__c spf : spfs ) {
                if ( applicantMap.get(spf.Applicant__c).PFS__r.Application_Last_Modified_by_Family__c != null ) {
                    spf.Application_Last_Modified_by_Family__c = applicantMap.get(spf.Applicant__c).PFS__r.Application_Last_Modified_by_Family__c;
                }
        }
        
    }   

    private void setPFS1Info(Student_Folder__c folder ,  School_PFS_Assignment__c assignment) {
        folder.PFS1_Total_Salary_and_Wages__c= (assignment.Total_Salary_and_Wages__c == null) ? assignment.Orig_Total_Salary_and_Wages__c :  assignment.Total_Salary_and_Wages__c   ;
        folder.PFS1_Parent_B__c =  assignment.Parent_B__c; 
        folder.PFS1_Parent_A__c =  assignment.Parent_A__c;
        folder.PFS1_Original_Submission_Date__c=  assignment.PFS_Received__c;
        folder.PFS1_Application_Last_Modified_by_Family__c=assignment.Application_Last_Modified_by_Family__c;
        folder.PFS1_Effective_Income_Revised__c = (assignment.Effective_Income_Currency__c == null) ? assignment.Orig_Effective_Income_Currency__c : assignment.Effective_Income_Currency__c;
        folder.PFS1_Income_Supplement_Revised__c = (assignment.Income_Supplement__c == null) ? assignment.Orig_Income_Supplement__c : assignment.Income_Supplement__c;
        folder.Grade_Applying__c= assignment.Grade_Applying__c;
        folder.Gender__c= assignment.Gender__c; // [CH] NAIS-1866 Removing Gender field from Folder.  All references should look at Contact.
        folder.Age__c = assignment.Age__c;
        folder.Birthdate__c = assignment.Birthdate__c;
        folder.Kamehameha_ID__c = assignment.Kamehameha_ID__c;
        folder.SSS_Code__c = assignment.School__r.SSS_School_Code__c; //NAIS-2103
        folder.PFS1_Net_Profit_Loss_Business_Farm__c = totalUpSBFAByDecimalField(assignment.School_Biz_Farm_Assignments__r, 'Net_Profit_Loss_Business_Farm__c', 'Orig_Net_Profit_Loss_Business_Farm__c');
        folder.PFS1_Business_Farm_Assets__c = totalUpSBFAByDecimalField(assignment.School_Biz_Farm_Assignments__r, 'Business_Farm_Assets__c', 'Orig_Business_Farm_Assets__c');
        folder.PFS1_Business_Farm_Share__c = totalUpSBFAByDecimalField(assignment.School_Biz_Farm_Assignments__r, 'Business_Farm_Share__c', 'Orig_Business_Farm_Share__c');
        folder.PFS1_Business_Farm_Owner__c = summarizeBizFarmOwners(assignment.School_Biz_Farm_Assignments__r);
        folder.PFS1_Children_in_Tuition_Charging_School__c = assignment.Num_Children_in_Tuition_Schools__c ;
    }
    private void setPFS2Info(Student_Folder__c folder ,  School_PFS_Assignment__c assignment) { 
        folder.PFS2_Total_Salary_and_Wages__c= (assignment.Total_Salary_and_Wages__c == null) ? assignment.Orig_Total_Salary_and_Wages__c :  assignment.Total_Salary_and_Wages__c   ;
        folder.PFS2_Parent_B__c =  assignment.Parent_B__c;
        folder.PFS2_Parent_A__c =  assignment.Parent_A__c;
        folder.PFS2_Original_Submission_Date__c=  assignment.PFS_Received__c;
        folder.PFS2_Application_Last_Modified_by_Family__c=assignment.Application_Last_Modified_by_Family__c;
        folder.PFS2_Effective_Income_Revised__c = (assignment.Effective_Income_Currency__c == null) ? assignment.Orig_Effective_Income_Currency__c : assignment.Effective_Income_Currency__c;
        folder.PFS2_Income_Supplement_Revised__c = (assignment.Income_Supplement__c == null) ? assignment.Orig_Income_Supplement__c : assignment.Income_Supplement__c;
        folder.PFS2_Net_Profit_Loss_Business_Farm__c = totalUpSBFAByDecimalField(assignment.School_Biz_Farm_Assignments__r, 'Net_Profit_Loss_Business_Farm__c', 'Orig_Net_Profit_Loss_Business_Farm__c');
        folder.PFS2_Business_Farm_Assets__c = totalUpSBFAByDecimalField(assignment.School_Biz_Farm_Assignments__r, 'Business_Farm_Assets__c', 'Orig_Business_Farm_Assets__c');
        folder.PFS2_Business_Farm_Share__c = totalUpSBFAByDecimalField(assignment.School_Biz_Farm_Assignments__r, 'Business_Farm_Share__c', 'Orig_Business_Farm_Share__c');
        folder.PFS2_Business_Farm_Owner__c = summarizeBizFarmOwners(assignment.School_Biz_Farm_Assignments__r);
        folder.PFS2_Children_in_Tuition_Charging_School__c = assignment.Num_Children_in_Tuition_Schools__c ;
    }

    // NAIS-2252 [DP] 02.05.2015 -- pushing Student_Folder.Day_Boarding__c down to SPA.Day_Boarding__c
    public static void updateChildSPAsOnDayBoardingChange(List<Student_Folder__c> folders){

        List<School_PFS_Assignment__c> spasForUpdate = new List<School_PFS_Assignment__c>();

        for (School_PFS_Assignment__c spa : [Select Id, Day_Boarding__c, Student_Folder__r.Day_Boarding__c 
                                                FROM School_PFS_Assignment__c
                                                WHERE Student_Folder__c in :folders]){
            if (spa.Day_Boarding__c != spa.Student_Folder__r.Day_Boarding__c){
                spa.Day_Boarding__c = spa.Student_Folder__r.Day_Boarding__c;
                spasForUpdate.add(spa);
            }
        }

        if (!spasForUpdate.isEmpty()){
            update spasForUpdate;
        }
    }

    // [DP] 08.13.2015 NAIS-2528 formely set from SPA, these are now set by looping through SBFA
    public static Decimal totalUpSBFAByDecimalField(List<School_Biz_Farm_Assignment__c> sbfaList, String fieldName, String origFieldName){
        Decimal runningTotal = 0;
        for (School_Biz_Farm_Assignment__c sbfa : sbfaList) {
            Decimal value = sbfa.get(fieldName) == null ? 0 : (Decimal)sbfa.get(fieldName);
            Decimal origValue = sbfa.get(origFieldName) == null ? 0 : (Decimal)sbfa.get(origFieldName);         
            runningTotal += (sbfa.get(fieldName) == null) ? origValue : value;
        }
        return runningTotal;
    }

    // [DP] 08.13.2015 NAIS-2528 formely set from SPA, these are now set by looping through SBFA and determining owner
    public static String summarizeBizFarmOwners(List<School_Biz_Farm_Assignment__c> sbfaList){
        Set<String> owners = new Set<String>();
        for (School_Biz_Farm_Assignment__c sbfa : sbfaList) {
            owners.add((sbfa.Business_Farm_Owner__c == null) ? sbfa.Orig_Business_Farm_Owner__c :  sbfa.Business_Farm_Owner__c);
        }

        if (owners.contains('Both') || (owners.contains('Parent A') && owners.contains('Parent B')) ){
            return 'Both';
        } else if (owners.contains('Parent A')){
            return 'Parent A';
        } else if (owners.contains('Parent B')){
            return 'Parent B';
        } else {
            return '';
        }
    }

    public boolean buildIDMapFromInsertBizFarm(List<School_Biz_Farm_Assignment__c> sbfaList){
        folderIDS =  new Set<ID>();
        schoolPFSIDs = new Set<ID>();
        Boolean modified= false;

        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){
            if(
                sbfa.Business_Farm_Owner__c != null || 
                (sbfa.Business_Farm_Owner__c ==null && (sbfa.Orig_Business_Farm_Owner__c != null)) || 
                
                sbfa.Business_Farm_Assets__c != null || 
                (sbfa.Business_Farm_Assets__c ==null && (sbfa.Orig_Business_Farm_Assets__c != null)) ||
                
                sbfa.Business_Farm_Share__c != null || 
                (sbfa.Business_Farm_Share__c ==null && (sbfa.Orig_Business_Farm_Share__c != null))
            ){

                modified = true;
                schoolPFSIDs.add(sbfa.School_PFS_Assignment__c);
            }
        }


        if (!schoolPFSIDs.isEmpty()){
            for (School_PFS_Assignment__c spa : [Select ID, Student_Folder__c FROM School_PFS_Assignment__c WHERE ID in :schoolPFSIDs]){
                folderIDS.add(spa.Student_Folder__c);
            }
        }
        return modified;
    }

    public boolean buildIDMapFromUpdateBizFarm(List<School_Biz_Farm_Assignment__c> sbfaList, Map<Id, School_Biz_Farm_Assignment__c> sbfaOldMap){
        folderIDS =  new Set<ID>();
        schoolPFSIDs = new Set<ID>();
        Boolean modified= false;

        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){
            School_Biz_Farm_Assignment__c oldsbfa = sbfaOldMap.get(sbfa.Id);

            if(
                oldsbfa.Business_Farm_Owner__c != sbfa.Business_Farm_Owner__c || 
                (sbfa.Business_Farm_Owner__c ==null && (oldsbfa.Orig_Business_Farm_Owner__c != sbfa.Orig_Business_Farm_Owner__c)) || 
                
                oldsbfa.Business_Farm_Assets__c != sbfa.Business_Farm_Assets__c || 
                (sbfa.Business_Farm_Assets__c ==null && (oldsbfa.Orig_Business_Farm_Assets__c != sbfa.Orig_Business_Farm_Assets__c)) || 
                
                oldsbfa.Business_Farm_Share__c != sbfa.Business_Farm_Share__c || 
                (sbfa.Business_Farm_Share__c ==null && (oldsbfa.Orig_Business_Farm_Share__c != sbfa.Orig_Business_Farm_Share__c))
                
            ){

                modified = true;
                schoolPFSIDs.add(sbfa.School_PFS_Assignment__c);
            }
        }


        if (!schoolPFSIDs.isEmpty()){
            for (School_PFS_Assignment__c spa : [Select ID, Student_Folder__c FROM School_PFS_Assignment__c WHERE ID in :schoolPFSIDs]){
                folderIDS.add(spa.Student_Folder__c);
            }
        }
        return modified;
    }
    
    //SFP-20, [G.S]
    //Update the optional fields from Annual Settings obtained from School and Academic Year picklist
    public static void updateOptionalFieldsFromAnnualSettings(List<School_PFS_Assignment__c> newList){
        
        Set<Id> schoolIDSet = new Set<ID>();
        Map<Id,Map<String, Annual_Setting__c>> schoolASMap = new Map<Id,Map<String, Annual_Setting__c>>();
        Map<String,Annual_Setting__c> asMap;
        Annual_Setting__c anSetting;
        for(School_PFS_Assignment__c spfsa : newList){
            schoolIDSet.add(spfsa.school__c);
        }
        
        for(Annual_Setting__c aSetting : [select Id, school__c, Academic_Year__c, Academic_Year__r.Name,Optional_1_Label__c,Optional_2_Label__c,
                                                 Optional_3_Label__c,Optional_4_Label__c,Optional_5_Label__c,Optional_6_Label__c 
                                            from Annual_Setting__c where School__c in :schoolIDSet]){
            if(!schoolASMap.containsKey(aSetting.school__c)){
                asMap = new Map<String,Annual_Setting__c>();
                schoolASMap.put(aSetting.school__c,asMap);
            }   
            schoolASMap.get(aSetting.school__c).put(aSetting.Academic_Year__r.Name,aSetting);                                   
        }
        for(School_PFS_Assignment__c spfsa : newList){
            if(String.isNotBlank(spfsa.Academic_Year_Picklist__c)){
                asMap = schoolASMap.get(spfsa.school__c);
                if(asMap != null && asMap.containsKey(spfsa.Academic_Year_Picklist__c)){
                    anSetting = asMap.get(spfsa.Academic_Year_Picklist__c);
                    if(anSetting != null){
                        if(String.isNotBlank(anSetting.Optional_1_Label__c)){
                            spfsa.Optional_1_Label__c = anSetting.Optional_1_Label__c;
                        }
                        if(String.isNotBlank(anSetting.Optional_2_Label__c)){
                            spfsa.Optional_2_Label__c = anSetting.Optional_2_Label__c;
                        }
                        if(String.isNotBlank(anSetting.Optional_3_Label__c)){
                            spfsa.Optional_3_Label__c = anSetting.Optional_3_Label__c;
                        }
                        if(String.isNotBlank(anSetting.Optional_4_Label__c)){
                            spfsa.Optional_4_Label__c = anSetting.Optional_4_Label__c;
                        }
                        if(String.isNotBlank(anSetting.Optional_5_Label__c)){
                            spfsa.Optional_5_Label__c = anSetting.Optional_5_Label__c;
                        }
                        if(String.isNotBlank(anSetting.Optional_6_Label__c)){
                            spfsa.Optional_6_Label__c = anSetting.Optional_6_Label__c;
                        }
                    }
                    
                }
            }
        }
    }
    
    public static void updateEthnicity(map<Id, String>  foldersWithChangedEthnicity)
    {
        list<Contact> contactsToUpdate = new list<Contact>();
        list<Student_Folder__c> foldersToUpdate = new list<Student_Folder__c>();
        
        for(Contact tmpContact : [Select Id, Ethnicity__c from Contact 
                                        where Id IN:foldersWithChangedEthnicity.keyset()])
        {
            if(foldersWithChangedEthnicity.containsKey(tmpContact.Id))
            {
                tmpContact.Ethnicity__c = foldersWithChangedEthnicity.get(tmpContact.Id);
                contactsToUpdate.add(tmpContact);
            }
        }
        if(contactsToUpdate.size()>0) update contactsToUpdate;
        
        for(Student_Folder__c tmpFolder:[Select Id, Student__c, Ethnicity__c from Student_Folder__c 
                                                where Student__c IN:foldersWithChangedEthnicity.keyset()])
        {
            if(foldersWithChangedEthnicity.containsKey(tmpFolder.Student__c) 
            && tmpFolder.Ethnicity__c!= foldersWithChangedEthnicity.get(tmpFolder.Student__c))
            {
                tmpFolder.Ethnicity__c = foldersWithChangedEthnicity.get(tmpFolder.Student__c);
                foldersToUpdate.add(tmpFolder);
            }
        }
        if(foldersToUpdate.size()>0) update foldersToUpdate;
    }//End:updateEthnicity
    
    /**
    * @description Method implemented to retrieve the Annual_Setting records
    * with Copy_Optional_Fields_From_Previous_Year__c=true for the given
    * schools in the specified academicYears.
    * @param schoolIds A list of school ids for which the Annual_Setting
    * records will be queried.
    * @param academicYears A list of academicYears (E.g.:2016-2017) for which 
    * the Annual_Setting records will be queried.
    *
    * @return A set of strings with format "{!schoolId}-{!AcademicYear}" that 
    * represents the schools that has an AnnualSetting record for the specified 
    * "{!AcademicYear}"
    */
    private static Set<String> getSchoolsWithCopyOptionalInfoFromPreviousYear(List<String> schoolIds, List<String> academicYears)
    {
        Set<String> result = new Set<String>();
        for(Annual_Setting__c a:[Select Id, Academic_Year__r.Name, School__c 
                                            from Annual_Setting__c 
                                            where Academic_Year__c <>null 
                                                and Academic_Year__r.Name IN:academicYears 
                                                and School__c IN:schoolIds 
                                                and School__c <> null
                                                and Copy_Optional_Fields_From_Previous_Year__c=true])
        {
            result.add(a.School__c+'-'+a.Academic_Year__r.Name);
        }
        return result;
    }//End:getSchoolsWithActiveCopyOptionalInfoFromPreviousYear
    
    /**
    * @description Method implemented to retrieve a map of applicants with
    * its related ContactId.
    * @param ApplicantIds The list of applicant's id to be query.
    *
    * @return A map of applicants with its related ContactId.
       */
    public static Map<Id, Id> getApplicantsContact(List<String> ApplicantIds)
    {
        Map<Id, Id> result = new Map<Id, Id>();
        for(Applicant__c a:[Select Id, Contact__c from Applicant__c 
                                                    where Id IN:ApplicantIds 
                                                        and Contact__c<>null])
        {
            result.put(a.Id,a.Contact__c);
        }
        return result;
    }//End:getApplicantsContact
    
    /**
    * @description Method implemeted to be used on before SPA trigger, to copy the
    * optional field values (Optional_1__c, Optional_2__c, Optional_3__c, Optional_4__c, Optional_5__c, Optional_6__c)
    * to the current academic year. If the applicant has an SPA from the previous year for that school.
    * All this copy logic is triggered, as long as, the related school has an AnnualSetting with 
    * Copy_Optional_Fields_From_Previous_Year__c=true for the current year.
    * @param ids The map with that contains the lists of ids (SchoolIds, Applicants, AcademicYear) 
    * needed to verify if the copy logic needs to be run for the given SPA.
    * @param allRecords A list with the SPA records who fired the trigger.
    */
    public static void copyOptionalInfoFromPreviousYear(PreviousYearOptionalInfo PreviousYearOptionalInfoHandler, 
                                                        List<School_PFS_Assignment__c> allRecords)
    {
        if( PreviousYearOptionalInfoHandler.shouldCopyInfo() )
        {
            List<String> schoolIds = PreviousYearOptionalInfoHandler.schoolIds;
            List<String> ApplicantIds = PreviousYearOptionalInfoHandler.applicantIds;
            Map<Id, Id> contactsByApplicantId = getApplicantsContact(ApplicantIds);
            List<String> priorAcademicYears = PreviousYearOptionalInfoHandler.priorAcademicYear;
            List<String> currentAcademicYears = PreviousYearOptionalInfoHandler.currentAcademicYear;
            map<String, School_PFS_Assignment__c> priorSPA = new map<String, School_PFS_Assignment__c>();
            
            //>>STEP 01: Retrieve the schools with Copy_Optional_Fields_From_Previous_Year__c=true
            Set<String> schools = getSchoolsWithCopyOptionalInfoFromPreviousYear(schoolIds, currentAcademicYears);
            
            if(schools!=null && schools.size()>0)
            {
                //STEP 02: Retrieve the SPA for the same applicant that triggers this logic from previous academic year
                for(School_PFS_Assignment__c a:[Select Id, Applicant__c, Applicant__r.Contact__c, School__c, Academic_Year_Name__c,
                                                    Optional_1__c, Optional_2__c, Optional_3__c, 
                                                    Optional_4__c, Optional_5__c, Optional_6__c  
                                                    from School_PFS_Assignment__c 
                                                    where Applicant__r.Contact__c <> null 
                                                        and Applicant__c <> null 
                                                        and School__c <> null 
                                                        and Academic_Year_Name__c <> null 
                                                        and Applicant__r.Contact__c IN:contactsByApplicantId.values()  
                                                        and School__c IN:schoolIds 
                                                        and Academic_Year_Name__c IN:priorAcademicYears
                                                        and (Optional_1__c<>null 
                                                            or Optional_2__c<>null 
                                                            or Optional_3__c<>null 
                                                            or Optional_4__c<>null 
                                                            or Optional_5__c<>null 
                                                            or Optional_6__c<>null)])
                {                    
                    if(schools.contains(a.School__c+'-'+GlobalVariables.getNextAcademicYearStr(a.Academic_Year_Name__c)))
                    {
                        priorSPA.put(a.School__c+'-'+a.Academic_Year_Name__c+'-'+a.Applicant__r.Contact__c, a);
                    }
                }
                //STEP 03: If there's optional data for previous SPA, then copy those values to the new SPA for the current academic year
                if(priorSPA.size()>0)
                {
                    School_PFS_Assignment__c spaPrior;
                    String tmpContact;
                    
                    for(School_PFS_Assignment__c spaNew:allRecords)
                    {
                        tmpContact = (contactsByApplicantId.containsKey(spaNew.Applicant__c)
                                    ?contactsByApplicantId.get(spaNew.Applicant__c):null);
                                    
                        if(tmpContact!=null 
                        && priorSPA.containsKey(spaNew.School__c+
                                                '-'+GlobalVariables.getPreviousAcademicYearStr(spaNew.Academic_Year_Name__c)+
                                                '-'+tmpContact))
                        {
                            spaPrior = priorSPA.get(spaNew.School__c+
                                                    '-'+GlobalVariables.getPreviousAcademicYearStr(spaNew.Academic_Year_Name__c)+
                                                    '-'+tmpContact);
                            spaNew.Optional_1__c = spaPrior.Optional_1__c;
                            spaNew.Optional_2__c = spaPrior.Optional_2__c;
                            spaNew.Optional_3__c = spaPrior.Optional_3__c;
                            spaNew.Optional_4__c = spaPrior.Optional_4__c;
                            spaNew.Optional_5__c = spaPrior.Optional_5__c;
                            spaNew.Optional_6__c = spaPrior.Optional_6__c;
                        }
                    }
                }
            }                
        }
    }//End:copyOptionalInfoFromPreviousYear
    
    
    
    /**
    * @description Class implemented to map all the required information to execute the logic
    * needed to copy optional fields (Optional_1__c, Optional_2__c, Optional_3__c, Optional_4__c,
    * Optional_5__c, Optional_6__c) to the new SPA from previous year.
    */
    public class PreviousYearOptionalInfo{
        public List<String> schoolIds{get;set;}
        public List<String> priorAcademicYear{get;set;}
        public List<String> currentAcademicYear{get;set;}
        public List<String> applicantIds{get;set;}
        
        public PreviousYearOptionalInfo(){
            this.schoolIds = new List<String>();
            this.priorAcademicYear = new List<String>();
            this.currentAcademicYear = new List<String>();
            this.applicantIds = new List<String>();
        }//End-Constructor
        
        public void add(String schoolId, String previousAcademicYear, String actualAcademicYear, String applicantId){
            if(schoolId!=null)this.schoolIds.add(schoolId);
            if(previousAcademicYear!=null)this.priorAcademicYear.add(GlobalVariables.getPreviousAcademicYearStr(previousAcademicYear));
            if(actualAcademicYear!=null)this.currentAcademicYear.add(actualAcademicYear);
            if(applicantId!=null)this.applicantIds.add(applicantId);
        }//End:add
        
        public boolean shouldCopyInfo(){
            return (!this.schoolIds.isEmpty() && !this.priorAcademicYear.isEmpty() 
                && !this.currentAcademicYear.isEmpty() && !this.applicantIds.isEmpty()?true:false);
        }//End:shouldCopyInfo
    }//End-InnerClass
}