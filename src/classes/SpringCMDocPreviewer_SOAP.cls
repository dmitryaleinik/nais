public with sharing class SpringCMDocPreviewer_SOAP implements ISpringCMDocPreviewer{

    
    
    // SFP-1251: We had to add an additional url parameter in response to some changes made by SpringCM which prevented us from previewing certain types of image and docx files inline.
    // The new parameter and value is pdf=true.
    @testVisible private static final String SCM_PUBLISH_URL_FORMAT = 'https://{0}/DownloadDocuments.ashx?aid={1}&oUid={2}&pslUid={3}&{4}';
    
    public static final Databank_Settings__c databankSettings = Databank_Settings__c.getInstance('Databank');
    public static final boolean UAT = Test.isRunningTest() ? false : databankSettings.Use_UAT__c;
    @testvisible String throwExceptionForTesting;
    private springCMProd.SpringCMServiceSoap prodService;
    private springCMUAT.SpringCMServiceSoap uatService;
    private SpringCMProd.SCMDocument prodDocument;
    private SpringCMUAT.SCMDocument uatDocument;
    public void setupService(){
        if (UAT){
            uatService = new springCMUAT.SpringCMServiceSoap();
        } else {
            prodService = new springCMProd.SpringCMServiceSoap();
        }
    }

    public String doAuthenticateSSO(){
        if (!Test.isRunningTest()) {
            if (UAT){
                return uatService.AuthenticateSSO(UserInfo.getUsername(), UserInfo.getSessionId(), databankSettings.API_Partner_Server_URL__c, databankSettings.APIKey__c, databankSettings.AccountId__c);
            } else {
                return prodService.AuthenticateSSO(UserInfo.getUsername(), UserInfo.getSessionId(), databankSettings.API_Partner_Server_URL__c, databankSettings.APIKey__c, databankSettings.AccountId__c);
            }
        } else {
            this.throwTestException();
            return 'testtoken';
        }
    }
    
    @testvisible private void throwTestException(){        
        if(!String.isBlank(throwExceptionForTesting)){
            throw new SpringCMProd.springCMException(throwExceptionForTesting);
        }
    }//End:throwTestException
    
    public String doGetPrevVersionDocId(String token, String docid){
        if (!Test.isRunningTest()) {
            if (UAT){
                SpringCMUAT.ArrayOfSCMDocument allVersions = uatService.GetDocumentRevisions(token, docid, false);
                if (allVersions != null && allVersions.SCMDocument != null && allVersions.SCMDocument.size() > 0){
                    // this is ordered from most recent to least recent, so take the last one
                    docid = allVersions.SCMDocument[allVersions.SCMDocument.size()-1].Id;
                }
            } else {
                SpringCMProd.ArrayOfSCMDocument allVersions = prodService.GetDocumentRevisions(token, docid, false);
                if (allVersions != null && allVersions.SCMDocument != null && allVersions.SCMDocument.size() > 0){
                    // this is ordered from most recent to least recent, so take the last one
                    docid = allVersions.SCMDocument[allVersions.SCMDocument.size()-1].Id;
                }
            }
        }else{
            this.throwTestException();
        }
        return docid;
    }

    public void doDocumentGetById(String token, String docid){
        if (!Test.isRunningTest()) {
            if(UAT){
                uatDocument = uatService.DocumentGetById(token, docid, false);
            } else {
                prodDocument = prodService.DocumentGetById(token, docid, false);
            }
        }else{
            this.throwTestException();
        }
    }

    public String doDocumentPublish(String token, Integer secs){
        String url;
        if (!Test.isRunningTest()) {
            if(UAT){
                url = uatService.DocumentPublish(token, uatDocument, uatDocument.Name, dateTime.now().addSeconds(secs), true);
            } else {
                url = prodService.DocumentPublish(token, prodDocument, prodDocument.Name, dateTime.now().addSeconds(secs), true);
            }
        } else {
            this.throwTestException();
            url = '/testpdfurl?';
        }
        return url;
    }

    private String token {get;set;}

    @TestVisible private String docid {get;set;}

    public boolean isValidPage {get; set;}
    public Family_Document__c theFamilyDocument {get; set;}
    private String errorMessageStr {get; set;}

    public string publishUrlStr {get;set;}

    public SpringCMDocPreviewer_SOAP() {
    }
    
    public String getPublishUrl() {
        
        return publishUrlStr;
    }
    
    public String getErrorMessage() {
        
        return errorMessageStr;
    }

    public PageReference getPdf(){
        isValidPage = true;
        theFamilyDocument = null;
        docid = ApexPages.currentPage().getParameters().get('id');

        if (docId == null) {
            errorMessageStr = 'The document is not currently available for preview.';
            isValidPage = false;
            return null;
        }

        if (String.isNotBlank(docid)) {
            for (Family_Document__c fd : [
                SELECT Id, Import_Id__c, Document_Status__c, Document_Type__c, Document_Year__c, Document_Pertains_to__c, Filename__c 
                FROM Family_Document__c
                WHERE Import_Id__c = :docid LIMIT 1]) {
                theFamilyDocument = fd;
            }
        }

        if (theFamilyDocument == null) {
            errorMessageStr = 'Unable to load document';
            isValidPage = false;
            return null;
        }

        string url;
        setupService();

        Boolean success = false;
        String lastErrorMSG = '';
        //Databank_Settings__c databankSettings;
        //databankSettings = Databank_Settings__c.getInstance('Databank');

        Integer tryAttempts = (databankSettings != null && databankSettings.TryAttemptsDocs__c != null) ? Integer.valueOf(databankSettings.TryAttemptsDocs__c) : 5;
        // [DP] NAIS-2009 -- retrying up to 10 times total (apex limit) to get around the POODLE error
        for (Integer i = 0; i < tryAttempts; i++){
            try {
                token = doAuthenticateSSO();
                i = tryAttempts + 1;     // once success occurs we break out of loop
                success = true;         // and set the booleand to true
            } catch (Exception e){
                if (e.getMessage().toLowerCase().contains('handshake')){
                    lastErrorMSG = e.getMessage();
                    SpringCMAction.fakeSleep();
                }else if (e.getMessage().toLowerCase().contains('invalid token')){//SFP-914 
                    errorMessageStr = Label.SpringCM_User_Configuration_Missing;
                    return null;
                }else {
                    throw new SpringCMProd.springCMException(e.getMessage() + ' - line ' + e.getLineNumber());
                }
            }
        }

        if (!success && !Test.isRunningTest()){
            throw new SpringCMProd.springCMException('There was an error authenticating to SpringCM.  Please retry or contact ' 
                + Label.Company_Name_Full + '.  Error: ' + lastErrorMSG);
        }

        // NAIS-2234 [DP] 03.27.2015 if this is a 1040, go get the docId for the original upload
        if (theFamilyDocument.Document_Type__c != null && theFamilyDocument.Document_Type__c.contains('1040')){
            success = false;
            lastErrorMSG = '';

            for (Integer i = 0; i < tryAttempts; i++){
                try {
                    docId = doGetPrevVersionDocId(token, docid);
                    i = tryAttempts + 1;     // once success occurs we break out of loop
                    success = true;         // and set the booleand to true
                } catch (Exception e){
                    if (e.getMessage().toLowerCase().contains('handshake')){
                        lastErrorMSG = e.getMessage();
                        SpringCMAction.fakeSleep();
                    } else {
                        throw new SpringCMProd.springCMException(e.getMessage() + ' - line ' + e.getLineNumber());
                    }
                }
            }
        }

        success = false;
        lastErrorMSG = '';

        for (Integer i = 0; i < tryAttempts; i++){
            try {
                doDocumentGetById(token, docid);

                i = tryAttempts + 1;     // once success occurs we break out of loop
                success = true;         // and set the booleand to true
            } catch (Exception e){
                if (e.getMessage().toLowerCase().contains('handshake')){
                    lastErrorMSG = e.getMessage();
                    SpringCMAction.fakeSleep();
                } else {
                    throw new SpringCMProd.springCMException(e.getMessage() + ' - line ' + e.getLineNumber());
                }
            }
        }

        if (!success && !Test.isRunningTest()){
            throw new SpringCMProd.springCMException('There was an error retrieving the document from SpringCM.  Please retry or contact ' 
                + Label.Company_Name_Full + '.  Error: ' + lastErrorMSG);
        }

        success = false;
        lastErrorMSG = '';
        for (Integer i = 0; i < tryAttempts; i++){
            try {
                //maxPages = document.PDFPageCount;
                Integer secs = Databank_Settings__c.getInstance('Databank').PDF_Publish_Seconds__c == null ? 5 : Integer.valueOf(Databank_Settings__c.getInstance('Databank').PDF_Publish_Seconds__c);
                url = doDocumentPublish(token, secs);

                i = tryAttempts + 1;     // once success occurs we break out of loop
                success = true;         // and set the booleand to true
            } catch (Exception e){
                if (e.getMessage().toLowerCase().contains('handshake')){
                    lastErrorMSG = e.getMessage();
                    SpringCMAction.fakeSleep();
                } else {
                    throw new SpringCMProd.springCMException(e.getMessage() + ' - line ' + e.getLineNumber());
                }
            }
        }

        if (!success){
            throw new SpringCMProd.springCMException('There was an error publishing in SpringCM.  Please retry or contact ' 
                + Label.Company_Name_Full + '.  Error: ' + lastErrorMSG);
        }
        this.publishUrlStr = buildPreviewUrl(url, theFamilyDocument.Filename__c);
        
        return new PageReference(publishUrlStr);
    }

    @testvisible private String buildPreviewUrl(String springUrl, String filename) {
        //If url has existing parameters it is legacy behavior. Should not be hit after upgrade on 7/8/2016
        if(springUrl.contains('?')) {
            return springUrl + '&inline=true&pdf=true';
        }
        
        //Spring API no longer returns a correct url that supports inline=true so we have to build the url ourselves.
        String[] urlSplit = springUrl.split('/');
        Integer splitSize = urlSplit.size();
        
        //Parse out the individual components of the url
        String baseUrl = urlSplit[2]; // like shareuatna11.springcm.com for uat url
        String accountId = urlSplit[splitSize - 3];
        String documentId = urlSplit[splitSize - 2];
        String previewId = urlSplit[splitSize - 1];
        
        List<String> SCMPublishURLParams = new List<String> {
                        baseUrl,
                        accountId,
                        documentId,
                        previewId};
                        
        SCMPublishURLParams.add( isFileImage(filename) ? 'inline=true' : 'inline=true&pdf=true' );//SFP-1251
        
        return String.format(SCM_PUBLISH_URL_FORMAT,SCMPublishURLParams);
    }//End:buildPreviewUrl
    

    private Boolean isFileImage(String fileName) {
        
        if(fileName != null) {
            
            fileName = fileName.toLowerCase();
            Set<String> IMAGE_FORMATS =  new Set<String>{'.jpg', '.jpeg', '.bmp'};
            for(String extension : IMAGE_FORMATS){
                if(fileName.endsWith(extension)){
                    return true;
                }
            }
        }
        
        return false;
    }//End:checkFileExtension
}