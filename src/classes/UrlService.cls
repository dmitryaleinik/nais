/**
 * @description Handle interacting with the Url and parameters.
 **/
public class UrlService {
    @testVisible private static final String ID_PARAM = 'Id';
    @testVisible private static final String DESIRED_TYPE_PARAM = 'desiredType';
    @testVisible private static final String QUERY_PARAM_PARAM = 'queryParam';

    /**
     * @description Get the Id Url Parameter and validate it's type
     *              is what is expected, returning either the Id or
     *              null.
     * @param desiredType The desired SObject Type that the Id should
     *        be a type of.
     * @returns The potential Id stored in the Id query parameter.
     */
    public Id getIdByType(SObjectType desiredType) {
        return getIdByType(ID_PARAM, desiredType);
    }

    /**
     * @description Get an Id from a Query Parameter and validate
     *              it's type is what is expected, returning either
     *              the Id or null.
     * @param queryParam The Query Parameter to get the potential
     *        Id from.
     * @param desiredType The desired SObject Type that the Id should
     *        be a type of.
     * @returns The potential Id stored in the provided query
     *          parameter.
     * @throws An ArgumentNullException if queryParam or desiredType
     *         are null.
     */
    public Id getIdByType(String queryParam, SObjectType desiredType) {
        ArgumentNullException.throwIfNull(queryParam, QUERY_PARAM_PARAM);
        ArgumentNullException.throwIfNull(desiredType, DESIRED_TYPE_PARAM);

        String potentialId = ApexPages.CurrentPage().getParameters().get(queryParam);

        Id parsedId = (potentialId instanceOf Id) ? (Id)potentialId : null;

        if (parsedId == null || parsedId.getSObjectType() != desiredType) {
            return null;
        }

        return parsedId;
    }

    /**
     * @description An instance property to store the singleton
     *              instance of the UrlService.
     */
    public static UrlService Instance {
        get {
            if (Instance == null) {
                Instance = new UrlService();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to keep this from being
     *              instantiated externally.
     */
    private UrlService() { }
}