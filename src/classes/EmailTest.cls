/**
 * EmailTest.cls
 *
 * @description: Test class for Email using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest
private class EmailTest {
    
    /* test data setup */
    @testSetup static void setupTestData() {

        // any necessary data setup
    }
    
    /* negative test cases */

    // Construct an Email object with nulls
    @isTest static void emailConstructor_withNulls_emptyEmail() {
        
        // Arrange
        // no work

        // Act
        Email e = new Email( null, null, null, null);
        Email e2 = new Email( null, null, null, null,
                              null, null, null, null, null, null);

        // Assert
        System.assertEquals( 0, e.toAddresses.size());
        System.assertEquals( 0, e.fromAddresses.size());
        System.assertEquals( 0, e2.toAddresses.size());
        System.assertEquals( 0, e2.fromAddresses.size());
    }


    /* postive test cases */

    // Construct an Email object with valid values
    @isTest static void emailConstructor_withValues_validEmail() {

        // Arrange
        Id schoolId = '003J0000011yJcHIAU';
        Id senderId = '003J0000011yJcHIAU';
        Id recipId = '003J0000011yJcHIAU';
        Id spaId = '003J0000011yJcHIAU';
        Id massEmailId = '003J0000011yJcHIAU';

        // Act
        Email e = new Email( new List<String> {'testemail@test.com'}, new List<String> {'info@test.com'}, 'test with space', 'more testing',
                             schoolId, senderId, recipId, spaId, massEmailId, '2016-2017');
        Email e2 = new Email( new List<String> {'testemail@test.com', 'testemail2@test.com'}, new List<String> {'info@test.com', 'info2@test.com'}, 'test with space', 'more testing',
                             schoolId, senderId, recipId, spaId, massEmailId, '2016-2017');

        // Assert
        System.assertEquals( 1, e.toAddresses.size());
        System.assertEquals( 1, e.fromAddresses.size());
        System.assertEquals( 2, e2.toAddresses.size());
        System.assertEquals( 2, e2.fromAddresses.size());
    }
    
}