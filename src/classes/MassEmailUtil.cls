/**
 * MassEmailUtil.cls
 *
 * @description: Utility class for common, reusable Mass Email Send related functionality
 *
 * @author: Chase Logan @ Presence PG
 */
public class MassEmailUtil {
    
    /* constants */
    public static final String COMMON_DATE_FORMAT = 'MMMMM d, YYYY h:mm a';
    
    // Mass Email Setting custom metadata record names
    public static final String MASS_EMAIL_TEMPLATE_HTML = 'MassEmailTemplateHTML';
    private static final String MASS_EMAIL_ENABLED = 'GloballyEnabled';
    private static final String MASS_EMAIL_ALLOWED_PROFILES = 'AllowedProfileNames';
    private static final String MASS_EMAIL_DEBUG_WEBHOOK = 'DebugEventWebhook';
    private static final String MASS_EMAIL_IMG_UPLOAD_URL = 'ImageUploadEndpointURL';
    // String constants
    private static final String PARAM_YES = 'yes';
    private static final String PARAM_NO = 'no';
    
    // default ctor
    public MassEmailUtil() {}

    /**
     * @description: Indicates whether Mass Email application is globally enabled/disabled
     *
     * @return: Boolean - indicator for whether application is enabled/disabled
     */
    public static Boolean isMassEmailGloballyEnabled() {
        Boolean returnVal = false;

        Mass_Email_Setting__mdt massESMdt = [select Value__c 
                                               from Mass_Email_Setting__mdt 
                                              where DeveloperName = :MASS_EMAIL_ENABLED];
        if ( massESMdt != null && massESMdt.Value__c.equalsIgnoreCase( PARAM_YES)) {

            returnVal = true;
        }
            
           return returnVal;
    }

    /**
     * @description: Indicates whether Mass Email application is globally enabled/disabled 
     * && enabled for this specific School (Account)
     *
     * @param: acctId - a valid School Id (Account.Id) - required
     *
     * @return: Boolean - indicator for whether application is enabled/disabled for this School (Account)
     */
    public static Boolean isMassEmailGloballyEnabledByAccount( String acctId) {
        
        return isMassEmailGloballyEnabledByAccount( acctId, null);
    }

    /**
     * @description: Indicates whether Mass Email application is globally enabled/disabled 
     * && enabled for this specific School (Account) and Profile
     *
     * @param: acctId - a valid School Id (Account.Id) - required
     * @param: profileId - profileId - a valid Profile.Id - optional
     *
     * @return: Boolean - indicator for whether application is enabled/disabled for this School (Account)
     */
    public static Boolean isMassEmailGloballyEnabledByAccount( String acctId, String profileId) {
        // acctId is required, or if app is globally disabled, early return false
        if ( String.isBlank( acctId) || MassEmailUtil.isMassEmailGloballyEnabled() == false) return false;

        // check to see if application is enabled at the School (Account) level
        Account acct = new AccountDataAccessService().getAccountById( acctId);

        return isMassEmailGloballyEnabledByAccount(acct, profileId);
    }

    /**
     * @description: Indicates whether Mass Email application is globally enabled/disabled
     * && enabled for this specific School (Account) and Profile
     *
     * @param: schoolAccount - A school account - required
     * @param: profileId - profileId - a valid Profile.Id - optional
     *
     * @return: Boolean - indicator for whether application is enabled/disabled for this School (Account)
     */
    public static Boolean isMassEmailGloballyEnabledByAccount(Account schoolAccount, String profileId) {
        Boolean returnVal = false;

        // schoolAccount is required, or if app is globally disabled, early return false
        if ( schoolAccount == null || MassEmailUtil.isMassEmailGloballyEnabled() == false) return returnVal;

        if ( schoolAccount != null && schoolAccount.Mass_Email_Enabled__c) {

            if ( String.isBlank( profileId)) {

                returnVal = true;
            } else if ( String.isNotBlank( profileId) && isCurrentProfileEnabled( profileId)) {

                returnVal = true;
            } else if ( String.isNotBlank( profileId) && !isCurrentProfileEnabled( profileId)) {

                returnVal = false;
            }
        }

        return returnVal;
    }

    /**
     * @description Returns the settings for the mass email feature which determine which parts of the feature are enabled
     *              for the specified school and profile.
     */
    public static EnablementSettings getFeatureSettings(String accountId, String profileId) {
        EnablementSettings featureSettings = new EnablementSettings();

        Account acct = new AccountDataAccessService().getAccountById(accountId);

        featureSettings.IsEnabled = isMassEmailGloballyEnabledByAccount(acct, profileId);

        // In order for a school to be able to use the mass email feature to send award letters, the global feature
        // toggle must be on and an individual toggle on the account must be enabled.
        featureSettings.IsSendEmailsPerApplicantEnabled = FeatureToggles.isEnabled('Allow_Mass_Email_Per_Applicant__c') && acct.Mass_Email_Award_Letters_Enabled__c;

        return featureSettings;
    }

    /**
     * @description: Indicates whether Mass Email Event Webhook is in Debug mode. 
     * If true, SendgridEventWebhook writes incoming requests and any exceptions to IntegrationLog__c
     *
     * @return: Boolean - indicator for whether application is enabled/disabled
     */
    public static Boolean isWebhookDebugEnabled() {
        Boolean returnVal = false;

        Mass_Email_Setting__mdt massESMdt = [select Value__c 
                                               from Mass_Email_Setting__mdt 
                                              where DeveloperName = :MASS_EMAIL_DEBUG_WEBHOOK];
        if ( massESMdt != null && massESMdt.Value__c.equalsIgnoreCase( PARAM_YES)) {

            returnVal = true;
        }
            
        return returnVal;
    }

    /**
     * @description: Writes a record to IntegrationLog__c
     *
     * @param: stackTraceString - String representation of stack trace
     * @param: exceptionMessage - String of actual caught exception message
     * @param: bodyContent - main content intended to be stored in IntegrationLog__c
     */
    public static void writeToIntegrationLog( String stackTraceString, String exceptionMessage, String bodyContent) {

        // if all params empty, early return
        if ( String.isBlank( stackTraceString) && String.isBlank( exceptionMessage) && String.isBlank( bodyContent)) return;

        IntegrationLog__c iLog = new IntegrationLog__c(
            Body__c = 'Stack Trace: ' + ( String.isNotBlank( stackTraceString) ? stackTraceString : 'N/A') +
                      ' :Exception Message: ' + ( String.isNotBlank( exceptionMessage) ? exceptionMessage : 'N/A') + 
                      ' :Log Content: ' + ( String.isNotBlank( bodyContent) ? bodyContent : 'N/A'));
        insert iLog;
    }

    /**
     * @description: Returns configured SchoolImageUploadEndpoint URL.
     *
     * @return: String - the accessible endpoint URL
     */
    public static String getImageUploadURL() {
        String returnVal;

        Mass_Email_Setting__mdt massESMdt = [select Value__c 
                                               from Mass_Email_Setting__mdt 
                                              where DeveloperName = :MASS_EMAIL_IMG_UPLOAD_URL];
        if ( massESMdt != null) {

            returnVal = massESMdt.Value__c;
        }
            
        return returnVal;
    }

    /**
     * @description: creates a map of merge field name to actual field value based on provided spa
     *
     * @param: spa - the SPA to create the map for - required
     *
     * @return: a map of merge field name to actual SPA field value
     */
    public static Map<String,String> createMergeMap( School_PFS_Assignment__c spa) {
        Map<String,String> returnMergeDataMap = new Map<String,String>();

        // early return on missing or invalid param(s)
        if ( spa == null) return returnMergeDataMap;

        final Map<String,Mass_Email_Merge_Setting__c> MERGE_SETTING_MAP = Mass_Email_Merge_Setting__c.getAll();

        Mass_Email_Merge_Setting__c mergeSetting;
        for (String s : MERGE_SETTING_MAP.keySet()) {
            mergeSetting = MERGE_SETTING_MAP.get(s);

            returnMergeDataMap.put(mergeSetting.Merge_Name__c,
                getMergeValue(mergeSetting.Model_Name__c, spa));
//                ( String)spa.get( MERGE_SETTING_MAP.get( s).Model_Name__c));
        }
        
        return returnMergeDataMap;
    }

    private static final List<String> CURRENCY_FORMAT = new List<String> { '0','number','###,###,##0.00' };

    private static Map<String, Schema.SObjectField> schoolPfsAssignmentFieldsByName;
    private static Map<String, Schema.SObjectField> getSchoolPfsAssignmentFieldsByName() {
        if (schoolPfsAssignmentFieldsByName != null) {
            return schoolPfsAssignmentFieldsByName;
        }

        schoolPfsAssignmentFieldsByName = Schema.SObjectType.School_PFS_Assignment__c.fields.getMap();

        return schoolPfsAssignmentFieldsByName;
    }

    private static Map<String, Schema.DescribeFieldResult> spaFieldMetadataByName;
    private static Schema.DescribeFieldResult getSpaFieldMetadata(String fieldName) {
        if (spaFieldMetadataByName == null) {
            spaFieldMetadataByName = new Map<String, Schema.DescribeFieldResult>();
        }

        Schema.DescribeFieldResult fieldMetadata = spaFieldMetadataByName.get(fieldName);

        if (fieldMetadata != null) {
            return fieldMetadata;
        }

        Schema.SObjectField spaSobjectField = getSchoolPfsAssignmentFieldsByName().get(fieldName);

        if (spaSobjectField == null) {
            return null;
        }

        fieldMetadata = spaSobjectField.getDescribe();

        spaFieldMetadataByName.put(fieldName, fieldMetadata);

        return fieldMetadata;
    }

    private static String getMergeValue(String fieldApiName, School_PFS_Assignment__c schoolPfsAssignment) {
        Object fieldValue = schoolPfsAssignment.get(fieldApiName);
        if (fieldValue == null) {
            return ' ';
        }

        Schema.DescribeFieldResult spaFieldMeta = getSpaFieldMetadata(fieldApiName);

        if (spaFieldMeta == null) {
            return String.valueOf(fieldValue);
        }

        if (spaFieldMeta.getType() == Schema.DisplayType.CURRENCY) {
            Decimal currencyValue = (Decimal)fieldValue;
            return String.format(currencyValue.format(), CURRENCY_FORMAT);
        }

        return String.valueOf(fieldValue);
    }

    /**
     * @description: Verifies any merge field names present in the provided messageBody are valid
     *
     * @param: messageBody - the message body to search and validate - required
     *
     * @return: A set of incorrect merge fields, this set will be empty if validation is successful
     */
    public static Set<String> verifyMergeFields( String messageBody) {
        Set<String> badMergeFieldSet = new Set<String>();

        // early return on missing or invalid param(s)
        if ( String.isEmpty( messageBody)) return badMergeFieldSet;

        final String MERGE_OPEN = '{{';
        final String MERGE_CLOSE = '}}';
        final String INVALID_SUFFIX = ' is an invalid merge field';
        final Set<String> VALID_MERGE_SET = Mass_Email_Merge_Setting__c.getAll().keySet();

        // early return if no merge fields present
        if ( !messageBody.contains( MERGE_OPEN)) return badMergeFieldSet;

        // verfiy all merge field names present are valid against VALID_MERGE_SET
        String messageClone = messageBody;
        while ( true) {

            // if no merge fields remain, break and return
            if ( messageClone.indexOf( MERGE_OPEN) == -1) break;

            String currentMergeField = messageClone.substring( messageClone.indexOf( MERGE_OPEN) + 2, messageClone.indexOf( MERGE_CLOSE));
            if ( !VALID_MERGE_SET.contains( currentMergeField)) {

                badMergeFieldSet.add( currentMergeField + INVALID_SUFFIX);
            } 

            // cut already scanned message out and continue from there
            messageClone = messageClone.substring( ( messageClone.indexOf( currentMergeField) + currentMergeField.length() + 2));
        }
        
        return badMergeFieldSet;
    }

    /**
     * @description: Replaces merge field syntax with actual field values, expects a map of merge field
     * name to actual field value.
     *
     * @param: messageBody - the message body to search and replace - required
     * @param: mergeDataMap - a map of merge field name to actual field value - required
     *
     * @return: String - the message body with any fields replaced.
     */
    public static String replaceMergeFields( String messageBody, Map<String,String> mergeDataMap) {

        // early return on missing or invalid param(s)
        if ( String.isEmpty( messageBody) || mergeDataMap == null || mergeDataMap.isEmpty()) return messageBody;

        for (String s : mergeDataMap.keySet()) {
            String mergeDataValue = mergeDataMap.get(s);

            messageBody = messageBody.replace(s, mergeDataValue);
        }
        
        return messageBody;
    }

    /* private methods */

    /**
     * @description: Indicates whether Mass Email application is enabled for a given Profile
     *
     * @param: profileId - a valid Profile.Id - required
     *
     * @return: Boolean - indicator for whether application is enabled/disabled for this Profile
     */
    private static Boolean isCurrentProfileEnabled( String profileId) {
        Boolean returnVal = false;

        // profileId is required
        if ( String.isBlank( profileId)) return returnVal;

        Mass_Email_Setting__mdt massESMdt = [select Value__c 
                                               from Mass_Email_Setting__mdt 
                                              where DeveloperName = :MASS_EMAIL_ALLOWED_PROFILES];
        
        if ( massESMdt != null && !String.isBlank( massESMdt.Value__c)) {

            List<String> profileNameList = massESMdt.Value__c.split( ',');
            List<Profile> profileList = [select Id from Profile where Name in :profileNameList];
            if ( profileList != null) {

                for ( Profile p : profileList) {

                    if ( p.Id == profileId) {

                        returnVal = true;
                    }
                }
            }
        }
            
           return returnVal;
    }
    
    public static String getEmailTemplateBody(Mass_Email_Send__c massES) {
        
        final Set<String> htmlEmailTemplates = new Set<String>{ MASS_EMAIL_TEMPLATE_HTML };
        
        String emailBody = '';
        
        // query VF Email template using field on MES record
        Id massESTemplateId = new MassEmailSendDataAccessService().getMassEmailSendTemplateId( massES.Visualforce_Template__c);
        Messaging.SingleEmailMessage sMessage =  Messaging.renderStoredEmailTemplate( massESTemplateId, null, massES.Id);
        emailBody = sMessage.getHtmlBody();
        
        if (MASS_EMAIL_TEMPLATE_HTML == massES.Visualforce_Template__c) {
            
            MassEmailController controller = new MassEmailController();
            controller.massEmailSendId = massES.Id;
            emailBody = getMassEmailComponent(emailBody, controller);
        }
        
        return emailBody;
    }
    
    private static String getMassEmailComponent(String templateBody, MassEmailController controller) {
        
        String body = '';
        String header = '';
        String footer = '';
        
        header = '<img height="60px" src=" ' + controller.logoURL + '" />';
        
        if (controller.hasHtmlBody) {
            
            body = controller.getHtmlBody();
            
        } else if (controller.hasPlainTextBody) {
             
             body = controller.getPlainTextBody();
        }
        
        if (controller.hasDefaultFooter) {
        
            footer = controller.getDefaultFooter();
            
        } else if (controller.hasFooter) {
        
            footer = controller.getFooter();
        }
        
        templateBody = templateBody.replace('[[HTML_HEAD]]', header);
        templateBody = templateBody.replace('[[HTML_BODY]]', body);
        templateBody = templateBody.replace('[[HTML_FOOTER]]', footer);
        
        return templateBody;
     }

    /**
     * @description This inner class is used to store config settings about this feature such as which features are enabled.
     */
    public class EnablementSettings {

        /**
         * @description True if the mass email feature is enabled.
         */
        public Boolean IsEnabled {
            get {
                return IsEnabled == null ? false : IsEnabled;
            }
            set;
        }

        /**
         * @description True if users should be able to send emails per applicant instead of per parent.
         */
        public Boolean IsSendEmailsPerApplicantEnabled {
            get {
                return IsSendEmailsPerApplicantEnabled == null ? false : IsSendEmailsPerApplicantEnabled;
            }
            set;
        }
    }
}