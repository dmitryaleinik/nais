/**
* @FamilyDocumentCleanupSchedulerTest.cls
* @date 3/6/2014
* @author  CH for Exponent Partners
* @description Unit tests for schedulable class for cleaning up orphaned Family Document records
*/

@isTest
private class FamilyDocumentCleanupSchedulerTest {

    @isTest
    private static void testDeleteOrphanedDocuments() {
        // Start with a custom setting that looks for records over 1 day old
        Document_Cleanup__c newDocCleanup = new Document_Cleanup__c();
        newDocCleanup.Name = 'Family Document Life';
        newDocCleanup.Number_Value__c = 1;
        newDocCleanup.Query_Size__c = 10;
        insert newDocCleanup;
        
        Academic_Year__c currentYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        
        Account school1 = TestUtils.createAccount('Test School 1', RecordTypes.schoolAccountTypeId, 3, true);
        
        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        Database.insert(new List<Contact>{parentA, student1});
        
        PFS__c pfsA = TestUtils.createPFS('PFS A', currentYear.Id, parentA.Id, true);
        
        // SFP-640
        Verification__c testVerification = new Verification__c();
        insert testVerification;

        pfsA.Verification__c = testVerification.Id;
        update pfsA;

        // need household__c associated to parents and family_document__c
        Household__c testHousehold = TestUtils.createHousehold('hh1', true);
        parentA.Household__c = testHousehold.id;
        update parentA;

        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, true);
        Student_Folder__c studentFolder11 = TestUtils.createStudentFolder('Student Folder 1.1', currentYear.Id, student1.Id, true);
        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(currentYear.Id, applicant1A.Id, school1.Id, studentFolder11.Id, true);
        
        // Put in a Required Document
        Required_Document__c reqDoc = TestUtils.createRequiredDocument(currentYear.Id, school1.Id, true);
                        
        // Put in a couple of document records
        Family_Document__c famDoc1 = new Family_Document__c(Document_Status__c = 'Processed');
        famDoc1.Household__c = testHousehold.Id;

        Family_Document__c famDoc2 = new Family_Document__c(Document_Status__c = 'Not Received');
        Family_Document__c famDoc3 = new Family_Document__c(Document_Status__c = 'Not Received');
        Family_Document__c famDoc4 = new Family_Document__c(Document_Status__c = 'Not Received');
        Database.insert(new List<Family_Document__c>{famDoc1, famDoc2, famDoc3, famDoc4});

        String documentYear = GlobalVariables.getDocYearStringFromAcadYearName(GlobalVariables.getCurrentYearString());

        // We don't need to insert a School Document Assignment linked to a Required Document because one is auto-generated
        //  when the Required Document record is inserted above
        // School_Document_Assignment__c docAssign1 = new School_Document_Assignment__c(Document__c = famDoc2.Id, School_PFS_Assignment__c = spfsa1.Id, Required_Document__c = reqDoc.Id);
        School_Document_Assignment__c docAssign2 = new School_Document_Assignment__c(Document__c = famDoc3.Id, School_PFS_Assignment__c = spfsa1.Id, PFS_Specific__c = true);
        docAssign2.Document_Year__c = documentYear;
        School_Document_Assignment__c docAssign3 = new School_Document_Assignment__c(Document__c = famDoc4.Id, School_PFS_Assignment__c = spfsa1.Id);
        docAssign3.Document_Year__c = documentYear;
        Database.insert(new List<School_Document_Assignment__c>{docAssign2, docAssign3});
        
        // Make sure filtering logic excludes documents that should not be deleted
        FamilyDocumentCleanupScheduler.doDeletes();
        
        System.assertEquals(3, [select count() from School_Document_Assignment__c]);
        System.assertEquals(4, [select count() from Family_Document__c]);
        
        // Update the lifespan parameter
        newDocCleanup.Number_Value__c = 0;
        update newDocCleanup;
        
        // Run the cleanup using the Schedulable interface
        Test.startTest();
            // first delete any jobs
            ScheduleSharingJobsController.deleteJobsRemotely();
            
            Id job0Id = System.schedule('Test Schedule', '0 59 23 * * ?', new FamilyDocumentCleanupScheduler());
            
            // Verify that the job was scheduled
            CronTrigger ct = [select Id, CronExpression from CronTrigger where Id = :job0Id];
            System.assertEquals('0 59 23 * * ?', ct.CronExpression);
        Test.stopTest();
        
        // Check that the correct documents were deleted
        System.assertEquals(2, [select count() from School_Document_Assignment__c]);
        System.assertEquals(1, [select count() from Family_Document__c]);
    }
}