public class EfcCalculatorBatchSchedulerSss implements Schedulable
{

    public void execute(SchedulableContext SC)
    {
        if (EfcUtil.getEfcSettingDisableSssAutoEfcCalc() != true)
        {
             EfcCalculatorBatchProcessor.doExecuteBatch(EfcCalculatorBatchProcessor.CalcType.SSS);
        }
    }
}