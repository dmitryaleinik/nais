@isTest
public without sharing class TestDataFamily {

    public Account school1 {get; set;}
    public Account school2 {get; set;}
    public Account schoolKS {get; set;}
    public Contact parentA {get; set;}
    public Contact student1 {get; set;}
    public Contact student2 {get; set;}
    public Student_Folder__c studentFolder1 {get; set;}
    public Student_Folder__c studentFolder2 {get; set;}
    public PFS__c pfsA {get; set;}
    public Applicant__c applicant1A {get; set;}
    public Applicant__c applicant2A {get; set;}
    public User familyPortalUser {get; set;}
    public School_PFS_Assignment__c schoolpfsa1 {get; set;}
    public School_PFS_Assignment__c schoolpfsa2 {get; set;}
    public Annual_Setting__c annSetSchool1 {get; set;}
    public Annual_Setting__c annSetSchool2 {get; set;}

    public List<Federal_Tax_Table__c> fedTaxTables {get; set;}
    public List<State_Tax_Table__c> stateTaxTables {get; set;}
    public List<Employment_Allowance__c> emplAllowances {get; set;}
    public List<Net_Worth_of_Business_Farm__c> busFarm {get; set;}
    public List<Retirement_Allowance__c> retAllowances {get; set;}
    public List<Asset_Progressivity__c> assetProgs {get; set;}
    public List<Income_Protection_Allowance__c> incProAllowances {get; set;}
    public List<Expected_Contribution_Rate__c> expContRates {get; set;}
    public List<Housing_Index_Multiplier__c> houseIndexes {get; set;}

    // Create SSSConstants records for the Academic Years
    public SSS_Constants__c sssConst1 {get; set;}
    public SSS_Constants__c sssConst2 {get; set;}

    public Id academicYearId {get; set;}
    public Id academicYearPastId {get; set;}

    public TestDataFamily() {
        this(false);
    }

    public TestDataFamily(Boolean createTables) {

        Family_Portal_Settings__c fps = new Family_Portal_Settings__c();
        fps.Name = 'Family';
        fps.Individual_Account_Owner_Id__c = [Select Id from User where IsActive = true AND UserRoleId != null AND Profile.Name = 'System Administrator'][0].Id;
        insert fps;

        //setup PFS
        // create test data
        List<Academic_Year__c> academicYears = TestUtils.createAcademicYears();
        academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        academicYearPastId = GlobalVariables.getPreviousAcademicYear().Id;



        parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
        parentA.Email = 'familyTestData@exp.exp';
        List<Contact> newFamilyContacts = new List<Contact> { parentA, student1, student2};

        List<Account> newFamilyAccounts = new List<Account>{
                                                ContactAction.getBucketAccountForFamilyContact(parentA, null),
                                                ContactAction.getBucketAccountForFamilyContact(student1, null),
                                                ContactAction.getBucketAccountForFamilyContact(student2, null)
        };
        insert newFamilyAccounts;

        parentA.AccountId = newFamilyAccounts[0].Id;
        student1.AccountId = newFamilyAccounts[1].Id;
        student2.AccountId = newFamilyAccounts[2].Id;
        insert newFamilyContacts;

        studentFolder1 = TestUtils.createStudentFolder('Student Folder 1.1', academicYearId, student1.Id, false);
        studentFolder2 = TestUtils.createStudentFolder('Student Folder 2.1', academicYearId, student2.Id, false);
        studentFolder1.New_Returning__c = 'New';
        studentFolder2.New_Returning__c = 'New';
        studentFolder1.Day_Boarding__c = 'Day'; // NAIS-2252 [DP] 02.05.2015
        studentFolder2.Day_Boarding__c = 'Day'; // NAIS-2252 [DP] 02.05.2015
        insert new List<Student_Folder__c> {studentFolder1, studentFolder2};

        familyPortalUser = TestUtils.createPortalUser(
            parentA.LastName, parentA.Email + String.valueOf(Math.random()) + String.valueOf(Math.random()), 'alias', parentA.Id, GlobalVariables.familyPortalProfileId, true, false);
        familyPortalUser.TimeZoneSidKey = UserInfo.getTimeZone().getID();
        insert familyPortalUser;

        school1 = TestUtils.createAccount('Test School 1', RecordTypes.schoolAccountTypeId, 3, false);
        school2 = TestUtils.createAccount('Test School 2', RecordTypes.schoolAccountTypeId, 3, false);
        schoolKS = TestUtils.createAccount('Test School KS 1', RecordTypes.schoolAccountTypeId, 3, false);
        school1.SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest();
        school2.SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest();
        schoolKS.SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest();
        insert new List<Account> {school1, school2, schoolKS};

        // Create school custom settings
        SchoolPortalSettings.KS_School_Account_Id = schoolKS.Id;

        pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
        insert new List<PFS__c> {pfsA};

        applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, false);
        applicant1A.Grade_in_Entry_Year__c = '4';
        applicant2A = TestUtils.createApplicant(student2.Id, pfsA.Id, false);
        applicant2A.Grade_in_Entry_Year__c = '11';
        insert new List<Applicant__c> {applicant1A, applicant2A};

        schoolpfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder1.Id, false);
        schoolpfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.Id, school1.Id, studentFolder2.Id, false);

        //schoolpfsa1.Day_Boarding__c = 'Day'; // NAIS-2252 [DP] 02.05.2015
        //schoolpfsa2.Day_Boarding__c = 'Day'; // NAIS-2252 [DP] 02.05.2015
        insert new List<School_PFS_Assignment__c> {schoolpfsa1,schoolpfsa2};

        annSetSchool1 = TestUtils.createAnnualSetting(school1.Id, academicYearId, false);
        annSetSchool1.Grade_4_Day_Tuition__c = 111;
        annSetSchool1.PFS_Deadline_Type__c = 'Soft';
        annSetSchool1.PFS_New__c = System.today().addDays(-1);
        annSetSchool1.PFS_Returning__c = System.today().addDays(-1);
        annSetSchool2 = TestUtils.createAnnualSetting(school2.Id, academicYearId, false);
        annSetSchool2.Grade_4_Day_Tuition__c = 2;
        annSetSchool2.Grade_4_Day_Travel_Expense__c = 22;
        annSetSchool2.Grade_4_Day_Fees__c = 222;
        annSetSchool2.Grade_4_Day_New_Student_Expense__c = 2222;
        annSetSchool2.Grade_4_Boarding_Tuition__c = 20;
        annSetSchool2.Grade_4_Boarding_Travel_Expense__c = 220;
        annSetSchool2.Grade_4_Boarding_Fees__c = 2220;
        annSetSchool2.Grade_4_Boarding_New_Student_Expense__c = 22220;
        annSetSchool2.One_Schedule_Day_Tuition__c = 9;
        annSetSchool2.One_Schedule_Day_Travel_Expense__c = 99;
        annSetSchool2.One_Schedule_Day_Fees__c = 999;
        annSetSchool2.One_Schedule_Day_New_Student_Expense__c = 9999;
        annSetSchool2.One_Schedule_Boarding_Tuition__c = 90;
        annSetSchool2.One_Schedule_Boarding_Travel_Expense__c = 990;
        annSetSchool2.One_Schedule_Boarding_Fees__c = 9990;
        annSetSchool2.One_Sched_Boarding_New_Student_Expense__c = 99990;

        insert new List<Annual_Setting__c>{annSetSchool1, annSetSchool2};

        //reload pfs
        String pfsSql = 'SELECT ' + String.Join(ExpCore.GetAllFieldNames('PFS__c'), ',') + ', (SELECT ' + String.Join(ExpCore.GetAllFieldNames('Applicant__c'), ',') + ' FROM Applicants__r) FROM PFS__c LIMIT 1';
        pfsA = Database.query(pfsSql);

        if (createTables){

            fedTaxTables = new List<Federal_Tax_Table__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                fedTaxTables.add(TestUtils.createFederalTaxTable(academicYears[i].Id, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_SINGLE, false));
            }
            Database.insert(fedTaxTables);

            stateTaxTables = new List<State_Tax_Table__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                stateTaxTables.add(TestUtils.createStateTaxTable(academicYears[i].Id, false));
            }
            Database.insert(stateTaxTables);

            emplAllowances = new List<Employment_Allowance__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                emplAllowances.add(TestUtils.createEmploymentAllowance(academicYears[i].Id, false));
            }
            Database.insert(emplAllowances);

            busFarm = new List<Net_Worth_of_Business_Farm__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                busFarm.add(TestUtils.createBusinessFarm(academicYears[i].Id, false));
            }
            Database.insert(busFarm);

            retAllowances = new List<Retirement_Allowance__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                retAllowances.add(TestUtils.createRetirementAllowance(academicYears[i].Id, false));
            }
            Database.insert(retAllowances);

            assetProgs = new List<Asset_Progressivity__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                assetProgs.add(TestUtils.createAssetProgressivity(academicYears[i].Id, false));
            }
            Database.insert(assetProgs);

            incProAllowances = new List<Income_Protection_Allowance__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                incProAllowances.add(TestUtils.createIncomeProtectionAllowance(academicYears[i].Id, false));
            }
            Database.insert(incProAllowances);

            expContRates = new List<Expected_Contribution_Rate__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                expContRates.add(TestUtils.createExpectedContributionRate(academicYears[i].Id, false));
            }
            Database.insert(expContRates);

            houseIndexes = new List<Housing_Index_Multiplier__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                houseIndexes.add(TestUtils.createHousingIndexMultiplier(academicYears[i].Id, false));
            }
            Database.insert(houseIndexes);
        }

        // Create SSSConstants records for the Academic Years
        sssConst1 = TestUtils.createSSSConstants(academicYearId, false);
        sssConst2 = TestUtils.createSSSConstants(academicYearPastId, false);
        Database.insert(new List<SSS_Constants__c>{sssConst1, sssConst2});
    }


}