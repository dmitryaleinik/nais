/**
 * SaoMockHttpResponseGenerator
 *
 * @description
 *
 * @author Mike Havrilla @ Presence PG
 */

@isTest
global class SaoMockHttpResponseGenerator implements HttpCalloutMock {
    // Implement required interface method
    global HTTPResponse respond( HTTPRequest req) {

        // Create the response
        HttpResponse res = new HttpResponse();

        res.setHeader( 'Content-Type', 'application/json');
        res.setBody( '{"folioId":"1111", "currentGrade": "Grade4", "id": "1234", "academicYear": "2017", "schoolId": "9999" }');
        res.setStatusCode( 200);

        return res;
    }
}