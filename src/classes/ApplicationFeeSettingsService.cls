/**
 * @description Service class for handling Application Fee Settings.
 */
public class ApplicationFeeSettingsService {

    /**
     * @description The Means Based Fee Waiver record type name.
     */
    public static final String MEANS_BASED_FEE_WAIVER_RECORDTYPE = 'Means Based Fee Waiver';
    
    /**
     * @description Returns the account Id of the only account with the Means Based Fee Waiver record type.
     */
    public Id getMeansBasedFeeWaiverAccountId() {
        List<Account> meansBasedFeeWaiverAccounts = AccountSelector.Instance
            .selectByRecordTypeName(MEANS_BASED_FEE_WAIVER_RECORDTYPE);

        if (meansBasedFeeWaiverAccounts == null || meansBasedFeeWaiverAccounts.isEmpty()) {
            return null;
        }

        return meansBasedFeeWaiverAccounts[0].Id;
    }

    /**
     * @description Singleton instance property.
     */
    public static ApplicationFeeSettingsService Instance {
        get {
            if (Instance == null) {
                Instance = new ApplicationFeeSettingsService();
            }
            return Instance;
        }
        private set;
    }

    private ApplicationFeeSettingsService() {}
}