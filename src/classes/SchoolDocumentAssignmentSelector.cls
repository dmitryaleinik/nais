/**
 * @description Query for School_Document_Assignment__c records.
 **/
public class SchoolDocumentAssignmentSelector extends fflib_SObjectSelector 
{

    /**
     * @description Selects All School Document Assignment records with only Id field.
     * @return A list of School Document Assignment records.
     */
    public List<School_Document_Assignment__c> selectAll()
    {
        assertIsAccessible();

        return Database.query(newQueryFactory().toSOQL());
    }

    private Schema.SObjectType getSObjectType()
    {
        return School_Document_Assignment__c.getSObjectType();
    }

    private List<Schema.SObjectField> getSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
                School_Document_Assignment__c.School__c,
                School_Document_Assignment__c.Document__c,
                School_Document_Assignment__c.School_PFS_Assignment__c,
                School_Document_Assignment__c.Required_Document__c,
                School_Document_Assignment__c.Requirement_Waiver_Status__c
        };
    }

    /**
     * @description Creates a new instance of the SchoolDocumentAssignmentSelector.
     * @return An instance of SchoolDocumentAssignmentSelector.
     */
    public static SchoolDocumentAssignmentSelector newInstance()
    {
        return new SchoolDocumentAssignmentSelector();
    }
    
    /**
     * @description Singleton property ensuring external sources do not
     *              instantiate this directly.
     */
    public static SchoolDocumentAssignmentSelector Instance
    {
        get
        {
            if (Instance == null)
            {
                Instance = new SchoolDocumentAssignmentSelector();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to enforce singleton access
     *              via the Instance property.
     */
    private SchoolDocumentAssignmentSelector() {}
}