/**
 * Class: SpringCMAjaxUploadController
 *
 * Copyright (C) 2015 NAIS
 *
 * Purpose: Ajax upload method for Spring CM upload -- most code was written by SpringCM
 *
 * Where Referenced: SpringCMAjaxUploadPage.page
 *
 *
 * Change History:
 *
 * Developer         Date                          Description
 * ---------------------------------------------------------------------------------------
 * Drew Piston       2015.10.20                    Initial Development
 *
 **/
public class SpringCMAjaxUploadController {
    // set from custom settings
    public String sendDocEndpoint {get; set;}
    public String getTokenEndpoint {get; set;}
    public String clientId {get; set;}
    public String clientSecret {get; set;}
    public String moveDocumentURL {get; set;}
    public String workflowURL {get; set;}
    public String checkExistingURL {get; set;}
    public String setEOSURL {get; set;}
    public Integer pageCounter {get; set;}
    public String XSpringCMContentSHA256 {get; set;}
    public Family_Document__c fDoc {get; set;}
    public Document_Type_Settings__c docSettings {get; set;}
    public PFS__c thePfs {get; set;}
    public String pfsnumber {get; set;}
    public String hhId {get; set;}
    public String hhName {get; set;}
    public String verifyFlag {get; set;}    // SFP-29
    public String scmAuthenticationError {get; set;}     // SFP-71
    public Integer maxPreviewAttempts {get; set;} // SFP-192 [DP] 11.24.2015

    // for button labels
    public Help_Configuration__c helpConfig {get; set;}

    private String pdocumentURL;
    public Blob Document {get; set;}
    public String DocumentName {get; set;}
    public String ForceFallBack {
        get {
                return ApexPages.currentPage().getParameters().get('fallback');
        }
    }

    public Boolean isRestrictFileSizeEnabled {
        get {
            return DocumentUploadSettings.restrictFileSize && DocumentUploadSettings.maxFileSize != null;
        }
        private set;
    }
    
    @RemoteAction public static SpringSMDocumentResponseWrapper isValidFileSize(String nativeFileSize)
    {
        Decimal fileSize = nativeFileSize != null ? Decimal.valueOf(nativeFileSize) : 0;
        
        Decimal maxFileSizeInMB = DocumentUploadSettings.maxFileSize;
        
        Decimal maxFileSizeInBytes = maxFileSizeInMB * 1024 * 1024;
        
        Boolean isValidDocSize = !(fileSize > maxFileSizeInBytes);

        return new SpringSMDocumentResponseWrapper(maxFileSizeInBytes, fileSize, isValidDocSize);
    }

    public String DocumentURL 
    {
        get 
        {
            if (pdocumentURL == null && Document != null) {
                SendDocument();
                Document = null;
            }
            return pdocumentURL;
        }
        set {
            pdocumentURL = value;
        }
    }
    
    public Boolean disableDocumentUploadPreviews{ 
        get {
            
            if (disableDocumentUploadPreviews == null) {
                disableDocumentUploadPreviews = DocumentUploadSettings.getBooleanValue('Disable_Document_Upload_Previews__c');
            }
            
            return disableDocumentUploadPreviews;
        }
        private set;
    }

    public Boolean alreadyFellback {
        get {
            return System.currentPagereference().getParameters().get('fellback') == 'true';
        }
        set;
    }
    public Boolean isTaxableDocument{
        get {
            return fDoc != null ? ApplicationUtils.isTaxDocument(fDoc.Document_Type__c) : false;
        }
    }
     public Boolean isEnforcePageLimitActive{
        get {
            if( isEnforcePageLimitActive == null ) {
                isEnforcePageLimitActive = DocumentUploadSettings.isEnabled('Enforce_Page_Limit__c');
            }
            return isEnforcePageLimitActive != null ? isEnforcePageLimitActive : false;
        }
        private set;
    }
    
     public Integer numberPageLimit{
        get {
            if( numberPageLimit == null ) {
                numberPageLimit = DocumentUploadSettings.getValue('Page_Limit__c');
            }
            return numberPageLimit != null ? numberPageLimit : 900;
        }
        private set;
    }
     public Boolean enableFileHashRestriction{
        get {
            if( enableFileHashRestriction == null ) {
                enableFileHashRestriction = GlobalVariables.isFamilyPortalUser() && Databank_Settings__c.getInstance('Databank').Enable_File_Hash_Restriction__c;
            }
            
            return enableFileHashRestriction != null ? enableFileHashRestriction : false;
        }
        private set;
     }

    // constructor
    public SpringCMAjaxUploadController() {

        String fDocId = System.currentPagereference().getParameters().get('fdocid');

        fDoc = [Select Id, Name, Document_Year__c, Document_Type__c, Household__c, Household__r.Name, Document_Source__c, School_Code__c, Document_Pertains_To__c
                    FROM Family_Document__c
                    WHERE Id = :fDocId];

        List<Document_Type_Settings__c>  documentsSettings = [
            SELECT Upload_Details_Label__c, Full_Type_Name__c
            FROM Document_Type_Settings__c
            WHERE Full_Type_Name__c = :fDoc.Document_Type__c ];

        if (!documentsSettings.isEmpty())
        {
            docSettings = documentsSettings[0];
        }

        //X040_with_all_filed_schedules_and_attachment_extra_info
        pfsNumber = System.currentPagereference().getParameters().get('pfsnum');

        if (!String.isEmpty(pfsNumber))
        {
            thePfs = [Select Id, Academic_Year_Picklist__c, PFS_Status__c, PFS_Number__c, Payment_Status__c FROM PFS__c where PFS_Number__c = :pfsNumber];
        }

        hhId = fDoc.Household__c;
        hhName = fDoc.Household__r.Name;


        Databank_Settings__c databankSettings = Databank_Settings__c.getInstance('Databank');

        sendDocEndpoint = databankSettings.sendDocEndpoint__c;
        getTokenEndpoint = databankSettings.getTokenEndpoint__c;
        clientId = databankSettings.clientId__c;
        clientSecret = databankSettings.clientSecret__c;
        moveDocumentURL = databankSettings.moveDocumentURL__c;
        workflowURL = databankSettings.workflowURL__c;
        checkExistingURL = databankSettings.checkExistingURL__c;
        setEOSURL = databankSettings.setEOSURL__c;
       
        String language = UserInfo.getLanguage();
        language = language == null || language == '' ? 'en_US' : language;
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        helpConfig = [Select Id, Doc_Upload_Guidelines_Heading__c, Doc_Upload_Guidelines__c, SpringCM_Cancel_Label__c, SpringCM_Next_Label__c, SpringCM_Page_Label__c, SpringCM_Previous_Label__c, SpringCM_Replace_Label__c, SpringCM_Submit_Label__c, SpringCM_Upload_Label__c
                            from Help_Configuration__c
                            where Academic_Year__c = : academicYearId
                            AND Language__c = :language limit 1];

        // SFP-192 [DP] 11.24.2015
        maxPreviewAttempts = Integer.valueOf(databankSettings.Upload_Preview_Attempts__c == null ? 30 : databankSettings.Upload_Preview_Attempts__c);

        authorizeAndSetTheToken(); // no longer get token in constructor! [DP] 11.04.2015 SFP-138

        // SFP-29 START [DP] 01.05.2016
        Boolean doVerify = false;
        for (School_Document_Assignment__c sda : [select Verify_Data__c from School_Document_Assignment__c where Document__c = :fDoc.Id and Verify_Data__c = true]) {
            doVerify = sda.Verify_Data__c;
        }
        verifyFlag = doVerify ? 'T' : 'F';
        // SFP-29 END
    }

    public void SendDocument() {
        
        HttpRequest req = new HttpRequest();
        req.setHeader('Accept', 'application/json');
        req.setHeader('Authorization', 'oauth ' + token);
        req.setEndpoint(sendDocEndpoint + EncodingUtil.urlEncode(DocumentName, 'UTF-8') );
        req.setMethod('POST');
        req.setBodyAsBlob(Document);
        Http http = new Http();

        //Execute web service call here
        HTTPResponse res = http.send(req);
        Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());

        DocumentURL = String.ValueOf(m.get('Href'));
    }

    public String Token {
        get{
            if (token == null || token == 'ERROR') {
                authorizeAndSetTheToken();
            }
            return token;
        }
        set;
    }

    public void authorizeAndSetTheToken() {
        HttpRequest req = new HttpRequest();

        // [DP] 08.19.2016 always use "Site.getBaseURL" since both Family and School use Community
        String url = Site.getBaseUrl() + '/services/Soap/u/18.0/' + UserInfo.getOrganizationId();

        req.setTimeout(60000);

        req.setHeader('Accept', 'application/json');
        req.setHeader('content-type', 'application/x-www-form-urlencoded');
        req.setEndpoint(getTokenEndpoint);
        req.setMethod('POST');
        req.setBody('SFSession=' + UserInfo.getSessionId() + '|' + EncodingUtil.urlEncode(url, 'UTF-8') + '&ClientId=' + clientId + '&ClientSecret=' + clientSecret);

        Http http = new Http();
        //Execute web service call here
        try {
            HTTPResponse res = http.send(req);
            // SFP-71 START
            if (res.getStatusCode() == 200) {
                Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
                token = String.ValueOf(m.get('AccessTokenValue'));
                scmAuthenticationError = null;
            } else {
                scmAuthenticationError = Label.SpringCM_Authentication_Error;
                token = 'ERROR';
            }
        } catch (Exception e) {
            scmAuthenticationError = Label.SpringCM_Authentication_Error;
            token = 'ERROR';
        }
        // SFP-71 END
    }

    // updates fam doc in SF after a successful submit
    public PageReference processSubmitSuccess() {

        // if the return page is loaded before the upload is processed by spring CM, then mark the status Upload Pending
        Family_Document__c familyDocument = null;

        if (String.isNotBlank(fDoc.Id)) {
            
            List<Family_Document__c> fds = new List<Family_Document__c>([
                SELECT Id, Document_Status__c, Request_to_Waive_Requirement__c 
                FROM Family_Document__c
                WHERE Id = :fDoc.Id 
                LIMIT 1
                FOR UPDATE  // Lock the record to avoid a race condition with the Spring CM update logic
                ]);
            
            familyDocument =  !fds.isEmpty() ? fds[0] : null;
            
            if (familyDocument != null) {
                Boolean needsUpdate = false;
                try {
                    if (familyDocument.Document_Status__c == 'Not Received' && familyDocument.Request_to_Waive_Requirement__c != 'Yes') {
                        familyDocument.Document_Status__c = 'Upload Pending';
                        needsUpdate = true;
                    }
                    
                    if(enableFileHashRestriction && XSpringCMContentSHA256 != null ) {
                        familyDocument.File_Hash__c = XSpringCMContentSHA256;
                        needsUpdate = true;
                    }
                    if(isEnforcePageLimitActive && pageCounter != null ) {
                        familyDocument.Page_Count__c = pageCounter;
                        needsUpdate = true;
                    }

                    // SFP-1150 if the feature toggle is on and the verifyFlag is true, set the Verification Requested Date.
                    if (populateVerificationRequestedDate(familyDocument)) {
                        needsUpdate = true;
                    }

                    if(needsUpdate) update familyDocument;
                }
                catch (DmlException e) {
                    String errmsg = 'ERROR: Unable to update family document ' + familyDocument.Id + ': ' + e.getMessage() + ' ' + e.getStackTraceString();
                    insert new Custom_Debug__c(Message__c = errmsg);
                }
            }
        }

        return returnToDocPage();
    }

    // This method returns a boolean to indicate whether or not we need to update the record.
    private Boolean populateVerificationRequestedDate(Family_Document__c familyDocumentToUpdate) {
        // If we aren't tracking the verification requested date, then return early.
        if (!FeatureToggles.isEnabled('Track_Verification_Request_Date__c')) {
            return false;
        }

        // If we are requesting verification for this document, we need to set the date when verification was requested.
        if (verifyFlag == 'T') {
            familyDocumentToUpdate.put('Verification_Requested_Date__c', System.now());
            return true;
        }

        return false;
    }

    // deletes fam doc in SF after a cancel
    public PageReference processCancelDelete() {
        if (String.isNotBlank(fDoc.Id)) {
            Family_Document__c familyDocument = null;

            // retrieve and delete the family document, but only if it hasn't been received
            for (Family_Document__c fd : [SELECT Id, (select Id from School_Document_Assignments__r where Required_Document__c = null and PFS_Specific__c != true)
                                            FROM Family_Document__c
                                            WHERE Id = :fDoc.Id
                                            AND Document_Status__c = 'Not Received'
                                            AND Import_Id__c = null
                                            AND Filename__c = null
                                            AND Request_to_Waive_Requirement__c != 'Yes'
                                            LIMIT 1]) {
                familyDocument = fd;
            }

            // delete the family document record
            if (familyDocument != null) {

                // delete the family document SDA records that are from an Other/Misc Document upload
                if (familyDocument.School_Document_Assignments__r != null) {
                    try {
                        delete familyDocument.School_Document_Assignments__r;
                    }
                    catch (DmlException e) {
                        System.debug(LoggingLevel.Error, 'ERROR: Unable to delete School Document Assignments ' + familyDocument.Id + ': ' + e.getMessage() + ' ' + e.getStackTraceString());
                    }
                }

                try {
                    delete familyDocument;
                }
                catch (DmlException e) {
                    System.debug(LoggingLevel.Error, 'ERROR: Unable to delete family document ' + familyDocument.Id + ': ' + e.getMessage() + ' ' + e.getStackTraceString());
                }
            }
        }

        return returnToDocPage();
    }

    public PageReference returnToDocPage() {
        PageReference pr;
        String spaId = System.currentPagereference().getParameters().get('schoolpfsassignmentid');
        if (!String.isBlank(spaID)) {
            pr = Page.SchoolPFSDocument;
        } else {
            pr = Page.FamilyDocuments;
        }

        persistURLParams(pr);
        return pr;
    }


    public static Set<String> urlParamsToPersist = new Set<String>{'id', 'academicyearid', 'schoolpfsassignmentid', 'requireddocumentid', 'pfstabname', 'pfsid', 'tabname'};
    public static void persistURLParams(PageReference pr) {
        for (String paramKey : System.currentPagereference().getParameters().keySet()) {
            if (urlParamsToPersist.contains(paramKey.toLowerCase())) {
                pr.getParameters().put(paramKey, System.currentPagereference().getParameters().get(paramKey));
            }
        }
    }

    // smart redirect (for either school or family portal) after cancel or submit from fallback method
    public PageReference alreadyFellbackRedirect() {
        PageReference pr = GlobalVariables.isSchoolPortalProfile(UserInfo.getProfileId()) ? Page.SchoolPFSDocument : Page.FamilyDocuments;
        persistURLParams(pr);
        return pr;
    }

    // url for "fallback" (when ajax is not supported)
    public PageReference getFallBackURL() {
        return null;
    }

    // translates sf doc type into databank friendly doc type
    public String getDatabankFriendlyDocType() {
        return SpringCMAction.getSpringCMDocTypeFromSFDocType(fDoc.Document_Type__c);
    }

    // pfs status css class
    public String getPFSStatusCSS() {
        if(thePfs != null) {
            return ApplicationUtils.calcPFSStatusForCSS(thePfs);
        } else {
            return '';
        }
    }

    // school versus family portal view
    public Boolean isSchoolPortal {
        get {
            return GlobalVariables.isSchoolPortalProfile(UserInfo.getProfileId());
        }
    }
    
    @RemoteAction public static SpringSMDocumentResponseWrapper hasDuplicatedSpringCMDocument(
                                                                        String downloadURL, 
                                                                        String authorizationHeader, 
                                                                        String householdId,
                                                                        String familyDocumentId) {

        String SHA256Var = null;
        Boolean isDuplicated = false;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(downloadURL);
        req.setMethod('HEAD');
        req.setHeader('Authorization', authorizationHeader);
        Http http = new Http();
        HTTPResponse res;
        
        try{
            res = http.send(req);
            
            if (res.getStatusCode() == 200) {
                
                SHA256Var = res.getHeader('X-SpringCM-Content-SHA256');
                return new SpringSMDocumentResponseWrapper(SHA256Var, householdId);
            }else if(Test.isRunningTest()) {
                return new SpringSMDocumentResponseWrapper(authorizationHeader, householdId);
            } else {
                throw new SpringCMApiException('Server returned HTTP response code: '+res.getStatusCode() + ' ' + res.getStatus()+ ' ' + res.getBody());
            }
        }catch(Exception e) {
             return handleRemoteActionsRequestFailures(res, e, familyDocumentId);
        }
        
        return null; 
    }//End:hasDuplicatedSpringCMDocument
    
    @RemoteAction public static SpringSMDocumentResponseWrapper pageCounterSpringCMDocument(
                                                                        String documentUrl, 
                                                                        String authorizationHeader,
                                                                        String familyDocumentId)
    {
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(documentUrl);
        req.setMethod('GET');
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Accept', 'application/pdf');
        Http http = new Http();
        HTTPResponse res;
        
        try{
            res = http.send(req);
            
            if (res.getStatusCode() == 200) {
                Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
                return new SpringSMDocumentResponseWrapper( responseMap.containsKey('PageCount') ? String.ValueOf(responseMap.get('PageCount')) : null );
            } else {
                throw new SpringCMApiException('Server returned HTTP response code: '+res.getStatusCode() + ' ' + res.getStatus()+ ' ' + res.getBody());
            }
        }catch(Exception e) {
             return handleRemoteActionsRequestFailures(res, e, familyDocumentId);
        }
        return null;
    }
    
    /**
    * @description Creates a custom_Debug record for the given exception, caused by a request to SPRING CM REST API.
    * To see the details of the error codes, go to: https://developer.springcm.com/guides/rest-api-response-and-error-codes.
    * @param response The response returned by the request to SpringCM.
    * @param exception The exception caused by the request to SpringCM.
    * @param RecordID The family Document Id where the user is trying to upload a document.
    */
    private static SpringSMDocumentResponseWrapper handleRemoteActionsRequestFailures(HTTPResponse response, Exception e, Id RecordID) {
        
        Set<String> statusToStopRetry = new Set<String>{'400','401', '403', '422', '429', '500'};
        Boolean shouldRetry = true;
        
        //1.- We verify if the error in the response won't need for us to keep retrying the request.
        if(response != null && statusToStopRetry.contains(String.ValueOf(response.getStatusCode())) ) {
            
            //1.1- When the request caused and exception, and the response is null.
            shouldRetry = false;
        } else {
            
            //1.2- When the response is not null and caused an exception by itself.
            for(String s : statusToStopRetry) {
                if( e.getMessage().contains('response code: '+s) ) {
                    shouldRetry = false;
                }
            }
        }
        
        //2.1- If the response is not related to security or springcm limits reached. Then, we retry the request.
        if(shouldRetry) {
            return null;
        }
        
        //2.2- Otherwise, we send a flag to prevent the caller method to keep retrying the request.
        SpringSMDocumentResponseWrapper result = new SpringSMDocumentResponseWrapper('-1');
        result.requestFailed = true;
        
        //3- We create the custom debug record.
        CustomDebugService.Instance.withCustomMessage('Family Document ID: ' + RecordID).logException(e);
        
        return result;     
    }//End:handleRemoteActionsRequestFailures
    
    public class SpringSMDocumentResponseWrapper{
        
        public String SHA256 { get; set; }
        public Id householdId { get; set; }
        public Boolean isDuplicated { get; set; }
        public String duplicateDocumentYear { get; set; }
        public String duplicateDocumentType { get; set; }
        public Integer pageCount { get; set; }
        public Decimal fileSize { get; set; }
        public Decimal maxFileSize { get; set; }
        public Boolean requestFailed { get; set; }
        public Boolean isValidFileSize { get; set; }
        
        public SpringSMDocumentResponseWrapper(Decimal maxFileSizeVar, Decimal fileSizeVar, Boolean isValidFileSizeVar) {
            
            maxFileSize = maxFileSizevar != null && maxFileSizevar > 0 ? (maxFileSizevar / 1024) / 1024 : 0;
            fileSize = fileSizeVar != null && fileSizeVar > 0 ? ((fileSizeVar/1024) / 1024).setScale(2) : 0;
            isValidFileSize = isValidFileSizeVar;
            requestFailed = false;
        }
        
        public SpringSMDocumentResponseWrapper(String pageCountVar) {
            
            pageCount = pageCountVar != null ? Integer.valueOf(pageCountVar) : -1;
            requestFailed = false;
        }
        
        public SpringSMDocumentResponseWrapper(String SHA256Var, Id householdIdVar) {
            
            SHA256 = SHA256Var;
            duplicateDocumentYear = '';
            duplicateDocumentType = '';
            householdId = householdIdVar;
            isDuplicated = isDuplicated();
            requestFailed = false;
        }//End-Constructor
        
        private Boolean isDuplicated() {
            
            if( householdId != null && SHA256 != null ) {
                
                List<Family_Document__c> docResults = new List<Family_Document__c>([
                    SELECT Id, Document_Year__c, Document_Type__c 
                    FROM Family_Document__c 
                    WHERE Household__c =: householdId  
                    AND File_Hash__c =: SHA256 
                    AND File_Hash__c != null 
                    AND Deleted__c != true 
                    LIMIT 1]);
                    
                 if(!docResults.isEmpty()) {
                     duplicateDocumentYear = docResults[0].Document_Year__c;
                     duplicateDocumentType = docResults[0].Document_Type__c;
                 }
             return !docResults.isEmpty();
            }
            
            return false;
        }//End:isDuplicated
    }
    public class SpringCMApiException extends Exception {}
}