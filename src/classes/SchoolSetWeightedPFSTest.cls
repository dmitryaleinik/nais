@isTest
private class SchoolSetWeightedPFSTest
{

    private static final String PFS_STATUS_APPLICATION_SUBMITTED = 'Application Submitted';
    private static final String PFS_STATUS_APPLICATION_IN_PROGRESS = 'Application In Progress';
    private static final String PFS_PAYMENT_STATUS_PAID_IN_FULL = 'Paid in Full';
    private static final String PFS_PAYMENT_STATUS_UNPAID = 'Unpaid';
    private static final String PFS_GENERIC_NAME_TEST_PFS = 'Test PFS ';
    private static final String PFS_GENERIC_NAME_PFS_A = 'PFS A';
    private static final String SCHOOL_INITAL_NAME = 'school';
    private static final String EXCLUDED_SCHOOL_NAME = 'excludedSchool';
    private static final String SCHOOL1_INITAL_NAME = 'school1';
    private static final String SCHOOL2_INITAL_NAME = 'school2';
    private static final String PARENT_A_INITAL_NAME = 'Parent A';
    private static final String STUDENT_1_INITIAL_LASTNAME = 'Student 1';
    private static final String STUDENT_FOLDER_INITIAL_NAME = 'Student Folder';
    private static final String STUDENT_FOLDER_1_INITIAL_NAME = 'Student Folder 1';
    private static final String STUDENT_FOLDER_2_INITIAL_NAME = 'Student Folder 2';
    private static final String STRING_YES = 'Yes';
    private static final String STRING_NO = 'No';
    private static final String ACCOUNT_NAME_SECOND_ACCOUNT = 'Second Account';
    private static final String WEIGHTED_PFS_NEGATIVE_EXPECTATION = 'Expected no weighted pfss to be created yet';
    private static final String ACTIVE_SSS_STATUS = GlobalVariables.getActiveSSSStatusForTest();
    private static final Id SCHOOL_ACCOUNT_TYPE_ID = RecordTypes.schoolAccountTypeId;

    public class TestDataLocal {
        Account school1;
        Contact parentA, student1;
        Id academicYearId;
        Annual_Setting__c annualSetting1;
        PFS__c pfs1;
        String academicYearName;
        Set<Id> pfsIds;
        Set<Id> annualSettingIds;

        private TestDataLocal() {
            school1 = insertSchool(SCHOOL1_INITAL_NAME, SCHOOL_ACCOUNT_TYPE_ID, ACTIVE_SSS_STATUS, false, null);

            parentA = ContactTestData.Instance
                .forLastName(PARENT_A_INITAL_NAME)
                .forRecordTypeId(RecordTypes.parentContactTypeId).create();
            student1 = ContactTestData.Instance
                .forLastName(STUDENT_1_INITIAL_LASTNAME)
                .forRecordTypeId(RecordTypes.studentContactTypeId).create();
            insert new List<Contact> {parentA, student1};

            Integer numberOfAcademicYears = 6;
            AcademicYearTestData.Instance.insertAcademicYears(numberOfAcademicYears);
            academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

            annualSetting1 = AnnualSettingsTestData.Instance
                .forSchoolId(school1.Id)
                .forAcademicYearId(academicYearId).insertAnnualSettings();
            annualSettingIds = new Set<Id>{annualSetting1.Id};

            academicYearName = GlobalVariables.getAcademicYear(academicYearId).Name;

            pfs1 = createPfs(PFS_GENERIC_NAME_PFS_A, academicYearName, parentA.Id, PFS_PAYMENT_STATUS_PAID_IN_FULL);
            insert pfs1;

            pfsIds = new Set<Id>{pfs1.Id};
        }
    }

    public class TestDataLocalSpfsa {

        Applicant__c applicant1;
        Student_Folder__c studentFolder1;
        School_PFS_Assignment__c spfsa1;

        private TestDataLocalSpfsa(TestDataLocal td) {
            applicant1 = ApplicantTestData.Instance
                .forContactId(td.student1.Id)
                .forPfsId(td.pfs1.Id).insertApplicant();

            studentFolder1 = insertStudentFolder(STUDENT_FOLDER_1_INITIAL_NAME, td.academicYearName, td.student1.Id);

            // school pfs assignment
            spfsa1 = insertSpfsa(td.academicYearName, applicant1.Id, td.school1.Id, null, studentFolder1.Id);
        }
    }

    private static TestDataLocal setup() {
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);

        return new TestDataLocal();
    }

    
    @isTest
    private static void testWeightedPFS1_expectOneWeightedPfs() {
        TestDataLocal td = setup();
        TestDataLocalSpfsa tdSpfsa = new TestDataLocalSpfsa(td);

        Test.startTest();
            // verify that weighted pfs is not created
            System.AssertEquals(0, WeightedPfsSelector.Instance.selectWithCustomFieldListByPfsAndAnnSetIds(
                td.pfsIds, td.annualSettingIds, new List<String>{'Id'}).size());

            activateFakeContent();

            td.pfs1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            update td.pfs1;

            // weighted pfs is created for submitted and not fee waived pfs
            tdSpfsa.spfsa1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            update tdSpfsa.spfsa1;
        Test.stopTest();

        System.AssertEquals(1, WeightedPfsSelector.Instance.selectWithCustomFieldListByPfsAndAnnSetIds(
            td.pfsIds, td.annualSettingIds, new List<String>{'Id'}).size());
    }

    @isTest
    private static void testWeightedPfs1_expectOneAfterRecalc() {
        TestDataLocal td = setup();
        TestDataLocalSpfsa tdSpfsa = new TestDataLocalSpfsa(td);

        Test.startTest();
            // verify that weighted pfs is not created
            System.AssertEquals(0, WeightedPfsSelector.Instance.selectWithCustomFieldListByPfsAndAnnSetIds(
                td.pfsIds, td.annualSettingIds, new List<String>{'Id'}).size());

            activateFakeContent();

            td.pfs1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            update td.pfs1;

            // weighted pfs is created for submitted and not fee waived pfs
            tdSpfsa.spfsa1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            update tdSpfsa.spfsa1;

            activateFakeContent();

            // weighted pfs is created for submitted and not fee waived pfs
            tdSpfsa.spfsa1.PFS_Status__c = PFS_STATUS_APPLICATION_IN_PROGRESS;
            td.pfs1.PFS_Status__c = PFS_STATUS_APPLICATION_IN_PROGRESS;
            td.pfs1.Fee_Waived__c = STRING_NO;
            update td.pfs1;
            update tdSpfsa.spfsa1;

            activateFakeContent();

            // weighted pfs is not created for submitted and fee waived pfs
            td.pfs1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            td.pfs1.Fee_Waived__c = STRING_YES;
            update td.pfs1;
        Test.stopTest();

        System.AssertEquals(1, WeightedPfsSelector.Instance.selectWithCustomFieldListByPfsAndAnnSetIds(
            td.pfsIds, td.annualSettingIds, new List<String>{'Id'}).size());
    }

    @isTest
    private static void testWeightedPFS2_expectOne() {
        TestDataLocal td = setup();
        TestDataLocalSpfsa tdSpfsa = new TestDataLocalSpfsa(td);

        Test.startTest();
            // verify that weighted pfs is not created
            System.AssertEquals(0, WeightedPfsSelector.Instance.selectWithCustomFieldListByPfsAndAnnSetIds(
                td.pfsIds, td.annualSettingIds, new List<String>{'Id'}).size());

            tdSpfsa.spfsa1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;

            activateFakeContent();

            td.pfs1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            td.pfs1.Fee_Waived__c = STRING_NO;
            update td.pfs1;
            update tdSpfsa.spfsa1;
        Test.stopTest();

        System.AssertEquals(1, WeightedPfsSelector.Instance.selectWithCustomFieldListByPfsAndAnnSetIds(
            td.pfsIds, td.annualSettingIds, new List<String>{'Id'}).size());
    }

    @isTest
    private static void testWeightedPfs2_expectNoneAfterRecalc() {
        TestDataLocal td = setup();
        TestDataLocalSpfsa tdSpfsa = new TestDataLocalSpfsa(td);

        Test.startTest();
            // verify that weighted pfs is not created
            System.AssertEquals(0, WeightedPfsSelector.Instance.selectWithCustomFieldListByPfsAndAnnSetIds(
                td.pfsIds, td.annualSettingIds, new List<String>{'Id'}).size());

            tdSpfsa.spfsa1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;

            activateFakeContent();

            td.pfs1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            td.pfs1.Fee_Waived__c = STRING_NO;
            update td.pfs1;
            update tdSpfsa.spfsa1;

            // NAIS-2175 [CH] Adding check for removing Weighted PFS when PFS is unpaid
            SchoolSetWeightedPFS.isExecutingFromPFS = false;

            td.pfs1.Payment_Status__c = PFS_PAYMENT_STATUS_UNPAID;
            update td.pfs1;
        Test.stopTest();

        System.AssertEquals(0, WeightedPfsSelector.Instance.selectWithCustomFieldListByPfsAndAnnSetIds(
            td.pfsIds, td.annualSettingIds, new List<String>{'Id'}).size());
    }

    @isTest
    private static void testSchoolCount_expectTwo() {
        TestDataLocal td = setup();
        TestDataLocalSpfsa tdSpfsa = new TestDataLocalSpfsa(td);

        Account school2 = insertSchool(
            SCHOOL2_INITAL_NAME, SCHOOL_ACCOUNT_TYPE_ID, ACTIVE_SSS_STATUS, false, null);

        Annual_Setting__c annualSetting2 = AnnualSettingsTestData.Instance
            .forSchoolId(school2.Id)
            .forAcademicYearId(td.academicYearId).insertAnnualSettings();

        // fake new context
        SchoolSetWeightedPFS.isExecuting = false;
        
        School_PFS_Assignment__c spfsa2 = insertSpfsa(
            td.academicYearName, tdSpfsa.applicant1.Id, school2.Id, null, tdSpfsa.studentFolder1.Id);

        Test.startTest();
            // fake new context
            SchoolSetWeightedPFS.isExecuting = false;

            td.pfs1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            update td.pfs1;

            // weighted pfs is created for submitted and not fee waived pfs
            tdSpfsa.spfsa1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            update tdSpfsa.spfsa1;
        Test.stopTest();

        System.AssertEquals(2, WeightedPfsSelector.Instance.selectWithCustomFieldListByPfsAndAnnSetIds(
            td.pfsIds, td.annualSettingIds, new List<String>{'School_Count__c'})[0].School_Count__c);
    }

    @isTest
    private static void testSchoolCount_expectOneSchoolAfterRecalc() {
        TestDataLocal td = setup();
        TestDataLocalSpfsa tdSpfsa = new TestDataLocalSpfsa(td);

        Account school2 = insertSchool(
            SCHOOL2_INITAL_NAME, SCHOOL_ACCOUNT_TYPE_ID, ACTIVE_SSS_STATUS, false, null);

        Annual_Setting__c annualSetting2 = AnnualSettingsTestData.Instance
            .forSchoolId(school2.Id)
            .forAcademicYearId(td.academicYearId).insertAnnualSettings();

        // fake new context
        SchoolSetWeightedPFS.isExecuting = false;

        School_PFS_Assignment__c spfsa2 = insertSpfsa(
            td.academicYearName, tdSpfsa.applicant1.Id, school2.Id, null, tdSpfsa.studentFolder1.Id);

        Test.startTest();
            // fake new context
            SchoolSetWeightedPFS.isExecuting = false;

            td.pfs1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            update td.pfs1;

            // weighted pfs is created for submitted and not fee waived pfs
            tdSpfsa.spfsa1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            update tdSpfsa.spfsa1;

            // fake new context
            SchoolSetWeightedPFS.isExecuting = false;
            // delete spfsa2 and verify that weighted pfs record is deleted
            delete spfsa2;
        Test.stopTest();

        System.AssertEquals(1, WeightedPfsSelector.Instance.selectWithCustomFieldListByPfsAndAnnSetIds(
            td.pfsIds, td.annualSettingIds, new List<String>{'School_Count__c'})[0].School_Count__c);
        System.AssertEquals(0, WeightedPfsSelector.Instance.selectWithCustomFieldListByPfsAndAnnSetIds(
            td.pfsIds, new Set<Id>{annualSetting2.Id}, new List<String>{'Id'}).size());
    }

    @isTest
    private static void testWPFSandTotalPFSCalculations() {
        TestDataLocal td = new TestDataLocal();

        Account secondAccount = insertSchool(
            ACCOUNT_NAME_SECOND_ACCOUNT, RecordTypes.accessOrgAccountTypeId, ACTIVE_SSS_STATUS, false, 1);

        // Add an Annual Setting for School 2
        Annual_Setting__c annualSetting2 = AnnualSettingsTestData.Instance
            .forSchoolId(secondAccount.Id)
            .forAcademicYearId(td.academicYearId).insertAnnualSettings();

        // Create 10 pfs records
        List<PFS__c> pfsRecordsToInsert = new List<PFS__c>{};
        for (Integer i=0; i<10; i++) {
            pfsRecordsToInsert.add(
                createPfs(PFS_GENERIC_NAME_TEST_PFS + i, td.academicYearName, td.parentA.Id, PFS_PAYMENT_STATUS_PAID_IN_FULL));
        }
        insert pfsRecordsToInsert;

        // Create one Applicant per PFS
        List<Applicant__c> applicantsToInsert = new List<Applicant__c>{};
        for(PFS__c pfsRecord : pfsRecordsToInsert){
            applicantsToInsert.add(
                ApplicantTestData.Instance
                    .forContactId(td.student1.Id)
                    .forPfsId(pfsRecord.Id).create());
        }
        insert applicantsToInsert;

        // Create a student folder for each applicant
        List<Student_Folder__c> foldersToInsert = new List<Student_Folder__c>{};
        for(Applicant__c appRecord : applicantsToInsert){
            foldersToInsert.add(
                createStudentFolder(STUDENT_FOLDER_INITIAL_NAME, td.academicYearName, td.student1.Id));
        }
        insert foldersToInsert;

        // Connect the first 5 PFS records to the first school
        List<School_PFS_Assignment__c> spaRecordsToInsert = new List<School_PFS_Assignment__c>{};
        for(Integer i=0; i<5; i++){
            spaRecordsToInsert.add(
                createSpfsa(td.academicYearName, applicantsToInsert[i].Id, td.school1.Id, null, foldersToInsert[i].Id));
        }

        // Connect the last 5 PFS records to both schools
        for(Integer i=5; i<10; i++){
            spaRecordsToInsert.add(
                createSpfsa(td.academicYearName, applicantsToInsert[i].Id, td.school1.Id, null, foldersToInsert[i].Id));
            spaRecordsToInsert.add(
                createSpfsa(td.academicYearName, applicantsToInsert[i].Id, secondAccount.Id, null, foldersToInsert[i].Id));
        }
        insert spaRecordsToInsert;

        Test.startTest();
            SchoolSetWeightedPFS.isExecuting = false;

            // Update the PFS records to be Application Submitted
            for(PFS__c pfsRecord : pfsRecordsToInsert){
                pfsRecord.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            }

            update pfsRecordsToInsert;
        Test.stopTest();

        List<Weighted_PFS__c> weightedPFSRecords = WeightedPfsSelector.Instance.selectAllWeightedPfs();

        // Check the weighted PFS and Total PFS numbers
        List<String> fieldsToSelect = new List<String>{'wPFS_Count__c','Total_PFS_Count__c'};

        td.annualSetting1 = AnnualSettingsSelector.newInstance().selectWithCustomFieldListById(
            new Set<Id>{td.annualSetting1.Id}, fieldsToSelect)[0];
        System.AssertEquals(7.50, td.annualSetting1.wPFS_Count__c);
        System.AssertEquals(10, td.annualSetting1.Total_PFS_Count__c);

        annualSetting2 = AnnualSettingsSelector.newInstance().selectWithCustomFieldListById(
            new Set<Id>{annualSetting2.Id}, fieldsToSelect)[0];
        System.AssertEquals(2.50, annualSetting2.wPFS_Count__c);
        System.AssertEquals(5, annualSetting2.Total_PFS_Count__c);
    }

    @isTest
    private static void testWeightedPFSBulk1a() {
        TestDataLocal td = setup();

        Integer schCount = 50;
        Integer pfsCount = 5;
        Integer spfsaCount = 200;

        SpfsaTestDataLocal spfsaTd = createSpsfaList(td, schCount, pfsCount, spfsaCount);
        List<PFS__c> pfsList = spfsaTd.pfsList;
        List<Annual_Setting__c> annSettList = spfsaTd.annSettList;
        List<School_PFS_Assignment__c> spfsaList = spfsaTd.spfsaList;

        Test.startTest();

            for(PFS__c pfsRecord : pfsList){
                pfsRecord.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            }
            update pfsList;

            // fake new context
            SchoolSetWeightedPFS.isExecuting = false;

            // weighted pfs is created for submitted and not fee waived pfs
            for (Integer i=0; i<spfsaCount; i++) {
                spfsaList[i].PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            }
            update spfsaList;
        Test.stopTest();

        // expected 10. you have 10 schools in this method
        System.AssertEquals(10, WeightedPfsSelector.Instance
            .selectWithCustomFieldListByPfsAndAnnSetIds(
                new Set<Id>{pfsList[0].Id}, new Set<Id>{annSettList[0].Id}, new List<String>{'School_Count__c'})[0].School_Count__c);
    }

    @isTest
    private static void testWeightedPFSBulk1b() {
        TestDataLocal td = setup();

        Integer schCount = 50;
        Integer pfsCount = 5;
        Integer spfsaCount = 200;

        SpfsaTestDataLocal spfsaTd = createSpsfaList(td, schCount, pfsCount, spfsaCount);
        List<PFS__c> pfsList = spfsaTd.pfsList;
        List<Annual_Setting__c> annSettList = spfsaTd.annSettList;
        List<School_PFS_Assignment__c> spfsaList = spfsaTd.spfsaList;

        Test.startTest();

            for(PFS__c pfsRecord : pfsList){
                pfsRecord.Fee_Waived__c = PFS_STATUS_APPLICATION_SUBMITTED;
            }
            update pfsList;

            // fake new context
            SchoolSetWeightedPFS.isExecuting = false;

            // weighted pfs is created for submitted and not fee waived pfs
            for (Integer i=0; i<spfsaCount; i++) {
                spfsaList[i].PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            }
            update spfsaList;
        Test.stopTest();

        System.AssertEquals(0, WeightedPfsSelector.Instance
            .selectWithCustomFieldListByPfsAndAnnSetIds(
                new Set<Id>{pfsList[0].Id}, new Set<Id>{annSettList[0].Id}, new List<String>{'Id'}).size());
    }

    @isTest
    private static void testWeightedPFSBulk2() {
        TestDataLocal td = setup();

        Integer schCount = 50;
        Integer pfsCount = 5;
        Integer spfsaCount = 200;

        SpfsaTestDataLocal spfsaTd = createSpsfaList(td, schCount, pfsCount, spfsaCount);
        List<PFS__c> pfsList = spfsaTd.pfsList;
        List<Annual_Setting__c> annSettList = spfsaTd.annSettList;
        List<School_PFS_Assignment__c> spfsaList = spfsaTd.spfsaList;

        Test.startTest();
            for(PFS__c pfsRecord : pfsList){
                pfsRecord.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            }
            update pfsList;

            // fake new context
            SchoolSetWeightedPFS.isExecuting = false;
            // weighted pfs is created for submitted and not fee waived pfs
            for (Integer i=0; i<spfsaCount; i++) {
                spfsaList[i].PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            }
            update spfsaList;
        Test.stopTest();

        // expected 10. you have 10 schools in this method
        System.AssertEquals(10, WeightedPfsSelector.Instance
            .selectWithCustomFieldListByPfsAndAnnSetIds(
                new Set<Id>{pfsList[0].Id}, new Set<Id>{annSettList[0].Id}, new List<String>{'School_Count__c'})[0].School_Count__c);
    }

    @isTest
    private static void testWeightedPFSBulk_expectNoneAfterRecalc()
    {
        TestDataLocal td = setup();

        // 5 pfs, 50 schools, each pfs refers to 10 schools
        Integer schCount = 50;
        Integer pfsCount = 5;
        Integer spfsaCount = 200;

        SpfsaTestDataLocal spfsaTd = createSpsfaList(td, schCount, pfsCount, spfsaCount);
        List<PFS__c> pfsList = spfsaTd.pfsList;
        List<Annual_Setting__c> annSettList = spfsaTd.annSettList;
        List<School_PFS_Assignment__c> spfsaList = spfsaTd.spfsaList;

        Test.startTest();
            for(PFS__c pfsRecord : pfsList)
            {
                pfsRecord.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
                pfsRecord.Fee_Waived__c = 'No';
            }
            update pfsList;

            // fake new context
            SchoolSetWeightedPFS.isExecuting = false;
            // weighted pfs is created for submitted and not fee waived pfs
            for (Integer i=0; i<spfsaCount; i++)
            {
                spfsaList[i].PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            }
            update spfsaList;

            // fake new context
            SchoolSetWeightedPFS.isExecuting = false;
            // weighted pfs is deleted when spfsa deleted
            List <School_PFS_Assignment__c> spfsaDeleteList = new List <School_PFS_Assignment__c>();
            for (Integer i=0; i<10; i++)
            {
                spfsaDeleteList.add(spfsaList[i]);
            }
            delete spfsaDeleteList;
        Test.stopTest();

        System.AssertEquals(0, WeightedPfsSelector.Instance.selectWithCustomFieldListByPfsAndAnnSetIds(
            new Set<Id>{pfsList[0].Id}, new Set<Id>{annSettList[0].Id}, new List<String>{'Id'}).size());
    }

    //NAIS-2402 [DP] 04.28.2015
    @isTest
    private static void testForceRecalcCheckbox() {
        TestDataLocal td = setup();
        TestDataLocalSpfsa tdSpfsa = new TestDataLocalSpfsa(td);

        // verify that weighted pfs is not created
        System.AssertEquals(0, WeightedPfsSelector.Instance
            .selectWithCustomFieldListByPfsAndAnnSetIds(
                new Set<Id>{td.pfs1.Id}, new Set<Id>{td.annualSetting1.Id}, new List<String>{'Id'}).size());

        Test.startTest();
            activateFakeContent();

            td.pfs1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            update td.pfs1;
        Test.stopTest();

        System.AssertEquals(1, WeightedPfsSelector.Instance
            .selectWithCustomFieldListByPfsAndAnnSetIds(
                new Set<Id>{td.pfs1.Id}, new Set<Id>{td.annualSetting1.Id}, new List<String>{'Id'}).size());
    }

    @isTest
    private static void testForceRecalcCheckbox_expectOneWeightedPfs() {
        TestDataLocal td = setup();
        TestDataLocalSpfsa tdSpfsa = new TestDataLocalSpfsa(td);

        // verify that weighted pfs is not created
        System.AssertEquals(0, WeightedPfsSelector.Instance
            .selectWithCustomFieldListByPfsAndAnnSetIds(
                new Set<Id>{td.pfs1.Id}, new Set<Id>{td.annualSetting1.Id}, new List<String>{'Id'}).size());

        Test.startTest();
            activateFakeContent();

            td.pfs1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            td.pfs1.SYSTEM_WPFS_Recalc__c = true;
            update td.pfs1;
        Test.stopTest();

        System.AssertEquals(1, WeightedPfsSelector.Instance
            .selectWithCustomFieldListByPfsAndAnnSetIds(
                new Set<Id>{td.pfs1.Id}, new Set<Id>{td.annualSetting1.Id}, new List<String>{'Id'}).size());
        System.Assert(
            !PfsSelector.Instance.selectWithCustomFieldListById(
                new Set<Id>{td.pfs1.Id}, new List<String>{'SYSTEM_WPFS_Recalc__c'})[0].SYSTEM_WPFS_Recalc__c,
            'Expected SYSTEM_WPFS_Recalc__c to uncheck after calculation re-ran.');
    }

    @isTest
    private static void testWeightedPFSNoAnnualSetting() {
        TestDataLocal td = setup();
        TestDataLocalSpfsa tdSpfsa = new TestDataLocalSpfsa(td);

        List<Annual_Setting__c> annSetList = AnnualSettingsSelector.newInstance().selectAllAnnualSettings();
        System.AssertEquals(1, annSetList.size());

        delete annSetList;

        Test.startTest();
            activateFakeContent();

            td.pfs1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            update td.pfs1;
        Test.stopTest();

        annSetList = AnnualSettingsSelector.newInstance()
            .selectWithCustomFieldList(new List<String>{'Total_PFS_Count__c', 'WPFS_Count__c'});
        System.AssertEquals(1, annSetList.size());
        System.AssertEquals(1, annSetList[0].Total_PFS_Count__c);
        System.AssertEquals(1, annSetList[0].WPFS_Count__c);
    }

    @isTest
    private static void setWeightedPfs_withdrawnSpa_expectWeightedPfs() {
        TestDataLocal td = setup();
        TestDataLocalSpfsa tdSpfsa = new TestDataLocalSpfsa(td);

        System.AssertEquals(0, WeightedPfsSelector.Instance
            .selectWithCustomFieldListByPfsAndAnnSetIds(
                new Set<Id>{td.pfs1.Id}, new Set<Id>{td.annualSetting1.Id}, new List<String>{'Id'}).size(),
            WEIGHTED_PFS_NEGATIVE_EXPECTATION);

        Test.startTest();
            activateFakeContent();

            td.pfs1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            update td.pfs1;

            tdSpfsa.spfsa1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            tdSpfsa.spfsa1.Withdrawn__c = STRING_YES;

            update tdSpfsa.spfsa1;
        Test.stopTest();

        System.AssertEquals(1, WeightedPfsSelector.Instance
            .selectWithCustomFieldListByPfsAndAnnSetIds(
                new Set<Id>{td.pfs1.Id}, new Set<Id>{td.annualSetting1.Id}, new List<String>{'Id'}).size(),
            'Expected weighted pfss to be created even when spa is withdrawn.');
    }

    @isTest
    private static void setWeightedPfs_excludedSchool_expectNoWeightedPfs() {
        TestDataLocal td = setup();
        TestDataLocalSpfsa tdSpfsa = new TestDataLocalSpfsa(td);

        System.AssertEquals(0, WeightedPfsSelector.Instance
            .selectWithCustomFieldListByPfsAndAnnSetIds(
                new Set<Id>{td.pfs1.Id}, new Set<Id>{td.annualSetting1.Id}, new List<String>{'Id'}).size(),
            WEIGHTED_PFS_NEGATIVE_EXPECTATION);

        activateFakeContent();

        // Set system exclude for the school so that no weighted PFSs are created.
        td.school1.System_Exclude__c = true;
        update td.school1;

        td.pfs1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
        update td.pfs1;

        tdSpfsa.spfsa1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
        update tdSpfsa.spfsa1;

        System.AssertEquals(0, WeightedPfsSelector.Instance.selectWithCustomFieldListByPfsAndAnnSetIds(
                td.pfsIds, td.annualSettingIds, new List<String>{'Id'}).size(),
            'Expected no weighted pfss to be created when the school is excluded from the calculation.');
    }

    @isTest
    private static void setWeightedPfs_excludedAndNonExcludedSchool_expectOneWeightedPfs() {
        TestDataLocal td = setup();

        // school
        Account excludedSchool =
            insertSchool(EXCLUDED_SCHOOL_NAME, SCHOOL_ACCOUNT_TYPE_ID, ACTIVE_SSS_STATUS, true, null);

        System.AssertEquals(0, WeightedPfsSelector.Instance
            .selectWithCustomFieldListByPfsAndAnnSetIds(
                new Set<Id>{td.pfs1.Id}, new Set<Id>{td.annualSetting1.Id}, new List<String>{'Id'}).size(),
            WEIGHTED_PFS_NEGATIVE_EXPECTATION);

        activateFakeContent();

        Test.startTest();
            createAnotherSpaAndMarkSpasSubmitted(td, excludedSchool.Id);
        Test.stopTest();

        List<Weighted_PFS__c> weightedPfss = WeightedPfsSelector.Instance
            .selectWithCustomFieldListByPfsAndAnnSetIds(
                new Set<Id>{td.pfs1.Id}, 
                new Set<Id>{td.annualSetting1.Id}, 
                new List<String>{'Annual_Setting__r.School__c', 'Weighted_School_Count__c'});

        System.AssertEquals(1, weightedPfss.size(),
            'Expected only 1 weighted pfss to be created when an excluded and nonexcluded school is used for the calculation.');
        System.AssertEquals(td.school1.Id, weightedPfss[0].Annual_Setting__r.School__c,
            'Expected the weighted PFS to be for the school that was not excluded.');
        System.AssertEquals(1, weightedPfss[0].Weighted_School_Count__c,
            'Expected weighted school count to 1 because the excluded school is not taken into account.');
    }

    @isTest
    private static void setWeightedPfs_schoolWithWpfsMarkedAsExcluded_expectWeightedPfsDeleted() {
        TestDataLocal td = setup();

        // School will be marked as excluded later in the test
        Account excludedSchool = 
            insertSchool(EXCLUDED_SCHOOL_NAME, SCHOOL_ACCOUNT_TYPE_ID, ACTIVE_SSS_STATUS, true, null);

        System.AssertEquals(0, WeightedPfsSelector.Instance
            .selectWithCustomFieldListByPfsAndAnnSetIds(
                new Set<Id>{td.pfs1.Id}, new Set<Id>{td.annualSetting1.Id}, new List<String>{'Id'}).size(),
            WEIGHTED_PFS_NEGATIVE_EXPECTATION);

        activateFakeContent();

        Test.startTest();
            createAnotherSpaAndMarkSpasSubmitted(td, excludedSchool.Id);

            activateFakeContent();

            excludedSchool.System_Exclude__c = true;
            update excludedSchool;

            td.pfs1.SYSTEM_WPFS_Recalc__c = true;
            update td.pfs1;
        Test.stopTest();

        List<Weighted_PFS__c> weightedPfsesAfterExclude = WeightedPfsSelector.Instance
            .selectWithCustomFieldListByPfsId(new Set<Id>{td.pfs1.Id}, new List<String>{'School_Count__c'});

        System.AssertEquals(1, weightedPfsesAfterExclude.size(),
            'Expected weighted pfs for excluded school to be deleted.');
        System.AssertEquals(1, weightedPfsesAfterExclude[0].School_Count__c,
            'Expected weighted pfs for excluded school to be deleted updating the school count to 1.');
    }

    @isTest
    private static void setWeightedPfs_schoolWithWpfsMarkedAsExcluded_expectTwoWeightedPfs() {
        TestDataLocal td = setup();

        Account excludedSchool = 
            insertSchool(EXCLUDED_SCHOOL_NAME, SCHOOL_ACCOUNT_TYPE_ID, ACTIVE_SSS_STATUS, false, null);

        System.AssertEquals(0, WeightedPfsSelector.Instance.selectWithCustomFieldListByPfsAndAnnSetIds(
                td.pfsIds, td.annualSettingIds, new List<String>{'Id'}).size(),
            WEIGHTED_PFS_NEGATIVE_EXPECTATION);

        activateFakeContent();

        Test.startTest();
            createAnotherSpaAndMarkSpasSubmitted(td, excludedSchool.Id);
        Test.stopTest();

        List<Weighted_PFS__c> weightedPfses = WeightedPfsSelector.Instance
            .selectWithCustomFieldListByPfsId(new Set<Id>{td.pfs1.Id}, new List<String>{'School_Count__c'});

        System.AssertEquals(2, weightedPfses.size(),
            'Expected two weighted pfss to be created when both schools are not excluded from the calculation.');
        System.AssertEquals(2, weightedPfses[0].School_Count__c,
            'Expected weighted school count to .5 because the both schools are not exluded.');
    }

    @isTest
    private static void setWeightedPfs_oneWpfsToExcludedSchool_expectOneWeightedPfs() {
        TestDataLocal td = setup();
        TestDataLocalSpfsa tdSpfsa = new TestDataLocalSpfsa(td);

        System.AssertEquals(0, WeightedPfsSelector.Instance.selectWithCustomFieldListByPfsAndAnnSetIds(
                td.pfsIds, td.annualSettingIds, new List<String>{'Id'}).size(),
            WEIGHTED_PFS_NEGATIVE_EXPECTATION);

        activateFakeContent();

        Test.startTest();
            td.pfs1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            update td.pfs1;

            tdSpfsa.spfsa1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
            update tdSpfsa.spfsa1;
        Test.stopTest();

        List<Weighted_PFS__c> weightedPfses = WeightedPfsSelector.Instance
            .selectWithCustomFieldListByPfsId(new Set<Id>{td.pfs1.Id}, new List<String>{'School_Count__c'});

        System.AssertEquals(1, weightedPfses.size(),
            'Expected two weighted pfss to be created when both schools are not excluded from the calculation.');
        System.AssertEquals(1, weightedPfses[0].School_Count__c,
            'Expected weighted school count to .5 because the both schools are not excluded.');
    }

    @isTest
    private static void setWeightedPfs_oneWpfsToExcludedSchool_expectWeightedPfsDeleted() {
        TestDataLocal td = setup();

        System.AssertEquals(0, WeightedPfsSelector.Instance.selectWithCustomFieldListByPfsAndAnnSetIds(
                td.pfsIds, td.annualSettingIds, new List<String>{'Id'}).size(),            
            WEIGHTED_PFS_NEGATIVE_EXPECTATION);

        activateFakeContent();

        Test.startTest();
            td.school1.System_Exclude__c = true;
            update td.school1;

            td.pfs1.SYSTEM_WPFS_Recalc__c = true;
            update td.pfs1;
        Test.stopTest();

        System.AssertEquals(0, WeightedPfsSelector.Instance.selectWithCustomFieldListByPfsId(
                td.pfsIds, new List<String>{'School_Count__c'}).size(),
            'Expected weighted pfs for excluded school to be deleted.');
    }

    private static void createAnotherSpaAndMarkSpasSubmitted(TestDataLocal td, Id accountIdForSpa) {
        TestDataLocalSpfsa tdSpfsa = new TestDataLocalSpfsa(td);

        Student_Folder__c studentFolder2 = insertStudentFolder(STUDENT_FOLDER_2_INITIAL_NAME, td.academicYearName, td.student1.Id);

        School_PFS_Assignment__c spfsa2 = insertSpfsa(
            td.academicYearName, tdSpfsa.applicant1.Id, accountIdForSpa, PFS_STATUS_APPLICATION_SUBMITTED, studentFolder2.Id);

        td.pfs1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
        update td.pfs1;

        tdSpfsa.spfsa1.PFS_Status__c = PFS_STATUS_APPLICATION_SUBMITTED;
        update tdSpfsa.spfsa1;
    }

    private static Account insertSchool(
        String schoolName, Id recordtypeId, String sSSSubscriberStatus, Boolean forSystemExclude, Integer maxNumberOfUsers) {

        Account school = createSchool(schoolName, recordtypeId, sSSSubscriberStatus, forSystemExclude, maxNumberOfUsers);
        
        insert school;
        
        return school;
    }

    private static Account createSchool(
        String schoolName, Id recordtypeId, String sSSSubscriberStatus, Boolean forSystemExclude, Integer maxNumberOfUsers) {
        
        return AccountTestData.Instance
            .forName(schoolName)
            .forRecordTypeId(recordtypeId)
            .forSSSSubscriberStatus(sSSSubscriberStatus)
            .forSystemExclude(forSystemExclude)
            .forMaxNumberOfUsers(maxNumberOfUsers).create();
    }

    private static School_PFS_Assignment__c insertSpfsa(
        String academicYearName, Id applicantId, Id schoolId, String pfsStatus, Id studentFolderId) {

        School_PFS_Assignment__c spfsa = createSpfsa(academicYearName, applicantId, schoolId, pfsStatus, studentFolderId);

        insert spfsa;

        return spfsa;
    }

    private static School_PFS_Assignment__c createSpfsa(
        String academicYearName, Id applicantId, Id schoolId, String pfsStatus, Id studentFolderId) {

       return SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYearName)
            .forApplicantId(applicantId)
            .forSchoolId(schoolId)
            .forPfsStatus(pfsStatus)
            .forStudentFolderId(studentFolderId).create();
    }

    private static PFS__c createPfs(String pfsName, String academicYearName, Id parentAId, String pfsPaymentStatus) {
        return PfsTestData.Instance
            .forName(pfsName)
            .forAcademicYearPicklist(academicYearName)
            .forParentA(parentAId)
            .forPaymentStatus(pfsPaymentStatus).create();
    }

    private static Student_Folder__c insertStudentFolder(String folderName, String academicYearName, Id studentId) {
        Student_Folder__c studentFolder = createStudentFolder(folderName, academicYearName, studentId);

        insert studentFolder;

        return studentFolder;
    }

    private static Student_Folder__c createStudentFolder(String folderName, String academicYearName, Id studentId) {
        return StudentFolderTestData.Instance
            .forName(folderName)
            .forAcademicYearPicklist(academicYearName)
            .forStudentId(studentId).create();
    }

    private static void activateFakeContent() {
        SchoolSetWeightedPFS.isExecuting = false;
        SchoolSetWeightedPFS.isExecutingFromPFS = false;
    }

    private static SpfsaTestDataLocal createSpsfaList(
        TestDataLocal td, Integer schCount, Integer pfsCount, Integer spfsaCount) {
        
        List<Account> schList = new List<Account>();
        for (Integer i=0; i<schCount; i++) {
            schList.add(
                createSchool(SCHOOL_INITAL_NAME+i, SCHOOL_ACCOUNT_TYPE_ID, ACTIVE_SSS_STATUS, false, null));
        }
        insert schList;

        List<Annual_Setting__c> annSettList = new List<Annual_Setting__c>();
        for (Integer i=0; i<schCount; i++) {
            annSettList.add(
                AnnualSettingsTestData.Instance
                    .forSchoolId(schList[i].Id)
                    .forAcademicYearId(td.academicYearId).create());
        }
        insert annSettList;

        List<PFS__c> pfsList = new List<PFS__c>();
        for (Integer i=0; i<pfsCount; i++) {
            pfsList.add(
                createPfs(PFS_GENERIC_NAME_PFS_A + i, td.academicYearName, td.parentA.Id, PFS_PAYMENT_STATUS_PAID_IN_FULL));
        }
        insert pfsList;

        List<Applicant__c> appList = new List<Applicant__c>();
        for (Integer i=0; i<spfsaCount; i++) {
            Integer pfsInt = (Integer)Math.floor(i/(spfsaCount/pfsCount));
            appList.add(
                ApplicantTestData.Instance
                    .forContactId(td.student1.Id)
                    .forPfsId(pfsList[pfsInt].Id).create());
        }
        insert appList;

        // fake new context
        SchoolSetWeightedPFS.isExecuting = false;

        Student_Folder__c studentFolder1 = insertStudentFolder(STUDENT_FOLDER_1_INITIAL_NAME, td.academicYearName, td.student1.Id);

        List<School_PFS_Assignment__c> spfsaList = new List<School_PFS_Assignment__c>();
        for (Integer i=0; i<spfsaCount; i++) {
            Integer schIndx = (Integer)Math.floor(i/(spfsaCount/schCount));
            spfsaList.add(
                createSpfsa(td.academicYearName, appList[i].Id, schList[schIndx].Id, null, studentFolder1.Id));
        }
        insert spfsaList;

        SpfsaTestDataLocal spfsaTd = new SpfsaTestDataLocal(pfsList, annSettList, spfsaList);

        return spfsaTd;
    }
    
    public class SpfsaTestDataLocal {
        List<PFS__c> pfsList;
        List<Annual_Setting__c> annSettList;
        List<School_PFS_Assignment__c> spfsaList;

        private SpfsaTestDataLocal(
            List<PFS__c> pfss, List<Annual_Setting__c> annSetts, List<School_PFS_Assignment__c> spfsas) {
            pfsList = pfss;
            annSettList = annSetts;
            spfsaList = spfsas;
        }
    }
}