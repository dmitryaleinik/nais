/**
 * @description This class is used to create Retirement Allowance records for unit tests.
 */
@isTest
public class RetirementAllowanceTestData extends SObjectTestData {
    @testVisible private static final Decimal AGE_LOW = 21;
    @testVisible private static final Decimal AGE_HIGH = 33;
    @testVisible private static final Decimal CONVERSION_COEFFICIENT_TWO_PARENT_FAM = 10;
    @testVisible private static final Decimal ALLOWANCE_TWO_PARENT_FAMILY = 15;
    @testVisible private static final Decimal CONVERSION_COEFFICIENT_ONE_PARENT_FAM = 5;
    @testVisible private static final Decimal ALLOWANCE_ONE_PARENT_FAMILY = 18;

    /**
     * @description Get the default values for the Retirement_Allowance__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Retirement_Allowance__c.Academic_Year__c => AcademicYearTestData.Instance.DefaultAcademicYear.Id,
                Retirement_Allowance__c.Age_Low__c => AGE_LOW,
                Retirement_Allowance__c.Age_High__c => AGE_HIGH,
                Retirement_Allowance__c.Conversion_Coefficient_Two_Parent_Fam__c => CONVERSION_COEFFICIENT_TWO_PARENT_FAM,
                Retirement_Allowance__c.Allowance_Two_Parent_Family__c => ALLOWANCE_TWO_PARENT_FAMILY,
                Retirement_Allowance__c.Conversion_Coefficient_One_Parent_Fam__c => CONVERSION_COEFFICIENT_ONE_PARENT_FAM,
                Retirement_Allowance__c.Allowance_One_Parent_Family__c => ALLOWANCE_ONE_PARENT_FAMILY
        };
    }

    /**
     * @description Sets the Academic Year for the current Retirement Allowance record.
     * @param academicYearId The Id of the AcademicYear to set on the current Retirement
     *             Allowance record.
     * @return The current working instance of RetirementAllowanceTestData.
     */
    public RetirementAllowanceTestData forAcademicYearId(Id academicYearId) {
        return (RetirementAllowanceTestData) with(Retirement_Allowance__c.Academic_Year__c, academicYearId);
    }

    /**
     * @description Insert the current working Retirement_Allowance__c record.
     * @return The currently operated upon Retirement_Allowance__c record.
     */
    public Retirement_Allowance__c insertRetirementAllowance() {
        return (Retirement_Allowance__c)insertRecord();
    }

    /**
     * @description Create the current working Retirement Allowance record without resetting
     *             the stored values in this instance of RetirementAllowanceTestData.
     * @return A non-inserted Retirement_Allowance__c record using the currently stored field
     *             values.
     */
    public Retirement_Allowance__c createRetirementAllowanceWithoutReset() {
        return (Retirement_Allowance__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Retirement_Allowance__c record.
     * @return The currently operated upon Retirement_Allowance__c record.
     */
    public Retirement_Allowance__c create() {
        return (Retirement_Allowance__c)super.buildWithReset();
    }

    /**
     * @description The default Retirement_Allowance__c record.
     */
    public Retirement_Allowance__c DefaultRetirementAllowance {
        get {
            if (DefaultRetirementAllowance == null) {
                DefaultRetirementAllowance = createRetirementAllowanceWithoutReset();
                insert DefaultRetirementAllowance;
            }
            return DefaultRetirementAllowance;
        }
        private set;
    }

    /**
     * @description Get the Retirement_Allowance__c SObjectType.
     * @return The Retirement_Allowance__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Retirement_Allowance__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static RetirementAllowanceTestData Instance {
        get {
            if (Instance == null) {
                Instance = new RetirementAllowanceTestData();
            }
            return Instance;
        }
        private set;
    }

    private RetirementAllowanceTestData() { }
}