@isTest
private class SchoolRenewalControllerTest {

    public static Account testAccount;
    public static Opportunity testOpp;
    public static User testUser;
    public static Academic_Year__c testAcademicYear;

    /***Create test data to use throughout this test class***/
    private static void init() {
        Account testAcc = testUtils.createAccount('Test Account', RecordTypes.schoolAccountTypeId, 1, true);

        Subscription__c testSubscription = testUtils.createSubscription(testAcc.Id, system.today()-14, system.today(), 'No', 'Basic', true);

        Contact testContact = testUtils.createContact('Test Contact', testAcc.Id, RecordTypes.schoolStaffContactTypeId, true);

        Subscription_Settings__c sSettings = new Subscription_Settings__c(); //Not included in testUtils class.
        sSettings.Name = 'Subscription Settings';
        sSettings.Start_Month__c = 10;
        sSettings.Start_Day__c = 1;
        sSettings.End_Month__c = 9;
        sSettings.End_Day__c = 30;
        insert sSettings;

        //Query for Account to get updated field values (from Subscription__c)
        testAccount = [
                SELECT
                        Name,
                        SSS_School_Code__c,
                        SSS_Subscription_Type_Current__c,
                        Paid_Through__c
                FROM Account
                WHERE Id =: testAcc.Id
        ];

        testAcademicYear = testUtils.createAcademicYear(
                testAccount.Paid_Through__c.addYears(1).year() + '-' + testAccount.Paid_Through__c.addYears(2).year(),
                false
        );
        testAcademicYear.Start_Date__c = system.today();
        testAcademicYear.End_Date__c = system.today().addYears(1);
        insert testAcademicYear;

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        //Insert the Portal User as the current system administrator to avoid Mixed DML Errors
        system.runAs(thisUser) {
            testUser = testUtils.createPortalUser('Test Portal User', 'port@l.user', 'test', testContact.Id, GlobalVariables.schoolPortalBasicAdminProfileId, true, false);
            testUser.TimeZoneSidKey = UserInfo.getTimeZone().getID();
            insert testUser;
        }
        System.debug('here');

        Profile schoolAdminProfile = [SELECT Name FROM Profile WHERE Id = :testUser.ProfileId][0];
        Profile_Type_Grouping__c ptg = new Profile_Type_Grouping__c();
        ptg.Name = schoolAdminProfile.Name;
        ptg.Profile_Name__c = schoolAdminProfile.Name;
        ptg.Is_School_Admin_Profile__c = true;
        ptg.Is_Call_Center_Profile__c = false;
        ptg.Is_Sys_Admin__c = true;
        ptg.Is_School_Profile__c = true;
        insert ptg;

        GlobalVariables.populateProfileIds();

        //Insert the PFS and Opportunity as the Portal User so that it is owned by that user.
        system.runAs(testUser) {
            PFS__c testPFS = testUtils.createPFS('Test PFS', testAcademicYear.Id, testContact.Id, true);

            testOpp = testUtils.createOpportunity('Test Opp', RecordTypes.opportunitySubscriptionFeeTypeId, testPFS.Id, testAcademicYear.Name, false);
            testOpp.AccountId = testAccount.Id;
            testOpp.Amount = 500;
            testOpp.CloseDate = date.newInstance(2015, 1, 1);
            testOpp.Subscription_Discount__c = 100;
            testOpp.SYSTEM_Suppress_Auto_Sale_TLI_Insert__c = true;
            testOpp.StageName = 'Open';
            insert testOpp;
        }
    }

    /***Test Constructor for when an Opportinty Id is passed into the page***/
    @isTest   
    private static void SchoolRenewalController_Test_Constructor_currentOpp_NotNull() {
        init();

        //Run as the portal user who created the Opp
        system.runAs(testUser) {

            test.StartTest();

            PageReference myPage = Page.SchoolRenewal;
            Test.setCurrentPage(myPage);
            Apexpages.currentPage().getParameters().put('Id',testOpp.Id);
            SchoolRenewalController controller = new SchoolRenewalController ();

            /***For testing Interface methods***/
            controller.academicYearId = 'Test';
            SchoolRenewalController thisControllerTest = controller.thisController;
            String getAcademicYearIdTest = controller.getAcademicYearId();
            PageReference SchoolAcademicYearSelector_OnChangeTest = controller.SchoolAcademicYearSelector_OnChange(true);

            test.stopTest();

            List<Apexpages.Message> msgs = ApexPages.getMessages();
            boolean b = false;

            for(Apexpages.Message msg:msgs){
                if (msg.getDetail().contains('There was an error retrieving the opportunity.')
                    || msg.getDetail().contains('There was an error retrieving the correct academic year.')
                ) {
                    b = true;
                }
            }
            system.assert(!b);

            system.assertEquals(testAccount.Id,controller.currentSchool.Id);
            system.assertEquals(testOpp.Id,controller.currentOpp.Id);
            system.assertEquals(null,controller.academicYear);
            system.assertNotEquals(null,controller.model);

            /***Test Interface methods***/
            system.assertEquals(thisControllerTest,controller);
            system.assertNotEquals(null,getAcademicYearIdTest);
            system.assertEquals(null,SchoolAcademicYearSelector_OnChangeTest);
        }
    }

    /***Test Constructor for when an Opportinty Id is NOT passed into the page***/
    @isTest   
    private static void SchoolRenewalController_Test_Constructor_currentOpp_Null() {
        init();

        //Run as the portal user who created the Opp
        system.runAs(testUser) {

            test.StartTest();

            PageReference myPage = Page.SchoolRenewal;
            Test.setCurrentPage(myPage);
            SchoolRenewalController controller = new SchoolRenewalController ();

            test.stopTest();

            List<Apexpages.Message> msgs = ApexPages.getMessages();
            boolean b = false;

            for(Apexpages.Message msg:msgs){
                if (msg.getDetail().contains('There was an error retrieving the opportunity.')
                    || msg.getDetail().contains('There was an error retrieving the correct academic year.')
                ) {
                    b = true;
                }
            }
            system.assert(!b);

            system.assertEquals(testAccount.Id,controller.currentSchool.Id);
            system.assertEquals(testOpp.AccountId,controller.currentSchool.Id);
            system.assertEquals(testOpp.RecordTypeId,RecordTypes.opportunitySubscriptionFeeTypeId);
            system.assertEquals(false,testOpp.IsClosed);
            system.assertEquals(testOpp.Academic_Year_Picklist__c,controller.academicYearString);
            system.assertNotEquals(null,controller.currentOpp);
            system.assertEquals(testOpp.Id,controller.currentOpp.Id);
            system.assertNotEquals(null,controller.model);
        }
    }

    /***Test getCurrentSchool***/
    @isTest   
    private static void SchoolRenewalController_Test_getCurrentSchool() {
        init();

        //Run as the portal user who created the Opp
        system.runAs(testUser) {

            test.StartTest();

            SchoolRenewalController controller = new SchoolRenewalController ();
            Account getCurrentSchoolTest = controller.getCurrentSchool(testAccount.Id);

            test.stopTest();

            system.assertEquals(testAccount.Id,getCurrentSchoolTest.Id);
        }
    }

    /***Test upsertOpportunity for when an Opportunity Id is passed into the page***/
    @isTest   
    private static void SchoolRenewalController_Test_upsertOpportunity_currentOpp_NotNull() {
        init();

        //Run as the portal user who created the Opp
        system.runAs(testUser) {

            test.StartTest();

            PageReference myPage = Page.SchoolRenewal;
            Test.setCurrentPage(myPage);
            Apexpages.currentPage().getParameters().put('Id',testOpp.Id);
            SchoolRenewalController controller = new SchoolRenewalController ();
            PageReference upsertOpportunityTest = controller.upsertOpportunity();

            test.stopTest();

            List<Apexpages.Message> msgs = ApexPages.getMessages();
            boolean b = false;

            for(Apexpages.Message msg:msgs){
                if (msg.getDetail().contains('There was a problem processing your request :')) {
                    b = true;
                }
            }
            system.assert(!b);

            system.assertEquals(testAccount.Id,controller.currentSchool.Id);
            system.assertEquals(testOpp.Id,controller.currentOpp.Id);
            system.assertNotEquals(null,controller.model.newSubscriptionType);
            system.assertEquals(controller.currentOpp.Subscription_Type__c,controller.model.newSubscriptionType);
            system.assertNotEquals(null,controller.model.subscriptionFee);
            system.assertEquals(controller.currentOpp.Amount,controller.model.subscriptionFee);
            system.assertNotEquals(null,controller.currentOpp.Subscription_Period_Start_Year__c);
            system.assertEquals(String.valueOf(System.today().year()),controller.currentOpp.Subscription_Period_Start_Year__c);
            system.assertNotEquals('Basic',controller.model.newSubscriptionType);
        }
    }

    /***Test upsertOpportunity for when the Opportunity Id is null***/
    @isTest   
    private static void SchoolRenewalController_Test_upsertOpportunity_currentOpp_Null() {
        init();

        //Run as the portal user who created the Opp
        system.runAs(testUser) {

            test.StartTest();

            PageReference myPage = Page.SchoolRenewal;
            Test.setCurrentPage(myPage);

            SchoolRenewalController controller = new SchoolRenewalController ();
            controller.currentOpp.Id = null;
            PageReference upsertOpportunityTest = controller.upsertOpportunity();

            test.stopTest();

            List<Apexpages.Message> msgs = ApexPages.getMessages();
            boolean b = false;
            for(Apexpages.Message msg:msgs){
                if (msg.getDetail().contains('There was a problem processing your request :')) {
                    b = true;
                }
            }
            system.assert(!b);

            system.assertEquals(testAccount.Id,controller.currentSchool.Id);
            system.assertNotEquals(null,controller.model.newSubscriptionType);
            system.assertNotEquals(null,controller.model.subscriptionFee);
            system.assertNotEquals(null,controller.currentOpp.Subscription_Type__c);
            system.assertNotEquals(null,controller.currentOpp.Subscription_Period_Start_Year__c);
            system.assertNotEquals('Basic',controller.model.newSubscriptionType);
            system.assertNotEquals(null,upsertOpportunityTest);
        }
    }

    /***Test insert Sale TLI and Subscription Record***/
    @isTest
    private static void SchoolRenewalController_Test_renewSubscription() {
        init();

        //Run as the portal user who created the Opp
        system.runAs(testUser) {
            Test.StartTest();

            PageReference myPage = Page.SchoolRenewal;
            Test.setCurrentPage(myPage);
            Apexpages.currentPage().getParameters().put('Id',testOpp.Id);
            SchoolRenewalController controller = new SchoolRenewalController();
            controller.renewSubscription();

            Test.stopTest();

            List<Apexpages.Message> msgs = ApexPages.getMessages();
            boolean b = false;

            for (Apexpages.Message msg:msgs) {
                if (msg.getDetail().contains('There was a problem processing your request :')) {
                    b = true;
                }
            }
            system.assert(!b);

            system.assertEquals(testAccount.Id,controller.currentSchool.Id);
            system.assertEquals(testOpp.Id,controller.currentOpp.Id);
            system.assertEquals('Closed Won',controller.currentOpp.stageName);
        }
    }

    /***Test cancelRedirect***/
    @isTest   
    private static void SchoolRenewalController_Test_cancelRedirect() {
        init();

        //Run as the portal user who created the Opp
        system.runAs(testUser) {

            test.StartTest();

            PageReference myPage = Page.SchoolRenewal;
            Test.setCurrentPage(myPage);
            Apexpages.currentPage().getParameters().put('Id',testOpp.Id);
            SchoolRenewalController controller = new SchoolRenewalController ();
            PageReference cancelRedirectTest = controller.cancelRedirect();

            test.stopTest();

            system.assertEquals(testAccount.Id,controller.currentSchool.Id);
            system.assertEquals(testOpp.Id,controller.currentOpp.Id);
            system.assertEquals('',controller.model.newSubscriptionType);
            system.assertNotEquals(null,cancelRedirectTest);
        }
    }

    /***Test generateReceipt***/
    @isTest   
    private static void SchoolRenewalController_Test_generateReceipt() {
        init();

        //Run as the portal user who created the Opp
        system.runAs(testUser) {

            test.StartTest();

            PageReference myPage = Page.SchoolRenewal;
            Test.setCurrentPage(myPage);
            Apexpages.currentPage().getParameters().put('Id',testOpp.Id);
            SchoolRenewalController controller = new SchoolRenewalController ();
            PageReference generateReceiptTest = controller.generateReceipt();

            test.stopTest();

            system.assertEquals(testAccount.Id,controller.currentSchool.Id);
            system.assertEquals(testOpp.Id,controller.currentOpp.Id);
            system.assertNotEquals(null,generateReceiptTest);
        }
    }

    /***Test updateThankYouPageURL***/
    @isTest   
    private static void SchoolRenewalController_Test_updateThankYouPageURL() {
        init();

        //Run as the portal user who created the Opp
        system.runAs(testUser) {

            test.StartTest();

            PageReference myPage = Page.SchoolRenewal;
            Test.setCurrentPage(myPage);
            Apexpages.currentPage().getParameters().put('Id',testOpp.Id);
            SchoolRenewalController controller = new SchoolRenewalController ();
            PageReference updateThankYouPageURLTest = controller.updateThankYouPageURL();

            test.stopTest();

            PageReference testPageRef = Page.SchoolRenewal;
            testPageRef.getParameters().put('Id',testOpp.Id);

            system.assertEquals(testPageRef.getParameters(),updateThankYouPageURLTest.getParameters());
        }
    }

    @isTest
    private static void isMissingSubscription_missingSubscriptionHasOpenOpportunity_expectFalse() {
        init();
        Boolean isMissing;

        System.runAs(testUser) {
            PageReference renewalPage = Page.SchoolRenewal;
            Test.setCurrentPage(renewalPage);
            Apexpages.currentPage().getParameters().put('Id', testOpp.Id);
            SchoolRenewalController controller = new SchoolRenewalController();

            Test.startTest();
            isMissing = controller.IsMissingSubscription;
            Test.stopTest();
        }

        System.assert(!isMissing, 'Expected the subscription not to be missing for a non closed won opp.');
    }

    @isTest
    private static void isMissingSubscription_missingSubscriptionHasClosedOpportunity_expectFalse() {
        init();
        TransactionLineItemTestData.Instance.forOpportunity(testOpp.Id).forAmount(testOpp.Amount).asSale().insertTransactionLineItem();
        TransactionLineItemTestData.Instance.forOpportunity(testOpp.Id).forAmount(testOpp.Amount).asCheck().insertTransactionLineItem();

        OpportunityService.Instance.markAsClosedWon(testOpp);

        List<Subscription__c> subscriptions = SubscriptionSelector.Instance.selectByAccountId(new Set<Id> { testOpp.AccountId });
        delete subscriptions;

        Boolean isMissing;

        System.runAs(testUser) {
            PageReference renewalPage = Page.SchoolRenewal;
            Test.setCurrentPage(renewalPage);
            Apexpages.currentPage().getParameters().put('Id', testOpp.Id);
            SchoolRenewalController controller = new SchoolRenewalController();
            System.assert(CurrentUser.hasSubscriptionOpportunityForCurrentAcademicYear());

            Test.startTest();
            isMissing = controller.IsMissingSubscription;
            Test.stopTest();
        }

        System.assert(isMissing, 'Expected the subscription to be missing for a closed won opp.');
    }

    @isTest
    private static void testSubscriptionHelpInfoDocLink()
    {
        init();

        System.runAs(testUser)
        {
            PageReference renewalPage = Page.SchoolRenewal;
            Test.setCurrentPage(renewalPage);
            Apexpages.currentPage().getParameters().put('Id', testOpp.Id);
            SchoolRenewalController controller = new SchoolRenewalController();

            System.assertEquals(null, controller.subscriptionHelpInfoDocLink);

            Document subscriptionHelpDoc = DocumentTestData.Instance
                .forDocumentName(SchoolRenewalController.SCHOOL_SUBSCRIPTION_INFORMATION).DefaultDocument;

            List<Document> subscriptionDocs = [
                SELECT Id
                FROM Document
                WHERE Name = :SchoolRenewalController.SCHOOL_SUBSCRIPTION_INFORMATION];
            System.assertEquals(SchoolRenewalController.FILE_LINK_URL + subscriptionDocs[0].Id , controller.subscriptionHelpInfoDocLink);
        }
    }
}