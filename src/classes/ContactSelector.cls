/**
 * @description Query selector for Contact records.
 */
public class ContactSelector extends fflib_SObjectSelector {

    @testVisible private static final String RECORDTYPE_ID  = 'recordTypeId';
    @testVisible private static final String SCHOOL_ID  = 'schoolId';
    @testVisible private static final String CONTACT_EMAIL  = 'contact email';

    /**
     * @description Selects Contact records by RecordType Id.
     * @param recordTypeId The Id of the RecordType to query for Contact records.
     * @return A list of Contact records.
     * @throws An ArgumentNullException if recordTypeId is null.
     */
    public List<Contact> selectByRecordTypeId(Id recordTypeId) {
        ArgumentNullException.throwIfNull(recordTypeId, RECORDTYPE_ID);

        assertIsAccessible();

        String contactQuery = newQueryFactory(true)
                .setCondition('RecordTypeId = :recordTypeId')
                .toSOQL();

        return Database.query(contactQuery);
    }

    /**
     * @description Selects Contact records by school Id and email address.
     * @param schoolId The Id of the school the contact is associated with.
     * @param email The email of the contact.
     * @return A list of Contact records.
     * @throws An ArgumentNullException if schoolId is null.
     * @throws An ArgumentNullException if email is null.
     */
    public List<Contact> selectBySchoolIdAndEmail(Id schoolId, String email)
    {
        ArgumentNullException.throwIfNull(schoolId, SCHOOL_ID);
        ArgumentNullException.throwIfNull(email, CONTACT_EMAIL);

        assertIsAccessible();

        String contactQuery = newQueryFactory(true)
                .setCondition('AccountId = :schoolId AND Email = :email')
                .toSOQL();

        return Database.query(contactQuery);
    }
    
    /**
     * @description Selects SSS Main Contact records by school.
     * @param schoolId The Id of the school.
     * @return A list of SSS Main Contact records for the given school.
     * @throws An ArgumentNullException if schoolId is null.
     */
    public List<Contact> selectSSSMainContactEmail(Id schoolId)
    {
        ArgumentNullException.throwIfNull(schoolId, SCHOOL_ID);

        assertIsAccessible();

        String contactQuery = newQueryFactory(true)
                .setCondition('AccountId = :schoolId AND SSS_Main_Contact__c = true')
                .toSOQL();

        return Database.query(contactQuery);
    }

    private Schema.SObjectType getSObjectType() {
        return Contact.getSObjectType();
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                Contact.Name,
                Contact.Id,
                Contact.Email,
                Contact.AccountId
        };
    }

    /**
     * @description Creates a new instance of the ContactSelector.
     * @return An instance of ContactSelector.
     */
    public static ContactSelector newInstance() {
        return new ContactSelector();
    }

    /**
     * @description Singleton property ensuring external sources do not
     *              instantiate this directly.
     */
    public static ContactSelector Instance {
        get {
            if (Instance == null) {
                Instance = new ContactSelector();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to enforce singleton access
     *              via the Instance property.
     */
    private ContactSelector() {}
}