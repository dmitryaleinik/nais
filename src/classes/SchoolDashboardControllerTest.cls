@isTest
private class SchoolDashboardControllerTest {

    // MOST OF CODE IS COVERED IN SCHOOLPICKER CLASS
    private static User sysAdminUser = UserSelector.newInstance().selectById(new Set<Id>{UserInfo.getUserId()})[0];
    private static User schoolPortalUser;

    @isTest
    private static void testController(){
        System.runAs(sysAdminUser) {
            Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
            Account school = AccountTestData.Instance.asSchool().DefaultAccount;
            
            String basicSubscriptionType = 'Basic';
            Subscription__c subscription = SubscriptionTestData.Instance
                .forAccount(school.Id)
                .forStartDate(system.today().addDays(-14))
                .forEndDate(system.today().addYears(1))
                .forSubscriptionType(basicSubscriptionType).DefaultSubscription;

            Contact schoolStaff = ContactTestData.Instance
                .forAccount(school.Id)
                .asSchoolStaff().insertContact();
        
            schoolPortalUser = UserTestData.Instance
                .forSystemTermsAndConditionsAccepted(System.now().addDays(-1))
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).DefaultUser;
            TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);

            Decimal totalFundsAmount = 10000;
            Budget_Group__c budget = BudgetGroupTestData.Instance
                .forSchoolId(school.Id)
                .forAcademicYearId(currentAcademicYear.Id)
                .forTotalFunds(totalFundsAmount).DefaultBudgetGroup;
        }

        Test.startTest();
            System.runAs(schoolPortalUser){
                List<Annual_Setting__c> annualSettings = GlobalVariables.getCurrentAnnualSettings(true, datetime.now().date(), null);
                System.assertEquals(true, annualSettings.size()>0);
                System.assertEquals(true, annualSettings[0]!=null);
                annualSettings[0].Send_PFS_Reminders__c = false;
				annualSettings[0].Send_Prior_Year_Document_Reminders__c = true;
				annualSettings[0].Send_Current_Year_Document_Reminders__c  = true;
                annualSettings[0].Annual_Settings_Updated__c = System.today();
                upsert annualSettings[0];
                // expect to redirect to school picker because this is a multi-school user without a school in focus
                SchoolDashboardController testController = new SchoolDashboardController();
                System.assertEquals(null, testController.init());
                System.assertEquals(true, testController.getHasBudgets());
            }
        Test.stopTest();
    }

    @isTest
    private static void testControllerAndInit(){
        System.runAs(sysAdminUser){
            Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
            Account school1 = AccountTestData.Instance.asSchool().create();
            Account school2 = AccountTestData.Instance.asSchool().create();
            insert new List<Account>{school1, school2};
            
            String basicSubscriptionType = 'Basic';
            Subscription__c subscription1 = SubscriptionTestData.Instance
                .forAccount(school1.Id)
                .forStartDate(system.today().addDays(-14))
                .forEndDate(system.today().addYears(1))
                .forSubscriptionType(basicSubscriptionType).create();
            Subscription__c subscription2 = SubscriptionTestData.Instance
                .forAccount(school2.Id)
                .forStartDate(system.today().addDays(-14))
                .forEndDate(system.today().addYears(1))
                .forSubscriptionType(basicSubscriptionType).create();
            insert new List<Subscription__c>{subscription1, subscription2};

            Contact schoolStaff = ContactTestData.Instance
                .forAccount(school1.Id)
                .asSchoolStaff().insertContact();
        
            schoolPortalUser = UserTestData.Instance
                .forSystemTermsAndConditionsAccepted(System.now().addDays(-1))
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).DefaultUser;
            TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);

            Decimal totalFundsAmount = 10000;
            Budget_Group__c budget = BudgetGroupTestData.Instance
                .forSchoolId(school1.Id)
                .forAcademicYearId(currentAcademicYear.Id)
                .forTotalFunds(totalFundsAmount).DefaultBudgetGroup;

            List<Affiliation__c> testAffs = new List<Affiliation__c>();
            testAffs.add(new Affiliation__c(
                Contact__c = schoolStaff.Id,
                Type__c = 'Additional School User',
                Organization__c = school1.Id,
                Status__c = 'Current'));
            testAffs.add(new Affiliation__c(
                Contact__c = schoolStaff.Id,
                Type__c = 'Additional School User',
                Organization__c = school2.Id,
                Status__c = 'Current',
                Role__c = 'drewtest'));

            insert testAffs;

            // won't matter that we persist because in focus school is blank
            schoolPortalUser.Persist_School_Focus__c = true;
            schoolPortalUser.SYSTEM_Terms_and_Conditions_Accepted__c = System.now().addDays(-1);
            update schoolPortalUser;
        }

        System.runAs(schoolPortalUser){
            Test.startTest();
                // expect to redirect to school picker because this is a multi-school user without a school in focus
                SchoolDashboardController testController = new SchoolDashboardController();
            Test.stopTest();
            
            System.assert(testController.init().getURL().contains('schoolpicker'));
        }
    }

    @isTest
    private static void testLoadMethods(){
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);

        initTestData();

        PageReference pr = Page.SchoolDashboard;
        Test.setCurrentPage(pr);

        Case c;
        Case c2;
        Case c3; //NAIS-1973
        Case c4; //NAIS-1973

        // [CH] NAIS-1601 Need to insert Cases as portal user for visibility
        System.runAs(schoolPortalUser){
            /*NAIS-1973*/
            List<Contact> schoolUsers = ContactSelector.newInstance().selectByRecordTypeId(RecordTypes.schoolStaffContactTypeId);
            System.assert(!schoolUsers.isEmpty());
            System.assertEquals(1, schoolUsers.size());
            Contact schoolStaff = schoolUsers[0];
            List<Account> schools = AccountSelector.Instance.selectByRecordTypeName(AccountSelector.RECORDTYPE_NAME_SCHOOL);
            System.assert(!schools.isEmpty());
            System.assertEquals(1, schools.size());
            Account school = schools[0];

            c = new Case(AccountId = school.Id, Subject = 'subj', RecordTypeId = RecordTypes.schoolParentCommunicationCaseTypeId, ContactId = schoolStaff.Id);
            c2 = new Case(AccountId = school.Id, Subject = 'subj2', RecordTypeId = RecordTypes.schoolParentCommunicationCaseTypeId, ContactId = schoolStaff.Id);
            c3 = new Case(AccountId = school.Id, Subject = 'subj', RecordTypeId = RecordTypes.schoolPortalCaseTypeId, ContactId = schoolStaff.Id);
            c4 = new Case(AccountId = school.Id, Subject = 'subj2', RecordTypeId = RecordTypes.schoolPortalCaseTypeId, ContactId = schoolStaff.Id);
            Database.insert(new List<Case>{c, c2,c3,c4});
            /*End NAIS-1973*/
        }

        // Insert comments not created by the portal user
        CaseComment cc1 = new CaseComment(ParentId = c.Id, IsPublished = true);
        CaseComment cc2 = new CaseComment(ParentId = c.Id, IsPublished = true);
        CaseComment cc3 = new CaseComment(ParentId = c2.Id, IsPublished = true);
        CaseComment cc4 = new CaseComment(ParentId = c2.Id, IsPublished = true);
        insert new List<CaseComment>{cc1, cc2, cc3, cc4};

        System.runAs(schoolPortalUser){
            Test.startTest();
                // Add a couple of comments to the case with visible comments but added by the current user
                CaseComment cc5 = new CaseComment(ParentId = c.Id, IsPublished = true);
                CaseComment cc6 = new CaseComment(ParentId = c.Id, IsPublished = true);
                insert new List<CaseComment>{cc5, cc6};

                List<Case> updatedCases = [Select Id, Created_Updated_for_School_Portal__c FROM Case];

                Academic_Year__c currentAcademicYear = AcademicYearService.Instance.getCurrentAcademicYear();
                Annual_Setting__c testAS = GlobalVariables.getCurrentAnnualSettings(true, System.today(), currentAcademicYear.Id)[0];
                testAS.Tuition_And_Fees_Updated__c = System.today();
                upsert testAS;

                // expect to redirect to school picker because this is a multi-school user without a school in focus
                SchoolDashboardController testController = new SchoolDashboardController();

                testController.loadAdminProgress();
                testController.loadPFSStats();
                testController.loadMSGStats();
                testController.loadAwardStats();
                testController.loadFolderIds();

                System.assertNotEquals(null, testController.SchoolAcademicYearSelector_OnChange(false));
                testController.getAcademicYearId();
            Test.stopTest();
            
            // pfs stats
            System.assertEquals(3, testController.pfsPastDay);
            System.assertEquals(3, testController.pfsPastWeek);
            System.assertEquals(3, testController.pfsUpdatedPastDay);

            // admin progress
            System.assertEquals(System.today(), testController.tuitionFeesDate);
            System.assertEquals(null, testController.schoolProfileDate);
            System.assertEquals(null, testController.genConfigDate);
            System.assertNotEquals(null, testController.budgetGroupsDate);
            System.assertEquals(null, testController.profJudgmentDate);
            System.assertEquals(true, testController.tuitionFeesUpdated);
            System.assertEquals(false, testController.schoolProfileUpdated);
            System.assertEquals(false, testController.genConfigUpdated);
            System.assertEquals(true, testController.budgetGroupsUpdated);
            System.assertEquals(false, testController.profJudgmentUpdated);

            // award stats
            System.assertEquals(3, testController.awardsGiven);
            System.assertEquals(3000, testController.avgAward);

            // [DP] 02.04.2015 commenting out for DEPLOY
            System.assertEquals(2, testController.openCases);
            System.assertEquals(2, testController.openTickets); //NAIS-1973
            System.assertEquals(6, [Select count() from CaseComment]);
            System.assertEquals(2, testController.msgsThisWeek);
            System.assertEquals(2, testController.msgsToday);
        }
    }

    // NAIS-2373 Subscription expired
    // NAIS-2375 Subscription Info
    @isTest
    private static void testSubscriptionStatus() {
        Subscription__c subscription;
        String basicSubscriptionType = 'Basic';

        System.runAs(sysAdminUser) {
            Account school = AccountTestData.Instance.asSchool().DefaultAccount;
            Contact schoolStaff = ContactTestData.Instance
                .forAccount(school.Id)
                .asSchoolStaff().insertContact();

            subscription = SubscriptionTestData.Instance
                .forAccount(school.Id)
                .forStartDate(system.today().addDays(-14))
                .forEndDate(system.today())
                .forSubscriptionType(basicSubscriptionType).DefaultSubscription;
                
            schoolPortalUser = UserTestData.Instance
                .forContactId(schoolStaff.Id)
                .forSystemTermsAndConditionsAccepted(System.now().addDays(-1))
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
            TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);
        }

        Test.startTest();
            System.runAs(schoolPortalUser) {
                SchoolDashboardController testController = new SchoolDashboardController();

                System.assertEquals(basicSubscriptionType, testController.subscriptionType);
                System.assertEquals(system.today(), testController.subscriptionEndDate);
                System.assertEquals(false, testController.renderRenewalLink);
                System.assertEquals(false, testController.isSubscriptionExpired);
            }

            System.runAs(sysAdminUser) {
                Renewal_Annual_Settings__c rs = new Renewal_Annual_Settings__c(Name = String.valueOf(System.today().year()));
                rs.Renewal_Season_Start_Date__c = System.today();
                rs.Renewal_Season_End_Date__c = System.today().addDays(10);
                insert rs;
            
                subscription.End_Date__c = system.today();
                update subscription;
            }

            System.runAs(schoolPortalUser) {
                SchoolDashboardController testController = new SchoolDashboardController();

                System.assertEquals(system.today(), testController.subscriptionEndDate);
                System.assertEquals(true, testController.renderRenewalLink);
            }
        Test.stopTest();
    }

    @isTest
    private static void testRedirectToAnnualSettingPage() {
        System.runAs(sysAdminUser) {
            Account school = AccountTestData.Instance.asSchool().DefaultAccount;
            Contact schoolStaff = ContactTestData.Instance
                .forAccount(school.Id)
                .asSchoolStaff().insertContact();

            String basicSubscriptionType = 'Basic';
            Subscription__c subscription = SubscriptionTestData.Instance
                .forAccount(school.Id)
                .forStartDate(system.today().addDays(-14))
                .forEndDate(system.today().addYears(1))
                .forSubscriptionType(basicSubscriptionType).DefaultSubscription;
                
            schoolPortalUser = UserTestData.Instance
                .forContactId(schoolStaff.Id)
                .forSystemTermsAndConditionsAccepted(System.now().addDays(-1))
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
            TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);
        }
 
        Test.startTest();
            System.runAs(schoolPortalUser){
                // expect to redirect to school picker because this is a multi-school user without a school in focus
                SchoolDashboardController testController = new SchoolDashboardController();
                List<Annual_Setting__c> annualSetting = new List<Annual_Setting__c>([Select Id from Annual_Setting__c
                                                            where School__c=:testController.schoolId
                                                            and Academic_Year__c=:testController.academicyearid
                                                            and (Send_PFS_Reminders__c = true or Send_Prior_Year_Document_Reminders__c =true or Send_Current_Year_Document_Reminders__c =true)]);
                System.assertEquals(true, annualSetting.size()==0);
                System.assertEquals('/apex/schoolannualsettings', ((testController.init()).getURL()).toLowerCase());
            }
        Test.stopTest();
    }

    @isTest
    private static void hasGlobalMessages_noGlobalMessages_expectFalse() {
        System.runAs(sysAdminUser) {
            schoolPortalUser = UserTestData.insertSchoolPortalUser();
        }

        Boolean hasGlobalMessages;

        System.runAs(schoolPortalUser) {
            SchoolDashboardController controller = new SchoolDashboardController();
            hasGlobalMessages = controller.getHasGlobalMessages();
        }

        System.assert(!hasGlobalMessages, 'Expected no global messages to be available.');
    }

    @isTest
    private static void hasGlobalMessages_globalMessageInserted_expectTrue()
    {
        Profile_Type_Grouping__c ptg = ProfileTypeGroupingTestData.Instance
            .asSchoolPortalUser().DefaultProfileTypeGrouping;

        Account school = AccountTestData.Instance.asSchool().DefaultAccount;
        Contact schoolStaff = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(school.Id).DefaultContact;

        System.runAs(sysAdminUser)
        {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalUserProfileId).insertUser();
        }

        insertGlobalMessage();

        Boolean hasGlobalMessages;

        System.runAs(schoolPortalUser) {
            SchoolDashboardController controller = new SchoolDashboardController();
            hasGlobalMessages = controller.getHasGlobalMessages();
        }

        System.assert(hasGlobalMessages, 'Expected global messages to be available.');
    }

    //SFP-1388
    @isTest 
    private static void isLimitedSchoolViewOrAccessOrg_accessOrgUser_isRedirectedToSchoolDashboard() {
        
        //1. Create the current AcademicYear.
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.insertAcademicYear();
        
        //2. Create the Access Organization.
        Account school = AccountTestData.Instance.forRecordTypeId(RecordTypes.accessOrgAccountTypeId).insertAccount();
        
        //2.1 Create Subscription record for that Access Organization.
        Subscription__c subscription = SubscriptionTestData.Instance
            .forStartDate(system.today().addDays(-14))
            .forEndDate(system.today().addYears(1))
            .forAccount(school.Id).insertSubscription();
        
        //3. Create a Contact for that Access Organization.
        Contact schoolStaff = ContactTestData.Instance
            .forAccount(school.Id)
            .asSchoolStaff().insertContact();
        
        //4. Get the User record for the newly created Access Organization. We use runAs administrator to avoid MIXED_DML exception.
        User schoolUser;
        System.runAs(sysAdminUser) {
            schoolUser =  UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId)
                .forSystemTermsAndConditionsAccepted(system.now()).insertUser();
                
        }
        
        //5. Make sure that the Access Organization do NOT have related AnnualSetting records.
        System.assertEquals(0, AnnualSettingsSelector.newInstance().selectBySchoolAndAcademicYear(
            new Set<Id>{school.Id}, new Set<String>{currentAcademicYear.Name}).size());
        
        //6. Login as Access Organization user.
        System.runAs(schoolUser) {
            
            //7. Create an instance of SchoolDashboardController
            SchoolDashboardController testController = new SchoolDashboardController();
            
            //8. Make sure that the user is not being redirected to AnnualSettings.page
            System.assertEquals(null, testController.init());
            
            //9. Make sure that the Access Organization now have a related AnnualSetting record for the current academicYear.
            System.assertEquals(1, AnnualSettingsSelector.newInstance().selectBySchoolAndAcademicYear(
                new Set<Id>{school.Id}, new Set<String>{currentAcademicYear.Name}).size());
        }
    }

    private static void initTestData() {
        Account family = AccountTestData.Instance.asFamily().create();
        Account school = AccountTestData.Instance.asSchool().create();
        insert new List<Account>{family, school};

        Contact parentA = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact parentB = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .forAccount(school.Id)
            .asSchoolStaff().create();
        Contact student1 = ContactTestData.Instance
            .asStudent().create();
        Contact student2 = ContactTestData.Instance
            .asStudent().create();
        insert new List<Contact>{parentA, parentB, schoolStaff, student1, student2};
        
        System.runAs(sysAdminUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.insertAcademicYear();

        System.runAs(schoolPortalUser) {
            PFS__c pfs1 = PfsTestData.Instance
                .asSubmitted()
                .forParentA(parentA.Id)
                .forApplicationLastModifiedByFamily(System.today())
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            PFS__c pfs2 = PfsTestData.Instance
                .asSubmitted()
                .forParentA(parentB.Id)
                .forApplicationLastModifiedByFamily(System.today())
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<PFS__c> {pfs1, pfs2});

            Applicant__c applicant1 = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student1.Id).create();
            Applicant__c applicant2 = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student2.Id).create();
            Applicant__c applicant3 = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student2.Id).create();
            Dml.WithoutSharing.insertObjects(new List<Applicant__c> {applicant1, applicant2, applicant3});

            String dayBoarding = 'Day';
            String folderSourceSchoolInitiated = 'School-Initiated';
            String folderStatusAwardApproved = 'Award Approved';
            String useBudgetGroups = 'No';
            String studentFolder1Name = 'Student Folder 1';
            String studentFolder2Name = 'Student Folder 2';
            String studentFolder3Name = 'Student Folder 3';
            String newReturning = 'New';
            Integer grantAwarded1000 = 1000;
            Integer grantAwarded3000 = 3000;
            Integer grantAwarded5000 = 5000;
            Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
                .forName(studentFolder1Name)
                .forNewReturning(newReturning)
                .forStudentId(student1.Id)
                .forSchoolId(school.Id)
                .forDayBoarding(dayBoarding)
                .forFolderSource(folderSourceSchoolInitiated)
                .forFolderStatus(folderStatusAwardApproved)
                .forUseBudgetGroups(useBudgetGroups)
                .forGrantAwarded(grantAwarded3000)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Student_Folder__c studentFolder2 = StudentFolderTestData.Instance
                .forName(studentFolder2Name)
                .forNewReturning(newReturning)
                .forStudentId(student2.Id)
                .forSchoolId(school.Id)
                .forDayBoarding(dayBoarding)
                .forFolderSource(folderSourceSchoolInitiated)
                .forFolderStatus(folderStatusAwardApproved)
                .forUseBudgetGroups(useBudgetGroups)
                .forGrantAwarded(grantAwarded1000)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Student_Folder__c studentFolder3 = StudentFolderTestData.Instance
                .forName(studentFolder3Name)
                .forNewReturning(newReturning)
                .forStudentId(student2.Id)
                .forSchoolId(school.Id)
                .forDayBoarding(dayBoarding)
                .forFolderSource(folderSourceSchoolInitiated)
                .forFolderStatus(folderStatusAwardApproved)
                .forUseBudgetGroups(useBudgetGroups)
                .forGrantAwarded(grantAwarded5000)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<Student_Folder__c> {studentFolder1, studentFolder2, studentFolder3});

            String fifthGrade = '5';
            School_PFS_Assignment__c spfsa1 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            School_PFS_Assignment__c spfsa2 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant2.Id)
                .forStudentFolderId(studentFolder2.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            School_PFS_Assignment__c spfsa3 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant3.Id)
                .forStudentFolderId(studentFolder2.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> {spfsa1, spfsa2, spfsa3});
        }

        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);
        
        Budget_Group__c budget1 = BudgetGroupTestData.Instance
            .forAcademicYearId(currentAcademicYear.Id)
            .forSchoolId(school.Id).insertBudgetGroup();

        insertGlobalMessage();
    }

     private static Global_Message__c insertGlobalMessage()
     {
         String schoolPortalName = 'School Portal';
         Global_Message__c newGlobalMessage = GlobalMessageTestData.Instance
             .forPortal(schoolPortalName).DefaultGlobalMessage;

         return newGlobalMessage;
     }
    
    @isTest
    private static void init_TwoOpenAYAndOneAnnualSettingForPreviousAY_expectedASCreatedForCurrentAY(){
        
        Academic_Year__c currentAcademicYear, priorAcademicYear;
        Account school;
        System.runAs(sysAdminUser) {
            
            //Create academic year records.
            currentAcademicYear = AcademicYearTestData.Instance.create();
            currentAcademicYear.Start_Date__c = Date.Today().addDays(-14);
            currentAcademicYear.End_Date__c = Date.Today().addYears(1);
            
            priorAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
	        priorAcademicYear.Start_Date__c = Date.Today().addYears(-1);
	        priorAcademicYear.End_Date__c = Date.Today().addDays(10);
        
            insert new List<Academic_Year__c>{currentAcademicYear, priorAcademicYear};
            
            //Create School.
            school = AccountTestData.Instance.asSchool().DefaultAccount;
            
            //Create a basic subscription for that school.
            String basicSubscriptionType = 'Basic';
            Subscription__c subscription = SubscriptionTestData.Instance
                .forAccount(school.Id)
                .forStartDate(system.today().addDays(-14))
                .forEndDate(system.today().addYears(1))
                .forSubscriptionType(basicSubscriptionType).DefaultSubscription;

            Contact schoolStaff = ContactTestData.Instance
                .forAccount(school.Id)
                .asSchoolStaff().insertContact();
        
            schoolPortalUser = UserTestData.Instance
                .forSystemTermsAndConditionsAccepted(System.now().addDays(-1))
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).DefaultUser;
            TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);

            //Create annual setting for previous academic year
	        Annual_Setting__c annualSetting = AnnualSettingsTestData.Instance
	            .forSchoolId(school.Id)
	            .forAcademicYearId(priorAcademicYear.Id)
	            .insertAnnualSettings();
        }
        Test.startTest();
            System.runAs(schoolPortalUser){
                // expect to redirect to school picker because this is a multi-school user without a school in focus
                SchoolDashboardController testController = new SchoolDashboardController();
                testController.init();
            }
        Test.stopTest();
        
         System.assertNotEquals(null, 
            [SELECT Id FROM Annual_Setting__c 
            WHERE Academic_Year__c =: priorAcademicYear.Id AND School__c =: school.Id]);
        
        System.assertNotEquals(null, 
            [SELECT Id FROM Annual_Setting__c 
            WHERE Academic_Year__c =: currentAcademicYear.Id AND School__c =: school.Id]);
    }
}