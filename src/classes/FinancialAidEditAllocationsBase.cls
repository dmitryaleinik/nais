public virtual class FinancialAidEditAllocationsBase {
    /*  Initialization  */
    public FinancialAidEditAllocationsBase(){

    }
    /*  End Initialization  */

    /*Properties*/
    public Student_Folder__c folder {get; set;}
    public Boolean allowBudgetChoice {get; set;}
    public String useBudgets {get; set;}
    public Boolean budgetChange {get; set;}
    public list<Budget_Allocation__c> budgetAllocations {get; set;}
    public Budget_Allocation__c newBudgetAlloc {get; set;}
    public Boolean amountErrorMsg {get; set;}
    public String AmountErrorMessage { get; set; }
    public list<Budget_Group__c> budgetGroups;
    public list<budget> budgets {get; set;}
    public map<Id, Budget_Group__c> budgetGroupIdToBudgetGroupMap;
    public double totalFromBudgets {get; set;}
    public List<Budget_Allocation__c> budgetAllocationsToDelete;
    public List<Budget_Allocation__c> budgetAllocationsToInsert;
    /*End Properties*/

    /*Inner Classes*/
    public class Budget{
        public Budget_Allocation__c budget {get; set;}
        public boolean isFirst {get; set;}
        public String name {get; set;}
        public Integer orderId {get; set;}

        public Budget(Budget_Allocation__c budgetParam){
            budget = budgetParam;
            isFirst = false;
            name = budget.Budget_Group__r.Name;
        }
        public Budget(Budget_Allocation__c budgetParam, String s){
            budget = budgetParam;
            isFirst = false;
            name = s;
        }
    }//End-Class:Budget
    /*End Innder Classes*/

    public List<SelectOption> getBudgetGroupOptions()
    {
        list<SelectOption> budgetGroupOptions = new list<SelectOption>();
        for (Budget_Group__c bg : budgetGroups){
            budgetGroupOptions.add(new SelectOption(bg.Id, bg.Name));
        }
        return budgetGroupOptions;
    }//End:getBudgetGroupOptions

    public List<SelectOption> getUseBudgetOptions()
    {
        list<SelectOption> budgetOptions = new list<SelectOption>();
        budgetOptions.add(new SelectOption('Yes', 'Yes'));
        budgetOptions.add(new SelectOption('No', 'No'));
        return budgetOptions;
    }//End:getUseBudgetOptions

    public void addBudgetAlloc()
    {
        try{
            amountErrorMsg = false;
            if (newBudgetAlloc.Amount_Allocated__c == null || newBudgetAlloc.Amount_Allocated__c <= 0){
                amountErrorMsg = true;
                AmountErrorMessage = Label.AmountMustBeGreaterThanZero;
            } else {
                newBudgetAlloc.Amount_Allocated__c = newBudgetAlloc.Amount_Allocated__c == null ? 0 : newBudgetAlloc.Amount_Allocated__c;
                budgets.add(new budget(newBudgetAlloc, budgetGroupIdToBudgetGroupMap.get(newBudgetAlloc.Budget_Group__c).Name));
                calcTotalFromBudgetsAndSetOrderIds();
                newBudgetInit();
                apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,
                                    'You will need to Save and Recalculate for your budget modifications to take effect.'));
                budgetChange = true;
            }
        }catch(Exception e){
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }//End:addBudgetAlloc

    public void deleteBudget()
    {
        Integer budgetIdToDelete = Integer.valueOf(System.currentPagereference().getParameters().get('budgetIdToDelete'));
        Budget_Allocation__c baToDelete;

        for (Integer i = 0; i < budgets.size(); i++)
        {
            if (budgets[i].orderId == budgetIdToDelete)
            {
                if (budgets[i].budget.Id != null)
                {
                    budgetAllocationsToDelete.add(budgets[i].budget);
                }
                budgets.remove(i);
            }
        }
        calcTotalFromBudgetsAndSetOrderIds();
        apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'You will need to Save and Recalculate for your budget modifications to take effect.'));
        budgetChange = true;
    }//End:deleteBudget

    public void calcTotalFromBudgetsAndSetOrderIds(){
        totalFromBudgets = 0;
        Integer orderInt = 0;

        for (budget b : budgets)
        {
            totalFromBudgets += b.budget.Amount_Allocated__c;
            b.orderId = orderInt++;
        }
    }//End:calcTotalFromBudgetsAndSetOrderIds

    public void newBudgetInit()
    {
        newBudgetAlloc = new Budget_Allocation__c();
        newBudgetAlloc.Student_Folder__c = folder.Id;
    }//End:newBudgetInit

    public void loadBudgets()
    {
        budgets = new List<budget>();
        budgetAllocations = [Select Status__c, Amount_Allocated__c, Budget_Group__c, Budget_Group__r.Name
                                        from Budget_Allocation__c where Student_Folder__c = :folder.ID];

        for (Budget_Allocation__c ba : budgetAllocations){
            budgets.add(new budget(ba));
        }

        if (budgets.size() > 0){
            budgets[0].isFirst = true;
            calcTotalFromBudgetsAndSetOrderIds();
        }

        budgetAllocationsToInsert = new List<Budget_Allocation__c>();
        budgetAllocationsToDelete = new List<Budget_Allocation__c>();
    }//End:loadBudgets

    // [CH] NAIS-1866 Adding to work across multiple save processes
    public void handleBudgetUpdates(){
        //folder.Grant_Awarded__c = totalFromBudgets == 0 ? null : totalFromBudgets; NAIS-2036 [DP] 11.20.2014

        for (Budget b : budgets){
            if (b.budget.Id == null){
                b.budget.Student_Folder__c = folder.Id; // [CH] NAIS-1866 Adding to handle newly created folders
                budgetAllocationsToInsert.add(b.budget);
            }
        }

        if (budgetAllocationsToInsert.size() > 0){
            insert budgetAllocationsToInsert;
        }
        if (budgetAllocationsToDelete.size() > 0){
            delete budgetAllocationsToDelete;
        }
    }//End:handleBudgetUpdates
}