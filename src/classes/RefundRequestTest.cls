@isTest
private class RefundRequestTest {
    private static final String ACADEMIC_YEAR = (System.today().year()+1) + '-' + (System.today().year() + 2);
    private static final String SUBSCRIPTION_TYPE = 'Full';

    private static Transaction_Line_Item__c generateTransaction() {
        Account schoolAccount = new Account(
                Name = 'Great School',
                SSS_School_Code__c = '123456');
        insert schoolAccount;

        Opportunity opportunityRecord = new Opportunity(
                AccountId = schoolAccount.Id,
                Name = 'Placeholder Name',
                New_Renewal__c = 'New',
                StageName = 'Prospecting',
                CloseDate = System.today(),
                Subscription_Period_Start_Year__c = ACADEMIC_YEAR.left(4),
                Academic_Year_Picklist__c = ACADEMIC_YEAR,
                Subscription_Type__c = SUBSCRIPTION_TYPE,
                Amount = 47.00);
        insert opportunityRecord;

        PaymentResult result = new PaymentResult();
        result.isSuccess = true;
        result.ErrorOccurred = !result.isSuccess;
        result.transactionResultCode = 0;
        Id tliId = PaymentUtils.insertCreditCardTransactionLineItem(opportunityRecord.Id, 47.00, 'Visa', '1234', result);
        Transaction_Line_Item__c transactionRecord = TransactionLineItemSelector.Instance.selectById(new Set<Id> { tliId })[0];

        return transactionRecord;
    }

    @isTest
    private static void constructor_nullParam_expectArgumentNullException() {
        try {
            Test.startTest();
            new RefundRequest(null);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, RefundRequest.TRANSACTION_LINE_ITEM_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void constructor_validParam_expectPropertiesSet() {
        Transaction_Line_Item__c tli = generateTransaction();

        Test.startTest();
        RefundRequest request = new RefundRequest(tli);
        Test.stopTest();

        System.assertNotEquals(null, request, 'Expected the request to not be null.');
        System.assertNotEquals(null, request.TransactionLineItem, 'Expected the Transaction Line Item to not be null.');
        System.assertEquals(tli, request.TransactionLineItem, 'Expected the Transaction Line Items to match.');
        System.assertEquals(tli.Transaction_ID__c, request.TransactionNumber, 'Expected the Transaction Number to match.');
        System.assertEquals(tli.Opportunity__c, request.Opportunity.Id, 'Expected the opportunities to match.');
    }
}