@isTest
private class SchoolBizFarmAssignmentSelectorTest
{    
    
    @isTest
    private static void selectAllWithCustomFieldList_expectRecordReturned()
    {
        School_Biz_Farm_Assignment__c sbfa = 
            SchoolBusinessFarmAssignmentTestData.Instance.DefaultSchoolBusinessFarmAssignment;

        Test.startTest();
            List<School_Biz_Farm_Assignment__c> records = 
                SchoolBizFarmAssignmentSelector.newInstance().selectAllWithCustomFieldList(new List<String>{'Name'});
            
            System.assert(!records.isEmpty(), 'Expected the returned list to not be empty.');
            System.assertEquals(1, records.size(), 'Expected there to be one sbfa record returned.');
            System.assertEquals(sbfa.Id, records[0].Id, 'Expected the Ids of the sbfa records to match.');
        Test.stopTest();
    }

    @isTest
    private static void selectAll_expectRecordReturned()
    {
        School_Biz_Farm_Assignment__c sbfa = 
            SchoolBusinessFarmAssignmentTestData.Instance.DefaultSchoolBusinessFarmAssignment;

        Test.startTest();
            List<School_Biz_Farm_Assignment__c> records = SchoolBizFarmAssignmentSelector.newInstance().selectAll();
            
            System.assert(!records.isEmpty(), 'Expected the returned list to not be empty.');
            System.assertEquals(1, records.size(), 'Expected there to be one sbfa record returned.');
            System.assertEquals(sbfa.Id, records[0].Id, 'Expected the Ids of the sbfa records to match.');
        Test.stopTest();
    }
}