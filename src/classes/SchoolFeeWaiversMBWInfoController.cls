public with sharing class SchoolFeeWaiversMBWInfoController implements SchoolAcademicYearSelectorInterface {
    
    public Id mySchoolId { get; private set; }
    private Id academicYearId;
    public SchoolFeeWaiversMBWInfoController Me { get { return this; } }
    public String currentAcademicYearStr {get{return GlobalVariables.getCurrentAcademicYear().name;}}
    public String currentAcademicYear {get{ return GlobalVariables.getCurrentAcademicYear().name.subString(0,4);}}
    
    public String getAcademicYearId() {
        return academicYearId;
    }

    public SchoolFeeWaiversMBWInfoController() {

    }
    public void setAcademicYearId(String idValue) {
        academicYearId = idValue;
    }
    
    public void initCommon() {
        mySchoolId = GlobalVariables.getCurrentSchoolId();
        loadAcademicYear();
    }
    
    public void loadAcademicYear() {
        Map<String, String> parameters = ApexPages.currentPage().getParameters();
        String academicYearId = null;
        if (parameters.containsKey('academicyearid'))
            setAcademicYearId(parameters.get('academicyearid'));
        else {
            setAcademicYearId(GlobalVariables.getCurrentAcademicYear().Id);
        }
    }
    
    public PageReference SchoolAcademicYearSelector_OnChange(Boolean saveRecord) {
        PageReference newPage = new PageReference(ApexPages.currentPage().getUrl());
        newPage.getParameters().put('academicyearid', getAcademicYearId());
        newPage.setRedirect(true);
        return newPage;
    }
    
    public PageReference purchaseAdditionalWaivers(){
        return Page.SchoolFeeWaiversPurchase;
    }
    
    public PageReference feeWaiverCancel() {
        PageReference pr = Page.SchoolFeeWaivers;
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference feeWaiverPurchase(){
        return Page.SchoolFeeWaiversPurchase;
    }
}