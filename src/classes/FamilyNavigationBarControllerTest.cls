@isTest
private class FamilyNavigationBarControllerTest
{

    private static Account school1;
    private static School_PFS_Assignment__c spfsa1;
    private static Student_Folder__c studentFolder11;
    private static PFS__c pfsA;
    private static Applicant__c applicant1A;

    private static void SetupData()
    {
        //setup PFS
        // create test data
        TestUtils.createAcademicYears();
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        
        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, true);
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, true);
        
        studentFolder11 = TestUtils.createStudentFolder('Student Folder 1.1', academicYearId, student1.Id, true);
        
        school1 = TestUtils.createAccount('Test School 1', RecordTypes.schoolAccountTypeId, 3, true);
        school1.SSS_Subscriber_Status__c = 'Current';
        update school1;
        
        Account schoolKS = TestUtils.createAccount('Test School KS 1', RecordTypes.schoolAccountTypeId, 3, true);
        schoolKS.SSS_Subscriber_Status__c = 'Current';
        update schoolKS;

        SchoolPortalSettings.KS_School_Account_Id = schoolKS.Id;
        
        pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, true);
        pfsA.Own_Business_or_Farm__c = 'Yes';
        pfsA.Number_of_Businesses__c = '1';
        update pfsA;
        
        applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, true);
        
        spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder11.Id, true);
        spfsa1.Salary_Wages_Parent_A__c = 10000;
        
        SSS_Constants__c constants = new SSS_Constants__c(
           Percentage_for_Imputing_Assets__c = 1,
           Default_COLA_Value__c = 1,
           Academic_Year__c = academicYearId,
           Exemption_Allowance__c = 0,
           Std_Deduction_Joint_Surviving__c = 0,
           Std_Deduction_Head_of_Household__c = 0,
           Std_Deduction_Single_Filing_Separately__c = 0,           
           Employment_Allowance_Maximum__c = 0,
           Medicare_Tax_Rate__c = 0,
           Medical_Dental_Allowance_Percent__c = 0,
           Negative_Contribution_Cap_Constant__c = 0,
           Boarding_School_Food_Allowance__c = 0,
           Social_Security_Tax_Threshold__c = 0,
           Social_Security_Tax_Rate__c = 0.25,
           IPA_Housing_For_Each_Additional__c = 1000, 
           IPA_Other_For_Each_Additional__c = 1000);
        
        insert constants;
    }

    @isTest
    private static void TestIsKSSchoolUserFalse()
    {
        FamilyNavigationBarControllerTest.SetupData();
        FamilyNavigationBarController controller = new FamilyNavigationBarController();
        
        //reload pfs
        String pfsSql = 'SELECT ' + String.Join(ExpCore.GetAllFieldNames('PFS__c'), ',') + ', (SELECT ' + String.Join(ExpCore.GetAllFieldNames('Applicant__c'), ',') + ' FROM Applicants__r) FROM PFS__c LIMIT 1';
        pfsA = Database.query(pfsSql);
        
        controller.PFS = pfsA;
        
        System.assert(!controller.IsKSSchoolUser);
    }

    @isTest
    private static void TestIsKSSchoolUserTrue()
    {
        FamilyNavigationBarControllerTest.SetupData();
        SchoolPortalSettings.KS_School_Account_Id = FamilyNavigationBarControllerTest.school1.Id;
        
        //reload pfs
        String pfsSql = 'SELECT ' + String.Join(ExpCore.GetAllFieldNames('PFS__c'), ',') + ', (SELECT ' + String.Join(ExpCore.GetAllFieldNames('Applicant__c'), ',') + ' FROM Applicants__r) FROM PFS__c LIMIT 1';
        pfsA = Database.query(pfsSql);
        System.Debug('FamilyNavigationBarControllerTest.TestIsKSSchoolUserTrue.pfsA.Applicants__r: ' + pfsA.Applicants__r);
        Set<Id> applicantIds = (new Map<Id, Applicant__c>(pfsA.Applicants__r)).keySet();
        
        System.debug(applicantIds);
        System.debug(SchoolPortalSettings.KS_School_Account_Id);
        List<School_PFS_Assignment__c> assignments = [SELECT Id, School__r.SSS_Subscriber_Status__c, Withdrawn__c FROM School_PFS_Assignment__c WHERE Applicant__c IN :applicantIds AND School__c = :SchoolPortalSettings.KS_School_Account_Id];
        System.debug('TestIsKSSchoolUserTrue.assignments: ' + assignments);
        System.debug('TestIsKSSchoolUserTrue.assignments.School__r: ' + assignments[0].School__r);

        FamilyNavigationBarController controller = new FamilyNavigationBarController();
        controller.PFS = pfsA;
        
        System.assert(controller.IsKSSchoolUser);
    }

    @isTest
    private static void TestPFS()
    {
        FamilyNavigationBarControllerTest.SetupData();
        FamilyNavigationBarController controller = new FamilyNavigationBarController();
        
        //reload pfs
        String pfsSql = 'SELECT ' + String.Join(ExpCore.GetAllFieldNames('PFS__c'), ',') + ', (SELECT ' + String.Join(ExpCore.GetAllFieldNames('Applicant__c'), ',') + ' FROM Applicants__r) FROM PFS__c LIMIT 1';
        pfsA = Database.query(pfsSql);
        
        controller.PFS = pfsA;
        
        System.assertNotEquals(controller.PFS, null);
    }

    @isTest
    private static void TestAppUtils()
    {
        FamilyNavigationBarControllerTest.SetupData();
        FamilyNavigationBarController controller = new FamilyNavigationBarController();
        
        //reload pfs
        String pfsSql = 'SELECT ' + String.Join(ExpCore.GetAllFieldNames('PFS__c'), ',') + ', (SELECT ' + String.Join(ExpCore.GetAllFieldNames('Applicant__c'), ',') + ' FROM Applicants__r) FROM PFS__c LIMIT 1';
        pfsA = Database.query(pfsSql);
        
        controller.PFS = pfsA;
        
        System.assertNotEquals(controller.getappUtils(), null);
    }

    @isTest
    private static void TestDisableHouseholdSummary()
    {
        FamilyNavigationBarControllerTest.SetupData();
        FamilyNavigationBarController controller = new FamilyNavigationBarController();
        
        //reload pfs
        String pfsSql = 'SELECT ' + String.Join(ExpCore.GetAllFieldNames('PFS__c'), ',') + ', (SELECT ' + String.Join(ExpCore.GetAllFieldNames('Applicant__c'), ',') + ' FROM Applicants__r) FROM PFS__c LIMIT 1';
        pfsA = Database.query(pfsSql);
        
        controller.DisableHouseholdSummary = true;
        
        System.assert(controller.DisableHouseholdSummary);
    }

    @isTest
    private static void TestHasApplicants()
    {
        FamilyNavigationBarControllerTest.SetupData();
        FamilyNavigationBarController controller = new FamilyNavigationBarController();
        
        //reload pfs
        String pfsSql = 'SELECT ' + String.Join(ExpCore.GetAllFieldNames('PFS__c'), ',') + ', (SELECT ' + String.Join(ExpCore.GetAllFieldNames('Applicant__c'), ',') + ' FROM Applicants__r) FROM PFS__c LIMIT 1';
        pfsA = Database.query(pfsSql);
        
        controller.PFS = pfsA;
        
        System.assert(controller.HasApplicants);
    }

    @isTest
    private static void TestOwnsBusinessOrFarm()
    {
        FamilyNavigationBarControllerTest.SetupData();
        FamilyNavigationBarController controller = new FamilyNavigationBarController();
        
        //reload pfs
        String pfsSql = 'SELECT ' + String.Join(ExpCore.GetAllFieldNames('PFS__c'), ',') + ', (SELECT ' + String.Join(ExpCore.GetAllFieldNames('Applicant__c'), ',') + ' FROM Applicants__r) FROM PFS__c LIMIT 1';
        pfsA = Database.query(pfsSql);
        pfsA.Business_Farm_Owner__c = 'Parent A';
        
        controller.PFS = pfsA;
        
        System.assert(controller.OwnsBusinessOrFarm);
        System.assertNotEquals(null, controller.bfLinkIconSpacing);
        System.assertNotEquals(null, controller.bfLinkSpacing);
    }
}