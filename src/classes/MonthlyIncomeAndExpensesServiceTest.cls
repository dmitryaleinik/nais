@isTest
private class MonthlyIncomeAndExpensesServiceTest {
    
    private static PFS__c pfs;
    
    private static void Setup() {
        
        //1. Create academic year record.
        Academic_Year__c academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        
        //2. Create a School.
        Account school = AccountTestData.Instance.asSchool().insertAccount();
        
        //3. Create contact for parent and student.
		Contact parent = ContactTestData.Instance.asParent().create();
		Contact student = ContactTestData.Instance.asStudent().create();
		insert new List<Contact> { parent, student };
		
		//4. Create PFS record.
		pfs = PfsTestData.Instance
		    .forParentA(parent.Id)
		    .forAcademicYearPicklist(academicYear.Name)
		    .asPaid()
		    .insertPfs();
		    
	    //5. Create applicant.
		Applicant__c applicant = ApplicantTestData.Instance
		    .forContactId(student.Id)
		    .forPfsId(pfs.Id)
		    .insertApplicant();
		    
	    //6. Create Folder.
		Student_Folder__c folder = StudentFolderTestData.Instance.forName('Folder1')
		    .forAcademicYearPicklist(academicYear.Name)
		    .forStudentId(student.Id)
		    .forSchoolId(school.Id)
		    .insertStudentFolder();
		    
	    //7. Create SPA.
		School_PFS_Assignment__c spa = SchoolPfsAssignmentTestData.Instance
		    .forAcademicYearPicklist(academicYear.Name)
		    .forApplicantId(applicant.Id)
		    .forSchoolId(school.Id)
		    .forStudentFolderId(folder.Id)
		    .insertSchoolPfsAssignment();
    }
    
    @isTest
    private static void getMonthlyNetIncome_expectNetTotalGreaterThanZero() {
        
        Setup();
        
        pfs.Alimony_Current__c = 30000;
        pfs.Child_Support_Received_Current__c = 15000;
        pfs.Monthly_Income_Parents_After_Taxes__c = 150000;
        pfs.Monthly_Income_Applicants_After_Taxes__c = 100;
        pfs.Other_Monthly_Income__c = 2000;
        
        
        pfs.Monthly_Expenses_Alimony__c = 100;
        pfs.Monthly_Expenses_Child_Support__c = 50;
        pfs.Other_Monthly_Support_Expenses__c = 0;
        
        pfs.Monthly_Expenses_Mortage_Rent_primary__c = 1500;
        pfs.Monthly_Expenses_Utilities__c = 100;
        pfs.Monthly_Expenses_Property_Taxes_Expenses__c = 0;
        pfs.Monthly_Expenses_Home_s_Owner_Insurance__c = 250;
        pfs.Monthly_Expenses_Association_Fees__c = 0;
        pfs.Monthly_Expenses_Maintenance__c = 0;
        pfs.Monthly_Expenses_Mortage_Rent_all__c = 0;
        pfs.Other_Monthly_Housing_Expenses__c = 0;
        
        pfs.Monthly_Expenses_Tuition__c = 600;
        pfs.Monthly_Expenses_Childcare__c = 400;
        pfs.Other_Monthly_Tuition_Childcare_Expenses__c = 50;
        
        
        pfs.Monthly_Expenses_Vehicle_Payments__c = 0;
        pfs.Monthly_Expenses_Vehicle_Insurance__c = 150;
        pfs.Monthly_Expenses_Vehicle_Maintenance__c = 10;
        pfs.Other_Monthly_Expenses_Vehicle__c = 60;
        
        pfs.Monthly_Expenses_Healthcare_Insurance__c = 100;
        pfs.Monthly_Expenses_Dental_Insurance__c = 30;
        pfs.Monthly_Expenses_Life_Insurance__c = 120;
        pfs.Monthly_Expenses_Insurance_not_covered__c = 0;
        pfs.Other_Monthly_Healthcare_Expenses__c = 60;
        
        pfs.Monthly_Expenses_Groceries__c = 1200;
        pfs.Monthly_Expenses_Dining_out__c = 200;
        pfs.Other_Monthly_Food_Expenses__c = 0;
        
        
        update pfs;
        
        Test.StartTest();
        
            pfs = [SELECT Id, Monthly_Net_Income__c FROM PFS__c WHERE Id =: pfs.Id LIMIT 1];
            
            System.AssertEquals(true, pfs.Monthly_Net_Income__c > 0);
        
        Test.StopTest();
        
    }
    
    @isTest
    private static void getMonthlyNetIncome_expectNetTotalLessThanZero() {
        
        Setup();
        
        pfs.Alimony_Current__c = 0;
        pfs.Child_Support_Received_Current__c = 0;
        pfs.Monthly_Income_Parents_After_Taxes__c = 3000;
        pfs.Monthly_Income_Applicants_After_Taxes__c = 0;
        pfs.Other_Monthly_Income__c = 0;
        
        
        pfs.Monthly_Expenses_Alimony__c = 0;
        pfs.Monthly_Expenses_Child_Support__c = 0;
        pfs.Other_Monthly_Support_Expenses__c = 0;
        
        pfs.Monthly_Expenses_Mortage_Rent_primary__c = 1500;
        pfs.Monthly_Expenses_Utilities__c = 100;
        pfs.Monthly_Expenses_Property_Taxes_Expenses__c = 0;
        pfs.Monthly_Expenses_Home_s_Owner_Insurance__c = 250;
        pfs.Monthly_Expenses_Association_Fees__c = 0;
        pfs.Monthly_Expenses_Maintenance__c = 0;
        pfs.Monthly_Expenses_Mortage_Rent_all__c = 0;
        pfs.Other_Monthly_Housing_Expenses__c = 0;
        
        pfs.Monthly_Expenses_Tuition__c = 600;
        pfs.Monthly_Expenses_Childcare__c = 400;
        pfs.Other_Monthly_Tuition_Childcare_Expenses__c = 50;
        
        
        pfs.Monthly_Expenses_Vehicle_Payments__c = 0;
        pfs.Monthly_Expenses_Vehicle_Insurance__c = 150;
        pfs.Monthly_Expenses_Vehicle_Maintenance__c = 10;
        pfs.Other_Monthly_Expenses_Vehicle__c = 60;
        
        pfs.Monthly_Expenses_Healthcare_Insurance__c = 100;
        pfs.Monthly_Expenses_Dental_Insurance__c = 30;
        pfs.Monthly_Expenses_Life_Insurance__c = 120;
        pfs.Monthly_Expenses_Insurance_not_covered__c = 0;
        pfs.Other_Monthly_Healthcare_Expenses__c = 60;
        
        pfs.Monthly_Expenses_Groceries__c = 1200;
        pfs.Monthly_Expenses_Dining_out__c = 200;
        pfs.Other_Monthly_Food_Expenses__c = 0;
        
        
        update pfs;
        
        Test.StartTest();
        
            pfs = [SELECT Id, Monthly_Net_Income__c FROM PFS__c WHERE Id =: pfs.Id LIMIT 1];
            
            System.AssertEquals(true, pfs.Monthly_Net_Income__c < 0);
        
        Test.StopTest();
        
    }
    
    private static Account schoolMIE1, schoolMIE2, schoolMIE3, schoolMIE4;
    private static Annual_Setting__c annualSettingMIE1, annualSettingMIE2, annualSettingMIE3, annualSettingMIE4;
    private static PFS__c pfsMIE1, pfsMIE2, pfsMIE3;
    private static Student_Folder__c folder11, folder12, folder23, folder24,folder34, folder44;
    private static User schoolPortalUserMIE;
    
    private static void SetupMIE() {
        
        //Create current academic year.
        Academic_Year__c academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        
        //Create test Schools.
        schoolMIE1 = AccountTestData.Instance.asSchool().forName('schoolMIE1').create();
        schoolMIE2 = AccountTestData.Instance.asSchool().forName('schoolMIE2').create();
        schoolMIE3 = AccountTestData.Instance.asSchool().forName('schoolMIE3').create();
        schoolMIE4 = AccountTestData.Instance.asSchool().forName('schoolMIE4').create();
        Account aParent1 = AccountTestData.Instance.asFamily().create();
        Account aParent2 = AccountTestData.Instance.asFamily().create();
        Account aParent3 = AccountTestData.Instance.asFamily().create();
        insert new List<Account>{schoolMIE1, schoolMIE2, schoolMIE3, schoolMIE4, aParent1, aParent2, aParent3};
        
        //Create Annual Setting records for each school.
		annualSettingMIE1 = AnnualSettingsTestData.Instance
		    .forSchoolId(schoolMIE1.Id)
		    .forAcademicYearId(academicYear.Id)
		    .create();
	    annualSettingMIE2 = AnnualSettingsTestData.Instance
            .forSchoolId(schoolMIE2.Id)
            .forAcademicYearId(academicYear.Id)
            .create();
        annualSettingMIE3 = AnnualSettingsTestData.Instance
            .forSchoolId(schoolMIE3.Id)
            .forAcademicYearId(academicYear.Id)
            .create();
        annualSettingMIE4 = AnnualSettingsTestData.Instance
            .forSchoolId(schoolMIE4.Id)
            .forAcademicYearId(academicYear.Id)
            .create();
        insert new List<Annual_Setting__c>{annualSettingMIE1, annualSettingMIE2, annualSettingMIE3, annualSettingMIE4};
        
        
        //Create 3 parents and 3 students.
        Contact parent1 = ContactTestData.Instance
            .asParent()
            .forAccount(aParent1.Id)
            .forEmail('aParent1@test.com')
            .forFirstName('aParent1')
            .forLastName('aParent1')
            .create();
        Contact parent2 = ContactTestData.Instance.asParent()
            .asParent()
            .forAccount(aParent2.Id)
            .forEmail('aParent2@test.com')
            .forFirstName('aParent2')
            .forLastName('aParent2')
            .create();
        Contact parent3 = ContactTestData.Instance.asParent()
            .asParent()
            .forAccount(aParent3.Id)
            .forEmail('aParent3@test.com')
            .forFirstName('aParent3')
            .forLastName('aParent3')
            .create();
        Contact student1 = ContactTestData.Instance.asStudent().create();
        Contact student2 = ContactTestData.Instance.asStudent().create();
        Contact student3 = ContactTestData.Instance.asStudent().create();
        Contact student4 = ContactTestData.Instance.asStudent().create();
        insert new List<Contact>{parent1, parent2, parent3, student1, student2, student3, student4};
        
        
        //Create 3 PFS records.
		pfsMIE1 = PfsTestData.Instance
		    .forParentA(parent1.Id)
		    .forAcademicYearPicklist(academicYear.Name)
		    .asPaid()
		    .create();
        pfsMIE2 = PfsTestData.Instance
            .forParentA(parent2.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .asPaid()
            .create();
        pfsMIE3 = PfsTestData.Instance
            .forParentA(parent3.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .asPaid()
            .create();
        insert new List<PFS__c>{pfsMIE1, pfsMIE2, pfsMIE3};
        
        //Create 3 applicants.
		Applicant__c applicant1 = ApplicantTestData.Instance
		    .forContactId(student1.Id)
		    .forPfsId(pfsMIE1.Id)
		    .create();
        Applicant__c applicant2 = ApplicantTestData.Instance
            .forContactId(student2.Id)
            .forPfsId(pfsMIE2.Id)
            .create();
        Applicant__c applicant3 = ApplicantTestData.Instance
            .forContactId(student3.Id)
            .forPfsId(pfsMIE3.Id)
            .create();
        Applicant__c applicant4 = ApplicantTestData.Instance
            .forContactId(student4.Id)
            .forPfsId(pfsMIE1.Id)
            .create();
        insert new List<Applicant__c>{applicant1, applicant2, applicant3, applicant4};
        
        //Create Folders
		folder11 = StudentFolderTestData.Instance.forName('Folder Applicant1-schoolMIE1')
		    .forAcademicYearPicklist(academicYear.Name)
		    .forStudentId(student1.Id)
		    .forSchoolId(schoolMIE1.Id)
		    .create();
        folder12 = StudentFolderTestData.Instance.forName('Folder Applicant1-schoolMIE2')
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student1.Id)
            .forSchoolId(schoolMIE2.Id)
            .create();
        folder23 = StudentFolderTestData.Instance.forName('Folder Applicant2-schoolMIE3')
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student2.Id)
            .forSchoolId(schoolMIE3.Id)
            .create();
        folder24 = StudentFolderTestData.Instance.forName('Folder Applicant2-schoolMIE4')
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student2.Id)
            .forSchoolId(schoolMIE4.Id)
            .create();
        folder34 = StudentFolderTestData.Instance.forName('Folder Applicant3-schoolMIE4')
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student3.Id)
            .forSchoolId(schoolMIE4.Id)
            .create();
        folder44 = StudentFolderTestData.Instance.forName('Folder Applicant4-schoolMIE4')
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student4.Id)
            .forSchoolId(schoolMIE4.Id)
            .create();
        insert new List<Student_Folder__c>{folder11, folder12, folder23, folder24,folder34, folder44};
        
        //Create 4 SPA records (2 for each applicant).
		School_PFS_Assignment__c spa11 = SchoolPfsAssignmentTestData.Instance
		    .forAcademicYearPicklist(academicYear.Name)
		    .forApplicantId(applicant1.Id)
		    .forSchoolId(schoolMIE1.Id)
		    .forStudentFolderId(folder11.Id)
		    .create();
        School_PFS_Assignment__c spa12 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forApplicantId(applicant1.Id)
            .forSchoolId(schoolMIE2.Id)
            .forStudentFolderId(folder12.Id)
            .create();
        School_PFS_Assignment__c spa23 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forApplicantId(applicant2.Id)
            .forSchoolId(schoolMIE3.Id)
            .forStudentFolderId(folder23.Id)
            .create();
        School_PFS_Assignment__c spa24 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forApplicantId(applicant2.Id)
            .forSchoolId(schoolMIE4.Id)
            .forStudentFolderId(folder24.Id)
            .create();
        School_PFS_Assignment__c spa34 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forApplicantId(applicant3.Id)
            .forSchoolId(schoolMIE4.Id)
            .forStudentFolderId(folder34.Id)
            .create();
        School_PFS_Assignment__c spa44 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forApplicantId(applicant4.Id)
            .forSchoolId(schoolMIE4.Id)
            .forStudentFolderId(folder44.Id)
            .create();
        insert new List<School_PFS_Assignment__c>{spa11, spa12, spa23, spa24, spa34, spa44};
        
        Contact staff = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(schoolMIE4.Id)
            .insertContact();
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            
            schoolPortalUserMIE = UserTestData.Instance
                .forUsername('SchoolPortalUser@test.com')
                .forContactId(staff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId)
                .insertUser();
                
            Test.startTest();                
                // call scheduled batch Apex to process new access pfs and folders.
                Database.executeBatch(new SchoolStaffShareBatch());
            Test.stopTest();
        }
    }
    
    @isTest
    private static void MIEResponseBulk_TwoPFSWithMIERequiredAndOneWithout_expectOnePfsToRequestIndividualMIE() {
        
        // We use the following test data from SetupMIE:
        //pfsMIE1:
        //  * Applicant1: Has applied for
        //      ** schoolMIE1
        //      ** schoolMIE2
        //      ** schoolMIE4
        //  * Applicant 4: Has applied for
        //      ** schoolMIE4
        //pfsMIE2:
        //  * Applicant2: Has applied for
        //      ** schoolMIE3
        //      ** schoolMIE4
        //pfsMIE3:
        //  * Applicant3: Has applied for
        //      ** schoolMIE4
        //This test covers the following cases:
        //  1. We set AnnualSetting.CollectMIE=Yes for schoolMIE3, this means that pfsMIE2 already has required MIE 
        //     (By default StatMonthlyIncomeExpenses__c is false). But, the parent have not completed it yet.
        //  2. We set pfsMIE3.MIE_Requested__c=true and StatMonthlyIncomeExpenses__c=true, this means that pfsMIE3 already 
        //     has required MIE. And, the parent have already completed it.
        //  3. The PFS for which we can request MIE Individually ends up being pfsMIE1.
        SetupMIE();
        
        SchoolPortalSettings.MIE_Pilot = true;
        
        annualSettingMIE3.Collect_MIE__c = 'Yes';
        update annualSettingMIE3;
        
        pfsMIE3.MIE_Requested__c = true;        
        pfsMIE3.StatMonthlyIncomeExpenses__c = true;
        update pfsMIE3;
        
        System.runAs(schoolPortalUserMIE){
            
            Set<Id> folderIds = new Set<Id>{folder24.Id, folder34.Id, folder44.Id};
            MonthlyIncomeAndExpensesService.MIERequestBulk request = new MonthlyIncomeAndExpensesService.MIERequestBulk(folderIds);
            
            MonthlyIncomeAndExpensesService.MIEResponseBulk response = new MonthlyIncomeAndExpensesService.MIEResponseBulk(request, true);
            
            System.AssertEquals(3, response.pfsRequestedCounter);
            
            System.AssertEquals(null, response.errorMessage);
            
            System.AssertEquals(1, response.pfsRequestedAndIncompletedCounter,
                'We set AnnualSetting.CollectMIE=Yes for schoolMIE3, this means that pfsMIE2 already has required MIE ' +
                '(By default StatMonthlyIncomeExpenses__c is false). But, the parent have not completed it yet.');
            System.AssertEquals(true, response.pfsRequestedAndIncompletedIds.contains(pfsMIE2.Id));
            
            System.AssertEquals(1, response.pfsAlreadyCompletedCounter,
                'We set pfsMIE3.MIE_Requested__c=true and StatMonthlyIncomeExpenses__c=true, this means that pfsMIE3 already ' +
                'has required MIE. And, the parent have already completed it.');
            System.AssertEquals(true, response.pfsAlreadyCompletedIds.contains(pfsMIE3.Id));
            
            System.AssertEquals(1, response.pfsUpdatedCounter, 
                'The PFS for which we can request MIE Individually must be pfsMIE1.');
            System.AssertEquals(true, response.pfsUpdatedIds.contains(pfsMIE1.Id));
        }
    }//End:MIEResponseBulk_TwoPFSWithMIERequiredAndOneWithout_expectOnePfsToRequestIndividualMIE
    
    @isTest
    private static void MIEResponseBulk_ThreePFSRelatedToAnnualSettingThatCollectMIE_expectZeroPfsToRequestIndividualMIE() {
        
        // We use the following test data from SetupMIE:
        //pfsMIE1:
        //  * Applicant1: Has applied for
        //      ** schoolMIE1
        //      ** schoolMIE2
        //      ** schoolMIE4
        //  * Applicant 4: Has applied for
        //      ** schoolMIE4
        //pfsMIE2:
        //  * Applicant2: Has applied for
        //      ** schoolMIE3
        //      ** schoolMIE4
        //pfsMIE3:
        //  * Applicant3: Has applied for
        //      ** schoolMIE4
        //This test covers the following cases:
        //  1. We set AnnualSetting.CollectMIE=Yes for schoolMIE4, this means that pfsMIE1 and pfsMIE2 and pfsMIE3 has already
        //     required MIE. By default StatMonthlyIncomeExpenses__c is false, and we set it to true for pfsMIE2. In order, to 
        //     label pfsMIE2 as MIE requested and completed by its parent.
        SetupMIE();
        
        SchoolPortalSettings.MIE_Pilot = true;
        
        annualSettingMIE4.Collect_MIE__c = 'Yes';
        update annualSettingMIE4;  
             
        pfsMIE2.StatMonthlyIncomeExpenses__c = true;
        update pfsMIE2;
        
        System.runAs(schoolPortalUserMIE){
            
            Set<Id> folderIds = new Set<Id>{folder24.Id, folder34.Id, folder44.Id};
            MonthlyIncomeAndExpensesService.MIERequestBulk request = new MonthlyIncomeAndExpensesService.MIERequestBulk(folderIds);
            
            MonthlyIncomeAndExpensesService.MIEResponseBulk response = new MonthlyIncomeAndExpensesService.MIEResponseBulk(request, true);
            
            System.AssertEquals(null, response.errorMessage);
            
            System.AssertEquals(3, response.pfsRequestedCounter);
            
            System.AssertEquals(2, response.pfsRequestedAndIncompletedCounter);
            System.AssertEquals(true, response.pfsRequestedAndIncompletedIds.contains(pfsMIE1.Id));
            System.AssertEquals(true, response.pfsRequestedAndIncompletedIds.contains(pfsMIE3.Id));
            
            System.AssertEquals(1, response.pfsAlreadyCompletedCounter);
            System.AssertEquals(true, response.pfsAlreadyCompletedIds.contains(pfsMIE2.Id));
            
            System.AssertEquals(0, response.pfsUpdatedCounter);
        }
    }//End:MIEResponseBulk_ThreePFSRelatedToAnnualSettingThatCollectMIE_expectZeroPfsToRequestIndividualMIE
    
    @isTest
    private static void MIEResponseBulk_SchoolPortalSettingMIEPilotIsDisabled_expectErrorMessage() {
        
        //SchoolPortalSettings.MIE_Pilot must be enable to be able to run the logic to request individual MIE.
        
        SchoolPortalSettings.MIE_Pilot = false;
        
        MonthlyIncomeAndExpensesService.MIERequestBulk request = new MonthlyIncomeAndExpensesService.MIERequestBulk(new Set<Id>{});
        
        MonthlyIncomeAndExpensesService.MIEResponseBulk response = new MonthlyIncomeAndExpensesService.MIEResponseBulk(request, true);
        
        System.AssertEquals(true, response.errorMessage.contains(Label.MIE_Pilot_Flag_Disabled_Message));
        
    }//End:MIEResponseBulk_SchoolPortalSettingMIEPilotIsDisabled_expectErrorMessage
    
    private static void showHideMIEMenuFromSchoolPortalBasedOnPFSMIEStats(Boolean statMIEIsCompleted, Boolean expectValue) {
        
        SetupMIE();
        
        SchoolPortalSettings.MIE_Pilot = true;
        
        annualSettingMIE1.Collect_MIE__c = 'No';
        annualSettingMIE2.Collect_MIE__c = 'No';
        annualSettingMIE4.Collect_MIE__c = 'No';
        update new List<Annual_Setting__c>{ annualSettingMIE1, annualSettingMIE2, annualSettingMIE4 };  
             
        pfsMIE1.StatMonthlyIncomeExpenses__c = statMIEIsCompleted;
        pfsMIE1.MIE_Requested__c = false;
        pfsMIE1.MIE_Requested_By_School_Id__c = null;
        pfsMIE1.MIE_Requested_By_School_Name__c = null;
        pfsMIE1.MIE_Requested_Date__c = null;
        update pfsMIE1;
        
        System.runAs(schoolPortalUserMIE){
            
            List<Student_Folder__c> folders = new List<Student_Folder__c>([
                SELECT Id FROM Student_Folder__c WHERE Id =: folder44.Id]);
            
            System.AssertEquals(1, folders.size());
            
            //The MIE Menu must be shown in StudentFolder page from School Portal.
            System.AssertEquals(expectValue, MonthlyIncomeAndExpensesService.isMIEActive(folder44.Id));
        }
    }//End:showHideMIEMenuFromSchoolPortalBasedOnPFSMIEStats
    
    @isTest
    private static void isMIEActive_pfsMIEIsMarkedAsCompleted_expectMIEIsActive() {
        
        //This scenario is for a pfs record for which MIE is marked as completed.
        //And there's no related annualSetting record with Collect_MIE__c = 'Yes'.
        //And the pfs.MIE_Requested__c = false.
        //This may happend when a School requires MIE and a parent completes the 
        //MIE section for his pfs. Then, the StatMonthlyIncomeExpenses__c is marked as true.
        //After that the school revertes the  Collect_MIE__c = 'No'.        
        //This means that the SP user should see the MIE menu for folder44.
        showHideMIEMenuFromSchoolPortalBasedOnPFSMIEStats(true, true);
        
    }//End:isMIEActive_pfsMIEIsMarkedAsCompleted_expectMIEIsActive
    
    @isTest
    private static void isMIEActive_pfsMIEIsMarkedAsCompleted_expectMIEIsInactive() {
        
        //This scenario is for a pfs record for which MIE is marked as completed.
        //And there's no related annualSetting record with Collect_MIE__c = 'Yes'.
        //And the pfs.MIE_Requested__c = false.
        //And also the pfs.StatMonthlyIncomeExpenses__c is false.
        //This means that the SP user should NOT see the MIE menu for folder44.
        
        showHideMIEMenuFromSchoolPortalBasedOnPFSMIEStats(false, false);
        
    }//End:isMIEActive_pfsMIEIsMarkedAsCompleted_expectMIEIsInactive
}