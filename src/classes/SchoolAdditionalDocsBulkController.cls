public with sharing class SchoolAdditionalDocsBulkController implements ISchoolRequireAdditionalDoc {
    
    //Private Variables
    private static String SOURCE_BULK_ADDITIONAL_DOCS = 'Mass Doc Requirement - My Applicants';
    private Decimal MAX_LIMIT_FOLDERS;
    private Boolean hasMultipleAcademicYears { get; set; }
    private ApexPages.StandardSetController controllerStandard { get; set; }
    
    private Map<Id, Student_Folder__c> folders {
        get{
            if (folders == null) {
                folders = new Map<Id, Student_Folder__c>((Student_Folder__c[])controllerStandard.getSelected());
            }
            
            return folders;
        }
        private set;
    }
    
    //Public Variables
    public List<Id> spaIds { get; private set; }
    public String academicYearString { get; private set; }
    public String resultMessage { get; private set; }
    
    public SchoolAdditionalDocsBulkController(ApexPages.StandardSetController controller)
    {
        MAX_LIMIT_FOLDERS = SchoolPortalSettings.Require_Additional_Documents_Bulk_Limit;
        additionalSchoolDocAssign = new School_Document_Assignment__c();
        hasMultipleAcademicYears = false;
        controllerStandard = controller;
        resultMessage = null;
    }//End-Constructor
    
    /**
    * @description Set the ids retrieved from the StandardSetController to folderIds.
    * @param controller The StandardSetController that represent the folders selected by the user.
    */
    private void parseSelectedFolderIds()
    {

        if( !folders.isEmpty() ) {
            
            Set<Id> folderIds = folders.keySet();
	        for(Student_Folder__c folder : [SELECT Id, Academic_Year_Picklist__c FROM Student_Folder__c WHERE ID IN :folderIds ] )
	        {
	            folders.put(folder.Id, folder);
	            
	            if( String.isBlank(academicYearString) ) {
	                
                    academicYearString = folder.Academic_Year_Picklist__c;
                    
	            } else if( academicYearString != folder.Academic_Year_Picklist__c ) {
	                
	                hasMultipleAcademicYears = true;
	                break;
	            }
	        }
        }
    }
    
    public PageReference saveNewDocument() {
        
        if (!isValidRequest()) {   
                     
            return null;
        } else if (!getIsTaxDocument()) {
            
            additionalSchoolDocAssign.Document_Year__c = String.valueOf(Integer.valueOf(academicYearString.split('-')[0]) - 1);
        } 
        
        DocumentRequirementService.Request request = new DocumentRequirementService.Request(
            additionalSchoolDocAssign.Document_Type__c, additionalSchoolDocAssign.Document_Year__c, folders.keyset(), true, true);
        
        request.source = SOURCE_BULK_ADDITIONAL_DOCS;
        
        DocumentRequirementService.Response response = DocumentRequirementService.Instance.requestAdditionalDocuments(request);
        
        resultMessage = String.format(
            Label.Multiple_SDA_Request_Success_Message, 
            new List<String>{additionalSchoolDocAssign.Document_Type__c, String.ValueOf(response.totalNewSDAs)});
        
        return null;
    }
    
    private Boolean isValidRequest() {
        
        if ( String.isBlank(additionalSchoolDocAssign.Document_Type__c) || (getIsTaxDocument() && string.isBlank(additionalSchoolDocAssign.Document_Year__c) )) {
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please complete required information.'));
        } else if (folders.size() > MAX_LIMIT_FOLDERS) {
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select no more than ' + MAX_LIMIT_FOLDERS +' applicants.'));
        } else if (!GlobalVariables.isEditableYear(academicYearString)) {
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Multiple_SDA_Request_Year_Not_Editable));
        } else if(hasMultipleAcademicYears) {
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You must select applicants from the same academic year.'));
        } else {
            return true;
        }
        
        return false;
    }
    
    public PageReference saveAndNewAdditionalDocument() {
        return null;
    }
    
    public PageReference saveAndUploadRequiredDocuments() {
        return null;
    }
    
    public PageReference cancelAdditionalDocument() {
        return null;
    }
    
    private School_Document_Assignment__c additionalSchoolDocAssign;
    public School_Document_Assignment__c getAdditionalSchoolDocAssign() {        
        return additionalSchoolDocAssign;
    }
    
    private List<SelectOption> nonSchoolSpecificDocumentTypes;
    public List<SelectOption> getNonSchoolSpecificDocumentTypes() {        
        if( nonSchoolSpecificDocumentTypes == null )
        {
            Set<String> optionsToExclude = new Set<String>{
                'Other/Misc Tax Form',
                ApplicationUtils.schoolSpecificDocType()};  
                          
            nonSchoolSpecificDocumentTypes = new List<SelectOption>{ new SelectOption('', '--None--') };
            
            for (Schema.PicklistEntry p : School_Document_Assignment__c.Document_Type__c.getDescribe().getPicklistValues())
            {
                String picklistValue = p.getValue();
                if( !optionsToExclude.contains(picklistValue) ) {
                    nonSchoolSpecificDocumentTypes.add(new SelectOption(picklistValue, picklistValue));
                }
            }
        }
        return nonSchoolSpecificDocumentTypes;
    }
    
    private Boolean premiumSubscription {
        get {
            Subscription__c subscription = GlobalVariables.getMostRecentSubscription();            
            if ( subscription.Subscription_Type__c == 'Premium' && subscription.Status__c == 'Current' ) {
                return true;
            } else {
                return false;
            }
        } set;        
    }
    
    private List<SelectOption> validDocumentYears;
    public List<SelectOption> getValidDocumentYears() {
        if(validDocumentYears == null && academicYearString != null) {
                
            validDocumentYears = new List<SelectOption>();
            validDocumentYears.add(new SelectOption('', '--None--'));
            Integer academicYearInt = Integer.valueOf(academicYearString.split('-')[0]);
            Integer forLoopVar = premiumSubscription ? academicYearInt - 3 : academicYearInt - 2;
            
            for(Integer i = forLoopVar; i < academicYearInt; i++){
                
                validDocumentYears.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
            }
        }
        
        return validDocumentYears;
    }
    
    public Boolean getIsTaxDocument() {
        return additionalSchoolDocAssign != null ? ApplicationUtils.isTaxDocument(additionalSchoolDocAssign.Document_Type__c) : false;
    }
    
    public PageReference loadDocumentYears() {
        parseSelectedFolderIds();
        return null;
    }
    
    public SchoolAdditionalDocsBulkController getMe() {
        return this;
    }
    
    public String getSubtitle() {
        return String.format(Label.Multiple_SDA_Request_Selected_Folders, new List<String>{ String.ValueOf(folders.size()) });
    }
    
    public Boolean isPopup {
        get {
            
            if (isPopup == null) {
                
                isPopup = ApexPages.currentPage().getParameters().get('popup') == 'yes';
            }
            
            return isPopup;
        }
        private set;
    }
    
    public String getIds() {
        
        String ids = '';
        for (Id fId :folders.keyset()) {
            
            ids += 'ids=' + String.ValueOf(fId) + '&';
        }
        
        return ids;
    }
}