@isTest 
private without sharing class FamilyAppViewPFSPDFBulkControllerTest {

    private static User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    private static Student_Folder__c studentFolder1, studentFolder2;

    private static void createTestData() {
        Account family = AccountTestData.Instance.asFamily().create();
        Account school = AccountTestData.Instance.asSchool().create();
        insert new List<Account>{family, school};

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
        Academic_Year__c previousAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, previousAcademicYear};

        Contact parentA = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact parentB = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(school.Id).create();
        Contact student1 = ContactTestData.Instance
            .asStudent()
            .forAccount(family.Id).create();
        Contact student2 = ContactTestData.Instance
            .asStudent()
            .forAccount(family.Id).create();
        insert new List<Contact> {schoolStaff, parentA, parentB, student1, student2};

        User schoolPortalUser;
        System.runAs(currentUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);
        String pfsStatusUnpaid = 'Unpaid';
        String folderSource = 'PFS';

        System.runAs(schoolPortalUser){
            PFS__c pfs1 = PfsTestData.Instance
                .asSubmitted()
                .forPaymentStatus(pfsStatusUnpaid)
                .forParentA(parentA.Id)
                .forOriginalSubmissionDate(System.today().addDays(-3))
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            PFS__c pfs2 = PfsTestData.Instance
                .asSubmitted()
                .forPaymentStatus(pfsStatusUnpaid)
                .forParentA(parentB.Id)
                .forOriginalSubmissionDate(System.today().addDays(-5))
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            PFS__c pfs1Past = PfsTestData.Instance
                .asSubmitted()
                .forPaymentStatus(pfsStatusUnpaid)
                .forParentA(parentA.Id)
                .forOriginalSubmissionDate(System.today().addDays(-3).addYears(-1))
                .forAcademicYearPicklist(previousAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<PFS__c> {pfs1, pfs2, pfs1Past});

            Applicant__c applicant1A = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student1.Id).create();
            Applicant__c applicant1B = ApplicantTestData.Instance
                .forPfsId(pfs2.Id)
                .forContactId(student1.Id).create();
            Applicant__c applicant2A = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student2.Id).create();
            Applicant__c applicant1PAST = ApplicantTestData.Instance
                .forPfsId(pfs1Past.Id)
                .forContactId(student1.Id).create();
            Dml.WithoutSharing.insertObjects(new List<Applicant__c> {applicant1A, applicant1B, applicant2A, applicant1PAST});

            studentFolder1 = StudentFolderTestData.Instance
                .forFolderSource(folderSource)
                .forStudentId(student1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            studentFolder2 = StudentFolderTestData.Instance
                .forFolderSource(folderSource)
                .forStudentId(student2.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Student_Folder__c studentFolder1Past = StudentFolderTestData.Instance
                .forFolderSource(folderSource)
                .forStudentId(student1.Id)
                .forAcademicYearPicklist(previousAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<Student_Folder__c> {studentFolder1, studentFolder2, studentFolder1Past});

            String fifthGrade = '5';
            School_PFS_Assignment__c spfsa1 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1A.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            School_PFS_Assignment__c spfsa2 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1B.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            School_PFS_Assignment__c spfsa3 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant2A.Id)
                .forStudentFolderId(studentFolder2.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> {spfsa1, spfsa2 ,spfsa3});
        }

        // Share records to school staff
        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);
    }

    @isTest
    private static void testBulkConstructerTwoFolders(){
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);

        createTestData();

        System.runAs(currentUser){
            Test.StartTest();
                List<Student_Folder__c> folders = new List<Student_Folder__c>();
                folders.add(studentFolder1);
                folders.add(studentFolder2);
                
                ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(folders);
                ApexPages.currentPage().getParameters().put('ids', studentFolder2.id);
                ssc.setSelected(folders);
                
                FamilyAppViewPFSPDFBulkController controller = new FamilyAppViewPFSPDFBulkController(ssc);
                System.assertEquals(2, controller.pfsList.size());
            Test.stopTest();
        }
    }
}