public class FamilyMessagesEditController{
    private FamilyTemplateController controller;
    
    public FamilyMessagesEditController (FamilyTemplateController c) {
        this.controller = c;
        if(this.controller.pfsRecord == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error: No PFS Record Found for current user.'));
            this.controller.pfsRecord = new PFS__c();
        }
        
        this.currentCaseId = ApexPages.currentPage().getParameters().get('id');
        this.currentCase = [SELECT
                                Id,
                                CaseNumber,
                                Status,
                                Student_s_Name__c,
                                Subject,
                                Description,
                                SSS_School_Code__c,
                                School_Name__c,
                                School_Code__c,
                                Account.Name,
                                RecordType.Name,
                                CreatedDate,
                                CreatedById,
                                ClosedDate,
                                LastModifiedDate,
                                CreatedBy.Contact.Account.Name,
                                CreatedBy.Contact.Account.SSS_School_Code__c,
                                Applicant__c, Applicant__r.First_Name__c, Applicant__r.Last_Name__c
                            FROM
                                Case
                            WHERE
                                Id = :this.currentCaseId
                            LIMIT 1];
                            
        System.debug('FamilyMessagesEditController.init.currentCase: ' + this.currentCase);
    }//End-Constructor
    
    public static FamilyTemplateController.config loadPFS(String pfsId, String academicYearId)
    {
        PFS__c pfsRecord;
        User currentUser = [Select Id, ContactId, ProfileId from User where Id = :UserInfo.getUserId()];
        Academic_Year__c academicYear = [select Id, Name from Academic_Year__c where Id = :academicYearId];
        
        List<PFS__c> pfsRecords = null;
        if (GlobalVariables.isSysAdminUser(currentUser) || GlobalVariables.isCallCenterUser(currentUser)){
            pfsRecords = [SELECT
                            Id, Own_Business_or_Farm__c, Academic_Year_Picklist__c, Fee_Waived__c, Parent_A__c, PFS_Status__c, Payment_Status__c, PFS_Number__c
                        FROM
                            PFS__c
                        WHERE
                            Id = :pfsId];
            if(!pfsRecords.isEmpty()) pfsRecord = pfsRecords[0];
            // if this not a sys admin or call center user, get record based on contact id and academic year
        } else {
            pfsRecords = [SELECT
                           Id, Own_Business_or_Farm__c, Academic_Year_Picklist__c, Fee_Waived__c,Parent_A__c, PFS_Status__c,  Payment_Status__c, PFS_Number__c
                        FROM
                            PFS__c
                        WHERE
                            Parent_A__c = :currentUser.ContactId
                            AND Academic_Year_Picklist__c = :academicYear.Name];
            if(!pfsRecords.isEmpty()) pfsRecord = pfsRecords[0];
        }
        return new FamilyTemplateController.config(pfsRecord, pfsRecords);
    }//End:loadPFS
    
    public String currentCaseId{get; set;}
    public Case currentCase{get; set;}
    public Family_Document__c familyDoc {get; set;}
    
    public String ParentCallRecordTypeId {
        get {
            return RecordTypes.parentCallCaseTypeId;
        }
    }
}