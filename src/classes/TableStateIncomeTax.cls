public with sharing class TableStateIncomeTax
{

    private static List<State_Tax_Table__c> getStateTaxTableData(Id academicYearId, String area)
    {
        Map<String, List<State_Tax_Table__c>> stateTaxTablesMapping = stateTaxTableCache.getTaxTables(academicYearId);
        List<State_Tax_Table__c> stateTaxTableData = stateTaxTablesMapping.get(area);

        return stateTaxTableData;
    }

    public static Decimal calculateTax(Id academicYearId, String state, String country, Decimal taxableIncome)
    {
        Decimal calculatedTax = null;
        
        if (taxableIncome != null)
        {
            // make sure the taxable income is rounded to the nearest whole number
            Decimal roundedTaxableIncome = taxableIncome.round(RoundingMode.HALF_UP);
            
            // [CH] NAIS-1659 Implementing logic for countries
            List<State_Tax_Table__c> stateTaxTableData = getStateTaxTableData(academicYearId, state);
            
            // If there's no match on the State value 
            if(stateTaxTableData == null || stateTaxTableData.size() == 0)
            {
                // Try a match on country
                stateTaxTableData = getStateTaxTableData(academicYearId, country);
            }
            
            // If there's no match on the Country value 
            if(stateTaxTableData == null || stateTaxTableData.size() == 0)
            {
                // Use the default
                stateTaxTableData = getStateTaxTableData(academicYearId, 'Not Reported');
            }
            
            if (stateTaxTableData != null)
            {
                State_Tax_Table__c matchingBracket = null;
                // assume the tax data is sorted in ascending order by the Income_Low__c field
                for (State_Tax_Table__c stateTaxTable : stateTaxTableData)
                {
                    if (roundedTaxableIncome >= stateTaxTable.Income_Low__c)
                    {
                        if ((stateTaxTable.Income_High__c == null) || (roundedTaxableIncome <= stateTaxTable.Income_High__c))
                        {
                            matchingBracket = stateTaxTable;
                            break;
                        }
                    }
                }
                
                if (matchingBracket != null)
                {    
                    calculatedTax = (roundedTaxableIncome * matchingBracket.Percent_of_Total_Income__c).round(RoundingMode.HALF_UP);    
                }
            }
        }

        if (calculatedTax == null)
        {
            // NAIS-980 - if the state is not found, then return 0 instead of throwing an exception
            // throw new EfcException('No state tax table record found for academic year \'' + EfcUtil.getAcademicYearName(academicYearId) + '\' for State = ' + state + ' and income = ' + taxableIncome);
            calculatedTax = 0;
        }
        
        return calculatedTax;
    }
}