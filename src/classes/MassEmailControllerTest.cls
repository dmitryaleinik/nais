/**
 * MassEmailControllerTest.cls
 *
 * @description: Test class for MassEmailController using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
private class MassEmailControllerTest {
    
    /* test data setup */
    @testSetup static void setupTestData() {
        
        MassEmailSendTestDataFactory.createEmailTemplate();
        
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
        }
    }
    
    /* negative test cases */

    // Attempt init with no massEmailSendId param for component controller
    @isTest static void init_nullIDParam_falseInit() {
        
        // Arrange
        // no work

        // Act
        MassEmailController massESCon = new MassEmailController();
        massESCon.massEmailSendId = null;

        // Assert
        System.assertEquals( false, massESCon.hasFooter);
        System.assertEquals( false, massESCon.hasHtmlBody);
        System.assertEquals( false, massESCon.hasPlainTextBody);
    }


    /* postive test cases */

    // Attempt init with valid massEmailSendId param for component controller
    @isTest static void init_withValidIDParam_validInit() {
        
        // Arrange
        Mass_Email_Send__c massES = [select Id from Mass_Email_Send__c where Name = 'TestMES1'];

        // Act
        MassEmailController massESCon = new MassEmailController();
        massESCon.massEmailSendId = massES.Id;

        // Assert
        System.assertEquals( true, massESCon.hasFooter);
        System.assertNotEquals( '', massESCon.getFooter());
        System.assertEquals( true, massESCon.hasHtmlBody);
        System.assertNotEquals( '', massESCon.getHtmlBody());
        System.assertEquals( false, massESCon.hasPlainTextBody);
        System.assertEquals( '', massESCon.getPlainTextBody());
    }

    // Attempt init with valid isMock param and no massEmailSendId param
    @isTest static void init_withValidMockParam_validInit() {
        
        // Arrange
        // no work

        // Act
        MassEmailController massESCon = new MassEmailController();
        massESCon.mockLoad = true;

        // Assert
        System.assertEquals( false, massESCon.hasFooter);
        System.assertEquals( false, massESCon.hasHtmlBody);
        System.assertEquals( false, massESCon.hasPlainTextBody);
        System.assertNotEquals( '', massESCon.getMockFooterContent());
        System.assertNotEquals( '', massESCon.getMockLogoURL());
    }

}