/**
 * @description Handles all interaction with Picklists.
 **/
public class PickListService {
    @testVisible private static final String FIELD_PARAM = 'field';

    /**
     * @description Get a list of select options to display on a visualforce
     *              page or component from the picklist entries associated
     *              with an SObjectField.
     * @param field The SObjectField to collect picklist entries from and
     *        generate a List of SelectOptions from.
     * @return A List of SelectOptions generated from the picklist entries
     *         associated with the given field.
     * @throws An ArgumentNullException if the field param is null.
     */
    public static List<SelectOption> getSelectOptionsFromField(SObjectField field) {
        ArgumentNullException.throwIfNull(field, FIELD_PARAM);

        List<SelectOption> options = new List<SelectOption>();

        Schema.DescribeFieldResult fieldResult = field.getDescribe();
        List<Schema.PicklistEntry> picklistEntries = fieldResult.getPicklistValues();

        for (Schema.PicklistEntry ple : picklistEntries) {
            options.add(new SelectOption(ple.getLabel(), ple.getValue()));
        }

        return options;
    }
}