/*
* NAIS-2365 School Portal - Documents visible from FCW
* 
* Leydi, 03-2015
*/
public with sharing class SchoolDocumentsQuickViewController
{

    /*Initialization*/
    public SchoolDocumentsQuickViewController()
    {
        this.currentYear = GlobalVariables.getDocYearStringFromAcadYearName(GlobalVariables.getCurrentYearString());
        this.priorYear = GlobalVariables.getPrevDocYearStringFromAcadYearName(GlobalVariables.getCurrentYearString());
        listDocuments = new List<QuickViewDocumentsWrapper>();
        getDocumentsBySchoolPFSAssigment();
        fullView = !GlobalVariables.isLimitedSchoolView();
    }
    /*End Initialization*/
    
    /*Properties*/
    public String schoolpfsassignmentid {get; set;}
    private String currentYear {get; set;}
    private String priorYear {get; set;}
    public List<QuickViewDocumentsWrapper> listDocuments {get;set;}
    public String dcmntsType {get; set;}
    public String dcmntsTitle {get; set;}
    public School_PFS_Assignment__c spfs {get;set;}
    public Boolean fullView {get; set;}
    
    //Wrapper Object
    public class QuickViewDocumentsWrapper
    {
        public QuickViewDocumentsWrapper( String varDocumentType, 
                                          String varDocumentYear, 
                                          String varDocumentFileName,
                                          String varDocumentImportId)
        {
            this.documentType = varDocumentType;
            this.documentYear = varDocumentYear;
            this.documentFileName = varDocumentFileName;
            this.documentImportId = varDocumentImportId;
        }

        public String documentType {get; set;}
        public String documentYear {get; set;}
        public String documentFileName {get; set;}
        public String documentImportId {get; set;}
    }
    /*End Properties*/
    
    
    /*Helper Methods*/
    private void getDocumentsBySchoolPFSAssigment()
    {
        schoolpfsassignmentid = ApexPages.currentPage().getParameters().get('schoolpfsassignmentid');

        if( schoolpfsassignmentid!=null )
        {
            //SFP-26 [G.S]
            spfs = [
                SELECT Id,School__c,Student_Folder__c,Applicant__r.PFS__c,Applicant__r.Contact__c, Applicant__c,
                    Applicant__r.pfs__r.Parent_A__c
                FROM School_PFS_Assignment__c 
                WHERE Id = :schoolpfsassignmentid];
            
            List<School_Document_Assignment__c> documents = [
                SELECT Id, Document__r.Document_Type__c, Academic_Year__c, Document__r.Id, Document__r.Name,
                    Document__r.Filename__c, Document__r.Import_Id__c, Document__r.Document_Year__c  
                FROM School_Document_Assignment__c
                WHERE School_PFS_Assignment__c =: schoolpfsassignmentid
                    AND Document__r.Duplicate__c != 'Yes' 
                    AND School_PFS_Assignment__r.Withdrawn__c != 'Yes'
                    AND Deleted__c = false 
                    AND Document__r.Deleted__c = false  
                    AND (Document__r.Document_Year__c =: this.currentYear 
                         OR Document__r.Document_Year__c =: this.priorYear) 
                    AND Document__r.Filename__c <> null 
                ORDER BY Document__r.Document_Year__c DESC ];

            this.SortQueryByYearAndDocumentName(documents);             
        }
    }
    
    /**
    * @brief Method implemented to sort the School_PFS_Assignment__c.Document__r
    * by year and filename. This is necessary since the "Document__r.Filename__c"
    * field is a picklist. And he way sorting works with picklists is that 
    * the order is determined by the soql "order by" of the picklist values as defined 
    * on the field in setup and NOT alphabetically as though they were simply text 
    */
    private void SortQueryByYearAndDocumentName(List<School_Document_Assignment__c> documents)
    {
        Map<String, Map<String, School_Document_Assignment__c>> mapToSortByYear = 
                                                new Map<String, Map<String, School_Document_Assignment__c>>();
        Map<String, School_Document_Assignment__c> tmp;
        School_Document_Assignment__c documentTmp;
        List<String> listToSortByFileName = new  List<String>();
        
        if( documents!=null && documents.size()>0 )
        {
            //Set a map with "Document Year" as the key
            for(School_Document_Assignment__c c:documents)
            {
                tmp = ( mapToSortByYear.containsKey(c.Document__r.Document_Year__c)
                        ?mapToSortByYear.get(c.Document__r.Document_Year__c)
                        :new Map<String, School_Document_Assignment__c>() );
                tmp.put(c.Document__r.Document_Type__c+'+'+c.Document__r.Id, c);
                mapToSortByYear.put(c.Document__r.Document_Year__c, tmp);
            }
            
            if( mapToSortByYear!=null && mapToSortByYear.size()>0 )
            {
                String year;
                List<String> years = new List<String>();
                years.addAll(mapToSortByYear.keySet());
                years.sort();//Sort years desc (highter first)
                for( Integer y=(years.size()-1); y>=0; y-- )
                {
                    year = years[y];
                    tmp = mapToSortByYear.get(year);
                    listToSortByFileName = new List<String>();
                    listToSortByFileName.addAll(tmp.keySet());
                    listToSortByFileName.sort();//Sort by "Document Name"
                    
                    for(String k:listToSortByFileName)
                    {
                        documentTmp = tmp.get(k);
                        listDocuments.add( new QuickViewDocumentsWrapper(
                                                documentTmp.Document__r.Document_Type__c,//+' - '+k,
                                                documentTmp.Document__r.Document_Year__c,
                                                documentTmp.Document__r.Filename__c,
                                                documentTmp.Document__r.Import_Id__c
                                                )
                                    );
                    }
                }
            }
        }
    }
    
    public PageReference getGoToFinancialReportPFS1()
    {
        PageReference pageRef = page.SchoolFinancialHistoryReportPDF;
        
        if(spfs != null)
        {
            pageRef.getParameters().put('studentId', spfs.Applicant__r.contact__c);
            pageRef.getParameters().put('schoolId', spfs.School__c);
            pageRef.getParameters().put('studentFolderId', spfs.Student_Folder__c);
            //SFP-274, [G.S]
            pageRef.getParameters().put('pfsParentId', spfs.Applicant__r.pfs__r.Parent_A__c);
        }

        pageRef.setRedirect(true);
        return pageRef;
    }
}