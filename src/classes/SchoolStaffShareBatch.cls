/*
 * Spec-134 School Portal - Multiple User Security; Req# R-493
 *    Batch class to process *new* School PFS Assignment records to share PFS and Student Folder records with *all* users 
 *    for each school they are affiliated with. 
 *
 *    Tests in SchoolStaffShareActionTest.cls
 *
 * WH, Exponent Partners, 2013
 */
global class SchoolStaffShareBatch implements Database.Batchable<sObject> {
    
    // [CH] NAIS-1766 Refactoring to pull more information to facilitate processing
    global static final String query = 'select Id, Applicant__r.PFS__c, Applicant__r.PFS__r.Parent_A__r.AccountId, School__c, Student_Folder__c from School_PFS_Assignment__c where Sharing_Processed__c = false order by Name asc';

    global SchoolStaffShareBatch() {}
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        // gather all School PFS Assignments that have not been processed
        return Database.getQueryLocator(SchoolStaffShareBatch.query);
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        if(scope != null && scope.size() > 0){
            SchoolSetWeightedPFS.preventCalculation = true; // prevent the code that re-calcs weighted PFSs
            // share records to all users affiliated with the schools
            SchoolStaffShareAction.shareRecordsToSchoolUsers(scope, null);
            SchoolSetWeightedPFS.preventCalculation = false;
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        // do nothing
    }
    
}