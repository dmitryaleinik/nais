public with sharing class SchoolPickerLinkController
{

    public Boolean displaySchoolPickerLink {get; set;}
    public String currentSchoolName {get; set;}

    public SchoolPickerLinkController()
    {
        List<Account> userAccounts = GlobalVariables.getAllSchoolsForUser();

        displaySchoolPickerLink = false;
        if (userAccounts.size() > 1)
        {
            currentSchoolName = '';
            displaySchoolPickerLink = true;
            
            currentSchoolName = new Map<Id, Account>(userAccounts).get(GlobalVariables.getCurrentSchoolId()).Name;
        }
    }
}