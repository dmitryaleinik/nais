/**
 * @description Used to represent school data received from an outside source (e.g. Ravenna, SAO, etc.)
 */
public class SchoolIntegrationModels {

    private static final String SUB_STATUS_CURRENT = 'Current';
    private static final String VALID_SCHOOL_QUERY_FORMAT = 'SELECT Id, Name, NCES_ID__c, {0} FROM Account WHERE ({0} in :externalIdsToQuery OR NCES_ID__c IN :ncesIdsToQuery) AND SSS_Subscriber_Status__c = :subStatus';

    private String externalIdFieldName;
    private List<School> schoolModels;
    private Map<String, School> schoolsByExternalId;
    private Map<String, School> schoolsByNcesId;
    private Map<String, School> validSchoolsByExternalId;
    private Map<String, School> validSchoolsByNcesId;
    private Map<String, Account> currentSubscriberSchoolAccountsById;

    private SchoolIntegrationModels(String accountExternalIdFieldName) {
        externalIdFieldName = accountExternalIdFieldName;
        schoolModels = new List<School>();
        schoolsByExternalId = new Map<String, School>();
        schoolsByNcesId = new Map<String, School>();
        validSchoolsByExternalId = new Map<String, School>();
        validSchoolsByNcesId = new Map<String, School>();
    }

    public Id getSchoolAccountId(String externalId) {
        School schoolModel = schoolsByExternalId.get(externalId);

        return schoolModel == null ? null : schoolModel.AccountId;
    }

    public Id getSchoolAccountIdByExternalId(String externalId) {
        School schoolModel = validSchoolsByExternalId.get(externalId);

        return schoolModel == null ? null : schoolModel.AccountId;
    }

    public Id getSchoolAccountIdByNcesId(String ncesId) {
        School schoolModel = validSchoolsByNcesId.get(ncesId);

        return schoolModel == null ? null : schoolModel.AccountId;
    }

    public void addSchool(String ncesIdValue, String externalIdValue) {
        if (String.isBlank(ncesIdValue) && String.isBlank(externalIdValue)) {
            return;
        }

        School newSchool = createSchool(ncesIdValue, externalIdValue, externalIdFieldName);

        // Add this to the list of schools.
        schoolModels.add(newSchool);

        if (newSchool.hasExternalId()) {
            schoolsByExternalId.put(newSchool.ExternalId, newSchool);
        }

        if (newSchool.hasNcesId()) {
            schoolsByNcesId.put(newSchool.NcesId, newSchool);
        }
    }

    /**
     * @description Requires that there is at least one current subscriber school. If there isn't, an integration
     *              exception is thrown saying the students aren't applying to any current subscriber schools.
     */
    public void requireCurrentSubscriberSchools() {
        if (getCurrentSubscriberSchoolsById().isEmpty()) {
            throw new IntegrationApiService.IntegrationException( System.Label.Integration_No_Subscriber_School_Error);
        }
    }

    public Map<String, Account> getCurrentSubscriberSchoolsById() {
        if (currentSubscriberSchoolAccountsById != null) {
            return currentSubscriberSchoolAccountsById;
        }

        // Valid schools are current subscribers to SSS.
        Set<String> externalIdsToQuery = schoolsByExternalId.keySet();
        Set<String> ncesIdsToQuery = schoolsByNcesId.keySet();
        String subStatus = SUB_STATUS_CURRENT;

        String query = String.format(VALID_SCHOOL_QUERY_FORMAT, new List<String> { this.externalIdFieldName });

        List<Account> schoolAccounts = Database.query(query);

        School schoolModel;
        for (Account schoolAccount : schoolAccounts) {
            schoolModel = schoolsByNcesId.get(schoolAccount.NCES_ID__c);
            if (schoolModel != null) {
                schoolModel.setAccountId(schoolAccount.Id);
                validSchoolsByNcesId.put(schoolModel.NcesId, schoolModel);
            }

            String accountExternalId = (String)schoolAccount.get(externalIdFieldName);
            if (String.isBlank(accountExternalId)) {
                continue;
            }

            schoolModel = schoolsByExternalId.get(accountExternalId);
            if (schoolModel != null) {
                schoolModel.setAccountId(schoolAccount.Id);
                validSchoolsByExternalId.put(schoolModel.ExternalId, schoolModel);
            }
        }

        currentSubscriberSchoolAccountsById = new Map<String, Account>(schoolAccounts);

        return currentSubscriberSchoolAccountsById;
    }

    /**
     * @description Creates new instance of the SchoolIntegrationModels
     */
    public static SchoolIntegrationModels newInstance(String accountExternalIdFieldName) {
        return new SchoolIntegrationModels(accountExternalIdFieldName);
    }

    public static School createSchool(String ncesId, String externalId, String externalIdFieldName) {
        return new School(ncesId, externalId, externalIdFieldName);
    }

    public class School {

        private School(String ncesIdValue, String externalIdValue, String externalIdFieldName) {
            NcesId = ncesIdValue;
            ExternalId = externalIdValue;
            ExternalIdField = externalIdFieldName;
        }

        public Boolean hasNcesId() {
            return String.isNotBlank(this.NcesId);
        }

        public Boolean hasExternalId() {
            return String.isNotBlank(this.ExternalId);
        }

        public void setAccountId(Id schoolAccountId) {
            this.AccountId = schoolAccountId;
        }

        /**
         * @description The Id of a school in another system.
         */
        public String ExternalId { get; private set; }

        /**
         * @description The name of the field on the Account object that should be used to match the external Id.
         */
        public String ExternalIdField { get; private set; }

        /**
         * @description The NCES Id of a school.
         */
        public String NcesId { get; private set; }

        /**
         * @description The Id of the school account in our system.
         */
        public Id AccountId { get; private set; }

        /**
         * @description Gets the value to use when matching a school in our system to a school in another system. If NCES
         *              Id is not null, we use that. Otherwise we use the external Id field.
         */
        public String getIdToMatchBy() {
            if (String.isNotBlank(this.NcesId)) {
                return this.NcesId;
            }

            return this.ExternalId;
        }
    }
}