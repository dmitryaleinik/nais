@isTest
private class PickListServiceTest {
    @isTest
    private static void getSelectOptionsFromField_nullParam_expectArgumentNullException() {
        try {
            Test.startTest();
            PickListService.getSelectOptionsFromField(null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, PickListService.FIELD_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void getSelectOptionsFromField_fieldWithOptions_expectPopulatedList() {
        Test.startTest();
        List<SelectOption> options = PickListService.getSelectOptionsFromField(Contact.LeadSource);
        Test.stopTest();

        System.assertNotEquals(null, options, 'Expected the options list to not be null.');
        System.assert(options.size() > 0, 'Expected there to be at least one option returned.');
    }

    @isTest
    private static void getSelectOptionsFromField_fieldWithoutOptions_expectEmptyList() {
        Test.startTest();
        List<SelectOption> options = PickListService.getSelectOptionsFromField(Contact.Name);
        Test.stopTest();

        System.assertNotEquals(null, options, 'Expected the options list to not be null.');
        System.assert(options.isEmpty(), 'Expected the result list to be empty.');
    }
}