public class FamilyPaymentData {
    public final static String PAYMENT_TYPE_CC = 'Credit/Debit';
    public final static String PAYMENT_TYPE_ECHECK = 'eCheck';
    public final static String PAYMENT_TYPE_WAIVER = 'Waiver';
    public final static String PAYMENT_TYPE_INVOICE = 'Invoice';         // NAIS-2270

    public final static String CC_TYPE_VISA = 'Visa';
    public final static String CC_TYPE_MASTERCARD = 'Master Card';
    public final static String CC_TYPE_AMEX = 'Amex';
    public final static String CC_TYPE_DISCOVER = 'Discover';

    // Payment type
    public String paymentType {get; set;}

    // Payment description
    public Decimal paymentAmount {get; set;}
    public String paymentDescription {get; set;}
    public String paymentTracker { get; set; }

    // General billing info
    public String billingFirstName {get; set;}
    public String billingLastName {get; set;}
    public String billingStreet1 {get; set;}
    public String billingStreet2 {get; set;}
    public String billingCity {get; set;}
    public String billingState {get; set;}
    public String billingPostalCode {get; set;}
    public String billingCountry {get; set;}
    public String billingEmail {get; set;}

    // Credit card specific info
    public String ccNameOnCard {get; set;}
    public String ccCardType {get; set;}
    public String ccCardNumber {get; set;}
    public String ccExpirationMM {
        get {
            if (ccExpirationMM == null) {
                return String.valueOf(System.today().month());
            }
            return ccExpirationMM;
        }
        set;
    }
    public String ccExpirationYY {
        get {
            if (ccExpirationYY == null) {
                return String.valueOf(System.today().year());
            }
            return ccExpirationYY;
        }
        set;
    }
    public String ccSecurityCode {get; set;}

    // eCheck specific info
    public String checkBankName {get; set;}
    public String checkAccountType {get; set;}
    public String checkAccountNumber {get; set;}
    public String checkRoutingNumber {get; set;}
    public String checkCheckType {get; set;}
    public String checkCheckNumber {get; set;}
    public String checkNameOnCheck {
        get {
            if (checkAccountType != 'Business Checking') {
                return billingFirstName + ' ' + billingLastName;
            }
            return checkNameOnCheck;
        }
        set;
    }

    public boolean isCreditCardPaymentType {
        public get {
            if (paymentType == PAYMENT_TYPE_CC) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    public boolean isECheckPaymentType {
        public get {
            if (paymentType == PAYMENT_TYPE_ECHECK) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    public boolean isWaiverPaymentType {
        public get {
            if (paymentType == PAYMENT_TYPE_WAIVER) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    // NAIS-2270
    public boolean isInvoicePaymentType {
        public get {
            if (paymentType == PAYMENT_TYPE_INVOICE) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    public String ccCardNumberMasked {
        public get {
            String maskedCardNumber = null;
            if (ccCardNumber != null) {
                String last4Digits = ccCardNumber.right(4);
                Integer numMaskCharacters = ccCardNumber.length() - 4;
                if (numMaskCharacters > 0) {
                    maskedCardNumber = 'X'.repeat(numMaskCharacters) + last4Digits;
                }
                else {
                    maskedCardNumber = last4Digits;
                }

            }
            return maskedCardNumber;
        }
    }

    public String ccCardNumberLast4Digits {
        public get {
            String last4Digits = null;
            if (ccCardNumber != null) {
                last4Digits = ccCardNumber.right(4);
            }
            return last4Digits;
        }
    }

    public void trimStrings() {
        if (billingFirstName != null) {billingFirstName = billingFirstName.trim();}
        if (billingLastName != null) {billingLastName = billingLastName.trim();}
        if (billingStreet1 != null) {billingStreet1 = billingStreet1.trim();}
        if (billingStreet2 != null) {billingStreet2 = billingStreet2.trim();}
        if (billingCity != null) {billingCity = billingCity.trim();}
        if (billingState != null) {billingState = billingState.trim();}
        if (billingPostalCode != null) {billingPostalCode = billingPostalCode.trim();}
        if (billingCountry != null) {billingCountry = billingCountry.trim();}
        if (billingEmail != null) {billingEmail = billingEmail.trim();}
        if (ccNameOnCard != null) {ccNameOnCard = ccNameOnCard.trim();}
        if (ccCardType != null) {ccCardType = ccCardType.trim();}
        if (ccCardNumber != null) {ccCardNumber = ccCardNumber.deleteWhitespace();}
        if (ccExpirationMM != null) {ccExpirationMM = ccExpirationMM.trim();}
        if (ccExpirationYY != null) {ccExpirationYY = ccExpirationYY.trim();}
        if (ccSecurityCode != null) {ccSecurityCode = ccSecurityCode.trim();}
        if (checkBankName != null) {checkBankName = checkBankName.trim();}                        // NAIS-2270
        if (checkAccountType != null) {checkAccountType = checkAccountType.trim();}
        if (checkAccountNumber != null) {checkAccountNumber = checkAccountNumber.trim();}
        if (checkRoutingNumber != null) {checkRoutingNumber = checkRoutingNumber.trim();}
        if (checkCheckType != null) {checkCheckType = checkCheckType.trim();}
        if (checkCheckNumber != null) {checkCheckNumber = checkCheckNumber.trim();}
    }
}