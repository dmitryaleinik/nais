/*
 * Spec-488 Payment Processing - See main controller class FamilyPaymentController.cls
 *
 * SL, Exponent Partners, 2013
 */
public without sharing class FamilyPaymentCompleteController {

    /* CONSTANTS */
    public static final String PFS_PARAM_NAME = 'id';
    public static final String ACADEMIC_YEAR_PARAM_NAME = 'academicyearid';
    public static final String TRANSACTION_LINE_ITEM_PARAM_NAME = 'tid';
    // NAIS-1688 [dp] For Databank Paper Entry
    public static final String IS_DATABANK_PARAM_NAME = 'dbank';
    /* END CONSTANTS */


    /* PROPERTIES */
    public String pfsIdParamValue {get; set;}
    public String academicYearIdParamValue {get; set;}
    public String transactionLineItemIdParamValue {get; set;}
    public PFS__c thePFS {get; set;}
    public Boolean isDatabank {get; set;}
    private FamilyTemplateController controller{get;set;}
    /* END PROPERTIES */


    /* INITIALIZATION */
    public FamilyPaymentCompleteController(FamilyTemplateController c) {
        this.controller = c;
        academicYearIdParamValue = ApexPages.currentPage().getParameters().get(ACADEMIC_YEAR_PARAM_NAME);
        pfsIdParamValue = ApexPages.currentPage().getParameters().get(PFS_PARAM_NAME);
        transactionLineItemIdParamValue = ApexPages.currentPage().getParameters().get(TRANSACTION_LINE_ITEM_PARAM_NAME);
        // NAIS-1688 [dp] For Databank Paper Entry
        isDatabank = ApexPages.currentPage().getParameters().get(IS_DATABANK_PARAM_NAME) == '1';

        // query PFS based on Id
        thePFS = this.controller.pfsRecord;
    }
    /* END INITIALIZATION */

    public static FamilyTemplateController.config loadPFS(String pfsId, String academicYearId)
    {
        PFS__c pfsRecord;
        User u = GlobalVariables.getCurrentUser();
        if (!String.isBlank(pfsId)) {
            pfsRecord = ApplicationUtils.queryPFSRecord(pfsId, null, null);
        } else {
            Id parentAContactId = GlobalVariables.getCurrentUser().ContactId;
            if (String.isBlank(academicYearId)) {
                academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
            }
            if ((parentAContactId != null) && (academicYearId != null)) {
                pfsRecord = ApplicationUtils.queryPFSRecord(null, parentAContactId, academicYearId);
            }
        }
        return new FamilyTemplateController.config(pfsRecord, null);
    }


    /* ACTION METHODS */
    public static PageReference returnToDashboard() {
        PageReference pr = Page.FamilyDashboard;
        pr.setRedirect(true);
        if (ApexPages.currentPage().getParameters().get(PFS_PARAM_NAME) != null) {
            pr.getParameters().put(PFS_PARAM_NAME, ApexPages.currentPage().getParameters().get(PFS_PARAM_NAME));
        }
        if (ApexPages.currentPage().getParameters().get(ACADEMIC_YEAR_PARAM_NAME) != null) {
            pr.getParameters().put(ACADEMIC_YEAR_PARAM_NAME, ApexPages.currentPage().getParameters().get(ACADEMIC_YEAR_PARAM_NAME));
        }
        return pr;
    }
    /* END ACTION METHODS */

    /* HELPER METHODS */
    public String getCreditCardStatementDescriptor() {
        return PaymentProcessor.CreditCardStatementDescriptor;
    }
}