public with sharing class WithoutSharingQueryClass {
        
    private WithoutSharingQueryClass() {}
    
    public List<School_PFS_Assignment__c> selectByPFSId(Set<Id> pfsIds, List<String> fieldsToQuery) {
        
        return  SchoolPfsAssignmentsSelector.Instance.selectByPFSId(pfsIds, fieldsToQuery);
    }
    
    public List<Annual_Setting__c> selectBySchoolIdAcademicYearName(Set<String> schoolIdAcademicYearNameKeyset, List<String> fieldsToQuery) {
    
        return AnnualSettingsSelector.Instance.selectBySchoolIdAcademicYearName(schoolIdAcademicYearNameKeyset, fieldsToQuery);
    }
    
    /**
     * @description Singleton instance of the selector to be reused at your leisure.
     */
    public static WithoutSharingQueryClass Instance {
        get {
            if (Instance == null) {
                Instance = new WithoutSharingQueryClass();
            }
            return Instance;
        }
        private set;
    }
}