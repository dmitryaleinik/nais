/**
 * @description School Family Contribution Worksheet Controller.
 **/
public with sharing class SchoolFamilyContribWrksheetController implements SchoolAcademicYearSelectorStudInterface {
    public SchoolFamilyContribWrksheetController() {
        SchoolPFSAssignmentId = ApexPages.currentPage().getParameters().get('schoolpfsassignmentid');
        ArgumentNullException.throwIfNull(SchoolPFSAssignmentId, 'schoolpfsassignmentid');

        List<School_PFS_Assignment__c> spas = SchoolPfsAssignmentsSelector.Instance.selectForFcwById(new Set<Id>{Id.ValueOf(SchoolPFSAssignmentId)});
        SchoolPFSAssignment = !spas.isEmpty() ? spas [0] : new School_PFS_Assignment__c();

        folder = GlobalVariables.getFolderAndSchoolPFSAssignments(SchoolPFSAssignment.Student_Folder__c);

        PFS = SchoolPFSAssignment.Applicant__r.PFS__r;

        Verification = SchoolPFSAssignment.Applicant__r.PFS__r.Verification__r;

        SSSConstants = [SELECT Percentage_for_Imputing_Assets__c, Default_COLA_Value__c, Self_Employment_Tax_Rate__c FROM SSS_Constants__c WHERE Academic_Year__r.Name = :SchoolPFSAssignment.Academic_Year_Picklist__c LIMIT 1];

        acadYear = GlobalVariables.getAcademicYearByName(SchoolPFSAssignment.Academic_Year_Picklist__c);

        List<Id> familyDocumentIds = new List<Id>();
        // [DP] NAIS-1789 refactor to avoid invalid query error
        for (School_Document_Assignment__c docAssign : [Select Document__c, Academic_Year__c FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c = :SchoolPFSAssignmentId]) {
            familyDocumentIds.add(docAssign.Document__c);
            //}
        }

        String academicYearId = acadYear.Id;
        String docYear = GlobalVariables.getDocYearStringFromAcadYearName(SchoolPFSAssignment.Academic_Year_Picklist__c);
        // SFP-28 Add previous year's documents for difference calculation
        String priorDocYear = GlobalVariables.getPrevDocYearStringFromAcadYearName(SchoolPFSAssignment.Academic_Year_Picklist__c);

        // refactored to use Doc Year instead of Acad Year
        String familyDocumentSql = 'SELECT ' + String.Join(ExpCore.GetAllFieldNames('Family_Document__c'), ',') + ' FROM Family_Document__c WHERE Id IN :familyDocumentIds AND Document_Year__c = :docYear';
        FamilyDocuments = Database.query(familyDocumentSql);

        String ucSql = 'SELECT ' + String.Join(ExpCore.GetAllFieldNames('Unusual_Conditions__c'), ',') + ' FROM Unusual_Conditions__c WHERE Academic_Year__c = :academicYearId LIMIT 1';
        List<Unusual_Conditions__c> tempUC = Database.query(ucSql);
        if (tempUC.size() > 0)
            UnusualConditions = tempUC[0];
    }

    /*End Initialization*/

    /*Properties*/

    public String SchoolPFSAssignmentId {get; set;}

    public School_PFS_Assignment__c SchoolPFSAssignment {get; set;}

    public Verification__c Verification {get; set;}

    public PFS__c PFS {get; set;}

    public SSS_Constants__c SSSConstants {get; set;}

    public List<Family_Document__c> FamilyDocuments {get; set;}

    public Id academicyearid;

    public Unusual_Conditions__c UnusualConditions {get; set;}

    public Student_Folder__c folder {get; set;}

    public Academic_Year__c acadYear {get; set;}

    /*End Properties*/

    /*Action Methods*/

    /*End Action Methods*/

    /*Helper Methods*/

    /*End Helper Methods*/

    public class ArgumentException extends Exception {}

    /* Interface Implementation */
    public SchoolFamilyContribWrksheetController Me {
        get { return this; }
    }

    public String getAcademicYearId() {
        return academicyearid;
    }

    public void setAcademicYearId(String academicYearIdParam) {
        academicyearid = academicYearIdParam;
    }

    public PageReference SchoolAcademicYearSelector_OnChange(Boolean saveRecord) {
        return null;
    }

    // on change of academic year, reload page with new folder
    public PageReference SchoolAcademicYearSelectorStudent_OnChange(PageReference newPage) {
        return newPage;
    }
    /* End Interface Implementation */
}