@isTest
private class SchoolFamilyContribWrksheetBulkContTest {

    @isTest
    private static void testOneFCWPrint() {
        SchoolFCWControllerTest.createTestData();

        Id portalProfileId = [select Id from Profile where Name = :ProfileSettings.PartnerProfileName].Id;
        Contact staff1 = TestUtils.createContact('Staff 1', SchoolFCWControllerTest.school1.Id, RecordTypes.schoolStaffContactTypeId, true);

        User u1 = TestUtils.createPortalUser('User 1', 'u1@test.org', 'u1', staff1.Id, portalProfileId, true, false);

        System.runAs(SchoolFamilyContribWrksheetBulkCont.thisUser) {
            insert u1;
        }

        String spfsaId = SchoolFCWControllerTest.spfsa1.Id;
        PageReference pageRef = new ApexPages.Pagereference('/apex/SchoolFamilyContributionWorksheetPrint?schoolpfsassignmentid=' + spfsaId);
        Test.setCurrentPage(pageRef);
        SchoolFamilyContribWrksheetPrintCont controller = new SchoolFamilyContribWrksheetPrintCont();
        SchoolFCWController fcwController = new SchoolFCWController();

        fcwController.SchoolPFSAssignment = controller.singleFCWPrintWrapper.SchoolPFSAssignment;
        fcwController.PFS = controller.singleFCWPrintWrapper.PFS;
    }

    @isTest
    private static void testBulkConstructerOnly() {
        SchoolFCWControllerTest.createTestData();

        Id portalProfileId = [select Id from Profile where Name = :ProfileSettings.PartnerProfileName].Id;
        Contact staff1 = TestUtils.createContact('Staff 1', SchoolFCWControllerTest.school1.Id, RecordTypes.schoolStaffContactTypeId, true);

        User u1 = TestUtils.createPortalUser('User 1', 'u1@test.org', 'u1', staff1.Id, portalProfileId, true, false);

        System.runAs(SchoolFamilyContribWrksheetBulkCont.thisUser) {
            insert u1;
        }

        List<Student_Folder__c> folders = new List<Student_Folder__c>();
        folders.add(SchoolFCWControllerTest.studentFolder11);

        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(folders);
        ssc.setSelected(folders);

        SchoolFamilyContribWrksheetBulkCont controller = new SchoolFamilyContribWrksheetBulkCont(ssc);
        System.assertEquals(1, controller.printCont.fcwPrintWrappers.size());
    }

    @isTest
    private static void testBulkConstructerTwoFolders() {
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);

        Account family = AccountTestData.Instance.asFamily().create();
        Account school = AccountTestData.Instance.asSchool().create();
        insert new List<Account>{family, school};

        Contact parentA = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .forAccount(school.Id)
            .asSchoolStaff().create();
        Contact student1 = ContactTestData.Instance
            .asStudent().create();
        Contact student2 = ContactTestData.Instance
            .asStudent().create();
        insert new List<Contact>{parentA, schoolStaff, student1, student2};
        
        User schoolPortalUser;
        System.runAs(SchoolFamilyContribWrksheetBulkCont.thisUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.insertAcademicYear();
        Student_Folder__c studentFolder1;
        Student_Folder__c studentFolder2;

        System.runAs(schoolPortalUser) {
            PFS__c pfs1 = PfsTestData.Instance
                .asSubmitted()
                .forParentA(parentA.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<PFS__c> {pfs1});

            Applicant__c applicant1 = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student1.Id).create();
            Applicant__c applicant2 = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student2.Id).create();
            Dml.WithoutSharing.insertObjects(new List<Applicant__c> {applicant1, applicant2});

            studentFolder1 = StudentFolderTestData.Instance
                .forStudentId(student1.Id)
                .forSchoolId(school.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            studentFolder2 = StudentFolderTestData.Instance
                .forStudentId(student2.Id)
                .forSchoolId(school.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<Student_Folder__c> {studentFolder1, studentFolder2});

            String fifthGrade = '5';
            School_PFS_Assignment__c spfsa1 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            School_PFS_Assignment__c spfsa2 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant2.Id)
                .forStudentFolderId(studentFolder2.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> {spfsa1, spfsa2});
        }

        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);

        Test.StartTest();
            List<Student_Folder__c> folders = new List<Student_Folder__c>();
            folders.add(studentFolder1);
            folders.add(studentFolder2);

            ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(folders);
            ssc.setSelected(folders);

            SchoolFamilyContribWrksheetBulkCont controller = new SchoolFamilyContribWrksheetBulkCont(ssc);
            System.assertEquals(2, controller.printCont.fcwPrintWrappers.size());
        Test.stopTest();
    }
}