/**
 * Created by yadjayanth on 3/2/16.
 */

public with sharing class SchoolFCWContainer {

    string currentSchoolPFSAssignmentId;

    public SchoolFCWContainer() {
        currentSchoolPFSAssignmentId = ApexPages.currentPage().getParameters().get('schoolpfsassignmentid');

        if (currentSchoolPFSAssignmentId == null) {
            throw new ArgumentException('Missing schoolpfsassignmentid');
        }

    }

    // getters to pass into full FCW
    private School_PFS_Assignment__c schoolPFSAssignment;
    public School_PFS_Assignment__c getSchoolPFSAssignment(){
        if( schoolPFSAssignment == null ) {
            List<School_PFS_Assignment__c> spas = SchoolPfsAssignmentsSelector.Instance.selectForFcwById(new Set<Id>{currentSchoolPFSAssignmentId});
            schoolPFSAssignment = !spas.isEmpty() ? spas [0] : new School_PFS_Assignment__c();
        }
        return schoolPFSAssignment;
    }

    private PFS__c PFS;
    public PFS__c getPFS(){
        if( PFS == null )
            PFS = getSchoolPFSAssignment().Applicant__r.PFS__r;

        return PFS;
    }

    private Verification__c verification;
    public Verification__c getVerification(){
        if( verification == null )
            verification = getSchoolPFSAssignment().Applicant__r.PFS__r.Verification__r;

        return verification;
    }

    private SSS_Constants__c SSSConstants;
    public SSS_Constants__c getSSSConstants(){
        if( SSSConstants == null){
            list<SSS_Constants__c> SSSConstantslist = [SELECT Percentage_for_Imputing_Assets__c, Default_COLA_Value__c, Self_Employment_Tax_Rate__c FROM SSS_Constants__c WHERE Academic_Year__r.Name =:getCurrentAcademicYear().name LIMIT 1];
            if(!SSSConstantslist.isEmpty())
                SSSConstants = SSSConstantslist[0];
        }

        return SSSConstants;
    }

    private list<id> familyDocumentIds;
    public list<id> getFamilyDocumentIds(){
        if ( familyDocumentIds == null){
            familyDocumentIds = new List<Id>();
            for (School_Document_Assignment__c docAssign : [Select Document__c, Academic_Year__c FROM School_Document_Assignment__c WHERE School_PFS_Assignment__c = :currentSchoolPFSAssignmentId]) {
                if(docAssign.document__c != null)
                    familyDocumentIds.add(docAssign.Document__c);
            }
        }
        return familyDocumentIds;
    }

    private list<Family_Document__c> currentYearFamilyDocuments;
    public list<Family_Document__c> getcurrentYearFamilyDocuments(){
        if( currentYearfamilyDocuments == null){
            String docYear = GlobalVariables.getDocYearStringFromAcadYearName(getCurrentYearPFS().Academic_Year_Picklist__c);
            list<id> fdIds = getFamilyDocumentIds();
            String familyDocumentSql = 'SELECT ' + String.Join(ExpCore.GetAllFieldNames('Family_Document__c'), ',') + ' FROM Family_Document__c WHERE Id IN :fdIds AND Document_Year__c = :docYear';
            system.debug(familyDocumentsql);
            currentYearFamilyDocuments = Database.query(familyDocumentSql);
        }

        return currentYearfamilyDocuments;
    }

    private list<Family_Document__c> previousYearFamilyDocuments;
    public list<Family_Document__c> getpreviousYearFamilyDocuments(){
        if( previousYearfamilyDocuments == null){
            String priorDocYear = GlobalVariables.getPrevDocYearStringFromAcadYearName(getCurrentYearPFS().Academic_Year_Picklist__c);
            list<id> fdIds = getFamilyDocumentIds();
            String pyfamilyDocumentSql = 'SELECT ' + String.Join(ExpCore.GetAllFieldNames('Family_Document__c'), ',') + ' FROM Family_Document__c WHERE Id IN :fdIds AND Document_Year__c = :priorDocYear';
            previousYearFamilyDocuments = Database.query(pyfamilyDocumentSql);
        }

        return previousYearfamilyDocuments;
    }

    private Unusual_Conditions__c unusualConditions;
    public Unusual_Conditions__c getUnusualConditions(){
        if ( unusualConditions == null){
            String ucSql = 'SELECT ' + String.Join(ExpCore.GetAllFieldNames('Unusual_Conditions__c'), ',') + ' FROM Unusual_Conditions__c WHERE Academic_Year__c =\''+getCurrentAcademicYear().id+'\' LIMIT 1';
            List<Unusual_Conditions__c> tempUC = Database.query(ucSql);
            if (tempUC.size() > 0)
                this.UnusualConditions = tempUC[0];
        }
        return unusualConditions;
    }

    private  Academic_Year__c currentAcademicYear;
    public Academic_Year__c getCurrentAcademicYear(){
        if( currentAcademicYear == null){
            currentAcademicYear = GlobalVariables.getAcademicYearByName(getCurrentYearPFS().Academic_Year_Picklist__c);
        }
        return currentAcademicYear;
    }

    private School_PFS_Assignment__c currentYearPFS;
    public School_PFS_Assignment__c getCurrentYearPFS(){
        if( currentYearPFS == null )
            currentYearPFS = getSchoolPFSAssignment();

        return currentYearPFS;
    }

    public class ArgumentException extends Exception {}
}