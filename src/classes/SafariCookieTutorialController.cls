public without sharing class SafariCookieTutorialController {
    public Help_Configuration__c theHelp {get; set;}

    public SafariCookieTutorialController() {
        theHelp = GlobalVariables.getCurrentTermsAndConditionsHelpRecord();        
    }
}