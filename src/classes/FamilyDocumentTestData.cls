/**
 * @description This class is used to create Family Document records for unit tests.
 */
@isTest
public class FamilyDocumentTestData extends SObjectTestData
{

    @testVisible private static final String NAME = 'FamilyDocument Name';
    public static final String DOC_STATUS_RECEIVED = 'Received/In Progress';
    public static final String DOC_STATUS_PROCESSED = 'Processed';
    public static final String DOC_SOURCE_MAILED = 'Mailed';
    public static final String DOC_SOURCE_PARENT_UPLOAD = 'Uploaded By Parent';

    private Integer counter = 1;
    private Set<String> importIdsAlreadyUsed = new Set<String>();

    /**
     * @description Get the default values for the Family_Document__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Family_Document__c.Name => NAME,
                Family_Document__c.Document_Status__c => DOC_STATUS_RECEIVED,
                Family_Document__c.Household__c => HouseholdTestData.Instance.DefaultHousehold.Id
        };
    }

    /**
     * @description Set the Deleted__c field on the current Family Document record to true.
     * @return The current working instance of FamilyDocumentTestData.
     */
    public FamilyDocumentTestData asDeleted() {
        return (FamilyDocumentTestData) with(Family_Document__c.Deleted__c, true);
    }

    /**
     * @description Set the Name field on the current Family Document record.
     * @param docName The Document Name to set on the Family Document.
     * @return The current working instance of FamilyDocumentTestData.
     */
    public FamilyDocumentTestData forName(String docName) {
        return (FamilyDocumentTestData) with(Family_Document__c.Name, docName);
    }

    /**
     * @description Set the Document_Type__c field on the current Family Document record.
     * @param docType The Document Type to set on the Family Document.
     * @return The current working instance of FamilyDocumentTestData.
     */
    public FamilyDocumentTestData forDocumentType(String docType) {
        return (FamilyDocumentTestData) with(Family_Document__c.Document_Type__c, docType);
    }

    /**
     * @description Set the Academic_Year_Picklist__c field on the current Family Document record.
     * @param acYearPicklist The Academic Year Picklist to set on the Family Document.
     * @return The current working instance of FamilyDocumentTestData.
     */
    public FamilyDocumentTestData forAcademicYearPicklist(String acYearPicklist) {
        return (FamilyDocumentTestData) with(Family_Document__c.Academic_Year_Picklist__c, acYearPicklist);
    }

    /**
     * @description Sets the Import_Id__c field.
     * @param importId The test import Id to use.
     * @return The current working instance of FamilyDocumentTestData.
     */
    public FamilyDocumentTestData withImportId(String importId) {
        return (FamilyDocumentTestData)with(Family_Document__c.Import_ID__c, importId);
    }

    /**
     * @description Sets the Document_Status__c field.
     * @param status The document status to use.
     * @return The current working instance of FamilyDocumentTestData.
     */
    public FamilyDocumentTestData withStatus(String status) {
        return (FamilyDocumentTestData)with(Family_Document__c.Document_Status__c, status);
    }

    /**
     * @description Sets the Document_Status__c field to Processed.
     * @return The current working instance of FamilyDocumentTestData.
     */
    public FamilyDocumentTestData isProcessed() {
        return withStatus(DOC_STATUS_PROCESSED);
    }

    /**
     * @description Sets the household for this document.
     * @param householdId The Id of a household.
     * @return The current instance of FamilyDocumentTestData.
     */
    public FamilyDocumentTestData forHousehold(Id householdId) {
        return (FamilyDocumentTestData)with(Family_Document__c.Household__c, householdId);
    }

    /**
     * @description Sets the Document Year for this document.
     * @param docYear The year to use.
     * @return The current instance of FamilyDocumentTestData.
     */
    public FamilyDocumentTestData forDocYear(String docYear) {
        return (FamilyDocumentTestData)with(Family_Document__c.Document_Year__c, docYear);
    }

    /**
     * @description Sets the Verification Requested Date for this document.
     * @param verifiedDate The date and time to use.
     * @return The current instance of FamilyDocumentTestData.
     */
    public FamilyDocumentTestData verificationRequestedOn(DateTime verifiedDate) {
        return (FamilyDocumentTestData)with(Family_Document__c.Verification_Requested_Date__c, verifiedDate);
    }

    /**
     * @description Sets the Document Source for this document.
     * @param documentSource The source to use.
     * @return The current instance of FamilyDocumentTestData.
     */
    public FamilyDocumentTestData withSource(String documentSource) {
        return (FamilyDocumentTestData)with(Family_Document__c.Document_Source__c, documentSource);
    }

    /**
     * @description Sets the Date Verified field for this document.
     * @param verifiedDate The date to use.
     * @return The current instance of FamilyDocumentTestData.
     */
    public FamilyDocumentTestData withDateVerified(Date verifiedDate) {
        return (FamilyDocumentTestData)with(Family_Document__c.Date_Verified__c, verifiedDate);
    }

    /**
     * @description Insert the current working Family_Document__c record.
     * @return The currently operated upon Family_Document__c record.
     */
    public Family_Document__c insertFamilyDocument() {
        return (Family_Document__c)insertRecord();
    }

    /**
     * @description Insert multiple Family_Document__c records.
     * @param numberToInsert The number of family documents to insert.
     * @return A list of family document records.
     */
    public List<Family_Document__c> insertFamilyDocuments(Integer numberToInsert) {
        return (List<Family_Document__c>)insertRecords(numberToInsert);
    }

    /**
     * @description Create the current working Family Document record without resetting
     *          the stored values in this instance of FamilyDocumentTestData.
     * @return A non-inserted Family_Document__c record using the currently stored field
     *          values.
     */
    public Family_Document__c createFamilyDocumentWithoutReset() {
        return (Family_Document__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Family_Document__c record.
     * @return The currently operated upon Family_Document__c record.
     */
    public Family_Document__c create() {
        return (Family_Document__c)super.buildWithReset();
    }

    /**
     * @description Handles certain things that should be done before inserting a record.
     *              If the record has an import Id, we check if that import Id has already been used. If so, we change
     *              it to make sure it is unique.
     * @param sObj The record that is about to be inserted.
     */
    protected override void beforeInsert(SObject sObj) {
        Family_Document__c record = (Family_Document__c)sObj;

        ensureImportIdIsUnique(record);

        counter++;
    }

    private void ensureImportIdIsUnique(Family_Document__c familyDocument) {
        if (String.isBlank(familyDocument.Import_ID__c)) {
            return;
        }

        String importId = familyDocument.Import_ID__c;

        if (importIdsAlreadyUsed.contains(importId)) {
            importId += counter;
            familyDocument.Import_ID__c = importId;
        }

        importIdsAlreadyUsed.add(importId);

    }

    /**
     * @description The default Family_Document__c record.
     */
    public Family_Document__c DefaultFamilyDocument {
        get {
            if (DefaultFamilyDocument == null) {
                DefaultFamilyDocument = createFamilyDocumentWithoutReset();
                insert DefaultFamilyDocument;
            }
            return DefaultFamilyDocument;
        }
        private set;
    }

    /**
     * @description Get the Family_Document__c SObjectType.
     * @return The Family_Document__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Family_Document__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static FamilyDocumentTestData Instance {
        get {
            if (Instance == null) {
                Instance = new FamilyDocumentTestData();
            }
            return Instance;
        }
        private set;
    }

    private FamilyDocumentTestData() { }
}