/**
 * AccountDataAccessServiceTest.cls
 *
 * @description: Test class for AccountDataAccessService using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
public class AccountDataAccessServiceTest {

    /* negative test cases */

    // Attempt getAccountById with null param, expect null object ref returned
    @isTest static void getAccountById_withNull_nullContact() {
        
        // Arrange
        Account acct = new Account();

        // Act
        acct = new AccountDataAccessService().getAccountById( null);

        // Assert
        System.assertEquals( null, acct);
    }

    
    /* positive test cases */

    // Attempt getAccountById with valid param, expect valid object ref returned
    @isTest static void getAccountById_withId_validContact() {
        
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
            
            // Arrange
            Account acct = new Account( Name = 'Tester Individual', RecordTypeId = RecordTypes.individualAccountTypeId);
            insert acct;

            // Act
            acct = new AccountDataAccessService().getAccountById( acct.Id);

            // Assert
            System.assertNotEquals( null, acct);

        }
    }

}