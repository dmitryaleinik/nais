global class FamilyDocumentsController{
    /*Initialization*/
    public FamilyTemplateController controller { get; set; }
    public String docPertainsTo { get; set; }
    public List<KnowledgeDataAccessService.KnowledgeWrapper> faqs { get; set; }
    
    public FamilyDocumentsController(FamilyTemplateController c){
        this.controller = c;
        if(this.controller.pfsRecord == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error: No PFS Record Found for current user.'));
            this.controller.pfsRecord = new PFS__c();
        }
        
        appUtils = new ApplicationUtils(UserInfo.getLanguage(), this.controller.pfsRecord, 'FamilyDocument');
        
        familyDoc = new Family_Document__c();

        faqs = new KnowledgeDataAccessService().getTopFamilyKnowledgeArticlesAbbreviated( 5, true, false);
    }
    /*End Initialization*/
    
    public static FamilyTemplateController.config loadPFS(String pfsId, String academicYearId)
    {
        PFS__c pfsRecord;
        User currentUser = [Select Id, ContactId, ProfileId from User where Id = :UserInfo.getUserId()];
        Academic_Year__c academicYear = [select Id, Name from Academic_Year__c where Id = :academicYearId];
        
        if (GlobalVariables.isSysAdminUser(currentUser) || GlobalVariables.isCallCenterUser(currentUser)){
            pfsRecord = ApplicationUtils.queryPFSRecord(pfsId, null, null);
            // if this not a sys admin or call center user, get record based on contact id and academic year
        } else {
            pfsRecord = ApplicationUtils.queryPFSRecord(null, currentUser.ContactId, academicYearId);
        }
        
        return new FamilyTemplateController.config(pfsRecord, null);
    }//End:loadPFS
    


    
    /*Properties*/    
    public ApplicationUtils appUtils {get; set;}
    public Family_Document__c familyDoc {get; set;}
    /*End Properties*/

}