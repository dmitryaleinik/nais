@isTest
private class SchoolPFSTopBarControllerTest {

    private static User schoolPortalUser;
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    private static Student_Folder__c studentFolder1;

    private static void createTestData() {
        Account family = AccountTestData.Instance.asFamily().create();
        Account school = AccountTestData.Instance.asSchool().create();
        insert new List<Account>{family, school};

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;

        Contact parentA = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(school.Id).create();
        Contact student1 = ContactTestData.Instance
            .asStudent()
            .forAccount(family.Id).create();
        insert new List<Contact> {schoolStaff, parentA, student1};

        System.runAs(thisUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);
        String pfsStatusUnpaid = 'Unpaid';
        String folderSource = 'PFS';

        System.runAs(schoolPortalUser){
            PFS__c pfs1 = PfsTestData.Instance
                .asSubmitted()
                .forPaymentStatus(pfsStatusUnpaid)
                .forParentA(parentA.Id)
                .forOriginalSubmissionDate(System.today().addDays(-3))
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<PFS__c> {pfs1});

            Applicant__c applicant1A = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student1.Id).create();
            Dml.WithoutSharing.insertObjects(new List<Applicant__c> {applicant1A});

            studentFolder1 = StudentFolderTestData.Instance
                .forFolderSource(folderSource)
                .forStudentId(student1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<Student_Folder__c> {studentFolder1});

            String fifthGrade = '5';
            School_PFS_Assignment__c spfsa1 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1A.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> {spfsa1});
        }

        // Share records to school staff
        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);
    }

    @isTest
    private static void testNormalPageLoad() {
        createTestData();

        // Run as full view user
        Test.startTest();
            PageReference pageRef = new PageReference('/apex/SchoolFolderSummary?id=' + studentFolder1.Id);
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(studentFolder1);
            SchoolFolderSummaryController pageController = new SchoolFolderSummaryController(stdController);
            SchoolPFSTopBarController compController = new SchoolPFSTopBarController();
            compController.studentFolderId = pageController.folder.Id;
        Test.stopTest();
        
        System.assert(compController.isFullView);
        // Verify that the right folder was passed and loaded
        System.assertEquals(compController.folder.Id, pageController.folder.Id);
        System.assertEquals(false, compController.atLeastOneSubmitted);
    }
}