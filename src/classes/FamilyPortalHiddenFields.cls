public class FamilyPortalHiddenFields {
    
    public Map<String, HiddenFieldWrapper> hiddenFieldsByScreen { get; set; }
    public String hiddenFieldsByScreenMapKey{get;set;}

    public FamilyPortalHiddenFields() {
        hiddenFieldsByScreenMapKey = '';
        hiddenFieldsByScreen = new Map<String, HiddenFieldWrapper>();
    }

    public FamilyPortalHiddenFields(String SubmissionProcess) {
        hiddenFieldsByScreenMapKey = '';
        hiddenFieldsByScreen = new Map<String, HiddenFieldWrapper>();
        retrieveHiddenFields(SubmissionProcess);
    }
    
    private void retrieveHiddenFields(String SubmissionProcess) {
        HiddenFieldWrapper tmp;
        
        for(Family_Portal_Hidden_Fields__mdt field : [
                               SELECT Id, Screen__c, Field_To_Hide__c, Submission_Process__c 
                               FROM Family_Portal_Hidden_Fields__mdt 
                               WHERE Submission_Process__c =: SubmissionProcess] ) {

            tmp = hiddenFieldsByScreen.containsKey(field.Screen__c)
                ? hiddenFieldsByScreen.get(field.Screen__c)
                : new HiddenFieldWrapper();
            
            tmp.add(field.Field_To_Hide__c);
            
            if(!hiddenFieldsByScreenMapKey.contains(field.Field_To_Hide__c)) {
                hiddenFieldsByScreenMapKey += field.Field_To_Hide__c + ', ';
            }
            if(!hiddenFieldsByScreenMapKey.contains(field.Screen__c)) {
                hiddenFieldsByScreenMapKey += field.Screen__c + ', ';
            }
            
            hiddenFieldsByScreen.put( field.Screen__c, tmp );
        }
    }//End:retrieveHiddenFields
    
    public class HiddenFieldWrapper { 
        public Boolean allInSection { get; set; }
        public String hiddenFields { get; set; }
        
        public HiddenFieldWrapper() {
            allInSection = false;
            hiddenFields = '';
        }
        
        /**
        * @description Creates a set of fields hidden for a given section, determined by current "screen". 
        *              If there's no fields defined and the screen has fieldName='all', it  means that the 
        *              entired section is hidden. 
        * @param fieldName 'All' if the entired section should be hidden. Otherwise, the name of the 
        *        field to be hide.
        */
        public void add(String fieldName) {
            
            if( fieldName != 'All' ) {
                
                if( !hiddenFields.contains(fieldName) ) {
                    hiddenFields += fieldName + ',';
                }
            } else {
                allInSection = true;
            }
        }//End:add
        
    }//End-Class:HiddenFieldWrapper
    
}