/**
 * @description Helper class for  navigation for the Family PFS process.
 **/
@isTest
public class FamilyNavigationServiceTestHelper
{

    @testVisible private static final String FAP_HOUSEHOLD_INFORMATION = 'HouseholdInformation';
    @testVisible private static final String FAP_PARENTS_GUARDIANS = 'ParentsGuardians';
    @testVisible private static final String FAP_APPLICANT_INFORMATION= 'ApplicantInformation';
    @testVisible private static final String FAP_DEPENDENT_INFORMATION = 'DependentInformation';
    @testVisible private static final String FAP_HOUSEHOLD_SUMMARY = 'HouseholdSummary';
    @testVisible private static final String FAP_SCHOOL_SELECTION = 'SchoolSelection';
    @testVisible private static final String FAP_SELECT_SCHOOLS = 'SelectSchools';
    @testVisible private static final String FAP_FAMILY_INCOME = 'FamilyIncome';
    @testVisible private static final String FAP_BASIC_TAX = 'BasicTax';
    @testVisible private static final String FAP_TOTAL_TAXABLE = 'TotalTaxable';
    @testVisible private static final String FAP_TOTAL_NON = 'TotalNon';
    @testVisible private static final String FAP_STUDENT_INCOME = 'StudentIncome';
    @testVisible private static final String FAP_FAMILY_ASSETS = 'FamilyAssets';
    @testVisible private static final String FAP_REAL_ESTATE = 'RealEstate';
    @testVisible private static final String FAP_VEHICLES = 'Vehicles';
    @testVisible private static final String FAP_OTHER_ASSETS = 'OtherAssets';
    @testVisible private static final String FAP_FAMILY_EXPENSES = 'FamilyExpenses';
    @testVisible private static final String FAP_EDUCATIONAL_EXPENSES = 'EducationalExpenses';
    @testVisible private static final String FAP_OTHER_EXPENSES = 'OtherExpenses';
    @testVisible private static final String FAP_BUSINESS_FARM = 'BusinessFarm';
    @testVisible private static final String FAP_BUSINESS_INFORMATION = 'BusinessInformation';
    @testVisible private static final String FAP_BUSINESS_INCOME = 'BusinessIncome';
    @testVisible private static final String FAP_BUSINESS_EXPENSES = 'BusinessExpenses';
    @testVisible private static final String FAP_BUSINESS_ASSETS = 'BusinessAssets';
    @testVisible private static final String FAP_BUSINESS_SUMMARY = 'BusinessSummary';
    @testVisible private static final String FAP_OTHER_INFORMATION = 'OtherInformation';
    @testVisible private static final String FAP_OTHER_CONSIDERATIONS = 'OtherConsiderations';
    @testVisible private static final String FAP_ADDITIONAL_QUESTIONS = 'AdditionalQuestions';
    @testVisible private static final String FAP_PFS_SUBMIT = 'PFSSubmit';
    @testVisible private static final String FAP_MONTHLY_INCOME_EXPENSES_MAIN = 'MonthlyIncomeExpensesMain';
    @testVisible private static final String FAP_MONTHLY_INCOME_EXPENSES = 'MonthlyIncomeExpenses';

    @testVisible
    private static PFS__c insertPfs(Boolean completed)
    {
        Pfs__c pfs = createPfs(completed);
        insert pfs;

        return pfs;
    }

    @testVisible
    private static PFS__c createPfs(Boolean completed)
    {
        Pfs__c pfs;
        String businessOrFarmOwner = 'Yes';

        if (completed)
        {
            pfs = PfsTestData.Instance
                .forOwnBusinessOrFarm(businessOrFarmOwner)
                .asCompleted().create();
        }
        else
        {
            pfs = PfsTestData.Instance
                .forOwnBusinessOrFarm(businessOrFarmOwner)
                .create();
        }

        return pfs;
    }

    @testVisible
    private static Business_Farm__c insertBusinessFarm(Id pfsId)
    {
        Business_Farm__c businessFarm = createBusinessFarm(pfsId);
        insert businessFarm;

        return businessFarm;
    }

    @testVisible
    private static Business_Farm__c createBusinessFarm(Id pfsId)
    {
        Business_Farm__c businessFarm = BusinessFarmTestData.Instance
            .forSequenceNumber(1)
            .asCompleted()
            .forPfsId(pfsId).create();

        return businessFarm;
    }

    @testVisible
    private static List<FamilyAppSettings__c> insertFamilyAppSettings()
    {
        List<FamilyAppSettings__c> familyAppSettings = createAllFamilyAppSettings();
        insert familyAppSettings;

        return familyAppSettings;
    }

    @testVisible
    private static List<FamilyAppSettings__c> createAllFamilyAppSettings() 
    {
        List<FamilyAppSettings__c> fASToInsert = new List<FamilyAppSettings__c>();

        fASToInsert.addAll(createFASsection_HouseholdInformation());
        fASToInsert.addAll(createFASsection_SchoolSelection());
        fASToInsert.addAll(createFASsection_FamilyIncome());
        fASToInsert.addAll(createFASsection_FamilyAssets());
        fASToInsert.addAll(createFASsection_FamilyExpenses());
        fASToInsert.addAll(createFASsection_BusinessFarm());
        fASToInsert.addAll(createFASsection_OtherInformation());
        fASToInsert.addAll(createFASsection_MonthlyIncomeExpensesMain());

        return fASToInsert;
    }

    @testVisible
    private static List<FamilyAppSettings__c> createFASsection_HouseholdInformation()
    {
        return new List<FamilyAppSettings__c> 
            {
            new FamilyAppSettings__c(
                Name = FAP_HOUSEHOLD_INFORMATION,
                Next_Page__c = FAP_PARENTS_GUARDIANS,
                Is_Speedbump_Page__c = true,
                Section_Info_Page__c = null,
                Main_Label_Field__c = 'HouseholdInformation__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = 'statHouseholdInformation__c',
                Status_Field__c = 'statHouseholdInformationBoolean__c',
                Sub_Label_Field__c = null),

            new FamilyAppSettings__c(
                Name = FAP_PARENTS_GUARDIANS,
                Next_Page__c = FAP_APPLICANT_INFORMATION,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'HouseholdInformation',
                Main_Label_Field__c = 'HouseholdInformation__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statParentsGuardians__c',
                Sub_Label_Field__c = 'ParentsGuardians__c'),

            new FamilyAppSettings__c(
                Name = FAP_APPLICANT_INFORMATION,
                Next_Page__c = FAP_DEPENDENT_INFORMATION,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'HouseholdInformation',
                Main_Label_Field__c = 'HouseholdInformation__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statApplicantInformation__c',
                Sub_Label_Field__c = 'ApplicantInformation__c'),

            new FamilyAppSettings__c(
                Name = FAP_DEPENDENT_INFORMATION,
                Next_Page__c = FAP_HOUSEHOLD_SUMMARY,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'HouseholdInformation',
                Main_Label_Field__c = 'HouseholdInformation__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statDependentInformation__c',
                Sub_Label_Field__c = 'DependentInformation__c'),

            new FamilyAppSettings__c(
                Name = FAP_HOUSEHOLD_SUMMARY,
                Next_Page__c = FAP_SCHOOL_SELECTION,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'HouseholdInformation',
                Main_Label_Field__c = 'HouseholdInformation__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statHouseholdSummary__c',
                Sub_Label_Field__c = 'HouseholdSummary__c')
            };
    }

    @testVisible
    private static List<FamilyAppSettings__c> createFASsection_SchoolSelection()
    {
        return new List<FamilyAppSettings__c> 
            {
            new FamilyAppSettings__c(
                Name = FAP_SCHOOL_SELECTION,
                Next_Page__c = FAP_SELECT_SCHOOLS,
                Is_Speedbump_Page__c = true,
                Section_Info_Page__c = null,
                Main_Label_Field__c = 'SchoolSelection__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = 'statSchoolSelection__c',
                Status_Field__c = 'statSchoolSelectionBoolean__c',
                Sub_Label_Field__c = null),
            
            new FamilyAppSettings__c(
                Name = FAP_SELECT_SCHOOLS,
                Next_Page__c = FAP_FAMILY_INCOME,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'SchoolSelection',
                Main_Label_Field__c = 'SchoolSelection__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statSelectSchools__c',
                Sub_Label_Field__c = 'SelectSchools__c')
            };
    }

    @testVisible
    private static List<FamilyAppSettings__c> createFASsection_FamilyIncome()
    {
        return new List<FamilyAppSettings__c> 
            {
            new FamilyAppSettings__c(
                Name = FAP_FAMILY_INCOME,
                Next_Page__c = FAP_BASIC_TAX,
                Is_Speedbump_Page__c = true,
                Section_Info_Page__c = null,
                Main_Label_Field__c = 'FamilyIncome__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = 'statFamilyIncome__c',
                Status_Field__c = 'statFamilyIncomeBoolean__c',
                Sub_Label_Field__c = null),

            new FamilyAppSettings__c(
                Name = FAP_BASIC_TAX,
                Next_Page__c = FAP_TOTAL_TAXABLE,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'FamilyIncome',
                Main_Label_Field__c = 'FamilyIncome__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statBasicTax__c',
                Sub_Label_Field__c = 'BasicTax__c'),

            new FamilyAppSettings__c(
                Name = FAP_TOTAL_TAXABLE,
                Next_Page__c = FAP_TOTAL_NON,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'FamilyIncome',
                Main_Label_Field__c = 'FamilyIncome__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statTotalTaxable__c',
                Sub_Label_Field__c = 'TotalTaxable__c'),

            new FamilyAppSettings__c(
                Name = FAP_TOTAL_NON,
                Next_Page__c = FAP_STUDENT_INCOME,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'FamilyIncome',
                Main_Label_Field__c = 'FamilyIncome__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statTotalNon__c',
                Sub_Label_Field__c = 'TotalNon__c'),

            new FamilyAppSettings__c(
                Name = FAP_STUDENT_INCOME,
                Next_Page__c = FAP_FAMILY_ASSETS,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'FamilyIncome',
                Main_Label_Field__c = 'FamilyIncome__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statStudentIncome__c',
                Sub_Label_Field__c = 'StudentIncome__c')
            };
    }

    @testVisible
    private static List<FamilyAppSettings__c> createFASsection_FamilyAssets()
    {
        return new List<FamilyAppSettings__c> 
            {
                new FamilyAppSettings__c(
                Name = FAP_FAMILY_ASSETS,
                Next_Page__c = FAP_REAL_ESTATE,
                Is_Speedbump_Page__c = true,
                Section_Info_Page__c = null,
                Main_Label_Field__c = 'FamilyAssets__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = 'statFamilyAssets__c',
                Status_Field__c = 'statFamilyAssetsBoolean__c',
                Sub_Label_Field__c = null),

            new FamilyAppSettings__c(
                Name = FAP_REAL_ESTATE,
                Next_Page__c = FAP_VEHICLES,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'FamilyAssets',
                Main_Label_Field__c = 'FamilyAssets__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statRealEstate__c',
                Sub_Label_Field__c = 'RealEstate__c'),

            new FamilyAppSettings__c(
                Name = FAP_VEHICLES,
                Next_Page__c = FAP_OTHER_ASSETS,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'FamilyAssets',
                Main_Label_Field__c = 'FamilyAssets__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statVehicles__c',
                Sub_Label_Field__c = 'Vehicles__c'),

            new FamilyAppSettings__c(
                Name = FAP_OTHER_ASSETS,
                Next_Page__c = FAP_FAMILY_EXPENSES,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'FamilyAssets',
                Main_Label_Field__c = 'FamilyAssets__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statOtherAssets__c',
                Sub_Label_Field__c = 'OtherAssets__c')
            };
    }

    @testVisible
    private static List<FamilyAppSettings__c> createFASsection_FamilyExpenses()
    {
        return new List<FamilyAppSettings__c> 
            {
                new FamilyAppSettings__c(
                Name = FAP_FAMILY_EXPENSES,
                Next_Page__c = FAP_EDUCATIONAL_EXPENSES,
                Is_Speedbump_Page__c = true,
                Section_Info_Page__c = null,
                Main_Label_Field__c = 'FamilyExpenses__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = 'statFamilyExpenses__c',
                Status_Field__c = 'statFamilyExpensesBoolean__c',
                Sub_Label_Field__c = null),

            new FamilyAppSettings__c(
                Name = FAP_EDUCATIONAL_EXPENSES,
                Next_Page__c = FAP_OTHER_EXPENSES,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'FamilyExpenses',
                Main_Label_Field__c = 'FamilyExpenses__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statEducationalExpenses__c',
                Sub_Label_Field__c = 'EducationalExpenses__c'),

            new FamilyAppSettings__c(
                Name = FAP_OTHER_EXPENSES,
                Next_Page__c = FAP_BUSINESS_FARM,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'FamilyExpenses',
                Main_Label_Field__c = 'FamilyExpenses__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statOtherExpenses__c',
                Sub_Label_Field__c = 'OtherExpenses__c')
            };
    }

    @testVisible
    private static List<FamilyAppSettings__c> createFASsection_BusinessFarm()
    {
        return new List<FamilyAppSettings__c> 
            {
                new FamilyAppSettings__c(
                Name = FAP_BUSINESS_FARM,
                Next_Page__c = FAP_BUSINESS_INFORMATION,
                Is_Speedbump_Page__c = true,
                Section_Info_Page__c = null,
                Main_Label_Field__c = 'BusinessFarm__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = 'statBusinessFarm__c',
                Status_Field__c = 'statBusinessFarmBoolean__c',
                Sub_Label_Field__c = null),

            new FamilyAppSettings__c(
                Name = FAP_BUSINESS_INFORMATION,
                Next_Page__c = FAP_BUSINESS_INCOME,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'BusinessFarm',
                Main_Label_Field__c = 'BusinessFarm__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statBusinessInformation__c',
                Sub_Label_Field__c = 'BusinessInformation__c'),

            new FamilyAppSettings__c(
                Name = FAP_BUSINESS_INCOME,
                Next_Page__c = FAP_BUSINESS_EXPENSES,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'BusinessFarm',
                Main_Label_Field__c = 'BusinessFarm__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statBusinessIncome__c',
                Sub_Label_Field__c = 'BusinessIncome__c'),

            new FamilyAppSettings__c(
                Name = FAP_BUSINESS_EXPENSES,
                Next_Page__c = FAP_BUSINESS_ASSETS,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'BusinessFarm',
                Main_Label_Field__c = 'BusinessFarm__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statBusinessExpenses__c',
                Sub_Label_Field__c = 'BusinessExpenses__c'),

            new FamilyAppSettings__c(
                Name = FAP_BUSINESS_ASSETS,
                Next_Page__c = FAP_BUSINESS_SUMMARY,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'BusinessFarm',
                Main_Label_Field__c = 'BusinessFarm__c',
                Loop_Back_Page__c = 'BusinessInformation',
                Completed_Field__c = null,
                Status_Field__c = 'statBusinessAssets__c',
                Sub_Label_Field__c = 'BusinessAssets__c'),

            new FamilyAppSettings__c(
                Name = FAP_BUSINESS_SUMMARY,
                Next_Page__c = FAP_MONTHLY_INCOME_EXPENSES_MAIN,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'BusinessFarm',
                Main_Label_Field__c = 'BusinessFarm__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statBusinessSummary__c',
                Sub_Label_Field__c = 'BusinessSummary__c')
            };
    }

    @testVisible
    private static List<FamilyAppSettings__c> createFASsection_MonthlyIncomeExpensesMain()
    {
        return new List<FamilyAppSettings__c> 
            {
            new FamilyAppSettings__c(
                Name = FAP_MONTHLY_INCOME_EXPENSES_MAIN,
                Next_Page__c = FAP_MONTHLY_INCOME_EXPENSES,
                Is_Speedbump_Page__c = true,
                Section_Info_Page__c = null,
                Main_Label_Field__c = 'MonthlyIncomeAndExpenses__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = 'statMonthlyIncomeExpensesMain__c',
                Status_Field__c = 'statMonthlyIncomeExpensesMainBoolean__c',
                Sub_Label_Field__c = null),

            new FamilyAppSettings__c(
                Name = FAP_MONTHLY_INCOME_EXPENSES,
                Next_Page__c = FAP_OTHER_INFORMATION,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'MonthlyIncomeExpensesMain',
                Main_Label_Field__c = 'MonthlyIncomeAndExpenses__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statMonthlyIncomeExpenses__c',
                Sub_Label_Field__c = 'MonthlyIncomeAndExpenses__c')
            };
    }

    @testVisible
    private static List<FamilyAppSettings__c> createFASsection_OtherInformation()
    {
        return new List<FamilyAppSettings__c> 
            {
                new FamilyAppSettings__c(
                Name = FAP_OTHER_INFORMATION,
                Next_Page__c = FAP_OTHER_CONSIDERATIONS,
                Is_Speedbump_Page__c = true,
                Section_Info_Page__c = null,
                Main_Label_Field__c = 'OtherInformation__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = 'statOtherInformation__c',
                Status_Field__c = 'statOtherInformationBoolean__c',
                Sub_Label_Field__c = null),

            new FamilyAppSettings__c(
                Name = FAP_OTHER_CONSIDERATIONS,
                Next_Page__c = FAP_ADDITIONAL_QUESTIONS,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'OtherInformation',
                Main_Label_Field__c = 'OtherInformation__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statOtherConsiderations__c',
                Sub_Label_Field__c = 'OtherConsiderations__c'),

            new FamilyAppSettings__c(
                Name = FAP_ADDITIONAL_QUESTIONS,
                Next_Page__c = FAP_PFS_SUBMIT,
                Is_Speedbump_Page__c = false,
                Section_Info_Page__c = 'OtherInformation',
                Main_Label_Field__c = 'OtherInformation__c',
                Loop_Back_Page__c = null,
                Completed_Field__c = null,
                Status_Field__c = 'statAdditionalQuestions__c',
                Sub_Label_Field__c = 'AdditionalQuestions__c')
            };
    }
}