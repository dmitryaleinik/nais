/**
 * @description Handles payments made using the Chargent payment provider.
 **/
public without sharing class ChargentProvider implements IPaymentProvider {
    private static final String PAYMENT_PROVIDER_NAME = 'Chargent';
    private static final String PAYMENT_PROCESSOR_NAME = 'NAIS';
    private static final String FRAUD_AVS_FILTERING_SERVICE_ERROR_CODE = '319';

    @testVisible private static final String REQUEST_PARAM = 'request';
    @testVisible private static final String OBJECT_TO_PARSE_PARAM = 'objectToParse';

    @testVisible private static final String NO_PAYMENT_PROVIDER_FOUND = 'No Payment Provider record has been found.';
    @testVisible private static final String NO_PAYMENT_PROCESSOR_FOUND = 'No Payment Processor record has been found.';
    @testVisible private static final String UNEXPECTED_OBJECT_TO_PROCESS =
            'An unexpected object to process has been passed in.';
    @testVisible private static final String DEFAULT_ERROR_MESSAGE =
            '<b>TRANSACTION RESULT:</b> [{0}] {1}<br/><b>ADDITIONAL INFO:</b> {2}';
    @testVisible private static final String UNEXPECTED_ERROR = 'An unexpected error occurred processing the payment.';

    private Payment_Provider__mdt paymentProvider;
    private Payment_Processor__mdt paymentProcessor;

    private List<Integer> internalErrorCodes = new List<Integer> { 100, 101, 102, 108, 252, 257, 258, 370 };
    private List<Integer> dataErrorCodes = new List<Integer> { 126, 212, 226, 301, 302, 305, 310, 320, 322, 323, 324,
            330, 340, 341, 346, 347, 348, 352, 364, 365, 368, 369 };
    private List<Integer> requestErrorCodes = new List<Integer> { 191, 192, 253, 303, 316, 321, 322, 331, 332, 336,
            366 };
    private List<Integer> processingErrorCodes = new List<Integer> { 120, 121, 122, 123, 124, 125, 130, 206, 211, 223,
            251 };
    private List<Integer> declineErrorCodes = new List<Integer> { 110, 111, 127, 207, 209, 213, 214, 215, 216, 217,
            218, 219, 221, 222, 229, 254, 255, 256, 304, 306, 307, 308, 309, 311, 312, 313, 315, 318, 319, 325, 326,
            327, 328, 335, 349, 350, 351, 354, 356, 357, 358, 360, 361, 362, 363, 367, 372, 373, 375, 376, 820 };

    private Map<String, String> avsResultCodeToMessageMapping = new Map<String, String> {
        '10' => Label.FiveDigitZipAddressDoesNotMatch,
        '11' => Label.NineDigitZipAddressDoesNotMatch,
        '12' => Label.ZipDoesNotMatch,
        '13' => Label.PostalCodeDoesNotMatch,
        '14' => Label.AddressNotVerified,
        '20' => Label.ZipAndAddressDoNotMatch,
        '30' => Label.AVSServiceNotSupportedIssuer,
        '31' => Label.AVSSystemNotAvailable,
        '32' => Label.AddressNotAvailable,
        '33' => Label.AVSGeneralError,
        '34' => Label.AVSNotPerformed,
        '40' => Label.AddressFailedEditChecks
    };

    /**
     * @description Prep the Opportunity for payment by transferring the payment information
     *              into the relevant Chargent fields.
     * @param request The Payment or Refund request to update the Opportunity for.
     * @return A PaymentResult that is true if the opportunity has updated properly, else
     *         false.
     * @throws An ArgumentNullException if the request is null.
     * @throws An ArgumentException if the request is or contains an unrecognized
     *         PaymentInformation object.
     */
    public PaymentResult processBefore(PaymentProcessorRequest request) {
        ArgumentNullException.throwIfNull(request, REQUEST_PARAM);
        PaymentResult result = new PaymentResult();
        result.Request = request;

        ChargentOrders__ChargentOrder__c order;
        Id opportunityOrderId;
        Id opportunityId;
        System.savepoint savepoint = Database.setSavepoint();

        try {
            if (request instanceof PaymentRequest) {
                PaymentRequest paymentRequest = (PaymentRequest)request;
                ArgumentNullException.throwIfNull(paymentRequest.Opportunity, 'Opportunity');
                opportunityOrderId = paymentRequest.Opportunity.Chargent_Order__c;
                opportunityId = paymentRequest.Opportunity.Id;

                order = processPaymentBefore(paymentRequest);

                if (opportunityOrderId == null) {
                    update new Opportunity(Id = opportunityId, Chargent_Order__c = order.Id);
                }
            } else if (request instanceof RefundRequest) {
                result.IsSuccess = true;
                result.ErrorOccurred = false;
            } else {
                throw new ArgumentException(UNEXPECTED_OBJECT_TO_PROCESS);
            }

            result.IsSuccess = true;
            result.ErrorOccurred = false;
        } catch (Exception e) {
            result.IsSuccess = false;
            result.ErrorOccurred = true;
            result.ErrorMessage = e.getMessage();

            Database.rollback(savepoint);

            CustomDebugService.Instance.logException(e);
        }

        return result;
    }

    /**
     * @description Create Transaction Line Items out of Transactions after processing
     *              the transaction.
     * @param objectToProcess The transaction to generate Transaction Line Item(s) from.
     * @return A PaymentResult with the outcome of the Transaction Line Item generation.
     * @throws An ArgumentNullException if objectToParse or the result's Request property
     *         is null.
     * @throws An ArgumentException if the result or the result's Request property is an
     *          unrecognized type.
     */
    public PaymentResult processAfter(Object objectToParse) {
        ArgumentNullException.throwIfNull(objectToParse, OBJECT_TO_PARSE_PARAM);

        if (!(objectToParse instanceof PaymentResult)) {
            throw new ArgumentException(UNEXPECTED_OBJECT_TO_PROCESS);
        }

        PaymentResult result = (PaymentResult)objectToParse;
        ArgumentNullException.throwIfNull(result.Request, REQUEST_PARAM);

        if (result.Request instanceof PaymentRequest) {
            PaymentRequest request = (PaymentRequest)result.Request;
            if (request.IsCreditCard) {
                return processCreditCardAfter(result);
            } else if (request.IsECheck) {
                return processECheckAfter(result);
            } else {
                throw new ArgumentException(UNEXPECTED_OBJECT_TO_PROCESS);
            }
        } else if (result.Request instanceof RefundRequest) {
            return processRefundAfter(result);
        } else {
            throw new ArgumentException(UNEXPECTED_OBJECT_TO_PROCESS);
        }

        return result;
    }

    /**
     * @description Process credit card and check payments, and refunds using
     *              a callout to the Chargent base package.
     * @param request The request of either a payment or refund.
     * @return A PaymentResult that contains the outcome of the payment or
     *         refund transaction.
     * @throws An ArgumentNullException if the request is null.
     * @throws An ArgumentException if the request is an unrecognized type.
     */
    public PaymentResult process(PaymentProcessorRequest request) {
        ArgumentNullException.throwIfNull(request, REQUEST_PARAM);
        PaymentResult result = new PaymentResult();
        result.Request = request;

        if (request instanceof PaymentRequest) {
            result = processPayment((PaymentRequest)request);
        } else if (request instanceof RefundRequest) {
            result = processRefund((RefundRequest)request);
        } else {
            throw new ArgumentException(UNEXPECTED_OBJECT_TO_PROCESS);
        }

        result = processAfter(result);

        return result;
    }

    public List<SelectOption> getBillingCountrySelectOptions() {
        return PickListService.getSelectOptionsFromField(
                ChargentOrders__ChargentOrder__c.ChargentOrders__Billing_Country__c);
    }

    public List<SelectOption> getBillingStateUSSelectOptions() {
        return PickListService.getSelectOptionsFromField(
                ChargentOrders__ChargentOrder__c.ChargentOrders__Billing_State__c);
    }

    public List<SelectOption> getExpireMonthSelectOptions() {
        List<SelectOption> options = new List<SelectOption>{
                new SelectOption('1', 'January'),
                new SelectOption('2', 'February'),
                new SelectOption('3', 'March'),
                new SelectOption('4', 'April'),
                new SelectOption('5', 'May'),
                new SelectOption('6', 'June'),
                new SelectOption('7', 'July'),
                new SelectOption('8', 'August'),
                new SelectOption('9', 'September'),
                new SelectOption('10', 'October'),
                new SelectOption('11', 'November'),
                new SelectOption('12', 'December')
        };

        return options;
    }

    public List<SelectOption> getExpireYearSelectOptions() {
        List<SelectOption> options = new List<SelectOption>();

        Integer currentYear = System.today().year();

        for (Integer i=0; i < 10; i++) {
            String currentYearString = String.valueOf(currentYear+i);
            options.add(new SelectOption(currentYearString, currentYearString));
        }

        return options;
    }

    public List<SelectOption> getAccountTypeSelectOptions() {
        return PickListService.getSelectOptionsFromField(
                ChargentOrders__ChargentOrder__c.ChargentOrders__Bank_Account_Type__c);
    }

    public String getCountryCode(String countryName) {
        return countryName;
    }

    public String getDefaultCountryCode() {
        return 'United States';
    }

    public String getCountryName(String countryCode) {
        return countryCode;
    }

    public String getAccountTypeLabel(String accountTypeValue) {
        List<SelectOption> options = getAccountTypeSelectOptions();
        for (SelectOption option : options) {
            if (option.getValue() == accountTypeValue) {
                return option.getLabel();
            }
        }
        return null;
    }

    /**
     * @description Determines if the error code returned from the transaction is an
     *              Internal error or not.
     */
    public Boolean isInternalError(Integer transactionResultCode) {
        return isErrorCodeInList(internalErrorCodes, transactionResultCode);
    }

    /**
     * @description Determines if the error code returned from the transaction is a
     *              Data error or not.
     */
    public Boolean isDataError(Integer transactionResultCode) {
        return isErrorCodeInList(dataErrorCodes, transactionResultCode);
    }

    /**
     * @description Determines if the error code returned from the transaction is a
     *              Request error or not.
     */
    public Boolean isRequestError(Integer transactionResultCode) {
        return isErrorCodeInList(requestErrorCodes, transactionResultCode);
    }

    /**
     * @description Determines if the error code returned from the transaction is a
     *              Processing error or not.
     */
    public Boolean isProcessingError(Integer transactionResultCode) {
        return isErrorCodeInList(processingErrorCodes, transactionResultCode);
    }

    /**
     * @description Determines if the error code returned from the transaction is a
     *              Decline error or not.
     */
    public Boolean isDeclineError(Integer transactionResultCode) {
        return isErrorCodeInList(declineErrorCodes, transactionResultCode);
    }

    public String getErrorMessage(PaymentResult result) {
        // There is no error message since this was a successful result.
        if (result != null && result.isSuccess) {
            return null;
        }

        String errorMessage;
        if (result == null ||
            isRequestError(result.TransactionResultCode) ||
            isProcessingError(result.TransactionResultCode)) {

            errorMessage = Label.PaymentErrorGeneral;
        } else if (isInternalError(result.TransactionResultCode)) {
            errorMessage = Label.PaymentErrorTemp;
        } else if (isDataError(result.TransactionResultCode)) {
            errorMessage = Label.PaymentErrorBadData;
        } else if (isDeclineError(result.TransactionResultCode)) {
            errorMessage = getDeclineErrorMessage(result);
        } else {
            errorMessage = Label.PaymentErrorGeneral;
        }

        return errorMessage;
    }

    private String getDeclineErrorMessage(PaymentResult result) {
        if (String.valueOf(result.TransactionResultCode) == FRAUD_AVS_FILTERING_SERVICE_ERROR_CODE &&
            result.AVSResultCode != null &&
            avsResultCodeToMessageMapping.containsKey(String.valueOf(result.AVSResultCode))) {

            return avsResultCodeToMessageMapping.get(String.valueOf(result.AVSResultCode));
        }
        return Label.PaymentErrorDecline;
    }

    private Boolean isErrorCodeInList(List<Integer> errorCodes, Integer givenErrorCode) {
        for (Integer errorCode : errorCodes) {
            if (givenErrorCode == errorCode) {
                return true;
            }
        }
        return false;
    }

    private ChargentOrders__ChargentOrder__c processPaymentBefore(PaymentRequest request) {
        ChargentOrders__ChargentOrder__c order = new ChargentOrders__ChargentOrder__c(
                Id = request.Opportunity.Chargent_Order__c);
        Boolean partialPayment =
                (request.PaymentInformation.PaymentAmount != request.Opportunity.Amount);

        // Set/Update the ChargentOrder fields required to do the current transaction
        order.ChargentOrders__Billing_First_Name__c = request.PaymentInformation.BillingFirstName;
        order.ChargentOrders__Billing_Last_Name__c = request.PaymentInformation.BillingLastName;
        order.ChargentOrders__Billing_Email__c = request.PaymentInformation.BillingEmail;
        order.ChargentOrders__Billing_Address__c = request.PaymentInformation.BillingStreet1;
        order.ChargentOrders__Billing_Address_Line_2__c = request.PaymentInformation.BillingStreet2;
        order.ChargentOrders__Billing_City__c = request.PaymentInformation.BillingCity;
        order.ChargentOrders__Billing_State__c = request.PaymentInformation.BillingState;
        order.ChargentOrders__Billing_Country__c = request.PaymentInformation.BillingCountryCode;
        order.ChargentOrders__Billing_Zip_Postal__c = request.PaymentInformation.BillingPostalCode;
        order.ChargentOrders__Charge_Amount__c = request.PaymentInformation.PaymentAmount;
        order.ChargentOrders__Invoice_Number__c = request.PaymentInformation.Tracker;
        order.ChargentOrders__Subtotal__c = request.Opportunity.Amount;
        order.ChargentOrders__Payment_Received__c = (partialPayment) ? 'Partial' : 'Full';

        // We are setting the shipping and tax to 0 to ensure that
        // we don't get invalid data from standard salesforce.
        order.ChargentOrders__Shipping__c = 0;
        order.ChargentOrders__Tax__c = 0;

        if (request.PaymentInformation instanceof CreditCardInformation) {
            CreditCardInformation ccInfo = (CreditCardInformation)request.PaymentInformation;
            order.ChargentOrders__Card_Number__c = ccInfo.CardNumber;
            order.ChargentOrders__Card_Last_4__c = ccInfo.CardNumberLastFour;
            order.ChargentOrders__Card_Expiration_Month__c = ccInfo.ExpirationMM;
            order.ChargentOrders__Card_Expiration_Year__c = ccInfo.ExpirationYY;
            order.ChargentOrders__Card_Security_Code__c = ccInfo.Cvv2;
            order.ChargentOrders__Card_Type__c = ccInfo.CardType;
            order.ChargentOrders__Payment_Method__c = 'Credit Card';

        } else if (request.PaymentInformation instanceof ECheckInformation) {
            ECheckInformation ecInfo = (ECheckInformation)request.PaymentInformation;
            order.ChargentOrders__Bank_Name__c = ecInfo.BankName;
            order.ChargentOrders__Bank_Account_Name__c = ecInfo.NameOnCheck;
            order.ChargentOrders__Bank_Account_Number__c = ecInfo.AccountNumber;
            order.ChargentOrders__Bank_Routing_Number__c = ecInfo.RoutingNumber;
            order.ChargentOrders__Bank_Account_Type__c = ecInfo.AccountType;
            order.ChargentOrders__Payment_Method__c = 'Check';

        } else {
            throw new ArgumentException(UNEXPECTED_OBJECT_TO_PROCESS);
        }

        upsert order;

        return order;
    }

    private PaymentResult processCreditCardAfter(PaymentResult result) {
        PaymentRequest request = (PaymentRequest)result.Request;
        CreditCardInformation ccInfo = (CreditCardInformation)request.PaymentInformation;

        if (result.IsSuccess) {
            result.TransactionLineItemId = PaymentUtils.insertCreditCardTransactionLineItem(request.OpportunityId, ccInfo.PaymentAmount, ccInfo.CardType, ccInfo.CardNumberLastFour, result);
        } else {
            result.ErrorOccurred = PaymentUtils.hasCrossedErrorAttempts(request.OpportunityId);

            if (result.ErrorOccurred) {
                result.ErrorMessage = Label.TLI_Unsuccessful_Payment_Attempts;
            } else {
                result.TransactionLineItemId = PaymentUtils.insertCreditCardTransactionLineItem(request.OpportunityId, ccInfo.PaymentAmount, ccInfo.CardType, ccInfo.CardNumberLastFour, result);
                result.ErrorMessage = String.format(DEFAULT_ERROR_MESSAGE, new List<String>{
                        String.valueOf(result.transactionResultCode),
                        result.ErrorData,
                        result.additionalInfo
                });
            }
        }

        return result;
    }

    private PaymentResult processECheckAfter(PaymentResult result) {
        PaymentRequest request = (PaymentRequest)result.Request;
        ECheckInformation ecInfo = (ECheckInformation)request.PaymentInformation;

        if (result.IsSuccess) {
            result.TransactionLineItemId = PaymentUtils.insertECheckTransactionLineItem(request.OpportunityId, ecInfo.paymentAmount, result);
        } else {
            result.ErrorOccurred = PaymentUtils.hasCrossedErrorAttempts(request.OpportunityId);

            if (result.ErrorOccurred) {
                result.ErrorMessage = Label.TLI_Unsuccessful_Payment_Attempts;
            } else {
                result.TransactionLineItemId = PaymentUtils.insertECheckTransactionLineItem(request.OpportunityId, ecInfo.paymentAmount, result);
                result.ErrorMessage = String.format(DEFAULT_ERROR_MESSAGE, new List<String> {
                        String.valueOf(result.TransactionResultCode),
                        result.ErrorData,
                        result.additionalInfo
                });
            }
        }

        return result;
    }

    private PaymentResult processRefundAfter(PaymentResult result) {
        RefundRequest request = (RefundRequest)result.Request;

        if (result.IsSuccess || System.Test.isRunningTest()) {
            // Gather the Transaction information
            Transaction_Settings__c transactionSettings = Transaction_Settings__c.getInstance('Refund-CC');
            ChargentOrders__Transaction__c transactionRecord = (ChargentOrders__Transaction__c)result.TransactionRecord;

            // Create refund TLI
            Transaction_Line_Item__c transactionLineItem = new Transaction_Line_Item__c(
                    Transaction_ID__c = (transactionRecord != null) ? transactionRecord.ChargentOrders__Gateway_ID__c : null,
                    Transaction_Result_Code__c = String.valueOf(result.TransactionResultCode),
                    VaultGuid__c = result.VaultGUID,
                    Tender_type__c = request.TransactionLineItem.Tender_type__c,
                    Account_Label__c = request.TransactionLineItem.Account_Label__c,
                    Amount__c = request.TransactionLineItem.Amount__c,
                    RecordTypeId = RecordTypes.refundTransactionTypeId,
                    Opportunity__c = request.TransactionLineItem.Opportunity__c,
                    Transaction_Status__c = 'Posted',
                    CC_Last_4_Digits__c = request.TransactionLineItem.CC_Last_4_Digits__c,
                    Description__c = 'Refund for TLI Id: ' + request.TransactionLineItem.Id,
                    Transaction_Date__c = Date.today(),
                    Transaction_Type__c = (transactionSettings != null) ? transactionSettings.Name : '',
                    Transaction__c = (transactionRecord != null) ? transactionRecord.Id : null,
                    Transaction_Code__c = (transactionSettings != null) ? transactionSettings.Transaction_Code__c : '',
                    Account_Code__c = (transactionSettings != null) ? transactionSettings.Account_Code__c : '');
            insert transactionLineItem;
            result.TransactionLineItemId = transactionLineItem.Id;
        } else {
            result.ErrorOccurred = true;
            result.ErrorMessage = (!String.isBlank(result.errorData)) ? result.errorData :
                    'There was a problem processing the refund.';
        }

        return result;
    }

    private PaymentResult processPayment(PaymentRequest request) {
        PaymentResult result = new PaymentResult();
        result.Request = request;

        ChargentOrders.TChargentOperations.TChargentResult chargentResult;

        // Perform transaction
        try {
            chargentResult = ChargentOrders.TChargentOperations.ChargeOrder_Click(request.Opportunity.Chargent_Order__c);
        } catch (Exception e) {
            // log here to custom debug object
            CustomDebugService.Instance.logException(e);

            result.isSuccess = false;
            result.ErrorOccurred = true;
            result.ErrorMessage = UNEXPECTED_ERROR;
        }

        // Query for the Transaction and Order records for validation
        List<ChargentOrders__Transaction__c> transactions;
        if (chargentResult != null && !result.ErrorOccurred) {
            transactions = ChargentOrdersTransactionSelector.Instance
                    .selectById(new Set<Id>{
                            chargentResult.TransactID
                    });

            if (!transactions.isEmpty()) {
                // SFP-1064 Extract the AVS Response Code until Chargent has done it on their end.
                //          The previous logic simply set the result.TransactionRecord = transactions[0];
                result.TransactionRecord = processAvsResponseCode(transactions[0]);

                List<ChargentOrders__ChargentOrder__c> orders = ChargentOrderSelector.Instance.selectById(
                        new Set<Id> { transactions[0].ChargentOrders__Order__c });

                // SFP-1123: If there's an order record use that otherwise attempt to use just the Id.
                // On occasion order records are not having payment information cleared and it seems
                // that this may be the issue.
                if (!orders.isEmpty()) {
                    result.OrderRecord = orders[0];
                } else if (transactions[0].ChargentOrders__Order__c != null) {
                    result.OrderRecord =
                            new ChargentOrders__ChargentOrder__c(Id = transactions[0].ChargentOrders__Order__c);
                }
            }
        }

        if (chargentResult == null || result.TransactionRecord == null) {
            // There is an error generating the transaction
            result.IsSuccess = false;
            result.ErrorOccurred = true;
            result.ErrorMessage = (result.ErrorMessage == null) ? UNEXPECTED_ERROR : result.ErrorMessage;
        } else {
            ChargentOrders__Transaction__c transactionRecord = (ChargentOrders__Transaction__c) result.TransactionRecord;

            // There transaction was processed, let's parse the results.
            result.IsSuccess = transactionRecord.ChargentOrders__Response_Status__c == 'Approved';
            result.ErrorOccurred = !result.IsSuccess;
            result.ErrorData = transactionRecord.ChargentOrders__Response_Status__c;
            result.AdditionalInfo = transactionRecord.ChargentOrders__Response_Message__c;
            result.TransactionResultCode = (transactionRecord.ChargentOrders__Response__c == null) ?
                    null : Integer.valueOf(transactionRecord.ChargentOrders__Response__c);
            result.AVSResultCode = (transactionRecord.ChargentOrders__AVS_Response_Code__c == null) ?
                    null : Integer.valueOf(transactionRecord.ChargentOrders__AVS_Response_Code__c);
            result.VaultGUID = transactionRecord.ChargentOrders__Tokenization__c;
            result.TransactionNumber = transactionRecord.ChargentOrders__Gateway_ID__c;

            if (!result.IsSuccess) {
                result.ErrorMessage = (String.isNotBlank(transactionRecord.ChargentOrders__AVS_Response_Code__c)) ?
                        transactionRecord.ChargentOrders__Description__c :
                        transactionRecord.ChargentOrders__Response_Message__c;
            }
        }

        // Allow our payment providers to be tested
        if (Test.isRunningTest()) {
            result = (new PaymentProviderTestHelpers(request)).Result;
            return result;
        }

        return result;
    }

    private PaymentResult processRefund(RefundRequest request) {
        PaymentResult result = new PaymentResult();
        result.Request = request;

        ChargentOrders.TChargentOperations.TChargentResult chargentResult;

        // Perform transaction
        try {
            chargentResult = ChargentOrders.TChargentOperations.RefundTransaction_Click(request.TransactionLineItem.Transaction__c);
        } catch (Exception e) {
            // log here to custom debug object
            CustomDebugService.Instance.logException(e);

            result.IsSuccess = false;
            result.ErrorOccurred = true;
            result.ErrorMessage = UNEXPECTED_ERROR;
        }

        // Query for the Transaction record for validation
        List<ChargentOrders__Transaction__c> transactions;
        if (!result.ErrorOccurred) {
            transactions = ChargentOrdersTransactionSelector.Instance
                    .selectById(new Set<Id>{
                            chargentResult.TransactID
                    });
        }

        if (chargentResult == null || transactions == null || transactions.isEmpty()) {
            // There is an error generating the transaction
            result.IsSuccess = false;
            result.ErrorOccurred = true;
            result.ErrorMessage = (result.ErrorMessage == null) ? UNEXPECTED_ERROR : result.ErrorMessage;
        } else {
            // There transaction was processed, let's parse the results.
            result.TransactionRecord = transactions[0];

            result.IsSuccess = transactions[0].ChargentOrders__Response_Status__c == 'Approved';
            result.ErrorOccurred = !result.IsSuccess;
            result.ErrorData = transactions[0].ChargentOrders__Response_Status__c;
            result.AdditionalInfo = transactions[0].ChargentOrders__Response_Message__c;
            result.TransactionResultCode = Integer.valueOf(transactions[0].ChargentOrders__Response__c);
            result.VaultGUID = transactions[0].ChargentOrders__Tokenization__c;
            result.TransactionNumber = transactions[0].ChargentOrders__Gateway_ID__c;

            if (!result.IsSuccess) {
                result.ErrorMessage = transactions[0].ChargentOrders__Response_Message__c;
            }
        }

        // Allow our payment providers to be tested
        if (Test.isRunningTest()) {
            return (new PaymentProviderTestHelpers(request)).Result;
        }

        return result;
    }

    private ChargentOrders__Transaction__c processAvsResponseCode(ChargentOrders__Transaction__c transactionRecord) {
        Pattern avsResultCodePattern = pattern.compile(' <avsResult>(\\d+)(?=)</avsResult>');
        Matcher avsResultCodeMatcher = avsResultCodePattern.matcher(transactionRecord.ChargentOrders__Gateway_Response__c);

        // Gather the error code from the xml
        String avsResponseCode;
        while (avsResultCodeMatcher.find()) {
            avsResponseCode = avsResultCodeMatcher.group(1);
        }

        // If nothing was found exit early
        if (String.isBlank(avsResponseCode)) {
            return transactionRecord;
        }

        // Update the AVS Response Code and Description fields
        transactionRecord.ChargentOrders__AVS_Response_Code__c = avsResponseCode;
        transactionRecord.ChargentOrders__Description__c = avsResultCodeToMessageMapping.get(avsResponseCode);
        update transactionRecord;

        return transactionRecord;
    }
}