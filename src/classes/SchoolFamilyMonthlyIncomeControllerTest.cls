@isTest
private class SchoolFamilyMonthlyIncomeControllerTest{
    
    @isTest
    private static void testSchedule() {
        Account family = AccountTestData.Instance.asFamily().create();
        Account school = AccountTestData.Instance.asSchool().create();
        insert new List<Account>{family, school};

        Contact parentA = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .forAccount(school.Id)
            .asSchoolStaff().create();
        Contact student1 = ContactTestData.Instance
            .asStudent().create();
        insert new List<Contact>{parentA, schoolStaff, student1};
        
        User schoolPortalUser;
        System.runAs(SchoolFamilyContribWrksheetBulkCont.thisUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.insertAcademicYear();
        PFS__c pfs1;
        School_PFS_Assignment__c spfsa1;

        System.runAs(schoolPortalUser) {
            pfs1 = PfsTestData.Instance
                .asSubmitted()
                .forParentA(parentA.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<PFS__c> {pfs1});

            Applicant__c applicant1 = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student1.Id).create();
            Dml.WithoutSharing.insertObjects(new List<Applicant__c> {applicant1});

            Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
                .forStudentId(student1.Id)
                .forSchoolId(school.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<Student_Folder__c> {studentFolder1});

            String fifthGrade = '5';
            spfsa1 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> {spfsa1});
        }

        //For: getTotalMonthlyIncome
        pfs1.Alimony_Current__c = 12000;//Divided by 12 months=1000
        pfs1.Child_Support_Received_Current__c = 24000;//Divided by 12 months=2000
        pfs1.Monthly_Income_Parents_After_Taxes__c = 2500;
        pfs1.Monthly_Income_Applicants_After_Taxes__c = 2400;
        pfs1.Other_Monthly_Income__c = 3;

        //For: getTotalMonthlyIncome
        pfs1.Monthly_Expenses_Alimony__c = 100;
        pfs1.Monthly_Expenses_Child_Support__c = 300;
        pfs1.Other_Monthly_Support_Expenses__c = 100;

        //For: getTotalMonthlyHousingExpenses
        pfs1.Monthly_Expenses_Mortage_Rent_primary__c = 100;
        pfs1.Monthly_Expenses_Utilities__c = 200;
        pfs1.Monthly_Expenses_Property_Taxes_Expenses__c = 300;
        pfs1.Monthly_Expenses_Home_s_Owner_Insurance__c = 400;
        pfs1.Monthly_Expenses_Association_Fees__c = 500;
        pfs1.Monthly_Expenses_Maintenance__c = 600;
        pfs1.Monthly_Expenses_Mortage_Rent_all__c = 700;
        pfs1.Other_Monthly_Housing_Expenses__c = 30;

        //For: getTotalMonthlyTuitionExpenses
        pfs1.Monthly_Expenses_Tuition__c = 50;
        pfs1.Monthly_Expenses_Childcare__c = 40;
        pfs1.Other_Monthly_Tuition_Childcare_Expenses__c = 300;

        //For: getTotalMonthlyVehicleExpenses
        pfs1.Monthly_Expenses_Vehicle_Payments__c = 50;
        pfs1.Monthly_Expenses_Vehicle_Insurance__c = 40;
        pfs1.Monthly_Expenses_Vehicle_Maintenance__c = 300;
        pfs1.Other_Monthly_Expenses_Vehicle__c = 300;

        //For: getTotalMonthlyHealthcareExpenses
        pfs1.Monthly_Expenses_Healthcare_Insurance__c = 250;
        pfs1.Monthly_Expenses_Dental_Insurance__c = 30;
        pfs1.Monthly_Expenses_Life_Insurance__c = 20;
        pfs1.Monthly_Expenses_Insurance_not_covered__c = 15;
        pfs1.Other_Monthly_Healthcare_Expenses__c = 12;

        //For: getTotalMonthlyFoodExpenses
        pfs1.Monthly_Expenses_Groceries__c = 120;
        pfs1.Monthly_Expenses_Dining_out__c = 30;
        pfs1.Other_Monthly_Food_Expenses__c = 20;

        //For: getTotalMonthlyMiscellaneousExpenses
        pfs1.Monthly_Expenses_Clothing__c = 15;
        pfs1.Monthly_Expenses_Pet_Care__c = 15;
        pfs1.Monthly_Expenses_Bus_Metro_Rail__c = 10;
        pfs1.Monthly_Expenses_Savings__c = 10;
        pfs1.Monthly_Expenses_Entertainment__c = 55;
        pfs1.Other_Monthly_Miscellaneous_Expenses__c = 35;

        update pfs1;
        
        PageReference pageTest = Page.SchoolFamilyMonthlyIncome;
        pageTest.getParameters().put('SchoolPFSAssignmentId', spfsa1.Id);
        Test.setCurrentPage(pageTest);
        
        Test.startTest();
            SchoolFamilyMonthlyIncomeController controller = new SchoolFamilyMonthlyIncomeController();
        Test.stopTest();
        
        //Test total formula fields  
        System.assertEquals('7903.00', String.ValueOf(controller.pfsRecord.Monthly_Income_Total__c));
        System.assertEquals('500.00', String.ValueOf(controller.pfsRecord.Monthly_Support_Total__c));
        System.assertEquals('2830.00', String.ValueOf(controller.pfsRecord.Monthly_Housing_Total__c));
        System.assertEquals('390.00', String.ValueOf(controller.pfsRecord.Monthly_Tuition_Total__c));
        System.assertEquals('690.00', String.ValueOf(controller.pfsRecord.Monthly_Vehicle_Total__c));
        System.assertEquals('327.00', String.ValueOf(controller.pfsRecord.Monthly_Healthcare_Total__c));
        System.assertEquals('170.00', String.ValueOf(controller.pfsRecord.Monthly_Food_Total__c));
        System.assertEquals('140.00', String.ValueOf(controller.pfsRecord.Monthly_Miscellaneous_Total__c));

        System.assertEquals(controller, controller.Me);
    }
}