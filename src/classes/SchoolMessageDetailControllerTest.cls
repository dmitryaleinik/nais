/**
 * SchoolMessageDetailControllerTest.cls
 *
 * @description: Test class for SchoolMessageDetailController using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
private class SchoolMessageDetailControllerTest {
    
    /* test data setup */
    @testSetup 
    static void setupTestData() {
        
        MassEmailSendTestDataFactory.createEmailTemplate();
    }

    /* postive test cases */

    // test controller init w/ header/academic year selector
    @isTest 
    static void constructor_validUser_validInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            Test.startTest();
                
                PageReference msgDetailPRef = Page.SchoolMessageDetail;
                Test.setCurrentPage( msgDetailPRef);
                SchoolMessageDetailController msgDetailController = new SchoolMessageDetailController();
                msgDetailController.loadAcademicYear();
                msgDetailController.SchoolAcademicYearSelector_OnChange( null);
            Test.stopTest();

            // Assert
            System.assertEquals( msgDetailController.Me, msgDetailController);
            System.assertEquals( msgDetailController.getAcademicYearId(), GlobalVariables.getCurrentAcademicYear().Id);
        }
    }

    // test page action method w/ valid user, valid init
    @isTest 
    static void initModelAndView_validUser_validInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();
                
                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();

                // re-query massES record and instantiate page
                Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                                     Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                                     Html_Body__c, Footer__c, Reply_To_Address__c
                                               from Mass_Email_Send__c
                                              where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];

                PageReference msgDetailPRef = Page.SchoolMessageDetail;
                msgDetailPRef.getParameters().put( SchoolMessageDetailController.QUERY_STRING_MESSAGE_DETAIL, massES.Id);
                Test.setCurrentPage( msgDetailPRef);
                SchoolMessageDetailController msgDetailController = new SchoolMessageDetailController();
                Boolean enabled = msgDetailController.massEmailEnabled;
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, msgDetailController.massESModel);
        }
    }

    // test page action method w/ valid user w/ 1 failed send event, 1 open event valid init
    @isTest 
    static void initModelAndView_validUserFailedAndOpenEvent_validInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();

                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();
                
                Contact c2 = [select Id, Name
                                from Contact
                               where Email = :MassEmailSendTestDataFactory.TEST_PARENT2_CONTACT_EMAIL];
                Contact c1 = [SELECT Id, Name
                              FROM Contact
                              WHERE Email = :MassEmailSendTestDataFactory.TEST_PARENT1_CONTACT_EMAIL];
                Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                                    Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                                    Html_Body__c, Footer__c, Reply_To_Address__c, Failed_to_Send__c
                                               from Mass_Email_Send__c
                                              where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
                massES.Failed_to_Send__c = c2.Id + ',';
                update massES;

                Contact contactStudent = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_STUDENT1_CONTACT_EMAIL];
                Applicant__c app = [select Id from Applicant__c where Contact__c = :contactStudent.Id];
                School_PFS_Assignment__c sPFS = [select Id, Name, Mass_Email_Events__c
                                                   from School_PFS_Assignment__c
                                                  where Applicant__c = :app.Id]; 
                sPFS.Mass_Email_Events__c = MassEmailSendTestDataFactory.createMassEmailJSONEvent( 
                                            sPFS.Id, massES.School__c, c1.Id, massES.Id, SchoolMassEmailSendModel.STATUS_OPENED);
                update sPFS;

                // re-query massES record and instantiate page
                massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                 Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                 Html_Body__c, Footer__c, Reply_To_Address__c, Failed_to_Send__c
                            from Mass_Email_Send__c
                           where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
                                          
                PageReference msgDetailPRef = Page.SchoolMessageDetail;
                msgDetailPRef.getParameters().put( SchoolMessageDetailController.QUERY_STRING_MESSAGE_DETAIL, massES.Id);
                Test.setCurrentPage( msgDetailPRef);
                SchoolMessageDetailController msgDetailController = new SchoolMessageDetailController();
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, msgDetailController.massESModel);
            System.assertNotEquals( null, msgDetailController.allRecipModelList);
            System.assertEquals( 2, msgDetailController.allRecipModelList.size());
            System.assertNotEquals( null, msgDetailController.bouncedRecipModelList);
            System.assertNotEquals( null, msgDetailController.openedRecipModelList);
            // Only one Open event created, so only 1 recip will return in query
            System.assertEquals( 1, msgDetailController.openedRecipModelList.size());
            System.assertNotEquals( null, msgDetailController.unsubRecipModelList);
            System.assertNotEquals( null, msgDetailController.failedRecipModelList);
            System.assertEquals( 1, msgDetailController.failedRecipModelList.size());
            System.assertEquals(0, msgDetailController.unopenedRecipModels.size());

        }
    }

    // test page action method w/ valid user w/ stored open event, valid init
    @isTest 
    static void initModelAndView_validUserOpenEvent_validInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();

                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();

                Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                                     Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                                     Html_Body__c, Footer__c, Reply_To_Address__c
                                               from Mass_Email_Send__c
                                              where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];

                Contact c = [select Id, Name
                               from Contact
                              where Email = :MassEmailSendTestDataFactory.TEST_PARENT2_CONTACT_EMAIL];

                Contact contactStudent = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_STUDENT1_CONTACT_EMAIL];
                Applicant__c app = [select Id from Applicant__c where Contact__c = :contactStudent.Id];
                School_PFS_Assignment__c sPFS = [select Id, Name, Mass_Email_Events__c
                                                   from School_PFS_Assignment__c
                                                  where Applicant__c = :app.Id]; 
                sPFS.Mass_Email_Events__c = MassEmailSendTestDataFactory.createMassEmailJSONEvent( 
                                            sPFS.Id, massES.School__c, c.Id, massES.Id, SchoolMassEmailSendModel.STATUS_OPENED); 
                update sPFS;
                                          
                PageReference msgDetailPRef = Page.SchoolMessageDetail;
                msgDetailPRef.getParameters().put( SchoolMessageDetailController.QUERY_STRING_MESSAGE_DETAIL, massES.Id);
                Test.setCurrentPage( msgDetailPRef);
                SchoolMessageDetailController msgDetailController = new SchoolMessageDetailController();
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, msgDetailController.massESModel);
            System.assertNotEquals( null, msgDetailController.allRecipModelList);
            System.assertEquals( 2, msgDetailController.allRecipModelList.size());
            System.assertNotEquals( null, msgDetailController.bouncedRecipModelList);
            System.assertNotEquals( null, msgDetailController.openedRecipModelList);
            // Only one Open event created, so only 1 recip will return in query
            System.assertEquals( 1, msgDetailController.openedRecipModelList.size());
            System.assertNotEquals( null, msgDetailController.unsubRecipModelList);
            System.assertEquals(1, msgDetailController.unopenedRecipModels.size());
        }
    }

    // test page action method w/ valid user w/ stored bounce event, valid init
    @isTest 
    static void initModelAndView_validUserBounceEvent_validInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData = new MassEmailSendTestDataFactory( false);

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();

                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();

                Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                                    Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                                    Html_Body__c, Footer__c, Reply_To_Address__c
                                               from Mass_Email_Send__c
                                              where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
                Contact c = [select Id, Name
                               from Contact
                              where Email = :MassEmailSendTestDataFactory.TEST_PARENT2_CONTACT_EMAIL];
                
                Contact contactStudent = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_STUDENT1_CONTACT_EMAIL];
                Applicant__c app = [select Id from Applicant__c where Contact__c = :contactStudent.Id];
                School_PFS_Assignment__c sPFS = [select Id, Name, Mass_Email_Events__c
                                                   from School_PFS_Assignment__c
                                                  where Applicant__c = :app.Id]; 
                sPFS.Mass_Email_Events__c = MassEmailSendTestDataFactory.createMassEmailJSONEvent( 
                                            sPFS.Id, massES.School__c, c.Id, massES.Id, SchoolMassEmailSendModel.STATUS_BOUNCED);
                update sPFS;
                                          
                PageReference msgDetailPRef = Page.SchoolMessageDetail;
                msgDetailPRef.getParameters().put( SchoolMessageDetailController.QUERY_STRING_MESSAGE_DETAIL, massES.Id);
                Test.setCurrentPage( msgDetailPRef);
                SchoolMessageDetailController msgDetailController = new SchoolMessageDetailController();
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, msgDetailController.massESModel);
            System.assertNotEquals( null, msgDetailController.allRecipModelList);
            System.assertEquals( 2, msgDetailController.allRecipModelList.size());
            System.assertNotEquals( null, msgDetailController.bouncedRecipModelList);
            // Only one Bounce event created, so only 1 recip will return in query
            System.assertEquals( 1, msgDetailController.bouncedRecipModelList.size());
            System.assertNotEquals( null, msgDetailController.openedRecipModelList);
            System.assertNotEquals( null, msgDetailController.unsubRecipModelList);
            System.assertEquals(1, msgDetailController.unopenedRecipModels.size());
        }
    }

    // test page action method w/ valid user w/ stored unsubscribe event, valid init
    @isTest 
    static void initModelAndView_validUserUnsubscribeEvent_validInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();

                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();

                Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                                    Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                                    Html_Body__c, Footer__c, Reply_To_Address__c
                                               from Mass_Email_Send__c
                                              where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
                Contact c = [select Id, Name
                               from Contact
                              where Email = :MassEmailSendTestDataFactory.TEST_PARENT2_CONTACT_EMAIL];

                Contact contactStudent = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_STUDENT1_CONTACT_EMAIL];
                Applicant__c app = [select Id from Applicant__c where Contact__c = :contactStudent.Id];
                School_PFS_Assignment__c sPFS = [select Id, Name, Mass_Email_Events__c
                                                   from School_PFS_Assignment__c
                                                  where Applicant__c = :app.Id]; 
                sPFS.Mass_Email_Events__c = MassEmailSendTestDataFactory.createMassEmailJSONEvent( 
                                            sPFS.Id, massES.School__c, c.Id, massES.Id, SchoolMassEmailSendModel.STATUS_UNSUB);
                update sPFS;
                                          
                PageReference msgDetailPRef = Page.SchoolMessageDetail;
                msgDetailPRef.getParameters().put( SchoolMessageDetailController.QUERY_STRING_MESSAGE_DETAIL, massES.Id);
                Test.setCurrentPage( msgDetailPRef);
                SchoolMessageDetailController msgDetailController = new SchoolMessageDetailController();
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, msgDetailController.massESModel);
            System.assertNotEquals( null, msgDetailController.allRecipModelList);
            System.assertEquals( 2, msgDetailController.allRecipModelList.size());
            System.assertNotEquals( null, msgDetailController.bouncedRecipModelList);
            System.assertNotEquals( null, msgDetailController.openedRecipModelList);
            System.assertNotEquals( null, msgDetailController.unsubRecipModelList);
            // Only one Unsub event created, so only 1 recip will return in query
            System.assertEquals( 1, msgDetailController.unsubRecipModelList.size());
            System.assertEquals(1, msgDetailController.unopenedRecipModels.size());
        }
    }

    // test page action method w/ valid user w/ stored open event on multiple mass ES records, valid init
    @isTest 
    static void initModelAndView_validUserOpenEventMultipleMassES_validInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
            Test.startTest();

                // Must create revelant test data records as portal user so that portal user owns records and they
                // are visible during test execution
                MassEmailSendTestDataFactory.createRequiredPFSData();

                Mass_Email_Send__c massES = [select Id, Name, Status__c, Sent_By__c, Sent_By__r.Name, Recipients__c,
                                                    Date_Sent__c, School_PFS_Assignments__c, School__c, Subject__c, Text_Body__c, 
                                                    Html_Body__c, Footer__c, Reply_To_Address__c
                                               from Mass_Email_Send__c
                                              where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];

                String massES2Id = [select Id from Mass_Email_Send__c where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL2_NAME].Id;
                Contact c = [select Id, Name
                               from Contact
                              where Email = :MassEmailSendTestDataFactory.TEST_PARENT2_CONTACT_EMAIL];

                Contact contactStudent = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_STUDENT1_CONTACT_EMAIL];
                Applicant__c app = [select Id from Applicant__c where Contact__c = :contactStudent.Id];
                School_PFS_Assignment__c sPFS = [select Id, Name, Mass_Email_Events__c
                                                   from School_PFS_Assignment__c
                                                  where Applicant__c = :app.Id]; 

                String massEmailEventString = MassEmailSendTestDataFactory.createMassEmailJSONEvent( 
                                              sPFS.Id, massES.School__c, c.Id, massES.Id, SchoolMassEmailSendModel.STATUS_OPENED);
                // create extra event tied to another massES record, assert will show this event does not get included in total count
                massEmailEventString += ',' + MassEmailSendTestDataFactory.createMassEmailJSONEvent( 
                                              sPFS.Id, massES.School__c, c.Id, massES2Id, SchoolMassEmailSendModel.STATUS_OPENED);

                sPFS.Mass_Email_Events__c = massEmailEventString;
                update sPFS;
                                          
                PageReference msgDetailPRef = Page.SchoolMessageDetail;
                msgDetailPRef.getParameters().put( SchoolMessageDetailController.QUERY_STRING_MESSAGE_DETAIL, massES.Id);
                Test.setCurrentPage( msgDetailPRef);
                SchoolMessageDetailController msgDetailController = new SchoolMessageDetailController();
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, msgDetailController.massESModel);
            System.assertNotEquals( null, msgDetailController.allRecipModelList);
            System.assertEquals( 2, msgDetailController.allRecipModelList.size());
            System.assertNotEquals( null, msgDetailController.bouncedRecipModelList);
            System.assertNotEquals( null, msgDetailController.openedRecipModelList);
            // Only one Open event created, so only 1 recip will return in query
            System.assertEquals( 1, msgDetailController.openedRecipModelList.size());
            System.assertNotEquals( null, msgDetailController.unsubRecipModelList);
            System.assertEquals(1, msgDetailController.unopenedRecipModels.size());
        }
    }

}