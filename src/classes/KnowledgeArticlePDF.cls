public without sharing class KnowledgeArticlePDF {
    
    public Knowledge__kav article { get; set; }
    
    public KnowledgeArticlePDF()
    {
        String articleId = System.currentPagereference().getParameters().get('id');
        this.article = this.retrieveKnowledgeArticle(Id.valueOf(articleId));
        
        if(this.article == null){            
           throw new KnowledgeArticlePDFException('Invalid id for Knowledge Article.');            
        }
    }//End-Constructor
    
    private Knowledge__kav retrieveKnowledgeArticle(Id articleId)
    {
        List<Knowledge__kav> queryResult = 
            new List<Knowledge__kav>([
                SELECT Article_Body__c, title 
                FROM Knowledge__kav   
                WHERE PublishStatus='Online' 
                AND IsLatestVersion=true 
                AND KnowledgeArticleId  IN 
                    (SELECT ParentId 
                    From KnowledgeArticleViewStat 
                    WHERE ParentId =: articleId
                    AND ParentId <> null)
            ]);
            
        return !queryResult.isEmpty()  ? queryResult[0] : null;
    }//End:retrieveKnowledgeArticle
    
    public class KnowledgeArticlePDFException extends Exception{}
}//End-Class