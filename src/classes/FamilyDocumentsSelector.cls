/**
 * @description This class is responsible for querying Family_Document__c records.
 */
public class FamilyDocumentsSelector extends fflib_SObjectSelector
{

    @testVisible private static final String ID_SET_PARAM = 'idSet';
    @testVisible private static final String HOUSE_HOLD_IDS_PARAM = 'householdIds';
    @testVisible private static final String FAMILY_DOCS_FIELDS_TO_SELECT_PARAM = 'familyDocFieldsToSelect';
    @testVisible private static final String SDA_FIELDS_TO_SELECT_PARAM = 'sdaFieldsToSelect';
    @testVisible private static final String NOT_DELETED_PARAM = 'notDeleted';
    @testVisible private static final String DOCUMENT_TYPES_PARAM = 'familyDocumentTypes';
    @testVisible private static final String DOCUMENT_STATUS_PARAM = 'documentStatusParams';

    private static final String SDA_SOBJECT_NAME = 'School_Document_Assignment__c';
    private static final String SDA_RELATION = 'School_Document_Assignments__r';

    /**
     * @description Default constructor for an instance of the FamilyDocumentsSelector that will not include field sets
     *              while enforcing FLS and CRUD.
     */
    public FamilyDocumentsSelector() {}

    /**
     * @description Queries Family Document records by Id.
     * @param idSet The Ids of the  records to query.
     * @return A list of Family Document records.
     * @throws ArgumentNullException if idSet is null.
     */
    public List<Family_Document__c> selectById(Set<Id> idSet)
    {
        ArgumentNullException.throwIfNull(idSet, ID_SET_PARAM);

        return (List<Family_Document__c>)super.selectSObjectsById(idSet);
    }

    /**
     * @description Queries Family Document records that are not
     *              deleted with associated School Document Assignment
     *              records by Id.
     * @param idSet The Ids of the records to query.
     * @return A list of Family Document records.
     * @throws An ArgumentNullException if idSet is null.
     */
    public List<Family_Document__c> selectNonDeletedByIdWithSchoolDocumentAssignments(Set<Id> idSet)
    {
        ArgumentNullException.throwIfNull(idSet, ID_SET_PARAM);

        return Database.query(String.format('SELECT {0}, (SELECT {1} FROM {2}) FROM {3} WHERE Id IN :idSet AND Deleted__c = false',
                new List<String> {
                        getFamilyDocumentListSObjectFieldListString(),
                        SchoolDocumentAssignmentSelector.Instance.getFieldListString(),
                        SDA_RELATION,
                        getSObjectName()
                }));
    }

    /**
     * @description Queries Family Document records that are not
     *              deleted with associated School Document Assignment
     *              records by Id and custom Field Lists.
     * @param idSet The Ids of the records to query.
     * @param familyDocFields Fields of the Family Document records to query.
     * @param sdaFields Fields of the School Document Assignment records to query.
     * @param notDeleted Set should query include deleted records or not.
     * @return A list of Family Document records.
     * @throws An ArgumentNullException if idSet is null.
     * @throws An ArgumentNullException if familyDocFields is null.
     * @throws An ArgumentNullException if sdaFields is null.
     * @throws An ArgumentNullException if notDeleted is null.
     */
    public List<Family_Document__c> selectByIdWithSchoolDocumentAssignmentsWithCustomFieldLists(
        Set<Id> idSet, List<String> familyDocFields, List<String> sdaFields, Boolean notDeleted)
    {
        
        ArgumentNullException.throwIfNull(idSet, ID_SET_PARAM);
        ArgumentNullException.throwIfNull(familyDocFields, FAMILY_DOCS_FIELDS_TO_SELECT_PARAM);
        ArgumentNullException.throwIfNull(sdaFields, SDA_FIELDS_TO_SELECT_PARAM);
        ArgumentNullException.throwIfNull(notDeleted, NOT_DELETED_PARAM);

        return Database.query(String.format('SELECT {0}, (SELECT {1} FROM {2}) FROM {3} WHERE Id IN :idSet {4}',
                new List<String> {
                        buildFieldListString(getSObjectName(), familyDocFields),
                        buildFieldListString(SDA_SOBJECT_NAME, sdaFields),
                        SDA_RELATION,
                        getSObjectName(),
                        (notDeleted) ? ' AND Deleted__c = false ' : ''
                }));
    }

    /**
     * @description Queries Family Document records that are not
     *              deleted with associated School Document Assignment
     *              records by Household Id.
     * @param idSet The Household Ids of the records to query.
     * @return A list of Family Document records.
     * @throws An ArgumentNullException if idSet is null.
     */
    public List<Family_Document__c> selectNonDeletedByHouseholdIdWithSchoolDocumentAssignments(Set<Id> idSet)
    {
        ArgumentNullException.throwIfNull(idSet, ID_SET_PARAM);

        return Database.query(String.format('SELECT {0}, (SELECT {1} FROM {2}) FROM {3} WHERE Household__c IN :idSet AND Deleted__c = false',
                new List<String> {
                        getFamilyDocumentListSObjectFieldListString(),
                        SchoolDocumentAssignmentSelector.Instance.getFieldListString(),
                        SDA_RELATION,
                        getSObjectName()
                }));
    }

    /**
     * @description Queries Family Document records that are not deleted and duplicates 
     *              by Household, Document Type and Document Status.
     * @param householdIds Household Ids related to the doc.
     * @param documentTypes Types of documents to query.
     * @param documentStatus Status of documents to query.
     * @return A list of Family Document records.
     * @throws An ArgumentNullException if householdIds is null.
     * @throws An ArgumentNullException if documentTypes is null.
     * @throws An ArgumentNullException if documentStatus is null.
     */
    public List<Family_Document__c> selectByHouseholdId_docType_docStatus(
        Set<Id> householdIds, Set<String> documentTypes, String documentStatus)
    {
        ArgumentNullException.throwIfNull(householdIds, HOUSE_HOLD_IDS_PARAM);
        ArgumentNullException.throwIfNull(documentTypes, DOCUMENT_TYPES_PARAM);
        ArgumentNullException.throwIfNull(documentStatus, DOCUMENT_STATUS_PARAM);

        String condition = 'Household__c IN :householdIds AND Document_Type__c IN :documentTypes '
            + ' AND Document_Status__c = :documentStatus AND Duplicate__c != \'Yes\' AND Deleted__c = false';

        return Database.query(String.format('SELECT {0} FROM {1} WHERE {2} LIMIT 50000', new List<String>
            {
                String.join(ExpCore.GetAllFieldNames('Family_Document__c'), ','),
                getSObjectName(),
                condition
            }));
    }

    /**
     * @description Queries Family Document records by household Id where the Document Type is W2.
     * @param householdIds The Id's of the Household records to get W2s for.
     * @return A list of W2s.
     * @throws ArgumentNullException if householdIds is null.
     */
    public List<Family_Document__c> selectW2sByHousehold(Set<String> householdIds)
    {
        ArgumentNullException.throwIfNull(householdIds, HOUSE_HOLD_IDS_PARAM);

        String condition = 'Document_Type__c = \'W2\' AND Household__c IN :householdIds';

        String formattedQuery = newQueryFactory().setCondition(condition).toSOQL();

        return Database.query(formattedQuery);
    }

    /**
     * @description Specifies CreatedDate in ascending order as the default ordering of Family Document records.
     * @return The default ordering of Family Document records.
     */
    public override String getOrderBy()
    {
        return 'CreatedDate ASC';
    }

    private Schema.SObjectType getSObjectType()
    {
        return Family_Document__c.SObjectType;
    }

    private String getFamilyDocumentListSObjectFieldListString()
    {
       List<Schema.SObjectField> fields = new List<Schema.SObjectField>
       {
                Family_Document__c.Document_Type__c,
                Family_Document__c.Document_Year__c,
                Family_Document__c.Document_Status__c,
                Family_Document__c.Date_Uploaded__c,
                Family_Document__c.Filename__c,
                Family_Document__c.Import_Id__c
        };

        String fieldListString = '';
        for (Schema.SObjectField field : fields) {
            String fieldName = getSObjectName() + '.' + field.getDescribe().getName();
            fieldListString += (String.isNotBlank(fieldListString)) ? ',' + fieldName : fieldName;
        }

        return fieldListString;
    }

    private List<Schema.SObjectField> getSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
                Family_Document__c.Name,
                Family_Document__c.Deleted__c,
                Family_Document__c.Document_Type__c,
                Family_Document__c.Duplicate__c,
                Family_Document__c.Duplicate_Family_Document__c,
                Family_Document__c.Household__c,
                Family_Document__c.W2_Deferred_Untaxed_1_A__c,
                Family_Document__c.W2_Deferred_Untaxed_2_B__c,
                Family_Document__c.W2_Deferred_Untaxed_3_C__c,
                Family_Document__c.W2_Deferred_Untaxed_4_D__c,
                Family_Document__c.W2_Deferred_Untaxed_5_E__c,
                Family_Document__c.W2_Deferred_Untaxed_6_F__c,
                Family_Document__c.W2_Deferred_Untaxed_7_G__c,
                Family_Document__c.W2_Deferred_Untaxed_8_H__c,
                Family_Document__c.W2_Employee_ID__c,
                Family_Document__c.W2_Employer_ID__c,
                Family_Document__c.W2_Medicare__c,
                Family_Document__c.W2_SS_Tax__c,
                Family_Document__c.W2_Wages__c,
                Family_Document__c.Document_Year__c
        };
    }

    private String buildFieldListString(String sObjectName, List<String> fields)
    {
        String fieldListString = '';
        for (String field : fields) {
            String fieldName = sObjectName + '.' + field;
            fieldListString += (String.isNotBlank(fieldListString)) ? ',' + fieldName : fieldName;
        }

        return fieldListString;
    }

    /**
     * @description Creates a new instance of the FamilyDocumentsSelector.
     * @return An instance of FamilyDocumentsSelector.
     */
    public static FamilyDocumentsSelector newInstance()
    {
        return new FamilyDocumentsSelector();
    }
}