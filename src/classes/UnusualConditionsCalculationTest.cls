@isTest
private class UnusualConditionsCalculationTest
{
    
    /***************************************************************************
    START UNIT TESTS
    ***************************************************************************/
    
    private static Account testA;
    private static Contact testC;
    private static Contact testStudent;
    private static User portalUser;
    private static PFS__c testPFS;
    private static Set<Id> testPFSIdSet;
    private static Unusual_Conditions__c testUC;
    private static Verification__c testVer;

    private static void setupTestData(){
        setupTestData(true);
    }

    private static void setupTestData(Boolean doInsert)
    {
        testA = TestUtils.createAccount('UNITTESTSETTINU', RecordTypes.individualAccountTypeId, 10, true);
        testC = TestUtils.createContact('Burdick', testA.Id, recordTypes.parentContactTypeId, true);
        testStudent = TestUtils.createContact('Applicant', testA.Id, recordTypes.studentContactTypeId, true);

        List<Academic_Year__c> testAYs = TestUtils.createAcademicYears();
        
        testVer = new Verification__c();
        insert testVer;
            
        testPFS = new PFS__c(Parent_A__c = testC.Id, Academic_Year_Picklist__c = testAYs[0].Name, Verification__c = testVer.ID);
        if (doInsert){
            insert testPFS;
        }
        
        testPFSIdSet = new Set<Id>();
        testPFSIdSet.add(testPFS.Id);
        
        testUC = TestUtils.createUnusualConditions(testAYs[0].Id, true);
    } 

    @isTest
    private static void testHighMedicalExpense(){
        setupTestData();
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].High_Medical_Expense__c);
        
        // this sets Total_Taxable_Income_Current__c and Total_Income_Current__c to 100
        testPFS.Total_Income_Current__c = 100;
        testPFS.Medical_Dental_Exp_Current__c = 6;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].High_Medical_Expense__c);

        // this sets Total_Taxable_Income_Current__c and Total_Income_Current__c to 100
        testPFS.Total_Income_Current__c = 100;
        testPFS.Medical_Dental_Exp_Current__c = 5;
        update testPFS;

        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].High_Medical_Expense__c);
    }

    @isTest
    private static void testHighUnusualExpense(){
        setupTestData();
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].High_Unusual_Expenses__c);
        
        // this sets Total_Taxable_Income_Current__c and Total_Income_Current__c to 100
        testPFS.Total_Income_Current__c = 100;
        testPFS.Total_Taxable_Income_Current__c = 100;
        testPFS.Unusual_Expenses_Current__c = 16;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].High_Unusual_Expenses__c);

        // this sets Total_Taxable_Income_Current__c and Total_Income_Current__c to 100
        testPFS.Total_Income_Current__c = 100;
        testPFS.Total_Taxable_Income_Current__c = 100;
        testPFS.Unusual_Expenses_Current__c = 14;
        update testPFS;

        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].High_Unusual_Expenses__c);
    }

    @isTest
    private static void calcHomeEquityCapped(){
        setupTestData();
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Home_Equity_Capped__c);
        
        // this sets Total_Income_Current__c to 100
        testPFS.Total_Income_Current__c = 100;
        testPFS.Home_Equity_Uncapped__c = 300;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Home_Equity_Capped__c);

        //UnusualConditionsCalculation.isFirstRun = true;
        // this sets Total_Income_Current__c to 100
        testPFS.Total_Income_Current__c = 0;
        testPFS.Home_Equity_Uncapped__c = 301;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].Home_Equity_Capped__c);

        // this sets Total_Income_Current__c to 100
        testPFS.Total_Income_Current__c = -1;
        testPFS.Home_Equity_Uncapped__c = 0;
        //UnusualConditionsCalculation.isFirstRun = true;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Home_Equity_Capped__c);
    }

    @isTest
    private static void testHighDebt(){
        setupTestData();
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].High_Debt__c);
        
        // this sets Total_Assets_Current__c to 100
        testPFS.Total_Assets_Current__c = 100;
        testPFS.Total_Debts__c = 101;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].High_Debt__c);

        // this sets testPFS.Total_Assets_Current__c to 100
        testPFS.Total_Assets_Current__c = 100;
        testPFS.Total_Debts__c = 99;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].High_Debt__c);
    }

    @isTest
    private static void testHighStudentAssets(){
        setupTestData();
        
        Applicant__c testApplicant1 = new Applicant__c(Contact__c = testStudent.ID, PFS__c = testPFS.Id, Last_Name__c = 'last');
        insert testApplicant1;

        Applicant__c testApplicant2 = new Applicant__c(Contact__c = testStudent.ID, PFS__c = testPFS.Id, Last_Name__c = 'last');
        insert testApplicant2;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].High_Student_Assets__c);
        
        //UnusualConditionsCalculation.isFirstRun = true;
        testApplicant2.Does_Applicant_Own_Assets__c = 'Yes';
        testApplicant2.Assets_Total_Value__c = 2001;
        update testApplicant2;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].High_Student_Assets__c);

        //UnusualConditionsCalculation.isFirstRun = true;
        testApplicant2.Assets_Total_Value__c = 1999;
        update testApplicant2;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].High_Student_Assets__c);
    }

    @isTest
    private static void testOtherTaxableIncomeNegative(){
        setupTestData();        
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Other_Taxable_Income_Negative__c);
        
        //UnusualConditionsCalculation.isFirstRun = true;
        //testPFS.Other_Taxable_Income_Current__c = -100;
        testPFS.Have_Other_Taxable_Income__c = 'Yes';
        testPFS.Other_Taxable_Income_Current__c = -100;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].Other_Taxable_Income_Negative__c);

        //UnusualConditionsCalculation.isFirstRun = true; 
        testPFS.Other_Taxable_Income_Current__c = 100;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Other_Taxable_Income_Negative__c);
    }

    @isTest
    private static void testBusinessNotIndicated(){
        setupTestData();        
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Business_Not_Indicated__c);
        
        //testPFS.Verification__r.X1040_Business_Income_Loss__c = 100;
        //update testPFS;
        testVer.X1040_Business_Income_Loss__c = 100;
        update testVer;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].Business_Not_Indicated__c);

        testPFS.Own_Business_or_Farm__c = 'Yes';
        testPFS.Number_of_Businesses__c = '1';
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Business_Not_Indicated__c);

        testPFS.Own_Business_or_Farm__c = 'No';
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].Business_Not_Indicated__c);

        testVer.X1040_Business_Income_Loss_B__c = 100;
        update testVer;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].Business_Not_Indicated__c);

        testVer.X1040_Business_Income_Loss__c = null;
        update testVer;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].Business_Not_Indicated__c);

        testVer.X1040_Business_Income_Loss_B__c = 0;
        update testVer;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Business_Not_Indicated__c);
    }

    @isTest
    private static void testMultipleTuitions(){
        setupTestData();        
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Multiple_Tuitions__c);
        
        testPFS.Num_Children_in_Tuition_Schools__c = 2;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].Multiple_Tuitions__c);

        testPFS.Num_Children_in_Tuition_Schools__c = 1;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Multiple_Tuitions__c);
    }

    @isTest
    private static void testIncomeZero(){
        setupTestData();        

        testPFS.Total_Taxable_Income_Current__c = 1;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Income_Zero__c);
        
        testPFS.Total_Taxable_Income_Current__c = 0;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].Income_Zero__c);

        testPFS.Total_Taxable_Income_Current__c = 1000000;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Income_Zero__c);
    }

    @isTest
    private static void testLargeIncomeChange(){
        setupTestData();        
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Large_Income_Change__c);
        
        testPFS.Salary_Wages_Parent_A__c = 100;
        testPFS.Salary_Wages_Parent_A_Est__c = 120;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].Large_Income_Change__c);

        testPFS.Salary_Wages_Parent_A__c = 100;
        testPFS.Salary_Wages_Parent_A_Est__c = 105;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Large_Income_Change__c);
        
        testPFS.Salary_Wages_Parent_B__c = 100;
        testPFS.Salary_Wages_Parent_B_Est__c = 116;
        update testPFS;

        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].Large_Income_Change__c);
    }

    @isTest
    private static void testHighIncomeAdjustment(){
        setupTestData();        
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].High_Income_Adjustments__c);
        
        testPFS.Total_Taxable_Income_Current__c = 100;
        testPFS.IRS_Adjustments_to_Income_Current__c = 30;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].High_Income_Adjustments__c);

        testPFS.Total_Taxable_Income_Current__c = 100;
        testPFS.IRS_Adjustments_to_Income_Current__c = 5;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].High_Income_Adjustments__c);
        
        testPFS.Total_Taxable_Income_Current__c = 100;
        testPFS.Salary_Wages_Parent_A_Est__c = 0;
        update testPFS;

        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].High_Income_Adjustments__c);
    }

    @isTest
    private static void testHighDividendInterest(){
        setupTestData();        
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].High_Dividends_Interest__c);
        
        testPFS.Bank_Account_Value__c = 99999;
        testPFS.Dividend_Income_Current__c = 2000;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].High_Dividends_Interest__c);

        testPFS.Bank_Account_Value__c = 0;
        testPFS.Dividend_Income_Current__c = 0;
        testPFS.Investments_Net_Value__c = 99999;
        testPFS.Interest_Income_Current__c = 2000;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].High_Dividends_Interest__c);

        testPFS.Investments_Net_Value__c = 0;
        testPFS.Interest_Income_Current__c = 0;
        testPFS.Bank_Account_Value__c = 100001;
        testPFS.Dividend_Income_Current__c = 2000;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].High_Dividends_Interest__c);
        
        testPFS.Total_Income_Current__c = 200;
        testPFS.Salary_Wages_Parent_A_Est__c = 150;
        update testPFS;

        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].High_Dividends_Interest__c);
    }

    @isTest
    private static void testLowDividendInterest(){
        setupTestData();        
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Low_Dividends_Interest__c);
        
        testPFS.Bank_Account_Value__c = 100000;
        testPFS.Dividend_Income_Current__c = 1999;
        update testPFS;

        testPFS.Bank_Account_Value__c = 0;
        testPFS.Dividend_Income_Current__c = 0;
        testPFS.Investments_Net_Value__c = 1000;
        testPFS.Interest_Income_Current__c = 1999;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].Low_Dividends_Interest__c);

        testPFS.Bank_Account_Value__c = 3000;
        testPFS.Dividend_Income_Current__c = 300;
        testPFS.Investments_Net_Value__c = 3000;
        testPFS.Interest_Income_Current__c = 300;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Low_Dividends_Interest__c);
        
        testPFS.Total_Income_Current__c = 4500;
        testPFS.Salary_Wages_Parent_A_Est__c = 1;
        update testPFS;

        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Low_Dividends_Interest__c);
    }

    @isTest
    private static void testNoIncomeTax(){
        setupTestData();        
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].No_Income_Tax__c);
        
        testPFS.Federal_Income_Tax_Reported__c = 0;
        testPFS.Total_Taxable_Income_Current__c = 20001;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].No_Income_Tax__c);

        testPFS.Federal_Income_Tax_Reported__c = 0;
        testPFS.Total_Taxable_Income_Current__c = 19999;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].No_Income_Tax__c);

        testPFS.Federal_Income_Tax_Reported__c = 100;
        testPFS.Total_Taxable_Income_Current__c = 20001;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].No_Income_Tax__c);
    }

    @isTest
    private static void testLowIncomeTax(){
        setupTestData();        
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Income_Tax_Low__c);
        
        testPFS.Federal_Income_Tax_Calculated__c = 100;
        testPFS.Federal_Income_Tax_Reported__c = 79;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].Income_Tax_Low__c);

        testPFS.Federal_Income_Tax_Calculated__c = 100;
        testPFS.Federal_Income_Tax_Reported__c = 81;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Income_Tax_Low__c);
    }

    @isTest
    private static void testHighIncomeTaxFUTURE(){
        setupTestData();        
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Income_Tax_High__c);
        
        testPFS.Federal_Income_Tax_Calculated__c = 100;
        testPFS.Federal_Income_Tax_Reported__c = 121;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].Income_Tax_High__c);

        testPFS.Federal_Income_Tax_Calculated__c = 100;
        testPFS.Federal_Income_Tax_Reported__c = 119;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Income_Tax_High__c);
    }

    @isTest
    private static void testHighIncomeTaxBULKandSAVE(){
        setupTestData();        
        
        Integer iterations = 5;
        
        List<PFS__c> testPFSList = new List<PFS__c>();
        
        for (Integer i = 0; i < iterations; i++){
            testPFSList.add(new PFS__c(Parent_A__c = testC.Id, Academic_Year_Picklist__c = testPFS.Academic_Year_Picklist__c));
        }
        
        upsert testPFSList;
        
        for (PFS__c p : testPFSList){
            testPFSIdSet.add(p.Id);
        }

        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, true);
        testPFSList = [Select Id, Income_Tax_High__c from PFS__c where Id in:testPFSList];
        System.assertEquals(iterations, testPFSList.size());
        
        for (PFS__c p : testPFSList){
            System.assertEquals('No', p.Income_Tax_High__c);
            p.Federal_Income_Tax_Calculated__c = 100;
            p.Federal_Income_Tax_Reported__c = 121;
        }
        
        update testPFSList;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, true);
        testPFSList = [Select Id, Income_Tax_High__c from PFS__c where Id in:testPFSList];
        System.assertEquals(iterations, testPFSList.size());
        
        for (PFS__c p : testPFSList){
            System.assertEquals('Yes', p.Income_Tax_High__c);
            p.Federal_Income_Tax_Calculated__c = 100;
            p.Federal_Income_Tax_Reported__c = 119;
        }

        update testPFSList;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, true);
        testPFSList = [Select Id, Income_Tax_High__c from PFS__c where Id in:testPFSList];
        System.assertEquals(iterations, testPFSList.size());
        
        for (PFS__c p : testPFSList){
            System.assertEquals('No', p.Income_Tax_High__c);
        }
    }

    @isTest
    private static void testHighIncomeTax(){
        setupTestData();        
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Income_Tax_High__c);
        
        testPFS.Federal_Income_Tax_Calculated__c = 100;
        testPFS.Federal_Income_Tax_Reported__c = 121;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('Yes', UnusualConditionsCalculation.PFSList[0].Income_Tax_High__c);

        testPFS.Federal_Income_Tax_Calculated__c = 100;
        testPFS.Federal_Income_Tax_Reported__c = 119;
        update testPFS;
        
        UnusualConditionsCalculation.calculateConditions(testPFSIdSet, false);
        System.assertEquals('No', UnusualConditionsCalculation.PFSList[0].Income_Tax_High__c);
    }

    @isTest
    private static void testHighIncomeTaxBULKandTRIGGER(){
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
        
        //TestData td = new TestData();
        setupTestData(false);        
        //insert testPFS;

        Integer iterations = 5;
        
        List<PFS__c> testPFSList = new List<PFS__c>();
        
        for (Integer i = 0; i < iterations; i++){
            testPFSList.add(new PFS__c(Parent_A__c = testC.Id, Academic_Year_Picklist__c = testPFS.Academic_Year_Picklist__c, PFS_Status__c = 'Application Submitted', SSS_EFC_Calc_Status__c = 'Current', Filing_Status__c = 'Single'));
        }
        
        System.debug('TESTING1120 about to insert' );
        UnusualConditionsCalculation.isFirstRun = true;
        // trigger fires, UC calculated
        insert testPFSList;
        
        testPFSList = [Select Id, Income_Tax_High__c, PFS_Status__c from PFS__c where Id in:testPFSList];
        System.assertEquals(iterations, testPFSList.size());
        
        for (PFS__c p : testPFSList){
            System.assertEquals('Application Submitted', p.PFS_Status__c);
            System.assertEquals('No', p.Income_Tax_High__c);
            p.Federal_Income_Tax_Calculated__c = 100;
            p.Federal_Income_Tax_Reported__c = 121;
        }
        
        System.debug('TESTING1135 about to insert' );
        UnusualConditionsCalculation.isFirstRun = true;
        // trigger fires, UC calculated
        update testPFSList;
        
        testPFSList = [Select Id, Income_Tax_High__c, Federal_Income_Tax_Calculated__c, Federal_Income_Tax_Reported__c, PFS_Status__c from PFS__c where Id in:testPFSList];
        System.assertEquals(iterations, testPFSList.size());
        
        for (PFS__c p : testPFSList){
            System.assertEquals(100, p.Federal_Income_Tax_Calculated__c);
            System.assertEquals(121, p.Federal_Income_Tax_Reported__c);
            System.assertEquals('Application Submitted', p.PFS_Status__c);
            System.assertEquals('Yes', p.Income_Tax_High__c);
            p.Federal_Income_Tax_Calculated__c = 100;
            p.Federal_Income_Tax_Reported__c = 119;
        }

        UnusualConditionsCalculation.isFirstRun = true;
        // trigger fires, UC calculated
        update testPFSList;
        
        testPFSList = [Select Id, Income_Tax_High__c, PFS_Status__c from PFS__c where Id in:testPFSList];
        System.assertEquals(iterations, testPFSList.size());
        
        for (PFS__c p : testPFSList){
            System.assertEquals('Application Submitted', p.PFS_Status__c);
            System.assertEquals('No', p.Income_Tax_High__c);
        }
    }
}