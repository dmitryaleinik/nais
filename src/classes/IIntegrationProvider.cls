/**
 * IIntegrationProvider.cls
 *
 * @description: 
 *
 * @author: Mike Havrila @ Presence PG
 */

public interface IIntegrationProvider {

    IIntegrationProvider init( String type);

    IIntegrationProvider preProcess();

    IIntegrationProvider process();

    IIntegrationProvider postProcess();

}