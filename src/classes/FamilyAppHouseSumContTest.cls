@isTest
private class FamilyAppHouseSumContTest
{

    @isTest
    private static void testDeleteApplicantAndRelatedSBFA()
    {
        TestDataFamily tdf = new TestDataFamily();

        Business_Farm__c bizFarm = BusinessFarmTestData.Instance
            .forPfsId(tdf.pfsA.Id).DefaultBusinessFarm;

        ApplicationUtils appUtils = new ApplicationUtils(UserInfo.getLanguage(), tdf.pfsA);

        Test.startTest();
            FamilyAppHouseSumCont compController = new FamilyAppHouseSumCont();
            compController.pfs = tdf.pfsA;
            compController.appUtils = appUtils;

            System.assertEquals(1, [
                SELECT count() 
                FROM School_Biz_Farm_Assignment__c 
                WHERE School_PFS_Assignment__r.Applicant__c = :tdf.applicant2A.id]);

            compController.delApplicantId = tdf.applicant2A.id;        
            compController.deleteApplicant();
            
            System.assertEquals(0, [
                SELECT count() 
                FROM School_Biz_Farm_Assignment__c 
                WHERE School_PFS_Assignment__r.Applicant__c = :tdf.applicant2A.id]);        

            compController.newApplicant();
        Test.stopTest();
    } 
}