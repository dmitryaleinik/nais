/**
 * BaseIntegrationService.cls
 *
 * @description:
 *
 * @author: Mike Havrila @ Presence PG
 */

public abstract class BaseIntegrationService {
    private final String LOOPKUP_TYPE = 'LOOKUP';
    private final String ID_TYPE = 'ID';
    private final String NUMBER_TYPE = 'NUMBER';
    private final String BOOLEAN_TYPE = 'BOOLEAN';
    private final String PICKLIST_TYPE = 'PICKLIST';
    private final String DATE_TYPE = 'DATE';

    // common exception class
    public class BaseIntegrationServiceException extends Exception {}
    public class IntegrationException extends Exception {}

    /**
     * @description: get Integration Mapping Vlaues and return a Map
     * 
     * @param source - The source name of the Auth Provider. 
     *
     * @param provierType - The Provder type to retreive the auth token (Open Id Connect);
     * 
     * @return An Auth Token from Auth Provider. 
     */
    public String getAuthToken( String source, String providerType) {

        String returnVal; 
        if( source == null && providerType == null) return null;

        try {

           Id authId = [select Id 
                          from AuthPRovider 
                         where DeveloperName = :source 
                         limit 1].Id;

           returnVal = Auth.AuthToken.getAccessToken( authId, providerType);
        } catch ( Exception e) {

            System.debug('DEBUG:::IntegrationService.getAuthToken Error: ' + e.getMessage());
        }
        
        return returnVal;
    }

    public List<Object> getObjectFromHttpRequest( String httpEndpoint, Map<String, String> httpHeaders, String httpBody, String httpMethod) {

        List<Object> returnVal;
        String httpResponse = HttpService.sendHttpRequest( httpEndpoint, httpHeaders, httpBody, HttpService.HTTP_GET);
        if( httpResponse != null) {

            returnVal = (List<Object>)JSON.deserializeUntyped( httpResponse);
        }

        return returnVal;
    }

    protected List<sObject> mapRecords( List<Object> dataToMap, List<IntegrationMappingModel> mappings, String targetObject, String sourceObject) {

        List<sObject> recordsToCreate = new List<sObject>();
        for( Object source : dataToMap) {
            
            // create a new instance of the targetObject   
            sObject newObject = Schema.getGlobalDescribe().get( targetObject).newSObject();

            Map<String, Object> sourceValues = ( Map<String, Object>)source;
            for( IntegrationMappingModel mapping : mappings) {

                Object tempValue;
                if( mapping.isSourceComponded) {

                    // This handles source fields that have more than once value;
                    for(String splitSource : mapping.sourceField.split(',')) {

                        if( sourceValues.get( splitSource) != null){
                            tempValue = sourceValues.get( splitSource);
                            break;
                        } else {
                            tempValue = sourceValues.get( mapping.sourceField);
                        }
                    }

                        // there may be source objects mapped to multiple sf objects, processed separately
                    if( mapping.targetObject == targetObject && tempValue != null) {
                        if( mapping.dataType == LOOPKUP_TYPE) {

                            newObject.put( mapping.targetField.split(':')[0], tempValue);
                        } else if( mapping.dataType == ID_TYPE) {

                            // if this is an id, treat it as an external id from a spefic external system : prepend this source
                            newObject.put( mapping.targetField, String.valueOf( tempValue));
                        } else if( mapping.dataType == NUMBER_TYPE) {
                            // implement me

                        } else if( mapping.dataType == BOOLEAN_TYPE) {
                            // implement me

                        } else if( mapping.dataType == PICKLIST_TYPE) {

                            //String mappedValue = mapPicklist(mapping.sourceField__c, string.valueOf(tempValue) );
                            newObject.put( mapping.targetField, String.valueOf( tempValue));
                        } else if( mapping.dataType == DATE_TYPE) {

                            String sourceDate = String.valueOf( tempValue);
                            Date d = Date.newInstance(Integer.valueOf( sourceDate.split('-')[0]), Integer.valueOf( sourceDate.split('-')[1]), Integer.valueOf( sourceDate.split('-')[2]));
                            newObject.put(mapping.targetField, d) ;
                        } else {

                            newObject.put(mapping.targetField, tempValue);
                        }
                    }
                }
            }
            recordsToCreate.add( newObject);
        }


        // do lookups if necessary
        for( IntegrationMappingModel mapping : mappings) {

          if( mapping.dataType == LOOPKUP_TYPE && mapping.targetObject == targetObject) {

                //recordsToCreate = this.lookupRecords( recordsToCreate, mapping);
            }  
        }

        return recordsToCreate;
    }

    /**
     * @description: populates fields on sObject by type. 
     * 
     * @param records - a list of records to loop over
     *
     * @param value - value to populate.
     *
     * @return List<sObject> that have the record populated.
     */
    public List<sObject> populatPfsIdOnObject( List<sObject> records, Id pfsId) {
        if( pfsId == null) return records;

        final String PFS_FIELD = 'PFS__c';
        for( sObject obj : records) {

            obj = this.populateFieldByType( obj, PFS_FIELD, pfsId);
        }

        return records;
    }

    /**
     * @description: populates a single field on an sObject
     * 
     * @param sourceField - soure field to update.
     *
     * @param value - value to populate.
     *
     * @return a single sObject that have the record populated.
     */
    public sObject populateFieldByType( sObject record, String sourceField, String value) {
        if( sourceField == null) return record;

        record.put( sourceField, value);

        return record;
    }



    /*public void checkForMatchingSchools() {
        
        String lookupTarget = this.getMappingFieldByType( this.mappingModelBySource.get('Student Applications'), 'Lookup', 'Student_Folder__c');
        if( lookupTarget != null) {
            lookupTarget = lookupTarget.split(':')[1].split(' ')[1];
        }

        List<Account> accounts = new AccountDataAccessService().getAccountBySchool( this.schoolIds, lookupTarget);
        Set<String> integrationIds = this.buildSetBySObject( accounts, 'IntegrationId__c');

        Boolean hasSchool = true;
        for( String studentId : this.studentIdToSobject.keySet()) {
            hasSchool = false;

            for( String schoolId : this.studentIdToSchools.get( studentId)) {
                if( integrationIds.contains( schoolId)) {
                    hasSchool = true;
                    break;
                }
            }

            // no school associated with this student so remove it.
            if( !hasSchool) {
                this.studentIdToSchools.remove( studentId);
            }
        }
    }*/

    /*private Set<String> buildSetBySObject( List<sObject> accounts, String targetField) {
        Set<String> returnVal;
        for( sObject obj : accounts) {
            // Make sure object has current annual settings,

            //GlobalVariableiables.getCurrentAnnualSettings(true, null, GlobalVariables.getAcademicYearByName(ipfs.pfs.Academic_Year_Picklist__c).id, (id)each.get('id')); 
            returnVal.add( (String)obj.get( targetField));

            // TODO: find where to put this.
            this.schoolIdToName.put( (String)obj.get( targetField), (String)obj.get('name'));
            this.schoolIds.add( (String)obj.get('id'));
        }

        return returnVal;
    }*/
}