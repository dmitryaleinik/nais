/**
 * @description This class is used to create Application Fee Waiver records for unit tests.
 */
@isTest
public class ApplicationFeeWaiverTestData extends SObjectTestData {
    /**
     * @description Get the default values for the Application_Fee_Waiver__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Application_Fee_Waiver__c.Contact__c => ContactTestData.Instance.DefaultContact.Id,
                Application_Fee_Waiver__c.Academic_Year__c => AcademicYearTestData.Instance.DefaultAcademicYear.Id,
                Application_Fee_Waiver__c.Account__c => AccountTestData.Instance.DefaultAccount.Id
        };
    }

    /**
     * @description Set the Contact__c field on the current Application Fee Waiver record.
     * @param contactId The Contact Id to set on the Application Fee Waiver.
     * @return The current working instance of ApplicationFeeWaiverTestData.
     */
    public ApplicationFeeWaiverTestData forContactId(Id contactId) {
        return (ApplicationFeeWaiverTestData) with(Application_Fee_Waiver__c.Contact__c, contactId);
    }

    /**
     * @description Set the Academic_Year__c field on the current Application Fee Waiver record.
     * @param academicYearId The Academic Year Id to set on the Application Fee Waiver.
     * @return The current working instance of ApplicationFeeWaiverTestData.
     */
    public ApplicationFeeWaiverTestData forAcademicYearId(Id academicYearId) {
        return (ApplicationFeeWaiverTestData) with(Application_Fee_Waiver__c.Academic_Year__c, academicYearId);
    }

    /**
     * @description Set the Account__c field on the current Application Fee Waiver record.
     * @param accountId The Account Id to set on the Application Fee Waiver.
     * @return The current working instance of ApplicationFeeWaiverTestData.
     */
    public ApplicationFeeWaiverTestData forAccountId(Id accountId) {
        return (ApplicationFeeWaiverTestData) with(Application_Fee_Waiver__c.Account__c, accountId);
    }

    public ApplicationFeeWaiverTestData asPending() {
        return (ApplicationFeeWaiverTestData) with(Application_Fee_Waiver__c.Status__c, 'Pending Qualification');
    }

    /**
     * @description Insert the current working Application_Fee_Waiver__c record.
     * @return The currently operated upon Application_Fee_Waiver__c record.
     */
    public Application_Fee_Waiver__c insertApplicationFeeWaiver() {
        return (Application_Fee_Waiver__c)insertRecord();
    }

    /**
     * @description Create the current working Application Fee Waiver record without resetting
     *             the stored values in this instance of ApplicationFeeWaiverTestData.
     * @return A non-inserted Application_Fee_Waiver__c record using the currently stored field
     *             values.
     */
    public Application_Fee_Waiver__c createApplicationFeeWaiverWithoutReset() {
        return (Application_Fee_Waiver__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Application_Fee_Waiver__c record.
     * @return The currently operated upon Application_Fee_Waiver__c record.
     */
    public Application_Fee_Waiver__c create() {
        return (Application_Fee_Waiver__c)super.buildWithReset();
    }

    /**
     * @description The default Application_Fee_Waiver__c record.
     */
    public Application_Fee_Waiver__c DefaultApplicationFeeWaiver {
        get {
            if (DefaultApplicationFeeWaiver == null) {
                DefaultApplicationFeeWaiver = createApplicationFeeWaiverWithoutReset();
                insert DefaultApplicationFeeWaiver;
            }
            return DefaultApplicationFeeWaiver;
        }
        private set;
    }

    /**
     * @description Get the Application_Fee_Waiver__c SObjectType.
     * @return The Application_Fee_Waiver__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Application_Fee_Waiver__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static ApplicationFeeWaiverTestData Instance {
        get {
            if (Instance == null) {
                Instance = new ApplicationFeeWaiverTestData();
            }
            return Instance;
        }
        private set;
    }

    private ApplicationFeeWaiverTestData() { }
}