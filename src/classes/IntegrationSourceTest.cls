/**
 * IntegrationSourceTest.cls
 *
 * @description: Test class for IntegrationSource using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 */

@isTest (seealldata=false)
public without sharing class IntegrationSourceTest {

    private static PFS__c pfs;
    static {
        AcademicYearTestData.Instance.forName('2016-2017').insertAcademicYear();
        AcademicYearTestData.Instance.forName('2017-2018').insertAcademicYear();

        // Create School & Parent Accts
        List<Account> acctList = new List<Account>();
        acctList.add( new Account( Name = MassEmailSendTestDataFactory.TEST_SCHOOL_NAME + 'sao', NCES_ID__c = '5902', SSS_School_Code__c = '5902', SSS_Subscriber_Status__c = 'Current', RecordTypeId = RecordTypes.schoolAccountTypeId));
        acctList.add( new Account( Name = MassEmailSendTestDataFactory.TEST_SCHOOL_NAME + 'ravenna', NCES_ID__c = '01324523', SSS_School_Code__c = '01324523', SSS_Subscriber_Status__c = 'Current', RecordTypeId = RecordTypes.schoolAccountTypeId));
        acctList.add( new Account( Name = 'Test Student Sao', RecordTypeId = RecordTypes.individualAccountTypeId));
        acctList.add( new Account( Name = 'Test Student Ravenna', RecordTypeId = RecordTypes.individualAccountTypeId));
        insert acctList;

        List<Contact> contactList = new List<Contact>();
        contactList.add( new Contact( FirstName = 'Testy', LastName = 'SchoolContact2', Email = MassEmailSendTestDataFactory.TEST_SCHOOL2_CONTACT_EMAIL, Account = acctList[0],
                RecordTypeId = RecordTypes.schoolStaffContactTypeId));
        contactList.add( new Contact( FirstName = 'Testy', LastName = 'Parentcontact', Email = MassEmailSendTestDataFactory.TEST_PARENT1_CONTACT_EMAIL, Account = acctList[1],
                RecordTypeId = RecordTypes.parentContactTypeId, Birthdate = Date.newInstance(2015, 01, 01)) );

        insert contactList;

        pfs = TestUtils.createPFS( 'Test', '2016-2017', contactList[1].Id, false);
        insert pfs;
    }

    /* Negative Test Cases */
    @isTest
    static void integrationSourcePage_newSAOIntegration_Error() {

        User u = [ select Id, ProcessingIntegration__c, IntegrationSource__c from User where Id = :UserInfo.getUserId() limit 1];
        u.ProcessingIntegration__c = 'sao';
        u.IntegrationSource__c = 'sao';
        update u;

        pfs.IntegrationErrors__c = 'ravenna';
        pfs.ProcessingIntegration__c = 'sao';
        update pfs;

        // Arrange
        final String INTEGRATION_NAME = 'sao';
        Boolean hasIntegrationException = false;
        IntegrationSource__c setting = new IntegrationSource__c();
        setting.BaseCalloutURL__c = '';
        setting.Name = INTEGRATION_NAME;
        setting.HomePageMessage__c = 'Pied Piper';
        insert setting;

        // Validate there is data in Integration_Process__mdt
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('SAOProfilesMock');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');


        Test.setMock( HttpCalloutMock.class, mock);
        FeatureToggles.setToggle('UseLegacyRavennaIntegration__c', false);
        PageReference pageRef = Page.FamilyDashboard;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('selectedIntegrationSource',INTEGRATION_NAME);
        pageRef.getParameters().put('integrationSource',INTEGRATION_NAME);
        pageRef.getParameters().put('source',INTEGRATION_NAME);

        IntegrationSource is = new IntegrationSource();
        is.pfs = pfs;
        is.integrationSourceFromCookie = INTEGRATION_NAME;
        is.selectedIntegrationSource = INTEGRATION_NAME;
        is.getUserRecord();
        is.getauthSourceToStatus();
        is.getIntegrationSources();
        is.getHomePageMessage();
        is.getDashboardURL();
        is.getIntegrationSource();
        is.getauthSourceToType();
        is.applyIntegrationSource();
        // Act
        Test.startTest();

            is.kickoffIntegration();
        Test.stopTest();

        // Assert
        // At this point were just making sure no configuration/callout exceptions happened.
        System.assert( ApexPages.hasMessages( ApexPages.Severity.ERROR));
    }

    @isTest
    static void integrationSourcePage_newRavennaIntegration_Error() {
        // insert Applicant Database
        // Arrange
        final String INTEGRATION_NAME = 'ravenna';
        final STring BASE_URL = 'http://api.piedpiper.com';
        final String STUDENTS_API_ENDPOINT =  BASE_URL + '/api/v1/students/';
        final String SCHOOLS_API_ENDPOINT = BASE_URL + '/api/v1/schools/59371';
        final String STUDENT_APPLICATION_API_ENDPOINT = BASE_URL + '/api/v1/students/90919/applications?admission_year=2015-2016';
        final String STUDENT_APPLICATION_API_ENDPOINT2 = BASE_URL + '/api/v1/students/91023/applications?admission_year=2015-2016';

        User u = [ select Id, ProcessingIntegration__c from User where Id = :UserInfo.getUserId() limit 1];
        u.ProcessingIntegration__c = 'ravenna';
        u.IntegrationSource__c = 'ravenna';
        update u;

        pfs.ProcessingIntegration__c = 'sao';
        update pfs;

        Boolean hasIntegrationException = false;
        IntegrationSource__c setting = new IntegrationSource__c();
        setting.BaseCalloutURL__c = ''; // sets this to false, just to validate the error handling is working.
        setting.Name = INTEGRATION_NAME;
        setting.HomePageMessage__c = 'Pied Piper';
        setting.DisplayName__c = 'Ravenna';
        insert setting;

        // Act
        Test.startTest();

            PageReference pageRef = Page.FamilyDashboard;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('selectedIntegrationSource',INTEGRATION_NAME);
            pageRef.getParameters().put('integrationSource',INTEGRATION_NAME);
            pageRef.getParameters().put('source',INTEGRATION_NAME);

            FeatureToggles.setToggle('UseLegacyRavennaIntegration__c', false);
            IntegrationSource is = new IntegrationSource();
            is.pfs = pfs;
            is.integrationSourceFromCookie = INTEGRATION_NAME;
            is.selectedIntegrationSource = INTEGRATION_NAME;

            is.token( INTEGRATION_NAME);
            is.getUserRecord();
            is.getauthSourceToStatus();
            is.getIntegrationSources();
            is.getHomePageMessage();
            is.getDashboardURL();
            is.getIntegrationSource();
            is.getauthSourceToType();
            is.applyIntegrationSource();
            is.kickoffIntegration();
        Test.stopTest();

        // Assert
        System.assert( ApexPages.hasMessages( ApexPages.Severity.ERROR));
    }

    /* Positive Test Cases */
    @isTest
    static void integrationSourcePage_newRavennaIntegration_Success() {
        // insert Applicant Database
        // Arrange
        final String INTEGRATION_NAME = 'ravenna';
        final STring BASE_URL = 'http://api.piedpiper.com';
        final String STUDENTS_API_ENDPOINT =  BASE_URL + '/api/v1/students/';
        final String SCHOOLS_API_ENDPOINT = BASE_URL + '/api/v1/schools/59371';
        final String STUDENT_APPLICATION_API_ENDPOINT = BASE_URL + '/api/v1/students/90919/applications?admission_year=2015-2016';
        final String STUDENT_APPLICATION_API_ENDPOINT2 = BASE_URL + '/api/v1/students/91023/applications?admission_year=2015-2016';

        Boolean hasIntegrationException = false;
        IntegrationSource__c setting = new IntegrationSource__c();
        setting.BaseCalloutURL__c = BASE_URL;
        setting.Name = INTEGRATION_NAME;
        setting.HomePageMessage__c = 'Pied Piper';

        insert setting;

        pfs.ProcessingIntegration__c = INTEGRATION_NAME;
        update pfs;

        MultiStaticResourceCalloutMock multiMock = new MultiStaticResourceCalloutMock();
        multiMock.setStaticResource( STUDENTS_API_ENDPOINT,  'RavennaStudentsMockResponse');
        multiMock.setStaticResource( STUDENT_APPLICATION_API_ENDPOINT,  'RavennaApplicationMockResponse');
        multiMock.setStaticResource( STUDENT_APPLICATION_API_ENDPOINT2,  'RavennaApplicationStudent2MockResponse');
        multiMock.setStaticResource( SCHOOLS_API_ENDPOINT,  'RavennaSchoolMockResponse');
        multiMock.setStatusCode(200);
        multiMock.setHeader('Content-Type', 'application/json');

        FeatureToggles.setToggle('UseLegacyRavennaIntegration__c', false);
        PageReference pageRef = Page.FamilyDashboard;
        Test.setMock( HttpCalloutMock.class, multiMock);
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('selectedIntegrationSource',INTEGRATION_NAME);
        pageRef.getParameters().put('integrationSource', INTEGRATION_NAME);
        pageRef.getParameters().put('source',INTEGRATION_NAME);

        IntegrationSource is = new IntegrationSource();
        is.pfs = pfs;
        is.integrationSourceFromCookie = INTEGRATION_NAME;
        is.selectedIntegrationSource = INTEGRATION_NAME;
        is.token('ravenna');
        is.getUserRecord();
        is.getauthSourceToStatus();
        is.getIntegrationSources();
        is.getHomePageMessage();
        is.getDashboardURL();
        is.getIntegrationSource();
        is.getauthSourceToType();
        is.applyIntegrationSource();

        // Act
        Test.startTest();

            is.kickoffIntegration();
        Test.stopTest();

        // Assert
        // At this point were just making sure no configuration/callout exceptions happened.
        System.assert( ApexPages.hasMessages( ApexPages.Severity.CONFIRM));
    }

    @isTest
    static void integrationSourcePage_newSAOIntegration_Success() {

        User u = [ select Id, ProcessingIntegration__c, IntegrationSource__c from User where Id = :UserInfo.getUserId() limit 1];

        // Arrange
        final String INTEGRATION_NAME = 'sao';
        Boolean hasIntegrationException = false;

        pfs.Academic_Year_Picklist__c = '2017-2018';
        pfs.IntegrationErrors__c = INTEGRATION_NAME;
        pfs.ProcessingIntegration__c = INTEGRATION_NAME;
        update pfs;

        IntegrationSource__c setting = new IntegrationSource__c();
        setting.BaseCalloutURL__c = 'http://api.piedpiper.com/v1/';
        setting.Name = INTEGRATION_NAME;
        setting.HomePageMessage__c = 'Pied Piper';
        insert setting;

        // Validate there is data in Integration_Process__mdt
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('SAOProfilesMock');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');

        PageReference pageRef = Page.FamilyDashboard;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('selectedIntegrationSource','sao');
        pageRef.getParameters().put('integrationSource','sao');
        pageRef.getParameters().put('source','sao');

        Test.setMock( HttpCalloutMock.class, mock);
        FeatureToggles.setToggle('UseLegacyRavennaIntegration__c', false);

        IntegrationSource is = new IntegrationSource();
        is.pfs = pfs;
        is.selectedIntegrationSource = INTEGRATION_NAME;
        is.integrationSourceFromCookie = INTEGRATION_NAME;
        is.getUserRecord();
        is.getauthSourceToStatus();
        is.getIntegrationSources();
        is.getHomePageMessage();
        is.getDashboardURL();
        is.getIntegrationSource();
        is.getauthSourceToType();
        is.applyIntegrationSource();
        is.kickoffRegistration();
        Boolean isAuthEmpty = is.getAuthSourceIsEmpty();
        Boolean isIntegrationInitialized = is.getIsIntegrationInitialized();
        String currentProcessingIntegration = is.getCurrentProcessingIntegration();
        // Act
        Test.startTest();

            is.kickoffIntegration();
        Test.stopTest();

        List<ApexPages.Message> messages = ApexPages.getMessages();
        for (ApexPages.Message msg : messages) {
            System.debug('&&& msg.severity: ' + msg.getSeverity().name());
            System.debug('&&& msg: ' + msg.getDetail());
        }

        // Assert
        // At this point were just making sure no configuration/callout exceptions happened.
        System.assert( ApexPages.hasMessages( ApexPages.Severity.CONFIRM));
    }
}