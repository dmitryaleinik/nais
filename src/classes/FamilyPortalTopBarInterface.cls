public interface FamilyPortalTopBarInterface {

    String getAcademicYearId();

    PageReference YearSelector_OnChange(Id newAcademicYearId);
}