public without sharing class FamilyReportController
{

    // in case we need to mass print in the future
    //public class FamilyReportWrapper{
    //    public transient PFS__c thePFS {get; set;}
    //    public transient Applicant__c[] theApplicants {get; set;}
    //    public transient Decimal boardingFoodAllowance {get; set;}
    //}

    //public FamilyReportWrapper singleWrapper {get; set;}
    public Boolean hasErrors {get; set;}
    public transient PFS__c thePFS {get; set;}
    public transient Applicant__c[] theApplicants {get; set;}
    public transient Decimal boardingFoodAllowance {get; set;}

    public FamilyReportController() {
        hasErrors = false;

        String thePFSId = System.currentPagereference().getParameters().get('Id');
        if (thePFSId == null || thePFSId == '') {
            hasErrors = true;
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'PFS Id is required as a URL Parameter'));
        } else {

            try {
                Map<String, Decimal> acadYearNameToSSSConstants = new Map<String, Decimal>();

                for (SSS_Constants__c sss : [Select Id, Academic_Year__r.Name, Academic_Year__c, Boarding_School_Food_Allowance__c FROM SSS_Constants__c]) {
                    acadYearNameToSSSConstants.put(sss.Academic_Year__r.Name, sss.Boarding_School_Food_Allowance__c);
                }

                // Prevent SOQL injection
                thePFSId = String.escapeSingleQuotes(thePFSId);

                List<String> fieldNames = new List<String>();
                Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.PFS__c.fields.getMap();
                fieldNames.addAll(fieldMap.keySet());

                String queryString = 'SELECT ' + String.Join(fieldNames, ',');
                queryString += ', (SELECT Business_Farm_Assets__c, Business_Farm_Debts__c, Business_Net_Profit_Share_Current__c, Academic_Year__c, Business_Entity_Type__c '
                               + ' FROM Business_Farms__r)'
                               + ', (SELECT Id, Contact__r.Name, Asset_Contribution__c FROM Applicants__r)'
                               + ' FROM PFS__c where Id = \'' + thePFSId + '\'';

                User u = GlobalVariables.getCurrentUser();
                if (GlobalVariables.isFamilyPortalProfile(u.ProfileId)) {
                    queryString += ' AND Parent_A__c = \'' + u.ContactId + '\'';
                }

                queryString += ' LIMIT 1';

                Boolean isSchoolPortalUser = GlobalVariables.isSchoolPortalProfile(u.ProfileId);

                // if this is not a schoolportal user, we'll do the query right here
                if (!isSchoolPortalUser) {
                    //for (PFS__c pfs : [Select Id, Name from PFS__c where Id = 'a0QJ0000001DYRq' limit 1]){
                    for (PFS__c pfs : Database.query(queryString)) {
                        thePFS = pfs;
                    }
                    // otherwise we'll query with sharing (this is a school portal user)
                } else {
                    WithSharingQueryClass qClass = new WithSharingQueryClass();
                    thePFS = qClass.queryPFS(queryString);
                }

                if (thePFS != null) {
                    theApplicants = thePFS.Applicants__r;
                    boardingFoodAllowance = acadYearNameToSSSConstants.get(thePFS.Academic_Year_Picklist__c);
                } else {
                    hasErrors = true;
                    apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Id or insufficient privileges.'));
                }
            } catch (exception e) {
                hasErrors = true;
                apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error getting PFS: ' + e));
            }
        }
    }

    // NAIS-2543 START
    public Decimal getBusinessFarmAssets() {
        return EfcDataManager.getSumFromBizFarms(thePFS.Business_Farms__r, 'Business_Farm_Assets__c');
    }

    public Decimal getBusinessFarmDebts() {
        return EfcDataManager.getSumFromBizFarms(thePFS.Business_Farms__r, 'Business_Farm_Debts__c');
    }

    public Decimal getBusinessFarmNetProfitLoss() {
        return EfcDataManager.getSumFromBizFarms(thePFS.Business_Farms__r, 'Business_Net_Profit_Share_Current__c');
    }
    // NAIS-2543 END

    // NAIS-1671: Display 0 if the contribution value is negative
    public Decimal getDisplayContribution() {
        Decimal valueToDisplay = null;
        if (thePFS.Est_Parental_Contribution__c != null) {
            valueToDisplay = (thePFS.Est_Parental_Contribution__c < 0) ? 0 : thePFS.Est_Parental_Contribution__c;
        }
        return valueToDisplay;
    }

    public List<String> getSchoolCalculationList() {
        return Label.School_Calculation_List.split('<br/>');
    }

    public List<String> getFinancialAidDecisionList() {
        return Label.Financial_Aid_Decision_List.split('<br/>');
    }

    // class that lets us query with sharing
    public with sharing class WithSharingQueryClass {
        public PFS__c queryPFS(String queryString) {
            PFS__c thePFS;
            for (PFS__c pfs : Database.query(queryString)) {
                thePFS = pfs;
            }
            return thePFS;
        }
    }
}