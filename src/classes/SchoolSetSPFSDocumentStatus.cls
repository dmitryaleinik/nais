/*
* SPEC-099
* NAIS-282 PFS Doc Statuses.xlsx
*
* Once all of the required documents have been provided and verified, the (document) stage of the folder should change
*
* Nathan, Exponent Partners, 2013
*
* [SL] NAIS-282 refactored code based on School Document Assignment lists
*/
public class SchoolSetSPFSDocumentStatus {

    public static String requiredDocsCompleteStatus = 'Required Documents Complete';

    // NAIS-2114 [DP] 12.18.2014 new method to calculate from SPA-Before Trigger
    public static void updateDocumentStatusFromSPABefore(List<School_PFS_Assignment__c> allSPAs){
        Map<Id, List<School_Document_Assignment__c>> spaIdToSDAList = new Map<Id, List<School_Document_Assignment__c>>();
        for (School_Document_Assignment__c sda : getChildSDAs(allSPAs)){
            if (spaIdToSDAList.get(sda.School_PFS_Assignment__c) == null){
                spaIdToSDAList.put(sda.School_PFS_Assignment__c, new List<School_Document_Assignment__c>());
            }
            spaIdToSDAList.get(sda.School_PFS_Assignment__c).add(sda);
        }


        updateDocumentStatus(allSPAs, spaIdToSDAList, null, false);
    }

    /*** [SL] NAIS-282 - BEGIN refactored code  ***/
    public static void updateDocumentStatusFromFamilyDocuments(Set<Id> familyDocumentIds, boolean isDelete, Set<Id> deletedFamilyDocumentIds) {
        
        Set<Id> schoolPFSAssignmentsToUpdate = new Set<Id>();
        List<School_Document_Assignment__c> sdasToUpdate = new List<School_Document_Assignment__c>();
        List<School_Document_Assignment__c> sdasToDelete = new List<School_Document_Assignment__c>();
        
        for (School_Document_Assignment__c sda : [SELECT Id, School_PFS_Assignment__c, Document__c, CreatedBy.ProfileId, Required_Document__c 
                                                  FROM School_Document_Assignment__c 
                                                  WHERE Document__c IN :familyDocumentIds]) {
            
            if( deletedFamilyDocumentIds != null && deletedFamilyDocumentIds.contains(sda.Document__c)) {
                
                //SFP-1118: This enhancement is to clear this family document out of any SDAs. If the SDA is for a required document, 
                //then keep the SDA. If the SDA is not for a required document, and is created by API User/not a school user, remove the SDA.
                if(sda.Required_Document__c == null && !GlobalVariables.isFamilyPortalProfile(sda.CreatedBy.ProfileId)){
                    sdasToDelete.add(sda);
                } else {
                   sda.Document__c = null;
                   sdasToUpdate.add(sda); 
                }
            }
            
            if (sda.School_PFS_Assignment__c != null) {
                schoolPFSAssignmentsToUpdate.add(sda.School_PFS_Assignment__c);
            }
        }
        
        if (!schoolPFSAssignmentsToUpdate.isEmpty()) {
            if (isDelete == true) {
                updateDocumentStatus(schoolPFSAssignmentsToUpdate, familyDocumentIds);
            }
            else {
                updateDocumentStatus(schoolPFSAssignmentsToUpdate, null);
            }
        }
        
        if(!sdasToDelete.isEmpty()) {
            delete sdasToDelete;
        }
        
        if(!sdasToUpdate.isEmpty()) {
            update sdasToUpdate;
        }
    }//End:updateDocumentStatusFromFamilyDocuments
    
    public static void updateDocumentStatus(Set<Id> schoolPFSAssignmentIds) {
        updateDocumentStatus(schoolPFSAssignmentIds, null);
    }
    
    // NAIS-2114 [DP] 12.18.2014 refactor to use map instead of child query
    public static void updateDocumentStatus(Set<Id> schoolPFSAssignmentIds, Set<Id> deletedFamilyDocIds) {
        List<School_PFS_Assignment__c> allSPAs = [SELECT Id, Academic_Year_Picklist__c, PFS_Document_Status__c, PFS_Current_Year_Tax_Document_Status__c, PFS_Prior_Year_Tax_Document_Status__c, PFS_Prior_Prior_Year_Tax_Document_Status__c
                                                    FROM School_PFS_Assignment__c 
                                                    WHERE Id IN: schoolPFSAssignmentIds
                                                    AND PFS_Document_Status_Override__c = false ];

        Map<Id, List<School_Document_Assignment__c>> spaIdToSDAList = new Map<Id, List<School_Document_Assignment__c>>();
        for (School_Document_Assignment__c sda : getChildSDAs(allSPAs)){
            if (spaIdToSDAList.get(sda.School_PFS_Assignment__c) == null){
                spaIdToSDAList.put(sda.School_PFS_Assignment__c, new List<School_Document_Assignment__c>());
            }
            spaIdToSDAList.get(sda.School_PFS_Assignment__c).add(sda);
        }

        updateDocumentStatus(allSPAs, spaIdToSDAList, deletedFamilyDocIds, true);
    }

    public static void updateDocumentStatus(List<School_PFS_Assignment__c> allSPAs, Map<Id, List<School_Document_Assignment__c>> spaIdToSDAList, Set<Id> deletedFamilyDocIds, Boolean doUpdate) {

        List<School_PFS_Assignment__c> spaRecordsToUpdate = new List<School_PFS_Assignment__c>();
        AllDocumentStatus documentStatus = null;
        // NAIS-2114 [DP] 12.18.2014 don't need to bother getting ones that have the status overridden, as they will be set by the SchoolPFSASsignmentBefore trigger
        for (School_PFS_Assignment__c spa : allSPAs) {
            
            // calculate the status
            //List<School_Document_Assignment__c> sdaList = new List<School_Document_Assignment__c>();
            //for (School_Document_Assignment__c sda : spa.School_Document_Assignments__r) {
            //    sdaList.add(sda);
            //}

            List<School_Document_Assignment__c> sdaList = spaIdToSDAList.get(spa.Id);

            // [CH] NAIS-2076 Adding some null-safing so that unit tests don't fail
            if(sdaList != null){
                documentStatus = calculateDocumentStatus(spa, sdaList, deletedFamilyDocIds);
                
                boolean addedToUpdate = false;
    
                if (documentStatus != null) {
                   // update the SPA record if necessary
                    if (documentStatus.DocumentStatus != null && spa.PFS_Document_Status__c != documentStatus.DocumentStatus) {
                        spa.PFS_Document_Status__c = documentStatus.DocumentStatus;
                        spaRecordsToUpdate.add(spa);
                        addedToUpdate = true;
                    }

                    if (documentStatus.CurrentYearTaxStatus != null && spa.PFS_Current_Year_Tax_Document_Status__c != documentStatus.CurrentYearTaxStatus) {
                        spa.PFS_Current_Year_Tax_Document_Status__c = documentStatus.CurrentYearTaxStatus;
                        if (!addedToUpdate) {
                            spaRecordsToUpdate.add(spa);
                            addedToUpdate = true;
                        }
                    }

                    if (documentStatus.PriorYearTaxStatus != null && spa.PFS_Prior_Year_Tax_Document_Status__c != documentStatus.PriorYearTaxStatus) {
                        spa.PFS_Prior_Year_Tax_Document_Status__c = documentStatus.PriorYearTaxStatus;
                        if (!addedToUpdate) {
                            spaRecordsToUpdate.add(spa);
                        }
                    }
                    
                    if (documentStatus.PriorPriorYearTaxStatus != null && spa.PFS_Prior_Prior_Year_Tax_Document_Status__c != documentStatus.PriorPriorYearTaxStatus) {
                        spa.PFS_Prior_Prior_Year_Tax_Document_Status__c = documentStatus.PriorPriorYearTaxStatus;
                        if (!addedToUpdate) {
                            spaRecordsToUpdate.add(spa);
                        }
                    }
                }
            }
            else{ // If the SPA no longer has any School Document Assignments attached
                spa.PFS_Document_Status__c = 
                    spa.PFS_Current_Year_Tax_Document_Status__c = 
                    spa.PFS_Prior_Year_Tax_Document_Status__c = 'No Documents Received';
                    
                spaRecordsToUpdate.add(spa);                
            }
        }
        
        // save any updated records
        if (!spaRecordsToUpdate.isEmpty() && doUpdate) {
            update spaRecordsToUpdate;
        }
    }
    
    public class AllDocumentStatus {
        public String DocumentStatus;
        public String CurrentYearTaxStatus;
        public String PriorYearTaxStatus;
        public String PriorPriorYearTaxStatus;
    }
    
    public static AllDocumentStatus calculateDocumentStatus(School_PFS_Assignment__c spa, List<School_Document_Assignment__c> sdaList, Set<Id> deletedFamilyDocIds) {
        AllDocumentStatus status = new AllDocumentStatus();
        status.DocumentStatus = status.CurrentYearTaxStatus = status.PriorYearTaxStatus = status.PriorPriorYearTaxStatus = 'No Documents Received';  
        
        // keep track of whether there are documents with specific statuses
        boolean existsNotReceivedDocs = false;
        boolean existsReceivedVerificationDocs = false;
        boolean existsReceivedNonVerificationDocs = false;
        boolean existsProcessedDocs = false;
        
        // current year tax documents
        boolean cytExistsNotReceivedDocs = false;
        boolean cytExistsReceivedVerificationDocs = false;
        boolean cytExistsReceivedNonVerificationDocs = false;
        boolean cytExistsProcessedDocs = false;
        boolean cytDocsFound = false;
        
        // prior year tax documents
        boolean pytExistsNotReceivedDocs = false;
        boolean pytExistsReceivedVerificationDocs = false;
        boolean pytExistsReceivedNonVerificationDocs = false;
        boolean pytExistsProcessedDocs = false;
        boolean pytDocsFound = false;
        
        // prior prior year tax documents
        boolean ppytExistsNotReceivedDocs = false;
        boolean ppytExistsReceivedVerificationDocs = false;
        boolean ppytExistsReceivedNonVerificationDocs = false;
        boolean ppytExistsProcessedDocs = false;
        boolean ppytDocsFound = false;
        
        // loop through the documents and set the status flags
        for (School_Document_Assignment__c sda : sdaList) {
            // [DP] 08.24.2016 SFP-511 -- Doc Status needs to respect N/A requests/statuses
            Boolean sdaRequested = sda.Document__r.Document_Status__c == 'Not Applicable/Waive Requested'; 
            Boolean sdaRequestedAndApproved = sdaRequested && sda.Requirement_Waiver_Status__c == 'Approved';
            
            boolean isCurrentYearTaxDoc = ApplicationUtils.isCurrentYearTaxDocument(sda.Document_Type__c, sda.School_PFS_Assignment__r.Academic_Year_Picklist__c, sda.Document_Year__c);

            boolean isPriorYearTaxDoc = ApplicationUtils.isPriorYearTaxDocument(sda.Document_Type__c, sda.School_PFS_Assignment__r.Academic_Year_Picklist__c, sda.Document_Year__c);
            
            boolean isPriorPriorYearTaxDoc = ApplicationUtils.isPriorPriorYearTaxDocument(sda.Document_Type__c, sda.School_PFS_Assignment__r.Academic_Year_Picklist__c, sda.Document_Year__c);

            if (isCurrentYearTaxDoc) {
                cytDocsFound = true;
            }
            if (isPriorYearTaxDoc) {
                pytDocsFound = true;
            }
            if (isPriorPriorYearTaxDoc) {
                ppytDocsFound = true;
            }
            
            Boolean noDoc = sda.Document__c == null;
            Boolean deletedOrNoStatusDoc = ((deletedFamilyDocIds != null) && (deletedFamilyDocIds.contains(sda.Document__c))) || (sda.Document__r.Document_Status__c == null) || (sda.Document__r.Deleted__c == true);
            Boolean badDocStatus = sda.Document__r.Document_Status__c == 'Not Received' || sda.Document__r.Document_Status__c == 'Error/Invalid Document' || sda.Document__r.Document_Status__c == 'Upload Pending';

            if (noDoc || deletedOrNoStatusDoc || badDocStatus || (sdaRequested && !sdaRequestedAndApproved)){                        
                existsNotReceivedDocs = true;
                if (isCurrentYearTaxDoc) {
                    cytExistsNotReceivedDocs = true;
                } else if (isPriorYearTaxDoc) {
                    pytExistsNotReceivedDocs = true;
                } else if (isPriorPriorYearTaxDoc) {
                    ppytExistsNotReceivedDocs = true;
                }
            }
            else if (sda.Document__r.Document_Status__c == 'Received/In Progress') {
                if (GlobalVariables.isVerifiableDocumentType(sda.Document__r.Document_Type__c)) {
                    existsReceivedVerificationDocs = true;
                    if (isCurrentYearTaxDoc) {
                        cytExistsReceivedVerificationDocs = true;
                    } else if (isPriorYearTaxDoc) {
                        pytExistsReceivedVerificationDocs = true;
                    } else if (isPriorPriorYearTaxDoc) {
                        ppytExistsReceivedVerificationDocs = true;
                    }
                }
                else {
                    existsReceivedNonVerificationDocs = true;
                    if (isCurrentYearTaxDoc) {
                        cytExistsReceivedNonVerificationDocs = true;
                    } else if (isPriorYearTaxDoc) {
                        pytExistsReceivedNonVerificationDocs = true;
                    } else if (isPriorPriorYearTaxDoc) {
                        ppytExistsReceivedNonVerificationDocs = true;
                    }
                }
            }
            // [DP] 08.24.2016 SFP-511 -- Doc Status needs to respect N/A requests/statuses -- 
            // if sda is requested and approved, consider it "processed"
            else if (sda.Document__r.Document_Status__c == 'Processed' || sdaRequestedAndApproved) {
                existsProcessedDocs = true;
                if (isCurrentYearTaxDoc) {
                    cytExistsProcessedDocs = true;
                } else if (isPriorYearTaxDoc) {
                    pytExistsProcessedDocs = true;
                } else if (isPriorPriorYearTaxDoc) {
                    ppytExistsProcessedDocs = true;
                }
            }
        }
        
        // calculate the combined document status based on the status flags
        if (existsNotReceivedDocs) {
            if (existsReceivedVerificationDocs || existsReceivedNonVerificationDocs || existsProcessedDocs) {
                status.DocumentStatus = 'Required Documents Outstanding';
            }
            else {
                status.DocumentStatus = 'No Documents Received';
            }
        }
        else {
            if (existsReceivedVerificationDocs) {
                status.DocumentStatus = 'Required Documents Received/In Progress';
            }
            else {
                status.DocumentStatus = RequiredDocsCompleteStatus;
            }
        }
        
        if (cytDocsFound) {
            if (cytExistsNotReceivedDocs) {
                if (cytExistsReceivedVerificationDocs || cytExistsReceivedNonVerificationDocs || cytExistsProcessedDocs) {
                    status.CurrentYearTaxStatus = 'Required Documents Outstanding';
                }
                else {
                    status.CurrentYearTaxStatus = 'No Documents Received';
                }
            } else {
                if (cytExistsReceivedVerificationDocs) {
                    status.CurrentYearTaxStatus = 'Required Documents Received/In Progress';
                }
                else {
                    status.CurrentYearTaxStatus = RequiredDocsCompleteStatus;
                }            
            }
        } else {
            status.CurrentYearTaxStatus = 'No Documents Received';            
        }

        if (pytDocsFound) {
            if (pytExistsNotReceivedDocs) {
                if (pytExistsReceivedVerificationDocs || pytExistsReceivedNonVerificationDocs || pytExistsProcessedDocs) {
                    status.PriorYearTaxStatus = 'Required Documents Outstanding';
                }
                else {
                    status.PriorYearTaxStatus = 'No Documents Received';
                }
            } else {
                if (pytExistsReceivedVerificationDocs) {
                    status.PriorYearTaxStatus = 'Required Documents Received/In Progress';
                }
                else {
                    status.PriorYearTaxStatus = RequiredDocsCompleteStatus;
                }            
            }
        } else {
            status.PriorYearTaxStatus = 'No Documents Received';            
        }
        
        if (ppytDocsFound) {
            if (ppytExistsNotReceivedDocs) {
                if (ppytExistsReceivedVerificationDocs || ppytExistsReceivedNonVerificationDocs || ppytExistsProcessedDocs) {
                    status.PriorPriorYearTaxStatus = 'Required Documents Outstanding';
                }
                else {
                    status.PriorPriorYearTaxStatus = 'No Documents Received';
                }
            } else {
                if (ppytExistsReceivedVerificationDocs) {
                    status.PriorPriorYearTaxStatus = 'Required Documents Received/In Progress';
                }
                else {
                    status.PriorPriorYearTaxStatus = RequiredDocsCompleteStatus;
                }            
            }
        } else {
            status.PriorPriorYearTaxStatus = 'No Documents Received';            
        }
         
        return status;
        
    }
    /*** [SL] NAIS-282 - END refactored code  ***/
    
    // NAIS-2114 [DP] 12.18.2014 generic method to query SDAs for parent SPAs
    // NAIS-2125 [CH] Adding filter to exclude Other/Misc documents from Document Status
    public static List<School_Document_Assignment__c> getChildSDAs(List<School_PFS_Assignment__c> allSPAs){
        return [SELECT Id, Deleted__c, Document__c, Document__r.Name, Document__r.Document_Status__c, Document__r.Deleted__c, 
                                                            Document__r.Document_Type__c, Requirement_Waiver_Status__c, School_PFS_Assignment__c,
                                                            // Document__r.Academic_Year__r.Name,         // NAIS-2383 [DP] 04.02.2015 refactor to get name for picklist value
                                                            Document__r.Document_Year__c,
                                                            Document_Type__c, Document_Year__c, School_PFS_Assignment__r.Academic_Year_Picklist__c
                                                            FROM School_Document_Assignment__c 
                                                            WHERE School_PFS_Assignment__c in :allSPAs
                                                            AND Document_Type__c != 'Other/Misc Tax Form' // NAIS-2125
                                                            //AND Requirement_Waiver_Status__c != 'Approved' / [DP] 08.24.2016 SFP-511
                                                            //AND Document__r.Document_Status__c != 'Not Applicable/Waive Requested' / [DP] 08.24.2016 SFP-511
                                                            AND Deleted__c = false ];
    }
    
/*
        // input
    Set <String> familyDocumentIdSet;
    Set <String> requiredDocumentIdSet;
    Set <String> schoolIdSet;

    // to prevent trigger recursive calls
    public static Boolean isExecuting = false;
    
    // constructor
    public SchoolSetSPFSDocumentStatus() {
    
    }
    
    public SchoolSetSPFSDocumentStatus(Set <String> fdIdSet, Set <String> rdIdSet, Set <String> schIdSet) {
        familyDocumentIdSet = fdIdSet;
        requiredDocumentIdSet = rdIdSet;
        schoolIdSet = schIdSet;
    }

    public void setSPFSDocumentStatus() {    
        // start trigger execution
        isExecuting = true;
        
        Set <String> schIdSet = new Set <String>();
        Map <String, Set <String>> schoolId_reqDocIdSet_map = new Map <String, Set <String>>();
        Map <String, Set <String>> schoolId_sdaReqDocIdSet_map = new Map <String, Set <String>>();
        List <School_PFS_Assignment__c> spfsaUpdateList = new List <School_PFS_Assignment__c>();
        Set <String> nonVerifyDocTypeSet = new Set <String>();
        
//refactored as part of NAIS-282 DP [6.20.13]
//        // load non verifiable document types from custom settings
//        List <Document_Custom_Settings__c> csList = Document_Custom_Settings__c.getAll().values();
//        for (Document_Custom_Settings__c cs : csList) {
//            if (cs.Value__c != null && cs.Active__c == true) {
//                if (cs.Name.startsWith('Non_Verifiable_Document_Type')) {
//                    for (String docType : cs.Value__c.split(';')) {
//                        if (docType != null && docType.trim() != null) {
//                            nonVerifyDocTypeSet.add(docType.trim());
//                        }
//                    }
//                }
//            }
//        }

                
        // get all affected schools
        // we need to get from school document assignment and school pfs assignment
        // either may be delinked from school
        if (familyDocumentIdSet != null && familyDocumentIdSet.size() > 0) {
            List <Family_Document__c> fdList = [select Id, Name, Deleted__c,
                (select Id, Name, 
                    Deleted__c,
                    Required_Document__c, 
                    Required_Document__r.School__c,
                    School_PFS_Assignment__c, 
                    School_PFS_Assignment__r.School__c
                    from School_Document_Assignments__r
                    where Deleted__c = false)
                from Family_Document__c
                where Id in :familyDocumentIdSet and Deleted__c = false];    
    
            for (Family_Document__c fd : fdList) {
                for (School_Document_Assignment__c sda : fd.School_Document_Assignments__r) {
                    if (sda.Required_Document__r.School__c != null) {
                        schIdSet.add(sda.Required_Document__r.School__c);
                    }
                    if (sda.School_PFS_Assignment__r.School__c != null) {
                        schIdSet.add(sda.School_PFS_Assignment__r.School__c);
                    }
                }
            } 
        }
        
        if (requiredDocumentIdSet != null && requiredDocumentIdSet.size() > 0) {
            for (Required_Document__c rd : [select Id, Name, Deleted__c, School__c 
                from Required_Document__c
                where Id in :requiredDocumentIdSet and Deleted__c = false]) {
                if (rd.School__c != null) {
                    schIdSet.add(rd.School__c);
                }    
            }
        }
        // add input school ids
        if (schoolIdSet != null) {
            schIdSet.addAll(schoolIdSet);
        }
        
        // top down rollup from school
        if (schIdSet.size() > 0) {
        
            // load school ->> required documents map
            List <Required_Document__c> rdList = [select Id, Name, Deleted__c, School__c,
                (select Id, Name, 
                    Document__c, 
                    Document__r.Deleted__c, 
                    Document__r.Document_Status__c
                    from School_Document_Assignments__r
                    where Document__c != null and Document__r.Deleted__c = false) 
                from Required_Document__c
                where School__c in :schIdSet and Deleted__c = false];
                
            Set <String> rdIdSet; 
            Map <String, List <School_Document_Assignment__c>> reqDocId_sdaList_map = new Map <String, List <School_Document_Assignment__c>>();   
            
            for (Required_Document__c rd : rdList) {
                if (schoolId_reqDocIdSet_map.containsKey(rd.School__c)) {
                    rdIdSet = schoolId_reqDocIdSet_map.get(rd.School__c);
                } else {
                    rdIdSet = new Set <String>();
                }
                rdIdSet.add(rd.Id);
                schoolId_reqDocIdSet_map.put(rd.School__c, rdIdSet);
                reqDocId_sdaList_map.put(rd.Id, rd.School_Document_Assignments__r);
            }

            // rollup to school pfs assignment
            
            List <School_PFS_Assignment__c> spfsaList = [select Id, Name, PFS_Document_Status__c, School__c,
                    (select Id, Name, 
                        Deleted__c,
                        Document__c, 
                        Document__r.Deleted__c, 
                        Document__r.Document_Status__c, 
                        Document__r.Document_Type__c,
                        Required_Document__c, 
                        Required_Document__r.Deleted__c
                    from School_Document_Assignments__r
                    where Deleted__c = false and
                        Document__c != null and 
                        Document__r.Deleted__c = false and
                        Required_Document__c != null and 
                        Required_Document__r.Deleted__c = false)
                from School_PFS_Assignment__c
                where School__c in :schIdSet];

            // get existing required documents for school pfs assignment
            for (School_PFS_Assignment__c spfsa : spfsaList) {
                Set <String> sdaReqDocIdSet;
                for (School_Document_Assignment__c sda : spfsa.School_Document_Assignments__r) {
                    if (schoolId_sdaReqDocIdSet_map.containsKey(spfsa.School__c)) {
                        sdaReqDocIdSet = schoolId_sdaReqDocIdSet_map.get(spfsa.School__c);
                    } else {
                        sdaReqDocIdSet = new Set <String>();
                    }
                    sdaReqDocIdSet.add(sda.Required_Document__c);
                    schoolId_sdaReqDocIdSet_map.put(spfsa.School__c, sdaReqDocIdSet);
                } 
            }
                
            for (School_PFS_Assignment__c spfsa : spfsaList) {
            
                // get all required documents for school
                Set <String> reqDocIdSet = new Set <String>();
                if (schoolId_reqDocIdSet_map.containsKey(spfsa.School__c)) {
                    reqDocIdSet = schoolId_reqDocIdSet_map.get(spfsa.School__c);
                }
                
                // get all received documents for school
                Set <String> sdaReqDocIdSet = new Set <String>();
                if (schoolId_sdaReqDocIdSet_map.containsKey(spfsa.School__c)) {
                    sdaReqDocIdSet = schoolId_sdaReqDocIdSet_map.get(spfsa.School__c);
                }
                
                Boolean areAllFamDocsAvailable = true;
                Boolean areAllFamDocsProcessed = true;
                Boolean atleastOneFamDocAvailable = false;
                
                for (String reqDocId : reqDocIdSet) {
                    if (sdaReqDocIdSet.contains(reqDocId) == false) {
                        // required document missing
                        areAllFamDocsAvailable = false;
                    } else if (sdaReqDocIdSet.size() > 0) {
                        // required document available
                        atleastOneFamDocAvailable = true;
                        // are all family documents assigned to required document have status Processed ?
                        if (reqDocId_sdaList_map.containsKey(reqDocId)) {
                            List <School_Document_Assignment__c> sdaList = reqDocId_sdaList_map.get(reqDocId);
                            for (School_Document_Assignment__c sda : sdaList) {
                                if (sda.Document__r.Document_Status__c != 'Processed') {
                                    areAllFamDocsProcessed = false;
                                }
                            }
                        }
                    }
                }
                
                // are all family documents additionally recquired have status processed ?
                // this applies only to verifiable documents
                for (School_Document_Assignment__c sda : spfsa.School_Document_Assignments__r) {
                    if (sda.Required_Document__c == null) {
                        // refactor as part of case NAIS-5282 - DP 6.20.13
                        if (GlobalVariables.isVerifiableDocumentType(sda.Document__r.Document_Type__c)){
                        //if (nonVerifyDocTypeSet.contains(sda.Document__r.Document_Type__c) == false) {
                            if (sda.Document__r.Document_Status__c != 'Processed') {
                                areAllFamDocsProcessed = false;
                            }
                        }
                    }
                }
                
                // decide school pfs status
                if (areAllFamDocsAvailable == true) {
                    if (areAllFamDocsProcessed == true) {
                        if (spfsa.PFS_Document_Status__c != 'Required Documents Complete') {
                            spfsa.PFS_Document_Status__c = 'Required Documents Complete';
                            spfsaUpdateList.add(spfsa);
                        }
                    } else {
                        if (spfsa.PFS_Document_Status__c != 'Required Documents Received/In Progress') {
                            spfsa.PFS_Document_Status__c = 'Required Documents Received/In Progress';
                            spfsaUpdateList.add(spfsa);
                        }
                    }
                } else {
                    if (atleastOneFamDocAvailable == true) {
                        if (spfsa.PFS_Document_Status__c != 'Required Documents Outstanding') {
                            spfsa.PFS_Document_Status__c = 'Required Documents Outstanding';
                            spfsaUpdateList.add(spfsa);
                        }
                    } else {
                        if (spfsa.PFS_Document_Status__c != 'No Documents Received') {
                            spfsa.PFS_Document_Status__c = 'No Documents Received';
                            spfsaUpdateList.add(spfsa);
                        }
                    }
                }
            }      
    
        } 
        
        // update 
        if (spfsaUpdateList.size() > 0) {
            update spfsaUpdateList;
        }
        
        // end trigger execution
        isExecuting = false;
    } 

    // this should be called from School_PFS_Assignment__c before insert, update trigger
    public void setSPFSDocumentStatus(List <School_PFS_Assignment__c> spfsaList) {
    
        // start trigger execution
        isExecuting = true;

        Set <String> schIdSet = new Set <String>();
        Map <String, Set <String>> schoolId_reqDocIdSet_map = new Map <String, Set <String>>();
        Map <String, Set <String>> schoolId_sdaReqDocIdSet_map = new Map <String, Set <String>>();

        // get all affected schools
        for (School_PFS_Assignment__c spfsa : spfsaList) {
            if (spfsa.School__c != null) {
                schIdSet.add(spfsa.School__c);
            }
        }            

        // top down rollup from school
        if (schIdSet.size() > 0) {

            Set <String> nonVerifyDocTypeSet = new Set <String>();
            
            // load non verifiable document types from custom settings
            List <Document_Custom_Settings__c> csList = Document_Custom_Settings__c.getAll().values();
            for (Document_Custom_Settings__c cs : csList) {
                if (cs.Value__c != null && cs.Active__c == true) {
                    if (cs.Name.startsWith('Non_Verifiable_Document_Type')) {
                        for (String docType : cs.Value__c.split(';')) {
                            if (docType != null && docType.trim() != null) {
                                nonVerifyDocTypeSet.add(docType.trim());
                            }
                        }
                    }
                }
            }
    
            // load school ->> required documents map
            List <Required_Document__c> rdList = [select Id, Name, Deleted__c, School__c,
                (select Id, Name, Document__c, Document__r.Deleted__c, Document__r.Document_Status__c
                    from School_Document_Assignments__r
                    where Document__c != null and Document__r.Deleted__c = false) 
                from Required_Document__c
                where School__c in :schIdSet and Deleted__c = false];
                
            Set <String> rdIdSet; 
            Map <String, List <School_Document_Assignment__c>> reqDocId_sdaList_map = new Map <String, List <School_Document_Assignment__c>>();   
            
            for (Required_Document__c rd : rdList) {
                if (schoolId_reqDocIdSet_map.containsKey(rd.School__c)) {
                    rdIdSet = schoolId_reqDocIdSet_map.get(rd.School__c);
                } else {
                    rdIdSet = new Set <String>();
                }
                rdIdSet.add(rd.Id);
                schoolId_reqDocIdSet_map.put(rd.School__c, rdIdSet);
                reqDocId_sdaList_map.put(rd.Id, rd.School_Document_Assignments__r);
            }

            // rollup to school pfs assignment
            // this may work for update but not for insert because i think School_PFS_Assignment__c record does not
            // exist in before insert event
            // we dont need to worry about insert because school document assignment does not exist 
            Map <String, List <School_Document_Assignment__c>> spfsaId_sdaList_map = new Map <String, List <School_Document_Assignment__c>>();
            List <School_PFS_Assignment__c> spfsaListLocal = [select Id, Name, PFS_Document_Status__c, School__c,
                    (select Id, Name, 
                        Deleted__c,
                        Document__c, 
                        Document__r.Deleted__c, 
                        Document__r.Document_Status__c, 
                        Document__r.Document_Type__c, 
                        Required_Document__c, 
                        Required_Document__r.Deleted__c
                    from School_Document_Assignments__r
                    where Deleted__c = false and 
                        Document__c != null and 
                        Document__r.Deleted__c = false and
                        Required_Document__c != null and 
                        Required_Document__r.Deleted__c = false)
                from School_PFS_Assignment__c
                where School__c in :schIdSet];

            // get existing required documents for school pfs assignment
            for (School_PFS_Assignment__c spfsa : spfsaListLocal) {
                spfsaId_sdaList_map.put(spfsa.Id, spfsa.School_Document_Assignments__r);
                
                Set <String> sdaReqDocIdSet;
                for (School_Document_Assignment__c sda : spfsa.School_Document_Assignments__r) {
                    if (schoolId_sdaReqDocIdSet_map.containsKey(spfsa.School__c)) {
                        sdaReqDocIdSet = schoolId_sdaReqDocIdSet_map.get(spfsa.School__c);
                    } else {
                        sdaReqDocIdSet = new Set <String>();
                    }
                    sdaReqDocIdSet.add(sda.Required_Document__c);
                    schoolId_sdaReqDocIdSet_map.put(spfsa.School__c, sdaReqDocIdSet);
                } 
            }

            for (School_PFS_Assignment__c spfsa : spfsaList) {
            
                // get all required documents for school
                Set <String> reqDocIdSet = new Set <String>();
                if (schoolId_reqDocIdSet_map.containsKey(spfsa.School__c)) {
                    reqDocIdSet = schoolId_reqDocIdSet_map.get(spfsa.School__c);
                }
                
                // get all received documents for school
                Set <String> sdaReqDocIdSet = new Set <String>();
                if (schoolId_sdaReqDocIdSet_map.containsKey(spfsa.School__c)) {
                    sdaReqDocIdSet = schoolId_sdaReqDocIdSet_map.get(spfsa.School__c);
                }
                
                Boolean areAllFamDocsAvailable = true;
                Boolean areAllFamDocsProcessed = true;
                Boolean atleastOneFamDocAvailable = false;
                
                for (String reqDocId : reqDocIdSet) {
                    if (sdaReqDocIdSet.contains(reqDocId) == false) {
                        // required document missing
                        areAllFamDocsAvailable = false;
                    } else if (sdaReqDocIdSet.size() > 0) {
                        // required document available
                        atleastOneFamDocAvailable = true;
                        // are all family documents assigned to required document have status Processed ?
                        if (reqDocId_sdaList_map.containsKey(reqDocId)) {
                            List <School_Document_Assignment__c> sdaList = reqDocId_sdaList_map.get(reqDocId);
                            for (School_Document_Assignment__c sda : sdaList) {
                                if (sda.Document__r.Document_Status__c != 'Processed') {
                                    areAllFamDocsProcessed = false;
                                }
                            }
                        }
                    }
                }
                
                // are all family documents additionally recquired have status processed ?
                // this applies only to verifiable documents
                if (spfsaId_sdaList_map.containsKey(spfsa.Id)) {
                    List <School_Document_Assignment__c> sdaList = spfsaId_sdaList_map.get(spfsa.Id);
                    for (School_Document_Assignment__c sda : sdaList) {
                        if (sda.Required_Document__c == null) {
                            // refactor as part of case NAIS-5282 - DP 6.20.13
                            if (GlobalVariables.isVerifiableDocumentType(sda.Document__r.Document_Type__c)){
                            //if (nonVerifyDocTypeSet.contains(sda.Document__r.Document_Type__c) == false) {
                                if (sda.Document__r.Document_Status__c != 'Processed') {
                                    areAllFamDocsProcessed = false;
                                }
                            }
                        }
                    }
                }
                
                // decide school pfs status
                if (areAllFamDocsAvailable == true) {
                    if (areAllFamDocsProcessed == true) {
                        spfsa.PFS_Document_Status__c = 'Required Documents Complete';
                    } else {
                        spfsa.PFS_Document_Status__c = 'Required Documents Received/In Progress';
                    }
                } else {
                    if (atleastOneFamDocAvailable == true) {
                        spfsa.PFS_Document_Status__c = 'Required Documents Outstanding';
                    } else {
                        spfsa.PFS_Document_Status__c = 'No Documents Received';
                    }
                }
            }      
        }

        // end trigger execution
        isExecuting = false;
    }
    
    */
    
}