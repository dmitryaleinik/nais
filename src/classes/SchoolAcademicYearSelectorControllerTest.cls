@isTest
private class SchoolAcademicYearSelectorControllerTest {

    @isTest
    private static void testAcadSelectorStudControl(){
        Contact student = ContactTestData.Instance.asStudent().insertContact();
        Account school = AccountTestData.Instance.asSchool().DefaultAccount;

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
        Academic_Year__c previousAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, previousAcademicYear};

        PFS__c pfsCurrent = PfsTestData.Instance
            .forAcademicYearPicklist(currentAcademicYear.Name).create();
        PFS__c pfsPrevious = PfsTestData.Instance
            .forAcademicYearPicklist(previousAcademicYear.Name).create();
        insert new List<PFS__c>{pfsCurrent, pfsPrevious};

        Applicant__c applicantCurrent = ApplicantTestData.Instance
            .forPfsId(pfsCurrent.Id)
            .forContactId(student.Id).create();
        Applicant__c applicantPrevious = ApplicantTestData.Instance
            .forPfsId(pfsPrevious.Id)
            .forContactId(student.Id).create();
        insert new List<Applicant__c>{applicantCurrent, applicantPrevious};

        Student_Folder__c studentFolder = StudentFolderTestData.Instance
            .forStudentId(student.Id)
            .forAcademicYearPicklist(currentAcademicYear.Name).create();
        Student_Folder__c studentFolderPast = StudentFolderTestData.Instance
            .forStudentId(student.Id)
            .forAcademicYearPicklist(previousAcademicYear.Name).create();
        insert new List<Student_Folder__c>{studentFolder, studentFolderPast};
        
        School_PFS_Assignment__c spfsaCurrent = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicantCurrent.Id)
            .forStudentFolderId(studentFolder.Id)
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forSchoolId(school.Id).create();
        School_PFS_Assignment__c spfsaPrevious = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicantPrevious.Id)
            .forStudentFolderId(studentFolderPast.Id)
            .forAcademicYearPicklist(previousAcademicYear.Name)
            .forSchoolId(school.Id).create();
        insert new List<School_PFS_Assignment__c>{spfsaCurrent, spfsaPrevious};

        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(studentFolder);
            SchoolFolderSummaryController consumer = new SchoolFolderSummaryController(sc);
            
            SchoolAcademicYearSelectorStudentControl testCont = new SchoolAcademicYearSelectorStudentControl();
            
            testCont.folder = studentFolder;
            testCont.Consumer = consumer;
            testCont.loadMaps();
                        
            System.assertEquals(2, testCont.acadYearNameToAcadYear.size());
            System.assertEquals(2, testCont.acadYearNameToFolder.values().size());
            System.assertEquals(2, testCont.AcademicYearOptions.size());
            
            testCont.NewAcademicYearId = previousAcademicYear.Id;
            PageReference newPR = testCont.SchoolAcademicYearSelectorStudent_OnChange();
        Test.stopTest();
        
        System.assert(newPR.getURL().contains(studentFolderPast.Id)); 
    }
    
    @isTest
    private static void TestCurrentAcademicYear() {
        Account a = TestUtils.createAccount('test account', RecordTypes.schoolAccountTypeId, 500, true);
        Contact c = TestUtils.createContact('test contact', a.Id, RecordTypes.schoolStaffContactTypeId, true);
        
        List<Annual_Setting__c> settings = new List<Annual_Setting__c>();
        List<Academic_Year__c> years = TestUtils.createAcademicYears();
        for(Academic_Year__c year : years) {
            Annual_Setting__c annualSetting = new Annual_Setting__c(
                School__c = a.Id,
                Academic_Year__c = year.Id,
                PFS_Reminder_Date__c = date.newInstance(2013, 3, 31),
                Document_Reminder_Date__c = date.newInstance(2013, 3, 31));
            
            settings.add(annualSetting);
        }
        
        insert settings;
        
        Integer uniquefier = 1;
        Id portalProfileId = [select Id from Profile where Name = :ProfileSettings.SchoolAdminProfileName].Id;
        User u = TestUtils.createPortalUser('test portaluser', 'testAnnSettings@exponentpartners.com' + String.valueOf(uniquefier++), 'test', c.Id, portalProfileId, true, false);
        
        // [CH] NAIS-1766 Adding to help avoid Mixed DML Errors
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        System.runAs(thisUser) {
            Test.startTest();
            
                Database.insert(u);
            
                System.runAs(u){
            
                    PageReference pageRef = Page.SchoolAnnualSettings;
                    Test.setCurrentPageReference(pageRef);
                    
                    SchoolAnnualSettingsController myPageCon = new SchoolAnnualSettingsController();
                    myPageCon.LoadAnnualSetting();
                    
                    SchoolAcademicYearSelectorController controller = new SchoolAcademicYearSelectorController();
                    controller.Consumer = myPageCon;
                    
                    System.assertNotEquals(controller.CurrentAcademicYear, null);
                    System.assertNotEquals(controller.getAcademicYearMap(), null);
                    System.assert(controller.getAcademicYears().size() > 0);
                    System.assert(controller.getAcademicYearOptions().size() > 0);
                    
                    controller.NewAcademicYearId = GlobalVariables.getPreviousAcademicYear().Id;
                    controller.saveRecord = 'false';
                    controller.SchoolAcademicYearSelector_OnChange();
                }
                
            Test.stopTest();
        }
    }
}