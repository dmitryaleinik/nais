@isTest
private class SchoolAnnualSettingsQuestionsCntrlTest {

    private static Annual_Setting__c annualSetting;
    private static Academic_Year__c currentYear;
    private static User schoolPortalUser;
    // make our own dates so we don't need to worry about timezones
    private static Date todayPlus1Day = System.today() + 1;
    private static Date todayPlus2Days = System.today() + 2;
    private static Date todayPlus3Days = System.today() + 3;
    private static Date todayPlus4Days = System.today() + 4;
    private static Date todayPlus5Days = System.today() + 5;
    private static Date todayPlus6Days = System.today() + 6;
    private static Question_Labels__c ql;
    private static Id academicYearId;

    private static void setup() {
        Account school = AccountTestData.Instance.DefaultAccount;
        schoolPortalUser = UserTestData.insertSchoolPortalUser();
        TestUtils.insertAccountShare(school.Id, schoolPortalUser.Id);

        currentYear = AcademicYearTestData.Instance
            .forFamilyPortalStartDate(Date.Today().addDays(-200))
            .forFamilyPortalEndDate(Date.Today().addDays(+200)).DefaultAcademicYear;
        academicYearId = currentYear.Id;

        annualSetting = AnnualSettingsTestData.Instance
            .forAcademicYearId(academicYearId)
            .forPFSReminderDate(Date.newInstance(Date.today().addyears(-3).year(), 3, 31))
            .forPriorYearTaxFormReminderNew(date.newInstance(Date.today().addyears(-3).year(), 3, 31))
            .usingAdditionalQuestions().insertAnnualSettings();

        ql = new Question_Labels__c(Academic_Year__c = academicYearId, Language__c = 'en_US');

        for (Integer i = 1; i < AnnualSettingHelper.CUSTOM_QUESTION_BANK_SIZE; i++) {
            ql.put('Custom_Question_' + String.valueOf(i) + '__c', 'Custom Question ' + String.valueOf(i) + ' Label');
        }
        ql.put('Custom_Question_15__c', null);

        insert ql;

        SchoolPortalSettings.Additional_Questions_Limit = 5.0;
    }

    @isTest
    private static void testAnnualSettingsCustomQuestionLimit() {
        setup();

        Test.startTest();
            System.runAs(schoolPortalUser) {
                PageReference pageRef = Page.SchoolAnnualSettings;
                pageRef.getParameters().put('academicyearid', academicYearId);
                Test.setCurrentPageReference(pageRef);

                SchoolAnnualSettingsController myPageCon = new SchoolAnnualSettingsController();
                myPageCon.LoadAnnualSetting();

                SchoolAnnualSettingsAddQuestionsCntrl questionsCompCtrl = new SchoolAnnualSettingsAddQuestionsCntrl();

                questionsCompCtrl.annualSettingsModel = myPageCon.Model;
                questionsCompCtrl.academicYearId = academicYearId;

                System.assertNotEquals(null, questionsCompCtrl.labels);
                System.assertEquals('Custom Question 1 Label', questionsCompCtrl.labels.Custom_Question_1__c);
                System.assertEquals(true, questionsCompCtrl.customQuestionHasLabel.get('Custom_Question_1__c'));
                System.assertEquals(false, questionsCompCtrl.customQuestionHasLabel.get('Custom_Question_15__c'));
                System.assertEquals('Yes', questionsCompCtrl.annualSettingsModel.Use_Additional_Questions__c);
                System.assertEquals('5', questionsCompCtrl.customQuestionLimit);
            }
        Test.stopTest();
    }

    @isTest
    private static void testAnnualSettingsCustomQuestionOverride() {
        setup();

        annualSetting.Additional_Questions_Limit_Override__c = 10.0;
        update annualSetting;

        // In order to edit custom question info, the family portal start date needs to be in the future.
        Date familyPortalStartDate = Date.today().addDays(10);
        
        updateFamilyPortalStartDate(academicYearId, familyPortalStartDate);

        SchoolAnnualSettingsController myPageCon;

        Test.startTest();
            System.runAs(schoolPortalUser) {
                PageReference pageRef = Page.SchoolAnnualSettings;
                pageRef.getParameters().put('academicyearid', academicYearId);
                Test.setCurrentPageReference(pageRef);

                myPageCon = new SchoolAnnualSettingsController();
                myPageCon.LoadAnnualSetting();

                SchoolAnnualSettingsAddQuestionsCntrl questionsCompCtrl = new SchoolAnnualSettingsAddQuestionsCntrl();

                questionsCompCtrl.annualSettingsModel = myPageCon.Model;
                questionsCompCtrl.academicYearId = academicYearId;

                System.assertEquals(true, questionsCompCtrl.customQuestionBankEditable);
                System.assertEquals('10', questionsCompCtrl.customQuestionLimit);
                System.assertEquals('0', questionsCompCtrl.countCustomQuestionsUsed);

                myPageCon.Model.Custom_Question_1__c = true;

                System.assertEquals('1', questionsCompCtrl.countCustomQuestionsUsed);
                System.assertEquals('(1/10)', questionsCompCtrl.customQuestionsLimitUsed);
                System.assertEquals('Please select up to 10 additional questions.', questionsCompCtrl.customQuestionLimitText);

                // set all settings to save successfully
                myPageCon.Model.Send_PFS_Reminders__c = false;
                myPageCon.Model.Send_Prior_Year_Document_Reminders__c = true;
                myPageCon.Model.Send_Current_Year_Document_Reminders__c  = true;
                myPageCon.Model.Curr_Year_Tax_Docs_Reminder_Date_New__c = todayPlus1Day;
                myPageCon.Model.Curr_Yr_Tax_Docs_Remind_Date_Returning__c = todayPlus2Days;
                myPageCon.Model.Document_Reminder_Date__c = todayPlus3Days;
                myPageCon.Model.Document_Reminder_Date_Returning__c = todayPlus4Days;
                myPageCon.Model.PFS_Reminder_Date__c = todayPlus5Days;
                myPageCon.Model.PFS_Reminder_Date_Returning__c = todayPlus6Days;

                myPageCon.Model.Current_Year_Tax_Documents_Deadline_Type__c = 'test deadline type';
                myPageCon.Model.Current_Year_Tax_Documents_New__c = todayPlus3Days;
                myPageCon.Model.Current_Year_Tax_Documents_Returning__c = todayPlus3Days;
                myPageCon.Model.Prior_Year_Tax_Documents_Deadline_Type__c = 'test deadline type';
                myPageCon.Model.Prior_Year_Tax_Documents_New__c = todayPlus1Day;
                myPageCon.Model.Prior_Year_Tax_Documents_Returning__c = todayPlus3Days;
                myPageCon.Model.PFS_Deadline_Type__c = 'test deadline type';
                myPageCon.Model.PFS_New__c = todayPlus1Day;
                myPageCon.Model.PFS_Returning__c = todayPlus2Days;

                myPageCon.Model.Tuition_Schedule_Type__c = 'One Schedule per Grade';

                // These should correspond to the 5th grade, which is the default for SPAs created using
                //  the TestUtils.createSchoolPFSAssignment() method
                myPageCon.Grades[9].Day_New_Student_Expense = 2;
                myPageCon.Grades[9].Day_Returning_Student_Expense = 3;

                myPageCon.applyAll();
                myPageCon.submit();

                //// Expected that error message will appear, because deadline dates have to be within Family Portal start and end dates
                System.assertEquals(false, AnnualSettingHelper.IsValid(myPageCon.Model), getValidationErrorsFromPageMessages());

                // begin second half of enormus test

                // In order to prevent editing custom question info, the family portal start date needs to be in the past.
                Date pastFamilyPortalStartDate = Date.today().addDays(-10);
                updateFamilyPortalStartDate(academicYearId, pastFamilyPortalStartDate);

                myPageCon = new SchoolAnnualSettingsController();

                myPageCon.LoadAnnualSetting();
                questionsCompCtrl = new SchoolAnnualSettingsAddQuestionsCntrl();

                questionsCompCtrl.annualSettingsModel = myPageCon.Model;
                questionsCompCtrl.academicYearId = academicYearId;

                System.assertEquals(true, questionsCompCtrl.annualSettingsModel.Custom_Question_1__c);

                System.assertEquals(false, questionsCompCtrl.customQuestionBankEditable);

                // test too many questions selected
                annualSetting.Additional_Questions_Limit_Override__c = 3.0;

                update annualSetting;

                myPageCon = new SchoolAnnualSettingsController();
                myPageCon.LoadAnnualSetting();

                questionsCompCtrl = new SchoolAnnualSettingsAddQuestionsCntrl();
                questionsCompCtrl.annualSettingsModel = myPageCon.Model;
                questionsCompCtrl.academicYearId = academicYearId;

                myPageCon.Model.Custom_Question_1__c = true;
                myPageCon.Model.Custom_Question_2__c = true;
                myPageCon.Model.Custom_Question_3__c = true;
                myPageCon.Model.Custom_Question_4__c = true;

                System.assertEquals('4', questionsCompCtrl.countCustomQuestionsUsed);
                System.assertEquals('3', questionsCompCtrl.customQuestionLimit);

                // set all settings to save successfully
                myPageCon.Model.Send_PFS_Reminders__c = false;
                myPageCon.Model.Send_Prior_Year_Document_Reminders__c = true;
                myPageCon.Model.Send_Current_Year_Document_Reminders__c  = true;
                myPageCon.Model.Curr_Year_Tax_Docs_Reminder_Date_New__c = todayPlus1Day;
                myPageCon.Model.Curr_Yr_Tax_Docs_Remind_Date_Returning__c = todayPlus2Days;
                myPageCon.Model.Document_Reminder_Date__c = todayPlus3Days;
                myPageCon.Model.Document_Reminder_Date_Returning__c = todayPlus4Days;
                myPageCon.Model.PFS_Reminder_Date__c = todayPlus5Days;
                myPageCon.Model.PFS_Reminder_Date_Returning__c = todayPlus6Days;

                myPageCon.Model.Current_Year_Tax_Documents_Deadline_Type__c = 'test deadline type';
                myPageCon.Model.Current_Year_Tax_Documents_New__c = todayPlus3Days;
                myPageCon.Model.Current_Year_Tax_Documents_Returning__c = todayPlus3Days;
                myPageCon.Model.Prior_Year_Tax_Documents_Deadline_Type__c = 'test deadline type';
                myPageCon.Model.Prior_Year_Tax_Documents_New__c = todayPlus1Day;
                myPageCon.Model.Prior_Year_Tax_Documents_Returning__c = todayPlus3Days;
                myPageCon.Model.PFS_Deadline_Type__c = 'test deadline type';
                myPageCon.Model.PFS_New__c = todayPlus1Day;
                myPageCon.Model.PFS_Returning__c = todayPlus2Days;

                myPageCon.Model.Tuition_Schedule_Type__c = 'One Schedule per Grade';

                // These should correspond to the 5th grade, which is the default for SPAs created using
                //  the TestUtils.createSchoolPFSAssignment() method
                myPageCon.Grades[9].Day_New_Student_Expense = 2;
                myPageCon.Grades[9].Day_Returning_Student_Expense = 3;

                myPageCon.applyAll();
                myPageCon.submit();

                // AnnualSettings is not valid due to too many questions selected
                System.assertEquals(false, AnnualSettingHelper.IsValid(myPageCon.Model));
            }
        Test.stopTest();
    }

    @isTest
    private static void testAnnualSettingsCustomQuestionsHiddenIfNoLabels() {
        setup();

        // Clear custom question labels
        for (Integer i = 1; i < AnnualSettingHelper.CUSTOM_QUESTION_BANK_SIZE; i++) {
            ql.put('Custom_Question_' + String.valueOf(i) + '__c', null);
        }

        update ql;

        Test.startTest();
            System.runAs(schoolPortalUser) {
                SchoolAnnualSettingsController myPageCon = new SchoolAnnualSettingsController();
                myPageCon.LoadAnnualSetting();
                SchoolAnnualSettingsAddQuestionsCntrl questionsCompCtrl = new SchoolAnnualSettingsAddQuestionsCntrl();

                System.assert(true, questionsCompCtrl.getNoQuestionLabels());
            }
        Test.stopTest();
    }

    @isTest
    private static void testAnnualSettingsCustomQuestionsHiddenIfLabels() {
        setup();

        Test.startTest();
            System.runAs(schoolPortalUser) {
                SchoolAnnualSettingsController myPageCon = new SchoolAnnualSettingsController();
                myPageCon.LoadAnnualSetting();
                SchoolAnnualSettingsAddQuestionsCntrl questionsCompCtrl = new SchoolAnnualSettingsAddQuestionsCntrl();

                System.assert(true, questionsCompCtrl.getNoQuestionLabels());
            }
        Test.stopTest();
    }

    @isTest
    private static void customQuestionBankEditable_familyPortalNotYetOpened_expectTrue() {
        Integer numberOfYears = 1;

        List<Academic_Year__c> academicYears = AcademicYearTestData.Instance.createAcademicYears(numberOfYears);
        academicYears[0].Family_Portal_Start_Date__c = Date.today().addDays(10);
        insert academicYears;

        SchoolAnnualSettingsAddQuestionsCntrl controller = new SchoolAnnualSettingsAddQuestionsCntrl();
        controller.academicYearId = academicYears[0].Id;

        System.assert(controller.customQuestionBankEditable,
            'Expected custom questions to be editable if the academic year is in the future.');
    }

    @isTest
    private static void customQuestionBankEditable_familyPortalHasOpened_expectFalse() {
        Integer numberOfYears = 1;

        List<Academic_Year__c> academicYears = AcademicYearTestData.Instance.createAcademicYears(numberOfYears);
        academicYears[0].Family_Portal_Start_Date__c = Date.today().addDays(-10);
        insert academicYears;

        SchoolAnnualSettingsAddQuestionsCntrl controller = new SchoolAnnualSettingsAddQuestionsCntrl();
        controller.academicYearId = academicYears[0].Id;

        System.assert(!controller.customQuestionBankEditable,
            'Expected custom questions to not be editable if the academic year is in the past.');
    }

    @isTest
    private static void testNextAcademicYear() {
        Test.startTest();
            SchoolAnnualSettingsAddQuestionsCntrl controller =  new SchoolAnnualSettingsAddQuestionsCntrl();
        Test.stopTest();

        System.assertEquals(
            ' ' + GlobalVariables.getNextAcademicYearStr(AcademicYearService.Instance.getCurrentAcademicYear().Name),
            controller.nextAcademicYear);
    }

    private static void updateFamilyPortalStartDate(Id academicYearToUpdate, Date familyPortalStartDate) {
        Academic_Year__c recordToUpdate = new Academic_Year__c(
            Id = academicYearToUpdate,
            Family_Portal_Start_Date__c = familyPortalStartDate);

        // First update the record then clear the AcademicYearService cache.
        update recordToUpdate;
        AcademicYearService.Instance = null;
    }

    private static String getValidationErrorsFromPageMessages() {
        List<String> pageMessages = new List<String>();
        for(ApexPages.Message message : ApexPages.getMessages()) {
            pageMessages.add(message.getComponentLabel() + ' : ' + message.getSummary());
        }
        return 'Validation Errors from Page Messages: ' + String.join(pageMessages, ', ');
    }
}