@isTest
private class PaymentServiceTest {
    private static final String COUNTRY_CODE_UNITED_STATES = '840';

    private static void createAppFeeSetting(String appFeeSettingName) {
        Integer appFeeSettingAmount = 50;
        Integer appFeeSettingDiscountedAmount = 40;
        String appFeeSettingProductCode = 'Test Product Code';
        String appFeeSettingDiscountedProductCode = 'KS Test Product Code';
        Application_Fee_Settings__c appFeeSettings = ApplicationFeeSettingsTestData.Instance
                .forName(appFeeSettingName)
                .forAmount(appFeeSettingAmount)
                .forDiscountedAmount(appFeeSettingDiscountedAmount)
                .forProductCode(appFeeSettingProductCode)
                .forDiscountedProductCode(appFeeSettingDiscountedProductCode).DefaultApplicationFeeSettings;
    }

    private static Transaction_Line_Item__c setupCreditCardTransactionLineItem() {
        Opportunity opportunity = new Opportunity(
            Name = 'User - Application Fee',
            StageName = 'Open',
            CloseDate = Date.today(),
            Amount = 10.00,
            RecordTypeId = RecordTypes.pfsApplicationFeeOpportunityTypeId);
        insert opportunity;

        Decimal amount = 10.00;
        String cardType = 'Visa';
        String ccCardNumberLast4Digits = '1234';

        // general info
        Transaction_Line_Item__c transactionLineItem = new Transaction_Line_Item__c();
        transactionLineItem.RecordTypeId = RecordTypes.paymentAutoTransactionTypeId;
        transactionLineItem.Opportunity__c = opportunity.Id;
        transactionLineItem.Transaction_Status__c = 'Posted';
        transactionLineItem.Transaction_Date__c = System.today();
        transactionLineItem.Source__c = 'Web';
        transactionLineItem.Amount__c = amount;

        // transaction info
        transactionLineItem.Transaction_Type__c = 'Credit/Debit Card';
        transactionLineItem.Tender_Type__c = cardType;

        // payment info
        transactionLineItem.CC_Last_4_Digits__c = ccCardNumberLast4Digits;
        transactionLineItem.Transaction_ID__c = '12345';
        transactionLineItem.Transaction_Result_Code__c = '5001';
        transactionLineItem.VaultGUID__c = '123abc';
        transactionLineItem.Account_Label__c = 'AccountLabel';

        insert transactionLineItem;
        return transactionLineItem;
    }

    private static void createTestData(Boolean createOppAndTLIToo) {
        ChargentBaseGatewayTestData.Instance.insertGateway();

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        createAppFeeSetting(currentAcademicYear.Name);
        Application_Fee_Waiver__c afw1 = ApplicationFeeWaiverTestData.Instance.DefaultApplicationFeeWaiver;

        // create the default test data
        String parentACountry = 'United States';
        String parentAAdress = 'Line 1 Address\nLine 2 Address';
        PFS__c pfs = PfsTestData.Instance
            .asSubmitted()
            .forParentAAddress(parentAAdress)
            .forParentACountry(parentACountry)
            .forAcademicYearPicklist(currentAcademicYear.Name).DefaultPfs;

        if (createOppAndTLIToo){
            FinanceAction.createOppAndTransaction(new Set<Id>{pfs.Id});
        }
    }

    private static FamilyPaymentData setupPaymentData(String paymentType) {
        FamilyPaymentData paymentData = new FamilyPaymentData();

        paymentData.billingFirstName = 'Morgan';
        paymentData.billingLastName = 'Freeman';
        paymentData.paymentType = paymentType;
        paymentData.billingStreet1 = '123 Acme Way';
        paymentData.billingStreet2 = 'Suite 2';
        paymentData.billingCity = 'Rochester';
        paymentData.billingState = 'New York';
        paymentData.billingPostalCode = '14450';
        paymentData.billingCountry = COUNTRY_CODE_UNITED_STATES;
        paymentData.billingEmail = 'freeman@nais.org';
        paymentData.ccNameOnCard = 'Morgan Freeman';
        paymentData.ccCardNumber = '4111111111111111';
        paymentData.ccCardType = FamilyPaymentData.CC_TYPE_VISA;
        paymentData.ccSecurityCode = '111';
        paymentData.ccExpirationMM = '12';
        paymentData.ccExpirationYY = String.valueOf(Date.today().year()).right(2);
        paymentData.paymentDescription = 'A payment description';
        paymentData.paymentAmount = 100.00;
        paymentData.paymentTracker = 'A payment tracker';
        paymentData.checkAccountType = 'Checking';
        paymentData.checkAccountNumber = '123456';
        paymentData.checkRoutingNumber = '654321';
        paymentData.checkBankName = 'BankOfAmerica';
        paymentData.checkCheckNumber = '1';
        paymentData.checkCheckType = 'check';

        return paymentData;
    }

    @isTest
    private static void processPayment_nullRequest_expectArgumentNullException() {
        try {
            Test.startTest();
            PaymentService.Instance.processPayment(null);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, PaymentService.REQUEST_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void processCreditCard_nullRequest_expectArgumentNullException() {
        try {
            Test.startTest();
            PaymentService.Instance.processCreditCard(null);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, PaymentService.REQUEST_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void processECheck_nullRequest_expectArgumentNullException() {
        try {
            Test.startTest();
            PaymentService.Instance.processECheck(null);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, PaymentService.REQUEST_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void processRefund_nullRequest_expectArgumentNullException() {
        try {
            Test.startTest();
            PaymentService.Instance.processRefund(null);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, PaymentService.REQUEST_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void request_nullTransactionLineItem_expectArgumentNullException() {
        try {
            Test.startTest();
            new PaymentService.Request(null);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, PaymentService.TRANSACTION_LINE_ITEM_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void request_nullOpportunityAndPfs_expectArgumentNullException() {
        try {
            Test.startTest();
            new PaymentService.Request(null, null, new FamilyPaymentData(), true);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, PaymentService.OPPORTUNITY_AND_PFS_NULL);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void request_nullPaymentData_expectArgumentNullException() {
        try {
            Test.startTest();
            new PaymentService.Request(new Opportunity(), new PFS__c(), null, true);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, PaymentService.PAYMENT_INFORMATION_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void request_nullSendReceipt_expectFalse() {
        Test.startTest();
        PaymentService.Request request = new PaymentService.Request(
                new Opportunity(), new PFS__c(), setupPaymentData(FamilyPaymentData.PAYMENT_TYPE_CC), null);
        Test.stopTest();

        System.assert(!request.SendReceipt, 'Expected Send Receipt to be false.');
    }

    @isTest
    private static void processPayment_unrecognizedPayment_expectArgumentNullException() {
        FamilyPaymentData paymentData = new FamilyPaymentData();
        paymentData.paymentType = FamilyPaymentData.PAYMENT_TYPE_WAIVER;

        try {
            Test.startTest();
            new PaymentService.Request(new Opportunity(), null, paymentData, true);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, PaymentService.PAYMENT_INFORMATION_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void processRefund_expectRefundTransactionLineItemCreated() {
        createTestData(false);
        Transaction_Line_Item__c transactionLineItem = setupCreditCardTransactionLineItem();
        PaymentService.Request request = new PaymentService.Request(transactionLineItem);

        Test.startTest();
            PaymentService.Response response = PaymentService.Instance.processRefund(request);
        Test.stopTest();

        System.assert(response.Result.isSuccess, 'Expected the refund to be marked as successful.');

        Set<Id> opportunityIds = new Set<Id> { transactionLineItem.Opportunity__c };
        Set<Id> recordTypeIds = new Set<Id> { RecordTypes.refundTransactionTypeId };
        List<Transaction_Line_Item__c> transactionLineItems = TransactionLineItemSelector.Instance
                .selectByOpportunityAndRecordType(opportunityIds, recordTypeIds);

        System.assertNotEquals(null, transactionLineItems, 'Expected the result list to not be null.');
        System.assertEquals(1, transactionLineItems.size(), 'Expected there to be one refunded Transaction Line Item.');
        System.assertEquals(response.TransactionLineItemId, transactionLineItems[0].Id,
                'Expected the returned Transaction Line Item to be the refunded Transaction Line Item.');
    }

    @isTest
    private static void processCreditCard_expectCreditCardTransactionLineItemCreated() {
        createTestData(true);
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        FamilyPaymentData paymentData = setupPaymentData(FamilyPaymentData.PAYMENT_TYPE_CC);
        Opportunity opportunity = OpportunitySelector.Instance.selectByPfsId(new Set<Id> { pfs.Id })[0];

        PaymentService.Request request = new PaymentService.Request(opportunity, pfs, paymentData, false);
        PaymentService.Response response = PaymentService.Instance.processBefore(request);

        Test.startTest();
            response = PaymentService.Instance.processCreditCard(request);
        Test.stopTest();

        Set<Id> recordTypeIds = new Set<Id> { RecordTypes.paymentAutoTransactionTypeId };
        Set<Id> opportunityIds = new Set<Id> { opportunity.Id };
        List<Transaction_Line_Item__c> transactionLineItems = TransactionLineItemSelector.Instance
            .selectByOpportunityAndRecordType(opportunityIds, recordTypeIds);

        System.assertNotEquals(null, response, 'Expected the response to not be null.');
        System.assertNotEquals(null, response.Result, 'Expected the result to not be null.');
        System.assert(response.Result.isSuccess, 'Expected the transaction to be successful: ' + response.Result.ErrorMessage);
        System.assertEquals(1, transactionLineItems.size(), 'Expected there to be one transaction line item.');
        System.assertEquals(transactionLineItems[0].Id, response.TransactionLineItemId,
            'Expected the transaction line item id created to match the posted transaction line item found.');
    }

    @isTest
    private static void processCreditCard_invalidCardNumber_expectError() {
        createTestData(true);
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        FamilyPaymentData paymentData = setupPaymentData(FamilyPaymentData.PAYMENT_TYPE_CC);
        paymentData.ccCardNumber = '1234';
        PaymentService.Request request = new PaymentService.Request(null, pfs, paymentData, false);

        Test.startTest();
            PaymentService.Response response = PaymentService.Instance.processCreditCard(request);
        Test.stopTest();

        System.assertNotEquals(null, response, 'Expected the response to not be null.');
        System.assertNotEquals(null, response.Result, 'Expected the result to not be null.');
        System.assert(!response.Result.isSuccess, 'Expected the transaction to not be successful.');

        String expectedMessage = String.format(PaymentService.DEFAULT_ERROR_MESSAGE, new List<String> {
                String.valueOf(PaymentProviderTestHelpers.ERROR_CODE),
                PaymentProviderTestHelpers.INVALID_CREDIT_CARD_NUMBER,
                PaymentProviderTestHelpers.INVALID_CREDIT_CARD_NUMBER
        });

        System.assertEquals(expectedMessage, response.ErrorMessage, 'Expected the error message: ' + expectedMessage);
    }

    @isTest
    private static void processECheck_expectECheckTransactionLineItemCreated() {
        createTestData(true);
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        FamilyPaymentData paymentData = setupPaymentData(FamilyPaymentData.PAYMENT_TYPE_ECHECK);
        Opportunity opportunity = OpportunitySelector.Instance.selectByPfsId(new Set<Id> { pfs.Id })[0];

        PaymentService.Request request = new PaymentService.Request(opportunity, pfs, paymentData, false);

        Test.startTest();
            PaymentService.Response response = PaymentService.Instance.processCreditCard(request);
        Test.stopTest();

        Set<Id> recordTypeIds = new Set<Id> { RecordTypes.paymentAutoTransactionTypeId };
        Set<Id> opportunityIds = new Set<Id> { opportunity.Id };
        List<Transaction_Line_Item__c> transactionLineItems = TransactionLineItemSelector.Instance
            .selectByOpportunityAndRecordType(opportunityIds, recordTypeIds);

        System.assert(response.Result.isSuccess, 'Expected the transaction to be successful.');
        System.assertEquals(1, transactionLineItems.size(), 'Expected there to be one transaction line item.');
        System.assertEquals(transactionLineItems[0].Id, response.TransactionLineItemId,
                'Expected the transaction line item id created to match the posted transaction line item found.');
    }

    @isTest
    private static void processECheck_invalidCheck_expectECheckTransactionLineItemCreated() {
        createTestData(true);
        PFS__c pfs = PfsTestData.Instance.DefaultPfs;

        FamilyPaymentData paymentData = setupPaymentData(FamilyPaymentData.PAYMENT_TYPE_ECHECK);
        paymentData.checkBankName = null;
        Opportunity opportunity = OpportunitySelector.Instance.selectByPfsId(new Set<Id> { pfs.Id })[0];

        PaymentService.Request request = new PaymentService.Request(opportunity, pfs, paymentData, false);

        Test.startTest();
            PaymentService.Response response = PaymentService.Instance.processCreditCard(request);
        Test.stopTest();

        Set<Id> recordTypeIds = new Set<Id> { RecordTypes.paymentAutoTransactionTypeId };
        Set<Id> opportunityIds = new Set<Id> { opportunity.Id };
        List<Transaction_Line_Item__c> transactionLineItems = TransactionLineItemSelector.Instance
            .selectByOpportunityAndRecordType(opportunityIds, recordTypeIds);

        System.assert(!response.Result.isSuccess, 'Expected the transaction to be successful.');

        String expectedMessage = String.format(PaymentService.DEFAULT_ERROR_MESSAGE, new List<String> {
            String.valueOf(PaymentProviderTestHelpers.ERROR_CODE),
            PaymentProcessor.UNEXPECTED_ERROR,
            PaymentProcessor.UNEXPECTED_ERROR
        });

        System.assertEquals(expectedMessage, response.Result.ErrorMessage, 'Expected the error messages to match.');
    }
}