/**
 * @description Test implementation for HTTP Callouts.
 **/
@isTest
public class HttpCalloutMockImplementation implements HttpCalloutMock {
    /**
     * @description The test value for the Access Token attribute.
     */
    public static final String ACCESS_TOKEN_VALUE = 'generic test value';

    /**
     * @description The test value for the Href attribute.
     */
    public static final String HREF_VALUE = 'http://google.com';

    /**
     * @description The response that will be generated when the mock is called. The
     *              status code will be defaulted to 200 and will have a body set
     *              with a JSON object that contains the attributes that are expected
     *              by tests accessing this mock.
     * @param request The placeholder request. Currently nothing is done with this.
     * @returns The generic HTTPResponse with relevant values expected by callouts.
     */
    public HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HTTPResponse();
        response.setStatusCode(200);
        response.setBody('{' +
                        '"AccessTokenValue":"' + ACCESS_TOKEN_VALUE + '",' +
                        '"Href":"' + HREF_VALUE + '"' +
                '}');

        return response;
    }
}