@isTest
private class CommunityDebuggerControllerTest {
    private static final String LOGIN_PAGE_NAME = 'FamilyLogin';

    private static CommunityDebuggerController setup() {
        PageReference debug = Page.CommunityDebugger;
        debug.getParameters().put('pageName', LOGIN_PAGE_NAME);
        debug.getParameters().put('retUrl', '/sampleRedirect');

        Test.setCurrentPage(debug);
        return new CommunityDebuggerController();
    }

    @isTest
    private static void constructor_expectToLoadToBePageParam() {
        CommunityDebuggerController debuggerController = setup();

        System.assertEquals(LOGIN_PAGE_NAME, debuggerController.toLoad,
            'Expected page to load to be the value set as the page parameter.');
    }

    @isTest
    private static void fetchFailingPage_expectPopulatedResponse() {
        CommunityDebuggerController debuggerController = setup();

        Test.startTest();
        debuggerController.fetchFailingPage();
        Test.stopTest();

        System.assert(String.isNotEmpty(debuggerController.failingPageResponse),
                'Expected failing page response to be populated.');
    }
}