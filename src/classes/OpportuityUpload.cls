public class OpportuityUpload {

  public OpportuityUpload(ApexPages.StandardController controller) {

    if (!Test.isRunningTest()) {
      GetToken();
    }
  }
  private string pdocumentURL;

  public string Token {get; set;}

  public Blob Document {get; set;}
  public string DocumentName {get; set;}
  public string ForceFallBack {
    get{
      return ApexPages.currentPage().getParameters().get('fallback');


    }
  }
  public string DocumentURL {
    get{
      if (pdocumentURL == null && Document != null) {
        SendDocument();
        Document = null;
      }
      return pdocumentURL;
    }
    set{
      pdocumentURL = value;
    }
  }



  public void SendDocument() {
            //System.assertEquals(1,2, 'sendingdocument');

    HttpRequest req = new HttpRequest();


    req.setHeader('Accept', 'application/json');
    req.setHeader('Authorization', 'oauth ' + Token);
    //req.setHeader('content-type', 'application/x-www-form-urlencoded');
    if (!Test.isRunningTest()) {
      // BELOW LINE IS DEMO CODE
      //req.setEndpoint('https://apiuploaduatna11.springcm.com/v201411/folders/478ff48b-2ff4-e411-bd70-6c3be5a75f4d/documents?name=' + EncodingUtil.urlEncode(DocumentName, 'UTF-8') );
      req.setEndpoint('https://apiuploaduatna11.springcm.com/v201411/folders/70424d26-c163-e611-bb8d-6c3be5a75f4d/documents?name=' + EncodingUtil.urlEncode(DocumentName, 'UTF-8') );
      //req.setEndpoint('https://apiuploaduatna11.springcm.com/v201411/folders/170ead2f-eb63-e511-9fcc-6c3be5a75f4d?name=' + EncodingUtil.urlEncode(DocumentName, 'UTF-8') );
    }
    req.setMethod('POST');
    if (!Test.isRunningTest()) {
      req.setBodyAsBlob(Document);
    }
    //req.setBody('SFSession=' + UserInfo.getSessionId() + '|' + EncodingUtil.urlEncode(url, 'UTF-8') + '&ClientId=f321ec76-7e9c-44a0-a0ac-a5d0384c63fd&ClientSecret=e78dcd4b24614efb8bd312017f624052d2r2mZzg8tibflRfQ1W37PT6wgCyp8zrByAg8e5NW3k8I5UtjqCK5MSOivFGXB1A40Zrkvhv6IgDoVx0LSO95pvzfw7MZz0V');


    Http http = new Http();




    //Execute web service call here
    if (!Test.isRunningTest()) {
      HTTPResponse res = http.send(req);
      Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());

      DocumentURL = string.ValueOf(m.get('Href'));



    }




  }


  public string GetToken() {
    HttpRequest req = new HttpRequest();

    String url = 'https://na15.salesforce.com/services/Soap/u/18.0/' + UserInfo.getOrganizationId();
    if (ApexPages.currentPage() != null) {
      url = 'https://' + ApexPages.currentPage().getHeaders().get('Host') + '/services/Soap/u/18.0/' + UserInfo.getOrganizationId();
    }
    req.setHeader('Accept', 'application/json');
    req.setHeader('content-type', 'application/x-www-form-urlencoded');
    req.setEndpoint('https://authuat.springcm.com/oauth/201309/token/sfaccesstoken');
    req.setMethod('POST');
    // BELOW LINE is demo code, commented out
    //req.setBody('SFSession=' + UserInfo.getSessionId() + '|' + EncodingUtil.urlEncode(url, 'UTF-8') + '&ClientId=f321ec76-7e9c-44a0-a0ac-a5d0384c63fd&ClientSecret=e78dcd4b24614efb8bd312017f624052d2r2mZzg8tibflRfQ1W37PT6wgCyp8zrByAg8e5NW3k8I5UtjqCK5MSOivFGXB1A40Zrkvhv6IgDoVx0LSO95pvzfw7MZz0V');
    req.setBody('SFSession=' + UserInfo.getSessionId() + '|' + EncodingUtil.urlEncode(url, 'UTF-8') + '&ClientId=4bef1522-6190-42e1-89dc-dde0942010e9&ClientSecret=efafddd29c85485086edfc89f7594c12PuaLqr5BnePXubOqtDapWmoxDSBiOVoZyTmk6FMHNTPI7eNPSQHTY9mnFmHYpWwSz0jbznto0TcDWg0jEwQ8NVByq9eT7xb0');


    Http http = new Http();





    //Execute web service call here
    if (!Test.isRunningTest()) {
      HTTPResponse res = http.send(req);
      Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
      Token = string.ValueOf(m.get('AccessTokenValue'));



    } else {
      return 'test passed';
    }



    return '';
  }



}