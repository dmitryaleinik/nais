@isTest
private class FamilyMessagesEditControllerTest {

    private static User CreateData() {
        // create test data
        TestUtils.createAcademicYears();
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        
        Id portalProfileId = GlobalVariables.familyPortalProfileId;

        Account account1 = TestUtils.createAccount('individual', RecordTypes.individualAccountTypeId, 3, true);
        Contact parentA = TestUtils.createContact('Parent A', account1.Id, RecordTypes.parentContactTypeId, true);
        
        User u = TestUtils.createPortalUser('User 1', 'u1@test.org', 'u1', parentA.Id, portalProfileId, true, true);
        
        TestUtils.createPFS('PFS A', academicYearId, parentA.Id, true);
        
        TestUtils.createFederalTaxTable(academicYearId, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_SINGLE, true);

        TestUtils.createStateTaxTable(academicYearId, true);
        
        TestUtils.createEmploymentAllowance(academicYearId, true);
        
        TestUtils.createBusinessFarm(academicYearId, true);

        TestUtils.createRetirementAllowance(academicYearId, true);
        
        TestUtils.createAssetProgressivity(academicYearId, true);
        
        TestUtils.createIncomeProtectionAllowance(academicYearId, true);

        TestUtils.createExpectedContributionRate(academicYearId, true);

        TestUtils.createHousingIndexMultiplier(academicYearId, true);
        
        return u;
    }

    @isTest
    private static void TestLoad()
    {
        User u = FamilyMessagesEditControllerTest.CreateData();
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        PFS__c pfs = [SELECT Id, Name FROM PFS__c LIMIT 1];
        
        Test.startTest();
        
        System.runAs(u) {
            Case c = new Case(subject = 'Test', description = 'Test', contactId = u.ContactId, RecordTypeId = RecordTypes.schoolParentCommunicationCaseTypeId);
            insert c;
            
            PageReference pageRef = new ApexPages.Pagereference('/apex/FamilyMessagesContoller?id=' + c.Id + '&pfsid=' + pfs.Id + '&AcademicYearId=' + academicYearId);
            Test.setCurrentPage(pageRef);
            FamilyTemplateController template = new FamilyTemplateController();
            FamilyMessagesEditController controller = new FamilyMessagesEditController(template);
            
            System.assertNotEquals(template.Me, null);
            System.assertEquals(RecordTypes.parentCallCaseTypeId, controller.ParentCallRecordTypeId);
        }
        
        Test.stopTest();
    }
}