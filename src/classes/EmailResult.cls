/**
 * EmailResult.cls
 *
 * @description: Used for tracking the result of an Email message.
 *
 * @author: Chase Logan @ Presence PG
 */
public class EmailResult {

    /* properties, lazy loaded where applicable */
    public List<String> errorList {
        get { return ( this.errorList == null ? this.errorList = new List<String>() : this.errorList); }

        set {
            this.errorList = value;
        }
    }
    public Boolean isSuccess {
        get;

        set {
            this.isSuccess = value;
        }
    }
    
    // default ctor
    public EmailResult() {}

    // overloaded ctor to populate properties on instantiation
    public EmailResult( List<String> errList, Boolean success) {

        this.errorList = ( errList != null && errList.size() > 0 ? errList : this.errorList);
        this.isSuccess = ( success != null ? success : this.isSuccess);
    } 

}