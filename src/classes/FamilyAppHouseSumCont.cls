public class FamilyAppHouseSumCont extends FamilyAppCompController {
    
    public String delApplicantId { get; set; } 

    /*Action Methods*/
    
    public PageReference newApplicant(){
        PageReference returnTarget = Page.FamilyApplicationMain;
        returnTarget.getParameters().put('id', pfs.Id);
        returnTarget.getParameters().put('screen', 'ApplicantInformation');
        returnTarget.getParameters().put('addApp', '1');
        returnTarget.setRedirect(true);
        
        return returnTarget;
    }   
    
    public PageReference deleteApplicant(){

        // [DP] 03.25.2016 SFP-201 refactored below to make use of method in virtual class so logic is centralized (FamilyAppCompController)
        deleteApplicantAndUpdateRelatedPFS(delApplicantId);
                
        PageReference returnTarget = Page.FamilyApplicationMain;
        returnTarget.getParameters().put('id', pfs.Id);
        returnTarget.getParameters().put('screen', 'HouseholdSummary');
        returnTarget.setRedirect(true);
        
        return returnTarget;
    }   
    
    public PageReference newDependent(){
        // Update the number of Dependents on the PFS record
        if(pfs.Number_of_Dependent_Children__c < 7){
            pfs.Number_of_Dependent_Children__c = pfs.Number_of_Dependent_Children__c + 1;
        }
        Database.update(pfs);
        
        PageReference depPage = new PageReference(dependentsPageUrl);
        depPage.setRedirect(true); 
        return depPage;
    }   
    
    // Sets the fields of one of the Parents to blank and 
    //  returns to the Parents/Guardians screen
    public PageReference deleteParent(){
        
        if(parentDelete == 'Parent A'){
            clearParentA();
        }
        
        if(parentDelete == 'Parent B'){
            clearParentB();
        }
        
        if(parentDelete == 'Other Parent'){
            clearOtherParent();
        }
        
        update pfs;
        
        PageReference depPage = new PageReference(parentsPageUrl);
        depPage.setRedirect(true); 
        return depPage;     
    }
    
/*End Action Methods*/

/*Helper Methods*/
    private void clearParentA(){
        pfs.Parent_A_Prefix__c= null;
        pfs.Parent_A_First_Name__c= null;
        pfs.Parent_A_Middle_Name__c= null;
        pfs.Parent_A_Last_Name__c= null;
        pfs.Parent_A_Suffix__c= null;
        pfs.Parent_A_Birthdate__c= null;
        pfs.Parent_A_Gender__c= null;               
        pfs.Parent_A_Country__c= null;
        pfs.Parent_A_Address__c= null;
        pfs.Parent_A_City__c= null;
        pfs.Parent_A_State__c= null;
        pfs.Parent_A_ZIP_Postal_Code__c= null;
        pfs.Parent_A_Email__c= null;
        pfs.Parent_A_Preferred_Phone__c= null;
        pfs.Parent_A_Home_Phone__c= null;   
        pfs.Parent_A_Work_Phone__c= null;   
        pfs.Parent_A_Mobile_Phone__c= null;
        pfs.Parent_A_Occupation__c= null;
        pfs.Parent_A_Employer__c= null;
        pfs.Parent_A_Title__c= null;
        pfs.Parent_A_Years_Worked__c= null;
        pfs.Parent_A_Add_l_Job_Descrip__c= null;
    }
    
    private void clearParentB(){
        pfs.Parent_B_Prefix__c= null;
        pfs.Parent_B_First_Name__c= null;
        pfs.Parent_B_Middle_Name__c= null;
        pfs.Parent_B_Last_Name__c= null;
        pfs.Parent_B_Suffix__c= null;
        pfs.Parent_B_Birthdate__c= null;
        pfs.Parent_B_Gender__c= null;               
        pfs.Parent_B_Country__c= null;
        pfs.Parent_B_Address__c= null;
        pfs.Parent_B_City__c= null;
        pfs.Parent_B_State__c= null;
        pfs.Parent_B_ZIP_Postal_Code__c= null;
        pfs.Parent_B_Email__c= null;
        pfs.Parent_B_Preferred_Phone__c= null;
        pfs.Parent_B_Home_Phone__c= null;   
        pfs.Parent_B_Work_Phone__c= null;   
        pfs.Parent_B_Mobile_Phone__c= null;
        pfs.Parent_B_Occupation__c= null;
        pfs.Parent_B_Employer__c= null;
        pfs.Parent_B_Title__c= null;
        pfs.Parent_B_Years_Worked__c= null;
        pfs.Parent_B_Add_l_Job_Descrip__c= null;
    }
    
    private void clearOtherParent(){
        pfs.Addl_Parent_Relationship__c = null;
        pfs.Addl_Parent_Joint_custody__c = null;
        pfs.Year_of_divorce__c = null;
        pfs.Addl_Parent_First_Name__c = null;
        pfs.Addl_Parent_Middle_Initial__c = null;
        pfs.Addl_Parent_Last_Name__c = null;
        pfs.Addl_Parent_Suffix__c = null;               
        pfs.Addl_Parent_Country__c = null;
        pfs.Addl_Parent_Street_Address__c = null;
        pfs.Addl_Parent_City__c = null;
        pfs.Addl_Parent_State_Province__c = null;
        pfs.Addl_Parent_ZIP_Postal_Code__c = null;
        pfs.Add_l_Parent_Phone__c = null;
    }
    
    /*End Helper Methods*/
}