/**
 * FamilyHelpSearchControllerTest.cls
 *
 * @description: Test class for FamilyHelpSearchControllerTest.cls using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Mike Havrilla @ Presence PG
 */

@isTest (seeAllData = false)
private class FamilyHelpSearchControllerTest {

    static List<Knowledge__kav> articles;
    static User testUser;
    static User portalUser;
    static PFS__c pfs;
    static List<Academic_Year__c> academicYears;

    /* test data setup */
    static {
        testUser = CurrentUser.getCurrentUser();
        testUser.UserPermissionsKnowledgeUser = true;
        update testUser;

        portalUser = UserTestData.insertFamilyPortalUser();

        System.runAs( testUser) {
            pfs = PfsTestData.Instance.DefaultPfs;
            academicYears = AcademicYearTestData.Instance.insertAcademicYears(5);

            articles = KnowledgeAVTestData.Instance.insertKnowledgeAVs(14);

            // Create articles with the default category.
            List<Knowledge__DataCategorySelection> categories = new List<Knowledge__DataCategorySelection>();

            for (Knowledge__kav article : articles) {
                categories.add(KnowledgeDCSTestData.Instance.forParentId(article.Id).create());
            }

            articles = KnowledgeTestHelper.getKnowledgeArticleByIds(articles);

            KnowledgeTestHelper.publishArticles( articles);

        }
    }

    // test page action method w/ valid search string
    @isTest private static void initModelAndView_validSearchWithSearchString_successfulRedirectWithSearchString() {

        System.runAs( portalUser) {
            Test.startTest();
                FamilyHelpSearchController controller = new FamilyHelpSearchController();
                controller.pfsId = pfs.Id;
                controller.yearId = academicYears[0].Id;
                controller.searchString = 'Test';

                // now reload the page after a redirect.
                PageReference resultsPage = controller.onSubmit();
                Test.setCurrentPage( resultsPage);
                controller = new FamilyHelpSearchController();

            Test.stopTest();

            // Assert
            System.assertEquals( academicYears[0].Id, resultsPage.getParameters().get('AcademicYearId'), 'Academic Year Id\'s do not match');
            System.assertEquals( controller.searchString, resultsPage.getParameters().get('searchq'), 'Search Strings do not match');
            System.assertEquals( pfs.Id, resultsPage.getParameters().get('id'), 'Pfs Id\'s do not match');
        }
    }

    // test page action method w/ invalid search string
    @isTest private static void initModelAndView_searchStringIsNull_noRedirect() {

        System.runAs( portalUser) {
            Test.startTest();
                FamilyHelpSearchController controller = new FamilyHelpSearchController();
                controller.pfsId = pfs.Id;
                controller.yearId = academicYears[0].Id;

                PageReference resultsPage = controller.onSubmit();
            Test.stopTest();

            // Assert
            System.assertEquals( null, resultsPage, 'Page Reference should be null');
            System.assertEquals( null, controller.searchString, 'Search Strings should be null');
        }
    }

    // test page action method w/ invalid search string
    @isTest private static void initModelAndView_searchStringIsNotNull_ResultsReturned() {

        System.runAs( testUser) {
            Test.startTest();
                FamilyHelpSearchController controller = new FamilyHelpSearchController();
                controller.pfsId = pfs.Id;
                controller.yearId = academicYears[0].Id;

                List<Knowledge__kav> knowledgeArticles = FamilyHelpSearchController.searchKnowledgeArticles( 'Test');
            Test.stopTest();

            // Assert
            System.assertEquals( 0, knowledgeArticles.size(), 'Search Strings should be null' + knowledgeArticles.size());
        }
    }
}