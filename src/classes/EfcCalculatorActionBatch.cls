/*
* SPEC-140, NAIS-66
* Contains handler methods and static variables for processing related
*  to updates of Professional Judgement settings
*
* CH, Exponent Partners 04/30/2013
*
* [SL 5/6/2013] Moved from the original class SchoolProfJudgement
*/
public with sharing class EfcCalculatorActionBatch {
    
/* [SL] NAIS-502 THIS CODE IS NO LONGER IN USE. Replaced by scheduled batch job */
    
/*    
    
    //**** Trigger Handlers 
    // Runs EFC re-calculations in chunks of a size specified by 
    //  the Batch_Size_Settings__c custom settings
    // [SL 5/7/13] modified to pass in Ids as params
    public static List<String> runRevisionEFCRecalculations(Set<Id> schoolPfsAssignmentIds, boolean updateRecords){
        List<Id> schoolPfsAssignmentIdList = new List<Id>(schoolPfsAssignmentIds);
        Integer mainIterator = 0;
        List<String> processReport = new List<String>{};
        
        
        Integer batchSize = getEFCBatchSize();
        Integer numberOfBatches = 0;
        if (!schoolPfsAssignmentIdList.isEmpty()) {
            numberOfBatches = schoolPfsAssignmentIdList.size() / batchSize;
        }
        Integer maxBatchCount = getEFCMaxBatchCount();
        
        if ((maxBatchCount == null) || (numberOfBatches <= maxBatchCount)) {
    
            // Loop until we hit the end of the list
            while(mainIterator < schoolPfsAssignmentIdList.size()){
                Set<Id> idsToProcess = new Set<Id>{};
                
                // Iterate to create one batch of id values
                for(Integer i=0; i<batchSize; i++){
                    idsToProcess.add(schoolPfsAssignmentIdList[mainIterator]);
                    
                    mainIterator += 1;
                    // If this iteration is incomplete but we've hit
                    //  the end of the record list
                    if(mainIterator == schoolPfsAssignmentIdList.size()){
                        break;
                    }
                }
                
                runRevisionEFCCalculationBatch(idsToProcess, updateRecords);
                processReport.add('Processing request for chunk of ' + idsToProcess.size() + ' at ' + String.valueOf(DateTime.now()));
            }
        }
        else {
            // exceeded the max number of batches
            // TODO: possible future handling of large batches. For now, just leave the status as Recalculate.
        }
        
        return processReport;
    }
    
    public static List<String> runSssEFCRecalculations(Set<Id> pfsIds, boolean updateRecords){
        List<Id> pfsIdList = new List<Id>(pfsIds);
        Integer mainIterator = 0;
        List<String> processReport = new List<String>{};
        
        
        Integer batchSize = getEFCBatchSize();
        Integer numberOfBatches = 0;
        if (!pfsIdList.isEmpty()) {
            numberOfBatches = pfsIdList.size() / batchSize;
        }
        Integer maxBatchCount = getEFCMaxBatchCount();
        
        if ((maxBatchCount == null) || (numberOfBatches <= maxBatchCount)) {
    
            // Loop until we hit the end of the list
            while(mainIterator < pfsIdList.size()){
                Set<Id> idsToProcess = new Set<Id>{};
                
                // Iterate to create one batch of id values
                for(Integer i=0; i<batchSize; i++){
                    idsToProcess.add(pfsIdList[mainIterator]);
                    
                    mainIterator += 1;
                    // If this iteration is incomplete but we've hit
                    //  the end of the record list
                    if(mainIterator == pfsIdList.size()){
                        break;
                    }
                }
                
                runSssEFCCalculationBatch(idsToProcess, updateRecords);
                processReport.add('Processing request for chunk of ' + idsToProcess.size() + ' at ' + String.valueOf(DateTime.now()));
            }
        }
        else {
            // exceeded the max number of batches
            // TODO: possible future handling of large batches. For now, just leave the status as Recalculate.
        }
        
        return processReport;
    }
    //* End Trigger Handlers 
    
    //* Helper Methods 
    // Helper method for getting the current EFC batch size, with a built-in default
    private static Integer getEFCBatchSize(){
        if(Batch_Size_Settings__c.getValues('EFC') != null){
            return Integer.valueOf(Batch_Size_Settings__c.getValues('EFC').Batch_Size__c); 
        }
        else{
            return 1000;
        }
    }
    
    private static Integer getEFCMaxBatchCount() {
        if(Batch_Size_Settings__c.getValues('EFC') != null){
            return Integer.valueOf(Batch_Size_Settings__c.getValues('EFC').Max_Batch_Count__c); 
        }
        else{
            return 10;
        }
    }
    
    @future
    private static void runRevisionEFCCalculationBatch(Set<Id> idsToProcess, boolean updateRecords){        
           // Call EfcCalculator methods to re-calculate for the current batch 
        EfcCalculator efcCalc = new EfcCalculator();
        
        List<EfcWorksheetData> worksheets = EfcDataManager.createEfcWorksheetDataForSchoolPFSAssignments(idsToProcess);
        efcCalc.calculateEfc(worksheets);
        
        // update records
        if (updateRecords == true) {
            EfcDataManager.commitEfcResults(worksheets);        
        }
    }
    
    @future
    private static void runSssEFCCalculationBatch(Set<Id> idsToProcess, boolean updateRecords){        
           // Call EfcCalculator methods to re-calculate for the current batch 
        EfcCalculator efcCalc = new EfcCalculator();
        
        List<EfcWorksheetData> worksheets = EfcDataManager.createEfcWorksheetDataForPFSs(idsToProcess);
        efcCalc.calculateEfc(worksheets);
        
        // update records
        if (updateRecords == true) {
            EfcDataManager.commitEfcResults(worksheets);        
        }
        
    }
    //* End Helper Methods
    
    //* Unit Tests 

    @isTest
    private static void testRevisionBatchProcessing(){
        System.assertEquals(1000, getEFCBatchSize());
        
        // Prevent this logic from running on initial insert so we can monitor
        //  chunking records more effectively
        
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        //EfcCalculatorAction.isCalculatingEFC = true;
        
        
        TestData testData = new TestData(true);
        
        // Create 100 SPA records to test chunking
        List<School_PFS_Assignment__c> newSPARecords = new List<School_PFS_Assignment__c>{}; 
        for(Integer i=0; i<100; i++){
            newSPARecords.add(
                TestUtils.createSchoolPFSAssignment(testData.academicYearId, 
                                                    testData.applicant1A.Id, 
                                                    testData.school1.Id, 
                                                    testData.studentFolder1.Id, 
                                                    false));
        }
        
        Database.insert(newSPARecords);
        
        // get the ids
        Set<Id> newSPAIds = new Set<Id>();
        for (School_PFS_Assignment__c newSPARecord : newSPARecords) {
            newSPAIds.add(newSPARecord.Id);
        }
         
        // Turn the trigger logic back on for the test
        
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(false);
        //EfcCalculatorAction.isCalculatingEFC = false;
        
        Test.startTest();        
        
        List<String> processingResults = runRevisionEFCRecalculations(newSPAIds, true);
        System.assertEquals(1, processingResults.size());
        
        Batch_Size_Settings__c newBatchSize = new Batch_Size_Settings__c(Name='EFC', Batch_Size__c = 20);
        Database.insert(newBatchSize);
        
        // Check that when a chunk size is specified it processes
        //  the records in chucks that are the specified size
        System.assertEquals(20, getEFCBatchSize());
        
        processingResults = runRevisionEFCRecalculations(newSPAIds, true);
        System.assertEquals(5, processingResults.size());    
        
        Test.stopTest();    
    }

    @isTest
    private static void testRevisionMaxBatchCount(){
        
        // Prevent this logic from running on initial insert so we can monitor
        //  chunking records more effectively
        
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        //EfcCalculatorAction.isCalculatingEFC = true;
        
        
        TestData testData = new TestData(true);
        
        // Create 10 SPA records to test chunking
        List<School_PFS_Assignment__c> newSPARecords = new List<School_PFS_Assignment__c>{}; 
        for(Integer i=0; i<10; i++){
            newSPARecords.add(
                TestUtils.createSchoolPFSAssignment(testData.academicYearId, 
                                                    testData.applicant1A.Id, 
                                                    testData.school1.Id, 
                                                    testData.studentFolder1.Id, 
                                                    false));
        }
        
        Database.insert(newSPARecords);
        
        // get the ids
        Set<Id> newSPAIds = new Set<Id>();
        for (School_PFS_Assignment__c newSPARecord : newSPARecords) {
            newSPAIds.add(newSPARecord.Id);
        }
         
        // Turn the trigger logic back on for the test
        
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(false);
        //EfcCalculatorAction.isCalculatingEFC = false;
        
        Test.startTest();    
        
        List<String> processingResults = runRevisionEFCRecalculations(newSPAIds, true);
        System.assertEquals(1, processingResults.size());
        
        Batch_Size_Settings__c newBatchSize = new Batch_Size_Settings__c(Name='EFC', Batch_Size__c = 2, Max_Batch_Count__c=3);
        Database.insert(newBatchSize);
        
        // Since there are 10 records and a batch size of 2, we should be exceeding the Max Batch Count of 3, so shouldn't do any processing
        System.assertEquals(2, getEFCBatchSize());
        
        processingResults = runRevisionEFCRecalculations(newSPAIds, true);
        System.assertEquals(0, processingResults.size());        
        
        Test.stopTest();    
    }

    @isTest
    private static void testSssBatchProcessing(){
        System.assertEquals(1000, getEFCBatchSize());
        
        // Prevent this logic from running on initial insert so we can monitor
        //  chunking records more effectively
        
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
        //EfcCalculatorAction.isCalculatingEFC = true;
        
        
        TestData testData = new TestData(true);
        
        // Create 100 PFS records to test chunking
        List<PFS__c> newPFSRecords = new List<PFS__c>{}; 
        for(Integer i=0; i<100; i++){
            newPFSRecords.add(
                TestUtils.createPFS('Test PFS', testData.academicYearId, testData.parentA.Id, false));
        }
        
        Database.insert(newPFSRecords);
        
        // get the ids
        Set<Id> newPFSIds = new Set<Id>();
        for (PFS__c newPFSRecord : newPFSRecords) {
            newPFSIds.add(newPFSRecord.Id);
        }
         
        // Turn the trigger logic back on for the test
        
        EfcCalculatorAction.setIsDisabledSssAutoCalc(false);
        //EfcCalculatorAction.isCalculatingEFC = false;
        
        Test.startTest();    
        
        List<String> processingResults = runSssEFCRecalculations(newPFSIds, true);
        System.assertEquals(1, processingResults.size());
        
        Batch_Size_Settings__c newBatchSize = new Batch_Size_Settings__c(Name='EFC', Batch_Size__c = 20);
        Database.insert(newBatchSize);
        
        // Check that when a chunk size is specified it processes
        //  the records in chucks that are the specified size
        System.assertEquals(20, getEFCBatchSize());
        
        processingResults = runRevisionEFCRecalculations(newPFSIds, true);
        System.assertEquals(5, processingResults.size());    
        
        Test.stopTest();        
    }

    @isTest
    private static void testSssMaxBatchCount(){
        
        // Prevent this logic from running on initial insert so we can monitor
        //  chunking records more effectively
        
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
        //EfcCalculatorAction.isCalculatingEFC = true;
        
        
        TestData testData = new TestData(true);
        
        // Create 10 SPA records to test chunking
        List<PFS__c> newPFSRecords = new List<PFS__c>{}; 
        for(Integer i=0; i<10; i++){
            newPFSRecords.add(
                TestUtils.createPFS('Test PFS', testData.academicYearId, testData.parentA.Id, false));
        }
        
        Database.insert(newPFSRecords);
        
        // get the ids
        Set<Id> newPFSIds = new Set<Id>();
        for (PFS__c newPFSRecord : newPFSRecords) {
            newPFSIds.add(newPFSRecord.Id);
        }
         
        // Turn the trigger logic back on for the test
        
        EfcCalculatorAction.setIsDisabledSssAutoCalc(false);
        //EfcCalculatorAction.isCalculatingEFC = false;
        
        Test.startTest();    
        
        List<String> processingResults = runRevisionEFCRecalculations(newPFSIds, true);
        System.assertEquals(1, processingResults.size());
        
        Batch_Size_Settings__c newBatchSize = new Batch_Size_Settings__c(Name='EFC', Batch_Size__c = 2, Max_Batch_Count__c=3);
        Database.insert(newBatchSize);
        
        // Since there are 10 records and a batch size of 2, we should be exceeding the Max Batch Count of 3, so shouldn't do any processing
        System.assertEquals(2, getEFCBatchSize());
        
        processingResults = runSssEFCRecalculations(newPFSIds, true);
        System.assertEquals(0, processingResults.size());    
        
        Test.stopTest();        
    }

    //* End Unit Tests 
    
    */
}