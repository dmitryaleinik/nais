/**
 * @description Query selector for CronTrigger records.
 */
public class CronTriggerSelector extends fflib_SObjectSelector
{

    @testVisible private static final String CRON_TRIGGER_IDS_PARAM  = 'cronTriggerIds';
    @testVisible private static final String CRON_JOB_DETAIL_NAMES_PARAM  = 'cronJobDetailNames';

    private List<Schema.SObjectField> getSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
                CronTrigger.Id,
                CronTrigger.CronExpression
            };
    }

    private Schema.SObjectType getSObjectType()
    {
        return CronTrigger.sObjectType;
    }

    public List<CronTrigger> selectById(Set<ID> idSet)
    {
        ArgumentNullException.throwIfNull(idSet, CRON_TRIGGER_IDS_PARAM);
        assertIsAccessible();

        return (List<CronTrigger>) selectSObjectsById(idSet);
    }

    public List<CronTrigger> selectByCronJobDetailName(List<String> cronJobDetailNames)
    {
        ArgumentNullException.throwIfNull(cronJobDetailNames, CRON_JOB_DETAIL_NAMES_PARAM);
        assertIsAccessible();

        return Database.query(String.format('SELECT {0} FROM CronTrigger WHERE CronJobDetail.Name IN :cronJobDetailNames',
            new List<String> {getFieldListString()}));
    }

    /**
     * @description Singleton property ensuring external sources do not
     *              instantiate this directly.
     */
    public static CronTriggerSelector Instance {
        get {
            if (Instance == null) {
                Instance = new CronTriggerSelector();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to enforce singleton access
     *              via the Instance property.
     */
    private CronTriggerSelector() {}
}