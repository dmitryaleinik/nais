@isTest
private class SchoolRequestMIEBulkControllerTest {
    
    private static Academic_Year__c academicYear;
    private static Account schoolMIE1, schoolMIE2, schoolMIE3;
    private static Applicant__c applicant1, applicant2, applicant3;
    private static Annual_Setting__c annualSettingMIE1, annualSettingMIE2, annualSettingMIE3;
    private static PFS__c pfsMIE1, pfsMIE2, pfsMIE3;
    private static Student_Folder__c folder1, folder2, folder3;
    private static Contact parent1, parent2, parent3, student1, student2, student3;
    private static User schoolPortalUserMIE;
    private static School_PFS_Assignment__c spa1, spa2, spa3;

    private static void Setup(Boolean runSharing, Academic_Year__c academicYearToUse) {
        //Create current academic year.
        System.assertNotEquals(null, academicYearToUse, 'The test must specify its own academic year record to use.');
        System.assertNotEquals(null, academicYearToUse.Id, 'The test must have an academic year record with a valid Id.');
        academicYear = academicYearToUse;

        //Create test Schools.
        schoolMIE1 = AccountTestData.Instance.asSchool().forName('schoolMIE1').create();
        schoolMIE2 = AccountTestData.Instance.asSchool().forName('schoolMIE2').create();
        schoolMIE3 = AccountTestData.Instance.asSchool().forName('schoolMIE3').create();
        Account aParent1 = AccountTestData.Instance.asFamily().create();
        Account aParent2 = AccountTestData.Instance.asFamily().create();
        Account aParent3 = AccountTestData.Instance.asFamily().create();
        insert new List<Account>{schoolMIE1, schoolMIE2, schoolMIE3, aParent1, aParent2, aParent3};

        //Create Annual Setting records for each school.
        annualSettingMIE1 = AnnualSettingsTestData.Instance
                .forSchoolId(schoolMIE1.Id)
                .forAcademicYearId(academicYear.Id)
                .create();
        annualSettingMIE2 = AnnualSettingsTestData.Instance
                .forSchoolId(schoolMIE2.Id)
                .forAcademicYearId(academicYear.Id)
                .create();
        annualSettingMIE3 = AnnualSettingsTestData.Instance
                .forSchoolId(schoolMIE3.Id)
                .forAcademicYearId(academicYear.Id)
                .create();
        insert new List<Annual_Setting__c>{annualSettingMIE1, annualSettingMIE2, annualSettingMIE3};

        //Create 3 parents and 3 students.
        parent1 = ContactTestData.Instance
                .asParent()
                .forAccount(aParent1.Id)
                .forEmail('aParent1@test.com')
                .forFirstName('aParent1')
                .forLastName('aParent1')
                .create();
        parent2 = ContactTestData.Instance.asParent()
                .asParent()
                .forAccount(aParent2.Id)
                .forEmail('aParent2@test.com')
                .forFirstName('aParent2')
                .forLastName('aParent2')
                .create();
        parent3 = ContactTestData.Instance.asParent()
                .asParent()
                .forAccount(aParent3.Id)
                .forEmail('aParent3@test.com')
                .forFirstName('aParent3')
                .forLastName('aParent3')
                .create();
        student1 = ContactTestData.Instance.asStudent().create();
        student2 = ContactTestData.Instance.asStudent().create();
        student3 = ContactTestData.Instance.asStudent().create();
        insert new List<Contact>{parent1, parent2, parent3, student1, student2, student3};

        //Create 3 PFS records.
        pfsMIE1 = PfsTestData.Instance
                .forParentA(parent1.Id)
                .forAcademicYearPicklist(academicYear.Name)
                .asPaid()
                .create();
        pfsMIE2 = PfsTestData.Instance
                .forParentA(parent2.Id)
                .forAcademicYearPicklist(academicYear.Name)
                .asPaid()
                .create();
        pfsMIE3 = PfsTestData.Instance
                .forParentA(parent3.Id)
                .forAcademicYearPicklist(academicYear.Name)
                .asPaid()
                .create();
        insert new List<PFS__c>{pfsMIE1, pfsMIE2, pfsMIE3};

        //Create 3 applicants.
        applicant1 = ApplicantTestData.Instance
                .forContactId(student1.Id)
                .forPfsId(pfsMIE1.Id)
                .create();
        applicant2 = ApplicantTestData.Instance
                .forContactId(student2.Id)
                .forPfsId(pfsMIE2.Id)
                .create();
        applicant3 = ApplicantTestData.Instance
                .forContactId(student3.Id)
                .forPfsId(pfsMIE3.Id)
                .create();
        insert new List<Applicant__c>{applicant1, applicant2, applicant3};

        //Create Folders
        folder1 = StudentFolderTestData.Instance.forName('Folder Applicant1')
                .forAcademicYearPicklist(academicYear.Name)
                .forStudentId(student1.Id)
                .forSchoolId(schoolMIE1.Id)
                .create();
        folder2 = StudentFolderTestData.Instance.forName('Folder Applicant2')
                .forAcademicYearPicklist(academicYear.Name)
                .forStudentId(student2.Id)
                .forSchoolId(schoolMIE2.Id)
                .create();
        folder3 = StudentFolderTestData.Instance.forName('Folder Applicant3')
                .forAcademicYearPicklist(academicYear.Name)
                .forStudentId(student3.Id)
                .forSchoolId(schoolMIE3.Id)
                .create();
        insert new List<Student_Folder__c>{folder1, folder2, folder3};

        //Create 3 SPA records (1 for each applicant).
        spa1 = SchoolPfsAssignmentTestData.Instance
                .forAcademicYearPicklist(academicYear.Name)
                .forApplicantId(applicant1.Id)
                .forSchoolId(schoolMIE1.Id)
                .forStudentFolderId(folder1.Id)
                .create();
        spa2 = SchoolPfsAssignmentTestData.Instance
                .forAcademicYearPicklist(academicYear.Name)
                .forApplicantId(applicant2.Id)
                .forSchoolId(schoolMIE2.Id)
                .forStudentFolderId(folder2.Id)
                .create();
        spa3 = SchoolPfsAssignmentTestData.Instance
                .forAcademicYearPicklist(academicYear.Name)
                .forApplicantId(applicant3.Id)
                .forSchoolId(schoolMIE3.Id)
                .forStudentFolderId(folder3.Id)
                .create();
        insert new List<School_PFS_Assignment__c>{spa1, spa2, spa3};

        if (runSharing) {

            setupSharing(schoolMIE1.Id);
        }
    }

    private static void Setup(Boolean runSharing) {
        // Use default academic year
        Setup(runSharing, AcademicYearTestData.Instance.DefaultAcademicYear);
    }//End:Setup
    
    private static void setupSharing(Id schoolId) {
        
        Contact staff = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(schoolId)
            .insertContact();
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            
            schoolPortalUserMIE = UserTestData.Instance
                .forUsername('SchoolPortalUser@test.com')
                .forContactId(staff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId)
                .insertUser();
                
            Test.startTest();                
                // call scheduled batch Apex to process new access pfs and folders.
                Database.executeBatch(new SchoolStaffShareBatch());
            Test.stopTest();
        }
    }//End:setupSharing
    
    @isTest
    private static void startProcess_pfsWithoutMIERequest_expectRequestMIEForAllPFS() {
        
        //1. Setup test data.
        Setup(false);
        
        //2. Set all spas in to the same school.
        spa2.School__c = schoolMIE1.Id;
        spa3.School__c = schoolMIE1.Id;
        update new List<School_PFS_Assignment__c>{spa2, spa3};
        
        //3. Run sharing for the existing pfs and folders.
        setupSharing(schoolMIE1.Id);
        
        //4. Activate the MIE for the entired site.
        SchoolPortalSettings.MIE_Pilot = true;
        
        //5. Let's run the test as school portal user of schoolMIE1.
        System.runAs(schoolPortalUserMIE){
            
            System.AssertEquals(3, ([SELECT Id FROM School_PFS_Assignment__c]).size());
            
            //6. Create a handler of the class that we are going to test.
            List<Student_Folder__c> folders = new List<Student_Folder__c>{folder1, folder2, folder3};
            ApexPages.StandardSetController standardSet = new ApexPages.StandardSetController(folders);
            standardSet.setSelected(folders);
            SchoolRequestMIEBulkController handler = new SchoolRequestMIEBulkController(standardSet);
            
            handler.startProcess();
            
            System.AssertEquals(true, String.isBlank(handler.response.errorMessage));
            System.AssertEquals(true, handler.processHasStarted);
            System.AssertEquals(3, handler.response.pfsRequestedCounter);
            System.AssertEquals(3, handler.response.pfsUpdatedCounter);
            System.AssertEquals(0, handler.response.pfsAlreadyCompletedCounter);
            System.AssertEquals(0, handler.response.pfsRequestedAndIncompletedCounter);
            System.AssertEquals(0, handler.response.pfsClosedFamilyPortalCounter);
        }
        
    }//End:startProcess_pfsWithoutMIERequest_expectRequestMIEForAllPFS
    
    @isTest
    private static void startProcess_allPfsWithMIERequest_expectDoNotRequestMIEForAllPFS() {
        
        //1. Setup test data.
        Setup(false);
        
        //2. Enable MIE for school1 and school2.
        annualSettingMIE1.Collect_MIE__c = 'Yes';
        annualSettingMIE2.Collect_MIE__c = 'Yes';
        update new List<Annual_Setting__c>{annualSettingMIE1, annualSettingMIE2}; 
        
        //3. Create folder for student2 to school1. This means that student2 has spas for school1 and school2.
        //   Create folder for student3 to school1. This means that student3 has spas for school1 and school3.
        Student_Folder__c folder4 = StudentFolderTestData.Instance.forName('Folder Applicant2-School1')
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student2.Id)
            .forSchoolId(schoolMIE1.Id)
            .create();
        Student_Folder__c folder5 = StudentFolderTestData.Instance.forName('Folder Applicant3-School1')
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student3.Id)
            .forSchoolId(schoolMIE1.Id)
            .create();
        insert new List<Student_Folder__c>{folder4, folder5};
        
        //4. Create spas for student2-school1 and student3-school1.
        School_PFS_Assignment__c spa4 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forApplicantId(applicant2.Id)
            .forSchoolId(schoolMIE1.Id)
            .forStudentFolderId(folder4.Id)
            .create();
        School_PFS_Assignment__c spa5 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forApplicantId(applicant3.Id)
            .forSchoolId(schoolMIE1.Id)
            .forStudentFolderId(folder5.Id)
            .create();
        insert new List<School_PFS_Assignment__c>{spa4, spa5};
        
        //5. Set Individual MIE for pfsMIE3.
        //   Mark MIE as completed for PfsMIE1.
        pfsMIE3.MIE_Requested__c = true;
        pfsMIE3.MIE_Requested_By_School_Id__c = schoolMIE2.Id;
        pfsMIE3.MIE_Requested_By_School_Name__c = 'School 2';
        pfsMIE3.MIE_Requested_Date__c = System.today();
        pfsMIE1.StatMonthlyIncomeExpenses__c = true;
        pfsMIE1.StatMonthlyIncomeExpensesMainBoolean__c = true;
        update new List<PFS__c>{pfsMIE1, pfsMIE3};
        
        //6. Run sharing for the existing pfs and folders.
        setupSharing(schoolMIE1.Id);
        
        //7. Activate the MIE for the entired site.
        SchoolPortalSettings.MIE_Pilot = true;
        
        //5. Let's run the test as school portal user of schoolMIE1.
        System.runAs(schoolPortalUserMIE){
            
            Map<Id, Student_Folder__c> studentFolders = new Map<Id, Student_Folder__c>([SELECT Id FROM Student_Folder__c]);
            System.AssertEquals(true, studentFolders.containsKey(folder1.Id));
            System.AssertEquals(true, studentFolders.containsKey(folder4.Id));
            System.AssertEquals(true, studentFolders.containsKey(folder5.Id));
            
            //6. Create a handler of the class that we are going to test.
            List<Student_Folder__c> folders = new List<Student_Folder__c>{folder1, folder4, folder5};
            ApexPages.StandardSetController standardSet = new ApexPages.StandardSetController(folders);
            standardSet.setSelected(folders);
            SchoolRequestMIEBulkController handler = new SchoolRequestMIEBulkController(standardSet);
            
            handler.startProcess();
            
            System.AssertEquals(false, handler.isPopup);
            
            System.AssertEquals(true, String.isBlank(handler.response.errorMessage));
            System.AssertEquals(true, handler.processHasStarted);
            
            System.AssertEquals(3, handler.response.pfsRequestedCounter);
            System.AssertEquals(true, handler.response.pfsRequestedIds.contains(pfsMIE1.Id));
            System.AssertEquals(true, handler.response.pfsRequestedIds.contains(pfsMIE2.Id));
            System.AssertEquals(true, handler.response.pfsRequestedIds.contains(pfsMIE3.Id));
            
            System.AssertEquals(0, handler.response.pfsUpdatedCounter);
            
            System.AssertEquals(2, handler.response.pfsRequestedAndIncompletedCounter);
            System.AssertEquals(true, handler.response.pfsRequestedAndIncompletedIds.contains(pfsMIE2.Id));
            System.AssertEquals(true, handler.response.pfsRequestedAndIncompletedIds.contains(pfsMIE3.Id));
            
            System.AssertEquals(0, handler.response.pfsClosedFamilyPortalCounter);
            
            System.AssertEquals(1, handler.response.pfsAlreadyCompletedCounter);
            System.AssertEquals(true, handler.response.pfsAlreadyCompletedIds.contains(pfsMIE1.Id));
            
            handler.redirectAdvancedList();
        }
        
    }//End:startProcess_pfsWithMIERequest_expectDoNotRequestMIEForAllPFS

    @isTest
    private static void startProcess_academicYearIsClosed_expectNoMIERequests() {
        // Insert an academic year that is closed to families.
        // Use the default academic year property so that it is referenced by other test data classes.
        Academic_Year__c testAcademicYear = AcademicYearTestData.Instance
                .forName('2018-2019')
                .forFamilyPortalStartDate(Date.today().addDays(-20))
                .forFamilyPortalEndDate(Date.today().addDays(-10))
                .DefaultAcademicYear;

        //1. Setup test data.
        Setup(false, testAcademicYear);

        //2. Set all spas in to the same school.
        spa2.School__c = schoolMIE1.Id;
        spa3.School__c = schoolMIE1.Id;
        update new List<School_PFS_Assignment__c>{spa2, spa3};

        //3. Run sharing for the existing pfs and folders.
        setupSharing(schoolMIE1.Id);

        //4. Activate the MIE for the entired site.
        SchoolPortalSettings.MIE_Pilot = true;

        //5. Let's run the test as school portal user of schoolMIE1.
        System.runAs(schoolPortalUserMIE){

            System.AssertEquals(3, ([SELECT Id FROM School_PFS_Assignment__c]).size());

            //6. Create a handler of the class that we are going to test.
            List<Student_Folder__c> folders = new List<Student_Folder__c>{folder1, folder2, folder3};
            ApexPages.StandardSetController standardSet = new ApexPages.StandardSetController(folders);
            standardSet.setSelected(folders);
            SchoolRequestMIEBulkController handler = new SchoolRequestMIEBulkController(standardSet);

            handler.startProcess();

            System.AssertEquals(true, String.isBlank(handler.response.errorMessage));
            System.AssertEquals(true, handler.processHasStarted);
            System.AssertEquals(3, handler.response.pfsRequestedCounter, 'Expected the request to be for all the PFSs.');
            System.AssertEquals(0, handler.response.pfsUpdatedCounter, 'Expected 0 PFSs to be updated.');
            System.AssertEquals(0, handler.response.pfsAlreadyCompletedCounter, 'Expected 0 PFSs to have already completed the MIE.');
            System.AssertEquals(0, handler.response.pfsRequestedAndIncompletedCounter, 'Expected 0 PFSs to already have MIE requested.');
            System.AssertEquals(3, handler.response.pfsClosedFamilyPortalCounter, 'Expected all the PFSs to be for closed academic years.');
        }
    }
}