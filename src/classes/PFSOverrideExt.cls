public with sharing class PFSOverrideExt {
    
    public final PFS__c pfsRecord { get; set; }
    private User currentUser {get; set;}
    
    public PFSOverrideExt(ApexPages.StandardController stdController){
        pfsRecord = (PFS__c)stdController.getRecord();
        
        currentUser = [Select Id, ContactId, Contact.AccountId from User where Id = :UserInfo.getUserId()];
    }
    
    public PageReference redirect(){
        if(currentUser.ContactId == null){
            return new PageReference('/' + pfsRecord.Id + '?nooverride=true');
        }
        else{
            List<School_PFS_Assignment__c> spaList = [Select Id, School__c, Applicant__r.PFS__c 
                                                        FROM School_PFS_Assignment__c 
                                                        WHERE Applicant__r.PFS__c = :pfsRecord.Id 
                                                        AND School__c = :currentUser.Contact.AccountId];

            String spaId = spaList.isEmpty() ? null : spaList[0].Id;

            PageReference redirectPage;

            if (spaId == null){
                redirectPage = Page.SchoolDashboard;
            } else {
                redirectPage = Page.SchoolPFSSummary;
                redirectPage.getParameters().put('schoolpfsassignmentid', spaId);
            }
            return redirectPage;
        }
    }
}