/*
* SPEC-121
*
* The school should be able to see all answers provided by the families on the main application screens
* This is the Household Information link under the PFS as well as the Additional Information link under the PFS. 
* The idea is to show all fields which have not fed into the EFC, but are asked on the PFS so that the school 
* can see everything without needing to go into the printable PFS to see the information.
*
* Nathan, Exponent Partners, 2013
*/
@isTest
private class SchoolHouseholdInformationControllerTest {
    
    private class TestData {
        Account school1;
        Contact staff1, parentA, parentB, student1, student2;
        String academicYearId;
        PFS__c pfs1;
        Applicant__c applicant1A;
        Student_Folder__c studentFolder1;
        School_PFS_Assignment__c spfsa1;
        Dependents__c dependents1;
        
        private TestData() {
            // school
            school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, null, false); 
            insert school1;       
            
            // school staff
            staff1 = TestUtils.createContact('Staff 1', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
            insert staff1;
            
            // parents and students
            parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
            parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
            student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
            student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
            insert new List<Contact> {parentA, parentB, student1, student2};
            
            // academic year
            TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
            academicYearId = GlobalVariables.getCurrentAcademicYear().Id;  
            
            // psf
            pfs1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
            pfs1.Number_of_Dependent_Children__c = 2; //NAIS-2093
            insert new List<PFS__c> {pfs1};
            
            // applicant
            applicant1A = TestUtils.createApplicant(student1.Id, pfs1.Id, false);
            insert new List<Applicant__c> {applicant1A};
            
            // student folder
            studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
            insert studentFolder1;
            
            // school pfs assignment
            spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder1.Id, false);
            insert spfsa1;
            
            // dependents
            dependents1 = new Dependents__c(PFS__c = pfs1.Id, 
            Dependent_1_School_Next_Year__c='schoolnext', 
            Dependent_1_Gender__c='female', 
            Dependent_1_Full_Name__c='full name1', 
            Dependent_1_Current_School__c='sacajawea', 
            Dependent_1_Current_Grade__c='5', 
            Dependent_1_Birthdate__c=System.today(), 
            Dependent_2_School_Next_Year__c='schoolnext', 
            Dependent_2_Gender__c='female', 
            Dependent_2_Full_Name__c='full name2', 
            Dependent_2_Current_School__c='sacajawea', 
            Dependent_2_Current_Grade__c='5', 
            Dependent_2_Birthdate__c=System.today());
            insert dependents1;  
        }
    }
    
    @isTest private static void testMemberCount() {
        TestData td = new TestData();
        
        Test.startTest();
        PageReference infoPage = Page.SchoolHouseholdInformation;
        infoPage.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(infoPage);
        SchoolHouseholdInformationController controller = new SchoolHouseholdInformationController();
        
        // 4 members expected. student, parent A and 2 dependents
        system.assertEquals(4, controller.householdMemberList.size());
        
        Test.stopTest();    
    }    

    @isTest private static void testEditStudentDetail() {
        TestData td = new TestData();
        
        Test.startTest();
        PageReference infoPage = Page.SchoolHouseholdInformation;
        infoPage.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(infoPage);
        SchoolHouseholdInformationController controller = new SchoolHouseholdInformationController();
        
        // test if records loaded as per url param
        system.assertEquals(controller.applicant.Id, td.applicant1A.Id);
        system.assertEquals(true, controller.applicant.Id!=null);
        
        // test save method
        controller.editStudentDetail();
        controller.folder.Student_ID__c = 'test123';
        controller.saveStudentDetail();
        system.assertEquals('test123', [select Student_ID__c 
                                          from Student_Folder__c 
                                          where Id = :controller.folder.Id].Student_ID__c);
        
        // test cancel method
        controller.editStudentDetail();
        controller.folder.Student_ID__c = 'test456';
        controller.cancelStudentDetail();
        system.assertEquals('test123', [select Student_ID__c from Student_Folder__c where Id = :controller.folder.Id].Student_ID__c);
        Test.stopTest();
    }    

    @isTest private static void testAddHouseholdMember() {
        TestData td = new TestData();
        
        Test.startTest();
        PageReference infoPage = Page.SchoolHouseholdInformation;
        infoPage.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(infoPage);
        SchoolHouseholdInformationController controller = new SchoolHouseholdInformationController();
        
        // simply calling add page. testing is done in target page test class
        controller.addNewHouseholdMember();
        Test.stopTest();
    }

    @isTest private static void testEditHouseholdMember() {
        TestData td = new TestData();
        
        Test.startTest();
        PageReference infoPage = Page.SchoolHouseholdInformation;
        infoPage.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(infoPage);
        SchoolHouseholdInformationController controller = new SchoolHouseholdInformationController();
        
        // simply calling edit page. testing is done in target page test class
        controller.editHouseholdMember();
        Test.stopTest();
    }

    @isTest private static void testDeleteDependent() {
        TestData td = new TestData();
        
        Test.startTest();
        PageReference infoPage = Page.SchoolHouseholdInformation;
        infoPage.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(infoPage);
        SchoolHouseholdInformationController controller = new SchoolHouseholdInformationController();
        
        // 4 members expected. student, parent A and 2 dependents
        system.assertEquals(4, controller.householdMemberList.size());
        
        infoPage.getParameters().put('DependentsId', td.dependents1.Id);
        infoPage.getParameters().put('DependentNumber', '1');
        controller.deleteHouseholdMember();
        
        // verify that dependent 1 data is cleared. we are not deleting records since it contains 10 dependents
        system.assertEquals('full name2', [select Dependent_1_Full_Name__c 
                                              from Dependents__c 
                                              where Id = :td.dependents1.Id].Dependent_1_Full_Name__c); //NAIS-2093
        system.assertEquals(3, controller.householdMemberList.size());
        
        Test.stopTest();
    }    

    @isTest private static void testDeleteAdditionalParent() {
        TestData td = new TestData();
        
        Test.startTest();
        
        // first create additional parent
        td.pfs1.Addl_Parent_Last_Name__c = 'test additional parent';
        update td.pfs1;
        
        PageReference infoPage = Page.SchoolHouseholdInformation;
        infoPage.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(infoPage);
        SchoolHouseholdInformationController controller = new SchoolHouseholdInformationController();
        
        // 5 members expected. student, parent A and 2 dependents, additional parent
        system.assertEquals(5, controller.householdMemberList.size());
        
        infoPage.getParameters().put('relationship', 'Additional Parent');
        controller.deleteHouseholdMember();
        
        // verify that additional parent data is cleared. we are not deleting records since it part of pfs
        system.assertEquals(null, [select Addl_Parent_Last_Name__c 
                                      from PFS__C 
                                      where Id = :td.pfs1.Id].Addl_Parent_Last_Name__c);
        system.assertEquals(4, controller.householdMemberList.size());
        
        Test.stopTest();
    }

    @isTest private static void testDeleteParentB() {
        TestData td = new TestData();
        
        Test.startTest();
        
        // first set Parent B
        td.pfs1.Parent_B__c = td.parentB.Id;
        update td.pfs1;
        
        PageReference infoPage = Page.SchoolHouseholdInformation;
        infoPage.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(infoPage);
        SchoolHouseholdInformationController controller = new SchoolHouseholdInformationController();
        
        // 5 members expected. student, parent A and 2 dependents, parent B
        system.assertEquals(5, controller.householdMemberList.size());
        
        infoPage.getParameters().put('HouseholdMemberId', td.parentB.Id);
        infoPage.getParameters().put('relationship', 'Parent B');
        controller.deleteHouseholdMember();
        
        // verify that parent B data is cleared. we are deleting contact record
        system.assertEquals(null, [select Parent_B__c from PFS__C where Id = :td.pfs1.Id].Parent_B__c);
        system.assertEquals(4, controller.householdMemberList.size());
        
        Test.stopTest();
    }

    @isTest private static void testAddSibling() {
        TestData td = new TestData();
        
        Test.startTest();
        
        // first add sibling for student 2
        
        // applicant
        Applicant__c applicant2A = TestUtils.createApplicant(td.student2.Id, td.pfs1.Id, false);
        insert applicant2A;
        
        // student folder
        Student_Folder__c studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', td.academicYearId, td.student2.Id, false);
        insert studentFolder2;
        
        // school pfs assignment
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(td.academicYearId, applicant2A.Id, td.school1.Id, studentFolder2.Id, false);
        insert spfsa2;
        
        PageReference infoPage = Page.SchoolHouseholdInformation;
        infoPage.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(infoPage);
        SchoolHouseholdInformationController controller = new SchoolHouseholdInformationController();
        
        // 5 members expected. student, parent A and 2 dependents, sibling
        system.assertEquals(5, controller.householdMemberList.size());
        
        Test.stopTest();
    }
    
    @isTest private static void saveStudentDetail_applicantEdited_ApplicantChangesSavedPFSRemainsTheSame() {
        
        //1. Create test data
        TestData info = new TestData();
        
        Test.startTest();
        
            //2. Create an instance of the controller to be tested
            PageReference testPage = Page.SchoolHouseholdInformation;
	        testPage.getParameters().put('SchoolPFSAssignmentId', info.spfsa1.Id);
	        Test.setCurrentPage(testPage);
	        SchoolHouseholdInformationController controller = new SchoolHouseholdInformationController();
            
            //3. Retrieve the lastModificatedDate for PFS and Applicant records.
            Applicant__c oldApplicant = [SELECT Id, LastModifiedDate, PFS__r.LastModifiedDate FROM Applicant__c WHERE Id=:controller.applicant.Id LIMIT 1];
            Datetime LastModifiedDatePFS = oldApplicant.PFS__r.LastModifiedDate;
            Datetime LastModifiedDateApplicant = oldApplicant.LastModifiedDate;
            
            //4. Add changes to the Applicant Record.
            controller.applicant.Current_School__c = 'This is a test';
            System.AssertEquals(true, controller.applicantHasChanged());
            System.AssertEquals(false, controller.applicantPFSHasChanged());
            controller.saveStudentDetail();
            
            //5. Retrieve the lastModificatedDate for PFS and Applicant records, after add changes to the Applicant Record.
            Applicant__c applicant = [SELECT Id, LastModifiedDate, PFS__r.LastModifiedDate FROM Applicant__c WHERE Id=:controller.applicant.Id LIMIT 1];
            
            System.AssertNotEquals(LastModifiedDateApplicant, applicant.LastModifiedDate, 'The APPLICANT record should be updated.');
            
        Test.stopTest();
    }//End:saveStudentDetail_applicantEdited_ApplicantChangesSavedPFSRemainsTheSame
    
    
    /* KS Logic commented out. Since, KS is not around any longer.
    @isTest private static void saveStudentDetail_pfsEdited_pfsChangesSavedApplicantRemainsTheSame() {
        
        //1. Create test data
        TestData info = new TestData();
        
        Test.startTest();
        
            //2. Create an instance of the controller to be tested
            PageReference testPage = Page.SchoolHouseholdInformation;
            testPage.getParameters().put('SchoolPFSAssignmentId', info.spfsa1.Id);
            Test.setCurrentPage(testPage);
            SchoolHouseholdInformationController controller = new SchoolHouseholdInformationController();
            
            //3. Retrieve the lastModificatedDate for PFS and Applicant records.
            Applicant__c oldApplicant = [SELECT Id, LastModifiedDate, PFS__r.LastModifiedDate FROM Applicant__c WHERE Id=:controller.applicant.Id LIMIT 1];
            Datetime LastModifiedDatePFS = oldApplicant.PFS__r.LastModifiedDate;
            Datetime LastModifiedDateApplicant = oldApplicant.LastModifiedDate;
            
            //4. Add changes to the PFS Record.
            controller.applicant.PFS__r.KS_Household_Address__c = 'This is a test';
            System.AssertEquals(false, controller.applicantHasChanged());
            System.AssertEquals(true, controller.applicantPFSHasChanged());
            controller.saveStudentDetail();
            
            //5. Retrieve the lastModificatedDate for PFS and Applicant records, after add changes to the Applicant Record.
            Applicant__c applicant = [SELECT Id, LastModifiedDate, PFS__r.LastModifiedDate FROM Applicant__c WHERE Id=:controller.applicant.Id LIMIT 1];
            
            System.AssertNotEquals(LastModifiedDateApplicant, applicant.PFS__r.LastModifiedDate, 'The record PFS should be updated.');
            System.AssertEquals(LastModifiedDatePFS, applicant.LastModifiedDate, 'The APPLICANT record should remain the same.');
            
        Test.stopTest();
    }//End:saveStudentDetail_applicantEdited_ApplicantChangesSavedPFSRemainsTheSame
    */
}