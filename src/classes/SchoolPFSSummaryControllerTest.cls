/*
* SPEC-150
*
* Have one page which is a summary of the entire PFS
*
* Nathan, Exponent Partners, 2013
*/
@isTest
private class SchoolPFSSummaryControllerTest {

    private class TestData {
        Account school1;
        Contact staff1, parentA, parentB, student1, student2;
        Id academicYearId;
        PFS__c pfs1;
        Applicant__c applicant1;
        Student_Folder__c studentFolder1;
        School_PFS_Assignment__c spfsa1;
        Required_Document__c rd1;
        Family_Document__c fd1;
        School_Document_Assignment__c sda1;
        
        private TestData() {
            // school
            school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, null, false); 
            insert school1;       
        
            // school staff
            staff1 = TestUtils.createContact('Staff 1', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
            insert staff1;
            
            // parents and students
            parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
            parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
            student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
            student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
            insert new List<Contact> {parentA, parentB, student1, student2};

            // academic year
            TestUtils.createAcademicYears();
            academicYearId = GlobalVariables.getCurrentAcademicYear().Id;  
            
            // psf
            pfs1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
            pfs1.Parent_B__c = parentB.Id;
            insert new List<PFS__c> {pfs1};
            
            // applicant
            applicant1 = TestUtils.createApplicant(student1.Id, pfs1.Id, false);
            insert new List<Applicant__c> {applicant1};
            
            // student folder
            studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
            insert studentFolder1;
            
            // school pfs assignment
            spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1.Id, school1.Id, studentFolder1.Id, false);
            insert spfsa1;
            
             // required documents
            rd1 = TestUtils.createRequiredDocument(academicYearId, school1.Id, false);
            insert rd1;

            // family documents
            fd1 = TestUtils.createFamilyDocument('FamilyDoc1', academicYearId, false);
            insert fd1;

            // school document assignments
            sda1 = TestUtils.createSchoolDocumentAssignment('sda1', fd1.Id, rd1.Id, spfsa1.Id, false);
            insert sda1;            
        }
    }

    @isTest
    private static void testNote() {
        TestData td = new TestData();

        Test.startTest();
        PageReference pageRef = Page.SchoolPFSSummary;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSSummaryController controller = new SchoolPFSSummaryController();
        
        // new note
        controller.newNote();
        controller.note.Name = 'Test Title 1';
        controller.note.Body__c = 'Test Body 1';
        controller.savelNote();
        
        controller.newNote();
        controller.note.Name = 'Test Title 2';
        controller.note.Body__c = 'Test Body 2';
        controller.savelNote();
        
        List<Note__c> notes = [select Id, Name, Body__c from Note__c where ParentId__c = :td.spfsa1.Id];
        
        // requery and verify that note is created for pfs
        system.assertEquals(2, notes.size());
        
        controller.viewNoteId = notes[0].Id;
        controller.viewNote();
        
        system.assertEquals(notes[0].Body__c, controller.note.Body__c);
        
        controller.editNoteId = notes[0].Id;
        controller.editNote();
        controller.note.Name = 'Test Title 1.1';
        controller.note.Body__c = 'Test Body 1.1';
        controller.savelNote();
        
        system.assertNotEquals(notes[0].Body__c, controller.note.Body__c);
        
        controller.deleteNoteId = notes[1].Id;
        controller.deleteNote();
        
        notes = [select Id, Name, Body__c from Note__c where ParentId__c = :td.spfsa1.Id];
        
        system.assertEquals(1, notes.size());
        
        // currently nothing significant to test here
        controller.viewAllNotes();
        controller.viewNotes();
        controller.onClickViewMore();
        controller.onClickCloseViewMore();
         
        Test.stopTest();    
    }  

    @isTest
    private static void testCase() {
        TestData td = new TestData();

        Test.startTest();
        PageReference pageRef = Page.SchoolPFSSummary;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSSummaryController controller = new SchoolPFSSummaryController();

        // new case
        PageReference editCasePageRef = controller.newCase();
        
        // case tab
        PageReference caseTabPageRef = controller.gotoCaseTab();
    }

    @isTest
    private static void testDocument() {
        TestData td = new TestData();

        Test.startTest();
        PageReference pageRef = Page.SchoolPFSSummary;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSSummaryController controller = new SchoolPFSSummaryController();

        // upload new document
        PageReference docPageRef = controller.uploadNewDocument();
    }

    @isTest
    private static void testOptionalFields() {
        TestData td = new TestData();

        Test.startTest();
        PageReference pageRef = Page.SchoolPFSSummary;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSSummaryController controller = new SchoolPFSSummaryController();

        // edit optional feilds
        controller.editOptional();
        controller.schoolPFSAssignment.Optional_1__c = 'Test Optional 1';
        controller.saveOptional();
        
        // requery and verify
        system.assertEquals('Test Optional 1', [select Optional_1__c from School_PFS_Assignment__c
            where Id = :td.spfsa1.Id].Optional_1__c);

        // edit optional feilds
        controller.editOptional();
        controller.schoolPFSAssignment.Optional_1__c = 'Test Optional 2';
        controller.cancelOptional();
        
        // requery and verify that previous value retained
        system.assertEquals('Test Optional 1', [select Optional_1__c from School_PFS_Assignment__c
            where Id = :td.spfsa1.Id].Optional_1__c);
    }

    @isTest
    private static void testUnusualConditions() {
        TestData td = new TestData();

        // create unusual condition
        Unusual_Conditions__c uc = new Unusual_Conditions__c();
        uc.Academic_Year__c = td.academicYearId;
        uc.Business_Not_Indicated_Description__c = 'Your business is not indicated';
        insert uc;
        
        td.pfs1.Business_Not_Indicated__c = 'Yes';
        update td.pfs1;
        
        Test.startTest();
        PageReference pageRef = Page.SchoolPFSSummary;
        pageRef.getParameters().put('SchoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSSummaryController controller = new SchoolPFSSummaryController();

        // verify that unusual condition exist
        system.assertEquals(1, controller.unusualConditionList.size());
        system.assertEquals('Your business is not indicated', controller.unusualConditionList[0].descValue);
    }

}