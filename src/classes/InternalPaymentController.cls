/**
 * @description Controls the Internal Payment page.
 *              NAIS-873 Virtual Terminal for Payments in SF
 *                  Mechanism off the opp (button) to be able to make a
 *                  CC/echeck payment for a family with arbitrary amount.
 *              SL, Exponent Partners, 2013
 **/
public with sharing class InternalPaymentController extends PaymentControllerBase {
    private static final String OPPORTUNITY_ID_PARAM_NAME = 'id';
    private static final String CLOSED_WON = 'Closed Won';
    private static final String PFS_ID_PARAM_NAME = 'pfsid';
    private static final String MESSAGE_BAD_PAYMENT_AMOUNT = 'Invalid payment amount.';

    /**
     * @description The Opportunity that a payment is being made for.
     */
    public Opportunity theOpportunity {
        get {
            if (theOpportunity == null) {
                theOpportunity = getOpportunity();
            }
            return theOpportunity;
        }
        set;
    }

    /**
     * @description The PFS that the current opportunity is associated with.
     */
    public PFS__c thePFS {
        get {
            if (thePFS == null) {
                thePFS = getPfs();
            }
            return thePFS;
        }
        set;
    }

    /**
     * @description Whether or not the payment information provided is valid.
     */
    public Boolean isValidPage { get; set; }

    /**
     * @description Whether or not there has been an error during the processBefore()
     *              method. This will make sure that the processPayment() method does
     *              not continue.
     */
    public Boolean hasProcessBeforeErrors {
        get {
            if (hasProcessBeforeErrors == null) {
                return false;
            }
            return hasProcessBeforeErrors;
        }
        set;
    }

    /**
     * @description Whether or not the page is in test mode.
     */
    public boolean isTestMode {
        get {
            return PaymentProcessor.InTestMode;
        }
    }

    /**
     * @description Constructor that will call the PaymentControllerBase
     *              constructor.
     */
    public InternalPaymentController() {
        super();
    }

    /**
     * @description [DP] 07.28.2015 NAIS-2469 now have init method called
     *              from page since we may need to do dml.
     */
    public void init() {
        isValidPage = false;

        // Validate that either the Opportunity or PFS is present.
        if (theOpportunity == null && thePFS == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Missing the Opportunity and PFS records'));
            return;
        }

        if (theOpportunity == null) {
            // If there is already an Opportunity on the PFS but it hasn't been provided
            // to the page display an error message.
            if (thePFS.Opportunities__r != null && !thePFS.Opportunities__r.isEmpty()) {
                if (thePFS.Opportunities__r.size() == 1) {
                    theOpportunity = thePFS.Opportunities__r[0];
                } else if (thePFS.Opportunities__r.size() > 1) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'PFS record already has an Opportunity -- the payment must be started from there.'));
                    return;
                }
            } else {
                // Create an opportunity to work on
                theOpportunity = FinanceAction.createOppAndTransaction(new Set<Id> {thePFS.Id}, true)[0];
                Boolean isKSOnly = FinanceAction.isKSOnlyPFS(thePFS);
                theOpportunity.amount = FinanceAction.getAmountDue(theOpportunity, theOpportunity.Academic_Year_Picklist__c, isKSOnly);
            }
        }

        // NAIS-2469: If the opportunity exists, see if it already has a sale transaction line item.
        // If not, create one and then re-query the opportunity.
        if (theOpportunity.Id != null &&
                (theOpportunity.RecordTypeId == RecordTypes.opportunityFeeWaiverPurchaseTypeId ||
                        theOpportunity.RecordTypeId == RecordTypes.opportunitySubscriptionFeeTypeId)) {

            List<Transaction_Line_Item__c> transactionLineItems = TransactionLineItemSelector.Instance
                    .selectByOpportunityAndRecordType(new Set<Id> { theOpportunity.Id }, new Set<Id> { RecordTypes.saleTransactionTypeId });

            if (transactionLineItems.isEmpty()) {
                PaymentUtils.insertTransactionLineItemsFromOpportunities(new List<Opportunity> { theOpportunity });
            }
            theOpportunity = getOpportunity(theOpportunity.Id);
        }

        if (thePFS == null) {
            thePFS = theOpportunity.PFS__r;
        } else {
            theOpportunity.PFS__r = thePFS;
        }

        // set up the payment data structure
        paymentData.paymentType = FamilyPaymentData.PAYMENT_TYPE_CC;  // default to credit card payment
        paymentData.billingCountry = PaymentProcessor.getDefaultCountryCode(); // default to US;

        String recordTypeName = theOpportunity.RecordType.Name;
        if (recordTypeName == null) {
            Map<Id, Schema.RecordTypeInfo> rtMapById = Opportunity.SObjectType.getDescribe().getRecordTypeInfosById();
            recordTypeName = rtMapById.get(theOpportunity.RecordTypeId).Name;
        }
        // NAIS-1674, 1810 Start
        // set the default payment description
        if (theOpportunity.RecordTypeId == RecordTypes.opportunityFeeWaiverPurchaseTypeId) {
            paymentData.paymentDescription = recordTypeName + ' - ' + theOpportunity.Total_Purchased_Waivers__c;
        } else {
            paymentData.paymentDescription = recordTypeName;
        }

        // set the default payment tracker
        if (theOpportunity.recordTypeId == RecordTypes.opportunityFeeWaiverPurchaseTypeId || theOpportunity.recordTypeId == RecordTypes.opportunitySubscriptionFeeTypeId) {
            paymentData.paymentTracker = theOpportunity.SSS_School_Code__c;
        } else {
            paymentData.paymentTracker = String.format(PaymentProcessor.PfsApplicationFeeTracker,
                                           new List<String> { theOpportunity.PFS__r.PFS_Number__c });
        }
        // NAIS-1674, 1810 End

        // set the default payment price
        paymentData.paymentAmount = null;
        if ((theOpportunity.Id != null && theOpportunity.Net_Amount_Due__c != null) && (theOpportunity.Net_Amount_Due__c > 0)) {
            paymentData.paymentAmount = theOpportunity.Net_Amount_Due__c;
        } else if (theOpportunity.Id == null) {
            paymentData.paymentAmount = theOpportunity.Amount; // [DP] 07.06.2015 NAIS-1933 amount field set earlier by FinanceAction.getAmountDue
        }

        // default the billing info based on Parent A info on the PFS
        paymentData.billingFirstName = theOpportunity.PFS__r.Parent_A_First_Name__c;
        paymentData.billingLastName = theOpportunity.PFS__r.Parent_A_Last_Name__c;
        paymentData.billingEmail = theOpportunity.PFS__r.Parent_A_Email__c;
        paymentData.billingCity = theOpportunity.PFS__r.Parent_A_City__c;
        paymentData.billingPostalCode = theOpportunity.PFS__r.Parent_A_ZIP_Postal_Code__c;

        // split the address into individual lines
        if (theOpportunity.PFS__r.Parent_A_Address__c != null) {
            List<String> addressLines = theOpportunity.PFS__r.Parent_A_Address__c.trim().split('\n');
            if (addressLines.size() > 0) {
                paymentData.billingStreet1 = addressLines[0];
            }
            if (addressLines.size() > 1) {
                paymentData.billingStreet2 = addressLines[1];
            }
        }

        // try to look up the country code
        if (theOpportunity.PFS__r.Parent_A_Country__c != null) {
            paymentData.billingCountry = PaymentProcessor.getCountryCode(theOpportunity.PFS__r.Parent_A_Country__c);
        }

        if (paymentData.billingCountry == PaymentProcessor.getDefaultCountryCode()) {
            paymentData.billingState = theOpportunity.PFS__r.Parent_A_State__c;
        }

        isValidPage = true;
    }

    /**
     * @description Cancel the payment being made to the opportunity and return
     *              the user to the opportunity record itself.
     * @returns A PageReference pointing to the current Opportunity record.
     */
    public PageReference actionCancel() {
        PageReference pr = new PageReference('/' + theOpportunity.Id);
        pr.setRedirect(true);
        return pr;
    }

    /**
     * @description Process the payment for the given information provided by the
     *              user. If the payment information is invalid a null PageReference
     *              will be returned with a PageMessage added to the page. If valid a
     *              Transaction Line Items will be created for the payment, and it
     *              will be processed through the merchant processor. The PageRefernce
     *              returned will send the user to the generated Transaction Line Item.
     * @returns A PageReference that is either null if the payment information is invalid
     *          or unsuccessful, otherwise a PageReference to send the user to the newly
     *          created Transaction Line Item record for this payment.
     */
    public override PageReference processPayment() {
        PageReference pr = null;

        if (hasProcessBeforeErrors) {
            return pr;
        }

        paymentData.trimStrings();
        // clear page errors
        this.fieldErrors = new FieldErrors();

        boolean isValid = false;
        if (paymentData.paymentType == FamilyPaymentData.PAYMENT_TYPE_CC) {
            isValid = validateCreditCardForm();
        } else if (paymentData.paymentType == FamilyPaymentData.PAYMENT_TYPE_ECHECK) {
            isValid = validateECheckForm();
        }

        if (!isValid) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, MESSAGE_PAGE_VALIDATION_ERROR));
            return null;
        }

        PaymentService.Request request = new PaymentService.Request(theOpportunity, thePFS, paymentData, !(theOpportunity.PFS__c != null));
        PaymentService.Response response = PaymentService.Instance.processPayment(request);

        if (response.Result != null && response.Result.isSuccess) {
            OpportunityService.Instance.markAsClosedWon(response.Opportunity);

            pr = new PageReference('/' + response.TransactionLineItemId).setRedirect(true);
        } else {
            this.ErrorMessage = (response.ErrorMessage != null) ? response.ErrorMessage :
                    'There was a problem processing your payment.';
            return handlePaymentError();
        }

        return pr;
    }

    /**
     * @description Override the base functionality and handle a payment error by
     *              displaying the message provided by the payment processor and
     *              return a null page reference.
     * @return A null page reference.
     */
    public override PageReference handlePaymentError() {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, this.ErrorMessage));

        return null;
    }

    public PageReference processBefore() {
        paymentData.trimStrings();
        // clear page errors
        this.fieldErrors = new FieldErrors();

        boolean isValid = false;
        if (paymentData.paymentType == FamilyPaymentData.PAYMENT_TYPE_CC) {
            isValid = validateCreditCardForm();
        } else if (paymentData.paymentType == FamilyPaymentData.PAYMENT_TYPE_ECHECK) {
            isValid = validateECheckForm();
        }

        if (!isValid) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, MESSAGE_PAGE_VALIDATION_ERROR));
            return null;
        }

        PaymentService.Request request = new PaymentService.Request(theOpportunity, thePFS, paymentData, (theOpportunity.PFS__c == null));
        PaymentService.Response response = PaymentService.Instance.processBefore(request);

        if (response.Result == null || !response.Result.isSuccess) {
            hasProcessBeforeErrors = true;
            String message = (response.Result.ErrorMessage == null) ?
                    PaymentProcessor.UNEXPECTED_ERROR : response.Result.ErrorMessage;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, message));
        }

        return null;
    }

    /**
     * @description The valid payment options that the user can process this payment with.
     *              Credit Card and eCheck are the two available options.
     * @returns A List of SelectOptions consisting of the available payment options.
     */
    public List<SelectOption> getPaymentTypeOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(FamilyPaymentData.PAYMENT_TYPE_CC, 'Credit Card'));
        options.add(new SelectOption(FamilyPaymentData.PAYMENT_TYPE_ECHECK, 'eCheck'));
        return options;
    }

    /**
     * @description Override the default Opportunity Id query parameter.
     * @returns A string value of the Opportunity Id query parameter.
     */
    protected override String getOpportunityIdParameter() {
        return OPPORTUNITY_ID_PARAM_NAME;
    }

    /**
     * @description Override the default PFS Id query parameter.
     * @returns A string value of the PFS Id query parameter.
     */
    protected override String getPfsIdParameter() {
        return PFS_ID_PARAM_NAME;
    }

    private boolean validateCreditCardForm() {
        boolean isValid = true;

        boolean isValidPaymentInfoFields = validatePaymentInfoFields();
        boolean isValidBillingFields = validateBillingFields();
        boolean isValidCreditCardFields = validateCreditCardFields();
        isValid = isValidPaymentInfoFields && isValidBillingFields && isValidCreditCardFields;

        return isValid;
    }

    private boolean validateECheckForm() {
        boolean isValid = true;

        boolean isValidPaymentInfoFields = validatePaymentInfoFields();
        boolean isValidBillingFields = validateBillingFields();
        boolean isValidECheckFields = validateECheckFields();

        isValid = isValidPaymentInfoFields && isValidBillingFields && isValidECheckFields;

        return isValid;
    }

    private boolean validatePaymentInfoFields() {
        boolean isValid = true;

        if (String.isBlank(paymentData.paymentDescription))  {
            isValid = false;
            fieldErrors.paymentDescription = MESSAGE_FIELD_VALUE_MISSING;
        }
        if (paymentData.paymentAmount == null) {
            isValid = false;
            fieldErrors.paymentAmount = MESSAGE_FIELD_VALUE_MISSING;
        }
        if (paymentData.paymentAmount <= 0)  {
            isValid = false;
            fieldErrors.paymentAmount = MESSAGE_BAD_PAYMENT_AMOUNT;
        }

        if (String.isBlank(paymentData.paymentTracker))  {
            isValid = false;
            fieldErrors.paymentTracker = MESSAGE_FIELD_VALUE_MISSING;
        }

        return isValid;
    }
}