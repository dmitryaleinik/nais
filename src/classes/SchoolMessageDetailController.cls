/**
 * SchoolMessageDetailController.cls
 *
 * @description: Server side controller for Mass Email message detail page, handles interactions w/ data model and view state manipulation.
 *
 * @author: Chase Logan @ Presence PG
 */
public class SchoolMessageDetailController implements SchoolAcademicYearSelectorInterface {

    /* member vars and properties */
    public Boolean massEmailEnabled {

        get {
            // Handle app enabled/disabled
            return MassEmailUtil.isMassEmailGloballyEnabledByAccount( 
                        GlobalVariables.getCurrentSchoolId(), UserInfo.getProfileId());
        }
        private set;
    }
    public SchoolMassEmailSendModel massESModel { get; set; }
    public List<RecipModel> allRecipModelList { 
        get {
            return ( this.allRecipModelList == null ? 
                this.allRecipModelList = new List<RecipModel>() : this.allRecipModelList);
        }
        private set; 
    }
    public List<RecipModel> openedRecipModelList { 
        get {
            return ( this.openedRecipModelList == null ? 
                this.openedRecipModelList = new List<RecipModel>() : this.openedRecipModelList);
        }
        private set; 
    }
    public List<RecipModel> unopenedRecipModels
    {
        get
        {
            if (unopenedRecipModels == null)
            {
                unopenedRecipModels = new List<RecipModel>();
            }
            return unopenedRecipModels;
        }
        private set;
    }
    public List<RecipModel> bouncedRecipModelList { 
        get {
            return ( this.bouncedRecipModelList == null ? 
                this.bouncedRecipModelList = new List<RecipModel>() : this.bouncedRecipModelList);
        }
        private set; 
    }
    public List<RecipModel> unsubRecipModelList { 
        get {
            return ( this.unsubRecipModelList == null ? 
                this.unsubRecipModelList = new List<RecipModel>() : this.unsubRecipModelList);
        }
        private set; 
    }
    public List<RecipModel> failedRecipModelList { 
        get {
            return ( this.failedRecipModelList == null ? 
                this.failedRecipModelList = new List<RecipModel>() : this.failedRecipModelList);
        }
        private set; 
    }

    /* class constants */
    public static final String QUERY_STRING_MESSAGE_DETAIL = 'messagedetailid';
    
    // default ctor
    public SchoolMessageDetailController() {

        // initialize view
        init();
    }


    /* private instance methods */

    // handles page initilization
    private void init()
    {
        // grab query string param and attempt lookup of corresponding massES record
        String currentDetailId = 
            ApexPages.currentPage().getParameters().get( SchoolMessageDetailController.QUERY_STRING_MESSAGE_DETAIL);
        Mass_Email_Send__c massES = new MassEmailSendDataAccessService().getMassEmailSendById( currentDetailId);
        this.massESModel = SchoolMassEmailSendModel.getModel(massES);

        // initialize data for collapsible lists based on message send events
        Map<String,List<RecipModel>> recipModelMap = this.constructRecipModelLists( massES);
        this.allRecipModelList = recipModelMap.get( SchoolMassEmailSendModel.ALL_RECIPS);
        this.openedRecipModelList = recipModelMap.get( SchoolMassEmailSendModel.STATUS_OPENED);
        this.bouncedRecipModelList = recipModelMap.get( SchoolMassEmailSendModel.STATUS_BOUNCED);
        this.unsubRecipModelList = recipModelMap.get( SchoolMassEmailSendModel.STATUS_UNSUB);
        this.failedRecipModelList = recipModelMap.get( SchoolMassEmailSendModel.STATUS_FAILED);

        List<RecipModel> notUnopenRecipModels = new List<RecipModel>(openedRecipModelList);
        notUnopenRecipModels.addAll(bouncedRecipModelList);
        notUnopenRecipModels.addAll(unsubRecipModelList);
        notUnopenRecipModels.addAll(failedRecipModelList);

        this.unopenedRecipModels = getUnopenRecipModels(allRecipModelList, notUnopenRecipModels);
    }

    private List<RecipModel> getUnopenRecipModels(List<RecipModel> allRecips, List<RecipModel> notUnopenedRecModels)
    {
        Map<Id, RecipModel> allRecipsByRecipId = new Map<Id, RecipModel>();
        for (RecipModel recMod : allRecips)
        {
            allRecipsByRecipId.put(recMod.recipId, recMod);
        }

        Map<Id, RecipModel> notUnopenedRecModelsByRecipId = new Map<Id, RecipModel>();
        for (RecipModel recMod : notUnopenedRecModels)
        {
            notUnopenedRecModelsByRecipId.put(recMod.recipId, recMod);
        }

        List<RecipModel> unopenRecModels = new List<RecipModel>();
        for (Id recipId : allRecipsByRecipId.keySet())
        {
            if (notUnopenedRecModelsByRecipId.get(recipId) == null)
            {
                unopenRecModels.add(allRecipsByRecipId.get(recipId));
            }
        }

        return unopenRecModels ;
    }

    
    // Construct RecipModel Lists for All/Opened/Boucned/Unsub based on ID's stored in massES.School_PFS_Assignments__c
    private Map<String,List<RecipModel>> constructRecipModelLists( Mass_Email_Send__c massES) {
        Map<String,List<RecipModel>> recipsByStatusMap = new Map<String,List<RecipModel>>();
        recipsByStatusMap.put( SchoolMassEmailSendModel.ALL_RECIPS, new List<RecipModel>());
        recipsByStatusMap.put( SchoolMassEmailSendModel.STATUS_OPENED, new List<RecipModel>());
        recipsByStatusMap.put( SchoolMassEmailSendModel.STATUS_BOUNCED, new List<RecipModel>());
        recipsByStatusMap.put( SchoolMassEmailSendModel.STATUS_UNSUB, new List<RecipModel>());
        recipsByStatusMap.put( SchoolMassEmailSendModel.STATUS_FAILED, new List<RecipModel>());

        // early return on invalid data
        if ( massES == null || String.isEmpty( massES.School_PFS_Assignments__c)) return recipsByStatusMap;

        // get any failed sends
        Set<String> failedIDSet = ( massES.Failed_to_Send__c != null ? 
                new Set<String>( massES.Failed_to_Send__c.split( SchoolMassEmailSendModel.SPLIT_CHAR)) : new Set<String>() );
        // get all SPA Id's off massES record, retreive SPA records to iterate over JSON stored in SPA.Mass_Email_Events__c
        Set<String> sPFSIdSet = 
                new Set<String>( massES.School_PFS_Assignments__c.split( SchoolMassEmailSendModel.SPLIT_CHAR));
        List<School_PFS_Assignment__c> sPFSEventList = 
                new SchoolPFSAssignmentDataAccessService().getSPAEmailEventByIdSet( sPFSIdSet, false);
        // get list of Parents (recipients) and Applicants via School PFS
        List<School_PFS_Assignment__c> schoolPFSList = 
                new SchoolPFSAssignmentDataAccessService().getSchoolPFSByIdSet( sPFSIdSet);
        
        
        if ( sPFSEventList != null && sPFSEventList.size() > 0) {

            Map<String,Set<String>> eventByRecipIdMap = new Map<String,Set<String>>();
            for ( School_PFS_Assignment__c sPFS : sPFSEventList) {

                if ( String.isNotEmpty( sPFS.Mass_Email_Events__c)) {

                    // Event's are stored as comma separted JSON objects, wrap events in leading and trailing braces
                    // to make valid JSON array before deserializing
                    String jsonArrayString = '[' + sPFS.Mass_Email_Events__c + ']';
                    List<SendgridEvent> tempList = ( List<SendgridEvent>)JSON.deserialize( jsonArrayString, List<SendgridEvent>.class);

                    for ( SendgridEvent evt : tempList) {
                        
                        // store all events for this massES record by recipient Id for later iteration
                        if ( evt.mass_email_send_id == massES.Id && eventByRecipIdMap.containsKey( evt.recipient_id)) {
                            
                            Set<String> tempSet = eventByRecipIdMap.get( evt.recipient_id);
                            if ( !tempSet.contains( evt.event)) {

                                tempSet.add( evt.event);
                                eventByRecipIdMap.put( evt.recipient_id, tempSet);
                            }
                        } else if ( evt.mass_email_send_id == massES.Id && !eventByRecipIdMap.containsKey( evt.recipient_id)) {
                            
                            Set<String> tempSet = new Set<String>();
                            tempSet.add( evt.event);
                            eventByRecipIdMap.put( evt.recipient_id, tempSet);
                        }
                    }
                }
            }

            // iterate back over eventByRecipIdMap and create RecipModel instances with necessary info for collapsible lists
            // that are organized by send category, either all, opened, bounced, or unsub
            Map<String,RecipModel> allRecipsMap = new Map<String,RecipModel>();
            
            if ( schoolPFSList != null && schoolPFSList.size() > 0) {
                
                for ( School_PFS_Assignment__c sPFS : schoolPFSList) {
                    
                    // add all recips from massES record with default status, replace with specific event status as they come in
                    allRecipsMap = this.updateAllRecipsMap( allRecipsMap, sPFS, SchoolMassEmailSendModel.STATUS_DELIVERED);

                    // handled failed recips first
                    String key = sPFS.Applicant__r.PFS__r.Parent_A__c;
                    if ( failedIDSet.contains( key)) {

                        recipsByStatusMap = this.updateRecipsByStatusMap( recipsByStatusMap, sPFS, SchoolMassEmailSendModel.STATUS_FAILED);

                        // replace default status with current event status for all recips
                        allRecipsMap = this.updateAllRecipsMap( allRecipsMap, sPFS, SchoolMassEmailSendModel.STATUS_FAILED);
                    }
                    
                    if ( eventByRecipIdMap.containsKey( key) && 
                            eventByRecipIdMap.get( key).contains( SchoolMassEmailSendModel.STATUS_OPENED)) {

                        recipsByStatusMap = this.updateRecipsByStatusMap( recipsByStatusMap, sPFS, SchoolMassEmailSendModel.STATUS_OPENED);

                        // replace default status with current event status for all recips
                        allRecipsMap = this.updateAllRecipsMap( allRecipsMap, sPFS, SchoolMassEmailSendModel.STATUS_OPENED);

                    } else if ( eventByRecipIdMap.containsKey( key) && 
                                    eventByRecipIdMap.get( key).contains( SchoolMassEmailSendModel.STATUS_BOUNCED)) {

                        recipsByStatusMap = this.updateRecipsByStatusMap( recipsByStatusMap, sPFS, SchoolMassEmailSendModel.STATUS_BOUNCED);

                        // replace default status with current event status for all recips
                        allRecipsMap = this.updateAllRecipsMap( allRecipsMap, sPFS, SchoolMassEmailSendModel.STATUS_BOUNCED);

                    } 

                    // unsubscribe will always be accompanied by an open, so check for it last
                    if ( eventByRecipIdMap.containsKey( key) && 
                                    eventByRecipIdMap.get( key).contains( SchoolMassEmailSendModel.STATUS_UNSUB)) {

                        recipsByStatusMap = this.updateRecipsByStatusMap( recipsByStatusMap, sPFS, SchoolMassEmailSendModel.STATUS_UNSUB);

                        // replace default status with current event status for all recips
                        allRecipsMap = this.updateAllRecipsMap( allRecipsMap, sPFS, SchoolMassEmailSendModel.STATUS_UNSUB);
                    }
                }
            }
            // add all recips back to recips by status map
            recipsByStatusMap.put( SchoolMassEmailSendModel.ALL_RECIPS, allRecipsMap.values());
        }

        return recipsByStatusMap;
    }

    // handles updates to allRecipsMap, repetitive logic condensed here
    private Map<String,RecipModel> updateAllRecipsMap( Map<String,RecipModel> inputMap, 
                                                        School_PFS_Assignment__c sPFS, String status) {
        // early return on empty params
        if ( inputMap == null || sPFS == null || String.isBlank( status)) return inputMap;

        if ( inputMap.containsKey( sPFS.Applicant__r.PFS__r.Parent_A__c)) {

            // list of applicants per parent is sent to View as comma separated string of names, 
            // must make sure same applicant isn't already present in the string
            RecipModel tempModel = inputMap.get( sPFS.Applicant__r.PFS__r.Parent_A__c);
            Set<String> currentAppsSplit = 
                new Set<String>( tempModel.commaSeparatedApplicants.split( SchoolMassEmailSendModel.SPLIT_CHAR));
            String newApps = ( String.isBlank( sPFS.Applicant_First_Name__c) ? '' : sPFS.Applicant_First_Name__c)  
                    + ' ' + ( String.isBlank( sPFS.Applicant_Last_Name__c) ? '' : sPFS.Applicant_Last_Name__c) + ',';

            if ( currentAppsSplit != null && !currentAppsSplit.contains( newApps.left( newApps.length() - 1))) {

                tempModel.commaSeparatedApplicants += newApps;
                inputMap.put( sPFS.Applicant__r.PFS__r.Parent_A__c, tempModel);
            }
        } else {

            inputMap.put( sPFS.Applicant__r.PFS__r.Parent_A__c, 
                new RecipModel( sPFS.Applicant__r.PFS__r.Parent_A__c, sPFS.Parent_A__c, 
                sPFS.Parent_A_Email_Migrated__c, sPFS.Applicant_First_Name__c + ' ' + sPFS.Applicant_Last_Name__c + ',', 
                status));
        }

        return inputMap;
    }

    // handles updates to recipsByStatusMap, repetitive logic condensed here
    private Map<String,List<RecipModel>> updateRecipsByStatusMap( Map<String,List<RecipModel>> inputMap, 
                                                                    School_PFS_Assignment__c sPFS, String status) {
        // early return on empty params
        if ( inputMap == null || sPFS == null || String.isBlank( status)) return inputMap;

        List<RecipModel> tempList = inputMap.get( status);
        Boolean alreadyExists = false;
        for ( RecipModel rModel : tempList) {

            if ( rModel.recipId == sPFS.Applicant__r.PFS__r.Parent_A__c) {

                String currentApps = rModel.commaSeparatedApplicants;
                String newApps = ( String.isBlank( sPFS.Applicant_First_Name__c) ? '' : sPFS.Applicant_First_Name__c)  
                            + ' ' + ( String.isBlank( sPFS.Applicant_Last_Name__c) ? '' : sPFS.Applicant_Last_Name__c) + ',';
                if ( currentApps != newApps) {

                    rModel.commaSeparatedApplicants = currentApps + newApps;
                }
                alreadyExists = true;
                break;
            }
        }

        if ( !alreadyExists) {

            RecipModel tempModel = 
                new RecipModel( sPFS.Applicant__r.PFS__r.Parent_A__c, sPFS.Parent_A__c, 
                        sPFS.Parent_A_Email_Migrated__c, sPFS.Applicant_First_Name__c + ' ' + sPFS.Applicant_Last_Name__c + ',', 
                            status);
            tempList.add( tempModel);
        }
        
        inputMap.put( status, tempList);

        return inputMap;
    }

    
    /* UI wrapper classes */
    public class RecipModel {

        public String recipId { get; set; }
        public String parentName { get; set; }
        public String parentEmail { get; set; }
        public String commaSeparatedApplicants { get; set; }
        public String status { get; set; }

        public String OPEN_STATUS {
            get { return SchoolMassEmailSendModel.STATUS_OPENED; }
            private set;
        }

        public String DELIVERED_STATUS {
            get { return SchoolMassEmailSendModel.STATUS_DELIVERED; }
            private set;
        }

        public RecipModel() {}
        
        public RecipModel( String recipId, String parentName, String parentEmail, String commaSeparatedApplicants,
                            String status) {

            this.recipId = recipId;
            this.parentName = parentName;
            this.parentEmail = parentEmail;
            this.commaSeparatedApplicants = commaSeparatedApplicants;
            this.status = status;
        }

    }


    /* 
     * Solely for supporting header and academic year selector, copied from existing code to stay consistent
     */
       public Id academicyearid;
    public Academic_Year__c currentAcademicYear { get; private set; }

    public SchoolMessageDetailController Me {
        get { return this; }
    }
    public String getAcademicYearId() {
        return academicyearid;
    }
    public void setAcademicYearId( String academicYearIdParam) {
        academicyearid = academicYearIdParam;
        currentAcademicYear = GlobalVariables.getAcademicYear( academicyearid);
    }
    // on change of academic year, reload page with new folder
    public PageReference SchoolAcademicYearSelector_OnChange( Boolean saveRecord) {
        PageReference newPage = new PageReference( ApexPages.currentPage().getUrl());
        newPage.getParameters().put( 'academicyearid', getAcademicYearId());
        newPage.setRedirect( true);
        return newPage;
    }
    // Due to the selector_onchange not refreshing properly, we redirect to the same page with academicyearid parameter.
    public void loadAcademicYear() {
        Map<String, String> parameters = ApexPages.currentPage().getParameters();
        String academicYearId = null;
        if ( parameters.containsKey( 'academicyearid'))
            setAcademicYearId(parameters.get( 'academicyearid'));
        else {
            setAcademicYearId( GlobalVariables.getCurrentAcademicYear().Id);
        }
    }
    /* end header support */

}