@isTest
private class StudentFoldersTest {

    @isTest
    private static void onAfterUpdate_folderStatusIsChanged_platformEventEnabled_expectEventPublished() {
        FinancialAidAPISettings.setToggle('Application_Update_Event_Trigger_Enabled__c', true);

        Test.startTest();
        String originalStatus = 'Submitted';
        String newStatus = 'Award Proposed';

        Account school = AccountTestData.Instance.asSchool().forFinancialAidAPIApp('Financial Aid API Ravenna').insertAccount();
        Student_Folder__c folder = StudentFolderTestData.Instance.forFolderStatus(originalStatus).forSchoolId(school.Id).insertStudentFolder();

        folder.Folder_Status__c = newStatus;
        update folder;
        Test.stopTest();

        List<Application_Update__e> publishedEvents = ApplicationUpdates.getEvents();
        System.assert(!publishedEvents.isEmpty(), 'Expected platform events to be published.');
        System.assertEquals('Award Proposed', publishedEvents[0].Status__c, ' Platform event didnt have updated award');
    }

    @isTest
    private static void onAfterUpdate_folderStatusChangedbySchoolAdmin_expectedEventPublished() {
        FinancialAidAPISettings.setToggle('Application_Update_Event_Trigger_Enabled__c', true);

        Account school = AccountTestData.Instance
                .asSchool()
                .forFinancialAidAPIApp('Financial Aid API Ravenna')
                .insertAccount();

        // Staff contact
        Contact schoolStaff = ContactTestData.Instance
                .forAccount(school.Id)
                .asSchoolStaff()
                .insertContact();

        // Create staff user
        User schoolPortalUser;
        System.runAs(CurrentUser.getCurrentUser()) {
            schoolPortalUser = UserTestData.Instance
                    .forContactId(schoolStaff.Id)
                    .forProfileId(GlobalVariables.schoolPortalAdminProfileId)
                    .insertUser();
        }

        String originalStatus = 'Submitted';
        String newStatus = 'Award Proposed';

        Student_Folder__c folder = StudentFolderTestData.Instance.forFolderStatus(originalStatus).forSchoolId(school.Id).insertStudentFolder();

        Test.startTest();
        System.runAs(schoolPortalUser) {
            folder.Folder_Status__c = newStatus;
            // Execute DML without sharing to avoid the need to create share records to grant the school user access to the folder.
            Dml.WithoutSharing.updateObjects(folder);
        }
        Test.stopTest();

        List<Application_Update__e> publishedEvents = ApplicationUpdates.getEvents();
        System.assert(!publishedEvents.isEmpty(), 'Expected platform events to be published.');
        System.assertEquals('Award Proposed', publishedEvents[0].Status__c, ' Platform event didnt have updated award');
    }

    @isTest
    private static void onAfterUpdate_folderStatusChangedbySchoolUser_expectedEventPublished() {
        FinancialAidAPISettings.setToggle('Application_Update_Event_Trigger_Enabled__c', true);

        Account school = AccountTestData.Instance
                .asSchool()
                .forFinancialAidAPIApp('Financial Aid API Ravenna')
                .insertAccount();

        Contact schoolStaff = ContactTestData.Instance
                .forAccount(school.Id)
                .asSchoolStaff()
                .insertContact();

        // Create staff user
        User schoolPortalUser;
        System.runAs(CurrentUser.getCurrentUser()) {
            schoolPortalUser = UserTestData.Instance
                    .forContactId(schoolStaff.Id)
                    .forProfileId(GlobalVariables.schoolPortalUserProfileId)
                    .insertUser();
        }

        String originalStatus = 'Submitted';
        String newStatus = 'Award Proposed';

        Student_Folder__c folder = StudentFolderTestData.Instance.forFolderStatus(originalStatus).forSchoolId(school.Id).insertStudentFolder();

        Test.startTest();
        System.runAs(schoolPortalUser) {
            folder.Folder_Status__c = newStatus;
            // Execute DML without sharing to avoid the need to create share records to grant the school user access to the folder.
            Dml.WithoutSharing.updateObjects(folder);
        }
        Test.stopTest();

        List<Application_Update__e> publishedEvents = ApplicationUpdates.getEvents();
        System.assert(!publishedEvents.isEmpty(), 'Expected platform events to be published.');
        System.assertEquals('Award Proposed', publishedEvents[0].Status__c, ' Platform event didnt have updated award');
    }

    @isTest
    private static void onAfterUpdate_folderStatusChangedbySchoolBasicAdmin_expectedEventPublished() {
        FinancialAidAPISettings.setToggle('Application_Update_Event_Trigger_Enabled__c', true);

        Account school = AccountTestData.Instance
                .asSchool()
                .forFinancialAidAPIApp('Financial Aid API Ravenna')
                .insertAccount();

        Contact schoolStaff = ContactTestData.Instance
                .forAccount(school.Id)
                .asSchoolStaff()
                .insertContact();

        // Create staff user
        User schoolPortalUser;
        System.runAs(CurrentUser.getCurrentUser()) {
            schoolPortalUser = UserTestData.Instance
                    .forContactId(schoolStaff.Id)
                    .forProfileId(GlobalVariables.schoolPortalBasicAdminProfileId)
                    .insertUser();
        }

        String originalStatus = 'Submitted';
        String newStatus = 'Award Proposed';

        Student_Folder__c folder = StudentFolderTestData.Instance.forFolderStatus(originalStatus).forSchoolId(school.Id).insertStudentFolder();

        Test.startTest();
        System.runAs(schoolPortalUser) {
            folder.Folder_Status__c = newStatus;
            // Execute DML without sharing to avoid the need to create share records to grant the school user access to the folder.
            Dml.WithoutSharing.updateObjects(folder);
        }
        Test.stopTest();

        List<Application_Update__e> publishedEvents = ApplicationUpdates.getEvents();
        System.assert(!publishedEvents.isEmpty(), 'Expected platform events to be published.');
        System.assertEquals('Award Proposed', publishedEvents[0].Status__c, ' Platform event didnt have updated award');
    }

    @isTest
    private static void onAfterUpdate_folderStatusChangedbySchoolBasicUser_expectedEventPublished() {
        FinancialAidAPISettings.setToggle('Application_Update_Event_Trigger_Enabled__c', true);

        Account school = AccountTestData.Instance
                .asSchool()
                .forFinancialAidAPIApp('Financial Aid API Ravenna')
                .insertAccount();

        Contact schoolStaff = ContactTestData.Instance
                .forAccount(school.Id)
                .asSchoolStaff()
                .insertContact();

        // Create staff user
        User schoolPortalUser;
        System.runAs(CurrentUser.getCurrentUser()) {
            schoolPortalUser = UserTestData.Instance
                    .forContactId(schoolStaff.Id)
                    .forProfileId(GlobalVariables.schoolPortalBasicUserProfileId)
                    .insertUser();
        }

        String originalStatus = 'Submitted';
        String newStatus = 'Award Proposed';

        Student_Folder__c folder = StudentFolderTestData.Instance.forFolderStatus(originalStatus).forSchoolId(school.Id).insertStudentFolder();

        Test.startTest();
        System.runAs(schoolPortalUser) {
            folder.Folder_Status__c = newStatus;
            // Execute DML without sharing to avoid the need to create share records to grant the school user access to the folder.
            Dml.WithoutSharing.updateObjects(folder);
        }
        Test.stopTest();

        List<Application_Update__e> publishedEvents = ApplicationUpdates.getEvents();
        System.assert(!publishedEvents.isEmpty(), 'Expected platform events to be published.');
        System.assertEquals('Award Proposed', publishedEvents[0].Status__c, ' Platform event didnt have updated award');
    }

    @isTest
    private static void onAfterUpdate_folderStatusIsChanged_platformEventDisabled_expectEventNotPublished() {
        FinancialAidAPISettings.setToggle('Application_Update_Event_Trigger_Enabled__c', false);

        Test.startTest();
        String originalStatus = 'Submitted';
        String newStatus = 'Award Proposed';

        Account school = AccountTestData.Instance.asSchool().forFinancialAidAPIApp('Financial Aid API Ravenna').insertAccount();
        Student_Folder__c folder = StudentFolderTestData.Instance.forFolderStatus(originalStatus).forSchoolId(school.Id).insertStudentFolder();

        folder.Folder_Status__c = newStatus;
        update folder;
        Test.stopTest();

        List<Application_Update__e> publishedEvents = ApplicationUpdates.getEvents();
        System.assertEquals(0, publishedEvents.size(), 'Platform Event still published after FinancialAidAPISettings updated to disabled');
    }

    @isTest
    private static void onAfterUpdate_multipleFoldersUpdated_platformEventsEnabled_expectMultipleEvents() {
        FinancialAidAPISettings.setToggle('Application_Update_Event_Trigger_Enabled__c', true);

        Test.startTest();
        String originalStatus = 'Submitted';
        String newStatus = 'Award Proposed';
        List<Student_Folder__c> folders = new List<Student_Folder__c>();

        Account school = AccountTestData.Instance.asSchool().forFinancialAidAPIApp('Financial Aid API Ravenna').insertAccount();

        for (Integer i = 0; i < 10; i++) {
            folders.add(StudentFolderTestData.Instance.forFolderStatus(originalStatus).forSchoolId(school.Id).create());
        }
        insert folders;

        for (Student_Folder__c folder : folders) {
            folder.Folder_Status__c = newStatus;
        }
        update folders;
        Test.stopTest();

        List<Application_Update__e> publishedEvents = ApplicationUpdates.getEvents();
        System.assertEquals(10, publishedEvents.size(), '10 Platform Events werent created');

        for (Application_Update__e eventUpdate : publishedEvents) {
            System.assertEquals(newStatus, eventUpdate.Status__c, 'Folder Status on events wasnt updated');
        }
    }

    @isTest
    private static void onAfterUpdate_folderLoanUpdated_platformEventsEnabled_eventNotCreated() {
        FinancialAidAPISettings.setToggle('Application_Update_Event_Trigger_Enabled__c', true);

        Test.startTest();
        Decimal originalLoan = 100.00;
        Decimal newLoan = 1000.00;

        Account school = AccountTestData.Instance.asSchool().forFinancialAidAPIApp('Financial Aid API Ravenna').insertAccount();
        Student_Folder__c folder = StudentFolderTestData.Instance.forLoanAmount(originalLoan).forSChoolId(school.Id).insertStudentFolder();

        folder.Loan__c = newLoan;
        update folder;
        Test.stopTest();

        List<Application_Update__e> publishedEvents = ApplicationUpdates.getEvents();
        System.assertEquals(0, publishedEvents.size(), 'Platform Event still published after Loan__c (non eventful) field updated');
    }

    @isTest
    private static void onAfterUpdate_folderStatusIsChanged_folderNotAPIEnabled_platformEventsEnabled_eventNotCreated() {
        FinancialAidAPISettings.setToggle('Application_Update_Event_Trigger_Enabled__c', true);

        Test.startTest();
        String originalStatus = 'Submitted';
        String newStatus = 'Award Proposed';
        Account school = AccountTestData.Instance.asSchool().forFinancialAidAPIApp('').insertAccount();
        Student_Folder__c folder = StudentFolderTestData.Instance.forFolderStatus(originalStatus).insertStudentFolder();

        folder.Folder_Status__c = newStatus;
        update folder;
        Test.stopTest();

        List<Application_Update__e> publishedEvents = ApplicationUpdates.getEvents();
        System.assertEquals(0, publishedEvents.size(), 'Platform Event still published with Student folder not being API Enabled');
    }
}