/**
 * LiveAgentDeploymentService.cls
 *
 * @description: Class for getting metadata types FamilyPortalContactPanelController.cls
 * This class is really just needed for the remote action to get the live agent data. 
 *
 * @author: Mike havrilla @ Presence PG
 */


public class LiveAgentDeploymentService extends DataAccessService {

    @RemoteAction
    public static LiveAgentConfigurationWrapper getLiveAgentInformation( String deploymentType) {
        LiveAgentConfigurationWrapper returnVal;

        try {
            LiveAgentConfiguration__mdt configuration = [select Id, LiveAgentButtonId__c, LiveAgentDeploymentId__c, 
                                                                LiveAgentDeploymentUrl__c, LiveAgentChatUrl__c, IsActive__c
                                                           from LiveAgentConfiguration__mdt 
                                                          where IsActive__c = true
                                                            and Deployment_Type__c = :deploymentType
                                                           limit 1];
            // get orgId minus last 3 characters, 
            // the script won't work with the full 18 character org id, 
            //so we reduce it to 15. 
            String orgId = UserInfo.getOrganizationId();
            orgId = orgId.substring(0, orgId.length() - 3);
            
            returnVal = new LiveAgentConfigurationWrapper( configuration.LiveAgentButtonId__c, configuration.LiveAgentDeploymentUrl__c, 
                                                           configuration.LiveAgentChatUrl__c, configuration.LiveAgentDeploymentId__c, 
                                                           orgId); 
        } catch ( Exception e) {
            // log it and bubble to caller for handling
            System.debug( 'DEBUG:::exception in FamilyPortalContactPanelController.getLiveAgentInformation' + e.getMessage());
            throw new DataAccessServiceException( e);
        }

        return returnVal;
    }

    public class LiveAgentConfigurationWrapper {
        public String liveAgentButtonId { get; set; }
        public String liveAgentDeploymentId { get; set; }
        public String liveAgentDeploymentUrl { get; set; }
        public String liveAgentChatUrl { get; set; }
        public String orgId { get; set; }
        public User currentUser { get; set; }
        public Id liveAgentCaseRecordTypeId { get; set; }

        public liveAgentConfigurationWrapper(String liveAgentButtonId, String liveAgentDeploymentUrl, String liveAgentChatUrl,  String liveAgentDeploymentId, String orgId) {
            this.liveAgentButtonId = liveAgentButtonId;
            this.liveAgentDeploymentUrl = liveAgentDeploymentUrl;
            this.liveAgentChatUrl = liveAgentChatUrl;
            this.liveAgentDeploymentId = liveAgentDeploymentId;
            this.orgId = orgId;
            this.currentUser = GlobalVariables.getCurrentUser();
            this.liveAgentCaseRecordTypeId = RecordTypes.familyLiveAgentCaseTypeId;
        }
    }
}