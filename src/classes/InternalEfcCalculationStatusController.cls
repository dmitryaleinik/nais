/*
 * Internal page to view EFC recalc status and to initiate batch processing (see NAIS-502)
 *
 * SL, Exponent Partners, 2013
 */
public with sharing class InternalEfcCalculationStatusController {

    /* PROPERTIES */
    public Integer recalcCountSSS {get; set;}
    public Integer recalcCountRevision {get; set;}
    public Integer batchRecalcCountSSS {get; set;}
    public Integer batchRecalcCountRevision {get; set;}
    public Id batchInstanceId {get; set;}
    /* END PROPERTIES */


    /* INITIALIZATION */
    public InternalEfcCalculationStatusController() {
        recalcCountSSS = [SELECT count() FROM PFS__c WHERE SSS_EFC_Calc_Status__c = :EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE];
        recalcCountRevision = [SELECT count() FROM School_PFS_Assignment__c WHERE Revision_EFC_Calc_Status__c = :EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE];
        batchRecalcCountSSS = [SELECT count() FROM PFS__c WHERE SSS_EFC_Calc_Status__c = :EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE_BATCH];
        batchRecalcCountRevision = [SELECT count() FROM School_PFS_Assignment__c WHERE Revision_EFC_Calc_Status__c = :EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE_BATCH];
    }
    /* END INITIALIZATION */


    /* ACTION METHODS */
    public void recalcSSSForRecalculateBatchStatus() {
        doRecalc(EfcCalculatorBatchProcessor.CalcType.SSS);  
    }
    
    public void recalcRevisionForRecalculateBatchStatus() {
        doRecalc(EfcCalculatorBatchProcessor.CalcType.Revision);
    }
    
    public void recalcAllForRecalculateBatchStatus() {
        doRecalc(null);
    }
    
    public void recalcSSSForRecalculateStatus() {
        doRecalc(EfcCalculatorBatchProcessor.CalcType.SSS, 'SELECT Id FROM PFS__c WHERE SSS_EFC_Calc_Status__c = \'' + EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE + '\'');
    }
    
    public void recalcRevisionForRecalculateStatus() {
        doRecalc(EfcCalculatorBatchProcessor.CalcType.Revision, 'SELECT Id FROM School_PFS_Assignment__c WHERE Revision_EFC_Calc_Status__c = \'' + EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE + '\'');
    }
    
    
    /* END ACTION METHODS */
    
    
    /* HELPER METHODS */
    private void doRecalc() {
        batchInstanceId = EfcCalculatorBatchProcessor.doExecuteBatch();
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Batch process id ' + batchInstanceId + ' has been queued. NOTE: The batch process may take a few minutes to complete. <a href=\"/apexpages/setup/listAsyncApexJobs.apexp\">View Apex Jobs Monitor</a>'));
    }
    
    private void doRecalc(EfcCalculatorBatchProcessor.CalcType calcType) {
        batchInstanceId = EfcCalculatorBatchProcessor.doExecuteBatch(calcType);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Batch process id ' + batchInstanceId + ' has been queued. NOTE: The batch process may take a few minutes to complete. <a href=\"/apexpages/setup/listAsyncApexJobs.apexp\">View Apex Jobs Monitor</a>'));
    }
    
    private void doRecalc(EfcCalculatorBatchProcessor.CalcType calcType, String queryString) {
        batchInstanceId = EfcCalculatorBatchProcessor.doExecuteBatch(calcType, queryString);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Batch process id ' + batchInstanceId + ' has been queued. NOTE: The batch process may take a few minutes to complete. <a href=\"/apexpages/setup/listAsyncApexJobs.apexp\">View Apex Jobs Monitor</a>'));
    }
    /* END HELPER METHODS */
}