/**
* @ InternalPFSCreatorController.cls
* @date 5/29/2014
* @author  Drew Piston for Exponent Partners
* @description Page controller for the InternalPFSController page
*/

/*
* NAIS-1670 Case # 5491: Paper Application Entry - Drew Piston
*
* Copyright Exponent Partners 2014
* Databank will be entering paper PFSs via the following process:
* 1. Search for contact. If found, click button to create the PFS and all associated records in the background. If not found, create new contact and then click button.
* 2. Navigate to PFS and click Family View button. From there, enter application via the family portal interface.
* The work required is to build the button and code to create the PFS and associated records, and create a profile with appropriate permissions for Databank users.
*/

public with sharing class InternalPFSCreatorController {

    public Boolean hasErrors {get; set;}
    public Contact theContact {get; set;}
    public String academicYearId;
    public String academicYearName;
    public PFS__c theNewPFS {get; set;}
    public Boolean existingPFS {get; set;}
    public Boolean mulitpleExistingPFSs {get; set;}
    public List<PFS__c> existingPFSList {get; set;}

    public InternalPFSCreatorController() {
        hasErrors = false;
        existingPFS = false;
        mulitpleExistingPFSs = false;
        Id conId = System.currentPagereference().getParameters().get('contactid');
        if (conId == null){
            hasErrors = true;
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid contactid parameter.'));
        } else {
            try {
                theContact = [Select Id, Household__c, FirstName, Middle_Name__c, LastName, Suffix__c, Salutation, Gender__c, MailingStreet, MailingCity, MailingState, 
                                    MailingCountry, MailingPostalCode, Birthdate, Email, HomePhone, MobilePhone, Phone, Preferred_Phone__c 
                                    from Contact where Id = :conID][0];
                academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
                academicYearName = GlobalVariables.getCurrentAcademicYear().Name;
                if (theContact != null){
                    // looking for existing PFSs
                    existingPFSList = [Select Id from PFS__c where Parent_A__c = :theContact.ID AND Academic_Year_Picklist__c = :academicYearName];
                    if (existingPFSList.size() > 0){
                        existingPFS = true;
                    }
                }
            } catch (Exception e){
                System.debug(LoggingLevel.ERROR, 'ERROR FINDING CONTACT with id: ' + conId);
                hasErrors = true;
            }
        }


    }

    public void init(){
        // if we didn't get any errors and found a contact and year...
        if (!hasErrors && theContact != null && academicYearId != null){
            // ... create a new PFS if one doesn't already exist
            if (!existingPFS){
                createPFS();
                apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'A PFS was successfully created.'));
            } else {
                // if an existing PFS does exist, we'll display some informative messages. user can still create the pfs via a link on the page.
                if (existingPFSList.size() > 1){
                    apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Multiple PFSs already exist for this academic year.'));
                    mulitpleExistingPFSs = true;
                } else {
                    apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'A PFS already exists for this academic year.'));
                }
            }
        } else {
            // if something didn't happen correctly, we'll show an error message
            hasErrors = true;
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error finding Contact or Academic Year.'));
        }
    }

    // called either from init method or from commandlink
    public void createPFS(){
        if (theContact.Household__c == null){
            Household__c hh = new Household__c();
            hh.Name = theContact.FirstName + ' ' + theContact.LastName;
            insert hh;

            theContact.Household__c = hh.Id;
            update theContact;
        }

        existingPFS = false;
        mulitpleExistingPFSs = false;
        theNewPFS = ApplicationUtils.generateBlankPFS(academicYearId, theContact);
    }
}