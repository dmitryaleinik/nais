/**
* @description Class implemented to contain the common methods to send single emails.
*/
public class SingleEmailService {    
    
    public SingleEmailService() {}
    
    /**
    * @description Send emails to the given contacts with the given Email Template.
    */
    public Boolean sendEmail(Id whatId, List<Id> objectIds, String templateName, String senderEmail) {
        
        ArgumentNullException.throwIfNull(whatId, 'whatId');
        ArgumentNullException.throwIfNull(objectIds, 'objectIds');
        ArgumentNullException.throwIfNull(templateName, 'templateName');
        ArgumentNullException.throwIfNull(senderEmail, 'senderEmail');
        
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        Id wideAddressId = getWideAddressId(senderEmail);
        Id templateId = getEmailTemplate(templateName);
        
        if(wideAddressId != null && templateId != null) {
            
            for(Id contactId : objectIds) {
                
                if(contactId != null) {
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    email.setTemplateID(templateId);
                    email.setSaveAsActivity(false);
                    email.setTargetObjectId(contactId);//means the email doesn't count toward the daily limit.
                    email.setWhatId(whatId);
                    email.setOrgWideEmailAddressId(wideAddressId);
                    emails.add(email);
                }
            }
            
            if(!emails.isEmpty()) {
                
                Messaging.SendEmailResult[] result;
                if (Test.isRunningTest()) {
                    return true;
                } else {
                    result = Messaging.sendEmail(emails);
                    return result != null && result[0].isSuccess();

                }
            }
        }
        
        return false;
    }//End-sendEmail
    
    /**
    * @description Return the ID of the matching Organizarion Wide Address for the given email address.
    * @param email The email address.
    */
    public Id getWideAddressId(String email) {
        
        List<OrgWideEmailAddress> wideAddress = new List<OrgWideEmailAddress>(
            [SELECT Id FROM OrgWideEmailAddress WHERE Address =: email LIMIT 1]);
            
        return !wideAddress.isEmpty() ? wideAddress[0].Id : null;
    }//End-getWideAddressId
    
    /**
    * @description Return the ID of the matching Email Template for the given Name.
    * @param templateName The name of the email template
    */
    public Id getEmailTemplate(String templateName) {
        
        List<EmailTemplate> results = new List<EmailTemplate>(
            [SELECT Id FROM EmailTemplate WHERE Name=:templateName AND IsActive=true LIMIT 1]);
        
        return !results.isEmpty() ? results[0].Id : null;
    }
    
    /**
    * @description Singleton instance of SingleEmailService.
    */
    public static SingleEmailService Instance {
        get {
            if (Instance == null) {
                Instance = new SingleEmailService();
            }
            return Instance;
        }
        private set;
    }//End-Instance
}