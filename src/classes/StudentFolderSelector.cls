/**
 * @description This class is responsible for querying Student Folder records.
 */
public class StudentFolderSelector extends fflib_SObjectSelector {

    @testVisible private static final String STUDENT_FOLDER_IDS_PARAM  = 'studentFolderIds';
    @testVisible private static final String STUDENT_FOLDER_FIELDS_TO_SELECT_PARAM = 'fieldsToSelect';
    @testVisible private static final String STUDENT_IDS = 'studentIds';
    @testVisible private static final String SCHOOL_IDS = 'schoolIds';
    @testVisible private static final String ACADEMIC_YEARS = 'academicYears';

    /**
     * @description Default constructor for an instance of the StudentFolderSelector that will not include 
     *              field sets while enforcing FLS and CRUD.
     */
    public StudentFolderSelector() { }
    
    /**
     * @description Queries student folder records by ids with custom field list.
     * @param studentFolderIds The ids of the student folder records to query.
     * @param fieldsToSelect The list of fields to query.
     * @return A list of student folder records.
     * @throws ArgumentNullException if studentFolderIds is null.
     * @throws ArgumentNullException if fieldsToSelect is null.
     */
    public List<Student_Folder__c> selectByIdWithCustomFieldList(Set<Id> studentFolderIds, List<String> fieldsToSelect) {
        ArgumentNullException.throwIfNull(studentFolderIds, STUDENT_FOLDER_IDS_PARAM);
        ArgumentNullException.throwIfNull(fieldsToSelect, STUDENT_FOLDER_FIELDS_TO_SELECT_PARAM);

        assertIsAccessible();

        String query = newQueryFactory(false)
                .selectFields(fieldsToSelect)
                .setCondition('Id IN :studentFolderIds')
                .toSOQL();

        return Database.query(query);
    }

    /**
     * @description Queries student folder records by StudentIds, SchoolIds and AcademicYear Names.
     * @param studentIds Ids of schools for querying Student Folder records.
     * @param schoolIds Ids of students for querying Student Folder records.
     * @param academicYears Names of Academic Years for querying Student Folder records.
     * @return A list of student folder records.
     * @throws ArgumentNullException if studentIds is null.
     */
    public List<Student_Folder__c> selectByStudentId_schoolId_academicYearName(
        Set<Id> studentIds, Set<Id> schoolIds, Set<String> academicYears) {
        ArgumentNullException.throwIfNull(studentIds, STUDENT_IDS);

        assertIsAccessible();

        String conditionString = 'Student__c IN :studentIds';
        
        if(schoolIds != null && !schoolIds.isEmpty()) {
            conditionString += ' AND School__c IN :schoolIds';
        }

        if(academicYears != null && !academicYears.isEmpty()) {
            conditionString += ' AND Academic_Year_Picklist__c IN :academicYears';
        }

        String query = newQueryFactory(true)
            .setCondition(conditionString)
            .toSOQL();

        return Database.query(query);
    }
    
    /**
     * @description Queries student folder with budget allocations records by StudentId, SchoolId and AcademicYear Name.
     * @return A list of Student Folder records.
     */
    public List<Student_Folder__c> selectByStudentSchoolAndYearWithBudgetAllocations(
        Id studentId, Id schoolId, String academicYear) {
        
        return selectByStudentSchoolAndYearWithBudgetAllocations(
        new Set<Id>{studentId}, new Set<Id>{schoolId}, new Set<String>{academicYear});
    }

    /**
     * @description Queries student folder with budget allocations records by StudentIds, SchoolIds and AcademicYear Names.
     * @return A list of Student Folder records.
     */
    public List<Student_Folder__c> selectByStudentSchoolAndYearWithBudgetAllocations(
        Set<Id> studentIds, Set<Id> schoolIds, Set<String> academicYears) {
        
        ArgumentNullException.throwIfNull(studentIds, STUDENT_IDS);
        ArgumentNullException.throwIfNull(schoolIds, SCHOOL_IDS);
        ArgumentNullException.throwIfNull(academicYears, ACADEMIC_YEARS);

        assertIsAccessible();

        String conditionString = 
            'Student__c IN :studentIds ' +
            'AND School__c IN :schoolIds ' +
            'AND Academic_Year_Picklist__c IN :academicYears ';
        
        fflib_QueryFactory studentFolderQueryFactory = newQueryFactory();

        fflib_QueryFactory budgetAllocationsQueryFactory =
	        BudgetAllocationSelector.newInstance()
            .addQueryFactorySubselect(studentFolderQueryFactory);
	            
        BudgetGroupSelector.newInstance()
            .configureQueryFactoryFields(budgetAllocationsQueryFactory, 'Budget_Group__c');
        
        
        String query = studentFolderQueryFactory
            .setCondition(conditionString)
            .toSOQL();
        return Database.query(query);
    }

    /**
     * @description Queries all Student Folder records.
     * @return A list of Student Folder records.
     */
    public List<Student_Folder__c> selectAll() {
        return Database.query(newQueryFactory().toSOQL());
    }

    private Schema.SObjectType getSObjectType() {
        return Student_Folder__c.SObjectType;
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                Student_Folder__c.Name,
                Student_Folder__c.Academic_Year_Picklist__c,
                Student_Folder__c.School__c,
                Student_Folder__c.Student__c
        };
    }

    /**
     * @description Creates a new instance of the StudentFolderSelector.
     * @return A StudentFolderSelector.
     */
    public static StudentFolderSelector newInstance() {
        return new StudentFolderSelector();
    }
}