/**
 * Class: SchoolCarryoverPJSettingController
 *
 * Copyright (C) 2015 NAIS
 *
 * Purpose: Add functionality to allow users to carryover the Professional Judgment settings 
 * they made in a prior year to the current year. 
 * (Only if the user don't have annual setting in  the current year)
 * 
 * 
 * Where Referenced:
 *        - SchoolPortal - SchoolCarryoverPJSettings page
 *
 * Change History:
 *
 * Developer           Date        JIRA Ref     Description
 * ---------------------------------------------------------------------------------------
 * Andrea Useche       05/26/15    NAIS-2357   Initial Development
 *
 * 
 */
public with sharing class SchoolCarryoverPJSettingController{
    /*Initialization*/  
    public SchoolCarryoverPJSettingController(ApexPages.StandardController stdController)
    {
        annualSetting = (Annual_Setting__c)stdController.getRecord();
        
    }
    /*End Initialization*/
   
    /*Properties*/
    public Annual_Setting__c annualSetting {get;set;}
    public string buttonMode {get;set;}
    
    public Id currentAcademicYearId{
        get{
            if(currentAcademicYearId== null)
                currentAcademicYearId = ApexPages.currentPage().getParameters().get('currentAcademicyearId');     
            return currentAcademicYearId;
        }
        set;}
    /*End Properties*/       
    /*Action Methods*/    
    public PageReference cancel()
    {
        PageReference PJSetting = new PageReference('/apex/SchoolGlobalProJudgment');
        PJSetting.setRedirect(true);
        return PJSetting;
    }  
    public pageReference copyPJSetting()
    {    
        if(buttonMode=='Cancel')
            return null;
        Annual_Setting__c myAnnualSettings = new Annual_Setting__c();
        list<Annual_Setting__c> lstAnnualSetting = GlobalVariables.getCurrentAnnualSettings(false,currentAcademicYearId);
        if(lstAnnualSetting.size()> 0)  
        {              
            myAnnualSettings = lstAnnualSetting.get(0); 
            
            myAnnualSettings.Apply_Minimum_Income__c = annualSetting.Apply_Minimum_Income__c;
            myAnnualSettings.Minimum_Income_Amount__c = annualSetting.Minimum_Income_Amount__c;
            myAnnualSettings.Add_Depreciation_Home_Business_Expense__c = annualSetting.Add_Depreciation_Home_Business_Expense__c;
            myAnnualSettings.Use_Home_Equity__c = annualSetting.Use_Home_Equity__c;
            myAnnualSettings.Use_Home_Equity_Cap__c = annualSetting.Use_Home_Equity_Cap__c;
            myAnnualSettings.Use_Housing_Index_Multiplier__c = annualSetting.Use_Housing_Index_Multiplier__c;
            myAnnualSettings.Use_Dividend_Interest_to_Impute_Assets__c = annualSetting.Use_Dividend_Interest_to_Impute_Assets__c;
            myAnnualSettings.Use_Cost_of_Living_Adjustment__c = annualSetting.Use_Cost_of_Living_Adjustment__c;
            myAnnualSettings.Impute_Rate__c = annualSetting.Impute_Rate__c;
            myAnnualSettings.COLA__c = annualSetting.COLA__c;
            myAnnualSettings.Override_Default_COLA_Value__c = annualSetting.Override_Default_COLA_Value__c;
            myAnnualSettings.Adjust_Housing_Portion_of_IPA__c = annualSetting.Adjust_Housing_Portion_of_IPA__c;
            myAnnualSettings.IPA_Housing_Family_of_4__c = annualSetting.IPA_Housing_Family_of_4__c; 
            myAnnualSettings.Professional_Judgment_Updated__c =system.today();
            
            update myAnnualSettings;
        }        
        if(buttonMode!='Future') 
            updateSPAs();
        return null;
    }
    /*End Action Methods*/   
    /*Helper Methods*/       
    private void updateSPAs()
    {        
        Id mySchoolId = GlobalVariables.getCurrentSchoolId();
        Academic_Year__c myAcademicYear = GlobalVariables.getCurrentAcademicYear();
        if ((annualSetting.Use_Cost_of_Living_Adjustment__c == 'Yes') && (annualSetting.COLA__c != null)) {
            COLA__c cola = [select SSS_COLA__c from COLA__c where Id = :annualSetting.COLA__c];
            if ((cola != null) && (cola.SSS_COLA__c != null)) 
                annualSetting.Override_Default_COLA_Value__c = cola.SSS_COLA__c;                
        }    
        list<School_PFS_Assignment__c> SPAs = [select Add_Deprec_Home_Bus_Expense__c, Use_Home_Equity__c,
                                                     Use_Home_Equity_Cap__c, Use_Housing_Index_Multiplier__c, 
                                                    Use_Div_Int_to_Impute_Assets__c, 
                                                    Impute__c, Adjust_Housing_Portion_of_IPA__c, IPA_Housing_Family_of_4__c,
                                                    Apply_Minimum_Income__c, Minimum_Income_Amount__c, 
                                                    Use_COLA__c, Override_Default_COLA_Value__c, Lock_Professional_Judgment__c,
                                                    Academic_Year_Picklist__c
                                                    from School_PFS_Assignment__c
                                                    where School__c = :mySchoolId 
                                                    and Academic_Year_Picklist__c = :myAcademicYear.Name];
        
        List<School_PFS_Assignment__c> spasToUpdate = new List<School_PFS_Assignment__c>{};    
        
        // Iterate through the list of related SPA records
        for (School_PFS_Assignment__c spa : SPAs) 
        {            
            // If we should try to update the current record
            if(buttonMode == 'All' || (buttonMode == 'Unlocked' && spa.Lock_Professional_Judgment__c == 'No'))
            {
                                
                if(spa.Add_Deprec_Home_Bus_Expense__c != annualSetting.Add_Depreciation_Home_Business_Expense__c)
                    spa.Add_Deprec_Home_Bus_Expense__c = annualSetting.Add_Depreciation_Home_Business_Expense__c;                
                if(spa.Use_Home_Equity__c != annualSetting.Use_Home_Equity__c)
                    spa.Use_Home_Equity__c = annualSetting.Use_Home_Equity__c;                        
                if(spa.Use_Home_Equity_Cap__c != annualSetting.Use_Home_Equity_Cap__c)
                    spa.Use_Home_Equity_Cap__c = annualSetting.Use_Home_Equity_Cap__c;                    
                if(spa.Use_Housing_Index_Multiplier__c != annualSetting.Use_Housing_Index_Multiplier__c)
                    spa.Use_Housing_Index_Multiplier__c = annualSetting.Use_Housing_Index_Multiplier__c;
                if((spa.Use_Div_Int_to_Impute_Assets__c != annualSetting.Use_Dividend_Interest_to_Impute_Assets__c)
                    || (spa.Impute__c != annualSetting.Impute_Rate__c))                     
                { 
                    spa.Use_Div_Int_to_Impute_Assets__c = annualSetting.Use_Dividend_Interest_to_Impute_Assets__c;
                    spa.Impute__c = annualSetting.Impute_Rate__c;        
                }                
                if((spa.Apply_Minimum_Income__c != annualSetting.Apply_Minimum_Income__c)
                    || (spa.Minimum_Income_Amount__c != annualSetting.Minimum_Income_Amount__c)) 
                { 
                    spa.Apply_Minimum_Income__c = annualSetting.Apply_Minimum_Income__c;
                    spa.Minimum_Income_Amount__c = annualSetting.Minimum_Income_Amount__c;                        
                }                
                if((spa.Use_COLA__c != annualSetting.Use_Cost_of_Living_Adjustment__c) 
                    || (spa.Override_Default_COLA_Value__c != annualSetting.Override_Default_COLA_Value__c))
                        
                {
                    spa.Use_COLA__c = annualSetting.Use_Cost_of_Living_Adjustment__c;                        
                    spa.Override_Default_COLA_Value__c = annualSetting.Override_Default_COLA_Value__c;            
                }
                if((spa.Adjust_Housing_Portion_of_IPA__c != annualSetting.Adjust_Housing_Portion_of_IPA__c) || 
                    (spa.IPA_Housing_Family_of_4__c != annualSetting.IPA_Housing_Family_of_4__c ))
                { 
                    spa.Adjust_Housing_Portion_of_IPA__c = annualSetting.Adjust_Housing_Portion_of_IPA__c;
                    spa.IPA_Housing_Family_of_4__c = annualSetting.IPA_Housing_Family_of_4__c ;        
                }
                spasToUpdate.add(spa);
            }
        }
        if(spasToUpdate.size()>0)
        {
            try{            
                BatchGenericUpsert batchUpsert = new BatchGenericUpsert();
                batchUpsert.recordsToUpsert = spasToUpdate;
                Database.executeBatch(batchUpsert);            
            }
            catch(Exception e){
                System.debug('Error Updating SPA: ' + e.getCause() + '\n' + e.getLineNumber() + '\n' + e.getMessage() + '\n' + e.getStackTraceString());
                throw e;
            }
        }        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Applied to ' + SPAs.size() + ' PFSs. (' + spasToUpdate.size() + ' changed.)'));
    } 
    /*End Helper Methods*/
}