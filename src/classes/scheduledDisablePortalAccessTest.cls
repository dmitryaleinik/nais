@isTest
private class scheduledDisablePortalAccessTest {

    @isTest
    private static void testSchedule() {
        Test.startTest();
            ScheduleSharingJobsController.deleteJobsRemotely();
            
            Id job0Id = System.schedule('Test Schedule', '0  0 * * * ?', new scheduledDisablePortalAccess());
        Test.stopTest();

        CronTrigger ct = [select Id, CronExpression from CronTrigger where Id = :job0Id];
        System.assertEquals('0  0 * * * ?', ct.CronExpression);
    }
    
    @isTest
    private static void userPortalEnabledActionBatch_deletesSPUserInBucketAccount_deletesSPUserIn1To1Account() {
        
        Contact thisContact = ContactTestData.Instance
            .forFirstName('Test Jane')
            .forLastName('Test Doe')
            .asSchoolStaff()
            .create();
        thisContact.Deleted__c = true;
        insert thisContact;
        
        Test.startTest();
            Database.executeBatch(new userPortalEnabledActionBatch(new Set<Id>{thisContact.Id}));            
        Test.stopTest();
        
        thisContact = [SELECT Id, Account.Name, FirstName, LastName FROM Contact WHERE Id =: thisContact.Id LIMIT 1];
        
        System.AssertEquals(thisContact.Account.Name, thisContact.FirstName + ' ' + thisContact.LastName + ' Account');
    }
}