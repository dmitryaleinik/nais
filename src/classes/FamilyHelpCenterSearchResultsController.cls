public class FamilyHelpCenterSearchResultsController {
    public FamilyTemplateController controller { get; set; }
    public List<ArticleListWrapper> articles { get; set; }
    public String jumpToPage { get; set; }
    public String searchString { get; set; }
    private Static Final Integer PAGE_NUMBER = 5;

    //Keeps track of current page & max size of article list
    Integer currentPage = 1;
    Integer maxSize = 1;

    // ctor
    public FamilyHelpCenterSearchResultsController( FamilyTemplateController controller) {
        this.controller = controller;

        if( this.controller.pfsRecord == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error: No PFS Record Found for current user.'));
            this.controller.pfsRecord = new PFS__c();
        }

        searchString = ApexPages.currentPage().getParameters().get('searchq');
        String page = ApexPages.currentPage().getParameters().get('page');
        if( page != null) {
            currentPage = Integer.valueOf( page);
        }

        // If Search string is empty do nothing. 
        if( String.isEmpty( searchString)) {
            return;
        } else {
            searchString = EncodingUtil.urlDecode(searchString, 'UTF-8');
        }

        maxSize = new KnowledgeDataAccessService().getKnowledgeArticleByKeywordCount( searchString);

        intializeRecords();
        
    }

    public static FamilyTemplateController.config loadPFS( String pfsId, String academicYearId) {
        PFS__c pfsRecord;
        User currentUser = [Select Id, ContactId, ProfileId from User where Id = :UserInfo.getUserId()];
        Academic_Year__c academicYear = [select Id, Name from Academic_Year__c where Id = :academicYearId];
        
        if ( GlobalVariables.isSysAdminUser(currentUser) || GlobalVariables.isCallCenterUser(currentUser)){
            pfsRecord = ApplicationUtils.queryPFSRecord( pfsId, null, null);
            // if this not a sys admin or call center user, get record based on contact id and academic year
        } else {
            pfsRecord = ApplicationUtils.queryPFSRecord( null, currentUser.ContactId, academicYearId);
        }
        
        return new FamilyTemplateController.config( pfsRecord, null);
    }//End:loadPFS

    // Initalized all the records. Gets the list of search results by keyword and offset and inflates the wrapper object. 
    public void intializeRecords() {
        Integer offset = ( PAGE_NUMBER * this.currentPage) - PAGE_NUMBER;
        Search.SearchResults searchResults = new KnowledgeDataAccessService().searchKnowledgeArticlesByKeyword( searchString, offset, PAGE_NUMBER);

        List<Search.SearchResult> articlelist = searchResults.get( 'Knowledge__kav');

        articles = new List<ArticleListWrapper>();
        for ( Search.SearchResult searchResult : articleList) { 
            Knowledge__kav article = (Knowledge__kav) searchResult.getSObject();
            ArticleListWrapper articleWrapper = new ArticleListWrapper();
            articleWrapper.Id = article.KnowledgeArticleId;
            articleWrapper.Title = article.Title;
            articleWrapper.Summary = article.Summary;
            articleWrapper.LastPublishedDate = article.LastPublishedDate;
            articleWrapper.Snippet = searchResult.getSnippet();
            articles.add( articleWrapper);
        } 
    }

    // Returns whether we need to see previous button or not
    public boolean getPrevRequired() {
        return currentPage > 1;
    }

    // Returns whether we need to see next button or not
    public boolean getNextRequired() {
        return currentPage * PAGE_NUMBER < maxSize;
    }
     
    //Returns current page number 
    public Decimal getCurrentPageNumber() {
        return this.currentPage;
    }

    // Builds a display for total number of records on a page. (i.e. 1-5 or 6-10)
    public String getDisplayResults() {
        Integer maxResult = this.currentPage * PAGE_NUMBER;
        Integer minResult = ( this.currentPage -1) * PAGE_NUMBER + 1;

        if( maxResult > this.maxSize) {
            maxResult = this.maxSize;
        }

        String returnVal = String.valueOf( minResult) + ' - ' + String.valueOf( maxResult);
        return returnVal;
    }

    // get page list values. To display for pagination.
    public List<String> getPageList() {
        List<String> pages = new List<String>();
        for( Integer i = 1; i <= getTotalPages(); i++) {
            if( i <= 5 ) {
                pages.add( String.valueOf( i));
            }
        }
        return pages;
    }

    // Returns the total number of articles returned. 
    public Integer getTotalRecords() {
        return this.maxSize;
    }

    // Returns the total number of pages. (Rounds up)
    public Integer getTotalPages() {
        Decimal totalPages = (Decimal) maxSize;
        Decimal numberPerPage = (Decimal) PAGE_NUMBER;
        return (Integer) Math.ceil( totalPages / numberPerPage); 
    }

    // Returns the return url for going to a selected article. 
    public String getReturnUrl() {
        String returnUrl = '/FamilyHelpCenterSearchResults?';
        returnUrl += 'id=' + this.controller.pfsRecord.Id;

        if( String.isNotEmpty( searchString)) {
            returnUrl += '&searchq=' + searchString;
        }        
        returnUrl += '&page=' + this.currentPage;

        String encodedReturn = EncodingUtil.urlEncode(returnUrl, 'UTF-8');
        return encodedReturn;
    }

    // Returns page refernce to go to a specific page
    public PageReference goToPage( String pageNumber) {
        PageReference nextPage = Page.FamilyHelpCenterSearchResults;
        nextPage.getParameters().put('id', this.controller.pfsRecord.Id);
        nextPage.getParameters().put('searchq', searchString);
        nextPage.getParameters().put('page', pageNumber);
        nextPage.setRedirect( true);
        return nextPage;
    }

    // action to skip to a page
    public PageReference skipToPage() {
        return goToPage( String.valueOf( jumpToPage));
    }

    //action for next click
    public PageReference next() {
        if( this.maxSize > this.currentPage * PAGE_NUMBER) {
            this.currentPage = this.currentPage + 1;
        }

        return goToPage( String.valueOf( this.currentPage));
    }

    //action for previous click
    public PageReference previous() {        
        if(this.currentPage > 1) {
            this.currentPage = this.currentPage - 1;
        }

        return goToPage( String.valueOf(this.currentPage));
    }

    // Inner class Article List wrapper.
    public class ArticleListWrapper {
        public String id { get; set; }
        public String title { get; set; }
        public String snippet { get; set; }
        public DateTime lastPublishedDate { get; set; }
        public String summary { get; set; }

        public ArticleListWrapper() {}
    }
}