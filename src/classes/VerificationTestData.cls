/**
 * @description This class is used to create Verification records for unit tests.
 */
@isTest
public class VerificationTestData extends SObjectTestData {

    /**
     * @description Get the default values for the Verification__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {};
    }

    /**
     * @description Insert the current working Verification__c record.
     * @return The currently operated upon Verification__c record.
     */
    public Verification__c insertVerification() {
        return (Verification__c)insertRecord();
    }

    /**
     * @description Create the current working Verification record without resetting
     *              the stored values in this instance of PfsTestData.
     * @return A non-inserted Verification record using the currently stored field values.
     */
    public Verification__c createVerificationWithoutReset() {
        return (Verification__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Verification__c record.
     * @return The currently operated upon Verification__c record.
     */
    public Verification__c create() {
        return (Verification__c)super.buildWithReset();
    }

    /**
     * @description The default Verification__c record.
     */
    public Verification__c DefaultVerification {
        get {
            if (DefaultVerification == null) {
                DefaultVerification = createVerificationWithoutReset();
                insert DefaultVerification;
            }
            return DefaultVerification;
        }
        private set;
    }

    /**
     * @description Get the Verification__c SObjectType.
     * @return The Verification__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Verification__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static VerificationTestData Instance {
        get {
            if (Instance == null) {
                Instance = new VerificationTestData();
            }
            return Instance;
        }
        private set;
    }

    private VerificationTestData() { }
}