/*
 * Spec-144 School Portal - School Profile;
 * BR, Exponent Partners, 2013
 */

public with sharing class SchoolSchoolProfileController implements SchoolAcademicYearSelectorInterface{

    /*Initialization*/
    
    public SchoolSchoolProfileController() {
            
        Id schoolId = GlobalVariables.getCurrentSchoolId();

        if (schoolId != null){
            mySchool = [SELECT Id, Name, Phone, Fax, Website, SSS_School_Code__c, NAIS_Member_Type__c, 
                             NAIS_School_Code__c, School_Type__c, Alert_Frequency__c, Profile_Updated__c,
                            BillingStreet, BillingCity, BillingStateCode, BillingPostalcode, BillingCountryCode, 
                            Paid_Through__c
                            FROM Account
                            WHERE Id = :schoolId];
            //NAIS-2155 Start
            List<Subscription__c> subscriptions = [SELECT Id, Name, Status__c, End_Date__c
                                    FROM Subscription__c
                                    WHERE Account__c = :schoolId
                                    order by End_Date__c desc];
                                    
            if(!subscriptions.isEmpty()){
                schoolSubscription = subscriptions[0];    
            }
            else{
                schoolSubscription = new Subscription__c();
            }
            
            //NAIS-2155 End
        }
        
        dummyValue='none'; // [DP] 03.20.2015 don't think this is used anywhere!
    }

    /*Properties*/
    
    public Account mySchool { get; set; }
    public Subscription__c schoolSubscription { get; set; } //NAIS-2155
    public SchoolSchoolProfilecontroller Me {
        get { return this; }
    }
    public String dummyValue {get; set;}
    public String getTabStyle(){
        String tabStyle = 'Setup2__tab';
        if (GlobalVariables.isLimitedSchoolView()){
            tabStyle = 'School_Profile__tab';
        }
        return tabStyle;
    }
    public boolean getRenderRenewSubscription() {
        if(mySchool.Paid_Through__c == null) { 
            return true; 
        }else if(mySchool.Paid_Through__c.year() < System.today().year()) { 
            return true; 
        }else if(mySchool.Paid_Through__c.year() == System.today().year() && GlobalVariables.isRenewalSeason()) {
            return true;
        }
        return false;
    }    
    /*Action Methods*/
    public PageReference goToRenewal() {
        return Page.SchoolRenewal.setRedirect(true);
    }
    // Naming this 'save' causes the Save button to disappear in portal. Why?
    public PageReference saveSettings() {
        
        mySchool.Profile_Updated__c = System.today();
        update mySchool;
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Profile Saved.'));
        return null;
    }

    public PageReference cancel() {
        //PageReference clearPage = new PageReference('/apex/SchoolSchoolProfileWrapper');
        PageReference clearPage = Page.SchoolSchoolProfileWrapper;
        clearPage.setRedirect(true);
        return clearPage;
    }
    
    public Boolean getRenderSubTabs(){
        Boolean b = !GlobalVariables.isLimitedSchoolView();
        return b;
    }
    
    /* SchoolAcademicYearSelectorInterface Implementation*/
    public String newAcademicYearId;
    public String getAcademicYearId() {return null;}
    public void setAcademicYearId(String academicYearId) {}
    public PageReference SchoolAcademicYearSelector_OnChange(Boolean saveRecord) {return null;}    
    /* End SchoolAcademicYearSelectorInterface Implementation*/
    
    /*Page Action*/
    public pagereference checkRedirect() {
        Account school = [SELECT Paid_Through__c
                            FROM Account
                            WHERE Id = :GlobalVariables.getCurrentSchoolId()];
        if(school.Paid_Through__c == null || (school.Paid_Through__c != null && school.Paid_Through__c < System.today())) {
            return Page.SchoolRenewal.setRedirect(true);
        }
        return null;
    }
}