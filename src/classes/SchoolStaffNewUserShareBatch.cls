/*
 * NAIS-1617 New Process for BulkSharing
 *    Delegate sharing in Trigger on User, Affiliation, and Contact to scheduled batch job on User when synchronous sharing would have exceeded DML Limit.
 *    Process *all* School PFS Assignment records to share PFS and Student Folder records with *new* school users.
 *
 *    Tests in SchoolStaffShareActionTest.cls
 *
 * WH, Exponent Partners, 2014
 */
 /* [CH] NAIS-1766 This should not be needed any more now that Users are added as members to Public Groups synchronously
  *                 Commented out references in SharingScheduler.cls
 */
global class SchoolStaffNewUserShareBatch implements Database.Batchable<sObject>, Database.Stateful {

    global Set<Id> newUserIds;
    global final Set<Id> schoolIds;
    global final Map<Id, Set<Id>> newUsersByAccountId;

    global SchoolStaffNewUserShareBatch() {

        newUserIds = new Set<Id>();
        newUsersByAccountId = new Map<Id, Set<Id>>();

        Map<Id, Id> userIdByContactId = new Map<Id, Id>();

        // pick up all new users to be processed; populate a map of Users by Account
        for (User u : [select Id, AccountId, ContactId from User where Process_Bulk_Share__c = true]) {
            newUserIds.add(u.Id);
            userIdByContactId.put(u.ContactId, u.Id);

            Set<Id> userIds = newUsersByAccountId.get(u.AccountId);
            if (userIds == null) {
                userIds = new Set<Id>();
                newUsersByAccountId.put(u.AccountId, userIds);
            }
            userIds.add(u.Id);
        }

        // add other school Accounts these new users are affiliated with
        for (Affiliation__c aff : [select Contact__c, Organization__c, Organization__r.RecordTypeId
                                            from Affiliation__c
                                            where Contact__c in :userIdByContactId.keySet()
                                                and Organization__r.RecordTypeId = :RecordTypes.schoolAccountTypeId
                                                and Status__c = 'Current']) {
            Set<Id> userIds = newUsersByAccountId.get(aff.Organization__c);
            if (userIds == null) {
                userIds = new Set<Id>();
                newUsersByAccountId.put(aff.Organization__c, userIds);
            }
            userIds.add(userIdByContactId.get(aff.Contact__c));
        }

        // list of school Accounts for which all applications are to be processed to share records with new users
        schoolIds = newUsersByAccountId.keySet();
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        // process all applications ordered by school to keep them in adjacent batches
        String query = 'select Id, School__c from School_PFS_Assignment__c where School__c in :schoolIds order by School__c';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        List<Id> spfsaIdList = new List<Id>();
        List<Id> userIds = new List<Id>();
        for (sObject s : scope) {
            spfsaIdList.add(s.Id);
        }
        userIds.addAll(newUserIds);
        if (spfsaIdList.size() > 0) {
            try {
                // share records to new users only
                /* [CH] NAIS-1766 This should not be needed any more now that Users are added as members to Public Groups synchronously
                * SchoolStaffShareAction.shareRecordsToSchoolUsers(spfsaIdList, userIds);
                */
            } catch (Exception e) {
                // error in processing some applications to this batch of schools; users of these Accounts would need to be reprocessed
                for (sObject s : scope) {
                    School_PFS_Assignment__c spfsa = (School_PFS_Assignment__c)s;
                    Set<Id> usersWithError = newUsersByAccountId.get(spfsa.School__c);
                    newUserIds.removeAll(usersWithError);
                }
            }
        }
    }

    global void finish(Database.BatchableContext bc) {
        // reset flag on all new users that have been processed without error
        List<User> usersToUpdate = new List<User>();
        for (Id uId : newUserIds) {
            usersToUpdate.add(new User(Id = uId, Process_Bulk_Share__c = false));
        }
        if (!usersToUpdate.isEmpty()) {
            update usersToUpdate;
        }
    }

}