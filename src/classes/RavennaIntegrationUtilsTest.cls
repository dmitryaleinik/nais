@isTest
private class RavennaIntegrationUtilsTest {

    private static IntegrationApiModel.ApiState createEmptyApiState() {
        return new IntegrationApiModel.ApiState('Ravenna', new PFS__c(), new Map<String, IntegrationApiModel>());
    }

    private static IntegrationApiModel.ApiState createMockApiState(Applicant__c ravennaApplicant, String ravennaId, String ravennaEthnicity) {
        IntegrationApiModel.ApiState apiState = createEmptyApiState();

        addStudentResponseToApiState(apiState, ravennaId, ravennaEthnicity);

        // Store the applicant we are testing on in the api state to mock what we expect to happen by the time our class is called.
        // When the integration upserts applicants, we expect them to be stored here.
        apiState.familyModel.applicantsToUpsert = new List<Applicant__c> { ravennaApplicant };

        return apiState;
    }

    private static void addStudentResponseToApiState(IntegrationApiModel.ApiState apiState, String ravennaId, String ravennaEthnicity) {
        // First create a single student response.
        Map<String, Object> studentResponse = createMockStudentResponse(ravennaId, ravennaEthnicity);

        // Now add that single response to a list. We store multiple responses per student Id (ravenna Id) since we might
        // actually get multiple results when we use the ravenna API.
        List<Object> studentResponses = new List<Object> { studentResponse };

        apiState.familyModel.studentIdToResponseMap.put(ravennaId, studentResponses);
    }

    // This method creates a single student response the way we deserialize the results from the ravenna API.
    private static Map<String, Object> createMockStudentResponse(String ravennaId, String ravennaEthnicity) {
        Map<String, Object> studentResponse = new Map<String, Object>();

        studentResponse.put('id', ravennaId);
        studentResponse.put(RavennaIntegrationUtils.RAVENNA_ETHNICITY_FIELD, ravennaEthnicity);

        return studentResponse;
    }

    private static Contact createExpectedContact(Id contactId, String expectedRavennaId, String expectedEthnicity) {
        return new Contact(Id = contactId, RavennaId__c = expectedRavennaId, Ethnicity__c = expectedEthnicity);
    }

    private static Map<Id, Contact> getContactsById(Set<Id> idSet) {
        return new Map<Id, Contact>([SELECT Id, RavennaId__c, Ethnicity__c FROM Contact WHERE Id IN :idSet]);
    }

    private static void assertContactsUpdatedCorrectly(Map<Id, Contact> expectedContactsById, Map<Id, Contact> actualContactsById) {
        // First we will just make sure the right number of records were updated.
        System.assertEquals(expectedContactsById.size(), actualContactsById.size(),
                'Expected the correct number of contacts to be updated.');

        // Next we will query for the contact records. We could compare to the actualContactsById map but querying
        // again just to be sure so the assertion isn't dependent on the method implementation
        // We got the contactIds from the actualContactsById set since that tells which records were actually processed by our method.
        Set<Id> contactIds = actualContactsById.keySet();

        Map<Id, Contact> updatedContactsById = getContactsById(contactIds);

        // Now we will iterate over our expected contacts and make our assertions.
        for (Id contactId : expectedContactsById.keySet()) {
            Contact expectedContact = expectedContactsById.get(contactId);
            Contact actualContact = updatedContactsById.get(contactId);
            System.assertNotEquals(null, actualContact,
                    'Expected the contact to have been updated. No Contact found for Id: ' + contactId);
            assertContactUpdatedCorrectly(expectedContact, actualContact);
        }
    }

    private static void assertContactUpdatedCorrectly(Contact expectedContact, Contact actualContact) {
        System.assertEquals(expectedContact.Id, actualContact.Id, 'The records being compared should have the same Id.');
        System.assertEquals(expectedContact.RavennaId__c, actualContact.RavennaId__c, 'Expected the correct ravenna Id.');
        System.assertEquals(expectedContact.Ethnicity__c, actualContact.Ethnicity__c, 'Expected the correct Ethnicity.');
    }
    
    private static void runContactSyncAndMakeAssertions(
        String originalEthnicity, 
        String expectedEthnicity, 
        String ravennaId, 
        String ravennaEthnicity) {

        // Insert the student contact. We make sure the ravenna Id is null just to be safe.
        Contact student = ContactTestData.Instance
                .forEthnicity(originalEthnicity)
                .withRavennaId(null)
                .asStudent()
                .insertContact();

        Applicant__c studentApplicant = ApplicantTestData.Instance
                .forContactId(student.Id)
                .withRavennaId(ravennaId)
                .insertApplicant();

        // Create mock API state. Calling it mock because it will only contain what is necessary for our unit tests.
        IntegrationApiModel.ApiState mockApiState = createMockApiState(studentApplicant, ravennaId, ravennaEthnicity);

        // Run our code!
        Test.startTest();
        Map<Id, Contact> actualContactsById = RavennaIntegrationUtils.syncContacts(mockApiState);
        Test.stopTest();

        // Create a map of contact records by Id where the contact record has the data we expect after running the utils class.
        Map<Id, Contact> expectedContactsById = new Map<Id, Contact>();
        Contact expectedContact = createExpectedContact(student.Id, ravennaId, expectedEthnicity);
        expectedContactsById.put(expectedContact.Id, expectedContact);

        // Make our assertions by comparing the actual contacts returned to our map of expected contacts.
        assertContactsUpdatedCorrectly(expectedContactsById, actualContactsById);
    }
    
    @isTest 
    private static void syncContacts_noApplicantsOnApiState_expectEmptyMap() {
                
        IntegrationApiModel.ApiState mockApiState = createEmptyApiState();
        
        Test.startTest();
        Map<Id, Contact> actualContactsById = RavennaIntegrationUtils.syncContacts(mockApiState);
        Test.stopTest();
        
        System.AssertEquals(0, actualContactsById.size());
    }

    // For all the following unit tests, we always expect the ravenna Id to be updated on the contact record.
    // Whatever ravennaId we specify on the applicant should be on the contact at the end of the test.
    @isTest 
    private static void syncContacts_contactHasNullEthnicity_responseDoesNotContainEthnicity_expectEthnicityNotUpdated() {
        
        String originalEthnicity = null;
        String expectedEthnicity = null;
        String ravennaId = '123456';
        String ravennaEthnicity = null;
        
        runContactSyncAndMakeAssertions(originalEthnicity, expectedEthnicity, ravennaId, ravennaEthnicity);
    }

    @isTest 
    private static void syncContacts_contactHasNullEthnicity_responseEthnicityDeclineToAnswer_expectEthnicityNotUpdated() {
        
        String originalEthnicity = null;
        String expectedEthnicity = originalEthnicity;
        String ravennaId = '123456';
        String ravennaEthnicity = RavennaIntegrationUtils.DECLINE_TO_ANSWER;
        
        runContactSyncAndMakeAssertions(originalEthnicity, expectedEthnicity, ravennaId, ravennaEthnicity);
    }
    
    @isTest 
    private static void syncContacts_contactHasNullEthnicity_responseEthnicityUnexpectedValue_expectEthnicityNotUpdated() {
        
        // This unit test is for a case where a family in ravenna entered their own ethnicity instead of choosing from the options provided by ravenna.
        String originalEthnicity = null;
        String expectedEthnicity = null;
        String ravennaId = '123456';
        String ravennaEthnicity = null;
        
        runContactSyncAndMakeAssertions(originalEthnicity, expectedEthnicity, ravennaId, ravennaEthnicity);
    }
    
    @isTest 
    private static void syncContacts_contactHasNullEthnicity_responseEthnicityNativeAmerican_expectEthnicityUpdated() {
        
        // For the tests where we expect the contact ethnicity to be updated, we should make sure it is updated to the
        // corresponding SSS Ethnicity found in RavennaIntegrationUtils.ETHNICITY_BY_RAVENNA_VALUE
        String originalEthnicity = null;
        String expectedEthnicity = 'American Indian';
        String ravennaId = '123456';
        String ravennaEthnicity = 'Native American';
        
        runContactSyncAndMakeAssertions(originalEthnicity, expectedEthnicity, ravennaId, ravennaEthnicity);
        
    }

    // TODO SFP-1178 syncContacts_contactHasNullEthnicity_responseEthnicityMultiracialAmerican_expectEthnicityUpdated()
    @isTest 
    private static void syncContacts_contactHasNullEthnicity_responseEthnicityMultiracialAmerican_expectEthnicityUpdated() {
        
        // For the tests where we expect the contact ethnicity to be updated, we should make sure it is updated to the
        // corresponding SSS Ethnicity found in RavennaIntegrationUtils.ETHNICITY_BY_RAVENNA_VALUE
        String originalEthnicity = null;
        String expectedEthnicity = RavennaIntegrationUtils.MULTIRACIAL_SSS;
        String ravennaId = '123456';
        String ravennaEthnicity = 'Multiracial American';
        
        runContactSyncAndMakeAssertions(originalEthnicity, expectedEthnicity, ravennaId, ravennaEthnicity);
        
    }
    
    @isTest 
    private static void syncContacts_contactHasNullEthnicity_responseEthnicityMultipleValues_expectEthnicityMultiracial() {
        
        // This unit test is for scenarios where a family selects multiple ethnicities in ravenna.
        String originalEthnicity = null;
        String expectedEthnicity = RavennaIntegrationUtils.MULTIRACIAL_SSS;
        String ravennaId = '123456';
        String ravennaEthnicity = 'Native American, African American, Middle Eastern American';
        
        runContactSyncAndMakeAssertions(originalEthnicity, expectedEthnicity, ravennaId, ravennaEthnicity);
        
    }
    // The following unit tests are for cases where the contact record already has ethnicity specified. In these cases, we don't transfer the ravenna ethnicity.
    @isTest 
    private static void syncContacts_contactAlreadyHasEthnicity_responseEthnicityInternational_expectEthnicityNotUpdated() {
        
        String originalEthnicity = 'International';
        String expectedEthnicity = 'International';
        String ravennaId = '123456';
        String ravennaEthnicity = 'Middle Eastern American';
        
        runContactSyncAndMakeAssertions(originalEthnicity, expectedEthnicity, ravennaId, ravennaEthnicity);
        
    }

    @isTest 
    private static void syncContacts_contactAlreadyHasEthnicity_responseEthnicityMultipleValues_expectEthnicityNotUpdated() {
        
        String originalEthnicity = 'Asian American';
        String expectedEthnicity = 'Asian American';
        String ravennaId = '123456';
        String ravennaEthnicity = 'Native American, African American, Middle Eastern American';
        
        runContactSyncAndMakeAssertions(originalEthnicity, expectedEthnicity, ravennaId, ravennaEthnicity);
        
    }

    @isTest
    private static void convertGradeToPicklist_firstGrade_expectSameValue() {
        String ravennaGrade = '1';
        String expectedGrade = '1';

        String currentGradeFieldResult = RavennaIntegrationUtils.convertGradeToPicklist(RavennaIntegrationUtils.CURRENT_GRADE_FIELD, ravennaGrade);
        System.assertEquals(expectedGrade, currentGradeFieldResult,
                'Expected the correct grade conversion for the current grade field.');

        String entryGradeFieldResult = RavennaIntegrationUtils.convertGradeToPicklist(RavennaIntegrationUtils.APPLY_GRADE_FIELD, ravennaGrade);
        System.assertEquals(expectedGrade, entryGradeFieldResult,
                'Expected the correct grade conversion for the entry grade field.');
    }
    
    @isTest
    private static void convertGradeToPicklist_ravennaPK_expectCorrectPicklistValues() {
        String ravennaGrade = RavennaIntegrationUtils.PK_SOURCE;
        String expectedCurrentGrade = RavennaIntegrationUtils.PRE_K_TYPE_CURRENT_GRADE;
        String expectedEntryGrade = RavennaIntegrationUtils.PRE_K_TYPE;

        String currentGradeFieldResult = RavennaIntegrationUtils.convertGradeToPicklist(RavennaIntegrationUtils.CURRENT_GRADE_FIELD, ravennaGrade);
        System.assertEquals(expectedCurrentGrade, currentGradeFieldResult,
                'Expected the correct grade conversion for the current grade field.');

        String entryGradeFieldResult = RavennaIntegrationUtils.convertGradeToPicklist(RavennaIntegrationUtils.APPLY_GRADE_FIELD, ravennaGrade);
        System.assertEquals(expectedEntryGrade, entryGradeFieldResult,
                'Expected the correct grade conversion for the entry grade field.');
    }

    @isTest
    private static void convertGradeToPicklist_ravennaPG_expectPostGraduation() {
        String ravennaGrade = RavennaIntegrationUtils.PG_SOURCE;
        String expectedGrade = RavennaIntegrationUtils.POST_GRAD_TYPE;

        String currentGradeFieldResult = RavennaIntegrationUtils.convertGradeToPicklist(RavennaIntegrationUtils.CURRENT_GRADE_FIELD, ravennaGrade);
        System.assertEquals(expectedGrade, currentGradeFieldResult,
                'Expected the correct grade conversion for the current grade field.');

        String entryGradeFieldResult = RavennaIntegrationUtils.convertGradeToPicklist(RavennaIntegrationUtils.APPLY_GRADE_FIELD, ravennaGrade);
        System.assertEquals(expectedGrade, entryGradeFieldResult,
                'Expected the correct grade conversion for the entry grade field.');
    }

    @isTest
    private static void convertGradeToPicklist_ravennaPS_expectPreschool() {
        String ravennaGrade = RavennaIntegrationUtils.PS_SOURCE;
        String expectedGrade = RavennaIntegrationUtils.PRESCHOOL_TYPE;

        String currentGradeFieldResult = RavennaIntegrationUtils.convertGradeToPicklist(RavennaIntegrationUtils.CURRENT_GRADE_FIELD, ravennaGrade);
        System.assertEquals(expectedGrade, currentGradeFieldResult,
                'Expected the correct grade conversion for the current grade field.');

        String entryGradeFieldResult = RavennaIntegrationUtils.convertGradeToPicklist(RavennaIntegrationUtils.APPLY_GRADE_FIELD, ravennaGrade);
        System.assertEquals(expectedGrade, entryGradeFieldResult,
                'Expected the correct grade conversion for the entry grade field.');
    }

    @isTest
    private static void convertGradeToPicklist_ravennaK_expectKindergarten() {
        String ravennaGrade = RavennaIntegrationUtils.K_SOURCE;
        String expectedGrade = RavennaIntegrationUtils.KINDERGARTEN_TYPE;

        String currentGradeFieldResult = RavennaIntegrationUtils.convertGradeToPicklist(RavennaIntegrationUtils.CURRENT_GRADE_FIELD, ravennaGrade);
        System.assertEquals(expectedGrade, currentGradeFieldResult,
                'Expected the correct grade conversion for the current grade field.');

        String entryGradeFieldResult = RavennaIntegrationUtils.convertGradeToPicklist(RavennaIntegrationUtils.APPLY_GRADE_FIELD, ravennaGrade);
        System.assertEquals(expectedGrade, entryGradeFieldResult,
                'Expected the correct grade conversion for the entry grade field.');
    }
}