/**
 * @description Controls the creation and display of an Opportunity's invoice.
 **/
public without sharing class SchoolInvoiceController {
    // // NAIS-2376: support transactions made by all staff of the school, not just the current user

    //URL parameter that will drive the page
    private final Id oppId;

    //Page variables
    public Opportunity opp { get; set; }
    public String toSchoolName { get; set; }
    public String toStreet { get; set; }
    public String toCity { get; set; }
    public String toState { get; set; }
    public String toPostalCode { get; set; }
    public String toPhone { get; set; }
    public String academicYear { get; set; }
    public String currentDate { get; set; }
    public String paymentMethod { get; set; }
    public String checkNumber { get; set; }
    public String schoolCode { get; set; }
    public String description { get; set; }
    public String oppDate { get; set; }
    public String subscriptionDates { get; set; }  // SFP-441
    public String productCode { get; set; }
    public Decimal totalPaid { get; set; }
    public Decimal totalAmount { get; set; }
    public Decimal totalDiscounts { get; set; }
    public Decimal qty { get; set; }
    public Decimal netDueInvoice { get; set; }
    public Decimal subDiscount { get; set; }
    public Boolean credit { get; set; }

    /**
     * @description Retrieve the Opportunity that the user is attempting
     *              to view the invoice for.
     */
    public SchoolInvoiceController() {
        this.oppId = ApexPages.currentPage().getParameters().get('id');
        opp = new Opportunity();
        credit = false;

        if (this.oppId == null) {
            system.debug('There was no Opportunity ID passed in.');
            return;
        }

        // Query and populate page variables
        buildPageData();

        if (opp.Transaction_Line_Items__r.size() == 0) {
            system.debug('No line items on this opportunity.');
            return;
        }

        // Find payment information based on TLI Record Type
        parseLineItems();
    }

    /**
     * @description Query for the Opportunity and set the properties for
     *              the page to build out the invoice with.
     */
    public void buildPageData() {
        // Find the opportunity that was passed in through the URL
        try {
            opp = [SELECT
                          Name,
                          Account.Name,
                          Account.BillingStreet,      // NAIS-2376
                          Account.BillingCity,        // NAIS-2376
                          Account.BillingState,       // NAIS-2376
                          Account.BillingPostalCode,  // NAIS-2376
                          Account.Phone,              // NAIS-2376
                          OwnerId,                    // NAIS-2376
                          Academic_Year_Picklist__c,
                          Amount,
                          CloseDate,
                          DateTime_Invoice_Generated__c,
                          SSS_School_Code__c,
                          Total_Discounts__c,
                          Net_Amount_Due__c,
                          Total_Payments__c,
                          Total_Revenue__c,
                          Total_Refunds__c,
                          Subscription_Period_Start_Year__c, //SFP-441
                          Subscription_Discount__c,
                          (SELECT
                                  Name,
                                  RecordTypeId,
                                  Amount__c,
                                  CC_Last_4_Digits__c,
                                  Check_Number__c,
                                  Product_Code__c,
                                  Tender_Type__c,
                                  Waiver_Quantity__c,
                                  Transaction_Type__c
                             FROM Transaction_Line_Items__r)
                     FROM Opportunity
                    WHERE Id = :oppId
                    LIMIT 1];
        } catch(Exception e) {
            system.debug('There is no opportunity with this ID or you do not have permission to view this opportunity.');
            return;
        }

        Account userAccount = opp.Account;
        toSchoolName = opp.Account.Name;
        toStreet = userAccount.BillingStreet;
        toCity = userAccount.BillingCity;
        toState = userAccount.BillingState;
        toPostalCode = userAccount.BillingPostalCode;
        toPhone = userAccount.Phone;
        currentDate = opp.CloseDate.format(); // NAIS-2376
        academicYear = opp.Academic_Year_Picklist__c;
        schoolCode = opp.SSS_School_Code__c;
        totalAmount = opp.Amount;
        description = opp.Name;
        oppDate = opp.CloseDate.format();
        subDiscount = (opp.Subscription_Discount__c == null) ? 0 : opp.Subscription_Discount__c;
        netDueInvoice = totalAmount - subDiscount;
        credit = netDueInvoice < 0;
        subscriptionDates = calculateSubscriptionDates(); //SFP-441
    }

    /**
     * @description Parse the Transaction Line Items associated with the
     *              current Opportunity, generating the total paid, quantity,
     *              product code, and payment method information.
     */
    public void parseLineItems() {
        //set defaults
        totalPaid = 0;
        qty = 1;
        productCode = '';
        paymentMethod = '';

        //Check TLIs for Payment information and Sale quantity/code
        for (Transaction_Line_Item__c tli :opp.Transaction_Line_Items__r) {
            system.debug('*tli'+tli);
            //If this is the Sale TLI, get it's quantity and product code
            if (tli.RecordTypeId == RecordTypes.saleTransactionTypeId) {
                if (tli.Waiver_Quantity__c != null) {qty = tli.Waiver_Quantity__c;}
                productCode = tli.Product_Code__c;
            }
            //If this is a payment TLI, populate Payment Method
            else if (tli.RecordTypeId == RecordTypes.paymentTransactionTypeId || tli.RecordTypeId == RecordTypes.paymentAutoTransactionTypeId) {
                paymentMethod = tli.Tender_Type__c;
                if (tli.CC_Last_4_Digits__c != null) {paymentMethod += ' ending in ' + tli.CC_Last_4_Digits__c;}
                checkNumber = tli.Check_Number__c;
                totalPaid += tli.Amount__c;
            }
        }
    }

    /**
     * @description Sets the DateTime_Invoice_Generated__c to the current date time
     *              the very first time that a school user views the invoice for
     *              the current opportunity.
     */
    public void applyInvoiceDateTime() {
        if (opp == null || opp.Id == null || opp.DateTime_Invoice_Generated__c != null) {
            return;
        }

        opp.DateTime_Invoice_Generated__c = System.NOW();
        update opp;
    }

    private string calculateSubscriptionDates() {
        if (this.opp!=null && this.opp.Subscription_Period_Start_Year__c!=null && this.opp.Subscription_Period_Start_Year__c.isNumeric()) {
            Integer year = Integer.ValueOf(this.opp.Subscription_Period_Start_Year__c);
            return '10/1/'+year+'-9/30/'+(year+1);
        }
        return null;
    }
}