/**
 * LiveAgentDeploymentServiceTest.cls
 *
 * @description: Test class for LiveAgentDeploymentService.cls No Real data to setup, just make sure the Metadata is not empty. 
 *
 * @author: Mike havrilla @ Presence PG
 */

@IsTest (seeAllData = false)
public class LiveAgentDeploymentServiceTest {
    @isTest
    private static void loadLiveAgentConfiguration_getLiveAgentInformation_liveAgentConfigPopulated() {
        // Arrange
        // No Action
        
        // Act 
        Test.startTest();
            LiveAgentDeploymentService.LiveAgentConfigurationWrapper wrapper = LiveAgentDeploymentService.getLiveAgentInformation( 'FamilyDocuments');
        Test.stopTest();
        
        //Assert
        System.assert( String.isNotEmpty(wrapper.liveAgentButtonId));
        System.assert( String.isNotEmpty(wrapper.liveAgentDeploymentUrl));
        System.assert( String.isNotEmpty(wrapper.liveAgentDeploymentId));
        System.assert( String.isNotEmpty(wrapper.liveAgentChatUrl));
    }
}