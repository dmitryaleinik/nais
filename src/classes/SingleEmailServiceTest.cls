@isTest
private with sharing class SingleEmailServiceTest
{
    
    private static PFS__c pfsRecord;
    private static ApplicationUtils appUtils;
    private static String currentScreen;
    private static Id academicYearId;
    private static Contact parentA;
    
    private static void generateBasicPFSData()
    {
        List<Household_Income_Threshold__c> result = HouseholdIncomeThresholdTestData.Instance.insertIncomeThresholds();
        academicYearId = result[0].Academic_Year__c;
        parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        parentA.Email = 'singleemailService@testtest.com';
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        
        insert new List<Contact>{parentA, student1};
        
        User userRecord = [select Id, Name, ContactId, Contact.LastName, Contact.Name, Contact.MailingStreet, Contact.MailingCity, Contact.MailingState,  
                        Contact.MailingPostalCode, Contact.MailingCountry, Contact.Gender__c, Contact.Birthdate, 
                        Contact.Email, Contact.HomePhone, Contact.MobilePhone, Contact.Preferred_Phone__c,
                        Contact.FirstName, Contact.Middle_Name__c, Contact.Suffix__c, Contact.Salutation, Contact.Phone,
                        LanguageLocaleKey
                        from User 
                        where Id = :UserInfo.getUserId()];
                        
        pfsRecord = ApplicationUtils.generateBlankPFS(academicYearId, userRecord);
        
        Id pfsId = [select Id from PFS__c limit 1].Id;
        pfsRecord = ApplicationUtils.queryPFSRecord(pfsId);
        
        if (currentScreen != null){
            appUtils = new ApplicationUtils(UserInfo.getLanguage(), pfsRecord, currentScreen);
        } else {
            appUtils = new ApplicationUtils(UserInfo.getLanguage(), pfsRecord);
        }
    }
    
    @isTest
    private static void sendEmail_emailSent_expectEmailSent()
    {
        generateBasicPFSData();
        Boolean sent = SingleEmailService.Instance.sendEmail(pfsRecord.Id, new List<Id>{parentA.Id}, 'Alert School to Complete Required Annual Setup', OrgWideEmails.Instance.Do_Not_Reply_Address);
        
        System.assertEquals(true, sent);
    }
}