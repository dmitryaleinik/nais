@isTest
private without sharing class FamilyAwardControllerTest {

    private static Account school_1, school_2;
    private static Contact parent, student_1, student_2;
    private static PFS__c pfs;
    private static Applicant__c applicant_1, applicant_2;
    private static Student_Folder__c folder_1, folder_2, folder_3, folder_4;
    private static School_PFS_Assignment__c spa_1, spa_2, spa_3, spa_4;
    private static Contact staff_1, staff_2;
    private static User staffUser_1, staffUser_2, parentUser;
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

    private static void setupData() {

        Academic_Year__c academicYear = AcademicYearTestData.Instance.insertAcademicYear();
        //Create Schools
        school_1 = AccountTestData.Instance
                .forName('School_1')
                .asSchool()
                .create();
        school_2 = AccountTestData.Instance
                .forName('School_2')
                .asSchool()
                .create();


        insert new List<Account>{
                school_1, school_2
        };

        //Create contacts for parents and students.
        parent = ContactTestData.Instance
                .asParent()
                .forFirstName('Steven')
                .forLastName('Tyler')
                .create();
        student_1 = ContactTestData.Instance
                .asStudent()
                .forFirstName('Liv')
                .forLastName('Tyler')
                .create();
        student_2 = ContactTestData.Instance
                .asStudent()
                .forFirstName('Mia')
                .forLastName('Tyler')
                .create();
        staff_1 = ContactTestData.Instance
                .asSchoolStaff()
                .forAccount(school_1.Id)
                .create();
        staff_2 = ContactTestData.Instance
                .asSchoolStaff()
                .forAccount(school_2.Id)
                .create();

        insert new List<Contact>{
                parent, student_1, student_2, staff_1, staff_2
        };

        //Create PFS records.
        pfs = PfsTestData.Instance
                .forParentA(parent.Id)
                .forAcademicYearPicklist(academicYear.Name)
                .asPaid()
                .create();

        insert pfs;

        //Create applicants.
        applicant_1 = ApplicantTestData.Instance
                .forContactId(student_1.Id)
                .forPfsId(pfs.Id)
                .forStudentFirstName('Liv')
                .forStudentLastName('Tyler')
                .create();
        applicant_2 = ApplicantTestData.Instance
                .forContactId(student_2.Id)
                .forPfsId(pfs.Id)
                .forStudentFirstName('Mia')
                .forStudentLastName('Tyler')
                .create();

        insert new List<Applicant__c>{
                applicant_1, applicant_2
        };

        //Create Folders
        folder_1 = StudentFolderTestData.Instance.forName('Folder: Applicant_1-School_1')
                .forAcademicYearPicklist(academicYear.Name)
                .forStudentId(student_1.Id)
                .forSchoolId(school_1.Id)
                .create();
        folder_2 = StudentFolderTestData.Instance.forName('Folder: Applicant_1-School_2')
                .forAcademicYearPicklist(academicYear.Name)
                .forStudentId(student_1.Id)
                .forSchoolId(school_2.Id)
                .create();
        folder_3 = StudentFolderTestData.Instance.forName('Folder: Applicant_1-School_1')
                .forAcademicYearPicklist(academicYear.Name)
                .forStudentId(student_2.Id)
                .forSchoolId(school_1.Id)
                .create();
        folder_4 = StudentFolderTestData.Instance.forName('Folder: Applicant_1-School_2')
                .forAcademicYearPicklist(academicYear.Name)
                .forStudentId(student_2.Id)
                .forSchoolId(school_2.Id)
                .create();

        insert new List<Student_Folder__c>{
                folder_1, folder_2, folder_3, folder_4
        };

        //Create SPA records
        spa_1 = SchoolPfsAssignmentTestData.Instance
                .forAcademicYearPicklist(academicYear.Name)
                .forApplicantId(applicant_1.Id)
                .forSchoolId(school_1.Id)
                .forStudentFolderId(folder_1.Id)
                .create();
        spa_2 = SchoolPfsAssignmentTestData.Instance
                .forAcademicYearPicklist(academicYear.Name)
                .forApplicantId(applicant_1.Id)
                .forSchoolId(school_2.Id)
                .forStudentFolderId(folder_2.Id)
                .create();
        spa_3 = SchoolPfsAssignmentTestData.Instance
                .forAcademicYearPicklist(academicYear.Name)
                .forApplicantId(applicant_2.Id)
                .forSchoolId(school_1.Id)
                .forStudentFolderId(folder_3.Id)
                .create();
        spa_4 = SchoolPfsAssignmentTestData.Instance
                .forAcademicYearPicklist(academicYear.Name)
                .forApplicantId(applicant_2.Id)
                .forSchoolId(school_2.Id)
                .forStudentFolderId(folder_4.Id)
                .create();
        spa_1.Applicant__r = applicant_1;
        spa_1.Applicant__r.PFS__r = pfs;
        spa_2.Applicant__r = applicant_1;
        spa_2.Applicant__r.PFS__r = pfs;

        insert new List<School_PFS_Assignment__c>{
                spa_1, spa_2, spa_3, spa_4
        };

        System.runAs(thisUser) {
            staffUser_1 = UserTestData.Instance
                    .forUsername('Staff_1@test.com')
                    .forContactId(staff_1.Id)
                    .forProfileId(GlobalVariables.schoolPortalAdminProfileId)
                    .create();
            staffUser_2 = UserTestData.Instance
                    .forUsername('Staff_2@test.com')
                    .forContactId(staff_2.Id)
                    .forProfileId(GlobalVariables.schoolPortalAdminProfileId)
                    .create();

            insert new List<User>{
                    staffUser_1, staffUser_2
            };
        }


    }//End:setupData

    @isTest
    private static void OneApplicantWithOneAward_expectParentHasAccess() {

        setupData();

        FeatureToggles.setToggle('Allow_Mass_Email_Per_Applicant__c', true);
        folder_1.Folder_Status__c = 'Award Approved';
        folder_1.Award_Letter_Sent__c = 'Yes';
        update folder_1;

        School_1.Family_Award_Acknowledgement_Enabled__c = true;
        update School_1;

        Test.StartTest();
        //Verify that the parent has access to folder_1

        PageReference familyAwardPage = new ApexPages.Pagereference('/apex/FamilyAward?id=' + folder_1.Id + '&pfsId=' + pfs.Id);
        Test.setCurrentPage(familyAwardPage);
        FamilyTemplateController template = new FamilyTemplateController();
        FamilyAwardController controller = new FamilyAwardController(template);
        System.AssertEquals(true, controller.hasAccessToFolder);

        System.AssertEquals('Liv Tyler', controller.applicant.applicantName);
        System.AssertEquals(1, controller.applicant.awards.size());

        Test.StopTest();

    }//End:OneApplicantWithOneAward_expectParentHasAccess

    @isTest
    private static void OneApplicantWithOneAwardAndToggleFeatureDisabled_expectParentHasNoAccess() {

        setupData();
        FeatureToggles.setToggle('Allow_Mass_Email_Per_Applicant__c', false);

        School_1.Family_Award_Acknowledgement_Enabled__c = true;
        update School_1;

        folder_1.Folder_Status__c = 'Award Approved';
        folder_1.Award_Letter_Sent__c = 'Yes';
        update folder_1;

        Test.StartTest();

        //Verify that the parent has access NO to folder_1
        PageReference familyAwardPage = new ApexPages.Pagereference('/apex/FamilyAward?id=' + folder_1.Id + '&pfsId=' + pfs.Id);
        Test.setCurrentPage(familyAwardPage);
        FamilyTemplateController template = new FamilyTemplateController();
        FamilyAwardController controller = new FamilyAwardController(template);
        System.AssertEquals(false, controller.hasAccessToFolder);

        Test.StopTest();

    }//End:OneApplicantWithOneAwardAndToggleFeatureDisabled_expectParentHasNoAccess

    @isTest
    private static void applicantWithAward_expectBeAbleToAcceptAward() {

        setupData();

        FeatureToggles.setToggle('Allow_Mass_Email_Per_Applicant__c', true);

        School_1.Family_Award_Acknowledgement_Enabled__c = true;
        School_2.Family_Award_Acknowledgement_Enabled__c = true;
        update new List<Account>{
                School_1, School_2
        };

        folder_1.Folder_Status__c = 'Award Approved';
        folder_1.Award_Letter_Sent__c = 'Yes';
        folder_2.Folder_Status__c = 'Award Approved';
        folder_2.Award_Letter_Sent__c = 'Yes';
        update new List<Student_Folder__c>{
                folder_1, folder_2
        };

        Test.StartTest();

        PageReference familyAwardPage = new ApexPages.Pagereference('/apex/FamilyAward?id=' + folder_1.Id + '&pfsId=' + pfs.Id);
        Test.setCurrentPage(familyAwardPage);
        FamilyTemplateController template = new FamilyTemplateController();
        FamilyAwardController controller = new FamilyAwardController(template);

        System.AssertEquals(true, controller.hasAccessToFolder);

        System.AssertEquals(1, controller.applicant.awards.size());

        controller.acceptAction();

        Test.StopTest();

        System.AssertEquals(
                System.today(),
                [SELECT Id, Award_Accepted_Date__c FROM Student_Folder__c WHERE Id = :folder_1.Id].Award_Accepted_Date__c);
    }//End:applicantWithAward_expectBeAbleToAcceptAward
}