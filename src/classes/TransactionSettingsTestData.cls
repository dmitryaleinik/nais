/**
 * @description This class is used to create Transaction Settings records for unit tests.
 */
@isTest
public class TransactionSettingsTestData extends SObjectTestData {
    @testVisible private static final String REFUND_NAME = 'Refund-CC';
    @testVisible private static final String REFUND_ACCOUNT_CODE = '1003';
    @testVisible private static final String REFUND_ACCOUNT_LABEL = 'Bank Acct SSS CAO';
    @testVisible private static final String REFUND_TRANSACTION_CODE = '6110';

    /**
     * @description Get the default values for the Transaction_Settings__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Transaction_Settings__c.Name => 'Credit/Debit Card',
                Transaction_Settings__c.Account_Code__c => '1003',
                Transaction_Settings__c.Transaction_Code__c => '4110',
                Transaction_Settings__c.Account_Label__c => 'Bank Acct SSS CAO'
        };
    }

    /**
     * @description Configure the current Transaction_Settings__c record as a
     *             refund Transaction Setting.
     * return The current instance of TransactionSettingsTestData.
     */
    public TransactionSettingsTestData asRefund() {
        return (TransactionSettingsTestData) with(Transaction_Settings__c.Name, REFUND_NAME)
                                            .with(Transaction_Settings__c.Account_Code__c, REFUND_ACCOUNT_CODE)
                                            .with(Transaction_Settings__c.Account_Label__c, REFUND_ACCOUNT_LABEL)
                                            .with(Transaction_Settings__c.Transaction_Code__c, REFUND_TRANSACTION_CODE);
    }

    /**
     * @description Insert the current working Transaction_Settings__c record.
     * @return The currently operated upon Transaction_Settings__c record.
     */
    public Transaction_Settings__c insertTransactionSettings() {
        return (Transaction_Settings__c)insertRecord();
    }

    /**
     * @description Create the current working Transaction Settings record without resetting
     *             the stored values in this instance of TransactionSettingsTestData.
     * @return A non-inserted Transaction_Settings__c record using the currently stored field
     *             values.
     */
    public Transaction_Settings__c createTransactionSettingsWithoutReset() {
        return (Transaction_Settings__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Transaction_Settings__c record.
     * @return The currently operated upon Transaction_Settings__c record.
     */
    public Transaction_Settings__c create() {
        return (Transaction_Settings__c)super.buildWithReset();
    }

    /**
     * @description The default Transaction_Settings__c record.
     */
    public Transaction_Settings__c DefaultTransactionSettings {
        get {
            if (DefaultTransactionSettings == null) {
                DefaultTransactionSettings = createTransactionSettingsWithoutReset();
                insert DefaultTransactionSettings;
            }
            return DefaultTransactionSettings;
        }
        private set;
    }

    /**
     * @description Get the Transaction_Settings__c SObjectType.
     * @return The Transaction_Settings__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Transaction_Settings__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static TransactionSettingsTestData Instance {
        get {
            if (Instance == null) {
                Instance = new TransactionSettingsTestData();
            }
            return Instance;
        }
        private set;
    }

    private TransactionSettingsTestData() { }
}