@isTest
private class ScheduleSharingJobsControllerTest {
    
    @isTest 
    private static void testScheduleJobsRemotely() {
        Test.startTest();
            ScheduleSharingJobsController.deleteJobsRemotely();
            
            ScheduleSharingJobsController.scheduleJobsRemotely();
        Test.stopTest();
            
        CronTrigger ct = getCronTriggerById(ScheduleSharingJobsController.job0Id);
        System.assertEquals(ScheduleSharingJobsController.schedule0, ct.CronExpression);
        
        ct = getCronTriggerById(ScheduleSharingJobsController.job1Id);
        System.assertEquals(ScheduleSharingJobsController.schedule1, ct.CronExpression);
        
        ct = getCronTriggerById(ScheduleSharingJobsController.accountJob0Id);
        System.assertEquals(ScheduleSharingJobsController.accountSchedule0, ct.CronExpression);
        
        ct = getCronTriggerById(ScheduleSharingJobsController.efcCalcJob0Id);
        System.assertEquals(ScheduleSharingJobsController.efcCalcSchedule0, ct.CronExpression);
    }
 
    @isTest 
    private static void testScheduleIsPortalEnabled() {
        Test.startTest();
            ScheduleSharingJobsController.deleteJobsRemotely();
            
            ScheduleSharingJobsController cont = new ScheduleSharingJobsController();
            cont.scheduleIsPortalEnabled();
        Test.stopTest();
        
        CronTrigger pe = getCronTriggerById(ScheduleSharingJobsController.job0Id);
        System.assertEquals(ScheduleSharingJobsController.disablePortalAccess, pe.CronExpression);
    }
        
    @isTest 
    private static void testSchedule() {
        Test.startTest();
            ScheduleSharingJobsController.deleteJobsRemotely();
            
            ScheduleSharingJobsController cont = new ScheduleSharingJobsController();
            cont.schedule();
        Test.stopTest();
        
        CronTrigger sc0 = getCronTriggerById(ScheduleSharingJobsController.job0Id);
        System.assertEquals(ScheduleSharingJobsController.schedule0, sc0.CronExpression);
        CronTrigger sc1 = getCronTriggerById(ScheduleSharingJobsController.job1Id);
        System.assertEquals(ScheduleSharingJobsController.schedule1, sc1.CronExpression);
    }

    @isTest 
    private static void testRunSharingJob() {
        Test.startTest();
            ScheduleSharingJobsController.deleteJobsRemotely();
            
            ScheduleSharingJobsController cont = new ScheduleSharingJobsController();
            cont.runSharingJob();
        Test.stopTest();
    }
    
    @isTest 
    private static void testScheduleEfcCalcJobs() {
        Test.startTest();
            ScheduleSharingJobsController.deleteJobsRemotely();
            
            ScheduleSharingJobsController cont = new ScheduleSharingJobsController();
            cont.scheduleEfcCalcJobs();     
        Test.stopTest();

        CronTrigger efc0 = getCronTriggerById(ScheduleSharingJobsController.efcCalcJob0Id);
        System.assertEquals(ScheduleSharingJobsController.efcCalcSchedule0, efc0.CronExpression);
    }

    @isTest 
    private static void testScheduleCopyVerificationToSchoolPFSAssignments() {
        Test.startTest();
            ScheduleSharingJobsController.deleteJobsRemotely();
            
            ScheduleSharingJobsController cont = new ScheduleSharingJobsController();
            cont.scheduleCopyVerificationToSchoolPFSAssignments();     

            CronTrigger cron = getCronTriggerByCronJobDetailName(ScheduleSharingJobsController.COPY_VERIFICATION_TO_SCHOOL_PFSA_JOB_NAME);
            System.assertEquals(ScheduleSharingJobsController.midnightScheduleString, cron.CronExpression);
            
            try {
                cont.scheduleCopyVerificationToSchoolPFSAssignments();
            } catch (Exception ex) {
                System.assertEquals(
                    ex.getMessage(),
                    ScheduleSharingJobsController.getAlreadyScheduledJobErrorMessage(
                        ScheduleSharingJobsController.COPY_VERIFICATION_TO_SCHOOL_PFSA_JOB_NAME));
            }
        Test.stopTest();
    }

    @isTest 
    private static void testScheduleDocumentVerificationRequests() {
        Test.startTest();
            ScheduleSharingJobsController.deleteJobsRemotely();
            
            ScheduleSharingJobsController cont = new ScheduleSharingJobsController();
            cont.scheduleDocumentVerificationRequests();     

            CronTrigger cron = getCronTriggerByCronJobDetailName(ScheduleSharingJobsController.DOCUMENT_VERIFICATION_REQUESTS_JOB_NAME);
            System.assertEquals(ScheduleSharingJobsController.midnightScheduleString, cron.CronExpression);
            
            try {
                cont.scheduleDocumentVerificationRequests();
            } catch (Exception ex) {
                System.assertEquals(
                    ex.getMessage(),
                    ScheduleSharingJobsController.getAlreadyScheduledJobErrorMessage(
                        ScheduleSharingJobsController.DOCUMENT_VERIFICATION_REQUESTS_JOB_NAME));
            }
        Test.stopTest();
    }

    @isTest 
    private static void testScheduleDocumentCleanup() {
        Test.startTest();
            ScheduleSharingJobsController.deleteJobsRemotely();
            
            ScheduleSharingJobsController cont = new ScheduleSharingJobsController();
            cont.scheduleDocumentCleanup();     

            CronTrigger cron = getCronTriggerByCronJobDetailName(ScheduleSharingJobsController.DOC_CLEANUP_JOB);
            System.assertEquals(ScheduleSharingJobsController.scheduleDocumentCleanupString, cron.CronExpression);
            
            try {
                cont.scheduleDocumentCleanup();
            } catch (Exception ex) {
                System.assertEquals(
                    ex.getMessage(),
                    ScheduleSharingJobsController.getAlreadyScheduledJobErrorMessage(
                        ScheduleSharingJobsController.DOC_CLEANUP_JOB));
            }
        Test.stopTest();
    }

    @isTest
    private static void testScheduleClearPaymentInformationNightly() {
        Test.startTest();
            ScheduleSharingJobsController.deleteJobsRemotely();
            
            ScheduleSharingJobsController cont = new ScheduleSharingJobsController();
            cont.scheduleClearPaymentInformationNightly();     

            CronTrigger cron = getCronTriggerByCronJobDetailName(ScheduleSharingJobsController.CLEAR_PAYMENT_INFO_JOB_NAME);
            System.assertEquals(ScheduleSharingJobsController.midnightScheduleString, cron.CronExpression);
            
            try {
                cont.scheduleClearPaymentInformationNightly();
            } catch (Exception ex) {
                System.assertEquals(
                    ex.getMessage(),
                    ScheduleSharingJobsController.getAlreadyScheduledJobErrorMessage(
                        ScheduleSharingJobsController.CLEAR_PAYMENT_INFO_JOB_NAME));
            }
        Test.stopTest();
    }

    private static CronTrigger getCronTriggerById(Id jobId) {
        return CronTriggerSelector.Instance.selectById(new Set<Id>{jobId})[0];
    }

    private static CronTrigger getCronTriggerByCronJobDetailName(String jobName) {
        return CronTriggerSelector.Instance.selectByCronJobDetailName(new List<String>{jobName})[0];
    }
    
    //SFP-1149
    @isTest
    private static void scheduleClearOrphanedSchoolDocumentAssignmentsNightly_deleteSpaWithSdaRecords_relaterSdasAreDeleted() {
        
        //1. Create test data records.
        Academic_Year__c academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        
        Account school = AccountTestData.Instance.forName('School1')
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forMaxNumberOfUsers(5)
                .insertAccount();
                
        Contact parentA = ContactTestData.Instance.asParent().create();
        Contact student = ContactTestData.Instance.asStudent().create();
        insert new List<Contact> { parentA, student };
                
        PFS__c pfs = PfsTestData.Instance
            .forParentA(parentA.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .asPaid()
            .insertPfs();
            
        Applicant__c applicant = ApplicantTestData.Instance
            .forContactId(student.Id)
            .forPfsId(pfs.Id)
            .insertApplicant();
                
        Student_Folder__c studentFolder = StudentFolderTestData.Instance
            .forAcademicYearPicklist(academicYear.Name)
            .forStudentId(student.Id)
            .insertStudentFolder();
                
        School_PFS_Assignment__c schoolPfsAssignment = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicant.Id)
            .forStudentFolderId(studentFolder.Id)
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(academicYear.Name)
            .createSchoolPfsAssignmentWithoutReset();
        insert schoolPfsAssignment;
        
        //2. Create 2 SDAs.
        List<School_Document_Assignment__c> sdas = new List<School_Document_Assignment__c>{
            SchoolDocumentAssignmentTestData.Instance
                .asType('W2')
                .forDocumentYear(academicYear.Name.left(4))
                .forPfsAssignment(schoolPfsAssignment.Id)
                .create(),
            SchoolDocumentAssignmentTestData.Instance
                .asType('1040')
                .forDocumentYear(academicYear.Name.left(4))
                .forPfsAssignment(schoolPfsAssignment.Id)
                .create()};
            insert sdas;
        
        //3. Save the SPA Id.
        Id spaId = schoolPfsAssignment.Id;
        System.AssertNotEquals(null, spaId);
        
        //4. Save the SDAs Ids.
        Id sda1Id = sdas[0].Id;
        Id sda2Id = sdas[1].Id;
        System.AssertNotEquals(null, sda1Id);
        System.AssertNotEquals(null, sda2Id);
        
        //5. Verify that the SDA exists in Database.
        System.assertEquals(1, (new List<School_Document_Assignment__c>([
            SELECT Id FROM School_Document_Assignment__c 
            WHERE Id =: sda1Id])).size());
        
        System.assertEquals(1, (new List<School_Document_Assignment__c>([
            SELECT Id FROM School_Document_Assignment__c 
            WHERE Id =: sda2Id])).size());
        
        //6. Delete the SPA record.
        delete schoolPfsAssignment;
            
        Test.startTest();
           
           //7. Verify that the SDA still exists in Database. 
            System.assertEquals(1, (new List<School_Document_Assignment__c>([
                SELECT Id FROM School_Document_Assignment__c 
                WHERE Id =: sda1Id])).size());
            
            System.assertEquals(1, (new List<School_Document_Assignment__c>([
                SELECT Id FROM School_Document_Assignment__c 
                WHERE Id =: sda2Id])).size());
            
            //8. We run the scheduled job to delete orphaned SDA records.
            ScheduleSharingJobsController cont = new ScheduleSharingJobsController();
            cont.scheduleClearOrphanedSchoolDocumentAssignmentsNightly();
            Database.executeBatch(new ClearOrphanedSDABatch(), 20);
        Test.stopTest();

        //9. Verify that the SDAs were sucessfully deleted.
        System.assertEquals(0, new List<School_Document_Assignment__c>([SELECT Id FROM School_Document_Assignment__c]).size(),
                'Expected all SDAs to be deleted.');
    }

    @isTest
    private static void scheduleRecalcSharingForMergedContacts_expectNoErrorsAndEarlyReturn()
    {
        Test.startTest();
            ScheduleSharingJobsController.deleteJobsRemotely();

            ScheduleSharingJobsController cont = new ScheduleSharingJobsController();
            cont.scheduleRecalcSharingForMergedContacts();
            Database.executeBatch(new RecalcSharingForMergedContactsBatch());

            System.assertEquals(1, [SELECT count() FROM CronTrigger], 'A job should be scheduled');
        Test.stopTest();
    }

    @isTest
    private static void scheduleRecalcSharingForMergedContacts_expectRecordsProcessed()
    {
        Integer numberOfRecordsToRecalculate = 50;

        List<Contact> parents = createContactsForRecalcSharing(numberOfRecordsToRecalculate);
        insert parents;
        List<PFS__c> pfss = createPfsForRecalcSharing(parents);
        insert pfss;
        List<Applicant__c> applicants = createApplicantsForRecalcSharing(pfss);
        insert applicants;
        List<School_PFS_Assignment__c> spfsas = createSpfsasForRecalcSharing(applicants);
        insert spfsas;

        Test.startTest();
            ScheduleSharingJobsController.deleteJobsRemotely();

            ScheduleSharingJobsController cont = new ScheduleSharingJobsController();
            cont.scheduleRecalcSharingForMergedContacts();
            Database.executeBatch(new RecalcSharingForMergedContactsBatch());
        Test.stopTest();

        List<School_PFS_Assignment__c> updatedSpfsas = SchoolPfsAssignmentsSelector.Instance.selectByIdWithCustomFieldList(
            new Map<Id, School_PFS_Assignment__c>(spfsas).keySet(),
            new List<String>{'Sharing_Processed__c', 'Applicant__r.PFS__r.Parent_A__r.Recalc_Sharing_For_Contact_Merge__c'});

        Boolean spfsaAndContactWereUpdated = true;
        for (School_PFS_Assignment__c spfsa : updatedSpfsas)
        {
            if (spfsa.Sharing_Processed__c || spfsa.Applicant__r.PFS__r.Parent_A__r.Recalc_Sharing_For_Contact_Merge__c)
            {
                spfsaAndContactWereUpdated = false;
                break;
            }
        }

        System.assert(spfsaAndContactWereUpdated);
    }

    private static List<Contact> createContactsForRecalcSharing(Integer numberOfRecordsToRecalculate)
    {
        List<Contact> parents = new List<Contact>();
        for (Integer i = 0; i < numberOfRecordsToRecalculate; i++)
        {
            Contact parent = ContactTestData.Instance.asParent().forRecalcSharingForContactMerge(true).create();

            parents.add(parent);
        }

        return parents;
    }

    private static List<PFS__c> createPfsForRecalcSharing(List<Contact> parents)
    {
        List<PFS__c> pfss = new List<PFS__c>();
        for (Contact parent : parents)
        {
            PFS__c pfs = PfsTestData.Instance.forParentA(parent.Id).create();

            pfss.add(pfs);
        }

        return pfss;
    }

    private static List<Applicant__c> createApplicantsForRecalcSharing(List<PFS__c> pfss)
    {
        List<Applicant__c> applicants = new List<Applicant__c>();
        for (PFS__c pfs : pfss)
        {
            Applicant__c applicant = ApplicantTestData.Instance.forPfsId(pfs.Id).create();

            applicants.add(applicant);

        }

        return applicants;
    }

    private static List<School_PFS_Assignment__c> createSpfsasForRecalcSharing(List<Applicant__c> applicants)
    {
        List<School_PFS_Assignment__c> spfsas = new List<School_PFS_Assignment__c>();
        for (Applicant__c applicant : applicants)
        {
            School_PFS_Assignment__c spfsa = SchoolPFSAssignmentTestData.Instance
                .forApplicantId(applicant.Id)
                .forSharingProcessed(true).create();

            spfsas.add(spfsa);
        }

        return spfsas;
    }
}