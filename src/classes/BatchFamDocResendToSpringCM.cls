global class BatchFamDocResendToSpringCM implements Database.Batchable<sObject>, Database.AllowsCallouts {
    
    public String query;
    public Date minDate;
    
    global BatchFamDocResendToSpringCM() {
        minDate = Date.newInstance(2016, 4, 5);
        query = 'select id, System_Processing_Error__c FROM Family_Document__c where LastModifiedDate >= :minDate AND SYSTEM_Need_Resend__c = TRUE order by CreatedDate asc';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

       global void execute(Database.BatchableContext BC, List<sObject> scope) {
        for (sObject so : scope){
            Family_Document__c fd = (Family_Document__c) so;
            if (fd.System_Processing_Error__c != null && !String.isBlank(fd.System_Processing_Error__c)){
                System.debug(LoggingLevel.ERROR, 'TESTINGDrew SEND THEN UPDATE');
                SpringCMAction.sendInfoToSpringCMSynchronous(new Set<Id>{fd.Id}, UserInfo.getSessionId());
                System.debug(LoggingLevel.ERROR, 'TESTINGDrew SENT');
                fd.System_Processing_Error__c = null;
                fd.SYSTEM_Need_Resend__c = false;
                update fd;
                System.debug(LoggingLevel.ERROR, 'TESTINGDrew  UPDATED');

            }
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
}