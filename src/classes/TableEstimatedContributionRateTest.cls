@IsTest
private class TableEstimatedContributionRateTest
{

    @isTest
    private static void testTableEstimatedContributionRate()
    {
        // create the academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        // create the table
        TableTestData.populateEstimatedContributionRateData(academicYear.Id);

        // test low range
        System.assertEquals(220, TableEstimatedContributionRate.getEstimatedContributionRate(academicYear.Id, 1000));

        // test middle range
        System.assertEquals(2326, TableEstimatedContributionRate.getEstimatedContributionRate(academicYear.Id, 10000));

        // test high range
        System.assertEquals(40643, TableEstimatedContributionRate.getEstimatedContributionRate(academicYear.Id, 100000));
    }
}