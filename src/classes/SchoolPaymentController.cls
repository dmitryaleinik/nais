/**
 * @description Controls the School Payment page.
 **/
public without sharing class SchoolPaymentController extends PaymentControllerBase {
    private static final String OPPORTUNITY_PARAM_NAME = 'id';
    private static final String MESSAGE_EMAIL_CONFIRM_MISMATCH = 'Error: The email addresses do not match';
    private static final String MESSAGE_CHECK_ACCOUNT_NUMBER_CONFIRM_MISMATCH = 'Error: The account numbers do not match';
    private static final String MESSAGE_PAYMENT_ERROR_TEMPORARY = Label.PaymentErrorTemp;
    private static final String MESSAGE_PAYMENT_ERROR_DECLINE = Label.PaymentErrorDecline;
    private static final String MESSAGE_PAYMENT_ERROR_BAD_DATA = Label.PaymentErrorBadData;
    private static final String MESSAGE_UNSUCCESSFUL_PAYMENT_ATTEMPTS = Label.TLI_Unsuccessful_Payment_Attempts;

    @testVisible private static final String MESSAGE_PAYMENT_ERROR_GENERAL = Label.PaymentErrorGeneral;

    public Contact thePayer { get; set; }
    public Account theSchool { get; set; }

    public String oppId { get; set; }
    public Opportunity theOpportunity { get; set; }

    public Boolean isFeeWaiver { get; set; }
    public Boolean isSubscription { get; set; }

    public Decimal feeWaiverQuantity { get; set; }

    public String paymentStep { get; set; }

    /**
     * @description Whether or not there has been an error during the processBefore()
     *              method. This will make sure that the processPayment() method does
     *              not continue.
     */
    public Boolean hasProcessBeforeErrors {
        get {
            if (hasProcessBeforeErrors == null) {
                return false;
            }
            return hasProcessBeforeErrors;
        }
        set;
    }

    public Boolean isTestMode {
        get {
            return PaymentProcessor.InTestMode;
        }
    }

    public String billingEmailConfirm { get; set; }
    public String checkAccountNumberConfirm { get; set; }

    public SchoolPaymentController Me {
        get { return this; }
    }

    public String getCreditCardStatementDescriptor() {
        return PaymentProcessor.CreditCardStatementDescriptor;
    }

    /**
     * @description Constructor that sets up the current instance of the School Payment page.
     *              Calls the PaymentControllerBase constructor to setup the base class defaults
     *              as well.
     */
    public SchoolPaymentController() {
        // call the base class constructor
        super();

        // initialize the page status vars
        PageError = false;

        // get the payer using the current user and its related Contact
        User currentUser = GlobalVariables.getCurrentUser();
        thePayer = this.getPayerRecord(currentUser.contactId);

        if (thePayer == null) {
            PageError = true;
            ErrorMessage = 'Payer record not found';
            return;
        }

        // get the school account
        theSchool = GlobalVariables.getCurrentSchool();

        // get the opportunity
        oppId = ApexPages.currentPage().getParameters().get(OPPORTUNITY_PARAM_NAME);
        if (String.isBlank(oppId)) {
            PageError = true;
            ErrorMessage = 'Missing Opportunity Id parameter';
            return;
        }

        for (Opportunity opp : [select Id, Academic_Year_Picklist__c, RecordTypeId,
                                    Subscription_Type__c, Amount, Subscription_Discount__c,                    // subscription fee
                                    Net_Amount_Due__c, Total_Purchased_Waivers__c, Total_Write_Offs__c        // fee waiver
                                from Opportunity
                                where Id = :oppId
                                and AccountId = :theSchool.Id
                                and StageName not in :OpportunityAction.closedoppStageNames]) {
            theOpportunity = opp;
            isFeeWaiver = (opp.RecordTypeId == RecordTypes.opportunityFeeWaiverPurchaseTypeId);
            isSubscription = (opp.RecordTypeId == RecordTypes.opportunitySubscriptionFeeTypeId);
            feeWaiverQuantity = opp.Total_Purchased_Waivers__c;
        }

        if (theOpportunity == null) {
            PageError = true;
            ErrorMessage = 'Payment opportunity record not found';
            return;
        }

        // set up the payment data structure
        paymentData.paymentType = FamilyPaymentData.PAYMENT_TYPE_CC; // default to credit card payment
        paymentData.billingCountry = PaymentProcessor.getDefaultCountryCode();

        // prepopulate the billing information
        paymentData.billingFirstName = thePayer.FirstName;
        paymentData.billingLastName = thePayer.LastName;
        paymentData.billingStreet1 = thePayer.MailingStreet;
        paymentData.billingCity = thePayer.MailingCity;
        paymentData.billingPostalCode = thePayer.MailingPostalCode;
        paymentData.billingEmail = thePayer.Email;

        // try to look up the country code
        if (thePayer.MailingCountry != null) {
            paymentData.billingCountry = PaymentProcessor.getCountryCode(thePayer.MailingCountry);
        }

        if (paymentData.billingCountry == PaymentProcessor.getDefaultCountryCode()) {
            paymentData.billingState = thePayer.MailingState;
        }

        // get the application fee
        Decimal balanceDue = this.getNetAmountDue();
        if (balanceDue == null || balanceDue <= 0 ) {
            PageError = true;
            ErrorMessage = 'You have no balance due at this time.';
            return;
        }
        paymentData.paymentAmount = balanceDue;

        paymentStep = 'PAYMENT_SELECT';

        // show payment decline error
        String errParam = System.currentPagereference().getParameters().get('paymenterror');
        if (errParam != null && errParam == 'true') {
            PageError = true;
            if (ErrorMessage == null) {
                ErrorMessage = System.currentPagereference().getParameters().get('errormessage');
            }
        }
    }

    /**
     * @description The School Payment page's initialization method defined in the
     *              page action attribute. Nothing occurs here.
     * @returns A null PageReference.
     */
    public PageReference initMethod() {
        return null;
    }

    /**
     * @description Cancel the current payment process and send the user back
     *              to the school renewal page if they are renewing a subscription,
     *              otherwise back to the school dashboard.
     * @returns A PageReference to the School Renewal page if this is a subscription
     *          renewal, otherwise to the School Dashboard.
     */
    public PageReference actionCancel() {
        if (isSubscription) {
            return Page.SchoolRenewal.setRedirect(true);
        } else {
            return Page.SchoolDashboard.setRedirect(true);
        }
    }

    /**
     * @description Navigate the user to the School Payment page after clearing any
     *              page errors and defining payment select as the next step.
     * @returns A PageReference to the Payment Select page.
     */
    public PageReference actionPaymentSelect() {
        clearPageError();
        paymentStep = 'PAYMENT_SELECT';

        return Page.SchoolPayment;
    }

    public PageReference actionPaymentForm() {
        PageReference pr = null;
        clearPageError();

        String paymentType = ApexPages.currentPage().getParameters().get('paymentTypeParam');
        if (paymentType != null) {
            paymentData.paymentType = paymentType;
        }

        if (paymentData.paymentType == FamilyPaymentData.PAYMENT_TYPE_INVOICE) {
            pr = actionInvoiceSubmit();
        } else {
            paymentStep = 'PAYMENT_FORM';
        }

        // Let's go ahead and update the Opportunity with the new close date
        theOpportunity.CloseDate = System.today();
        update theOpportunity;

        return pr;
    }

    public PageReference actionPaymentConfirm() {
        clearPageError();
        paymentData.trimStrings();

        Boolean isValid = false;
        if (paymentData.paymentType == FamilyPaymentData.PAYMENT_TYPE_CC) {
            isValid = validateCreditCardForm();
        } else if (paymentData.paymentType == FamilyPaymentData.PAYMENT_TYPE_ECHECK) {
            isValid = validateECheckForm();
        }

        if (isValid) {
            PaymentService.Request request = new PaymentService.Request(theOpportunity, null, paymentData, true);
            PaymentService.Response response = PaymentService.Instance.processBefore(request);

            if (!response.Result.isSuccess) {
                hasProcessBeforeErrors = true;
                String message = (response.Result.ErrorMessage == null) ?
                        PaymentProcessor.UNEXPECTED_ERROR : response.Result.ErrorMessage;
                setPageError(message);
                return null;
            }
        }

        if (!isValid) {
            setPageError(MESSAGE_PAGE_VALIDATION_ERROR);
        }

        if (this.PageError != true && !hasProcessBeforeErrors) {
            paymentStep = 'PAYMENT_CONFIRM';
        }

        return null;
    }

    public PageReference actionPaymentSubmit() {
        clearPageError();
        PageReference pr = Page.SchoolPaymentSubmit;
        return pr;
    }

    public PageReference actionInvoiceSubmit() {
        clearPageError();

        paymentStep = 'PAYMENT_INVOICE';

        return null;
    }

    public PageReference actionInvoiceDone() {
        return Page.SchoolDashboard;
    }

    /**
     * @description Handle a transaction error by setting this instance's errorMessage variable
     *              to the error associated with the transaction result.
     */
    @testVisible
    private void handleTransactionError(PaymentResult result, Boolean errorLimit) {
        this.PageError = true;

        if (errorLimit) {
            this.ErrorMessage = MESSAGE_UNSUCCESSFUL_PAYMENT_ATTEMPTS;
        } else {
            this.ErrorMessage = result.getErrorMessage();
        }
    }

    /**
     * @description Process the payment being provided by the school using the information
     *              provided by the user on the School Payment page.
     * @returns A PageReference that will send the user to the School Renewal page if the
     *          payment is successful, otherwise the School Payment Error page.
     */
    public override PageReference processPayment() {
        paymentData.paymentDescription = 'Subscription Renewal for Account ' + theOpportunity.Id;
        paymentData.paymentTracker = theOpportunity.Id;

        if ((paymentData.paymentAmount == null) || (paymentData.paymentAmount <= 0)) {
            this.PageError = true;
            this.ErrorMessage = 'Unable to process payment amount: ' + paymentData.paymentAmount;
            return null;
        }

        PaymentService.Request request = new PaymentService.Request(theOpportunity, null, paymentData, true);
        PaymentService.Response response = PaymentService.Instance.processPayment(request);

        // Subscription flow: insert missing Sale and Discount (if any) TLI
        if (isSubscription) {
            insertSaleAndDiscountTLIs();
        }

        if (!response.Result.isSuccess) {
            this.PageError = true;
            handleTransactionError(response.Result, response.ErrorMessage == MESSAGE_UNSUCCESSFUL_PAYMENT_ATTEMPTS);
        }

        if (response.ErrorOccurred) {
            this.PageError = true;
            this.ErrorMessage = response.ErrorMessage;
            return handlePaymentError();
        }

        // Show success or error page
        PageReference pr;
        if (response.Result != null && response.Result.isSuccess == true) {
            // update Opportunity StageName to 'Closed Won' to trigger creation of Subscription Record
            theOpportunity.StageName = 'Closed Won';
            theOpportunity.CloseDate = System.today();
            update theOpportunity;

            // Subscription Payment Thank You page
            pr = Page.SchoolRenewal;
            pr.getParameters().put(OPPORTUNITY_PARAM_NAME, theOpportunity.Id);
            pr.setRedirect(true); // force a GET to flush the page state and prevent a re-post on page reload
        } else {
            pr = Page.SchoolPaymentError;
        }

        return pr;
    }

    /**
     * @description Reload the School Payment page with page messages displaying the
     *              current errors with this transaction.
     * @returns A PageReference to the School Payment page.
     */
    public override PageReference handlePaymentError() {
        PageReference pr = Page.SchoolPayment;
        pr.getParameters().put(OPPORTUNITY_PARAM_NAME, theOpportunity.Id);
        pr.getParameters().put('paymenterror', 'true');
        pr.getParameters().put('errormessage', this.errormessage);

        pr.setRedirect(true);
        return pr;
    }

    private Decimal getNetAmountDue() {
        Decimal netAmountDue = null;
        if (theOpportunity != null) {
            if (isFeeWaiver) {
                netAmountDue = theOpportunity.Net_Amount_Due__c;

                // NAIS-1235: add any writeoffs to the net amount due
                if (theOpportunity.Total_Write_Offs__c != null) {
                    netAmountDue = netAmountDue + theOpportunity.Total_Write_Offs__c;
                }
            } else if (isSubscription) {
                // not using Net Amount Due rollup from TLIs; Sale TLI is not inserted until payment is made
                netAmountDue = theOpportunity.Amount != null ? theOpportunity.Amount : 0;
                netAmountDue -= theOpportunity.Subscription_Discount__c != null ? theOpportunity.Subscription_Discount__c : 0;
            }
        }
        return netAmountDue;
    }

    @testVisible
    private void clearPageError() {
        this.PageError = false;
        this.ErrorMessage = null;
        this.hasProcessBeforeErrors = false;
        this.fieldErrors = new FieldErrors();
    }

    private void setPageError(String message) {
        this.PageError = true;
        this.ErrorMessage = message;
    }

    // School Subscription Renewal:
    // Insert Sale and Discount TLI only if none exists for the Opportunity
    //    Reuse existing Sale and Discount TLI in case previous payment attempt failed and user goes through renewal flow again
    //    Subscription Type cannot be changed in re-entry of renewal flow:
    //        Basic subscription will not have payment failure
    //        Basic account with failed payment attempt for upgrade to Full will not have choice to go back to Basic on re-entering renewal flow
    private void insertSaleAndDiscountTLIs() {
        Boolean hasSaleTLI = false;
        Boolean hasDiscountTLI = false;

        for (Transaction_Line_Item__c tli : [select RecordTypeId from Transaction_Line_Item__c where Opportunity__c = :theOpportunity.Id]) {
            if (tli.RecordTypeId == RecordTypes.saleTransactionTypeId) {
                hasSaleTLI = true;
            }
            if (tli.RecordTypeId == RecordTypes.adjustmentTransactionTypeId) {
                hasDiscountTLI = true;
            }
        }

        List<Transaction_Line_Item__c> transactionsToInsert = new List<Transaction_Line_Item__c>();
        if (!hasSaleTLI) {
            transactionsToInsert.add( TransactionLineItemUtils.createSaleTLI(theOpportunity, false) );
        }

        if (!hasDiscountTLI && theOpportunity.Subscription_Discount__c != null && theOpportunity.Subscription_Discount__c > 0) {
            Transaction_Line_Item__c discountTransaction = TransactionLineItemUtils.createSchoolEarlyBirdDiscountTLI(
                    theOpportunity, theOpportunity.Subscription_Discount__c, false);

            if (discountTransaction != null && discountTransaction.Id == null) {
                transactionsToInsert.add(discountTransaction);
            }
        }
        if (!transactionsToInsert.isEmpty()) { insert transactionsToInsert; }
    }

    /**
     * @description Get the Contact associated with the given Payer Contact Id.
     * @param payerId The Id of the Contact record to query for.
     * @return A Contact record associated with the given Payer Contact Id.
     */
    private Contact getPayerRecord(Id payerId) {
        Contact payerRecord = null;

        for (Contact c : [select Id, AccountId, FirstName, LastName, Email, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry
        from Contact where Id = :payerId limit 1]) {
            payerRecord = c;
        }

        return payerRecord;
    }

    private Boolean validateECheckAccountConfirm() {
        Boolean isValid = true;
        if (String.isBlank(checkAccountNumberConfirm)) {
            isValid = false;
            fieldErrors.checkAccountNumberConfirm = MESSAGE_FIELD_VALUE_MISSING;
        }
        else if (checkAccountNumberConfirm.trim() != paymentData.checkAccountNumber) {
            isValid = false;
            fieldErrors.checkAccountNumberConfirm = MESSAGE_CHECK_ACCOUNT_NUMBER_CONFIRM_MISMATCH;
        }

        return isValid;
    }

    private Boolean validateEmailConfirm() {
        Boolean isValid = true;

        if (String.isBlank(billingEmailConfirm)) {
            isValid = false;
            fieldErrors.billingEmailConfirm = MESSAGE_FIELD_VALUE_MISSING;
        }
        else if (billingEmailConfirm.trim() != paymentData.billingEmail) {
            isValid = false;
            fieldErrors.billingEmailConfirm = MESSAGE_EMAIL_CONFIRM_MISMATCH;
        }

        return isValid;
    }

    private Boolean validateCreditCardForm() {
        Boolean isValid = true;

        Boolean isValidBillingFields = validateBillingFields();
        Boolean isValidCreditCardFields = validateCreditCardFields();

        isValid = isValidBillingFields && isValidCreditCardFields;    // && isValidEmailConfirm --> NAIS-909 no longer capturing email

        return isValid;
    }

    private Boolean validateECheckForm() {
        Boolean isValid = true;

        Boolean isValidBillingFields = validateBillingFields();
        Boolean isValidECheckFields = validateECheckFields();
        Boolean isValidEmailConfirm = validateEmailConfirm();
        Boolean isValidECheckAccountConfirm = validateECheckAccountConfirm();

        isValid = isValidBillingFields && isValidECheckFields && isValidECheckAccountConfirm && isValidEmailConfirm;

        return isValid;
    }
}