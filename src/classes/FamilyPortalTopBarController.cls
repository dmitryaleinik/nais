/**
* @description This class is user for providing relevant information 
*               and conditional html rendering in the Family Portal.
*/

public class FamilyPortalTopBarController
{

    /*Initialization*/
    public FamilyPortalTopBarController()
    {
        //Avoid querying if we have a guest user.
        if (CurrentUser.isGuest()) { return; }
        // Look up the current user record's related Contact record
        userRecord = [
            SELECT Id, Name, ContactId, Contact.LastName, Contact.Name, Contact.MailingStreet, Contact.MailingCity, Contact.MailingState,  
            Contact.MailingPostalCode, Contact.MailingCountry, Contact.Gender__c, Contact.Birthdate, 
            Contact.Email, Contact.HomePhone, Contact.MobilePhone, Contact.Preferred_Phone__c,
            Contact.FirstName, Contact.Middle_Name__c, Contact.Suffix__c, Contact.Salutation, Contact.Phone,
            LanguageLocaleKey
            FROM User 
            WHERE Id = :UserInfo.getUserId()];
    }
    /*End Initialization*/
    
    /*Properties*/
    public String selectedTab {get; set;}
    public String currentPFSId {get; set;}
    public PFS__c currentPFSRecord {get; set;}
    private User userRecord {get; set;}
    
    public Boolean getcanCreatePFS()
    {
        Boolean canCreatePFS = false;
        // If the academic year can be changed
        if(inOverlapPeriodOrMultipleYears){
            Set<String> academicYearNames = new Set<String>{}; // NAIS-2417 [DP] 05.19.2015
            for(SelectOption option : academicYearOptions){
                academicYearNames.add(option.getLabel());
            }
            
            // if this is a portal user, use the associated Contact Id. otherwise it is probably a call center user and we should
            // use the parent A id from the PFS
            Id parentAContactId = userRecord.ContactId != null ? userRecord.ContactId : currentPFSRecord.Parent_A__c;
            Integer pfsRecordCount = [select count() from PFS__c where Parent_A__c = :parentAContactId and Academic_Year_Picklist__c in :academicYearNames ];

            if(pfsRecordCount < 2){
                canCreatePFS = true;
            }
        }
        
        return canCreatePFS;
    }

    public Boolean pfsSubmittedAndPaid
    {
        get
        {
            if (currentPFSRecord != null && currentPFSRecord.PFS_Status__c == 'Application Submitted' 
                && currentPFSRecord.Payment_Status__c == 'Paid In Full')
            {
                return true;
            }
            
            return false;
        } 
        set;
    }
    
    public String documentPageUrl
    {
        get
        {
            // TO DO: Make this return the url for the My Documents tab to use 
            PageReference documentsPageRef = Page.FamilyDocuments;
            documentsPageRef.getParameters().put('id',currentPfsId);
            documentsPageRef.getParameters().put('academicyearid',academicYearId);
            return documentsPageRef.getUrl();
        }
        set; 
    }
    
    public FamilyPortalTopBarInterface Consumer
    {
        get; 
        set
        {
            Consumer = value;
            academicYearId = Consumer.getAcademicYearId();
        }
    }
    
    public String academicYearId
    { 
        get; 
        set
        {
            academicYearId = value;
            if (newAcademicYearId == null)
            {
                newAcademicYearId = value;
            }
        } 
    }
    public String newAcademicYearId { get; set; }
    private List<SelectOption> descAcademicYearOptions_x = null; //NAIS-1874
    public List<SelectOption> academicYearOptions
    {
        get
        {
            if (academicYearOptions == null)
            {
                academicYearOptions = new List<SelectOption>();

                for(Academic_Year__c year : getAcademicYears())
                {
                    academicYearOptions.add(new SelectOption(year.Id, year.Name));
                }
            }
            this.academicYearOptions.sort();
            descAcademicYearOptions_x = new List<SelectOption>();
            for(Integer i = this.academicYearOptions.size()-1; i >= 0; i--)
            {
                descAcademicYearOptions_x.add(this.academicYearOptions[i]);
            }
            return this.descAcademicYearOptions_x;
        }
        private set;
    }
    
    public Boolean inOverlapPeriodOrMultipleYears
    {
        get
        {
            return (academicYearOptions.size() > 1);
        }
    }
    
    // pfs display name
    public String getPFSName()
    {
        return ApplicationUtils.calcPFSNameForUser(currentPFSRecord);
    }
    
    // pfs user friendly status
    public String getPFSStatus()
    {
        return ApplicationUtils.calcPFSStatusForUser(currentPFSRecord);
    }
    
    // pfs status css class
    public String getPFSStatusCSS()
    {
        return ApplicationUtils.calcPFSStatusForCSS(currentPFSRecord);
    }
    
    public Boolean getIsFamilyPortalUser()
    {
        return userRecord.ContactId != null;
    }
    /*End Properties*/

    public PageReference AcademicYearSelectorOnChange()
    {
        // Re-query the PFS record for the specified Academic Year
        List<PFS__c> tempPFSs = [
            SELECT Id 
            FROM PFS__c 
            WHERE Parent_A__c = :currentPFSRecord.Parent_A__c 
                AND Academic_Year_Picklist__c = : GlobalVariables.getAcademicYear(newAcademicYearId).Name];

        ID pfsId;
        
        if (tempPFSs != null && tempPFSs.size() > 0)
        {
            pfsId = tempPFSs[0].Id;
        } else {
            // Auto-create a blank PFS for the selected year
            PFS__c newPFS = ApplicationUtils.generateBlankPFS(newAcademicYearId, userRecord);
            pfsId = newPFS.Id;
        }

        // Set the new PFS Id value on the underlying page controller
        Consumer.YearSelector_OnChange(pfsId);
        
        PageReference reloadPage = Page.FamilyDashboard;
        reloadPage.getParameters().put('id', pfsId);
        reloadPage.getParameters().put('academicyearid', newAcademicYearId);
        reloadPage.getParameters().remove('AJAXREQUEST');
        reloadPage.setRedirect(true);
        
        return reloadPage;
    }
    
    public List<Academic_Year__c> getAcademicYears()
    {
        return AcademicYearService.Instance.getAvailableAcademicYears();
    }
    
    public Boolean getShowAwardsTab() {
        
        return FamilyAwardsService.Instance.showAwardsTabForPFS(currentPFSId);
    }
}