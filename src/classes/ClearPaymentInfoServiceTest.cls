@isTest
private class ClearPaymentInfoServiceTest {
    
    @isTest
    private static void testHandleTransactionAndOrderRecords() {
        Integer numberOfOrders = 10;
        List<ChargentOrders__ChargentOrder__c> chargentOrders = 
            ClearPaymentInfoTestHelper.Instance.createChargentOrderWithPaymentData(numberOfOrders);
        List<ChargentOrders__Transaction__c> transactions = 
            ClearPaymentInfoTestHelper.Instance.createTransactionsWithPaymentData(chargentOrders);

        Test.startTest();
            ClearPaymentInfoService.Instance.handleTransactionAndOrderRecords(transactions);
        Test.stopTest();

        transactions = ClearPaymentInfoTestHelper.Instance.getTransactionsByIds(
            new Map<Id,ChargentOrders__Transaction__c>(transactions).keySet());
        List<ChargentOrders__Transaction__c> transactionsToCheck = 
            new List<ChargentOrders__Transaction__c>{transactions[0], transactions[transactions.size()-1]};
        System.assert(ClearPaymentInfoTestHelper.Instance.transactionsPaymentInfoIsCLeared(transactionsToCheck));
        
        chargentOrders = ClearPaymentInfoTestHelper.Instance.getChargentOrdersByIds(
            new Map<Id,ChargentOrders__ChargentOrder__c>(chargentOrders).keySet());
        List<ChargentOrders__ChargentOrder__c> chargentOrdersToCheck = 
            new List<ChargentOrders__ChargentOrder__c>{chargentOrders[0], chargentOrders[chargentOrders.size()-1]};
        System.assert(ClearPaymentInfoTestHelper.Instance.ordersPaymentInfoIsCleared(chargentOrdersToCheck));
    }
}