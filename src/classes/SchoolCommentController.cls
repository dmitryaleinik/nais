/**
 * Class: SchoolCommentController
 *
 * Copyright (C) 2015 NAIS
 *
 * Purpose: Provides the functionality to create a new comment for a school admin
 * 
 * Where Referenced:
 *   School Portal - SchoolPFSSummary page
 *
 * Change History:
 *
 * Developer           Date        JIRA Ref     Description
 * ---------------------------------------------------------------------------------------
 * Leydi Rangel         24/03/15    NAIS-2344   Initial Development
 *
 * 
 */
public with sharing class SchoolCommentController {    
    /*Initialization*/
    public SchoolCommentController()
    {
        tabName = ApexPages.currentPage().getParameters().get('tabName');
        schoolPFSAssignmentId = ApexPages.currentPage().getParameters().get('schoolPFSAssignmentId');
        noteId = ApexPages.currentPage().getParameters().get('id');

        // NAIS-2337 [DP] 04.09.2015 
        schoolPFSAssignment = [select Applicant_First_Name__c, Applicant_Last_Name__c, School__c, Student_Folder__r.Id, Student_Folder__r.Student__c, Applicant__r.Id,Applicant__r.PFS__c
                                        from School_PFS_Assignment__c 
                                        where Id = :schoolPFSAssignmentId limit 1];

        if(noteId!=null){
            note = [SELECT Body__c, Id, Name, OwnerId, CreatedDate FROM Note__c WHERE Id=: noteId];
        }
        
        if(!( note!=null && note.Id!=null))
        {
            note = new Note__c();
            note.ParentId__c = schoolPFSAssignmentId;
            note.OwnerId = UserInfo.getUserId();
        }

        copyToSiblings = true;

        siblingSPAs = GlobalVariables.getSiblingApplicantSchoolPFSAssignments(new Set<Id> {schoolPFSAssignment.Applicant__r.PFS__c}, schoolPFSAssignment.Student_Folder__r);
        hasSiblings = !siblingSPAs.isEmpty();
    }
    /*End Initialization*/
    
    
    /*Properties*/
    public String schoolPFSAssignmentId {get; set;}
    public School_PFS_Assignment__c schoolPFSAssignment {get; set;} // NAIS-2337 [DP] 04.09.2015 
    public String tabName {get; set;}
    public String SchoolPFSAssignmentStudentName {
        get{
            this.SchoolPFSAssignmentStudentName = '';
            if( schoolPFSAssignmentId!=null )
            {
                // NAIS-2337 [DP] 04.09.2015 moved query to constructor
                School_PFS_Assignment__c pfsAssigment = schoolPFSAssignment;
                if( pfsAssigment!=null && pfsAssigment.Id!=null )
                {
                    this.SchoolPFSAssignmentStudentName = pfsAssigment.Applicant_First_Name__c+
                                                        ' '+pfsAssigment.Applicant_Last_Name__c;
                }
            }
            return this.SchoolPFSAssignmentStudentName;
        }
        set;
    }

    public Id noteId {get; set;}
    public Note__c note {get; set;}
    public Boolean isViewMode{
        get{
            return Boolean.ValueOf(ApexPages.currentPage().getParameters().get('isViewMode'));
        }
        set;
    }
    public Boolean yearIsEditable{
        get{
            Boolean isEditable = Boolean.ValueOf(ApexPages.currentPage().getParameters().get('yearIsEditable'));
            isEditable = (isEditable==null?false:isEditable);
            return isEditable;
        }
        set;
    }

    private List<School_PFS_Assignment__c> siblingSPAs; // NAIS-2337 [DP] 04.13.2015
    public Boolean hasSiblings {get; set;}
    public Boolean copyToSiblings {get; set;} // NAIS-2337 [DP] 04.09.2015
    
    private PageReference returnURL()
    {        
        PageReference returnPage = Page.SchoolPFSSummary;
        returnPage.getParameters().put('schoolpfsassignmentid', schoolPFSAssignmentId);
        returnPage.getParameters().put('tabName', ( String.isBlank(tabName) ? 'PFS1' : tabName) );
        returnPage.setRedirect(true);
        return returnPage;
    }
    /*End Properties*/    
    
    /*Action Methods*/    
    public PageReference savelNote()
    {
        try {
            // NAIS-2337 [DP] 04.09.2015 
            List<Note__c> allNotesForUpsert = new List<Note__c>();
            allNotesForUpsert.add(note);

            if (copyToSiblings){
                for (School_PFS_Assignment__c spa : siblingSPAs){
                    // only create comments for siblings in this school
                    if (spa.School__c == schoolPFSAssignment.School__c){
                        Note__c siblingNote = note.clone(false, true);
                        siblingNote.ParentId__c = spa.Id;
                        allNotesForUpsert.add(siblingNote);
                    }
                }
            }

            upsert allNotesForUpsert;   
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Comment successfully saved.'));
        } catch (DMLException ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'An error occured attempting to save the comment: ' + ex.getDMLMessage(0)));
            return null;
        }
        return returnURL();
    }
    
    public PageReference cancelNote()
    {
        return returnURL();
    }
    /*End Action Methods*/
}