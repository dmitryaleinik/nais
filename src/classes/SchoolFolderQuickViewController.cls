/*
* SPEC-091
*
* The EFC should be visible throughout the folder view and aid allocation processes, because the school users
* will be changing fields and needing to understand the ramifications on the EFC
* The system should provide a summary area for each folder with the most relevant information that appears on each screen
*
* Nathan, Exponent Partners, 2013
*/
public with sharing class SchoolFolderQuickViewController {

    /*Initialization*/

    Set <String> currentSchoolIdSet = new Set <String>();
    Set <String> previousSchoolIdSet = new Set <String>();
    Set <string> studentFolderIdSet = new Set <String>();
    Set <String> siblingIdSet = new Set <String>();    // to add each sibling only once in list

    // constructor
    public SchoolFolderQuickViewController() {
        showQuickView = 'FALSE';
        selectedTab = 'pfs1';
        parentCanAffordToPay = 0;
        priorYearAwareded = 0;
        selectedAcademicYear = new Academic_Year__c();
        previousAcademicYear = new Academic_Year__c();
        previousStudentFolder = new Student_Folder__c();
        householdMemberList = new List <HouseholdMember>();

        fullView = !GlobalVariables.isLimitedSchoolView();
    }

    /*End Initialization*/

    /*Properties*/
    // input parameters
    public String studentFolderId {
        get{
            // If a folder id is not provided yet try getting one from parameters
            if(studentFolderId == null){
                studentFolderId = ApexPages.currentPage().getParameters().get('Id');
            }
            return studentFolderId;
        }
        set;
    }

    // output
    public String showQuickView {get; set;}
    public Student_Folder__c studentFolder {
        get {
            if (studentFolder == null) {
                loadFolderQuickView();
            }
            return studentFolder;
        }
        private set;
    }
    public Academic_Year__c selectedAcademicYear {get; private set;}
    public Academic_Year__c previousAcademicYear {get; private set;}
    public Student_Folder__c previousStudentFolder {get; private set;}

    public Boolean fullView {get; set;}
    public Boolean historyReport {get; set;}

    // wrapper class for Household Member
    public class HouseholdMember {
        public Contact member {get; set;}
        public Applicant__c applicant {get; set;}
        public School_PFS_Assignment__c spfsa {get; set;}
        public String name {get; private set;}
        public String relationship {get; private set;}
        public String spfsaSiblingId {get; private set;}

        public HouseholdMember() {
            member = new Contact();
            applicant = new Applicant__c();
            spfsa = new School_PFS_Assignment__c();
        }
    }
    public List <HouseholdMember> householdMemberList {get; private set;}

    public SchoolPFSTotal pfsTotalCurrent1 {
        get {
            if (pfsTotalCurrent1 == null) {
                loadFolderQuickView();
            }
            return pfsTotalCurrent1;
        }
        private set;
    }
    public SchoolPFSTotal pfsTotalCurrent2 {get {if (pfsTotalCurrent2 == null) {loadFolderQuickView();} return pfsTotalCurrent2;} private set;}
    public SchoolPFSTotal pfsTotalPrevious1 {get {if (pfsTotalPrevious1 == null) {loadFolderQuickView();} return pfsTotalPrevious1;} private set;}
    public SchoolPFSTotal pfsTotalPrevious2 {get {if (pfsTotalPrevious2 == null) {loadFolderQuickView();} return pfsTotalPrevious2;} private set;}
    public Boolean pfsCurrent1Submitted {get; set;}
    public Boolean pfsCurrent2Submitted {get; set;}
    public Boolean pfsPrevious1Submitted {get; set;}
    public Boolean pfsPrevious2Submitted {get; set;}

    public Decimal parentCanAffordToPay {get; private set;}
    public Decimal priorYearAwareded {get; private set;}
    public String pfs1SubmittedDate {get; private set;}
    public String pfs2SubmittedDate {get; private set;}
    public String selectedTab {get; set;}
    public String folderId {get; set;}    // sibling folder id

    /*End Properties*/

    /*Action Methods*/

   /*
    public void onTabEnter() {

        // if no pfs2 then it is only one tab. do not change tabs
        if ((pfsTotalCurrent2 != null && pfsTotalCurrent2.existData == true) ||
            (pfsTotalPrevious2 != null && pfsTotalPrevious2.existData == true)) {
            if (selectedTab == 'pfs1') {
                selectedTab = 'pfs2';
            } else {
                selectedTab = 'pfs1';
            }
        }
    }
    */

    public PageReference onClickSibling() {

        PageReference pageRef = page.SchoolFolderSummary;
        if (folderId != null) {
            pageRef.getParameters().put('id', folderId);
        }
        pageRef.setRedirect(true);
        return pageRef;
    }

    /*NAIS-1597 Redirect to Financial Report PDF */
    public PageReference getGoToFinancialReportPFS1(){
        return goToFinancialReportPFS1();
    }
    public PageReference goToFinancialReportPFS1(){
        PageReference pageRef = page.SchoolFinancialHistoryReportPDF;
        if (studentFolderId != null) {
            pageRef.getParameters().put('studentFolderId', studentFolderId);
        }
        pageRef.getParameters().put('studentId', pfsTotalCurrent1.studentId);
        if (pfsTotalCurrent1.schoolPFSAssignment != null) {
            pageRef.getParameters().put('schoolId', pfsTotalCurrent1.schoolPFSAssignment.School__c);
        }
        //SFP-274, [G.S]
        if (pfsTotalCurrent1.applicant != null) {
            pageRef.getParameters().put('pfsParentId', pfsTotalCurrent1.pfs.parent_A__c);
        }
        pageRef.setRedirect(true);
        return pageRef;
    }

    public PageReference getGoToFinancialReportPFS2(){
        return goToFinancialReportPFS2();
    }
    public PageReference goToFinancialReportPFS2(){
        PageReference pageRef = page.SchoolFinancialHistoryReportPDF;
        if (studentFolderId != null) {
            pageRef.getParameters().put('studentFolderId', studentFolderId);
        }
        pageRef.getParameters().put('studentId', pfsTotalCurrent2.studentId);
        if (pfsTotalCurrent2.schoolPFSAssignment != null) {
            pageRef.getParameters().put('schoolId', pfsTotalCurrent2.schoolPFSAssignment.School__c);
        }
        //SFP-274, [G.S]
        if (pfsTotalCurrent2.applicant != null) {
            pageRef.getParameters().put('pfsParentId', pfsTotalCurrent2.pfs.parent_A__c);
        }
        pageRef.setRedirect(true);
        return pageRef;
    }
    /*End NAIS-1597*/
    /*End Action Methods*/

    private void loadFolderQuickView() {
        loadStudentFolder();
        loadSchoolPFSAssignment();
    }

    /*Helper Methods*/

    private void loadStudentFolder() {
        // init
        studentFolder = new Student_Folder__c();
        previousStudentFolder = new Student_Folder__c();
        studentFolderIdSet = new Set <String>();
        currentSchoolIdSet = new Set <String>();
        selectedAcademicYear = new Academic_Year__c();
        previousAcademicYear = new Academic_Year__c();

        if (studentFolderId != null) {
            List <Student_Folder__c> sfList = [select Id, Name,
                Academic_Year_Picklist__c,
                Grant_Awarded__c,
                Student__c,
                Student__r.Name,
                Student__r.BirthDate,
                Student__r.Gender__c,
                (select Id, Name, School__c, Applicant__r.Gender__c from School_PFS_Assignments__r)
            from Student_Folder__c where Id = :studentFolderId];
            if (sfList.size() > 0) {
                studentFolder = sfList[0];
                selectedAcademicYear = GlobalVariables.getAcademicYearByName(studentFolder.Academic_Year_Picklist__c);
                studentFolderIdSet.add(studentFolder.Id);

                // get current schools
                for (School_PFS_Assignment__c spfsa : studentFolder.School_PFS_Assignments__r) {
                    system.debug(spfsa);
                    if (spfsa.School__c != null) {
                        currentSchoolIdSet.add(spfsa.School__c);
                    }
                }

                // get previous academic year
                List <Academic_Year__c> ayList = GlobalVariables.getAllAcademicYears();
                for (Integer i=0; i<ayList.size(); i++) {
                    if (selectedAcademicYear != null && ayList[i].Id == selectedAcademicYear.Id && i+1 < ayList.size()) {
                        previousAcademicYear = ayList[i+1];
                        break;
                    }
                }

                // If there's no record for the previous year we still want to display the label for it
                if(previousAcademicYear == null || previousAcademicYear.Name == null){
                    Integer firstYear = Integer.valueOf(selectedAcademicYear.Name.left(4))-1;
                    Integer secondYear = Integer.valueOf(selectedAcademicYear.Name.right(4))-1;

                    String yearString = String.valueOf(firstYear) + '-' + String.valueOf(secondYear);

                    previousAcademicYear = new Academic_Year__c(Name=yearString);
                }

                // get previous academic year student folders for same student
                if (previousAcademicYear != null && previousAcademicYear.Id != null) {

                    List <Student_Folder__c> prevSFfList = [select Id, Name,
                        Academic_Year_Picklist__c,
                        Grant_Awarded__c,
                        Student__c,
                        Student__r.Name,
                        Student__r.BirthDate,
                        Student__r.Gender__c,
                        (select Id, Name, School__c from School_PFS_Assignments__r)
                    from Student_Folder__c
                    where Student__c = :studentFolder.Student__c and
                        Academic_Year_Picklist__c = :previousAcademicYear.Name];

                    for (Student_Folder__c sf : prevSFfList) {

                        // get previous schools
                        for (School_PFS_Assignment__c spfsa : sf.School_PFS_Assignments__r) {
                            if (spfsa.School__c != null) {

                                // we should only ever be returning School PFS Assignments from one school,
                                // the school of the current User
                                if (currentSchoolIdSet.contains(spfsa.School__c)) {
                                    previousStudentFolder = sf;
                                    studentFolderIdSet.add(sf.Id);
                                }
                                previousSchoolIdSet.add(spfsa.School__c);
                            }
                        }
                    }
                }
            }
        }
    }

    private void loadSchoolPFSAssignment() {

        pfsTotalCurrent1 = new SchoolPFSTotal();
        pfsTotalCurrent2 = new SchoolPFSTotal();
        pfsTotalPrevious1 = new SchoolPFSTotal();
        pfsTotalPrevious2 = new SchoolPFSTotal();

        List <School_PFS_Assignment__c> spfsaCurrentList = new List <School_PFS_Assignment__c>();
        List <School_PFS_Assignment__c> spfsaPreviousList = new List <School_PFS_Assignment__c>();

        List <Student_Folder__c> sfList = [select Id, Name,
            Student__c,
            Student__r.Name,
            Student__r.BirthDate,
            Student__r.Gender__c,
            (select Id, Name,
                Academic_Year_Picklist__c,
                Applicant__c,
                Applicant__r.Contact__c, //NAIS-1597
                Applicant__r.Parent_A_Relationship_to_Applicant__c,
                Applicant__r.Parent_B_Relationship_to_Applicant__c,
                Applicant__r.PFS__c,
                Applicant__r.PFS__r.Name,
                Applicant__r.PFS__r.Original_Submission_Date__c,
                Applicant__r.PFS__r.Parent_A__r.Name,
                Applicant__r.PFS__r.Parent_A_Name__c,
                Applicant__r.PFS__r.Parent_B__r.Name,
                Applicant__r.PFS__r.Parent_B_Name__c,
                Applicant__r.PFS__r.PFS_Number__c,
                Applicant__r.PFS__r.PFS_Status__c,
                Applicant__r.PFS__r.Payment_Status__c,
                Applicant__r.PFS__r.Visible_to_Schools_Until__c,
                Day_Boarding__c,
                //Day_Boarding_Form__c, // NAIS-2252 [DP] 02.05.2015
                Discretionary_Income_Currency__c,
                Effective_Income_Currency__c,
                Est_Family_Contribution__c,
                Est_Family_Contribution_Boarding__c,
                Est_Parental_Contribution_All_Students__c,
                Orig_Est_Parental_Contribution_All__c,
                Orig_Discretionary_Income__c,
                Orig_Effective_Income_Currency__c,
                Orig_Est_Family_Contribution__c,
                Orig_Est_Family_Contribution_Boarding__c,
                Orig_Income_Supplement__c,
                Orig_Parent_Can_Afford_to_Pay__c,
                Orig_Total_Allowances__c,
                Orig_Total_Income__c,
                //Parent_Can_Afford_to_Pay__c,
                Income_Supplement__c,
                Total_Allowances__c,
                Total_Income_Currency__c,
                School__c

                from School_PFS_Assignments__r
                order by Applicant__r.PFS__r.CreatedDate asc, Applicant__r.PFS__r.PFS_Number__c asc)

            from Student_Folder__c
            where Id in :studentFolderIdSet];

        for (Student_Folder__c sf : sfList) {
            for (School_PFS_Assignment__c spfsa : sf.School_PFS_Assignments__r) {

                // get all pfs for current academic year
                if (selectedAcademicYear != null && spfsa.Academic_Year_Picklist__c == selectedAcademicYear.Name) {
                    spfsaCurrentList.add(spfsa);
                }
                // get all pfs for previous academic year
                if (previousAcademicYear != null && spfsa.Academic_Year_Picklist__c == previousAcademicYear.Name) {
                    spfsaPreviousList.add(spfsa);
                }
            }
        }

        // consolidate pfs fields

        // To determine the previous year's PFS 1 and 2, I think we should try to match up on PFS.Parent A.
        // So if the current Year's PFS 1 has "Margaret Thatcher" as Parent A, we want to set last year's
        // PFS with "Margaret Thatcher" as Parent A to also be PFS 1. And same with PFS 2.
        // If there isn't a match on either one it should follow the PFS.CreatedDate desc rule.

        Boolean previous1Matched = false;
        Boolean previous2Matched = false;
        Set <String> pfsIdIncluded = new Set <String>();    // include each pfs only once

        // NAIS-328
        // If a PFS does not have a prior year, still show the section and all of the fields,
        // but have the value "N/A" displayed instead of the currency

        if (selectedAcademicYear != null && selectedAcademicYear.Name != null) {
            pfsTotalCurrent1.title = selectedAcademicYear.Name;
            pfsTotalCurrent2.title = selectedAcademicYear.Name;
        }
        if (previousAcademicYear != null && previousAcademicYear.Name != null) {
            pfsTotalPrevious1.title = previousAcademicYear.Name;
            pfsTotalPrevious2.title = previousAcademicYear.Name;
        }

        if (spfsaCurrentList.size() > 0) {
            School_PFS_Assignment__c spfsa = spfsaCurrentList[0];
            pfsCurrent1Submitted = GlobalVariables.pfsIsSubmitted(spfsa);

            pfsIdIncluded.add(spfsa.Applicant__r.PFS__c);
            pfsTotalCurrent1.existData = true;
            if (selectedAcademicYear != null && selectedAcademicYear.Name != null) {
                pfsTotalCurrent1.title = selectedAcademicYear.Name;
            }
            setPFSTotal(pfsTotalCurrent1, spfsa);

            if (spfsa.Applicant__r.PFS__r.Original_Submission_Date__c != null) {
                pfs1SubmittedDate = spfsa.Applicant__r.PFS__r.Original_Submission_Date__c.date().format();
            }

            for (School_PFS_Assignment__c spfsa2 : spfsaPreviousList) {
                if (spfsa.Applicant__r.PFS__r.Parent_A__c != null &&
                    spfsa.Applicant__r.PFS__r.Parent_A__c == spfsa2.Applicant__r.PFS__r.Parent_A__c &&
                    pfsIdIncluded.contains(spfsa2.Applicant__r.PFS__c) == false)
                {
                    pfsIdIncluded.add(spfsa2.Applicant__r.PFS__c);
                    previous1Matched = true;
                    pfsTotalPrevious1.existData = true;
                    if (previousAcademicYear != null && previousAcademicYear.Name != null) {
                        pfsTotalPrevious1.title = previousAcademicYear.Name;
                    }
                    setPFSTotal(pfsTotalPrevious1, spfsa2);
                    break;
                }
            }
        }

        if (spfsaCurrentList.size() > 1) {
            School_PFS_Assignment__c spfsa = spfsaCurrentList[1];
            pfsCurrent2Submitted = GlobalVariables.pfsIsSubmitted(spfsa);

            if (pfsIdIncluded.contains(spfsa.Applicant__r.PFS__c) == false) {
                pfsIdIncluded.add(spfsa.Applicant__r.PFS__c);
                pfsTotalCurrent2.existData = true;
                if (selectedAcademicYear != null && selectedAcademicYear.Name != null) {
                    pfsTotalCurrent2.title = selectedAcademicYear.Name;
                }
                setPFSTotal(pfsTotalCurrent2, spfsa);

                if (spfsa.Applicant__r.PFS__r.Original_Submission_Date__c != null) {
                    pfs2SubmittedDate = spfsa.Applicant__r.PFS__r.Original_Submission_Date__c.date().format();
                }
            }

            for (School_PFS_Assignment__c spfsa2 : spfsaPreviousList) {
                if (spfsa.Applicant__r.PFS__r.Parent_A__c != null &&
                    spfsa.Applicant__r.PFS__r.Parent_A__c == spfsa2.Applicant__r.PFS__r.Parent_A__c &&
                    pfsIdIncluded.contains(spfsa2.Applicant__r.PFS__c) == false)
                {
                    pfsIdIncluded.add(spfsa2.Applicant__r.PFS__c);
                    previous2Matched = true;
                    pfsTotalPrevious2.existData = true;
                    if (previousAcademicYear != null && previousAcademicYear.Name != null) {
                        pfsTotalPrevious2.title = previousAcademicYear.Name;
                    }
                    setPFSTotal(pfsTotalPrevious2, spfsa2);
                    break;
                }
            }
        }


        if (previous1Matched == false && spfsaPreviousList.size() > 0) {
            School_PFS_Assignment__c spfsa = spfsaPreviousList[0];
            pfsPrevious1Submitted = GlobalVariables.pfsIsSubmitted(spfsa);

            if (pfsIdIncluded.contains(spfsa.Applicant__r.PFS__c) == false) {
                pfsIdIncluded.add(spfsa.Applicant__r.PFS__c);
                pfsTotalPrevious1.existData = true;
                if (previousAcademicYear != null && previousAcademicYear.Name != null) {
                    pfsTotalPrevious1.title = previousAcademicYear.Name;
                }
                setPFSTotal(pfsTotalPrevious1, spfsa);
            }
        }
        if (previous2Matched == false && spfsaPreviousList.size() > 1) {
            School_PFS_Assignment__c spfsa = spfsaPreviousList[1];
            pfsPrevious2Submitted = GlobalVariables.pfsIsSubmitted(spfsa);

            if (pfsIdIncluded.contains(spfsa.Applicant__r.PFS__c) == false) {
                pfsIdIncluded.add(spfsa.Applicant__r.PFS__c);
                pfsTotalPrevious2.existData = true;
                if (previousAcademicYear != null && previousAcademicYear.Name != null) {
                    pfsTotalPrevious2.title = previousAcademicYear.Name;
                }
                setPFSTotal(pfsTotalPrevious2, spfsa);
            }
        }

        for (School_PFS_Assignment__c spfsa : spfsaCurrentList) {
            //if (spfsa.Parent_Can_Afford_to_Pay__c != null) {
            //    parentCanAffordToPay += spfsa.Parent_Can_Afford_to_Pay__c;
            //} else
            if (spfsa.Orig_Parent_Can_Afford_to_Pay__c != null) {
                parentCanAffordToPay += spfsa.Orig_Parent_Can_Afford_to_Pay__c;
            }
        }
        /* THIS IS NOT USED ANYWHERE -- [DP] 5.16.13
        for (School_PFS_Assignment__c spfsa : spfsaPreviousList) {
            if (spfsa.Parent_Can_Afford_to_Pay__c != null) {
                priorYearAwareded += spfsa.Parent_Can_Afford_to_Pay__c;
            } else if (spfsa.Orig_Parent_Can_Afford_to_Pay__c != null) {
                priorYearAwareded += spfsa.Orig_Parent_Can_Afford_to_Pay__c;
            }
        }
        */
        loadSibling(studentFolder, currentSchoolIdSet);
        loadSibling(previousStudentFolder, currentSchoolIdSet);
    }

    private void setPFSTotal(SchoolPFSTotal pfsSum, School_PFS_Assignment__c spfsa) {

        pfsSum.pfs = spfsa.Applicant__r.PFS__r;
        pfsSum.schoolPFSAssignment = spfsa;
        pfsSum.studentId = spfsa.Applicant__r.Contact__c; //NAIS-1597
        pfsSum.applicant = spfsa.Applicant__r;
        pfsSum.status = spfsa.Applicant__r.PFS__r.PFS_Status__c;

        // Parent A and B Names
        pfsSum.parentAName = spfsa.Applicant__r.PFS__r.Parent_A_Name__c;
        pfsSum.parentBName = spfsa.Applicant__r.PFS__r.Parent_B_Name__c;

        // Parent A and B Relations
        pfsSum.parentARelation = spfsa.Applicant__r.Parent_A_Relationship_to_Applicant__c;
        pfsSum.parentBRelation = spfsa.Applicant__r.Parent_B_Relationship_to_Applicant__c;

        //NAIS-1896
        pfsSum.parentalContributionAllStudentsTitle = 'Parent Contrib. All Students';
        pfsSum.parentalContributionAllStudents = (spfsa.Est_Parental_Contribution_All_Students__c!=null?
                                                    spfsa.Est_Parental_Contribution_All_Students__c:0);
        pfsSum.origParentalContributionAllStudents = (spfsa.Orig_Est_Parental_Contribution_All__c!=null?
                                                    spfsa.Orig_Est_Parental_Contribution_All__c:0);


        // The label is dynamic, and displays either "EFC Day" or "EFC Boarding" depending on the Day_Boarding__c field
        //NAIS-2038 Start
        //if(spfsa.Day_Boarding_Form__c != null){
        if(spfsa.Day_Boarding__c == 'Day'){ // NAIS-2252 [DP] 02.05.2015
            pfsSum.efcTitle = 'EFC This Student Day';
        }else if(spfsa.Day_Boarding__c == 'Boarding'){ // NAIS-2252 [DP] 02.05.2015
            pfsSum.efcTitle = 'EFC This Student Boarding';
        }
        //}else {
        //    if (spfsa.Day_Boarding__c == 'Day') {
        //        pfsSum.efcTitle = 'EFC Day';
        //    } else if (spfsa.Day_Boarding__c == 'Boarding') {
        //        pfsSum.efcTitle = 'EFC Boarding';
        //    }
        //}
        //NAIS-2038 End

        // Discretionary Income
        if (spfsa.Orig_Discretionary_Income__c != null) {
            pfsSum.origDiscretionaryIncome += spfsa.Orig_Discretionary_Income__c;
        }
        if (spfsa.Discretionary_Income_Currency__c != null) {
            pfsSum.discretionaryIncome += spfsa.Discretionary_Income_Currency__c;
        } else if (spfsa.Orig_Discretionary_Income__c != null) {
            pfsSum.discretionaryIncome += spfsa.Orig_Discretionary_Income__c;
        }

        // Effective Income
        if (spfsa.Orig_Effective_Income_Currency__c != null) {
            pfsSum.origEffectiveIncome += spfsa.Orig_Effective_Income_Currency__c;
        }
        if (spfsa.Effective_Income_Currency__c != null) {
            pfsSum.effectiveIncome += spfsa.Effective_Income_Currency__c;
        } else if (spfsa.Orig_Effective_Income_Currency__c != null) {
            pfsSum.effectiveIncome += spfsa.Orig_Effective_Income_Currency__c;
        }

        // EFC
        // [DP] NAIS-1591,2038 -- updating to dynamically pull EFC field based on day/boarding
        //if(spfsa.Day_Boarding_Form__c != null){
        if (spfsa.Day_Boarding__c == 'Day') { // NAIS-2252 [DP] 02.05.2015
            if (spfsa.Orig_Est_Family_Contribution__c != null) {
                pfsSum.origEstFamilyContribution += spfsa.Orig_Est_Family_Contribution__c;
            }
            if (spfsa.Est_Family_Contribution__c != null) {
                pfsSum.estFamilyContribution += spfsa.Est_Family_Contribution__c;
            } else if (spfsa.Orig_Est_Family_Contribution__c != null) {
                pfsSum.estFamilyContribution += spfsa.Orig_Est_Family_Contribution__c;
            }
        } else if (spfsa.Day_Boarding__c == 'Boarding') { // NAIS-2252 [DP] 02.05.2015
            if (spfsa.Orig_Est_Family_Contribution_Boarding__c != null) {
                pfsSum.origEstFamilyContribution += spfsa.Orig_Est_Family_Contribution_Boarding__c;
            }
            if (spfsa.Est_Family_Contribution_Boarding__c != null) {
                pfsSum.estFamilyContribution += spfsa.Est_Family_Contribution_Boarding__c;
            } else if (spfsa.Orig_Est_Family_Contribution_Boarding__c != null) {
                pfsSum.estFamilyContribution += spfsa.Orig_Est_Family_Contribution_Boarding__c;
            }
        }
        //}else{
        //    if (spfsa.Day_Boarding__c == 'Day') {
        //        if (spfsa.Orig_Est_Family_Contribution__c != null) {
        //            pfsSum.origEstFamilyContribution += spfsa.Orig_Est_Family_Contribution__c;
        //        }
        //        if (spfsa.Est_Family_Contribution__c != null) {
        //            pfsSum.estFamilyContribution += spfsa.Est_Family_Contribution__c;
        //        } else if (spfsa.Orig_Est_Family_Contribution__c != null) {
        //            pfsSum.estFamilyContribution += spfsa.Orig_Est_Family_Contribution__c;
        //        }
        //    } else if (spfsa.Day_Boarding__c == 'Boarding') {
        //        if (spfsa.Orig_Est_Family_Contribution_Boarding__c != null) {
        //            pfsSum.origEstFamilyContribution += spfsa.Orig_Est_Family_Contribution_Boarding__c;
        //        }
        //        if (spfsa.Est_Family_Contribution_Boarding__c != null) {
        //            pfsSum.estFamilyContribution += spfsa.Est_Family_Contribution_Boarding__c;
        //        } else if (spfsa.Orig_Est_Family_Contribution_Boarding__c != null) {
        //            pfsSum.estFamilyContribution += spfsa.Orig_Est_Family_Contribution_Boarding__c;
        //        }
        //    }
        //}

        // Income Supplement
        if (spfsa.Orig_Income_Supplement__c != null) {
            pfsSum.origIncomeSupplement += spfsa.Orig_Income_Supplement__c;
        }
        if (spfsa.Income_Supplement__c != null) {
            pfsSum.incomeSupplement += spfsa.Income_Supplement__c;
        } else if (spfsa.Orig_Income_Supplement__c != null) {
            pfsSum.incomeSupplement += spfsa.Orig_Income_Supplement__c;
        }

        // Total Allowances
        if (spfsa.Orig_Total_Allowances__c != null) {
            pfsSum.origTotalAllowances += spfsa.Orig_Total_Allowances__c;
        }
        if (spfsa.Total_Allowances__c != null) {
            pfsSum.totalAllowances += spfsa.Total_Allowances__c;
        } else if (spfsa.Orig_Total_Allowances__c != null) {
            pfsSum.totalAllowances += spfsa.Orig_Total_Allowances__c;
        }

        // Total Income
        if (spfsa.Orig_Total_Income__c != null) {
            pfsSum.origTotalIncome += spfsa.Orig_Total_Income__c;
        }
        if (spfsa.Total_Income_Currency__c != null) {
            pfsSum.totalIncome += spfsa.Total_Income_Currency__c;
        } else if (spfsa.Orig_Total_Income__c != null) {
            pfsSum.totalIncome += spfsa.Orig_Total_Income__c;
        }

        // Parent can afford to pay
        //if (spfsa.Parent_Can_Afford_to_Pay__c != null) {
        //    pfsSum.parentCanAffordToPay += spfsa.Parent_Can_Afford_to_Pay__c;
        //} else
        if (spfsa.Orig_Parent_Can_Afford_to_Pay__c != null && pfsSum.parentCanAffordToPay != null) {
            pfsSum.parentCanAffordToPay = spfsa.Orig_Parent_Can_Afford_to_Pay__c;
        }
    }

    private void loadSibling(Student_Folder__c sf, Set <String> schoolIdSet) {

        SiblingWithoutSharing swos = new SiblingWithoutSharing(sf);
        List <School_PFS_Assignment__c> spfsaList = swos.getSiblings();
        for (School_PFS_Assignment__c spfsa : spfsaList) {
            HouseholdMember hm = new HouseholdMember();
            hm.member = spfsa.Applicant__r.Contact__r;
            hm.applicant = spfsa.Applicant__r;
            hm.spfsa = spfsa;
            hm.name = getContactFullName(spfsa.Applicant__r.Contact__r);
            hm.spfsaSiblingId = spfsa.Id;
            if (schoolIdSet.contains(spfsa.School__c)) {
                hm.relationship = 'Sibling Applicant';
            } else {
                hm.relationship = 'Sibling';
            }

            // add each sibling only once in list
            if (siblingIdSet.add(spfsa.Applicant__r.Contact__c)) {
                householdMemberList.add(hm);
            }
        }
    }

    private String getApplicantFullName(Applicant__c app) {
        String name = '';
        if (app != null) {
            if (app.First_Name__c != null) {
                name += app.First_Name__c;
            }
            if (app.Middle_Initial__c != null) {
                name += ' ' + app.Middle_Initial__c;
            }
            if (app.Last_Name__c != null) {
                name += ' ' + app.Last_Name__c;
            }
        }
        return name;
    }

    private String getContactFullName(Contact con) {
        String name = '';
        if (con != null) {
            if (con.FirstName != null) {
                name += con.FirstName;
            }
            if (con.Middle_Name__c != null) {
                name += ' ' + con.Middle_Name__c;
            }
            if (con.LastName != null) {
                name += ' ' + con.LastName;
            }
        }
        return name;
    }

    // inner class to bypass record visibility for siblings
    // if siblings are applying to other schools then they are not visible to current school since school (Account)
    // is defined as private object. We need to show these siblings in addition to siblings applying to current school

    public without sharing class SiblingWithoutSharing {
        Student_Folder__c studentFolder;
        public Map <String, Boolean> spfsaId_isApplicant_map = new Map <String, Boolean>();
        public SiblingWithoutSharing(Student_Folder__c sf) {
            studentFolder = sf;
        }
        public List <School_PFS_Assignment__c> getSiblings() {
            List <School_PFS_Assignment__c> spfsaList = new List <School_PFS_Assignment__c>();

            // looks like the logic for sibling is same school, PFS but different student folder
            if (studentFolder != null && studentFolder.Id != null) {
                Set <String> pfsIdSet = new Set <String>();
                for (School_PFS_Assignment__c spfsa : [select Id, Name, Applicant__r.PFS__c, Student_Folder__c
                    from School_PFS_Assignment__c
                    where Student_Folder__c = :studentFolder.Id]) {
                    if (spfsa.Applicant__r.PFS__c != null) {
                        pfsIdSet.add(spfsa.Applicant__r.PFS__c);
                    }
                }
                if (pfsIdSet.size() > 0) {
                    spfsaList = [select Id, Name, Student_Folder__c, School__c,
                        Applicant__r.Contact__c,
                        Applicant__r.Contact__r.FirstName,
                        Applicant__r.Contact__r.LastName,
                        Applicant__r.Contact__r.Middle_Name__c,
                        Applicant__r.Contact__r.Name,
                        Applicant__r.First_Name__c,
                        Applicant__r.Last_Name__c,
                        Applicant__r.Middle_Initial__c,
                        Applicant__r.PFS__c

                        from School_PFS_Assignment__c
                        where Applicant__r.PFS__c in :pfsIdSet and
                            Student_Folder__c != :studentFolder.Id and
                            Student_Folder__r.Student__c != :studentFolder.Student__c
                            ];

                }
            }
            return spfsaList;
        }
    }
}