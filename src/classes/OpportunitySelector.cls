/**
 * @description Query selector for Opportunity records.
 */
public class OpportunitySelector extends fflib_SObjectSelector {
    @testVisible private static final String OPPORTUNITY_IDS_PARAM = 'opportunityIds';
    @testVisible private static final String ACCOUNT_IDS_PARAM = 'accountIds';
    @testVisible private static final String PFS_IDS_PARAM = 'pfsIds';

    // Relations
    private static final String PFS_RELATION = 'PFS__r';
    private static final String TRANSACTION_LINE_ITEM_RELATION = 'Transaction_Line_Items__r';
    private static final String CHARGENT_ORDERS_RELATION = 'Chargent_Orders__r';

    /**
     * @description Select Opportunity records by their Ids.
     * @param opportunityIds The set of Ids to select Opportunity records by.
     * @return A list of Opportunity records associated with the set of Ids
     *          provided.
     * @throws An ArgumentNullException if opportunityIds is null.
     */
    public List<Opportunity> selectByIdWithPfs(Set<Id> opportunityIds) {
        ArgumentNullException.throwIfNull(opportunityIds, OPPORTUNITY_IDS_PARAM);

        if (isEnforcingCRUD()) {
            assertIsAccessible();
        }
        PfsSelector.Instance.assertIsAccessible();

        return Database.query(String.format('SELECT RecordType.Name, {0}, {1} FROM Opportunity WHERE Id IN :opportunityIds ORDER BY {2} ASC',
                new List<String> { getFieldListString(), PfsSelector.Instance.getRelatedFieldListString(PFS_RELATION), getOrderBy() }));
    }

    /**
     * @description Select Opportunity records with Transaction Line Items by
     *              their Ids.
     * @param opportunityIds The set of Ids to select Opportunity records by.
     * @return A list of Opportunity records associated with the set of Ids
     *          provided.
     * @throws An ArgumentNullException if opportunityIds is null.
     */
    public List<Opportunity> selectByIdWithTransactionLineItems(Set<Id> opportunityIds) {
        ArgumentNullException.throwIfNull(opportunityIds, OPPORTUNITY_IDS_PARAM);

        if (isEnforcingCRUD()) {
            assertIsAccessible();
        }
        TransactionLineItemSelector.Instance.assertIsAccessible();

        return Database.query(String.format(
                'SELECT RecordType.Name, {0}, (SELECT {1} FROM {2}) FROM {3} WHERE Id IN :opportunityIds ORDER BY {4} ASC',
                new List<String> {
                        getFieldListString(),
                        TransactionLineItemSelector.Instance.getFieldListString(),
                        TRANSACTION_LINE_ITEM_RELATION,
                        getSObjectName(),
                        getOrderBy()
                }));
    }

    /**
    * @description Select Opportunity records by the Account__c field.
    * @param accountIds The Account Ids to select Opportunity records by.
    * @return A list of Opportunity records associated with the set of
    *         Account Ids provided.
    * @throws An ArgumentNullException if accountIds is null.
    */
    public List<Opportunity> selectByAccountId(Set<Id> accountIds) {
        ArgumentNullException.throwIfNull(accountIds, ACCOUNT_IDS_PARAM);

        if (isEnforcingCRUD()) {
            assertIsAccessible();
        }

        return Database.query(String.format('SELECT {0} FROM {1} WHERE AccountId IN :accountIds ORDER BY {2}',
                new List<String> {
                        getFieldListString(),
                        getSObjectName(),
                        getOrderBy()
                }));
    }

    /**
    * @description Selects allOpportunity records.
    * @return A list of all Opportunity records
    */
    public List<Opportunity> selectAll()
    {
        if (isEnforcingCRUD())
        {
            assertIsAccessible();
        }

        return Database.query(String.format('SELECT {0} FROM {1}', new List<String>{
            getFieldListString(), getSObjectName()}));
    }

    /**
     * @description Select Opportunity records by their associated PFS Ids.
     * @param pfsIds The set of PFS__c Ids to select Opportunity records by.
     * @returns A list of Opportunity records associated with the set of PFS__c
     *          Ids provided.
     * @throws An ArgumentNullException if pfsIds is null.
     */
    public List<Opportunity> selectByPfsId(Set<Id> pfsIds) {
        ArgumentNullException.throwIfNull(pfsIds, PFS_IDS_PARAM);

        if (isEnforcingCRUD()) {
            assertIsAccessible();
        }
        PfsSelector.Instance.assertIsAccessible();

        return Database.query(String.format('SELECT RecordType.Name, {0}, {1} FROM Opportunity WHERE PFS__r.Id IN :pfsIds ORDER BY {2} ASC',
                new List<String> { getFieldListString(), PfsSelector.Instance.getRelatedFieldListString(PFS_RELATION), getOrderBy() }));
    }

    private Schema.SObjectType getSObjectType() {
        return Opportunity.getSObjectType();
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                Opportunity.Academic_Year_Picklist__c,
                Opportunity.AccountId,
                Opportunity.Amount,
                Opportunity.Chargent_Order__c,
                Opportunity.CloseDate,
                Opportunity.Id,
                Opportunity.Name,
                Opportunity.Net_Amount_Due__c,
                Opportunity.Paid_Status__c,
                Opportunity.Parent_A__c,
                Opportunity.PFS__c,
                Opportunity.PFS_Number__c,
                Opportunity.Product_Quantity__c,
                Opportunity.RecordTypeId,
                Opportunity.SSS_School_Code__c,
                Opportunity.StageName,
                Opportunity.Subscription_Type__c,
                Opportunity.Total_Purchased_Waivers__c,
                Opportunity.Total_Write_Offs__c
        };
    }

    /**
     * @description Singleton property ensuring external sources do not
     *              instantiate this directly.
     */
    public static OpportunitySelector Instance {
        get {
            if (Instance == null) {
                Instance = new OpportunitySelector();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to enforce singleton access
     *              via the Instance property.
     */
    private OpportunitySelector() {
        super(false, false, false);
    }
}