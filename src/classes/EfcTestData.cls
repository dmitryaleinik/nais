/**
 * @description This class is used to create Efc records for unit tests.
 */
@isTest
public class EfcTestData {
    /* NOTE: I think those methods should be rewritten/removed based on TestData classes. (Please, remove this comment, if it's fine) */

    /**
     * @description Create test Pfs record.
     * @param academicYearId The academic Year Id to set on the Pfs.
     * @param doInsert Determine if the record should be inserted.
     * @return The PFS__c record.
     */
    public static PFS__c createTestPfs(Id academicYearId, boolean doInsert) {
        PFS__c pfs = new PFS__c();
        pfs.Academic_Year_Picklist__c = GlobalVariables.getAcademicYear(academicYearId).Name;
        pfs.Have_Other_Taxable_Income__c = 'Yes';
        pfs.Have_Other_Nontaxable_Income__c = 'Yes';
        pfs.Own_Business_or_Farm__c = 'Yes';
        pfs.Salary_Wages_Parent_A__c = 100000;
        pfs.Salary_Wages_Parent_B__c = 100001;
        pfs.Dividend_Income_Current__c = 1001;
        pfs.Interest_Income_Current__c = 1002;
        pfs.Alimony_Current__c = 1003;
        pfs.Net_Profit_Loss_Business_Farm_Current__c = 1004;
        pfs.Taxable_Refunds_Current__c = 1005;
        pfs.Capital_Gain_Loss_Current__c = 1006;
        pfs.Other_Gain_Loss_Current__c = 1007;
        pfs.IRA_Distribution_Current__c = 1008;
        pfs.Pensions_and_Annuities_Current__c = 1009;
        pfs.Rental_Real_Estate_Trusts_etc_Current__c = 1010;
        pfs.Unemployment_Compensation_Current__c = 1011;
        pfs.Social_Security_Benefits_Taxable_Current__c = 1012;
        pfs.Other_Income_Current__c = 1013;
        pfs.Keogh_Plan_Payment_Current__c = 1014;
        pfs.Untaxed_IRA_Plan_Payment_Current__c = 1015;
        pfs.Tax_Defer_Pension_Saving_Current__c = 1017;
        pfs.IRS_Adjustments_to_Income_Current__c = 1016;
        pfs.Child_Support_Received_Current__c = 1018;
        pfs.Social_Security_Benefits_Current__c = 1019;
        pfs.Fringe_Benefit_Plan_Untaxed_Current__c = 1020;
        pfs.Cash_Support_Gift_Income_Current__c = 1021;
        pfs.Tax_Exempt_Investments_Current__c = 1022;
        pfs.Income_Abroad_Current__c = 1023;
        pfs.Other_Untaxed_Income_Current__c = 1024;
        pfs.Business_Farm_Owner__c = EfcPicklistValues.BUSINESS_FARM_OWNER_PARENT_A;
//        pfs.Depreciation_Sec_179_Exp_Sch_C__c = 1026;    // NAIS-857 - remove depreciation calc from SSS
//        pfs.Depreciation_Schedule_E__c = 1027;             // NAIS-857 - remove depreciation calc from SSS
//        pfs.Depreciation_Sec_179_Exp_Sch_F__c = 1028;    // NAIS-857 - remove depreciation calc from SSS
//        pfs.Depreciation_Sec_179_Exp_4562__c = 1029;    // NAIS-857 - remove depreciation calc from SSS
//        pfs.Home_Bus_Exp_Sch_C__c = 1030;                // NAIS-857 - remove depreciation calc from SSS
        pfs.Filing_Status__c = EfcPicklistValues.FILING_STATUS_MARRIED_JOINT;
        pfs.File_Schedule_A__c = 'Yes';
        pfs.Income_Tax_Exemptions__c = 2;
        pfs.Itemized_Deductions__c = 1035;
        pfs.Federal_Income_Tax_Calculated__c = 1036;
        pfs.Social_Security_Tax_Allowance_Calc__c = 1037;
        pfs.Medicare_Tax_Allowance_Calculated__c = 1038;
        pfs.Self_Employed_Tax_Paid__c = 1039;
        pfs.Parent_A_State__c = 'Alabama';
        pfs.State_Other_Tax_Allowance_Calculated__c = 1042;
        pfs.Employment_Allowance__c = 1043;
        pfs.Medical_Dental_Exp_Current__c = 1044;
        pfs.Medical_Allowance__c = 1045;
        pfs.Unusual_Expenses_Current__c = 1046;
        pfs.Own_Home__c = 'Yes';
        pfs.Equity_Loan_2nd_Mortgage__c = 'Yes';
        pfs.Home_Market_Value__c = 1049;
        pfs.Unpaid_Principal_1st_Mortgage__c = 1050;
        pfs.Unpaid_Principal_2nd_Mortgage__c = 1051;
        pfs.Home_Purchase_Year__c = '1990';
        pfs.Home_Purchase_Price__c = 1053;
        pfs.Own_Other_Real_Estate__c = 'Yes';
        pfs.Num_of_Add_l_Properties__c = '1';
        pfs.Other_RE_Unpaid_Principal_Amount_1__c = 1055;
        pfs.Other_Real_Estate_Unpaid_Principal__c = 1055;
        pfs.Other_RE_Current_Market_Value_1__c = 1056;
        pfs.Other_Real_Estate_Market_Value_Total__c = 1056;
        //pfs.Business_Entity_Type__c = 'Partnership';
        //pfs.Business_Farm_Ownership_Percent__c = 50;
        pfs.Business_Farm_Share__c = 1061;
        pfs.Bank_Account_Value__c = 1062;
        pfs.Investments_Net_Value__c = 1063;
        pfs.Total_Debts__c = 1064;
        pfs.Net_Worth__c = 1066;
        pfs.Parent_A_Birthdate__c = Date.newInstance(1970, 1, 1);
        pfs.Parent_B_Birthdate__c = Date.newInstance(1971, 2, 2);
        pfs.Discretionary_Net_Worth__c = 1070;
        pfs.Income_Supplement__c = 1071;
        pfs.Revised_Adjusted_Effective_Income__c = 1073;
        pfs.Family_Size__c = 5;
        pfs.Income_Protection_Allowance__c = 1075;
        pfs.Est_Parental_Contribution__c = 1077;
        pfs.Num_Children_in_Tuition_Schools__c = 3;
        pfs.Est_Parental_Contribution_per_Child__c = 1079;

        if (doInsert == true) {
            insert pfs;
        }

        return pfs;
    }

    /**
     * @description Create test School Pfs Assignment record.
     * @param academicYearName The Academic Year Name to set on the School Pfs Assignment.
     * @param studentFolderId The Student Folder Id to set on the School Pfs Assignment.
     * @param schoolId The School Id to set on the School Pfs Assignment.
     * @param applicantId The Applicant Id to set on the School Pfs Assignment.
     * @param doInsert Determine if the record should be inserted.
     * @return The School_PFS_Assignment__c record.
     */
    public static School_PFS_Assignment__c createTestSchoolPfsAssignment(String academicYearName, Id studentFolderId, Id schoolId, Id applicantId, boolean doInsert) {
        School_PFS_Assignment__c pfsAssign = new School_PFS_Assignment__c();
        pfsAssign.Academic_Year_Picklist__c = academicYearName;
        pfsAssign.Student_Folder__c = studentFolderId;
        pfsAssign.School__c = schoolId;
        pfsAssign.Applicant__c = applicantId;

        pfsAssign.Salary_Wages_Parent_A__c = 100000;
        pfsAssign.Salary_Wages_Parent_B__c = 100001;
        pfsAssign.Dividend_Income__c = 1001;
        pfsAssign.Interest_Income__c = 1002;
        pfsAssign.Alimony_Received__c = 1003;
        //pfsAssign.Net_Profit_Loss_Business_Farm__c = 1004;
        pfsAssign.Taxable_Refunds__c = 1005;
        pfsAssign.Capital_Gain_Loss__c = 1006;
        pfsAssign.Other_Gain_Loss__c = 1007;
        pfsAssign.IRA_Distribution__c = 1008;
        pfsAssign.Pensions_and_Annuities__c = 1009;
        pfsAssign.Rental_Real_Estate_Trusts_etc__c = 1010;
        pfsAssign.Unemployment_Compensation__c = 1011;
        pfsAssign.Social_Security_Benefits_Taxable__c = 1012;
        pfsAssign.Other_Income__c = 1013;
        pfsAssign.Keogh_Plan_Payment__c = 1014;
        pfsAssign.Untaxed_IRA_Plan_Payment__c = 1015;
        pfsAssign.Child_Support_Received__c = 1018;
        pfsAssign.Social_Security_Benefits__c = 1019;
        pfsAssign.Pre_Tax_Fringe_Benefit_Contribution__c = 1020;
        pfsAssign.Cash_Support_Gift_Income__c = 1021;
        pfsAssign.Tax_Exempt_Investment_Income__c = 1022;
        pfsAssign.Income_Earned_Abroad__c = 1023;
        pfsAssign.Other_Untaxed_Income__c = 1024;
        pfsAssign.Business_Farm_Owner__c = EfcPicklistValues.BUSINESS_FARM_OWNER_PARENT_A;
        pfsAssign.Depreciation_Sec_179_Exp_Sch_C__c = 1026;
        pfsAssign.Depreciation_Schedule_E__c = 1027;
        pfsAssign.Depreciation_Sec_179_Exp_Sch_F__c = 1028;
        pfsAssign.Depreciation_Sec_179_Exp_4562__c = 1029;
        pfsAssign.Home_Bus_Exp_Sch_C__c = 1030;
        pfsAssign.Filing_Status__c = EfcPicklistValues.FILING_STATUS_MARRIED_JOINT;
        pfsAssign.Income_Tax_Exemptions__c = 2;
        pfsAssign.Itemized_Deductions__c = 1035;
        pfsAssign.Federal_Income_Tax_Calculated__c = 1036;
        pfsAssign.Social_Security_Tax_Allowance_Calc__c = 1037;
        pfsAssign.Medicare_Tax_Allowance_Calc__c = 1038;
        pfsAssign.Total_SE_Tax_Paid__c = 1039;
        pfsAssign.Parent_State__c = 'Alabama';
        pfsAssign.State_Other_Tax_Allowance_Calculated__c = 1042;
        pfsAssign.Employment_Allowance__c = 1043;
        pfsAssign.Medical_Dental_Expense__c = 1044;
        pfsAssign.Medical_Allowance__c = 1045;
        pfsAssign.Unusual_Expense__c = 1046;
        pfsAssign.Home_Market_Value__c = 1049;
        pfsAssign.Unpaid_Principal_1st_Mortgage__c = 1050;
        pfsAssign.Unpaid_Principal_2nd_Mortgage__c = 1051;
        pfsAssign.Home_Purchase_Year__c = '1990';
        pfsAssign.Home_Purchase_Price__c = 1053;
        pfsAssign.Other_Real_Estate_Unpaid_Principal__c = 1055;
        pfsAssign.Other_Real_Estate_Market_Value__c = 1056;
        //pfsAssign.Business_Farm_Ownership_Percent__c = 50;
        pfsAssign.Business_Farm_Share__c = 1061;
        pfsAssign.Bank_Accounts__c = 1062;
        pfsAssign.Investments__c = 1063;
        pfsAssign.Total_Debts__c = 1064;
        pfsAssign.Net_Worth__c = 1066;
        pfsAssign.Discretionary_Net_Worth__c = 1070;
        pfsAssign.Income_Supplement__c = 1071;
        pfsAssign.Revised_Adjusted_Effective_Income__c = 1073;
        pfsAssign.Family_Size__c = 5;
        pfsAssign.Income_Protection_Allowance__c = 1075;
        pfsAssign.Est_Parental_Contribution_All_Students__c = 1077;
        pfsAssign.Num_Children_in_Tuition_Schools__c = 3;
        pfsAssign.Est_Parental_Contribution_per_Child__c = 1079;

        if (doInsert == true) {
            insert pfsAssign;
        }

        return pfsAssign;
    }

    /**
     * @description Create test School Pfs Assignment record.
     * @param doInsert Determine if the record should be inserted.
     * @param academicYear The Academic Year to set on the School Pfs Assignment.
     * @return The School_PFS_Assignment__c record.
     */
    public static School_PFS_Assignment__c createTestSchoolPfsAssignment(boolean doInsert, Academic_Year__c academicYear) {
        Account school = TestUtils.createAccount('School', RecordTypes.schoolAccountTypeId, 5, true);
        Account family = TestUtils.createAccount('The individual', RecordTypes.individualAccountTypeId, 5, true);
        Contact student = TestUtils.createContact('Student', family.Id, RecordTypes.studentContactTypeId, true);
        Contact parent = TestUtils.createContact('Parent', family.Id, RecordTypes.parentContactTypeId, true);
        Student_Folder__c folder = TestUtils.createStudentFolder('Folder', academicYear.Id, student.Id, true);
        PFS__c pfs = TestUtils.createPFS('pfs', academicYear.Id, parent.Id, true);
        Applicant__c applicant = TestUtils.createApplicant(student.Id, pfs.Id, true);
        return (createTestSchoolPfsAssignment(academicYear.Name, folder.Id, school.Id, applicant.Id, doInsert));
    }

    /**
     * @description Create test School Pfs Assignment record.
     * @param doInsert Determine if the record should be inserted.
     * @return The School_PFS_Assignment__c record.
     */
    public static School_PFS_Assignment__c createTestSchoolPfsAssignment(boolean doInsert) {
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        return createTestSchoolPfsAssignment(doInsert, academicYear);
    }
}