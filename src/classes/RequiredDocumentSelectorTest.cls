@isTest
private class RequiredDocumentSelectorTest {
    
    @isTest
    private static void mapBySchoolAcademicYear_schoolHasRequiredDocuments_expectRequiredDocuments() {
        
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
       
        Account school = AccountTestData.Instance.forRecordTypeId(RecordTypes.schoolAccountTypeId).DefaultAccount;
        
        AnnualSettingsTestData.Instance
            .forSchoolId(school.Id)
            .forAcademicYearId(currentAcademicYear.Id)
            .insertAnnualSettings();
        
        RequiredDocumentTestData.Instance
                .forSchoolId(school.Id)
                .forDocType('1040 with all filed schedules and attachments')
                .forAcademicYearId(currentAcademicYear.Id)
                .forYear(currentAcademicYear.Name.left(4))
                .insertRequiredDocument();
        
        Test.startTest();
            
            Map<String, List<Required_Document__c>> result = RequiredDocumentSelector.newInstance()
                .mapBySchoolAcademicYear(new Set<Id>{school.Id},  new Set<String>{currentAcademicYear.Name});
            
            System.assertEquals(1, result.size());
            
            String mapKey = RequiredDocumentSelector.newInstance().createMapKey(school.Id, currentAcademicYear.Name, currentAcademicYear.Name.left(4));
        
            System.assertEquals(school.Id + '-' + currentAcademicYear.Name + '-' + currentAcademicYear.Name.left(4), mapKey);
        Test.stopTest();
    }
    
}