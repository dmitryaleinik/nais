/**
 * SendgridEvent.cls
 *
 * @description Wrapper class representing JSON content from Sendgrid Webhook API
 * Supported Events: Processed, Dropped, Delivered, Deferred, Bounce, Open, Click, Spam Report
 * Sendgrid Webhook Event API docs: https://sendgrid.com/docs/API_Reference/Webhooks/event.html
 *
 * @author Chase Logan @ Presence PG
 */
global class SendgridEvent {

    // properties representing JSON fields
    public String response { 
        get; 
        set {
            this.response = value;
        }
    }
    public String sg_event_id { 
        get; 
        set {
            this.sg_event_id = value;
        }
    }
    public String sg_message_id { 
        get; 
        set {
            this.sg_message_id = value;
        }
    }
    public String event { 
        get; 
        set {
            this.event = value;
        }
    }
    public String email { 
        get; 
        set {
            this.email = value;
        }
    }
    public Integer timestamp { 
        get; 
        set {
            this.timestamp = value;
        }
    }
    public String useragent {
        get;
        set {
            this.useragent = value;
        }
    }
    public String send_at { 
        get; 
        set {
            this.send_at = value;
        }
    }
    public String attempt { 
        get; 
        set {
            this.attempt = value;
        }
    }
    public String school_id {
        get;
        set {
            this.school_id = value;
        }
    }
    public String sender_id {
        get;
        set {
            this.sender_id = value;
        }
    }
    public String recipient_id {
        get;
        set {
            this.recipient_id = value;
        }
    }
    public String spa_id {
        get; 
        set {
            this.spa_id = value;
        }
    }
    public String mass_email_send_id {
        get; 
        set {
            this.mass_email_send_id = value;
        }
    }
    public String academic_year {
        get;
        set {
            this.academic_year = value;
        }
    }
    public Integer asm_group_id { 
        get;
        set {
            this.asm_group_id = value;
        }
    }
    public String ip { 
        get;
        set {
            this.ip = value;
        }
    }
    public String tls {
        get;
        set {
            this.tls = value;
        }
    }
    public String cert_err { 
        get;
        set {
            this.cert_err = value;
        }
    }
    public String reason { 
        get;
        set {
            this.reason = value;
        }
    }
    public String url { 
        get; 
        set {
            this.url = value;
        }
    }
    public Map<String,String> url_offset { 
        get; 
        set {
            this.url_offset = value;
        }
    }

}