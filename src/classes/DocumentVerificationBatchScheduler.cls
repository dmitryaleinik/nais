global class DocumentVerificationBatchScheduler implements Schedulable {
    
    global void execute(SchedulableContext sc) {
        Database.executebatch(new DocumentVerificationBatch());
    }
}