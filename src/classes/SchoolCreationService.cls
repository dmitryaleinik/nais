/**
@description This class will be used to auto generate SSS codes for new schools and access organisations.
             SSS Codes are unique across the accounts in the entire org.
*/
public class SchoolCreationService
{

    /**
     * @description Private constructor to force use of Instance property.
     */
    private SchoolCreationService() { }

    public void setSSSCodes(List<Account> accounts)
    {
        if(!FeatureToggles.isEnabled('Auto_Generate_SSS_Codes__c'))
        {
            return;
        }

        try
        {
            List<Account> schoolsAndAccessOrgsToUpdate = new List<Account>(filterByRecordType(accounts));
            if (schoolsAndAccessOrgsToUpdate.isEmpty())
            {
                return;
            }

            Account_Settings__c accountSettings = Account_Settings__c.getValues('New Accounts');
            Integer lastGeneratedSSSCode = Integer.valueOf(accountSettings.Last_SSS_Code__c);

            populateSSSCodes(schoolsAndAccessOrgsToUpdate, lastGeneratedSSSCode);
            accountSettings.Last_SSS_Code__c = Decimal.valueOf(
            schoolsAndAccessOrgsToUpdate[schoolsAndAccessOrgsToUpdate.size() - 1].SSS_School_Code__c);

            update accountSettings;
        }
        catch (Exception e)
        {
            CustomDebugService.Instance.logException(e);
        }
    }

    private List<Account> filterByRecordType(List<Account> allAccounts)
    {
        List<Account> schoolsAndAccessOrgs = new List<Account>();

        for (Account account : allAccounts)
        {
            if (account.RecordTypeId == RecordTypes.schoolAccountTypeId
                    || account.RecordTypeId == RecordTypes.accessOrgAccountTypeId)
            {
                schoolsAndAccessOrgs.add(account);
            }
        }

        return schoolsAndAccessOrgs;
    }

    private Set<Integer> getExistingHigherSSSCodes(Integer lastGeneratedSSSCode)
    {
        List<Account> schoolsAndAccessOrgsWithExistingHigherSSSCode = AccountSelector.Instance
                .selectSchoolsAndAccessOrgsWithHigherSSSCodes(lastGeneratedSSSCode);

        if (schoolsAndAccessOrgsWithExistingHigherSSSCode.isEmpty())
        {
            return new Set<Integer>{};
        }

        Set<Integer> existingHigherSSSCodes = new Set<Integer>{};
        for (Account school : schoolsAndAccessOrgsWithExistingHigherSSSCode)
        {
            existingHigherSSSCodes.add(Integer.valueOf(school.SSS_School_Code_Number__c));
        }

        return existingHigherSSSCodes;
    }

    private void populateSSSCodes(List<Account> schoolsAndAccessOrgs, Integer currentSSSCode)
    {
        Set<Integer> existingSSSCodes = getExistingHigherSSSCodes(currentSSSCode);

        for (Account acc : schoolsAndAccessOrgs)
        {
            currentSSSCode++;

            if (!existingSSSCodes.isEmpty())
            {
                while (existingSSSCodes.contains(currentSSSCode))
                {
                    currentSSSCode++;
                }
            }

            acc.SSS_School_Code__c = String.valueOf(currentSSSCode);
        }
    }

    public static SchoolCreationService Instance
    {
        get
        {
            if (Instance == null)
            {
                Instance = new SchoolCreationService();
            }
            return Instance;
        }
        private set;
    }
}