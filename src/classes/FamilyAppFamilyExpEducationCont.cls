public class FamilyAppFamilyExpEducationCont extends FamilyAppCompController {
    
    public String currentApplicantId {get; set;}
    public String haveEducationExpense {get; set;}
    public String otherSourcesCurrent {get; set;}
    public String otherSourcesEst {get; set;}
    public String dependentId { get; set; }
    
    public String currentYearColumnName {
        get{
            if(currentPFSYear != null){
                String retVal = currentPFSYear.Name.subString(0,5);
                retVal += currentPFSYear.Name.subString(7,9);
                return retVal;
            }
            else{
                return '';
            }
        }
        set;
    }
    
    public String previousYearColumnName {
        get{
            if(previousPFSYear != null){
                String retVal = previousPFSYear.Name.subString(0,5);
                retVal += previousPFSYear.Name.subString(7,9); 
                return retVal;
            }
            else{
                return '';
            }
        }
        set;
    }
    
    private Map<String, Applicant__c> priorContactToApplicantMap{
        get{
            if(priorContactToApplicantMap == null){
                // Iterate through prior year pfs Applicants and populate the map
                priorContactToApplicantMap = new Map<String, Applicant__c>{};
                for(Applicant__c prior : priorYearPFS.Applicants__r) {
                    priorContactToApplicantMap.put(prior.Contact__c, prior);
                }                
            }
            
            return     priorContactToApplicantMap;
        } 
        set;
    }
    
    public Map<String, Applicant__c> PriorYearApplicants{
        get{
            if(PriorYearApplicants == null){
                PriorYearApplicants = new Map<String, Applicant__c>();
                for(Applicant__c applicant : pfs.Applicants__r) {
                    Applicant__c priorApplicant;
                    // Look up the prior year applicant and populate the map to return
                    if(priorYearPFS != null) {
                        priorApplicant = priorContactToApplicantMap.get(applicant.Contact__c);
                    }
                    if(priorApplicant == null){
                         priorApplicant = new Applicant__c();
                    }
                    // if(priorApplicant == null) priorApplicant = new Applicant__c();
                    PriorYearApplicants.put(applicant.Id, priorApplicant);
                }
            }
            
            return PriorYearApplicants;
        }
        set;
    }
    
    public PageReference UpdateDelegates() {
        if(this.currentApplicantId != null && this.haveEducationExpense != null) {
            Map<Id, Applicant__c> applicantMap = new Map<Id, Applicant__c>(this.pfs.Applicants__r);
            
            if(applicantMap.containsKey(this.currentApplicantId)) {
                Applicant__c applicant = applicantMap.get(this.currentApplicantId);
                
                applicant.Have_Education_Expenses__c = this.haveEducationExpense;
            }
            
        }
        
        return null;
    }    
    
    public PageReference updateOtherSources() {
        if(this.currentApplicantId != null) {
            Map<Id, Applicant__c> applicantMap = new Map<Id, Applicant__c>(this.pfs.Applicants__r);
            
            if(applicantMap.containsKey(this.currentApplicantId)) {
                Applicant__c applicant = applicantMap.get(this.currentApplicantId);
                applicant.Other_Source_Sources_Current__c = stringtoInteger(applicant.Other_Source_Sources_Current__c, otherSourcesCurrent);
                applicant.Other_Source_Sources_Est__c = stringtoInteger(applicant.Other_Source_Sources_Est__c, otherSourcesEst);
            }            
        }
        
        return null;
    }
    
    private Integer stringtoInteger( Decimal currentValue, String newValue ) {  
        Integer retVal;
        
        if (currentValue != null) {
            retVal = currentValue.intValue();
        }
              
        if (newValue != null) {
            try {
                retVal = Integer.valueOf(newValue);
            } catch (Exception ex) {
                // do nothing
            }
        }
        
        return retVal;
    }
    
    public Dependents__c dependentsRecord {
        get{
            if(dependentsRecord == null){
                if (pfs.Dependents__r != null && pfs.Dependents__r.size() > 0){
                    dependentsRecord = pfs.Dependents__r[0];
                }
                else{
                    dependentsRecord = new Dependents__c();
                }
            }
            
            return dependentsRecord;
        }
        set;
    }
    
    public PageReference updateDependentOtherSources() {  
        if (dependentId != null && pfs != null && pfs.Dependents__r != null && pfs.Dependents__r.size() > 0) {
            if (dependentId == '1') {
                pfs.Dependents__r[0].Dependent_1_Other_Source_Sources_Current__c = stringtoInteger(pfs.Dependents__r[0].Dependent_1_Other_Source_Sources_Current__c, otherSourcesCurrent);
                pfs.Dependents__r[0].Dependent_1_Other_Source_Sources_Est__c = stringtoInteger(pfs.Dependents__r[0].Dependent_1_Other_Source_Sources_Est__c, otherSourcesEst);
            } else if (dependentId == '2') {
                pfs.Dependents__r[0].Dependent_2_Other_Source_Sources_Current__c = stringtoInteger(pfs.Dependents__r[0].Dependent_2_Other_Source_Sources_Current__c, otherSourcesCurrent);
                pfs.Dependents__r[0].Dependent_2_Other_Source_Sources_Est__c = stringtoInteger(pfs.Dependents__r[0].Dependent_2_Other_Source_Sources_Est__c, otherSourcesEst);
            } else if (dependentId == '3') {
                pfs.Dependents__r[0].Dependent_3_Other_Source_Sources_Current__c = stringtoInteger(pfs.Dependents__r[0].Dependent_3_Other_Source_Sources_Current__c, otherSourcesCurrent);
                pfs.Dependents__r[0].Dependent_3_Other_Source_Sources_Est__c = stringtoInteger(pfs.Dependents__r[0].Dependent_3_Other_Source_Sources_Est__c, otherSourcesEst);
            } else if (dependentId == '4') {
                pfs.Dependents__r[0].Dependent_4_Other_Source_Sources_Current__c = stringtoInteger(pfs.Dependents__r[0].Dependent_4_Other_Source_Sources_Current__c, otherSourcesCurrent);
                pfs.Dependents__r[0].Dependent_4_Other_Source_Sources_Est__c = stringtoInteger(pfs.Dependents__r[0].Dependent_4_Other_Source_Sources_Est__c, otherSourcesEst);
            } else if (dependentId == '5') {
                pfs.Dependents__r[0].Dependent_5_Other_Source_Sources_Current__c = stringtoInteger(pfs.Dependents__r[0].Dependent_5_Other_Source_Sources_Current__c, otherSourcesCurrent);
                pfs.Dependents__r[0].Dependent_5_Other_Source_Sources_Est__c = stringtoInteger(pfs.Dependents__r[0].Dependent_5_Other_Source_Sources_Est__c, otherSourcesEst);
            } else if (dependentId == '6') {
                pfs.Dependents__r[0].Dependent_6_Other_Source_Sources_Current__c = stringtoInteger(pfs.Dependents__r[0].Dependent_6_Other_Source_Sources_Current__c, otherSourcesCurrent);
                pfs.Dependents__r[0].Dependent_6_Other_Source_Sources_Est__c = stringtoInteger(pfs.Dependents__r[0].Dependent_6_Other_Source_Sources_Est__c, otherSourcesEst);
            } else {
                System.debug('*** Unknown dependent value passed...' + dependentId);
            }
        }
        
        return null;
    }             
    
    public Integer applicantNumber {get; set;} 
    
    public PageReference clearDependenTuitionProgramNextYearFields1() {
        
        return clearDependenTuitionProgramNextYearFields(1);
    }
    
    public PageReference clearDependenTuitionProgramNextYearFields2() {
        
        return clearDependenTuitionProgramNextYearFields(2);
    }
    
    public PageReference clearDependenTuitionProgramNextYearFields3() {
        
        return clearDependenTuitionProgramNextYearFields(3);
    }
    
    public PageReference clearDependenTuitionProgramNextYearFields4() {
        
        return clearDependenTuitionProgramNextYearFields(4);
    }
    
    public PageReference clearDependenTuitionProgramNextYearFields5() {
        
        return clearDependenTuitionProgramNextYearFields(5);
    }
    
    public PageReference clearDependenTuitionProgramNextYearFields6() {
        
        return clearDependenTuitionProgramNextYearFields(6);
    }
    
    /**
    * @description Clear out dependent fields dependet to the user's answer to question "Dependent_NNN_Tuition_Program_Next_Year__c".
    *              If answer is "No", the fields are clear out. Otherwise, the fields remains with the same value that already have.
    *              NOTE: Each dependent 1-6 has his own public method, to avoid rerender all panels in the VF Page. This mean that with
    *              separated methods for each depentent, we rerender only the related panels for that dependent in the VFPage.
    * @param dependent Since there could be 1 to 6 dependent, we should specify wich dependent was modified.
    */
    private PageReference clearDependenTuitionProgramNextYearFields(Integer depentent) {
        
        Set<String> fieldsToClear = new Set<String>();
        
        fieldsToClear.add('Dependent_NNN_Tuition_Cost_Est__c');
        fieldsToClear.add('Dependent_NNN_Parent_Sources_Est__c');
        fieldsToClear.add('Dependent_NNN_Student_Sources_Est__c');
        fieldsToClear.add('Dependent_NNN_Other_Source_Sources_Est__c');
        fieldsToClear.add('Dependent_NNN_Fin_Aid_Sources_Est__c');
        fieldsToClear.add('Dependent_NNN_School_Next_Year__c');
        fieldsToClear.add('Dependent_NNN_Grade_Next_Year__c');
        fieldsToClear.add('Dependent_NNN_School_Next_Year_Type__c');
        
        String answer = String.ValueOf(pfs.Dependents__r[0].get('Dependent_' + depentent +'_Tuition_Program_Next_Year__c'));
        
        if (answer != 'Yes') {
            return clearDependentFields(fieldsToClear, String.ValueOf(depentent));
        }
        
        return null;
    }
    
    private PageReference clearDependentFields(Set<String> fieldsToClear, String dependentNumber) {
       
        if (dependentNumber != null && dependentNumber.isNumeric()) {       
            
            for (String f : fieldsToClear) {
                
                String fieldName = f.replace('NNN', dependentNumber);
                String fieldType = String.ValueOf(Schema.SObjectType.Dependents__c.fields.getMap().get(fieldName).getDescribe().getType());
                
                if (fieldType == 'CURRENCY') {
                    pfs.Dependents__r[0].put(fieldName, 0);
                } else {
                    pfs.Dependents__r[0].put(fieldName, null);
                }
            }
        }
        return null;
    }
    
    /**
    * @description Clear out dependent fields dependet to the user's answer to question "Dependent_NNN_Have_Education_Expenses__c".
    *              If answer is "No", the fields are clear out. Otherwise, the fields remains with the same value that already have.
    *              NOTE: Each dependent 1-6 has his own public method, to avoid rerender all panels in the VF Page. This mean that with
    *              separated methods for each dependent, we rerender only the related panels for that dependent in the VFPage.
    * @param dependent Since there could be 1 to 6 dependent, we should specify wich dependent was modified.
    */
    private PageReference clearDependenHaveEducationExpensesFields(Integer dependent) {
        
        Set<String> fieldsToClear = new Set<String>();
        
        fieldsToClear.add('Dependent_NNN_Tuition_Cost_Current__c');
        fieldsToClear.add('Dependent_NNN_Parent_Sources_Current__c');
        fieldsToClear.add('Dependent_NNN_Student_Sources_Current__c');
        fieldsToClear.add('Dependent_NNN_Other_Source_Sources_Current__c');
        fieldsToClear.add('Dependent_NNN_Fin_Aid_Sources_Current__c');
        
        String answer = String.ValueOf(pfs.Dependents__r[0].get('Dependent_' + dependent +'_Have_Education_Expenses__c'));
        
        
        if (answer != 'Yes') {
            return clearDependentFields(fieldsToClear, String.ValueOf(dependent));
        }
        
        return null;
    }
    
    public PageReference clearDependenHaveEducationExpensesFields1() {
        
        return clearDependenHaveEducationExpensesFields(1);
    }
    
    public PageReference clearDependenHaveEducationExpensesFields2() {
        
        return clearDependenHaveEducationExpensesFields(2);
    }
    
    public PageReference clearDependenHaveEducationExpensesFields3() {
        
        return clearDependenHaveEducationExpensesFields(3);
    }
    
    public PageReference clearDependenHaveEducationExpensesFields4() {
        
        return clearDependenHaveEducationExpensesFields(4);
    }
    
    public PageReference clearDependenHaveEducationExpensesFields5() {
        
        return clearDependenHaveEducationExpensesFields(5);
    }
    
    public PageReference clearDependenHaveEducationExpensesFields6() {
        
        return clearDependenHaveEducationExpensesFields(6);
    }
}