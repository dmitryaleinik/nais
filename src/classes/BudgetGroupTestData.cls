/**
 * @description This class is used to create Budget Group records for unit tests.
 */
@isTest
public class BudgetGroupTestData extends SObjectTestData {
    @testVisible private static final String NAME = 'BudgetGroup Name';

    /**
     * @description Get the default values for the Budget_Group__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Budget_Group__c.Name => NAME,
                Budget_Group__c.Academic_Year__c => AcademicYearTestData.Instance.DefaultAcademicYear.Id,
                Budget_Group__c.School__c => AccountTestData.Instance.DefaultAccount.Id
        };
    }

    /**
     * @description Set the Academic_Year__c field on the current Budget Group record.
     * @param academicYearId The Academic Year Id to set on the Budget Group.
     * @return The current working instance of BudgetGroupTestData.
     */
    public BudgetGroupTestData forAcademicYearId(Id academicYearId) {
        return (BudgetGroupTestData) with(Budget_Group__c.Academic_Year__c, academicYearId);
    }

    /**
     * @description Set the School__c field on the current Budget Group record.
     * @param accountId The School Id to set on the Budget Group.
     * @return The current working instance of BudgetGroupTestData.
     */
    public BudgetGroupTestData forSchoolId(Id accountId) {
        return (BudgetGroupTestData) with(Budget_Group__c.School__c, accountId);
    }

    /**
     * @description Set the Total_Funds__c field on the current Budget Group record.
     * @param totalFunds The total funds amount to set on the Budget Group.
     * @return The current working instance of BudgetGroupTestData.
     */
    public BudgetGroupTestData forTotalFunds(Decimal totalFunds) {
        return (BudgetGroupTestData) with(Budget_Group__c.Total_Funds__c, totalFunds);
    }

    /**
     * @description Insert the current working Budget_Group__c record.
     * @return The currently operated upon Budget_Group__c record.
     */
    public Budget_Group__c insertBudgetGroup() {
        return (Budget_Group__c)insertRecord();
    }

    /**
     * @description Create the current working Budget Group record without resetting
     *             the stored values in this instance of BudgetGroupTestData.
     * @return A non-inserted Budget_Group__c record using the currently stored field
     *             values.
     */
    public Budget_Group__c createBudgetGroupWithoutReset() {
        return (Budget_Group__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Budget_Group__c record.
     * @return The currently operated upon Budget_Group__c record.
     */
    public Budget_Group__c create() {
        return (Budget_Group__c)super.buildWithReset();
    }

    /**
     * @description The default Budget_Group__c record.
     */
    public Budget_Group__c DefaultBudgetGroup {
        get {
            if (DefaultBudgetGroup == null) {
                DefaultBudgetGroup = createBudgetGroupWithoutReset();
                insert DefaultBudgetGroup;
            }
            return DefaultBudgetGroup;
        }
        private set;
    }

    /**
     * @description Get the Budget_Group__c SObjectType.
     * @return The Budget_Group__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Budget_Group__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static BudgetGroupTestData Instance {
        get {
            if (Instance == null) {
                Instance = new BudgetGroupTestData();
            }
            return Instance;
        }
        private set;
    }

    private BudgetGroupTestData() { }
}