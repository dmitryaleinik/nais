public class PFSRowData{
        public string currentYearPFSId {get;set;}
        public string previousYearPFSId {get;set;}
        public string fieldName {get;set;}
        public decimal previousYearValue {get;set;}
        public decimal currentYearValue {get;set;}
        public string positiveCommentary {get;set;}
        public string negativeCommentary {get;set;}
        public boolean flagRevisitLater {get;set;}
        public boolean flagNotAnIssue {get;set;}
        //public boolean hasComments {get;set;}
        public string comments {get;set;}
        //public decimal thresholdValue {get;set;}
        public decimal comparisonResult {get;set;}
        public boolean isOverThreshold {get;set;}
        public string lineNumber {get;set;}
        
        public PFSRowData(string currentYearPFSId, string previousYearPFSId, string fieldName, decimal previousYearValue, decimal currentYearValue, string positiveCommentary, string negativeCommentary, decimal thresholdValue, string comments, string status, string lineNumber){
            // need to query for these, since they are by field, might need to be a custom object
            // speficy type (comment, flaglater, flagnot an issue), and fieldName
            // records are queried for a given row fieldName on this object related to currentyearpfs 
            this.comments = comments;
            this.lineNumber = lineNumber;
            //flagRevisitLater = false;
            //flagNotAnIssue = false;
            system.debug('*fieldname:'+fieldNAme + ' status:'+status);
            if(status == 'Not An Issue'){
                flagNotAnIssue = true;
                flagRevisitLater = false;
            }
            else if(status =='Revisit Later'){
                flagRevisitLater = true;
                flagNotAnIssue = false;
            }
            else{
                flagRevisitLater = false;
                flagNotAnIssue = false;
            }

            this.isOverThreshold = false;
            // initialize props passed in from this/previous pfs
            this.currentYearPFSId = currentYearpfsId;
            this.previousYearPFSId = previousYearPFSId;
            this.fieldName = fieldName;
            this.previousYearValue = previousYearValue;
            this.currentYearValue = currentYearValue;
            this.positiveCommentary = positiveCommentary;
            this.negativeCommentary = negativeCommentary;

            // do calculations
            if(previousYearValue == null){
                previousYearValue = 0;
                this.previousYearValue = 0;
            }

            if( currentYearValue != null ){
                if( previousYearValue != 0 && previousYearValue != null ){
                    comparisonResult = (previousYearValue - currentYearValue)/previousYearValue;
                    if( math.ABS(comparisonResult) > thresholdValue )
                        isOverThreshold = true;
                }
                else if (previousYearValue != null) {
                    if(math.abs(currentYearValue - previousYearValue) > 1000 ) {
                        isOverThreshold = true;
                        if (currentYearValue - previousYearValue>0)
                            comparisonResult = -1;
                        else
                                comparisonResult = 1;
                    }
                    // value for previous year is zero, do not flag as over threshold
                    //isOverThreshold = true;
                    //comparisonResult = 1;
                }
                /*else{
                    if(math.abs(currentYearValue) > 1000 ) {
                        isOverThreshold = true;
                        if (currentYearValue > 0)
                            comparisonResult = -1;
                        else
                            comparisonResult = 1;
                    }
                }*/

            }
            
            if(comparisonResult != null )
                comparisonResult = Math.floor(comparisonResult*100);
        }
        public PFSRowData(){
            isOverThreshold = false;
        }

        public PFSRowData(string currentYearPFSId, string fieldName, decimal currentYearValue, string comments, string status){
            isOverThreshold = false;
            //flagRevisitLater = false;
            //flagNotAnIssue = false;
            if(status == 'Not An Issue')
                flagNotAnIssue = true;
            else if(status =='Revisit Later')
                flagRevisitLater = true;
            else{
                flagRevisitLater = false;
                flagNotAnIssue = false;
            }
            this.isOverThreshold = false;
            // initialize props passed in from this/previous pfs
            this.comments = comments;
            this.currentYearPFSId = currentYearpfsId;
            this.fieldName = fieldName;
            this.currentYearValue = currentYearValue;

        }

}