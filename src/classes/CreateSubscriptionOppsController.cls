/**
* @CreateSubscriptionOppsController.cls
* @date 3/1/2014
* @author  CH for Exponent Partners
* @description Controller for page to auto-generate missing Subsctiption Opportunites
*/
public with sharing class CreateSubscriptionOppsController {
    
    /*Initialization*/
    public CreateSubscriptionOppsController(){
        loadInfo();
    }
    
    // Separate load method so it can be called to prepare for updating the page after Save
    private void loadInfo(){
         Date calendarStart = Date.newInstance(Date.today().year(), 1, 1);
        Date calendarEnd = Date.newInstance(Date.today().year(), 12, 31);
        
        // Query for Subscription records that expire this calendar year
        accountsWithExpirations = [select Id, Account__c, Account__r.Name, Account__r.SSS_Subscription_Type_Current__c
                                                from Subscription__c 
                                                where End_Date__c >= :calendarStart 
                                                and End_Date__c <= :calendarEnd 
                                                and Cancelled__c != 'Yes'];
        
        // Query for existing Opportunity records
        existingOpportunities = new Set<Id>{};
        for(Opportunity oppRecord : [select Id, AccountId
                                        from Opportunity 
                                        where CloseDate >= :calendarStart 
                                        and CloseDate <= :calendarEnd 
                                        and RecordTypeId = :subscriptionRecordType])
        {
            // Collect the Set of Account Ids for which there's already an Opportunity
            existingOpportunities.add(oppRecord.AccountId);
        }
        
        // Create the list of Accounts to have Opportunities created
        accountsNeedingOpportunities = new Set<Account>{};
        for(Subscription__c subRecord : accountsWithExpirations){
            // If the given expiring subscription does not already have an Opportunity for this year 
            if(!existingOpportunities.contains(subRecord.Account__c)){
                accountsNeedingOpportunities.add(
                    new Account(
                        Id = subRecord.Account__c, 
                        Name = subRecord.Account__r.Name, 
                        SSS_Subscription_Type_Current__c = subRecord.Account__r.SSS_Subscription_Type_Current__c
                    )
                );
            }
        }        
    }
    /*End Initialization*/
    
    /*Properties*/
    
    public Integer numExpirations {
        get{
            return accountsWithExpirations.size();
        } 
        set;
    }
    
    public Integer numMissing {
        get{
            return accountsNeedingOpportunities.size();
        } 
        set;
    }
    
    // private Set<Id> accountsWithExpirations {get; set;}
    private List<Subscription__c> accountsWithExpirations {get; set;}
    
    private Set<Id> existingOpportunities {get; set;}
    private Set<Account> accountsNeedingOpportunities {get; set;}
    
    private String subscriptionRecordType {
        get{
            if(subscriptionRecordType == null){
                subscriptionRecordType = RecordTypes.opportunitySubscriptionFeeTypeId;            
            }
            return subscriptionRecordType;
        }
        set;
    }
    
    /*End Properties*/
    
    /*Action Methods*/
        
    public PageReference createSubscriptions(){
        List<Opportunity> opportunitiesToInsert = new List<Opportunity>{};
        // For the list of Subscription records with no Opportunity
        for(Account accountRecord : accountsNeedingOpportunities){
            Opportunity newOpportunity = new Opportunity(
                AccountId = accountRecord.Id,
                Name = accountRecord.Name + ' Subscription ' + Date.today().year(),
                Subscription_Type__c = accountRecord.SSS_Subscription_Type_Current__c,
                RecordTypeId = subscriptionRecordType,
                CloseDate = Date.today(),
                StageName = 'Prospecting'
            );
            opportunitiesToInsert.add(newOpportunity);
        }
        // Create Opportunity records
        Database.insert(opportunitiesToInsert);
        
        // Note: Transaction Line Items are create automatically when a subscription type 
        //             Opportunity is inserted
        // PaymentUtils.insertTransactionLineItemsFromOpportunities(opportunitiesToInsert);
        
        // Refresh lists to support page refresh
        loadInfo();
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, opportunitiesToInsert.size() + ' Opportunites and related Transaction Line Items were created successfully.'));

        return null;
    }
    
    public PageReference cancel(){
        return new PageReference('/');
    }
    
    /*End Action Methods*/
    
    /*Helper Methods*/
    /*End Helper Methods*/
}