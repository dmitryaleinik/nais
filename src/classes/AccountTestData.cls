/**
 * @description This class is used to create Account records for unit tests.
 */
@isTest
public class AccountTestData extends SObjectTestData {
    @testVisible private static final String SCHOOL_NAME = 'Great School';
    @testVisible private static final Integer MAX_NUMBER_OF_USERS = 5;

    private static Integer counter = 0;
    private static Integer sssSchoolCode = 0;

    /**
     * @description Get the default values for the Account object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Account.Name => SCHOOL_NAME,
                Account.Max_Number_of_Users__c => MAX_NUMBER_OF_USERS
        };
    }

    /**
     * @description Set the Name field on the current Account record.
     * @param name The Name to set on the Account.
     * @return The current working instance of AccountTestData.
     */
    public AccountTestData forName(String name) {
        return (AccountTestData) with(Account.Name, name);
    }

    /**
     * @description Set the RecordTypeId field on the current Account record.
     * @param recordTypeId The Record Type Id to set on the Account.
     * @return The current working instance of AccountTestData.
     */
    public AccountTestData forRecordTypeId(Id recordTypeId) {
        return (AccountTestData) with(Account.RecordTypeId, recordTypeId);
    }

    public AccountTestData forFinancialAidAPIApp(String app) {
        return (AccountTestData) with(Account.Financial_Aid_Api_Apps__c, app);
    }

    /**
     * @description Set the SSS_Subscriber_Status__c field on the current Account record.
     * @param sssSubscriberStatus The SSS Subscriber Status to set on the Account.
     * @return The current working instance of AccountTestData.
     */
    public AccountTestData forSSSSubscriberStatus(String sssSubscriberStatus) {
        return (AccountTestData) with(Account.SSS_Subscriber_Status__c, sssSubscriberStatus);
    }

    /**
     * @description Set the SSS_Subscription_Type_Current__c field on the current Account record.
     * @param sssSubscriptionTypeCurrent The SSS Subscription Type Current to set on the Account.
     * @return The current working instance of AccountTestData.
     */
    public AccountTestData forSSSSubscriptionTypeCurrent(String sssSubscriptionTypeCurrent) {
        return (AccountTestData) with(Account.SSS_Subscription_Type_Current__c, sssSubscriptionTypeCurrent);
    }

    /**
     * @description Set the Max_Number_of_Users__c field on the current Account record.
     * @param maxNumberOfUsers The Max Number Of Users to set on the Account.
     * @return The current working instance of AccountTestData.
     */
    public AccountTestData forMaxNumberOfUsers(Integer maxNumberOfUsers) {
        return (AccountTestData) with(Account.Max_Number_of_Users__c, maxNumberOfUsers);
    }

    /**
     * @description Set the Portal_Version__c field on the current Account record.
     * @param portalVersion The portal version to set on the Account.
     * @return The current working instance of AccountTestData.
     */
    public AccountTestData forPortalVersion(String portalVersion) {
        return (AccountTestData) with(Account.Portal_Version__c, portalVersion);
    }

    /**
     * @description Set the SSS_School_Code__c field on the current Account record.
     * @param schoolCode The SSS School Code to set on the Account.
     * @return The current working instance of AccountTestData.
     */
    public AccountTestData forSSSSchoolCode(String schoolCode) {
        return (AccountTestData) with(Account.SSS_School_Code__c, schoolCode);
    }

    /**
     * @description Set the System_Exclude__c field on the current Account record.
     * @param systemExclude The System Exclude to set on the Account.
     * @return The current working instance of AccountTestData.
     */
    public AccountTestData forSystemExclude(Boolean systemExclude) {
        return (AccountTestData) with(Account.System_Exclude__c, systemExclude);
    }

    /**
     * @description Set the SSS_Subscriber_Status__c field on the current Account record with 'Current' value.
     * @return The current working instance of AccountTestData.
     */
    public AccountTestData asCurrent() {
        return (AccountTestData) with(Account.SSS_Subscriber_Status__c, 'Current');
    }

    /**
     * @description Set the NCES_ID__c field on the current Account record with the specified value.
     * @param ncesId The value to set.
     * @return The current working instance of AccountTestData.
     */
    public AccountTestData withNcesId(String ncesId) {
        return (AccountTestData) with(Account.NCES_ID__c, ncesId);
    }

    /**
     * @description Sets the record type to School.
     * @return The current instance.
     */
    public AccountTestData asSchool() {
        return forRecordTypeId(RecordTypes.schoolAccountTypeId);
    }

    /**
     * @description Sets the record type to Access Organisation.
     * @return The current instance.
     */
    public AccountTestData asAccessOrganisation()
    {
        return forRecordTypeId(RecordTypes.accessOrgAccountTypeId);
    }
    
    /**
     * @description Sets the record type to Individual.
     * @return The current instance.
     */
    public AccountTestData asFamily() {
        return forRecordTypeId(RecordTypes.individualAccountTypeId);
    }

    /**
     * @description Sets Hide from School Selection field of the account record.
     * @param hideFromSchoolSelection The Hide From School Selection field value to set on the Account.
     * @return The current working instance of AccountTestData.
     */
    public AccountTestData forHideFromSchoolSelection(Boolean hideFromSchoolSelection)
    {
        return (AccountTestData) with(Account.Hide_from_School_Selection__c, hideFromSchoolSelection);
    }

    /**
     * @description Handle setting values that can't be assumed/static
     *              for the default value map.
     * @param sObj The current account record being operated on.
     */
    protected override void beforeInsert(SObject sObj) {
        Account account = (Account)sObj;
        if (account.SSS_School_Code__c == null) {
            account.SSS_School_Code__c = String.valueOf(sssSchoolCode++);
        }

        if (account.Name == SCHOOL_NAME) {
            account.Name = SCHOOL_NAME + counter;
        }
    }

    protected override void afterInsert(SObject sObj) {
        // Increment the counter to keep the certain values unique.
        counter++;
    }

    /**
     * @description Insert the current working Account record.
     * @return The currently operated upon Account record.
     */
    public Account insertAccount() {
        return (Account)insertRecord();
    }

    public List<Account> insertAccounts(Integer numberOfRecords) {
        return (List<Account>)insertRecords(numberOfRecords);
    }

    /**
     * @description Create the current working Account record without resetting
     *              the stored values in this instance of AccountTestData.
     * @return A non-inserted Account record using the currently stored field values.
     */
    public Account createAccountWithoutReset() {
        return (Account)buildWithoutReset();
    }

    /**
     * @description Create the current working Account record.
     * @return The currently operated upon Account record.
     */
    public Account create() {
        return (Account)super.buildWithReset();
    }

    /**
     * @description The default account record.
     */
    public Account DefaultAccount {
        get {
            if (DefaultAccount == null) {
                DefaultAccount = createAccountWithoutReset();
                insert DefaultAccount;
            }
            return DefaultAccount;
        }
        private set;
    }

    /**
     * @description Get the Account SObjectType.
     * @return The Account SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Account.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static AccountTestData Instance {
        get {
            if (Instance == null) {
                Instance = new AccountTestData();
            }
            return Instance;
        }
        private set;
    }

    private AccountTestData() { }
}