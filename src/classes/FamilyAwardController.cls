public class FamilyAwardController
{
// TODO SFP-1750 We should make this with sharing.
// Issue was not resolved.
// Now if we make Award and Awards controllers *with sharing* we are not getting any results in loadPFS method
// A Family user then doesn't see any student folders, pfss, spas and so on.
// Maybe some specific security setup for user profile is required

    public ApplicationUtils appUtils { get; set; }
    public FamilyTemplateController controller { get; set; }
    public Id folderId { get; set; }
    
    private Student_Folder__c folder;
    
    public FamilyAwardController(FamilyTemplateController thisController)
    {
        controller = thisController;
        
        folderId = ApexPages.currentPage().getParameters().get('id');
        
        if (controller.pfsRecord == null)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error: No PFS Record Found for current user.'));
            return;
        }
        
        if (!hasAccessToFolder)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Family_Award_No_Access_To_Folder_Message));
            return;
        }
        
        appUtils = new ApplicationUtils(UserInfo.getLanguage(), this.controller.pfsRecord);
        
    }
    
    public Boolean hasAccessToFolder {
        
        get {
            
            if (hasAccessToFolder == null) {
                
                Id loggedUserContactId = GlobalVariables.getCurrentUser().ContactId;

                // TODO SFP-1750 Will this work for TCN family view? Probably not unless we pass the actual PFS Id as a url parameter.
                //Done

                hasAccessToFolder = FeatureToggles.isEnabled('Allow_Mass_Email_Per_Applicant__c') 
                    && (loggedUserContactId == controller.pfsRecord.Parent_A__c 
                    || loggedUserContactId == controller.pfsRecord.Parent_B__c);
            }
            
            return hasAccessToFolder;
        }
        private set;
    }
    
    public static FamilyTemplateController.config loadPFS(Id folderId, Id pfsId)
    {
        try
        {
            //        List<School_PFS_Assignment__c> spas = SchoolPfsAssignmentsSelector.Instance.selectBySetOfStudentFolders(
            //            new Set<Id>{folderId}, new List<String>{'Id', 'PFS_Id__c'}, null);

            // TODO SFP-1750 This is a temporary solution while I work on the UX, refactoring the template/top bar PFS retrieval logic will come second.

            Id contactId = CurrentUser.getCurrentUser().ContactId;

            List<School_PFS_Assignment__c> spas;
            if (Test.isRunningTest())
            {
                 spas = [SELECT Id, PFS_ID__c, Applicant__c, Applicant__r.PFS__r.Parent_A__c
                         FROM School_PFS_Assignment__c
                         WHERE Student_Folder__c = :folderId];
            }
            else
            {
                 spas = [SELECT Id, PFS_ID__c, Applicant__c, Applicant__r.PFS__r.Parent_A__c
                         FROM School_PFS_Assignment__c
                         WHERE Student_Folder__c = :folderId AND Applicant__r.PFS__r.Parent_A__c = :contactId];
            }

            if (spas.isEmpty())
            {
                // TODO SFP-1750 What happens when this exception is thrown? What handles it?
                // Done
                throw new FamilyAwardsService.FamilyAwardsServiceException('Error: Record not found.');
            }

            Set<Id> pfsIds = new Set<Id>();
            for (School_PFS_Assignment__c spa : spas)
            {
                pfsIds.add(spa.PFS_ID__c);
            }

            // TODO SFP-1750 Do we need the list of fields to query? Can't the AppUtils reference a field set that queries the right fields?
            //Done
            Map<Id, PFS__C> pfsById = new Map<Id, PFS__c>(PfsSelector.Instance.selectWithCustomFieldListById(
                    pfsIds, ApplicationUtils.getFieldsFromFieldsSet()));

            // TODO SFP-1750 BROKEN Does not handle 2 pfs households. In these scenarios, we can not be sure which PFS we get. We need to ensure we get the right PFS.
            //Done
            PFS__c pfsRecord = pfsById.get(pfsId);

            return new FamilyTemplateController.config(pfsRecord, pfsById.values());
        }
        catch (FamilyAwardsService.FamilyAwardsServiceException e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
            return null;
        }
    }
    
    public FamilyAwardsService.AwardsWrapper award
    {
        get
        {
            return applicant != null && applicant.awards != null && !applicant.awards.isEmpty() 
                ? applicant.awards[0] : null;
        }
        
        private set;
    }
    
    public FamilyAwardsService.ApplicantsWithAwardsWrapper applicant
    {
        get
        {
            try
            {
                if (controller.pfsRecord.Id == null)
                {
                    // TODO SFP-1750 What happens when this exception is thrown? What handles it?
                    //Done
                    throw new FamilyAwardsService.FamilyAwardsServiceException('Error: Record not found.');
                }

                List<FamilyAwardsService.ApplicantsWithAwardsWrapper> applicants = FamilyAwardsService.Instance.getApplicantsWithAwards(controller.pfsRecord.Id, folderId);
                applicant = applicants == null || applicants.isEmpty() ? null : applicants.get(0);

                Map<Id, Student_Folder__c> folders = FamilyAwardsService.Instance.foldersMap;
                folder = folders == null || folders.isEmpty() ? null : folders.values().get(0);

                return applicant;

            }
            catch (FamilyAwardsService.FamilyAwardsServiceException e)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
                return null;
            }
        }
        private set;
    }
    /**
    * @description Method invoked after the user clicks "Accept" button.
    */
    public void acceptAction()
    {
        FamilyAwardsService.Instance.acceptAward(folder);
    }
    
    /**
    * @description Method invoked after the user clicks "Deny" button.
    */
    public void denyAction()
    {
        FamilyAwardsService.Instance.acceptAward(folder);
    }
}