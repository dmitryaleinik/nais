public class StudentFolderAction{
    
    public static void CopyDataFromPreviousYear(PreviousYearFoldersWrapper info)
    {
        Map<String, Integer> academicYearWeight = getAcademicYearsEnum();
        
        for(Contact c : [SELECT Id, 
                            (SELECT Id, School__c, Student__c, Student_ID__c, Faculty_Child__c, Academic_Year_Picklist__c
                                FROM Student_Folders__r 
                                WHERE School__c IN: info.schoolId) 
                            FROM Contact 
                            WHERE Id IN: info.studentId ])
        {
            if( !c.Student_Folders__r.isEmpty() )
            {
                Map<Integer, Student_Folder__c> foldersByAcademicYearWeight = new Map<Integer, Student_Folder__c>();
                
                for(Student_Folder__c ff : c.Student_Folders__r )
                {
                    if( academicYearWeight.containsKey(ff.Academic_Year_Picklist__c) )
                    {
                        foldersByAcademicYearWeight.put(academicYearWeight.get(ff.Academic_Year_Picklist__c), ff);
                    }
                }
                if( !foldersByAcademicYearWeight.isEmpty() 
                && info.foldersByStudent.containsKey(c.Id))
                {
                    Student_Folder__c existingFolder;
                    
                    for(Student_Folder__c f : info.foldersByStudent.get(c.Id) )//Folders triggered
                    {
                        Integer thisFolderWeight = academicYearWeight.get(f.Academic_Year_Picklist__c);
                        List<Integer> allWeights = new List<Integer>();
                        allWeights.addAll(foldersByAcademicYearWeight.keyset());
                        allWeights.sort();

                        Boolean studentIdUpdated = false;
                        Boolean facultyChildUpdated = false;

                        // We check all of the previous folders for that school for data to pull forward. If a 2018-2019 folder is inserted,
                        // we get all previous folders for that student. We would first check the 17-18 folder for the data to pull forward.
                        // If that folder has what we need, we move on to the next school that the student is applying to.
                        // We check all past years instead of just the previous year in case a student skips a year and comes back.
                        for(Integer weight : allWeights)
                        {
                            existingFolder = foldersByAcademicYearWeight.get(weight);
                            if (f.Id != existingFolder.Id && f.School__c == existingFolder.School__c) {
                                // Check if we have already found a student Id to copy over.
                                // If so, we don't need to update it any more.
                                // We also make sure the student Id isn't null to avoid copying an empty Student_ID__c
                                if (!studentIdUpdated && String.isNotBlank(existingFolder.Student_ID__c)) {
                                    f.Student_ID__c = existingFolder.Student_ID__c;
                                    // Once we pull a student Id forward, set the boolean toggle so we don't keep doing this.
                                    // We need this flag so we can continue looking for a faculty child value to
                                    // pull forward and vice versa.
                                    studentIdUpdated = true;
                                }

                                // SFP-1651: We will now carry over the faculty child picklist as well.
                                // If we have already found a value to pull forward (not blank or null) then skip this.
                                if (!facultyChildUpdated && String.isNotBlank(existingFolder.Faculty_Child__c)) {
                                    f.Faculty_Child__c = existingFolder.Faculty_Child__c;
                                    // Once we pull the faculty child value forward, set the boolean toggle so we don't keep doing this.
                                    // We need this flag so we can continue looking for a student Id value to
                                    // pull forward and vice versa.
                                    facultyChildUpdated = true;
                                }
                            }

                            if (studentIdUpdated && facultyChildUpdated) {
                                break;
                            }
                        }
                        
                    }
                }
            }
        }
    }//End:CopyDataFromPreviousYear
    
    public static Map<String, Integer> getAcademicYearsEnum()
    {
        Map<String, Integer> result = new Map<String, Integer>();
        List<Academic_Year__c> academicYears = GlobalVariables.getAllAcademicYears();
        for (Integer i=(academicYears.size()-1); i>=0; i--){
            result.put(academicYears[i].Name, i);
        }
        
        return result;
    }
    
    
    public class PreviousYearFoldersWrapper
    {
         public Set<Id> studentId{ get; private set; }//ContactIds
         public Set<Id> schoolId{ get; private set; }
         public Map<Id, List<Student_Folder__c>> foldersByStudent{ get; private set; }
         
         public PreviousYearFoldersWrapper()
         {
             this.studentId = new Set<Id>();
             this.schoolId = new Set<Id>();
             this.foldersByStudent = new Map<Id, List<Student_Folder__c>>();
         }
         
         public void put(Id varStudentId, Id varSchoolId, Student_Folder__c f)
         {
             this.studentId.add(varStudentId);
             this.schoolId.add(varSchoolId);
             List<Student_Folder__c> tmp = ( this.foldersByStudent.containsKey(f.Student__c)
                                         ? this.foldersByStudent.get(f.Student__c)
                                         : new List<Student_Folder__c>() );
             tmp.add(f);
             this.foldersByStudent.put(f.Student__c, tmp);
         }
     }//End-Class:PreviousYearFoldersWrapper
}