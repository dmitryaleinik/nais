/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
@IsTest
private with sharing class CommunitiesLandingControllerTest
{

    @IsTest(SeeAllData=true)
    private static void testCommunitiesLandingController()
    {
        // Instantiate a new controller with all parameters in the page
        CommunitiesLandingController controller = new CommunitiesLandingController();
    }
}