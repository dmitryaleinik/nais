/**
* @SchoolDashboardController.cls
* @author Exponent Partners, 2013
* @description Controller for the SchoolDashboard.page
*/

public without sharing class SchoolDashboardController implements SchoolAcademicYearSelectorInterface {

    /*Initialization*/
    public SchoolDashboardController(){
        u = GlobalVariables.getCurrentUser();
        academicyearid = System.currentPagereference().getParameters().get('academicyearid');
        schoolId = GlobalVariables.getCurrentSchoolId();
        schoolRepId = String.valueOf(schoolId).substring(0,15); //NAIS-1833
        isAdminExpanded = true;

        // NAIS-2375 Subscription Info Start
        for (Account a : [select RecordTypeId, Paid_Through__c, SSS_Subscription_Type_Current__c,
                            (select Subscription_Type__c from Subscriptions__r where Cancelled__c = 'No' order by End_Date__c desc limit 1)
                            from Account where Id = :schoolId]) {
            // show the latest subscription type that ends on subscriptionEndDate
            //    not necessarily the current subscription type if account is renewed for a different subscription type than the current type
            //subscriptionType = a.SSS_Subscription_Type_Current__c;
            subscriptionType = (a.Subscriptions__r.size() > 0) ? a.Subscriptions__r[0].Subscription_Type__c : a.SSS_Subscription_Type_Current__c;
            subscriptionEndDate = a.Paid_Through__c;
            renderRenewalLink = (subscriptionEndDate != null && subscriptionEndDate.year() == System.today().year() && GlobalVariables.isRenewalSeason());
            isSubscriptionExpired = (subscriptionEndDate == null || subscriptionEndDate < System.today());            // NAIS-2373
            schoolType = a.RecordTypeId;
        }
        // NAIS-2375 Subscription Info End

        //NAIS-1833 Start
        List<String> reports = new List<String>{'PFSs Received in Last 7 Days','PFSs Received in Last 1 Day','PFSs updated in last 24 hours'};
        List<Report> queriedReports = [SELECT Id, Name FROM Report WHERE name IN :reports];
        for(Report rep: queriedReports){
            if(rep.Name == 'PFSs Received in Last 7 Days'){
                PFSRepReceivedWeek = rep;
            }else if(rep.Name == 'PFSs Received in Last 1 Day'){
                PFSRepReceivedDay = rep;
            }else if(rep.Name == 'PFSs updated in last 24 hours'){
                PFSRepUpdatedDay = rep;
            }
        }
        //NAIS-1833 End

        // allow academic year to be passed in through URL
        if (academicyearid == null){
            acadYear = GlobalVariables.getCurrentAcademicYear();
            academicyearid = acadYear.Id;
        } else {
            acadYear = [SELECT Id, Name, Start_Date__c, End_Date__c FROM Academic_Year__c WHERE Id = :academicyearid];
        }

        year = acadYear.Name;

        List<Annual_Setting__c> annSets = [Select Id, Name, Total_Waivers_Override_Default__c, Waiver_Credit_Balance_Formula__c, Waivers_Assigned__c, Academic_Year__c, Academic_Year__r.Name,
                    Professional_Judgment_Updated__c, Tuition_And_Fees_Updated__c, Annual_Settings_Updated__c, Accept_Documents_Online__c, Email_Reminders_to_Parents__c,
                    PFS_Deadline_Type__c, Prior_Year_Tax_Documents_Deadline_Type__c, Current_Year_Tax_Documents_Deadline_Type__c
                    from Annual_Setting__c
                    where School__c = :schoolId
                    AND Academic_Year__c = :academicyearid limit 1];

        annSet = annSets.size() > 0 ? annSets[0] : null;

        //SFP-623: If there's no annualSetting created, or if the existing one do not have the required fields empty.
        this.hasAnnualSetting = (annSet==null || annSet.Annual_Settings_Updated__c== null?false:true);

        // Default null to zero so screen displays well
        if(annSet == null){
            annSet = new Annual_Setting__c();
        }
        if (annSet.Total_Waivers_Override_Default__c == null){
            annSet.Total_Waivers_Override_Default__c = 0;
        }

        

        loadFolderIds();
        loadPFSStats();
        loadMSGStats();
        loadAwardStats();
        loadAdminProgress();

        basicVersion = GlobalVariables.isLimitedSchoolView();
        // [DP] custom settings refactor - NAIS-1498 2.5.14
        nonAdminUser = !GlobalVariables.isSchoolPortalAdminProfile(u); // default to nonAdminUser [DP] NAIS-1498
        basicVersionClass = basicVersion ? 'basicversion' : '';


        //NAIS-1960 Start
        String q = 'SELECT Id FROM Case Limit 1';
        ApexPages.Standardsetcontroller caseListViews = new ApexPages.Standardsetcontroller(Database.getQueryLocator(q));
        List<SelectOption> test = caseListViews.getListViewOptions();
        for(SelectOption so: test){
            if(so.getLabel() == 'Open Cases - All'){
                openCaseListView = so.getValue().substring(0,15);
            }
        }
        //NAIS-1960 End

        // SFP-36, change to dyanmic links
        redirectMessagePage= Page.SchoolListMessage.getUrl(); //NAIS-1973
        redirectSupportTicketPage =Page.SchoolSupportTicket.getUrl();//NAIS-1973
    }
    /*End Initialization*/

    /*Properties*/
    public Academic_Year__c acadYear {get; set;}
    public Annual_Setting__c annSet {get; set;}
    public User u {get; set;}
    public List<Subscription__c> subs {get; set;}
    public Report PFSRepReceivedWeek {get; set;}
    public Report PFSRepReceivedDay {get; set;}
    public Report PFSRepUpdatedDay {get; set;}

    public Id academicyearid;

    public Id schoolId {get; set;}
    public String schoolRepId {get; set;}

    // NAIS-2375 Subscription Info Start
    public String subscriptionType {get; set;}
    public Date subscriptionEndDate {get; set;}
    public Boolean renderRenewalLink {get; set;}
    // NAIS-2375 Subscription Info End

    public Boolean isSubscriptionExpired {get; set;}        // NAIS-2373
    public Boolean hasAnnualSetting {get; set;}

    public String year {get; set;}
    public String redirectMessagePage{get;set;} //NAIS-1973
    public String redirectSupportTicketPage{get;set;} //NAIS-1973

    //public String welcomeMessage {get; set;}

    public Boolean genConfigUpdated {get; set;}
    public Boolean tuitionFeesUpdated {get; set;}
    public Boolean budgetGroupsUpdated {get; set;}
    public Boolean profJudgmentUpdated {get; set;}
    public Boolean schoolProfileUpdated {get; set;}
    public Boolean reqDocsUpdated {get; set;}
    public String adminStatus {get; set;}
    public String openCaseListView {get; set;}

    public Date genConfigDate {get; set;}
    public Date tuitionFeesDate {get; set;}
    public Date budgetGroupsDate {get; set;}
    public Date profJudgmentDate {get; set;}
    public Date schoolProfileDate {get; set;}
    public Date reqDocsDate {get; set;}

    public Boolean displayReqDocs {get; set;}

    public Integer awardsGiven {get; set;}
    public Double avgAward {get; set;}

    public Integer pfsPastWeek {get; set;}
    public Integer pfsPastDay {get; set;}
    public Integer pfsUpdatedPastDay {get; set;}

    public Integer msgsToday {get; set;}
    public Integer ticketsToday {get;set;} //NAIS-1973
    public Integer msgsThisWeek {get; set;}
    public Integer ticketsThisWeek {get; set;} //NAIS-1973
    public Integer openCases {get; set;}
    public Integer openTickets {get; set;} //NAIS-1973
    //public List<Case> cases {get; set;}

    public Integer openFeeWaiverRequests {get; set;}

    public List<Id> folderIdList {get; set;}
    public Set<Id> folderIds;
    public Set<Id> PFSIds;

    public Boolean displayMsgFeeWaiv {get; set;} //NAIS-1738

    public Boolean basicVersion {get; set;}
    public Boolean nonAdminUser {get; set;}
    public String basicVersionClass {get; set;}
    public Boolean isAdminExpanded {get;set;}//[SG] 8/26 SFP-14

    public Boolean getHasBudgets(){
        return GlobalVariables.usersSchoolHasBudgetGroups(acadYear.Id);
    }

    public List<Global_Message__c> GlobalMessages{
        get {
            Id currentUser = UserInfo.getProfileId();
            List<Global_Message__c> globalMessages = GlobalVariables.GlobalMessages(currentUser, true);

            return globalMessages;
        }
        set;
    }

    /**
     * @description Checks to see if there are any global messages available to the user.
     * @return True if there are global messages to display.
     */
    public Boolean getHasGlobalMessages() {
        return GlobalMessages != null && !GlobalMessages.isEmpty();
    }

    public Integer getFeeWaiverRequests(){
        OpenCases oc = new OpenCases();
        return oc.feeWaiverRequestCount(schoolID, acadYear);
    }

    public SchoolDashboardController Me {
        get { return this; }
    }

    public String getAcademicYearId() {
        return this.academicyearid;
    }

    public void setAcademicYearId(String academicYearIdParam) {
        this.academicyearid = academicYearIdParam;
    }

    /*End Properties*/

    /*Action Methods*/

    public PageReference init(){
        
        // pass in "isSchoolPortal" boolean param of true
        if (!GlobalVariables.userHasAcceptedTermsAndConditions(true)){
            
            PageReference terms = Page.SchoolTermsAndConditions;
            terms.getParameters().put('inSchoolPortal', 'true');
            return terms;
        }

        Boolean justLoggedIn = System.currentPagereference().getParameters().get('login') == 'true';
        Boolean redirectToSchoolPicker = false;
        
        List<Account> userAccounts = GlobalVariables.getAllSchoolsForUser();
        
        // we will redirect multiusers to the school picker page in the following scenarios:
        if (userAccounts.size() > 1){
            // 1. they don't have an in focus school
            if (u.In_Focus_School__c == null){
                redirectToSchoolPicker = true;
            }

            // 2. they just logged in and do not have their persist setting checked
            if (justLoggedIn && u.Persist_School_Focus__c != true){
                redirectToSchoolPicker = true;
            }
        }
        
        if (redirectToSchoolPicker)
        {
            //TODO redirect to school picker here TODO
            return Page.SchoolPicker;
        }
        else
        {
            // NAIS-2373 subscription expired
            if (isSubscriptionExpired )
            {
                return Page.SchoolRenewal;
            }
            else if( !this.hasAnnualSetting )
            {
                if ( !isAllowAccessWithoutAnnualSettings() && !isLimitedSchoolViewOrAccessOrg )
                {
                    return Page.SchoolAnnualSettings;
                }else{
                    
                    List<Annual_Setting__c> annualSettings = GlobalVariables.getCurrentAnnualSettings(true, acadYear.Id);
                    
                    if(isLimitedSchoolViewOrAccessOrg && annualSettings!=null && !annualSettings.isEmpty()){
                        
                        annualSettings[0].Annual_Settings_Updated__c = system.today();
                        update annualSettings[0];
                    }
                }
            }
            
            return null;
        }
    }
    
    private Id schoolType;
    private Boolean isLimitedSchoolViewOrAccessOrg {
        get{
            if( isLimitedSchoolViewOrAccessOrg == null) {
                isLimitedSchoolViewOrAccessOrg = GlobalVariables.isLimitedSchoolView() || schoolType == RecordTypes.accessOrgAccountTypeId;
            }
            
            return isLimitedSchoolViewOrAccessOrg;
        }
        set;
    }

    private Boolean isAllowAccessWithoutAnnualSettings() {
        if (u.Profile.Name != ProfileSettings.SchoolAdminProfileName) {
            Cookie isVisitedAnnualSettings = ApexPages.currentPage().getCookies().get('AnnualSettingsVisited');

            if (isVisitedAnnualSettings != null) {
                return true;
            }
        }

        return false;
    }


    public PageReference SchoolAcademicYearSelector_OnChange(Boolean saveRecord) {
        PageReference newPage = null;

        PageReference thisPage = ApexPages.currentPage();
        newPage = new PageReference(thisPage.getUrl());
        newPage.getParameters().put('id', '');
        newPage.getParameters().put('academicyearid', this.academicyearid);

        newPage.setRedirect(true);
        return newPage;
    }

    /*End Action Methods*/

    /*Helper Methods*/

    public void loadAdminProgress(){
        if (annSet != null){
            genConfigDate = annSet.Annual_Settings_Updated__c;
             tuitionFeesDate = annSet.Tuition_And_Fees_Updated__c;
             profJudgmentDate = annSet.Professional_Judgment_Updated__c;
             displayReqDocs = annSet.Accept_Documents_Online__c != 'No';
        }
        // NAIS-1738 Start
        Account currentAccount = [Select Id, Name, Profile_Updated__c, SSS_Subscription_Type_Current__c from Account where Id = :schoolId];
        displayMsgFeeWaiv = true;
        if(currentAccount.SSS_Subscription_Type_Current__c == 'Basic'){
            displayMsgFeeWaiv = false;
        }
        // NAIS-1738 End
        schoolProfileDate = currentAccount.Profile_Updated__c;

        List<Budget_Group__c> budgetGroups = [Select Id, LastModifiedDate, CreatedDate from Budget_Group__c
                                                                             where School__c = :schoolId
                                                                             AND Academic_Year__c = :acadYear.Id
                                                                             order by CreatedDate desc];

        if (budgetGroups != null && budgetGroups.size() > 0){
            budgetGroupsDate = Date.valueOf(budgetGroups[0].LastModifiedDate);
        }

        List<Required_Document__c> reqDocList = [Select Id, LastModifiedDate from Required_Document__c
                                                     where School__c = :schoolId
                                                     AND Academic_Year__c = :acadYear.Id
                                                     order by CreatedDate desc];

        if (reqDocList != null && reqDocList.size() > 0){
            reqDocsDate = Date.valueOf(reqDocList[0].LastModifiedDate);
        }

        genConfigUpdated = genConfigDate == null ? false : true;
        tuitionFeesUpdated = tuitionFeesDate == null ? false : true;
        budgetGroupsUpdated = budgetGroupsDate == null ? false : true;
        profJudgmentUpdated = profJudgmentDate == null ? false : true;
        schoolProfileUpdated = schoolProfileDate == null ? false : true;
        reqDocsUpdated = reqDocsDate == null ? false : true;

        adminStatus = genConfigUpdated && tuitionFeesUpdated && budgetGroupsUpdated && profJudgmentUpdated && schoolProfileUpdated && (!displayReqDocs || reqDocsUpdated) ? 'Complete' : 'Incomplete';

        //[SG] 8/26 SFP-14
        if(adminStatus == 'Complete'){
            isAdminExpanded = false;
        }
        else{
            Cookie asCookie = ApexPages.currentPage().getCookies().get('Dashboard_Admin_Setup');
            if(asCookie != null && String.isNotblank(asCookie.getValue())){
                isAdminExpanded = (asCookie.getValue()=='true')?true:false;
            }
        }
    }

    public void loadPFSStats(){
        Date pastWeek = system.today().addDays(-7);
        Date pastDay = System.today().addDays(-1);
        Date PFSmidnightTwodaysAgo = system.today().addDays(-1);

        pfsPastWeek = 0;
        pfsPastDay = 0;

        // [CH] NAIS-2014 Adjusting logic for PFSs recieved in the past day
        List<Id> uniquePFSIdsPastDay = new List<Id>();
        List<Id> uniquePFSIdsPastWeek = new List<Id>(); //[GH] 11-13-14 1833
        for(School_PFS_Assignment__c spa : [select Id, Applicant__r.PFS__c, Applicant__r.PFS__r.Original_Submission_Date__c, Applicant__r.PFS__r.PFS_Status__c, CreatedDate
                                                from School_PFS_Assignment__c
                                                where Applicant__r.PFS__r.Academic_Year_Picklist__c = :acadYear.Name
                                                and School__c = :schoolId
                                                and ( Applicant__r.PFS__r.Original_Submission_Date__c >= :pastWeek
                                                or (Applicant__r.PFS__r.PFS_Status__c = 'Application Submitted' and CreatedDate >= :pastWeek)) ])
        {
            //[GH] 11-13-14 1833
            if(spa.Applicant__r.PFS__r.Original_Submission_Date__c >= pastDay || (spa.Applicant__r.PFS__r.PFS_Status__c == 'Application Submitted' && spa.CreatedDate >= pastDay)){
                System.debug('*********************** 11 -- '+spa.Id);
                uniquePFSIdsPastDay.add(spa.Id);
            }
            uniquePFSIdsPastWeek.add(spa.Id);
        }

        pfsPastDay = uniquePFSIdsPastDay.size();
        pfsPastWeek = uniquePFSIdsPastWeek.size();  //[GH] 11-13-14 1833

        pfsUpdatedPastDay = [Select Count() from School_PFS_Assignment__c WHERE Applicant__r.PFS__c in :PFSIds AND Application_Last_Modified_by_Family__c > :PFSmidnightTwoDaysAgo AND School__c = :schoolId];
    }
    //NAIS-1833 End
    public void loadMSGStats(){
        DateTime weekStart = System.now().dateGMT().addDays(-7);

        OpenCases oc = new OpenCases(); //NAIS-1960

        List<AggregateResult> caseResults = oc.newCases(schoolId, weekStart);
        List<AggregateResult> ticketsResults = oc.newSupportTickets(schoolId, weekStart);//NAIS-1973


        msgsToday = 0;
        ticketsToday=0; //NAIS-1973
        msgsThisWeek = 0;
        ticketsThisWeek =0;
        for (AggregateResult ar : caseResults){
            if (Integer.valueOf(ar.get('numDate')) == System.now().dayGmt() || Integer.valueOf(ar.get('numDate')) == System.now().addDays(-1).dayGmt()){
                msgsToday += Integer.valueOf(ar.get('commentCount'));
            }
            msgsThisWeek += Integer.valueOf(ar.get('commentCount'));
        }
        /*NAIS-1973*/
        for (AggregateResult ar : ticketsResults){
            if (Integer.valueOf(ar.get('numDate')) == System.now().dayGmt() || Integer.valueOf(ar.get('numDate')) == System.now().addDays(-1).dayGmt()){
                ticketsToday += Integer.valueOf(ar.get('commentCount'));
            }
            ticketsThisWeek += Integer.valueOf(ar.get('commentCount'));
        }
        /*End NAIS-1973*/

        openCases = oc.openCases(); //NAIS-1960
        openTickets = oc.openTickets(); //NAIS-1973
    }

    //NAIS-1960 Start
    public with sharing class OpenCases {
        public Integer openCases(){
            Integer openCases = [Select Count() from Case where IsClosed = false AND RecordTypeId in :SchoolMessageListController.schoolPortalCaseRecordTypes]; // [DP] 06.29.2015 NAIS-2480
            return openCases;
        }
        /*NAIS-1973*/
        public Integer openTickets(){
            Integer openTickets = [Select Count() from Case where IsClosed = false AND RecordTypeId in :SchoolMessageListController.schoolSupportCaseRecordTypes]; // [DP] 06.29.2015 NAIS-2480
            return openTickets;
        }
        /*End NAIS-1973*/
        public List<AggregateResult> newCases(Id SchoolId, DateTime weekStart){
            List<AggregateResult> caseResults = [SELECT count(ID) commentCount, DAY_IN_MONTH(Created_Updated_for_School_Portal__c) numDate
                                                    FROM Case
                                                    WHERE Created_Updated_for_School_Portal__c >= :weekStart
                                                    AND RecordTypeId in :SchoolMessageListController.schoolPortalCaseRecordTypes // [DP] 06.29.2015 NAIS-2480
                                                    group by DAY_IN_MONTH(Created_Updated_for_School_Portal__c)];

            return caseResults;
        }
        /*NAIS-1973*/
        public List<AggregateResult> newSupportTickets(Id SchoolId, DateTime weekStart){
            List<AggregateResult> caseResults = [SELECT count(ID) commentCount, DAY_IN_MONTH(Created_Updated_for_School_Portal__c) numDate
                                                    FROM Case
                                                    WHERE Created_Updated_for_School_Portal__c >= :weekStart
                                                    AND RecordTypeId in :SchoolMessageListController.schoolSupportCaseRecordTypes // [DP] 06.29.2015 NAIS-2480
                                                    group by DAY_IN_MONTH(Created_Updated_for_School_Portal__c)];

            return caseResults;
        }
        /*End NAIS-1973*/


        public Integer feeWaiverRequestCount(Id SchoolId, Academic_Year__c acadYear){
            return [SELECT Count() FROM PFS__c
                    WHERE Parent_A__c IN (SELECT Contact__c            // In AFW Requesting list.
                                        FROM Application_Fee_Waiver__c
                                        WHERE Account__c = :schoolId
                                            AND Academic_Year__c = :acadYear.Id
                                            AND Status__c = 'Pending Approval')
                    AND Academic_Year_Picklist__c = :acadYear.Name
                    AND Payment_Status__c != 'Paid in Full'
            ];
        }

    }
    //NAIS-1960 End
    public void loadAwardStats(){
        Set<String> approvedStatuses = new Set<String> {'Award Approved', 'Complete, Award Accepted'};

        List<AggregateResult> awardResults = [SELECT count(ID) awdCount, sum(Grant_Awarded__c) amt, avg(Grant_Awarded__c) avgAmt from Student_Folder__c
                                                    where Id in :folderIds
                                                    AND Folder_Status__c in :approvedStatuses
                                                    ];


        awardsGiven = Integer.valueOf(awardResults[0].get('awdCount'));
        avgAward = Integer.valueOf(awardResults[0].get('avgAmt'));
    }

    // load sets of folder Ids and PFS Ids for this school
    public void loadFolderIds(){
        folderIds = new Set<Id>();
        PFSIds = new Set<Id>();

        for (School_PFS_Assignment__c spfs : [Select Id, School__c, Student_Folder__c, Applicant__r.PFS__c
                                                from School_PFS_Assignment__c
                                                where School__c = :schoolId
                                                AND Academic_Year_Picklist__c = :acadYear.Name ]){
            if (!folderIds.contains(spfs.Student_Folder__c)){
                folderIds.add(spfs.Student_Folder__c);
            }

            if (!PFSIds.contains(spfs.Applicant__r.PFS__c)){
                PFSIds.add(spfs.Applicant__r.PFS__c);
            }

        }

        //SFP- 25
        for(Student_Folder__c sf : [select Id from Student_Folder__c where School__c = :schoolId
                                       AND Academic_Year_Picklist__c = :acadYear.Name
                                       and Folder_Source__c = 'School-Initiated']){
            folderIds.add(sf.Id);
        }

        folderIdList = new List<Id>(folderIds);
    }

    /*End Helper Methods*/
}