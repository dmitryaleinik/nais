public with sharing class SSSFamilySiteLogoController {
    
    public List<Global_Message__c> getGmLandingPageOnly()
    {
        List<Global_Message__c> allGlobalMessages = GlobalVariables.GlobalMessages('Family Login', false);
        List<Global_Message__c> gmLandingPageOnly = new List<Global_Message__c>();
        
        for (Global_Message__c gm : allGlobalMessages)
        {
            if (gm.Portal__c.contains('Family Login - Landing Page Only'))
            {
                gmLandingPageOnly.add(gm);
            }
        }

        return gmLandingPageOnly;
    }

    public List<Global_Message__c> getGmAllPages()
    {
        List<Global_Message__c> allGlobalMessages = GlobalVariables.GlobalMessages('Family Login', false);
        List<Global_Message__c> gmAllPages = new List<Global_Message__c>();

        for (Global_Message__c gm : allGlobalMessages)
        {
            if (gm.Portal__c.contains('Family Login - All Pages'))
            {
                gmAllPages.add(gm);
            }
        }

        return gmAllPages;
    }
    
    public Boolean isLandingPage
    {
        get
        {
            return GlobalVariables.isFamilyLoginLandingPage();
        } set;
    }
}