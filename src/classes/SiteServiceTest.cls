@isTest
private class SiteServiceTest
{

    private static final String SAMPLE_USERNAME = 'test@siteservice.com';
    private static final String SAMPLE_PASSWORD = 'qwerty12345';
    private static final String NEW_PASSWORD = 'qwer1234';

    private static void setStartUrl(String startUrlValue) {
        ApexPages.currentPage().getParameters().put(SiteService.START_URL_PARAM, startUrlValue);
    }

    private static SiteService.Request createLoginRequest(SiteService.PortalType currentPortal) {
        return new SiteService.Request(currentPortal, SAMPLE_USERNAME, SAMPLE_PASSWORD);
    }

    private static SiteService.Request createChangePasswordRequest(SiteService.PortalType currentPortal) {
        return new SiteService.Request(currentPortal, NEW_PASSWORD, NEW_PASSWORD, SAMPLE_PASSWORD);
    }

    private static void mockExpiredPassword() {
        // Setting this to true simulates scenarios where a user has chosen to reset their password and clicked on a password reset email.
        SiteService.mockIsPasswordExpired = true;
    }

    private static List<Academic_Year__c> insertAcademicYears() {
        DateTime landingPageStart = DateTime.now().addDays(1);

        return insertAcademicYears(landingPageStart);
    }

    private static List<Academic_Year__c> insertAcademicYears(DateTime landingPageStart) {
        List<Academic_Year__c> testAcademicYears = createAcademicYears(landingPageStart);

        insert testAcademicYears;

        return testAcademicYears;
    }

    private static List<Academic_Year__c> createAcademicYears(DateTime landingPageStart) {
        Integer numberOfRecords = 3;

        List<Academic_Year__c> testAcademicYears = AcademicYearTestData.Instance.createAcademicYears(numberOfRecords);

        // make first academic year in the future for family users.
        testAcademicYears[0].Family_Landing_Page_Start_Date__c = landingPageStart;
        testAcademicYears[0].Family_Portal_Start_Date__c = Date.today().addDays(10);
        testAcademicYears[0].Family_Portal_End_Date__c = Date.today().addDays(20);

        // make the second academic year current based on todays date for family users.
        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-5);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(5);

        // make the third academic year overlap with the current academic year for family users.
        testAcademicYears[2].Family_Portal_Start_Date__c = Date.today().addDays(-15);
        testAcademicYears[2].Family_Portal_End_Date__c = Date.today().addDays(1);

        return testAcademicYears;
    }

    @isTest private static void login_schoolPortal_expectNull() {
        // Create a login request for the school portal.
        SiteService.Request request = createLoginRequest(SiteService.PortalType.SCHOOL);

        PageReference postLoginPage = SiteService.Instance.login(request);

        // We expect null here because Site.cls methods return null even when successful.
        System.assertEquals(null, postLoginPage, 'Expected login to return null.');
    }

    @isTest private static void login_familyPortal_expectNull() {
        // Create a login request for the family portal.
        SiteService.Request request = createLoginRequest(SiteService.PortalType.FAMILY);

        PageReference postLoginPage = SiteService.Instance.login(request);

        // We expect null here because Site.cls methods return null even when successful.
        System.assertEquals(null, postLoginPage, 'Expected login to return null.');
    }

    @isTest private static void changePassword_schoolPortal_expectNull() {
        // Create a change password request for the school portal.
        SiteService.Request request = createChangePasswordRequest(SiteService.PortalType.SCHOOL);

        PageReference postLoginPage = SiteService.Instance.changePassword(request);

        // We expect null here because Site.cls methods return null even when successful.
        System.assertEquals(null, postLoginPage, 'Expected login to return null.');
    }

    @isTest private static void changePassword_familyPortal_expectNull() {
        // Create a change password request for the family portal.
        SiteService.Request request = createChangePasswordRequest(SiteService.PortalType.FAMILY);

        PageReference postLoginPage = SiteService.Instance.changePassword(request);

        // We expect null here because Site.cls methods return null even when successful.
        System.assertEquals(null, postLoginPage, 'Expected login to return null.');
    }

    @isTest private static void changePassword_familyPortal_mockExpiredPassword_expectNull() {
        mockExpiredPassword();

        // Create a change password request for the family portal.
        SiteService.Request request = createChangePasswordRequest(SiteService.PortalType.FAMILY);

        PageReference postLoginPage = SiteService.Instance.changePassword(request);

        // We expect null here because Site.cls methods return null even when successful.
        System.assertEquals(null, postLoginPage, 'Expected login to return null.');
    }

    @isTest private static void getPostLoginUrl_schoolPortal_expectNull() {
        String postLoginUrl = SiteService.Instance.getPostLoginUrl(SiteService.PortalType.SCHOOL);

        System.assertEquals(null, postLoginUrl, 'Expected the post login url to be null for the school portal.');
    }

    @isTest private static void getPostLoginUrl_schoolPortal_startUrlSetToDashboard_expectSchoolDashboard() {
        String startUrl = '/SchoolDashboard';
        setStartUrl(startUrl);

        String postLoginUrl = SiteService.Instance.getPostLoginUrl(SiteService.PortalType.SCHOOL);

        System.assertEquals(startUrl, postLoginUrl, 'Expected the post login url to equal the start url.');
    }

    @isTest private static void getPostLoginUrl_familyPortal_outsideLandingPageDates_expectFamilyDashboard() {
        // Landing page start date is in future.
        DateTime landingPageStart = DateTime.now().addDays(3);

        insertAcademicYears(landingPageStart);

        User familyUser = UserTestData.createFamilyPortalUser();

        String postLoginUrl;
        System.runAs(familyUser) {
            postLoginUrl = SiteService.Instance.getPostLoginUrl(SiteService.PortalType.FAMILY);
        }

        System.assertEquals(SiteService.FAMILY_DASHBOARD_PAGE, postLoginUrl, 'Expected the post login url to be the family dashboard url.');
    }

    @isTest private static void getPostLoginUrl_familyPortal_outsideLandingPageDates_startUrlSetToFamilyDocuments_expectFamilyDocuments() {
        // Landing page start date is in future.
        DateTime landingPageStart = DateTime.now().addDays(3);

        insertAcademicYears(landingPageStart);

        String startUrl = '/FamilyDocuments';
        setStartUrl(startUrl);

        User familyUser = UserTestData.insertFamilyPortalUser();

        String postLoginUrl;
        System.runAs(familyUser) {
            postLoginUrl = SiteService.Instance.getPostLoginUrl(SiteService.PortalType.FAMILY);
        }

        System.assertEquals(startUrl, postLoginUrl, 'Expected the post login url to be the start url param value.');
    }

    @isTest private static void getPostLoginUrl_familyPortal_withinLandingPageDates_earlyAccessClosed_expectFamilyLandingPage() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessClosed().DefaultFamilyPortalSettings;

        // Landing page start date is in the past so we are within the landing page period.
        DateTime landingPageStart = DateTime.now().addDays(-1);

        List<Academic_Year__c> testAcademicYears = insertAcademicYears(landingPageStart);

        User familyUser = UserTestData.insertFamilyPortalUser();

        String postLoginUrl;
        System.runAs(familyUser) {
            postLoginUrl = SiteService.Instance.getPostLoginUrl(SiteService.PortalType.FAMILY);
        }

        System.assertEquals(SiteService.FAMILY_LANDING_PAGE, postLoginUrl,
                'Expected the post login url to be the family landing page.');
    }

    @isTest private static void getPostLoginUrl_familyPortal_withinLandingPageDates_earlyAccessOpen_userDoesNotHaveEarlyAccess_expectFamilyLandingPage() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessOpened().DefaultFamilyPortalSettings;

        // Landing page start date is in the past so we are within the landing page period.
        DateTime landingPageStart = DateTime.now().addDays(-1);

        List<Academic_Year__c> testAcademicYears = insertAcademicYears(landingPageStart);

        User familyUser = UserTestData.insertFamilyPortalUser();

        String postLoginUrl;
        System.runAs(familyUser) {
            postLoginUrl = SiteService.Instance.getPostLoginUrl(SiteService.PortalType.FAMILY);
        }

        System.assertEquals(SiteService.FAMILY_LANDING_PAGE, postLoginUrl,
                'Expected the post login url to be the family landing page.');
    }

    @isTest private static void getPostLoginUrl_familyPortal_withinLandingPageDates_earlyAccessOpen_userHasEarlyAccess_expectFamilyDashboard() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessOpened().DefaultFamilyPortalSettings;

        // Landing page start date is in the past so we are within the landing page period.
        DateTime landingPageStart = DateTime.now().addDays(-1);

        List<Academic_Year__c> testAcademicYears = insertAcademicYears(landingPageStart);

        User familyUser = UserTestData.createFamilyPortalUser();
        familyUser.Early_Access__c = true;
        insert familyUser;

        String postLoginUrl;
        System.runAs(familyUser) {
            postLoginUrl = SiteService.Instance.getPostLoginUrl(SiteService.PortalType.FAMILY);
        }

        System.assertEquals(SiteService.FAMILY_DASHBOARD_PAGE, postLoginUrl,
                'Expected the post login url to be the family dashboard page.');
    }

    // Unit tests for request inner class.
    @isTest private static void loginRequestConstructor_nullCurrentPortal_expectArgumentNullException() {
        try {
            new SiteService.Request(null, SAMPLE_USERNAME, SAMPLE_PASSWORD);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, SiteService.CURRENT_PORTAL_PARAM);
        }
    }

    @isTest private static void loginRequestConstructor_nullUsername_expectArgumentNullException() {
        try {
            new SiteService.Request(SiteService.PortalType.FAMILY, null, SAMPLE_PASSWORD);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, SiteService.USERNAME_PARAM);
        }
    }

    @isTest private static void loginRequestConstructor_nullPassword_expectArgumentNullException() {
        try {
            new SiteService.Request(SiteService.PortalType.FAMILY, SAMPLE_USERNAME, null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, SiteService.PASSWORD_PARAM);
        }
    }

    @isTest private static void loginRequestConstructor_validParams_expectPropertiesSet() {
        SiteService.PortalType currentPortal = SiteService.PortalType.FAMILY;
        String username = SAMPLE_USERNAME;
        String password = SAMPLE_PASSWORD;

        SiteService.Request request = new SiteService.Request(currentPortal, username, password);

        System.assertEquals(currentPortal, request.Portal, 'Expected the portal to be set on the request.');
        System.assertEquals(username, request.Username, 'Expected the username to be set on the request.');
        System.assertEquals(password, request.Password, 'Expected the password to be set on the request.');
    }

    @isTest private static void changePasswordRequestConstructor_nullCurrentPortal_expectArgumentNullException() {
        try {
            new SiteService.Request(null, NEW_PASSWORD, NEW_PASSWORD, SAMPLE_PASSWORD);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, SiteService.CURRENT_PORTAL_PARAM);
        }
    }

    @isTest private static void changePasswordRequestConstructor_nullNewPassword_expectArgumentNullException() {
        try {
            new SiteService.Request(SiteService.PortalType.FAMILY, null, NEW_PASSWORD, SAMPLE_PASSWORD);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, SiteService.NEW_PASSWORD_PARAM);
        }
    }

    @isTest private static void changePasswordRequestConstructor_nullVerifyPassword_expectArgumentNullException() {
        try  {
            new SiteService.Request(SiteService.PortalType.FAMILY, NEW_PASSWORD, null, SAMPLE_PASSWORD);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, SiteService.VERIFY_PASSWORD_PARAM);
        }
    }

    @isTest private static void changePasswordRequestConstructor_nullOldPassword_expectNullProperty() {
        SiteService.Request request = new SiteService.Request(SiteService.PortalType.FAMILY, NEW_PASSWORD, NEW_PASSWORD, null);

        System.assertEquals(null, request.Password, 'Expected the password property to be null when the old password is null.');
    }

    @isTest private static void changePasswordRequestConstructor_validParams_expectPropertiesSet() {
        SiteService.PortalType currentPortal = SiteService.PortalType.FAMILY;
        String username = SAMPLE_USERNAME;
        String newPassword = NEW_PASSWORD;
        String verifyPassword = NEW_PASSWORD;
        String oldPassword = SAMPLE_PASSWORD;

        SiteService.Request request = new SiteService.Request(currentPortal, newPassword, verifyPassword, oldPassword);

        System.assertEquals(currentPortal, request.Portal, 'Expected the portal to be set on the request.');
        System.assertEquals(newPassword, request.NewPassword, 'Expected the new password property to be set.');
        System.assertEquals(verifyPassword, request.VerifyPassword, 'Expected the verify password property be set.');
        System.assertEquals(oldPassword, request.Password, 'Expected the password to be set on the request.');
    }
}