@isTest
private class UserSelectorTest {

    @isTest
    private static void selectById_nullIdSet_expectArgumentNullException() {
        try {
            UserSelector.newInstance().selectById(null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, UserSelector.ID_SET_PARAM);
        }
    }

    @isTest
    private static void selectById_emptyIdSet_expectEmptyList() {
        Set<Id> userIds = new Set<Id>();

        List<User> users = UserSelector.newInstance().selectById(userIds);

        System.assertNotEquals(null, users, 'Expected the users to not be null.');
        System.assert(users.isEmpty(), 'Expected the users to be empty.');
    }

    @isTest
    private static void selectById_userRecordsExist_expectUserReturned() {
        User targetUser = UserTestData.insertFamilyPortalUser();

        Set<Id> userIds = new Set<Id> { targetUser.Id };

        List<User> userRecords = UserSelector.newInstance().selectById(userIds);

        System.assertNotEquals(null, userRecords, 'Expected the user records to not be null.');
        System.assertEquals(1, userRecords.size(), 'Expected the list of records to contain one user.');
        System.assertEquals(targetUser.Id, userRecords[0].Id, 'Expected the inserted user to be queried.');
    }

    @isTest
    private static void selectWithCustomFieldListById_expectRecordReturned() {
        User user = UserTestData.Instance.DefaultUser;

        Test.startTest();
            List<User> userRecords = UserSelector.newInstance().selectByIdWithCustomFieldList(new Set<Id>{user.Id}, new List<String>{'Name'});
        Test.stopTest();

        System.assert(!userRecords.isEmpty(), 'Expected the returned list to not be empty.');
        System.assertEquals(1, userRecords.size(), 'Expected there to be one record returned.');
        System.assertEquals(user.Id, userRecords[0].Id, 'Expected the Ids of the records to match.');
    }

    @isTest
    private static void selectWithCustomFieldListById_expectArgumentNullException() {
        User user = UserTestData.Instance.DefaultUser;

        Test.startTest();
            try {
                UserSelector.newInstance().selectByIdWithCustomFieldList(new Set<Id>{user.Id}, null);

                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, UserSelector.USER_FIELDS_TO_SELECT_PARAM);
            }

            try {
                UserSelector.newInstance().selectByIdWithCustomFieldList(null, new List<String>{'Name'});

                TestHelpers.expectedArgumentNullException();
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, UserSelector.USER_IDS_PARAM);
            }
        Test.stopTest();
    }
}