/**
 * @description This class is used to encapsulate business logic related to Annual_Setting__c records.
 */
public class AnnualSettings extends fflib_SObjectDomain {

    @testVisible private static final String ACADEMIC_YEAR_NAME_PARAM = 'academicYearName';
    @testVisible private static final String SCHOOL_NAME_PARAM = 'schoolName';
    @testVisible private static final String ANNUAL_SETTING_RECORDS_PARAM = 'annualSettingRecords';
    @testVisible private static final String EXISTING_RECORDS_PARAM = 'existingRecords';
    
    public static final String FIELDSET_TO_CARRY_FORWARD = 'CarryForwardOnCreation';
    public static final String FIELDSET_DEADLINES_TO_CARRY = 'DeadlineFieldsSettings';
    public static final String FIELDSET_CURRENT_DEADLINES_TO_CARRY = 'DeadlineFieldsCurrentTaxYear';
    public static final String FIELDSET_PRIOR_DEADLINES_TO_CARRY = 'DeadlineFieldsPriorTaxYear';
    
    public static final String PY_PICKLIST = 'Prior Year Verification Only';
    public static final String CY_PICKLIST = 'Current Year Verification Only';
    public static final String CYANDPY_PICKLIST = 'Prior + Current Year Verification';
    public static final String NO_VERIFICATION_PICKLIST = 'No Verification';

    public static final String PY_VERIFICATION = 'PY';
    public static final String CY_VERIFICATION = 'CY';
    public static final String CYANDPY_VERIFICATION = 'CYANDPY';
    public static final String NO_VERIFICATION = 'NONE';

    // These constants correspond to picklist values of the Email_Reminders_To_Parents__c field.
    public static final String UNSUBMITTED_PFS_REMINDERS_PICKLIST = 'Unsubmitted PFSs only';
    public static final String DOCUMENT_REMINDERS_PICKLIST = 'Missing required documents only';
    public static final String PFS_AND_DOCUMENT_REMINDERS_PICKLIST = 'Missing required documents and unsubmitted PFSs';
    public static final String NO_REMINDERS_PICKLIST = 'Do not send any reminders';

    // These constants are used to check which type of reminders schools send based on the Email_Reminders_To_Parents__c field.
    // We use them by seeing if the selected value contains one these.
    private static final String PFS_REMINDERS = 'Unsubmitted PFSs';
    private static final String DOCUMENT_REMINDERS = 'Missing required documents';

    private static final Map<String, String> docVerificationCodesByPicklistValue = new Map<String, String> {
            PY_PICKLIST => PY_VERIFICATION,
            CY_PICKLIST => CY_VERIFICATION,
            CYANDPY_PICKLIST => CYANDPY_VERIFICATION,
            NO_VERIFICATION_PICKLIST => NO_VERIFICATION
    };

    private Map<String, Annual_Setting__c> recordsByAcademicYearAndSchool;

    /**
     * @description Default constructor which groups the provided records by the keys used to determine which annual
     *              setting record applies to a school for a particular academic year. The keys are created in the
     *              format {ACADEMIC_YEAR_NAME}-{SCHOOL_NAME}.
     */
    private AnnualSettings(List<Annual_Setting__c> annualSettingRecords) {
        super(annualSettingRecords);

        this.recordsByAcademicYearAndSchool = groupSettingsByAcademicYearAndSchool(this.Records);
    }
    
    /**
     * @description Handles logic that should be done before AS are inserted.
     */
    public override void onBeforeInsert() {
        
        Set<Id> schoolIds = new Set<Id>();
        Set<String> academicYearNames = new Set<String>();
        
        for (Annual_Setting__c record : (List<Annual_Setting__c>)this.Records) {
            // SFP-1521 & SFP-1522: We should only carry forward previous settings for Full and Extended schools.
            if (shouldCarryForwardPreviousSettings(record)) {
                
               schoolIds.add(record.School__c);
               academicYearNames.add(record.Academic_Year_Name__c);
            }
            if (shouldCarryForwardPremiumSettings(record)) {		
                schoolIds.add(record.School__c);		
                academicYearNames.add(record.Academic_Year_Name__c);		
            }            
        }
        
        if (!schoolIds.isEmpty()) {
            carryForwardValuesFromPreviousAS(schoolIds, academicYearNames);
        }
    }

    private Boolean shouldCarryForwardPreviousSettings(Annual_Setting__c recordToCheck) {
        return String.isNotBlank(recordToCheck.School_Subscription_Type__c) &&
                (SubscriptionTypes.SUBSCRIPTION_FULL_TYPE.equalsIgnoreCase(recordToCheck.School_Subscription_Type__c) || SubscriptionTypes.EXTENDED_TYPE.equalsIgnoreCase(recordToCheck.School_Subscription_Type__c));
    }
    
    private Boolean shouldCarryForwardPremiumSettings(Annual_Setting__c recordToCheck) {
        return String.isNotBlank(recordToCheck.School_Subscription_Type__c) && (SubscriptionTypes.PREMIUM_TYPE.equalsIgnoreCase(recordToCheck.School_Subscription_Type__c));
    }
    
    @testVisible private Set<String> fieldsFromFieldSet(String fieldSetName) {
        
        Set<String> result = new Set<String>{'Id'};
        for(FieldSetMember f : Schema.SObjectType.Annual_Setting__c.fieldSets.getMap().get(fieldSetName).getFields()) {
            result.add(f.getFieldPath());
        }
        
        return result;
    }
    
    /**
    * @description Copy the values mentioned above from a school's previous annual settings.
    *              The fields to copy are saved in AnnualSettin's fieldsets:
    *              - CarryForwardOnCreation
    *              - DeadlineFieldsSettings
    *              - DeadlineFieldsPriorTaxYear
    *              - DeadlineFieldsCurrentTaxYear
    * @param accounts A list of records that must contain the Annual_Settings__r sublist.
    * @return a list with academic year names, related to the given accounts.
    */
    private void carryForwardValuesFromPreviousAS(Set<Id> schoolIds, Set<String> academicYearNames) {
        
        String fieldType;
        Account thisAccount;
        Annual_Setting__c latestAS;
        
        //1. Retrieve the fieldSets with fields to by carried forward from previous academic years.
        Set<String> fieldsForDeadlines = fieldsFromFieldSet(FIELDSET_DEADLINES_TO_CARRY);
        Set<String> fieldsForCurrentDeadlines = fieldsFromFieldSet(FIELDSET_CURRENT_DEADLINES_TO_CARRY);
        Set<String> fieldsForPriorDeadlines = fieldsFromFieldSet(FIELDSET_PRIOR_DEADLINES_TO_CARRY);
        Set<String> fieldsToCopy = fieldsFromFieldSet(FIELDSET_TO_CARRY_FORWARD);
        
        //2. Group all deadline fields.
        fieldsForDeadlines.addAll(fieldsForCurrentDeadlines);
        fieldsForDeadlines.addAll(fieldsForPriorDeadlines);
        
        //3. Group all fields to query.
        Set<String> allFields = new Set<String>(fieldsToCopy);
        allFields.addAll(fieldsForDeadlines);
        
        //4. Query to find latest AS record related to the newly inserted AS, that triggered this logic.
        List<Account> queryResult = AccountSelector.Instance.selectWithLatestAnnualSetting(schoolIds, allFields);
        
        //5. Map the results by SchoolId. It will helpus to find the matching LatestAS, easily during a loop of multiple AS records.
        Map<Id, Account> latestASBySchoolId = new Map<Id, Account>(queryResult);
        
        //6. Get the list of academic year names, related to the latestASBySchoolId.
        Set<String> latestASAcademicYears = getAcademicYearsFromAnnualSettingInAccount(latestASBySchoolId.values());
        
        //7. Query the RD for the academic year names in latestASBySchoolId.
        RequiredDocumentSelector RequiredDocumentSelectorHandler = RequiredDocumentSelector.newInstance();
        Map<String, List<Required_Document__c>> requiredDocuments = RequiredDocumentSelectorHandler
            .mapBySchoolAcademicYear(schoolIds, latestASAcademicYears);   
        
        for (Annual_Setting__c record : (List<Annual_Setting__c>)this.Records) {

            thisAccount = latestASBySchoolId.get(record.School__c);
            
            if (thisAccount != null && !thisAccount.Annual_Settings__r.isEmpty()) {

                latestAS = thisAccount.Annual_Settings__r[0];
                
                //8. Search Required Documents for previous tax year in latest annual setting.
                String priorTaxYear = GlobalVariables.getPrevDocYearStringFromAcadYearName(latestAS.Academic_Year_Name__c);
                String keySearchPriorRD = RequiredDocumentSelectorHandler.createMapKey(latestAS.School__c, latestAS.Academic_Year_Name__c, priorTaxYear);
                Boolean hasPriorDocuments = requiredDocuments.containsKey(keySearchPriorRD);
                
                //9. Search Required Documents for current tax year in latest annual setting.
                String currentTaxYear = GlobalVariables.getDocYearStringFromAcadYearName(latestAS.Academic_Year_Name__c);
                String keySearchCurrentRD = RequiredDocumentSelectorHandler.createMapKey(latestAS.School__c, latestAS.Academic_Year_Name__c, currentTaxYear);
                Boolean hasCurrentDocuments = requiredDocuments.containsKey(keySearchCurrentRD);

                for(String sfield : allFields) {
                    
                    //10. If it is a deadline field of previous tax year. And there's no RD for that tax year. Then, we do not copy values from latestAS.
                    if (fieldsForPriorDeadlines.contains(sfield) && !hasPriorDocuments) { continue; }
                    
                    //11. If it is a deadline field of current tax year. And there's no RD for that tax year. Then, we do not copy values from latestAS.
                    if (fieldsForCurrentDeadlines.contains(sfield) && !hasCurrentDocuments) { continue; }
                    
                    //12. We copy values from latestAS to this sfield.
                    setValueOrAddYearsToDates(sfield, record, latestAS);
                }

                // SFP-1530: Correctly populate the new PFS/Document reminder checkboxes based on the previous settings Email_Reminder_To_Parents__c picklist value.
                // We will include whether or not the school actually had required documents for current/prior year in the logic so we don't say a school sends prior year doc reminders when they don't actually require any.
                setReminderCheckboxesBasedOnPreviousReminderPicklist(record, latestAS, hasPriorDocuments, hasCurrentDocuments);
            }
        }
    }

    private void setReminderCheckboxesBasedOnPreviousReminderPicklist(Annual_Setting__c settingToUpdate, Annual_Setting__c previousSetting, Boolean requiresPriorYearDocs, Boolean requiresCurrentYearDocs) {
        String previousReminderSettings = previousSetting.Email_Reminders_to_Parents__c;

        // If there isn't a value selected for the legacy reminder settings picklist, don't make any changes.
        if (String.isBlank(previousReminderSettings)) {
            return;
        }

        // If the annual settings are not for the 2018-2019 academic year, this won't be necessary since by that time,
        // schools will be using the new checkboxes and we don't have to worry about the legacy picklist value.
        if (String.isNotBlank(settingToUpdate.Academic_Year_Name__c) &&
                !settingToUpdate.Academic_Year_Name__c.equalsIgnoreCase('2018-2019')) {
            return;
        }

        settingToUpdate.Send_PFS_Reminders__c = previousReminderSettings.containsIgnoreCase(PFS_REMINDERS);
        settingToUpdate.Send_Prior_Year_Document_Reminders__c = previousReminderSettings.containsIgnoreCase(DOCUMENT_REMINDERS) && requiresPriorYearDocs;
        settingToUpdate.Send_Current_Year_Document_Reminders__c = previousReminderSettings.containsIgnoreCase(DOCUMENT_REMINDERS) && requiresCurrentYearDocs;
    }

    /**
    * @description Retrieves the academicYearNames related to the given accounts.
    * @param accounts A list of records that must contain the Annual_Settings__r sublist.
    * @return a list with academic year names, related to the given accounts.
    */
    private Set<String> getAcademicYearsFromAnnualSettingInAccount(List<Account> accounts) {
        
        Set<String> result = new Set<String>();
        
        for (Account a : accounts) {
            
            for (Annual_Setting__c annualSetting : a.Annual_Settings__r) {
                
                result.add(annualSetting.Academic_Year_Name__c);
            }
        }
        
        return result;
        
    }//End:getAcademicYearsFromAnnualSettingInAccount
    
    /**
    * @description If the given value is not null, add one year to Date/Datetime values. Otherwise, we assign the value as such it is in originalAS.
    * @param fieldName of the date/datime value.
    * @param finalAS The record to which we will apply the new values.
    * @param finalAS The record from which we will retrieve the new values to be set to finalAS.
    */
    private void setValueOrAddYearsToDates(String fieldName, Annual_Setting__c finalAS, Annual_Setting__c sourceAS) {
        
        String fieldType = String.ValueOf(Schema.SObjectType.Annual_Setting__c.fields.getMap().get(fieldName).getDescribe().getType());
        
        Boolean valueIsNull = sourceAS.get(fieldName) == null;
        
        if (!valueIsNull && fieldType == 'DATE') {
            
            finalAS.put(fieldName, ((Date)sourceAS.get(fieldName)).addYears(1));
            
        } else if (!valueIsNull && fieldType == 'DATETIME') {
            
            finalAS.put(fieldName, ((Datetime)sourceAS.get(fieldName)).addYears(1));            
        
        } else {//If it is NULL or it's not Date/Datetime.
            
            finalAS.put(fieldName, sourceAS.get(fieldName));  
        }
    }//End:incrementedDatesByOne
    
    public override void onAfterInsert() {
        
        PullForwardRequiredDocuments( (List<Annual_Setting__c>)this.Records);
        
    }

    private Map<String, Annual_Setting__c> groupSettingsByAcademicYearAndSchool(List<Annual_Setting__c> annualSettingRecords) {
        Map<String, Annual_Setting__c> settingsByYearAndSchool = new Map<String, Annual_Setting__c>();

        for (Annual_Setting__c setting : annualSettingRecords) {
            String key = getSettingKey(setting);

            settingsByYearAndSchool.put(key, setting);
        }

        return settingsByYearAndSchool;
    }

    private String getSettingKey(Annual_Setting__c settingRecord) {
        return createSettingKey(settingRecord.Academic_Year_Name__c, settingRecord.School_Name__c);
    }

    private String createSettingKey(String academicYearName, String schoolName) {
        return academicYearName + '-' + schoolName;
    }

    /**
     * @description Gets the document verification mode from the annual setting record which is for the specified academic year and school. If no annual setting record corresponds to the specified values, null is returned.
     * @param academicYearName The name of an academic year (e.g. 2016-2017).
     * @param schoolName The name of a school.
     * @return A verification mode.
     * @throws ArgumentNullException if either parameter is null.
     */
    public String getDocVerificationMode(String academicYearName, String schoolName) {
        ArgumentNullException.throwIfNull(academicYearName, ACADEMIC_YEAR_NAME_PARAM);
        ArgumentNullException.throwIfNull(schoolName, SCHOOL_NAME_PARAM);

        String settingKey = createSettingKey(academicYearName, schoolName);

        Annual_Setting__c settingRecord = this.recordsByAcademicYearAndSchool.get(settingKey);

        if (settingRecord == null) {
            return null;
        }

        return docVerificationCodesByPicklistValue.get(settingRecord.Document_Verification__c);
    }

    @testVisible
    private void updateSDAsToVerify(Map<Id, Annual_Setting__c> oldSettings) {
        Set<String> academicYearNames = new Set<String>();
        Set<Id> schoolIds = new Set<Id>();

        // Populate academicYearNames and school Ids from the settings that have different document verification criteria.
        // This will identify which SDAs need to be updated.
        outputSDAQueryData(oldSettings, academicYearNames, schoolIds);

        List<School_Document_Assignment__c> sdasToUpdate = getSDAsToUpdate(academicYearNames, schoolIds);

        if (sdasToUpdate.isEmpty()) {
            return;
        }

        SchoolDocumentAssignments documentAssignments = SchoolDocumentAssignments.newInstance(sdasToUpdate);

        documentAssignments.setVerificationDetails(this);

        commitOrScheduleSDAUpdate(documentAssignments);
    }

    private void outputSDAQueryData(Map<Id, Annual_Setting__c> oldSettingRecords, Set<String> yearNamesToOutput, Set<Id> schoolIdsToOutput) {
        for (Annual_Setting__c updatedSetting : (List<Annual_Setting__c>)this.Records) {
            Annual_Setting__c oldSetting = oldSettingRecords.get(updatedSetting.Id);

            if (oldSetting == null) {
                continue;
            }

            if (valueHasChanged(Annual_Setting__c.Document_Verification__c, updatedSetting, oldSetting)) {
                yearNamesToOutput.add(updatedSetting.Academic_Year_Name__c);
                schoolIdsToOutput.add(updatedSetting.School__c);
            }
        }
    }

    private Boolean valueHasChanged(Schema.SObjectField fieldToCheck, Annual_Setting__c newRecord, Annual_Setting__c oldRecord) {
        Object newValue = newRecord.get(fieldToCheck);
        Object oldValue = oldRecord.get(fieldToCheck);

        return newValue != oldValue;
    }

    private List<School_Document_Assignment__c> getSDAsToUpdate(Set<String> academicYearNames, Set<Id> schoolIds) {
        // TODO Selector School_Document_Assignment__c
        return [SELECT Name, Verify_Data__c, School_PFS_Assignment__c,
                Academic_Year__c, School_PFS_Assignment__r.School__c, School__c, School_Id__c, Document_Year__c
        FROM School_Document_Assignment__c
        WHERE School_PFS_Assignment__r.School__c IN :schoolIds
        AND Academic_Year__c IN :academicYearNames];
    }

    private void commitOrScheduleSDAUpdate(SchoolDocumentAssignments docAssignments) {
        List<School_Document_Assignment__c> sdaRecords = (List<School_Document_Assignment__c>)docAssignments.Records;

        Integer numberOfRecordsToUpdate = sdaRecords.size();
        Integer schoolPortalSettingThreshold = getSDADmlThreshold();

        // if there are more than 1000 (or whatever the threshold is) and we're not in a future method, schedule a batch.
        if (numberOfRecordsToUpdate > schoolPortalSettingThreshold && !System.isFuture()){
            // [DP] 06.22.2015 NAIS-1755 now using batch job to run insert when above threshold
            BatchGenericUpsert batchUpsert = new BatchGenericUpsert();
            batchUpsert.recordsToUpsert = sdaRecords;
            Database.executeBatch(batchUpsert);
            // otherwise, continue
        } else {
            update sdaRecords;
        }
    }

    private Integer getSDADmlThreshold() {
        return SchoolPortalSettings.Req_Doc_Synchronous_Threshold;
    }
    
    private void PullForwardRequiredDocuments(List<Annual_Setting__c> records) {

        if (!FeatureToggles.isEnabled('Auto_Create_Required_Documents__c')) {
            
            return;
        }
        
        //We create a map with the keys(E.g:"PreviousAcademicYearName-SchoolId" => annualSettingRecord).
        //We will use this map to check which RD must be created from previous academicYear for the newly inserted academicYear.
        Map<String, Annual_Setting__c> recordsByPreviousAcademicYearAndSchoolId = new Map<String, Annual_Setting__c>();

        Set<String> previousAcademicYears = new Set<String>();
        Set<String> currentAcademicYears = new Set<String>();
        Set<Id> schoolIds = new Set<Id>();
        String previousAcademicYear, mapKey;
        String previouspreviousAcademicYear, ppyMapKey;		
        Set<String> documentYearsSet = new Set<String>();
        
        for (Annual_Setting__c record : records) {
            documentYearsSet.add(String.valueOf((Integer.valueOf(record.Academic_Year_Name__c.left(4))-2)));
            documentYearsSet.add(String.valueOf((Integer.valueOf(record.Academic_Year_Name__c.left(4))-3)));
            
            if (shouldCarryForwardPreviousSettings(record)) {
                previousAcademicYear = GlobalVariables.addIntegerToAcademicYearStr(record.Academic_Year_Name__c, -1);
                mapKey = createSettingKey(previousAcademicYear, String.ValueOf(record.School__c));
                recordsByPreviousAcademicYearAndSchoolId.put(mapKey, record);
                previousAcademicYears.add( previousAcademicYear );
                currentAcademicYears.add( record.Academic_Year_Name__c );
                schoolIds.add(record.School__c);
                
            }
            if (shouldCarryForwardPremiumSettings(record)) {
                //SFP-1813 : If the subscriptionType is PREMIUM then also include the Previous Previous Academic Years(for PPY).
                if (SubscriptionTypes.PREMIUM_TYPE.equalsIgnoreCase(record.School_Subscription_Type__c)) {  
                    previousAcademicYear = GlobalVariables.addIntegerToAcademicYearStr(record.Academic_Year_Name__c, -1);
                    mapKey = createSettingKey(previousAcademicYear, String.ValueOf(record.School__c));
                    recordsByPreviousAcademicYearAndSchoolId.put(mapKey, record);
                    previousAcademicYears.add( previousAcademicYear );
                    currentAcademicYears.add( record.Academic_Year_Name__c );
                    schoolIds.add(record.School__c); 

                    documentYearsSet.add(String.valueOf((Integer.valueOf(record.Academic_Year_Name__c.left(4))-4)));
                }
            } 
        }
        
        if (!schoolIds.isEmpty()) {
            createRequiredDocumentsFromPreviousAcademicYear(recordsByPreviousAcademicYearAndSchoolId, currentAcademicYears, previousAcademicYears, schoolIds, documentYearsSet);
        }
    }
    
    /**
     * @description Pull forward the required documents for the given annualSettings. For Example: we have a new annualSetting(AcademicYear = 2017-2018, School=ABC).
     *              Then, we will clone the RequiredDocuments with (AcademicYear = 2016-2017 AND School=ABC). 
     *              And those new RequiredDocument records will contain: 
     *                  newRD.Academic_Year = 2017-2018
     *                  newRD.Document_Year = PreviousRD.DocumentYear - 1
     * @param annualSettingsByPreviousAcademicYearAndSchoolId The formatt of this map is: 
     *                  Key = newAnnualSetting.previousAcademicYearName + '-' + AnnualSetting.SchoolId
     *                  Value = newAnnualSetting
     * @param currentAcademicYears The list of current academic years for the annualSetting records that are being inserted.
     * @param previousAcademicYears The list of previous academic years for the annualSetting records that are being inserted.
     * @param schoolIds The list of "School__c" for the annualSetting records that are being inserted.
     * @param documentYears The list of "Document Years" for the RequiredDocument records which are getting query.
     * @return The required documents that will be created.
     */
    private void createRequiredDocumentsFromPreviousAcademicYear(
        Map<String, Annual_Setting__c> annualSettingsByPreviousAcademicYearAndSchoolId,
        Set<String> currentAcademicYears, 
        Set<String> previousAcademicYears, 
        Set<Id> schoolIds,
        Set<String> documentYears) {
            
        requiredDocumentsByAnnualSetting existingRequiredDocs =  new requiredDocumentsByAnnualSetting(currentAcademicYears, schoolIds);
        
        Map<Id, Required_Document__c> requiredDocuments = new Map<Id, Required_Document__c>();
        Annual_Setting__c currentAnnualSetting;
        Required_Document__c rdClone;
        String academicYearSchoolId;
        
        for (Required_Document__c rd : [
            SELECT Id, Academic_Year__c, Document_Type__c, Document_Year__c, Label_for_School_Specific_Document__c, 
            School__c, SYSTEM_Future_Failure__c, SYSTEM_Future_Method_Failed__c, Academic_Year__r.Name 
            FROM Required_Document__c 
            WHERE  School__c IN: schoolIds 
            AND Academic_Year__r.Name IN :previousAcademicYears
            AND Document_Year__c IN :documentYears
            AND Deleted__c = false]) {
                
            academicYearSchoolId = createSettingKey(rd.Academic_Year__r.Name, rd.School__c);

            if (annualSettingsByPreviousAcademicYearAndSchoolId.containsKey(academicYearSchoolId)) {
                
                currentAnnualSetting = annualSettingsByPreviousAcademicYearAndSchoolId.get(academicYearSchoolId);
                rdClone = rd.clone(false);
                rdClone.Document_Year__c = rd.Document_Year__c != null ? String.ValueOf(Integer.ValueOf(rd.Document_Year__c) + 1) : null;
                rdClone.Academic_Year__c = currentAnnualSetting.Academic_Year__c;
                rdClone.SYSTEM_Future_Method_Failed__c = false;
                rdClone.SYSTEM_Future_Failure__c = '';

                if (!existingRequiredDocs.contains(rdClone)) {
                    requiredDocuments.put(rd.Id, rdClone);
                }
            }
        }
        
        if (!requiredDocuments.isEmpty()) {
                
            insert requiredDocuments.Values();
            
            insertAttachmentsForRequiredDocuments(requiredDocuments);
        }
    }
    
    /**
    * @description Pull forward attachment__c records from required documents of previous academic years.
    *              Notice that each Attachment__c records contains one related Attachment record that represents a file.
    * @param newlyDocsByOrigDocId Mapped by original requiredDocument Id, from which the newly requiredDocument was cloned.
    *                             The values in this map are the newly inserted requiredDocument records.
    */
    private void insertAttachmentsForRequiredDocuments(Map<Id, Required_Document__c> newlyDocsByOrigDocId) {
        
        Map<Id, Attachment__c> newParentAttachment = new Map<Id, Attachment__c>();
        Attachment__c clonedParentAttachment;
        
        for (Attachment__c a : [
            SELECT Id, Attachment__c, Deleted__c, Parent_ID__c, Parent_Type__c 
            FROM Attachment__c 
            WHERE Parent_ID__c IN: newlyDocsByOrigDocId.keyset() 
            AND Deleted__c <> true]) {
            
            clonedParentAttachment = a.clone(false);
            clonedParentAttachment.Parent_ID__c = (newlyDocsByOrigDocId.get(a.Parent_ID__c)).Id;//The newly inserted requiredDocument Id.
            newParentAttachment.put(a.Id, clonedParentAttachment);
        }
        
        
        if (!newParentAttachment.isEmpty()) {
            
            insert newParentAttachment.Values();
            
            insertAttachmentsNewlyRequiredDocuments(newParentAttachment);
        }
    }
    
    /**
    * @description Pull forward attachment records from required documents of previous academic years.
    * @param newParentAttachment Mapped by original requiredDocument Id, from which the newly requiredDocument was cloned.
    *                            The values in this map are the newly inserted Attachment__c records.
    */
    private void insertAttachmentsNewlyRequiredDocuments(Map<Id, Attachment__c> newParentAttachment) {
        
        Attachment__c parentAttachment;
        Attachment clonedAttachment;
        List<Attachment> attachmentsToInsert = new List<Attachment>();
        
        for (Attachment a : [
            SELECT Id, Name, ParentId, Body, IsPrivate 
            FROM Attachment 
            WHERE ParentId IN: newParentAttachment.keySet() ]) {
            
            parentAttachment = newParentAttachment.get(a.ParentId);
            
            if (parentAttachment != null) {
                
                clonedAttachment = a.clone(false);
                clonedAttachment.ParentId = parentAttachment.Id;
                attachmentsToInsert.add(clonedAttachment);
            }
        }
        
        if (!attachmentsToInsert.isEmpty()) {
            
            insert attachmentsToInsert;
        }
    }
    
    /**
     * @description Helper  to retrieve the related required documents for the given academicYear/SchoolId of an AnnualSetting.
     */
    public class requiredDocumentsByAnnualSetting {
        
        public Map<String, Set<String>> result { get; set; }
        
        /**
         * @description Constructor that maps the related required documents for the given lists of AnnualSetting.academicYear/AnnualSetting.SchoolId.
         */
        public requiredDocumentsByAnnualSetting(Set<String> academicYearNames, Set<Id> schoolIds) {
            
            String annualSettingKey, requiredDocumentKey;
            Set<String> tmpSet;
            result = new Map<String, Set<String>>();
            
            for (Required_Document__c rd : [
                SELECT Id, Academic_Year__r.Name, Document_Type__c, Document_Year__c, School__c 
                FROM Required_Document__c 
                WHERE School__c IN: schoolIds 
                AND Academic_Year__r.Name IN :academicYearNames 
                AND Deleted__c = false]) {
               
               annualSettingKey = rd.Academic_Year__c + '_' + rd.School__c;
               requiredDocumentKey = getRequiredDocumentKey(rd);
               
               if (result.containsKey(annualSettingKey)) {
                   tmpSet = result.get(annualSettingKey);
               } else {
                   tmpSet = new Set<String>();
               }
               
               tmpSet.add(requiredDocumentKey);
               result.put(annualSettingKey, tmpSet);
            }
        }
        
        private String getRequiredDocumentKey(Required_Document__c rd) {
        
            return rd != null ? rd.Document_Type__c + '_' + rd.Document_Year__c + '_' + rd.Academic_Year__c : '';
        }
        
        /**
        * @description Returns true if the given required document already exists. Otherwise, false.
        */
        public Boolean contains(Required_Document__c rd) {
            
            String annualSettingKey = rd.Academic_Year__c + '_' + rd.School__c;
            String requiredDocumentKey = getRequiredDocumentKey(rd);

            return result.containsKey(annualSettingKey) && result.get(annualSettingKey).contains(requiredDocumentKey);
        }
    }//End-Helper:requiredDocumentsByAnnualSetting

    /**
     * @description Creates a new instance of the AnnualSettings domain class from the specified records.
     * @param annualSettingRecords The records to create a new domain class for.
     * @return An instance of the AnnualSettings domain class.
     */
    public static AnnualSettings newInstance(List<Annual_Setting__c> annualSettingRecords) {
        ArgumentNullException.throwIfNull(annualSettingRecords, ANNUAL_SETTING_RECORDS_PARAM);
        return new AnnualSettings(annualSettingRecords);
    }
}