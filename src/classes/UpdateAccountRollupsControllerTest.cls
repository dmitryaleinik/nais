/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class UpdateAccountRollupsControllerTest
{

    @isTest
    private static void testAccountRollups()
    {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User portalUser;
        Account school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, 5, false);
        school1.Alert_Frequency__c = 'Weekly digest of new applications';
        school1.SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest();
        school1.SSS_School_Code__c = '11111';
        insert school1;

        Account family = TestUtils.createAccount('Individual Account', RecordTypes.individualAccountTypeId, 5, true);

        Contact parentA = TestUtils.createContact('Parent A', family.Id, RecordTypes.parentContactTypeId, false);
        parentA.Email = 'parenta@test.org';
        insert parentA;

        System.runAs(thisUser){
        portalUser = TestUtils.createPortalUser(parentA.LastName, parentA.Email + String.valueOf(Math.random()) +
            String.valueOf(Math.random()), 'alias', parentA.Id, GlobalVariables.familyPortalProfileId , true, true);
        }

        UpdateAccountRollupsController urc = new UpdateAccountRollupsController();
        urc.updateAccountRollups();
        Account accnt = [select Id,  Most_Recent_PFS_Count__c, Most_Recent_wPFS_Count__c, Prior_Year_PFS_Count__c,
                              Prior_Year_wPFS_Count__c from Account where Id = :school1.Id];

        System.assertEquals(accnt.Most_Recent_PFS_Count__c, null);
        System.assertEquals(accnt.Prior_Year_PFS_Count__c, null);
        System.assertEquals(accnt.Most_Recent_wPFS_Count__c, null);
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        Test.startTest();
        Annual_Setting__c annualSetting = TestUtils.createAnnualSetting(school1.Id, academicYearId, true);
        urc.updateAccountRollups();
        accnt = [select Id,  Most_Recent_PFS_Count__c, Most_Recent_wPFS_Count__c, Prior_Year_PFS_Count__c,
                              Prior_Year_wPFS_Count__c from Account where Id = :school1.Id];

        System.assertEquals(accnt.Most_Recent_PFS_Count__c, null);
        System.assertEquals(accnt.Prior_Year_PFS_Count__c, null);
        System.assertEquals(accnt.Most_Recent_wPFS_Count__c, null);

        PFS__c pfs1;
        System.runAs(portalUser){
        pfs1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
        pfs1.Original_Submission_Date__c = (Datetime)System.today().addDays(-3);
        pfs1.PFS_Status__c = 'Application Submitted';
        pfs1.Payment_Status__c = 'Unpaid';
        insert pfs1;
        }

        Weighted_PFS__c wpfs = new Weighted_PFS__c(Annual_Setting__c=annualSetting.Id, PFS__c=pfs1.Id, School_Count__c=3);
        Weighted_PFS__c wpfs1 = new Weighted_PFS__c(Annual_Setting__c=annualSetting.Id, PFS__c=pfs1.Id, School_Count__c=2);
        insert new list<Weighted_PFS__c>{wpfs,wpfs1};

        urc.updateAccountRollups();
        accnt = [select Id,  Most_Recent_PFS_Count__c, Most_Recent_wPFS_Count__c, Prior_Year_PFS_Count__c,
                              Prior_Year_wPFS_Count__c from Account where Id = :school1.Id];

        System.assertEquals(2, accnt.Most_Recent_PFS_Count__c);
        System.assertEquals(null, accnt.Prior_Year_PFS_Count__c);
        System.assertEquals(0.8333333, accnt.Most_Recent_wPFS_Count__c);
    }
}