/**
 * @description This class is used to create Global Message records for unit tests.
 */
@isTest
public class GlobalMessageTestData extends SObjectTestData
{

    private static final String MESSAGE_NAME = 'Test Global Message';
    private static final String MESSAGE_TEXT = 'Test Global Message Text';
    private static final String MESSAGE_PORTAL = GlobalVariables.GM_PORTAL_FAMILY_ALL_PAGES;

    /**
     * @description Get the default values for the Global Message object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() 
    {
        return new Map<Schema.SObjectField,Object>
            {
                Global_Message__c.Name => MESSAGE_NAME,
                Global_Message__c.Display_Start_Date_Time__c => System.now().addDays(-3),
                Global_Message__c.Display_End_Date_Time__c => System.now().addDays(3),
                Global_Message__c.Message__c => MESSAGE_TEXT,
                Global_Message__c.Portal__c => MESSAGE_PORTAL
            };
    }

    /**
     * @description Set the Name field on the current Global Message record.
     * @param name The Name to set on the Global_Message__c.
     * @return The current working instance of GlobalMessageTestData.
     */
    public GlobalMessageTestData forName(String name) {
        return (GlobalMessageTestData) with(Global_Message__c.Name, name);
    }

    /**
     * @description Set the Portal field on the current Global Message record.
     * @param portal The visibility/portal type to set on the Global_Message__c.
     * @return The current working instance of GlobalMessageTestData.
     */
    public GlobalMessageTestData forPortal(String portal) {
        return (GlobalMessageTestData) with(Global_Message__c.Portal__c, portal);
    }

    /**
     * @description Set the Page Name field on the current Global Message record.
     * @param pageName The name of apex page or screen name to set on the Global_Message__c.
     * @return The current working instance of GlobalMessageTestData.
     */
    public GlobalMessageTestData forPageName(String pageName)
    {
        return (GlobalMessageTestData) with(Global_Message__c.Page_Name__c, pageName);
    }

    /**
     * @description Set the Academic Year field on the current Global Message record.
     * @param academicYear The name of academic year to set on the Global_Message__c.
     * @return The current working instance of GlobalMessageTestData.
     */
    public GlobalMessageTestData forAcademicYear(String academicYear)
    {
        return (GlobalMessageTestData) with(Global_Message__c.Academic_Year__c, academicYear);
    }

    /**
     * @description Set the Display Start Date Time field on the current Global Message record.
     * @param displayStartDateTime The display start date time to set on the Global_Message__c.
     * @return The current working instance of GlobalMessageTestData.
     */
    public GlobalMessageTestData forDisplayStartDateTime(Datetime displayStartDateTime)
    {
        return (GlobalMessageTestData) with(Global_Message__c.Display_Start_Date_Time__c, displayStartDateTime);
    }

    /**
     * @description Set the Display End Date Time field on the current Global Message record.
     * @param displayEndDateTime The display end date time to set on the Global_Message__c.
     * @return The current working instance of GlobalMessageTestData.
     */
    public GlobalMessageTestData forDisplayEndDateTime(Datetime displayEndDateTime)
    {
        return (GlobalMessageTestData) with(Global_Message__c.Display_End_Date_Time__c, displayEndDateTime);
    }

    /**
     * @description Insert the current working Global Message record.
     * @return The currently operated upon Global Message record.
     */
    public Global_Message__c insertGlobalMessage() {
        return (Global_Message__c)insertRecord();
    }

    /**
     * @description Create the current working Global Message record without resetting
     *              the stored values in this instance of GlobalMessageTestData.
     * @return A non-inserted Global Message record using the currently stored field values.
     */
    public Global_Message__c createGlobalMessageWithoutReset() {
        return (Global_Message__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Global Message record.
     * @return The currently operated upon Global Message record.
     */
    public Global_Message__c create() {
        return (Global_Message__c)super.buildWithReset();
    }

    /**
     * @description The default Global Message record.
     */
    public Global_Message__c DefaultGlobalMessage {
        get {
            if (DefaultGlobalMessage == null) {
                DefaultGlobalMessage = createGlobalMessageWithoutReset();
                insert DefaultGlobalMessage;
            }
            return DefaultGlobalMessage;
        }
        private set;
    }

    /**
     * @description Get the Global Message SObjectType.
     * @return The Global Message SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Global_Message__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static GlobalMessageTestData Instance {
        get {
            if (Instance == null) {
                Instance = new GlobalMessageTestData();
            }
            return Instance;
        }
        private set;
    }

    private GlobalMessageTestData() {}
}