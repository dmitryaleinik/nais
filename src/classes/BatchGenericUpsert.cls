global class BatchGenericUpsert implements Database.Batchable<sObject> {
    
    global List<SOBJECT> recordsToUpsert { get; set; }
    
    global BatchGenericUpsert() {
        
    }
    
    global List<SOBJECT> start(Database.BatchableContext bc) {
        return recordsToUpsert;
    }

    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        Database.UpsertResult[] ur = Database.upsert(scope, false);
        for (Database.UpsertResult result : ur){
            System.debug(LoggingLevel.ERROR, 'Upsert failed: ' + result);
        }
    }
    
    global void finish(Database.BatchableContext bc) {
    }
    
}