/**
 * Created by hansen on 4/8/16.
 */

public with sharing class FamilySSOLogin {

    public String getCommunityUrl() {
        //return Site.getBaseSecureUrl();
        return Auth.AuthConfiguration.getAuthProviderSsoUrl(null, '/FamilyDashboard', 'FamilyGithub');
    }
    public PageReference loginWithGithub() {

        String ssoUrl = Auth.AuthConfiguration.getAuthProviderSsoUrl(null, '/FamilyDashboard', 'FamilyGithub');
        System.debug(ssoUrl);
        return new PageReference(ssoUrl);
    }

    public PageReference loginWithFacebook() {

        String ssoUrl = Auth.AuthConfiguration.getAuthProviderSsoUrl(null, '/FamilyDashboard', 'familyfacebook');
        System.debug(ssoUrl);
        return new PageReference(ssoUrl);
    }

    public PageReference loginWithRavenna() {

        String ssoUrl = Auth.AuthConfiguration.getAuthProviderSsoUrl(null, '/FamilyDashboard', 'ravenna');
        System.debug(ssoUrl);
        return new PageReference(ssoUrl);
    }
}