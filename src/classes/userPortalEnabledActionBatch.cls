/*
 * @description Scheduled batch Apex to disable Portal Access to Inactive school portal users:
 *              1. Remove portal access to Inactive users marked as IsPortalEnabled=true.
 *              2. Create an individual 
 */
global class userPortalEnabledActionBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    global SchoolUserAdminController.individualAccountForScoolStaffContact handler;
    global Set<Id> schoolStaffIds;
    global String query;
    
    global userPortalEnabledActionBatch() {
        
        //0. This is called from the "scheduledDisablePortalAccess" and is executed hourly.
        handler = new SchoolUserAdminController.individualAccountForScoolStaffContact();
        query =  'SELECT Id, IsPortalEnabled, IsActive, ContactId, Contact.RecordTypeId, Contact.AccountId, Contact.Deleted__c ' + 
                 'FROM User WHERE IsActive=false AND ContactId!=null AND IsPortalEnabled=true';
    }
    
    global userPortalEnabledActionBatch(Set<Id> contactIds) {
        
        //0. This is used create the individual account for school contacts without portal access (without related active user)
        schoolStaffIds = contactIds;
        handler = new SchoolUserAdminController.individualAccountForScoolStaffContact();
        query = 'SELECT Id, RecordTypeId, AccountId, Deleted__c FROM Contact WHERE Id IN: schoolStaffIds';
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        
        List<User> users = new List<User>();
        Set<Id> staffIds = new Set<Id>();
        User tmpUser;
        
        if (scope instanceof List<User>) {
            
            //1. Disable Portal Access to Users: this method execute a dml operation over user object records. 
            //   And get its related contactIds, to be used in next steps.
            staffIds = SchoolUserAdminProfileController.disablePortalAccessForUsers( (List<User>)scope );
            
        } else if (scope instanceof List<Contact>) {
            
            //1. Get no portal access contact's Ids to be processed. And create an individual account for each contact related to a school. 
            Map<Id, sObject> contacts = new Map<Id, SObject>(scope);
            staffIds = contacts.keySet();
        }
        
        //2. Create (without dml insert) individual account for each inactive school contact.
        handler.createIndividualAccountForContacts(staffIds);
    }
    
    global void finish(Database.BatchableContext bc) {
        
        //3. Run a dml insert for the accounts created on step2. This is done on "finish", to avoid conflicts with the 
        //   update made on "execute" for user object recors. That's the reason why we use stateful anotation.
        //   The exception we are trying to avoid is: Insert failed. First exception on row 0; first error: 
        //   MIXED_DML_OPERATION, DML operation on setup object is not permitted after you have updated a non-setup object (or vice versa): Account, original object: User: []
        handler.insertAccounts();
        
        //4. Once the accounts are created we can link each contact with its own individual account.
        handler.upsertContactWithOwnIndividualAccount();
    }
    
}