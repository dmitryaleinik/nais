/**
 * RavennaIntegrationPostProcess
 *
 * @description Used to handle the responses returned by the Ravenna API.
 *
 */
public class RavennaIntegrationPostProcess implements IntegrationApiModel.IAction {

    private final String STUDENT_ID_FIELD = 'id';
    private static final String SCHOOL_API = 'Schools';
    private final String STUDENTS_API = 'Students';
    private final String RESPONSE_GENDER_FIELD = 'gender';
    private final String RESPONSE_APPLY_GRADE_FIELD = 'apply_grade_default'; // TODO:: CHANGE THIS TO 'apply_grade'
    private static final String APPLICATIONS_API = 'StudentApplications';
    private static final String CURRENT_GRADE_RESPONSE_FIELD = 'current_grade';
    private static final String ACCOUNT_EXTERNAL_ID_FIELD = 'RavennaId__c';

    public RavennaIntegrationPostProcess() {}

    public void invoke(IntegrationApiModel.ApiState apiState) {

        // We have these if statements to check the current API but it isn't necessary. Instead we could just create
        // individual classes for each if statement and specify those classes on the different Integration API Setting
        // custom metadata records. That way, changes for one step of the integration are less likely to affect other
        // parts of the integration.
        if(apiState.currentApi == this.STUDENTS_API) {
            // This updates the parameters for the next API (Student Applications) so it includes the school Id and the academic year.
            // We need to do this ourselves instead of using the framework because the ravenna student response doesn't
            // include the academic year so we need to get it from the PFS and make sure it gets passed to the next endpoint.
            this.findStudents( apiState);

            // Populate the applicant objects using the results from the students API.
            // This will populate the apiState.familyModel.studentIdToApplicantMap with the Applicant records
            // by the external Id configured for the integration. In this case, RavennaId__c
            IntegrationUtils.populateApplicantObjects(apiState, this.STUDENT_ID_FIELD, this.STUDENTS_API);

            // Iterate over the applicant records we have generated and convert the values used for current grade and entry grade.
            // Some of Ravenna's grade values are different so we need to make sure we don't try to upsert applicant
            // records with the wrong picklist values.
            this.populateFields(apiState);
        }

        // Then later on, instead of going from the student response to create the folders/PFS Assignments, we will use the actual applications...
        if (apiState.currentApi == APPLICATIONS_API) {
            // Group Student Application responses by student external Id (The student Id in the ravenna system).
            // This will be used to insert an application to each school that the student applied to.
            groupApplicationsByStudentExternalId(apiState);
        }

        if(apiState.currentApi == SCHOOL_API) {
            // Post processing for the schools API has many steps noted below. For more details, check out the processSchoolsResponse() method.
            // Step 1: Get all the external Ids and NCES Ids we have for the schools included in the response.
            // Step 2: Query for current subscriber School Accounts using the external Ids and NCES Ids.
            // Step 3: Make sure we have at least one current subscriber school. If not, throw exception.
            // Step 4: Make sure each school has annual settings. If some of them need to be inserted, we should insert them in bulk instead of one at a time.
            // Step 5: Iterate over the applications by student Id. If a student does not have an application for any
            //         current SSS subscriber, remove them from the map of applicant records to insert.
            //         As we iterate, populate the school_id field with the Account record Id.
            processSchoolsResponse(apiState);
        }
    }

    private static void processSchoolsResponse(IntegrationApiModel.ApiState apiState) {
        // if we didn't get any school responses, make sure we have an empty list to process.
        List<Object> rawSchoolResponses = apiState.httpResponseByApi.get(SCHOOL_API);
        if (rawSchoolResponses == null) {
            rawSchoolResponses = new List<Object>();
        }

        // Get the school models collection class by specifying the external Id field to use on the account records.
        // This class will be used to ensure we have at least one school account with a current SSS subscription for this family.
        // We use this class by feeding it the nces Id and external Id from the schools API response.
        // It will then query for the schools with those values.
        // TODO SFP-1686 We might want to get the external Id field to use via Integration Mapping records.
        // TODO SFP-1686 We could make the new instance method create the student models by passing in the schools API response...
        //      account External Id field, and the mappings to use for the Id and nces Id on the response.
        SchoolIntegrationModels schoolModels = apiState.familyModel.getSchoolModels(ACCOUNT_EXTERNAL_ID_FIELD);

        for (Object rawSchoolResponse : rawSchoolResponses) {
            Map<String, Object> schoolResponse = (Map<String, Object>)rawSchoolResponse;

            String externalId = String.valueOf(schoolResponse.get('id'));

            String ncesId = (String)schoolResponse.get('nces_id');

            schoolModels.addSchool(ncesId, externalId);
        }

        // Require that there is at least one current subscriber school. If we couldn't find one based on the external
        // Ids and ncesIds, then we throw an integration exception. This will display a message to the family user in the family portal.
        schoolModels.requireCurrentSubscriberSchools();

        // For each of the current subscriber schools, make sure they have annual settings. We do this by iterating over the schools and
        // calling GlobalVariables.getCurrentAnnualSettings() This is not bulkified at all so we do SOQL/DML in a for loop here.
        // This should be refactored to handle this in bulk.
        IntegrationUtils.ensureSchoolsHaveAnnualSettings(apiState, schoolModels.getCurrentSubscriberSchoolsById().values());

        // Be sure to populate the schoolIdsToAccountMap on the ApiState otherwise we won't be able to find the accounts
        // we need when creating the student folders and SPAs in the IntegrationUtils.handleIntegrationDML() method.
        apiState.familyModel.schoolIdsToAccountMap = (Map<String, sObject>)schoolModels.getCurrentSubscriberSchoolsById();

        // Now iterate over the student applications responses for each student and gather the school external Ids for each student.
        // We will use the nces Id if available, otherwise we will fall back to the external Id.
        for (String studentId : apiState.familyModel.ApplicationResponsesByStudentId.keySet()) {
            List<Object> applicationsForStudent = apiState.familyModel.ApplicationResponsesByStudentId.get(studentId);
            // Once we find a current subscriber school for this applicant we set this to true. If we don't find any
            // current subscriber schools for this applicant, then we remove them from apiState.familyModel.studentIdToApplicantMap
            Boolean isApplyingToCurrentSubscriberSchool = false;

            for (Object studentApplication : applicationsForStudent) {
                Map<String, Object> application = (Map<String, Object>)studentApplication;

                String schoolExternalId = String.valueOf(application.get('school_id'));
                if (String.isBlank(schoolExternalId)) {
                    continue;
                }

                Id schoolAccountId = schoolModels.getSchoolAccountId(schoolExternalId);

                if (schoolAccountId != null) {
                    application.put('school_id', schoolAccountId);
                    isApplyingToCurrentSubscriberSchool = true;
                }
            }

            // If we didn't find a single subscriber school for all of this applicants applications, we remove them from the map.
            // We only insert applicants that would have financial aid applications for SSS subscribers.
            if (!isApplyingToCurrentSubscriberSchool) {
                apiState.familyModel.studentIdToApplicantMap.remove( studentId);
            }
        }
    }

    private static void groupApplicationsByStudentExternalId(IntegrationApiModel.ApiState apiState) {
        // Get each set of params used to call the student applications API. The params will be in the format student-id:academic-year.
        // We will split each set of params on ":" to get the student Id. We will then grab the responses for each set of params and group them by the student Id.
        // This will allow us to retrieve the applications we should be inserting for each student by student Id.
        for (String params : apiState.httpParamsByApi.get(APPLICATIONS_API)) {
            if (String.isBlank(params)) {
                continue;
            }

            List<String> paramValues = params.split(':');
            if (paramValues.isEmpty()) {
                continue;
            }

            String studentId = paramValues[0];

            List<Object> rawApplicationsForStudent = apiState.httpResponseByParamMap.get(params);
            if (rawApplicationsForStudent == null) {
                continue;
            }

            apiState.familyModel.ApplicationResponsesByStudentId.put(studentId, rawApplicationsForStudent);
        }
    }

    private void populateFields( IntegrationApiModel.ApiState apiState) {
        // Loop over Student HTTP response and get apply_grade field.
        for ( String studentId : apiState.familyModel.studentIdToApplicantMap.keySet()) {
            List<Object> studentResponse = apiState.familyModel.studentIdToResponseMap.get(studentId);
            for (Object student : studentResponse) {
                Map<String, Object> studentMap = (Map<String, Object>) student;
                
                // Convert the grade values from ravenna so they are valid SSS picklist values.
                String entryGrade = RavennaIntegrationUtils.convertGradeToPicklist(this.RESPONSE_APPLY_GRADE_FIELD, (String)studentMap.get(this.RESPONSE_APPLY_GRADE_FIELD));
                String currentGrade = RavennaIntegrationUtils.convertGradeToPicklist(CURRENT_GRADE_RESPONSE_FIELD, (String)studentMap.get(CURRENT_GRADE_RESPONSE_FIELD));
                
                //Convert the gender value from ravena so it is a valid SSS picklist value.
                String gender = RavennaIntegrationUtils.convertGenderToPicklist((String)studentMap.get(RESPONSE_GENDER_FIELD));
                
                SObject studentApplicant = apiState.familyModel.studentIdToApplicantMap.get(studentId);

                // Set the converted grade values on the applicant record that will be upserted for this student.
                studentApplicant.put('Grade_In_Entry_Year__c', entryGrade);
                studentApplicant.put('Current_Grade__c', currentGrade);
                studentApplicant.put('Gender__c', gender);
            }
        }
    }

    private void findStudents( IntegrationApiModel.ApiState apiState) {

        // Now we need to set the admissionyear Param
        String admissionYear = IntegrationUtils.getAdmissionYear( apiState.pfs);

        List<String> params = new List<String>();
        for( Object response : apiState.httpResponseByApi.get( apiState.currentApi)) {

            Map<String, Object> values = (Map<String, Object>)response;
            String studentIdKey = String.valueOf( values.get( this.STUDENT_ID_FIELD));
            params.add( studentIdKey + ':' + admissionYear);
        }

        IntegrationApiModel apiModel = apiState.integrationModelByNameMap.get( apiState.currentApi);
        if( String.isNotBlank( apiModel.nextApiName)) {

            apiState.httpParamsByApi.put( apiModel.nextApiName, params);
        }
    }
}