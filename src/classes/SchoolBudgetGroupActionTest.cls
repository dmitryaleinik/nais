@isTest
private class SchoolBudgetGroupActionTest 
{
    
    private static User thisUser = UserSelector.newInstance().selectById(new Set<Id>{UserInfo.getUserId()})[0];
    private static final String STRING_NEW = 'New';
    private static final String STRING_YES = 'Yes';
    private static Set<Id> budgetIds;

    @isTest
    private static void testInsertAndUpdate(){
        createTestData();
        List<Budget_Group__c> budgetGroups = BudgetGroupSelector.newInstance().selectSObjectsById(budgetIds);
        Budget_Group__c budget1 = budgetGroups[0];
        Budget_Group__c budget2 = budgetGroups[1];
        List<Student_Folder__c> studentFolders = StudentFolderSelector.newInstance().selectAll();
        Student_Folder__c studentFolder1 = studentFolders[0];
        Student_Folder__c studentFolder2 = studentFolders[1];

        Test.startTest();

            List<Budget_Allocation__c> testBudgAllocs = new List<Budget_Allocation__c>();
            testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget1.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder1.Id));
            testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget1.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder1.Id));
            testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget1.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder2.Id));
            testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget2.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder1.Id));
            
            upsert testBudgAllocs;
            
            System.assertEquals(3000, [Select Id, Total_Allocated__c from Budget_Group__c where Id = :budget1.Id limit 1][0].Total_Allocated__c);
            System.assertEquals(1000, [Select Id, Total_Allocated__c from Budget_Group__c where Id = :budget2.Id limit 1][0].Total_Allocated__c);
            
            for (Budget_Allocation__c ba : testBudgAllocs){
                ba.Amount_Allocated__c = 2000;
            }
                    
            update testBudgAllocs;
        Test.stopTest();
            
        System.assertEquals(6000, [Select Id, Total_Allocated__c from Budget_Group__c where Id = :budget1.Id limit 1][0].Total_Allocated__c);
        System.assertEquals(2000, [Select Id, Total_Allocated__c from Budget_Group__c where Id = :budget2.Id limit 1][0].Total_Allocated__c);
    }

    @isTest
    private static void testDeleteAndUndelete(){
        createTestData();
        List<Budget_Group__c> budgetGroups = BudgetGroupSelector.newInstance().selectSObjectsById(budgetIds);
        Budget_Group__c budget1 = budgetGroups[0];
        Budget_Group__c budget2 = budgetGroups[1];
        List<Student_Folder__c> studentFolders = StudentFolderSelector.newInstance().selectAll();
        Student_Folder__c studentFolder1 = studentFolders[0];
        Student_Folder__c studentFolder2 = studentFolders[1];
        
        Test.startTest();
            List<Budget_Allocation__c> testBudgAllocs = new List<Budget_Allocation__c>();
            testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget1.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder1.Id));
            testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget1.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder1.Id));
            testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget1.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder2.Id));
            testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget2.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder1.Id));
            
            upsert testBudgAllocs;
            
            System.assertEquals(3000, [Select Id, Total_Allocated__c from Budget_Group__c where Id = :budget1.Id limit 1][0].Total_Allocated__c);
            System.assertEquals(1000, [Select Id, Total_Allocated__c from Budget_Group__c where Id = :budget2.Id limit 1][0].Total_Allocated__c);

            delete new List<Budget_Allocation__c> {testBudgAllocs[0], testBudgAllocs[3]};
            
            System.assertEquals(2000, [Select Id, Total_Allocated__c from Budget_Group__c where Id = :budget1.Id limit 1][0].Total_Allocated__c);
            System.assertEquals(0, [Select Id, Total_Allocated__c from Budget_Group__c where Id = :budget2.Id limit 1][0].Total_Allocated__c);
            
            undelete new List<Budget_Allocation__c> {testBudgAllocs[0], testBudgAllocs[3]};
            
        Test.stopTest();
        
        System.assertEquals(3000, [Select Id, Total_Allocated__c from Budget_Group__c where Id = :budget1.Id limit 1][0].Total_Allocated__c);
        System.assertEquals(1000, [Select Id, Total_Allocated__c from Budget_Group__c where Id = :budget2.Id limit 1][0].Total_Allocated__c);
    }

    @isTest
    private static void testFolderStatusUpdate1(){
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
        
        createTestData();
        List<Budget_Group__c> budgetGroups = BudgetGroupSelector.newInstance().selectSObjectsById(budgetIds);
        Budget_Group__c budget1 = budgetGroups[0];
        Budget_Group__c budget2 = budgetGroups[1];
        List<Student_Folder__c> studentFolders = StudentFolderSelector.newInstance().selectAll();
        Student_Folder__c studentFolder1 = studentFolders[0];
        Student_Folder__c studentFolder2 = studentFolders[1];
        
        List<Budget_Allocation__c> testBudgAllocs = new List<Budget_Allocation__c>();
        testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget1.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder1.Id));
        testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget1.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder1.Id));
        testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget1.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder2.Id));
        testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget2.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder1.Id));
        
        Test.startTest();
            upsert testBudgAllocs;
            
            System.assertEquals(3000, [Select Id, Total_Allocated__c from Budget_Group__c where Id = :budget1.Id limit 1][0].Total_Allocated__c);
            System.assertEquals(1000, [Select Id, Total_Allocated__c from Budget_Group__c where Id = :budget2.Id limit 1][0].Total_Allocated__c);

            studentFolder1.Folder_Status__c = 'Complete, Award Denied';
            update studentFolder1;
            
            System.assertEquals(1000, [Select Id, Total_Allocated__c from Budget_Group__c where Id = :budget1.Id limit 1][0].Total_Allocated__c);
            System.assertEquals(0, [Select Id, Total_Allocated__c from Budget_Group__c where Id = :budget2.Id limit 1][0].Total_Allocated__c);
        
            studentFolder2.Folder_Status__c = 'Complete, Award Refused';
            update studentFolder2;
        Test.stopTest();
        
        System.assertEquals(0, [Select Id, Total_Allocated__c from Budget_Group__c where Id = :budget1.Id limit 1][0].Total_Allocated__c);
        System.assertEquals(0, [Select Id, Total_Allocated__c from Budget_Group__c where Id = :budget2.Id limit 1][0].Total_Allocated__c);
    }

    @isTest
    private static void testFolderStatusUpdate2(){
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
        
        createTestData();
        List<Budget_Group__c> budgetGroups = BudgetGroupSelector.newInstance().selectSObjectsById(budgetIds);
        Budget_Group__c budget1 = budgetGroups[0];
        Budget_Group__c budget2 = budgetGroups[1];
        List<Student_Folder__c> studentFolders = StudentFolderSelector.newInstance().selectAll();
        Student_Folder__c studentFolder1 = studentFolders[0];
        Student_Folder__c studentFolder2 = studentFolders[1];
        
        Test.startTest();
            List<Budget_Allocation__c> testBudgAllocs = new List<Budget_Allocation__c>();
            testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget1.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder1.Id));
            testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget1.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder1.Id));
            testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget1.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder2.Id));
            testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget2.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder1.Id));
            
            upsert testBudgAllocs;

            studentFolder1.Folder_Status__c = 'Submitted';
            studentFolder2.Folder_Status__c = 'Complete, Award Refused';
            update new List<Student_Folder__c> {studentFolder1, studentFolder2};
        Test.stopTest();
        
        System.assertEquals(2000, [Select Id, Total_Allocated__c from Budget_Group__c where Id = :budget1.Id limit 1][0].Total_Allocated__c);
        System.assertEquals(1000, [Select Id, Total_Allocated__c from Budget_Group__c where Id = :budget2.Id limit 1][0].Total_Allocated__c);
    }

    @isTest
    private static void testFolderGrantAwardedCalc(){
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
        
        createTestData();
        List<Budget_Group__c> budgetGroups = BudgetGroupSelector.newInstance().selectSObjectsById(budgetIds);
        Budget_Group__c budget1 = budgetGroups[0];
        Budget_Group__c budget2 = budgetGroups[1];
        List<Student_Folder__c> studentFolders = StudentFolderSelector.newInstance().selectAll();
        Student_Folder__c studentFolder1 = studentFolders[0];
        Student_Folder__c studentFolder2 = studentFolders[1];
        
        List<Budget_Allocation__c> testBudgAllocs = new List<Budget_Allocation__c>();
        testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget1.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder1.Id));
        testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget1.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder1.Id));
        testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget1.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder2.Id));
        testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget2.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder1.Id));
        
        Test.startTest();
            upsert testBudgAllocs;
            
            System.assertEquals(3000, [Select Id, Grant_Awarded__c from Student_Folder__c where Id = :studentFolder1.Id limit 1][0].Grant_Awarded__c);

            testBudgAllocs[0].Amount_Allocated__c = 3000;
            update testBudgAllocs[0];
        Test.stopTest();

        System.assertEquals(5000, [Select Id, Grant_Awarded__c from Student_Folder__c where Id = :studentFolder1.Id limit 1][0].Grant_Awarded__c);
    }

    @isTest
    private static void testFolderGrantAwardedCalcAndDeleted(){
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
        
        createTestData();
        List<Budget_Group__c> budgetGroups = BudgetGroupSelector.newInstance().selectSObjectsById(budgetIds);
        Budget_Group__c budget1 = budgetGroups[0];
        Budget_Group__c budget2 = budgetGroups[1];
        List<Student_Folder__c> studentFolders = StudentFolderSelector.newInstance().selectAll();
        Student_Folder__c studentFolder1 = studentFolders[0];
        Student_Folder__c studentFolder2 = studentFolders[1];
        
        List<Budget_Allocation__c> testBudgAllocs = new List<Budget_Allocation__c>();
        testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget1.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder1.Id));
        testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget1.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder1.Id));
        testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget1.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder2.Id));
        testBudgAllocs.add(new Budget_Allocation__c(Budget_Group__c = budget2.Id, Amount_Allocated__c = 1000, Student_Folder__c = studentFolder1.Id));
        
        Test.startTest();
            upsert testBudgAllocs;
            
            System.assertEquals(3000, [Select Id, Grant_Awarded__c from Student_Folder__c where Id = :studentFolder1.Id limit 1][0].Grant_Awarded__c);

            studentFolder1.Grant_Awarded__c = 100;
            studentFolder1.Use_Budget_Groups__c = 'No';
            update studentFolder1;
        Test.stopTest();

        // test delete and no re-calc
        System.assertEquals(100, [Select Id, Grant_Awarded__c from Student_Folder__c where Id = :studentFolder1.Id limit 1][0].Grant_Awarded__c);
        System.assertEquals(0, [Select count() From Budget_Allocation__c WHERE Student_Folder__c = :studentFolder1.Id ]);
    }

    @isTest
    private static void testDeleteBudgetBalance()
    {
        createTestData();
        List<School_PFS_Assignment__c>  schoolPfsAssignments = SchoolPfsAssignmentsSelector.newInstance().selectAllWithCustomFieldList(
            new List<String>{'Withdrawn__c', 'Student_Folder__c'});

        Budget_Allocation__c budgetAllocationDelete = BudgetAllocationTestData.Instance
            .forStudentFolder(schoolPfsAssignments[0].Student_Folder__c).create();
        Budget_Allocation__c budgetAllocationNotDelete = BudgetAllocationTestData.Instance
            .forStudentFolder(schoolPfsAssignments[1].Student_Folder__c).create();
        insert new List<Budget_Allocation__c>{budgetAllocationDelete, budgetAllocationNotDelete};

        schoolPfsAssignments[0].Withdrawn__c = STRING_YES;

        Set<Id> budgetAllocationIds = new Set<Id>{budgetAllocationDelete.Id, budgetAllocationNotDelete.Id};
        List<Budget_Allocation__c> budgetAllocations = BudgetAllocationSelector.newInstance().selectById(budgetAllocationIds);
        System.assertEquals(2, budgetAllocations.size());

        Test.startTest();
            update schoolPfsAssignments[0];
        Test.stopTest();

        budgetAllocations = BudgetAllocationSelector.newInstance().selectById(budgetAllocationIds);
        System.assertEquals(1, budgetAllocations.size());
        System.assertEquals(schoolPfsAssignments[1].Student_Folder__c, budgetAllocations[0].Student_Folder__c, 
            'Expected that only Budget Allocation for not withdrawn SPFSA record left');

        schoolPfsAssignments = SchoolPfsAssignmentsSelector.newInstance().selectAllWithCustomFieldList(
            new List<String>{'Withdrawn__c', 'Student_Folder__r.Grant_Awarded__c'});

        for (School_PFS_Assignment__c schoolPfsAssignment : schoolPfsAssignments)
        {
            if (schoolPfsAssignment.Withdrawn__c == STRING_YES)
            {
                System.assertEquals(null, schoolPfsAssignment.Student_Folder__r.Grant_Awarded__c);
            } else 
            {
                System.assertNotEquals(null, schoolPfsAssignment.Student_Folder__r.Grant_Awarded__c);
            }
        }
    }

    private static void createTestData() 
    {
        Account family = AccountTestData.Instance.asFamily().create();
        Account school = AccountTestData.Instance.asSchool().create();
        insert new List<Account>{family, school};

        Contact parent = ContactTestData.Instance
            .forAccount(family.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .forAccount(school.Id)
            .asSchoolStaff().create();
        Contact student1 = ContactTestData.Instance
            .asStudent().create();
        Contact student2 = ContactTestData.Instance
            .asStudent().create();

        insert new List<Contact>{parent, schoolStaff, student1, student2};
        
        User schoolPortalUser;
        System.runAs(thisUser) 
        {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.insertAcademicYear();
        Student_Folder__c studentFolder1, studentFolder2;

        System.runAs(schoolPortalUser) 
        {
            PFS__c pfs1 = PfsTestData.Instance
                .forParentA(parent.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<PFS__c> {pfs1});

            Applicant__c applicant1 = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student1.Id).create();
            Applicant__c applicant2 = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student2.Id).create();
            Dml.WithoutSharing.insertObjects(new List<Applicant__c> {applicant1, applicant2});

            studentFolder1 = StudentFolderTestData.Instance
                .forNewReturning(STRING_NEW)
                .forStudentId(student1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            studentFolder2 = StudentFolderTestData.Instance
                .forNewReturning(STRING_NEW)
                .forStudentId(student2.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<Student_Folder__c> {studentFolder1, studentFolder2});

            String fifthGrade = '5';
            School_PFS_Assignment__c spfsa1 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            School_PFS_Assignment__c spfsa2 = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant2.Id)
                .forStudentFolderId(studentFolder2.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSchoolId(school.Id).create();
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> {spfsa1, spfsa2});
        }

        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);
        
        Budget_Group__c budget1 = BudgetGroupTestData.Instance.forSchoolId(school.Id).create();
        Budget_Group__c budget2 = BudgetGroupTestData.Instance.forSchoolId(school.Id).create();
        insert new List<Budget_Group__c>{budget1, budget2};
        budgetIds = new Set<Id>{budget1.Id, budget2.Id};
    }
}