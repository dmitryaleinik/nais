global without sharing class FamilyMessagesNewController{
    private FamilyTemplateController controller;
    
    public FamilyMessagesNewController(ApexPages.StandardController stdController) {
        this.controller = FamilyTemplateController.getInstance();
        
        if(this.controller.pfsRecord == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error: No PFS Record Found for current user.'));
            this.controller.pfsRecord = new PFS__c();
        }
        
        this.caseType = ApexPages.currentPage().getParameters().get('type'); //NAIS-1996               
        this.newCase = (Case)stdController.getRecord();  
        //COMMENTED OUT BY SARA: 9/26 this.newCase.ContactId = currentUser.ContactId;        
        this.newCase.ContactId = null;
        this.newCase.Origin = 'Family Portal';
        this.newCase.RecordTypeId = RecordTypes.familyPortalCaseTypeId;
        this.newCase.Reason=(caseType!=null && caseType=='support')?'I need help with this site':'I have a question for a school';
        this.initSchoolsAppliedTo(this.controller.pfsRecords);
        this.dummyContact = new Contact(MailingCountryCode='US');
    }//End-Constructor
    
    public String getTemplate()
    {
        return (this.controller!=null?this.controller.getTemplate():'FamilyTemplate');
    }
    
    public static FamilyTemplateController.config loadPFS(String pfsId, String academicYearId)
    {
        User currentUser = [Select Id, ContactId, ProfileId from User where Id = :UserInfo.getUserId()];
        Academic_Year__c academicYear = [select Id, Name from Academic_Year__c where Id = :academicYearId];
        
        List<PFS__c> pfsRecords = null;
        PFS__c pfsRecord;
        
        if (GlobalVariables.isSysAdminUser(currentUser) || GlobalVariables.isCallCenterUser(currentUser)){
            pfsRecords = [SELECT Id, Own_Business_or_Farm__c, Fee_Waived__c, Academic_Year_Picklist__c, Parent_A__c, PFS_Status__c, Payment_Status__c, PFS_Number__c,
                                 (select Id, name, First_Name__c, Last_Name__c from Applicants__r)
                        FROM
                            PFS__c
                        WHERE
                            Id = :pfsId];
            if(!pfsRecords.isEmpty()) pfsRecord = pfsRecords[0];
            // if this not a sys admin or call center user, get record based on contact id and academic year
        } else {
            pfsRecords = [SELECT Id, Own_Business_or_Farm__c, Fee_Waived__c, Academic_Year_Picklist__c, Parent_A__c, PFS_Status__c,  Payment_Status__c, PFS_Number__c,
                                 (select Id, name, First_Name__c, Last_Name__c from Applicants__r)
                        FROM
                            PFS__c
                        WHERE
                            Parent_A__c = :currentUser.ContactId
                            AND Academic_Year_Picklist__c = :academicYear.Name];
            if(!pfsRecords.isEmpty()) pfsRecord = pfsRecords[0];
        }
        return new FamilyTemplateController.config(pfsRecord, pfsRecords);
    }//End:loadPFS
    
    public String caseType {get;set;} //NAIS-1996
    public String caseIdClone {get;set;} //NAIS-1996
    public list<SelectOption> schoolsAppliedTo{get;set;}//SFP-469
    public string selectedSchools{get;set;}//SFP-469
    public Contact dummyContact{get;set;}//SFP-469
    public string searchSchoolName{get;set;}//SFP-469
    
    public Case newCase {get; set;}

    public Family_Document__c familyDoc {get; set;}
    public Boolean isSpecifiApplicant {get;Set;}
    public Id selectedApplicant {get;Set;}
    
    private List<SelectOption> reasonOptions_x = null;
    public List<SelectOption> ReasonOptions {
        get {
            if (this.reasonOptions_x == null) {
                this.reasonOptions_x = new List<SelectOption>();
                this.reasonOptions_x.add(new SelectOption('', ''));
                
                DescribeFieldResult d = Case.Reason.getDescribe();
                for(PicklistEntry e : d.getPicklistValues()) {
                    if (e.getLabel() == 'I need help with this site' ||
                       e.getLabel() == 'I have a question for a school') {
                        this.reasonOptions_x.add(new SelectOption(e.getValue(), e.getLabel()));
                    }
                }
            }
            return this.reasonOptions_x;
        }
    }
    
    private List<SelectOption> applicants_x = null;
    public List<SelectOption> applicants {
        get {
            if (this.applicants_x == null) {
                this.applicants_x = new List<SelectOption>();
                this.applicants_x.add(new SelectOption('', '--none--'));
                if(this.controller.pfsRecord.applicants__r != null && this.controller.pfsRecord.applicants__r.size() > 0){
                    for(Applicant__c app : this.controller.pfsRecord.applicants__r) {
                       this.applicants_x.add(new SelectOption(app.Id, (app.First_Name__c+' '+app.Last_Name__c)));
                    }
                }
            }
            return this.applicants_x;
        }
    }
    
    public PageReference studentNameChange(){
        return null;
    }
    
    public PageReference CancelNewMessage() {
        
        PageReference p = Page.FamilyMessages;
        p.setRedirect(true);
        
        p.getParameters().put('id', this.controller.pfsRecord.Id);
        p.getParameters().put('academicYearId', this.controller.academicYearId);
        
        return p;
    }
    
    public PageReference SaveNewMessage() {
        if(this.newCase.Reason == 'I have a question for a school' && (this.selectedSchools==null || this.selectedSchools=='null')){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'School to Receive Question: You must enter a value'));
            return null;
        }else if(this.newCase.Reason == 'I need help with this site' && (this.newCase.PFS_Question_Number__c==null || this.newCase.PFS_Question_Number__c=='')){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'PFS Question Number: You must enter a value'));
            return null;
        }else{
            if(this.selectedSchools!='Other'){
                this.newCase.School_to_Receive_Question__c = this.selectedSchools;
            }else{
                if(this.searchSchoolName==null){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'School Name: You must enter a value'));
                    return null;
                }else if(this.dummyContact.MailingStateCode==null){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'School State/Province: You must enter a value'));
                    return null;
                }else if(this.dummyContact.MailingCity==null){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'School City: You must enter a value'));
                    return null;
                }else{
                    try{
                        list<Account> resultSchoolList = Database.query('SELECT Id, Name, SSS_School_Code__c from Account '+
                                            'where Name like \'%'+String.escapeSingleQuotes(this.searchSchoolName)+'%\' '+ 
                                                'and BillingStateCode=\''+this.dummyContact.MailingStateCode+'\' '+
                                                'and BillingCountryCode=\''+this.dummyContact.MailingCountryCode+'\' '+
                                                'and BillingCity=\''+String.escapeSingleQuotes(this.dummyContact.MailingCity)+'\' ');
                        if(resultSchoolList.size()>0)
                        {
                            this.newCase.School_to_Receive_Question__c = resultSchoolList[0].SSS_School_Code__c+' - '+resultSchoolList[0].Name;
                        }else{
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'We could not find any SSS Subscribers with the given school information.'));
                            return null;
                        }
                    }catch(Exception e){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                        return null;
                    }
                }
            }
        }
        
        //SFP-302, G.S]
        if(String.isBlank(selectedApplicant) && !isSpecifiApplicant){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select either "This is not about a specific applicant" or "Applicant"'));
            return null;
        }
        if(!isSpecifiApplicant){
            this.newCase.Applicant__c = selectedApplicant;
        }
        try{
            insert this.newCase;    
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }
        PageReference p = Page.FamilyMessages;
        p.setRedirect(true);
        
        p.getParameters().put('id', this.controller.pfsRecord.Id);
        p.getParameters().put('academicYearId', this.controller.academicYearId);
        
        return p;
    }
    
    public void initSchoolsAppliedTo(list<PFS__c> pfsList)
    {
        this.schoolsAppliedTo = new list<SelectOption>{new SelectOption('null', '')};
        list<Id> applicantIds = new list<Id>();       
        set<Id> verifyRepeated = new set<Id>();
        
        for(PFS__c pfs:pfsList)
        {
            if(pfs.applicants__r!=null && pfs.applicants__r.size()>0)
            {
                for(Applicant__c applicant:pfs.applicants__r)
                {
                    applicantIds.add(applicant.Id);
                }
            }
        }
        
        if(applicantIds.size()>0)
        {
            for(School_PFS_Assignment__c spfsa:[Select Id, School__c, School__r.Name, School__r.SSS_School_Code__c 
                                                    from School_PFS_Assignment__c 
                                                    where Applicant__c IN: applicantIds])
            {
                if(!verifyRepeated.contains(spfsa.School__c))
                {
                    verifyRepeated.add(spfsa.School__c);
                    this.schoolsAppliedTo.add(new SelectOption(spfsa.School__r.SSS_School_Code__c+' - '+spfsa.School__r.Name, 
                                    spfsa.School__r.SSS_School_Code__c+' - '+spfsa.School__r.Name));
                }
            }
        }
        this.schoolsAppliedTo.add(new SelectOption('Other', 'Other'));
    }//End:initSchoolsAppliedTo
}