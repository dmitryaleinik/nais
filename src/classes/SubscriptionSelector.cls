/**
 * @description Queries for Subscription__c records.
 **/
public class SubscriptionSelector extends fflib_SObjectSelector {
    private static final String ACCOUNT_IDS_PARAM = 'accountIds';
    private static final String ACADEMIC_YEAR_PARAM = 'academicYear';
    private static final String ACADEMIC_YEARS_PARAM = 'academicYears';

    /**
     * @description Select subscription records by Account Ids.
     * @param accountIds The Account Ids to select Subscription__c
     *        records by.
     * @return A list of Subscription__c records.
     * @throw An ArgumentNullException if accountIds is null.
     */
    public List<Subscription__c> selectByAccountId(Set<Id> accountIds) {
        ArgumentNullException.throwIfNull(accountIds, ACCOUNT_IDS_PARAM);

        assertIsAccessible();

        return Database.query(String.format('SELECT {0} FROM {1} WHERE Account__c IN :accountIds ORDER BY {2}',
                new List<String> {
                        getFieldListString(),
                        getSObjectName(),
                        getOrderBy()
                }));
    }

    /**
     * @description
     * @param accountIds
     * @return
     * @throw An ArgumentNullException if either accountIds or academicYear are null.
     */
    public List<Subscription__c> selectByAccountIdForAcademicYear(Set<Id> accountIds, Integer academicYear) {
        ArgumentNullException.throwIfNull(accountIds, ACCOUNT_IDS_PARAM);
        ArgumentNullException.throwIfNull(academicYear, ACADEMIC_YEAR_PARAM);

        assertIsAccessible();

        return Database.query(String.format('SELECT {0} FROM {1} WHERE Account__c IN :accountIds AND ' +
                'CALENDAR_YEAR(End_Date__c) = :academicYear ORDER BY {2}',
                new List<String> {
                        getFieldListString(),
                        getSObjectName(),
                        getOrderBy()
                }));
    }

    /**
     * @description Select Subscription records by account Id and academic year.
     * @param accountIds The Account Ids to select subscription records by.
     * @param startYears The academic start years to select subscription records by.
     * @return A list of Subscription records associated with the Account Ids and Academic Years.
     * @throw An ArgumentNullException if either accountIds or academicYear are null.
     */
    public List<Subscription__c> selectByAccountIdForAcademicYear(Set<Id> accountIds, Set<Integer> academicYears) {
        ArgumentNullException.throwIfNull(accountIds, ACCOUNT_IDS_PARAM);
        ArgumentNullException.throwIfNull(academicYears, ACADEMIC_YEARS_PARAM);

        assertIsAccessible();

        return Database.query(String.format('SELECT {0} FROM {1} WHERE Account__c IN :accountIds AND ' +
                'CALENDAR_YEAR(End_Date__c) IN :academicYears ORDER BY {2}',
                new List<String> {
                        getFieldListString(),
                        getSObjectName(),
                        getOrderBy()
                }));
    }

    private Schema.SObjectType getSObjectType() {
        return Subscription__c.getSObjectType();
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                Subscription__c.Account__c,
                Subscription__c.Cancelled__c,
                Subscription__c.End_Date__c,
                Subscription__c.Start_Date__c,
                Subscription__c.Subscription_Type__c
        };
    }

    /**
     * @description Singleton property ensuring external sources do not
     *              instantiate this directly.
     */
    public static SubscriptionSelector Instance {
        get {
            if (Instance == null) {
                Instance = new SubscriptionSelector();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to enforce singleton access
     *              via the Instance property.
     */
    private SubscriptionSelector() {}
}