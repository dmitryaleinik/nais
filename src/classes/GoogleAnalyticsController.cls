/**
 * @description Controls the Google Analytics component.
 **/
public class GoogleAnalyticsController {
    /**
     * @description This will query for and return the first Google
     *              Analytics code in the Google_Analytics__c custom
     *              object associated with the current site name.
     * @return The Google Analytics Code to use on the component.
     */
    public String GoogleAnalyticsCode {
        get {
            if (GoogleAnalyticsCode == null) {
                GoogleAnalyticsCode = getGoogleAnalyticsCode();
            }
            return GoogleAnalyticsCode;
        }
        private set;
    }

    /**
     * @description This will check if user is guest or logged in.
     * @return Boolean value means user is guest or not.
     */
    public Boolean isGuest {
        get {
            isGuest = CurrentUser.isGuest();
            return isGuest;
        }
        private set;
    }

    /**
     * @description This will query for and return the first Google
     *              Analytics code in the Google_Analytics__c custom
     *              object associated with the current site name.
     * @return a Google Analytics Code in string format.
     */
    private String getGoogleAnalyticsCode() {
        String communityType = Site.getName();
        Google_Analytics__c googleAnalyticsCode = Google_Analytics__c.getInstance(communityType);

        if (googleAnalyticsCode != null) {
            return googleAnalyticsCode.Google_Analytics_Code__c;
        }

        return null;
    }
}