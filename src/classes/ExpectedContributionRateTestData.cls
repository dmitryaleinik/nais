/**
 * @description This class is used to create Expected Contribution Rate records for unit tests.
 */
@isTest
public class ExpectedContributionRateTestData extends SObjectTestData {
    @testVisible private static final Decimal DISCRETIONARY_INCOME_LOW = 20;
    @testVisible private static final Decimal EXPECTED_CONTRIBUTION = 15;
    @testVisible private static final Decimal EXPECTED_CONTRIBUTION_PERCENTAGE = 1.5;

    /**
     * @description Get the default values for the Expected_Contribution_Rate__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Expected_Contribution_Rate__c.Academic_Year__c => AcademicYearTestData.Instance.DefaultAcademicYear.Id,
                Expected_Contribution_Rate__c.Discretionary_Income_Low__c => DISCRETIONARY_INCOME_LOW,
                Expected_Contribution_Rate__c.Expected_Contribution__c => EXPECTED_CONTRIBUTION,
                Expected_Contribution_Rate__c.Expected_Contribution_Percentage__c => EXPECTED_CONTRIBUTION_PERCENTAGE
        };
    }

    /**
     * @description Set the Academic Year on the current Expected Contribution
     *             Rate record.
     * @param academicYearId The Id of the Academic Year to set on the current
     *             Expected Contribution Rate record.
     * @return The current working instance of ExpectedContributionRateTestData.
     */
    public ExpectedContributionRateTestData forAcademicYearId(Id academicYearId) {
        return (ExpectedContributionRateTestData) with(Expected_Contribution_Rate__c.Academic_Year__c, academicYearId);
    }

    /**
     * @description Insert the current working Expected_Contribution_Rate__c record.
     * @return The currently operated upon Expected_Contribution_Rate__c record.
     */
    public Expected_Contribution_Rate__c insertExpectedContributionRate() {
        return (Expected_Contribution_Rate__c)insertRecord();
    }

    /**
     * @description Create the current working Expected Contribution Rate record without resetting
     *             the stored values in this instance of ExpectedContributionRateTestData.
     * @return A non-inserted Expected_Contribution_Rate__c record using the currently stored field
     *             values.
     */
    public Expected_Contribution_Rate__c createExpectedContributionRateWithoutReset() {
        return (Expected_Contribution_Rate__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Expected_Contribution_Rate__c record.
     * @return The currently operated upon Expected_Contribution_Rate__c record.
     */
    public Expected_Contribution_Rate__c create() {
        return (Expected_Contribution_Rate__c)super.buildWithReset();
    }

    /**
     * @description The default Expected_Contribution_Rate__c record.
     */
    public Expected_Contribution_Rate__c DefaultExpectedContributionRate {
        get {
            if (DefaultExpectedContributionRate == null) {
                DefaultExpectedContributionRate = createExpectedContributionRateWithoutReset();
                insert DefaultExpectedContributionRate;
            }
            return DefaultExpectedContributionRate;
        }
        private set;
    }

    /**
     * @description Get the Expected_Contribution_Rate__c SObjectType.
     * @return The Expected_Contribution_Rate__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Expected_Contribution_Rate__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static ExpectedContributionRateTestData Instance {
        get {
            if (Instance == null) {
                Instance = new ExpectedContributionRateTestData();
            }
            return Instance;
        }
        private set;
    }

    private ExpectedContributionRateTestData() { }
}