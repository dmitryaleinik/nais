@isTest
private class BudgetGroupSelectorTest {
    
    @isTest
    private static void selectAll_TwoBudgetGroups_AllBudgetGroups()
    {
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
        insert currentAcademicYear;
        
        Account school = AccountTestData.Instance.asSchool().DefaultAccount;
        
        Budget_Group__c budgetGroup1 = BudgetGroupTestData.Instance
            .forAcademicYearId(currentAcademicYear.Id)
            .forSchoolId(school.Id)
            .forTotalFunds(50000).create();
            
        Budget_Group__c budgetGroup2 = BudgetGroupTestData.Instance
            .forAcademicYearId(currentAcademicYear.Id)
            .forSchoolId(school.Id)
            .forTotalFunds(50000).create();
            
        insert new List<Budget_Group__c>{budgetGroup1, budgetGroup2};
     
        Test.startTest();
            System.assertEquals(2, (BudgetGroupSelector.newInstance().selectById(new Set<Id>{budgetGroup1.Id, budgetGroup2.Id})).size());
        Test.stopTest();
    }
}