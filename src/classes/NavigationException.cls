/**
 * @description Custom exception to be thrown when a navigation error occurs.
 **/
public class NavigationException extends Exception { }