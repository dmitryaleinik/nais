/**
 *    NAIS-69: Document Preview
 *
 *    SL, Exponent Partners, 2013
 */
@isTest
private class SpringCMDocPreviewer_SOAPTest
{

    @isTest
    private static void testSpringCMDocPreviewer_SOAP()
    {
        Databank_Settings__c testSettings = new Databank_Settings__c(Name = 'Databank', PDF_Publish_Seconds__c = 5);
        insert testSettings;

        // create test family document record
        Family_Document__c familyDocument = new Family_Document__c();
        familyDocument.Import_ID__c = '12345';
        insert familyDocument;
        
        // test the controller
        Test.setCurrentPage(Page.DocumentPreviewPagePDF);
        SpringCMDocPreviewer_SOAP controller = new SpringCMDocPreviewer_SOAP();
        controller.getPdf();
        System.assertEquals(false, controller.isValidPage);
        
        ApexPages.currentPage().getParameters().put('id', 'badid');
        controller = new SpringCMDocPreviewer_SOAP();
        controller.getPdf();
        System.assertEquals(false, controller.isValidPage);
        
        ApexPages.currentPage().getParameters().put('id', '12345');
        controller = new SpringCMDocPreviewer_SOAP();
        controller.getPdf();
        
        System.assertNotEquals(null, controller.theFamilyDocument);
        System.assertEquals(familyDocument.Id, controller.theFamilyDocument.Id);
        System.assertEquals(familyDocument.Import_ID__c, controller.docid); // NAIS-2234 [DP] 03.18.2015
    }

    // NAIS-2234 [DP] 03.18.2015
    @isTest
    private static void testSpringCMDocPreviewer_SOAP1040PrevId() {
        Databank_Settings__c testSettings = new Databank_Settings__c(Name = 'Databank', PDF_Publish_Seconds__c = 5);
        insert testSettings;

        // create test family document record
        Family_Document__c familyDocument = new Family_Document__c();
        familyDocument.Import_ID__c = '12345';
        familyDocument.Document_Type__c = '1040';
        familyDocument.Filename__c= 'sample.pdf';
        insert familyDocument;
        
        // test the controller
        Test.setCurrentPage(Page.DocumentPreviewPagePDF);
        SpringCMDocPreviewer_SOAP controller = new SpringCMDocPreviewer_SOAP();
        controller.getPdf();
        System.assertEquals(false, controller.isValidPage);
        
        ApexPages.currentPage().getParameters().put('id', 'badid');
        controller = new SpringCMDocPreviewer_SOAP();
        controller.getPdf();
        System.assertEquals(false, controller.isValidPage);
        
        ApexPages.currentPage().getParameters().put('id', '12345');
        controller = new SpringCMDocPreviewer_SOAP();
        controller.getPdf();
        
        System.assertNotEquals(null, controller.theFamilyDocument);
        System.assertEquals(familyDocument.Id, controller.theFamilyDocument.Id);
        
        // This is an example of the url we receive from SpringCM which we then use to generate our own preview url.
        String springUrl = 'http://dev.com/001J000001TEuoW/001J000001TEuoW/001J000001TEuoW';
        
        // These variables just represent the parts of the springUrl that would be split by our controller.
        String baseUrl = 'dev.com';
        String aidUrlParam = '001J000001TEuoW';
        String oUidUrlParam = '001J000001TEuoW';
        String pslUidUrlParam = '001J000001TEuoW';
        
        String expectedPreviewUrl = String.format(SpringCMDocPreviewer_SOAP.SCM_PUBLISH_URL_FORMAT, new List<String> { baseUrl, aidUrlParam, oUidUrlParam, pslUidUrlParam, 'inline=true&pdf=true' });
        
        System.assertEquals(expectedPreviewUrl, controller.buildPreviewUrl(springUrl, 'sample.pdf'), 'Expected the correct url for previewing a document inline.');
        
        controller.throwExceptionForTesting = 'Test handshake exception.';
        try{ controller.getPdf(); }catch(Exception e){
            System.assertEquals(true, (e.getMessage()).contains('handshake'));
        }
    }
}