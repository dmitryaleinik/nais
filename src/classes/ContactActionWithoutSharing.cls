/**
* @brief Class created to handle the logic related to Contact triggers.
* This class allow access to those records for wich the user that 
* triggers the event does not have access.
*/
public without sharing class ContactActionWithoutSharing
{
    /**
    * @brief Method implemented to update the StudentFolder.Name and SPFSA.Name
    * related to a contact that changes its FirstName or LastName. This method also
    * update the StudentFolder.Ethnicity__c related to a contact that changes its
    * Ethnicity__c. The "updateStudentFieldsFuture" is asyncronus to avoid lose 
    * the SPFSA.Name after updated (see the bug when is async: http://www.screencast.com/t/A3FPzuEQXCR).
    *
    * @param contactWithChangedNames A list whit the Ids of those contacts who changed
    * its FirstName or LastName.
    * @param contactWithChangedEthnicity A list whit the Ids of those contacts who changed
    * its Ethnicity__c.
    */    
    public static void studentFieldChanged(set<Id> contactWithChangedNames, set<Id> contactWithChangedEthnicity)
    {
        if(!system.isBatch() && !system.isFuture()){
            updateStudentFieldsFuture(contactWithChangedNames, contactWithChangedEthnicity);
        }else{
            updateStudentFields(contactWithChangedNames, contactWithChangedEthnicity);
        }
        
    }//End:studentFieldChanged
    
    @future
    private static void updateStudentFieldsFuture(set<Id> contactWithChangedNames, set<Id> contactWithChangedEthnicity)
    {
        updateStudentFields(contactWithChangedNames, contactWithChangedEthnicity);
    }//End:updateStudentFieldsFuture
    
    private static void updateStudentFields(set<Id> contactWithChangedNames, set<Id> contactWithChangedEthnicity)
    {
        list<Student_Folder__c> studentFoldersToUpdate = new list<Student_Folder__c>();
        list<School_PFS_Assignment__c> spfsaToUpdate = new list<School_PFS_Assignment__c>();

        string tmpString;
        boolean folderHasChanges;
        
        List<Student_Folder__c> studentFolders = [
                SELECT Name, Student__r.Ethnicity__c, Student__c, School__c, School__r.Name, Student__r.Name, Ethnicity__c, 
                        (SELECT Name FROM School_PFS_Assignments__r)
                FROM Student_Folder__c
                WHERE Student__c IN :contactWithChangedNames
                OR Student__c IN :contactWithChangedEthnicity];
        
        for (Student_Folder__c tmpFolder : studentFolders) {
            
            folderHasChanges=false;
            //Contact changed its FirstName or LastName.
            if(contactWithChangedNames.contains(tmpFolder.Student__c) && GlobalVariables.isSchoolPortalProfile(UserInfo.getProfileId()) 
            && tmpFolder.School__c==GlobalVariables.getCurrentSchoolId())
            {
                tmpString = tmpFolder.Student__r.Name + ' - ' + tmpFolder.School__r.Name;
                tmpFolder.Name = tmpString.left(73)+' Folder';
                folderHasChanges=true;
                
                for(School_PFS_Assignment__c spfsa:tmpFolder.School_PFS_Assignments__r)
                {
                    spfsa.Name = tmpString.left(73);
                    spfsaToUpdate.add(spfsa);
                }
            }
            //Contact changed its ethnicity.
            if(contactWithChangedEthnicity.contains(tmpFolder.Student__c))
            {
                tmpFolder.Ethnicity__c = tmpFolder.Student__r.Ethnicity__c;
                folderHasChanges=true;
            }
            
            if(folderHasChanges){                
                studentFoldersToUpdate.add(tmpFolder);
            }
        }
        if(studentFoldersToUpdate.size()>0) update studentFoldersToUpdate;
        if(spfsaToUpdate.size()>0) update spfsaToUpdate;
    }//End:updateStudentFields
}