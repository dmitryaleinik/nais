/**
 *    NAIS-1877 Test SchooleFeeWaiverPaymentController
 *    Mostly adapted from SchoolPaymentControllerTest [jB]
 */
@isTest
private class SchoolFeeWaiverPaymentControllerTest {
    public static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

    class TestData {
        public Id academicYearId { get; set; }
        public Account school { get; set; }
        public Contact staff { get; set; }
        public User schoolPortalUser { get; set; }
        public Opportunity theOpp { get; set; }

        public TestData(Boolean createOpp, Boolean createtLI) {
            List<Academic_Year__c> academicYears = TestUtils.createAcademicYears();
            academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

            school = TestUtils.createAccount('Test School', RecordTypes.schoolAccountTypeId, 3, false);
            school.SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest();
            insert school;

            // insert annual settings (needed for testing count of fee waivers after purchase)
            List<Annual_Setting__c> annualSettings = GlobalVariables.getCurrentAnnualSettings(true, null, academicYearId, school.id);

            staff = TestUtils.createContact('Test School Staff', school.Id, RecordTypes.schoolStaffContactTypeId, false);
            staff.Email = 'testschoolstaff@school.test' + String.valueOf(Math.random());
            staff.MailingCountry = 'United States';
            insert staff;

            schoolPortalUser = TestUtils.createPortalUser(staff.LastName, staff.Email, 'alias', staff.Id, GlobalVariables.schoolPortalUserProfileId, true, true);

            if (createOpp) {
                Transaction_Settings__c transSetting1 = new Transaction_Settings__c();
                transSetting1.Name = 'School Subscription';
                transSetting1.Account_Code__c = '5100';
                transSetting1.Transaction_Code__c = '1210';
                transSetting1.Account_Label__c = 'Subscription Revenue';
                Transaction_Settings__c transSetting2 = new Transaction_Settings__c();
                transSetting2.Name = 'School Discount';
                transSetting2.Account_Code__c = '5985';
                transSetting2.Transaction_Code__c = '2210';
                transSetting2.Account_Label__c = 'Promotional Discounts';
                Transaction_Settings__c transSetting3 = new Transaction_Settings__c();
                transSetting3.Name = 'ACH';
                transSetting3.Account_Code__c = '1003';
                transSetting3.Transaction_Code__c = '4120';
                transSetting3.Account_Label__c = 'Bank Acct SSS CAO';
                Transaction_Settings__c transSetting4 = new Transaction_Settings__c();
                transSetting4.Name = 'Credit/Debit Card';
                transSetting4.Account_Code__c = '1003';
                transSetting4.Transaction_Code__c = '4110';
                transSetting4.Account_Label__c = 'Bank Acct SSS CAO';
                Transaction_Settings__c transSetting5 = new Transaction_Settings__c();
                transSetting5.name = 'PFS Fee Waiver';
                transSetting5.Account_Label__c = 'PFS Fee Waivers';
                transSetting5.Account_Code__c = '5980';
                transSetting5.Transaction_Code__c = '2310';
                insert new List<Transaction_Settings__c> { transSetting1, transSetting2, transSetting3, transSetting4, transSetting5 };

                // Adding transaction fee line item to calculate costs

                Transaction_Annual_Settings__c fees = new Transaction_Annual_Settings__c(Name='1 Pack - 2016', Product_Amount__c=47.00,
                    Product_Code__c='123-100 Extra Fee Waivers - 1 waiver - 2016-17', Quantity__c=1.0, Year__c=GlobalVariables.getCurrentYearString().left(4));
                insert fees;

                theOpp = new Opportunity();
                theOpp.Name = 'Test Opp';
                theOpp.AccountId = school.Id;
                theOpp.Academic_Year_Picklist__c = GlobalVariables.getCurrentAcademicYear().Name;
                theOpp.StageName = 'Prospecting';
                theOpp.CloseDate = Date.today();
                theOpp.Product_Quantity__c = 10;
                theOpp.RecordTypeId = RecordTypes.opportunityFeeWaiverPurchaseTypeId;
                if (createOpp) {
                    insert theOpp;
                }

                if (createtLI) {
                    PaymentUtils.insertTransactionLineItemsFromOpportunities(new List<Opportunity> { theOpp });
                }
            }
        }
    }

    @isTest
    private static void testSchoolFeeWaiverPaymentSelect() {
        TestData tData = new TestData(true, false);

        Test.startTest();

        PageReference pr = Page.SchoolFeeWaiverPayment;
        pr.getParameters().put('id', tData.theOpp.Id);
        Test.setCurrentPage(pr);

        SchoolFeeWaiverPaymentController controller = new SchoolFeeWaiverPaymentController();
        System.assertEquals(true,        controller.pageError);
        System.assertNotEquals(null,    controller.errorMessage);
        System.assertEquals(null,        controller.thePayer);
        System.assertEquals(null,        controller.theSchool);
        System.assertEquals(null,        controller.theOpportunity);
        System.assertEquals(null,        controller.paymentStep);

        Test.stopTest();
    }

    @isTest
    private static void testPaymentSelectPageWithInvalidOppId() {
        TestData tData = new TestData(false, false);

        Test.startTest();

        System.runAs(tData.schoolPortalUser) {
            PageReference pr = Page.SchoolFeeWaiverPayment;
            pr.getParameters().put('id', 'invalidOppId');
            Test.setCurrentPage(pr);
            SchoolFeeWaiverPaymentController controller = new SchoolFeeWaiverPaymentController();

            System.assertEquals(true,        controller.pageError);
            System.assertNotEquals(null,    controller.errorMessage);
            System.assertNotEquals(null,    controller.thePayer);
            System.assertNotEquals(null,    controller.theSchool);
            System.assertEquals(null,        controller.theOpportunity);
            System.assertEquals(null,        controller.paymentStep);
        }

        Test.stopTest();
    }

    @isTest
    private static void testPaymentSelectPageWithNoBalanceDue() {
        TestData tData = new TestData(true, false);
        //tData.theOpp.Amount = 50;
        update tData.theOpp;

        Test.startTest();

        System.runAs(tData.schoolPortalUser) {
            PageReference pr = Page.SchoolFeeWaiverPayment;
            pr.getParameters().put('id', tData.theOpp.Id);
            Test.setCurrentPage(pr);
            SchoolFeeWaiverPaymentController controller = new SchoolFeeWaiverPaymentController();

            System.assertEquals(true,        controller.pageError);
            System.assertNotEquals(null,    controller.errorMessage);
            System.assertNotEquals(null,    controller.thePayer);
            System.assertNotEquals(null,    controller.theSchool);
            System.assertNotEquals(null,    controller.theOpportunity);
            System.assertEquals(null,        controller.paymentStep);
        }

        Test.stopTest();
    }

    @isTest
    private static void testPaymentSelectPage() {
        TestData tData = new TestData(true, true);

        PageReference pr = Page.SchoolFeeWaiverPayment;

        System.runAs(tData.schoolPortalUser) {
            pr.getParameters().put('id', tData.theOpp.Id);
            Test.setCurrentPage(pr);

            SchoolFeeWaiverPaymentController controller = new SchoolFeeWaiverPaymentController();

            System.debug(controller.pageError);
            System.debug(controller.errorMessage);

            System.assertEquals(false,        controller.pageError);
            System.assertEquals(null,        controller.errorMessage);
            System.assertNotEquals(null,    controller.IsTestMode);
            System.assertNotEquals(null,    controller.me);
            System.assertNotEquals(null,    controller.thePayer);
            System.assertNotEquals(null,    controller.theSchool);
            System.assertEquals('Test School',    controller.theSchool.Name);
            System.assertNotEquals(null,    controller.theOpportunity);

            System.assertEquals(470.00, controller.paymentData.paymentAmount);

            System.assertEquals('PAYMENT_SELECT', controller.paymentStep);

            // submit payment type
            controller.actionPaymentForm();

            // verify the payment form page loads without an error
            System.assertEquals(null,        controller.errorMessage);
            System.assertEquals('PAYMENT_FORM', controller.paymentStep);
            System.assertEquals(true,        controller.paymentData.isCreditCardPaymentType);    // default to CC

            // test select lists are populated
            System.assert(controller.getBillingCountrySelectOptions().size() > 0);
            System.assert(controller.getBillingStateUSSelectOptions().size() > 0);
            System.assert(controller.getExpireMonthSelectOptions().size() > 0);
            System.assert(controller.getExpireYearSelectOptions().size() > 0);
            System.assert(controller.getAccountTypeSelectOptions().size() > 0);
            System.assert(controller.getCreditCardTypeSelectOptions().size() > 0);

            // US should be selected by default
            System.assertEquals(true, controller.getIsUnitedStatesSelected());

            // test going back
            controller.actionPaymentSelect();

            // verify the payment select page loads without an error
            System.assertEquals(null,        controller.errorMessage);
            System.assertEquals('PAYMENT_SELECT', controller.paymentStep);

            // cancel
            pr = controller.actionCancel();
            System.assertEquals(Page.SchoolFeeWaiversPurchase.getUrl(), pr.getUrl());
        }
    }

    @isTest
    private static void testSchoolFeeWaiverCreditCardForm() {
        TestData tData = new TestData(true, true);

        Test.startTest();

        PageReference pr = Page.SchoolFeeWaiverPayment;

        System.runAs(tData.schoolPortalUser) {
            pr.getParameters().put('id', tData.theOpp.Id);
            Test.setCurrentPage(pr);

            SchoolFeeWaiverPaymentController controller = new SchoolFeeWaiverPaymentController();
            controller.paymentStep = 'PAYMENT_FORM';
            controller.paymentData.paymentType = FamilyPaymentData.PAYMENT_TYPE_CC;

            // submit form
            controller.actionPaymentConfirm();

            // verify validation errors
            System.assertEquals(true, controller.pageError);
            System.assertEquals(PaymentControllerBase.MESSAGE_PAGE_VALIDATION_ERROR, controller.errorMessage);
            System.assertEquals('PAYMENT_FORM', controller.paymentStep);

            // additional validation errors
            controller.paymentData.ccCardNumber = 'abcde';
            controller.paymentData.ccSecurityCode = 'xyz';

            // resubmit form
            controller.actionPaymentConfirm();

            System.assertEquals(true, controller.pageError);
            System.assertEquals(PaymentControllerBase.MESSAGE_PAGE_VALIDATION_ERROR, controller.errorMessage);
            System.assertEquals('PAYMENT_FORM', controller.paymentStep);

            // enter form data for real
            controller.paymentData.billingFirstName = 'Test First Name';
            controller.paymentData.billingLastName = 'Test Last Name';
            controller.paymentData.billingStreet1 = 'Test Street 1';
            controller.paymentData.billingStreet2 = 'Test Street 2';
            controller.paymentData.billingCity = 'Test City';
            controller.paymentData.billingState = 'Test State';
            controller.paymentData.billingPostalCode = '11111';
            controller.paymentData.billingCountry = '123';
            controller.paymentData.billingEmail = 'test@test.com';
            controller.paymentData.ccNameOnCard = 'Test Name';
            controller.paymentData.ccCardType = 'Visa';
            controller.paymentData.ccCardNumber = '4111111111111111';
            controller.paymentData.ccExpirationMM = String.valueOf(System.today().month());
            controller.paymentData.ccExpirationYY = String.valueOf(System.today().addYears(1).year());
            controller.paymentData.ccSecurityCode = '123';

            // submit form
            controller.actionPaymentConfirm();

            // verify the confirm page loads without an error
            System.assertEquals(false, controller.pageError);
            System.assertEquals(null, controller.errorMessage);
            System.assertEquals('PAYMENT_CONFIRM', controller.paymentStep);

            // submit payment
            pr = controller.actionPaymentSubmit();

            System.debug('Page Error: '+controller.errorMessage);

            // verify the submit redirect page loads without an error
            System.assertEquals(false, controller.pageError);
            System.assertEquals(null, controller.errorMessage);
            System.assertEquals(Page.SchoolFeeWaiverPaymentSubmit.getUrl(), pr.getUrl());

            // process the payment
            pr = controller.processPayment();

            System.debug('Page Error: '+controller.errorMessage);
            // verify the payment complete page loads without an error
            System.assertEquals(false, controller.pageError);
            System.assertEquals(null, controller.errorMessage);
            System.assert(pr.getUrl().contains(Page.SchoolFeeWaivers.getUrl()));
            System.assert(pr.getUrl().contains('id=' + tData.theOpp.Id));
        }

        Test.stopTest();

        // verify Sale, Discount, and Payment transaction line items got created
        List<Transaction_Line_Item__c> transactions = [select Id, RecordTypeId, Amount__c, Transaction_Status__c
                from Transaction_Line_Item__c
                where Opportunity__c = :tData.theOpp.Id
                                       order by Id];

        System.debug(transactions);

        // Sale TLI
        System.assertEquals(RecordTypes.saleTransactionTypeId, transactions[0].RecordTypeId);
        System.assertEquals('Posted',    transactions[0].Transaction_Status__c);
        System.assertEquals(470.00,        transactions[0].Amount__c);

        System.assertEquals(RecordTypes.paymentAutoTransactionTypeId, transactions[1].RecordTypeId);
        System.assertEquals('Posted',    transactions[1].Transaction_Status__c);
        System.assertEquals(470.00,        transactions[1].Amount__c);

        // verify Opportunity is closed and Subscription record is created
        Opportunity opp = [select StageName from Opportunity where Id = :tData.theOpp.Id];

        System.assert(OpportunityAction.closedoppStageNames.contains(opp.StageName));

        List<Annual_Setting__c> annualSettingRecord = [select Id, Total_Waivers_Override_Default__c, School__c, Academic_Year__c, Academic_Year__r.Name
                                                        from Annual_Setting__c
                                                        where School__c = :tData.school.id];

        System.debug('Annual settings: '+annualSettingRecord);
    }

    @isTest
    static void testRenewalPaymentCreditCardDeclined() {
        TestData tData = new TestData(true, true);

        Test.startTest();

        PageReference pr = Page.SchoolFeeWaiverPayment;

        System.runAs(tData.schoolPortalUser) {
            pr.getParameters().put('id', tData.theOpp.Id);
            Test.setCurrentPage(pr);

            SchoolFeeWaiverPaymentController controller = new SchoolFeeWaiverPaymentController();
            controller.paymentStep = 'PAYMENT_FORM';
            controller.paymentData.paymentType = FamilyPaymentData.PAYMENT_TYPE_CC;

            // enter form data
            controller.paymentData.billingFirstName = 'Test First Name';
            controller.paymentData.billingLastName = 'Test Last Name';
            controller.paymentData.billingStreet1 = 'Test Street 1';
            controller.paymentData.billingStreet2 = 'Test Street 2';
            controller.paymentData.billingCity = 'Test City';
            controller.paymentData.billingState = 'Test State';
            controller.paymentData.billingPostalCode = '99999';
            controller.paymentData.billingCountry = '123';
            controller.paymentData.billingEmail = 'test@test.com';
            controller.paymentData.ccNameOnCard = 'Test Name';
            controller.paymentData.ccCardType = 'Visa';
            controller.paymentData.ccCardNumber = '4400000000000000';
            controller.paymentData.ccExpirationMM = String.valueOf(System.today().month());
            controller.paymentData.ccExpirationYY = String.valueOf(System.today().addYears(1).year());
            controller.paymentData.ccSecurityCode = '123';

            // submit form
            controller.actionPaymentConfirm();

            // verify the confirm page loads without an error
            System.assertEquals(false, controller.pageError);
            System.assertEquals(null, controller.errorMessage);
            System.assertEquals('PAYMENT_CONFIRM', controller.paymentStep);

            // submit payment
            pr = controller.actionPaymentSubmit();

            // verify the submit redirect page loads without an error
            System.assertEquals(false, controller.pageError);
            System.assertEquals(null, controller.errorMessage);
            System.assertEquals(Page.SchoolFeeWaiverPaymentSubmit.getUrl(), pr.getUrl());

            // process the payment
            pr = controller.processPayment();

            // verify the form page loads with an error
            System.assertEquals(true, controller.pageError);
            System.assertNotEquals(null, controller.errorMessage);

            controller.handlePaymentError();
            System.assertEquals('PAYMENT_CONFIRM', controller.paymentStep);
        }

        Test.stopTest();
    }

    @isTest
    private static void testRenewalPaymentCreditCardRetry() {
        TestData tData = new TestData(true, true);

        Test.startTest();

        PageReference pr = Page.SchoolFeeWaiverPayment;

        System.runAs(tData.schoolPortalUser) {
            pr.getParameters().put('id', tData.theOpp.Id);
            Test.setCurrentPage(pr);

            SchoolFeeWaiverPaymentController controller = new SchoolFeeWaiverPaymentController();
            controller.paymentStep = 'PAYMENT_FORM';
            controller.paymentData.paymentType = FamilyPaymentData.PAYMENT_TYPE_CC;

            // enter form data
            controller.paymentData.billingFirstName = 'Test First Name';
            controller.paymentData.billingLastName = 'Test Last Name';
            controller.paymentData.billingStreet1 = 'Test Street 1';
            controller.paymentData.billingStreet2 = 'Test Street 2';
            controller.paymentData.billingCity = 'Test City';
            controller.paymentData.billingState = 'Test State';
            controller.paymentData.billingPostalCode = '11111';
            controller.paymentData.billingCountry = '123';
            controller.paymentData.billingEmail = 'test@test.com';
            controller.paymentData.ccNameOnCard = 'Test Name';
            controller.paymentData.ccCardType = 'Visa';
            controller.paymentData.ccCardNumber = '4400000000000000';
            controller.paymentData.ccExpirationMM = '05';
            controller.paymentData.ccExpirationYY = '25';
            controller.paymentData.ccSecurityCode = '123';

            // submit form
            controller.actionPaymentConfirm();

            // submit payment
            pr = controller.actionPaymentSubmit();

            // process the payment
            pr = controller.processPayment();

            // verify the form page loads with an error
            System.assertEquals(true,        controller.pageError);
            System.assertNotEquals(null,    controller.errorMessage);

            // correct error
            controller.paymentData.ccCardNumber = '4111111111111111';

            // retry payment
            pr = controller.processPayment();

            // verify the payment complete page loads without an error
            System.assertEquals(false,    controller.pageError);
            System.assertEquals(null,    controller.errorMessage);
            System.assert(pr.getUrl().contains(Page.SchoolFeeWaivers.getUrl()));
            System.assert(pr.getUrl().contains('id=' + tData.theOpp.Id));
        }

        Test.stopTest();
    }

    @isTest
    private static void testSchoolFeeWaiversPaymentECheckForm() {
        TestData tData = new TestData(true, true);

        Test.startTest();

        System.runAs(tData.schoolPortalUser) {
            PageReference pr = Page.SchoolFeeWaiverPayment;
            pr.getParameters().put('id', tData.theOpp.Id);
            Test.setCurrentPage(pr);
            SchoolFeeWaiverPaymentController controller = new SchoolFeeWaiverPaymentController();
            controller.paymentStep = 'PAYMENT_FORM';
            controller.paymentData.paymentType = FamilyPaymentData.PAYMENT_TYPE_ECHECK;

            // submit form
            controller.actionPaymentConfirm();

            // verify validation errors
            System.assertEquals(true, controller.pageError);
            System.assertEquals(PaymentControllerBase.MESSAGE_PAGE_VALIDATION_ERROR, controller.errorMessage);
            System.assertEquals('PAYMENT_FORM', controller.paymentStep);

            // additional validation errors
            controller.paymentData.checkRoutingNumber = 'abcde';
            controller.paymentData.checkCheckNumber = 'xyz';
            controller.paymentData.checkAccountNumber = 'zzz4567890';
            controller.checkAccountNumberConfirm = '5555555';
            controller.paymentData.billingEmail = 'test#test.com';
            controller.billingEmailConfirm = 'test2@test.com';

            // resubmit form
            controller.actionPaymentConfirm();

            System.assertEquals(true, controller.pageError);
            System.assertEquals(PaymentControllerBase.MESSAGE_PAGE_VALIDATION_ERROR, controller.errorMessage);
            System.assertEquals('PAYMENT_FORM', controller.paymentStep);

            SelectOption option = PaymentProcessor.getAccountTypeSelectOptions()[0];

            // enter form data for real
            controller.paymentData.billingFirstName = 'Test First Name';
            controller.paymentData.billingLastName = 'Test Last Name';
            controller.paymentData.billingStreet1 = 'Test Street 1';
            controller.paymentData.billingStreet2 = 'Test Street 2';
            controller.paymentData.billingCity = 'Test City';
            controller.paymentData.billingState = 'Test State';
            controller.paymentData.billingPostalCode = '99999';
            controller.paymentData.billingCountry = '123';
            controller.paymentData.billingEmail = 'test@test.com';
            controller.paymentData.checkBankName = 'BankOfAmerica';
            controller.paymentData.checkAccountType = option.getValue();
            controller.paymentData.checkAccountNumber = '1234567890';
            controller.paymentData.checkRoutingNumber = '111111111';
            controller.paymentData.checkCheckType = 'Personal';
            controller.paymentData.checkCheckNumber = '123';
            controller.billingEmailConfirm = 'test@test.com';
            controller.checkAccountNumberConfirm = '1234567890';

            // check the selected account type name
            System.assertEquals(option.getLabel(), controller.getSelectedAccountType());

            // submit form
            controller.actionPaymentConfirm();

            // verify the confirm page loads without an error
            System.assertEquals(false, controller.pageError);
            System.assertEquals(null, controller.errorMessage);
            System.assertEquals('PAYMENT_CONFIRM', controller.paymentStep);

            // submit payment
            pr = controller.actionPaymentSubmit();

            // verify the submit redirect page loads without an error
            System.assertEquals(false, controller.pageError);
            System.assertEquals(null, controller.errorMessage);
            System.assertEquals(Page.SchoolFeeWaiverPaymentSubmit.getUrl(), pr.getUrl());

            // process the payment
            pr = controller.processPayment();

            // verify the payment complete page loads without an error
            System.assertEquals(false, controller.pageError);
            System.assertEquals(null, controller.errorMessage);
            System.assert(pr.getUrl().contains(Page.SchoolFeeWaivers.getUrl()));
            System.assert(pr.getUrl().contains('id=' + tData.theOpp.Id));
        }

        Test.stopTest();
    }

    @isTest
    private static void testSchoolFeeWaiversECheckRetry() {
        TestData tData = new TestData(true, true);

        Test.startTest();

        System.runAs(tData.schoolPortalUser) {
            PageReference pr = Page.SchoolFeeWaiverPayment;
            pr.getParameters().put('id', tData.theOpp.Id);
            Test.setCurrentPage(pr);
            SchoolFeeWaiverPaymentController controller = new SchoolFeeWaiverPaymentController();
            controller.paymentStep = 'PAYMENT_FORM';
            controller.paymentData.paymentType = FamilyPaymentData.PAYMENT_TYPE_ECHECK;

            // enter form data
            controller.paymentData.billingFirstName = 'Test First Name';
            controller.paymentData.billingLastName = 'Test Last Name';
            controller.paymentData.billingStreet1 = 'Test Street 1';
            controller.paymentData.billingStreet2 = 'Test Street 2';
            controller.paymentData.billingCity = 'Test City';
            controller.paymentData.billingState = 'Test State';
            controller.paymentData.billingPostalCode = '99999';
            controller.paymentData.billingCountry = '123';
            controller.paymentData.billingEmail = 'test@test.com';
            controller.paymentData.checkBankName = 'BankOfAmerica';
            controller.paymentData.checkAccountType = 'CheckingAccount';
            controller.paymentData.checkAccountNumber = '0987654321';
            controller.paymentData.checkRoutingNumber = '111111111';
            controller.paymentData.checkCheckType = 'Personal';
            controller.paymentData.checkCheckNumber = '123';
            controller.billingEmailConfirm = 'test@test.com';
            controller.checkAccountNumberConfirm = '0987654321';

            // submit form
            controller.actionPaymentConfirm();

            // submit payment
            pr = controller.actionPaymentSubmit();

            // process the payment
            pr = controller.processPayment();

            // verify the form page loads with an error
            System.assertEquals(true, controller.pageError);
            System.assertNotEquals(null, controller.errorMessage);

            // correct error
            controller.paymentData.checkAccountNumber = '0123456789';
            controller.checkAccountNumberConfirm = '0123456789';

            // retry payment
            pr = controller.processPayment();

            // verify the payment complete page loads without an error
            System.assertEquals(false, controller.pageError);
            System.assertEquals(null, controller.errorMessage);
            System.assert(pr.getUrl().contains(Page.SchoolFeeWaivers.getUrl()));
            System.assert(pr.getUrl().contains('id=' + tData.theOpp.Id));
            System.assertEquals(null, controller.actionInvoiceSubmit());
            System.assertEquals('PAYMENT_INVOICE', controller.paymentStep);
            System.assertEquals('/apex/schooldashboard', (controller.actionInvoiceDone()).getUrl());
            System.assertEquals(null, controller.initMethod());
        }

        Test.stopTest();
    }

    @isTest
    private static void handleTransactionError_genericFailingResult_expectEarlyExitAndGeneralError() {
        Test.setCurrentPage(Page.SchoolFeeWaiverPayment);
        SchoolFeeWaiverPaymentController controller = new SchoolFeeWaiverPaymentController();

        PaymentResult result = new PaymentResult();
        result.IsSuccess = false;

        Test.startTest();
        controller.handleTransactionError(result, false);
        Test.stopTest();

        System.assertEquals(SchoolFeeWaiverPaymentController.MESSAGE_PAYMENT_ERROR_GENERAL, controller.ErrorMessage,
                'Expected the general error message.');
    }
}