global with sharing class AnnualSettingsUpdateBatch implements Database.Batchable<sObject>
{
    global List<Annual_Setting__c> recordsToUpdate {get; private set;}

    global AnnualSettingsUpdateBatch(List<Annual_Setting__c> annualSettings)
    {
        if (annualSettings != null && !annualSettings.isEmpty())
        {
            this.recordsToUpdate = annualSettings;
        }
    }

    global List<Annual_Setting__c> start(Database.BatchableContext bc)
    {
        return recordsToUpdate;
    }

    global void execute(Database.BatchableContext bc, List<Annual_Setting__c> scope)
    {
        Database.SaveResult[] updateResults = Database.update(recordsToUpdate, false);
        for (Database.SaveResult result : updateResults)
        {
            System.debug(LoggingLevel.ERROR, 'Upsert failed: ' + result);
        }
    }

    global void finish(Database.BatchableContext bc)
    {
        Id currentUserContactId = UserSelector.newInstance().selectById(new Set<Id>{UserInfo.getUserId()})[0].ContactId;

        for (Annual_Setting__c obj : recordsToUpdate)
        {
            SingleEmailService.Instance.sendEmail(
                obj.Id,
                new List<Id>{currentUserContactId},
                'AnnualSettingsUpdateBatchEmail',
                OrgWideEmails.Instance.Do_Not_Reply_Address);
        }
    }
}