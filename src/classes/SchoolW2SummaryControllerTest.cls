@isTest
private class SchoolW2SummaryControllerTest {
    private static School_PFS_Assignment__c pfsAssignment;
    private static User portalUser;

    private static void initData() {
        List<Academic_Year__c> accYears = AcademicYearTestData.Instance.insertAcademicYears(5);
        String documentYear = GlobalVariables.getDocYearStringFromAcadYearName(GlobalVariables.getCurrentYearString());

        portalUser = UserTestData.insertSchoolPortalUser();

        Account school = AccountTestData.Instance.DefaultAccount;

        AccountShare accountSharingSettings = AccountShareTestData.Instance
                .forUserOrGroupId(portalUser.Id)
                .DefaultAccountShare;

        SchoolPortalSettings.KS_School_Account_Id = school.Id;

        Family_Document__c familyDoc = FamilyDocumentTestData.Instance
                .forDocumentType('W2')
                .forAcademicYearPicklist(accYears[0].Name)
                .DefaultFamilyDocument;

        pfsAssignment = SchoolPfsAssignmentTestData.Instance
                .forAcademicYearPicklist(accYears[0].Name)
                .DefaultSchoolPfsAssignment;

        List<School_PFS_Assignment__c> pfsAssignmentsForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(pfsAssignmentsForSharing, null);

        School_Document_Assignment__c docAssignment = SchoolDocumentAssignmentTestData.Instance
                .forDocument(familyDoc.Id)
                .forDocumentYear(documentYear)
                .DefaultSchoolDocumentAssignment;
    }

    @isTest private static void testW2SummaryInit_validData_myDocs() {
        initData();

        System.runAs(portalUser) {
            Test.setCurrentPage(Page.SchoolW2Summary);

            // Need to pass in a School PFS Assignment Id.
            System.currentPageReference().getParameters().put('SchoolPFSAssignmentId', pfsAssignment.Id);

            ApexPages.StandardController sc = new ApexPages.StandardController(new Account());
            SchoolW2SummaryController testCtrlr = new SchoolW2SummaryController(sc);
            System.assertEquals(1, testCtrlr.myDocs.size());

            System.assertEquals(null, testCtrlr.pfsNumber);
            System.assertEquals(StudentFolderTestData.Instance.DefaultStudentFolder.Id, testCtrlr.folderId);
            System.assertEquals(false, testCtrlr.isEditableYear);
            System.assert(testCtrlr.schoolPFSAssignment != null);
        }
    }
}