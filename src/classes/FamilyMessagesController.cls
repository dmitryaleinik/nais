global class FamilyMessagesController{
    private FamilyTemplateController controller{get;set;}
    
    public FamilyMessagesController(FamilyTemplateController c) {
        this.controller = c;
        
        if(this.controller.pfsRecord == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error: No PFS Record Found for current user.'));
            this.controller.pfsRecord = new PFS__c();
        }
        this.pfsName = ApplicationUtils.calcPFSNameForUser(this.controller.pfsRecord);
        this.pfsStatus = ApplicationUtils.calcPFSStatusForUser(this.controller.pfsRecord);
    }//End-Constructor
    
    public static FamilyTemplateController.config loadPFS(String pfsId, String academicYearId)
    {
        PFS__c pfsRecord;
        User currentUser = [Select Id, ContactId, ProfileId from User where Id = :UserInfo.getUserId()];
        Academic_Year__c academicYear = [select Id, Name from Academic_Year__c where Id = :academicYearId];
        
        List<PFS__c> pfsRecords = null;
        
        if (GlobalVariables.isSysAdminUser(currentUser) || GlobalVariables.isCallCenterUser(currentUser)){
            pfsRecords = [SELECT
                            Id, Own_Business_or_Farm__c, IntegrationProcessed__c, IntegrationErrors__c, IntegrationSource__c, Academic_Year_Picklist__c, Fee_Waived__c, Parent_A__c, PFS_Status__c, Payment_Status__c, PFS_Number__c
                        FROM
                            PFS__c
                        WHERE
                            Id = :pfsId];
            if(!pfsRecords.isEmpty()) pfsRecord = pfsRecords[0];
            // if this not a sys admin or call center user, get record based on contact id and academic year
        } else {
            pfsRecords = [SELECT
                           Id, Own_Business_or_Farm__c, IntegrationProcessed__c, IntegrationErrors__c, IntegrationSource__c, Academic_Year_Picklist__c, Fee_Waived__c, Parent_A__c, PFS_Status__c,  Payment_Status__c, PFS_Number__c
                        FROM
                            PFS__c
                        WHERE
                            Parent_A__c = :currentUser.ContactId
                            AND Academic_Year_Picklist__c = :academicYear.Name];
            if(!pfsRecords.isEmpty()) pfsRecord = pfsRecords[0];
        }
        return new FamilyTemplateController.config(pfsRecord, pfsRecords);
    }//End:loadPFS
    
    public Family_Document__c familyDoc {get; set;}
    public String pfsName{get;set;}
    public String pfsStatus{get;set;}
    
    private List<Case> cases_x = null;   
    public List<Case> Cases {
        get {
            if(this.cases_x == null) {
                
                List<Id> rTypes = new List<Id>{ RecordTypes.schoolParentCommunicationCaseTypeId, 
                                                RecordTypes.familyPortalCaseTypeId, 
                                                RecordTypes.familyPortalCaseSchoolTypeId, 
                                                RecordTypes.parentCallCaseTypeId,
                                                RecordTypes.familyLiveAgentCaseTypeId
                                                 };
                User currentUser = [Select Id, ContactId, ProfileId from User where Id = :UserInfo.getUserId()];
                // NAIS-1758 Query Modified to to include createdBy field on case
                this.cases_x = [SELECT
                                    Id,
                                    CaseNumber,
                                    Subject,
                                    Status,
                                    CreatedDate,
                                    LastModifiedDate,
                                    ClosedDate,
                                    Account.SSS_School_Code__c,
                                    Account.Name,
                                    SSS_School_Code__c,
                                    Contact.Name,
                                    CreatedBy.Name,
                                    CreatedBy.Contact.Account.Name,
                                    CreatedBy.Contact.Account.SSS_School_Code__c,
                                    Contact.Email,
                                    RecordType.Name, 
                                    Reason, //NAIS-1996, 
                                    School_Name__c, 
                                    School_Code__c 
                                FROM
                                    Case
                                WHERE
                                    ContactId = :currentUser.ContactId  
                                    AND RecordTypeId IN :rTypes
                                ORDER BY
                                    CreatedDate DESC];
                
            }
            
            return this.cases_x;
        }
    }
    
    public PageReference NewSupportTicket() {
        
        PageReference p = Page.FamilyMessagesNew;
        p.setRedirect(true);

        p.getParameters().put('pfsid', this.controller.pfsRecord.Id);
        p.getParameters().put('academicYearId', this.controller.academicYearId);
        p.getParameters().put('type', 'support');
        return p;
    }  
    /*NAIS-1996*/
    public PageReference NewQuestionToSchool(){
        PageReference p = Page.FamilyMessagesNew;
        p.setRedirect(true);

        p.getParameters().put('pfsid', this.controller.pfsRecord.Id);
        p.getParameters().put('academicYearId', this.controller.academicYearId);
        return p;
    }
    /*End NAIS-1996*/
}