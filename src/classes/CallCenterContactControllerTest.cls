@IsTest
private class CallCenterContactControllerTest
{

    private static void  setupData()
    {
        FieldIDbyName__c dnisSetting = new FieldIDbyName__c();
        dnisSetting.Field_ID__c = '1234';
        dnisSetting.Name='dnis';

        FieldIDbyName__c anisSetting = new FieldIDbyName__c();
        anisSetting.Field_ID__c = '2234';
        anisSetting.Name='ani';

        FieldIDbyName__c phoneIfNotFoundSetting = new FieldIDbyName__c();
        phoneIfNotFoundSetting.Field_ID__c = '4444';
        phoneIfNotFoundSetting.Name='phoneIfNotInSystem';
        insert new List<FieldIDbyName__c>{dnisSetting, anisSetting, phoneIfNotFoundSetting};

        List<Contact> parents = new List<Contact>();
        parents.add(new Contact(LastName = 'contact1', mobilephone='(222) -123-1234', RecordTypeID = RecordTypes.parentContactTypeId));
        parents.add(new Contact(LastName = 'contact2',phone='2221231234', RecordTypeID = RecordTypes.parentContactTypeId));
        parents.add(new Contact(LastName = 'contact3',homephone='222-123-1234',RecordTypeID = RecordTypes.schoolStaffContactTypeId));
        parents.add(new Contact(LastName = 'contact4', mobilephone='(222) -123-7232', RecordTypeID = RecordTypes.parentContactTypeId));
        insert parents;
        List<ID> fixedSearchResults =  new List<ID>();
        for (Contact contact: parents) {
            fixedSearchResults.add(contact.id);
        }
        Test.setFixedSearchResults(fixedSearchResults);
    }

    @isTest
    private static void testControllerNoContactFound()
    {
        setupData();
        PageReference pageRef = new PageReference('/apex/ContactCenterAction?dnis=parentdnis&phone=1234&sessionID=thession&ani=asdf');
        Test.setCurrentPage(pageRef);

        CallCenterContactController controller = new CallCenterContactController();
        controller.findContacts();

        System.assertEquals(controller.ani, 'asdf');

        System.assertEquals(controller.contacts, null);

        System.assertEquals(controller.url, null);
        String contacturl='/003/e?RecordType=' + RecordTypes.parentContactTypeId+ '&con13=asdf'
                + '&retURL='
                + EncodingUtil.urlEncode('/apex/ContactCenterAction?', 'UTF-8');
        System.assert(controller.contactUrl.startsWith(contacturl));    // actual order of params in retURL could be scrambled

        //search phone
        controller.searchTerm='contact';
        controller.doSearch();
        System.assertEquals(controller.contacts.size() , 3);
        //http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_testing_SOSL.htm
        //in SOSL the results are always empty unless you specify fixed results and the search term no matter what it is, is ignored
    }

    @isTest
    private static void testControllerSingleContact()
    {
        setupData();
        PageReference pageRef = new PageReference('/apex/ContactCenterAction?dnis=dd&phone=1234&sessionID=thession&ani=(222) -123-7232');
        Test.setCurrentPage(pageRef);

        CallCenterContactController controller = new CallCenterContactController();
        controller.findContacts();

        System.assertEquals(controller.ani, '(222) -123-7232');

        System.assertEquals(controller.contacts.size(), 1);

        System.assertEquals(controller.url, '/'+controller.contacts[0].id);
    }

    @isTest
    private static void testControllerMultipleContacts()
    {
        setupData();
        PageReference pageRef = new PageReference('/apex/ContactCenterAction?dnis=dd&phone=1234&callid=callid&ani=222-123-1234');
        Test.setCurrentPage(pageRef);

        CallCenterContactController controller = new CallCenterContactController();
        controller.findContacts();

        System.assertEquals(controller.ani, '222-123-1234');
        System.assertEquals(controller.dnis,'dd');
        System.assertEquals(controller.callid, 'callid');

        System.assertEquals(controller.contacts.size(), 2);

        System.assertEquals(controller.url, null);
    }

    @isTest
    private static void testControllerNewCaseURL()
    {
        setupData();
        PageReference pageRef = new PageReference('/apex/ContactCenterAction?dnis=parentdnis&phone=1234&sessionID=thession&ani=asdf');
        Test.setCurrentPage(pageRef);

        CallCenterContactController controller = new CallCenterContactController();
        controller.findContacts();

        System.assertEquals(controller.ani, 'asdf');
        System.assertEquals(controller.contacts, null);
        System.assertEquals(controller.url, null);

        System.assert(controller.newCaseUrl.contains('asdf'));
    }
}