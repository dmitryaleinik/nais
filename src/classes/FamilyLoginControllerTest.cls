@isTest
private class FamilyLoginControllerTest
{

    private static void setFamilyPortalSettingEarlyAccessDate(DateTime setTime) {
        // Update the Family Portal Setting's Early Access Date field
        Family_Portal_Settings__c familyPortalSettings = Family_Portal_Settings__c.getInstance('Family');
        familyPortalSettings.Early_Access_Start_Date__c = setTime;
        update familyPortalSettings;
    }

    // email/username not recognized
    @isTest
    private static void emailUnrecognized(){
        FamilyLoginController roller = new FamilyLoginController();

        roller.username = 'unrecognized@drew.dru';
        roller.password = 'xxxxxx';

        // go to unrecognized page
        PageReference pr = roller.login();
        System.assert(pr.getURL().contains('familyloginunrecognized'));
    }

    // email matches existing user
    @isTest
    private static void emailFoundOnActiveUser(){
        User schoolPortalUser = UserTestData.insertSchoolPortalUser();

        FamilyLoginController roller = new FamilyLoginController();

        roller.username = schoolPortalUser.Username;
        roller.password = 'xxxxxx';

        roller.integrationSourceFromCookie = 'testenna';
        PageReference pr = roller.login();
        // password is wrong, always expect null
        System.assertEquals(null, pr);
        System.assertEquals(schoolPortalUser.Id, roller.existingUser.Id);
    }

    // email matches Contact only
    @isTest
    private static void emailFoundOnContact(){
        Contact testContact = new Contact(LastName = 'Unittest', AccountId = null, Email = 'newUser@drew.dru', recordTypeId=RecordTypes.parentContactTypeId);
        insert testContact;


        Test.startTest();

        FamilyLoginController roller = new FamilyLoginController();
        roller.username = testContact.Email;
        roller.password = 'xxxxx';

        // go to unrecognized page
        PageReference pr = roller.login();
        System.assert(pr.getURL().contains('familyloginupdateaccount'));
        System.assertEquals(testContact.Id, roller.existingContact.Id);

        Test.stopTest();
    }

    // create brand new account
    @isTest
    private static void createNewAccountTest(){
        Test.startTest();

        FamilyLoginController roller = new FamilyLoginController();
        roller.username = 'email@drew.dru';
        roller.password = 'xxxxx';

        // go to unrecognized page
        PageReference pr = roller.login();
        System.assert(pr.getURL().contains('familyloginunrecognized'));
        System.assertEquals(null, roller.existingContact);

        System.assertEquals(0, [Select count() from Contact where MailingStreet = '999 Unit Street']);

        roller.dummyContact.FirstName = 'unit';
        roller.dummyContact.LastName = 'test999';
        roller.dummyContact.MailingStreet = '999 Unit Street';

        roller.dummyContact.email = 'email@drew.dru';
        roller.emailConfirm = 'email@drew.dru';

        pr = roller.createAccount();

        Test.stopTest();

        System.assertEquals(1, [Select count() from Contact where MailingStreet = '999 Unit Street']);
        // assert HH created
        System.assertNotEquals(null, [Select Household__c from Contact where MailingStreet = '999 Unit Street'][0].Household__c);
    }

    // create brand new account
    @isTest
    private static void createNewAccountTestExistingContact(){
        Contact testContact = new Contact(LastName = 'Unittest', AccountId = null, Email = 'newUser@drew.dru');
        insert testContact;

        Test.startTest();
            System.assertEquals(0, [Select count() from Contact where MailingStreet = '999 Unit Street']);

            FamilyLoginController roller = new FamilyLoginController();

            roller.dummyContact.FirstName = 'unit';
            roller.dummyContact.LastName = 'test999';
            roller.dummyContact.MailingStreet = '999 Unit Street';

            roller.dummyContact.email = testContact.Email;
            roller.emailConfirm = testContact.Email;

            PageReference pr = roller.createAccount();
        Test.stopTest();

        // verify existing contact got updated
        System.assertEquals(null, pr);
        System.assertEquals(0, [Select count() from Contact where MailingStreet = '999 Unit Street']);
    }

    // update existing Contact, create User
    @isTest
    private static void updateAccounTest(){
        Contact testContact = new Contact(LastName = 'Unittest', AccountId = null, Email = 'newUser@drew.dru', recordTypeId=RecordTypes.parentContactTypeId);
        insert testContact;

        Test.startTest();

        FamilyLoginController roller = new FamilyLoginController();
        roller.username = testContact.Email;
        roller.password = 'xxxxx';

        // go to unrecognized page
        PageReference pr = roller.login();
        System.assert(pr.getURL().contains('familyloginupdateaccount'));
        System.assertEquals(testContact.Id, roller.existingContact.Id);

        roller.dummyContact.MailingStreet = '999 Unit Street';
        pr = roller.updateAccount();

        Test.stopTest();

        // User will not actually be created because this is a test
        testContact = [Select Id, MailingStreet, Household__c from Contact where Id = :testContact.Id];
        System.assertEquals('999 Unit Street', testContact.MailingStreet);
        // assert HH created
        System.assertNotEquals(null, testContact.Household__c);
        System.assertEquals(true, [Select IsDeleted FROM Account where Name = 'thisisatempaccount' all rows][0].IsDeleted);
    }

    // test Find Subscribers
    @isTest
    private static void findSubscribersTest(){
        Account alaska1 = new Account(Name = 'alaska1', BillingCountryCode = 'US', BillingStateCode = 'AK', SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest(), SSS_School_Code__c = 'alask1');
        Account alaska2 = new Account(Name = 'alaska2', BillingCountryCode = 'US', BillingStateCode = 'AK', SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest(), SSS_School_Code__c = 'alask2');
        Account alaska3 = new Account(Name = 'alaska3', BillingCountryCode = 'US', BillingStateCode = 'AK', SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest(), SSS_School_Code__c = 'alask3');
        Account alaska4Inactive = new Account(Name = 'alaska4', BillingCountryCode = 'US', BillingStateCode = 'AK', SSS_Subscriber_Status__c = 'inactive', SSS_School_Code__c = 'alask4');
        Account antarctica1 = new Account(Name = 'antarctica', BillingCountryCode = 'AQ', SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest(), SSS_School_Code__c = 'antrq1');
        insert new List<Account>{alaska1, alaska2, alaska3, alaska4Inactive, antarctica1};

        FamilyLoginController roller = new FamilyLoginController();

        roller.findSubscribers();
        System.assertEquals(false, roller.searchHasOccurred);
        System.assertEquals(false, roller.sssSearch);
        System.assertEquals(false, roller.locationSearch);
        System.assertEquals(0, roller.getAccountResults().size());

        roller.sssCode = 'alask1';
        roller.findSubscribers();
        System.assertEquals(true, roller.searchHasOccurred);
        System.assertEquals(true, roller.sssSearch);
        System.assertEquals(false, roller.locationSearch);
        System.assertEquals(1, roller.getAccountResults().size());

        roller.sssCode = null;
        roller.dummyAccount.BillingCountryCode = 'US';
        roller.dummyAccount.BillingStateCode = 'AK';
        roller.findSubscribers();
        System.assertEquals(true, roller.searchHasOccurred);
        System.assertEquals(false, roller.sssSearch);
        System.assertEquals(true, roller.locationSearch);
        System.assertEquals(3, roller.getAccountResults().size());

        roller.sssCode = '123';
        roller.dummyAccount.BillingCountryCode = 'US';
        roller.dummyAccount.BillingStateCode = 'AK';
        roller.findSubscribers();
        System.assertEquals(false, roller.searchHasOccurred);
        System.assertEquals(true, roller.sssSearch);
        System.assertEquals(true, roller.locationSearch);
        System.assertEquals(0, roller.getAccountResults().size());

        roller.sssCode = null;
        roller.dummyAccount.BillingCountryCode = 'AQ';
        roller.dummyAccount.BillingStateCode = null;
        roller.findSubscribers();
        System.assertEquals(true, roller.searchHasOccurred);
        System.assertEquals(false, roller.sssSearch);
        System.assertEquals(true, roller.locationSearch);
        System.assertEquals(1, roller.getAccountResults().size());
    }

    // test Find Subscribers
    @isTest
    private static void findSubscribersPILOTTest(){
        Account alaska1 = new Account(Name = 'alaska1', BillingCountryCode = 'US', BillingStateCode = 'AK', SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest(), SSS_School_Code__c = 'alask1', Pilot_School__c = true);
        Account alaska2 = new Account(Name = 'alaska2', BillingCountryCode = 'US', BillingStateCode = 'AK', SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest(), SSS_School_Code__c = 'alask2', Pilot_School__c = true);
        Account alaska3 = new Account(Name = 'alaska3', BillingCountryCode = 'US', BillingStateCode = 'AK', SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest(), SSS_School_Code__c = 'alask3');
        Account alaska4Inactive = new Account(Name = 'alaska4', BillingCountryCode = 'US', BillingStateCode = 'AK', SSS_Subscriber_Status__c = 'inactive', SSS_School_Code__c = 'alask4');
        Account antarctica1 = new Account(Name = 'antarctica', BillingCountryCode = 'AQ', SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest(), SSS_School_Code__c = 'antrq1');
        insert new List<Account>{alaska1, alaska2, alaska3, alaska4Inactive, antarctica1};

        FamilyLoginController roller = new FamilyLoginController();

        roller.sssCode = null;
        roller.dummyAccount.BillingCountryCode = 'US';
        roller.dummyAccount.BillingStateCode = 'AK';
        roller.findSubscribers();
        System.assertEquals(true, roller.searchHasOccurred);
        System.assertEquals(false, roller.sssSearch);
        System.assertEquals(true, roller.locationSearch);
        System.assertEquals(3, roller.getAccountResults().size());

        Family_Portal_Settings__c fps = new Family_Portal_Settings__c();
        fps.Name = 'Family';
        fps.In_Pilot__c = true;
        insert fps;

        roller = new FamilyLoginController();

        roller.sssCode = null;
        roller.dummyAccount.BillingCountryCode = 'US';
        roller.dummyAccount.BillingStateCode = 'AK';
        roller.findSubscribers();
        System.assertEquals(true, roller.searchHasOccurred);
        System.assertEquals(false, roller.sssSearch);
        System.assertEquals(true, roller.locationSearch);
        // we got 3 the first time, now we are expecting 2 (since only 2 are pilot schools)
        System.assertEquals(2, roller.getAccountResults().size());
    }

    // languages, cancel
    @isTest
    private static void miscMethods(){
        PageReference famLog = Page.FamilyLogin;
        famLog.getParameters().put('source','testenna');
        Test.setCurrentPage(famLog);
        insert new IntegrationSource__c(name='testenna',homePageMessage__c='huzzah!');
        FamilyLoginController roller = new FamilyLoginController();
        roller.getIntegrationSource();
        roller.getDocumentLogoUrl();
        roller.getDashBoardUrl();
        System.assertNotEquals(null, roller.getLanguages());
        System.assertNotEquals(null, roller.cancel());
    }

    // test forgot password
    @isTest
    private static void testForgotPasswordExistingUser(){
        //setupTestData();
        User schoolPortalUser = UserTestData.insertSchoolPortalUser();

        FamilyLoginController roller = new FamilyLoginController();

        roller.username = schoolPortalUser.Username;
        PageReference pr = roller.forgotPassword();
        // test will always be null because we're not in a site
        System.assertEquals(null, pr);
    }

    // test forgot password
    @isTest
    private static void testForgotPasswordNoExistingUserExistingContact(){
        Contact testContact = new Contact(LastName = 'Unittest', AccountId=null, Email = 'newUser@drew.dru', RecordTypeId=RecordTypes.parentContactTypeId);
        insert testContact;

        Test.startTest();

        FamilyLoginController roller = new FamilyLoginController();
        roller.username = testContact.Email;

        PageReference pr = roller.forgotPassword();
        // test will always be null because we're not in a site
        System.assertEquals(Page.FamilyLoginUpdateAccount.getURL(), pr.getURL());
    }

    /**
     * @description NAIS-536 Account Suspension
     *              email matches existing user with account suspended
     */
    @isTest
    private static void emailFoundOnActiveUserWithAccountSuspended(){
        Account family = TestUtils.createAccount('Individual Account', RecordTypes.individualAccountTypeId, 5, true);

        Contact c = TestUtils.createContact('Parent', family.Id, RecordTypes.parentContactTypeId, false);
        c.Email = 'test@parent.org';
        c.Account_Suspended__c = 'Yes';
        insert c;

        Id familyProfileId = [select Id from Profile where Name = :ProfileSettings.FamilyProfileName].Id;

        User u = TestUtils.createPortalUser('Parent', c.Email, 'tpg', c.Id, familyProfileId, true, true);

        FamilyLoginController controller = new FamilyLoginController();

        controller.username = c.Email;
        controller.password = 'xxxxxx';

        PageReference pr = controller.login();

        // account is suspended
        System.assertEquals(Page.FamilyLoginAccountSuspended.getUrl(), pr.getUrl());
    }

    @isTest
    private static void earlyAccess_noParam_expectNotEarlyAccess() {
        FamilyLoginController controller = new FamilyLoginController();

        Test.startTest();
        Boolean earlyAccessGranted = controller.earlyAccessGranted;
        Cookie earlyAccessCookie = ApexPages.currentPage().getCookies().get('earlyaccess');
        Test.stopTest();

        System.assert(!earlyAccessGranted, 'Expected there to not be early access granted.');
        System.assertEquals(null, earlyAccessCookie, 'Expected there to not be an early access cookie created.');
    }

    @isTest
    private static void earlyAccess_invalidParam_expectNotEarlyAccess() {
        ApexPages.currentPage().getParameters().put('earlyaccess', 'bad');
        FamilyLoginController controller = new FamilyLoginController();

        Test.startTest();
        Boolean earlyAccessGranted = controller.earlyAccessGranted;
        Cookie earlyAccessCookie = ApexPages.currentPage().getCookies().get('earlyaccess');
        Test.stopTest();

        System.assert(!earlyAccessGranted, 'Expected there to not be early access granted.');
        System.assertEquals(null, earlyAccessCookie, 'Expected there to not be an early access cookie created.');
    }

    @isTest
    private static void earlyAccess_validParam_expectEarlyAccessCookie() {
        Family_Portal_Settings__c fps = FamilyPortalSettingsTestData.Instance
            .asEarlyAccessOpened().DefaultFamilyPortalSettings;
        ApexPages.currentPage().getParameters().put(AccessService.EARLY_ACCESS_URL_PARAM, 'true');
        FamilyLoginController controller = new FamilyLoginController();

        Test.startTest();
        Cookie earlyAccessCookie = ApexPages.currentPage().getCookies().get(AccessService.EARLY_ACCESS_COOKIE);
        Test.stopTest();

        System.assertNotEquals(null, earlyAccessCookie, 'Expected there to be an early access cookie created.');
        System.assertEquals('true', earlyAccessCookie.getValue(), 'Expected the cookie to be set to true.');
    }

    @isTest
    private static void loginUser_expectNull() {
        String username = 'test@bogus.org';
        String password = 'password';

        FamilyLoginController controller = new FamilyLoginController();
        controller.username = username;
        controller.password = password;

        // We expect null because Salesforce Site.cls methods return null in unit tests.
        System.assertEquals(null, controller.loginUser(), 'Expected the page reference to be null.');
    }

    @isTest
    private static void enforceCommunityAccess_expectNull() {
        //Not a great way to test this with Site methods returning null in unit tests
        FamilyLoginController controller = new FamilyLoginController();
        System.assertEquals(null, controller.enforceCommunityAccess(),
                'Expected null since Site methods return null in unit tests');
    }
}