/**
* @ApplicantTriggerTest.cls
* @date 3/11/2014
* @author CH for Exponent Partners
* @description Unit tests for Applicant triggers
*/
@isTest
private class ApplicantTriggerTest
{
    
    @isTest
    private static void testSiblingCountUpdates(){
        Account testAccount = TestUtils.createAccount('Test Account', RecordTypes.individualAccountTypeId, 1, true);
        
        Contact parentA = TestUtils.createContact('Parent A', testAccount.Id, RecordTypes.parentContactTypeId, false);
        Contact student1 = TestUtils.createContact('Student 1', testAccount.Id, RecordTypes.studentContactTypeId, false);
        Contact student2 = TestUtils.createContact('Student 2', testAccount.Id, RecordTypes.studentContactTypeId, false);
        Contact student3 = TestUtils.createContact('Student 3', testAccount.Id, RecordTypes.studentContactTypeId, false);
        Contact student4 = TestUtils.createContact('Student 4', testAccount.Id, RecordTypes.studentContactTypeId, false);
        Contact student5 = TestUtils.createContact('Student 5', testAccount.Id, RecordTypes.studentContactTypeId, false);
        Database.insert(new List<Contact>{parentA, student1, student2, student3, student4, student5});
        
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        
        PFS__c testPFS = TestUtils.createPFS('Test PFS', academicYear.Id, parentA.Id, true);
        
        Applicant__c testApplicant1 = TestUtils.createApplicant(student1.Id, testPFS.Id, true);
        
        List<Applicant__c> verifyApplicants = [select Id, Number_of_Applicants__c from Applicant__c];
        System.assertEquals(1, verifyApplicants.size());
        System.assertEquals(1, verifyApplicants[0].Number_of_Applicants__c);
        
        Applicant__c testApplicant2 = TestUtils.createApplicant(student2.Id, testPFS.Id, false);
        Applicant__c testApplicant3 = TestUtils.createApplicant(student3.Id, testPFS.Id, false);
        Applicant__c testApplicant4 = TestUtils.createApplicant(student4.Id, testPFS.Id, false);
        Applicant__c testApplicant5 = TestUtils.createApplicant(student5.Id, testPFS.Id, false);
        
        Database.insert(new List<Applicant__c> {testApplicant2, testApplicant3, testApplicant4, testApplicant5});
        
        verifyApplicants = [select Id, Number_of_Applicants__c from Applicant__c];
        System.assertEquals(5, verifyApplicants.size());
        System.assertEquals(5, verifyApplicants[0].Number_of_Applicants__c);
        System.assertEquals(5, verifyApplicants[1].Number_of_Applicants__c);
        System.assertEquals(5, verifyApplicants[2].Number_of_Applicants__c);
        System.assertEquals(5, verifyApplicants[3].Number_of_Applicants__c);
        System.assertEquals(5, verifyApplicants[4].Number_of_Applicants__c);
        
        Database.delete(new List<Applicant__c>{testApplicant1, testApplicant3, testApplicant5});
        
        verifyApplicants = [select Id, Number_of_Applicants__c from Applicant__c];
        System.assertEquals(2, verifyApplicants.size());
        System.assertEquals(2, verifyApplicants[0].Number_of_Applicants__c);
        System.assertEquals(2, verifyApplicants[1].Number_of_Applicants__c);
    }

    @isTest
    private static void testSettingKSFields() {
        Account testAccount = TestUtils.createAccount('Test Account', RecordTypes.individualAccountTypeId, 1, true);
        
        Contact parentA = TestUtils.createContact('Parent A', testAccount.Id, RecordTypes.parentContactTypeId, false);
        Contact student1 = TestUtils.createContact('Student 1', testAccount.Id, RecordTypes.studentContactTypeId, false);
        Database.insert(new List<Contact>{parentA, student1});
        
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        
        PFS__c testPFS = TestUtils.createPFS('Test PFS', academicYear.Id, parentA.Id, true);
        
        Applicant__c testApplicant = TestUtils.createApplicant(student1.Id, testPFS.Id, true);
        
        String applicantId = testApplicant.Id;
        
        String queryString = 'select Id, Apply_for_Kipona_Scholars_Program__c, Apply_for_Kipona_Scholars_Program_Hist__c, '; 
                queryString += 'Apply_for_KS_K_12_Financial_Aid__c, Apply_for_KS_K_12_Financial_Aid_Hist__c, '; 
                queryString += 'Apply_for_KS_Preschool_Financial_Aid__c, Apply_for_KS_Preschool_Fin_Aid_Hist__c, ';  
                queryString += 'Apply_for_Pauahi_Keiki_Scholars_Cycle_1__c, Apply_for_PK_Scholars_Cycle_1_Hist__c, ';
                queryString += 'Apply_for_Pauahi_Keiki_Scholars_Cycle_2__c, Apply_for_PK_Scholars_Cycle_2_Hist__c ';
                queryString += 'from Applicant__c ';
                queryString += 'where Id = :applicantId';
        
        Test.startTest();
        
        // Make sure that the fields start out in the right status 
        testApplicant = Database.query(queryString);
        System.assertEquals(testApplicant.Apply_for_Kipona_Scholars_Program_Hist__c, 'Not Applied');
        System.assertEquals(testApplicant.Apply_for_KS_K_12_Financial_Aid_Hist__c, 'Not Applied');
        System.assertEquals(testApplicant.Apply_for_KS_Preschool_Fin_Aid_Hist__c, 'Not Applied');
        System.assertEquals(testApplicant.Apply_for_PK_Scholars_Cycle_1_Hist__c, 'Not Applied');
        System.assertEquals(testApplicant.Apply_for_PK_Scholars_Cycle_2_Hist__c, 'Not Applied');
        
        // Check applied status
        testApplicant.Apply_for_Kipona_Scholars_Program__c = true;
        testApplicant.Apply_for_KS_K_12_Financial_Aid__c = true;
        testApplicant.Apply_for_KS_Preschool_Financial_Aid__c = true;
        testApplicant.Apply_for_Pauahi_Keiki_Scholars_Cycle_1__c = true;
        testApplicant.Apply_for_Pauahi_Keiki_Scholars_Cycle_2__c = true;
        update testApplicant;
        
        testApplicant = Database.query(queryString);
        System.assertEquals(testApplicant.Apply_for_Kipona_Scholars_Program_Hist__c, 'Applied');
        System.assertEquals(testApplicant.Apply_for_KS_K_12_Financial_Aid_Hist__c, 'Applied');
        System.assertEquals(testApplicant.Apply_for_KS_Preschool_Fin_Aid_Hist__c, 'Applied');
        System.assertEquals(testApplicant.Apply_for_PK_Scholars_Cycle_1_Hist__c, 'Applied');
        System.assertEquals(testApplicant.Apply_for_PK_Scholars_Cycle_2_Hist__c, 'Applied');
        
        // Check withdrawn status
        testApplicant.Apply_for_Kipona_Scholars_Program__c = false;
        testApplicant.Apply_for_KS_K_12_Financial_Aid__c = false;
        testApplicant.Apply_for_KS_Preschool_Financial_Aid__c = false;
        testApplicant.Apply_for_Pauahi_Keiki_Scholars_Cycle_1__c = false;
        testApplicant.Apply_for_Pauahi_Keiki_Scholars_Cycle_2__c = false;
        update testApplicant;
        
        testApplicant = Database.query(queryString);
        System.assertEquals(testApplicant.Apply_for_Kipona_Scholars_Program_Hist__c, 'Withdrawn');
        System.assertEquals(testApplicant.Apply_for_KS_K_12_Financial_Aid_Hist__c, 'Withdrawn');
        System.assertEquals(testApplicant.Apply_for_KS_Preschool_Fin_Aid_Hist__c, 'Withdrawn');
        System.assertEquals(testApplicant.Apply_for_PK_Scholars_Cycle_1_Hist__c, 'Withdrawn');
        System.assertEquals(testApplicant.Apply_for_PK_Scholars_Cycle_2_Hist__c, 'Withdrawn');
        
        // Make sure Withdrawn status stays past a subsequent update
        update testApplicant;
        
        testApplicant = Database.query(queryString);
        System.assertEquals(testApplicant.Apply_for_Kipona_Scholars_Program_Hist__c, 'Withdrawn');
        System.assertEquals(testApplicant.Apply_for_KS_K_12_Financial_Aid_Hist__c, 'Withdrawn');
        System.assertEquals(testApplicant.Apply_for_KS_Preschool_Fin_Aid_Hist__c, 'Withdrawn');
        System.assertEquals(testApplicant.Apply_for_PK_Scholars_Cycle_1_Hist__c, 'Withdrawn');
        System.assertEquals(testApplicant.Apply_for_PK_Scholars_Cycle_2_Hist__c, 'Withdrawn');
                            
        Test.stopTest();
    }
}