public with sharing class SSSSiteLogoController
{

    public List<Global_Message__c> getSchoolGlobalMessages()
    {
        List<Global_Message__c> globalMessages = GlobalVariables.GlobalMessages('School Login', false);

        return globalMessages;
    }
}