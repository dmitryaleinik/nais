/*
* SPEC-101
*
* Document Management - School Portal
*
* Nathan, Exponent Partners, 2013
*/
public with sharing class SchoolDocumentController implements SchoolDocumentListInterface {

    /*** [SL] NAIS-934 - refactored code ***/
    
    public List<SchoolDocAssignWrapper> sdaWrapperList {get; set;}
    public School_PFS_Assignment__c schoolPFSAssignment {
        get; 
        
        set {
            // [SL] NAIS-934 - do the initialization in the setter, but make sure it only runs once
            if (schoolPFSAssignment == null) {
                // set the value
                schoolPFSAssignment = value;
                
                // load the list of sda wrappers
                sdaWrapperList = SchoolDocAssignWrapper.getOutstandingSchoolDocAssignWrappers(schoolPFSAssignment.Id);
            }
            
        }
    }
/*
    // input
    public String schoolPFSAssignId {
        get; 
        
        // [SL] NAIS-934 - do the initialization in the setter, but make sure it only runs once
        set {
            if (schoolPFSAssignId == null) {
                schoolPFSAssignId = value;
                
                // load the school pfs assignment
                for (School_PFS_Assignment__c spa : [SELECT Id, PFS_Document_Status__c, Applicant__r.PFS__c                                                
                                                        FROM School_PFS_Assignment__c 
                                                        WHERE Id = :schoolPFSAssignId LIMIT 1]) {
                    schoolPFSAssignment = spa;
                }
                
                // load the list of sda wrappers
                sdaWrapperList = SchoolDocAssignWrapper.getOutstandingSchoolDocAssignWrappers(schoolPFSAssignId);
            }    
        }
    }
*/    
    
    public String overallDocumentStatus {
        get {
            String status = null;
            if (schoolPFSAssignment != null) {
                status = schoolPFSAssignment.PFS_Document_Status__c;
            }
            return status;
        }
    }
    
    public String overallDocumentStatusColor {
        get {
            String color;
            if (overallDocumentStatus == 'Required Documents Complete') {
                color = 'green';
            }
            else if (overallDocumentStatus == 'Required Documents Received/In Progress') {
                color = 'green';
            }
            else if (overallDocumentStatus == 'Required Documents Outstanding') {
                color = 'red';
            }
            else if (overallDocumentStatus == 'No Documents Received') {
                color = 'red';
            }
            else {
                color = 'black';
            }
            
            return color;
        }
    }
    
    // [SL] NAIS-934: SchoolDocumentListInterface implementation
    public void editDocument() {}
    public void deleteDocument() {}
    public void uploadDocument() {}
    public void archiveDocument() {}//SFP-38, [G.S]
    
    public SchoolDocumentController getThisController() {
        return this;
    }

/*
    // wrapper class for document
    public class DocumentWrapper {
        public Required_Document__c requiredDocument {get; set;}
        public Family_Document__c familyDocument {get; set;}
        
        public DocumentWrapper() {
            requiredDocument = new Required_Document__c();
            familyDocument = new Family_Document__c();
        }
    }
    
    // output
    public List <DocumentWrapper> documentWrapperList {
        get {if (documentWrapperList == null) {loadDocuments();} return documentWrapperList;}    
        private set;
    }
    public List <DocumentWrapper> outstandingDocumentWrapperList {
        get {if (outstandingDocumentWrapperList == null) {loadDocuments();} return outstandingDocumentWrapperList;}    
        private set;
    }

    public String overallDocumentStatus {get; private set;}
    public String overallDocumentStatusColor {get; private set;}
    public String requiredDocumentId {get; set;}

    public School_PFS_Assignment__c schoolPFSAssignment = new School_PFS_Assignment__c();
    public List <School_Document_Assignment__c> schoolDocumentAssignmentList = new List <School_Document_Assignment__c>();
    public Map <String, Required_Document__c> id_requiredDocument_map = new Map <String, Required_Document__c>();
    public Map <String, Family_Document__c> id_familyDocument_map = new Map <String, Family_Document__c>();
    public Map <String, List <School_Document_Assignment__c>> fdId_sdaList_map = new Map <String, List <School_Document_Assignment__c>>();

    List <Required_Document__c> requiredDocumentList = new List <Required_Document__c>();
    
    Set <String> requiredDocumentIdSet = new Set <String>();
    Set <String> requiredDocumenReceivedtIdSet = new Set <String>();
    Set <String> familyDocumentIdSet = new Set <String>();
    Set <String> householdIdSet = new Set <String>();
    Map <String, List <School_Document_Assignment__c>> reqDocId_sdaList_map = new Map <String, List <School_Document_Assignment__c>>();
    
    // constructor
    public SchoolDocumentController() {}

    private void loadDocuments() {
        loadSchoolPFSAssignment();
        loadRequiredDocument();
        loadFamilyDocument();
        loadNotReceivedDocuments();
        loadSDADocuments();
        //loadHouseholdDocuments(); 
        determineOverallStatus(); 
        
        documentWrapperList = sortDocumentWrapperList(documentWrapperList);  
        outstandingDocumentWrapperList = sortDocumentWrapperList(outstandingDocumentWrapperList);  
    }

    public PageReference uploadOutstandingDocument() {
    
        PageReference docPageRef = Page.SchoolPFSDocument;    
        if (schoolPFSAssignment != null && schoolPFSAssignment.Id != null) {
            docPageRef.getParameters().put('schoolPFSAssignmentId', schoolPFSAssignment.Id);
            docPageRef.getParameters().put('requiredDocumentId', requiredDocumentId);
            docPageRef.getParameters().put('upload', 'true');
        }
        // prepare return page
        PageReference retPageRef = Page.SchoolPFSSummary;
        retPageRef.getParameters().put('schoolpfsassignmentid', schoolPFSAssignment.Id);
        docPageRef.getParameters().put('retURL', retPageRef.getURL());
        
        docPageRef.setRedirect(true);
        
        return docPageRef;    
    }
    
    private void loadSchoolPFSAssignment() {
        
        // init
        householdIdSet = new Set <String>();
        
        if (schoolPFSAssignId != null) {    
            List <School_PFS_Assignment__c> spfsaList = [select Id, Name, 
                Academic_Year__c,
                Applicant__r.Contact__r.Household__c,
                Applicant__r.PFS__r.Parent_A__r.Household__c,
                Applicant__r.PFS__r.Parent_B__r.Household__c,
                Applicant__r.PFS__r.PFS_Number__c,
                School__c, School__r.Name,
                Student_Folder__c,
                (select Id, Name, 
                    Document__c, 
                    Document__r.Date_Received__c,
                    Document__r.Date_Verified__c,
                    Document__r.Deleted__c,
                    Document__r.Description__c,
                    Document__r.Document_Pertains_to__c,
                    Document__r.Document_Source__c,
                    Document__r.Document_Status__c,
                    Document__r.Document_Type__c,
                    Document__r.Document_Year__c,
                    Document__r.Issue_Categories__c,
                    Document__r.Name,
                    Document__r.Notes__c,
                    Document__r.Other_Document_Label__c,
                    Document__r.Reason_for_Request__c,
                    Document__r.Request_to_Waive_Requirement__c,
                    Required_Document__c,
                    Required_Document__r.Document_Type__c, 
                    School_PFS_Assignment__c
                    from School_Document_Assignments__r
                    where Deleted__c = false
                    order by Document__r.Document_Type__c asc, 
                        Document__r.Document_Year__c desc)
                from School_PFS_Assignment__c
                where Id = :schoolPFSAssignId];
            if (spfsaList.size() > 0) {                
                schoolPFSAssignment = spfsaList[0];
                schoolDocumentAssignmentList = spfsaList[0].School_Document_Assignments__r;
                householdIdSet.add(schoolPFSAssignment.Applicant__r.Contact__r.Household__c);
                householdIdSet.add(schoolPFSAssignment.Applicant__r.PFS__r.Parent_A__r.Household__c);
                householdIdSet.add(schoolPFSAssignment.Applicant__r.PFS__r.Parent_B__r.Household__c);
                householdIdSet.remove(null);    
            }
        }
    }
    
    // get all required document for school
    private void loadRequiredDocument() {

        // init
        requiredDocumentIdSet = new Set <String>();
        id_requiredDocument_map = new Map <String, Required_Document__c>();

        if (schoolPFSAssignment != null && schoolPFSAssignment.School__c != null) {    
            
            requiredDocumentList = [select Id, Name,
                Document_Type__c,
                Document_Year__c,
                Label_for_School_Specific_Document__c,
                School__c,
                 (select Id, Name, Document__c, Document__r.Deleted__c, Document__r.Document_Status__c, Required_Document__r.Document_Type__c 
                    from School_Document_Assignments__r
                    where Deleted__c = false and // Document__c != null and Document__r.Deleted__c = false and 
                        School_PFS_Assignment__c = :schoolPFSAssignId)
            
            from Required_Document__c 
            where School__c = :schoolPFSAssignment.School__c and
                Academic_Year__c = :schoolPFSAssignment.Academic_Year__c and
                Deleted__c = false
            order by Document_Type__c asc, Label_for_School_Specific_Document__c asc, Document_Year__c desc];
            
            for (Required_Document__c rd : requiredDocumentList) {
                if (rd.School_Document_Assignments__r.size() > 0) {
                    requiredDocumentIdSet.add(rd.Id);
                    id_requiredDocument_map.put(rd.Id, rd);
                    reqDocId_sdaList_map.put(rd.Id, rd.School_Document_Assignments__r);
                }
            }    
        }
    }
    
    // get all received documents for required documents
    private void loadFamilyDocument() {
    
        // init
        familyDocumentIdSet = new Set <String>();
        documentWrapperList = new List <DocumentWrapper>();
        requiredDocumenReceivedtIdSet = new Set <String>();
        id_familyDocument_map = new Map <String, Family_Document__c>();
        fdId_sdaList_map = new Map <String, List <School_Document_Assignment__c>>();
        
        List <School_Document_Assignment__c> sdaList = [select Id, Name,
            Deleted__c,
            Document__c,
            Document__r.Date_Received__c,
            Document__r.Date_Verified__c,
            Document__r.Deleted__c,
            Document__r.Description__c,
            Document__r.Document_Pertains_to__c,
            Document__r.Document_Source__c,
            Document__r.Document_Status__c,
            Document__r.Document_Type__c,
            Document__r.Document_Year__c,
            Document__r.Issue_Categories__c,
            Document__r.Name,
            Document__r.Notes__c,
            Document__r.Other_Document_Label__c,
            Document__r.Reason_for_Request__c,
            Document__r.Request_to_Waive_Requirement__c,
            Required_Document__c,
            Required_Document__r.Deleted__c,
            Required_Document__r.Document_Type__c,
            Required_Document__r.Label_for_School_Specific_Document__c,
            Requirement_Waiver_Comments__c,
            Requirement_Waiver_Status__c
            
            from School_Document_Assignment__c
            where School_PFS_Assignment__c = :schoolPFSAssignment.Id and 
                Deleted__c = false and
                Document__c != null and
                Document__r.Deleted__c = false
            order by Required_Document__r.Document_Type__c asc, 
                Required_Document__r.Label_for_School_Specific_Document__c asc, 
                Required_Document__r.Document_Year__c desc];
        
        for (School_Document_Assignment__c sda : sdaList) {
            
            if (sda.Document__c != null) {
                id_familyDocument_map.put(sda.Document__c, sda.Document__r);
            }
            
            if (requiredDocumentIdSet.contains(sda.Required_Document__c)) {
                requiredDocumenReceivedtIdSet.add(sda.Required_Document__c);
                
                // add only once
                if (familyDocumentIdSet.add(sda.Document__c)) {
                
                    DocumentWrapper dw = new DocumentWrapper();
                    dw.requiredDocument = sda.Required_Document__r;
                    dw.familyDocument = sda.Document__r;
                
                    documentWrapperList.add(dw);
                }
            }
            
            List <School_Document_Assignment__c> sdaL;
            if (fdId_sdaList_map.containsKey(sda.Document__c)) {
                sdaL = fdId_sdaList_map.get(sda.Document__c);
            } else {
                sdaL = new List <School_Document_Assignment__c>();
            }
            sdaL.add(sda);
            fdId_sdaList_map.put(sda.Document__c, sdaL);
        }    
    }

    // get all not received documents for required documents
    private void loadNotReceivedDocuments() {
    
        outstandingDocumentWrapperList = new List <DocumentWrapper>();
        
        for (Required_Document__c rd : requiredDocumentList) {
            if (requiredDocumenReceivedtIdSet.contains(rd.Id) == false) {
                if (id_requiredDocument_map.containsKey(rd.Id)) {
                    DocumentWrapper dw = new DocumentWrapper();
                    dw.requiredDocument = id_requiredDocument_map.get(rd.Id);
                    
                    documentWrapperList.add(dw);
                    outstandingDocumentWrapperList.add(dw);
                }
            }
        }
    }

    // get remaining documents through school document assignment
    private void loadSDADocuments() {
    
        for (School_Document_Assignment__c sda : schoolDocumentAssignmentList) {
            if (sda.Document__c != null && sda.Document__r.Deleted__c == false) {
                // add only once
                if (familyDocumentIdSet.add(sda.Document__c)) {
                    DocumentWrapper dw = new DocumentWrapper();
                    dw.familyDocument = sda.Document__r;
                    
                    documentWrapperList.add(dw);
                }
            }
        }
    }
    
    // get remaining documents through household
    private void loadHouseholdDocuments() {
        
        if (householdIdSet.size() > 0) {
            List <Family_Document__c> fdList = [select Id, Name,
                Date_Received__c,
                Date_Verified__c,
                Deleted__c,
                Description__c,
                Document_Pertains_to__c,
                Document_Source__c,
                Document_Status__c,
                Document_Type__c,
                Document_Year__c,
                Household__c,
                Issue_Categories__c,
                Notes__c,
                Other_Document_Label__c,
                Reason_for_Request__c,
                Request_to_Waive_Requirement__c
            from Family_Document__c where Household__c in :householdIdSet and Deleted__c = false];
            
            for (Family_Document__c fd : fdList) {
                id_familyDocument_map.put(fd.Id, fd);
                // add only once
                if (familyDocumentIdSet.add(fd.Id)) {
                    DocumentWrapper dw = new DocumentWrapper();
                    dw.familyDocument = fd;
                    
                    documentWrapperList.add(dw);
                }
            }
        }        
    }

    // determine overall status of documents
    private void determineOverallStatus() {
    
        Boolean areAllFamDocsAvailable = true;
        Boolean areAllFamDocsProcessed = true;
        Boolean atleastOneFamDocAvailable = false;
        
        
        Set <String> nonVerifyDocTypeSet = new Set <String>();
        
// refactored as part of NAIS-282 DP [6.20.13]
//        // load non verifiable document types from custom settings
//        List <Document_Custom_Settings__c> csList = Document_Custom_Settings__c.getAll().values();
//        for (Document_Custom_Settings__c cs : csList) {
//            if (cs.Value__c != null && cs.Active__c == true) {
//                if (cs.Name.startsWith('Non_Verifiable_Document_Type')) {
//                    for (String docType : cs.Value__c.split(';')) {
//                        if (docType != null && docType.trim() != null) {
//                            nonVerifyDocTypeSet.add(docType.trim());
//                        }
//                    }
//                }
//            }
//        }
//
        
        for (String reqDocId : requiredDocumentIdSet) {
            if (requiredDocumenReceivedtIdSet.contains(reqDocId) == false) {
                // required document missing
                areAllFamDocsAvailable = false;
            } else if (requiredDocumenReceivedtIdSet.size() > 0) {
                // required document available
                atleastOneFamDocAvailable = true;
                // are all family documents assigned to required document have status Processed ?
                if (reqDocId_sdaList_map.containsKey(reqDocId)) {
                    List <School_Document_Assignment__c> sdaList = reqDocId_sdaList_map.get(reqDocId);
                    for (School_Document_Assignment__c sda : sdaList) {
                        // added criteria to exempt "School-Specific Documents"
                        // changed method for seeing if doc is verifiable -- only verifiable docs need to be processed
                        //if (sda.Document__r.Document_Status__c != 'Processed' && sda.Required_Document__r.Document_Type__c != 'School-Specific Document' && !nonVerifyDocTypeSet.contains(sda.Required_Document__r.Document_Type__c)){
                        if (sda.Document__r.Document_Status__c != 'Processed' && sda.Required_Document__r.Document_Type__c != 'School-Specific Document' && GlobalVariables.isVerifiableDocumentType(sda.Required_Document__r.Document_Type__c)){
                            areAllFamDocsProcessed = false;
                        }
                    }
                }
            }
        } 
        
        if (areAllFamDocsAvailable == true) { 
            if (areAllFamDocsProcessed == true) {
                overallDocumentStatus = 'Required Documents Complete';
                overallDocumentStatusColor = 'green';
            } else {
                overallDocumentStatus = 'Required Documents Received/In Progress';
                overallDocumentStatusColor = 'green';
            }
        } else {
            if (atleastOneFamDocAvailable == true) {
                overallDocumentStatus = 'Required Documents Outstanding';
                overallDocumentStatusColor = 'red';
            } else {
                overallDocumentStatus = 'No Documents Received';
                overallDocumentStatusColor = 'red';
            }
        }  
    }

    private List <DocumentWrapper> sortDocumentWrapperList(List <DocumentWrapper> dwList) {
    
        // NAIS-283
        // The documents should be listed be priority and then beyond that alphabetically
        
        Map <String, List <DocumentWrapper>> docType_dwList_map = new Map <String, List <DocumentWrapper>>(); 
        List <DocumentWrapper> dwListSorted = new List <DocumentWrapper>();   
        Set <String> docTypeAddedSet = new Set <String>();
        
        // load custom setting
        List <Document_Custom_Settings__c> csList = [select Name, Value__c from Document_Custom_Settings__c
            where Active__c = true and Name like 'Document_Type_Order%' and Value__c != null
            order by Name];
        
        for (DocumentWrapper dw : dwList) {
            String docType = '';    // put empty string. i think null is not allowed in key
            if (dw.familyDocument != null && dw.familyDocument.Id != null && dw.familyDocument.Document_Type__c != null) {
                docType = dw.familyDocument.Document_Type__c;
            } else if (dw.requiredDocument != null && dw.requiredDocument.Id != null && dw.requiredDocument.Document_Type__c != null) {
                docType = dw.requiredDocument.Document_Type__c;
            }
            
            List <DocumentWrapper> dwListLocal = new List <DocumentWrapper>();
            if (docType_dwList_map.containsKey(docType)) {
                dwListLocal = docType_dwList_map.get(docType);
            }
            dwListLocal.add(dw);
            docType_dwList_map.put(docType, dwListLocal);
        } 
        
        // add doucment types specified in custom setting
        for (Document_Custom_Settings__c cs : csList) {
        
            if (docType_dwList_map.containsKey(cs.Value__c)) {
                if (docTypeAddedSet.add(cs.Value__c)) {
                    dwListSorted.addAll(docType_dwList_map.get(cs.Value__c));
                }
            }
        } 
        
        // add school specific types
        if (docType_dwList_map.containsKey('School-Specific Document')) {
            if (docTypeAddedSet.add('School-Specific Document')) {
                dwListSorted.addAll(docType_dwList_map.get('School-Specific Document'));
            }
        }
        
        // add remaining tax schedules
        List <String> docTypeList = new List <String>(docType_dwList_map.keySet());
        docTypeList.sort(); 
        
        for (String docType : docTypeList) {
            if (docType.startsWith('Tax Schedule')) {
                if (docTypeAddedSet.add(docType)) {
                    dwListSorted.addAll(docType_dwList_map.get(docType));
                }
            }
        }
        
        // add remaining document types
        for (String docType : docTypeList) {
            if (docTypeAddedSet.add(docType)) {
                dwListSorted.addAll(docType_dwList_map.get(docType));
            }
        }
        
        return dwListSorted;
    }
    */
    
    
    
}