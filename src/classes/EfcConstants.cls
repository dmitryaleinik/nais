public with sharing class EfcConstants {

    private static Map<Id, SSS_Constants__c> academicYearIdToSssConstantsMap = new Map<Id, SSS_Constants__c>();

    public static Integer efcReCalcBatchSize {get {return 10;}}

    public static SSS_Constants__c getSssConstantsForYear(Id academicYearId) {

        SSS_Constants__c sssConstants = null;
        if (academicYearId != null) {
            sssConstants = academicYearIdToSssConstantsMap.get(academicYearId);
            if (sssConstants == null) {
                for (SSS_Constants__c c : [SELECT Academic_Year__r.Name,
                                                Boarding_School_Food_Allowance__c, Default_COLA_Value__c, Exemption_Allowance__c,
                                                Medical_Dental_Allowance_Percent__c,
                                                Medicare_Tax_Rate__c, Negative_Contribution_Cap_Constant__c, Percentage_for_Imputing_Assets__c,
                                                Std_Deduction_Head_of_Household__c, Std_Deduction_Joint_Surviving__c, Std_Deduction_Single_Filing_Separately__c,
                                                Employment_Allowance_Maximum__c, IPA_For_Each_Additional__c, Social_Security_Tax_Rate__c,
                                                IPA_Housing_For_Each_Additional__c, IPA_Other_For_Each_Additional__c, IPA_Housing_Multiplier_for_Additional__c, // NAIS-1885 Adding for separate calculation of IPA
                                                Social_Security_Tax_Threshold__c,
                                                Medicare_Tax_Rate_Married_Joint__c, Medicare_Tax_Rate_Married_Separate__c, Medicare_Threshold__c,
                                                Medicare_Threshold_Married_Joint__c, Medicare_Threshold_Married_Separate__c, Medicare_Threshold_Rate__c,
                                                Medicare_Threshold_Rate_Married_Joint__c, Medicare_Threshold_Rate_Married_Separate__c,
                                                Self_Employment_Tax_Rate__c
                                            FROM SSS_Constants__c
                                            WHERE Academic_Year__c = :academicYearId LIMIT 1]) {
                    sssConstants = c;
                }
                academicYearIdToSssConstantsMap.put(academicYearId, sssConstants);
            }
        }

        if (sssConstants == null) {
            throw new EfcException('Missing SSS Constants for academic year \'' + EfcUtil.getAcademicYearName(academicYearId) + '\'');
        }

        return sssConstants;
    }

    public static Decimal getExemptionAllowance(Id academicYearId) {

        SSS_Constants__c sssConstants = getSssConstantsForYear(academicYearId);
        Decimal exemptionAllowance = sssConstants.Exemption_Allowance__c;
        if (exemptionAllowance == null) {
            throw new EfcException('Missing SSS Constant value for academic year ' + sssConstants.Academic_Year__r.Name + ': Exemption Allowance');
        }

        return exemptionAllowance;
    }

    public static boolean isFilingMarriedSeparated(String filingStatus, String filingStatusParentB)
    {
        if((filingStatus == EfcPicklistValues.FILING_STATUS_MARRIED_SEPARATE)
        ||((filingStatus == EfcPicklistValues.FILING_STATUS_SINGLE
            || filingStatus == EfcPicklistValues.FILING_STATUS_HEAD_OF_HOUSEHOLD)
        && (filingStatusParentB == EfcPicklistValues.FILING_STATUS_SINGLE
            || filingStatusParentB == EfcPicklistValues.FILING_STATUS_HEAD_OF_HOUSEHOLD))
        ){
            return true;
        }
        return false;
    }//End:isFilingMarriedSeparated

    public static Decimal getStandardDeductionForFilingStatus(Id academicYearId, String filingStatus, String filingStatusParentB) {

        SSS_Constants__c sssConstants = getSssConstantsForYear(academicYearId);

        Decimal standardDeduction = null;
        if ((filingStatus == EfcPicklistValues.FILING_STATUS_SINGLE)
            || (EfcConstants.isFilingMarriedSeparated(filingStatus, filingStatusParentB)))  {
            standardDeduction = sssConstants.Std_Deduction_Single_Filing_Separately__c;
        }
        else if (filingStatus == EfcPicklistValues.FILING_STATUS_MARRIED_JOINT
        || filingStatus== EfcPicklistValues.FILING_STATUS_QUALIFYING_WINDOW_WITH_CHILD) {
            standardDeduction = sssConstants.Std_Deduction_Joint_Surviving__c;
        }
        else if (filingStatus == EfcPicklistValues.FILING_STATUS_HEAD_OF_HOUSEHOLD) {
            standardDeduction = sssConstants.Std_Deduction_Head_of_Household__c;
        }

        if (standardDeduction == null) {
            throw new EfcException('Missing SSS Constant value for academic year ' + sssConstants.Academic_Year__r.Name
                                        + ': Standard Deduction (filing status = ' + filingStatus + ')');
        }
        return standardDeduction;
    }

    public static Decimal getMedicareTaxRate(Id academicYearId) {
        SSS_Constants__c sssConstants = getSssConstantsForYear(academicYearId);
        Decimal medicareTaxRate = sssConstants.Medicare_Tax_Rate__c;
        if (medicareTaxRate == null) {
            throw new EfcException('Missing SSS Constant value for academic year ' + sssConstants.Academic_Year__r.Name + ': Medicare Tax Rate');
        }

        return medicareTaxRate;
    }

    public static Decimal getEmploymentAllowanceMaximum(Id academicYearId) {
        SSS_Constants__c sssConstants = getSssConstantsForYear(academicYearId);
        Decimal maxAllowance = sssConstants.Employment_Allowance_Maximum__c;
        if (maxAllowance == null) {
            throw new EfcException('Missing SSS Constant value for academic year ' + sssConstants.Academic_Year__r.Name + ': Employment Allowance Maximum');
        }

        return maxAllowance;
    }

    // [CH] NAIS-1850 Look up the self employment tax rate from SSSConstants
    public static Decimal getSelfEmploymentTaxRate(Id academicYearId){
        SSS_Constants__c sssConstants = getSssConstantsForYear(academicYearId);
        Decimal seTaxRateMultiplier;
        if(sssConstants != null && sssConstants.Self_Employment_Tax_Rate__c != null){
            seTaxRateMultiplier = sssConstants.Self_Employment_Tax_Rate__c;
        }
        else{
            seTaxRateMultiplier = 15.3;
        }

        return seTaxRateMultiplier;
    }

    public static Decimal getMedicalDentalAllowancePercent(Id academicYearId) {
        SSS_Constants__c sssConstants = getSssConstantsForYear(academicYearId);
        Decimal percent = sssConstants.Medical_Dental_Allowance_Percent__c;
        if (percent == null) {
            throw new EfcException('Missing SSS Constant value for academic year ' + sssConstants.Academic_Year__r.Name + ': Medical/Dental Allowance Percent');
        }

        return percent;
    }

    public static Decimal getDefaultColaValue(Id academicYearId) {
        SSS_Constants__c sssConstants = getSssConstantsForYear(academicYearId);
        Decimal defaultColaValue = sssConstants.Default_COLA_Value__c;
        if (defaultColaValue == null) {
            throw new EfcException('Missing SSS Constant value for academic year ' + sssConstants.Academic_Year__r.Name + ': Default COLA Value');
        }

        return defaultColaValue;
    }

    public static Decimal getIpaForEachAdditional(Id academicYearId) {
        SSS_Constants__c sssConstants = getSssConstantsForYear(academicYearId);
        Decimal allowance = sssConstants.IPA_For_Each_Additional__c;
        if (allowance == null) {
            throw new EfcException('Missing SSS Constant value for academic year ' + sssConstants.Academic_Year__r.Name + ': IPA For Each Additional');
        }

        return allowance;
    }

    public static Decimal getBoardingSchoolFoodAllowance(Id academicYearId) {
        SSS_Constants__c sssConstants = getSssConstantsForYear(academicYearId); // 1834
        Decimal allowance = sssConstants.Boarding_School_Food_Allowance__c;
        if (allowance == null) {
            throw new EfcException('Missing SSS Constant value for academic year ' + sssConstants.Academic_Year__r.Name + ': Boarding School Food Allowance');
        }

        return allowance;
    }

    public static Decimal getNegativeContributionCapConstant(Id academicYearId) {
        SSS_Constants__c sssConstants = getSssConstantsForYear(academicYearId);
        Decimal negativeContributionCap = sssConstants.Negative_Contribution_Cap_Constant__c;
        if (negativeContributionCap == null) {
            throw new EfcException('Missing SSS Constant value for academic year ' + sssConstants.Academic_Year__r.Name + ': Negative Contribution Cap Constant');
        }

        return negativeContributionCap;
    }

    public static Decimal getSocialSecurityTaxRate(Id academicYearId) {
        SSS_Constants__c sssConstants = getSssConstantsForYear(academicYearId);
        Decimal socialSecurityTaxRate = sssConstants.Social_Security_Tax_Rate__c;
        if (socialSecurityTaxRate == null) {
            throw new EfcException('Missing SSS Constant value for academic year ' + sssConstants.Academic_Year__r.Name + ': Social Security Tax Rate');
        }

        return socialSecurityTaxRate;
    }

    public static Decimal getSocialSecurityTaxThreshold(Id academicYearId) {
        SSS_Constants__c sssConstants = getSssConstantsForYear(academicYearId);
        Decimal socialSecurityTaxThreshold = sssConstants.Social_Security_Tax_Threshold__c;
        if (socialSecurityTaxThreshold == null) {
            throw new EfcException('Missing SSS Constant value for academic year ' + sssConstants.Academic_Year__r.Name + ': Social Security Tax Threshold');
        }

        return socialSecurityTaxThreshold;
    }

    public static Decimal getPercentageForImputingAssets(Id academicYearId) {
        SSS_Constants__c sssConstants = getSssConstantsForYear(academicYearId);
        Decimal percentageForImputingAssets = sssConstants.Percentage_for_Imputing_Assets__c;
        if (percentageForImputingAssets == null) {
            throw new EfcException('Missing SSS Constant value for academic year ' + sssConstants.Academic_Year__r.Name + ': Percentage For Imputing Assets');
        }

        return percentageForImputingAssets;
    }

    // NAIS-1202: Additional Medicare Rates
    public static Decimal getMedicareRateForFilingStatus(Id academicYearId, String filingStatus, String filingStatusParentB) {
        SSS_Constants__c sssConstants = getSssConstantsForYear(academicYearId);
        Decimal medicareRate = null;
        if (EfcConstants.isFilingMarriedSeparated(filingStatus, filingStatusParentB))  {
            medicareRate = sssConstants.Medicare_Tax_Rate_Married_Separate__c;
        }
        else if (filingStatus == EfcPicklistValues.FILING_STATUS_MARRIED_JOINT
        || filingStatus== EfcPicklistValues.FILING_STATUS_QUALIFYING_WINDOW_WITH_CHILD) {
            medicareRate = sssConstants.Medicare_Tax_Rate_Married_Joint__c;
        }

        // default value
        if (medicareRate == null) {
            medicareRate = sssConstants.Medicare_Tax_Rate__c;
        }

        if (medicareRate == null) {
            throw new EfcException('Missing SSS Constant value for academic year ' + sssConstants.Academic_Year__r.Name
                                        + ': Medicare Rate (filing status = ' + filingStatus + ')');
        }
        return medicareRate;
    }

    public static Decimal getMedicareThresholdForFilingStatus(Id academicYearId, String filingStatus, String filingStatusParentB) {
        SSS_Constants__c sssConstants = getSssConstantsForYear(academicYearId);
        Decimal medicareThreshold = null;
        if (EfcConstants.isFilingMarriedSeparated(filingStatus, filingStatusParentB))  {
            medicareThreshold = sssConstants.Medicare_Threshold_Married_Separate__c;
        }
        else if (filingStatus == EfcPicklistValues.FILING_STATUS_MARRIED_JOINT
        || filingStatus== EfcPicklistValues.FILING_STATUS_QUALIFYING_WINDOW_WITH_CHILD) {
            medicareThreshold = sssConstants.Medicare_Threshold_Married_Joint__c;
        }

        // default value
        if (medicareThreshold == null){
            medicareThreshold = sssConstants.Medicare_Threshold__c;
        }

        return medicareThreshold;
    }

    public static Decimal getMedicareThresholdRateForFilingStatus(Id academicYearId, String filingStatus, String filingStatusParentB) {
        SSS_Constants__c sssConstants = getSssConstantsForYear(academicYearId);
        Decimal medicareThresholdRate = null;
        if (EfcConstants.isFilingMarriedSeparated(filingStatus, filingStatusParentB))  {
            medicareThresholdRate = sssConstants.Medicare_Threshold_Rate_Married_Separate__c;
        }
        else if (filingStatus == EfcPicklistValues.FILING_STATUS_MARRIED_JOINT
        || filingStatus== EfcPicklistValues.FILING_STATUS_QUALIFYING_WINDOW_WITH_CHILD) {
            medicareThresholdRate = sssConstants.Medicare_Threshold_Rate_Married_Joint__c;
        }

        // default value
        if (medicareThresholdRate == null) {
            medicareThresholdRate = sssConstants.Medicare_Threshold_Rate__c;
        }

        if (medicareThresholdRate == null) {
            throw new EfcException('Missing SSS Constant value for academic year ' + sssConstants.Academic_Year__r.Name
                                        + ': Medicare Threshold Rate (filing status = ' + filingStatus + ')');
        }
        return medicareThresholdRate;
    }
}