/**
 * NAIS-967 Roll Up Subscription Status and Type to Account
 *
 * 1) set Account.SSS_Subscriber_Status__c
 *    no subscription records for the account --> Never
 *    no current subscription records and at least one expired or cancelled --> Former
 *    most recent subscription record is current (start date today or in the past, end date in the future and not cancelled) --> Current
 *
 * 2) set Account.SSS_Subscription_Type_Current__c
 *    most recent subscription is current --> Subscription.Subscription_Type__c
 *    most recent subscription is not current --> null (suppressed on 2015-05-04 for NAIS-2373)
 *
 * WH, Exponent Partners, 2013
 **/
public class AccountAction {
    public static void rollupSubscriptionStatus(List<Id> accountIds) {

        List<Account> accountsToUpdate = new List<Account>();

        // find all subscriptions of each account, sorted by descending order of start date (required in page layout)
        for (Account a : [select Id, SSS_Subscriber_Status__c, SSS_Subscription_Type_Current__c,
                (select Status__c, Subscription_Type__c from Subscriptions__r where Start_Date__c <= TODAY order by Start_Date__c desc)
                from Account where Id in :accountIds]) {

            if (a.Subscriptions__r.size() == 0) {

                // no subscriptions starting <= today
                if (a.SSS_Subscriber_Status__c != 'Never' || a.SSS_Subscription_Type_Current__c != null) {
                    a.SSS_Subscriber_Status__c = 'Never';
                    a.SSS_Subscription_Type_Current__c = null;
                    accountsToUpdate.add(a);
                }

            } else {

                // look for a current subscription
                Boolean isCurrent = false;
                for (Subscription__c s : a.Subscriptions__r) {
                    if (s.Status__c == 'Current') {
                        if (a.SSS_Subscriber_Status__c != 'Current' || a.SSS_Subscription_Type_Current__c != s.Subscription_Type__c) {
                            a.SSS_Subscriber_Status__c = 'Current';
                            a.SSS_Subscription_Type_Current__c = s.Subscription_Type__c;
                            accountsToUpdate.add(a);
                        }
                        isCurrent = true;
                        break;
                    }
                }

                // if no current, must be expired or cancelled subscriptions
                if (!isCurrent) {
                    if (a.SSS_Subscriber_Status__c != 'Former' /* || a.SSS_Subscription_Type_Current__c != null */) {
                        a.SSS_Subscriber_Status__c = 'Former';
//                        a.SSS_Subscription_Type_Current__c = null; // NAIS-2373 maintain prior subscription type after expiration
                        accountsToUpdate.add(a);
                    }
                }

            }
        }

        if (!accountsToUpdate.isEmpty()) {
            update accountsToUpdate;
        }
    }

    // [CH] NAIS-1766 Creates a new public group for school accounts that are added
    // [GH] NAIS-2094 Adjusts length of Account name when account is created so Acc Name + SSS School Code will not exceed length of 40.
    public static void generatePublicGroups(Map<Id, Account> accountMap)
    {
        // Null-safe just in case
        if(accountMap != null){
            List<Group> groupsToInsert = new List<Group>();

            // Create a new Group for each Account passed in
            for(Id accountId : accountMap.keySet())
            {
                Account currentAccount = accountMap.get(accountId);
                // Only create sharing groups for School Accounts
                if (currentAccount.RecordTypeId == RecordTypes.schoolAccountTypeId
                    || currentAccount.RecordTypeId == RecordTypes.accessOrgAccountTypeId)
                {
                    Group newGroup = new Group();
                    newGroup.DeveloperName = 'X' + accountId;

                    setGroupName(currentAccount, newGroup);
                    // Add it to the list of Group records to insert
                    groupsToInsert.add(newGroup);
                }
            }
            Database.insert(groupsToInsert);
        }
    }

    // [CH] NAIS-2063 Update public group labels for Accounts that have had values changed
    // [GH] NAIS-2094 Adjusts length of Account name when account is updated so Acc Name + SSS School Code will not exceed length of 40.
    public static void updatePublicGroups(Map<String, Account> accountsMap) {
        List<Group> groupsToUpdate = [select Id, DeveloperName from Group where DeveloperName in :accountsMap.keySet()];
        for(Group grp : groupsToUpdate){
            // Look up the Account to get new Name and SSS School Code values
            Account thisAccount = accountsMap.get(grp.DeveloperName);
            setGroupName(thisAccount, grp);
        }
        update groupsToUpdate;
    }

    private static void setGroupName(Account currentAccount, Group grp)
    {
        Integer sssCodeLength = 0;
        Integer currentAccountLength = currentAccount.Name.length();

        if (currentAccount.SSS_School_Code__c != null)
        {
            sssCodeLength = currentAccount.SSS_School_Code__c.length();
        }

        //Should be 37 because we use additional 3 symbols in naming between name and ssscode ' - '
        if (currentAccountLength + sssCodeLength >= 37)
        {
            grp.Name = currentAccount.Name.subString(0, 37 - sssCodeLength);
        }
        else
        {
            grp.Name = currentAccount.Name; // [CH] NAIS-2063 Include SSS code
        }

        if(currentAccount.SSS_School_Code__c != null)
        {
            grp.Name += ' - ' + currentAccount.SSS_School_Code__c;
        }
    }
}