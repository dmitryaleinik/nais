/**
 * @description Controls the Internal Refund page.
 */
public with sharing class InternalRefundController extends PaymentControllerBase {
    private static final String OPPORTUNITY_ID_PARAM = 'Id';
    private static final String SELECTED_TLI_PARAM = 'selectedTLI';
    private static final String PAYMENT = 'Payment';
    private static final String AUTO_PAYMENT = 'Payment - Auto';

    /**
     * @description The Opportunity that a refund is being issued for.
     */
    public Opportunity opp {
        get {
            if (opp == null) {
                opp = getOpportunity();
            }
            return opp;
        }
        set;
    }

    /**
     * @description The Transaction Line Item associated with the payment
     *              that is being refuneded.
     */
    public Transaction_Line_Item__c tlItem { get; set; }

    /**
     * @description The collection of Transaction Line Items associated with
     *              payments that are being refunded.
     */
    public List<Transaction_Line_Item__c> tliList {
        get {
            if (tliList == null) {
                tliList = getTLItemsById().values();
            }
            return tliList;
        }
        set;
    }

    /**
     * @description Whether or not there is an error on the page. This occurs
     *              when the Opportunity Id is null or invalid.
     */
    public Boolean hasError { get; set; }

    /**
     * @description Whether or not there has been an error during the processBefore()
     *              method. This will make sure that the processPayment() method does
     *              not continue.
     */
    public Boolean hasProcessBeforeErrors {
        get {
            if (hasProcessBeforeErrors == null) {
                return false;
            }
            return hasProcessBeforeErrors;
        }
        set;
    }

    /**
     * @description Whether or not to show the Transaction Line Items.
     *              True if there is at least one Transaction Line Item in
     *              the tliList collection, else false.
     */
    public Boolean showTLI { get; set; }

    /**
     * @description Whether or not the Payment Processor is in test mode.
     */
    public Boolean isTestMode {
        get {
            return PaymentProcessor.InTestMode;
        }
    }

    /**
     * @description The currently selected Transaction Line Item's Id.
     */
    public ID selectedTliId { get; set; }

    /**
     * @description Gather page values on page load. Called from the Page action attribute.
     */
    public void init() {
        showTLI = false;
        hasError = false;

        // Display an error if there is no Opportunity
        if (opp == null) {
            hasError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.OpportunityNotFound));
            return;
        }

        // Handle the TLIs displaying an error if there are none found.
        if (tliList.size() == 0) {
            hasError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.NoRefundableTransactionLineItems));
            return;
        } else if (tliList.size() == 1) {
            showTLI = true;
            tlItem = tliList.get(0);
            selectedTliId = tlItem.Id;
        }
    }

    public PageReference processBefore() {
        hasProcessBeforeErrors = false;
        ApexPages.getMessages().clear();

        Map<Id, Transaction_Line_Item__c> refundableTransactionLineItems =
                new Map<Id, Transaction_Line_Item__c>(tliList);

        if (selectedTliId != null && refundableTransactionLineItems.get(selectedTliId) != null) {
            PaymentService.Request request = new PaymentService.Request(refundableTransactionLineItems.get(selectedTliId));
            PaymentService.Response response = PaymentService.Instance.processBefore(request);

            // Handle the response
            if (!response.Result.isSuccess) {
                hasProcessBeforeErrors = true;
                String message = (response.Result.ErrorMessage == null) ?
                        PaymentProcessor.UNEXPECTED_ERROR : response.Result.ErrorMessage;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, message));
            }

        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Unable to select transaction line item, please try again.'));
        }

        return null;
    }

    /**
     * @description Process the refund for the current Opportunity record. A new
     *              refund Transaction Line Item is created, leaving the existing
     *              payment Transaction Line Items for historical purposes.
     * @returns A PageReference to the Opportunity record if the refund is succesful,
     *          otherwise a null PageReference.
     */
    public PageReference processRefund() {
        if (hasProcessBeforeErrors) {
            return null;
        }

        PageReference pr = new PageReference('/' + opp.Id);

        Map<Id, Transaction_Line_Item__c> refundableTransactionLineItems =
                new Map<Id, Transaction_Line_Item__c>(tliList);

        Transaction_Line_Item__c refundTransactionLineItem = refundableTransactionLineItems.get(selectedTliId);

        if (refundTransactionLineItem != null) {
            PaymentService.Request request = new PaymentService.Request(refundTransactionLineItem);
            PaymentService.Response response = PaymentService.Instance.processRefund(request);

            if ((response.Result == null || !response.Result.isSuccess) && !Test.isRunningTest()) {
                String errorMessage = (response.ErrorMessage != null) ? response.ErrorMessage :
                    'There was a problem processing the refund.';
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage));
                return null;
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Unable to select Transaction Line Item. Please try again.'));
            return null;
        }

        pr.setRedirect(true);
        return pr;
    }

    /**
     * @description Show the Transaction Line Item section based on the selected
     *              Transaction Line Item.
     * @returns A null PageReference.
     */
    public PageReference showTLISection() {
        hasError = false;

        Id tliId = UrlService.Instance.getIdByType(SELECTED_TLI_PARAM, Transaction_Line_Item__c.getSObjectType());

        if (tliId == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least one Transaction Line Item'));
            return null;
        }

        List<Transaction_Line_Item__c> transactionLineItems = TransactionLineItemSelector.Instance
                .selectById(new Set<Id> { tliID });

        if (!transactionLineItems.isEmpty()) {
            tlItem = transactionLineItems[0];
            showTLI = true;
        }

        return null;
    }

    /**
     * @description Cancel the current refund and return to the current
     *              Opportunity record.
     * @returns A PageReference to the Opportunity record currently being
     *          interacted with if it is not null, otherwise a null
     *          PageReference.
     */
    public PageReference cancel() {
        if (opp != null) {
            return new ApexPages.StandardController(opp).view();
        }
        return null;
    }

    /**
     * @description Placeholder constructor.
     */
    public InternalRefundController() { }

    /**
     * @description Override the default Opportunity Id query parameter.
     * @returns A string value of the Opportunity Id query parameter.
     */
    protected override String getOpportunityIdParameter() {
        return OPPORTUNITY_ID_PARAM;
    }

    private Map<Id, Transaction_Line_Item__c> getTLItemsById() {
        Set<Id> opportunityIds = new Set<Id> { opp.Id };

        List<Transaction_Line_Item__c> transactionLineItems = TransactionLineItemSelector.Instance
                .selectRefundableTransactionLineItems(opportunityIds);
        Map<Id, Transaction_Line_Item__c> refundableTransactionLineItems =
                new Map<Id, Transaction_Line_Item__c>(transactionLineItems);

        for (Transaction_Line_Item__c transactionLineItem : transactionLineItems) {
            if (transactionLineItem.RecordTypeId == RecordTypes.refundTransactionTypeId) {
                for (Transaction_Line_Item__c refundedTransactionLineItem : transactionLineItems) {
                    if (transactionLineItem.Transaction_ID__c == refundedTransactionLineItem.Transaction_ID__c) {
                        // Remove both the original transaction and the refund transaction.
                        refundableTransactionLineItems.remove(transactionLineItem.Id);
                        refundableTransactionLineItems.remove(refundedTransactionLineItem.Id);

                        // Since we only refund one TLI at a time, let's break out of this loop
                        break;
                    }
                }
            }
        }

        return refundableTransactionLineItems;
    }
}