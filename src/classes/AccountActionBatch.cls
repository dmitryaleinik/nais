/*
 * NAIS-967 Roll Up Subscription Status and Type to Account
 *
 *    Scheduled batch Apex to rollup status updates when subscription end date expires without insert/update/delete of subscriptions
 *
 * WH, Exponent Partners, 2013
 */
global class AccountActionBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    global final Set<Id> accountIdsNewlyBasic = new Set<Id>();
    global final Set<Id> accountIdsNewlyFull = new Set<Id>();
    global final String query = 
        'select Id, SSS_Subscription_Type_Current__c, Portal_Version__c, SSS_Subscriber_Status__c, '+
        ' (select Status__c, Subscription_Type__c from Subscriptions__r where Start_Date__c <= TODAY order by Start_Date__c desc limit 1) '
        +'from Account where RecordTypeId in (\'' + RecordTypes.schoolAccountTypeId + '\', \'' + RecordTypes.accessOrgAccountTypeId + '\')';
    
    global AccountActionBatch() {
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        List<Id> accountIds = new List<Id>();
        String newPortalVersion;
        Account thisAccount;
        
        for (Account a : (List<Account>)scope) {
            accountIds.add(a.Id);
            
            //Apply rollupSubscriptionStatus logic to determine if user should be updated */
             if( a.Subscriptions__r.size()>0 && a.Subscriptions__r[0].Status__c == 'Current' 
             && (a.SSS_Subscriber_Status__c != 'Current' || a.SSS_Subscription_Type_Current__c != a.Subscriptions__r[0].Subscription_Type__c))
             {
                newPortalVersion = SubscriptionTypes.Instance
                        .getPortalVersionSubscriptionType(a.Subscriptions__r[0].Subscription_Type__c);
                
                if(newPortalVersion != null && a.Portal_Version__c != newPortalVersion) {
                    if (newPortalVersion == 'Basic') {
                        accountIdsNewlyBasic.add(a.Id);
                    } else if (newPortalVersion == 'Full-featured') {
                        accountIdsNewlyFull.add(a.Id);
                    }
                }
             }
        }
        if (accountIds.size() > 0) {
            AccountAction.rollupSubscriptionStatus(accountIds);
        }
    }
    
    global void finish(Database.BatchableContext bc) {    
        /* To avoid MIXED_DML_OPERATION: DML operation on setup object is not permitted 
        after you have updated a non-setup object (or vice versa): User, original object: Account: []*/
        if (!accountIdsNewlyFull.isEmpty() || !accountIdsNewlyBasic.isEmpty()) {
            userAction.updateUserProfiles(accountIdsNewlyBasic, accountIdsNewlyFull);
        }
    }
    
}