/**
 * @description 
 */
public class PfsProcessService {
    
    public final String PARAM_SHOW_EZ_ALERT_MODAL = 'ezsuccess';
    public final Set<String> SECTION_NEED_CONFIRMATION_BEFORE_SUBMIT = new Set<String>{'basictax','totaltaxable'};
    
    private final String EMAIL_ADDRESS_FOR_EMAIL = OrgWideEmails.Instance.Do_Not_Reply_Address;        
    private final Set<String> STATS_TO_ALLOW_NAVIGATION = 
        new Set<String>{ 
        'statHouseholdInformation__c', 
        'statSchoolSelection__c'
    };
    private final Set<String> SECTIONS_ALLOWED_WITHOUT_ADDITIONAL_EZ_INFO = 
        new Set<String>{
        'householdinformation',
        'parentsguardians',
        'applicantinformation',
        'dependentinformation',
        'householdsummary',
        'schoolselection',
        'selectschools'
        };
    
    /**
    * @description Determines if a PFS is EZ by its Submission_Process__c field.
    * @param SubmissionProcess The value in PFS__c.Submission_Process__c
    */
    public Boolean isEZProcess(String SubmissionProcess) {
        
        return !String.isBlank(SubmissionProcess) && SubmissionProcess.equals('EZ')  && isEZProcessActive;
    }//End:isEZProcess
    
    private static Boolean isEZProcessActiveBoolean;
    public Boolean isEZProcessActive{
        get{
           if(isEZProcessActiveBoolean == null) {
               isEZProcessActiveBoolean = FeatureToggles.isEnabled('PFS_EZ_Enabled__c');
           }
           
           return isEZProcessActiveBoolean;
        }
        private set;
    }//End:isEZProcessActive
    
    /**
    * @description This will tell us if we should disable (through JS+CSS) sections from 6-20, to avoid the EZ popup to be shown. 
    *              Because, at that point we do not have the data necessary to calculate the FamilySize, used to render the interest threshold.
    *              Or if we haven't complete section 5 either.
    */
    public Boolean LockNavigationForNonAllowedSections(PFS__c pfs) {
        return isEZProcessActive && pfs.Submission_Process__c == null && (getFamilySize(pfs) == null || pfs.statSchoolSelection__c == 'Incomplete');
    }//End:LockNavigationForNonAllowedSections
    
    /**
    * @description Returns a wrapper with all the values needed wich fields should be hidden in Family Portal, when the current PFS is EZ.
    * @param SubmissionProcess The value in PFS__c.Submission_Process__c
    */
    public FamilyPortalHiddenFields checkHiddenItemsUI(String SubmissionProcess) {
        if( isEZProcess(SubmissionProcess) ) {
            
            return hiddenSectionsEZ;
        }
        
        return new FamilyPortalHiddenFields();
    }//End:checkHiddenItemsUI
    
    private static FamilyPortalHiddenFields hiddenSectionsEZ {
        get {
            if(hiddenSectionsEZ == null) {
                
                hiddenSectionsEZ = new FamilyPortalHiddenFields('EZ');
            }
            
            return hiddenSectionsEZ;
        }
        private set;
    }
    
    /**
    * @description Determines if a section of the PFS is hidden for families submitting an EZ PFS.
    * @param SubmissionProcess Value retrieved from PFS.Submission_Process__c.
    * @param screen The current section being access through FamilyApplicationMain in Family Portal.
    * @return True, if the given section is hidden for EZ PFS. Otherwise, false.
    */
    public Boolean isSectionHidden(String SubmissionProcess, String screen) {
        
        return isEZProcess(SubmissionProcess) && hiddenSectionsEZ.hiddenFieldsByScreen.containsKey(screen) &&  hiddenSectionsEZ.hiddenFieldsByScreen.get(screen).allInSection;
    }//End:isSectionHidden
    
    /**
    * @description Determines if the current section, given by the "screen" param, is a section that can be access without the need to answer EZ popup questions.
    */
    public Boolean isAllowedSection(String screen) {
        
        return screen == null || (screen != null && SECTIONS_ALLOWED_WITHOUT_ADDITIONAL_EZ_INFO.contains(screen.toLowerCase()));
    }
    
    public Boolean checkPFSStatsToAllowNavigation(PFS__c pfs) {
        
        for(String statField : STATS_TO_ALLOW_NAVIGATION) {
            if(pfs.get(statField) != 'Complete') {
                return false;
            }
        }
        
        return true;
    }//End:checkPFSStatsToAllowNavigation
    
    public void toFull(PFS__c record) {
        // SFP-1276: We no longer send an email with this method. Instead, we rely on the email being sent by the Request Parent Fills Out Full PFS workflow rule.
        toFull(record, true);
    }//End:toFull

    /**
     * @description Converts a EZ PFS to Full and send an email to parents to notify that there's additional information required.
     * @param record The current PFS record.
     */
    public PFS__c toFull(PFS__c record, Boolean forUpdate) {
        // SFP-1276: We no longer send an email with this method. Instead, we rely on the email being sent by the Request Parent Fills Out Full PFS workflow rule.
        ArgumentNullException.throwIfNull(record, 'PFS record');
        
        if( record.Submission_Process__c != 'Full') {
            
           record.Submission_Process__c = 'Full';
           record = updateNavigationBarFlagsForEZ(record, false);
           record = updateNavigationBarFlagsForFull(record, false);
           
           if(forUpdate) {
               update record;
           }
        }
        
        return record;
    }//End:toFull
    
    /**
    * @description Determines if the current PFS with Submission_Processs__c='EZ' shoulb be set to 'Full'. 
    *              This happens if the parent answer "Yes" to Own_Business_or_Farm__c question in section 6.
    * @return The given PFS records with the stats fields set to FALSE for sections not required in EZ.
    */
    public RecalculateResult reCalculateSubmissionProcess(PFS__c pfs, String screen) {
        
        RecalculateResult result = new RecalculateResult();
        
        if(PfsProcessService.Instance.isEZProcessActive && pfs.Submission_Process__c == 'EZ') {
            
            if(screen.ToLowerCase() == 'basictax' && pfs.Own_Business_or_Farm__c == 'Yes') {//SFP-779
                
                result.hasChanged = true;
                result.message = Label.Business_Farm_Owners_Require_Full_PFS;
            } else if(screen.ToLowerCase() == 'totaltaxable' && pfs.Household_Income_Under_Threshold__c == 'Yes') {//SFP-939
                
                Id academicYearId = GlobalVariables.getAcademicYearByName(pfs.Academic_Year_Picklist__c).Id;
                Decimal familySize = getFamilySize(pfs);
                Decimal IncomeThreshold = IncomeThresholdService.Instance.getIncomeThreshold(Integer.ValueOf(familySize), academicYearId);
                EfcWorksheetData pfsWorkSheetData = new EfcWorksheetData();
                EfcDataManager.populateTaxableIncomeData(pfsWorksheetData, pfs);
                EfcCalculationSteps.TaxableIncome efcStep = new EfcCalculationSteps.TaxableIncome();
                efcStep.doCalculation(pfsWorkSheetData);
                
                if(pfsWorkSheetData.totalTaxableIncome > IncomeThreshold) {
                    
                    String totalTaxableIncomeCurrency = '$' + String.ValueOf(pfsWorkSheetData.totalTaxableIncome.format());
                    String IncomeThresholdCurrency = '$' + String.ValueOf(IncomeThreshold.format());
                    
                    result.hasChanged = true;
                    result.message = String.format(Label.Taxable_Income_Higher_Than_EZ_Threshold, new List<String>{totalTaxableIncomeCurrency, IncomeThresholdCurrency});
                }
            }
        }
        
        return result;
    }//End:reCalculateSubmissionProcess
    
    /**
    * @description Determine whether or not the user qualifies for PFS EZ. If so, we need to update the PFS Submission Process to be "EZ".
    *              Regardless of whether or not the user qualifies for PFS EZ, we need to update the StatSubmissionProcess__c field to be 
    *              true after the user completes this step.  The logic to determine PFS FULL or PFS EZ is in this order:
    *              "True" to 3 = PFS EZ (stop)
    *              "True" to 4 OR 5 = PFS Full (stop)
    *              "True" to 1 AND 2 AND "False" to 4 AND 5 (PFS EZ)
    */
    public String calculateSubmissionProcess(PFS__c pfs) {
        
        Boolean question1 = pfs.Household_Income_Under_Threshold__c == 'Yes';
        Boolean question2 = pfs.Household_Assets_Under_Threshold__c == 'Yes';
        Boolean question3 = pfs.Household_Has_Received_Benefits__c == 'Yes';
        Boolean question4 = pfs.Guardian_Owns_Business__c == 'Yes';
        Boolean question5 = pfs.Guardian_Self_Employed_Freelancer__c == 'Yes';
        
        String submissionProcess = question3 || (question1 && question2 && !question4 && !question5) ? 'EZ' : 'Full';
        
        submissionProcess = applyHasOptOutEZLogic(pfs, submissionProcess);
        
        return submissionProcess;
        
    }//End:calculateSubmissionProcess
    
    public String applyHasOptOutEZLogic(PFS__c pfs, String submissionProcess) {
        
        Set<Id> schoolIds = getSchoolIdsForPFS(pfs);
        
        return applyHasOptOutEZLogic(pfs, schoolIds, submissionProcess);
    }
    
    public String applyHasOptOutEZLogic(PFS__c pfs, Set<Id> schoolIds, String submissionProcess) {
        
        Boolean hasOptOut = hasAtLeastOneSchoolOptOutEZ(schoolIds, pfs.Academic_Year_Picklist__c);
        
        if (hasOptOut) {
            
            if (pfs.PFS_Status__c == 'Application In Progress') {
                
                submissionProcess = 'Full';
                
            } else if (pfs.PFS_Status__c == 'Application Submitted' && submissionProcess == 'EZ') {
                
                submissionProcess = 'Full';
            }
        }
        
        return submissionProcess;
    }//End:applyHasOptOutEZLogic
    
    /**
    * @description Get the set of school ids for the given pfs.
    * @param pfs record.
    * @return A set of school ids for the given pfs.
    */
    private Set<Id> getSchoolIdsForPFS(PFS__c pfs) {
        
        Set<Id> schoolIds = new Set<Id>();
        
        List<School_PFS_Assignment__c> spas = SchoolPfsAssignmentsSelector.Instance.selectByPFSId(new Set<Id>{pfs.Id});
        
        for (School_PFS_Assignment__c spa :  spas) {
            
            if (spa.School__c != null && spa.Withdrawn__c != 'Yes') {
                schoolIds.add(spa.School__c);
            }
        }
        
        return schoolIds;
    }//End:getSchoolIdsForPFS
    
    public List<String> getEZQuestionLabelFields(List<String> questionFields) {
        
        if(PfsProcessService.Instance.isEZProcessActive) {            
            Map<String, Schema.fieldSet> fieldSets = Schema.SObjectType.Question_Labels__c.fieldSets.getMap();
            
            if(fieldSets.containsKey('PfsEZ')) {            
              Schema.fieldSet ezFieldSet = fieldSets.get('PfsEZ');
              for (FieldSetMember fieldEZ : ezFieldSet.getFields()) {
                  questionFields.add(fieldEZ.getFieldPath());
              }
            }
        }
        return questionFields;
    }
    
    /**
     * @description Return the PFS record with the update of the flags related to not visible sections in the EZ PFS.
     * @param record PFS.
     * @param value Boolean value to be set for each flag.
     */
    public PFS__c updateNavigationBarFlagsForEZ(PFS__c record, Boolean value) {
        
        record.statStudentIncome__c = value;
        record.statRealEstate__c = value;
        record.statVehicles__c = value;
        record.statBusinessAssets__c  = value;
        record.statBusinessExpenses__c  = value;
        record.statBusinessIncome__c  = value;
        record.statBusinessInformation__c  = value;
        record.statBusinessSummary__c = value;
        
        return record;
    }
    
    /**
     * @description Return the PFS record with stats related to not visible sections in the EZ PFS.
     * @param record PFS.
     * @return The pfs with EZ stats set to true.
     */
    public PFS__c lockStatsUpdateForEZ(PFS__c pfs) {
       
        if( isEZProcessActive && pfs.Submission_Process__c == 'EZ' ) {
            
            pfs = updateNavigationBarFlagsForEZ(pfs, true);
        } 
        
        return pfs;
    }
    
    /**
     * @description When we convert an EZ PFS to a full PFS, there are sections that the user may have already filled out 
     *              but after converting to a full PFS, those sections now require them to fill out even more information. 
     *              These sections are: 6, 7, 8, 12 and 14.
    */
    public PFS__c updateNavigationBarFlagsForFull(PFS__c record, Boolean value) {
        
           record.statBasicTax__c = value;//Section 6
           record.statTotalTaxable__c = value;//Section 7
           record.statTotalNon__c = value;//Section 8
           record.statOtherAssets__c = value;//Section 12
           record.statOtherExpenses__c = value;//Section 14
        
        return record;
    }//End:updateNavigationBarFlagsForFull
    
    public Decimal getFamilySize(PFS__c pfs) {
        
        if(pfs != null) {
            if(pfs.Family_Size__c != null && pfs.Family_Size__c > 0) return pfs.Family_Size__c;
            
            if(pfs.Family_Status__c != null && pfs.Applicants__r != null && pfs.Applicants__r.size() > 0 && pfs.statDependentInformation__c) {
                /*Apply same formula used in EfcCalculationSteps.doCalculation */
                Decimal numParents = pfs.Family_Status__c == EfcPicklistValues.FAMILY_STATUS_1_PARENT ? 1 : 2;
                Decimal numApplicants = pfs.Applicants__r.size();
                Decimal numDependents = EfcUtil.nullToZero(pfs.Number_of_Dependent_Children__c);//This can be null, that's the reason why we check "statDependentInformation"
                return numParents + numApplicants + numDependents;
            }
        }
        return null;
    }//End:getFamilySize
    
    public Boolean validCountryForEZ(String country) {
        
        return country != null && country.toLowerCase() == 'united states';
    }//End:validCountryForEZ
    
    private PfsProcessService() { }
    
    /**
    * @description Determine if at least one of a set of schools has opt out EZ.
    * @param schoolIds The schools ids for which we will query if at least one has opt out EZ.
    * @param academicYearName The academic year for which we will query 
    *        the annualSetting.Opt_Out_Of_PFS_EZ__c for the given school ids.
    * @return True if if at least one of a set of schools has opt out EZ. Otherwise, false.
    */
    public Boolean hasAtLeastOneSchoolOptOutEZ(Set<Id> schoolIds, String academicYearName) {
        
        List<Account> schoolsWithAnnualSetting = AccountSelector.Instance.selectHasOptOutEZ(schoolIds, academicYearName);
        for(Account a : schoolsWithAnnualSetting) {
            
            if (!a.Annual_Settings__r.isEmpty() && a.Annual_Settings__r[0].Opt_Out_Of_PFS_EZ__c == true) {
                return true;
            }
        }
        
        return false;
    }
    
    public static PfsProcessService Instance {
        get {
            if (Instance == null) {
                Instance = new PfsProcessService();
            }
            return Instance;
        }
        private set;
    }
    
    public class RecalculateResult{
        public Boolean hasChanged { get; set; }
        public String message { get; set; }
        
        public RecalculateResult() {
            hasChanged = false;
            message = '';
        }
    }//End:RecalculateResult
}