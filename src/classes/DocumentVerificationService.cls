/**
 * @description This class is used to handle operations related to document verification such as transfering
 *              verification values to SDA records, summing certain verification fields, and updating SPA records with
 *              the verification sums.
 */
public class DocumentVerificationService {


    @testVisible private static final String SCHOOL_PFS_ASSIGNMENT_IDS = 'schoolPfsAssignmentIds';
    @testVisible private static final String DOC_ASSIGNMENT_IDS_PARAM = 'docAssignmentIds';
    @testVisible private static final String PROCESSING_REQUIRED = 'Processing Required';
    private static final String BATCH_JOB_NAME = CopyVerificationForPfsAssignmentsBatch.class.toString();
    private static final Set<String> PENDING_JOB_STATUSES = new Set<String> { 'Holding', 'Queued' };
    private static final String WORKFLOW_NAME = 'SF_InitiateReprocess';
    private static final String MISSING_REQUIRED_INFORMATION =
            'This record was previously marked for verification but is now missing the following required info: {0}';

    private Integer maxCalloutsPerRecord;

    private DocumentVerificationService() { }

    /**
     * @description Queries the specified pfs assignment records and checks if they need to be marked for processing by
     *              the CopyVerificationForPfsAssignmentsQueue. If records were flagged for processing, the
     *              CopyVerificationForPfsAssignmentsQueue job is queued for those records.
     * @param schoolPfsAssignmentIds The Ids of the records to check for processing.
     * @return Id of the batch job that was scheduled or null if none was necessary.
     */
    public Id queueVerificationUpdates(Set<Id> schoolPfsAssignmentIds) {
        ArgumentNullException.throwIfNull(schoolPfsAssignmentIds, SCHOOL_PFS_ASSIGNMENT_IDS);

        // If the set is empty, there are no updates necessary.
        if (schoolPfsAssignmentIds.isEmpty()) {
            return null;
        }

        Map<Id, School_PFS_Assignment__c> recordsToProcess = new Map<Id, School_PFS_Assignment__c>();

        // Query school pfs assignment records.
        List<School_PFS_Assignment__c> pfsAssignments = queryPfsAssignments(schoolPfsAssignmentIds);

        // For each one that is not marked for processing, flag for processing and add to list to update.
        Integer numberOfRecords = pfsAssignments.size();
        for (Integer i = 0; i < numberOfRecords; i++) {
            School_PFS_Assignment__c record = pfsAssignments[i];

            // If Verification Status does not equal Processing Required, create a copy of the record and add it to the list to process.
            // We create a copy so we know that we are only updating the Verification Status field.
            if (!PROCESSING_REQUIRED.equalsIgnoreCase(record.Verification_Processing_Status__c)) {
                School_PFS_Assignment__c recordToProcess = createRecordToProcess(record.Id);

                recordsToProcess.put(record.Id, recordToProcess);
            }
        }

        // If any records need updating, do it.
        if (!recordsToProcess.isEmpty()) {
            update recordsToProcess.values();

            return System.enqueueJob(new CopyVerificationForPfsAssignmentsQueue(recordsToProcess.keySet()));
        }

        return null;
    }

    /**
     * @description Queries the specified school document assignments and attempts to request document verification from
     *              a third party. Only records with Verification Request Status set to incomplete will be processed.
     *              The records are grouped by document Id so that only one request is needed per document. Each record
     *              will be updated with the results of the operation. If the Verify_Existing_Documents__c is disabled,
     *              this method will exit immediately. If SpringCM is down for maintenance, we will exit immediately.
     * @param docAssignmentIds The Ids of the records to process.
     * @throws ArgumentNullException if docAssignmentIds is null.
     */
    public void requestVerificationForProcessedDocs(Set<Id> docAssignmentIds) {
        // If the feature is not enabled, return early in case we need to turn the feature off after a job has already been queued.
        if (!FeatureToggles.isEnabled('Verify_Existing_Documents__c')) {
            return;
        }

        // Don't do anything if SpringCM is down for maintenance.
        // We explicity check for '== true' because this method returns null sometimes :(
        if (GlobalVariables.isMaintenanceWindow() == true) {
            return;
        }

        ArgumentNullException.throwIfNull(docAssignmentIds, DOC_ASSIGNMENT_IDS_PARAM);

        Map<Id, School_Document_Assignment__c> documentAssignmentsById = getDocAssignmentsById(docAssignmentIds);

        // Use the records being processed by the service to check for documents that have already been verified.
        IVerifiedDocumentChecker checker = getVerifiedDocumentChecker();
        Set<Id> previouslyVerifiedDocIds = checker.getPreviouslyVerifiedDocIds(documentAssignmentsById);

        // Group the SDAs that need to be processed by document Id.
        // We will send one request per document and update the related SDAs with the results.
        Map<Id, List<School_Document_Assignment__c>> documentAssignmentsByDocId =
                groupDocAssignmentsByDocId(documentAssignmentsById.values());

        Map<Id, School_Document_Assignment__c> schoolDocAssignmentsToUpdateById = new Map<Id, School_Document_Assignment__c>();
        Map<Id, Family_Document__c> documentsToUpdateById = new Map<Id, Family_Document__c>();
        for (Id docId : documentAssignmentsByDocId.keySet()) {
            List<School_Document_Assignment__c> documentAssignments = documentAssignmentsByDocId.get(docId);

            // Attempt to request verification for documents that haven't already been verified.
            VerificationRequestResult results = requestVerification(documentAssignments, previouslyVerifiedDocIds);

            // Store the records that need to be updated for DML after all requests are done.
            schoolDocAssignmentsToUpdateById.putAll(results.DocumentAssignmentsById);
            // If the Track Verification Requested Date feature is not enabled, DocumentsById will be empty.
            documentsToUpdateById.putAll(results.DocumentsById);
        }

        if (!schoolDocAssignmentsToUpdateById.isEmpty()) {
            update schoolDocAssignmentsToUpdateById.values();
        }

        // To be extra careful, family documents are only updated if the Track Verification Request Date feature is enabled.
        // The extra toggle is in place in case doing extra DML to fam docs in production when we are experiencing high
        // volume causes unexpected row locking and we need to SHUT IT DOWN.
        if (FeatureToggles.isEnabled('Track_Verification_Request_Date__c') && !documentsToUpdateById.isEmpty()) {
            update documentsToUpdateById.values();
        }
    }

    private List<School_PFS_Assignment__c> queryPfsAssignments(Set<Id> recordIds) {
        if (recordIds == null || recordIds.isEmpty()) {
            return new List<School_PFS_Assignment__c>();
        }

        return [SELECT Id, Verification_Processing_Status__c FROM School_PFS_Assignment__c WHERE Id IN :recordIds];
    }

    private School_PFS_Assignment__c createRecordToProcess(Id recordId) {
        return new School_PFS_Assignment__c(Id = recordId, Verification_Processing_Status__c = PROCESSING_REQUIRED);
    }

    private Map<Id, School_Document_Assignment__c> getDocAssignmentsById(Set<Id> recordIds) {
        return new Map<Id, School_Document_Assignment__c>(queryDocumentAssignments(recordIds));
    }

    private Map<Id, List<School_Document_Assignment__c>> groupDocAssignmentsByDocId(List<School_Document_Assignment__c> recordsToGroup) {
        Map<Id, List<School_Document_Assignment__c>> docAssignmentsByDocId = new Map<Id, List<School_Document_Assignment__c>>();

        for (School_Document_Assignment__c docAssignment : recordsToGroup) {
            List<School_Document_Assignment__c> documentAssignments = docAssignmentsByDocId.get(docAssignment.Document__c);

            if (documentAssignments == null) {
                documentAssignments = new List<School_Document_Assignment__c>();
            }

            documentAssignments.add(docAssignment);

            docAssignmentsByDocId.put(docAssignment.Document__c, documentAssignments);
        }

        return docAssignmentsByDocId;
    }

    private List<School_Document_Assignment__c> queryDocumentAssignments(Set<Id> recordIds) {
        // TODO Selector School_Document_Assignment__c
        return [SELECT Id, Document__c, Document_Import_Id__c, Document_Type__c, PFS_Number__c, School_Code__c
        FROM School_Document_Assignment__c
        WHERE Id IN :recordIds AND Verification_Request_Status__c = :SchoolDocumentAssignments.VERIFICATION_REQUEST_INCOMPLETE FOR UPDATE];
    }

    private Integer getMaxCalloutsPerRecord() {
        if (maxCalloutsPerRecord != null) {
            return maxCalloutsPerRecord;
        }

        Databank_Settings__c databankSettings = Databank_Settings__c.getInstance('Databank');

        maxCalloutsPerRecord = (databankSettings != null && databankSettings.TryAttemptsWorkflow__c != null) ? Integer.valueOf(databankSettings.TryAttemptsWorkflow__c) : 5;

        // Multiply the configuration value by 2 since we use this limit in two separate loops.
        // One for authentication, one for actually sending the doc.
        maxCalloutsPerRecord = maxCalloutsPerRecord * 2;

        return maxCalloutsPerRecord;
    }

    private Boolean areNotEnoughCallouts() {
        return ((Limits.getLimitCallouts() - Limits.getCallouts()) < getMaxCalloutsPerRecord() && !Test.isRunningTest());
    }

    private VerificationRequestResult requestVerification(List<School_Document_Assignment__c> documentAssignments, Set<Id> previouslyVerifiedDocIds) {
        if (documentAssignments == null || documentAssignments.isEmpty()) {
            return new VerificationRequestResult();
        }

        // Instantiate maps of the records that may need to be updated.
        Map<Id, School_Document_Assignment__c> sdasToUpdateById = new Map<Id, School_Document_Assignment__c>();
        Map<Id, Family_Document__c> documentsToUpdateById = new Map<Id, Family_Document__c>();

        // Since all of these document assignments should be related to the same family document record, we will use the first record to write the request xml.
        School_Document_Assignment__c docAssignment = documentAssignments[0];

        // Check to see if the document has already been verified.
        // If so, clear out the Verification Request Status field so we don't try to process these records again later on.
        if (previouslyVerifiedDocIds.contains(docAssignment.Document__c)) {
            sdasToUpdateById = createRecordsForUpdate(documentAssignments, null, null);
            return buildResult(sdasToUpdateById, documentsToUpdateById);
        }

        // Check to see if we have all the information necessary for requesting verification.
        String missingInfo = getMissingInfo(docAssignment);

        // if record does not have the required info, set Verification Request Status to error.
        if (String.isNotBlank(missingInfo)) {
            String missingInfoError = String.format(MISSING_REQUIRED_INFORMATION, new List<String> { missingInfo });
            sdasToUpdateById = createRecordsForUpdate(documentAssignments, SchoolDocumentAssignments.VERIFICATION_REQUEST_ERROR, missingInfoError);
            return buildResult(sdasToUpdateById, documentsToUpdateById);
        }

        // If we don't have enough callouts to process the record, return early with an empty map to indicate that there are no updates to make at this time.
        // When making callouts to SpringCM, we will try as many times as specified on the Databank_Settings__c.TryAttemptsWorkflow__c field.
        // If this field is null, we will do up to 10 callouts per record.
        if (areNotEnoughCallouts()) {
            return new VerificationRequestResult();
        }

        // Do callout to springCM
        try {
            String verificationData = writeXml(docAssignment);

            // if successful, set Verification Request Status to COMPLETE
            // Using SpringCMAction.sparkSpringCMWorkflow() will throw an exception if the request fails.
            SpringCMAction.sparkSpringCMWorkflow(verificationData, WORKFLOW_NAME, UserInfo.getSessionId());

            // Set status to complete and wipe out any errors from previous attempts.
            sdasToUpdateById = createRecordsForUpdate(documentAssignments, SchoolDocumentAssignments.VERIFICATION_REQUEST_COMPLETE, '');

            // Only create family documents to update if we are tracking the verification requested date.
            if (FeatureToggles.isEnabled('Track_Verification_Request_Date__c')) {
                // Create family document record to update with the Verification Requested Date set.
                documentsToUpdateById = createFamilyDocsForUpdate(docAssignment.Document__c);
            }
        } catch (Exception e) {
            // We will want to catch the exception to allow other records to be processed.
            sdasToUpdateById = createRecordsForUpdate(documentAssignments, SchoolDocumentAssignments.VERIFICATION_REQUEST_ERROR, e.getMessage());
        }

        return buildResult(sdasToUpdateById, documentsToUpdateById);
    }

    private VerificationRequestResult buildResult(Map<Id, School_Document_Assignment__c> sdasForResult, Map<Id, Family_Document__c> docsForResult) {
        VerificationRequestResult results = new VerificationRequestResult();
        results.DocumentAssignmentsById.putAll(sdasForResult);
        results.DocumentsById.putAll(docsForResult);
        return results;
    }

    private Map<Id, Family_Document__c> createFamilyDocsForUpdate(Id famDoc) {
        Family_Document__c docToUpdate = new Family_Document__c(Id = famDoc);
        docToUpdate.Verification_Requested_Date__c = System.now();
        Map<Id, Family_Document__c> docsToUpdateById = new Map<Id, Family_Document__c>();
        docsToUpdateById.put(docToUpdate.Id, docToUpdate);
        return docsToUpdateById;
    }

    private Map<Id, School_Document_Assignment__c> createRecordsForUpdate(List<School_Document_Assignment__c> recordsToUpdate, String status, String errors) {
        Map<Id, School_Document_Assignment__c> updatedRecords = new Map<Id, School_Document_Assignment__c>();

        if (recordsToUpdate == null || recordsToUpdate.isEmpty()) {
            return updatedRecords;
        }

        for (School_Document_Assignment__c record : recordsToUpdate) {
            School_Document_Assignment__c newRecord = new School_Document_Assignment__c();
            newRecord.Id = record.Id;
            newRecord.Verification_Request_Status__c = status;
            newRecord.Verification_Request_Error__c = errors;
            updatedRecords.put(newRecord.Id, newRecord);
        }

        return updatedRecords;
    }

    private String getMissingInfo(School_Document_Assignment__c docAssignment) {
        Set<String> missingFields = new Set<String>();

        if (String.isBlank(docAssignment.Document_Import_Id__c)) {
            missingFields.add('Document_Import_Id__c');
        }

        if (String.isBlank(docAssignment.Document_Type__c)) {
            missingFields.add('Document_Type__c');
        }

        if (String.isBlank(docAssignment.PFS_Number__c)) {
            missingFields.add('PFS_Number__c');
        }

        return String.join(new List<String>(missingFields), ', ');
    }

    /**
     * @description Builds the xml string that needs to be sent to SpringCM.
     * @example     <SFData>
     *                  <ImportID>1bd2637d-b490-e611-bb8d-6c3be5a75f4d</ImportID>
     *                  <PFSid>22-111111</PFSid>
     *                  <SchoolCode>1234</SchoolCode>
     *                  <DocType>W2</DocType>
     *              </SFData>
     * @return String containing the xml.
     */
    private String writeXml(School_Document_Assignment__c docAssignment) {
        XmlStreamWriter xmlWriter = new XmlStreamWriter();

        xmlWriter.writeStartDocument(null, '1.0');

        // Start outer SFData element.
        xmlWriter.writeStartElement(null, 'SFData', null);

        xmlWriter.writeStartElement(null, 'ImportID', null);
        xmlWriter.writeCharacters(docAssignment.Document_Import_Id__c);
        xmlWriter.writeEndElement();

        xmlWriter.writeStartElement(null, 'PFSid', null);
        xmlWriter.writeCharacters(docAssignment.PFS_Number__c);
        xmlWriter.writeEndElement();

        xmlWriter.writeStartElement(null, 'SchoolCode', null);
        xmlWriter.writeCharacters(docAssignment.School_Code__c);
        xmlWriter.writeEndElement();

        xmlWriter.writeStartElement(null, 'DocType', null);
        xmlWriter.writeCharacters(SpringCMAction.getSpringCMDocTypeFromSFDocType(docAssignment.Document_Type__c));
        xmlWriter.writeEndElement();

        // End outer SFData element.
        xmlWriter.writeEndElement(); // end SFData.

        xmlWriter.writeEndDocument();

        String xmlOutput = xmlWriter.getXmlString();
        xmlWriter.close();
        return xmlOutput;
    }

    /**
     * @description Interface used to abstract behavior for finding previously verified documents.
     */
    private interface IVerifiedDocumentChecker {

        Set<Id> getPreviouslyVerifiedDocIds(Map<Id, School_Document_Assignment__c> currentDocAssignmentsById);
    }

    private IVerifiedDocumentChecker getVerifiedDocumentChecker() {
        if (FeatureToggles.isEnabled('Track_Verification_Request_Date__c')) {
            return new VerifiedDocumentCheckerByDate();
        }

        return new VerifiedDocumentCheckerByRelatedSdas();
    }

    /**
     * @description This class is used to check for documents that have already been verified. We do not enforce sharing
     *              because we need to check all SDAs that are related to the documents which may be verified.
     *              This is the first version of this class which finds previously verified docs based on related SDAs.
     */
    private without sharing class VerifiedDocumentCheckerByRelatedSdas implements IVerifiedDocumentChecker {

        public DocumentVerificationService.VerifiedDocumentCheckerByRelatedSdas() {}

        /**
         * @description Determines if a document has already been verified by looking at the other related SDAs not currently being processed.
         *              If at least one of the SDAs has verify data set to true, we can assume that we have at least sent
         *              the document to springCM/databank for verification. We need to make this check so that we don't
         *              request verification for documents multiple times.
         * @param currentDocAssignmentsById A map containing the school document assignments currently being processed.
         * @return A set containing the Ids of documents that have either been verified or resent to SpringCM for verification.
         */
        public Set<Id> getPreviouslyVerifiedDocIds(Map<Id, School_Document_Assignment__c> currentDocAssignmentsById) {
            Set<Id> verifiedDocumentIds = new Set<Id>();

            Set<Id> relatedDocumentIds = new Set<Id>();
            for (School_Document_Assignment__c docAssignment : currentDocAssignmentsById.values()) {
                relatedDocumentIds.add(docAssignment.Document__c);
            }

            Set<String> verifiableDocTypes = VerificationAction.getVerifiableDocTypes();

            Set<Id> currentSdaIds = currentDocAssignmentsById.keySet();
            // Query for the family documents with school document assignments where verify data is true.
            // If a family document has at least one SDA with verify data true that is not currently being processed,
            // we assume that we have already sent a request to have the document verified.
            List<Family_Document__c> familyDocsWithAssignments =
            [SELECT Id, (SELECT Id FROM School_Document_Assignments__r WHERE Verify_Data__c = true)
            FROM Family_Document__c WHERE Id IN :relatedDocumentIds AND Deleted__c = false AND Document_Type__c IN :verifiableDocTypes];

            for (Family_Document__c document : familyDocsWithAssignments) {
                Map<Id, School_Document_Assignment__c> assignmentsById = new Map<Id, School_Document_Assignment__c>(document.School_Document_Assignments__r);

                // Get the Ids of the SDAs to check by removing the ones that are being processed by this service.
                Set<Id> assignmentIdsToCheck = assignmentsById.keySet();
                assignmentIdsToCheck.removeAll(currentSdaIds);

                // Since we only query for SDAs where Verify Data is true, we can assume the doc has been verified if the set isn't empty.
                if (!assignmentIdsToCheck.isEmpty()) {
                    verifiedDocumentIds.add(document.Id);
                }
            }

            return verifiedDocumentIds;
        }
    }

    /**
     * @description This class is used to check for documents that have already been verified. We do not enforce sharing
     *              because we need to check all the family documents for the SDAs being processed regardless of which
     *              user owns them.
     *              This is the second version of this class which finds previously verified docs based on the Verification Requested Date field.
     *              This is used when the Track Verification Requested Date feature is enabled.
     */
    private without sharing class VerifiedDocumentCheckerByDate implements IVerifiedDocumentChecker {

        /**
         * @description A basic constructor for basic needs.
         */
        public DocumentVerificationService.VerifiedDocumentCheckerByDate() {}

        /**
         * @description Determines if a document has already been verified by querying for family documents where the
         *              Verification Requested Date is not null and the document type is one that we verify. We need to
         *              make this check so that we don't request verification for documents multiple times.
         * @param currentDocAssignmentsById A map containing the school document assignments currently being processed.
         * @return A set containing the Ids of documents that have either been verified or resent to SpringCM for verification.
         */
        public Set<Id> getPreviouslyVerifiedDocIds(Map<Id, School_Document_Assignment__c> currentDocAssignmentsById) {
            Set<Id> relatedDocumentIds = new Set<Id>();
            for (School_Document_Assignment__c docAssignment : currentDocAssignmentsById.values()) {
                relatedDocumentIds.add(docAssignment.Document__c);
            }

            Set<String> verifiableDocTypes = VerificationAction.getVerifiableDocTypes();

            Set<Id> currentSdaIds = currentDocAssignmentsById.keySet();
            // Query for the family documents by Id that are for our verifiable doc types, have not been verified yet, and have not been marked for deletion.
            // If a family document has the Verification Date Requested field populated, we know we have already requested verification.
            Map<Id, Family_Document__c> previouslyVerifiedDocsById =
                new Map<Id, Family_Document__c>([SELECT Id FROM Family_Document__c
                WHERE Id IN :relatedDocumentIds AND
                    Deleted__c = false AND
                    Document_Type__c IN :verifiableDocTypes AND
                    Verification_Requested_Date__c != null]);

            return previouslyVerifiedDocsById.keySet();
        }
    }

    /**
     * @description This class is used to capture the results of initiating the Resend For Verification workflow to
     *              SpringCM. This class will contain the School Document Assignment records that need to be updated as
     *              well as the Family Document record that needs to be updated.
     */
    private class VerificationRequestResult {

        /**
         * @description Basic constructor.
         */
        public DocumentVerificationService.VerificationRequestResult() { }

        /**
         * @description The school document assignment records that need to be updated by Id.
         */
        public Map<Id, School_Document_Assignment__c> DocumentAssignmentsById {
            get {
                if (DocumentAssignmentsById == null) {
                    DocumentAssignmentsById = new Map<Id, School_Document_Assignment__c>();
                }
                return DocumentAssignmentsById;
            }
            private set;
        }

        /**
         * @description The family document records that need to be updated by Id.
         */
        public Map<Id, Family_Document__c> DocumentsById {
            get {
                if (DocumentsById == null) {
                    DocumentsById = new Map<Id, Family_Document__c>();
                }
                return DocumentsById;
            }
            private set;
        }
    }

    /**
     * @description Singleton instance of the DocumentVerificationService.
     */
    public static DocumentVerificationService Instance {
        get {
            if (Instance == null) {
                Instance = new DocumentVerificationService();
            }
            return Instance;
        }
        private set;
    }
}