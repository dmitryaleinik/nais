/*
* @description Family Portal - Family Portal Tab Naming
*/
public class FamilyPageUtils
{

    private static Map<String, String> familyPageTitleByName;

    public static String getFamilyPageTitle(PageReference currentPage)
    {
        String pageName = getPageName(currentPage);
        String screen = ApexPages.currentPage().getParameters().get('screen');

        if (pageName == 'familyapplicationmain' && screen != null)
        {
            pageName = pageName + '_' + screen.toLowerCase();
        }

        familyPageTitleByName = new Map<String, String> {
            'familymessages' => 'Family Messages',
            'familymessagesnew' => (ApexPages.currentPage().getParameters().get('type')=='support'
                                    ?'New Support Ticket for SSS':'New Question to School'),
            'familymessagesedit' => 'Edit Message',
            'familydocuments' => 'My Documents',
            'familydashboard' => 'Dashboard',
            'familypaymentfeewaiverapplied' => 'Payment',
            'familyapplicationmain' => 'Household Information',
            'familyapplicationmain_householdinformation' => 'Household Information',
            'familyapplicationmain_parentsguardians' => 'Parent/Guardian Information',
            'familyapplicationmain_applicantinformation' => 'Applicant Information',
            'familyapplicationmain_dependentinformation' => 'Dependent Information',
            'familyapplicationmain_householdsummary' => 'Household Summary',
            'familyapplicationmain_schoolselection' => 'School Selection',
            'familyapplicationmain_selectschools' => 'Select Schools',
            'familyapplicationmain_familyincome' => 'Family Income',
            'familyapplicationmain_basictax' => 'Basic Tax Information',
            'familyapplicationmain_totaltaxable' => 'Total Taxable Income',
            'familyapplicationmain_totalnon' => 'Total Nontaxable Income',
            'familyapplicationmain_studentincome' => 'Applicant Income',
            'familyapplicationmain_familyassets' => 'Family Assets & Debts',
            'familyapplicationmain_realestate' => 'Real Estate',
            'familyapplicationmain_vehicles' => 'Vehicles',
            'familyapplicationmain_otherassets' => 'Other Assets & Debts',
            'familyapplicationmain_familyexpenses' => 'Family Expenses',
            'familyapplicationmain_educationalexpenses' => 'Educational Expenses',
            'familyapplicationmain_otherexpenses' => 'Other Expenses',
            'familyapplicationmain_businessinformation' => 'Business Information',
            'familyapplicationmain_businessincome' => 'Business Income',
            'familyapplicationmain_businessexpenses' => 'Business Expenses',
            'familyapplicationmain_businessassets' => 'Business Assets',
            'familyapplicationmain_businesssummary' => 'Business/Farm Summary',
            'familyapplicationmain_forkamehamehaapplicant' => 'Kamehameha Schools Applicant Information',
            'familyapplicationmain_forkamehamehaprogram' => 'Program Selection',
            'familyapplicationmain_forkamehameha' => 'Supplemental KS Schools Info',
            'familyapplicationmain_businessfarm' => 'Business/Farm',
            'familyapplicationmain_otherinformation' => 'Other Information',
            'familyapplicationmain_otherconsiderations' => 'Other Considerations',
            'familyapplicationmain_additionalquestions' => 'Additional Questions',
            'familyapplicationmain_monthlyincomeexpensesmain' => 'Monthly Income and Expenses',
            'familyapplicationmain_monthlyincomeexpenses' => 'Monthly Income and Expense Statement',
            'familyawards' => 'Family Awards',
            'familyaward' => 'Family Award'
        };

        return String.escapeSingleQuotes(familyPageTitleByName != null && familyPageTitleByName.containsKey(pageName)
                ?familyPageTitleByName.get(pageName)
                :'' );
    }

    public static String getPageName(PageReference thisPage)
    {
        return SchoolPagesUtils.getPageName(thisPage);
    }
}