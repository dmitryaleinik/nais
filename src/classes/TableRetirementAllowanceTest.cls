@IsTest
private class TableRetirementAllowanceTest
{

    @isTest
    private static void testTableRetirementAllowance()
    {
        // create the academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        // create the table
        TableTestData.populateRetirementAllowanceData(academicYear.Id);

        TableRetirementAllowance.AllowanceData allowanceData;

        // test low age, not in table
        allowanceData = TableRetirementAllowance.getRetirementAllowance(academicYear.Id, EfcPicklistValues.FAMILY_STATUS_1_PARENT, 20);
        System.assertEquals(9500, allowanceData.allowance);
        System.assertEquals(0.04, allowanceData.conversionCoefficient);
        allowanceData = TableRetirementAllowance.getRetirementAllowance(academicYear.Id, EfcPicklistValues.FAMILY_STATUS_2_PARENT, 20);
        System.assertEquals(32100, allowanceData.allowance);
        System.assertEquals(0.07, allowanceData.conversionCoefficient);

        // test age in table
        allowanceData = TableRetirementAllowance.getRetirementAllowance(academicYear.Id, EfcPicklistValues.FAMILY_STATUS_1_PARENT, 50);
        System.assertEquals(11900, allowanceData.allowance);
        System.assertEquals(0.03, allowanceData.conversionCoefficient);
        allowanceData = TableRetirementAllowance.getRetirementAllowance(academicYear.Id, EfcPicklistValues.FAMILY_STATUS_2_PARENT, 50);
        System.assertEquals(40900, allowanceData.allowance);
        System.assertEquals(0.04, allowanceData.conversionCoefficient);

        // test high age, not in table
        allowanceData = TableRetirementAllowance.getRetirementAllowance(academicYear.Id, EfcPicklistValues.FAMILY_STATUS_1_PARENT, 70);
        System.assertEquals(17400, allowanceData.allowance);
        System.assertEquals(0.02, allowanceData.conversionCoefficient);
        allowanceData = TableRetirementAllowance.getRetirementAllowance(academicYear.Id, EfcPicklistValues.FAMILY_STATUS_2_PARENT, 70);
        System.assertEquals(61800, allowanceData.allowance);
        System.assertEquals(0.02, allowanceData.conversionCoefficient);
    }
}