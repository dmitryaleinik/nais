@isTest
private class SchoolMessageControllerTest
{

    private static SchoolMessageController setupController() {
        Case theMessage = new Case();
        ApexPages.StandardController messageController = new ApexPages.StandardController(theMessage);

        return new SchoolMessageController(messageController);
    }

    @isTest
    private static void testControllerSaveNew() {
        List<Academic_Year__c> academicYears = TestUtils.createAcademicYears();
        Account testAccount = TestUtils.createAccount('Test Account', RecordTypes.individualAccountTypeId, 5, true);
        Contact theParent = TestUtils.createContact('Parent A', testAccount.Id, RecordTypes.parentContactTypeId, true);
        User parentAUser = TestUtils.createPortalUser('Test User', 'test@parentAUser.com', 'TestP', theParent.Id, GlobalVariables.familyPortalProfileId, true, true);
        PFS__c thePFS = TestUtils.createPFS('Test PFS', academicYears[0].id, theParent.id, true);

        PageReference messagePage = Page.SchoolMessage;
        Map<String, String > params = messagePage.getParameters();
        params.put('RecordType', RecordTypes.schoolParentCommunicationCaseTypeId);
        params.put('accountId', testAccount.id);
        params.put('contactId', theParent.id);
        params.put('pfsId', thePFS.id);
        params.put('retURL', '/' + testAccount.Id);
        Test.setCurrentPageReference(messagePage);

        SchoolMessageController theController = setupController();

        // CH Fix
        theController.theCase.subject = 'Test Message 1';
        theController.theCase.description = 'Test Description 1';
        theController.theCase.Status = 'New';
        // write current record, wipe id...
        theController.saveNew();
        List<Case> theCases = [ select contactId, accountId, recordTypeId, subject, description, status, origin from case where pfs__c = :thePfs.id ];

        System.assertEquals(1, theCases.size());
        System.assertEquals(theParent.id, theCases[0].contactId);
        System.assertEquals(testAccount.id, theCases[0].accountId);
        System.assertEquals(RecordTypes.schoolParentCommunicationCaseTypeId, theCases[0].recordTypeId);
        System.assertEquals('Test Message 1', theCases[0].subject);
        System.assertEquals('Test Description 1', theCases[0].description);
        System.assertEquals('New', theCases[0].status);
        System.assertEquals('School Portal', theCases[0].origin);

        PFS__c pfsRecord = theController.pfsRecord;

        // Due to some validation rules we need to ensure that these
        // are set before attempting to close the case.
        theController.theCase.Disposition__c = 'Family Report';
        theController.theCase.Sub_Category__c = 'Family Report Explanation';
        theController.theCase.Type = 'Family Portal';

        theController.closeCase();
        Case cse = [ select contactId, accountId, recordTypeId, subject, description, status, origin from case where id = :theController.caseId ];
        System.assertEquals('Closed', cse.status, 'The case was expected to be closed. ' + ApexPages.getMessages());
    }

    @isTest
    private static void saveNew_schoolSupportCase_expectSuccessfulSave()
    {
        Account schoolA = AccountTestData.Instance.asSchool().DefaultAccount;
        Account schoolB = AccountTestData.Instance.asSchool().insertAccount();
        User thisUser = CurrentUser.getCurrentUser();
        thisUser.In_Focus_School__c = schoolA.Id;
        update thisUser;

        PageReference messagePage = Page.SchoolMessage;
        messagePage.getParameters().put('isSupport', '1');
        messagePage.getParameters().put('RecordType', RecordTypes.schoolPortalCaseTypeId);
        Test.setCurrentPage(messagePage);

        SchoolMessageController controller = setupController();
        controller.saveNew();

        List<Case> insertedCases = [SELECT Id, Applicant__c, AccountId FROM Case];
        System.assertEquals(1, insertedCases.size(), 'Expected only one case to be inserted from saving an new case.');
        System.assert(String.isEmpty(insertedCases[0].Applicant__c),
            'Expected the applicant field to not be set for a school support ticket.');
        System.assertEquals(schoolA.Id, AccountTestData.Instance.DefaultAccount.Id);
    }

    @isTest
    private static void saveNew_expectCasesAssignedToInFocusSchool()
    {
        Account schoolA = AccountTestData.Instance.asSchool().forName('SchoolA').create();
        Account schoolB = AccountTestData.Instance.asSchool().forName('SchoolB').create();
        User schoolPortalUser;

        System.runAs(GlobalVariables.getCurrentUser())
        {
            insert new List<Account>{schoolA, schoolB};

            Contact schoolUserContact = ContactTestData.Instance.forAccount(schoolA.Id).DefaultContact;
            schoolPortalUser = UserTestData.createSchoolPortalUser();
            schoolPortalUser.In_Focus_School__c = schoolB.Id;
            insert schoolPortalUser;

            AccountShare accountShareForSchoolA = AccountShareTestData.Instance
                .forUserOrGroupId(schoolPortalUser.Id)
                .forAccountId(schoolA.Id).create();
            AccountShare accountShareForSchoolB = AccountShareTestData.Instance
                .forUserOrGroupId(schoolPortalUser.Id)
                .forAccountId(schoolB.Id).create();

            insert new List<AccountShare>{accountShareForSchoolA, accountShareForSchoolB};
        }

        System.runAs(schoolPortalUser)
        {
            Affiliation__c aff = AffiliationTestData.Instance
                .forOrganizationId(schoolB.Id)
                .forStatus('Current')
                .forType('Additional School User').DefaultAffiliation;

            PageReference messagePage = Page.SchoolMessage;
            messagePage.getParameters().put('isSupport', '1');
            messagePage.getParameters().put('RecordType', RecordTypes.schoolPortalCaseTypeId);
            Test.setCurrentPage(messagePage);

            Test.startTest();
                SchoolMessageController controller = setupController();
                controller.theCase.subject = 'Test Subject';
                controller.theCase.description = 'Test Description'; 
                controller.saveNew();

                List<Case> insertedCases = [SELECT Id, AccountId FROM Case];
                System.assertEquals(1, insertedCases.size(), 'Expected only one case to be inserted from saving an new case.');
                System.assertEquals(schoolB.Id, insertedCases[0].AccountId);
            Test.stopTest();
        }
    }

    @isTest
    private static void edit_schoolSupportCase_expectRedirectToSchoolMessagePage() {
        PageReference messagePage = Page.SchoolMessage;
        messagePage.getParameters().put('isSupport', '1');
        messagePage.getParameters().put('RecordType', RecordTypes.schoolPortalCaseTypeId);
        Test.setCurrentPage(messagePage);

        SchoolMessageController controller = setupController();
        controller.saveNew();

        List<Case> insertedCases = [SELECT Id, Applicant__c FROM Case];
        controller.edit();
    }

    @isTest
    private static void view_schoolSupportCase_expectRedirectToSchoolMessagePage() {
        PageReference messagePage = Page.SchoolMessage;
        messagePage.getParameters().put('isSupport', '1');
        messagePage.getParameters().put('RecordType', RecordTypes.schoolPortalCaseTypeId);
        Test.setCurrentPage(messagePage);

        SchoolMessageController controller = setupController();
        controller.saveNew();

        List<Case> insertedCases = [SELECT Id, Applicant__c FROM Case];
        controller.view();
        controller.getAcademicYearId();
        controller.uploadAttachment();
        controller.cancelR();
        List<SelectOption> options = controller.applicants;
        Student_Folder__c folder = controller.studentFolder;
        Boolean isEdit = controller.editMode;
    }
}