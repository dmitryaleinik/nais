/**
---   INITIAL DEVELOPMENT -- SFP-206, [G.S]
**/
global class ScheduledUpdateTuitionDependents implements schedulable
{
        
    public void execute (SchedulableContext sc)
    {
        String pfsQuery;
        pfsQuery = 'select Id, Tuition_Able_to_Pay_All_Dependents__c, ( select Id,Dependent_1_Parent_Sources_Est__c,Dependent_2_Parent_Sources_Est__c,Dependent_3_Parent_Sources_Est__c, '; 
        pfsQuery = pfsQuery +'Dependent_4_Parent_Sources_Est__c,Dependent_5_Parent_Sources_Est__c,Dependent_6_Parent_Sources_Est__c from Dependents__r) ';
        pfsQuery = pfsQuery +'from PFS__c where academic_year_picklist__c = \'2015-2016\' and academic_year_picklist__c = \'2016-2017\'';
        
        BatchUpdateTuitionDependents ucs = new BatchUpdateTuitionDependents(pfsQuery);
        id lBatchId = database.executebatch(ucs,1000);
    }
}