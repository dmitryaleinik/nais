public class DocumentLinkMappingService
{
       
    @testvisible public static final String TEST_DOCUMENT_LINK_MAPPING_RECORD_NAME = 'Test_Doc_Link_Mapping_Record';
    @testvisible private static Document_Link_Mapping__mdt documentInfo;
    @testvisible private static Id documentId;

    public static Id getDocumentRecordId(String mappingRecordDevName) {
        List<Document_Link_Mapping__mdt> mappingDocRecordInfo = getMappingDocRecordInfoByDevName(mappingRecordDevName);
        if (mappingDocRecordInfo.isEmpty()) {
            return null;
        }

        documentInfo = mappingDocRecordInfo[0];

        List<Document> documents = 
            [SELECT Id FROM Document WHERE DeveloperName = :documentInfo.Document_Developer_Name__c];
        if (documents.isEmpty()) {
            return null;
        }

        documentId = documents[0].Id;

        return documentId;
    }

    private static List<Document_Link_Mapping__mdt> getMappingDocRecordInfoByDevName(String refDocumentInfoDevName) {
        List<Document_Link_Mapping__mdt> documentsInfo = [
            SELECT Document_Name__c, Document_Developer_Name__c
            FROM Document_Link_Mapping__mdt
                WHERE DeveloperName = :refDocumentInfoDevName];
        if (documentsInfo.isEmpty()) {
            return null;
        }
        
        return documentsInfo;
    }
}