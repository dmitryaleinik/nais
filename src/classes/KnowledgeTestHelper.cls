@isTest
public class KnowledgeTestHelper {
    public static List<Knowledge__kav> getKnowledgeArticleByIds(List<Knowledge__kav> articles) {
        return [SELECT Id, KnowledgeArticleId FROM Knowledge__kav WHERE Id IN :articles];
    }

    /**
    * @description: Publishes a list of articles.
    * @param a list of Knowledge__kav objects to publish.
    */
    public static void publishArticles( List<Knowledge__kav> articlesToPublish) {
        for( Knowledge__kav article : articlesToPublish) {
            KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
        }
    }
}