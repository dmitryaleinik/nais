/*
* 07.21.2016 SFP-605
* Adapted from SchoolPFSAdditionalInfoControllerTest
* [jB]
*/
@isTest
private class SchoolPFSAdditionalQuestionsCtrlTest
{
        public class TestData {
        String academicYearId;
        Account school1;
        Contact staff1, parentA, parentB, student1, student2;
        Applicant__c applicant1A, applicant2A;
        PFS__c pfs1;
        Student_Folder__c studentFolder1, studentFolder2;
        School_PFS_Assignment__c spfsa1, spfsa2;

        public TestData() {
            // academic year
            TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
            academicYearId = GlobalVariables.getCurrentAcademicYear().Id;  

            // school
            school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, null, false); 
            insert school1;       

            // parents and students
            parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
            parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
            student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
            student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
            insert new List<Contact> {parentA, parentB, student1, student2};

            // psf
            pfs1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
            insert new List<PFS__c> {pfs1};
            
            // applicant
            applicant1A = TestUtils.createApplicant(student1.Id, pfs1.Id, false);
            applicant2A = TestUtils.createApplicant(student2.Id, pfs1.Id, false);
            insert new List<Applicant__c> {applicant1A, applicant2A};

            // student folder
            studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
            studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', academicYearId, student2.Id, false);
            insert new List <Student_Folder__c> {studentFolder1, studentFolder2};

            // school pfs assignment
            spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder1.Id, false);
            spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.Id, school1.Id, studentFolder2.Id, false);
            insert new List <School_PFS_Assignment__c> {spfsa1, spfsa2};
        }
    }

    @isTest
    static void testSchoolPFSAdditionalQuestionsController()
    {
        TestData td = new TestData();
        // weird assignment of id to academic year picklist
        td.spfsa1.Academic_Year_Picklist__c = GlobalVariables.getCurrentAcademicYear().Name;
        update td.spfsa1;
        
        Annual_Setting__c annualSetting = TestUtils.createAnnualSetting(td.school1.Id, td.academicYearId, false);
        annualSetting.Use_Additional_Questions__c = 'Yes';
        annualSetting.Custom_Question_1__c = true;
        insert annualSetting;

        Question_Labels__c labels = new Question_Labels__c(Language__c = 'en_US', Academic_Year__c = td.academicYearId, Custom_Question_1__c = 'What is your birthday?');
        insert labels;

        Test.startTest();

        PageReference pageRef = Page.SchoolPFSAdditionalInformation;
        pageRef.getParameters().put('schoolPFSAssignmentId', td.spfsa1.Id);
        Test.setCurrentPage(pageRef);
        SchoolPFSAdditionalQuestionsController ctrl = new SchoolPFSAdditionalQuestionsController();
        Test.stopTest();

        System.assertEquals(true,ctrl.customQuestionsEnabledForAcademicYear.get('Custom_Question_1__c'));
        System.assertEquals(false,ctrl.customQuestionsEnabledForAcademicYear.get('Custom_Question_2__c'));
    
        System.assertEquals('What is your birthday?', ctrl.questionLabels.Custom_Question_1__c);
    }
}