/**
* Batch that daily clears SDA records with empty School_Document_Assignment field. 
*/
global class ClearOrphanedSDABatch implements Database.Batchable<sObject> {
    /**
     * @description String for the quering records from the Database.
     */
    private final String CLEAR_SDA_BATCH_QUERY_STRING =
        'SELECT Id FROM School_Document_Assignment__c WHERE '
        + ' School_PFS_Assignment__c = NULL';

    global ClearOrphanedSDABatch() {}
    
    /**
     * @description Gets a list of records that have to be handled from the Database.
     * @param bc System Batchable Context.
     */
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(CLEAR_SDA_BATCH_QUERY_STRING);
    }

    /**
     * @description Gets a chunk of records that were selected and handles them.
     * @param bc System Batchable Context.
     * @param scope Chunk of the records to handle.
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        if (scope == null || scope.isEmpty()) {
            return;
        }
        
        delete scope;
    }
    
    /**
     * @description Finish method to perform some operations after batch was processed.
     * @param bc System Batchable Context.
     */
    global void finish(Database.BatchableContext bc) {}
}