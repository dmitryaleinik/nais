/**
 * SchoolMassEmailSummaryController.cls
 *
 * @description: Server side extension for Mass Email configuration, handles interactions w/ data model and view state manipulation.
 *
 * @author: Chase Logan @ Presence PG
 */
public class SchoolMassEmailSummaryController {

    /* member vars and properties */
    public Boolean massEmailEnabled {

        get {
            // Handle app enabled/disabled
            return MassEmailUtil.isMassEmailGloballyEnabledByAccount( 
                        GlobalVariables.getCurrentSchoolId(), UserInfo.getProfileId());
        }
        private set;
    }
    public Id studentFolderId { 
        get; 
        set {
            if ( this.massEmailEMList == null) {
                this.init( value);
            }
            this.studentFolderId = value;
        } 
    }
    public List<MassEmailEventModel> massEmailEMList { get; set; }
    public String selectedMassESRecord { get; set; }
    public Boolean hasMassEmail { get; private set; }

    /* public methods */

    // nav to individual Mass ES record message detail page
    public PageReference navMassEmailView() {

        PageReference pRef = Page.SchoolMessageDetail;
        pref.getParameters().put( SchoolMessageDetailController.QUERY_STRING_MESSAGE_DETAIL, this.selectedMassESRecord);
        pref.setRedirect( true);

        return pRef;
    }
    
    /* private methods */

    // page initialization
    private void init( String studentFolderId) {

        this.hasMassEmail = false;
        // get student folder by ID
        this.massEmailEMList = new List<MassEmailEventModel>();
        List<School_PFS_Assignment__c> sPFSList = 
            new SchoolPFSAssignmentDataAccessService().getSchoolPFSByStudentFolderId( studentFolderId);

        if ( sPFSList != null) {

            for ( School_PFS_Assignment__c sPFS : sPFSList) {

                List<MassEmailEventModel> tempModelList = this.createMassEmailEventModelList( sPFS);
                this.massEmailEMList.addAll( tempModelList);
            }

            if ( this.massEmailEMList.size() > 0) {

                this.hasMassEmail = true;
            }
        }
    }

    // create MassEmailEventModel List based on parsed Event JSON
    private List<MassEmailEventModel> createMassEmailEventModelList( School_PFS_Assignment__c sPFS) {
        List<MassEmailEventModel> returnList = new List<MassEmailEventModel>();

        if ( sPFS == null) return returnList;

        // parse stored Mass Email Event data stored on student folder, inflate massEmailEMList
        Map<String,MassEmailEventModel> massESIdToEventModelMap = 
            this.parseEventJSON( sPFS.Mass_Email_Events__c, sPFS.Applicant__r.PFS__r.Parent_A__c);
        List<Mass_Email_Send__c> massEmailList = 
            new MassEmailSendDataAccessService().getMassEmailSendByIdSet( massESIdToEventModelMap.keySet());

        if ( massEmailList != null && massEmailList.size() > 0) {

            for ( Mass_Email_Send__c massES : massEmailList) {

                if ( massESIdToEventModelMap.containsKey( massES.Id)) {

                    massESIdToEventModelMap.get( massES.Id).massEmailName = massES.Name;
                    massESIdToEventModelMap.get( massES.Id).sentBy = massES.Sent_By__r.Name;
                    massESIdToEventModelMap.get( massES.Id).sentDate = ( massES.Date_Sent__c != null ? 
                                        massES.Date_Sent__c.format( MassEmailUtil.COMMON_DATE_FORMAT) : '');
                }
            }

            returnList = massESIdToEventModelMap.values();
        }

        return returnList;
    }

    // parse stored Mass Email Event JSON string into MassEmailEventModel instances
    private Map<String,MassEmailEventModel> parseEventJSON( String jsonString, String recipId) {
        Map<String,MassEmailEventModel> massESIdToEventModelMap = new Map<String,MassEmailEventModel>();

        if ( jsonString != null) {

            // Event's are stored as comma separted JSON objects, wrap events in leading and trailing braces
            // to make valid JSON array before deserializing
            String jsonArrayString = '[' + jsonString + ']';
            List<SendgridEvent> tempList = ( List<SendgridEvent>)JSON.deserialize( jsonArrayString, List<SendgridEvent>.class);

            for ( SendgridEvent evt : tempList) {

                // as student folder can contain multiple SPA's, make sure this matches correct recipient
                if ( evt.recipient_id != recipId) continue;

                // store event data by massES ID
                if ( String.isNotEmpty( evt.mass_email_send_id)) {

                    // Set massEEModel.eventStatus in trickle-down order, delivered > opened > bounced > unsub, one status
                    // displayed on screen based on whether that type of event was received.
                    MassEmailEventModel massEEModel;
                    if ( massESIdToEventModelMap.containsKey( evt.mass_email_send_id)) {

                        massEEModel = massESIdToEventModelMap.get( evt.mass_email_send_id);
                    } else {

                        massEEModel = new MassEmailEventModel( evt.recipient_id, evt.mass_email_send_id, evt.email);
                    }
                    if ( evt.event == SchoolMassEmailSendModel.STATUS_DELIVERED) {

                        massEEModel.eventStatus = massEEModel.EVENT_DELIVERED;
                    } else if ( evt.event == SchoolMassEmailSendModel.STATUS_OPENED) {

                        massEEModel.eventStatus = massEEModel.EVENT_OPENED;
                    } else if ( evt.event == SchoolMassEmailSendModel.STATUS_BOUNCED) {

                        massEEModel.eventStatus = massEEModel.EVENT_BOUNCED;
                    } else if ( evt.event == SchoolMassEmailSendModel.STATUS_UNSUB) {

                        massEEModel.eventStatus = massEEModel.EVENT_UNSUB;
                    }
                    
                    massESIdToEventModelMap.put( evt.mass_email_send_id, massEEModel);
                }
            }
        }

        return massESIdToEventModelMap;
    }

    
    /* UI wrapper class */

    public class MassEmailEventModel {

        /* properties */
        public String recipId { get; set; }
        public String massEmailId { get; set; }
        public String massEmailName { get; set; }
        public String sentBy { get; set; }
        public String sentDate { get; set; }
        public String recipient { get; set; }
        public String eventStatus { get; set; }

        /* class constants */
        public final String EVENT_DELIVERED = 'Delivered';
        public final String EVENT_OPENED = 'Opened';
        public final String EVENT_BOUNCED = 'Bounced';
        public final String EVENT_UNSUB = 'Unsubscribed';

        // default ctor
        public MassEmailEventModel() {}

        // overridden ctor for constructing new MassEmailEventModel instances
        public MassEmailEventModel( String recipId, String massEmailId, String recipient) {

            this.recipId = ( String.isNotEmpty( recipId) ? recipId : this.recipId);
            this.massEmailId = ( String.isNotEmpty( massEmailId) ? massEmailId : this.massEmailId);
            this.recipient = ( String.isNotEmpty( recipient) ? recipient : this.recipient);
        }

    }

}