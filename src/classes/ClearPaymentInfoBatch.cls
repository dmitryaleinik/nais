/**
* Batch that daily clears payment fields of Chargent Order and Transaction records. 
*/
global class ClearPaymentInfoBatch implements Database.Batchable<sObject> {

    /**
     * @description String for the quering records from the Database.
     */
    private final String CLEAR_PAYMENT_INFO_BATCH_QUERY_STRING =
        'SELECT Id, ChargentOrders__Order__c FROM ChargentOrders__Transaction__c WHERE '
        + ' PaymentInfoIsCleared__c = false OR ChargentOrders__Order__r.PaymentInfoIsCleared__c = false';

    global ClearPaymentInfoBatch() {}
    
    /**
     * @description Gets a list of records that have to be handled from the Database.
     * @param bc System Batchable Context.
     */
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(CLEAR_PAYMENT_INFO_BATCH_QUERY_STRING);
    }

    /**
     * @description Gets a chunk of records that were selected and handles them.
     * @param bc System Batchable Context.
     * @param scope Chunk of the records to handle.
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        if (scope == null || scope.isEmpty()) {
            return;
        }

        ClearPaymentInfoService.Instance.handleTransactionAndOrderRecords(scope);
    }
    
    /**
     * @description Finish method to perform some operations after batch was processed.
     * @param bc System Batchable Context.
     */
    global void finish(Database.BatchableContext bc) {}
}