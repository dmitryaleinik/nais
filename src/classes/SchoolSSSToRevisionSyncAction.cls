/*
 * NAIS-882 - The revision fields should be kept in synch with the SSS/Orig fields both upon intial submission of the PFS 
 * as well as subsequent updates. Each field will need to be checked to see if it differs from the Orig counterpart, and 
 * if it does then leave it as is. If it is the same (meaning no revision has been made) then update it along with the Orig field.
 * 
 * SL 8/4/2013
 */
 
public with sharing class SchoolSSSToRevisionSyncAction {

    public static void syncOrigToRevisionFields(School_PFS_Assignment__c oldSPA, School_PFS_Assignment__c newSPA) {
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Salary_Wages_Parent_A__c', 'Salary_Wages_Parent_A__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Salary_Wages_Parent_B__c', 'Salary_Wages_Parent_B__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Interest_Income__c', 'Interest_Income__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Dividend_Income__c', 'Dividend_Income__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Alimony_Received__c', 'Alimony_Received__c');
        //syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Net_Profit_Loss_Business_Farm__c', 'Net_Profit_Loss_Business_Farm__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Taxable_Refunds__c', 'Taxable_Refunds__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Capital_Gain_Loss__c', 'Capital_Gain_Loss__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Other_Gain_Loss__c', 'Other_Gain_Loss__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_IRA_Distribution__c', 'IRA_Distribution__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Pensions_and_Annuities__c', 'Pensions_and_Annuities__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Rental_Real_Estate_Trusts_etc__c', 'Rental_Real_Estate_Trusts_etc__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Unemployment_Compensation__c', 'Unemployment_Compensation__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Social_Security_Benefits_Taxable__c', 'Social_Security_Benefits_Taxable__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Other_Income__c', 'Other_Income__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Deductible_Part_Self_Employment_Tax__c', 'Deductible_Part_of_Self_Employment_Tax__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Keogh_Plan_Payment__c', 'Keogh_Plan_Payment__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Untaxed_IRA_Plan_Payment__c', 'Untaxed_IRA_Plan_Payment__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Child_Support_Received__c', 'Child_Support_Received__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Social_Security_Benefits__c', 'Social_Security_Benefits__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Pre_Tax_Payment_to_Retirement_Plans__c', 'Pre_Tax_Payments_to_Retirement_Plans__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Pre_Tax_Fringe_Benefit_Contribution__c', 'Pre_Tax_Fringe_Benefit_Contribution__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Cash_Support_Gift_Income__c', 'Cash_Support_Gift_Income__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Other_Support_from_Divorced_Spouse__c', 'Other_Support_from_Divorced_Spouse__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Food_Housing_Other_Allowance__c', 'Food_Housing_Other_Allowance__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Earned_Income_Credits__c', 'Earned_Income_Credits__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Welfare_Veterans_and_Workers_Comp__c', 'Welfare_Veterans_and_Workers_Comp__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Tax_Exempt_Investment_Income__c', 'Tax_Exempt_Investment_Income__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Income_Earned_Abroad__c', 'Income_Earned_Abroad__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Other_Untaxed_Income__c', 'Other_Untaxed_Income__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Current_Tax_Return_Status__c', 'Current_Tax_Return_Status__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Filing_Status__c', 'Filing_Status__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Parent_B_Filing_Status__c', 'Parent_B_Filing_Status__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Income_Tax_Exemptions__c', 'Income_Tax_Exemptions__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Itemized_Deductions__c', 'Itemized_Deductions__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Total_SE_Tax_Paid__c', 'Total_SE_Tax_Paid__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Parent_State__c', 'Parent_State__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Medical_Dental_Expense__c', 'Medical_Dental_Expense__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Unusual_Expense__c', 'Unusual_Expense__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Home_Purchase_Year__c', 'Home_Purchase_Year__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Home_Purchase_Price__c', 'Home_Purchase_Price__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Home_Market_Value__c', 'Home_Market_Value__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Unpaid_Principal_1st_Mortgage__c', 'Unpaid_Principal_1st_Mortgage__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Unpaid_Principal_2nd_Mortgage__c', 'Unpaid_Principal_2nd_Mortgage__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Other_Real_Estate_Market_Value__c', 'Other_Real_Estate_Market_Value__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Other_Real_Estate_Unpaid_Principal__c', 'Other_Real_Estate_Unpaid_Principal__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Own_Home__c', 'Own_Home__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Business_Farm_Owner__c', 'Business_Farm_Owner__c');
        //syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Business_Farm_Ownership_Percent__c', 'Business_Farm_Ownership_Percent__c');
        //syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Business_Farm_Assets__c', 'Business_Farm_Assets__c');
        //syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Business_Farm_Debts__c', 'Business_Farm_Debts__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Bank_Accounts__c', 'Bank_Accounts__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Investments__c', 'Investments__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Total_Debts__c', 'Total_Debts__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Dependent_Children__c', 'Dependent_Children__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Family_Size__c', 'Family_Size__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Num_Children_in_Tuition_Schools__c', 'Num_Children_in_Tuition_Schools__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Student_Assets__c', 'Student_Assets__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Grade_Applying__c', 'Grade_Applying__c');
        syncOrigToRevisionField(oldSPA, newSPA, 'Orig_Other_Adjustments_to_Income__c', 'Other_Adjustments_to_Income__c'); // NAIS-990
    }

    // [DP] 08.10.2015 NAIS-2529 also sync SBFA fields
    public static void syncOrigToRevisionFields(School_Biz_Farm_Assignment__c oldSBFA, School_Biz_Farm_Assignment__c newSBFA) {
        syncOrigToRevisionField(oldSBFA, newSBFA, 'Orig_Business_Farm_Assets__c', 'Business_Farm_Assets__c');
        syncOrigToRevisionField(oldSBFA, newSBFA, 'Orig_Business_Farm_Debts__c', 'Business_Farm_Debts__c');
        syncOrigToRevisionField(oldSBFA, newSBFA, 'Orig_Business_Farm_Owner__c', 'Business_Farm_Owner__c');
        syncOrigToRevisionField(oldSBFA, newSBFA, 'Orig_Business_Farm_Ownership_Percent__c', 'Business_Farm_Ownership_Percent__c');
        syncOrigToRevisionField(oldSBFA, newSBFA, 'Orig_Business_Farm_Share__c', 'Business_Farm_Share__c');
        syncOrigToRevisionField(oldSBFA, newSBFA, 'Orig_Net_Profit_Loss_Business_Farm__c', 'Net_Profit_Loss_Business_Farm__c');

        // SFP-1617 Sync Orig to revision fields for Business Entity Type.
        // This is only necessary if the feature toggle is on and the biz farm is for 2018-2019 or beyond.
        // Before 2018-2019, we did not track business entity type on SBFAs.
        if (FeatureToggles.isEnabled('Exclude_S_Corps_Partnerships_From_Sums__c') && AcademicYearUtil.is20182019OrLater(newSBFA.Academic_Year__c)) {
            syncOrigToRevisionField(oldSBFA, newSBFA, 'Orig_Business_Entity_Type__c', 'Business_Entity_Type__c');
        }
    }

    
    private static void syncOrigToRevisionField(School_PFS_Assignment__c oldSPA, School_PFS_Assignment__c newSPA, String origFieldName, String revisionFieldName) {
        if (((oldSPA == null) && (newSPA.get(revisionFieldName) == null))
                || ((oldSPA != null) && (newSPA.get(origFieldName) != oldSPA.get(origFieldName)) 
                        && ((oldSPA.get(revisionFieldName) == oldSPA.get(origFieldName)) || (newSPA.get(revisionFieldName) == null)))) {
            newSPA.put(revisionFieldName, newSPA.get(origFieldName));     
        }
    }

    // [DP] 08.10.2015 NAIS-2529 also sync SBFA fields
    private static void syncOrigToRevisionField(School_Biz_Farm_Assignment__c oldSBFA, School_Biz_Farm_Assignment__c newSBFA, String origFieldName, String revisionFieldName) {
        if (((oldSBFA == null) && (newSBFA.get(revisionFieldName) == null))
                || ((oldSBFA != null) && (newSBFA.get(origFieldName) != oldSBFA.get(origFieldName)) 
                        && ((oldSBFA.get(revisionFieldName) == oldSBFA.get(origFieldName)) || (newSBFA.get(revisionFieldName) == null)))) {
            newSBFA.put(revisionFieldName, newSBFA.get(origFieldName));     
        }
    }

}