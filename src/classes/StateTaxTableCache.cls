/**
 *  @description Class for handling retrieving the State_Tax_Table__c records
 *               from the platform cache by academic year Id.
 *               The cache key will be "stateTax“ + academic year id
 */
public with sharing class StateTaxTableCache
{

    @testVisible
    private static final String CACHE_KEY_PREFIX = 'local.sssSystem.stateTax';
    @testVisible
    private static String queryTaxTablesException = 'State Tax Tables for the Academic Year Id = {0} were not found. '
        + 'Please contact your administrator for assistance.';

    /**
     *  @description Checks the cache first and if null, queries the records.
     *               After querying, populate the cache.
     *  @param academicYearId Id of an academic year to use it as criteria in query
     */
    public static Map<String, List<State_Tax_Table__c>> getTaxTables(Id academicYearId)
    {
        String cacheKey = CACHE_KEY_PREFIX + String.valueOf(academicYearId);

        Map<String, List<State_Tax_Table__c>> stateTaxTablesMapping = 
            (Map<String, List<State_Tax_Table__c>>) Cache.Org.get(StateTaxTableInfoCache.class, cacheKey);
        
        if (stateTaxTablesMapping.isEmpty()) {
            
            throw new TaxTablesException(queryTaxTablesException.replace('{0}', academicYearId));
        }

        return stateTaxTablesMapping;
    }

    private static List<State_Tax_Table__c> queryTaxTables(Id academicYearId)
    {
        return [
            SELECT Income_Low__c, Income_High__c, Percent_of_Total_Income__c, State__c
            FROM State_Tax_Table__c 
            WHERE Academic_Year__c = :academicYearId
              ORDER BY Income_Low__c ASC];
    }

    private static Map<String, List<State_Tax_Table__c>> buildStateTaxTablesMapping(List<State_Tax_Table__c> stateTaxTablesList)
    {
        Map<String, List<State_Tax_Table__c>> newStateTaxTablesMapping = new Map<String, List<State_Tax_Table__c>>();
        for (State_Tax_Table__c table : stateTaxTablesList)
        {
            List<State_Tax_Table__c> tablesForState = newStateTaxTablesMapping.get(table.State__c);
            if (tablesForState == null)
            {
                newStateTaxTablesMapping.put(table.State__c, new List<State_Tax_Table__c>{table});
            }
            else
            {
                tablesForState.add(table);
                newStateTaxTablesMapping.put(table.State__c, tablesForState);
            }
        }

        return newStateTaxTablesMapping;
    }

    private class StateTaxTableInfoCache implements Cache.CacheBuilder 
    {
        public Object doLoad(String cacheKey) 
        {
            //doLoad can never return null. The returned value can be of any type which you then cast to the appropriate type.
            //This means that we can not return null or thrown an exception from here. Since, it will cause an unexpected exception
            //that can not be cached in code.
            Map<String, List<State_Tax_Table__c>> stateTaxTablesMapping = new Map<String, List<State_Tax_Table__c>>();
            String academicYearId = Id.valueOf(cacheKey.removeStart('stateTax'));
            List<State_Tax_Table__c> stateTaxTables = (List<State_Tax_Table__c>)queryTaxTables(academicYearId);

            if (stateTaxTables != null && !stateTaxTables.isEmpty())
            {
                stateTaxTablesMapping = buildStateTaxTablesMapping(stateTaxTables);
            }
            
            return stateTaxTablesMapping;
        }
    }

    private class TaxTablesException extends Exception {}
}