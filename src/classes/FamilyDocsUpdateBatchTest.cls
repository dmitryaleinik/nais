@isTest
private class FamilyDocsUpdateBatchTest {

    @isTest
    private static void execute_familyDocToUpdate_withoutW2DupeCheck_expectDocUpdated() {
        Family_Document__c document = FamilyDocumentTestData.Instance.insertFamilyDocument();

        document.Document_Pertains_to__c = 'Parent B';

        FamilyDocsUpdateBatch newBatch = new FamilyDocsUpdateBatch(new List<Family_Document__c> { document }, false);

        Test.startTest();
        Database.executeBatch(newBatch);
        Test.stopTest();

        document = [SELECT Id, Document_Pertains_to__c FROM Family_Document__c WHERE Id = :document.Id LIMIT 1];

        System.assertEquals('Parent B', document.Document_Pertains_to__c, 'Expected the doc to be updated.');
    }
}