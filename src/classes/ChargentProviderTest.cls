@isTest
public class ChargentProviderTest {
    private static final String ACADEMIC_YEAR = (System.today().year()+1) + '-' + (System.today().year() + 2);
    private static final String SUBSCRIPTION_TYPE = 'Full';

    @testSetup
    private static void setup() {
        // We need to set it to something to ensure it gets created
        Transaction_Settings__c transactionSettings = TransactionSettingsTestData.Instance.DefaultTransactionSettings;
        TransactionSettingsTestData.Instance.asRefund().insertTransactionSettings();
    }

    private static PaymentResult buildSuccessfulResult() {
        PaymentResult result = new PaymentResult();
        result.IsSuccess = true;
        result.ErrorOccurred = !result.IsSuccess;
        result.transactionResultCode = 0;
        return result;
    }

    private static void assertPaymentInformation(PaymentInformation paymentInformation, Opportunity opp) {
        System.assertNotEquals(null, paymentInformation, 'Expected the paymentInformation to not be null.');
        System.assertNotEquals(null, opp, 'Expected the opportunity to not be null.');
        System.assertNotEquals(null, opp.Chargent_Order__c, 'Expected there to be an order populated on the opportunity.');

        List<ChargentOrders__ChargentOrder__c> orders = ChargentOrderSelector.Instance
                .selectById(new Set<Id> { opp.Chargent_Order__c });

        System.assertNotEquals(null, orders, 'Expected the result list to not be null.');
        System.assertEquals(1, orders.size(), 'Expected the result list to have exactly one order in it.');

        ChargentOrders__ChargentOrder__c order = orders[0];

        System.assertEquals(paymentInformation.PaymentAmount, order.ChargentOrders__Charge_Amount__c);
        System.assertEquals(paymentInformation.BillingFirstName, order.ChargentOrders__Billing_First_Name__c);
        System.assertEquals(paymentInformation.BillingLastName, order.ChargentOrders__Billing_Last_Name__c);
        System.assertEquals(paymentInformation.BillingStreet1, order.ChargentOrders__Billing_Address__c);
        System.assertEquals(paymentInformation.BillingStreet2, order.ChargentOrders__Billing_Address_Line_2__c);
        System.assertEquals(paymentInformation.BillingCity, order.ChargentOrders__Billing_City__c);
        System.assertEquals(paymentInformation.BillingState, order.ChargentOrders__Billing_State__c);
        System.assertEquals(paymentInformation.BillingPostalCode, order.ChargentOrders__Billing_Zip_Postal__c);
        System.assertEquals(paymentInformation.BillingCountryCode, order.ChargentOrders__Billing_Country__c);
        System.assertEquals(paymentInformation.BillingEmail, order.ChargentOrders__Billing_Email__c);

        if (paymentInformation instanceof CreditCardInformation) {
            CreditCardInformation ccInfo = (CreditCardInformation)paymentInformation;

            System.assertEquals(ccInfo.CardType, order.ChargentOrders__Card_Type__c);
            System.assertEquals(ccInfo.NameOnCard, order.ChargentOrders__Credit_Card_Name__c);
            System.assertEquals(ccInfo.CardNumber, order.ChargentOrders__Card_Number__c);
            System.assertEquals(ccInfo.Cvv2, order.ChargentOrders__Card_Security_Code__c);
            System.assertEquals(ccInfo.ExpirationMM, order.ChargentOrders__Card_Expiration_Month__c);
            System.assertEquals(ccInfo.ExpirationYY, order.ChargentOrders__Card_Expiration_Year__c);
            System.assertEquals('Credit Card', order.ChargentOrders__Payment_Method__c);
        } else if (paymentInformation instanceof ECheckInformation) {
            ECheckInformation ecInfo = (ECheckInformation)paymentInformation;

            System.assertEquals(ecInfo.BankName, order.ChargentOrders__Bank_Name__c);
            System.assertEquals(ecInfo.AccountNumber, order.ChargentOrders__Bank_Account_Number__c);
            System.assertEquals(ecInfo.RoutingNumber, order.ChargentOrders__Bank_Routing_Number__c);
            System.assertEquals(ecInfo.AccountType, order.ChargentOrders__Bank_Account_Type__c);
            System.assertEquals('Check', order.ChargentOrders__Payment_Method__c);
        }
    }

    @isTest
    private static void processBefore_creditCard_expectSuccessfulResult() {
        CreditCardInformation ccInfo = PaymentInformationTestData.Instance.asCreditCardInformation();

        PaymentRequest request = new PaymentRequest();
        request.PaymentInformation = ccInfo;
        request.OpportunityId = OpportunityTestData.Instance.DefaultOpportunity.Id;

        ChargentProvider provider = new ChargentProvider();

        Test.startTest();
        PaymentResult result = provider.processBefore(request);
        Test.stopTest();

        System.assert(result.IsSuccess, 'Expected the result to be a success.');

        Opportunity opportunity = OpportunitySelector.Instance.selectByIdWithPfs(new Set<Id> { request.OpportunityId })[0];
        assertPaymentInformation(ccInfo, opportunity);
    }

    @isTest
    private static void processBefore_check_expectSuccessfulResult() {
        ECheckInformation ecInfo = PaymentInformationTestData.Instance.asECheckInformation();

        PaymentRequest request = new PaymentRequest();
        request.PaymentInformation = ecInfo;
        request.OpportunityId = OpportunityTestData.Instance.DefaultOpportunity.Id;

        ChargentProvider provider = new ChargentProvider();

        Test.startTest();
        PaymentResult result = provider.processBefore(request);
        Test.stopTest();

        System.assert(result.IsSuccess, 'Expected the result to be a success.');

        Opportunity opportunity = OpportunitySelector.Instance.selectByIdWithPfs(new Set<Id> { request.OpportunityId })[0];
        assertPaymentInformation(ecInfo, opportunity);
    }

    @isTest
    private static void processBefore_refund_expectSuccessfulResult() {
        RefundRequest request = new RefundRequest(new Transaction_Line_Item__c());
        ChargentProvider provider = new ChargentProvider();

        Test.startTest();
        PaymentResult result = provider.processBefore(request);
        Test.stopTest();

        // There is no handling in the processBefore for refunds. This should be a success.
        System.assert(result.IsSuccess, 'Expected the result to be a success.');
    }

    @isTest
    private static void processBefore_paymentProcessorRequest_expectArgumentException() {
        ChargentProvider provider = new ChargentProvider();

        Test.startTest();
        PaymentResult result = provider.processBefore(new PaymentProcessorRequest());
        Test.stopTest();

        System.assert(!result.IsSuccess, 'Expected the process to be a failure.');
        System.assert(result.ErrorOccurred, 'Expected there to be an error.');
        System.assertEquals(ChargentProvider.UNEXPECTED_OBJECT_TO_PROCESS, result.ErrorMessage,
                'Expected the error messages to match.');
    }

    @isTest
    private static void processBefore_paymentInformationBase_expectArgumentException() {
        ChargentProvider provider = new ChargentProvider();
        PaymentRequest request = new PaymentRequest();
        request.PaymentInformation = new PaymentInformation();

        Test.startTest();
        PaymentResult result = provider.processBefore(request);
        Test.stopTest();

        System.assert(!result.IsSuccess, 'Expected the process to be a failure.');
        System.assert(result.ErrorOccurred, 'Expected there to be an error.');
        System.assertEquals('Expected Opportunity to not be null.', result.ErrorMessage,
                'Expected the error messages to match.');
    }

    @isTest
    private static void processBefore_nullParam_expectArgumentNullException() {
        ChargentProvider provider = new ChargentProvider();
        try {
            Test.startTest();
            provider.processBefore(null);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, ChargentProvider.REQUEST_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void process_creditCard_expectSuccess() {
        ChargentBase__Gateway__c gateway = ChargentBaseGatewayTestData.Instance.DefaultGateway;
        CreditCardInformation ccInfo = PaymentInformationTestData.Instance.asCreditCardInformation();

        PaymentRequest request = new PaymentRequest();
        request.PaymentInformation = ccInfo;
        request.OpportunityId = OpportunityTestData.Instance.DefaultOpportunity.Id;

        ChargentProvider provider = new ChargentProvider();
        provider.processBefore(request);

        Test.startTest();
        PaymentResult result = provider.process(request);
        Test.stopTest();

        System.assert(result.IsSuccess, 'Expected the result to be a success.');
    }

    @isTest
    private static void process_invalidCreditCard_expectFailure() {
        ChargentBase__Gateway__c gateway = ChargentBaseGatewayTestData.Instance.DefaultGateway;
        CreditCardInformation ccInfo = PaymentInformationTestData.Instance.asCreditCardInformation();
        ccInfo.CardNumber = '123456';

        PaymentRequest request = new PaymentRequest();
        request.PaymentInformation = ccInfo;
        request.OpportunityId = OpportunityTestData.Instance.DefaultOpportunity.Id;

        ChargentProvider provider = new ChargentProvider();
        provider.processBefore(request);

        Test.startTest();
        PaymentResult result = provider.process(request);
        Test.stopTest();

        System.assert(!result.IsSuccess, 'Expected the result to not be a success.');

        String expectedError = String.format(ChargentProvider.DEFAULT_ERROR_MESSAGE, new List<String> {
                String.valueOf(PaymentProviderTestHelpers.ERROR_CODE),
                PaymentProviderTestHelpers.INVALID_CREDIT_CARD_NUMBER,
                PaymentProviderTestHelpers.INVALID_CREDIT_CARD_NUMBER
        });
        System.assertEquals(expectedError, result.ErrorMessage);
    }

    @isTest
    private static void process_invalidAVSCreditCard_expectFailure() {
        ChargentBase__Gateway__c gateway = ChargentBaseGatewayTestData.Instance.DefaultGateway;
        CreditCardInformation ccInfo = PaymentInformationTestData.Instance.asCreditCardInformation();
        ccInfo.BillingPostalCode = '99999';

        PaymentRequest request = new PaymentRequest();
        request.PaymentInformation = ccInfo;
        request.OpportunityId = OpportunityTestData.Instance.DefaultOpportunity.Id;

        ChargentProvider provider = new ChargentProvider();
        provider.processBefore(request);

        Test.startTest();
        PaymentResult result = provider.process(request);
        Test.stopTest();

        System.assert(!result.IsSuccess, 'Expected the result to not be a success.');

        String expectedError = String.format(ChargentProvider.DEFAULT_ERROR_MESSAGE, new List<String> {
                String.valueOf(PaymentProviderTestHelpers.ERROR_CODE),
                Label.ZipAndAddressDoNotMatch,
                Label.ZipAndAddressDoNotMatch
        });
        System.assertEquals(expectedError, result.ErrorMessage);
    }

    @isTest
    private static void process_check_expectSuccess() {
        ChargentBaseGatewayTestData.Instance.insertGateway();
        ECheckInformation ecInfo = PaymentInformationTestData.Instance.asECheckInformation();

        PaymentRequest request = new PaymentRequest();
        request.PaymentInformation = ecInfo;
        request.OpportunityId = OpportunityTestData.Instance.DefaultOpportunity.Id;

        ChargentProvider provider = new ChargentProvider();
        provider.processBefore(request);

        Test.startTest();
        PaymentResult result = provider.process(request);
        Test.stopTest();

        System.assert(result.IsSuccess, 'Expected the result to be a success.');
    }

    @isTest
    private static void process_invalidCheck_expectFailure() {
        ChargentBaseGatewayTestData.Instance.insertGateway();
        ECheckInformation ecInfo = PaymentInformationTestData.Instance.asECheckInformation();
        ecInfo.BankName = null;

        PaymentRequest request = new PaymentRequest();
        request.PaymentInformation = ecInfo;
        request.OpportunityId = OpportunityTestData.Instance.DefaultOpportunity.Id;

        ChargentProvider provider = new ChargentProvider();
        provider.processBefore(request);

        Test.startTest();
        PaymentResult result = provider.process(request);
        Test.stopTest();

        System.assert(!result.IsSuccess, 'Expected the result to not be a success.');

        String expectedError = String.format(ChargentProvider.DEFAULT_ERROR_MESSAGE, new List<String> {
                String.valueOf(PaymentProviderTestHelpers.ERROR_CODE),
                PaymentProcessor.UNEXPECTED_ERROR,
                PaymentProcessor.UNEXPECTED_ERROR
        });
        System.assertEquals(expectedError, result.ErrorMessage);
    }

    @isTest
    private static void process_refund_expectSuccess() {
        ChargentBaseGatewayTestData.Instance.insertGateway();

        Opportunity opp = OpportunityTestData.Instance.DefaultOpportunity;
        Transaction_Line_Item__c tli = new Transaction_Line_Item__c(Opportunity__c = opp.Id);
        insert tli;
        RefundRequest request = new RefundRequest(tli);

        ChargentProvider provider = new ChargentProvider();
        provider.processBefore(request);

        Test.startTest();
        PaymentResult result = provider.process(request);
        Test.stopTest();

        System.assert(result.IsSuccess, 'Expected the result to be a success.');
    }

    @isTest
    private static void process_nullParam_expectArgumentNullException() {
        ChargentProvider provider = new ChargentProvider();
        try {
            Test.startTest();
            provider.process(null);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, ChargentProvider.REQUEST_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void process_paymentProcessorRequest_expectArgumentException() {
        ChargentProvider provider = new ChargentProvider();
        try {
            Test.startTest();
            provider.process(new PaymentProcessorRequest());
            TestHelpers.expectedArgumentException();
        } catch (Exception e) {
            TestHelpers.assertArgumentException(e, ChargentProvider.UNEXPECTED_OBJECT_TO_PROCESS);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void processAfter_creditCard_expectTliCreated() {
        ChargentProvider provider = new ChargentProvider();
        PaymentResult result = buildSuccessfulResult();
        PaymentRequest request = new PaymentRequest();
        request.OpportunityId = OpportunityTestData.Instance.DefaultOpportunity.Id;
        request.PaymentInformation = PaymentInformationTestData.Instance.asCreditCardInformation();
        result.Request = request;

        Test.startTest();
        result = provider.processAfter(result);
        Test.stopTest();

        System.assert(result.IsSuccess);
        System.assertNotEquals(null, result.TransactionLineItemId);

        Id paymentRecordTypeId = RecordTypes.paymentAutoTransactionTypeId;
        Transaction_Line_Item__c paymentTli = TransactionLineItemSelector.Instance.selectById(new Set<Id> { result.TransactionLineItemId })[0];
        System.assertEquals(paymentRecordTypeId, paymentTli.RecordTypeId);
        System.assertEquals(47.00, paymentTli.Amount__c);
    }

    @isTest
    private static void processAfter_check_expectTliCreated() {
        ChargentProvider provider = new ChargentProvider();
        PaymentResult result = buildSuccessfulResult();
        PaymentRequest request = new PaymentRequest();
        request.OpportunityId = OpportunityTestData.Instance.DefaultOpportunity.Id;
        request.PaymentInformation = PaymentInformationTestData.Instance.asECheckInformation();
        result.Request = request;

        Test.startTest();
        result = provider.processAfter(result);
        Test.stopTest();

        System.assert(result.IsSuccess);
        System.assertNotEquals(null, result.TransactionLineItemId);

        Id paymentRecordTypeId = RecordTypes.paymentAutoTransactionTypeId;
        Transaction_Line_Item__c paymentTli = TransactionLineItemSelector.Instance.selectById(new Set<Id> { result.TransactionLineItemId })[0];
        System.assertEquals(paymentRecordTypeId, paymentTli.RecordTypeId);
        System.assertEquals(47.00, paymentTli.Amount__c);
    }

    @isTest
    private static void processAfter_refund_expectTliCreated() {
        ChargentProvider provider = new ChargentProvider();
        Opportunity opp = OpportunityTestData.Instance.DefaultOpportunity;
        PaymentResult result = buildSuccessfulResult();
        Id transactionLineItemId = PaymentUtils.insertCreditCardTransactionLineItem(opp.Id, 47.00, 'Visa', '4242', result);
        Transaction_Line_Item__c tli = TransactionLineItemSelector.Instance.selectById(new Set<Id> { transactionLineItemId })[0];
        RefundRequest request = new RefundRequest(tli);
        result.Request = request;

        Test.startTest();
        result = provider.processAfter(result);
        Test.stopTest();

        System.assert(result.IsSuccess);
        System.assertNotEquals(null, result.TransactionLineItemId);

        Id refundRecordTypeId = RecordTypes.refundTransactionTypeId;
        Transaction_Line_Item__c refundTli = TransactionLineItemSelector.Instance.selectById(new Set<Id> { result.TransactionLineItemId })[0];
        System.assertEquals(refundRecordTypeId, refundTli.RecordTypeId);
        System.assertEquals(47.00, refundTli.Amount__c);
        System.assertEquals(TransactionSettingsTestData.REFUND_ACCOUNT_CODE, refundTli.Account_Code__c);
        System.assertEquals(TransactionSettingsTestData.REFUND_TRANSACTION_CODE, refundTli.Transaction_Code__c);
        System.assertEquals(TransactionSettingsTestData.REFUND_ACCOUNT_LABEL, refundTli.Account_Label__c);
    }

    @isTest
    private static void processAfter_nullParam_expectArgumentNullException() {
        ChargentProvider provider = new ChargentProvider();
        try {
            Test.startTest();
            provider.processAfter(null);
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, ChargentProvider.OBJECT_TO_PARSE_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void processAfter_invalidObject_expectArgumentException() {
        ChargentProvider provider = new ChargentProvider();
        try {
            Test.startTest();
            provider.processAfter(new Account());
            TestHelpers.expectedArgumentException();
        } catch (Exception e) {
            TestHelpers.assertArgumentException(e, ChargentProvider.UNEXPECTED_OBJECT_TO_PROCESS);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void processAfter_requestNull_expectArgumentNullException() {
        ChargentProvider provider = new ChargentProvider();
        try {
            Test.startTest();
            provider.processAfter(new PaymentResult());
            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, ChargentProvider.REQUEST_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void processAfter_invalidRequest_expectArgumentException() {
        ChargentProvider provider = new ChargentProvider();
        PaymentResult result = new PaymentResult();
        result.Request = new PaymentProcessorRequest();

        try {
            Test.startTest();
            provider.processAfter(result);
        } catch (Exception e) {
            TestHelpers.assertArgumentException(e, ChargentProvider.UNEXPECTED_OBJECT_TO_PROCESS);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void processAfter_requestNotCheckOrCc_expectArgumentException() {
        ChargentProvider provider = new ChargentProvider();
        PaymentResult result = new PaymentResult();
        PaymentRequest request = new PaymentRequest();
        request.PaymentInformation = new PaymentInformation();
        result.Request = request;

        try {
            Test.startTest();
            provider.processAfter(result);
        } catch (Exception e) {
            TestHelpers.assertArgumentException(e, ChargentProvider.UNEXPECTED_OBJECT_TO_PROCESS);
        } finally {
            Test.stopTest();
        }
    }
}