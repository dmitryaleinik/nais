/**
 * IntegrationOAuthProviderTest
 *
 * @description
 *
 * @author Mike Havrilla @ Presence PG
 */
@IsTest
private class IntegrationOAuthProviderTest {

    private static final String OAUTH_TOKEN = 'testToken';
    private static final String STATE = 'mocktestState';
    private static final String REFRESH_TOKEN = 'oauthSecret';
    private static final String LOGIN_ID = 'testLoginId';
    private static final String USERNAME = 'testUsername';
    private static final String FIRST_NAME = 'testFirstName';
    private static final String LAST_NAME = 'testLastName';
    private static final String EMAIL_ADDRESS = 'testEmailAddress';
    private static final String LOCALE_NAME = 'testLocalName';
    private static final String FULL_NAME = FIRST_NAME + ' ' + LAST_NAME;
    private static final String PROVIDER = 'sao';
    private static final String REDIRECT_URL = 'http://localhost/services/authcallback/orgId/soa';
    private static final String KEY = 'testKey';
    private static final String SECRET = 'testSecret';
    private static final String STATE_TO_PROPOGATE  = 'testState';
    private static final String ACCESS_TOKEN_URL = 'http://www.dummyhost.com/accessTokenUri';
    private static final String API_USER_VERSION_URL = 'http://www.dummyhost.com/user/20/1';
    private static final String AUTH_URL = 'http://www.dummy.com/authurl';
    private static final String API_USER_URL = 'www.ssatb-api-dev.azurewebsites.net.com/user/api';

    // in the real world scenario , the key and value would be read from the (custom fields in) custom metadata type record
    private static Map<String,String> setupAuthProviderConfig () {
        Map<String,String> authProviderConfiguration = new Map<String,String>();
        authProviderConfiguration.put('ConsumerKey__c', KEY);
        authProviderConfiguration.put('ConsumerSecret__c', SECRET);
        authProviderConfiguration.put('TokenUrl__c', ACCESS_TOKEN_URL);
        authProviderConfiguration.put('RedirectUrl__c',REDIRECT_URL);
        authProviderConfiguration.put('AuthUrl__c', AUTH_URL);
        authProviderConfiguration.put('API_User_Url__c',API_USER_URL);
        authProviderConfiguration.put('API_User_Version_Url__c',API_USER_VERSION_URL);
        authProviderConfiguration.put('ProviderName__c', PROVIDER);
        return authProviderConfiguration;
    }

    @isTest static void init_successInit() {
        String stateToPropogate = 'mocktestState';
        Map<String,String> authProviderConfiguration = setupAuthProviderConfig();
        IntegrationOAuthProvider saoOauth = new IntegrationOAuthProvider();
        saoOauth.redirectUrl = authProviderConfiguration.get('Redirect_Url__c');

        PageReference expectedUrl =  new PageReference(authProviderConfiguration.get('AuthUrl__c') + '?client_id='+
                authProviderConfiguration.get('ConsumerKey__c') +'&scope=null&redirect_uri=' +
                authProviderConfiguration.get('RedirectUrl__c') +
                '&response_type=code&' +
                authProviderConfiguration.get('ConsumerSecret__c') + '&state=' +
                STATE_TO_PROPOGATE);
        PageReference actualUrl = saoOauth.initiate(authProviderConfiguration, STATE_TO_PROPOGATE);
        System.assertEquals(expectedUrl.getUrl(), actualUrl.getUrl());
    }

    @isTest static void callback_handleCallback_Success() {
        Map<String,String> authProviderConfiguration = setupAuthProviderConfig();
        IntegrationOAuthProvider saoOauth = new IntegrationOAuthProvider();
        saoOauth.redirectUrl = authProviderConfiguration.get('Redirect_Url__c');

        Test.setMock(HttpCalloutMock.class, new SAOMockHttpResponseGenerator());

        Map<String,String> queryParams = new Map<String,String>();
        queryParams.put('code','code');
        queryParams.put('state',authProviderConfiguration.get('State_c'));

        Auth.AuthProviderCallbackState cbState = new Auth.AuthProviderCallbackState(null,null,queryParams);
        Auth.AuthProviderTokenResponse actualAuthProvResponse = saoOauth.handleCallback(authProviderConfiguration, cbState);
        Auth.AuthProviderTokenResponse expectedAuthProvResponse = new Auth.AuthProviderTokenResponse('sao', OAUTH_TOKEN, REFRESH_TOKEN, null);

        System.assertEquals(expectedAuthProvResponse.provider, actualAuthProvResponse.provider);
        System.assertEquals('4a07e019-abaa-e611-99ae-000d3a1641e5:' + OAUTH_TOKEN, actualAuthProvResponse.oauthToken);
        System.assertEquals(expectedAuthProvResponse.oauthSecretOrRefreshToken, actualAuthProvResponse.oauthSecretOrRefreshToken);
        System.assertEquals(expectedAuthProvResponse.state, actualAuthProvResponse.state);
    }

    // implementing a mock http response generator for concur
    public class SAOMockHttpResponseGenerator implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {

            // Make sure you double escape nested objects, so that you can parse it with JSON.deserializedUntypes(String);
            String response = '{"claims": "{ \\"gat\\":1488169267,\\"sub\\":[\\"4a07e019-abaa-e611-99ae-000d3a1641e5\\",\\"TestSSS\\"],\\"amr\\":\\"password\\",\\"auth_time\\":1488139234,\\"idp\\":\\"idsrv\\",\\"name\\":\\"Test Student SSS\\" }", "scope": "openid profile offline_access schoolapp", "access_token": "testToken"}';
            HttpResponse res = new HttpResponse();
            res.setHeader( 'Content-Type', 'application/json');
            res.setBody( response);
            res.setStatusCode( 200);

            return res;
        }

    }
}