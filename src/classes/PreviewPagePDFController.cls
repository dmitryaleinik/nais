public with sharing class PreviewPagePDFController implements ISpringCMDocPreviewer{

   @testVisible private ISpringCMDocPreviewer handler;

   public PreviewPagePDFController() {
       
         if (FeatureToggles.isEnabled('SpringCM_REST_Preview_Enabled__c') ) {
                handler = new SpringCMDocPreviewer_REST();
          }else{
                handler = new SpringCMDocPreviewer_SOAP();
          }

   }

   public PageReference getPDF() {
        return handler.getPDF();
   }
   
   public string getPublishUrl(){
        return handler.getPublishUrl();
   }
   
   public String getErrorMessage() {
        return handler.getErrorMessage();
   }
}