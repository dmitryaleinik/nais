/**
 * @description Service class used to determine who a document pertains to based on the data on a SpringCM_Document__c record and the data for the related PFS__c.
 */
public without sharing class DocumentNameMatchService {

    private static final String SPRING_CM_DOCUMENTS_PARAM = 'springCmDocumentRecords';

    // These Pertains To constants refer to the values we expected from 3rd party vendors in the past.
    // We convert these to a full value in our system. For example, A becomes Parent A.
    @testVisible private static final String PERTAINS_TO_PARENT_A = 'A';
    @testVisible private static final String PERTAINS_TO_PARENT_B = 'B';
    @testVisible private static final String PERTAINS_TO_STUDENT = 'S';
    @testVisible private static final String PERTAINS_TO_DEPENDENT = 'D';

    private static final List<Schema.SObjectField> DEPENDENT_NAMES_TO_MATCH_DOCS_AGAINST = new List<Schema.SObjectField> {
            Dependents__c.Dependent_1_Full_Name__c,
            Dependents__c.Dependent_2_Full_Name__c,
            Dependents__c.Dependent_3_Full_Name__c,
            Dependents__c.Dependent_4_Full_Name__c,
            Dependents__c.Dependent_5_Full_Name__c,
            Dependents__c.Dependent_6_Full_Name__c
    };

    private DocumentNameMatchService() {}

    /**
     * @description Singleton instance for this service class.
     */
    public static DocumentNameMatchService Instance {
        get {
            if (Instance == null) {
                Instance = new DocumentNameMatchService();
            }
            return Instance;
        }
        private set;
    }

    /**
    * @description When a SpringCM record comes back from Scalehub, we should do some matching to determine if its for
    *              Parent A, Parent B, Student, or Dependent. If we find a match, the corresponding pertains to value is
    *              set on the SpringCM_Document__c record. If no match is found, we leave the pertains to value blank on
    *              the SpringCM_Document__c record and check Match_Not_Found__c. Leaving the pertains to field
    *              blank causes the rest of the SpringCM logic to use the original pertains to value on the family document
    *              record.
    * @param springCmDocumentRecords The list of SpringCM_Document__c records that triggered this logic.
    * @throws ArgumentNullException if springCmDocumentRecords is null.
    */
    public void setDocumentPertainsTo(List<SpringCM_Document__c> springCmDocumentRecords) {
        if (!FeatureToggles.isEnabled('Match_Parent_To_SpringCM_Doc__c')) {
            return;
        }

        try {
            doSetDocumentPertainsTo(springCmDocumentRecords);
        } catch (Exception ex) {
            CustomDebugService.Instance.logException(ex);
        }
    }

    private void doSetDocumentPertainsTo(List<SpringCM_Document__c> springCMDocumentRecords) {
        ArgumentNullException.throwIfNull(springCmDocumentRecords, SPRING_CM_DOCUMENTS_PARAM);

        //0. Get the PFSNumbers from SpringCMDocument records.
        Set<String> pfsNumbers = new Set<String>();

        for (SpringCM_Document__c scd : springCmDocumentRecords) {
            if (String.isNotBlank(scd.PFS_Number__c)) {
                pfsNumbers.add(scd.PFS_Number__c);
            }
        }

        //1. Get a map of DocumentMatchProcessors by PFS_Number. The processors handle matching the document to PFS, Applicants, and Dependents.
        Map<String, DocumentMatchProcessor> documentMatchersPfsNumber = getDocumentMatchersByPfsNumber(pfsNumbers);
        if (documentMatchersPfsNumber.isEmpty()) {
            return;
        }

        //2. Determine if the given SpringCM Document records, is for parentA, parentB, one of the applicants, or one of the dependents based on its First/Last name fields.
        for (SpringCM_Document__c scd : springCmDocumentRecords) {
            // If the pertains to field is specified on the SpringCM Document, it was done as part of the verification process so we don't need to attempt to match.
            if (String.isNotBlank(scd.Document_Pertains_to__c)) {
                // SFP-1751: We will now set a flag on the SpringCM Document so we can identify the records where the Document Pertains To was provided by the document processor.
                scd.Match_Not_Found__c = true;
                scd.Match_Not_Found_Reason__c = SpringCmDocuments.MATCH_PROVIDED;
                continue;
            }

            // If the document was mailed in, don't attempt to match. We don't set the Match Not Found info here because mailed in documents will have the pertains to already set.
            if (SpringCmDocuments.DOC_SOURCE_MAILED.equalsIgnoreCase(scd.Document_Source__c)) {
                continue;
            }

            DocumentMatchProcessor docMatcher = documentMatchersPfsNumber.get(scd.PFS_Number__c);

            // If we don't have a processor for the current PFS Number, set Match Not Found information.
            // Then move to the next SpringCM_Document__c.
            if (docMatcher == null) {
                scd.Match_Not_Found__c = true;
                scd.Match_Not_Found_Reason__c = SpringCmDocuments.PFS_NOT_FOUND;
                continue;
            }

            // Use the document matcher to determine who the doc is for based on the name information on the SpringCM_Document__c.
            // The doc matcher will try to find a match with the PFS__c, Applicant__c, and Dependent__c records.
            // They are processed in that order and the first match is returned
            docMatcher.setDocumentPertainsToInfo(scd);
        }
    }

    /**
    * @description Query the pfs record related to the given "pfsNumbers", and maps the results by "pfsNumbers".
    * @param pfsNumbers The list for pfsNumbers to be used in the query to PFS records.
    * @return A map of DocumentMatchProcessors with the PFS_Number as key.
    */
    private static Map<String, DocumentMatchProcessor> getDocumentMatchersByPfsNumber(Set<String> pfsNumbers) {
        Map<String, DocumentMatchProcessor> docMatchersByPfsNumber = new Map<String, DocumentMatchProcessor>();

        // TODO SFP-1506 Should I query for middle names where available as well? I could do a better job of identifying specific parts of the name and storing first, middle, last initials
        if (!pfsNumbers.isEmpty()) {
            List<PFS__c> pfsRecords = [SELECT Id, Filing_Status__c, Parent_B_Filing_Status__c, PFS_Number__c, Parent_A_First_Name__c, Parent_A_Last_Name__c, Parent_B_First_Name__c, Parent_B_Last_Name__c,
            (SELECT Id, First_Name__c, Last_Name__c FROM Applicants__r),
            (SELECT Id, Dependent_1_Full_Name__c, Dependent_2_Full_Name__c, Dependent_3_Full_Name__c, Dependent_4_Full_Name__c, Dependent_5_Full_Name__c, Dependent_6_Full_Name__c FROM Dependents__r)
            FROM PFS__c WHERE PFS_Number_Searchable__c IN :pfsNumbers];

            for (PFS__c pfs : pfsRecords) {
                docMatchersByPfsNumber.put(pfs.PFS_Number__c, new DocumentMatchProcessor(pfs));
            }
        }

        return docMatchersByPfsNumber;
    }

    private static Map<String, Databank_Doc_Type_Mapping__c> docTypeMappingByExternalDocType;
    private static Map<String, Databank_Doc_Type_Mapping__c> getDocTypeMappingsByExternalDocType() {
        if (docTypeMappingByExternalDocType != null) {
            return docTypeMappingByExternalDocType;
        }

        docTypeMappingByExternalDocType = new Map<String, Databank_Doc_Type_Mapping__c>();

        for (Databank_Doc_Type_Mapping__c mapping : Databank_Doc_Type_Mapping__c.getAll().values()){
            // only put in a mapping if the mapping is active and this is NOT exclusively for SF Doc Type to DB Doc Type
            if (mapping.Active__c && !mapping.For_Salesforce_to_Databank_Mapping_ONLY__c){
                docTypeMappingByExternalDocType.put(mapping.Databank_Name__c.toLowerCase(), mapping);
            }
        }

        return docTypeMappingByExternalDocType;
    }

    private static Databank_Doc_Type_Mapping__c getDocTypeMapping(String externalDocType) {
        if (String.isBlank(externalDocType)) {
            return null;
        }

        return getDocTypeMappingsByExternalDocType().get(externalDocType.toLowerCase());
    }

    /**
     * @description Used to contain the results of trying to determine who a document belongs to.
     */
    private class DocumentPertainsToResult {

        private String pertainsTo;

        private Boolean fullMatch;

        /**
         * @description Constructor for scenarios where we are unable to determine who the document is for.
         */
        public DocumentPertainsToResult() {}

        /**
         * @description Constructor for scenarios where we know who the document is for.
         */
        public DocumentPertainsToResult(String pertainsToValue) {
            pertainsTo = pertainsToValue;
        }

        public DocumentPertainsToResult(String pertainsToValue, Boolean isFullMatch) {
            pertainsTo = pertainsToValue;
            fullMatch = isFullMatch;
        }

        public String getPertainsTo() {
            return pertainsTo;
        }

        public Boolean isMatch() {
            return String.isNotBlank(pertainsTo);
        }

        public Boolean isFullMatch() {
            return isMatch() && (fullMatch == true);
        }

        public Boolean isPartialMatch() {
            return isMatch() && !isFullMatch();
        }
    }

    /**
     * @description Interface specifying the behavior for determining who a document is for. The matching behavior is
     *              different depending on what SObject we are matching a SpringCM_Document__c against. The different
     *              objects we use are PFS__c, Applicant__c, and Dependents__c.
     *
     *              A note about using names to match individuals to documents:
     *              Unlike Databank, Scalehub won't always be providing a separate first and last name for the documents they verify.
     *              Scalehub will only be providing a separate first and last name for documents that have separate first
     *              and last name fields in the form (e.g. 1040, 1040A, 1040EZ) For most other types of documents, we
     *              will receive the first and last name in one single field: SpringCM_Document__c.Name__c.
     */
    private interface IDocumentPertainsToMatcher {

        DocumentPertainsToResult matchDocumentByName(INameComparator nameToCompare);
    }

    /**
     * @description This wrapper class contains all the information necessary for matching an individual to a SpringCM Document.
     *              This wrapper will process the matching operation by using classes which implement the IDocumentPertainsToMatcher interface.
     *              The wrapper is responsible for processing the results and returning the first match that we find.
     *              The records used to do the match operation are PFS__c, Applicant__c, and Dependents__c.
     *              They are processed in that order.
     */
    private class DocumentMatchProcessor {

        private String parentAFilingStatus;
        private String parentBFilingStatus;

        private List<IDocumentPertainsToMatcher> documentMatchers;

        public DocumentMatchProcessor(PFS__c recordToWrap) {
            documentMatchers = new List<IDocumentPertainsToMatcher>();

            documentMatchers.addAll(getPfsDocumentMatchers(recordToWrap));

            documentMatchers.add(new ApplicantsDocumentMatcher(recordToWrap.Applicants__r));

            // Only add a dependents matcher if the PFS actually has dependents.
            if (recordToWrap.Dependents__r != null && !recordToWrap.Dependents__r.isEmpty()) {
                documentMatchers.add(new DependentsDocumentMatcher(recordToWrap.Dependents__r[0]));
            }

            // We need the filing status of both parents. For 1040 and sub-schedule doc types, we won't attempt to match if the parents file jointly.
            parentAFilingStatus = recordToWrap.Filing_Status__c;
            parentBFilingStatus = recordToWrap.Parent_B_Filing_Status__c;
        }

        private String matchDocument(SpringCM_Document__c springCMDocument) {
            INameComparator documentNameToCompare = createNameComparator(springCMDocument);

            // If null, return early. This could happen if the SpringCM Document doesn't have any of the name fields populate: Name__c, First_Name__c, Last_Name__c.
            if (documentNameToCompare == null) {
                return null;
            }

            // SFP-1751: We will store the first partial match we receive in case we aren't able to find a full match.
            DocumentPertainsToResult partialMatchResult;

            // This block is for when we just returned after the first 'match'.
            for (IDocumentPertainsToMatcher matcher : documentMatchers) {
                DocumentPertainsToResult result = matcher.matchDocumentByName(documentNameToCompare);

                // Return the first full match result we get. Otherwise we will keep going to see if we can at least find a partial result.
                if (result.isFullMatch()) {
                    return result.getPertainsTo();
                }

                // We only store the first partial match result. We will return this value if we are unable to find a complete match.
                // Perhaps we should store all partial matches then find the one with the smallest levensthein distance or something.
                if (result.isPartialMatch() && partialMatchResult == null) {
                    partialMatchResult = result;
                }
            }

            return partialMatchResult == null ? null : partialMatchResult.getPertainsTo();
        }

        private Boolean isDoc1040OrSubschedule(Databank_Doc_Type_Mapping__c docTypeMapping) {
            return docTypeMapping.Salesforce_Name__c.containsIgnoreCase('1040') || docTypeMapping.X1040_Related_Doc__c;
        }

        private Boolean isFilingJointly() {
            return EfcPicklistValues.FILING_STATUS_MARRIED_JOINT.equalsIgnoreCase(parentAFilingStatus);
        }

        /**
         * @description Attempts to set the Document Pertains To field on the specified SpringCM Document. This is done
         *              by trying to match the name data on the SpringCM_Document__c record to the name data on the related
         *              PFS, Applicants, or Dependents.
         *
         *              There are several scenarios where we don't attempt to find a match. In these cases we set
         *              Match_Not_Found__c to true and populate Match_Not_Found_Reason__c to provide some context.
         *              These scenarios include:
         *                  - We don't have a Databank_Doc_Type_Mapping__c record for the doc type on the SpringCM record.
         *                  - The doc type is not one that gets verified.
         *                  - The doc type is a 1040 or sub-schedule (e.g. Schedule A, Schedule C) and parent A filing status is Married Filing Jointly.
         *
         *              If we actually attempt to find a match and are unable to, Match_Not_Found__c is set to true and
         *              Match_Not_Found_Reason__c indicates that we tried and failed to find a match.
         *
         *              If we find a match, Document_Pertains_To__c is set to a letter that corresponds to the full pertains
         *              to value in our system. For example, 'A' corresponds to 'Parent A'. We chose to use the letters
         *              even though the actual picklist values use the full values to minimize changes.
         * @param springCMDocument The record that will be updated based on the results of our matching operation.
         */
        public void setDocumentPertainsToInfo(SpringCM_Document__c springCMDocument) {
            Databank_Doc_Type_Mapping__c docTypeMapping = getDocTypeMapping(springCMDocument.Document_Type__c);
            // If we don't have information for this particular doc type, do not set the pertains to value.
            // Without the doc type mapping, we are unable to determine if the doc type is one that gets verified.
            if (docTypeMapping == null) {
                springCMDocument.Match_Not_Found__c = true;
                springCMDocument.Match_Not_Found_Reason__c = SpringCmDocuments.NO_DOC_TYPE_MAPPING;
                return;
            }

            // If the doc type is not one that we verify, do not set the pertains to value.
            // We won't have name information for documents that aren't verified so we couldn't find a match anyway.
            if (!VerificationAction.isDocTypeVerifiable(docTypeMapping.Salesforce_Name__c)) {
                springCMDocument.Match_Not_Found__c = true;
                springCMDocument.Match_Not_Found_Reason__c = SpringCmDocuments.DOC_TYPE_NOT_VERIFIED;
                return;
            }

            // If Doc Type is 1040, Schedule AND Married Filing Jointly, we will just default to the original pertains to value on the family document (return null).
            if (isDoc1040OrSubschedule(docTypeMapping) && isFilingJointly()) {
                springCMDocument.Match_Not_Found__c = true;
                springCMDocument.Match_Not_Found_Reason__c = SpringCmDocuments.JOINT_FILERS_1040_RELATED_DOC;
                return;
            }

            // We should only reach this point if we are going to try to match the name information and determine who the document pertains to.
            String docPertainsTo = matchDocument(springCMDocument);

            // If the calculated pertains to value is null/blank, we were unable to find a match.
            // In this case, set the Unable_To_Match_Name__c field to true so we can report on the number of times this happens.
            if (String.isNotBlank(docPertainsTo)) {
                springCMDocument.Document_Pertains_to__c = docPertainsTo;
                springCMDocument.Match_Not_Found__c = false;
                springCMDocument.Match_Not_Found_Reason__c = null;
            } else {
                // We were unable to find a match based on the name data provided.
                springCMDocument.Match_Not_Found__c = true;
                springCMDocument.Match_Not_Found_Reason__c = SpringCmDocuments.UNABLE_TO_FIND_MATCH;
            }
        }
    }

    private static List<IDocumentPertainsToMatcher> getPfsDocumentMatchers(PFS__c pfsRecord) {
        // Create document matchers for the PFS for parent A and parent B if necessary. We only create the matchers for the parents that have name data on the PFS.
        // For example, if there is no parent B, only a parent A document name matcher will be returned.
        List<IDocumentPertainsToMatcher> pfsMatchers = new List<IDocumentPertainsToMatcher>();
        if (String.isNotBlank(pfsRecord.Parent_A_First_Name__c) && String.isNotBlank(pfsRecord.Parent_A_Last_Name__c)) {
            pfsMatchers.add(new ParentADocumentMatcher(pfsRecord));
        }

        if (String.isNotBlank(pfsRecord.Parent_B_First_Name__c) && String.isNotBlank(pfsRecord.Parent_B_Last_Name__c)) {
            pfsMatchers.add(new ParentBDocumentMatcher(pfsRecord));
        }

        return pfsMatchers;
    }

    private class PfsDocumentMatcher implements IDocumentPertainsToMatcher {

        private PFS__c record;

        public PfsDocumentMatcher(PFS__c pfsRecord) {
            record = pfsRecord;
        }

        public DocumentPertainsToResult matchDocumentByName(INameComparator nameToCompare) {
            DocumentPertainsToResult parentAResult = nameToCompare.compareName(PERTAINS_TO_PARENT_A, record.Parent_A_First_Name__c, record.Parent_A_Last_Name__c);
            if (parentAResult.isMatch()) {
                return parentAResult;
            }

            // See if parent b is a match. If the parent b fields are null/empty, the result will not indicate a match.
            DocumentPertainsToResult parentBResult = nameToCompare.compareName(PERTAINS_TO_PARENT_B, record.Parent_B_First_Name__c, record.Parent_B_Last_Name__c);
            if (parentBResult.isMatch()) {
                return parentBResult;
            }

            // We were unable to find a match for either parent so return an empty result.
            return new DocumentPertainsToResult();
        }
    }

    // SFP-1751: We are splitting out the Parent A/B document matchers now so that a partial match for parent A doesn't prevent us from checking parent B for a possible full match.
    // This is necessary to handle parents with the same initials.
    private class ParentADocumentMatcher implements IDocumentPertainsToMatcher {

        private String firstName;
        private String lastName;

        public ParentADocumentMatcher(PFS__c pfsRecord) {
            firstName = pfsRecord.Parent_A_First_Name__c;
            lastName = pfsRecord.Parent_A_Last_Name__c;
        }

        public DocumentPertainsToResult matchDocumentByName(INameComparator nameToCompare) {
            DocumentPertainsToResult parentAResult = nameToCompare.compareName(PERTAINS_TO_PARENT_A, firstName, lastName);
            if (parentAResult.isMatch()) {
                return parentAResult;
            }

            // We were unable to find a match so return an empty result.
            return new DocumentPertainsToResult();
        }
    }

    // SFP-1751: We are splitting out the Parent A/B document matchers now so that a partial match for parent A doesn't prevent us from checking parent B for a possible full match.
    // This is necessary to handle parents with the same initials.
    private class ParentBDocumentMatcher implements IDocumentPertainsToMatcher {

        private String firstName;
        private String lastName;

        public ParentBDocumentMatcher(PFS__c pfsRecord) {
            firstName = pfsRecord.Parent_B_First_Name__c;
            lastName = pfsRecord.Parent_B_Last_Name__c;
        }

        public DocumentPertainsToResult matchDocumentByName(INameComparator nameToCompare) {
            DocumentPertainsToResult parentBResult = nameToCompare.compareName(PERTAINS_TO_PARENT_B, firstName, lastName);
            if (parentBResult.isMatch()) {
                return parentBResult;
            }

            // We were unable to find a match so return an empty result.
            return new DocumentPertainsToResult();
        }
    }

    private class ApplicantsDocumentMatcher implements IDocumentPertainsToMatcher {

        private List<Applicant__c> records;

        public ApplicantsDocumentMatcher(List<Applicant__c> applicantRecords) {
            // Make sure the records private variable isn't set to null. We will always have a list even if it is empty.
            records = applicantRecords == null ? new List<Applicant__c>() : applicantRecords;
        }

        public DocumentPertainsToResult matchDocumentByName(INameComparator nameToCompareTo) {
            for (Applicant__c record : records) {
                DocumentPertainsToResult result = nameToCompareTo.compareName(PERTAINS_TO_STUDENT, record.First_Name__c, record.Last_Name__c);

                if (result.isMatch()) {
                    return result;
                }
            }

            // We were unable to find a match for any of the applicants so return an empty result.
            return new DocumentPertainsToResult();
        }
    }

    private class DependentsDocumentMatcher implements IDocumentPertainsToMatcher {

        private Dependents__c record;

        public DependentsDocumentMatcher(Dependents__c dependentsRecord) {
            record = dependentsRecord;
        }

        public DocumentPertainsToResult matchDocumentByName(INameComparator nameToCompareTo) {
            for (Schema.SObjectField fieldToCheck : DEPENDENT_NAMES_TO_MATCH_DOCS_AGAINST) {
                String dependentName = (String)record.get(fieldToCheck);

                DocumentPertainsToResult result = nameToCompareTo.compareName(PERTAINS_TO_DEPENDENT, dependentName);

                if (result.isMatch()) {
                    return result;
                }
            }

            return new DocumentPertainsToResult();
        }
    }

    /**
     * @description Interface for comparing names and determining how close they are to matching
     */
    private interface INameComparator {

        DocumentPertainsToResult compareName(String pertainsTo, String firstName, String lastName);

        DocumentPertainsToResult compareName(String pertainsTo, String fullName);
    }

    private static INameComparator createNameComparator(SpringCM_Document__c springCmDocument) {
        if (String.isNotBlank(springCmDocument.First_Name__c) || String.isNotBlank(springCmDocument.Last_Name__c)) {
            return new SpringCMDocSplitNameData(springCmDocument);
        }

        if (String.isNotBlank(springCmDocument.Name__c)) {
            return new SpringCMDocFullNameData(springCmDocument);
        }

        // If the SpringCM_Document__c record does not have any name data then return the NoNameData class which will always return results that indicate no matches.
        return new SpringCMDocNoNameData();
    }

    /**
     * @description Implements name comparison behavior when the SpringCM Document contains the full name.
     */
    private class SpringCMDocFullNameData implements INameComparator {

        private SpringCM_Document__c record;
        private Name documentOwnerName;

        public SpringCMDocFullNameData(SpringCM_Document__c springCmDocument) {
            record = springCmDocument;
            documentOwnerName = new Name(springCmDocument.Name__c);
        }

        public DocumentPertainsToResult compareName(String pertainsTo, String firstName, String lastName) {
            // SFP-1751: We now check for exact matches and partial matches.
            NameCompareResult nameComparison = documentOwnerName.compareNames(firstName, lastName);
            if (nameComparison.IsExactMatch) {
                return new DocumentPertainsToResult(pertainsTo, true);
            }

            if (nameComparison.IsPartialMatch) {
                return new DocumentPertainsToResult(pertainsTo, false);
            }

            return new DocumentPertainsToResult();
        }

        public DocumentPertainsToResult compareName(String pertainsTo, String fullName) {
            // SFP-1751: We now check for exact matches and partial matches.
            NameCompareResult nameComparison = documentOwnerName.compareNames(fullName);
            if (nameComparison.IsExactMatch) {
                return new DocumentPertainsToResult(pertainsTo, true);
            }

            if (nameComparison.IsPartialMatch) {
                return new DocumentPertainsToResult(pertainsTo, false);
            }


            return new DocumentPertainsToResult();
        }
    }

    /**
     * @description Implements name comparison behavior when the SpringCM Document contains separate first and last names.
     */
    private class SpringCMDocSplitNameData implements INameComparator {

        private SpringCM_Document__c record;
        private Name documentOwnerName;

        public SpringCMDocSplitNameData(SpringCM_Document__c springCmDocument) {
            record = springCmDocument;
            documentOwnerName = new Name(springCmDocument.First_Name__c, springCmDocument.Last_Name__c);
        }

        public DocumentPertainsToResult compareName(String pertainsTo, String firstName, String lastName) {
            // SFP-1751: We now check for exact matches and partial matches.
            NameCompareResult nameComparison = documentOwnerName.compareNames(firstName, lastName);
            if (nameComparison.IsExactMatch) {
                return new DocumentPertainsToResult(pertainsTo, true);
            }

            if (nameComparison.IsPartialMatch) {
                return new DocumentPertainsToResult(pertainsTo, false);
            }

            return new DocumentPertainsToResult();
        }

        public DocumentPertainsToResult compareName(String pertainsTo, String fullName) {
            // SFP-1751: We now check for exact matches and partial matches.
            NameCompareResult nameComparison = documentOwnerName.compareNames(fullName);
            if (nameComparison.IsExactMatch) {
                return new DocumentPertainsToResult(pertainsTo, true);
            }

            if (nameComparison.IsPartialMatch) {
                return new DocumentPertainsToResult(pertainsTo, false);
            }


            return new DocumentPertainsToResult();
        }
    }

    /**
     * @description Handles name comparisons when a SpringCM_Document__c does not have any name data. That is, Name__c, First_Name__c, and Last_Name__c are all null/blank/empty.
     */
    private class SpringCMDocNoNameData implements INameComparator {

        public DocumentPertainsToResult compareName(String pertainsTo, String firstName, String lastName) {
            return new DocumentPertainsToResult();
        }

        public DocumentPertainsToResult compareName(String pertainsTo, String fullName) {
            return new DocumentPertainsToResult();
        }
    }

    /**
     * @description This class represents the result of a name comparison. It will tell us if the name was an exact match, contained all fragments, or matched by initials.
     */
    private class NameCompareResult {

        public NameCompareResult() {
            IsExactMatch = false;
            IsPartialMatch = false;
        }

        public NameCompareResult(Boolean exactMatch, Boolean partialMatch) {
            IsExactMatch = exactMatch;
            IsPartialMatch = partialMatch;
        }

        public Boolean IsExactMatch { get; private set; }

        public Boolean IsPartialMatch { get; private set; }
    }

    private class Name {

        // The individual parts of the name after it is split on spaces and hyphens.
        private List<String> parts;

        public Name(String fullName) {
            parts = new List<String>();
            parts.addAll(splitName(fullName));
            //System.debug('&&& fullName parts: ' + parts);
        }

        public Name(String firstName, String lastName) {
            parts = new List<String>();
            parts.addAll(splitName(firstName));
            parts.addAll(splitName(lastName));
            //System.debug('&&& split name parts: ' + parts);
        }

        public Boolean isMatch(String fullName) {
            if (String.isBlank(fullName) || parts.isEmpty()) {
                return false;
            }

            return isMatch(new Name(fullName));
        }

        public Boolean isMatch(String firstName, String lastName) {
            if (String.isBlank(firstName) && String.isBlank(lastName)) {
                return false;
            }

            return isMatch(new Name(firstName, lastName));
        }

        public NameCompareResult compareNames(String firstName, String lastName) {
            if (String.isBlank(firstName) && String.isBlank(lastName)) {
                return new NameCompareResult();
            }

            Name nameToCompare = new Name(firstName, lastName);

            return doNameComparison(nameToCompare);
        }

        public NameCompareResult compareNames(String fullName) {
            if (String.isBlank(fullName) || parts.isEmpty()) {
                return new NameCompareResult();
            }

            Name nameToCompare = new Name(fullName);

            return doNameComparison(nameToCompare);
        }

        private NameCompareResult doNameComparison(Name nameToCompare) {
            // If the names contain all the same fragments, return an exact match result.
            if (namesContainAllFragments(nameToCompare)) {
                return new NameCompareResult(true, true);
            }

            // Next, check that at least some of the fragments are common between the two names and see if one of the names has all the initials as the other.
            // If so, return a partial match result.
            if (namesContainSomeFragments(nameToCompare) && namesContainInitials(nameToCompare)) {
                return new NameCompareResult(false, true);
            }

            return new NameCompareResult();
        }

        private Boolean isMatch(Name nameToCompare) {
            // First check to see if either name contains all fragments of the other name.
            if (namesContainAllFragments(nameToCompare)) {
                return true;
            }

            // Next, check that at least some of the fragments are common between the two names and see if one of the names has all the initials as the other.
            if (namesContainSomeFragments(nameToCompare) && namesContainInitials(nameToCompare)) {
                return true;
            }

            return false;
        }

        private Boolean namesContainAllFragments(Name nameToCompare) {
            Set<String> uniqueFragmentsToCheck = new Set<String>(nameToCompare.getFragments());
            Set<String> uniqueFragments = new Set<String>(getFragments());

            return uniqueFragmentsToCheck.containsAll(uniqueFragments) || uniqueFragments.containsAll(uniqueFragmentsToCheck);
        }

        private Boolean namesContainSomeFragments(Name nameToCompare) {
            Set<String> fragmentsToCheck = new Set<String>(nameToCompare.getFragments());
            fragmentsToCheck.retainAll(new Set<String>(getFragments()));

            return fragmentsToCheck.size() > 0;
        }

        private Boolean levenshteinUnderThreshold(Name nameToCompare) {
            // First we will join the name fragments together for both names with a single space.

            //System.debug('&&& name: ' + toString());
            //System.debug('&&& compare to: ' + nameToCompare.toString());
            // Then we will calculate the levenshtein distance.
            Integer levenshteinDistance = toString().getLevenshteinDistance(nameToCompare.toString());
            //System.debug('&&& levenshtein: ' + levenshteinDistance);

            // After that, we will check to see if it is under our acceptable threshold.

            return levenshteinDistance <= 10;
        }

        private Boolean namesContainInitials(Name nameToCompare) {
            Set<String> initialsToCompare = new Set<String>(nameToCompare.getInitials());
            Set<String> initials = new Set<String>(getInitials());

            return initials.containsAll(initialsToCompare) || initialsToCompare.containsAll(initials);
        }

        private List<String> getInitialsFromNameFragments(List<String> nameFragments) {
            List<String> initials = new List<String>();

            for (String fragment : nameFragments) {
                initials.add(fragment.left(1));
            }

            return initials;
        }

        public List<String> getFragments() {
            // Return a clone of the set so we can use retain/remove all without losing part of the original name.
            return parts;
        }

        public List<String> getInitials() {
            return getInitialsFromNameFragments(getFragments());
        }

        public override String toString() {
            return String.join(getFragments(), ' ');
        }
    }

    private static final String HYPHEN = '-';
    private static final String SPACE = ' ';
    private static List<String> splitName(String nameToSplit) {
        List<String> nameFragments = new List<String>();

        if (String.isBlank(nameToSplit)) {
            return nameFragments;
        }

        // First split the name on spaces.
        List<String> spaceFragments = nameToSplit.split(SPACE);

        // Then split each of those fragments on hyphens and add all the individual pieces to the set.
        for (String fragment : spaceFragments) {
            List<String> hyphenFragments = fragment.toLowerCase().split(HYPHEN);
            nameFragments.addAll(hyphenFragments);
        }

        return nameFragments;
    }
}