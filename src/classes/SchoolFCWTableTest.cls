@isTest
private class SchoolFCWTableTest {

    @isTest
    private static void testTellMeMoreDocId() {
        SchoolFCWTable controller = new SchoolFCWTable();
        Id tellMeMoreDocId = controller.tellMeMoreDocId;

        System.assert(String.isBlank(tellMeMoreDocId));

        Test.startTest();
            Document doc = DocumentTestData.Instance
                .forDocumentDeveloperName(DocumentLinkMappingService.documentInfo.Document_Developer_Name__c).insertDocument();

            controller = new SchoolFCWTable();
            tellMeMoreDocId = controller.tellMeMoreDocId;

            System.assertEquals(DocumentLinkMappingService.documentId, tellMeMoreDocId);
        Test.stopTest();
    }
}