public interface SchoolAcademicYearSelectorInterface {

    String getAcademicYearId();
    
    void setAcademicYearId(String academicYearId);
    
    PageReference SchoolAcademicYearSelector_OnChange(Boolean saveRecord);
    
    //void saveSettings();
}