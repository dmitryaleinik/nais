/*
 * NAIS-967 Roll Up Subscription Status and Type to Account
 *
 *    Scheduled batch Apex to rollup status updates when subscription end date expires without insert/update/delete of subscriptions
 *
 * WH, Exponent Partners, 2013
 */
global class AccountActionScheduler implements Schedulable {
    
    global void execute(SchedulableContext sc) {
        // batch process Accounts to rollup subscription status and type from Subscriptions to Accounts
        Database.executeBatch(new AccountActionBatch());
    }
    
}