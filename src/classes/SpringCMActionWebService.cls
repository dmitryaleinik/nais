Global without sharing class SpringCMActionWebService {
    webservice static void manuallySendFamDocUpdateToSpringCM(Id famDocId) { 
        SpringCMAction.sendInfoToSpringCMSynchronous(new Set<Id>{famDocId}, UserInfo.getSessionId());

        try {
            Family_Document__c fd = [Select Id, System_Processing_Error__c from Family_Document__c where Id = :famDocId];
            if (fd.System_Processing_Error__c != null && fd.System_Processing_Error__c != ''){
                fd.System_Processing_Error__c = null;
                update fd;
            }
        } catch (Exception e){
            // do nothing
        }
    }
    // [DP] NAIS-2019
    webservice static void manuallySendNewUserInfoToSpringCM(Id userId) { 
        List<User> userList = [Select Id, FirstName, LastName, Email, SpringCM_PortalUser__c, Username FROM User where Id = :userId];
        if (!userList.isEmpty()){
            String xmlString = SpringCMAction.writeNewUserXML(userList);
            String workflowName = 'SF_CreateUser';
            SpringCMAction.sparkSpringCMWorkflow(xmlString, workflowName, UserInfo.getSessionId());
        }
    }


    webservice static void manuallyCreateSDAsForReqDoc(Id reqDocId) { 
        List<Required_Document__c> reqDocs = [Select Id, Academic_Year__c, Academic_Year__r.Name, School__c, Document_Type__c, Document_Year__c, Label_for_School_Specific_Document__c, System_Future_Failure__c, System_Future_Method_Failed__c from Required_Document__c 
            WHERE Id = :reqDocId
            AND System_Future_Method_Failed__c = true 
            AND Deleted__c = false 
            order by CreatedDate asc limit 1];

        if (reqDocs.size() == 0){
            throw new webserviceException('The Required_Document__c must have System_Future_Method_Failed__c checked to create SDAs.');
            return;
        } else {

            reqDocs[0].System_Future_Failure__c = null;
            reqDocs[0].System_Future_Method_Failed__c = false;
            System.debug('THIS REQ DOC: ' + reqDocs[0].Id);
            update reqDocs[0];

            SchoolDocumentAssignmentAction.createSchoolDocAssignments(reqDocs);
        }
    }

    public class webserviceException extends Exception {}




    webservice static void manuallyAttachSDAsToReqDoc(Id reqDocId) { 
        Required_Document__c theReqDoc = [Select Id, Document_Year__c, Document_Type__c, School__c, Academic_Year__c, Academic_Year__r.Name from Required_Document__c where Id = :reqDocId limit 1];


        List<School_Document_Assignment__c> sdasToUpdateTemp = new List<School_Document_Assignment__c>();
        List<School_Document_Assignment__c> sdasToUpdate = new List<School_Document_Assignment__c>();
        Set<Id> spaIdsHandled = new Set<Id>();
        Set<Id> spaIdsThatAlreadyHaveReqDoc = new Set<Id>();
        Set<String> docType1040s = new Set<String>();
        for (Databank_Doc_Type_Mapping__c dtm : Databank_Doc_Type_Mapping__c.getAll().values()){
            if (dtm.Salesforce_Name__c != null && dtm.Salesforce_Name__c.contains('1040')){
                docType1040s.add(dtm.Salesforce_Name__c);
            }
        }

        String queryString = 'Select Id, School_PFS_Assignment__c, Required_Document__c from School_Document_Assignment__c WHERE School_PFS_Assignment__r.School__c = \'' + theReqDoc.School__c + '\' AND School_PFS_Assignment__r.Academic_Year_Picklist__c = \'' + theReqDoc.Academic_Year__r.Name + '\' AND Document_Year__c = \'' + theReqDoc.Document_Year__c + '\'';
        if (theReqDoc.Document_Type__c.contains('1040')){
            queryString += ' AND (Document_Type__c = \'1040\' OR Document_Type__c = \'1040A\' OR Document_Type__c = \'1040EZ\' OR Document_Type__c = \'1040 with all filed schedules and attachments\' OR Document_Type__c in :docType1040s)';
        } else {
            queryString += ' And Document_Type__c = \'' + theReqDoc.Document_Type__c + '\'';
        }
        queryString += ' order by CreatedDate asc';

        for (School_Document_Assignment__c sda : Database.query(queryString)){
            // don't continue if this SPA already has an SDA with the Req Doc filled out
            if (!spaIdsThatAlreadyHaveReqDoc.contains(sda.School_PFS_Assignment__c)){
                // if the SDA has the req doc filled out, add the SPA id to the set
                if (sda.Required_Document__c != null){
                    spaIdsThatAlreadyHaveReqDoc.add(sda.School_PFS_Assignment__c);
                // otherwise, only continue if we haven't already handled this SPA
                } else if (!spaIdsHandled.contains(sda.School_PFS_Assignment__c)){
                    sda.Required_Document__c = theReqDoc.Id;
                    spaIdsHandled.add(sda.School_PFS_Assignment__c);
                    sdasToUpdateTemp.add(sda);
                }
            }

        }

        for (School_Document_Assignment__c sda : sdasToUpdateTemp){
            if (!spaIdsThatAlreadyHaveReqDoc.contains(sda.School_PFS_Assignment__c)){
                sdasToUpdate.add(sda);
            }
        }

        if (sdasToUpdate.size() == 0){
            throw new webserviceException('No SDAs required an update were found.');
        } else {
            update sdasToUpdate;
        }
    }


    webservice static void manuallyWriteFamDocIdsToSDAs(Id pfsID) { 
        Map<Id, School_Document_Assignment__c> sdaMap = new Map<Id, School_Document_Assignment__c> (
            [Select Id, Required_Document__c, Document_Type__c, Document_Year__c, Import_Id__c, School_PFS_Assignment__c FROM School_Document_Assignment__c WHERE School_PFS_Assignment__r.Applicant__r.PFS__c = :pfsId AND Document__c = null]
        );

        Map<Id, School_PFS_Assignment__c> spaMap = new Map<Id, School_PFS_Assignment__c>(
            [Select Id, Academic_Year_Picklist__c, School__c, Applicant__c, School__r.SSS_School_Code__c, Parent_A_Email__c from School_PFS_Assignment__c where Applicant__r.PFS__c = :pfsID]
        );
    
        Set<Id> applicantIds = new Set<Id>();
        for (School_PFS_Assignment__c spa : spaMap.values()){
            applicantIds.add(spa.Applicant__c);
        }

        SchoolDocumentAssignmentAction.sDocAssignmentsToInsert = sdaMap.values();

        SchoolDocumentAssignmentAction.writeFamDocIdsToNewSchoolDocAssignments(applicantIds, spaMap, true);

        List<School_Document_Assignment__c> sdasToUpdate = SchoolDocumentAssignmentAction.sDocAssignmentsToInsert;
        upsert sdasToUpdate;
    }

}