@IsTest
private class TableAssetProgressivityTest
{

    @isTest
    private static void testTableAssetProgressivity()
    {
        // create the academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        // create the table
        TableTestData.populateAssetProgressivityData(academicYear.Id);

        // test net worth in table
        System.assertEquals(10, TableAssetProgressivity.calculateIncomeSupplement(academicYear.Id, 50000, 0.03));

        // test high net worth, not in table
        System.assertEquals(30, TableAssetProgressivity.calculateIncomeSupplement(academicYear.Id, 100000, 0.03));
    }
}