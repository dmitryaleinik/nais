/**
 * @description This class is used to provide the SchoolPFSAssignments.page with the School_PFS_Assignment__c records
 *              related to a PFS__c record. This controller is necessary because a PFS and an SPA are connected by
 *              Applicant__c records. Without a direct lookup, we are unable to display SPAs on the PFS page layout via
 *              related list.
 */
public with sharing class SchoolPfsAssignmentsController {

    private PFS__c record;

    /**
     * @description Constructor that leverages the PFS__c standard controller.
     * @param stdController A standard controller.
     */
    public SchoolPfsAssignmentsController(ApexPages.StandardController stdController) {
        Id recordId = Id.valueOf(stdController.getId());

        record = [SELECT Id, PFS_Number__c FROM PFS__c WHERE Id = :recordId];
    }

    /**
     * @description Gets the School_PFS_Assignment__c records using the PFS Number from the PFS__c record retrieved
     *              by the standard controller.
     * @returns A list of SPA records.
     */
    public List<School_PFS_Assignment__c> getSchoolPfsAssignments() {
        return [SELECT Id, Name, Applicant__c, Applicant_Name__c, School__c, School__r.Name, Student_Folder__c, Student_Folder__r.Name FROM School_PFS_Assignment__c WHERE PFS_Number__c = :record.PFS_Number__c ORDER BY Name];
    }
}