/**
 * @description Controller for uploading and deleting attachments.
 */
public with sharing class AttachmentsController {

    @testVisible private static final String ID_DELETE_PARAM = 'idToDelete';
    @testVisible private static final String ID_EDIT_PARAM = 'idToEdit';
    /** @description Id of the parent record from which attachments will be loaded. */
    public Id RecordId {get; set;}

    /**
     * @description Wrapper for a new attachment that will be uploaded.
     */
    public Attachment NewAttachment {
        get {
            if(NewAttachment == null) {
                NewAttachment = new Attachment();
            }
            return NewAttachment;
        }
        set;
    }

    public Attachment AttachmentForEdit { get; set; }

    /**
     * @description List of attachments to display to the user.
     */
    public List<Attachment> Attachments {
        get {
            if (Attachments == null) {
                Attachments = [SELECT Id, Name, Description, CreatedDate FROM Attachment WHERE ParentId = :RecordId];
            }
            return Attachments;
        }
        set;
    }

    /**
     * @description Whether or not there are existing attachments to display.
     * @return True if there are attachments, otherwise false.
     */
    public Boolean getHasAttachments() {
        return !Attachments.isEmpty();
    }

    /**
     * @description Deletes an attachment passed in from the idToDelete url parameter. A user can delete attachments
     *              that they can view but do not own so the url parameter is validated.
     * @return A null page reference to reload the page.
     */
    public PageReference deleteAttachment() {
        Id idToDelete = Id.valueOf(System.currentPageReference().getParameters().get(ID_DELETE_PARAM));
        if (!validateAccessToAttachment(idToDelete)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Error deleting internal document.'));
            return null;
        }
        try {
            Attachment attachmentToDelete = new Attachment(Id = idToDelete);
            new UnitOfWork().deleteRecord(attachmentToDelete);
        }catch (Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Error deleting internal document.'));
        }
        Attachments = null;
        return null;
    }

    /**
     * @description Uploads a new attachment to the parent record specified in the RecordId property.
     *              Attachment is uploaded with IsPrivate = false so other school users may view it.
     * @return Null page reference to reload the page.
     */
    public PageReference upload() {
        NewAttachment.ParentId = RecordId; // the record the file is attached to
        NewAttachment.IsPrivate = false;

        try {
            insert NewAttachment;
        } catch (DMLException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Error uploading internal document.'));
            return null;
        } finally {
            NewAttachment = new Attachment();
            Attachments = null;
        }

        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO,'Internal document uploaded successfully.'));
        return null;
    }

    /**
     * @description Uses the idToEdit page parameter to set the AttachmentForEdit property.
     */
    public void renderEditAttachment() {
        Id idToEditTemp = Id.valueOf(System.currentPageReference().getParameters().get(ID_EDIT_PARAM));
        AttachmentForEdit = new Map<Id, Attachment>(Attachments).get(idToEditTemp);
    }

    /**
     * @description Updates the AttachmentForEdit.
     * @return Null page reference to reload the page.
     */
    public PageReference updateAttachment() {
        try {
            update AttachmentForEdit;
        } catch (DmlException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Error updating name of internal document. ' + e.getMessage()));
            return null;
        } finally {
            Attachments = null;
        }

        return null;
    }

    private Boolean validateAccessToAttachment(Id attachmentId) {
        return new Map<Id, Attachment>(Attachments).containsKey(attachmentId);
    }

    /**
     * @description Used for DML to delete attachments which were uploaded by a different school user.
     */
    private without sharing class UnitOfWork {
        /**
         * @description Used for DML to delete attachments which were uploaded by a different school user.
         * @param objToDelete The sObject that will be deleted.
        */
        public void deleteRecord(Sobject objToDelete) {
            delete objToDelete;
        }
    }

}