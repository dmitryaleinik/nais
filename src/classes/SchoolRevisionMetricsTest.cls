/*
* SPEC-108, NAIS-40
*
* Maintain a per school count of revised fields for audit reporting requirement
*
* Nathan, Exponent Partners, 2013
*/
@isTest
private class SchoolRevisionMetricsTest
{

    private static User loginUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    
    private class TestDataLocal {
        Id academicYearId;
        Account school1;
        Contact staff1, parentA, parentB, student1, student2;
        Applicant__c applicant1A;
        PFS__c pfs1;
        Student_Folder__c studentFolder1;
        School_PFS_Assignment__c spfsa1;
        
        private TestDataLocal() {
            // disable automatic EFC calculations
            EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
            EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
            
            // academic year
            TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
            academicYearId = GlobalVariables.getCurrentAcademicYear().Id;  

            // school
            school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, null, false); 
            insert school1;       

            // parents and students
            parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
            parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
            student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
            student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
            insert new List<Contact> {parentA, parentB, student1, student2};

            // psf
            pfs1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
            pfs1.Payment_Status__c = 'Paid in Full';
            insert new List<PFS__c> {pfs1};

            // applicant
            applicant1A = TestUtils.createApplicant(student1.Id, pfs1.Id, false);
            insert new List<Applicant__c> {applicant1A};

            // student folder
            studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
            insert studentFolder1;

            // school pfs assignment
            spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder1.Id, false);
            spfsa1.PFS_Status__c = 'Application Submitted';
            insert spfsa1;
        }
    }

    @isTest
    private static void testMetricCountUnsubmitted() {
        TestDataLocal td = new TestDataLocal();
        td.spfsa1.PFS_Status__c = 'Unsubmitted';
        update td.spfsa1;
        
        System.assertNotEquals('Application Submitted', [Select Id, PFS_Status__c from School_PFS_Assignment__c where Id = :td.spfsa1.Id][0].PFS_Status__c);

        Test.startTest();
        
        // this may fail if Family_Contribution__c is not tracked
        // from this class I can not determine what is tracked. you need to replace it with a tracked field manually
        // fake new context
        schoolRevisionMetrics.isExecuting = false;
        td.spfsa1.Family_Contribution__c = 10;
        update td.spfsa1;
                
        // requery field count and verify that it is set to 1 for first change
        system.assertEquals(0, [select Family_Contribution__c from Field_Count__c
            where School__c = :td.school1.Id and Academic_Year__c = :td.academicYearId].size());

        Test.stopTest();    
    }   

    @isTest
    private static void testMetricCount() {
        User usr = getPortalUser();
        System.runAs(usr){//[G.S], SFP-8, count only if a portal user
            Test.startTest();
            TestDataLocal td = new TestDataLocal();
            
            School_PFS_Assignment__c updatedSPFSA = [Select Id, PFS_Status__c, Payment_Status__c from School_PFS_Assignment__c where Id = :td.spfsa1.Id limit 1];
            System.assertEquals('Application Submitted', updatedSPFSA.PFS_Status__c);
            System.assertEquals('Paid in Full', updatedSPFSA.Payment_Status__c);
        
            // this may fail if Family_Contribution__c is not tracked
            // from this class I can not determine what is tracked. you need to replace it with a tracked field manually
            // fake new context
            schoolRevisionMetrics.isExecuting = false;
            td.spfsa1.Family_Contribution__c = 10;
            update td.spfsa1;
                    
            // requery field count and verify that it is set to 1 for first change
            system.assertEquals(1, [select Family_Contribution__c from Field_Count__c
                where School__c = :td.school1.Id and Academic_Year__c = :td.academicYearId].Family_Contribution__c);
    
            // fake new context
            schoolRevisionMetrics.isExecuting = false;
            td.spfsa1.Family_Contribution__c = 20;
            update td.spfsa1;
                    
            // requery field count and verify that it is set to 2 for second change
            system.assertEquals(2, [select Family_Contribution__c from Field_Count__c
                where School__c = :td.school1.Id and Academic_Year__c = :td.academicYearId].Family_Contribution__c);
    
            // fake new context
            schoolRevisionMetrics.isExecuting = false;
            update td.spfsa1;
    
            // requery field count and verify that 2 is not changed. we are not updating any metric field 
            system.assertEquals(2, [select Family_Contribution__c from Field_Count__c
                where School__c = :td.school1.Id and Academic_Year__c = :td.academicYearId].Family_Contribution__c);
    
            // fake new context
            schoolRevisionMetrics.isExecuting = false;
            delete td.spfsa1;
    
            // requery field count and verify that 2 is not changed. we are not touching field count record for deletes 
            system.assertEquals(2, [select Family_Contribution__c from Field_Count__c
                where School__c = :td.school1.Id and Academic_Year__c = :td.academicYearId].Family_Contribution__c);
            
            Test.stopTest();  
        }  
    } 

    @isTest
    private static void testMetricCountWithOrig() {
        User usr = getPortalUser();
        System.runAs(usr){//[G.S], SFP-8, count only if a portal user
            TestDataLocal td = new TestDataLocal();
    
            td.spfsa1.Orig_Home_Market_Value__c = 10;
            // fake new context
            schoolRevisionMetrics.isExecuting = false;
            update td.spfsa1;
        
            Test.startTest();
            
            // this may fail if Home_Market_Value__c is not tracked
            // from this class I can not determine what is tracked. you need to replace it with a tracked field manually
            // fake new context
            schoolRevisionMetrics.isExecuting = false;
            td.spfsa1.Home_Market_Value__c = 11;
            update td.spfsa1;
                    
            // requery field count and verify that it is set to 1 for first change
            system.assertEquals(2, [select Home_Market_Value__c from Field_Count__c
                where School__c = :td.school1.Id and Academic_Year__c = :td.academicYearId].Home_Market_Value__c);
    
            td.spfsa1.Home_Market_Value__c = 9;
            // fake new context
            schoolRevisionMetrics.isExecuting = false;
            update td.spfsa1;
                    
            // requery field count and verify that it is set to 2 for second change
            system.assertEquals(3, [select Home_Market_Value__c from Field_Count__c
                where School__c = :td.school1.Id and Academic_Year__c = :td.academicYearId].Home_Market_Value__c);
    
            // fake new context
            schoolRevisionMetrics.isExecuting = false;
            update td.spfsa1;
    
            //SchoolRevisionMetrics.drewdebug = true;
            td.spfsa1.Home_Market_Value__c = 10;
            // fake new context
            schoolRevisionMetrics.isExecuting = false;
            update td.spfsa1;
    
            // requery field count and verify that 2 is not changed. we are setting it back to orig value 
            system.assertEquals(4, [select Home_Market_Value__c from Field_Count__c
                where School__c = :td.school1.Id and Academic_Year__c = :td.academicYearId].Home_Market_Value__c);
                        
            Test.stopTest();  
        }  
    }    

    @isTest
    private static void testTwoMetricCount() {
        User usr = getPortalUser();
        System.runAs(usr){    //[G.S], SFP-8, count only if a portal user
            TestDataLocal td = new TestDataLocal();
            Test.startTest();
            
            // this may fail if Family_Contribution__c and Apply_Minimum_Income__c not tracked
            // from this class I can not determine what is tracked. you need to replace it with a tracked field manually
            // fake new context
            schoolRevisionMetrics.isExecuting = false;
            td.spfsa1.Family_Contribution__c = 10;
            td.spfsa1.Apply_Minimum_Income__c = 'Yes';
            update td.spfsa1;
                    
            // requery field count and verify that only one record is created
            system.assertEquals(1, [select count() from Field_Count__c
                where School__c = :td.school1.Id and Academic_Year__c = :td.academicYearId]);
    
            // verify that each metric is set
            system.assertEquals(1, [select Family_Contribution__c from Field_Count__c
                where School__c = :td.school1.Id and Academic_Year__c = :td.academicYearId].Family_Contribution__c);
            system.assertEquals(1, [select Apply_Minimum_Income__c from Field_Count__c
                where School__c = :td.school1.Id and Academic_Year__c = :td.academicYearId].Apply_Minimum_Income__c);
    
            // update only one metric
            td.spfsa1.Family_Contribution__c = 20;
            // fake new context
            schoolRevisionMetrics.isExecuting = false;
            update td.spfsa1;
    
            // verify that only one metric is set
            system.assertEquals(2, [select Family_Contribution__c from Field_Count__c
                where School__c = :td.school1.Id and Academic_Year__c = :td.academicYearId].Family_Contribution__c);
            system.assertEquals(1, [select Apply_Minimum_Income__c from Field_Count__c
                where School__c = :td.school1.Id and Academic_Year__c = :td.academicYearId].Apply_Minimum_Income__c);
    
            // blank update
            // fake new context
            schoolRevisionMetrics.isExecuting = false;
            update td.spfsa1;
    
            // verify no metric is changed
            system.assertEquals(2, [select Family_Contribution__c from Field_Count__c
                where School__c = :td.school1.Id and Academic_Year__c = :td.academicYearId].Family_Contribution__c);
            system.assertEquals(1, [select Apply_Minimum_Income__c from Field_Count__c
                where School__c = :td.school1.Id and Academic_Year__c = :td.academicYearId].Apply_Minimum_Income__c);
        
            Test.stopTest();
        }
    }
    
    @isTest
    private static void testMetricCountBulk() {
        User usr = getPortalUser();
        System.runAs(usr){//[G.S], SFP-8, count only if a portal user
            TestDataLocal td = new TestDataLocal();
    
            Integer batchSize = 200;
            List <Account> schList = new List <Account>();
            List <School_PFS_Assignment__c> spfsaList = new List <School_PFS_Assignment__c>();
            
            // create schools
            for (Integer i=0; i<batchSize; i++) {
                schList.add(TestUtils.createAccount('school' + i, RecordTypes.schoolAccountTypeId, null, false));
            }
            insert schList;
           
            // create school pfs assignments
            for (Integer i=0; i<batchSize; i++) {
                School_PFS_Assignment__c spaTemp = (TestUtils.createSchoolPFSAssignment(td.academicYearId, td.applicant1A.Id, schList[i].Id, td.studentFolder1.Id, false));
                spaTemp.PFS_Status__c = 'Application Submitted';
                spfsaList.add(spaTemp);
            }
            // fake new context
            schoolRevisionMetrics.isExecuting = false;
            insert spfsaList;
        
            Test.startTest();
    
            // verify that no metric records created. we did not update any spfsa records
            system.assertEquals(0, [select Id from Field_Count__c
                where School__c in :schList and Academic_Year__c = :td.academicYearId].size());
    
            for (Integer i=0; i<batchSize; i++) {
                spfsaList[i].Family_Contribution__c = 10;
            }
            // fake new context
            schoolRevisionMetrics.isExecuting = false;
            update spfsaList;
    
            // verify that metric records created for each school. we updated spfsa metric field
            system.assertEquals(batchSize, [select Id from Field_Count__c
                where School__c in :schList and Academic_Year__c = :td.academicYearId].size());
    
            Test.stopTest();
        }  
    }
    
    private static User getPortalUser(){
        Account school = TestUtils.createAccount('Test School', RecordTypes.schoolAccountTypeId, 3, true);
        Contact staff = TestUtils.createContact('Staff 1', school.Id, RecordTypes.schoolStaffContactTypeId, true);
        Profile portalProfile = [select Id,name from Profile where Name = :ProfileSettings.SchoolAdminProfileName];
        Profile_Type_Grouping__c schoolAdminPTG = new Profile_Type_Grouping__c();
        SchoolAdminPTG.Name = 'PTG School Name';
        schoolAdminPTG.Profile_Name__c = ProfileSettings.SchoolAdminProfileName;
        schoolAdminPTG.Is_School_Admin_Profile__c = true;
        insert schoolAdminPTG;
        User usr;
        System.runAs(loginUser){
            usr = TestUtils.createPortalUser('SchoolPAdmin 1', 'sst1@test.com', 'ss1', staff.Id, portalProfile.Id, true, true);
        }
        return usr;
    }
}