/*
*   SPEC-086, R-147
*
*   The school version of the Family Report is called the RFC and it specifies the financial fields
*   as well as the School specific EFC calculations.
*   Ideally the RFC would be editable as well as printable.
*   This way they could tweak values in the financial fields and have the EFC recalculate
*   so they can see the results all on the same page. This estimate is just for the printable version
*
*   DP, Exponent Partners, 2013
*/


public with sharing class SchoolFamilyContribWrksheetPrintCont {
    /*Initialization*/

    public SchoolFamilyContribWrksheetPrintCont() {
        String schoolPFSAssignmentId = ApexPages.currentPage().getParameters().get('schoolpfsassignmentid');
        String folderId = ApexPages.currentPage().getParameters().get('folderid');
        Set<Id> tempFolderIds = new Set<Id>();
        // use school pfs assigment if it's provided
        if(schoolPFSAssignmentId != null) {
            this.SchoolPFSAssignmentId = schoolPFSAssignmentId;
            useSchoolPFSIdParam = true;
        // use folder Id if it's provided
        } else if (folderId != null){
            tempFolderIds.add(folderId);
            useSchoolPFSIdParam = false;
            folderIds = tempFolderIds;
        // throw exception if neither is provided
        } else {
            throw new ArgumentException('Missing schoolpfsassignmentid');
        }

        runQuery();
    }

    public SchoolFamilyContribWrksheetPrintCont(Set<Id> folderIdsParam) {

        useSchoolPFSIdParam = false;
        folderIds = folderIdsParam;
        runQuery();
    }

    /*End Initialization*/

    /*Properties*/

    public Boolean useSchoolPFSIdParam;
    public String SchoolPFSAssignmentId {get; set;}
    public Set<Id> folderIds {get; set;}
    public transient List<FCWPrintWrapper> fcwPrintWrappers {get; set;}
    public transient FCWPrintWrapper singleFCWPrintWrapper {get; set;}

    /*End Properties*/

    /*Action Methods*/

    /*End Action Methods*/

    /*Helper Methods*/

    public void runQuery(){
        String[] schoolPFSAssignmentFieldNames = ExpCore.GetAllFieldNames('School_PFS_Assignment__c');
        String[] PFSFieldNames = ExpCore.GetAllFieldNames('PFS__c');
        String[] VerificationFieldNames = ExpCore.GetAllFieldNames('Verification__c');

        for(Integer i = 0; i < PFSFieldNames.size(); i++) {
            PFSFieldNames[i] = 'Applicant__r.PFS__r.' + PFSFieldNames[i];
        }

        schoolPFSAssignmentFieldNames.add('Applicant__r.First_Name__c');
        schoolPFSAssignmentFieldNames.add('Applicant__r.Last_Name__c');
        schoolPFSAssignmentFieldNames.add('Applicant__r.Gender__c');
        schoolPFSAssignmentFieldNames.add('Applicant__r.Birthdate_new__c');
        schoolPFSAssignmentFieldNames.add('Applicant__r.Contact__c');
        schoolPFSAssignmentFieldNames.add('Student_Folder__r.Student_Tuition__c');
        schoolPFSAssignmentFieldNames.add('Student_Folder__r.Financial_Need__c');
        schoolPFSAssignmentFieldNames.add('Student_Folder__r.Total_Tuition_and_Expenses__c');
        schoolPFSAssignmentFieldNames.add('Student_Folder__r.Total_Contributions__c');
        schoolPFSAssignmentFieldNames.add('Student_Folder__r.Student__c');
        schoolPFSAssignmentFieldNames.add('Student_Folder__r.Total_Aid__c');
        schoolPFSAssignmentFieldNames.add('Student_Folder__r.Other_Aid_Notes__c');
        schoolPFSAssignmentFieldNames.add('Student_Folder__r.Tuition_and_Expense_Notes_Long__c');

        for(Integer i = 0; i < VerificationFieldNames.size(); i++) {
            VerificationFieldNames[i] = 'Applicant__r.PFS__r.Verification__r.' + VerificationFieldNames[i];
        }

        List<String> fieldNames = new List<String>();
        fieldNames.addAll(schoolPFSAssignmentFieldNames);
        fieldNames.addAll(PFSFieldNames);
        fieldNames.addAll(VerificationFieldNames);

        String sql;

        if (useSchoolPFSIdParam){
            // NAIS-2544 START
            sql = 'SELECT ' + String.Join(fieldNames, ',');
            sql += ', (SELECT Academic_Year__c, Business_Entity_Type__c, Orig_Business_Entity_Type__c, Business_Farm__r.Name, Business_Farm__r.Business_or_Farm__c, Business_Farm__r.Academic_Year__c, Business_Farm__r.Business_Entity_Type__c, Net_Profit_Loss_Business_Farm__c, Business_Farm_Share__c, Orig_Net_Profit_Loss_Business_Farm__c, Orig_Business_Farm_Share__c FROM School_Biz_Farm_Assignments__r)';
            sql += ' FROM School_PFS_Assignment__c WHERE Id = :schoolPFSAssignmentId LIMIT 1';
            // NAIS-2544 END
        } else {
            sql = 'SELECT ' + String.Join(fieldNames, ',');
            sql += ', (SELECT Academic_Year__c, Business_Entity_Type__c, Orig_Business_Entity_Type__c, Business_Farm__r.Name, Business_Farm__r.Business_or_Farm__c, Business_Farm__r.Academic_Year__c, Business_Farm__r.Business_Entity_Type__c, Net_Profit_Loss_Business_Farm__c, Business_Farm_Share__c, Orig_Net_Profit_Loss_Business_Farm__c, Orig_Business_Farm_Share__c FROM School_Biz_Farm_Assignments__r)';
            sql += ' FROM School_PFS_Assignment__c WHERE Student_Folder__c in :folderIds AND Withdrawn__c != \'Yes\' order by Applicant__r.Last_Name__c, Applicant__r.First_Name__c';
        }
            
        String schoolContactPreviousAcademicYearKey;
        Set<Id> academicYearIds = new Set<Id>();
        Set<String> academicYearNames = new Set<String>();
        Set<Id> schoolIds = new Set<Id>();
        Set<String> schoolContactPreviousAcademicYearNameKeys = new Set<String>();

        List<School_PFS_Assignment__c> spfsList = Database.query(sql);

        for (School_PFS_Assignment__c spfs : spfsList){
            
            academicYearNames.add(spfs.Academic_Year_Picklist__c);
            
            schoolIds.add(spfs.school__c);
            
            //We will use this key to query previous SPAs, with this key we avoid to retrieve unnecessary records (E.g.: When the user selects multiple folders of different academic years).
            schoolContactPreviousAcademicYearKey = getSPAKey(spfs.school__c, spfs.Contact_ID__c, GlobalVariables.getPreviousAcademicYearStr(spfs.Academic_Year_Name__c));
            schoolContactPreviousAcademicYearNameKeys.add(schoolContactPreviousAcademicYearKey);
        }
        
        //Maps that will be used to query records in batch to avoid too many SOQL queries.
        Map<String, Annual_Setting__c> annualSettingBySchool = new Map<String, Annual_Setting__c>();//Query result of annual setting records for the schools of the selected SPAS
        Map<String, School_PFS_Assignment__c> spasBySchoolContactPreviousAcademicYearNameKey = new Map<String, School_PFS_Assignment__c>();//Query result of previous SPA records for the selected SPAS
        
        if (!useSchoolPFSIdParam){
        
            //Get the previous spa records for the selected studentFolders.
	        for (School_PFS_Assignment__c previousSpa : SchoolPfsAssignmentsSelector.newInstance()
	            .selectByStudentAndSchoolForAcademicYear(schoolContactPreviousAcademicYearNameKeys)) {
	            
	            spasBySchoolContactPreviousAcademicYearNameKey.put(previousSpa.School_Contact_Academic_Year_Name__c, previousSpa);
	        }
            
            //Get the AnnualSetting records for the selected studentFolders.School__c.
            for (Annual_Setting__c a : AnnualSettingsSelector.newInstance()
                .selectBySchoolAndAcademicYear(schoolIds, academicYearNames)) {
                
                annualSettingBySchool.put(a.school__c + '-' + a.Academic_Year_Name__c, a);
            }
            
        }

        // map of acad year Id to UC
        Map<String, Unusual_Conditions__c> acadYearNameToUConditions = new Map<String, Unusual_Conditions__c>();

        String ucQuery = UnusualConditionsWrapper.getUnusualConditionsQuery();
        ucQuery += ' where Academic_Year__r.Name  in :academicYearNames order by CreatedDate desc';

        for (Unusual_Conditions__c uc : Database.Query(ucQuery)){
            if (acadYearNameToUConditions.get(uc.Academic_Year__r.Name) == null){
                acadYearNameToUConditions.put(uc.Academic_Year__r.Name, uc);
            }
        }

        // added because we need this the SSS Constants record for the default COLA value [dp] 1.7.13
        // map of acad year Id to SSS Constants
        Map<String, SSS_Constants__c> acadYearNameToSSSConstants = new Map<String, SSS_Constants__c>();
        for (SSS_Constants__c sss : [SELECT Percentage_for_Imputing_Assets__c, Default_COLA_Value__c, Academic_Year__c, Academic_Year__r.Name FROM SSS_Constants__c where Academic_Year__r.Name  in :academicYearNames order by CreatedDate desc]){
            acadYearNameToSSSConstants.put(sss.Academic_Year__r.Name, sss);
        }

        // we only need one of these -- this map has the field names we want
        Map<String, UnusualConditionsWrapper> pfs_UC_map = UnusualConditionsWrapper.getNewPFSFieldNameToUCFieldNamesMap();

        fcwPrintWrappers = new List<FCWPrintWrapper>();
        FCWPrintWrapper tempWrapper;
        for (School_PFS_Assignment__c spfs : spfsList){
            
            schoolContactPreviousAcademicYearKey = getSPAKey(spfs.school__c, spfs.Contact_ID__c, GlobalVariables.getPreviousAcademicYearStr(spfs.Academic_Year_Name__c));
            
            School_PFS_Assignment__c previousSPA = spasBySchoolContactPreviousAcademicYearNameKey.get(schoolContactPreviousAcademicYearKey);
            System.debug('previousSPA ('+schoolContactPreviousAcademicYearKey+')>>>>>> '+previousSPA);
            Annual_Setting__c annualSetting = annualSettingBySchool.get(spfs.school__c + '-' + spfs.Academic_Year_Name__c);
            
            // if records exist, add a wrapper with the params
            if (spfs != null && spfs.Applicant__c != null && spfs.Applicant__r.PFS__r != null && acadYearNameToUConditions != null && acadYearNameToUConditions.get(spfs.Academic_Year_Picklist__c) != null){
                
                tempWrapper = new FCWPrintWrapper(spfs, spfs.Applicant__r.PFS__r, acadYearNameToUConditions.get(spfs.Academic_Year_Picklist__c), pfs_UC_map, acadYearNameToSSSConstants.get(spfs.Academic_Year_Picklist__c));
            // else, call the error constructor
            } else {
                
                tempWrapper = new FCWPrintWrapper(spfs.Id, spfs.Name);
            }
            
            //If it is buk process we set the values retrieved in batch to be sent to SchoolFCWPrint.component.
            //In this way we avoid queries in loops that will cause too many soql queries.
            if (!useSchoolPFSIdParam){
                
                //SchoolFCWPrint.component attribute: annualSettingBulk.
                if (annualSetting == null) {
                    tempWrapper.annualSetting = new Annual_Setting__c();
                } else {
                    tempWrapper.annualSetting = annualSetting;
                }
                
                //SchoolFCWPrint.component attribute: previousYearSpfsaBulk.
                if (previousSPA == null) {
                    tempWrapper.previousSPA = new School_PFS_Assignment__c();
                } else {
                    tempWrapper.previousSPA = previousSPA;
                }
            }
            fcwPrintWrappers.add(tempWrapper);
        }

        // set isFirst
        if (!fcwPrintWrappers.isEmpty()){
            singleFCWPrintWrapper = fcwPrintWrappers[0];
            fcwPrintWrappers[0].isFirst = true;
        }

        //START populating siblings

        // set of pfs ids in this run
        Set<Id> pfsIds = new Set<Id>();
        for (FCWPrintWrapper wrapper : fcwPrintWrappers){
            // only using the wrappers that are not missing records
            if (!wrapper.missingRecords){
                pfsIds.add(wrapper.PFS.Id);
            }
        }

        // map of PFS Id to list of all school pfs assignments in the pfs
        Map<Id, List<School_PFS_Assignment__c>> pfsIdToSiblingSPFSList = getMapOfSiblingsSpas(pfsIds);

        

        // loop through wrappers and populate sibling name collections
        for (FCWPrintWrapper wrapper : fcwPrintWrappers){
            
            // only using the wrappers that are not missing records
            if (!wrapper.missingRecords && pfsIdToSiblingSPFSList.get(wrapper.PFS.Id) != null) {
                
                for (School_PFS_Assignment__c spfs : pfsIdToSiblingSPFSList.get(wrapper.PFS.Id)) {
                    
                    // if this is a school pfs assignment for the same, local PFS but a different contact, add the s to the contact list
                    if (spfs.Student_Folder__r.Student__c != wrapper.SchoolPFSAssignment.Student_Folder__r.Student__c) {
                        
                        wrapper.addSiblingName(spfs.Applicant__r.First_Name__c + ' ' + spfs.Applicant__r.Last_Name__c);
                    }
                }
            }
        }
    }
    
    private String getSPAKey(Id schoolId, Id contactId, String academicYearName) {
        
        return Id.ValueOf(schoolId) + ':' + Id.ValueOf(contactId) + ':' + academicYearName;
    }
    
    /**
    * @description Fill a map with siblings for the given PFS ids, the map's key is the PFSId.
    * @param The ids of the pfs for which all spas will be retrieved.
    * @return A map with all applicants for the given pfs ids. 
    */
    private Map<Id, List<School_PFS_Assignment__c>> getMapOfSiblingsSpas(Set<Id> pfsIds) {
        
        Map<Id, List<School_PFS_Assignment__c>> result = new Map<Id, List<School_PFS_Assignment__c>>();
        
        List<School_PFS_Assignment__c> siblingSPFSList = SchoolPfsAssignmentsSelector.Instance.selectByPFSId(pfsIds);
            
        // populate map
        for(School_PFS_Assignment__c spa : siblingSPFSList){
            if(result.get(spa.Applicant__r.PFS__c) == null){
                result.put(spa.Applicant__r.PFS__c, new List<School_PFS_Assignment__c>());
            }

            result.get(spa.Applicant__r.PFS__c).add(spa);
        }
        
        return result;
    }
    
    /*End Helper Methods*/

    /*Internal Wrapper Class*/
    public class FCWPrintWrapper {

        public School_PFS_Assignment__c SchoolPFSAssignment {get; set;}
        public School_PFS_Assignment__c previousSPA {get; set;}
        public Annual_Setting__c annualSetting {get; set;}
        //public Verification__c Verification {get; set;}
        public PFS__c PFS {get; set;}
        public SSS_Constants__c SSSConstants {get; set;}
        public Boolean isFirst {get; set;}
        public Date todayDate {get; set;}
        public Unusual_Conditions__c uConditions {get; set;}
        public Decimal medicareAndSSCombinedOrig {get; set;}
        public Decimal medicareAndSSCombinedRevised {get; set;}

        public Boolean missingRecords {get; set;}
        public Id spfsId {get; set;}
        public String spfsName {get; set;}

        public Map<String, UnusualConditionsWrapper> pfs_UC_map {get; set;}

        public List<String> ucTitles {get; set;}

        // need a sibling list to pass to the component and a sibling set to avoid dupes
        public List<String> siblingNames {get; set;}
        public Set<String> siblingNameSet {get; set;}

        // constructor
        public FCWPrintWrapper(School_PFS_Assignment__c SchoolPFSAssignmentParam, PFS__c PFSParam, Unusual_Conditions__c uConditionsParam, Map<String, UnusualConditionsWrapper> pfs_UC_mapParam, SSS_Constants__c SSSConstantsParam){
            SchoolPFSAssignment = SchoolPFSAssignmentParam;
            //Verification = VerificationParam;
            PFS = PFSParam;
            SSSConstants = SSSConstantsParam;
            uConditions = uConditionsParam;
            pfs_UC_map = pfs_UC_mapParam;

            isFirst = false;
            todayDate = System.today();
            siblingNames = new List<String>();
            siblingNameSet = new Set<String>();
            ucTitles = new List<String>();

            if (pfs_UC_map != null && uConditions != null){
                populateUnusualConditionsDescs();
                setCombinedMedicareAndSSFields();
            }

            missingRecords = false;
        }

        // constructor for when records are missing
        public FCWPrintWrapper(Id spfsIdParam, String spfsNameParam){
            missingRecords = true;
            spfsId = spfsIdParam;
            spfsName = spfsNameParam;

        }

        // populate sibling names
        public void addSiblingName(String s){
            // only add it to the list if it's not already in the set
            if (siblingNameSet.add(s)){
                siblingNames.add(s);
            }
        }

        // get short titles from UC records
        public void populateUnusualConditionsDescs(){
            for (String pfsField : pfs_UC_map.keySet()) {
                if ((String)PFS.get(pfsField) == 'Yes') {
                    UnusualConditionsWrapper uc = pfs_UC_map.get(pfsField);
                    ucTitles.add(String.valueOf((uConditions.get(uc.titleField))));
                    //ucTitles.add(uc.Title);
                }
            }
        }

        /**
         * @description Sum the Medicare and Social Security fields for the
         *              RFC print and excel pages. This should use the revised
         *              values if any otherwise use the family original values.
         */
        public void setCombinedMedicareAndSSFields()
        {
            // Null guard the original values, and sum them
            Decimal originalMedicareAmount = (SchoolPFSAssignment.Orig_Medicare_Tax_Allowance_Calculated__c == null) ?
                    0 : SchoolPFSAssignment.Orig_Medicare_Tax_Allowance_Calculated__c;
            Decimal originalSocialSecurityAmount = (SchoolPFSAssignment.Orig_Social_Security_Tax_Allow_Calc__c == null) ?
                    0 : SchoolPFSAssignment.Orig_Social_Security_Tax_Allow_Calc__c;
            medicareAndSSCombinedOrig = originalMedicareAmount + originalSocialSecurityAmount;
            if (medicareAndSSCombinedOrig == 0) medicareAndSSCombinedOrig = null;

            // Determine if either of the values have been revised and if so substitute those for the revision.
            if (SchoolPFSAssignment.Medicare_Tax_Allowance_Calc_Revision__c != null ||
                SchoolPFSAssignment.Social_Security_Tax_Allowance_Calc_Rev__c != null)
            {
                medicareAndSSCombinedRevised = (SchoolPFSAssignment.Medicare_Tax_Allowance_Calc_Revision__c == null) ?
                    SchoolPFSAssignment.Medicare_Tax_Allowance_Calc__c : SchoolPFSAssignment.Medicare_Tax_Allowance_Calc_Revision__c;

                medicareAndSSCombinedRevised += (SchoolPFSAssignment.Social_Security_Tax_Allowance_Calc_Rev__c == null) ?
                    SchoolPFSAssignment.Social_Security_Tax_Allowance_Calc__c : SchoolPFSAssignment.Social_Security_Tax_Allowance_Calc_Rev__c;
            }
        }
    }
}