@isTest
private class FamilyDocumentsSelectorTest
{

    private static final String HOUSEHOLD_NAME = 'House Of Cards';
    private static final String FAMILY_DOC_NAME = 'Fam Doc';
    private static final String DEFAULT_ACADEMIC_YEAR_NAME = System.today().addYears(-1).year() + '-' + System.today().year();
    private static final String DOC_TYPE_W2 = 'W2';
    private static final String DOC_TYPE_1099 = '1099';

    private static List<Household__c> insertHouseholds(Integer numberToInsert)
    {
        List<Household__c> recordsToInsert = new List<Household__c>();

        for (Integer i = 0; i < numberToInsert; i++)
        {
            recordsToInsert.add(TestUtils.createHousehold(HOUSEHOLD_NAME + i, false));
        }

        insert recordsToInsert;

        return recordsToInsert;
    }

    private static List<Family_Document__c> insertFamilyDocuments(Integer numberToInsert)
    {
        return insertFamilyDocuments(numberToInsert, null);
    }

    private static List<Family_Document__c> insertFamilyDocuments(Integer numberToInsert, Id householdId)
    {
        List<Family_Document__c> recordsToInsert = createFamilyDocuments(numberToInsert, householdId);
        insert recordsToInsert;

        return recordsToInsert;
    }

    private static List<Family_Document__c> createFamilyDocuments(Integer numberToCreate)
    {
        return createFamilyDocuments(numberToCreate, null);
    }

    private static List<Family_Document__c> createFamilyDocuments(Integer numberToCreate, Id houseHoldId)
    {
        List<Family_Document__c> recordsToCreate = new List<Family_Document__c>();

        for (Integer i = 0; i < numberToCreate; i++)
        {
            Family_Document__c newDoc = FamilyDocumentTestData.Instance
                .forName(FAMILY_DOC_NAME + i)
                .forDocumentType(DOC_TYPE_W2)
                .forHousehold(houseHoldId)
                .forDocYear(DEFAULT_ACADEMIC_YEAR_NAME).create();

            recordsToCreate.add(newDoc);
        }

        return recordsToCreate;
    }

    @isTest
    private static void testSelectById_nullIdSet_expectArgumentNullException()
    {
        try {
            FamilyDocumentsSelector.newInstance().selectById(null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, FamilyDocumentsSelector.ID_SET_PARAM);
        }
    }

    @isTest
    private static void testSelectById_familyDocsExist_doNotSpecifyIds_expectNoRecordsQueried()
    {
        Integer numberOfRecords = 5;
        List<Family_Document__c> familyDocs = insertFamilyDocuments(numberOfRecords);

        Set<Id> recordIds = new Set<Id>();

        List<Family_Document__c> queriedRecords = FamilyDocumentsSelector.newInstance().selectById(recordIds);

        System.assertNotEquals(null, queriedRecords, 'Expected queried records to not be null.');
        System.assertEquals(0, queriedRecords.size(), 'Expected queried records to be empty.');
    }

    @isTest
    private static void testSelectById_familyDocsExist_expectRecordsQueried()
    {
        Integer numberOfRecords = 5;
        List<Family_Document__c> familyDocs = insertFamilyDocuments(numberOfRecords);

        Set<Id> recordIds = new Map<Id, Family_Document__c>(familyDocs).keySet();

        List<Family_Document__c> queriedRecords = FamilyDocumentsSelector.newInstance().selectById(recordIds);

        System.assertNotEquals(null, queriedRecords, 'Expected queried records to not be null.');
        System.assertEquals(numberOfRecords, queriedRecords.size(), 'Expected all of the inserted records to be queried.');
    }

    @isTest
    private static void testSelectW2sByHousehold_nullIdSet_expectArgumentNullException()
    {
        try {
            FamilyDocumentsSelector.newInstance().selectW2sByHousehold(null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, FamilyDocumentsSelector.HOUSE_HOLD_IDS_PARAM);
        }
    }

    @isTest
    private static void testSelectW2sByHousehold_w2sExistForHousehold_expectRecordsQueried()
    {
        Integer numberOfHouseholds = 1;
        List<Household__c> households = insertHouseholds(numberOfHouseholds);

        Integer numberOfDocsPerHousehold = 5;
        List<Family_Document__c> familyW2s = insertFamilyDocuments(numberOfDocsPerHousehold, households[0].Id);

        Set<String> householdIds = new Set<String> { (String)households[0].Id };

        List<Family_Document__c> queriedRecords = FamilyDocumentsSelector.newInstance().selectW2sByHousehold(householdIds);

        System.assertNotEquals(null, queriedRecords, 'Expected the queried family documents to not be null.');
        System.assertEquals(numberOfDocsPerHousehold, queriedRecords.size(),
                'Expected all of the inserted records to be queried.');
    }

    @isTest
    private static void testSelectW2sByHousehold_1099sExistForHousehold_expectNoRecordsQueried()
    {
        Integer numberOfHouseholds = 1;
        List<Household__c> households = insertHouseholds(numberOfHouseholds);

        Integer numberOfDocsPerHousehold = 5;
        List<Family_Document__c> family1099s = createFamilyDocuments(numberOfDocsPerHousehold, households[0].Id);
        for (Family_Document__c fd : family1099s)
        {
            fd.Document_Type__c = DOC_TYPE_1099;
        }
        insert family1099s;

        Set<String> householdIds = new Set<String> { (String)households[0].Id };

        List<Family_Document__c> queriedRecords = FamilyDocumentsSelector.newInstance().selectW2sByHousehold(householdIds);

        System.assertNotEquals(null, queriedRecords, 'Expected the queried family documents to not be null.');
        System.assertEquals(0, queriedRecords.size(), 'Expected none of the 1099s to be queried.');
    }

    @isTest
    private static void testSelectW2sByHousehold_householdDoesNotHaveDocs_expectNoRecordsQueried()
    {
        Integer numberOfHouseholds = 2;
        List<Household__c> households = insertHouseholds(numberOfHouseholds);

        Integer numberOfDocsPerHousehold = 5;
        // Insert W2s for the first household.
        List<Family_Document__c> familyW2s = insertFamilyDocuments(numberOfDocsPerHousehold, households[0].Id);

        // Query W2s for second household.
        Set<String> householdIds = new Set<String> { (String)households[1].Id };

        List<Family_Document__c> queriedRecords = FamilyDocumentsSelector.newInstance().selectW2sByHousehold(householdIds);

        System.assertNotEquals(null, queriedRecords, 'Expected the queried family documents to not be null.');
        System.assertEquals(0, queriedRecords.size(),
                'Expected no records to be queried for the household that does not have docs.');
    }

    @isTest
    private static void testSelectNonDeletedByIdWithSchoolDocumentAssignments_nullParam_expectArgumentNullException()
    {
        try {
            Test.startTest();
            FamilyDocumentsSelector.newInstance().selectNonDeletedByIdWithSchoolDocumentAssignments(null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, FamilyDocumentsSelector.ID_SET_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void testSelectNonDeletedByHouseholdIdWithSchoolDocumentAssignments_nullParam_expectArgumentNullException() 
    {
        try {
            Test.startTest();
            FamilyDocumentsSelector.newInstance().selectNonDeletedByHouseholdIdWithSchoolDocumentAssignments(null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, FamilyDocumentsSelector.ID_SET_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void testSelectNonDeletedByIdWithSchoolDocumentAssignments_oneNotdeleted_expectOneRecord()
    {
        Family_Document__c fd = FamilyDocumentTestData.Instance.insertFamilyDocument();

        // Just making sure this doesn't get returned with the other record
        FamilyDocumentTestData.Instance.asDeleted().insertFamilyDocument();

        Test.startTest();
        List<Family_Document__c> fds = FamilyDocumentsSelector.newInstance()
                .selectNonDeletedByIdWithSchoolDocumentAssignments(new Set<Id> { fd.Id });
        Test.stopTest();

        System.assertNotEquals(null, fds, 'Expected the returned list to not be null.');
        System.assertEquals(1, fds.size(), 'Expected the returned list to be empty.');
        System.assertEquals(fd.Id, fds[0].Id, 'Expected the given record to be returned.');
    }

    @isTest
    private static void testSelectNonDeletedByHouseholdIdWithSchoolDocumentAssignments_oneNotDeleted_expectOneRecord()
    {
        Family_Document__c fd = FamilyDocumentTestData.Instance.insertFamilyDocument();

        // Just making sure this doesn't get returned with the other record
        FamilyDocumentTestData.Instance.asDeleted().insertFamilyDocument();

        Test.startTest();
        List<Family_Document__c> fds = FamilyDocumentsSelector.newInstance()
                .selectNonDeletedByHouseholdIdWithSchoolDocumentAssignments(new Set<Id> { fd.Household__c });
        Test.stopTest();

        System.assertNotEquals(null, fds, 'Expected the returned list to not be null.');
        System.assertEquals(1, fds.size(), 'Expected the returned list to be empty.');
        System.assertEquals(fd.Id, fds[0].Id, 'Expected the given record to be returned.');
    }

    @isTest
    private static void testSelectByIdWithSchoolDocumentAssignmentsWithCustomFieldLists()
    {
        Integer numberOfRecords = 5;
        List<Family_Document__c> familyDocs = insertFamilyDocuments(numberOfRecords);

        Test.startTest();
            try {
                List<Family_Document__c> queriedRecords = FamilyDocumentsSelector.newInstance()
                    .selectByIdWithSchoolDocumentAssignmentsWithCustomFieldLists(
                        null, new List<String>{'Name'}, new List<String>{'Name'}, true);
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, FamilyDocumentsSelector.ID_SET_PARAM);
            }

            try {
                List<Family_Document__c> queriedRecords = FamilyDocumentsSelector.newInstance()
                    .selectByIdWithSchoolDocumentAssignmentsWithCustomFieldLists(
                        new Map<Id, Family_Document__c>(familyDocs).keySet(), null, new List<String>{'Name'}, true);
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, FamilyDocumentsSelector.FAMILY_DOCS_FIELDS_TO_SELECT_PARAM);
            }

            try {
                List<Family_Document__c> queriedRecords = FamilyDocumentsSelector.newInstance()
                    .selectByIdWithSchoolDocumentAssignmentsWithCustomFieldLists(
                        new Map<Id, Family_Document__c>(familyDocs).keySet(), new List<String>{'Name'}, null, true);
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, FamilyDocumentsSelector.SDA_FIELDS_TO_SELECT_PARAM);
            }
        
            List<Family_Document__c> queriedRecords = FamilyDocumentsSelector.newInstance()
                .selectByIdWithSchoolDocumentAssignmentsWithCustomFieldLists(
                    new Map<Id, Family_Document__c>(familyDocs).keySet(), new List<String>{'Name'}, new List<String>{'Name'}, true);
        Test.stopTest();
        
        System.assertNotEquals(null, queriedRecords, 'Expected the queried records to not be null.');
        System.assertEquals(familyDocs.size(), queriedRecords.size(), 'Expected all inserted records to be returned.');
    }

    @isTest
    private static void testSelectByHouseholdId_docType_docStatus_Exc()
    {
        Household__c household = HouseholdTestData.Instance.DefaultHousehold;
        Set<Id> nullSetId = null;

        Test.startTest();
            try {
                List<Family_Document__c> queriedRecords = FamilyDocumentsSelector.newInstance()
                    .selectByHouseholdId_docType_docStatus(nullSetId, new Set<String>{DOC_TYPE_W2}, VerificationAction.PROCESSED);
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, FamilyDocumentsSelector.HOUSE_HOLD_IDS_PARAM);
            }

            try {
                List<Family_Document__c> queriedRecords = FamilyDocumentsSelector.newInstance()
                    .selectByHouseholdId_docType_docStatus(
                        new Set<Id>{household.Id}, null, VerificationAction.PROCESSED);
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, FamilyDocumentsSelector.DOCUMENT_TYPES_PARAM);
            }

            try {
                List<Family_Document__c> queriedRecords = FamilyDocumentsSelector.newInstance()
                    .selectByHouseholdId_docType_docStatus(
                        new Set<Id>{household.Id}, new Set<String>{DOC_TYPE_W2}, null);
            } catch (Exception e) {
                TestHelpers.assertArgumentNullException(e, FamilyDocumentsSelector.DOCUMENT_STATUS_PARAM);
            }
        Test.stopTest();
    }

    @isTest
    private static void testSelectByHouseholdId_docType_docStatus_Suc()
    {
        Household__c household = HouseholdTestData.Instance.DefaultHousehold;
        Integer numberOfRecords = 7;
        List<Family_Document__c> familyDocs = createFamilyDocuments(numberOfRecords, household.Id);

        for (Family_Document__c fd : familyDocs)
        {
            fd.Deleted__c = false;
            fd.Duplicate__c = 'No';
            fd.Document_Status__c = VerificationAction.PROCESSED;
        }
        insert familyDocs;

        List<Family_Document__c> familyDocuments;
        
        Test.startTest();
            familyDocuments = FamilyDocumentsSelector.newInstance().selectByHouseholdId_docType_docStatus(
                new Set<Id>{household.Id}, new Set<String>{DOC_TYPE_W2}, VerificationAction.PROCESSED);
            System.assertEquals(7, familyDocuments.size());
            
            familyDocs[0].Document_Type__c = DOC_TYPE_1099;
            familyDocs[1].Deleted__c = true;
            familyDocs[2].Duplicate__c = 'Yes';
            familyDocs[3].Document_Status__c = VerificationAction.PENDING;
            familyDocs[4].Household__c = null;
            update familyDocs;

            familyDocuments = FamilyDocumentsSelector.newInstance().selectByHouseholdId_docType_docStatus(
                new Set<Id>{household.Id}, new Set<String>{DOC_TYPE_W2}, VerificationAction.PROCESSED);
            System.assertEquals(2, familyDocuments.size());
            System.assert(new Map<Id, Family_Document__c>(familyDocuments).keySet().contains(familyDocs[5].Id) 
                && new Map<Id, Family_Document__c>(familyDocuments).keySet().contains(familyDocs[6].Id));
        Test.stopTest();
    }
}