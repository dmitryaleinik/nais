/**
 *    NAIS-69: Document Preview
 *
 *    SL, Exponent Partners, 2013
 */
@isTest
private class PreviewPageControllerTest {

    @isTest
    private static void testPreviewPageController() {
        // create test family document record
        Family_Document__c familyDocument = new Family_Document__c();
        familyDocument.Import_ID__c = '12345';
        insert familyDocument;
        
        // test the controller
        Test.setCurrentPage(Page.DocumentPreviewPage);
        PreviewPageController controller = new PreviewPageController();
        System.assertEquals(false, controller.isValidPage);
        
        ApexPages.currentPage().getParameters().put('id', 'badid');
        controller = new PreviewPageController();
        System.assertEquals(false, controller.isValidPage);
        
        ApexPages.currentPage().getParameters().put('id', '12345');
        controller = new PreviewPageController();
        
        System.assertNotEquals(null, controller.theFamilyDocument);
        System.assertEquals(familyDocument.Id, controller.theFamilyDocument.Id);
        System.assertEquals('Test Image Data', controller.previewImage);
        System.assertEquals(1, controller.currentPage);
        
        controller.nextPage();
        System.assertEquals(2, controller.currentPage);
        
        controller.previousPage();
        System.assertEquals(1, controller.currentPage);
    }
}