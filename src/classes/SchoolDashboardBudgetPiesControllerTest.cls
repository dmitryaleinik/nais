@isTest 
private class SchoolDashboardBudgetPiesControllerTest {
    
    @isTest
    private static void testController() {
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.insertAcademicYear();
        Account school = AccountTestData.Instance.asSchool().DefaultAccount;
        Contact schoolStaff = ContactTestData.Instance
            .forAccount(school.Id)
            .asSchoolStaff().insertContact();
        
        User thisUser = UserSelector.newInstance().selectById(new Set<Id>{UserInfo.getUserId()})[0];
        User schoolPortalUser;
        System.runAs(thisUser) {
            schoolPortalUser = UserTestData.Instance
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }
        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);

        SchoolDashboardBudgetPiesController.BudgetPieWrapper result = null;
        
        System.runAs(schoolPortalUser){
            Id schoolId = GlobalVariables.getCurrentSchoolId();
            Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
            Test.startTest();           
                SchoolDashboardBudgetPiesController controller = new SchoolDashboardBudgetPiesController();
                controller.schoolId = schoolId;
                controller.academicYearId = currentAcademicYear.Id;
                System.assertEquals(true, controller.getChartNames().size()==10);
                result = SchoolDashboardBudgetPiesController.getSchoolBudgetPieData(
                    schoolId, currentAcademicYear.Id);
            Test.stopTest();
            
            System.assertEquals(true, result != null);
            System.assertEquals(2, result.result.size());
        }
    }
}