/**
 * @description Handle inline processing of Payments for all subsystems.
 **/
public without sharing class PaymentService {
    @testVisible private static final String REQUEST_PARAM = 'request';
    @testVisible private static final String OPPORTUNITY_RECORD_PARAM = 'opportunityRecord';
    @testVisible private static final String PFS_RECORD_PARAM = 'pfsRecord';
    @testVisible private static final String PAYMENT_DATA_PARAM = 'paymentData';
    @testVisible private static final String PAYMENT_INFORMATION_PARAM = 'PaymentInformation';
    @testVisible private static final String TRANSACTION_LINE_ITEM_PARAM = 'transactionLineItem';
    @testVisible private static final String OPPORTUNITY_AND_PFS_NULL =
            OPPORTUNITY_RECORD_PARAM + ' and ' + PFS_RECORD_PARAM;

    @testVisible private static final String UNRECOGNIZED_PAYMENT_ERROR = 'The payment type is not recognized.';
    @testVisible private static final String ERROR_CONTACT_NAIS =
            'There was an error with your payment. Please contact ' + Label.Company_Name_Full + ' before retrying.';
    @testVisible private static final String DEFAULT_ERROR_MESSAGE =
            '<b>TRANSACTION RESULT:</b> [{0}] {1}<br/><b>ADDITIONAL INFO:</b> {2}';

    /**
     * @description Process a credit card payment using the details provided in the PaymentData
     *              instance passed in with the request.
     * @param request The Payment Service request object that contains the payment information
     *        to process.
     * @returns A Payment Service response that contains the result of the credit card payment
     *          processing.
     * @throws An ArgumentNullException if the request parameter is null.
     */
    public Response processCreditCard(Request request) {
        ArgumentNullException.throwIfNull(request, REQUEST_PARAM);
        Response response = new Response();
        response.Opportunity = retrieveOrCreateOpportunity(request);

        try {
            PaymentRequest ccRequest = new PaymentRequest();
            ccRequest.PaymentInformation = request.PaymentInformation;
            ccRequest.OpportunityId = response.Opportunity.Id;

            response.Result = PaymentProcessor.process(ccRequest);
            response.ErrorOccurred = response.Result.ErrorOccurred;
            response.ErrorMessage = response.Result.ErrorMessage;
        } catch (Exception e) {
            CustomDebugService.Instance.logException(e);

            response.ErrorOccurred = true;
            response.ErrorMessage = ERROR_CONTACT_NAIS;
        }

        return response;
    }

    /**
     * @description Process an E Check given the provided request.
     * @param request The request containing the Payment Data and the Opportunity or
     *        PFS record.
     * @return A response object containing the outcome of the echeck processing,
     *         the transaction line item, and any errors if something went wrong.
     * @throws An ArgumentNullException if the request parameter is null.
     */
    public Response processECheck(Request request) {
        ArgumentNullException.throwIfNull(request, REQUEST_PARAM);
        Response response = new Response();
        response.Opportunity = retrieveOrCreateOpportunity(request);

        // At this time, existing logic does not have a try/catch around the eCheck payments.
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.PaymentInformation = request.PaymentInformation;
        paymentRequest.OpportunityId = response.Opportunity.Id;

        response.Result = PaymentProcessor.process(paymentRequest);
        response.ErrorOccurred = response.Result.ErrorOccurred;
        response.ErrorMessage = response.Result.ErrorMessage;

        return response;
    }

    /**
     * @description Process the payment provided using either an ECheck or Credit Card,
     *              based on the PaymentData.isCreditCardPaymentType and
     *              PaymentData.isECheckPaymentType booleans.
     * @param request The request object for the current payment.
     * @returns A payment service response with the results of the payment.
     * @throws An ArgumentNullException if the request parameter is null.
     */
    public Response processPayment(Request request) {
        ArgumentNullException.throwIfNull(request, REQUEST_PARAM);
        Response response;

        if (request.PaymentInformation instanceof CreditCardInformation) {
            response = processCreditCard(request);
        } else if (request.PaymentInformation instanceof ECheckInformation) {
            response = processECheck(request);
        } else {
            // Unhandled processing type
            throw new ArgumentException(UNRECOGNIZED_PAYMENT_ERROR);
        }

        return response;
    }

    /**
     * @description Process the setup for the transaction.
     * @param request The request to process containing the Payment or
     *        Refund information.
     * @return A PaymentService response with the outcome of the transaction.
     * @throws An ArgumentNullException if the request is null.
     */
    public Response processBefore(Request request) {
        ArgumentNullException.throwIfNull(request, REQUEST_PARAM);
        Response response = new Response();

        if ((request.Opportunity != null && request.Opportunity.Id != null) ||
            (request.PFS != null && request.PFS.Id != null)) {
            response.Opportunity = retrieveOrCreateOpportunity(request);

            // It's a payment process before request
            PaymentRequest paymentRequest = new PaymentRequest();
            paymentRequest.PaymentInformation = request.PaymentInformation;
            paymentRequest.PFS = request.Pfs;
            paymentRequest.OpportunityId = response.Opportunity.Id;

            response.Result = PaymentProcessor.processBefore(paymentRequest);
        } else if (request.TransactionLineItem != null && request.TransactionLineItem.Id != null) {
            // It's a refund process before request
            RefundRequest refundRequest = new RefundRequest(request.TransactionLineItem);

            response.Result = PaymentProcessor.processBefore(refundRequest);
        }

        response.ErrorOccurred = response.Result.ErrorOccurred;
        response.ErrorMessage = response.Result.ErrorMessage;

        return response;
    }

    /**
     * @description Process the refund given the provided request.
     * @param request The request object for the current Refund.
     * @returns A Payment Service response with the results of the refund.
     * @throws An ArgumentNullException if the request parameter is null.
     */
    public Response processRefund(Request request) {
        ArgumentNullException.throwIfNull(request, REQUEST_PARAM);
        Response response = new Response();
        response.Request = request;

        try {
            RefundRequest refundRequest = new RefundRequest(request.TransactionLineItem);
            response.Result = PaymentProcessor.process(refundRequest);
            response.ErrorOccurred = response.Result.ErrorOccurred;
            response.ErrorMessage = response.Result.ErrorMessage;
        } catch (Exception e) {
            CustomDebugService.Instance.logException(e);

            response.ErrorOccurred = true;
            response.ErrorMessage = ERROR_CONTACT_NAIS;
        }

        return response;
    }

    private Opportunity retrieveOrCreateOpportunity(Request request) {
        Opportunity opportunityRecord;

        // Create an opportunity if one doesn't already exist.
        opportunityRecord = request.Opportunity;
        if ((opportunityRecord == null || opportunityRecord.Id == null) &&
                request.Pfs != null && request.Pfs.Id != null) {

            // Query for one first
            List<Opportunity> opportunities = OpportunitySelector.Instance.selectByPfsId(new Set<Id> { request.Pfs.Id });
            if (opportunities.isEmpty()) {
                opportunityRecord = FinanceAction.createOppAndTransaction(new Set<Id>{
                        request.Pfs.Id
                }, false)[0];
            } else {
                opportunityRecord = opportunities[0];
            }
        }

        return opportunityRecord;
    }

    /**
     * @description The request object used when interacting with the PaymentService.
     **/
    public class Request {
        /**
         * @description The Opportunity associated with the payment being made.
         */
        public Opportunity Opportunity { get; private set; }

        /**
         * @description The PFS associated with the payment being made.
         */
        public PFS__c Pfs { get; private set; }

        /**
         * @description The Payment Information containing the user's billing
         *              information and payment relevant data.
         */
        public PaymentInformation PaymentInformation { get; private set; }

        /**
         * @description The Transaction Line Item to refund.
         */
        public Transaction_Line_Item__c TransactionLineItem { get; private set; }

        /**
         * @description Whether or not to send a receipt to the user.
         */
        public Boolean SendReceipt { get; set; }

        /**
         * @description Constructor used for requesting a payment to be processed
         *              by the PaymentService.
         * @param opportunityRecord The Opportuntiy to process a payment for.
         * @param pfsRecord The PFS record to process a payment for.
         * @param paymentData The data associated with the payment itself, such
         *        as the payment amount and billing address.
         */
        public Request(Opportunity opportunityRecord, PFS__c pfsRecord, FamilyPaymentData paymentData, Boolean sendReceipt) {
            this.Opportunity = opportunityRecord;
            this.Pfs = pfsRecord;
            this.PaymentInformation = this.getPaymentInformation(paymentData);
            this.SendReceipt = (sendReceipt == null) ? false : sendReceipt;

            validate();
        }

        /**
         * @description Constructor used for requesting a refund to be processed
         *              by the PaymentService.
         * @param transactionLineItem The Transaction Line Item you are processing
         *        a refund for.
         * @throws An ArgumentNullException if the param is null.
         */
        public Request(Transaction_Line_Item__c transactionLineItem) {
            ArgumentNullException.throwIfNull(transactionLineItem, TRANSACTION_LINE_ITEM_PARAM);
            this.TransactionLineItem = transactionLineItem;
        }

        private void validate() {
            if (this.Opportunity == null && this.Pfs == null) {
                throw new ArgumentNullException(String.format(ArgumentNullException.MESSAGE, new List<String> {
                        OPPORTUNITY_AND_PFS_NULL
                }));
            }

            ArgumentNullException.throwIfNull(this.PaymentInformation, PAYMENT_INFORMATION_PARAM);
        }

        /**
         * @description This converts a FamilyPaymentData object into a PaymentInformation
         *              object. Ideally this will no longer be needed once all the pages
         *              have been swapped to use PaymentInformation types.
         */
        private PaymentInformation getPaymentInformation(FamilyPaymentData paymentData) {
            if (paymentData == null) {
                return null;
            }

            PaymentInformation paymentInformation;

            if (paymentData.isCreditCardPaymentType) {
                CreditCardInformation ccInfo = new CreditCardInformation();

                ccInfo.CardType = paymentData.ccCardType;
                ccInfo.NameOnCard = paymentData.ccNameOnCard;
                ccInfo.CardNumber = paymentData.ccCardNumber;
                ccInfo.Cvv2 = paymentData.ccSecurityCode;
                ccInfo.ExpirationMM = paymentData.ccExpirationMM;
                ccInfo.ExpirationYY = paymentData.ccExpirationYY;

                paymentInformation = ccInfo;
            } else if (paymentData.isECheckPaymentType) {
                ECheckInformation ecInfo = new ECheckInformation();

                ecInfo.AccountType = paymentData.checkAccountType;
                ecInfo.AccountNumber = paymentData.checkAccountNumber;
                ecInfo.BankName = paymentData.checkBankName;
                ecInfo.CheckNumber = paymentData.checkCheckNumber;
                ecInfo.CheckType = paymentData.checkCheckType;
                ecInfo.NameOnCheck = paymentData.checkNameOnCheck;
                ecInfo.RoutingNumber = paymentData.checkRoutingNumber;

                paymentInformation = ecInfo;
            } else {
                return null;
            }

            paymentInformation.Tracker = paymentData.paymentTracker;
            paymentInformation.ItemName = paymentData.paymentDescription;
            paymentInformation.PaymentAmount = paymentData.paymentAmount;
            paymentInformation.BillingFirstName = paymentData.billingFirstName;
            paymentInformation.BillingLastName = paymentData.billingLastName;
            paymentInformation.BillingStreet1 = paymentData.billingStreet1;
            paymentInformation.BillingStreet2 = paymentData.billingStreet2;
            paymentInformation.BillingCity = paymentData.billingCity;
            paymentInformation.BillingState = paymentData.billingState;
            paymentInformation.BillingPostalCode = paymentData.billingPostalCode;
            paymentInformation.BillingCountryCode = paymentData.billingCountry;
            paymentInformation.BillingEmail = paymentData.billingEmail;

            return paymentInformation;
        }
    }

    /**
     * @description The response returned from any PaymentService interaction.
     */
    public class Response {
        /**
         * @description The original request.
         */
        public Request Request { get; private set; }

        /**
         * @description The generated Transaction Line Item Id from the processed payment.
         */
        public Id TransactionLineItemId {
            get {
                if (TransactionLineItemId == null &&
                    Result != null &&
                    Result.TransactionLineItemId != null) {

                    TransactionLineItemId = Result.TransactionLineItemId;
                }
                return TransactionLineItemId;
            }
            private set;
        }

        /**
         * @description The generated Opportunity if one is necessary
         */
        public Opportunity Opportunity { get; private set; }

        /**
         * @description The Payment Processor PaymentResult that is returned from processing a payment.
         */
        public PaymentResult Result { get; private set; }

        /**
         * @description Whether or not the payment has encountered an error.
         */
        public Boolean ErrorOccurred {
            get {
                if (ErrorOccurred == null) {
                    ErrorOccurred = false;
                }
                return ErrorOccurred;
            }
            private set;
        }

        /**
         * @description The message associated with the error provided during processing.
         */
        public String ErrorMessage { get; private set; }
    }

    /**
     * @description An instance property to allow singleton access
     *              to the PaymentService.
     */
    public static PaymentService Instance {
        get {
            if (Instance == null) {
                Instance = new PaymentService();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to enforce singleton access.
     */
    private PaymentService() { }
}