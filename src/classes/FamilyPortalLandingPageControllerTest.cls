@isTest
private class FamilyPortalLandingPageControllerTest
{
    @isTest private static void testFamilyPortalLandingPage()
    {
        Test.startTest();
        FamilyPortalLandingPageController controller = new FamilyPortalLandingPageController();
        System.assertEquals((Page.FamilyDashboard).getUrl(),(controller.goToDashboard()).getUrl());
        Test.stopTest();
    }

    @isTest private static void getMessage_asFamilyUser_expectCorrectMessage() {
        Date overlapStartDate = Date.today().addDays(10);

        User familyUser = UserTestData.createFamilyPortalUser();

        Integer numberOfYears = 2;
        List<Academic_Year__c> testAcademicYears = AcademicYearTestData.Instance.createAcademicYears(numberOfYears);

        testAcademicYears[0].Family_Portal_Start_Date__c = overlapStartDate;
        testAcademicYears[0].Family_Portal_End_Date__c = Date.today().addDays(20);

        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-5);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(15);
        insert testAcademicYears;

        String landingPageMessage;
        String expectedMessage;
        System.runAs(familyUser) {
            FamilyPortalLandingPageController controller = new FamilyPortalLandingPageController();

            landingPageMessage = controller.getMessage();
            expectedMessage = String.format(Label.FAM_Landing_Page_Message,
                    new List<String> {
                            controller.nextAcademicYear,
                            overlapStartDate.format(),
                            controller.currentAcademicYear });
        }

        System.assertEquals(expectedMessage, landingPageMessage, 'Expected the messages to be equal.');
    }
}