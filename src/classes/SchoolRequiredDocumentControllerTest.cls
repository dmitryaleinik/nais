/*
* SPEC-098
*
* Schools should be able to create custom labels for the non-standard documents they require which currently have generic names
*
* SPEC-102
* 
* Schools should be able to see the required documents they had chosen for the prior application cycle
*
* Nathan, Exponent Partners, 2013
*/
@isTest
private class SchoolRequiredDocumentControllerTest
{

    private static final String PAGE_ERROR_MESSAGE = 'Document already exists';
    private class TestData {
        Id academicYearId;
        Account school1;
        Contact staff1, parentA, parentB, student1, student2;
        Applicant__c applicant1A;
        PFS__c pfs1;
        Student_Folder__c studentFolder1;
        School_PFS_Assignment__c spfsa1;
        Required_Document__c rd1, rd2;
        Family_Document__c fd1, fd2;
        School_Document_Assignment__c sda1, sda2;

        private TestData() {
            // academic year
            TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
            academicYearId = GlobalVariables.getCurrentAcademicYear().Id;  

            // school
            school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, null, false); 
            insert school1;       

            // parents and students
            parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
            parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
            student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
            student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
            insert new List<Contact> {parentA, parentB, student1, student2};

            // psf
            pfs1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
            insert new List<PFS__c> {pfs1};

            // applicant
            applicant1A = TestUtils.createApplicant(student1.Id, pfs1.Id, false);
            insert new List<Applicant__c> {applicant1A};

            // student folder
            studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
            insert studentFolder1;

            // school pfs assignment
            spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder1.Id, false);
            insert spfsa1;

            // required documents
            rd1 = TestUtils.createRequiredDocument(academicYearId, school1.Id, false);
            rd1.Document_Type__c = 'Doc Type 1';    // required in vf page
            rd1.Document_Year__c = '2010';    // required in vf page
            insert rd1;
            rd2 = TestUtils.createRequiredDocument(academicYearId, school1.Id, false);
            rd2.Document_Type__c = 'Doc Type 2';    // required in vf page
            rd2.Document_Year__c = '2010';    // required in vf page
            insert rd2;

            // family documents
            fd1 = TestUtils.createFamilyDocument('FamilyDoc1', academicYearId, false);
            insert fd1;
            fd2 = TestUtils.createFamilyDocument('FamilyDoc2', academicYearId, false);
            insert fd2;

            // school document assignments
            sda1 = TestUtils.createSchoolDocumentAssignment('sda1', fd1.Id, rd1.Id, spfsa1.Id, false);
            insert sda1;            
            sda2 = TestUtils.createSchoolDocumentAssignment('sda2', fd2.Id, rd2.Id, spfsa1.Id, false);
            insert sda2;            
        }
    }

    @isTest
    private static void testDocumentListSize() {
        TestData td = new TestData();

        Test.startTest();
        PageReference pageRef = Page.SchoolRequiredDocument;
        pageRef.getParameters().put('schoolId', td.school1.Id);
        Test.setCurrentPage(pageRef);
        SchoolRequiredDocumentController controller = new SchoolRequiredDocumentController();        
        
        // seed test data has 2 required docs for this academic year
        pageRef.getParameters().put('academicYearId', td.academicYearId);
        system.assertNotEquals(null, controller.selectedAcademicYear);     // this will load academic year map
        system.assertEquals(2, controller.requiredDocumentList.size());     // this will load required document map
        
        List<SelectOption> docYearList = controller.documentYearOptions;
        system.assertEquals(2, docYearList.size());
        Academic_Year__c currAcadYear = GlobalVariables.getCurrentAcademicYear();
        Integer acadYear = integer.valueof(currAcadYear.name.substring(0,4));
        system.assert(docYearList.get(0).getValue() == string.valueof(acadYear-1) || docYearList.get(1).getValue() == string.valueof(acadYear-2));
        Test.stopTest();    
    }

    @isTest
    private static void testDeleteDocument() {
        TestData td = new TestData();

        Test.startTest();
        PageReference pageRef = Page.SchoolRequiredDocument;
        pageRef.getParameters().put('schoolId', td.school1.Id);
        Test.setCurrentPage(pageRef);
        SchoolRequiredDocumentController controller = new SchoolRequiredDocumentController();        
        
        // delete rd1
        pageRef.getParameters().put('academicYearId', td.academicYearId);
        system.assertNotEquals(null, controller.selectedAcademicYear);     // this will load academic year map
        system.assertEquals(2, controller.requiredDocumentList.size());     // this will load required document map
        pageRef.getParameters().put('requiredDocumentId', td.rd1.Id);
        controller.errorMessageStr = PAGE_ERROR_MESSAGE;
        controller.deleteDocument();
        
        // wire frame note 5.
        // If a required document is deleted, we will set a delete flag on it, and remove all
        // School Document Assigments related to it

        // requery and verify
        system.assertEquals(true, [select Deleted__c from Required_Document__c where Id = :td.rd1.Id].Deleted__c);
        system.assertEquals(0, [select Id from School_Document_Assignment__c where Required_Document__c = :td.rd1.Id].size());
        system.assertEquals(null, controller.errorMessageStr);     
        Test.stopTest();    
    }

    @isTest
    private static void testChangeDocumentType() {
        TestData td = new TestData();

        Test.startTest();
        PageReference pageRef = Page.SchoolRequiredDocument;
        pageRef.getParameters().put('schoolId', td.school1.Id);
        pageRef.getParameters().put('isKSSchoolUser', 'true');
        Test.setCurrentPage(pageRef);
        SchoolRequiredDocumentController controller = new SchoolRequiredDocumentController();        
        
        // delete rd1
        pageRef.getParameters().put('academicYearId', td.academicYearId);
        system.assertNotEquals(null, controller.selectedAcademicYear);     // this will load academic year map
        system.assertEquals(2, controller.requiredDocumentList.size());     // this will load required document map
        pageRef.getParameters().put('requiredDocumentId', td.rd1.Id);
        controller.errorMessageStr = PAGE_ERROR_MESSAGE;
        
        controller.editDocument();

        system.assertEquals(null, controller.errorMessageStr);
        controller.requiredDocument.Document_Type__c = 'Guardianship Document';
        
        // Data dictionary for object: Required Document, field: Document Type
        // Notes: last four items are conditionally required for KS
        
        controller.requiredDocument.Document_Type__c = 'Test Type1';

        // Data dictionary for object: Required Document, field: Document Type
        // Notes: last four items are conditionally required for KS
        
        controller.requiredDocument.Document_Type__c = 'Guardianship Document';

        Test.stopTest();    
    }

    @isTest
    private static void testSaveAndNewDocument()
    {
        Document_Type_Settings__c documentSetting1 = new Document_Type_Settings__c();
        documentSetting1.Name = 'NonTax';
        documentSetting1.Full_Type_Name__c = 'NonTax';
        documentSetting1.Tax_Document__c = false;
        
        Document_Type_Settings__c documentSetting2 = new Document_Type_Settings__c();
        documentSetting2.Name = 'School/Organization Other Document';
        documentSetting2.Full_Type_Name__c = 'School/Organization Other Document';
        documentSetting2.Category__c = 'School-Specific Document';
        documentSetting2.Tax_Document__c = false;
        Database.insert(new List<Document_Type_Settings__c>{documentSetting1, documentSetting2});
        
        TestData td = new TestData();

        Test.startTest();
        PageReference pageRef = Page.SchoolRequiredDocument;
        pageRef.getParameters().put('schoolId', td.school1.Id);
        Test.setCurrentPage(pageRef);
        SchoolRequiredDocumentController controller = new SchoolRequiredDocumentController();
        
        // edit rd1
        pageRef.getParameters().put('academicYearId', td.academicYearId);
        system.assertNotEquals(null, controller.selectedAcademicYear);     // this will load academic year map
        system.assertEquals(2, controller.requiredDocumentList.size());     // this will load required document map
        pageRef.getParameters().put('requiredDocumentId', td.rd1.Id);
        controller.editDocument();        
        
        // change a field and verify
        controller.requiredDocument.Document_Type__c = 'NonTax';
        system.assertEquals(false, controller.isTaxDocument);

        controller.errorMessageStr = PAGE_ERROR_MESSAGE;

        controller.saveDocument();
        
        system.assertEquals(null, controller.errorMessageStr);
        // Verify that there were no validation errors
        system.assertEquals(false, ApexPages.hasMessages(ApexPages.Severity.ERROR));
        
        // Verify that year defaulting for non-tax is working
        // NAIS-2191 [DP] 01.28.2015 -- now making non-tax docs the year previous to the acad year (e.g., 2014-2015 non-tax docs get a year of 2013)
        system.assertEquals(String.valueOf(Integer.valueOf(controller.academicYear.Name.split('-')[0])-1), [select Document_Year__c from Required_Document__c where Id = :td.rd1.Id].Document_Year__c);
        
        controller.requiredDocument = new Required_Document__c();
        controller.requiredDocument.Document_Type__c = 'School/Organization Other Document';
        controller.requiredDocument.Label_for_School_Specific_Document__c = 'testLabel';
        controller.saveDocument();
        
        // NAIS-2191 [DP] 01.28.2015 -- now making non-tax docs the year previous to the acad year (e.g., 2014-2015 non-tax docs get a year of 2013)
        system.assertEquals(String.valueOf(Integer.valueOf(controller.academicYear.Name.split('-')[0])-1), [select Document_Year__c from School_Document_Assignment__c where Document_Type__c = 'School/Organization Other Document' limit 1].Document_Year__c);
          
        
        controller.requiredDocument = new Required_Document__c();
        controller.requiredDocument.Document_Type__c = 'School/Organization Other Document';
        controller.requiredDocument.Label_for_School_Specific_Document__c = 'testLabel2';
        controller.saveDocument();
        
        // Verify that there is a validation error
        system.assertEquals(true, ApexPages.hasMessages(ApexPages.Severity.ERROR));      

        Test.stopTest();
    }
  
    @isTest
    private static void testOnChangeAcademicYear() {
        TestData td = new TestData();

        Test.startTest();
        PageReference pageRef = Page.SchoolRequiredDocument;
        pageRef.getParameters().put('schoolId', td.school1.Id);
        Test.setCurrentPage(pageRef);
        SchoolRequiredDocumentController controller = new SchoolRequiredDocumentController();        
        
        // edit rd1
        pageRef.getParameters().put('academicYearId', td.academicYearId);
        system.assertNotEquals(null, controller.selectedAcademicYear);     // this will load academic year map
        system.assertEquals(2, controller.requiredDocumentList.size());     // this will load required document map
        pageRef.getParameters().put('requiredDocumentId', td.rd1.Id);
        controller.editDocument();
        controller.onChangeAcademicYear();
        
        // it loads a new new required document
        system.assertEquals(null, controller.requiredDocument.Id);
        
        Test.stopTest();    
    }
    
    @isTest
    private static void testSaveOtherDocument()
    {
        Document_Type_Settings__c documentType1 = new Document_Type_Settings__c(
                                                            Name = 'State Tax Return',
                                                            Full_Type_Name__c = 'State Tax Return',
                                                            Tax_Document__c = false);
        
        Document_Type_Settings__c documentType2 = new Document_Type_Settings__c(
                                                            Name = 'School/Organization Other Document',
                                                            Full_Type_Name__c = 'School/Organization Other Document',
                                                            Category__c = 'School-Specific Document',
                                                            Tax_Document__c = false);
        Database.insert(new List<Document_Type_Settings__c>{documentType1, documentType2});
        
        TestData testData = new TestData();
        Test.startTest();
        
        PageReference pageRef = Page.SchoolRequiredDocument;
        pageRef.getParameters().put('schoolId', Id.ValueOf(testData.school1.Id));
        pageRef.getParameters().put('academicYearId', testData.academicYearId);
        Test.setCurrentPage(pageRef);
        SchoolRequiredDocumentController controller = new SchoolRequiredDocumentController();
        
        // change a field and verify
        controller.requiredDocument.Document_Type__c = 'School/Organization Other Document';
        controller.requiredDocument.Label_for_School_Specific_Document__c = 'Other 1';
        controller.attachments.inputFileBody = Blob.valueOf('Test Data');
        controller.attachments.inputFileName = 'Test Data';
        controller.saveDocument();
        system.assertEquals(false, ApexPages.hasMessages(ApexPages.Severity.ERROR));
        
         Attachment parentAttachment = [Select Id, ParentId from Attachment 
                                                        where Id=:controller.attachments.attachmentRecord.Id 
                                                        limit 1];
        Attachment__c parentAttachmentCustom = [Select Id, Parent_ID__c from Attachment__c 
                                                        where Id=:parentAttachment.parentId 
                                                        limit 1];
        system.assertNotEquals(null, parentAttachmentCustom.Parent_ID__c);
        
        
        Required_Document__c requiredDocument = [Select Id from Required_Document__c 
                                                        where Id=:parentAttachmentCustom.Parent_ID__c 
                                                        limit 1];
        
        pageRef.getParameters().put('requiredDocumentId', requiredDocument.Id);
        controller.requiredDocument.Document_Type__c = 'School/Organization Other Document';
        controller.requiredDocument.Label_for_School_Specific_Document__c = 'Other 2';
        controller.attachments.inputFileBody = Blob.valueOf('Test Data 2');
        controller.attachments.inputFileName = 'Test Data 2';
        controller.editDocument();
        controller.setAcademicYearId(testData.academicYearId);
        system.assertEquals(testData.academicYearId, controller.getAcademicYearId());
        system.assertEquals(false, ApexPages.hasMessages(ApexPages.Severity.ERROR));
        
        controller.SchoolAcademicYearSelector_OnChange(true);
        system.assertEquals(false, ApexPages.hasMessages(ApexPages.Severity.ERROR));
        controller.cancelDocument();
        system.assertEquals(false, ApexPages.hasMessages(ApexPages.Severity.ERROR));
        
        Test.stopTest();
    }
}