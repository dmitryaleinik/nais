@isTest
public class FamilyDashboardTestDataHelper {
    
    /**
    * @description: Creates contact records for test of security questions check.
    */
    public static List<Contact> createContactsForSecurityQuestionsCheck() {
        String testQuestion = 'Test Question';
        String testAnswer = 'Test Answer';
        Contact validContact = ContactTestData.Instance
            .forSecQuestion1(testQuestion)
            .forSecQuestion2(testQuestion)
            .forSecQuestion3(testQuestion)
            .forSecQuestionAnswer1(testAnswer)
            .forSecQuestionAnswer2(testAnswer)
            .forSecQuestionAnswer3(testAnswer).create();
        Contact notValidContact1 = ContactTestData.Instance.create();
        Contact notValidContact2 = ContactTestData.Instance
            .forSecQuestion1(testQuestion)
            .forSecQuestion2(testQuestion)
            .forSecQuestionAnswer1(testAnswer)
            .forSecQuestionAnswer2(testAnswer).create();

        return new List<Contact>{validContact, notValidContact1, notValidContact2};
    }
}