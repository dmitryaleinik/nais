/* 
 * Spec-137 School Portal - W2 Summary;
 * BR, Exponent Partners, 2013
 */
       
public with sharing class SchoolW2SummaryController {

    /*Initialization*/
    
    public SchoolW2SummaryController(ApexPages.StandardController controller) {
        /* REFACTORE TO USE SchoolPFSAssignmentId
        pfsNumber = System.currentPageReference().getParameters().get('pfsnum');
        if (pfsNumber != null)
        {
            myDocs = [SELECT PFS_Number__c, First_Name__c, Last_Name__c, W2_Employee_ID__c, W2_Employer_ID__c, W2_Wages__c, W2_SS_Tax__c,
                            W2_Medicare__c, W2_Deferred_Untaxed_1_A__c, W2_Deferred_Untaxed_2_B__c, W2_Deferred_Untaxed_3_C__c, W2_Deferred_Untaxed_4_D__c
                            FROM Family_Document__c
                            WHERE PFS_Number__c = :pfsNumber and Document_Type__c = 'W2'];
        }

        */
        schoolPFSAssignmentId = ApexPages.currentPage().getParameters().get('SchoolPFSAssignmentId');
        
        Set<Id> docIds = new Set<Id>();
        for (School_Document_Assignment__c sda : [Select School_PFS_Assignment__c, Document__c From School_Document_Assignment__c WHERE School_PFS_Assignment__c = :schoolPFSAssignmentId]){
            docIds.add(sda.Document__c);
        }
                                                        
        myDocs = [SELECT PFS_Number__c, First_Name__c, Last_Name__c, W2_Employee_ID__c, W2_Employer_ID__c, W2_Wages__c, W2_SS_Tax__c,
                            W2_Medicare__c, W2_Deferred_Untaxed_1_A__c, W2_Deferred_Untaxed_2_B__c, W2_Deferred_Untaxed_3_C__c, W2_Deferred_Untaxed_4_D__c
                            FROM Family_Document__c
                            WHERE Id in :docIds and Document_Type__c = 'W2' and Duplicate__c != 'Yes'];
                            // [dp] NAIS-1650 added line to filter out duplicates 4.30.14
                
        if(schoolPFSAssignmentId != null){
            // To be able to pull the pfsId to feed to the Top Bar component
            List<School_PFS_Assignment__c> tempSPAs = [Select Id, Student_Folder__c from School_PFS_Assignment__c where Id = :schoolPFSAssignmentId];
            if(tempSPAs.size() > 0){
                folderId = tempSPAs[0].Student_Folder__c;
            }
        }
    }

    /*Properties*/
    public List<Family_Document__c> myDocs { get; set; }
    public String pfsNumber { get; set; }
    public String folderId { get; set; }
    
    // [CH] NAIS-756
    public Boolean isEditableYear {
        get{
            return GlobalVariables.isEditableYear(schoolPFSAssignment.Academic_Year_Picklist__c);
        }
        set;
    }
    
    // For LeftNav.
    public String schoolPFSAssignmentId {get; set;}
    public School_PFS_Assignment__c schoolPFSAssignment {
        get{
            if(schoolPFSAssignment == null){
                schoolPFSAssignment = [select Id, Academic_Year_Picklist__c from School_PFS_Assignment__c where id = :schoolPFSAssignmentId];
            }
            return schoolPFSAssignment;
        } 
        set;
    }
    
}