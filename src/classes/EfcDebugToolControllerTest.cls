@isTest
private class EfcDebugToolControllerTest
{

    static void populateWorksheet(EfcWorksheetData worksheet)
    {
        worksheet.parentState = 'Alabama';
        worksheet.familyStatus = '2 Parents';
        worksheet.ageParentA = 45;
        worksheet.ageParentB = 40;
        worksheet.familySize = 5;
        worksheet.numChildrenInTuitionSchools = 2;
        worksheet.gradeApplying = '4';
        worksheet.dayOrBoarding = 'Day';
        worksheet.filingStatus = 'Married, Filing Jointly';
        worksheet.incomeTaxExemptions = 4;
        worksheet.salaryWagesParentA = 100000; 
        worksheet.salaryWagesParentB = 50000;
        worksheet.interestIncome = 10000;
        worksheet.otherIncome = 10000;
        worksheet.otherUntaxedIncome = 10000;
        worksheet.medicalDentalExpenses = 10000;
        worksheet.unusualExpenses = 10000;
        worksheet.homePurchaseYear = '2000';
        worksheet.homePurchasePrice = 400000;
        worksheet.homeMarketValue = 500000;
        worksheet.unpaidPrincipal1stMortgage = 100000;
        worksheet.otherRealEstateMarketValue = 100000;
        worksheet.otherRealEstateUnpaidMortgagePrincipal = 10000;
        worksheet.bankAccounts = 10000;
        worksheet.totalDebts = 1000;
        worksheet.studentAssets = 1000;
        worksheet.pjOverrideDefaultColaValue = 1.2;
        worksheet.pjUseDividendInterestIncomeToImputeAssets = true;
        worksheet.pjPercentageForImputingAssets = 2;
        worksheet.pjUseHomeEquity = true;
        worksheet.pjUseHomeEquityCap = true;
        worksheet.pjUseHousingIndexMultiplier = true;
    }

    @isTest
    private static void testEfcDebugToolController()
    {
        // set up the test data
        List<Academic_Year__c> years = TestUtils.createAcademicYears();
        Academic_Year__c academicYear = GlobalVariables.getCurrentAcademicYear();
        TestUtils.createSSSConstants(academicYear.Id, true);
        TableTestData.populateAllEfcTables(academicYear.Id);
        
        // instantiate the controller
        Test.setCurrentPageReference(Page.InternalEfcDebugTool);
        EfcDebugToolController controller = new EfcDebugToolController();
        
        // test calc all
        controller.worksheet.academicYearId = academicYear.Id;
        populateWorksheet(controller.worksheet);
        controller.calcAll();
        for (ApexPages.Message msg : ApexPages.getMessages()) {
            System.debug('EfcDebugToolController.calcAll() message: ' + msg.getSummary());
            System.assertNotEquals(ApexPages.Severity.ERROR, msg.getSeverity());
        }
        
        // test calc steps
        controller = new EfcDebugToolController();
        controller.worksheet.academicYearId = academicYear.Id;
        populateWorksheet(controller.worksheet);
        controller.calcTaxableIncome();
        controller.calcNonTaxableIncome();
        controller.calcTotalIncome();
        controller.calcTotalAllowances();
        controller.calcEffectiveIncome();
        controller.calcHomeEquity();
        controller.calcNetWorth();
        controller.calcDiscretionaryNetWorth();
        controller.calcIncomeSupplement();
        controller.calcAdjustedEffectiveIncome();
        controller.calcRevisedAdjustedEffectiveIncome();
        controller.calcIncomeProtectionAllowance();
        controller.calcDiscretionaryIncome();
        controller.calcEstimatedParentalContribution();
        controller.calcEstimatedParentalContributionPerChild();
        controller.calcStudentAssetContribution();
        controller.calcEstimatedFamilyContribution();
        //controller.calcEstimatedFamilyContributionDay();
        //controller.calcEstimatedFamilyContributionBoarding();
        for (ApexPages.Message msg : ApexPages.getMessages()) {
            System.debug('EfcDebugToolController.calcAll() message: ' + msg.getSummary());
            System.assertNotEquals(ApexPages.Severity.ERROR, msg.getSeverity());
        }
        
        // test lookups
        controller = new EfcDebugToolController();
        
        System.assertEquals(true, (controller.getFilingStatusOptions()).size()>0);
        System.assertEquals(true, (controller.getParentBFilingStatusOptions()).size()>0);
        System.assertEquals(true, (controller.getAcademicYearOptions()).size()>0);
        controller.tableAcademicYear = academicYear.Id;
        controller.tableFilingStatus = EfcPicklistValues.FILING_STATUS_MARRIED_JOINT;
        controller.tableFederalIncome = 100000;
        controller.lookupFederalTax();
        
        controller.tableState = 'Alabama';
        controller.tableStateIncome = 100000;
        controller.lookupStateTax();
    
        controller.tableHimHomePurchaseYear = '2000';    
        controller.lookupHousingIndexMultiplier();
        
        controller.tableValueOfBusinessFarm = 100000;
        controller.lookupBusinessFarmShare();
        
        controller.tableFamilyStatus = EfcPicklistValues.FAMILY_STATUS_2_PARENT;
        controller.tableAge = 42;
        controller.lookupRetirementAllowance();
        
        controller.tableEmploymentAllowanceIncome = 100000;
        controller.lookupEmploymentAllowance();
        
        controller.tableDiscretionaryNetWorth = 100000;
        controller.tableConversionCoefficient = .2;
        controller.lookupAssetProgressivity();
        
        controller.tableFamilySize = 4;
        controller.lookupIncomeProtectionAllowance();
        
        controller.tableContributionDiscretionaryIncome = 100000;
        controller.lookupEstimatedContributionRate();
        
        controller.lookupSssConstants();
        
        for (ApexPages.Message msg : ApexPages.getMessages()) {
            System.debug('EfcDebugToolController.calcAll() message: ' + msg.getSummary());
            System.assertNotEquals(ApexPages.Severity.ERROR, msg.getSeverity());
        }
        
        // test serialize/deserialize
        controller = new EfcDebugToolController();
        controller.worksheet.academicYearId = academicYear.Id;
        populateWorksheet(controller.worksheet);
        controller.serializeWorksheet();
        String serializedWorksheet = controller.jsonWorksheet;
        controller = new EfcDebugToolController();
        controller.jsonWorksheet = serializedWorksheet;
        controller.deserializeWorksheet();
        System.assertEquals(100000, controller.worksheet.salaryWagesParentA);
    }

    @isTest
    private static void testLookupTables()
    {
        // create a test academic year
        Academic_Year__c academicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        // create table data
        TableTestData.populateIncomeProtectionAllowanceData(academicYear.Id);
        // create SSS Constants
        TestUtils.createSSSConstants(academicYear.Id, true);
        
        Account family = TestUtils.createAccount('The individual', RecordTypes.individualAccountTypeId, 5, true);
        Contact parent = TestUtils.createContact('Parent', family.Id, RecordTypes.parentContactTypeId, true);
        Contact student = TestUtils.createContact('Student', family.Id, RecordTypes.studentContactTypeId, true);
        Student_Folder__c folder = TestUtils.createStudentFolder('Folder', academicYear.Id, student.Id, true);
        PFS__c pfs = TestUtils.createPFS('pfs', academicYear.Id, parent.Id, true);
        
        // Create an SPA record
        School_PFS_Assignment__c spaRecord = EfcTestData.createTestSchoolPfsAssignment(false, academicYear); 
        spaRecord.Academic_Year_Picklist__c = academicYear.Name;
        spaRecord.Adjust_Housing_Portion_of_IPA__c = 'Yes';
        spaRecord.IPA_Housing_Family_of_4__c  = 4000;
        insert spaRecord;

        // SDAs need to have Document Year populated now.
        String documentYear = GlobalVariables.getDocYearStringFromAcadYearName(GlobalVariables.getCurrentYearString());
        School_Document_Assignment__c sda = new School_Document_Assignment__c(School_PFS_Assignment__c=spaRecord.Id);
        sda.Document_Year__c = documentYear;
        insert sda;
        
        EfcDebugToolController controller = new EfcDebugToolController();
        controller.dummySchoolPfsAssignmentLookup = sda;
        controller.loadFromSchoolPfsAssignment();
        System.assertEquals(true, controller.worksheet!=null);
        controller.worksheet = null;
        
        Applicant__c applicant1 = new Applicant__c();
        applicant1.PFS__c = pfs.Id;
        applicant1.First_Name__c = 'Bob';
        applicant1.Last_Name__c = 'Thomas';
        applicant1.Contact__c = student.Id;
        applicant1.SSN__c = '6789';
        insert applicant1;
        
        controller.dummyPfsLookup = applicant1;
        controller.loadFromPfs();
        System.assertEquals(true, controller.worksheet!=null);
    }
}