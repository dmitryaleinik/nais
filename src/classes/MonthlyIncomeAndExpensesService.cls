public class MonthlyIncomeAndExpensesService {
    
    public static final Integer MIE_Request_Enabled = 1;
    public static final Integer MIE_Request_Incompleted = 2;
    public static final Integer MIE_Request_Completed = 3;
    public static final Integer MIE_Request_Has_Errors = 4;
    
    private MonthlyIncomeAndExpensesService() { }
    
    private Decimal nullToZero(Decimal val) {
        
        return val == null ? 0 : val;
    }
    
    public Decimal getMonthlyIncomeTotal(PFS__c pfs) {
        
        Decimal Alimony_Current = nullToZero(pfs.Alimony_Current__c);
        Alimony_Current = Alimony_Current != 0 ? Alimony_Current / 12 : 0;
        
        Decimal Child_Support_Received_Current = nullToZero(pfs.Child_Support_Received_Current__c);
        Child_Support_Received_Current = Child_Support_Received_Current != 0 ? Child_Support_Received_Current / 12 : 0;
        
        return nullToZero(pfs.Monthly_Income_Parents_After_Taxes__c) 
            + nullToZero(pfs.Monthly_Income_Applicants_After_Taxes__c) 
            + Alimony_Current 
            + Child_Support_Received_Current 
            + nullToZero(pfs.Other_Monthly_Income__c);
        
    }
    
    public Decimal getMonthlySupportTotal(PFS__c pfs) {
        
        return nullToZero(pfs.Monthly_Expenses_Alimony__c) 
            + nullToZero(pfs.Monthly_Expenses_Child_Support__c) 
            + nullToZero(pfs.Other_Monthly_Support_Expenses__c);
    }
    
    
    public Decimal getMonthlyHousingTotal(PFS__c pfs) {
        
        return nullToZero(pfs.Monthly_Expenses_Mortage_Rent_primary__c) 
            + nullToZero(pfs.Monthly_Expenses_Utilities__c) 
            + nullToZero(pfs.Monthly_Expenses_Property_Taxes_Expenses__c) 
            + nullToZero(pfs.Monthly_Expenses_Home_s_Owner_Insurance__c) 
            + nullToZero(pfs.Monthly_Expenses_Association_Fees__c) 
            + nullToZero(pfs.Monthly_Expenses_Maintenance__c) 
            + nullToZero(pfs.Monthly_Expenses_Mortage_Rent_all__c) 
            + nullToZero(pfs.Other_Monthly_Housing_Expenses__c);
    }
    
    public Decimal getMonthlyTuitionTotal(PFS__c pfs) {
        
        return nullToZero(pfs.Monthly_Expenses_Tuition__c) 
            + nullToZero(pfs.Monthly_Expenses_Childcare__c) 
            + nullToZero(pfs.Other_Monthly_Tuition_Childcare_Expenses__c);
    }
    
    public Decimal getMonthlyVehicleTotal(PFS__c pfs) {
        
        return nullToZero(pfs.Monthly_Expenses_Vehicle_Payments__c) 
            + nullToZero(pfs.Monthly_Expenses_Vehicle_Insurance__c) 
            + nullToZero(pfs.Monthly_Expenses_Vehicle_Maintenance__c) 
            + nullToZero(pfs.Other_Monthly_Expenses_Vehicle__c);
    }
    
    
    public Decimal getMonthlyHealthcareTotal(PFS__c pfs) {
        
        return nullToZero(pfs.Monthly_Expenses_Healthcare_Insurance__c) 
            + nullToZero(pfs.Monthly_Expenses_Dental_Insurance__c) 
            + nullToZero(pfs.Monthly_Expenses_Life_Insurance__c) 
            + nullToZero(pfs.Monthly_Expenses_Insurance_not_covered__c) 
            + nullToZero(pfs.Other_Monthly_Healthcare_Expenses__c);
    }
    
    public Decimal getMonthlyFoodTotal(PFS__c pfs) {
        
        return nullToZero(pfs.Monthly_Expenses_Groceries__c) 
            + nullToZero(pfs.Monthly_Expenses_Dining_out__c) 
            + nullToZero(pfs.Other_Monthly_Food_Expenses__c);
    }
    
    public Decimal getMonthlyMiscellaneousTotal(PFS__c pfs) {
        
        return nullToZero(pfs.Monthly_Expenses_Clothing__c) 
            + nullToZero(pfs.Monthly_Expenses_Pet_Care__c) 
            + nullToZero(pfs.Monthly_Expenses_Bus_Metro_Rail__c) 
            + nullToZero(pfs.Monthly_Expenses_Savings__c) 
            + nullToZero(pfs.Monthly_Expenses_Entertainment__c)
            + nullToZero(pfs.MIE_Travel_Expenses__c)
            + nullToZero(pfs.MIE_Student_Loan_Payments__c)
            + nullToZero(pfs.Other_Monthly_Miscellaneous_Expenses__c);
    }
    
    public Decimal getMonthlyDebtPaymentsTotal(PFS__c pfs) {
        
         return nullToZero(pfs.MIE_Retirement_Savings__c) 
            + nullToZero(pfs.MIE_Debt_Payments__c);
    }//End:getMonthlyDebtPaymentsTotal
    
    public Decimal getMonthlyTotalExpenses(PFS__c pfs) {
        
        Decimal Monthly_Support_Total = MonthlyIncomeAndExpensesService.Instance
            .getMonthlySupportTotal(pfs);
        
        Decimal Monthly_Housing_Total = MonthlyIncomeAndExpensesService.Instance
            .getMonthlyHousingTotal(pfs);
        
        Decimal Monthly_Tuition_Total = MonthlyIncomeAndExpensesService.Instance
            .getMonthlyTuitionTotal(pfs);
        
        Decimal Monthly_Vehicle_Total = MonthlyIncomeAndExpensesService.Instance
            .getMonthlyVehicleTotal(pfs);
        
        Decimal Monthly_Healthcare_Total = MonthlyIncomeAndExpensesService.Instance
            .getMonthlyHealthcareTotal(pfs);
            
        Decimal Monthly_Food_Total = MonthlyIncomeAndExpensesService.Instance
            .getMonthlyFoodTotal(pfs);
        
        Decimal Monthly_Miscellaneous_Total = MonthlyIncomeAndExpensesService.Instance
            .getMonthlyMiscellaneousTotal(pfs);
            
        Decimal Monthly_Debt_Payments = MonthlyIncomeAndExpensesService.Instance
            .getMonthlyDebtPaymentsTotal(pfs);
        
        return Monthly_Support_Total 
            + Monthly_Housing_Total 
            + Monthly_Tuition_Total 
            + Monthly_Vehicle_Total 
            + Monthly_Healthcare_Total 
            + Monthly_Food_Total 
            + Monthly_Miscellaneous_Total
            + Monthly_Debt_Payments;
    }
    
    public Decimal getMonthlyNetIncome(PFS__c pfs) {
        
        Decimal Monthly_Income_Total = MonthlyIncomeAndExpensesService.Instance
            .getMonthlyIncomeTotal(pfs);
        
        Decimal Monthly_Total_Expenses = MonthlyIncomeAndExpensesService.Instance
            .getMonthlyTotalExpenses(pfs);
        
        return Monthly_Income_Total - Monthly_Total_Expenses;
    }
    
    
    /**
    * @description Returns true if the MIE was requested for the given studentFolder. 
    *              This method can be used to show/hide MIE menu.
    * @param studentFolder The Id of the studentFolder record for which we want to determine if MIE was already requested.
    * @return Returns true if the MIE was requested for the given studentFolder. Otherwise, false.
    */
    public static Boolean isMIEActive(Id studentFolderId) {
        
        //1. Make sure that the studentFolderId is not null.
        if (studentFolderId == null) { return false; }
        
        //2. Verify if the MIE Request was requested for the given pfs.
        MonthlyIncomeAndExpensesService.MIERequestBulk request = 
            new MonthlyIncomeAndExpensesService.MIERequestBulk(new Set<Id>{studentFolderId});
            
        MonthlyIncomeAndExpensesService.MIEResponseBulk response = 
            new MonthlyIncomeAndExpensesService.MIEResponseBulk(request, false, true);
        
        //3. Put all pfs that already has request MIE in a single set of ids.
        Set<Id> allPFSAlreadyRequestedMIE = response.pfsAlreadyCompletedIds;
        allPFSAlreadyRequestedMIE.addAll(response.pfsRequestedAndIncompletedIds);
        
        //4. Return true, if all the pfs related to the given PFS are contained in allPFSAlreadyRequestedMIE. 
        return allPFSAlreadyRequestedMIE.containsAll(response.pfsRequestedIds);
    }

    /**
     * @description Returns the MIEResponse which contains informations about whether or not MIE information is available for the PFSs related to the specified folder.
     * @param studentFolderId The Id of the student folder to get MIE Status information for.
     * @return An MIEResponseBulk instance.
     */
    public static MIEResponseBulk getMIEStatus(Id studentFolderId) {
        //2. Verify if the MIE Request was requested for the given pfs.
        MonthlyIncomeAndExpensesService.MIERequestBulk request =
                new MonthlyIncomeAndExpensesService.MIERequestBulk(new Set<Id>{studentFolderId});

        MonthlyIncomeAndExpensesService.MIEResponseBulk response =
                new MonthlyIncomeAndExpensesService.MIEResponseBulk(request, false, true);

        return response;
    }

    /**
    * @description Returns true if the MIE was requested for the given PFS. 
    *              This method can be used to show/hide MIE menu.
    * @param pfs The PFS record for which we want to determine if MIE was already requested.
    * @return Returns true if the MIE was requested for the given PFS. Otherwise, false.
    */
    public static Boolean isMIEActive(PFS__c pfs) {
        
        //1. Make sure that the PFS is not null.
        if (pfs == null || pfs.Id == null) { return false; }
        
        
        //2. Verify if the MIE Request was requested for the given pfs.
        MonthlyIncomeAndExpensesService.MIERequestBulk request = 
            new MonthlyIncomeAndExpensesService.MIERequestBulk(pfs.Id);
            
        MonthlyIncomeAndExpensesService.MIEResponseBulk response = 
            new MonthlyIncomeAndExpensesService.MIEResponseBulk(request, false);
            
        return response.pfsAlreadyCompletedIds.contains(pfs.Id) || response.pfsRequestedAndIncompletedIds.contains(pfs.Id);
        
    }//End:isMIEActive

    public static Integer isRequestMIEIndividuallyEnabled(PFS__c pfs) {
        // If the feature toggle is off, return 0 to indicate that it is not enabled.
        if (!FeatureToggles.isEnabled('Individual_MIE_Requests__c')) {
            return 0;
        }


        System.debug(pfs);
        //1. Make sure that the PFS is not null.
        if (pfs == null || pfs.Id == null) { return 0; }
         
        //2. Verify if the family portal is Open.
        Academic_Year__c academicYear = GlobalVariables.getAcademicYearByName(pfs.Academic_Year_Picklist__c);
        if (!AcademicYearService.Instance.isFamilyPortalOpen(academicYear.Id)) { return 0; }
        
        //3. Verify if the MIE Request is available for the given pfs.
        MonthlyIncomeAndExpensesService.MIERequestBulk request = 
            new MonthlyIncomeAndExpensesService.MIERequestBulk(pfs.Id);
            
        MonthlyIncomeAndExpensesService.MIEResponseBulk response = 
            new MonthlyIncomeAndExpensesService.MIEResponseBulk(request, false);
        
        if (!String.isBlank(response.errorMessage)) {
            return MIE_Request_Has_Errors;
            
        } else if (response.pfsUpdatedIds.contains(pfs.Id)) {
            
            //5. The PFS has not MIE requested by any of the selected schools or individually.
            return MIE_Request_Enabled;
            
        } else if (response.pfsAlreadyCompletedIds.contains(pfs.Id)) {
            
            //6. The MIE has been requested for the given PFS by any of the selected schools or individually. And it was already completed.
            return MIE_Request_Completed;
            
        } else if (response.pfsRequestedAndIncompletedIds.contains(pfs.Id)) {
            
            //7. The MIE has been requested for the given PFS by any of the selected schools or individually. And it has NOT been completed yet.
            return MIE_Request_Incompleted;
        }
        
        return 0;
    }
    
    public static String requestMIEIndividuallyDML(Id pfsId) {
        
        Set<Id> pfsUpdatedIds = new Set<Id>{pfsId};
        
        return requestMIEIndividuallyDML(pfsUpdatedIds);
        
    }//End:requestMIEIndividuallyDML
    
    public static String requestMIEIndividuallyDML(Set<Id> pfsUpdatedIds) {
        
        if (pfsUpdatedIds.isEmpty()) { return null; }
            
        Account currentSchool = GlobalVariables.getCurrentSchool();
        String currentSchoolName = currentSchool.Name;
        String currentSchoolId = Id.ValueOf(currentSchool.Id);
            
        List<PFS__c> pfsToUpdate = new List<PFS__c>();
        for (PFS__c pfs : PfsSelector.Instance.selectById(pfsUpdatedIds)) {
            
            pfs.MIE_Requested__c = true;
            pfs.MIE_Requested_By_School_Id__c = currentSchoolId;
            pfs.MIE_Requested_By_School_Name__c = currentSchoolName;
            pfsToUpdate.add(pfs);
        }

        if (!pfsToUpdate.isEmpty()) {
            
            try {
                update pfsToUpdate;
                
            }catch(Exception e) {
                return e.getMessage();
            }
        }
        
        return null;
            
    }//End:requestMIEIndividuallyDML
        
    /**
    * @description Returns the list of closed academic years.
    * @param spas The list of spas for which we neet to determine the closed AY for FP.
    * @return The list of closed AY for FP.
    */
    public static Set<String> familyPortalIsClosedForAcademicYear(List<School_PFS_Assignment__c> spas) {
        
        Date todayDate = system.today();
        Set<String> closedAY = new Set<String>();
        Set<String> academicYearNames = new Set<String>();
        List<School_PFS_Assignment__c> spasResult = new List<School_PFS_Assignment__c>();
        
        for (School_PFS_Assignment__c spa : spas) {
            
            academicYearNames.add(spa.Academic_Year_Name__c);
        }
        
        for (String academicYearName : academicYearNames) {
            if (!AcademicYearService.Instance.isFamilyPortalOpen(academicYearName)) {
                closedAY.add(academicYearName);
            }
        }
        
        return closedAY;
        
    }//End:familyPortalIsClosedForAcademicYear
    
    /**
     * @description Singleton instance of the MonthlyIncomeAndExpensesService.
     */
    public static MonthlyIncomeAndExpensesService Instance {
        get {
            if (Instance == null) {
                Instance = new MonthlyIncomeAndExpensesService();
            }
            return Instance;
        }
        private set;
    }
    
    //INNER CLASSES
    private without sharing class WithoutSharingQueryClass{
        
        public WithoutSharingQueryClass() {}
        
        public List<School_PFS_Assignment__c> selectByPFSId(Set<Id> pfsIds, List<String> fieldsToQuery) {
            
            return  SchoolPfsAssignmentsSelector.Instance.selectByPFSId(pfsIds, fieldsToQuery);
        }
        
        public List<Annual_Setting__c> selectBySchoolIdAcademicYearName(Set<String> schoolIdAcademicYearNameKeyset, List<String> fieldsToQuery) {
        
            return AnnualSettingsSelector.Instance.selectBySchoolIdAcademicYearName(schoolIdAcademicYearNameKeyset, fieldsToQuery);
        }
        
        public List<School_PFS_Assignment__c> selectBySetOfStudentFolders(Set<Id> folderIds, List<String> fieldsToQuery, String queryCondition) {
            
            return SchoolPfsAssignmentsSelector.Instance
                .selectBySetOfStudentFolders(folderIds, fieldsToQuery, queryCondition);
        }
    }
    
    public class MIERequestBulk {
        
        private Set<String> closedAY { get; set; }
        private Set<Id> pfsIds { get; set; }
        private List<School_PFS_Assignment__c> allRelatedSPAS { get; set; } //The list of all spa records related to the selected folder Ids.

        public Set<Id> StudentFolderIds {
            get {
                if (StudentFolderIds == null) {
                    StudentFolderIds = new Set<Id>();
                }
                return StudentFolderIds;
            }
            private set;
        }

        public MIERequestBulk(Set<Id> folderIds) {
            this.StudentFolderIds = folderIds;
            closedAY = new Set<String>();
            pfsIds = new Set<Id>();
            
            if (!SchoolPortalSettings.MIE_Pilot) { return; }
            
            //1. Define the list of fields to query for spa records.
            List<String> fieldsToQuery = getSPAFieldsToQuery();
            
            //2. Define the filters to be used in the query to spas.
            String queryCondition = 'Student_Folder__c IN :folderIds AND Withdrawn__c <> \'Yes\' ';
            
            //3. Select the spa records related to the current school (without sharing) for the given folder Ids.
            List<School_PFS_Assignment__c> spasWithSharing = (new WithoutSharingQueryClass()).selectBySetOfStudentFolders(folderIds, fieldsToQuery, queryCondition);
            
            //4. Get the PFS ids for the retrieved spa records.
            for (School_PFS_Assignment__c spa : spasWithSharing ) {
                
                pfsIds.add(spa.PFS_ID__c);
            }
            
            //5. Get the list of ALL spa records related to the selected folders 
            // (withou sharing: This means that we include spas from other schools). 
            //We need to run this query without sharing to be able to see spas from other schools.
            // We need to see SPAs from other schools so that we can determine if they require MIE globally. If this is
            // the case, then the family is already supposed to fill out the MIE information so we shouldn't send them another request.
            allRelatedSPAS = (new WithoutSharingQueryClass()).selectByPFSId(pfsIds, fieldsToQuery);
            
            //6. Get closed FP for selected AcademicYears.
            closedAY = familyPortalIsClosedForAcademicYear(allRelatedSPAS);
        }
        
        public MIERequestBulk(Id pfsId) {
            
            pfsIds = new Set<Id>{pfsId};
            
            //1. Get the list of ALL spa records related to the selected pfs 
            // (withou sharing: This means that we include spas from other schools). 
            //We need to run this query without sharing to be able to see spas from other schools.
            allRelatedSPAS = (new WithoutSharingQueryClass()).selectByPFSId(pfsIds, getSPAFieldsToQuery());
            
            //2. Get closed FP for selected AcademicYears.
            closedAY = familyPortalIsClosedForAcademicYear(allRelatedSPAS);
        }
        
        private List<String> getSPAFieldsToQuery() {
            
            List<String> fieldsToQuery = new List<String>();
            fieldsToQuery.add('Id');
            fieldsToQuery.add('Student_Folder__c');
            fieldsToQuery.add('School_ID__c');
            fieldsToQuery.add('PFS_ID__c');
            fieldsToQuery.add('PFS_MIE_Requested__c');
            fieldsToQuery.add('PFS_StatMonthlyIncomeExpenses__c');
            fieldsToQuery.add('Academic_Year_Name__c');
            fieldsToQuery.add('PFS_Academic_Year__c');
            fieldsToQuery.add('School_Name__c');  
            
            return fieldsToQuery;
        }
                
    }//End-Class:MIERequestBulk
    
    public class MIEResponseBulk {
        
        public Boolean runDML { get; set; }
        public String errorMessage { get; set; }
        public Set<String> closedAY { get; set; }

        public Set<Id> StudentFolderIds {
            get {
                if (StudentFolderIds == null) {
                    StudentFolderIds = new Set<Id>();
                }
                return StudentFolderIds;
            }
            private set;
        }

        public Integer NumberOfApplicants {
            get {
                return StudentFolderIds.size();
            }
        }

        public Set<Id> pfsRequestedIds { get; set; } //The set of PFS records selected throug its studentFolder.
        public Set<Id> pfsUpdatedIds { get; set; } //The set of PFS records for which we'll request MIE.
        public Set<Id> pfsAlreadyCompletedIds { get; set; } //The Id of PFS records that were not updated. Because, they have already completed MIE.
        public Set<Id> pfsRequestedAndIncompletedIds { get; set; } //The Id of PFS records that were not updated. Because, they already have request MIE. But, haven't completed it yet.
        public Set<Id> pfsClosedFamilyPortalIds { get; set; } //The set of PFS records with closed academicYear for Family Portal.
        
        public Integer pfsRequestedCounter {
            get {
                return pfsRequestedIds != null ? pfsRequestedIds.size() : 0;
            }
            
            private set;
        }
        
        public Integer pfsUpdatedCounter {
            get {
                return pfsUpdatedIds != null ? pfsUpdatedIds.size() : 0;
            }
            
            private set;
        }
        
        public Integer pfsAlreadyCompletedCounter {
            get {
                return pfsAlreadyCompletedIds != null ? pfsAlreadyCompletedIds.size() : 0;
            }
            
            private set;
        }
        
        public Integer pfsRequestedAndIncompletedCounter {
            get {
                return pfsRequestedAndIncompletedIds != null ? pfsRequestedAndIncompletedIds.size() : 0;
            }
            
            private set;
        }
        
        public Integer pfsClosedFamilyPortalCounter {
            get {
                return pfsClosedFamilyPortalIds != null ? pfsClosedFamilyPortalIds.size() : 0;
            }
            
            private set;
        }
        
        public MIEResponseBulk(MIERequestBulk request, Boolean shouldUpdate) {
            
            init(request, shouldUpdate, false);
        }
        
        public MIEResponseBulk(MIERequestBulk request, Boolean shouldUpdate, Boolean skipCloseAY) {
            
            init(request, shouldUpdate, skipCloseAY);
        }

        /**
         * @description Checks to see if the MIE information is available for the specified PFS. The MIE Step must be completed and the MIE feature must be enabled.
         */
        public Boolean isMIEInfoAvailable(Id pfsId) {
            return pfsAlreadyCompletedIds.contains(pfsId);
        }

        private void init(MIERequestBulk request, Boolean shouldUpdate, Boolean skipCloseAY) {
            
            runDML = shouldUpdate;

            this.StudentFolderIds = request.StudentFolderIds;

            pfsRequestedIds = request.pfsIds;
            closedAY = skipCloseAY ? request.closedAY : new Set<String>();
            pfsUpdatedIds = new Set<Id>();
            pfsAlreadyCompletedIds = new Set<Id>();
            pfsRequestedAndIncompletedIds = new Set<Id>();
            pfsClosedFamilyPortalIds = new Set<Id>();
            
            if (!SchoolPortalSettings.MIE_Pilot) {
                
                errorMessage = Label.MIE_Pilot_Flag_Disabled_Message;
                return;
            }
            
            
            Map<String, List<School_PFS_Assignment__c>> spasByAnnualSettingKeyToQuery = new Map<String, List<School_PFS_Assignment__c>>();
            List<School_PFS_Assignment__c> tmpSpas = new List<School_PFS_Assignment__c>();
            
            //1. Parse the spa records to determine which are:
            //      * The PFSs that has already completed the MIE step. (pfsAlreadyCompletedIds)
            //      * The PFSs that has already requested MIE. But, it haven't been completed by the parent. (pfsRequestedAndIncompletedIds)
            //      * The PFSs that need to be updated. It represents the PFSs for whcih we'll request MIE Indiviudally. (pfsUpdatedIds)
            //      * All the PFS related to the selected folders. (pfsRequestedIds)
            Boolean needUpdate;
            String mapkey;
            Set<Id> tmpSetId;
            Map<String, Set<Id>> mapKeysOfPFSThatAlreadyRequestedMIE = new Map<String, Set<Id>>();
            for (School_PFS_Assignment__c spa : request.allRelatedSPAS) {
                
                needUpdate = false;
                mapkey = Id.ValueOf(spa.School_ID__c) + ':' + spa.Academic_Year_Name__c;
                
                if (spa.PFS_MIE_Requested__c) {
                    
                    tmpSetId = mapKeysOfPFSThatAlreadyRequestedMIE.containsKey(mapkey) 
                        ? mapKeysOfPFSThatAlreadyRequestedMIE.get(mapkey) : new Set<Id>();
                    tmpSetId.add(spa.Id);
                    mapKeysOfPFSThatAlreadyRequestedMIE.put(mapkey, tmpSetId);
                    
                    if (spa.PFS_StatMonthlyIncomeExpenses__c) {
                        
                        //1.1. PFS with Individual MIE Requested and already completed.
                        pfsAlreadyCompletedIds.add(spa.PFS_ID__c);
                        
                    } else {
                        
                        //1.2. PFS already have an Individual MIE Requeste. But, it have not been completed.
                        pfsRequestedAndIncompletedIds.add(spa.PFS_ID__c);
                    }
                } else if (spa.PFS_StatMonthlyIncomeExpenses__c) {
                    
                    //1.3 At some point one of the selected schools required MIE. But, the AnnualSetting.Collect_MIE__c
                    //    was updated from "Yes" to "No". Even thoug, the MIE section of the PFS is marked as completed.
                    //     So, we won't allow to require MIE once again.
                    pfsAlreadyCompletedIds.add(spa.PFS_ID__c);
                } else {
                    
                    //1.4 Have not request MIE for this individual PFS. This means that we still need to query the related annual setting to verify if the MIE was required by the school.
                    tmpSpas = spasByAnnualSettingKeyToQuery.containsKey(mapkey) 
                        ? spasByAnnualSettingKeyToQuery.get(mapkey) : new List<School_PFS_Assignment__c>();
                    
                    tmpSpas.add(spa);
                    
                    spasByAnnualSettingKeyToQuery.put(mapkey, tmpSpas);
                }
                
                pfsRequestedIds.add(spa.PFS_ID__c);
            }
            
            Set<Id> spasWithMIE;
            List<School_PFS_Assignment__c> spasWithoutMIE;
            List<School_PFS_Assignment__c> spasWithoutMIEFinal;
            for (String s : mapKeysOfPFSThatAlreadyRequestedMIE.keySet()) {
                
                if (spasByAnnualSettingKeyToQuery.containsKey(s)) {
                    
	                spasWithMIE = mapKeysOfPFSThatAlreadyRequestedMIE.get(s);
	                spasWithoutMIE = spasByAnnualSettingKeyToQuery.get(s);
	                spasWithoutMIEFinal = new List<School_PFS_Assignment__c>();
	                
	                for (School_PFS_Assignment__c spa : spasWithoutMIE) {
	                    
	                    if (!spasWithMIE.contains(spa.Id)) {
	                        
	                        spasWithoutMIEFinal.add(spa);
	                    }
	                }
	                
	                if (spasWithoutMIEFinal.isEmpty()) {
	                    spasByAnnualSettingKeyToQuery.remove(s);
	                } else {
	                    spasByAnnualSettingKeyToQuery.put(s, spasWithoutMIEFinal);
	                }
                }
            }
            
            //2. Get the annualSetting records for those PFS with MIE_Requested__c=false. 
            //In order to verify if the annualSetting has Collect_MIE__c <> Yes.
            getPFSToRequestMIEAfterVerifyItsAnnualSetting(spasByAnnualSettingKeyToQuery);
            
            //3. Execute the DML that will update the PFS records.
            if (runDML) { 
                runDML();
            }
            
        }//End-Constructor
        
        private void runDML() {

            errorMessage = MonthlyIncomeAndExpensesService.requestMIEIndividuallyDML(pfsUpdatedIds);
            
        }//End:requestMIEForPFS
        
        private void getPFSToRequestMIEAfterVerifyItsAnnualSetting(Map<String, List<School_PFS_Assignment__c>> spasByAnnualSettingKeyToQuery) {
            if (spasByAnnualSettingKeyToQuery.isEmpty()) { return; }
            
            Map<Id, Boolean> result = new Map<Id, Boolean>();
            
            //1. Define the list of fields to query for annualSetting records.
            List<String> fieldsToQuery = new List<String>();
            fieldsToQuery.add('Id');
            fieldsToQuery.add('Collect_MIE__c');
            fieldsToQuery.add('School_Academic_Year_Picklist__c');
            
            //2. Query the related annualSetting records in order to determine if at least one of the selected schools had
            //already required MIE through annualSetting.Collect_MIE__c.
            List<Annual_Setting__c> annualSettings = (new WithoutSharingQueryClass())
                .selectBySchoolIdAcademicYearName(spasByAnnualSettingKeyToQuery.keySet(), fieldsToQuery);
            
            //3. Loop over the retrieved annualSetting records.
            for (Annual_Setting__c annualSetting : annualSettings) {
                
                //4. Loop over the spa records related to the given annualSetting. In order to determine which pfs records are
                //still need able to request MIE.
                
                for (School_PFS_Assignment__c spa : spasByAnnualSettingKeyToQuery.get(annualSetting.School_Academic_Year_Picklist__c)) {

                    //5. Initialize the PFS in the result map.
                    if (!result.containsKey(spa.PFS_ID__c)) {
                        
                        result.put(spa.PFS_ID__c, false);
                    }
                    
                    if (closedAY.contains(spa.PFS_Academic_Year__c)) {
                        
                        pfsClosedFamilyPortalIds.add(spa.PFS_ID__c);
                    }
                    
                    //6. Determine if even after verify the related annualSetting record. We are still able to request MIE
                    //   for the selected PFS records. Here are the conditions detailed:
                    //   !closedAY.contains(spa.PFS_Academic_Year__c): If the FP is closed for the given PFS.
                    //   !result.get(spa.PFS_ID__c): If the result flag have not been never set to true. AND
                    //   annualSetting.Collect_MIE__c = 'Yes': The school did collect MIE for that AY.
                    if (!closedAY.contains(spa.PFS_Academic_Year__c) && !result.get(spa.PFS_ID__c) && annualSetting.Collect_MIE__c == 'Yes') {
                        
                        result.put(spa.PFS_ID__c, true);
                        
                        //7. Keep track of the PFS records that has already completed the MIE step.
                        if (spa.PFS_StatMonthlyIncomeExpenses__c) {
                            
                            pfsAlreadyCompletedIds.add(spa.PFS_ID__c);
                        } else {
                            
                            pfsRequestedAndIncompletedIds.add(spa.PFS_ID__c);
                        }
                    }
                }
            }
            
            //8. Remove already completed PFSs from pfsRequestedAndIncompletedIds. 
            pfsRequestedAndIncompletedIds.removeAll(pfsAlreadyCompletedIds);
            
            for (Id i : result.keyset()) {
                
                if (!result.get(i) && !pfsClosedFamilyPortalIds.contains(i)) {
                    
                    //9. Add PFS to the Set of PFS ids for which we still can request MIE.
                    pfsUpdatedIds.add(i);
                }
            }
            
        }//End:getPFSToRequestMIEAfterVerifyItsAnnualSetting
    
    }//End-Class:MIEResponseBulk
    
    public class MonthlyIncomeAndExpensesServiceException extends Exception {}
}