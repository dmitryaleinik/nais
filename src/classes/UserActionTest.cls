@isTest
private class UserActionTest {
    
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    private static User sysAdminUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    private static User updatedUser;

    private static void createSchoolPortalSettings() {
        SchoolPortalSettings.Max_Number_of_Users = 2;
    }

    private static void createProfileTypeGrouping() {
        Profile_Type_Grouping__c profileTypeGrouping = new Profile_Type_Grouping__c();
        profileTypeGrouping.Name = 'School Portal Basic Administrator';
        profileTypeGrouping.Is_School_Profile__c = true;
        profileTypeGrouping.Profile_Name__c = ProfileSettings.SchoolBasicAdminProfileName;
        profileTypeGrouping.Is_School_Admin_Profile__c = true;
        profileTypeGrouping.Is_Basic_School_View__c = true;
        insert profileTypeGrouping;
    }

    // [CH] NAIS-1766 Testing sharing based on public groups
    @isTest
    private static void testPublicGroupSharing() {

        Id schoolProfileId = [SELECT Id FROM Profile WHERE Name = :ProfileSettings.SchoolUserProfileName LIMIT 1].Id;

        Profile_Type_Grouping__c ptg = new Profile_Type_Grouping__c();
        ptg.Name = 'School Portal User';
        ptg.Is_School_Profile__c = true;
        ptg.Profile_Name__c = ProfileSettings.SchoolUserProfileName;
        insert ptg;

        Account account1 = TestUtils.createAccount('Account 1', RecordTypes.schoolAccountTypeId, 1, false);
        Account account2 = TestUtils.createAccount('Account 1', RecordTypes.schoolAccountTypeId, 1, false);
        Account account3 = TestUtils.createAccount('Account 1', RecordTypes.schoolAccountTypeId, 1, false);
        Account account4 = TestUtils.createAccount('Account 1', RecordTypes.schoolAccountTypeId, 1, false);

        Database.insert(new List<Account>{account1, account2, account3, account4});

        Contact contact1 = TestUtils.createContact('Test User', account1.Id, RecordTypes.schoolStaffContactTypeId, false);
        Contact contact2 = TestUtils.createContact('Test User', account2.Id, RecordTypes.schoolStaffContactTypeId, false);
        Contact contact3 = TestUtils.createContact('Test User', account3.Id, RecordTypes.schoolStaffContactTypeId, false);
        Contact contact4 = TestUtils.createContact('Test User', account4.Id, RecordTypes.schoolStaffContactTypeId, false);

        Database.insert(new List<Contact>{contact1, contact2, contact3, contact4});

        Test.startTest();
        User user1 = TestUtils.createPortalUser('testuser1@testing.stuff', 'testuser1@testing.stuff', 'tst1', contact1.Id, schoolProfileId, true, false);
        User user2 = TestUtils.createPortalUser('testuser2@testing.stuff', 'testuser2@testing.stuff', 'tst2', contact2.Id, schoolProfileId, true, false);
        User user3 = TestUtils.createPortalUser('testuser3@testing.stuff', 'testuser3@testing.stuff', 'tst3', contact3.Id, schoolProfileId, true, false);
        User user4 = TestUtils.createPortalUser('testuser4@testing.stuff', 'testuser4@testing.stuff', 'tst4', contact4.Id, schoolProfileId, true, false);

        System.runAs(thisUser) {
            Database.insert(new List<User>{user1, user2, user3, user4});
        }

        // User re-query
        List<Id> userIdList = new List<Id>{user1.Id, user2.Id, user3.Id, user4.Id};
        Map<Id, User> usersMap = new Map<Id, User>([SELECT Id, AccountId FROM User WHERE Id in :userIdList ]);

        // Verify GroupMember creation
        Map<String, GroupMember> groupVerificationMap = new Map<String, GroupMember>();
        for( GroupMember member : [SELECT Id, UserOrGroupId, Group.DeveloperName FROM GroupMember WHERE UserOrGroupId IN :userIdList ]) {
            groupVerificationMap.put(member.UserOrGroupId + '-' + member.Group.DeveloperName, member);
        }

        System.assert(groupVerificationMap.get(user1.Id + '-' + 'X' + usersMap.get(user1.Id).AccountId) != null);
        System.assert(groupVerificationMap.get(user2.Id + '-' + 'X' + usersMap.get(user2.Id).AccountId) != null);
        System.assert(groupVerificationMap.get(user3.Id + '-' + 'X' + usersMap.get(user3.Id).AccountId) != null);
        System.assert(groupVerificationMap.get(user4.Id + '-' + 'X' + usersMap.get(user4.Id).AccountId) != null);

        // Deactivate a user and verify that their sharing is removed
        user1.IsActive = false;
        System.runAs(thisUser) {
            update user1;
        }

        System.assertEquals(0, [SELECT count() FROM GroupMember WHERE UserOrGroupId = :user1.Id AND Group.RelatedId = :user1.AccountId]);

        Test.stopTest();
    }

    /*
     * Spec-074 School Portal - Delegated User Administration; Req# R-109, R-343
     */
    @isTest
    private static void cannotInsertSchoolPortalUsersExceedingAccountLimit() {
        createSchoolPortalSettings();

        Id portalProfileId = [SELECT Id FROM Profile WHERE Name = :ProfileSettings.PartnerProfileName].Id;

        Account accountA = TestUtils.createAccount('Account A', RecordTypes.accessOrgAccountTypeId, null, true);    // inherit default FROM custom settings - 2 users max
        Account accountB = TestUtils.createAccount('Account B', RecordTypes.schoolAccountTypeId, 1, true);            // local override on Account to 1 user max

        Contact contactA1 = TestUtils.createContact('Contact A1', accountA.Id, RecordTypes.schoolStaffContactTypeId, true);
        Contact contactA2 = TestUtils.createContact('Contact A2', accountA.Id, RecordTypes.schoolStaffContactTypeId, true);
        Contact contactB1 = TestUtils.createContact('Contact B1', accountB.Id, RecordTypes.schoolStaffContactTypeId, true);
        Contact contactB2 = TestUtils.createContact('Contact B2', accountB.Id, RecordTypes.schoolStaffContactTypeId, true);

        User uA1 = TestUtils.createPortalUser('User A1', 'ua1@test.org', 'ua1', contactA1.Id, portalProfileId, true, false);
        User uA2 = TestUtils.createPortalUser('User A2', 'ua2@test.org', 'ua2', contactA2.Id, portalProfileId, true, false);
        User uB1 = TestUtils.createPortalUser('User B1', 'ub1@test.org', 'ub1', contactB1.Id, portalProfileId, true, false);
        User uB2 = TestUtils.createPortalUser('User B2', 'ub2@test.org', 'ub2', contactB2.Id, portalProfileId, true, false);

        Test.startTest();

        System.runAs(thisUser) {
            insert new List<User> { uA1, uB1 };        // 1 user left on accountA, 0 left on accountB
        }

        Database.SaveResult[] lsr = Database.insert(new List<User> { uA2, uB2 }, false);

        System.assertEquals(true, lsr[0].isSuccess());        // uA2
        System.assertEquals(false, lsr[1].isSuccess());        // uB2
        System.assert(lsr[1].getErrors()[0].getMessage().contains('Maximum number of portal users exceeded for this school account'));

        Test.stopTest();
    }

    // LIMIT does not apply to other account types - 'Individual'
    @isTest
    private static void nonschoolAccountNotSubjectToLimit() {
        createSchoolPortalSettings();

        Id portalProfileId = [SELECT Id FROM Profile WHERE Name = :ProfileSettings.PartnerProfileName].Id;

        Account accountA = TestUtils.createAccount('Account A', RecordTypes.schoolAccountTypeId, null, true);        // inherit default from custom settings - 2 users max
        Account accountB = TestUtils.createAccount('Account B', RecordTypes.individualAccountTypeId, 1, true);        // no limit on non-school Account

        Contact contactA1 = TestUtils.createContact('Contact A1', accountA.Id, RecordTypes.schoolStaffContactTypeId, true);
        Contact contactA2 = TestUtils.createContact('Contact A2', accountA.Id, RecordTypes.schoolStaffContactTypeId, true);
        Contact contactB1 = TestUtils.createContact('Contact B1', accountB.Id, RecordTypes.parentContactTypeId, true);
        Contact contactB2 = TestUtils.createContact('Contact B2', accountB.Id, RecordTypes.studentContactTypeId, true);
        Contact contactB3 = TestUtils.createContact('Contact B3', accountB.Id, RecordTypes.parentContactTypeId, true);

        User uA1 = TestUtils.createPortalUser('User A1', 'ua1@test.org', 'ua1', contactA1.Id, portalProfileId, true, false);
        User uA2 = TestUtils.createPortalUser('User A2', 'ua2@test.org', 'ua2', contactA2.Id, portalProfileId, true, false);
        User uB1 = TestUtils.createPortalUser('User B1', 'ub1@test.org', 'ub1', contactB1.Id, portalProfileId, true, false);
        User uB2 = TestUtils.createPortalUser('User B2', 'ub2@test.org', 'ub2', contactB2.Id, portalProfileId, true, false);
        User uB3 = TestUtils.createPortalUser('User B3', 'ub3@test.org', 'ub3', contactB3.Id, portalProfileId, true, false);

        Test.startTest();

        System.runAs(thisUser) {
            insert new List<User> { uA1, uB1, uB2 };        // 1 user left on accountA, no limit on non-school accountB
        }

        Database.SaveResult[] lsr = Database.insert(new List<User> { uA2, uB3 }, false);

        System.assertEquals(true, lsr[0].isSuccess());        // uA2
        System.assertEquals(true, lsr[1].isSuccess());        // uB3

        Test.stopTest();
    }

    // LIMIT does not apply to non-portal users
    @isTest
    private static void nonportalUsersNotCountedInAccountLimit() {
        createSchoolPortalSettings();

        Id portalProfileId = [SELECT Id FROM Profile WHERE Name = :ProfileSettings.PartnerProfileName].Id;
        Id platformProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id;

        Account accountA = TestUtils.createAccount('Account A', RecordTypes.accessOrgAccountTypeId, null, true);    // inherit default from custom settings - 2 users max
        Account accountB = TestUtils.createAccount('Account B', RecordTypes.schoolAccountTypeId, 1, true);            // local override on Account to 1 user max

        Contact contactA1 = TestUtils.createContact('Contact A1', accountA.Id, RecordTypes.schoolStaffContactTypeId, true);
        Contact contactA2 = TestUtils.createContact('Contact A2', accountA.Id, RecordTypes.schoolStaffContactTypeId, true);
        Contact contactB1 = TestUtils.createContact('Contact B1', accountB.Id, RecordTypes.schoolStaffContactTypeId, true);
        Contact contactB2 = TestUtils.createContact('Contact B2', accountB.Id, RecordTypes.schoolStaffContactTypeId, true);

        User uA1 = TestUtils.createPortalUser('User A1', 'ua1@test.org', 'ua1', contactA1.Id, portalProfileId, true, false);
        User uA2 = TestUtils.createPortalUser('User A2', 'ua2@test.org', 'ua2', contactA2.Id, portalProfileId, true, false);
        User uB1 = TestUtils.createPlatformUser('User B1', 'ub1@test.org', 'ub1', platformProfileId, true, false);
        User uB2 = TestUtils.createPortalUser('User B2', 'ub2@test.org', 'ub2', contactB2.Id, portalProfileId, true, false);

        Test.startTest();

        System.runAs(thisUser) {
            insert new List<User> { uA1, uB1 };        // 1 user left on accountA, 1 left on accountB (regular SF user uB1 does not count against limit)
        }

        Database.SaveResult[] lsr = Database.insert(new List<User> { uA2, uB2 }, false);

        System.assertEquals(true, lsr[0].isSuccess());        // uA2
        System.assertEquals(true, lsr[1].isSuccess());        // uB2

        Test.stopTest();
    }

    // LIMIT does not apply to inactive users
    @isTest
    private static void inactiveUsersNotCountedInAccountLIMIT() {
        createSchoolPortalSettings();

        Id portalProfileId = [SELECT Id FROM Profile WHERE Name = :ProfileSettings.PartnerProfileName].Id;

        Account accountA = TestUtils.createAccount('Account A', RecordTypes.accessOrgAccountTypeId, null, true);    // inherit default from custom settings - 2 users max
        Account accountB = TestUtils.createAccount('Account B', RecordTypes.schoolAccountTypeId, 1, true);            // local override on Account to 1 user max

        Contact contactA1 = TestUtils.createContact('Contact A1', accountA.Id, RecordTypes.schoolStaffContactTypeId, true);
        Contact contactA2 = TestUtils.createContact('Contact A2', accountA.Id, RecordTypes.schoolStaffContactTypeId, true);
        Contact contactB1 = TestUtils.createContact('Contact B1', accountB.Id, RecordTypes.schoolStaffContactTypeId, true);
        Contact contactB2 = TestUtils.createContact('Contact B2', accountB.Id, RecordTypes.schoolStaffContactTypeId, true);

        User uA1 = TestUtils.createPortalUser('User A1', 'ua1@test.org', 'ua1', contactA1.Id, portalProfileId, true, false);
        User uA2 = TestUtils.createPortalUser('User A2', 'ua2@test.org', 'ua2', contactA2.Id, portalProfileId, true, false);
        User uB1 = TestUtils.createPortalUser('User B1', 'ub1@test.org', 'ub1', contactB1.Id, portalProfileId, false, false);
        User uB2 = TestUtils.createPortalUser('User B2', 'ub2@test.org', 'ub2', contactB2.Id, portalProfileId, true, false);

        Test.startTest();

        System.runAs(thisUser) {
            insert new List<User> { uA1, uB1 };        // 1 user left on accountA, 1 left on accountB (inactive uB1 does not count against limit)
        }

        Database.SaveResult[] lsr = Database.insert(new List<User> { uA2, uB2 }, false);

        System.assertEquals(true, lsr[0].isSuccess());        // uA2
        System.assertEquals(true, lsr[1].isSuccess());        // uB2

        Test.stopTest();
    }

    // LIMIT applies to re-activating inactive users
    @isTest
    private static void cannotActivateUsersExceedingAccountLimit() {
        createSchoolPortalSettings();

        Id portalProfileId = [SELECT Id FROM Profile WHERE Name = :ProfileSettings.PartnerProfileName].Id;

        Account accountA = TestUtils.createAccount('Account A', RecordTypes.accessOrgAccountTypeId, null, true);    // inherit default from custom settings - 2 users max
        Account accountB = TestUtils.createAccount('Account B', RecordTypes.schoolAccountTypeId, 1, true);            // local override on Account to 1 user max

        Contact contactA1 = TestUtils.createContact('Contact A1', accountA.Id, RecordTypes.schoolStaffContactTypeId, true);
        Contact contactA2 = TestUtils.createContact('Contact A2', accountA.Id, RecordTypes.schoolStaffContactTypeId, true);
        Contact contactB1 = TestUtils.createContact('Contact B1', accountB.Id, RecordTypes.schoolStaffContactTypeId, true);
        Contact contactB2 = TestUtils.createContact('Contact B2', accountB.Id, RecordTypes.schoolStaffContactTypeId, true);

        User uA1 = TestUtils.createPortalUser('User A1', 'ua1@test.org', 'ua1', contactA1.Id, portalProfileId, true, false);
        User uA2 = TestUtils.createPortalUser('User A2', 'ua2@test.org', 'ua2', contactA2.Id, portalProfileId, true, false);
        User uB1 = TestUtils.createPortalUser('User B1', 'ub1@test.org', 'ub1', contactB1.Id, portalProfileId, false, false);
        User uB2 = TestUtils.createPortalUser('User B2', 'ub2@test.org', 'ub2', contactB2.Id, portalProfileId, true, false);

        Test.startTest();

        System.runAs(thisUser) {
            insert new List<User> { uA1, uB1, uA2, uB2 };        // no user left on accountA or AccountB
        }

        uB1.IsActive = true;

        Database.SaveResult[] lsr = Database.update(new List<User> { uB1 }, false);

        System.assertEquals(false, lsr[0].isSuccess());

        Test.stopTest();
    }

/*
* SPEC-108: School Portal - Feature Version, R-160, R-159, 60 hours [Drew]
* For schools which have the full feature view, it is as speced. For schools which have the limited feature view,
* they will need to have a different nav so they won't be able to see most of the admin functions.
* The NAIS Administrator should be able to set at a Global level whether their school sees the full or basic version.
*
* DP Exponent Partners, 2013
*/

    @isTest
    private static void testSingleUserPortalVersionChange() {
        User portalUser;

        System.runAs(sysAdminUser) {
            createSchoolPortalSettings();
            Account school1 = AccountTestData.Instance
                .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                .forSSSSubscriptionTypeCurrent(SubscriptionTypes.SUBSCRIPTION_FULL_TYPE).DefaultAccount;

            portalUser = UserTestData.insertSchoolPortalUser();
            System.assertEquals(GlobalVariables.schoolPortalAdminProfileId, portalUser.ProfileId);

            Test.startTest();
                school1.SSS_Subscription_Type_Current__c = SubscriptionTypes.BASIC_TYPE;

                update school1;
            Test.stopTest();
        }

        updatedUser = UserSelector.newInstance()
            .selectByIdWithCustomFieldList(new Set<Id>{portalUser.Id}, new List<String>{'ProfileId'})[0];
        System.assertEquals(GlobalVariables.schoolPortalBasicAdminProfileId, updatedUser.ProfileId);
    }

    @isTest
    private static void testSingleUserPortalVersionChange2() {
        User portalUser;

        System.runAs(sysAdminUser) {
            createProfileTypeGrouping();
            Account school = AccountTestData.Instance
                .forRecordTypeId(RecordTypes.schoolAccountTypeId).DefaultAccount;
            portalUser = UserTestData.insertSchoolPortalUser();

            updatedUser = UserSelector.newInstance()
                .selectByIdWithCustomFieldList(new Set<Id>{portalUser.Id}, new List<String>{'ProfileId', 'Profile.Name'})[0];
            updatedUser.ProfileId = GlobalVariables.schoolPortalBasicAdminProfileId;

            update updatedUser;
            System.assertEquals(GlobalVariables.schoolPortalBasicAdminProfileId, updatedUser.ProfileId);

            Test.startTest();
                school.SSS_Subscription_Type_Current__c = SubscriptionTypes.SUBSCRIPTION_FULL_TYPE;
                update school;
            Test.stopTest();
        }

        updatedUser = UserSelector.newInstance()
                .selectByIdWithCustomFieldList(new Set<Id>{portalUser.Id}, new List<String>{'ProfileId'})[0];
        System.assertEquals(GlobalVariables.schoolPortalAdminProfileId, updatedUser.ProfileId);
    }

    @isTest
    private static void testMultipleUserPortalVersionChange() {
        List<User> updatedUsers;

        System.runAs(sysAdminUser) {
            Account user1Account = TestUtils.createAccount('user1Account', RecordTypes.schoolAccountTypeId, 3, false);
            Account user2Account = TestUtils.createAccount('user2Account', RecordTypes.schoolAccountTypeId, 3, false);
            Account user3Account = TestUtils.createAccount('user3Account', RecordTypes.schoolAccountTypeId, 3, false);
            insert new List<Account> {user1Account, user2Account, user3Account};

            Contact con1 = TestUtils.createContact('user1Con', user1Account.Id, RecordTypes.schoolStaffContactTypeId, false);
            Contact con2 = TestUtils.createContact('user2Con', user2Account.Id, RecordTypes.schoolStaffContactTypeId, false);
            Contact con3 = TestUtils.createContact('user3Con', user3Account.Id, RecordTypes.schoolStaffContactTypeId, false);
            insert new List<Contact> {con1, con2, con3};

            User user1 = TestUtils.createPortalUser('user1', 'user1@email.email', 'user1', con1.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);
            User user2 = TestUtils.createPortalUser('user2', 'user2@email.email', 'user2', con2.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);
            User user3 = TestUtils.createPortalUser('user3', 'user3@email.email', 'user3', con3.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);
            updatedUsers = new List<User> {user1, user2, user3};
            insert updatedUsers;

            List<Account> tDataAccounts = new List<Account> {user1Account, user2Account, user3Account};

            updatedUsers = [SELECT Id, ProfileId FROM User WHERE Id in :updatedUsers];
            for (User u : updatedUsers) {
                System.assertEquals(GlobalVariables.schoolPortalAdminProfileId, u.ProfileId);
            }

            Test.startTest();

            for (Account a : tDataAccounts) {
                a.SSS_Subscription_Type_Current__c = SubscriptionTypes.BASIC_TYPE;
            }
            update tDataAccounts;

            Test.stopTest();
        }

        updatedUsers = [SELECT Id, ProfileId FROM User WHERE Id IN :updatedUsers];
        for (User u : updatedUsers) {
            System.assertEquals(GlobalVariables.schoolPortalBasicAdminProfileId, u.ProfileId);
        }
    }

    @isTest
    private static void testMultipleUserPortalVersionChange2() {
        List<User> updatedUsers;

        System.runAs(sysAdminUser) {
            Account user1Account = TestUtils.createAccount('user1Account', RecordTypes.schoolAccountTypeId, 3, false);
            Account user2Account = TestUtils.createAccount('user2Account', RecordTypes.schoolAccountTypeId, 3, false);
            Account user3Account = TestUtils.createAccount('user3Account', RecordTypes.schoolAccountTypeId, 3, false);
            insert new List<Account> {user1Account, user2Account, user3Account};

            Contact con1 = TestUtils.createContact('user1Con', user1Account.Id, RecordTypes.schoolStaffContactTypeId, false);
            Contact con2 = TestUtils.createContact('user2Con', user2Account.Id, RecordTypes.schoolStaffContactTypeId, false);
            Contact con3 = TestUtils.createContact('user3Con', user3Account.Id, RecordTypes.schoolStaffContactTypeId, false);
            insert new List<Contact> {con1, con2, con3};

            User user1 = TestUtils.createPortalUser('user1', 'user1@email.email', 'user1', con1.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);
            User user2 = TestUtils.createPortalUser('user2', 'user2@email.email', 'user2', con2.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);
            User user3 = TestUtils.createPortalUser('user3', 'user3@email.email', 'user3', con3.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);
            updatedUsers = new List<User> {user1, user2, user3};
            insert updatedUsers;

            List<Account> tDataAccounts = new List<Account> {user1Account, user2Account, user3Account};

            updatedUsers = [SELECT Id, ProfileId FROM User WHERE Id IN :updatedUsers];
            for (User u : updatedUsers) {
                System.assertEquals(GlobalVariables.schoolPortalAdminProfileId, u.ProfileId);
            }

            Test.startTest();

            for (Account a : tDataAccounts) {
                a.Portal_Version__c = 'Full-Featured';
            }
            update tDataAccounts;

            Test.stopTest();
        }

        updatedUsers = [SELECT Id, ProfileId FROM User WHERE Id IN :updatedUsers];
        for (User u : updatedUsers) {
            System.assertEquals(GlobalVariables.schoolPortalAdminProfileId, u.ProfileId);
        }
    }

    @isTest
    private static void testEmailXML() {
        SpringCMActionTest.setupCustomSettings();

        Map<String, String> newToOldEmail = new Map<String, String>();
        newToOldEmail.put('new1@new.new', 'old1@old.old');
        newToOldEmail.put('new2@new.new', 'old2@old.old');

        String xmlOutput = UserAction.writeEmailXML(newToOldEmail);
        //String expectedString1 = '<?xml version="1.0"?><SFData><FamilyDocument><Id>' + String.valueOf(famDoc1a.Id).left(15) + '</Id><Name>' + famDoc1a.Name + '</Name><Year>' + famDoc1a.Document_Year__c + '</Year></FamilyDocument><Household><Id>' + String.valueOf(hh1.Id).left(15) + '</Id><Name>' + 'house &amp; hold' + '</Name></Household><ImportID>' + famDoc1a.Import_Id__c + '</ImportID></SFData><SFData><FamilyDocument><Id>' + String.valueOf(famDoc2.Id).left(15) + '</Id><Name>' + famDoc2.Name + '</Name><Year>' + famDoc2.Document_Year__c + '</Year></FamilyDocument><Household><Id>' + String.valueOf(hh2.Id).left(15) + '</Id><Name>' + hh2.Name + '</Name></Household><ImportID>' + famDoc2.Import_Id__c + '</ImportID></SFData>';
        String expectedString1 = '<?xml version="1.0"?><SFData><OldUserEmail>old1@old.old</OldUserEmail><NewUserEmail>new1@new.new</NewUserEmail></SFData><SFData><OldUserEmail>old2@old.old</OldUserEmail><NewUserEmail>new2@new.new</NewUserEmail></SFData>';
        String expectedString2 = '<?xml version="1.0"?><SFData><OldUserEmail>old2@old.old</OldUserEmail><NewUserEmail>new2@new.new</NewUserEmail></SFData><SFData><OldUserEmail>old1@old.old</OldUserEmail><NewUserEmail>new1@new.new</NewUserEmail></SFData>';

        // should be one of these two (maps are unordered)
        System.assert(expectedString1 == xmlOutput || expectedString2 == xmlOutput);
        System.debug('TESTING380 ' + xmlOutput);
    }

    @isTest
    private static void testSparkSpringCMWithEmails() {
         SpringCMActionTest.setupCustomSettings();
         SpringCMAction.isTest = true;

        Map<String, String> newToOldEmail = new Map<String, String>();
        newToOldEmail.put('new1@new.new', 'old1@old.old');
        newToOldEmail.put('new2@new.new', 'old2@old.old');
        newToOldEmail.put('new3@new.new', 'old3@old.old');

        Test.startTest();
        UserAction.sendEmailUpdateXMLToSpring(newToOldEmail, UserInfo.getSessionId());
        Test.stopTest();

        System.assertEquals('testsuccessful', UserAction.workflowResult);
    }

    @isTest
    private static void testSparkSpringCMWithEmailsOnTrigger() {
         SpringCMActionTest.setupCustomSettings();
         SpringCMAction.isTest = true;

        Account testA = TestUtils.createAccount('TestMe', RecordTypes.individualAccountTypeId, 10, true);

        Contact c1 = TestUtils.createContact('c1', testA.Id, RecordTypes.parentContactTypeId, false);
        Contact c2 = TestUtils.createContact('c1', testA.Id, RecordTypes.parentContactTypeId, false);
        Contact c3 = TestUtils.createContact('c1', testA.Id, RecordTypes.parentContactTypeId, false);

        c1.Email = 'old1@old.old';
        c2.Email = 'old2@old.old';
        c3.Email = 'old3@old.old';

        insert new List<Contact> {c1, c2, c3};

        Test.startTest();
        c1.Email = 'new1@new.new';
        c2.Email = 'new2@new.new';
        c3.Email = 'new3@new.new';
        update new List<Contact> {c1, c2, c3};
        Test.stopTest();
    }

    // NAIS-2476 Test update of Account preferences on SSS_Main_Contact__c [jB]
    @isTest
    private static void testChangePfsAlertPrefOnMainContact() {
        Id schoolProfileId = [SELECT Id FROM Profile WHERE Name = :ProfileSettings.SchoolUserProfileName LIMIT 1].Id;

        Profile_Type_Grouping__c ptg = new Profile_Type_Grouping__c();
        ptg.Name = 'School Portal User';
        ptg.Is_School_Profile__c = true;
        ptg.Profile_Name__c = ProfileSettings.SchoolUserProfileName;
        insert ptg;

        Account account1 = TestUtils.createAccount('Account 1', RecordTypes.schoolAccountTypeId, 1, false);

        Database.insert(new List<Account>{account1});

        Contact contact1 = TestUtils.createContact('Test User', account1.Id, RecordTypes.schoolStaffContactTypeId, false);
        Contact contact2 = TestUtils.createContact('Test User', account1.Id, RecordTypes.schoolStaffContactTypeId, false);

        Database.insert(new List<Contact>{contact1, contact2});

        Test.startTest();

        System.assert(contact1.PFS_Alert_Preferences__c == null);
        System.assert(account1.Alert_Frequency__c == null);

        // set contact1 to be main account user & set not to recieve alerts
        contact1.SSS_Main_Contact__c = true;
        contact1.PFS_Alert_Preferences__c = 'Do Not Receive Alerts';
        update contact1;

        // make sure they're the main contact
        contact1 = [SELECT Id, PFS_Alert_Preferences__c, SSS_Main_Contact__c FROM Contact WHERE AccountId = :account1.id
                        AND SSS_Main_Contact__c = true];

        System.assert(contact1 != null);

        System.assert(contact1.PFS_Alert_Preferences__c == 'Do Not Receive Alerts');
        System.assert(contact1.SSS_Main_Contact__c);

        // update the account alert preferences
        account1.Alert_Frequency__c = 'No Alerts';
        update account1;

        // ensure it changed on the account - not on contact
        account1 = [SELECT Alert_Frequency__c FROM Account WHERE Id = :account1.id];
        System.assert(account1.Alert_Frequency__c == 'No Alerts');

        contact1 = [SELECT PFS_Alert_Preferences__c FROM Contact WHERE Id = :contact1.Id];
        System.assert(contact1.PFS_Alert_Preferences__c == 'Do Not Receive Alerts');

        account1.Alert_Frequency__c = 'Daily digest of new applications';
        update account1;

        // test that it changed
        contact1 = [SELECT PFS_Alert_Preferences__c FROM Contact WHERE Id = :contact1.Id];
        System.assert(contact1.PFS_Alert_Preferences__c == 'Receive Alerts');

        account1.Alert_Frequency__c ='No Alerts';
        update account1;

        // test that it changed back
        contact1 = [SELECT PFS_Alert_Preferences__c FROM Contact WHERE Id = :contact1.Id];
        System.assert(contact1.PFS_Alert_Preferences__c == 'Do Not Receive Alerts');

        account1.Alert_Frequency__c ='Weekly digest of new applications';
        update account1;

        // and again
        contact1 = [SELECT PFS_Alert_Preferences__c FROM Contact WHERE Id = :contact1.Id];
        System.assert(contact1.PFS_Alert_Preferences__c == 'Receive Alerts');

        // and check to make sure other contacts didn't get changed
        contact2 = [SELECT PFS_Alert_Preferences__c FROM Contact WHERE Id = :contact2.Id];
        System.assert(contact2.PFS_Alert_Preferences__c == null);

        Test.stopTest();
    }
}