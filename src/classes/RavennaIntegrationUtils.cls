/**
 * @description This class is used to assist with certain functions needed when processing the integration with Ravenna
 *              which pulls in family/applicant data when families pull their ravenna applications into SSS. Uses include:
 *                  - Syncing ethnicity information.
 */
public class RavennaIntegrationUtils {

    private static final String API_STATE_PARAM = 'apiState';

    // The value that will come in from ravenna when a family does not want to include their ethnicity.
    // Test visible so that this can be used when creating data for unit tests.
    @testVisible private static final String DECLINE_TO_ANSWER = 'Decline to answer';
    // The name of the attribute on the ravenna response which contains ethnicity data.
    @testVisible private static final String RAVENNA_ETHNICITY_FIELD = 'ethnicity';
    @testVisible private static final String ETHNICITY_DELIMITER = ',';
    @testVisible private static final String MULTIRACIAL_SSS = 'Multiracial';

    @testVisible private static final String APPLY_GRADE_FIELD = 'apply_grade';
    @testVisible private static final String CURRENT_GRADE_FIELD = 'current_grade';
    @testVisible private static final String PS_SOURCE = 'PS';
    @testVisible private static final String PK_SOURCE = 'PK';
    @testVisible private static final String K_SOURCE = 'K';
    @testVisible private static final String PG_SOURCE = 'PG';
    @testVisible private static final String PRESCHOOL_TYPE = 'Preschool';
    @testVisible private static final String PRE_K_TYPE = 'Pre-Kindergarten';
    @testVisible private static final String PRE_K_TYPE_CURRENT_GRADE = 'Pre-K';
    @testVisible private static final String KINDERGARTEN_TYPE = 'Kindergarten';
    @testVisible private static final String POST_GRAD_TYPE = 'Post Graduation';
    
    private static final Map<String, String> GENDER_VALUES = new Map<String, String>{'F' => 'Female',
																			         'M' => 'Male'};

    // A map of ethnicity values where they keys are ravenna ethnicities and the values represent the corresponding value in SSS.
    private static final Map<String, String> ETHNICITY_BY_RAVENNA_VALUE = new Map<String, String> {
            'Native American' => 'American Indian',
            'African American' => 'African American',
            'Asian American' => 'Asian American',
            'Pacific Islander American' => 'Pacific Islander',
            'International' => 'International',
            'Latino/Hispanic American' => 'Latino/Hispanic American',
            'Middle Eastern American' => 'Middle Eastern',
            'Multiracial American' => MULTIRACIAL_SSS,
            'European American (Caucasian)' => 'Caucasian',
            'Unsure' => 'Unsure'
    };

    /**
     * @description: Converts a grade value from the ravenna system into one that SSS accepts. For example, ravenna uses
     *              PK for "Pre-K" while SSS uses Pre-K and Pre-Kindergarten. The SSS value to use depends on the ravenna
     *              field and where it gets stored in SSS.
     *
     * @param String fieldName: field name to check.
     *
     * @param String value: value to validate against
     *
     * @return retuns a converted string. If no values match just return the value param.
     */
    public static String convertGradeToPicklist(String fieldName, String value) {
        String returnVal = value;

        if (value.equalsIgnoreCase(PS_SOURCE)) {
            returnVal = PRESCHOOL_TYPE;
        } else if (value.equalsIgnoreCase(PK_SOURCE)) {
            // SSS uses two different values for Pre-K. The current grade field needs to use Pre-K and the other fields use Pre-Kindergarten.
            returnVal = fieldName.equalsIgnoreCase(CURRENT_GRADE_FIELD) ? PRE_K_TYPE_CURRENT_GRADE : PRE_K_TYPE;
        } else if (value.equalsIgnoreCase(K_SOURCE)) {

            returnVal = KINDERGARTEN_TYPE;
        } else if (value.equalsIgnoreCase(PG_SOURCE)) {

            returnVal = POST_GRAD_TYPE;
        }

        return returnVal;
    }

    /**
     * @description Updates certain fields on contact records for the Applicant records that were upserted by the Ravenna Integration.
     *              This is done by querying the applicant records along with ravenna Ids and ethnicity information from
     *              the related contact record. The related contacts will always have the ravenna Id field overridden by
     *              the applicant record. Contact Ethnicity information is only updated if the contact does not have ethnicity
     *              specified and the ethnicity data obtained from ravenna corresponds to a value in our system.
     *              The ethnicity information is obtained from the original response from the ravenna student API. We identify
     *              the response to pull data from by using the ravenna Id on the queried applicant records.
     *
     *              This method relies on the IntegrationApiModel.ApiState having familyModel.applicantsToUpsert
     *              populated by IntegrationUtils.handleIntegrationDML(). Therefore, that method should be called first.
     *
     *              This is currently being handled by the IntegrationApiModel.IAction classes specified on the Integration
     *              Api Setting custom metadata record named Ravenna - Schools. This method is called by the IAction class
     *              named RavennaUpdateContacts. The RavennaUpdateContacts class is listed after IntegrationFinalize.
     * @param apiState The API State that contains the responses from the callouts made by the integration and the applicant
     *                 records we need to query. The Api State is passed through all Pre/Post Process classes configured
     *                 for the integration.
     * @return A Map of the contacts that were updated by Id.
     * @throws ArgumentNullException if apiState is null.
     */
    public static Map<Id, Contact> syncContacts(IntegrationApiModel.ApiState apiState) {
        ArgumentNullException.throwIfNull(apiState, API_STATE_PARAM);

        Map<Id, Contact> contactsToUpdateById = new Map<Id, Contact>();

        // If we don't have the applicants data we need, don't do any DML and return empty map.
        if (apiState.familyModel.applicantsToUpsert == null || apiState.familyModel.applicantsToUpsert.isEmpty()) {
            return contactsToUpdateById;
        }

        // Cast the list of applicants that were upserted in a previous step.
        // We will use this list to query the related student contact info, and ravenna Id.
        // The ravenna Id will be used to get the student response from ravenna which contains the ethnicity info.
        List<Applicant__c> applicants = getApplicantsWithRelatedStudent((List<Applicant__c>)apiState.familyModel.applicantsToUpsert);

        // For each applicant, we need to get the student response by the applicant ravenna Id. Each response should contain the ethnicity information.
        // There may be multiple responses for each ravenna Id so we need to handle that scenario.
        for (Applicant__c applicant : applicants) {
            Contact contactToUpdate = getContactToUpdate(applicant, apiState);

            contactsToUpdateById.put(contactToUpdate.Id, contactToUpdate);
        }

        if (!contactsToUpdateById.isEmpty()) {
            update contactsToUpdateById.values();
        }

        return contactsToUpdateById;
    }

    private static Contact getContactToUpdate(Applicant__c applicant, IntegrationApiModel.ApiState apiState) {
        Contact contactToUpdate = new Contact(Id = applicant.Contact__c);

        // We check if we should transfer ethnicity over to the contact. If so, we return the contact record to update.
        String ravennaEthnicity = getEthnicityFromAllRavennaResponses(applicant.RavennaId__c, apiState);

        if (shouldTransferEthnicity(applicant.Contact__r, ravennaEthnicity)) {
            // Now that we know we should transfer the ravenna ethnicity, we can get the corresponding SSS value.
            // Ethnicity in ravenna is a multiselect. The values returned by the ravenna API are comma separated.
            // We will split the values and if we have more than 1 we default to multiracial.
            // If there is only one value, we map the ravenna value to what we have in SSS.
            // This is done by grabbing the SSS value from a map where the ravenna values are the keys.
            String sssEthnicityValue = getSSSEthnicity(ravennaEthnicity);

            // Only update the Ethnicity field if we actually have a corresponding SSS Ethnicity.
            if (String.isNotBlank(sssEthnicityValue)) {
                contactToUpdate.Ethnicity__c = sssEthnicityValue;
            }
        }

        // Transfer ravenna Id from applicant to contact. This will let us provide the ravenna Id in the financial aid
        // API since student folders are only linked to the student contact record not the applicant record.
        contactToUpdate.RavennaId__c = applicant.RavennaId__c;

        return contactToUpdate;
    }

    /**
     * @description Gets all responses for a particular student by ravenna Id. We store a list of responses for each
     *              student Id so it may be possible that there are multiple responses per ravenna Id. This method
     *              iterates over each response until we get an ethnicity value.
     */
    private static String getEthnicityFromAllRavennaResponses(String studentRavennaId, IntegrationApiModel.ApiState apiState) {
        List<Object> studentRavennaResponses = apiState.familyModel.studentIdToResponseMap.get(studentRavennaId);

        // If we don't have a response for the specified student, return null.
        // A null value will tell us not to update the ethnicity.
        if (studentRavennaResponses == null || studentRavennaResponses.isEmpty()) {
            return null;
        }

        for (Object response : studentRavennaResponses) {
            String ethnicityFromRavenna = getEthnicityFromSingleStudentResponse(response);

            // Return the first non null/blank value we find.
            if (String.isNotBlank(ethnicityFromRavenna)) {
                return ethnicityFromRavenna;
            }
        }

        return null;
    }

    /**
     * @description This method handles retrieving an ethnicity value from a single student response. We store API responses
     *              as a generic object in the ApiState. For students we expect the response to be Map<String, Object>.
     *              This method will return null if the response is some other type. Otherwise, we get the value stored
     *              in that map using the name of the ravenna ethnicity attribute.
     */
    private static String getEthnicityFromSingleStudentResponse(Object studentResponse) {
        // First try casting this response to Map<String, Object> which is the expected format of the student responses we receive from ravenna.
        // If it isn't return a null ethnicity value.
        if (!(studentResponse instanceof Map<String, Object>)) {
            return null;
        }

        Map<String, Object> ravennaStudentData = (Map<String, Object>)studentResponse;

        return (String)ravennaStudentData.get(RAVENNA_ETHNICITY_FIELD);
    }

    private static List<Applicant__c> getApplicantsWithRelatedStudent(List<Applicant__c> applicantsToQuery) {
        Set<Id> applicantIds = new Map<Id, Applicant__c>(applicantsToQuery).keySet();

        return [SELECT Id, RavennaId__c, Contact__c, Contact__r.Ethnicity__c, Contact__r.RavennaId__c FROM Applicant__c WHERE Id IN :applicantIds];
    }

    private static Boolean shouldTransferEthnicity(Contact contactToSync, String ravennaEthnicity) {
        // If our student contact record already has ethnicity specified, don't override it.

        // We only want to transfer the ethnicity if the contact doesn't already have it specified and we get data from ravenna.
        // In the ravenna UI, if a family selects 'Decline to answer', all other ethnicity fields are cleared out so we
        // don't need to worry about splitting the ravenna value and searching for this value in the list in this situation.
        return String.isBlank(contactToSync.Ethnicity__c) && String.isNotBlank(ravennaEthnicity) && !DECLINE_TO_ANSWER.equalsIgnoreCase(ravennaEthnicity);
    }

    private static List<String> splitRavennaEthnicity(String ravennaEthnicity) {
        if (String.isBlank(ravennaEthnicity)) {
            return new List<String>();
        }

        return ravennaEthnicity.split(ETHNICITY_DELIMITER);
    }

    private static String getSSSEthnicity(String ravennaEthnicity) {
        // First split up the value from ravenna since they allow multiple ethnicity selections.
        List<String> ethnicityValues = splitRavennaEthnicity(ravennaEthnicity);

        if (ethnicityValues.isEmpty()) {
            return null;
        }

        // If there are multiple ethnicities selected, return multiracial.
        // We split the ethnicity value from the ravenna API by comma. If we end up with more than one result we consider it multiracial.
        // This is important because ravenna also has an 'Other Ethnicities' text field where parents can enter whatever they want.
        // This value simply gets included in the normal ethnicity attribute from the ravenna API.
        if (ethnicityValues.size() > 1) {
            return MULTIRACIAL_SSS;
        }

        // If there is only 1 ethnicity selected in ravenna, grab the corresponding SSS value.
        return ETHNICITY_BY_RAVENNA_VALUE.get(ethnicityValues[0]);
    }
    
    public static String convertGenderToPicklist(String value) {
        
        return GENDER_VALUES.get(value);
    }
}