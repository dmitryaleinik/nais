/*
 * Payment utility methods (used for NAIS-488 and NAIS-873)
 *
 * SL, Exponent Partners, 2013
 */
public class PaymentUtils {
    /**
     * @description Insert a Credit Card Transaction Line Item and mark it
     *              appropriately based on the outcome of the transaction
     *              through the payment provider: Posted if success, Error
     *              if failure.
     * @return The Id of the generated Transaction Line Item.
     */
    public static Id insertCreditCardTransactionLineItem(Id opportunityId, Decimal amount, String cardType, String ccCardNumberLast4Digits, PaymentResult result) {
        Id transactionLineItemId = null;

        if (opportunityId != null) {
            // general info
            Transaction_Line_Item__c transactionLineItem = new Transaction_Line_Item__c();
            transactionLineItem.RecordTypeId = RecordTypes.paymentAutoTransactionTypeId;
            transactionLineItem.Opportunity__c = opportunityId;
            transactionLineItem.Transaction_Status__c = 'Posted';
            transactionLineItem.Transaction_Date__c = System.today();
            transactionLineItem.Source__c = 'Web';
            transactionLineItem.Amount__c = amount;

            // transaction info
            transactionLineItem.Transaction_Type__c = 'Credit/Debit Card';
            transactionLineItem.Tender_Type__c = cardType;

            // [CH] NAIS-1489 Updating to use a custom setting instead of hard-coding
            Transaction_Settings__c settingRecord = Transaction_Settings__c.getValues(transactionLineItem.Transaction_Type__c);
            if (settingRecord != null) {
                transactionLineItem.Transaction_Code__c = settingRecord.Transaction_Code__c;
                transactionLineItem.Account_Code__c = settingRecord.Account_Code__c;
                transactionLineItem.Account_Label__c = settingRecord.Account_Label__c;
            }

            // payment info
            transactionLineItem.CC_Last_4_Digits__c = ccCardNumberLast4Digits;

            System.debug('Transaction line item: ' + transactionLineItem);

            if (result != null) {
                transactionLineItem.Transaction_ID__c = result.transactionNumber;

                // If the payment provider was Chargent stamp the Transaction field with the Id
                // to the Chargent Transaction and carry over the true process amount.
                if (result.TransactionRecord != null &&
                    result.TransactionRecord.getSObjectType() == ChargentOrders__Transaction__c.getSObjectType()) {

                    ChargentOrders__Transaction__c tRecord = (ChargentOrders__Transaction__c)result.TransactionRecord;
                    transactionLineItem.Transaction__c = tRecord.Id;
                    transactionLineItem.Amount__c = tRecord.ChargentOrders__Amount__c;
                }

                // [CH] NAIS-1716 Capturing result code and inserting record even if there's an error
                transactionLineItem.Transaction_Result_Code__c = String.valueOf(result.transactionResultCode);
                // SFP-22 need GUID for refunds [jB]
                transactionLineItem.VaultGUID__c = String.valueOf(result.VaultGUID != null ? result.VaultGUID : '');
                if (result.transactionResultCode == 0) {
                    transactionLineItem.Amount__c = (transactionLineItem.Amount__c == null) ? amount : transactionLineItem.Amount__c;
                } else {
                    transactionLineItem.Amount__c = 0;
                    transactionLineItem.Transaction_Status__c = 'ERROR'; // NAIS-2049 [DP] 12.10.2014 marking these as errors for clarity
                }
            }

            insert transactionLineItem;
            transactionLineItemId = transactionLineItem.Id;
        }
        return transactionLineItemId;
    }

    public static Id insertECheckTransactionLineItem(Id opportunityId, Decimal amount, PaymentResult result) {
        Id transactionLineItemId = null;
        if (opportunityId != null) {

            // general info
            Transaction_Line_Item__c transactionLineItem = new Transaction_Line_Item__c();
            transactionLineItem.RecordTypeId = RecordTypes.paymentAutoTransactionTypeId;
            transactionLineItem.Opportunity__c = opportunityId;
            transactionLineItem.Transaction_Status__c = 'Posted';
            transactionLineItem.Transaction_Date__c = System.today();
            transactionLineItem.Source__c = 'Web';
            transactionLineItem.Amount__c = amount;

            // transaction info
            transactionLineItem.Transaction_Type__c = 'ACH';
            transactionLineItem.Tender_Type__c = 'ACH';

            // [CH] NAIS-1489 Updating to use a custom setting instead of hard-coding
            Transaction_Settings__c settingRecord = Transaction_Settings__c.getValues(transactionLineItem.Transaction_Type__c);

            if (settingRecord != null) {
                transactionLineItem.Transaction_Code__c = settingRecord.Transaction_Code__c;
                transactionLineItem.Account_Code__c = settingRecord.Account_Code__c;
                transactionLineItem.Account_Label__c = settingRecord.Account_Label__c;
            }

            // payment info
            if (result != null) {
                transactionLineItem.Transaction_ID__c = result.transactionNumber;

                if (result.TransactionRecord != null &&
                        result.TransactionRecord.getSObjectType() == ChargentOrders__Transaction__c.getSObjectType()) {

                    ChargentOrders__Transaction__c tRecord = (ChargentOrders__Transaction__c)result.TransactionRecord;
                    transactionLineItem.Transaction__c = tRecord.Id;
                    transactionLineItem.Amount__c = tRecord.ChargentOrders__Amount__c;
                }

                transactionLineItem.Transaction_Result_Code__c = String.valueOf(result.transactionResultCode);
                transactionLineItem.VaultGUID__c = String.valueOf((result.VaultGUID != null) ? result.VaultGUID : '');

                // [DP] 07.20.2015 NAIS-1933 also creating error transactions for failed ACH payment attempts
                if (result.transactionResultCode == 0) {
                    transactionLineItem.Amount__c = (transactionLineItem.Amount__c == null) ? amount : transactionLineItem.Amount__c;
                } else {
                    transactionLineItem.Amount__c = 0;
                    transactionLineItem.Transaction_Status__c = 'ERROR'; // NAIS-2049 [DP] 12.10.2014 marking these as errors for clarity
                }
            }

            insert transactionLineItem;
            transactionLineItemId = transactionLineItem.Id;
        }
        return transactionLineItemId;
    }

    // NAIS-1245: Send email receipt via Apex instead of workflow
    public static void emailPaymentReceipt(Id parentAContactId, Id transactionLineItemId) {
        // retrieve the settings
        String orgWideEmailAddressValue = PaymentProcessor.ReceiptOrgWideEmailAddress;
        String emailSubject = PaymentProcessor.ReceiptEmailSubject;

        // get the org wide email address
        if (orgWideEmailAddressValue != null) {
            OrgWideEmailAddress orgWideEmailAddress = null;
            for (OrgWideEmailAddress owea : [SELECT Id FROM OrgWideEmailAddress WHERE Address = :orgWideEmailAddressValue LIMIT 1]) {
                orgWideEmailAddress = owea;
            }

            if (orgWideEmailAddress != null) {

                // instantiate a single email message
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

                // set the subject
                if (emailSubject != null) {
                    mail.setSubject(emailSubject);
                }

                // set the email body (both html and plaintext)
                mail.setHtmlBody(FamilyPaymentReceiptController.getHtmlContent(transactionLineItemId));
                mail.setPlainTextBody(FamilyPaymentReceiptController.getPlainTextContent(transactionLineItemId));

                // Set the email recipient
                mail.setTargetObjectId(parentAContactId);

                // Set the org wide email address
                mail.setOrgWideEmailAddressId(orgWideEmailAddress.Id);

                mail.setBccSender(false);
                mail.setUseSignature(false);
                mail.setSaveAsActivity(false);

                // send the message
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
        }
    }

    // [CH] NAIS-1556 This method is called by the OpportunityAfter trigger to create 5-pack Fee Waiver and Subscription TLI records
    public static List<Transaction_Line_Item__c> insertTransactionLineItemsFromOpportunities(List<Opportunity> opportunityList) {
        return insertTransactionLineItemsFromOpportunities(opportunityList, false, null);
    }

    public static List<Transaction_Line_Item__c> insertTransactionLineItemsFromOpportunities(List<Opportunity> opportunityList, Boolean forSingleFeeWaivers, Integer courtesyWaiverQuantity) {
        List<Transaction_Line_Item__c> transactionLineItemList = new List<Transaction_Line_Item__c> {};

        // Create a map of Transaction_Annual_Settings records that uses the quantity and year as the key
        Map<String, Transaction_Annual_Settings__c> annualSettingsWaiverMap = new Map<String, Transaction_Annual_Settings__c> {};
        Map<String, Transaction_Annual_Settings__c> annualSettingsSubscriptionMap = new Map<String, Transaction_Annual_Settings__c> {};
        for (Transaction_Annual_Settings__c annualSetting : Transaction_Annual_Settings__c.getAll().values()) {
            annualSettingsWaiverMap.put(annualSetting.Quantity__c + '|' + annualSetting.Year__c, annualSetting);
            annualSettingsSubscriptionMap.put(annualSetting.Subscription_Type__c + '|' + annualSetting.Year__c, annualSetting);
        }

        // NAIS-2417 [DP] 05.13.2015 commenting out
        //// Create a map of Academic Year records to use in looking up the correct annual custom setting
        //Set<Id> academicYearIds = new Set<Id>{};
        //for(Opportunity opportunity : opportunityList){
        //    academicYearIds.add(opportunity.Academic_Year__c);
        //}
        //Map<Id, Academic_Year__c> academicYearMap = new Map<Id, Academic_Year__c>([select Id, Name from Academic_Year__c where Id in :academicYearIds]);

        // For each Opportunity passed in
        for (Opportunity opportunity : opportunityList) {
            // Create a new Transaction Line item record
            Transaction_Line_Item__c transactionLineItem = new Transaction_Line_Item__c();
            transactionLineItem.RecordTypeId = RecordTypes.saleTransactionTypeId;
            transactionLineItem.Opportunity__c = opportunity.Id;
            transactionLineItem.Transaction_Status__c = 'Posted';
            transactionLineItem.Transaction_Date__c = System.today();

            // Fill in Extra Fee Waiver specific fields
            if (opportunity.RecordTypeId == RecordTypes.opportunityFeeWaiverPurchaseTypeId) {
                transactionLineItem.Source__c = 'Web';
                transactionLineItem.Transaction_Type__c = 'Extra Fee Waivers';

                // [CH] NAIS-1556 Need to make this behave differently for 5-packs versus single fee waivers
                String matchingValue = '';

                if (forSingleFeeWaivers) {
                    transactionLineItem.Waiver_Quantity__c = courtesyWaiverQuantity;
                    matchingValue = '1.0';
                } else {
                    transactionLineItem.Waiver_Quantity__c = opportunity.Product_Quantity__c;
                    //matchingValue = '5.0';
                    matchingValue = '1.0'; // [DP] NAIS-1983 only supporting single waiver packs now
                }

                // Look up the related Transaction_Annual_Settings values
                if (annualSettingsWaiverMap != null) {
                    //Academic_Year__c academicYearRecord = academicYearMap.get(opportunity.Academic_Year__c);
                    //String year = (academicYearRecord != null && academicYearRecord.Name != null) ? academicYearRecord.Name.split('-')[0] : '2014';
                    //String year = opportunity.Academic_Year_Picklist__c != null ? opportunity.Academic_Year_Picklist__c.split('-')[0] : '2014'; // NAIS-2417 [DP] 05.13.2015
                    String year = opportunity.Academic_Year_Picklist__c != null ? opportunity.Academic_Year_Picklist__c.split('-')[0] : GlobalVariables.getCurrentYearString().left(4);

                    Transaction_Annual_Settings__c annualSettingRecord = annualSettingsWaiverMap.get(matchingValue + '|' + year);
                    if (annualSettingRecord != null) {
                        transactionLineItem.Product_Amount__c = String.valueOf(annualSettingRecord.Product_Amount__c);
                        transactionLineItem.Product_Code__c = annualSettingRecord.Product_Code__c;
                    }
                }

                if (transactionLineItem.Waiver_Quantity__c != null && transactionLineItem.Product_Amount__c != null) {
                    Decimal amount = Decimal.valueOf(transactionLineItem.Product_Amount__c);
                    Decimal quantity = transactionLineItem.Waiver_Quantity__c;

                    transactionLineItem.Amount__c = amount * quantity;
                } else {
                    transactionLineItem.Amount__c = 0;
                }

            }
            // Fill in Subscription specific fields
            else if (opportunity.RecordTypeId == RecordTypes.opportunitySubscriptionFeeTypeId) {
                transactionLineItem.Transaction_Type__c = 'School Subscription';
                transactionLineItem.Product_Amount__c = '0';

                // Look up the related Transaction_Annual_Settings values
                if (annualSettingsSubscriptionMap != null) {
                    String year = opportunity.Academic_Year_Picklist__c != null ? opportunity.Academic_Year_Picklist__c.split('-')[0] : GlobalVariables.getCurrentYearString().left(4);
                    Transaction_Annual_Settings__c annualSettingRecord = annualSettingsSubscriptionMap.get(opportunity.Subscription_Type__c + '|' + year);
                    if (annualSettingRecord != null) {
                        transactionLineItem.Product_Amount__c = String.valueOf(annualSettingRecord.Product_Amount__c);
                        transactionLineItem.Product_Code__c = annualSettingRecord.Product_Code__c;
                        // [DP] NAIS-1983 set amount in code
                        transactionLineItem.Amount__c = annualSettingRecord.Product_Amount__c;
                    } else {
                        transactionLineItem.Product_Amount__c = '0';
                        transactionLineItem.Amount__c = 0;
                    }
                }
            }

            // Fill in values from Transaction_Settings__c
            Transaction_Settings__c settingRecord = Transaction_Settings__c.getValues(transactionLineItem.Transaction_Type__c);
            if (settingRecord != null) {
                transactionLineItem.Transaction_Code__c = settingRecord.Transaction_Code__c;
                transactionLineItem.Account_Code__c = settingRecord.Account_Code__c;
                transactionLineItem.Account_Label__c = settingRecord.Account_Label__c;
            }

            // Add the record to the list to be inserted
            transactionLineItemList.add(transactionLineItem);
        }

        // Insert records
        if (transactionLineItemList.size() > 0) {
            insert transactionLineItemList;
        }

        // Return the newly created records
        return transactionLineItemList;
    }

    public static List<Transaction_Line_Item__c> createCreditAdjustmentLineItems(List<Transaction_Line_Item__c> saleLineItems) {
        // Retrieve Transaction Settings
        Transaction_Settings__c settingRecord = Transaction_Settings__c.getValues('School Courtesy Credit');

        // Clone the list that's passed in
        List<Transaction_Line_Item__c> lineItemsToInsert = new List<Transaction_Line_Item__c> {};

        // Adjust line item records to turn them into adjustments
        for (Transaction_Line_Item__c lineItem : saleLineItems) {
            Transaction_Line_Item__c adjustment = lineItem.clone(false, true, false, false);
            adjustment.RecordTypeId = RecordTypes.adjustmentTransactionTypeId;
            adjustment.Transaction_Type__c = 'School Courtesy Credit';
            adjustment.Amount__c = lineItem.Amount__c;
            adjustment.Waiver_Quantity__c = null;

            if (settingRecord != null) {
                adjustment.Account_Code__c = settingRecord.Account_Code__c;
                adjustment.Account_Label__c = settingRecord.Account_Label__c;
                adjustment.Transaction_Code__c = settingRecord.Transaction_Code__c;
            }

            lineItemsToInsert.add(adjustment);
        }

        Database.insert(lineItemsToInsert);

        return lineItemsToInsert;
    }

    //SFP-311, [G.S]
    public static Boolean hasCrossedErrorAttempts(Id opportunityId){
        Integer count = 0;
        for (Transaction_Line_Item__c tli : [select Id, Transaction_Status__c from Transaction_Line_Item__c
                                             where Opportunity__c = :opportunityId order by lastmodifieddate desc]) {

            if (tli.Transaction_Status__c == 'Error') {
                count++;
                if(count > 2){
                    return true;
                }
            } else {
               break;
            }

        }
        return false;
    }
}