public interface SchoolDocumentListInterface {
    void editDocument();
    void deleteDocument();
    void uploadDocument();
    void archiveDocument();//SFP-38, [G.S]
}