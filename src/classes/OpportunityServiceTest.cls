@isTest
private class OpportunityServiceTest {
    private static void assertClosedWonOpportunity(Opportunity opportunityToUpdate, Opportunity updatedOpportunity) {
        System.assertEquals(opportunityToUpdate.Id, updatedOpportunity.Id,
                'Expected the returned Opportunity to be the same.');
        System.assertEquals(OpportunityService.CLOSED_WON, updatedOpportunity.StageName,
                'Expected the Stage Name to be ' + OpportunityService.CLOSED_WON);
        System.assertEquals(System.today(), updatedOpportunity.CloseDate,
                'Expected the returned Close Date to be today.');
    }

    private static void assertClosedWonOpportunities(List<Opportunity> opportunitiesToUpdate, List<Opportunity> updatedOpportunities) {
        System.assertEquals(opportunitiesToUpdate.size(), updatedOpportunities.size(),
                'Expected the same number of Opportunities.');

        Map<Id, Opportunity> updatedOpportunitiesByIds = new Map<Id, Opportunity>(updatedOpportunities);
        for (Opportunity opportunity : opportunitiesToUpdate) {
            System.assert(updatedOpportunitiesByIds.containsKey(opportunity.Id), 'Expected an Opportunity to be updated.');
            assertClosedWonOpportunity(opportunity, updatedOpportunitiesByIds.get(opportunity.Id));
        }
    }

    @isTest
    private static void markAsClosedWon_withId_expectUpdated() {
        Opportunity opportunityToUpdate = OpportunityTestData.Instance.DefaultOpportunity;
        TransactionLineItemTestData.Instance.insertTransactionLineItem();

        Test.startTest();
        Opportunity updatedOpportunity = OpportunityService.Instance.markAsClosedWon(opportunityToUpdate.Id);
        Test.stopTest();

        assertClosedWonOpportunity(opportunityToUpdate, updatedOpportunity);
    }

    @isTest
    private static void markAsClosedWon_withRecord_expectUpdated() {
        Opportunity opportunityToUpdate = OpportunityTestData.Instance.DefaultOpportunity;
        TransactionLineItemTestData.Instance.insertTransactionLineItem();

        Test.startTest();
        Opportunity updatedOpportunity = OpportunityService.Instance.markAsClosedWon(opportunityToUpdate);
        Test.stopTest();

        assertClosedWonOpportunity(opportunityToUpdate, updatedOpportunity);
    }

    @isTest
    private static void markAsClosedWon_withIdSet_expectUpdated() {
        List<Opportunity> opportunitiesToUpdate = new List<Opportunity> {
                OpportunityTestData.Instance.DefaultOpportunity,
                OpportunityTestData.Instance.insertOpportunity()
        };

        // We need to make sure the Opportunities have TLIs or it will exception out.
        TransactionLineItemTestData.Instance.forOpportunity(opportunitiesToUpdate[0].Id).insertTransactionLineItem();
        TransactionLineItemTestData.Instance.forOpportunity(opportunitiesToUpdate[1].Id).insertTransactionLineItem();

        Test.startTest();
        List<Opportunity> updatedOpportunities = OpportunityService.Instance
                .markAsClosedWon(new Map<Id, Opportunity>(opportunitiesToUpdate).keySet());
        Test.stopTest();

        assertClosedWonOpportunities(opportunitiesToUpdate, updatedOpportunities);
    }

    @isTest
    private static void markAsClosedWon_withListCollection_expectUpdated() {
        List<Opportunity> opportunitiesToUpdate = new List<Opportunity> {
                OpportunityTestData.Instance.DefaultOpportunity,
                OpportunityTestData.Instance.insertOpportunity()
        };

        // We need to make sure the Opportunities have TLIs or it will exception out.
        TransactionLineItemTestData.Instance.forOpportunity(opportunitiesToUpdate[0].Id).insertTransactionLineItem();
        TransactionLineItemTestData.Instance.forOpportunity(opportunitiesToUpdate[1].Id).insertTransactionLineItem();

        Test.startTest();
        List<Opportunity> updatedOpportunities = OpportunityService.Instance.markAsClosedWon(opportunitiesToUpdate);
        Test.stopTest();

        assertClosedWonOpportunities(opportunitiesToUpdate, updatedOpportunities);
    }

    @isTest
    private static void markAsClosedWon_nullId_expectArgumentNullException() {
        try {
            Test.startTest();
            OpportunityService.Instance.markAsClosedWon((Id)null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, OpportunityService.OPPORTUNITY_ID_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void markAsClosedWon_invalidId_expectArgumentException() {
        Id invalidId = TransactionLineItemTestData.Instance.DefaultTransactionLineItem.Id;

        try {
            Test.startTest();
            OpportunityService.Instance.markAsClosedWon(invalidId);

            TestHelpers.expectedArgumentException();
        } catch (Exception e) {
            TestHelpers.assertArgumentException(e, OpportunityService.INVALID_SOBJECT_TYPE);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void markAsClosedWon_nullSet_expectArgumentNullException() {
        try {
            Test.startTest();
            OpportunityService.Instance.markAsClosedWon((Set<Id>)null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, OpportunityService.OPPORTUNITY_IDS_PARAM);
        } finally {
            Test.stopTest();
        }
    }

    @isTest
    private static void markAsClosedWon_nullList_expectArgumentNullException() {
        try {
            Test.startTest();
            OpportunityService.Instance.markAsClosedWon((List<Opportunity>)null);

            TestHelpers.expectedArgumentNullException();
        } catch (Exception e) {
            TestHelpers.assertArgumentNullException(e, OpportunityService.OPPORTUNITIES_PARAM);
        } finally {
            Test.stopTest();
        }
    }
}