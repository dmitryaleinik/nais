/**
 * @description Manage subscriptions.
 */
public class SubscriptionService
{

    @testVisible private static final String OPPORTUNITY_PARAM = 'opportunity';
    @testVisible private static final String OPPORTUNITIES_PARAM = 'opportunities';
    @testVisible private static final String SUBSCRIPTION_TYPE_PREMIUM = 'Premium';
    @testVisible private static final String SUBSCRIPTION_TYPE_FULL = 'Full';

    /**
     * @description Determine if a given oppoturnity requires that a subscription be created for
     *              it based on whether it already has a subscription, and whether it is paid
     *              and marked as closed.
     * @param opportunity The opportunity to check if a subscription should be created or not.
     * @return True if a subscription should be created, else false.
     * @throws An ArgumentNullException if opportunity is null.
     */
    @testVisible private Boolean shouldCreateSubscription(Opportunity opportunity) {
        
        ArgumentNullException.throwIfNull(opportunity, OPPORTUNITY_PARAM);
        
        if (opportunity.AccountId == null || String.isBlank(opportunity.Academic_Year_Picklist__c)) {
            return false;
        }

        Id subscriptionFeeRecordTypeId = RecordTypes.opportunitySubscriptionFeeTypeId;
        Integer startYear = Integer.valueOf(opportunity.Academic_Year_Picklist__c.left(4));
        if (startYear == null) {
            return false;
        }
        
        List<Subscription__c> subscriptions = SubscriptionSelector.Instance
                .selectByAccountIdForAcademicYear(new Set<Id> { opportunity.AccountId }, startYear);
        
        return shouldCreateSubscription(opportunity, subscriptions);
    }
    
    @testVisible private Boolean shouldCreateSubscription(Opportunity opportunity, List<Subscription__c> subscriptions) {
        ArgumentNullException.throwIfNull(opportunity, OPPORTUNITY_PARAM);

        // Return true if there are no subscriptions already existing for the opportunity and the
        // opportunity is marked in a closed Stage and has a Paid Status of either paid or overpaid.
        return OpportunityService.Instance.isMarkedAsClosed(opportunity) &&
               OpportunityService.Instance.isPaid(opportunity) &&
               subscriptions.isEmpty();
    }

    public static void updateAnnualSettingDisplayEstimatesField (List<Subscription__c> newValues, Map<Id, Subscription__c> oldValues)
    {
        Set<Id> relatedSchoolIds = new Set<Id>();
        for(Subscription__c subscription : newValues)
        {
            if(subscription.Status__c == 'Current')
            {
                if(subscription.Subscription_Type__c != SUBSCRIPTION_TYPE_PREMIUM
                    && oldValues.get(subscription.Id).Subscription_Type__c == SUBSCRIPTION_TYPE_PREMIUM)
                {
                    relatedSchoolIds.add(subscription.Account__c);
                }
            }
        }

        if (relatedSchoolIds.size() > 0)
        {
            AnnualSettingHelper.disableDisplayNextYearTaxEstimates(relatedSchoolIds);
        }
    }

    /**
     * @description Determine if a given oppoturnity requires that a subscription be deleted for
     *              it based on whether it already has a subscription, and whether it is unpaid
     *              or marked as open.
     * @param opportunity The opportunity to check if a subscription should be deleted or not.
     * @return True if a subscription should be deleted, else false.
     * @throws An ArgumentNullException if opportunity is null.
     */
     @testVisible private Boolean shouldDeleteSubscription(Opportunity opportunity) {
        ArgumentNullException.throwIfNull(opportunity, OPPORTUNITY_PARAM);
        
        if (opportunity.AccountId == null ||
            String.isBlank(opportunity.Academic_Year_Picklist__c) ||
            opportunity.RecordTypeId != RecordTypes.opportunitySubscriptionFeeTypeId) {
            return false;
        }
        
        Integer startYear = Integer.valueOf(opportunity.Academic_Year_Picklist__c.left(4));
        if (startYear == null) {
            return false;
        }
        
        List<Subscription__c> subscriptions = SubscriptionSelector.Instance
                .selectByAccountIdForAcademicYear(new Set<Id> { opportunity.AccountId }, startYear);
        
        return shouldDeleteSubscription(opportunity, subscriptions);
     }

    /**
     * @description Determine if a given oppoturnity requires that a subscription be deleted for
     *              it based on whether it already has a subscription, and whether it is unpaid
     *              or marked as open.
     * @param opportunity The opportunity to check if a subscription should be deleted or not.
     * @param subscriptions The list of related subscriptions.
     * @return True if a subscription should be deleted, else false.
     * @throws An ArgumentNullException if opportunity is null.
     */
    @testVisible private Boolean shouldDeleteSubscription(Opportunity opportunity, List<Subscription__c> subscriptions) {
        ArgumentNullException.throwIfNull(opportunity, OPPORTUNITY_PARAM);

        // Return true if there are is a subscription to delete and the user has either marked the
        // StageName to something other than a closed stage or the Paid Status to something other
        // than paid or overpaid.
        return (!OpportunityService.Instance.isMarkedAsClosed(opportunity) ||
                !OpportunityService.Instance.isPaid(opportunity)) &&
                !subscriptions.isEmpty();
    }

    /**
     * @description Delete subscriptions associated with a given Opportunity.
     * @param opportunity The opportunity to delete subscriptions for.
     * @throws An ArgumentNullException if opportunity is null.
     */
    public void deleteSubscriptions(Opportunity opportunity) {
        ArgumentNullException.throwIfNull(opportunity, OPPORTUNITY_PARAM);
        deleteSubscriptions(new List<Opportunity> { opportunity });
    }

    /**
     * @description Delete subscriptions associated with the given opportunities.
     * @param opportunities The collection of opportunities to delete subscriptions
     *        for.
     * @throws An ArgumentNullException if opportunities is null.
     */
    public void deleteSubscriptions(List<Opportunity> opportunities) {
        ArgumentNullException.throwIfNull(opportunities, OPPORTUNITIES_PARAM);

        List<Subscription__c> subscriptionsToDelete = new List<Subscription__c>();
        List<Subscription__c> subscriptionsForOpportunities = getSubscriptionsForOpportunities(opportunities);
        Map<Id, List<Opportunity>> opportunitiesByAccountId = getOpportunitiesByAccountId(opportunities);

        for (Subscription__c subscription : subscriptionsForOpportunities) {
            List<Opportunity> accountOpportunities = opportunitiesByAccountId.get(subscription.Account__c);

            for (Opportunity opportunity : accountOpportunities) {
                if (opportunity.Academic_Year_Picklist__c.left(4) == String.valueOf(subscription.End_Date__c.year())) {
                    subscriptionsToDelete.add(subscription);
                    break;
                }
            }
        }

        if (!subscriptionsToDelete.isEmpty()) {
            delete subscriptionsToDelete;
        }
    }

    private Map<Id, List<Opportunity>> getOpportunitiesByAccountId(List<Opportunity> opportunities) {
        Map<Id, List<Opportunity>> opportunitiesByAccountId = new Map<Id, List<Opportunity>>();

        for (Opportunity opportunity : opportunities) {
            if (!opportunitiesByAccountId.containsKey(opportunity.AccountId)) {
                opportunitiesByAccountId.put(opportunity.AccountId, new List<Opportunity>());
            }
            opportunitiesByAccountId.get(opportunity.AccountId).add(opportunity);
        }

        return opportunitiesByAccountId;
    }

    private List<Subscription__c> getSubscriptionsForOpportunities(List<Opportunity> opportunities) {
        Set<Id> accountIds = new Set<Id>();
        Set<Integer> startYears = new Set<Integer>();

        for (Opportunity opportunity : opportunities) {
            if (!isValidSubscriptionOpportunity(opportunity)) {
                continue;
            }

            startYears.add(Integer.valueOf(opportunity.Academic_Year_Picklist__c.left(4)));
            accountIds.add(opportunity.AccountId);
        }

        return SubscriptionSelector.Instance.selectByAccountIdForAcademicYear(accountIds, startYears);
    }

    private Boolean isValidSubscriptionOpportunity(Opportunity opportunity) {
        return opportunity.AccountId != null &&
                String.isNotBlank(opportunity.Academic_Year_Picklist__c) &&
                opportunity.RecordTypeId == RecordTypes.opportunitySubscriptionFeeTypeId;
    }

    /**
     * @description Singleton Instance property.
     */
    public static SubscriptionService Instance {
        get {
            if (Instance == null) {
                Instance = new SubscriptionService();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to force access through the
     *              singleton instance.
     */
    private SubscriptionService() {}
    
    public class Response {

        private Response (Map<Id, Opportunity> oppIdsThatNeedSubsDeleted, Map<Id, Opportunity> oppIdsThatNeedSubsCreated) {
            
            OppIdsToDeleteSubs = oppIdsThatNeedSubsDeleted;
            OppIdsToCreateSubs = oppIdsThatNeedSubsCreated;
        }

        public Map<Id, Opportunity> OppIdsToDeleteSubs { 
            get {
                if (OppIdsToDeleteSubs == null) {
                    OppIdsToDeleteSubs = new Map<Id, Opportunity>();
                }
                return OppIdsToDeleteSubs;
            } 
            private set;
        }

        public Map<Id, Opportunity> OppIdsToCreateSubs { 
            get {
                if (OppIdsToCreateSubs == null) {
                    OppIdsToCreateSubs = new Map<Id, Opportunity>();
                }
                return OppIdsToCreateSubs;
            } 
            private set;
        }
    }
    
    public Response getOppsThatNeedSubAdjustments(List<Opportunity> opportunities, Map<Id, Opportunity> oldOpportunities) {
        
        Map<Id, Opportunity> oppsToDeleteSubscriptions = new Map<Id, Opportunity>();
        Map<Id, Opportunity> oppsToGetSubscriptions = new Map<Id, Opportunity>();
        List<Subscription__c> subscriptions;
        String mapKey;
        Integer startYear;
        
        //Get the subscriptions for the given opportunities
        Map<String, List<Subscription__c>> mapSubscriptions = mapSubscriptionsByAccountAndAcademicYear(opportunities);
        
        for(Opportunity opp : opportunities) {
            
            startYear = opp.Academic_Year_Picklist__c != null ? Integer.valueOf(opp.Academic_Year_Picklist__c.left(4)) : null;
            mapKey = opp.AccountId + '-' + startYear;
            
            if (startYear == null) {
                continue;
            }
            
            subscriptions = mapSubscriptions.containsKey(mapKey) ? mapSubscriptions.get(mapKey) : new List<Subscription__c>();
            if (oldOpportunities.get(opp.Id).StageName != opp.StageName && shouldDeleteSubscription(opp, subscriptions)) {
                //Opportunities for which its subscription will be deleted.
                oppsToDeleteSubscriptions.put(opp.Id, opp);
                
            } else if (shouldCreateSubscription(opp, subscriptions)) {
                //Opportunities for which a subscription will be created.
                oppsToGetSubscriptions.put(opp.Id, opp);
            }
        }
        
        return new Response(oppsToDeleteSubscriptions, oppsToGetSubscriptions);
    }//End:getOppsThatNeedSubAdjustments
    
    private Map<String, List<Subscription__c>> mapSubscriptionsByAccountAndAcademicYear(List<Opportunity> opportunities) {
        ArgumentNullException.throwIfNull(opportunities, 'opportunities');
        
        if (opportunities.isEmpty()) {
           return new Map<String, List<Subscription__c>>(); 
        }
        
        Integer startYear;
        Set<Id> accountIds = new Set<Id>();
        Set<Integer> startYears = new Set<Integer>();
        
        for(Opportunity opp : opportunities) {
            
            if (opp.AccountId == null || String.isBlank(opp.Academic_Year_Picklist__c)) {
                continue;
            }
            
            startYear = Integer.valueOf(opp.Academic_Year_Picklist__c.left(4));                
            accountIds.add(opp.AccountId);
            startYears.add(startYear);
        }
        
        return mapSubscriptions(accountIds, startYears);
    }
    
    private Map<String, List<Subscription__c>> mapSubscriptions(Set<Id> accountIds, Set<Integer> startYears) {
        ArgumentNullException.throwIfNull(accountIds, 'accountIds');
        ArgumentNullException.throwIfNull(startYears, 'startYears');
        
        Map<String, List<Subscription__c>> result = new Map<String, List<Subscription__c>>();
        
        List<Subscription__c> subscriptions = SubscriptionSelector.Instance
                .selectByAccountIdForAcademicYear(accountIds, startYears);
        
        String mapKey;
        List<Subscription__c> tmpSubscriptions;
        for(Subscription__c s : subscriptions) {
            
            mapKey = s.Account__c + '-' + s.End_Date__c.year();
            
            tmpSubscriptions = result.containsKey(mapKey) 
                ? result.get(mapKey) : new List<Subscription__c>();
            
            tmpSubscriptions.add(s);
            result.put(mapKey, tmpSubscriptions);
        }
        
        return result;
    }//End:mapSubscriptionsByAccountAndAcademicYear
}