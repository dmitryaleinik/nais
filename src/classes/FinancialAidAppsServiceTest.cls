@isTest
private class FinancialAidAppsServiceTest {

    @isTest
    private static void getOtherSchoolAppsByStudentId_studentWithOnlyOneApp_expectEmptyMap() {
        String academicYearName = '2018-2019';

        Account schoolOne = AccountTestData.Instance.asSchool().asCurrent().insertAccount();

        Contact studentOne = ContactTestData.Instance.asStudent().insertContact();

        Student_Folder__c currentFolderForSchoolOne = StudentFolderTestData.Instance.forSchoolId(schoolOne.Id).forAcademicYearPicklist(academicYearName).forStudentId(studentOne.Id).insertStudentFolder();

        Set<Id> studentIds = new Set<Id> { studentOne.Id };

        Map<Id, List<Student_Folder__c>> otherSchoolsFoldersByStudentId = FinancialAidAppsService.Instance.getOtherSchoolAppsByStudentId(studentIds, academicYearName, schoolOne.Id);

        System.assertNotEquals(null, otherSchoolsFoldersByStudentId, 'Expected the map to not be null.');
        System.assertEquals(0, otherSchoolsFoldersByStudentId.size(), 'Expected the map to be empty.');
    }

    @isTest
    private static void getOtherSchoolAppsByStudentId_studentWithMultipleAppsForDifferentAcademicYear_expectEmptyMap() {
        String currentAcademicYearName = '2018-2019';
        String pastAcademicYearName = '2017-2018';

        Account schoolOne = AccountTestData.Instance.asSchool().asCurrent().insertAccount();

        Contact studentOne = ContactTestData.Instance.asStudent().insertContact();

        Student_Folder__c currentFolderForStudentOneSchoolOne = StudentFolderTestData.Instance.forSchoolId(schoolOne.Id).forAcademicYearPicklist(currentAcademicYearName).forStudentId(studentOne.Id).insertStudentFolder();
        Student_Folder__c pastFolderForStudentOneSchoolOne = StudentFolderTestData.Instance.forSchoolId(schoolOne.Id).forAcademicYearPicklist(pastAcademicYearName).forStudentId(studentOne.Id).insertStudentFolder();

        Set<Id> studentIds = new Set<Id> { studentOne.Id };

        Map<Id, List<Student_Folder__c>> otherSchoolsFoldersByStudentId = FinancialAidAppsService.Instance.getOtherSchoolAppsByStudentId(studentIds, currentAcademicYearName, schoolOne.Id);

        System.assertNotEquals(null, otherSchoolsFoldersByStudentId, 'Expected the map to not be null.');
        System.assertEquals(0, otherSchoolsFoldersByStudentId.size(), 'Expected the map to be empty.');
    }

    @isTest
    private static void getOtherSchoolAppsByStudentId_studentWithMultipleApps_expectMapContainsOtherSchools() {
        String currentAcademicYearName = '2018-2019';
        String pastAcademicYearName = '2017-2018';

        Account schoolOne = AccountTestData.Instance.asSchool().asCurrent().insertAccount();
        Account schoolTwo = AccountTestData.Instance.asSchool().asCurrent().insertAccount();

        Contact studentOne = ContactTestData.Instance.asStudent().insertContact();

        Student_Folder__c currentFolderForStudentOneSchoolOne = StudentFolderTestData.Instance.forSchoolId(schoolOne.Id).forAcademicYearPicklist(currentAcademicYearName).forStudentId(studentOne.Id).insertStudentFolder();
        Student_Folder__c currentFolderForStudentOneSchoolTwo = StudentFolderTestData.Instance.forSchoolId(schoolTwo.Id).forAcademicYearPicklist(currentAcademicYearName).forStudentId(studentOne.Id).insertStudentFolder();
        Student_Folder__c pastFolderForStudentOneSchoolOne = StudentFolderTestData.Instance.forSchoolId(schoolOne.Id).forAcademicYearPicklist(pastAcademicYearName).forStudentId(studentOne.Id).insertStudentFolder();

        Set<Id> studentIds = new Set<Id> { studentOne.Id };

        Map<Id, List<Student_Folder__c>> otherSchoolsFoldersByStudentId = FinancialAidAppsService.Instance.getOtherSchoolAppsByStudentId(studentIds, currentAcademicYearName, schoolOne.Id);

        System.assertNotEquals(null, otherSchoolsFoldersByStudentId, 'Expected the map to not be null.');
        System.assertEquals(1, otherSchoolsFoldersByStudentId.size(), 'Expected the map to have results.');

        List<Student_Folder__c> otherAppsForStudentOne = otherSchoolsFoldersByStudentId.get(studentOne.Id);
        System.assertNotEquals(null, otherAppsForStudentOne, 'Expected results for student one.');
        System.assertEquals(1, otherAppsForStudentOne.size(), 'Expected one other application for student one.');
        System.assertEquals(schoolTwo.Id, otherAppsForStudentOne[0].School__c, 'Expected the other application to be for school two.');
    }

    @isTest
    private static void getOtherSchoolAppsByStudentId_twoStudentsWithMultipleApps_expectMapContainsOtherSchools() {
        String currentAcademicYearName = '2018-2019';
        String pastAcademicYearName = '2017-2018';

        Account schoolOne = AccountTestData.Instance.asSchool().asCurrent().insertAccount();
        Account schoolTwo = AccountTestData.Instance.asSchool().asCurrent().insertAccount();

        Contact studentOne = ContactTestData.Instance.asStudent().insertContact();
        Contact studentTwo = ContactTestData.Instance.asStudent().insertContact();

        // Insert folders for student one.
        Student_Folder__c currentFolderForStudentOneSchoolOne = StudentFolderTestData.Instance.forSchoolId(schoolOne.Id).forAcademicYearPicklist(currentAcademicYearName).forStudentId(studentOne.Id).insertStudentFolder();
        Student_Folder__c currentFolderForStudentOneSchoolTwo = StudentFolderTestData.Instance.forSchoolId(schoolTwo.Id).forAcademicYearPicklist(currentAcademicYearName).forStudentId(studentOne.Id).insertStudentFolder();
        Student_Folder__c pastFolderForStudentOneSchoolOne = StudentFolderTestData.Instance.forSchoolId(schoolOne.Id).forAcademicYearPicklist(pastAcademicYearName).forStudentId(studentOne.Id).insertStudentFolder();

        // Insert folders for student two.
        Student_Folder__c currentFolderForStudentTwoSchoolOne = StudentFolderTestData.Instance.forSchoolId(schoolOne.Id).forAcademicYearPicklist(currentAcademicYearName).forStudentId(studentTwo.Id).insertStudentFolder();
        Student_Folder__c currentFolderForStudentTwoSchoolTwo = StudentFolderTestData.Instance.forSchoolId(schoolTwo.Id).forAcademicYearPicklist(currentAcademicYearName).forStudentId(studentTwo.Id).insertStudentFolder();
        Student_Folder__c pastFolderForStudentTwoSchoolOne = StudentFolderTestData.Instance.forSchoolId(schoolOne.Id).forAcademicYearPicklist(pastAcademicYearName).forStudentId(studentTwo.Id).insertStudentFolder();

        Set<Id> studentIds = new Set<Id> { studentOne.Id, studentTwo.Id };

        Map<Id, List<Student_Folder__c>> otherSchoolsFoldersByStudentId = FinancialAidAppsService.Instance.getOtherSchoolAppsByStudentId(studentIds, currentAcademicYearName, schoolOne.Id);

        System.assertNotEquals(null, otherSchoolsFoldersByStudentId, 'Expected the map to not be null.');
        System.assertEquals(2, otherSchoolsFoldersByStudentId.size(), 'Expected the map to have results for two students.');

        List<Student_Folder__c> otherAppsForStudentOne = otherSchoolsFoldersByStudentId.get(studentOne.Id);
        System.assertNotEquals(null, otherAppsForStudentOne, 'Expected results for student one.');
        System.assertEquals(1, otherAppsForStudentOne.size(), 'Expected one other application for student one.');
        System.assertEquals(schoolTwo.Id, otherAppsForStudentOne[0].School__c, 'Expected the other application to be for school two.');

        List<Student_Folder__c> otherAppsForStudentTwo = otherSchoolsFoldersByStudentId.get(studentTwo.Id);
        System.assertNotEquals(null, otherAppsForStudentTwo, 'Expected results for student two.');
        System.assertEquals(1, otherAppsForStudentTwo.size(), 'Expected one other application for student two.');
        System.assertEquals(schoolTwo.Id, otherAppsForStudentTwo[0].School__c, 'Expected the other application to be for school two.');
    }
}