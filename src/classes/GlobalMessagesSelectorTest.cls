@isTest
private class GlobalMessagesSelectorTest
{

    @isTest
    private static void testSelectRelevantByPortalPageAcademicYear()
    {
        String familyApplicationMainPage = 'FamilyApplicationMain';
        String familyMessagesPage = 'FamilyMessages';
        String familyPortalValue = 'Family Portal';
        String schoolPortalValue = 'School Portal';
        String springCMValue = 'Spring CM - Maintenance Window';
        String applicantInformationScreenName = 'ApplicantInformation';
        String selectSchoolsScreenName = 'SelectSchools';

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
        Academic_Year__c nextAcademicYear = AcademicYearTestData.Instance.asNextYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, nextAcademicYear};

        FamilyAppSettings__c fasApplicantInformation = FamilyAppSettingsTestData.Instance.forName(applicantInformationScreenName).create();
        FamilyAppSettings__c fasSelectSchools = FamilyAppSettingsTestData.Instance.forName(selectSchoolsScreenName).create();
        insert new List<FamilyAppSettings__c>{fasApplicantInformation, fasSelectSchools};

        // Global Message with appropriate start and end dates, for current year, family portal and all pages
        Global_Message__c gm1 = GlobalMessageTestData.Instance
            .forAcademicYear(currentAcademicYear.Name)
            .forPortal(familyPortalValue).create();

        // Global Message with appropriate start and end dates, for all years, family portal all pages
        Global_Message__c gm2 = GlobalMessageTestData.Instance
            .forPortal(familyPortalValue).create();

        // Global Message with appropriate start and end dates, for current year, null portal and Applicant Information screen
        // of FamilyDashboard page
        Global_Message__c gm3 = GlobalMessageTestData.Instance
            .forAcademicYear(currentAcademicYear.Name)
            .forPortal(null)
            .forPageName(applicantInformationScreenName).create();

        // Global Message with appropriate start and end dates, for current year, null portal and Select Schools screen
        // of FamilyDashboard page
        Global_Message__c gm4 = GlobalMessageTestData.Instance
            .forAcademicYear(currentAcademicYear.Name)
            .forPortal(null)
            .forPageName(selectSchoolsScreenName).create();

        // Global Message with appropriate start and end dates, for current year, null portal
        // and All screens of familyApplicationMain page
        Global_Message__c gm5 = GlobalMessageTestData.Instance
            .forAcademicYear(currentAcademicYear.Name)
            .forPortal(null)
            .forPageName(familyApplicationMainPage).create();

        // Global Message with appropriate start and end dates, for current year, null portal and FamilyMessages page
        Global_Message__c gm6 = GlobalMessageTestData.Instance
            .forAcademicYear(currentAcademicYear.Name)
            .forPortal(null)
            .forPageName(familyMessagesPage).create();

        // Global Message with appropriate start and end dates, with SpringCM portal param
        Global_Message__c gm7 = GlobalMessageTestData.Instance
            .forPortal(springCMValue)
            .forAcademicYear(currentAcademicYear.Name).create();

        // Global Message with appropriate start and end dates, for next year, family portal and all pages
        Global_Message__c gm8 = GlobalMessageTestData.Instance
            .forAcademicYear(nextAcademicYear.Name)
            .forPortal(familyPortalValue).create();

        // Global Message with appropriate start and end dates, for current year, school portal all pages
        Global_Message__c gm9 = GlobalMessageTestData.Instance
            .forAcademicYear(currentAcademicYear.Name)
            .forPortal(schoolPortalValue).create();

        // Global Message with not appropriate start and end dates, for current year, family portal all pages
        Global_Message__c gm10 = GlobalMessageTestData.Instance
            .forDisplayStartDateTime(system.now().addDays(1))
            .forDisplayEndDateTime(system.now().addDays(2))
            .forAcademicYear(currentAcademicYear.Name)
            .forPortal(familyPortalValue).create();

        // Global Message with not appropriate start and end dates, for current year, family portal all pages
        Global_Message__c gm11 = GlobalMessageTestData.Instance
            .forDisplayStartDateTime(system.now().addDays(-2))
            .forDisplayEndDateTime(system.now().addDays(-1))
            .forAcademicYear(currentAcademicYear.Name)
            .forPortal(familyPortalValue).create();

        insert new List<Global_Message__c>{gm1, gm2, gm3, gm4, gm5, gm6, gm7, gm8, gm9, gm10, gm11};

        Test.startTest();
            List<Global_Message__c> globalMessages = GlobalMessagesSelector.newInstance().selectRelevantByPortalPageAcademicYear(
                familyPortalValue, new List<String> {familyApplicationMainPage, applicantInformationScreenName}, currentAcademicYear.Name);
            System.assertEquals(4, globalMessages.size());
            System.assert((new Map<Id, Global_Message__c>(globalMessages).keySet()).containsAll(new Set<Id>{gm1.Id, gm2.Id, gm3.Id, gm5.Id}));

            globalMessages = GlobalMessagesSelector.Instance.selectRelevantByPortalPageAcademicYear(null, (List<String>)null, null);
            System.assertEquals(0, globalMessages.size(), 'expected no rows since portal and addPortal parameters are null');

            globalMessages = GlobalMessagesSelector.Instance.selectRelevantByPortalPageAcademicYear(
                springCMValue, new List<String>{familyMessagesPage}, currentAcademicYear.Name);
            System.assertEquals(2, globalMessages.size());
            System.assert((new Map<Id, Global_Message__c>(globalMessages).keySet()).containsAll(new Set<Id>{gm6.Id, gm7.Id}));

            globalMessages = GlobalMessagesSelector.Instance.selectRelevantByPortalPageAcademicYear(
                schoolPortalValue, (List<String>)null, null);
            System.assertEquals(1, globalMessages.size());
            System.assertEquals(gm9.Id, globalMessages[0].Id);
        Test.stopTest();
    }
}