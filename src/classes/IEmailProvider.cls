/**
 * IEmailProvider.cls
 *
 * @description: Interface which all email Providers implement to handler Provider-specific details.
 *
 * @author: Chase Logan @ Presence PG
 */
public interface IEmailProvider {

    // STUB: Send Emails via Provider w/ Provider-specific implementation details
    List<EmailResult> sendEmails( List<Email> emails);
    
    // STUB: Provider type name
    String getType();
    
}