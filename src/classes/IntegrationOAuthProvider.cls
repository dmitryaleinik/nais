global class IntegrationOAuthProvider implements Auth.AuthProviderPlugin {

    public String redirectUrl; // use this URL for the endpoint that the authentication provider calls back to for configuration
    private String key;
    private String secret;
    private String authUrl;    // application redirection to the Concur website for authentication and authorization
    private String accessTokenUrl; // uri to get the new access token from concur  using the GET verb
    private String customMetadataTypeApiName; // api name for the custom metadata type created for this auth provider
    private String userAPIUrl; // api url to access the user in concur
    private String userAPIVersionUrl; // version of the user api url to access data from concur
    private String userInfo;
    private String providerName;
    private String scope;
    private Map<String, Object> saoResponse;


    global String getCustomMetadataType() {
        return 'IntegrationOauth__mdt';
    }

    global PageReference initiate(Map<String, String> authProviderConfiguration, String stateToPropagate) {

        key = authProviderConfiguration.get('ConsumerKey__c');
        secret = authProviderConfiguration.get('ConsumerSecret__c');
        authUrl = authProviderConfiguration.get('AuthUrl__c');
        redirectUrl = authProviderConfiguration.get('RedirectUrl__c');
        scope = authProviderConfiguration.get('Scope__c');

        if( !String.isEmpty( scope)) {
            scope = EncodingUtil.urlEncode(scope, 'UTF-8');
        }

        // TODO: Set logging here, place a DEBUG option on the custom metadata Type.
        // Example endpoint:
        //String url = 'https://ssatbdev-test.apigee.net/oauth/authorize?response_type=code&client_id=ZT0eVWhkhqQGiDA1aA8Vbp25XBQxZkkp&redirect_uri=https://test.salesforce.com/services/authcallback/00DJ0000003O9R2MAK/SAO&scope=openid%20profile%20offline_access&state=' + stateToPropagate;
        String url = authUrl + '?response_type=code&client_id=' + key + '&scope=' + scope + '&redirect_uri=' + redirectUrl + '&state=' + stateToPropagate;
        return new PageReference(url);
    }

    global Auth.AuthProviderTokenResponse handleCallback(Map<String, String> authProviderConfiguration, Auth.AuthProviderCallbackState state ) {
        System.Debug('DEBUG:::handleCallback' + state);

        Map<String, String> queryParams = state.queryParameters;
        String code = queryParams.get('code');
        String sfdcState = queryParams.get('state');
        key = authProviderConfiguration.get('ConsumerKey__c');
        secret = authProviderConfiguration.get('ConsumerSecret__c');
        accessTokenUrl = authProviderConfiguration.get('TokenUrl__c');
        redirectUrl = authProviderConfiguration.get('RedirectUrl__c');
        providerName = authProviderConfiguration.get('ProviderName__c');

        String reqBody = 'grant_type=authorization_code';
        reqBody += '&code='+EncodingUtil.urlEncode(code, 'UTF-8');
        reqBody += '&client_id='+EncodingUtil.urlEncode(key, 'UTF-8');
        reqBody += '&client_secret='+EncodingUtil.urlEncode(secret, 'UTF-8');
        reqBody += '&redirect_uri='+EncodingUtil.urlEncode(redirectUrl, 'UTF-8');
        
        Map<String, String> httpHeaders = new Map<String, String>();
        httpHeaders.put('Content-Type', 'application/x-www-form-urlencoded');
        httpHeaders.put('Accept', 'application/json');

        // get response;
        String responseBody = HttpService.sendHttpRequest( accessTokenUrl, httpHeaders, reqBody, 'POST');
        if( responseBody.contains('fault')) {

            return new Auth.AuthProviderTokenResponse( providerName, null, 'oauthSecret', sfdcState);
        }

        saoResponse = ( Map<String, Object>) JSON.deserializeUntyped( responseBody);
        Map<String, Object> claims = (Map<String, Object>) JSON.deserializeUntyped(String.valueOf( saoResponse.get('claims')));

        List<String> userInfos;
        if( claims.get('sub') != null) {
            userInfos = String.valueOf(claims.get('sub')).replace('(', '').replace(')', '').split(',');
        }

        if( userInfos == null) {
            // throw exception;
        }

        userInfo = userInfos[0];
        System.debug('DEBUG:::handleCallbackuserInfo' + userInfo);

        // need to comma delimite this token so that we can get the auth token and user Id.
        String token = userInfo + ':' + String.valueOf( saoResponse.get('access_token'));

        // Provider Name will be the same as ProviderType you use when you call Auth.getAuthToken(Id, ProviderType);
        return new Auth.AuthProviderTokenResponse( providerName, token, 'oauthSecret', sfdcState);
    }

    global Auth.UserData getUserInfo( Map<string, string> authProviderConfiguration, Auth.AuthProviderTokenResponse response) {

        return new Auth.UserData(null, null, null, null, null,
                'link', null, null, providerName, null, new Map<String, String>());
    }
}