@isTest
private class SchoolDocumentsQuickViewControllerTest
{

    private class TestDataLocal
    {
        Account school1;
        Contact staff1, parentA, parentB, student1, student2;
        Id academicYearId;
        String academicYearName;
        PFS__c pfs;
        Applicant__c applicant1;
        Student_Folder__c studentFolder1;
        School_PFS_Assignment__c schoolPFS;
        Required_Document__c rd1;
        Family_Document__c fd1;
        School_Document_Assignment__c sda1;
        
        private TestDataLocal() {
            // school
            school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, null, false); 
            insert school1;       
        
            // school staff
            staff1 = TestUtils.createContact('Staff 1', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
            insert staff1;
            
            // parents and students
            parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
            parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
            student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
            student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
            insert new List<Contact> {parentA, parentB, student1, student2};

            // academic year
            TestUtils.createAcademicYears();
            Academic_Year__c ay = [Select Id, Name From Academic_Year__c where Name = :GlobalVariables.getCurrentYearString() limit 1];  
            academicYearId = ay.Id;
            academicYearName = ay.Name;
            
            // psf
            pfs = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
            pfs.Parent_B__c = parentB.Id;
            insert new List<PFS__c> {pfs};
            
            // applicant
            applicant1 = TestUtils.createApplicant(student1.Id, pfs.Id, false);
            insert new List<Applicant__c> {applicant1};
            
            // student folder
            studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
            insert studentFolder1;
            
            // school pfs assignment
            schoolPFS = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1.Id, school1.Id, studentFolder1.Id, false);
            insert schoolPFS;  
        }
    }
    
    @isTest
    private static void testLoadDocuments()
    {
        TestDataLocal TestDataLocal = new TestDataLocal();
        
        Required_Document__c requiredDocument = TestUtils.createRequiredDocument(TestDataLocal.academicYearId, TestDataLocal.school1.Id, false);
        requiredDocument.Document_Year__c = GlobalVariables.getCurrentYearString();
        requiredDocument.Document_Type__c = 'W2';        
        database.insert(requiredDocument);    
        
        Document_Type_Settings__c documentType = new Document_Type_Settings__c(Name='W2',
                                                                    Full_Type_Name__c='W2',
                                                                    Index_Order__c = 0,
                                                                    Tax_Document__c = true);
        Database.insert(documentType);

        String documentYear = GlobalVariables.getDocYearStringFromAcadYearName(GlobalVariables.getCurrentYearString());

        Family_Document__c familyDocument = new Family_Document__c(
                                            Name = 'doc1',
                                            Academic_Year_Picklist__c = GlobalVariables.getCurrentAcademicYear().Name,
                                            Date_Uploaded__c = system.today(),
                                            Date_Verified__c = system.today() ,
                                            Deleted__c = false,
                                            Filename__c = 'test',
                                            Import_Id__c = '030393',
                                            Duplicate__c = 'No',
                                            Document_Type__c = documentType.Full_Type_Name__c,
                                            Document_Year__c =     documentYear
                                            );
        Database.insert(familyDocument);
        
        TestDataLocal.schoolPFS.Withdrawn__c = 'No';
        TestDataLocal.schoolPFS.Academic_Year_Picklist__c = TestDataLocal.academicYearName;
        update TestDataLocal.schoolPFS;
        
        School_Document_Assignment__c sda1 = new School_Document_Assignment__c(
                                                School_PFS_Assignment__c =  TestDataLocal.schoolPFS.Id,
                                                Deleted__c = false,
                                                document__c = familyDocument.Id,
                                                Document_Type__c = documentType.Full_Type_Name__c,
                                                Document_Year__c = documentYear
                                                );
        Database.insert(sda1);

        sda1 = [Select Id, Academic_Year__c FROM School_Document_Assignment__c WHERE Id = :sda1.Id];
        
        Test.startTest();
        PageReference testPage = Page.SchoolPFSSummary;
        testPage.getParameters().put('schoolpfsassignmentid', TestDataLocal.schoolPFS.Id);
        
        Test.setCurrentPage(testPage);
        SchoolDocumentsQuickViewController quickViewDocuments = new SchoolDocumentsQuickViewController();
        system.assertEquals(TestDataLocal.schoolPFS.Id, quickViewDocuments.schoolpfsassignmentid);
        system.assertEquals(sda1.Academic_Year__c, TestDataLocal.schoolPFS.Academic_Year_Picklist__c);
        system.assertEquals(1, quickViewDocuments.listDocuments.size());
        system.assertEquals('test', quickViewDocuments.listDocuments[0].documentFileName);
        system.assertEquals(String.ValueOf(GlobalVariables.getDocYearStringFromAcadYearName(GlobalVariables.getCurrentYearString())), 
                            quickViewDocuments.listDocuments[0].documentYear);
        Test.stopTest();        
    }

    @isTest
    private static void testLoadDocumentsDeletedDoesntShow()
    {
        TestDataLocal TestDataLocal = new TestDataLocal();
        
        Required_Document__c requiredDocument = TestUtils.createRequiredDocument(TestDataLocal.academicYearId, TestDataLocal.school1.Id, false);
        requiredDocument.Document_Year__c = GlobalVariables.getCurrentYearString();
        requiredDocument.Document_Type__c = 'W2';       
        database.insert(requiredDocument);  
        
        Document_Type_Settings__c documentType = new Document_Type_Settings__c(Name='W2',
                                                                    Full_Type_Name__c='W2',
                                                                    Index_Order__c = 0,
                                                                    Tax_Document__c = true);
        Database.insert(documentType);

        String documentYear = GlobalVariables.getDocYearStringFromAcadYearName(GlobalVariables.getCurrentYearString());

        Family_Document__c familyDocument = new Family_Document__c(
                                            Name = 'doc1',
                                            Academic_Year_Picklist__c = GlobalVariables.getCurrentAcademicYear().Name,
                                            Date_Uploaded__c = system.today(),
                                            Date_Verified__c = system.today() ,
                                            Deleted__c = false,
                                            Filename__c = 'test',
                                            Import_Id__c = '030393',
                                            Duplicate__c = 'No',
                                            Document_Type__c = documentType.Full_Type_Name__c,
                                            Document_Year__c =  documentYear
                                            );
        Database.insert(familyDocument);
        
        TestDataLocal.schoolPFS.Withdrawn__c = 'No';
        TestDataLocal.schoolPFS.Academic_Year_Picklist__c = TestDataLocal.academicYearName;
        update TestDataLocal.schoolPFS;
        
        School_Document_Assignment__c sda1 = new School_Document_Assignment__c(
                                                School_PFS_Assignment__c =  TestDataLocal.schoolPFS.Id,
                                                Deleted__c = false,
                                                document__c = familyDocument.Id,
                                                Document_Type__c = documentType.Full_Type_Name__c,
                                                Document_Year__c = documentYear
                                                );
        Database.insert(sda1);
        
        sda1 = [Select Id, Academic_Year__c FROM School_Document_Assignment__c WHERE Id = :sda1.Id];
        
        Test.startTest();
        PageReference testPage = Page.SchoolPFSSummary;
        testPage.getParameters().put('schoolpfsassignmentid', TestDataLocal.schoolPFS.Id);
        
        Test.setCurrentPage(testPage);
        SchoolDocumentsQuickViewController quickViewDocuments = new SchoolDocumentsQuickViewController();
        system.assertEquals(TestDataLocal.schoolPFS.Id, quickViewDocuments.schoolpfsassignmentid);
        system.assertEquals(sda1.Academic_Year__c, TestDataLocal.schoolPFS.Academic_Year_Picklist__c);
        system.assertEquals(1, quickViewDocuments.listDocuments.size());
        system.assertEquals('test', quickViewDocuments.listDocuments[0].documentFileName);
        system.assertEquals(String.ValueOf(GlobalVariables.getDocYearStringFromAcadYearName(GlobalVariables.getCurrentYearString())), 
                            quickViewDocuments.listDocuments[0].documentYear);

        familyDocument.Deleted__c = true;
        update familyDocument;

        quickViewDocuments = new SchoolDocumentsQuickViewController();
        system.assertEquals(0, quickViewDocuments.listDocuments.size());
        Test.stopTest();        
    }
}