@isTest
private class SchoolUserAdminProfileControllerTest 
{

    public static User adminUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    public static Account school;
    public static User schoolStaffUser, staffUser1, staffUser2;
    public static contact schoolStaffContact, staff1, staff2;
    public static String testEmail;
    public static String schoolStaffUserEmail;
    public static CollaborationGroup testGroup1;

    public static void createTestData()
    {
        schoolStaffUserEmail = 'Nais2013Test@test.org';
        testEmail = 'test_nais_2013@unittestnais.com';
        list<Profile_Type_Grouping__c> profileTypeGrouping = new List<Profile_Type_Grouping__c>();
        String profileName = [Select Name from Profile where Id = :GlobalVariables.schoolPortalAdminProfileId][0].Name;
        profileTypeGrouping.add(new Profile_Type_Grouping__c(
                                            Name=profileName,
                                            Is_School_Admin_Profile__c=true,
                                            Is_School_Profile__c=true,
                                            Profile_Name__c=profileName));
        insert profileTypeGrouping;

        school = TestUtils.createAccount('Nais2013', RecordTypes.schoolAccountTypeId, 3, false);
        insert school;

        schoolStaffContact = TestUtils.createContact('Nais2013', school.Id, RecordTypes.schoolStaffContactTypeId, false);
        insert schoolStaffContact;

        schoolStaffUser = TestUtils.createPortalUser('Nais2013', schoolStaffUserEmail, 'sa1', schoolStaffContact.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);

        System.runAs(adminUser){
            insert schoolStaffUser;
        }

        testGroup1 = new CollaborationGroup();
        testGroup1.Name='Test Group 1';
        testGroup1.CollaborationType='Public'; //can be 'Public' or 'Private'
        insert testGroup1;

       insert new ChatterGroupAutoAssign__c(enabled__c = true, chattergroupid__c = testgroup1.id, name='Test Group 1');
    }//End:createTestData

    @isTest
    private static void caseMyProfilePage()
    {
        createTestData();
        NetworkMember m = [SELECT networkId, memberId FROM NetworkMember Limit 1][0];
        User u = [SELECT id FROM User WHERE id=:m.memberId];
        Test.startTest();
        System.runAs(u){
            PageReference thisPage = Page.schooluseradminprofile;
            thisPage.getParameters().put('o', '1');
            Test.setCurrentPage(thisPage);
            ApexPages.StandardController standardController = new ApexPages.StandardController(new Contact());
            SchoolUserAdminProfileController theController = new SchoolUserAdminProfileController(standardController);

            //Assert
            System.assertEquals(theController.currentUser.ContactId, theController.currentContact.Id);
        }
        Test.stopTest();
    }//End:myProfile


    @isTest
    private static void case1NewContact()
    {
        createTestData();
        SchoolUserAdminProfileController controller;


        Test.startTest();
        System.runAs(schoolStaffUser){
            PageReference thisPage = Page.schooluseradminprofile;
            Test.setCurrentPage(thisPage);
            ApexPages.StandardController standardController = new ApexPages.StandardController(new Contact());
            controller = new SchoolUserAdminProfileController(standardController);
            controller.currentContact.FirstName = 'Test Nais-2013';
            controller.currentContact.LastName = 'Test Nais-2013';
            controller.currentContact.Email = testEmail;
            controller.currentUserProfileId = null;
            controller.saveAction();
        }
        Test.stopTest();

        //Asserts
        User newUser;
        Contact newContact;
        try{
            newContact = [Select Id from Contact where Email=:testEmail limit 1];
            newUser = [Select Id from User where Email=:testEmail limit 1];
        }catch(Exception e){}
        System.assertNotEquals(newContact, null);
        System.assertEquals(newUser, null);
    }//End:case1NewContact

    @isTest
    private static void case2CreateContactWithPortalAccess()
    {
        createTestData();
        Id profileSchoolPortalAdmin = [Select Id, Name from Profile where Name = :ProfileSettings.SchoolAdminProfileName limit 1].Id;
        SchoolUserAdminProfileController controller;


        System.runAs(schoolStaffUser){
            PageReference thisPage = Page.schooluseradminprofile;
            Test.setCurrentPage(thisPage);
            ApexPages.StandardController standardController = new ApexPages.StandardController(new Contact());
            controller = new SchoolUserAdminProfileController(standardController);
            controller.currentContact.FirstName = 'Test Nais-2013';
            controller.currentContact.LastName = 'Test Nais-2013';
            controller.currentContact.Email = testEmail;
            controller.currentUserProfileId = profileSchoolPortalAdmin;
            Test.startTest();
            controller.saveAction();
            Test.stopTest();
        }

        User newUser;
        Contact newContact;
        try{
            newContact = [Select Id from Contact where Email=:testEmail limit 1];
            newUser = [Select Id from User where Email=:testEmail limit 1];
        }catch(Exception e){}
        System.assertNotEquals(newContact, null);
        System.assertNotEquals(newUser, null);
        controller.cancelAction();
    }//End:case2CreateContactWithPortalAccess

    @isTest
    private static void case3UpdateContactWithPortalAccessToNoPortalAccess()
    {
        createTestData();
        Id profileSchoolPortalAdmin = [Select Id, Name from Profile where Name= :ProfileSettings.SchoolAdminProfileName limit 1].Id;
        SchoolUserAdminProfileController controller;
        ApexPages.StandardController standardController;
        PageReference thisPage;

        System.runAs(schoolStaffUser){
            thisPage = Page.schooluseradminprofile;
            Test.setCurrentPage(thisPage);
            standardController = new ApexPages.StandardController(new Contact());
            controller = new SchoolUserAdminProfileController(standardController);
            controller.currentContact.FirstName = 'Test Nais-2013';
            controller.currentContact.LastName = 'Test Nais-2013';
            controller.currentContact.Email = testEmail;
            controller.currentUserProfileId = profileSchoolPortalAdmin;
            controller.currentContact.SSS_Main_Contact__c = true;
            Test.startTest();
            controller.validateContact();
            System.assertEquals(false,controller.showPopup);
            Test.stopTest();
        }

        User newUser;
        Contact newContact;
        try{
            newContact = [Select Id from Contact where Email=:testEmail limit 1];
            newUser = [Select Id, IsActive, ContactId from User where Email=:testEmail and IsActive=true limit 1];
        }catch(Exception e){}
        System.assertEquals(newContact.Id, controller.currentContact.Id);
        System.assertNotEquals(newUser, null); //newUser was inserted in a future methods, so at this point the controller has not set the currentUser


        thisPage = Page.schooluseradminprofile;
        thisPage.getParameters().put('id', newContact.Id);
        Test.setCurrentPage(thisPage);
        controller = new SchoolUserAdminProfileController(standardController);
        controller.editAction();
        controller.currentUserProfileId = null;
        controller.saveAction();
    }//End:case3UpdateContactWithPortalAccessToNoPortalAccess

    @isTest
    private static void case4NewContactWithDuplicatedEmail()
    {
        setupData2();
        
        String existingContactEmail = [SELECT Id, email FROM User WHERE Id =: staffUser1.Id].Email;
        System.assertNotEquals(null, existingContactEmail);
        
        SchoolUserAdminProfileController controller;

        Test.startTest();
            System.runAs(staffUser1)
            {
                List<Contact> existingContacts = new List<Contact>([SELECT Id FROM Contact WHERE email =: existingContactEmail]);
                System.assertEquals(1, existingContacts.size());
                
                PageReference thisPage = Page.schooluseradminprofile;
                Test.setCurrentPage(thisPage);
                ApexPages.StandardController standardController = new ApexPages.StandardController(new Contact());
                controller = new SchoolUserAdminProfileController(standardController);
                
                controller.currentContact.FirstName = 'Bill';
                controller.currentContact.LastName = 'Brown';
                controller.currentContact.Email = existingContactEmail;
                controller.saveAction();
                
                List<Apexpages.Message> msgs = ApexPages.getMessages();
                System.assertEquals(1, msgs.size(), msgs);
                System.assert(msgs[0].getDetail().contains(Label.SP_Duplicate_Contact_Email_Error_Message));
            }
        Test.stopTest();
    }

    @isTest
    private static void pageTest()
    {
        createTestData();
        Id profileSchoolPortalAdmin = [Select Id, Name from Profile where Name= :ProfileSettings.SchoolAdminProfileName limit 1].Id;
        Id profileSchoolPortalUser = [Select Id, Name from Profile where Name= :ProfileSettings.SchoolUserProfileName limit 1].Id;
        SchoolUserAdminProfileController controller;
        ApexPages.StandardController standardController;
        PageReference thisPage;

        System.runAs(schoolStaffUser){
            thisPage = Page.schooluseradminprofile;
            Test.setCurrentPage(thisPage);
            standardController = new ApexPages.StandardController(new Contact());
            controller = new SchoolUserAdminProfileController(standardController);
            controller.reloadPage();
            Test.startTest();
            controller.getIsMyProfilePage();
            controller.currentUserProfileId = profileSchoolPortalAdmin;
            controller.onChangeSelectedUserProfile();
            SchoolUserAdminProfileController.changeUserProfile(schoolStaffUser.Id, profileSchoolPortalUser);
            Test.stopTest();
        }
        User newUser;
        try{
            newUser = [Select Id, profileId from User where Email=:schoolStaffUserEmail and IsActive=true limit 1];
        }catch(Exception e){}
        System.assertEquals(profileSchoolPortalAdmin, newUser.profileId);
    }

    @isTest
    private static void testValidatePopup()
    {
        createTestData();
        schoolStaffContact.SSS_Main_Contact__c = true;
        update schoolStaffContact;
        Id profileSchoolPortalAdmin = [Select Id, Name from Profile where Name= :ProfileSettings.SchoolAdminProfileName limit 1].Id;
        SchoolUserAdminProfileController controller;
        System.runAs(schoolStaffUser){
            PageReference thisPage = Page.schooluseradminprofile;
            Test.setCurrentPage(thisPage);
            ApexPages.StandardController standardController = new ApexPages.StandardController(new Contact());
            controller = new SchoolUserAdminProfileController(standardController);
            controller.currentContact.FirstName = 'Test Nais-2015';
            controller.currentContact.LastName = 'Test Nais-2015';
            controller.currentContact.Email = testEmail;
            controller.currentContact.SSS_Main_Contact__c = true;
            controller.currentUserProfileId = profileSchoolPortalAdmin;
            Test.startTest();
            controller.validateContact();
            System.assertEquals(controller.showPopup, true);
            Test.stopTest();
        }
    }

    @isTest
    private static void testChatterNotifications()
    {
        createTestData();

        list<id> userIds = new list<id>();
        map<id,networkMember> idToNetworkMemberMap = new map<id,networkMember>();
        for(NetworkMember each : [SELECT networkId, memberId FROM NetworkMember LIMIT 25000]){
            userIds.add(each.memberId);
            idToNetworkMemberMap.put(each.memberId,each);
        }
        user testUser = [SELECT Id, Contactid from User where Id IN :userIds AND ContactId != null AND ProfileId IN :GlobalVariables.schoolPortalProfileIds AND IsActive = true ][0];
        id networkId = idToNetworkMemberMap.get(testUser.id).networkId;

        System.runAs(testUser){
            ApexPages.StandardController standardController = new ApexPages.StandardController(new Contact(id=testUser.contactId));
             SchoolUserAdminProfileController controller = new SchoolUserAdminProfileController(standardController);
            controller.getCurrentNetworkMember().member = idToNetworkMemberMap.get(testUser.id);
            controller.getCurrentNetworkMember().followsMe = false;
            controller.getCurrentNetworkMember().setMemberFields();
             update controller.getCurrentNetworkMember().member;
            list<NetworkMember> members = [SELECT NetworkId, DefaultGroupNotificationFrequency, DigestFrequency, PreferencesDisableAllFeedsEmail, PreferencesDisableBookmarkEmail, PreferencesDisableChangeCommentEmail, PreferencesDisableEndorsementEmail, PreferencesDisableFollowersEmail, PreferencesDisableLaterCommentEmail, PreferencesDisableLikeEmail, PreferencesDisableMentionsPostEmail, PreferencesDisableMessageEmail, PreferencesDisableProfilePostEmail, PreferencesDisableSharePostEmail, PreferencesDisCommentAfterLikeEmail, PreferencesDisMentionsCommentEmail, PreferencesDisProfPostCommentEmail
                                            FROM NetworkMember WHERE NetworkId=:networkId AND MemberId=:testUser.id];
            system.assertEquals(members[0].PreferencesDisableFollowersEmail, true);
        }
    }
    
    private static void setupData2() {
        
       //Create a School.
       Account school = AccountTestData.Instance.asSchool().insertAccount();
       
       //Test data to be used.
       String firstName1 = 'Jane';
       String firstName2 = 'Chris';
       String lastName1 = 'Cornell';
       String lastName2 = 'Cornell';
       String emailStr_1 = 'janecornell@testtt.com';
       String emailStr_2 = 'chriscornell2@testtt.com';
       
       //Create 2 School Portal Users.
       staff1 = ContactTestData.Instance
            .asSchoolStaff()
            .forFirstName('Jane')
            .forLastName('Cornell')
            .forAccount(school.Id)
            .forEmail(emailStr_1)
            .create();
       
       staff2 = ContactTestData.Instance
            .asSchoolStaff()
            .forFirstName('Chris')
            .forLastName('Cornell')
            .forAccount(school.Id)
            .forEmail(emailStr_2)
            .create();
            
       insert new List<Contact>{staff1, staff2};
       
       Id ContactId;
       
       staffUser1 = UserTestData.createSchoolPortalUser();
       staffUser2 = UserTestData.createSchoolPortalUser();
       
       //Insert portal users with system administrator users to avoid exceptions.
       User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       System.runAs(thisUser){
            
            staffUser1.ContactId = staff1.Id;
            staffUser1.FirstName = firstName1;
            staffUser1.LastName = lastName1;
            staffUser1.Email = emailStr_1;
            
            staffUser2.ContactId = staff2.Id;
            staffUser2.FirstName = firstName2;
            staffUser2.LastName = lastName2;
            staffUser2.Email = emailStr_2;
            
            insert new List<User>{staffUser1,staffUser2};
       }
    }
    
    @isTest
    private static void saveAction_updateUserProfile_userSuccessfullyUpdated()
    {
       setupData2();
       
       Id ContactId;
       
       System.runAs(staffUser1){
           
           staffUser1 = [SELECT Id, ContactId, Email, FirstName, LastName, ProfileId FROM User WHERE Email =: staffUser1.Email LIMIT 1];
           staffUser2 = [SELECT Id, ContactId, Email, FirstName, LastName, ProfileId FROM User WHERE Email =: staffUser2.Email LIMIT 1];
           
           System.assertNotEquals(null, staffUser1);
           System.assertNotEquals(null, staffUser2);
           
           System.assertEquals(GlobalVariables.schoolPortalAdminProfileId, staffUser1.ProfileId);
           System.assertEquals(GlobalVariables.schoolPortalAdminProfileId, staffUser2.ProfileId);
           
	       ContactId = staffUser2.ContactId;
        
           Test.startTest();
           
            System.assertNotEquals(null, ContactId);
			PageReference thisPage = Page.schooluseradminprofile;
			thisPage.getParameters().put('id', ContactId);
            thisPage.getParameters().put('return', '1');
			Test.setCurrentPage(thisPage);
			
			ApexPages.StandardController standardController = new ApexPages.StandardController(new Contact());			
			SchoolUserAdminProfileController controller = new SchoolUserAdminProfileController(standardController);
            
            System.assertEquals(ContactId, controller.currentContact.Id);
            System.assertNotEquals(null, staff2.Email);
            System.assertEquals(staff2.Email, controller.currentContact.Email);
            System.assertEquals(staff2.Email, controller.priorEmail);
            
            //Update partner user profile.
			controller.currentUserProfileId = GlobalVariables.schoolPortalUserProfileId;
			controller.saveAction();
			
			List<Apexpages.Message> msgs = ApexPages.getMessages();
            System.assertEquals(0, msgs.size(), msgs);
           Test.stopTest();    
       }
	    
	   User u = [SELECT Id, ProfileId FROM User WHERE ContactId =: ContactId LIMIT 1];
	    
	   System.assertEquals(GlobalVariables.schoolPortalUserProfileId, u.ProfileId);    
        	        
    }//End:saveAction_updateUserProfile_userSuccessfullyUpdated
}