/**
 * IntegrationRegistrationHandler
 *
 * @description Generic registration handler for Oauth/and OpenId.
 * Only time we need to do this is if we want true SSO, and you can signup/login via SSO.
 * Other than that, it's ok for it to return null;
 *
 * @author Mike Havrilla @ Presence PG
 */

global class IntegrationRegistrationHandler implements Auth.RegistrationHandler {

    global User createUser(Id portalId, Auth.UserData data){
        System.debug('DEBUG::: IntegrationRegistrationHandler:createUser' + data);
        return null;
    }

    global void updateUser(Id userId, Id portalId, Auth.UserData data) {
        System.debug('DEBUG::: IntegrationRegistrationHandler:updateUser' + data);

    }
}