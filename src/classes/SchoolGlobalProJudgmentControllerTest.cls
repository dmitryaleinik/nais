@isTest
private class SchoolGlobalProJudgmentControllerTest {

    private without sharing class LocalTestData {

        public User schoolPortalUser;
        public Id academicYearId;
        public Annual_Setting__c pastAnnualSetting;

        public LocalTestData() {
            Account school = AccountTestData.Instance
                    .forRecordTypeId(RecordTypes.schoolAccountTypeId)
                    .DefaultAccount;

            Contact schoolStuff = ContactTestData.Instance
                    .forAccount(school.Id)
                    .forRecordTypeId(RecordTypes.schoolStaffContactTypeId)
                    .DefaultContact;

            System.runAs(CurrentUser.getCurrentUser()) {
                schoolPortalUser = UserTestData.createSchoolPortalUser();
                schoolPortalUser.In_Focus_School__c = school.Id;
                insert schoolPortalUser;
            }

            insertAccountShare(school.Id, schoolPortalUser.Id);

            List<Academic_Year__c> accYears = AcademicYearTestData.Instance.insertAcademicYears(5);
            Academic_Year__c currentAcademicYear = GlobalVariables.getCurrentAcademicYear();
            academicYearId = currentAcademicYear.Id;
            String academicYearName = currentAcademicYear.Name;

            Contact student = ContactTestData.Instance
                    .forRecordTypeId(RecordTypes.studentContactTypeId)
                    .createContactWithoutReset();
            insert student;


            System.runAs(schoolPortalUser) {
                PFS__c pfs = PfsTestData.Instance
                        .forParentA(schoolStuff.Id)
                        .createPfsWithoutReset();
                Dml.WithoutSharing.insertObjects(new List<PFS__c>{ pfs });

                List<Applicant__c> applicants = new List<Applicant__c> {
                        ApplicantTestData.Instance.createApplicantWithoutReset(),
                        ApplicantTestData.Instance.createApplicantWithoutReset(),
                        ApplicantTestData.Instance.createApplicantWithoutReset()
                };
                Dml.WithoutSharing.insertObjects(applicants);

                Student_Folder__c studentFolder = StudentFolderTestData.Instance
                        .forStudentId(student.Id)
                        .createStudentFolderWithoutReset();
                Dml.WithoutSharing.insertObjects(new List<Student_Folder__c>{ studentFolder });


                Id academicYearPastId = GlobalVariables.getPreviousAcademicYear().Id;
                Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

                List<Annual_Setting__c> annualSettings = new List<Annual_Setting__c> {
                    AnnualSettingsTestData.Instance
                            .forSchoolId(school.Id)
                            .forAcademicYearId(academicYearPastId)
                            .createAnnualSettingsWithoutReset(),
                    AnnualSettingsTestData.Instance
                            .forSchoolId(school.Id)
                            .forAcademicYearId(academicYearId)
                            .createAnnualSettingsWithoutReset()
                };
                Dml.WithoutSharing.insertObjects(annualSettings);

                pastAnnualSetting = annualSettings[0];

                List<School_PFS_Assignment__c> pfsAssignments = new List<School_PFS_Assignment__c>();

                for (Integer i = 0; i < 3; i++) {
                    pfsAssignments.add(
                        SchoolPfsAssignmentTestData.Instance
                                .forSchoolId(school.Id)
                                .forStudentFolderId(studentFolder.Id)
                                .forAcademicYearPicklist(academicYearName)
                                .forApplicantId(applicants[i].Id)
                                .createSchoolPfsAssignmentWithoutReset()
                    );
                }

                Dml.WithoutSharing.insertObjects(pfsAssignments);
            }

            List<School_PFS_Assignment__c> pfsAssignmentsForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
            SchoolStaffShareAction.shareRecordsToSchoolUsers(pfsAssignmentsForSharing, null);
        }

        private void insertAccountShare(Id accountId, Id userId) {
            insert new AccountShare(
                    AccountId = AccountId,
                    CaseAccessLevel = 'Edit',
                    OpportunityAccessLevel = 'Edit',
                    AccountAccessLevel = 'Edit',
                    UserOrGroupId = userId
            );
        }
    }

    private static Map<Id, School_PFS_Assignment__c> getPfsAssignments(Id schoolId, String academicYearName) {
        return new Map<Id, School_PFS_Assignment__c>([
                SELECT Add_Deprec_Home_Bus_Expense__c, Use_Home_Equity__c,
                    Use_Home_Equity_Cap__c, Use_Housing_Index_Multiplier__c,
                    Use_Div_Int_to_Impute_Assets__c,
                    //ImputeRate,
                    Impute__c,
                    Apply_Minimum_Income__c, Minimum_Income_Amount__c,
                    Use_COLA__c, Override_Default_COLA_Value__c, Lock_Professional_Judgment__c,
                    Academic_Year_Picklist__c
                FROM School_PFS_Assignment__c
                WHERE School__c = :schoolId
                AND Academic_Year_Picklist__c = :academicYearName]);
    }

    private static List<School_PFS_Assignment__c> getPfsAssignmentsToAssert(Set<Id> pfsaIds) {
        return [
                SELECT Add_Deprec_Home_Bus_Expense__c,
                        Lock_Professional_Judgment__c,
                        Use_COLA__c, Override_Default_COLA_Value__c,
                        Apply_Minimum_Income__c, Minimum_Income_Amount__c,
                        Use_Div_Int_to_Impute_Assets__c, Use_Housing_Index_Multiplier__c,
                        Use_Home_Equity_Cap__c, Use_Home_Equity__c
                FROM School_PFS_Assignment__c
                WHERE Id IN :pfsaIds];
    }

    @isTest
    private static void testController_validData_testFutureAndCancel() {
        LocalTestData testData = new LocalTestData();

        System.runAs(testData.schoolPortalUser) {
            Test.startTest();
                Test.setCurrentPage(Page.SchoolGlobalProJudgment);
                SchoolGlobalProJudgmentController testCtrlr = new SchoolGlobalProJudgmentController();
                testCtrlr.pageLoad();
                System.assert(testCtrlr.myAnnualSettings != null);

                // Simulate clicking "Save Settings for Future PFSs Only"
                testCtrlr.lastButton = '';
                testCtrlr.saveSettings();

                // Simulate clicking cancel on the confirmation
                testCtrlr.lastButton = 'cancel';
                testCtrlr.saveSettings();
            Test.stopTest();
        }
    }

    @isTest
    private static void testController_validData_testLockDepreciation() {
        LocalTestData testData = new LocalTestData();

        System.runAs(testData.schoolPortalUser) {
            Test.startTest();
                Test.setCurrentPage(Page.SchoolGlobalProJudgment);
                SchoolGlobalProJudgmentController testCtrlr = new SchoolGlobalProJudgmentController();
                testCtrlr.pageLoad();
                System.assert(testCtrlr.myAnnualSettings != null);


                Map<Id, School_PFS_Assignment__c> pfsAssignments = getPfsAssignments(testCtrlr.mySchoolId, testCtrlr.myAcademicYear.Name);

                School_PFS_Assignment__c lockSPA = pfsAssignments.values()[0];
                lockSPA.Lock_Professional_Judgment__c = 'Yes';
                Database.update(lockSPA);

                // Test updating all SPAs
                testCtrlr.myAnnualSettings.Add_Depreciation_Home_Business_Expense__c = 'Yes';
                testCtrlr.lastButton = 'depreciation:Unlocked';
                testCtrlr.saveSettings();
            Test.stopTest();

            // Make sure the lastButton parameter was reset
            System.assertEquals('', testCtrlr.lastButton);

            // Check that only unlocked records were updated
            List<School_PFS_Assignment__c> pfsAssignmentsToAssert = getPfsAssignmentsToAssert(pfsAssignments.keySet());

            System.assertEquals(3, pfsAssignmentsToAssert.size());

            for (School_PFS_Assignment__c pfsAssignment : pfsAssignmentsToAssert) {
                if (pfsAssignment.Lock_Professional_Judgment__c == 'Yes') {
                    System.assertNotEquals(
                        testCtrlr.myAnnualSettings.Add_Depreciation_Home_Business_Expense__c,
                        pfsAssignment.Add_Deprec_Home_Bus_Expense__c
                    );
                } else {
                    System.assertEquals(
                        testCtrlr.myAnnualSettings.Add_Depreciation_Home_Business_Expense__c,
                        pfsAssignment.Add_Deprec_Home_Bus_Expense__c
                    );
                }
            }
        }
    }

    @isTest
    private static void testController_validData_testDeprecitation() {
        LocalTestData testData = new LocalTestData();

        System.runAs(testData.schoolPortalUser) {
            Test.startTest();
                Test.setCurrentPage(Page.SchoolGlobalProJudgment);
                SchoolGlobalProJudgmentController testCtrlr = new SchoolGlobalProJudgmentController();
                testCtrlr.pageLoad();
                System.assert(testCtrlr.myAnnualSettings != null);

                // This will be used in queries to check results later
                Map<Id, School_PFS_Assignment__c> pfsAssignments = getPfsAssignments(testCtrlr.mySchoolId, testCtrlr.myAcademicYear.Name);

                testCtrlr.myAnnualSettings.Add_Depreciation_Home_Business_Expense__c = 'No';
                testCtrlr.lastButton = 'depreciation:All';
                testCtrlr.saveSettings();
            Test.stopTest();

            // Check that all records were updated
            List<School_PFS_Assignment__c> pfsAssignmentsToAssert = getPfsAssignmentsToAssert(pfsAssignments.keySet());

            for (School_PFS_Assignment__c pfsAssignment : pfsAssignmentsToAssert) {
                System.assertEquals(
                    testCtrlr.myAnnualSettings.Add_Depreciation_Home_Business_Expense__c,
                    pfsAssignment.Add_Deprec_Home_Bus_Expense__c
                );
            }
        }
    }

    @isTest
    private static void testController_validData_testThreeUpdates() {
        LocalTestData testData = new LocalTestData();

        System.runAs(testData.schoolPortalUser) {
            Test.startTest();
                Test.setCurrentPage(Page.SchoolGlobalProJudgment);
                SchoolGlobalProJudgmentController testCtrlr = new SchoolGlobalProJudgmentController();
                testCtrlr.pageLoad();
                System.assert(testCtrlr.myAnnualSettings != null);

                // This will be used in queries to check results later
                Map<Id, School_PFS_Assignment__c> pfsAssignments = getPfsAssignments(testCtrlr.mySchoolId, testCtrlr.myAcademicYear.Name);

                testCtrlr.myAnnualSettings.Use_Home_Equity__c = 'No';
                testCtrlr.lastButton = 'homeequity:All';
                testCtrlr.saveSettings();

                testCtrlr.myAnnualSettings.Use_Housing_Index_Multiplier__c = 'Yes';
                testCtrlr.lastButton = 'housingindex:All';
                testCtrlr.saveSettings();

                testCtrlr.myAnnualSettings.Use_Home_Equity_Cap__c = 'Yes';
                testCtrlr.lastButton = 'homeequitycap:All';
                testCtrlr.saveSettings();
            Test.stopTest();

            // Check that records were updated correctly
            List<School_PFS_Assignment__c> pfsAssignmentsToAssert = getPfsAssignmentsToAssert(pfsAssignments.keySet());

            // Check that all of the editable fields were updated
            for (School_PFS_Assignment__c pfsAssignment : pfsAssignmentsToAssert) {
                System.assertEquals(
                    testCtrlr.myAnnualSettings.Use_Housing_Index_Multiplier__c,
                    pfsAssignment.Use_Housing_Index_Multiplier__c
                );
                System.assertEquals(
                    testCtrlr.myAnnualSettings.Use_Home_Equity_Cap__c,
                    pfsAssignment.Use_Home_Equity_Cap__c
                );
                System.assertEquals(
                    testCtrlr.myAnnualSettings.Use_Home_Equity__c,
                    pfsAssignment.Use_Home_Equity__c
                );
            }
        }
    }

    @isTest
    private static void testController_validData_testThreeMoreUpdates() {
        LocalTestData testData = new LocalTestData();

        System.runAs(testData.schoolPortalUser) {
            Test.startTest();
                Test.setCurrentPage(Page.SchoolGlobalProJudgment);
                SchoolGlobalProJudgmentController testCtrlr = new SchoolGlobalProJudgmentController();
                testCtrlr.pageLoad();
                System.assert(testCtrlr.myAnnualSettings != null);

                // This will be used in queries to check results later
                Map<Id, School_PFS_Assignment__c> pfsAssignments = getPfsAssignments(testCtrlr.mySchoolId, testCtrlr.myAcademicYear.Name);

                testCtrlr.myAnnualSettings.Use_Dividend_Interest_to_Impute_Assets__c = 'Yes';
                testCtrlr.myAnnualSettings.Impute_Rate__c = 0.25;
                testCtrlr.lastButton = 'impute:All';
                testCtrlr.saveSettings();

                testCtrlr.myAnnualSettings.Apply_Minimum_Income__c = 'Yes';
                testCtrlr.myAnnualSettings.Minimum_Income_Amount__c = 2000;
                testCtrlr.lastButton = 'minincome:All';
                testCtrlr.saveSettings();

                testCtrlr.myAnnualSettings.Use_Cost_of_Living_Adjustment__c = 'Yes';
                testCtrlr.myAnnualSettings.Override_Default_COLA_Value__c = 0.75;
                testCtrlr.lastButton = 'cola:All';
                testCtrlr.saveSettings();
            Test.stopTest();

            // Check that records were updated correctly
            List<School_PFS_Assignment__c> pfsAssignmentsToAssert = getPfsAssignmentsToAssert(pfsAssignments.keySet());

            // Check that all of the editable fields were updated
            for (School_PFS_Assignment__c pfsAssignment : pfsAssignmentsToAssert) {
                System.assertEquals(
                    testCtrlr.myAnnualSettings.Use_Dividend_Interest_to_Impute_Assets__c,
                    pfsAssignment.Use_Div_Int_to_Impute_Assets__c
                );
                System.assertEquals(
                    testCtrlr.myAnnualSettings.Apply_Minimum_Income__c,
                    pfsAssignment.Apply_Minimum_Income__c
                );
                System.assertEquals(
                    testCtrlr.myAnnualSettings.Use_Cost_of_Living_Adjustment__c,
                    pfsAssignment.Use_COLA__c
                );
            }
        }
    }

    @isTest
    private static void testController_validData_testPageValidation() {
        LocalTestData testData = new LocalTestData();

        System.runAs(testData.schoolPortalUser) {
            Test.startTest();
                Test.setCurrentPage(Page.SchoolGlobalProJudgment);
                SchoolGlobalProJudgmentController testCtrlr = new SchoolGlobalProJudgmentController();
                testCtrlr.pageLoad();

                testCtrlr.myAnnualSettings.Apply_Minimum_Income__c = 'Yes';
                testCtrlr.myAnnualSettings.Minimum_Income_Amount__c = 0;

                testCtrlr.myAnnualSettings.Use_Dividend_Interest_to_Impute_Assets__c = 'Yes';
                testCtrlr.myAnnualSettings.Impute_Rate__c = 0;

                testCtrlr.myAnnualSettings.Use_Cost_of_Living_Adjustment__c = 'Yes';
                testCtrlr.myAnnualSettings.COLA__c = null;
                testCtrlr.myAnnualSettings.Override_Default_COLA_Value__c = 0;

                testCtrlr.lastButton = '';
                testCtrlr.saveSettings();

                System.assert(ApexPages.getMessages().size() == 3);

                ApexPages.currentPage().getParameters().put('isColaAction', 'Yes');
                testCtrlr.myAnnualSettings.COLA__c = '000000000000000000';
                testCtrlr.myAnnualSettings.Override_Default_COLA_Value__c = 100;

                testCtrlr.lastButton = '';
                testCtrlr.saveSettings();

                System.assertEquals(4, ApexPages.getMessages().size());
            Test.stopTest();
        }
    }

    @isTest
    private static void testController_validData_testCopyPJSetting() {
        LocalTestData testData = new LocalTestData();

        System.runAs(testData.schoolPortalUser) {
            Test.startTest();
                testData.pastAnnualSetting.Apply_Minimum_Income__c = 'Yes';
                testData.pastAnnualSetting.Add_Depreciation_Home_Business_Expense__c = 'Yes';
                update testData.pastAnnualSetting;

                PageReference pageRef = Page.SchoolGlobalProJudgment;
                pageRef.getParameters().put('academicyearid', testData.academicYearId);
                Test.setCurrentPage(pageRef);
                SchoolGlobalProJudgmentController controller = new SchoolGlobalProJudgmentController();
                controller.pageLoad();
                controller.buttonMode = 'Future';
                controller.copyPJSetting();

                Annual_Setting__c annualSetting = GlobalVariables.getCurrentAnnualSettings(false, testData.academicYearId).get(0);
                System.assertEquals(annualSetting.Apply_Minimum_Income__c, 'Yes');
            Test.stopTest();
        }
    }

    @isTest
    private static void testController_validData_testOther() {
        LocalTestData testData = new LocalTestData();

        System.runAs(testData.schoolPortalUser) {
            Test.startTest();
                Test.setCurrentPage(Page.SchoolGlobalProJudgment);
                SchoolGlobalProJudgmentController testCtrlr = new SchoolGlobalProJudgmentController();
                testCtrlr.pageLoad();

                System.assertEquals(null, testCtrlr.isCopyPJSetting);
                System.assertEquals(false, testCtrlr.prevUsesCOLARecord);

                System.assertEquals('', testCtrlr.prevCOLAClass);

                testCtrlr.cancel();
                testCtrlr.updateImputeRate();

                System.assertEquals(testData.academicYearId, testCtrlr.getAcademicYearId());

                testCtrlr.SchoolAcademicYearSelector_OnChange(false);
            Test.stopTest();
        }
    }

    @isTest
    private static void testTellMeMoreDocumentId() {
        SchoolGlobalProJudgmentController controller = new SchoolGlobalProJudgmentController();
        Id tellMeMoreDocumentId = controller.tellMeMoreDocumentId;

        System.assert(String.isBlank(tellMeMoreDocumentId));

        Test.startTest();
            Document doc = DocumentTestData.Instance
                .forDocumentDeveloperName(DocumentLinkMappingService.documentInfo.Document_Developer_Name__c).insertDocument();

            controller = new SchoolGlobalProJudgmentController();
            tellMeMoreDocumentId = controller.tellMeMoreDocumentId;

            System.assertEquals(DocumentLinkMappingService.documentId, tellMeMoreDocumentId);
        Test.stopTest();
    }
}