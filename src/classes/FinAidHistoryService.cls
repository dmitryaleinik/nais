/**
 * @description This class is responsible to retrieve financial aid data from previous years.
 */
public class FinAidHistoryService {
     /**
     * @description Constructor for an instance of the FinAidHistoryService.
     */
    private FinAidHistoryService() { }
    
    /**
     * @description Queries student folder records by student, school and academic year.
     * @param studentId The id of the student for which the StudentFolder records will be query.
     * @param schoolId The id of the school for which the StudentFolder records will be query.
     * @param currentAcademicYearName The name of the Academic Year for which the StudentFolder records will be query.
     * @return A FinAidInfo record.
     */
    public FinAidInfo getPreviousAwards(
        Id studentId, Id schoolId, String currentAcademicYearName) {
        
        if(studentId != null && schoolId != null && currentAcademicYearName != null) {
        
	        List<Student_Folder__c> folders = StudentFolderSelector.newInstance()
	            .selectByStudentSchoolAndYearWithBudgetAllocations(studentId, schoolId, currentAcademicYearName);
	            
	        if(folders != null && !folders.isEmpty()) {
	            
	            return new FinAidInfo(folders[0]);
	        }
        }
        
        return new FinAidInfo();
    }
    
    /**
     * @description Creates a new instance of the FinAidHistoryService.
     * @return A FinAidHistoryService.
     */
    public static FinAidHistoryService Instance {
        get {
            if (Instance == null) {
                Instance = new FinAidHistoryService();
            }
            return Instance;
        }
        private set;
    }
    
    /* Start: Inner Clases */
    
    public class FinAidInfo{
        
        public Id StudentId { get; set; }
        public Id SchoolId { get; set; }
        public String AcademicYearName { get; set; }
        public Student_Folder__c folder { get; set; }
        public List<Budget_Allocation__c> budgetAllocations { get; set; }
        private Decimal totalBudgetAllocations;
        
        private FinAidInfo() { }
        
        private FinAidInfo(Student_Folder__c f) {
            
            folder = f;
            StudentId = folder.Student__c;
            SchoolId = folder.School__c;
            AcademicYearName = folder.Academic_Year_Picklist__c;
            budgetAllocations = folder.Budget_Allocations__r;
        }
        
        /**
	     * @description Summarize the total awards from Budget Allocation records.
	     * @return The total award allocated.
	     */
        public Decimal getTotalAward() {
            
            totalBudgetAllocations = 0;
            if( getHasBudgetAllocations() ) {
                
                for(Budget_Allocation__c ba : budgetAllocations) {
                    totalBudgetAllocations += ba.Amount_Allocated__c;
                }
            }
            
            return totalBudgetAllocations;
        }
        
        /**
         * @description Indicates if there's Budget Allocation records.
         * @return True if there are Budget Allocation records.
         */
        public Boolean getHasBudgetAllocations() {
            
            return folder != null && !budgetAllocations.isEmpty();
        }
    }
    
    /* End: Inner Clases */
}