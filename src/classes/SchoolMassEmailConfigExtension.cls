/**
 * SchoolMassEmailConfigExtension.cls
 *
 * @description: Server side extension for Mass Email configuration, handles interactions w/ data model and view state manipulation.
 *
 * @author: Chase Logan @ Presence PG
 */
public class SchoolMassEmailConfigExtension implements SchoolAcademicYearSelectorInterface {

    /* member vars and properties */
    public Boolean massEmailEnabled {

        get {
            // Handle app enabled/disabled
            return MassEmailUtil.isMassEmailGloballyEnabledByAccount( 
                        GlobalVariables.getCurrentSchoolId(), UserInfo.getProfileId());
        }
        private set;
    }
    public Boolean renderPreview { get; set; }
    public Attachment attachment { get; set; }
    public AccountModel acctModel { get; set; }

    public static final String QUERY_STRING_PREVIEW_MSG = 'preview';

    private final String DEFAULT_FOLDER_ID = 'DefaultDocumentFolderId';
    private final String LOGO_FAILED_MSG = 'Unable to update logo, please try again.';
    private final String PREVIEW_TRUE = '1';
    private final String PREVIEW_FALSE = '0';
    // default ctor
    public SchoolMassEmailConfigExtension( ApexPages.StandardController stdController) {
        
        this.init();
    }

    // attempt to save changes on Account record
    public PageReference saveChanges() {

        if ( this.acctModel != null) {

            this.acctModel.acct.Mass_Email_Reply_To__c = 
                ( String.isEmpty( this.acctModel.replyToAddress) ? null : this.acctModel.replyToAddress);
            this.acctModel.acct.Mass_Email_Footer__c = 
                ( String.isEmpty( this.acctModel.footer) ? null : this.acctModel.footer);
            
            // Logo saved as a new Attachment on Account record, and as a Document for public accessibility,
            // update logo name field with name of attachment
            if ( this.attachment != null && this.attachment.Name != null) {

                this.attachment.parentid = this.acctModel.acct.Id;
                this.acctModel.acct.Mass_Email_Logo_Name__c = this.attachment.Name;

                SavePoint sp = Database.setSavePoint();
                try {
                    
                    Mass_Email_Setting__mdt massEmailMdt = [select Value__c
                                                              from Mass_Email_Setting__mdt 
                                                             where DeveloperName = :this.DEFAULT_FOLDER_ID];
                    Document d = new Document(); 
                    d.Name = this.attachment.Name;
                    d.Body = this.attachment.Body;
                    d.IsPublic = true;
                    d.FolderId = massEmailMdt.Value__c;
                    
                    insert this.attachment;
                    insert d;
                } catch ( Exception e) {

                    System.debug( 'DEBUG:::error occurred in SchoolMassEmailConfigExtension.saveChanges(), message: ' +
                                    e.getMessage() + e.getStackTraceString());
                    Database.rollback( sp);
                    ApexPages.AddMessage( new ApexPages.Message( ApexPages.Severity.ERROR, this.LOGO_FAILED_MSG));
                }
            }

            this.acctModel.save();
            this.init();
        }

        return null;
    }

    // save message content and redirect to show preview modal
    // work around for no support of rerender on rich text areas
    public PageReference executePreview() {

        this.saveChanges();

        PageReference pRef = Page.SchoolMassEmailConfig;
        pRef.getParameters().put( SchoolMassEmailConfigExtension.QUERY_STRING_PREVIEW_MSG, this.PREVIEW_TRUE);
        pRef.setRedirect( true);

        return pRef;
    }

    // hide preview modal
    public PageReference hidePreview() {

        this.renderPreview = false;

        PageReference pRef = Page.SchoolMassEmailConfig;
        pRef.getParameters().put( SchoolMassEmailConfigExtension.QUERY_STRING_PREVIEW_MSG, this.PREVIEW_FALSE);
        pRef.setRedirect( true);

        return pRef;
    }


    // page initialization
    private void init() {

        Account acct = new AccountDataAccessService().getAccountById( GlobalVariables.getCurrentSchoolId());
        if ( acct != null) {

            this.acctModel = new AccountModel( acct, acct.Mass_Email_Reply_To__c, 
                    acct.Mass_Email_Footer__c, acct.Mass_Email_Logo_Name__c);
        }
        
        academicyearid = ApexPages.currentPage().getParameters().get('academicyearid');

        this.attachment = new Attachment();

        // grab query string param(s) in case this is a preview
        String displayPreview = 
            ApexPages.currentPage().getParameters().get( SchoolMassEmailConfigExtension.QUERY_STRING_PREVIEW_MSG);

        // render preview if this a preview redirect
        if ( String.isNotEmpty( displayPreview) && displayPreview == this.PREVIEW_TRUE) {

            this.renderPreview = true;
        }
    }

    /* UI wrapper class */

    public class AccountModel {

        private Account acct;
        public String replyToAddress { get; set; }
        public String footer { get; set; }
        public String logoName { get; set; }
        public String logoURL { get; set; }

        public AccountModel() {}

        // overridden ctor for constructing new AccountModel instances
        public AccountModel( Account acct, String replyToAddress, String footer, String logoName) {

            if ( acct != null) {
                String BASE_LOGO_URL = 'LogoBaseURL';

                this.acct = acct;
                this.replyToAddress = ( String.isNotEmpty( replyToAddress) ? replyToAddress : this.replyToAddress);
                this.footer = ( String.isNotEmpty( footer) ? footer : this.footer);

                if ( String.isNotEmpty( logoName)) {

                    this.logoName = logoName;
                    this.logoURL = new MassEmailController().constructLogoURL( logoName);
                }
            }
        }

        // attempt persistance back to data model
        public void save() {

            try {

                upsert this.acct;
            } catch ( Exception e) {

                System.debug( 'DEBUG:::exception occurred in AccountModel.save(), message:' + e.getMessage());
            }
        }

    }


    /* 
     * Solely for supporting header and academic year selector, copied from existing code to stay consistent
     */
       public Id academicyearid;
    public Academic_Year__c currentAcademicYear { get; private set; }

    public SchoolMassEmailConfigExtension Me {
        get { return this; }
    }
    public String getAcademicYearId() {
        return academicyearid;
    }
    public void setAcademicYearId( String academicYearIdParam) {
        academicyearid = academicYearIdParam;
        currentAcademicYear = GlobalVariables.getAcademicYear( academicyearid);
    }
    // on change of academic year, reload page with new folder
    public PageReference SchoolAcademicYearSelector_OnChange( Boolean saveRecord) {
        String pageName = '/apex/'+SchoolPagesUtils.getPageName(ApexPages.currentPage());
        PageReference newPage = new PageReference(pageName);
        newPage.getParameters().put( 'academicyearid', getAcademicYearId());
        newPage.setRedirect( true);
        return newPage;
    }
    // Due to the selector_onchange not refreshing properly, we redirect to the same page with academicyearid parameter.
    public void loadAcademicYear() {
        Map<String, String> parameters = ApexPages.currentPage().getParameters();
        String academicYearId = null;
        if ( parameters.containsKey( 'academicyearid'))
            setAcademicYearId(parameters.get( 'academicyearid'));
        else {
            setAcademicYearId( GlobalVariables.getCurrentAcademicYear().Id);
        }
    }
    /* end header support */

}