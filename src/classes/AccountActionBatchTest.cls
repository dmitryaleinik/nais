@isTest
private class AccountActionBatchTest {
    @isTest
    private static void subscriptionStatusRolledUpToAllSchoolAccounts() {
        Account a1 = TestUtils.createAccount('School 1', RecordTypes.schoolAccountTypeId, 2, false);
        Account a2 = TestUtils.createAccount('School 2', RecordTypes.schoolAccountTypeId, 2, false);
        Account a3 = TestUtils.createAccount('Access Org', RecordTypes.accessOrgAccountTypeId, 2, false);
        List<Account> accounts = new List<Account> { a1, a2, a3 };
        insert accounts;

        // account a1 - 1 'Not Started Yet', 1 'Current' subscription
        Subscription__c s11 = TestUtils.createSubscription(a1.Id, Date.today().addDays(3), Date.today().addYears(1), 'No', 'Extended', false);
        Subscription__c s12 = TestUtils.createSubscription(a1.Id, Date.today().addDays(-3), Date.today().addYears(1), 'No', 'Full', false);

        // account a2 - 1 'Expired', 1 'Cancelled' subscription
        Subscription__c s21 = TestUtils.createSubscription(a2.Id, Date.today().addYears(-1), Date.today().addDays(-1), 'No', 'Waiver Distributor', false);
        Subscription__c s22 = TestUtils.createSubscription(a2.Id, Date.today().addDays(-3), Date.today().addYears(1), 'Yes', 'Basic', false);

        // account s3 - 1 'Not Started Yet' subscription
        Subscription__c s31 = TestUtils.createSubscription(a3.Id, Date.today().addDays(3), Date.today().addYears(1), 'No', 'Extended', false);

        insert new List<Subscription__c> { s11, s12, s21, s22, s31 };

        // blank out subscription status and type on accounts
        a1.SSS_Subscriber_Status__c = null;
        a1.SSS_Subscription_Type_Current__c = null;
        a2.SSS_Subscriber_Status__c = null;
        // NAIS-2373 maintain prior subscription type after expiration
        a2.SSS_Subscription_Type_Current__c = 'Waiver Distributor';
        a3.SSS_Subscriber_Status__c = null;
        a3.SSS_Subscription_Type_Current__c = null;

        update accounts;

        // run batch Apex to rollup subscription status and type to all accounts
        Test.startTest();
        Database.executeBatch(new AccountActionBatch());
        Test.stopTest();

        Map<Id, Account> updatedAccounts = new Map<Id, Account>(
                [select Id, SSS_Subscriber_Status__c, SSS_Subscription_Type_Current__c from Account where Id in :accounts] );

        System.assertEquals('Current', updatedAccounts.get(a1.Id).SSS_Subscriber_Status__c);
        System.assertEquals('Full', updatedAccounts.get(a1.Id).SSS_Subscription_Type_Current__c);
        System.assertEquals('Former', updatedAccounts.get(a2.Id).SSS_Subscriber_Status__c);
        // NAIS-2373 maintain prior subscription type after expiration
        System.assertEquals('Waiver Distributor', updatedAccounts.get(a2.Id).SSS_Subscription_Type_Current__c);
        System.assertEquals('Never', updatedAccounts.get(a3.Id).SSS_Subscriber_Status__c);
        System.assertEquals(null, updatedAccounts.get(a3.Id).SSS_Subscription_Type_Current__c);
    }
}