public with sharing class TableAssetProgressivity
{
    
    private static Map<Id, List<Asset_Progressivity__c>> assetProgressivityDataByYear = new Map<Id, List<Asset_Progressivity__c>>();
    
    public static List<Asset_Progressivity__c> getAssetProgressivityData(Id academicYearId)
    {
        List<Asset_Progressivity__c> assetProgressivityData = assetProgressivityDataByYear.get(academicYearId);
        if (assetProgressivityData == null) {
            assetProgressivityData = [SELECT Discretionary_Net_Worth_Low__c, Discretionary_Net_Worth_High__c,
                                            Index__c 
                                        FROM Asset_Progressivity__c 
                                        WHERE Academic_Year__c = :academicYearId
                                        ORDER BY Discretionary_Net_Worth_Low__c ASC];
            assetProgressivityDataByYear.put(academicYearId, assetProgressivityData);
        } 
        return assetProgressivityData;
    }
    
    public static Decimal calculateIncomeSupplement(Id academicYearId, Decimal discretionaryNetWorth, Decimal conversionCoefficient)
    {
        if (discretionaryNetWorth == null) {
            throw new EfcException('Missing discretionary net worth for Income Supplement asset progressivity lookup');
        }
        if (conversionCoefficient == null) {
            throw new EfcException('Missing conversion coefficient for Income Supplement asset progressivity lookup');
        }
        Decimal incomeSupplement = 0;
        List<Asset_Progressivity__c> assetProgressivityData = getAssetProgressivityData(academicYearId);
        if (assetProgressivityData.isEmpty()) {
            throw new EfcException.EfcMissingDataException('No Asset Progressivity data found for academic year \'' + EfcUtil.getAcademicYearName(academicYearId) + '\'');
        }
        else {
            for (Asset_Progressivity__c assetProgressivity : assetProgressivityData) {
                if (discretionaryNetWorth >= assetProgressivity.Discretionary_Net_Worth_Low__c) {
                    Decimal netWorthSegment = 0;
                    if (assetProgressivity.Discretionary_Net_Worth_High__c == null) {
                        netWorthSegment = discretionaryNetWorth - assetProgressivity.Discretionary_Net_Worth_Low__c + 1;
                    }
                    else {
                        if (discretionaryNetWorth > assetProgressivity.Discretionary_Net_Worth_High__c) {
                            netWorthSegment = assetProgressivity.Discretionary_Net_Worth_High__c - assetProgressivity.Discretionary_Net_Worth_Low__c + 1;
                        }
                        else {
                            netWorthSegment = discretionaryNetWorth - assetProgressivity.Discretionary_Net_Worth_Low__c + 1;
                        }
                    }
                    incomeSupplement = incomeSupplement + (netWorthSegment * assetProgressivity.Index__c * conversionCoefficient/100);
                }
            }
        }
        return incomeSupplement.round(RoundingMode.HALF_UP);
    } 
}