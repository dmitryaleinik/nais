/**
 * @description This class is used to create KnowledgeArticleVersions records for unit tests.
 */
@isTest
public class KnowledgeAVTestData extends SObjectTestData
{

    private static final String KAV_VALIDATION_STATUS_VALIDATED = 'Validated';
    private static final String KAV_TEST_SUMMARY = 'This is a Test Knowledge Article created for Unit Tests';
    private static final String KAV_TEST_LANGUAGE = 'en_US';
    private static String kavTestTitle = 'Test Knowledge Article ';
    private static String kavTestUrlName = 'test-knowledge-article-';
    private static String kavTestArticleBody = 'Test';
    private static Integer sequence = 0;
    private static Integer uniqueNumber = 100000;
    
    /**
     * @description Get the default values for the KnowledgeArticleVersion object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap()
    {
        uniqueNumber += 1;
        String uniqueNumberString = String.valueOf(uniqueNumber);

        return new Map<Schema.SObjectField,Object> {
            Knowledge__kav.Title => kavTestTitle + uniqueNumberString,
            Knowledge__kav.UrlName => kavTestUrlName + uniqueNumberString,
            Knowledge__kav.ValidationStatus => KAV_VALIDATION_STATUS_VALIDATED,
            Knowledge__kav.Summary => KAV_TEST_SUMMARY,
            Knowledge__kav.Language => KAV_TEST_LANGUAGE,
            Knowledge__kav.Article_Body__c => kavTestArticleBody
        };
    }

    /**
     * @description Add to the Knowledge__kav Article_Body__c field substring.
     * @param valueToAppendToBody The value that should be added to Knowledge__kav Article_Body__c.
     * @return The current instance of KnowledgeAVTestData.
     */
    public KnowledgeAVTestData asConcatenatedArticleBody(String valueToAppendToBody) {
        return (KnowledgeAVTestData) with(Knowledge__kav.Article_Body__c, Knowledge__kav.Article_Body__c + valueToAppendToBody);
    }

    /**
     * @description Handle setting values that can't be assumed/static
     *              for the default value map.
     * @param sObj The current Knowledge__kav record being operated on.
     */
    protected override void beforeInsert(SObject sObj)
    {
        uniqueNumber += 1;
        String uniqueNumberString = String.valueOf(uniqueNumber);

        Knowledge__kav knowledgeAV = (Knowledge__kav)sObj;
        knowledgeAV.Sequence__c = sequence++;
        knowledgeAV.Title = kavTestTitle + uniqueNumberString;
        knowledgeAV.UrlName = kavTestUrlName + uniqueNumberString;
    }

    /**
     * @description Set the ValidationStatus of the KnowledgeArticleVersion record.
     * @param validationStatus The ValidationStatus to set the current KnowledgeArticleVersion record to.
     * @return The current instance of KnowledgeAVTestData.
     */
    public KnowledgeAVTestData forValidationStatus(String validationStatus) {
        return (KnowledgeAVTestData) with(Knowledge__kav.ValidationStatus, validationStatus);
    }

    /**
     * @description Set the Title of the KnowledgeArticleVersion record.
     * @param title The Title to set the current KnowledgeArticleVersion record to.
     * @return The current instance of KnowledgeAVTestData.
     */
    public KnowledgeAVTestData forTitle(String title) {
        return (KnowledgeAVTestData) with(Knowledge__kav.Title, title);
    }

    /**
     * @description Set the UrlName of the KnowledgeArticleVersion record.
     * @param urlName The UrlName to set the current KnowledgeArticleVersion record to.
     * @return The current instance of KnowledgeAVTestData.
     */
    public KnowledgeAVTestData forUrlName(String urlName) {
        return (KnowledgeAVTestData) with(Knowledge__kav.UrlName, urlName);
    }

    /**
     * @description Set the Summary of the KnowledgeArticleVersion record.
     * @param summary The Summary to set the current KnowledgeArticleVersion record to.
     * @return The current instance of KnowledgeAVTestData.
     */
    public KnowledgeAVTestData forSummary(String summary) {
        return (KnowledgeAVTestData) with(Knowledge__kav.Summary, summary);
    }

    /**
     * @description Set the Sequence__c of the KnowledgeArticleVersion record.
     * @param sequence The Sequence__c to set the current Knowledge record to.
     * @return The current instance of KnowledgeAVTestData.
     */
    public KnowledgeAVTestData forSequence(Integer customSequence) {
        return (KnowledgeAVTestData) with(Knowledge__kav.Sequence__c, customSequence);
    }

    /**
     * @description Set the Article_Body__c of the KnowledgeArticleVersion record.
     * @param articleBody The Article_Body__c to set the current Knowledge record to.
     * @return The current instance of KnowledgeAVTestData.
     */
    public KnowledgeAVTestData forArticleBody(String articleBody) {
        return (KnowledgeAVTestData) with(Knowledge__kav.Article_Body__c, articleBody);
    }

    /**
     * @description Set the Language of the KnowledgeArticleVersion record.
     * @param language The Language to set the current Knowledge record to.
     * @return The current instance of KnowledgeAVTestData.
     */
    public KnowledgeAVTestData forLanguage(String language) {
        return (KnowledgeAVTestData) with(Knowledge__kav.Language, language);
    }

    /**
     * @description Insert the current working KnowledgeArticleVersion record.
     * @return The currently operated upon KnowledgeArticleVersion record.
     */
    public Knowledge__kav insertKnowledgeAV() {
        return (Knowledge__kav)insertRecord();
    }

    /**
     * @description Inserts a list of Knowledge__kav and ties into the before and after hooks.
     * @param numToInsert the number of Knowledge__kav to insert.
     * @return The inserted SObjects.
     */
    public List<Knowledge__kav> insertKnowledgeAVs(Integer numToInsert) {
        return (List<Knowledge__kav>)insertRecords(numToInsert);
    }

    /**
     * @description Create the current working KnowledgeArticleVersion record without resetting
     *             the stored values in this instance of KnowledgeAVTestData.
     * @return A non-inserted KnowledgeArticleVersion record using the currently stored field
     *             values.
     */
    public Knowledge__kav createKnowledgeAVWithoutReset() {
        return (Knowledge__kav)buildWithoutReset();
    }

    /**
     * @description Create the current working KnowledgeArticleVersion record.
     * @return The currently operated upon KnowledgeArticleVersion record.
     */
    public Knowledge__kav create() {
        return (Knowledge__kav)super.buildWithReset();
    }

    /**
     * @description The default KnowledgeArticleVersion record.
     */
    public Knowledge__kav DefaultKnowledgeAV 
    {
        get 
        {
            if (DefaultKnowledgeAV == null) 
            {
                DefaultKnowledgeAV = createKnowledgeAVWithoutReset();
                insert DefaultKnowledgeAV;
            }

            DefaultKnowledgeAV = KnowledgeAVSelector.newInstance().selectById(
                new Set<Id>{KnowledgeAVTestData.Instance.DefaultKnowledgeAV.Id})[0];

            return DefaultKnowledgeAV;
        }
        private set;
    }

    /**
     * @description Get the Knowledge__kav SObjectType.
     * @return The Knowledge__kav SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Knowledge__kav.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static KnowledgeAVTestData Instance {
        get {
            if (Instance == null) {
                Instance = new KnowledgeAVTestData();
            }
            return Instance;
        }
        private set;
    }

    private KnowledgeAVTestData() { }
}