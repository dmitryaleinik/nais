/**
 * @description Used to determine if users have access to certain areas of the portal as well as granting access if
 *              various criteria has been met.
 */
public with sharing class AccessService {
    
    @testVisible private static final String EARLY_ACCESS_URL_PARAM = 'earlyaccess';
    @testVisible private static final String EARLY_ACCESS_COOKIE = 'earlyaccess';
    private static final Integer EARLY_ACCESS_EXPIRATION = 300; // 300 seconds == 5 minutes.

    // Private constructor to prevent classes from creating this inline.
    // Callers should use newInstance() method or access singleton instance.
    private AccessService() { }

    /**
     * @description Grants early access to users. Early access is granted by setting the current user's record
     *              Early_Access__c field to true. The record is updated only if:
     *                  - The current user is logged in
     *                  - The early access cookie has been set
     *                  - The current user record has not already been updated
     */
    public void grantEarlyAccess() {
        if (CurrentUser.isGuest()) {
            return;
        }

        User currentUserRecord = CurrentUser.getCurrentUser();

        if (currentUserRecord != null && currentUserRecord.Early_Access__c) {
            return;
        }

        // If the early access cookie is not set, we shouldn't update the user record.
        if (!isEarlyAccessCookieSet()) {
            return;
        }

        currentUserRecord.Early_Access__c = true;
        update currentUserRecord;
    }

    /**
     * @description Determines whether or not early access is open based on the Family Portal Settings. If Early Access
     *              Start Date is not null and is on or before the current date and time, early access is open.
     * @return True if the early access is open for the family portal.
     */
    public Boolean isEarlyAccessOpen() {
        return FamilyPortalSettings.Early_Access_Start_Date != null &&
               FamilyPortalSettings.Early_Access_Start_Date <= System.now();
    }

    /**
     * @description Sets the early access cookie so that the system knows when to allow the current user into the family
     *              portal for the current academic year and update the current user's record. The early access cookie
     *              will only be set if early access is open and the early access url param has been set to true.
     */
    public void setEarlyAccessCookie() {
        // Only set the early access cookie if early access is actually open.
        if (!isEarlyAccessOpen()) {
            return;
        }

        String earlyAccessParameter = ApexPages.currentPage().getParameters().get(EARLY_ACCESS_URL_PARAM);

        if (earlyAccessParameter == 'true') {
            Cookie earlyAccessCookie = createEarlyAccessCookie();
            ApexPages.currentPage().setCookies(new List<Cookie> { earlyAccessCookie });
        }
    }

    /**
     * @description Determines if the early access cookie has been set.
     * @return True if the early access cookie exists and has a value  of true.
     */
    public Boolean isEarlyAccessCookieSet() {
        Map<String, Cookie> currentCookiesByName = ApexPages.currentPage().getCookies();

        Cookie earlyAccessCookie = currentCookiesByName.get(EARLY_ACCESS_COOKIE);

        return earlyAccessCookie != null && earlyAccessCookie.getValue() == 'true';
    }

    private Cookie createEarlyAccessCookie() {
        return new Cookie(EARLY_ACCESS_COOKIE, 'true', '/', EARLY_ACCESS_EXPIRATION, false);
    }

    /**
     * @description Creates an instance of the AccessService.
     * @return A new instance of AccessService.
     */
    public static AccessService newInstance() {
        return new AccessService();
    }

    /**
     * @description Singleton instance of the AccessService.
     *
     */
    public static AccessService Instance {
        get {
            if (Instance == null) {
                Instance = newInstance();
            }

            return Instance;
        }
        private set;
    }
}