/**
* @SchoolFodlerLeftNavController.cls
* @author Exponent Partners, 2013
* @description Controller for the SchoolFolderLeftNav.component
*   handles display of the component on Folder Summary and PFS Summary pages
*/

public class SchoolFolderLeftNavController {
    /*Initialization*/
    public SchoolFolderLeftNavController() {
        fullView = GlobalVariables.isLimitedSchoolView() ? false : true;
    }
    /*End Initialization*/    
    
    /*Properties*/
    public Student_Folder__c studentFolder { // [CH] NAIS-1866 Refactoring to eliminate loadRecords method
        get{
            if((studentFolder == null && studentFolderId != null)){
                studentFolder = GlobalVariables.getFolderAndSchoolPFSAssignments(studentFolderId);
            }
            
            return studentFolder;
        } 
        set;
    }
    public Id studentFolderId { // [CH] NAIS-1866 Refactoring to eliminate loadRecords method
        get{
            if (studentFolderId == null && spfsId != null){
                studentFolderId = [Select Id, Student_Folder__c from School_PFS_Assignment__c where Id = :spfsId limit 1][0].Student_Folder__c;
            }
            
            return studentFolderId;
        } 
        set;
    }
    public School_PFS_Assignment__c spfs1Record { // [CH] NAIS-1866 Refactoring to eliminate loadRecords method
        get{
            if(spfs1Record == null && studentFolder.School_PFS_Assignments__r.size() > 0){
                spfs1Record = studentFolder.School_PFS_Assignments__r.size() > 0 ? studentFolder.School_PFS_Assignments__r[0] : null;
            }
            
            return spfs1Record;
        } 
        set;
    }
    public School_PFS_Assignment__c spfs2Record{ // [CH] NAIS-1866 Refactoring to eliminate loadRecords method
        get{
            if(spfs2Record == null && studentFolder.School_PFS_Assignments__r.size() > 1){
                spfs2Record = studentFolder.School_PFS_Assignments__r.size() > 1 ? studentFolder.School_PFS_Assignments__r[1] : null;
            }            
            return spfs2Record;
        }    
        set;
    }
    public Boolean folderLevelView {get; set;}
    public Boolean fullView {get; set;}
    public Id spfsId {get; set;}

    public Boolean isFAScreen {
        get{
            return isFAScreen == null ? false : isFAScreen;
        }
        set;
    }

    private MonthlyIncomeAndExpensesService.MIEResponseBulk MIEStatusInfo;
    private MonthlyIncomeAndExpensesService.MIEResponseBulk getMIEStatusInfo() {
        if (MIEStatusInfo != null) {
            return MIEStatusInfo;
        }

        MIEStatusInfo = MonthlyIncomeAndExpensesService.getMIEStatus(studentFolder.Id);

        return MIEStatusInfo;
    }

    public Boolean getIsMIEAvailableForPfs1() {
        // There should always be at least 1 PFS for the folder but if not, return false immediately.
        if (spfs1Record == null) {
            return false;
        }

        return getMIEStatusInfo().isMIEInfoAvailable(spfs1Record.Applicant__r.PFS__c);
    }

    public Boolean getIsMIEAvailableForPfs2() {
        // If there isn't a second PFS then just return false immediately.
        if (spfs2Record == null) {
            return false;
        }

        return getMIEStatusInfo().isMIEInfoAvailable(spfs2Record.Applicant__r.PFS__c);
    }

    public Student_Folder__c requeryedFolder {get; set;}

    public String getGrade(){
        loadRecords();
        String grade;
        grade = spfs1Record.Grade_Applying__c == null ? null : SchoolFolderSummaryController.suffixizeGrade(spfs1Record.Grade_Applying__c);
        return grade;
    }
    
    public String getColor(){
        loadRecords();
        String spfs1status = spfs1Record == null ? null : spfs1Record.PFS_Document_Status__c;
        String spfs2status = spfs2Record == null ? null : spfs2Record.PFS_Document_Status__c;
        return SchoolFolderSummaryController.calculateColor(studentFolder, spfs1status, spfs2status);
    }

    public Student_Folder__c getStudentFolder(){
        loadRecords();
        return studentFolder;
    }
    
    public PageReference getFolderURL(){
        loadRecords();
        PageReference pr = Page.SchoolFolderSummary;
        pr.getParameters().put('id', studentFolder.Id);
        return pr;
    }
    
    public School_PFS_Assignment__c getSpfs1Record(){
        loadRecords();
        if (spfsId == null){
            return spfs1Record;
        } else {
            return spfs1Record != null && spfs1Record.Id == spfsId ? spfs1Record : null;
        }
    }

    public School_PFS_Assignment__c getSpfs2Record(){
        loadRecords();
        if (spfsId == null){
            return spfs2Record;
        } else {
            return spfs2Record != null && spfs2Record.Id == spfsId ? spfs2Record : null;
        }
    }
    
    public Boolean getSpfs1Submitted(){
        return GlobalVariables.pfsIsSubmitted(spfs1Record);
    }

    public Boolean getSpfs2Submitted(){
        return GlobalVariables.pfsIsSubmitted(spfs2Record);
    }
    
    // NAIS-2020 [DP] 11.19.2014 changing "bothSubmitted" to "atLeastOneSubmitted" and commenting out "bothSubmitted" logic
    //public Boolean getBothSubmitted(){
    //    return (spfs1Record == null || GlobalVariables.pfsIsSubmitted(spfs1Record)) && (spfs2Record == null || GlobalVariables.pfsIsSubmitted(spfs2Record));
    //}
    public Boolean getAtLeastOneSubmitted(){
        return (spfs1Record != null && GlobalVariables.pfsIsSubmitted(spfs1Record)) || (spfs2Record != null && GlobalVariables.pfsIsSubmitted(spfs2Record)) || studentFolder.Folder_Source__c == 'School-Initiated';
    }

    /*End Properties*/
    
    /*Action Methods*/
    public pageReference lockPFS1(){
        spfs1Record.Family_May_Submit_Updates__c = 'No';
        update spfs1Record;
        return null;
    }

    public pageReference unlockPFS1(){
        spfs1Record.Family_May_Submit_Updates__c = 'Yes';
        update spfs1Record;
        return null;
    }

    public pageReference lockPFS2(){
        spfs2Record.Family_May_Submit_Updates__c = 'No';
        update spfs2Record;
        return null;
    }

    public pageReference unlockPFS2(){
        spfs2Record.Family_May_Submit_Updates__c = 'Yes';
        update spfs2Record;
        return null;
    }
       /*End Action Methods*/

    /*Helper Methods*/
    public void loadRecords(){
    }
    /*End Helper Methods*/

}