@isTest
private class FeatureTogglesTest {

    @isTest
    private static void isEnabled_recalcWithBizFarmsEnabled_expectTrue() {
        FeatureToggles.setToggle('Recalc_EFC_With_Biz_Farms__c', true);

        System.assertEquals(true, FeatureToggles.isEnabled('Recalc_EFC_With_Biz_Farms__c'),
                'Expected the feature to be enabled.');
    }

    @isTest
    private static void isEnabled_recalcWithBizFarmsDisabled_expectFalse() {
        FeatureToggles.setToggle('Recalc_EFC_With_Biz_Farms__c', false);

        System.assertEquals(false, FeatureToggles.isEnabled('Recalc_EFC_With_Biz_Farms__c'),
                'Expected the feature to not be enabled.');
    }
}