public class FamilyAppKSProgramSelectionCont extends FamilyAppCompController {

    private transient Map<Id, Boolean> ksApplicantIds_x = null;
    public Map<Id, Boolean> KSApplicantIds {
        get {
            if(this.ksApplicantIds_x == null) {
                
                this.ksApplicantIds_x = new Map<Id, Boolean>();
                
                if(SchoolPortalSettings.KS_School_Account_Id != null) {
                    
                    Map<Id, Applicant__c> applicants = new Map<Id, Applicant__c>(this.PFS.Applicants__r);
                    Set<Id> applicantIds = applicants.keySet(); 
                    
                    List<School_PFS_Assignment__c> pfsAssignments = [SELECT Id, Applicant__c, School__c 
                                                                     FROM School_PFS_Assignment__c 
                                                                     WHERE School__c = :SchoolPortalSettings.KS_School_Account_Id 
                                                                     AND Applicant__c IN :applicantIds 
                                                                     AND Withdrawn__c != 'Yes'];
                    
                    for(School_PFS_Assignment__c assignment : pfsAssignments) {
                        this.ksApplicantIds_x.put(assignment.Applicant__c, true);
                    }
                    
                    for(Id applicantId : applicantIds) {
                        if(!this.ksApplicantIds_x.containsKey(applicantId)) {
                            this.ksApplicantIds_x.put(applicantId, false);
                        }
                    }
                    
                } else {
                    throw new KSProgramSelectionException('School Settings not found or missing KS_School_Account_Id__c');
                }
            }
            System.debug('FamilyAppKSProgramSelectionCont.KSApplicantIds: ' + this.ksApplicantIds_x);
            return this.ksApplicantIds_x;
        }
    }

    private transient List<SelectOption> receivedK12FinancialAidOptions_x = null;
    public List<SelectOption> ReceivedK12FinancialAidOptions{
        get {
            if(this.receivedK12FinancialAidOptions_x == null) {
                this.receivedK12FinancialAidOptions_x = new List<SelectOption>();
                
                for(Schema.PicklistEntry entry : Applicant__c.Received_K_12_Financial_Aid_Before__c.getDescribe().getPicklistValues()) {
                    this.receivedK12FinancialAidOptions_x.add(new SelectOption(entry.getLabel(), entry.getValue()));
                }
                this.receivedK12FinancialAidOptions_x.sort(); //NAIS-2281 [DP] 02.27.2015 sort lists alphabetically
            }
            
            return this.receivedK12FinancialAidOptions_x;
        }
    }
    
    private transient List<SelectOption> receivedPreSchoolFinancialAidOptions_x = null;
    public List<SelectOption> ReceivedPreSchoolFinancialAidOptions{
        get {
            if(this.receivedPreSchoolFinancialAidOptions_x == null) {
                this.receivedPreSchoolFinancialAidOptions_x = new List<SelectOption>();
                
                for(Schema.PicklistEntry entry : Applicant__c.Received_Preschool_Financial_Aid_Before__c.getDescribe().getPicklistValues()) {
                    this.receivedPreSchoolFinancialAidOptions_x.add(new SelectOption(entry.getLabel(), entry.getValue()));
                }
                this.receivedPreSchoolFinancialAidOptions_x.sort(); //NAIS-2281 [DP] 02.27.2015 sort lists alphabetically
            }
            
            return this.receivedPreSchoolFinancialAidOptions_x;
        }
    }
    
    private transient AnnualSettingModel annualSetting_x = null;
    public AnnualSettingModel AnnualSetting {
        get {
            
            if(this.annualSetting_x == null) {
                if(this.pfs.Academic_Year_Picklist__c != null) {

                    List<Annual_Setting__c> settings = new List<Annual_Setting__c>();
                
                    if(SchoolPortalSettings.KS_School_Account_Id != null) {
                        settings = [SELECT
                                        Pauahi_Keiki_Scholars_Schools_Cycle_1__c,
                                        Pauahi_Keiki_Schools_Cycle_1_N_Z__c,
                                        Pauahi_Keiki_Scholars_Schools_Cycle_2__c,
                                        Pauahi_Keiki_Schools_Cycle_2_N_Z__c,
                                        Kipona_Scholarship_Program_Schools__c,
                                        KS_K_12_Financial_Max_Birthdate__c,
                                        KS_K_12_Financial_Min_Birthdate__c,
                                        KS_K_12_Financial_Aid_Open_Date__c ,
                                        KS_K_12_Financial_Aid_Close_Date__c ,
                                        KS_Preschool_Financial_Aid_Open_Date__c,
                                        KS_Preschool_Financial_Aid_Close_Date__c,
                                        KS_Preschool_Financial_Max_Birthdate__c,
                                        KS_Preschool_Financial_Min_Birthdate__c,
                                        Pauahi_Keiki_Scholars_Open_Date_Cycle_1__c,
                                        Pauahi_Keiki_Close_Date_Cycle_1__c,
                                        Pauahi_Keiki_Min_Birthdate_Cycle_1__c,
                                        Pauahi_Keiki_Max_Birthdate_Cycle_1__c,
                                        Pauahi_Keiki_Open_Date_Cycle_2__c,
                                        Pauahi_Keiki_Scholars_Close_Date_Cycle_2__c,
                                        Pauahi_Keiki_Min_Birthdate_Cycle_2__c,
                                        Pauahi_Keiki_Max_Birthdate_Cycle_2__c,
                                        Kipona_Scholarship_Open_Date__c,
                                        Kipona_Scholarship_Close_Date__c,
                                        Kipona_Scholarship_Min_Birthdate__c,
                                        Kipona_Scholarship_Max_Birthdate__c
                                    FROM
                                        Annual_Setting__c
                                    WHERE
                                        School__c = :SchoolPortalSettings.KS_School_Account_Id 
                                        AND Academic_Year__r.Name = :this.pfs.Academic_Year_Picklist__c];
                    } else {
                        throw new KSProgramSelectionException('School Settings not found or missing KS_School_Account_Id__c');
                    }
                    
                    if(!settings.isEmpty()) {
                        this.annualSetting_x = new AnnualSettingModel();
                        this.annualSetting_x.Id = settings[0].Id;
                        this.annualSetting_x.Pauahi_Keiki_Scholars_Schools_Cycle_1 = settings[0].Pauahi_Keiki_Scholars_Schools_Cycle_1__c;
                        this.annualSetting_x.Pauahi_Keiki_Schools_Cycle_1_N_Z = settings[0].Pauahi_Keiki_Schools_Cycle_1_N_Z__c;
                        this.annualSetting_x.Pauahi_Keiki_Scholars_Schools_Cycle_2 = settings[0].Pauahi_Keiki_Scholars_Schools_Cycle_2__c;
                        this.annualSetting_x.Pauahi_Keiki_Schools_Cycle_2_N_Z = settings[0].Pauahi_Keiki_Schools_Cycle_2_N_Z__c;
                        this.annualSetting_x.Kipona_Scholarship_Program_Schools = settings[0].Kipona_Scholarship_Program_Schools__c;
                        this.annualSetting_x.KS_K_12_Financial_Max_Birthdate = settings[0].KS_K_12_Financial_Max_Birthdate__c;
                        this.annualSetting_x.KS_K_12_Financial_Min_Birthdate = settings[0].KS_K_12_Financial_Min_Birthdate__c;
                        this.annualSetting_x.KS_K_12_Financial_Aid_Open_Date = settings[0].KS_K_12_Financial_Aid_Open_Date__c;
                        this.annualSetting_x.KS_K_12_Financial_Aid_Close_Date = settings[0].KS_K_12_Financial_Aid_Close_Date__c;
                        this.annualSetting_x.KS_Preschool_Financial_Aid_Open_Date = settings[0].KS_Preschool_Financial_Aid_Open_Date__c;
                        this.annualSetting_x.KS_Preschool_Financial_Aid_Close_Date = settings[0].KS_Preschool_Financial_Aid_Close_Date__c;
                        this.annualSetting_x.KS_Preschool_Financial_Max_Birthdate = settings[0].KS_Preschool_Financial_Max_Birthdate__c;
                        this.annualSetting_x.KS_Preschool_Financial_Min_Birthdate = settings[0].KS_Preschool_Financial_Min_Birthdate__c;
                        this.annualSetting_x.Pauahi_Keiki_Scholars_Open_Date_Cycle_1 = settings[0].Pauahi_Keiki_Scholars_Open_Date_Cycle_1__c;
                        this.annualSetting_x.Pauahi_Keiki_Close_Date_Cycle_1 = settings[0].Pauahi_Keiki_Close_Date_Cycle_1__c;
                        this.annualSetting_x.Pauahi_Keiki_Min_Birthdate_Cycle_1 = settings[0].Pauahi_Keiki_Min_Birthdate_Cycle_1__c;
                        this.annualSetting_x.Pauahi_Keiki_Max_Birthdate_Cycle_1 = settings[0].Pauahi_Keiki_Max_Birthdate_Cycle_1__c;
                        this.annualSetting_x.Pauahi_Keiki_Open_Date_Cycle_2 = settings[0].Pauahi_Keiki_Open_Date_Cycle_2__c;
                        this.annualSetting_x.Pauahi_Keiki_Scholars_Close_Date_Cycle_2 = settings[0].Pauahi_Keiki_Scholars_Close_Date_Cycle_2__c;
                        this.annualSetting_x.Pauahi_Keiki_Min_Birthdate_Cycle_2 = settings[0].Pauahi_Keiki_Min_Birthdate_Cycle_2__c;
                        this.annualSetting_x.Pauahi_Keiki_Max_Birthdate_Cycle_2 = settings[0].Pauahi_Keiki_Max_Birthdate_Cycle_2__c;
                        this.annualSetting_x.Kipona_Scholarship_Open_Date = settings[0].Kipona_Scholarship_Open_Date__c;
                        this.annualSetting_x.Kipona_Scholarship_Close_Date = settings[0].Kipona_Scholarship_Close_Date__c;
                        this.annualSetting_x.Kipona_Scholarship_Min_Birthdate = settings[0].Kipona_Scholarship_Min_Birthdate__c;
                        this.annualSetting_x.Kipona_Scholarship_Max_Birthdate = settings[0].Kipona_Scholarship_Max_Birthdate__c;
                    } else {
                        throw new KSProgramSelectionException('Annual Settings not found');
                    }
                }
            }
            
            System.debug('FamilyAppKSProgramSelectionCont.AnnualSetting: ' + this.annualSetting_x);
            System.debug('FamilyAppKSProgramSelectionCont.AnnualSetting.Id: ' + this.annualSetting_x.Id);
            return this.annualSetting_x;
        }
    }
    
    private transient List<SelectOption> pkCycle1SchoolOptions_x = null;
    public List<SelectOption> PkCycle1SchoolOptions{
        get {
            if(this.pkCycle1SchoolOptions_x == null) {
                this.pkCycle1SchoolOptions_x = new List<SelectOption>();
                this.pkCycle1SchoolOptions_x.add(new SelectOption('',''));
                
                if(this.AnnualSetting != null) {
                    
                    System.debug('FamilyAppKSProgramSelectionCont.PkCycle1SchoolOptions.Pauahi_Keiki_Scholars_Schools_Cycle_1: ' + this.AnnualSetting.Pauahi_Keiki_Scholars_Schools_Cycle_1);
                    System.debug('FamilyAppKSProgramSelectionCont.PkCycle1SchoolOptions.Pauahi_Keiki_Schools_Cycle_1_N_Z: ' + this.AnnualSetting.Pauahi_Keiki_Schools_Cycle_1_N_Z);
                    
                    if(this.AnnualSetting.Pauahi_Keiki_Scholars_Schools_Cycle_1 != null) {
                        for(String entry : this.AnnualSetting.Pauahi_Keiki_Scholars_Schools_Cycle_1.split(';')) {
                            this.pkCycle1SchoolOptions_x.add(new SelectOption(entry, entry));
                        }
                    }
                    
                    if(this.AnnualSetting.Pauahi_Keiki_Schools_Cycle_1_N_Z != null) {
                        for(String entry : this.AnnualSetting.Pauahi_Keiki_Schools_Cycle_1_N_Z.split(';')) {
                            this.pkCycle1SchoolOptions_x.add(new SelectOption(entry, entry));
                        }
                    }
                }
                this.pkCycle1SchoolOptions_x.sort(); //NAIS-2281 [DP] 02.27.2015 sort lists alphabetically
            }
            
            return this.pkCycle1SchoolOptions_x;
        }
    }
    
    private transient List<SelectOption> pkCycle2SchoolOptions_x = null;
    public List<SelectOption> PkCycle2SchoolOptions{
        get {
            if(this.pkCycle2SchoolOptions_x == null) {
                this.pkCycle2SchoolOptions_x = new List<SelectOption>();
                this.pkCycle2SchoolOptions_x.add(new SelectOption('',''));
                
                if(this.AnnualSetting != null) {
                    
                    System.debug('FamilyAppKSProgramSelectionCont.PkCycle1SchoolOptions.Pauahi_Keiki_Scholars_Schools_Cycle_2: ' + this.AnnualSetting.Pauahi_Keiki_Scholars_Schools_Cycle_2);
                    System.debug('FamilyAppKSProgramSelectionCont.PkCycle1SchoolOptions.Pauahi_Keiki_Schools_Cycle_2_N_Z: ' + this.AnnualSetting.Pauahi_Keiki_Schools_Cycle_2_N_Z);
                    
                    if(this.AnnualSetting.Pauahi_Keiki_Scholars_Schools_Cycle_2 != null) {
                        for(String entry : this.AnnualSetting.Pauahi_Keiki_Scholars_Schools_Cycle_2.split(';')) {
                            this.pkCycle2SchoolOptions_x.add(new SelectOption(entry, entry));
                        }
                    }
                    
                    if(this.AnnualSetting.Pauahi_Keiki_Schools_Cycle_2_N_Z != null) {
                        for(String entry : this.AnnualSetting.Pauahi_Keiki_Schools_Cycle_2_N_Z.split(';')) {
                            this.pkCycle2SchoolOptions_x.add(new SelectOption(entry, entry));
                        }
                    }
                }
                this.pkCycle2SchoolOptions_x.sort(); //NAIS-2281 [DP] 02.27.2015 sort lists alphabetically
            }
            
            return this.pkCycle2SchoolOptions_x;
        }
    }
    
    private transient List<SelectOption> receivedPKScholarshipOptions_x = null;
    public List<SelectOption> ReceivedPKScholarshipOptions{
        get {
            if(this.receivedPKScholarshipOptions_x == null) {
                this.receivedPKScholarshipOptions_x = new List<SelectOption>();
                
                for(Schema.PicklistEntry entry : Applicant__c.Received_Pauahi_Keiki_Scholarship_Before__c.getDescribe().getPicklistValues()) {
                    this.receivedPKScholarshipOptions_x.add(new SelectOption(entry.getLabel(), entry.getValue()));
                }
                this.receivedPKScholarshipOptions_x.sort(); //NAIS-2281 [DP] 02.27.2015 sort lists alphabetically
            }
            
            return this.receivedPKScholarshipOptions_x;
        }
    }
    
    private transient List<SelectOption> kiponaSchoolOptions_x = null;
    public List<SelectOption> KiponaSchoolOptions{
        get {
            if(this.kiponaSchoolOptions_x == null) {
                this.kiponaSchoolOptions_x = new List<SelectOption>();
                this.kiponaSchoolOptions_x.add(new SelectOption('',''));
                
                if(this.AnnualSetting != null) {
                    
                    if(this.AnnualSetting.Kipona_Scholarship_Program_Schools != null) {
                        for(String entry : this.AnnualSetting.Kipona_Scholarship_Program_Schools.split(';')) {
                            this.kiponaSchoolOptions_x.add(new SelectOption(entry, entry));
                        }
                    }
                }
                this.kiponaSchoolOptions_x.sort(); //NAIS-2281 [DP] 02.27.2015 sort lists alphabetically
            }
            
            return this.kiponaSchoolOptions_x;
        }
    }
    
    private transient List<SelectOption> receivedKiponaScholarshipOptions_x = null;
    public List<SelectOption> ReceivedKiponaScholarshipOptions{
        get {
            if(this.receivedKiponaScholarshipOptions_x == null) {
                this.receivedKiponaScholarshipOptions_x = new List<SelectOption>();
                
                for(Schema.PicklistEntry entry : Applicant__c.Received_Kipona_Scholarship_Before__c.getDescribe().getPicklistValues()) {
                    this.receivedKiponaScholarshipOptions_x.add(new SelectOption(entry.getLabel(), entry.getValue()));
                }
                this.receivedKiponaScholarshipOptions_x.sort(); //NAIS-2281 [DP] 02.27.2015 sort lists alphabetically
            }
            
            return this.receivedKiponaScholarshipOptions_x;
        }
    }

    public class KSProgramSelectionException extends Exception {}
    
    public class AnnualSettingModel {
       
           public String Id {get; set;}
           public String Pauahi_Keiki_Scholars_Schools_Cycle_1 {get; set;}
        public String Pauahi_Keiki_Schools_Cycle_1_N_Z {get; set;}
        public String Pauahi_Keiki_Scholars_Schools_Cycle_2 {get; set;}
        public String Pauahi_Keiki_Schools_Cycle_2_N_Z {get; set;}
        public String Kipona_Scholarship_Program_Schools {get; set;}
        public Date KS_K_12_Financial_Max_Birthdate {get; set;}
        public Date KS_K_12_Financial_Min_Birthdate {get; set;}
        public Date KS_K_12_Financial_Aid_Open_Date {get; set;}
        public Date KS_K_12_Financial_Aid_Close_Date {get; set;}
        public Date KS_Preschool_Financial_Aid_Open_Date {get; set;}
        public Date KS_Preschool_Financial_Aid_Close_Date {get; set;}
        public Date KS_Preschool_Financial_Max_Birthdate {get; set;}
        public Date KS_Preschool_Financial_Min_Birthdate {get; set;}
        public Date Pauahi_Keiki_Scholars_Open_Date_Cycle_1 {get; set;}
        public Date Pauahi_Keiki_Close_Date_Cycle_1 {get; set;}
        public Date Pauahi_Keiki_Min_Birthdate_Cycle_1 {get; set;}
        public Date Pauahi_Keiki_Max_Birthdate_Cycle_1  {get; set;}
        public Date Pauahi_Keiki_Open_Date_Cycle_2 {get; set;}
        public Date Pauahi_Keiki_Scholars_Close_Date_Cycle_2 {get; set;}
        public Date Pauahi_Keiki_Min_Birthdate_Cycle_2 {get; set;}
        public Date Pauahi_Keiki_Max_Birthdate_Cycle_2 {get; set;}
        public Date Kipona_Scholarship_Open_Date {get; set;}
        public Date Kipona_Scholarship_Close_Date {get; set;}
        public Date Kipona_Scholarship_Min_Birthdate {get; set;}
        public Date Kipona_Scholarship_Max_Birthdate {get; set;}
    }
}