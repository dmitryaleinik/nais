global class UpdateAccountRollUpsBatch implements Database.Batchable<sObject> {
    
    global final String query = 'select Id, Most_Recent_PFS_Count__c, Most_Recent_wPFS_Count__c, Prior_Year_PFS_Count__c, '+ 
                                'Prior_Year_wPFS_Count__c from Account where RecordTypeID = \''+RecordTypes.schoolAccountTypeId+'\'';
        
    
    global UpdateAccountRollUpsBatch() {
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        
       /*Map<Id,Account> accntMap = new Map<Id, Account>([select Id, Most_Recent_PFS_Count__c, Most_Recent_wPFS_Count__c, Prior_Year_PFS_Count__c, 
                                                                Prior_Year_wPFS_Count__c from Account 
                                                          where Id = '001J000001ah6OX' and RecordTypeID = :RecordTypes.schoolAccountTypeId]);*/
        Map<Id,Account> accntMap = new Map<Id,Account>();
        for (sObject accnt : scope) {
            accntMap.put(((Account) accnt).Id, ((Account) accnt));
        }
        
        //Get the last 2 academic years
        List<ID> academicYearIds = new List<ID>();
        Academic_Year__c currentAcademicYear = GlobalVariables.getCurrentAcademicYear();
        Academic_Year__c previousAcademicYear = GlobalVariables.getPreviousAcademicYear();
        
        academicYearIds.add(currentAcademicYear.Id);
        academicYearIds.add(previousAcademicYear.Id);
        
        Map<Id, Map<Id, Annual_Setting__c>> accountASMap = new Map<Id, Map<Id, Annual_Setting__c>>();
        Set<Id> asIDSet = new Set<Id>();
        
        //Get all the Annual setting records for this and previous Academic years for the selected schools
        for(Annual_Setting__c aSetting : [select Id, School__c, Academic_Year__c,wPFS_Count__c, Number_of_wPFS_Records__c
                                            from Annual_Setting__c where Academic_Year__c in :academicYearIds and School__c in :accntMap.keySet()]){
            //Prepare a map based on the school for the previous 2 academic years
            if(!accountASMap.containsKey(aSetting.School__c)){
                accountASMap.put(aSetting.School__c, new Map<Id, Annual_Setting__c>());
            }
            accountASMap.get(aSetting.School__c).put(aSetting.Academic_Year__c, aSetting);
        }
                
        Map<Id, Account> updateAccntMap = new Map<Id, Account>();
        //Loop through the map
        for(Id schoolId : accntMap.KeySet()){
            Map<Id, Annual_Setting__c> asMap = accountASMap.get(schoolId);
            Account schoolAccnt = accntMap.get(schoolId);
            if(asMap != null){
                Annual_Setting__c currentAS = asMap.get(currentAcademicYear.Id);
                Annual_Setting__c previousAS = asMap.get(previousAcademicYear.Id);
                //If there are existing WPFS records
                if(currentAS != null){
                    schoolAccnt.Most_Recent_PFS_Count__c = currentAS.Number_of_wPFS_Records__c;
                    schoolAccnt.Most_Recent_wPFS_Count__c = currentAS.wPFS_Count__c;
                }
                else{//if there is no Annual setting or no WPFS records
                    schoolAccnt.Most_Recent_PFS_Count__c = 0;
                    schoolAccnt.Most_Recent_wPFS_Count__c = 0;
                }
                if(previousAS != null){
                    schoolAccnt.Prior_Year_PFS_Count__c = previousAS.Number_of_wPFS_Records__c;
                    schoolAccnt.Prior_Year_wPFS_Count__c = previousAS.wPFS_Count__c;
                }
                else{
                    schoolAccnt.Prior_Year_PFS_Count__c = 0;
                    schoolAccnt.Prior_Year_wPFS_Count__c = 0;
                }
            }
            else{
                schoolAccnt.Most_Recent_PFS_Count__c = 0;
                schoolAccnt.Most_Recent_wPFS_Count__c = 0;
                schoolAccnt.Prior_Year_PFS_Count__c = 0;
                schoolAccnt.Prior_Year_wPFS_Count__c = 0;
            }
            
            updateAccntMap.put(schoolAccnt.Id, schoolAccnt);
        }
        
        //Update the Account
        if(updateAccntMap.size() > 0){
            Database.update(updateAccntMap.values());
        }
    }
    
    global void finish(Database.BatchableContext bc) {
    }
    

}