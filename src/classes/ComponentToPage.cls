public class ComponentToPage {

    // component to page communication
    // component can return its state to page only by reference
    // add any other variables required to pass from component to page
    // currently used in SchoolFolderLeftNav component 

    public String pageName {get; set;}
    public String schoolPFSAssignmentId {get; set;}
}