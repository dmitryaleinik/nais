/*
*    SPEC-092 Student Folder & Aid Allocation (Folder Summary), R-167, R-180, R-125, R-161, R-117 20-24 hours [Drew]
*    The school folder is a VF page, with some folder level items and statuses, and links to the one or two PFS' within.
*    The main folder page is also currently where the Aid Allocation is done. There are a number of editable fields which display the inputs to the aid decision and some can be revised as well.
*    Need to populate the Tuition fields (4 of them) based on grade and the enrollment status, and day vs. boarding."
*    - School user should be able to easily navigate between sibling folders
*    - The UI should clearly differentiate folder information (which is relevant to all PFS's) versus single PFS information
*    - School user should be able to obviously see when a folder has two PFS's in it
*    - School should be able to adjust the tuition & fees at the folder level which will allow them to apply their own discounts for siblings, etc.
*    - A folder should have a visual indicator if the student is new or returning
*    - A school aid officer should be able to allocate financial aid from any number of budgets they have specified.
*/

@isTest
public class SchoolFinancialAidControllerTest {

    private static final String PARENT_A_LASTNAME = 'Parent A';
    private static final String PARENT_B_LASTNAME = 'Parent B';

    private static User schoolPortalUser;

    // [CH] NAIS-1866 Adding for more streamlined unit tests and a whole new TestData
    //             instance is overkill for some tests
    public static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

    private static Academic_Year__c firstYear;
    private static Academic_Year__c secondYear;
    private static Academic_Year__c thirdYear;
    private static Academic_Year__c fourthYear;

    private static Account school;
    private static Account family;
    private static Account school1;

    private static Contact parent1;
    private static Contact student1;
    private static Contact staff1;
    private static Contact student1A;
    private static Contact student2;
    private static Contact parentA;
    private static Contact parentB;

    private static Academic_Year__c currentAcademicYear;
    private static Academic_Year__c previousAcademicYear;

    private static User portalUser;

    private static void generateBasicDataSet(){
        firstYear = new Academic_Year__c(Non_PFS_Folder_Open__c=System.Today().addDays(-100), Non_PFS_Folder_Close__c=System.Today().addDays(-2));
        secondYear = new Academic_Year__c(Non_PFS_Folder_Open__c=System.Today().addDays(-50), Non_PFS_Folder_Close__c=System.Today().addDays(2));
        thirdYear = new Academic_Year__c(Non_PFS_Folder_Open__c=System.Today().addDays(-2), Non_PFS_Folder_Close__c=System.Today().addDays(50));
        fourthYear = new Academic_Year__c(Non_PFS_Folder_Open__c=System.Today().addDays(2), Non_PFS_Folder_Close__c=System.Today().addDays(50));
        Database.insert(new List<Academic_Year__c>{firstYear, secondYear, thirdYear, fourthYear});

        family = TestUtils.createAccount('Individual Account', RecordTypes.individualAccountTypeId, 5, false);
        school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, 5, false);
        school1.Alert_Frequency__c = 'Weekly digest of new applications';
        school1.SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest();
        school1.SSS_School_Code__c = '11111';
        Database.insert(new List<Account>{family, school1});

        student1 = TestUtils.createContact('Student 1', family.Id, RecordTypes.studentContactTypeId, false);
        student1.Birthdate = System.today().addDays(-100);
        parent1 = TestUtils.createContact('Parent 1', family.Id, RecordTypes.parentContactTypeId, false);
        parent1.FirstName = 'Parent 1';
        parent1.Email = 'new@unittestemails.com';
        staff1 = TestUtils.createContact('Staff 1', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
        staff1.Email = 'staff1@test.org';
        staff1.PFS_Alert_Preferences__c = 'Receive Alerts';
        Database.insert(new List<Contact>{student1, parent1, staff1});

        System.runAs(thisUser){
            portalUser = TestUtils.createPortalUser(
                staff1.LastName, staff1.Email + String.valueOf(Math.random()) + String.valueOf(Math.random()), 'alias', 
                staff1.Id, GlobalVariables.schoolPortalAdminProfileId, true, true);
        }
    }

    private static void createTestData() {
        Account family1 = AccountTestData.Instance.asFamily().create();
        school = AccountTestData.Instance.asSchool().create();
        insert new List<Account>{family1, school};

        currentAcademicYear = AcademicYearTestData.Instance.create();
        previousAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, previousAcademicYear};

        parentA = ContactTestData.Instance
            .forLastName(PARENT_A_LASTNAME)
            .forAccount(family1.Id)
            .asParent().create();
        parentB = ContactTestData.Instance
            .forLastName(PARENT_B_LASTNAME)
            .forAccount(family1.Id)
            .asParent().create();
        Contact schoolStaff = ContactTestData.Instance
            .forAccount(school.Id)
            .asSchoolStaff().create();
        student1A = ContactTestData.Instance.asStudent().create();
        student2 = ContactTestData.Instance.asStudent().create();
        insert new List<Contact>{parentA, parentB, schoolStaff, student1A, student2};

        System.runAs(thisUser) {
            schoolPortalUser = UserTestData.Instance
                .forUsername(UserTestData.generateTestEmail())
                .forContactId(schoolStaff.Id)
                .forProfileId(GlobalVariables.schoolPortalAdminProfileId).insertUser();
        }

        TestUtils.insertAccountShare(schoolStaff.AccountId, schoolPortalUser.Id);

        String pfsStatusUnpaid = 'Unpaid';
        String isPfsFeeWaived = 'Yes';
        System.runAs(schoolPortalUser) {
            PFS__c pfs1 = PfsTestData.Instance
                .asSubmitted()
                .forPaymentStatus(pfsStatusUnpaid)
                .forParentA(parentA.Id)
                .forOriginalSubmissionDate(System.today().addDays(-3))
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            PFS__c pfs2 = PfsTestData.Instance
                .asSubmitted()
                .forPaymentStatus(pfsStatusUnpaid)
                .forParentA(parentB.Id)
                .forOriginalSubmissionDate(System.today().addDays(-5))
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(new List<PFS__c> {pfs1, pfs2});

            Applicant__c applicant1A = ApplicantTestData.Instance
                .forPfsId(pfs1.Id)
                .forContactId(student1A.Id).create();
            Applicant__c applicant1B = ApplicantTestData.Instance
                .forPfsId(pfs2.Id)
                .forContactId(student1A.Id).create();
            Dml.WithoutSharing.insertObjects(
                new List<Applicant__c> {applicant1A, applicant1B});

            Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
                .forStudentId(student1A.Id)
                .forSchoolId(school.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Student_Folder__c studentFolder2 = StudentFolderTestData.Instance
                .forStudentId(student2.Id)
                .forSchoolId(school.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name).create();
            Dml.WithoutSharing.insertObjects(
                new List<Student_Folder__c> {studentFolder1, studentFolder2});

            String fifthGrade = '5';
            Decimal salaryWagesParentA = 60000;
            School_PFS_Assignment__c spfsa1A = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1A.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSalaryWagesParentA(salaryWagesParentA)
                .forSchoolId(school.Id).create();
            School_PFS_Assignment__c spfsa1B = SchoolPfsAssignmentTestData.Instance
                .forApplicantId(applicant1B.Id)
                .forStudentFolderId(studentFolder1.Id)
                .forAcademicYearPicklist(currentAcademicYear.Name)
                .forGradeApplying(fifthGrade)
                .forSalaryWagesParentA(salaryWagesParentA)
                .forSchoolId(school.Id).create();
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> {spfsa1A, spfsa1B});
        }

        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);
    }

    @isTest
    public static void testSaveAndRecalc() {
        createTestData();

        Student_Folder__c updatedSF;
        List<Student_Folder__c> studentFolders = StudentFolderSelector.newInstance().selectByStudentId_schoolId_academicYearName(
            new Set<Id>{student1A.Id}, new Set<Id>{school.Id}, new Set<String>{currentAcademicYear.Name});
        System.assertEquals(1, studentFolders.size());
        Student_Folder__c studentFolder1 = studentFolders[0];

        Test.startTest();
            PageReference pageRef = Page.SchoolFinancialAid;
            pageRef.getParameters().put('id', String.valueOf(studentFolder1.Id));
            Test.setCurrentPage(pageRef);
            SchoolFinancialAidController testCont = new SchoolFinancialAidController();

            System.assertEquals(testCont.folder.Id, studentFolder1.Id);

            testCont.folder.Student_Tuition__c = 10;

            testCont.saveAndRecalc();

            updatedSF = [Select Id, Student_Tuition__c from Student_Folder__c where ID = :studentFolder1.Id];
            System.assertEquals(10, updatedSF.Student_Tuition__c);

            testCont.cancel();
        Test.stopTest();
    }

    @isTest
    private static void saveAndRecalc_awardNotSaved_expectError() {
        createTestData();
        Student_Folder__c updatedSF;
        List<Student_Folder__c> studentFolders = StudentFolderSelector.newInstance().selectByStudentId_schoolId_academicYearName(
            new Set<Id>{student1A.Id}, new Set<Id>{school.Id}, new Set<String>{currentAcademicYear.Name});
        System.assertEquals(1, studentFolders.size());
        Student_Folder__c studentFolder1 = studentFolders[0];

        Test.startTest();
            PageReference pageRef = Page.SchoolFinancialAid;
            pageRef.getParameters().put('id', String.valueOf(studentFolder1.Id));
            Test.setCurrentPage(pageRef);
            SchoolFinancialAidController controller = new SchoolFinancialAidController();

            controller.newBudgetAlloc.Amount_Allocated__c = 10;
            controller.saveAndRecalc();
        Test.stopTest();

        System.assert(controller.amountErrorMsg, 'Expected there to be an error message.');
        System.assertEquals(Label.AwardAmountNotSaved, controller.AmountErrorMessage,
                'Expected the given error message.');
    }

    // [CH] NAIS-1866 Testing new Academic Years list method
    @isTest
    private static void testAvailableAcademicYears(){
        generateBasicDataSet();

        Test.startTest();
        System.runAs(portalUser){
            PageReference pageRef = Page.SchoolFinancialAid;
            pageRef.getParameters().put('firstname', student1.FirstName);
            pageRef.getParameters().put('lastname', student1.LastName);
            pageRef.getParameters().put('birthdate', String.valueOf(student1.Birthdate));
            Test.setCurrentPage(pageRef);

            SchoolFinancialAidController controller = new SchoolFinancialAidController();

            System.assertEquals(2, controller.AvailableAcademicYears.size());
        }

        Student_Folder__c existingFolder = TestUtils.createStudentFolder('Test Folder', secondYear.Id, student1.Id, false);
        existingFolder.School__c = school1.Id;
        insert existingFolder;

        System.runAs(portalUser){
            PageReference pageRef = Page.SchoolFinancialAid;
            pageRef.getParameters().put('seedid', existingFolder.Id);
            Test.setCurrentPage(pageRef);

            SchoolFinancialAidController controller = new SchoolFinancialAidController();

            System.assertEquals(1, controller.AvailableAcademicYears.size());
        }

        Test.stopTest();
    }

    // [CH] NAIS-1866 Testing avoiding duplicate Contacts
    @isTest
    private static void testSubmit()
    {
        generateBasicDataSet();

        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
        Academic_Year__c previousAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, previousAcademicYear};

        Annual_Setting__c currentYearAnnualSettings = AnnualSettingsTestData.Instance
            .forAcademicYearId(currentAcademicYear.Id)
            .forSchoolId(school1.Id).create();
        Annual_Setting__c previousYearAnnualSettings = AnnualSettingsTestData.Instance
            .forAcademicYearId(previousAcademicYear.Id)
            .forSchoolId(school1.Id).create();
        insert new List<Annual_Setting__c>{currentYearAnnualSettings, previousYearAnnualSettings};

        Test.startTest();
            System.runAs(portalUser){
                PageReference pageRef = Page.SchoolFinancialAid;
                pageRef.getParameters().put('firstname', student1.FirstName);
                pageRef.getParameters().put('lastname', student1.LastName);
                pageRef.getParameters().put('birthdate', String.valueOf(student1.Birthdate));
                Test.setCurrentPage(pageRef);

                SchoolFinancialAidController controller = new SchoolFinancialAidController();

                controller.parentContact.FirstName = parent1.FirstName;
                controller.parentContact.LastName = parent1.LastName;
                controller.parentContact.Email = parent1.Email;

                controller.submit();

                // Verify that the existing contact records were found
                System.assertEquals(controller.applicantContact.Id, student1.Id);
                System.assertEquals(controller.parentContact.Id, parent1.Id);
                System.assertEquals(controller.parentContact.Id, parent1.Id);
                System.assertEquals(4, controller.availableAcademicYears.size(), 
                    'Initially, we should expect only 2 academic years here, but another 2 records are created in the generateBasicDataSet() method');

                // Reset things
                controller.loadFolder();

                controller.applicantContact.FirstName = 'UniqueName';

                controller.parentContact.FirstName = parent1.FirstName;
                controller.parentContact.LastName = parent1.LastName;
                controller.parentContact.Email = 'someemail@someurl.com';

                controller.submit();

                // Verify that new Contact records were created
                System.assertNotEquals(controller.applicantContact.Id, student1.Id);
                System.assertNotEquals(controller.parentContact.Id, parent1.Id);
            }

        Test.stopTest();
    }

    // [CH] NAIS-1866 Testing new Academic Years list method
    @isTest
    private static void testSubmitExistingFolder(){
        generateBasicDataSet();

        Student_Folder__c existingFolder = TestUtils.createStudentFolder('Test Folder', secondYear.Id, student1.Id, false);
        existingFolder.School__c = school1.Id;
        existingFolder.Academic_Year_Picklist__c = secondYear.Name;
        insert existingFolder;

        Test.startTest();
            System.runAs(portalUser){
                PageReference pageRef = Page.SchoolFinancialAid;
                pageRef.getParameters().put('seedid', existingFolder.Id);
                Test.setCurrentPage(pageRef);

                SchoolFinancialAidController controller = new SchoolFinancialAidController();
                controller.parentContact.FirstName = parent1.FirstName;
                controller.parentContact.LastName = parent1.LastName;
                controller.parentContact.Email = parent1.Email;
                System.assertEquals(2, controller.AvailableAcademicYears.size());
                controller.applicantContact.Id = student1.Id;
                controller.folder.Academic_Year_Picklist__c = secondYear.Name;
                controller.folder.School__c = school1.Id;
                controller.submit();

                // Make sure that the existing folder was found so there will be an error
                System.assertEquals(existingFolder.Id, controller.existingFolderId);
            }
        Test.stopTest();
    }

    @isTest
    private static void testBudgetAllocAndDelete(){
        createTestData();
        Student_Folder__c updatedSF;
        
        List<Student_Folder__c> studentFolders =  StudentFolderSelector.newInstance().selectByStudentId_schoolId_academicYearName(
            new Set<Id>{student1A.Id}, new Set<Id>{school.Id}, new Set<String>{currentAcademicYear.Name});
        System.assertEquals(1, studentFolders.size());
        Student_Folder__c studentFolder1 = studentFolders[0];

        Decimal totalFundsAmount = 10000;
        Budget_Group__c budget1 = BudgetGroupTestData.Instance
            .forSchoolId(school.Id)
            .forAcademicYearId(currentAcademicYear.Id)
            .forTotalFunds(totalFundsAmount).insertBudgetGroup();

        Test.startTest();
            PageReference pageRef = Page.SchoolFinancialAid;
            pageRef.getParameters().put('id', String.valueOf(studentFolder1.Id));
            Test.setCurrentPage(pageRef);
            SchoolFinancialAidController testCont = new SchoolFinancialAidController();

            System.assertEquals(testCont.folder.Id, studentFolder1.Id);

            testCont.newBudgetAlloc.Budget_Group__c = budget1.Id;
            testCont.newBudgetAlloc.Amount_Allocated__c = 49;
            testCont.addBudgetAlloc();

            testCont.newBudgetAlloc.Budget_Group__c = budget1.Id;
            testCont.newBudgetAlloc.Amount_Allocated__c = 51;
            testCont.addBudgetAlloc();

            testCont.saveAndRecalc();
            testCont = new SchoolFinancialAidController();
            System.assertEquals(100, testCont.totalFromBudgets);

            System.assertNotEquals(null, testCont.getUseBudgetOptions());
            System.assertEquals(1, testCont.getBudgetGroupOptions().size());

            pageRef.getParameters().put('budgetIdToDelete', '0');

            testCont.deleteBudget();

            testCont.saveAndRecalc();
            testCont = new SchoolFinancialAidController();

            System.assertEquals(51, testCont.totalFromBudgets);
        Test.stopTest();
    }

    @isTest
    private static void testCombinedContribution() {
        createTestData();

        List<Student_Folder__c> studentFolders = StudentFolderSelector.newInstance().selectByStudentId_schoolId_academicYearName(
            new Set<Id>{student1A.Id}, new Set<Id>{school.Id}, new Set<String>{currentAcademicYear.Name});
        System.assertEquals(1, studentFolders.size());
        Student_Folder__c studentFolder1 = studentFolders[0];

        PFS__c pfs1 = PfsSelector.newInstance().selectByContactIdAndAcademicYearId(parentA.Id, currentAcademicYear.Id)[0];
        PFS__c pfs2 = PfsSelector.newInstance().selectByContactIdAndAcademicYearId(parentB.Id, currentAcademicYear.Id)[0];

        List<Applicant__c> applicants = ApplicantSelector.newInstance().selectByPFSandStudent(
            new Set<Id>{pfs1.Id, pfs2.Id}, new Set<Id>{student1A.Id});
        System.assertEquals(2, applicants.size());
        Map<Id, Applicant__c> applicantsMapping = new Map<Id, Applicant__c>();
        for(Applicant__c applicant : applicants) {
            applicantsMapping.put(applicant.PFS__c, applicant);
        }
        Applicant__c applicant1A = applicantsMapping.get(pfs1.Id);
        Applicant__c applicant1B = applicantsMapping.get(pfs2.Id);

        List<School_PFS_Assignment__c> spfsas = SchoolPfsAssignmentsSelector.newInstance().selectAll();
        System.assertEquals(2, spfsas.size());
        Map<String, School_PFS_Assignment__c> spfsaMapping = new Map<String, School_PFS_Assignment__c>();
        for(School_PFS_Assignment__c spfsa : spfsas) {
            spfsaMapping.put(spfsa.Applicant__c, spfsa);
        }

        School_PFS_Assignment__c spfsa1 = spfsaMapping.get(applicant1A.Id);
        School_PFS_Assignment__c spfsa2 = spfsaMapping.get(applicant1B.Id);
        spfsa1.Family_Contribution__c = 10;
        update new List<School_PFS_Assignment__c> {spfsa1, spfsa2};

        Student_Folder__c updatedSF;

        Test.startTest();
            PageReference pageRef = Page.SchoolFinancialAid;
            pageRef.getParameters().put('id', String.valueOf(studentFolder1.Id));
            Test.setCurrentPage(pageRef);
            SchoolFinancialAidController testCont = new SchoolFinancialAidController();
        Test.stopTest();

        System.assertEquals(testCont.folder.Id, StudentFolder1.Id);
        System.assertEquals(10, testCont.getCombinedContribution());
    }

    @isTest
    private static void saveAndRecalcWhenSPAIsEditedOutOfTheFinancialAidPage() {
        createTestData();

        List<Student_Folder__c> studentFolders = StudentFolderSelector.newInstance().selectByStudentId_schoolId_academicYearName(
            new Set<Id>{student1A.Id}, new Set<Id>{school.Id}, new Set<String>{currentAcademicYear.Name});
        System.assertEquals(1, studentFolders.size());
        Student_Folder__c studentFolder1 = studentFolders[0];

        PFS__c pfs1 = PfsSelector.newInstance().selectByContactIdAndAcademicYearId(parentA.Id, currentAcademicYear.Id)[0];
        PFS__c pfs2 = PfsSelector.newInstance().selectByContactIdAndAcademicYearId(parentB.Id, currentAcademicYear.Id)[0];

        List<Applicant__c> applicants = ApplicantSelector.newInstance().selectByPFSandStudent(
            new Set<Id>{pfs1.Id, pfs2.Id}, new Set<Id>{student1A.Id});
        System.assertEquals(2, applicants.size());
        Map<Id, Applicant__c> applicantsMapping = new Map<Id, Applicant__c>();
        for(Applicant__c applicant : applicants) {
            applicantsMapping.put(applicant.PFS__c, applicant);
        }
        Applicant__c applicant1A = applicantsMapping.get(pfs1.Id);
        Applicant__c applicant1B = applicantsMapping.get(pfs2.Id);

        List<School_PFS_Assignment__c> spfsas = SchoolPfsAssignmentsSelector.newInstance().selectAll();
        System.assertEquals(2, spfsas.size());
        Map<String, School_PFS_Assignment__c> spfsaMapping = new Map<String, School_PFS_Assignment__c>();
        for(School_PFS_Assignment__c spfsa : spfsas) {
            spfsaMapping.put(spfsa.Applicant__c, spfsa);
        }

        School_PFS_Assignment__c spfsa1 = spfsaMapping.get(applicant1A.Id);
        School_PFS_Assignment__c spfsa2 = spfsaMapping.get(applicant1B.Id);

        studentFolder1.New_Returning__c = 'Returning';
        update studentFolder1;
        SchoolFinancialAidController controller;

        PageReference pageRef = Page.SchoolFinancialAid;
        pageRef.getParameters().put('id', String.valueOf(studentFolder1.Id));
        Test.setCurrentPage(pageRef);
        controller = new SchoolFinancialAidController();

        System.assertEquals(controller.folder.Id, studentFolder1.Id);
        System.assertEquals(controller.spfs1.Id, spfsa1.Id);
        System.assertEquals(controller.spfs2.Id, spfsa2.Id);
        System.assertEquals(60000, controller.spfs1.Salary_Wages_Parent_A__c);

        School_PFS_Assignment__c spa1 = new School_PFS_Assignment__c(Id=controller.spfs1.Id);
        spa1.Salary_Wages_Parent_A__c = 30000;
        update spa1;

        Test.startTest();
            System.assertEquals(null, controller.spfs1.Family_Contribution__c);
            controller.folder.New_Returning__c = 'New';
            controller.saveAndRecalc();
        Test.stopTest();

        School_PFS_Assignment__c latestSPA = [Select Id,Salary_Wages_Parent_A__c from
                                                School_PFS_Assignment__c where Id=:controller.spfs1.Id limit 1];
        System.assertEquals(30000, latestSPA.Salary_Wages_Parent_A__c);
    }//End:saveAndRecalcWhenSPAIsEditedOutOfTheFinancialAidPage

    @isTest
    private static void saveAndRecalcWhenSPAIsEditedOutOfTheFinancialAidPageAndInsideThePage(){
        createTestData();

        List<Student_Folder__c> studentFolders = StudentFolderSelector.newInstance().selectByStudentId_schoolId_academicYearName(
            new Set<Id>{student1A.Id}, new Set<Id>{school.Id}, new Set<String>{currentAcademicYear.Name});
        System.assertEquals(1, studentFolders.size());
        Student_Folder__c studentFolder1 = studentFolders[0];

        PFS__c pfs1 = PfsSelector.newInstance().selectByContactIdAndAcademicYearId(parentA.Id, currentAcademicYear.Id)[0];
        PFS__c pfs2 = PfsSelector.newInstance().selectByContactIdAndAcademicYearId(parentB.Id, currentAcademicYear.Id)[0];

        List<Applicant__c> applicants = ApplicantSelector.newInstance().selectByPFSandStudent(
            new Set<Id>{pfs1.Id, pfs2.Id}, new Set<Id>{student1A.Id});
        System.assertEquals(2, applicants.size());
        Map<Id, Applicant__c> applicantsMapping = new Map<Id, Applicant__c>();
        for(Applicant__c applicant : applicants) {
            applicantsMapping.put(applicant.PFS__c, applicant);
        }
        Applicant__c applicant1A = applicantsMapping.get(pfs1.Id);
        Applicant__c applicant1B = applicantsMapping.get(pfs2.Id);

        List<School_PFS_Assignment__c> spfsas = SchoolPfsAssignmentsSelector.newInstance().selectAll();
        System.assertEquals(2, spfsas.size());
        Map<String, School_PFS_Assignment__c> spfsaMapping = new Map<String, School_PFS_Assignment__c>();
        for(School_PFS_Assignment__c spfsa : spfsas) {
            spfsaMapping.put(spfsa.Applicant__c, spfsa);
        }

        School_PFS_Assignment__c spfsa1 = spfsaMapping.get(applicant1A.Id);
        School_PFS_Assignment__c spfsa2 = spfsaMapping.get(applicant1B.Id);

        studentFolder1.New_Returning__c = 'Returning';
        update studentFolder1;

        SchoolFinancialAidController controller;

        PageReference pageRef = Page.SchoolFinancialAid;
        pageRef.getParameters().put('id', String.valueOf(StudentFolder1.Id));
        Test.setCurrentPage(pageRef);
        controller = new SchoolFinancialAidController();

        System.assertEquals(controller.folder.Id, studentFolder1.Id);
        System.assertEquals(controller.spfs1.Id, spfsa1.Id);
        System.assertEquals(controller.spfs2.Id, spfsa2.Id);
        System.assertEquals(60000, controller.spfs1.Salary_Wages_Parent_A__c);
        System.assertEquals(null, controller.spfs1.Family_Contribution__c);

        School_PFS_Assignment__c spa1 = new School_PFS_Assignment__c(Id=controller.spfs1.Id);
        spa1.Salary_Wages_Parent_A__c = 30000;
        update spa1;

        Test.startTest();
        System.assertEquals(null, controller.spfs1.Family_Contribution__c);
        controller.folder.New_Returning__c = 'New';
        controller.spfs1.Family_Contribution__c = 3000;
        controller.saveAndRecalc();
        Test.stopTest();

        School_PFS_Assignment__c latestSPA = [Select Id,Salary_Wages_Parent_A__c, Family_Contribution__c from
                                                School_PFS_Assignment__c where Id=:controller.spfs1.Id limit 1];
        System.assertEquals(30000, latestSPA.Salary_Wages_Parent_A__c);
        System.assertEquals(3000, latestSPA.Family_Contribution__c);
    }//End:saveAndRecalcWhenSPAIsEditedOutOfTheFinancialAidPageAndInsideThePage
    
    
    @isTest
    private static void finAidHistoryInfo_folderWithPreviousAllocations_listWithPreviousBudgetAllocations(){
        
        //1. Create current and previous academic year.
        currentAcademicYear = AcademicYearTestData.Instance.create();
        previousAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, previousAcademicYear};
        
        //2. Create one School.
        school = AccountTestData.Instance.asSchool().DefaultAccount;
        
        //3. Create student.
        Contact student = ContactTestData.Instance.asStudent().create();
        insert student;
        
        //4. Create 2 folders for each Academic Year.
        Student_Folder__c studentFolderPrevious = StudentFolderTestData.Instance
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(previousAcademicYear.Name)
            .forStudentId(student.Id).create();
            
        Student_Folder__c studentFolderCurrent = StudentFolderTestData.Instance
            .forSchoolId(school.Id)
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forStudentId(student.Id).create();
            
        insert new List<Student_Folder__c>{studentFolderPrevious, studentFolderCurrent};
        
        //5. Create Budget Groups for the school.
        Budget_Group__c groupPrevious = BudgetGroupTestData.Instance
            .forAcademicYearId(previousAcademicYear.Id)
            .forSchoolId(school.Id)
            .forTotalFunds(30000).create();
        groupPrevious.Name = 'Previous Academic Year Group';
        
        Budget_Group__c groupCurrent = BudgetGroupTestData.Instance
            .forAcademicYearId(currentAcademicYear.Id)
            .forSchoolId(school.Id)
            .forTotalFunds(40000).create();
        groupCurrent.Name = 'Current Academic Year Group';
        
        insert new List<Budget_Group__c>{groupPrevious, groupCurrent};
        
        //6. Create Budget Allocations for each academic year.
        Budget_Allocation__c allocationPreviousYear = BudgetAllocationTestData.Instance
            .forBudgetGroupId(groupPrevious.Id)
            .forAmountAllocated(7000)
            .forStudentFolder(studentFolderPrevious.Id).create();
            
        Budget_Allocation__c allocationCurrentYear = BudgetAllocationTestData.Instance
            .forBudgetGroupId(groupCurrent.Id)
            .forAmountAllocated(3000)
            .forStudentFolder(studentFolderCurrent.Id).create();
            
        insert new List<Budget_Allocation__c>{allocationPreviousYear, allocationCurrentYear};
        
        Test.startTest();
            PageReference pageRef = Page.SchoolFinancialAid;
            pageRef.getParameters().put('id', String.valueOf(studentFolderCurrent.Id));
            Test.setCurrentPage(pageRef);
            SchoolFinancialAidController controller = new SchoolFinancialAidController();
            
            System.assertEquals(true, controller.finAidHistoryInfo.getHasBudgetAllocations());
            System.assertEquals(1, controller.finAidHistoryInfo.budgetAllocations.size());
            System.assertEquals('Previous Academic Year Group', controller.finAidHistoryInfo.budgetAllocations[0].Budget_Group__r.Name);
        Test.stopTest();
    }
    
    @isTest
    private static void onChangeAwardLetterSent_awardLetterSentNo_dateAwardLetterSentNull() {
        
        Student_Folder__c studentFolder = StudentFolderTestData.Instance.DefaultStudentFolder;
        
        
        PageReference pageRef = Page.SchoolFinancialAid;
        pageRef.getParameters().put('id', String.valueOf(studentFolder.Id));
        Test.setCurrentPage(pageRef);
        SchoolFinancialAidController controller = new SchoolFinancialAidController();
        
        controller.onChangeAwardLetterSent();
        System.assertEquals(null, controller.folder.Date_Award_Letter_Sent__c);
        
        controller.folder.Award_Letter_Sent__c = 'Yes';
        controller.folder.Date_Award_Letter_Sent__c = System.today();
        
        controller.onChangeAwardLetterSent();
        System.assertEquals(System.today(), controller.folder.Date_Award_Letter_Sent__c);
        
        controller.folder.Award_Letter_Sent__c = 'No';
        controller.folder.Date_Award_Letter_Sent__c = System.today();
        
        controller.onChangeAwardLetterSent();
        System.assertEquals(null, controller.folder.Date_Award_Letter_Sent__c);
    }
}