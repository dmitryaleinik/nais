/**
 * @description This service is used to handle operations that are common throughout different points of the Financial
 *              Aid API. This can include API activation within connected app plugins, logging exceptions, etc.
 */
public class FinancialAidApiService {

    private static final String AUTH_REQUEST_PARAM = 'authRequest';
    private static final String ACTIVATE_REQUEST_PARAM = 'activateRequest';

    private static final String FIN_AID_AUTH_LOG_SOURCE = 'Financial Aid API - Authentication';

    private static final String SCHOOL_ACCESS_ERROR_FORMAT = 'The current user does not have access to the school with Id {0}';
    private static final String AUTHORIZE_ERROR_FORMAT = 'There was an unexpected problem authorizing user with Id {0} into the connected app named {1}.';

    private static final String NEW_LINE = '\n';
    private static final String MULTI_SELECT_DELIMITER = ';';

    /**
     * @description Private constructor. Do not instantiate inline. Instead use static Instance property.
     */
    private FinancialAidApiService() {}

    /**
     * @description Flags the specified user with the name of the connected app that is authorizing access to the Financial
     *              Aid API. The name of the connected app is stamped on the user so that we can activate the API for
     *              the school they are working with when they begin interacting with the Financial Aid API. If there
     *              are any exceptions thrown during this operation, they are caught and the details are stored in a
     *              new IntegrationLog__c record.
     *
     *              This method is meant to be called during ConnectedAppPlugins.
     *
     * @param authRequest Contains the Id of the user authorizing with a connected app and the Id of that connected app.
     * @return A response containing any errors that occurred so the connected app plugin can determine if authorization was successful or not.
     * @throws ArgumentNullException if authRequest is null.
     */
    public Response authorizeUserForApi(AuthorizationRequest authRequest) {
        ArgumentNullException.throwIfNull(authRequest, AUTH_REQUEST_PARAM);

        Response authResponse = new Response();

        try {
            // Get the connected app so we can use the name to store on the user record.
            ConnectedApplication connectedApp = getConnectedAppInformation(authRequest.ConnectedAppId);

            // Store the connected app on the user that is authenticating with the connected app.
            // Before setting the connected app on the user, make sure it doesn't already have that value stored.
            flagUserWithConnectedApp(authRequest.UserId, connectedApp.Name);
        } catch (Exception ex) {
            // Add errors to response so that the response indicates that the operation wasn't successful.
            authResponse.addError(ex.getMessage());
            // Log the exception so our team can react proactively.
            logAuthorizeException(authRequest.UserId, authRequest.ConnectedAppId, ex);
        }

        return authResponse;
    }

    /**
     * @description Activates the Financial Aid API for the school being worked on by the current User. First we query
     *              the current User and check if they have been authorized for the Financial Aid API by any of our
     *              connected apps. The user record is used to store the names of Connected Apps that the user has
     *              authenticated with for the Financial Aid API. This only happens after they complete authorization
     *              with the connected app. At that point, we store the name of the connected app on the user record.
     *              Here we see if those values need to be transfered to a school account. Transferring the values to
     *              the school account allows us to determine if a school is using the API and if so, publish student
     *              folder updates through the Application Update platform events.
     *
     *              In order to transfer the connected app names to the school account, we must first query those accounts.
     *              If the user only has access to one school, we transfer the connected app names to that account. If
     *              the user has access to multiple schools, a School Id must be specified in the request otherwise
     *              an exception is thrown.
     *
     *              School accounts are only updated if they do not already have the connected app names selected that
     *              are on the user.
     *
     *              The user record will not be updated in this method. That is, the user record will retain all the
     *              connected app names to better handle revoking access later on.
     * @param activateRequest Contains the information needed to activate the API for the current user's school.
     * @return A response with any errors that were handled while performing this operation.
     * @throws ArgumentNullException if activateRequest is null.
     * @throws ActivationException if the user does not have access to any schools.
     * @throws ActivationException if the user has access to multiple schools, but the SchoolId was not specified in the request.
     * @throws ActivationException if the user has access to multiple schools, but not the School specified in the request.
     */
    public Response activateApiForCurrentUsersSchool(ActivationRequest activateRequest) {
        ArgumentNullException.throwIfNull(activateRequest, ACTIVATE_REQUEST_PARAM);

        Response activationResponse = new Response();

        // Get current user, see if they have Fin Aid Connected Apps to transfer to the school.
        // If not, we can return early since we don't have any info to transfer to school accounts.
        User currentUser = selectUserById(UserInfo.getUserId());
        if (String.isBlank(currentUser.Financial_Aid_API_Apps__c)) {
            return activationResponse;
        }

        // Get the names of the Connected Apps the user has used to access the Financial Aid API.
        Set<String> finAidApiAppsToTransfer = splitConnectedAppNames(currentUser.Financial_Aid_API_Apps__c);

        // The user record is used to store the names of Connected Apps that the user has authenticated with for the Financial Aid API. This only happens after they complete authorization with the connected app. At that point, we store the name of the connected app on the user record. Here we see if those values need to be transfered to a school account. Transferring the values to the school account allows us to determine if a school is using the API and if so, publish student folder updates through the Application Update platform events.

        // Get the school record we will be activating the API for.
        Account schoolToActivate = getSchoolToActivate(activateRequest);

        Set<String> schoolFinAidApiApps = splitConnectedAppNames(schoolToActivate.Financial_Aid_Api_Apps__c);

        // Make sure the school doesn't already have the Connected App Name from the user specified.
        // If it does, we don't need to update the account.
        finAidApiAppsToTransfer.removeAll(schoolFinAidApiApps);

        if (finAidApiAppsToTransfer.isEmpty()) {
            return activationResponse;
        }

        // We won't clear out the Financial Aid App values from the user after transfering them to the account.
        // This should make it easier for us to handle revoking access for a user later on and determining whether or not we also need to deactivate the integration for a school.
        // For example, we can check if a school has any remaining users with access to the API before removing the flag on the school.
        updateSchoolFinancialAidApps(schoolToActivate, finAidApiAppsToTransfer);

        return activationResponse;
    }

    private User selectUserById(Id userId) {
        return [SELECT Id, Financial_Aid_Api_Apps__c FROM User WHERE Id = :userId LIMIT 1];
    }

    private ConnectedApplication getConnectedAppInformation(Id connectedAppId) {
        return [SELECT Name From ConnectedApplication WHERE Id = :connectedAppId];
    }

    /**
     * @description Queries the user then updates the User.Financial_Aid_Api_Apps__c field with the name of this connected app if it doesn't already contain the name of this connected app. This allows us to transfer the values to the school account later when the user accesses the Financial Aid API.
     * @param userId The Id of the user who is authenticating with the connected app.
     * @param connectedAppName The name of the current connected app.
     */
    private void flagUserWithConnectedApp(Id userId, String connectedAppName) {
        // First get the user that is authenticating. We will store the name of the connected app on that user.
        // Once the user accesses the REST API, we will transfer the stored values to the school they are accessing through the API.
        User currentUser = selectUserById(userId);

        if (isFinancialAidAppSet(currentUser, connectedAppName)) {
            return;
        }

        updateUserFinancialAidApps(currentUser, connectedAppName);
    }

    private Boolean isFinancialAidAppSet(User userToCheck, String connectedAppName) {
        return String.isNotBlank(userToCheck.Financial_Aid_Api_Apps__c) && userToCheck.Financial_Aid_Api_Apps__c.containsIgnoreCase(connectedAppName);
    }

    private void updateUserFinancialAidApps(User userToUpdate, String connectedAppName) {
        // If the user doesn't have a connected app set at all, populate the field and return.
        if (String.isBlank(userToUpdate.Financial_Aid_Api_Apps__c)) {
            userToUpdate.Financial_Aid_Api_Apps__c = connectedAppName;
        } else {
            // Otherwise, we will append the new connected app name to the current value since the Financial_Aid_Api_Apps__c field is a multiselect.
            userToUpdate.Financial_Aid_Api_Apps__c += MULTI_SELECT_DELIMITER + connectedAppName;
        }

        update userToUpdate;
    }

    private IntegrationLog__c logAuthorizeException(Id userId, Id connectedAppId, Exception exceptionToLog) {
        // First we create a short error summary which notes that this error occurred during authentication with a connected app.
        // The summary includes the userId and Connected App Name. The userId is especially important because this class
        // is actually run as the system user.
        String errorSummary = String.format(AUTHORIZE_ERROR_FORMAT, new List<String> { (String)userId, (String)connectedAppId });

        // Now we create a list of all the information we will place in the IntegrationLog__c.Errors__c field.
        // This will include the error summary, exception type, message, and stack trace.
        List<String> errorContents = new List<String> { errorSummary };
        errorContents.add('Exception Type: ' + exceptionToLog.getTypeName());
        errorContents.add('Exception Message: ' + exceptionToLog.getMessage());
        errorContents.add('Stack Trace: ' + exceptionToLog.getStackTraceString());

        IntegrationLog__c newLog = new IntegrationLog__c();
        newLog.Source__c = FIN_AID_AUTH_LOG_SOURCE; // The source is 'Financial Aid API Authentication'
        newLog.Errors__c = String.join(errorContents, NEW_LINE);
        // Setting the status to fatal since errors here mean users aren't even able to enter the API.
        newLog.Status__c = IntegrationLogs.Status.FATAL.name();

        insert newLog;

        return newLog;
    }

    private Set<String> splitConnectedAppNames(String connectedAppNameMultiSelectVal) {
        Set<String> connectedAppNames = new Set<String>();

        // Just to be safe, make sure the multi select field isn't null.
        if (String.isBlank(connectedAppNameMultiSelectVal)) {
            return connectedAppNames;
        }

        connectedAppNames.addAll(connectedAppNameMultiSelectVal.split(MULTI_SELECT_DELIMITER));

        return connectedAppNames;
    }

    private Account getSchoolToActivate(ActivationRequest request) {
        // Get current user's schools.
        // We use the GlobalVariables.cls to do this which caches the current user's schools.
        Map<Id, Account> schoolsById = new Map<Id, Account>(GlobalVariables.getAllSchoolsForUser());

        // Throw exception if the user doesn't have any schools? Or Return silently?
        if (schoolsById.isEmpty()) {
            throw new ActivationException('The current user does not have access to any schools.');
        }

        // If the user only has access to one school, return it.
        if (schoolsById.size() == 1) {
            return schoolsById.values()[0];
        }

        // If more than 1, make sure that the SchoolId property on the request is populated.
        // If not, throw an exception.
        if (schoolsById.size() > 1 && request.SchoolId == null) {
            String errorMessage = 'School Id was not specified. The current user has access to multiple schools. Multi campus users must specify the School Id they are working with.';
            throw new ActivationException(errorMessage);
        }

        // Throw exception if the specified school Id is not one of the user's schools.
        Account schoolToActivate = schoolsById.get(request.SchoolId);
        if (schoolToActivate == null) {
            String errorMessage = String.format(SCHOOL_ACCESS_ERROR_FORMAT, new List<String> { (String)request.SchoolId });
            throw new ActivationException(errorMessage);
        }

        return schoolToActivate;
    }

    private void updateSchoolFinancialAidApps(Account schoolToActivate, Set<String> financialAidAppsToAdd) {
        String newFinAidAppValues = String.join(new List<String>(financialAidAppsToAdd), MULTI_SELECT_DELIMITER);

        if (String.isBlank(schoolToActivate.Financial_Aid_Api_Apps__c)) {
            schoolToActivate.Financial_Aid_Api_Apps__c = newFinAidAppValues;
        } else {
            schoolToActivate.Financial_Aid_Api_Apps__c += MULTI_SELECT_DELIMITER + newFinAidAppValues;
        }

        update schoolToActivate;
    }

    /**
     * @description This class contains information necessary for activating the Financial Aid API for a user and the
     *              school they are currently working with.
     */
    public class ActivationRequest {

        /**
         * @description Constructor for an API activation request for the current user and school.
         *              School information is optional and will only be required for multi campus users.
         * @param schoolIdToActivate The Id of the school to activate the API for. This is an optional parameter.
         *                           This is only necessary for multi school users.
         */
        public ActivationRequest(Id schoolIdToActivate) {
            this.SchoolId = schoolIdToActivate;
        }

        /**
         * @description The Id of the school that requires activation. Necessary to identify the correct school for multi campus users.
         */
        public Id SchoolId { get; private set; }
    }

    /**
     * @description A request to handle authorization which occurs while authenticating through a connected app. Connected
     *              apps are used to access the Financial Aid API. The request contains the Id of the user and connected
     *              app so the connected app name can be stamped on the user who is authenticating.
     */
    public class AuthorizationRequest {

        public AuthorizationRequest(Id userIdToAuthorize, Id appUsedToAuthorize) {
            this.UserId = userIdToAuthorize;
            this.ConnectedAppId = appUsedToAuthorize;
        }

        /**
         * @description The Id of the user being authorized by the connected app. This is necessary because the
         *              connected app runs as the system user.
         */
        public Id UserId { get; private set; }

        /**
         * @description The Id of the connected app doing the authorization. This is used to retrieve the connected app
         *              name so we can flag users and schools actively using the API.
         */
        public Id ConnectedAppId { get; private set; }
    }

    /**
     * @description Contains any errors that may have occurred while calling this service. Can be used to determine if an operation was successful.
     */
    public class Response {

        private List<String> errors;

        /**
         * @description Default constructor.
         */
        public Response() {
            errors = new List<String>();
        }

        /**
         * @description Adds an error to the response.
         */
        public void addError(String errorToAdd) {
            this.errors.add(errorToAdd);
        }

        /**
         * @description Returns true if the response does not contain any errors.
         */
        public Boolean isSuccessful() {
            return errors.isEmpty();
        }
    }

    /**
     * @description Exception to be thrown when we don't have enough information to activate the Financial Aid API for a user and school.
     */
    public class ActivationException extends Exception { }

    /**
     * @description Singleton instance.
     */
    public static FinancialAidApiService Instance {
        get {
            if (Instance == null) {
                Instance = new FinancialAidApiService();
            }
            return Instance;
        }
        private set;
    }
}