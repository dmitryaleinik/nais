public class DocumentUploadSettings {
    
    private static Document_Upload_Settings__c record;
    private static Set<String> fieldNames;

    private static Document_Upload_Settings__c getInstance() {
        if (record != null) {
            return record;
        }

        record = Document_Upload_Settings__c.getInstance();
        
        if (record == null || Test.isRunningTest()) {
            record = getDefault();
        }

        return record;
    }
    
    public static Boolean isEnabled(String settingName) {
        ArgumentNullException.throwIfNull(settingName, 'settingName');

        return hasField(settingName) ? (Boolean)getInstance().get(settingName) : false;
    }
    
    public static Integer getValue(String settingName) {
        ArgumentNullException.throwIfNull(settingName, 'settingName');

        return hasField(settingName) ? Integer.ValueOf(getInstance().get(settingName)) : null;
    }

    public static String getStringValue(String settingName)
    {
        ArgumentNullException.throwIfNull(settingName, 'settingName');

        return hasField(settingName) ? String.ValueOf(getInstance().get(settingName)) : null;
    }
    
    public static Boolean getBooleanValue(String settingName)
    {
        ArgumentNullException.throwIfNull(settingName, 'settingName');

        return hasField(settingName) ? Boolean.ValueOf(getInstance().get(settingName)) : null;
    }
    
    @testVisible private static void setSetting(String fieldName, Boolean fieldValue) {
        ArgumentNullException.throwIfNull(fieldName, 'fieldName');
        ArgumentNullException.throwIfNull(fieldValue, 'fieldValue');
        
        if (hasField(fieldName)) {
            getInstance().put(fieldName, fieldValue);
        }
    }
    
    @testVisible private static void setSetting(String fieldName, Decimal fieldValue) {
        ArgumentNullException.throwIfNull(fieldName, 'fieldName');
        ArgumentNullException.throwIfNull(fieldValue, 'fieldValue');
        
        if (hasField(fieldName)) {
            getInstance().put(fieldName, fieldValue);
        }
    }
    
    @testVisible private static void setSetting(String fieldName, String fieldValue) {
        ArgumentNullException.throwIfNull(fieldName, 'fieldName');
        ArgumentNullException.throwIfNull(fieldValue, 'fieldValue');
        
        if (hasField(fieldName)) {
            getInstance().put(fieldName, fieldValue);
        }
    }
    
    private static Document_Upload_Settings__c getDefault() {
        return (Document_Upload_Settings__c)Document_Upload_Settings__c.SObjectType.newSObject(null, true);
    }

    private static Set<String> getFieldNames() {
        if (fieldNames != null) {
            return fieldNames;
        }

        fieldNames = new Set<String>();

        for (String fieldName : Schema.SObjectType.Document_Upload_Settings__c.fields.getMap().keySet()) {
            fieldNames.add(fieldName.toLowerCase());
        }

        return fieldNames;
    }

    private static Boolean hasField(String fieldName) {
        return String.isNotBlank(fieldName) && getFieldNames().contains(fieldName.toLowerCase());
    }
    
    public static Decimal maxFileSize {
        
        get {
            if (maxFileSize == null) {
               maxFileSize = getInstance().Max_File_Size__c;
            }
            
            return maxFileSize;
        }
        
        set;
    }
    
    public static Boolean restrictFileSize {
        
        get {
            if (restrictFileSize == null) {
               restrictFileSize = getInstance().Restrict_File_Size__c;
            }
            
            return restrictFileSize;
        }
        
        set;
    }
}