@IsTest
private class CallCenterDispatcherTest
{

    private static void setupData()
    {
        DNIS__c parentDNIS  =new DNIS__c();

        parentDNIS.Name='parentdnis';
        parentDNIS.isParentDNIS__c = true;

        DNIS__c schoolDNIS = new DNIS__c();
        schoolDNIS.Name='schooldnis';
        schoolDNIS.isSchoolDNIS__c = true;
        insert new List<DNIS__C> {schoolDNIS,parentDNIS};
    }

    @isTest
    private static void testSchoolDispatch_useSchoolDNIS_expectVisualforceUrl()
    {
        setupData();

        User testUser = createTestUser();
        CurrentUser.currentUserRecord = testUser;

        PageReference pageRef = new PageReference('/apex/CallCenterAction?dnis=schooldnis&callid=callid&ani=(222) -123-7232');
        Test.setCurrentPage(pageRef);

        CallCenterDispatcher controller = new CallCenterDispatcher();
        PageReference redir;
        System.runAs(testUser) {
            redir = controller.getRedir();
        }

        Cookie cDnis = redir.getCookies().get('dnis');
        Cookie cAni =  redir.getCookies().get('ani');
        Cookie cCallId =  redir.getCookies().get('callID');
        System.assertEquals(cDnis.getValue(),'schooldnis');

        System.assertEquals(cAni.getValue(), '(222) -123-7232');

        System.assertEquals(cCallId.getValue(), 'callid');

        String expectedUrl = '/apex/CallCenterSchoolCaller?' +
                'ani=' + Encodingutil.urlEncode('(222) -123-7232','UTF-8') +
                '&callID=callid&dnis=schooldnis';

        System.assertEquals(expectedUrl, redir.geturl());
    }

    @isTest
    private static void testParentDispatch_useParentDNIS_expectVisualforceUrl()
    {
        setupData();

        User testUser = createTestUser();
        CurrentUser.currentUserRecord = testUser;

        PageReference pageRef = new PageReference('/apex/CallCenterAction?dnis=parentdnis&callid=callid&ani=(222) -123-7232');
        Test.setCurrentPage(pageRef);

        CallCenterDispatcher controller = new CallCenterDispatcher();
        PageReference redir;
        System.runAs(testUser) {
            redir = controller.getRedir();
        }

        Cookie cDnis = redir.getCookies().get('dnis');
        Cookie cAni =  redir.getCookies().get('ani');
        Cookie cCallId =  redir.getCookies().get('callID');
        System.assertEquals(cDnis.getValue(),'parentdnis');

        System.assertEquals(cAni.getValue(), '(222) -123-7232');

        System.assertEquals(cCallId.getValue(), 'callid');

        String actualUrl = redir.getUrl();

        System.assertEquals(true, String.isNotBlank(actualUrl), 'Expected actual url to not be blank');
        System.assertEquals(true, actualUrl.startsWithIgnoreCase('/console'),
                'Expected console url when use console flag is true for the user.');
    }

    private static User createTestUser()
    {
        return TestUtils.createPlatformUser(
                'Test Administrator', 'testemail' + Math.random() + '@test.org', 'test123', GlobalVariables.sysAdminProfileId, true, false);
    }
}