@isTest
private class AttachmentsControllerTest {

    private static final String ATTACHMENT_NAME = 'Unit Test Attachment';
    private static final String ACCOUNT_NAME = 'Test Account';

    @testSetup
    private static void setupData() {
        Account acct = new Account(Name=ACCOUNT_NAME);
        insert acct;
        insert createAttachment(acct.Id);
    }

    private static Attachment createAttachment(Id parentId) {
        return new Attachment(
                Name = ATTACHMENT_NAME,
                Description = ATTACHMENT_NAME,
                Body = Blob.valueOf('Content'),
                IsPrivate = false,
                ParentId = parentId);
    }

    private static Attachment getTestAttachment() {
        List<Attachment> attachments = [SELECT Id FROM Attachment WHERE Name = :ATTACHMENT_NAME];
        return attachments[0];
    }

    private static AttachmentsController createControllerWithRecordId() {
        AttachmentsController controller = new AttachmentsController();
        controller.RecordId = [SELECT Id FROM Account WHERE Name = :ACCOUNT_NAME].Id;
        return controller;
    }

    @isTest
    private static void getHasAttachments_none_expectFalse() {
        AttachmentsController controller = new AttachmentsController();
        System.assert(!controller.getHasAttachments(),
                'Expected getHasAttachments to return false with no attachments.');
    }

    @isTest
    private static void getHasAttachments_withAttachment_expectFalse() {
        AttachmentsController controller = createControllerWithRecordId();
        System.assert(controller.getHasAttachments(),
                'Expected getHasAttachments to return false with an attachment.');
    }

    @isTest
    private static void attachments_expectAttachmentReturned() {
        AttachmentsController controller = createControllerWithRecordId();
        System.assertEquals(1, controller.Attachments.size(), 'Expected only one attachment to be returned.');
        System.assertEquals(ATTACHMENT_NAME, controller.Attachments[0].Name,
                'Expected test attachment to be returned.');
    }

    @isTest
    private static void upload_expectAttachmentInserted() {
        String newAttachmentName = 'New Attachment';
        AttachmentsController controller = createControllerWithRecordId();
        controller.NewAttachment.Name = newAttachmentName;
        controller.NewAttachment.Body = Blob.valueOf('Content');
        controller.upload();

        Attachment uploadedAttachment = [SELECT Id, Name, ParentId, IsPrivate FROM Attachment WHERE Name = :newAttachmentName];
        System.assertEquals(controller.RecordId, uploadedAttachment.ParentId,
                'Expected ParentId to be that of the record.');
        System.assert(!uploadedAttachment.IsPrivate, 'Expected IsPrivate to be set to false.');
        System.assertEquals(2, controller.Attachments.size(),
                'Expected test attachment and new attachment to be returned.');
        System.assert(!ApexPages.hasMessages(ApexPages.Severity.ERROR),
                'Expected no error messages to be on the page.');
        System.assertEquals(null, controller.NewAttachment.Id,
                'Expected new attachment property to not have an id after upload.');
    }

    @isTest
    private static void deleteAttachment_expectAttachmentDeleted() {
        PageReference pageRef = Page.SchoolFolderSummary;
        pageRef.getParameters().put(AttachmentsController.ID_DELETE_PARAM, getTestAttachment().Id);
        Test.setCurrentPage(pageRef);
        AttachmentsController controller = createControllerWithRecordId();
        controller.deleteAttachment();
        System.assertEquals(0, controller.Attachments.size(), 'Expected no attachments to be returned after delete.');
        System.assertEquals(0, [SELECT Id FROM Attachment].size(), 'Expected no attachments to be in Salesforce.');
        System.assert(!ApexPages.hasMessages(ApexPages.Severity.ERROR),
                'Expected no error messages to be on the page.');
    }

    @isTest
    private static void deleteAttachment_attachmentForDifferentRecord_expectErrorMessage() {
        Account accountNotForController = new Account(Name = 'Separate Account');
        insert accountNotForController;
        Attachment attachmentWithoutAccess = createAttachment(accountNotForController.Id);
        insert attachmentWithoutAccess;

        PageReference pageRef = Page.SchoolFolderSummary;
        pageRef.getParameters().put(AttachmentsController.ID_DELETE_PARAM, attachmentWithoutAccess.Id);
        Test.setCurrentPage(pageRef);

        AttachmentsController controller = createControllerWithRecordId();
        System.assertEquals(1, controller.Attachments.size(), 'Expected only one attachment to be returned.');

        controller.deleteAttachment();

        System.assertEquals(1, controller.Attachments.size(), 'Expected correct attachment to still be returned.');
        System.assertEquals(2, [SELECT Id FROM Attachment].size(),
                'Expected no attachments to be deleted in Salesforce.');
        System.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR),
                'Expected error message to be on the page.');
    }

    @isTest
    private static void renderEditAttachment_expectAttachmentForEditSet() {
        PageReference pageRef = Page.SchoolFolderSummary;
        pageRef.getParameters().put(AttachmentsController.ID_EDIT_PARAM, getTestAttachment().Id);
        Test.setCurrentPage(pageRef);
        AttachmentsController controller = createControllerWithRecordId();
        controller.renderEditAttachment();
        System.assertNotEquals(null, controller.AttachmentForEdit, 'Expected AttachmentForEdit to be set.');
        System.assertEquals(getTestAttachment().Id, controller.AttachmentForEdit.Id,
                'Expected AttachmentForEdit to be the attachment passed in the ID_EDIT_PARAM.');
        System.assert(!ApexPages.hasMessages(ApexPages.Severity.ERROR),
                'Expected no error messages to be on the page.');
    }

    @isTest
    private static void updateAttachment_expectAttachmentDescriptionUpdated() {
        String newName = 'Updated Name';
        AttachmentsController controller = createControllerWithRecordId();
        Attachment testAttachment = getTestAttachment();
        testAttachment.Description = newName;
        controller.AttachmentForEdit = testAttachment;
        controller.updateAttachment();
        System.assertEquals(newName, [SELECT Description FROM Attachment].Description,
                'Expected attachment description to be updated.');
        System.assertEquals(newName, controller.Attachments[0].Description,
                'Expected Attachments list to be updated with the attachment having a new description.');
        System.assert(!ApexPages.hasMessages(ApexPages.Severity.ERROR),
                'Expected no error messages to be on the page.');
    }

}