@isTest
private class SchoolBizFarmAssignmentActionTest
{
    @isTest
    private static void sbfaCreatedOnSchoolPFSAssignmentInsert()
    {
        Account school1 = TestUtils.createAccount('Test School 1', RecordTypes.schoolAccountTypeId, 3, false);
        Account school2 = TestUtils.createAccount('Test School 2', RecordTypes.schoolAccountTypeId, 3, false);
        insert new List<Account> { school1, school2 };

        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        Contact parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);

        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        Contact student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);

        insert new List<Contact> { parentA, parentB, student1, student2 };

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
        PFS__c pfsB = TestUtils.createPFS('PFS B', academicYearId, parentB.Id, false);
        insert new List<PFS__c> { pfsA, pfsB };

        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, false);
        Applicant__c applicant1B = TestUtils.createApplicant(student1.Id, pfsB.Id, false);
        Applicant__c applicant2A = TestUtils.createApplicant(student2.Id, pfsA.Id, false);
        insert new List<Applicant__c> { applicant1A, applicant1B, applicant2A };

        Student_Folder__c studentFolder11 = TestUtils.createStudentFolder('Student Folder 1.1', academicYearId, student1.Id, false);
        Student_Folder__c studentFolder21 = TestUtils.createStudentFolder('Student Folder 2.1', academicYearId, student2.Id, false);
        Student_Folder__c studentFolder22 = TestUtils.createStudentFolder('Student Folder 2.2', academicYearId, student2.Id, false);
        insert new List<Student_Folder__c> { studentFolder11, studentFolder21, studentFolder22 };

        List<Business_Farm__c> bizFarmList = new List<Business_Farm__c>();
        bizFarmList.add(TestUtils.createBusinessFarm(pfsA.Id, 'MyBiz1', 'Business', 'Sole Proprietorship', false));
        bizFarmList.add(TestUtils.createBusinessFarm(pfsA.Id, 'MyBiz2', 'Business', 'Sole Proprietorship', false));
        bizFarmList.add(TestUtils.createBusinessFarm(pfsB.Id, 'MyBiz3', 'Farm', 'Sole Proprietorship', false));
        insert bizFarmList;


        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder11.Id, false);
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1B.Id, school1.Id, studentFolder11.Id, false);
        School_PFS_Assignment__c spfsa3 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.Id, school1.Id, studentFolder21.Id, false);
        School_PFS_Assignment__c spfsa4 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.Id, school2.Id, studentFolder22.Id, false);
        List<School_PFS_Assignment__c> allSPFSAs = new List<School_PFS_Assignment__c> { spfsa1, spfsa2, spfsa3, spfsa4 };

        Test.startTest();
            insert allSPFSAs;
        Test.stopTest();

        // first three SPAs have two businesses each, fourth SPA has one.  3 x 2 + 1 = 7
        List<School_Biz_Farm_Assignment__c> sbfaList = [select Id, Business_Farm__c from School_Biz_Farm_Assignment__c where School_PFS_Assignment__c in :allSPFSAs];
        System.assertEquals(7, sbfaList.size());

        Integer b0 = 0;
        Integer b1 = 0;
        Integer b2 = 0;

        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){
            if (sbfa.Business_Farm__c == bizFarmList[0].Id){b0++;}
            if (sbfa.Business_Farm__c == bizFarmList[1].Id){b1++;}
            if (sbfa.Business_Farm__c == bizFarmList[2].Id){b2++;}
        }

        System.assertEquals(3, b0);
        System.assertEquals(3, b1);
        System.assertEquals(1, b2);
    }

    // [DP] 08.24.2015 NAIS-2527 efc recalc status should get set to recalc batch
    @isTest
    private static void spaEFCRecalcOnSBFAInsert()
    {
        Account school1 = TestUtils.createAccount('Test School 1', RecordTypes.schoolAccountTypeId, 3, false);
        Account school2 = TestUtils.createAccount('Test School 2', RecordTypes.schoolAccountTypeId, 3, false);
        insert new List<Account> { school1, school2 };

        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        Contact parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);

        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        Contact student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);

        insert new List<Contact> { parentA, parentB, student1, student2 };

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
        PFS__c pfsB = TestUtils.createPFS('PFS B', academicYearId, parentB.Id, false);

        // submit the PFSs
        pfsA.PFS_Status__c = EfcPicklistValues.PFS_STATUS_SUBMITTED;
        pfsA.Payment_Status__c = EfcPicklistValues.PAYMENT_STATUS_PAID_IN_FULL;
        pfsB.PFS_Status__c = EfcPicklistValues.PFS_STATUS_SUBMITTED;
        pfsB.Payment_Status__c = EfcPicklistValues.PAYMENT_STATUS_PAID_IN_FULL;

        insert new List<PFS__c> { pfsA, pfsB };

        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, false);
        Applicant__c applicant1B = TestUtils.createApplicant(student1.Id, pfsB.Id, false);
        Applicant__c applicant2A = TestUtils.createApplicant(student2.Id, pfsA.Id, false);
        insert new List<Applicant__c> { applicant1A, applicant1B, applicant2A };

        Student_Folder__c studentFolder11 = TestUtils.createStudentFolder('Student Folder 1.1', academicYearId, student1.Id, false);
        Student_Folder__c studentFolder21 = TestUtils.createStudentFolder('Student Folder 2.1', academicYearId, student2.Id, false);
        Student_Folder__c studentFolder22 = TestUtils.createStudentFolder('Student Folder 2.2', academicYearId, student2.Id, false);
        insert new List<Student_Folder__c> { studentFolder11, studentFolder21, studentFolder22 };

        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder11.Id, false);
        List<School_PFS_Assignment__c> allSPFSAs = new List<School_PFS_Assignment__c> { spfsa1 };

        insert allSPFSAs;

        List<Business_Farm__c> bizFarmList = new List<Business_Farm__c>();
        bizFarmList.add(TestUtils.createBusinessFarm(pfsA.Id, 'MyBiz1', 'Business', 'Sole Proprietorship', false));

        Test.startTest();
        insert bizFarmList;
        Test.stopTest();


        List<School_Biz_Farm_Assignment__c> sbfaList = [select Id, Business_Farm__c, School_PFS_Assignment__c, School_PFS_Assignment__r.Revision_EFC_Calc_Status__c from School_Biz_Farm_Assignment__c where School_PFS_Assignment__c in :allSPFSAs];

        School_Biz_Farm_Assignment__c sbfaForInsert = sbfaList[0].clone(false, true);

        delete sbfaList;

        insert sbfaForInsert;

        sbfaList = [select Id, Business_Farm__c, School_PFS_Assignment__c, School_PFS_Assignment__r.Revision_EFC_Calc_Status__c from School_Biz_Farm_Assignment__c where School_PFS_Assignment__c in :allSPFSAs];
        System.assertEquals(1, sbfaList.size());


        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){
            System.assertEquals(EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE, sbfa.School_PFS_Assignment__r.Revision_EFC_Calc_Status__c);
            System.debug(LoggingLevel.ERROR, 'TESTINGDrewActual: ' + sbfa.School_PFS_Assignment__r.Revision_EFC_Calc_Status__c);
        }
    }

    // [DP] 08.24.2015 NAIS-2527 efc recalc status should get set to recalc batch
    @isTest
    private static void spaEFCRecalcOnSBFAUpdate()
    {
        Account school1 = TestUtils.createAccount('Test School 1', RecordTypes.schoolAccountTypeId, 3, false);
        Account school2 = TestUtils.createAccount('Test School 2', RecordTypes.schoolAccountTypeId, 3, false);
        insert new List<Account> { school1, school2 };

        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        Contact parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);

        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        Contact student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);

        insert new List<Contact> { parentA, parentB, student1, student2 };

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);

        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
        PFS__c pfsB = TestUtils.createPFS('PFS B', academicYearId, parentB.Id, false);

        // submit the PFSs
        pfsA.PFS_Status__c = EfcPicklistValues.PFS_STATUS_SUBMITTED;
        pfsA.Payment_Status__c = EfcPicklistValues.PAYMENT_STATUS_PAID_IN_FULL;
        pfsB.PFS_Status__c = EfcPicklistValues.PFS_STATUS_SUBMITTED;
        pfsB.Payment_Status__c = EfcPicklistValues.PAYMENT_STATUS_PAID_IN_FULL;

        insert new List<PFS__c> { pfsA, pfsB };

        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, false);
        Applicant__c applicant1B = TestUtils.createApplicant(student1.Id, pfsB.Id, false);
        Applicant__c applicant2A = TestUtils.createApplicant(student2.Id, pfsA.Id, false);
        insert new List<Applicant__c> { applicant1A, applicant1B, applicant2A };

        Student_Folder__c studentFolder11 = TestUtils.createStudentFolder('Student Folder 1.1', academicYearId, student1.Id, false);
        Student_Folder__c studentFolder21 = TestUtils.createStudentFolder('Student Folder 2.1', academicYearId, student2.Id, false);
        Student_Folder__c studentFolder22 = TestUtils.createStudentFolder('Student Folder 2.2', academicYearId, student2.Id, false);
        insert new List<Student_Folder__c> { studentFolder11, studentFolder21, studentFolder22 };

        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder11.Id, false);
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1B.Id, school1.Id, studentFolder11.Id, false);
        School_PFS_Assignment__c spfsa3 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.Id, school1.Id, studentFolder21.Id, false);
        School_PFS_Assignment__c spfsa4 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.Id, school2.Id, studentFolder22.Id, false);
        List<School_PFS_Assignment__c> allSPFSAs = new List<School_PFS_Assignment__c> { spfsa1, spfsa2, spfsa3, spfsa4 };

        Test.startTest();
        insert allSPFSAs;

        List<Business_Farm__c> bizFarmList = new List<Business_Farm__c>();
        bizFarmList.add(TestUtils.createBusinessFarm(pfsA.Id, 'MyBiz1', 'Business', 'Sole Proprietorship', false));
        bizFarmList.add(TestUtils.createBusinessFarm(pfsA.Id, 'MyBiz2', 'Business', 'Sole Proprietorship', false));
        bizFarmList.add(TestUtils.createBusinessFarm(pfsB.Id, 'MyBiz3', 'Farm', 'Sole Proprietorship', false));
        insert bizFarmList;

        Test.stopTest();

        // first three SPAs have two businesses each, fourth SPA has one.  3 x 2 + 1 = 7
        List<School_Biz_Farm_Assignment__c> sbfaList = [select Id, Business_Farm__c, School_PFS_Assignment__c, School_PFS_Assignment__r.Revision_EFC_Calc_Status__c from School_Biz_Farm_Assignment__c where School_PFS_Assignment__c in :allSPFSAs];
        System.assertEquals(7, sbfaList.size());

        Integer b0 = 0;
        Integer b1 = 0;
        Integer b2 = 0;

        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){
            if (sbfa.Business_Farm__c == bizFarmList[0].Id){b0++;}
            if (sbfa.Business_Farm__c == bizFarmList[1].Id){b1++;}
            if (sbfa.Business_Farm__c == bizFarmList[2].Id){b2++;}

            // Not recalc yet
            System.assertNotEquals(EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE, sbfa.School_PFS_Assignment__r.Revision_EFC_Calc_Status__c);
            sbfa.Business_Farm_Assets__c = 101;
        }

        // this update should cause recalc
        update sbfaList;

        sbfaList = [select Id, Business_Farm__c, School_PFS_Assignment__c, School_PFS_Assignment__r.Revision_EFC_Calc_Status__c from School_Biz_Farm_Assignment__c where School_PFS_Assignment__c in :allSPFSAs];
        System.assertEquals(7, sbfaList.size());

        for (School_Biz_Farm_Assignment__c sbfa : sbfaList){
            // now this is recalc status
            System.assertEquals(EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE, sbfa.School_PFS_Assignment__r.Revision_EFC_Calc_Status__c);
            System.debug(LoggingLevel.ERROR, 'TESTINGDrewActual: ' + sbfa.School_PFS_Assignment__r.Revision_EFC_Calc_Status__c);
        }
    }

    //SFP-55, [G.S]
    @isTest
    private static void testFieldCountWithSBFSA()
    {
        User usr = getPortalUser();
        
        System.runAs(usr)
        {
            Account school1 = AccountTestData.Instance.asSchool().DefaultAccount;

            Contact parentA = ContactTestData.Instance.asParent().create();
            Contact student1 = ContactTestData.Instance.asStudent().create();
            insert new List<Contact> { parentA, student1};

            Academic_Year__c academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
            TestUtils.createUnusualConditions(academicYear.Id, true);

            PFS__c pfsA = PfsTestData.Instance
                .asSubmitted()
                .asPaid()
                .forParentA(parentA.Id)
                .forAcademicYearPicklist(academicYear.Name).DefaultPfs;

            Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, false);
            insert applicant1A;

            Student_Folder__c studentFolder11 = TestUtils.createStudentFolder('Student Folder 1.1', academicYear.Id, student1.Id, false);
            insert studentFolder11;

            School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYear.Id, applicant1A.Id, school1.Id, studentFolder11.Id, false);
            
            Test.startTest();
                insert spfsa1;

                List<Business_Farm__c> bizFarmList = new List<Business_Farm__c>();
                bizFarmList.add(TestUtils.createBusinessFarm(pfsA.Id, 'MyBiz1', 'Business', 'Sole Proprietorship', false));
                insert bizFarmList;

                List<Field_Count__c> fcList = [
                    SELECT Id 
                    FROM Field_Count__c 
                    WHERE School__c = :school1.Id];
                System.assertEquals(0,fcList.size());

                List<School_Biz_Farm_Assignment__c> sbfaList = [
                    SELECT Id, Business_Farm__c, School_PFS_Assignment__c, 
                        School_PFS_Assignment__r.Revision_EFC_Calc_Status__c 
                    FROM School_Biz_Farm_Assignment__c
                    WHERE School_PFS_Assignment__c = :spfsa1.Id];
                School_Biz_Farm_Assignment__c sbfa = sbfaList.get(0);
                sbfa.Business_Farm_Assets__c = 100;
                sbfa.Business_Farm_Debts__c = 100;
                sbfa.Net_Profit_Loss_Business_Farm__c = 100;
                sbfa.Business_Farm_Owner__c = 'Test';
                update sbfa;

                fcList = [
                    SELECT Id,Business_Farm_Owner__c, Business_Farm_Assets__c, Business_Farm_Debts__c, 
                        Business_Farm_Ownership_Percent__c, Net_Profit_Loss_Business__c from Field_Count__c 
                    WHERE School__c = :school1.Id 
                        AND Academic_Year__c = :academicYear.Id];
                System.assertEquals(1,fcList.size());
                Field_Count__c fc = fcList.get(0);
                System.assertEquals(1,fc.Business_Farm_Assets__c);
                System.assertEquals(1,fc.Business_Farm_Debts__c);
                System.assertEquals(1,fc.Net_Profit_Loss_Business__c);
                System.assertEquals(1,fc.Business_Farm_Owner__c);
            Test.stopTest();
        }
    }

    @isTest
    private static void testCreateChildSbizFarmAs()
    {   
        Account school1 = AccountTestData.Instance.asSchool().create();
        Account school2 = AccountTestData.Instance.asSchool().create();
        insert new List<Account>{school1, school2};

        Pfs__c pfsA = PfsTestData.Instance.create();
        insert new List<Pfs__c>{pfsA};

        Applicant__c applicantA = ApplicantTestData.Instance
            .forPfsId(pfsA.Id).create();
        Applicant__c applicantB = ApplicantTestData.Instance
            .forPfsId(pfsA.Id).create();
        Applicant__c applicantC = ApplicantTestData.Instance
            .forPfsId(pfsA.Id).create();
        Applicant__c applicantAnotherSchool = ApplicantTestData.Instance
            .forPfsId(pfsA.Id).create();
        insert new List<Applicant__c>{applicantA, applicantB, applicantC, applicantAnotherSchool};

        Business_Farm__c businessFarmA = BusinessFarmTestData.Instance
            .forPfsId(pfsA.Id).create();
        Business_Farm__c businessFarmAnotherSchool = BusinessFarmTestData.Instance
            .forPfsId(pfsA.Id).create();
        insert new List<Business_Farm__c>{businessFarmA, businessFarmAnotherSchool};

        School_PFS_Assignment__c spfsaA = SchoolPfsAssignmentTestData.Instance
            .forSchoolId(school1.Id)
            .forApplicantId(applicantA.Id).create();
        School_PFS_Assignment__c spfsaB = SchoolPfsAssignmentTestData.Instance
            .forSchoolId(school1.Id)
            .forApplicantId(applicantB.Id).create();
        School_PFS_Assignment__c spfsaC = SchoolPfsAssignmentTestData.Instance
            .forSchoolId(school1.Id)
            .forApplicantId(applicantC.Id).create();
        School_PFS_Assignment__c spfsaAnotherSchool = SchoolPfsAssignmentTestData.Instance
            .forSchoolId(school2.Id)
            .forApplicantId(applicantAnotherSchool.Id).create();
        insert new List<School_PFS_Assignment__c>{spfsaA, spfsaB, spfsaC, spfsaAnotherSchool};

        String sbfaOwner = 'Parent A';
        Integer sbfaBusinessFarmOwnershipPercent = 100;
        Integer sbfaBusinessFarmAssets = 100;
        Integer sbfaBusinessFarmDebts = 100;
        Integer sbfaNetProfitLossBusinessFarm = 100;
        Integer sbfaBusinessFarmEquityShare = 100;

        School_Biz_Farm_Assignment__c sbfa1 = SchoolBusinessFarmAssignmentTestData.Instance
            .forBusinessFarm(businessFarmA.Id)
            .forBusinessFarmAssets(sbfaBusinessFarmAssets)
            .forBusinessFarmDebts(sbfaBusinessFarmDebts)
            .forBusinessFarmOwner(sbfaOwner)
            .forBusinessFarmOwnershipPercent(sbfaBusinessFarmOwnershipPercent)
            .forBusinessFarmEquityShare(sbfaBusinessFarmEquityShare)
            .forNetProfitLossBusinessFarm(sbfaNetProfitLossBusinessFarm)
            .forSchoolPfsAssignment(spfsaA.Id).create();
        School_Biz_Farm_Assignment__c sbfa2 = SchoolBusinessFarmAssignmentTestData.Instance
            .forBusinessFarm(businessFarmA.Id)
            .forBusinessFarmAssets(sbfaBusinessFarmAssets)
            .forBusinessFarmDebts(sbfaBusinessFarmDebts)
            .forBusinessFarmOwner(sbfaOwner)
            .forBusinessFarmOwnershipPercent(sbfaBusinessFarmOwnershipPercent)
            .forBusinessFarmEquityShare(sbfaBusinessFarmEquityShare)
            .forNetProfitLossBusinessFarm(sbfaNetProfitLossBusinessFarm)
            .forSchoolPfsAssignment(spfsaB.Id).create();
        School_Biz_Farm_Assignment__c sbfaAnotherSchool = SchoolBusinessFarmAssignmentTestData.Instance
            .forBusinessFarm(businessFarmAnotherSchool.Id)
            .forBusinessFarmAssets(sbfaBusinessFarmAssets)
            .forBusinessFarmDebts(sbfaBusinessFarmDebts)
            .forBusinessFarmOwner(sbfaOwner)
            .forBusinessFarmOwnershipPercent(sbfaBusinessFarmOwnershipPercent)
            .forBusinessFarmEquityShare(sbfaBusinessFarmEquityShare)
            .forNetProfitLossBusinessFarm(sbfaNetProfitLossBusinessFarm)
            .forSchoolPfsAssignment(spfsaAnotherSchool.Id).create();
        insert new List<School_Biz_Farm_Assignment__c>{sbfa1, sbfa2, sbfaAnotherSchool};

        Test.startTest();
            SchoolBizFarmAssignmentAction.createChildSbizFarmAs(new List<School_PFS_Assignment__c>());

            spfsaB = SchoolPfsAssignmentsSelector.newInstance().selectByIdWithCustomFieldList(
                new Set<Id> {spfsaC.Id}, new List<String> {'Applicant__r.Pfs__c', 'School__c'})[0];

            SchoolBizFarmAssignmentAction.createChildSbizFarmAs(new List<School_PFS_Assignment__c>{spfsaB});
        Test.stopTest();
        
        List<School_Biz_Farm_Assignment__c> sbfas = SchoolBizFarmAssignmentSelector.newInstance().selectAllWithCustomFieldList(
            new List<String> {'School_PFS_Assignment__r.School__c', 'Business_Farm__c', 'Business_Farm_Assets__c', 
                'Business_Farm_Debts__c', 'Business_Farm_Owner__c', 'Business_Farm_Ownership_Percent__c', 
                'Business_Farm_Share__c', 'Net_Profit_Loss_Business_Farm__c'});
        System.assertEquals(5, sbfas.size());
        
        for (School_Biz_Farm_Assignment__c sbfa : sbfas)
        {
            if (sbfa.Id != sbfa1.Id && sbfa.Id != sbfa2.Id && sbfa.Business_Farm__c == sbfa1.Business_Farm__c)
            {
                System.assertEquals(sbfa1.Business_Farm_Assets__c, sbfa.Business_Farm_Assets__c);
                System.assertEquals(sbfa1.Business_Farm_Debts__c, sbfa.Business_Farm_Debts__c);
                System.assertEquals(sbfa1.Business_Farm_Owner__c, sbfa.Business_Farm_Owner__c);
                System.assertEquals(sbfa1.Business_Farm_Ownership_Percent__c, sbfa.Business_Farm_Ownership_Percent__c);
                System.assertEquals(sbfa1.Business_Farm_Share__c, sbfa.Business_Farm_Share__c);
                System.assertEquals(sbfa1.Net_Profit_Loss_Business_Farm__c, sbfa.Net_Profit_Loss_Business_Farm__c);
            }
        }
    }

    private static User getPortalUser()
    {
        User loginUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        Account school = TestUtils.createAccount('Test School', RecordTypes.schoolAccountTypeId, 3, true);
        Contact staff = TestUtils.createContact('Staff 1', school.Id, RecordTypes.schoolStaffContactTypeId, true);
        Profile portalProfile = [select Id,name from Profile where Name = :ProfileSettings.SchoolAdminProfileName];
        Profile_Type_Grouping__c schoolAdminPTG = new Profile_Type_Grouping__c();
        SchoolAdminPTG.Name = 'PTG School Name';
        schoolAdminPTG.Profile_Name__c = ProfileSettings.SchoolAdminProfileName;
        schoolAdminPTG.Is_School_Admin_Profile__c = true;
        insert schoolAdminPTG;
        
        User usr;
        System.runAs(loginUser)
        {
            usr = TestUtils.createPortalUser('SchoolPAdmin 1', 'sst1@test.com', 'ss1', staff.Id, portalProfile.Id, true, true);
        }
        
        return usr;
    }
}