public class TestPSFComp {
    public School_PFS_Assignment__c shoolPFSAssign {get; private set;}
    
    public TestPSFComp() {
        loadSchoolPFSAssignment();
    }
    private void loadSchoolPFSAssignment() {
        List <School_PFS_Assignment__c> shoolPFSAssignList = [select Id, Name
            from School_PFS_Assignment__c 
            where Id = 'a07J0000003sNz7'];
        if (shoolPFSAssignList.size() > 0) {
            shoolPFSAssign = shoolPFSAssignList[0];
        }
    }
}