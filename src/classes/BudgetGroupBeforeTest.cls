/*
 * Prevent budget groups that have award amounts from being deleted
 */
@isTest
private class BudgetGroupBeforeTest
{

    @isTest
    private static void budgetGroupsWithAmountAllocatedCannotBeDeleted()
    {
        Account school = TestUtils.createAccount('School', RecordTypes.schoolAccountTypeId, 3, true);
        Contact student = TestUtils.createContact('Student', null, RecordTypes.studentContactTypeId, true);
        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        Student_Folder__c studentFolder = TestUtils.createStudentFolder('Student Folder', academicYearId, student.Id, true);
        
        Budget_Group__c budget1 = new Budget_Group__c(Name = 'Budget 1', Academic_Year__c = academicYearId, School__c = school.Id, Total_Funds__c = 5000);
        Budget_Group__c budget2 = new Budget_Group__c(Name = 'Budget 2', Academic_Year__c = academicYearId, School__c = school.Id, Total_Funds__c = 3000);
        Budget_Group__c budget3 = new Budget_Group__c(Name = 'Budget 3', Academic_Year__c = academicYearId, School__c = school.Id, Total_Funds__c = 1000);
        List<Budget_Group__c> budgets = new List<Budget_Group__c> { budget1, budget2, budget3 };
        insert budgets;
        
        Budget_Allocation__c alloc11 = new Budget_Allocation__c(Budget_Group__c = budget1.Id, Student_Folder__c = studentFolder.Id, Amount_Allocated__c = 1000);
        Budget_Allocation__c alloc21 = new Budget_Allocation__c(Budget_Group__c = budget2.Id, Student_Folder__c = studentFolder.Id, Amount_Allocated__c = 500);
        insert new List<Budget_Allocation__c> { alloc11, alloc21 };
        delete alloc21;
        
        List<Budget_Group__c> budgetGroups = [select Name, Total_Allocated__c from Budget_Group__c where Id in :budgets order by Name];
        System.assertEquals(1000,    budgetGroups[0].Total_Allocated__c);
        System.assertEquals(0,        budgetGroups[1].Total_Allocated__c);
        System.assertEquals(null,    budgetGroups[2].Total_Allocated__c);
        
        Test.startTest();
        Database.DeleteResult[] ldr = Database.delete(budgets, false);
        
        System.assertEquals(false,    ldr[0].isSuccess());        // budget1
        System.assertEquals(true,    ldr[1].isSuccess());        // budget2
        System.assertEquals(true,    ldr[2].isSuccess());        // budget3
        System.assert(ldr[0].getErrors()[0].getMessage().contains('Awards have been made from this Budget Group, so it can not be deleted.'));
        Test.stopTest();
    }
}