/**
 * SchoolPFSAssignments.cls
 *
 * @description School_PFS_Assignment__c domain class using fflib domain pattern, implemented solely for handling trigger interactions.
 *
 * @author Chase Logan @ Presence PG
 */
public class SchoolPFSAssignments extends fflib_SObjectDomain {

    // constants
    private static final String MISSING_PARAM_EXC_MSG = 'List of School_PFS_Assignment__c records is required';
    
    /**
     * @description Default constructor which will store academic year names and school Ids from the provided records.
     */
    private SchoolPFSAssignments(List<School_PFS_Assignment__c> spaList) {

        super(spaList);
        Configuration.disableTriggerCRUDSecurity();
    }

    /**
     * @description Handles logic that should be done after SPA's are updated
     * @param existingRecords The old records before any updates were made.
     */
    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {

        // ensure single trigger execution per transaction
        if (!TriggerHelper.getSchoolPFSAssignmentTriggerHasRun()) {

            // generate agg webhook event counts
            TriggerHelper.setSchoolPFSAssignmentTriggerHasRun(true);
            new MassEmailEventsService().calculateAggregateEventCounts(Trigger.newMap, existingRecords);
        }
    }
    
    /**
     * @description Handles logic that should be done before SPA's are deleted.
     */
    public override void onAfterDelete() {
        
        School_PFS_Assignment__c spa;
        Set<Id> folderIdsToClear = new Set<Id>();
        Set<Id> applicantIdsToEvalApplicationFee = new Set<Id>();
        
        for (SObject spaa : Records) {
            
            spa = (School_PFS_Assignment__c)spaa;
            
            if (needsEvaluateApplicationFee(spa)) {
                applicantIdsToEvalApplicationFee.add(spa.Applicant__c);
            }
            
            folderIdsToClear.add(spa.Student_Folder__c);
        }
        
        clearFolderRollupsAndRemoveEmptyStudentFolders(folderIdsToClear);
        
        if (!applicantIdsToEvalApplicationFee.isEmpty()) {
            FinanceAction.updateSaleTransactionForApplicantIds(applicantIdsToEvalApplicationFee, false);
        }
    }
    
    private Boolean needsEvaluateApplicationFee(School_PFS_Assignment__c spa) {
        
        return spa.PFS_Status__c == 'Application Submitted' && spa.Payment_Status__c != 'Paid in Full' && spa.Withdrawn__c != 'Yes' && spa.Applicant__c != null;
    }
    
    /**
    * @description Clear and restamp the PFS rollup fields on the related Student Folder for deleted spa records. 
    *              And delete Student Folder records without related SPAs.
    * @param folderIds The StudentFolders ids related to deleted SPAs.
    */
    private void clearFolderRollupsAndRemoveEmptyStudentFolders(Set<Id> folderIds) {
        
        if( !folderIds.isEmpty() ) {
            
            new SchoolPFSAction().clearFolderRollupsAndRemoveEmptyStudentFolders(folderIds);
        }
    }
    
    /**
     * @description Creates a new instance of the SchoolPFSAssignments domain class.
     * @param spaList The records to create the domain class for.
     * @return An instance of SchoolPFSAssignments.
     * @throws ArgumentNullException if spaList is null.
     */
    public static SchoolPFSAssignments newInstance(List<School_PFS_Assignment__c> spaList) {
        ArgumentNullException.throwIfNull(spaList, MISSING_PARAM_EXC_MSG);
        return new SchoolPFSAssignments(spaList);
    }

}