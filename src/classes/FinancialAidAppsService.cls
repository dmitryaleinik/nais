/**
 * @description This class is used to get financial aid applications with various critiera.
 */
public without sharing class FinancialAidAppsService {

    private static final String STUDENT_IDS_PARAM = 'studentIds';
    private static final String ACADEMIC_YEAR_NAME_PARAM = 'academicYearName';
    private static final String SCHOOL_TO_EXCLUDE_PARAM = 'schoolToExclude';

    private static final String NO = 'No';

    private FinancialAidAppsService() {}

    /**
     * @description Gets all student folders for the specified students and academic year that do not belong to a certain school. This is useful when one school wants to see the other schools that a family has applied to.
     * @param studentIds The Ids of contact records which represent the students.
     * @param academicYearName The name of the academic year we want folders for.
     * @param schoolToExclude The Id of the school that we don't need folders for.
     * @return A map of student folders by student Id.
     * @throws ArgumentNullException if any of the parameters are null.
     */
    public Map<Id, List<Student_Folder__c>> getOtherSchoolAppsByStudentId(Set<Id> studentIds, String academicYearName, Id schoolToExclude) {
        ArgumentNullException.throwIfNull(studentIds, STUDENT_IDS_PARAM);
        ArgumentNullException.throwIfNull(academicYearName, ACADEMIC_YEAR_NAME_PARAM);
        ArgumentNullException.throwIfNull(schoolToExclude, SCHOOL_TO_EXCLUDE_PARAM);

        Map<Id, List<Student_Folder__c>> otherSchoolFoldersByStudentId = getOtherSchoolFoldersByStudentId(studentIds, academicYearName, new Set<Id> { schoolToExclude });

        return otherSchoolFoldersByStudentId;
    }

    private Map<Id, List<Student_Folder__c>> getOtherSchoolFoldersByStudentId(Set<Id> studentIds, String academicYearName, Set<Id> schoolsToExclude) {
        Map<Id, List<Student_Folder__c>> foldersByStudentId = new Map<Id, List<Student_Folder__c>>();

        List<Student_Folder__c> foldersForAllStudents = [SELECT Id, Student__c, School__c, School__r.Name, School__r.NCES_ID__c FROM Student_Folder__c WHERE Student__c IN :studentIds AND Academic_Year_Picklist__c = :academicYearName AND School__r.SSS_Subscriber_Status__c IN :GlobalVariables.activeSSSStatuses AND All_SPAs_Withdrawn__c = :NO];
        
        for (Student_Folder__c folderRecord : foldersForAllStudents) {
            // If this folder is for a school we are excluding, then skip it.
            // We exclude folders sometimes because a school user will want to know what other schools a student has applied to.
            if (schoolsToExclude.contains(folderRecord.School__c)) {
                continue;
            }

            List<Student_Folder__c> foldersForStudent = foldersByStudentId.get(folderRecord.Student__c);
            if (foldersForStudent == null) {
                foldersForStudent = new List<Student_Folder__c>();
            }

            foldersForStudent.add(folderRecord);
            foldersByStudentId.put(folderRecord.Student__c, foldersForStudent);
        }

        return foldersByStudentId;
    }

    /**
     * @description Singleton to use when interacting with this service.
     */
    public static FinancialAidAppsService Instance {
        get {
            if (Instance == null) {
                Instance = new FinancialAidAppsService();
            }
            return Instance;
        }
        private set;
    }
}