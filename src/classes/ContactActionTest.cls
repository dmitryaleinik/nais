@isTest
private without sharing class ContactActionTest {
    
    private static Contact student, parentA;
    
    @isTest
    private static void testBulkContacts(){
        List<Contact> testContacts = new List<Contact>();
        Integer Iterations = 66;
        for (Integer i = 0; i < iterations; i++){
            testContacts.add(new Contact(LastName = 'student' + i, RecordTypeId = recordTypes.studentContactTypeId));
            testContacts.add(new Contact(LastName = 'parent' + i, RecordTypeId = recordTypes.parentContactTypeId));
            testContacts.add(new Contact(LastName = 'schoolstaff' + i, RecordTypeId = recordTypes.schoolStaffContactTypeId));
        }

        insert testContacts;

        testContacts = [Select Id, AccountId, FirstName, LastName, RecordTypeId, Account.Name from Contact where Id in :testContacts];

        Integer consWithAcct = 0;
        Integer consWithoutAcct = 0;

        for (Contact c : testContacts){
            if (c.AccountId != null){
                System.assert(c.RecordTypeId == recordTypes.studentContactTypeId || c.RecordTypeId == recordTypes.parentContactTypeId);
                System.assert(c.Account.Name == c.FirstName+' '+c.LastName+' Account');
                consWithAcct++;
            } else {
                consWithoutAcct++;
            }
        }

        System.assertEquals(2 * iterations, consWithAcct);
        System.assertEquals(iterations, consWithoutAcct);
    }

    // [CH] NAIS-1766 Adding to help avoid Mixed DML Errors
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

    @isTest
    private static void testSameEmail()
    {
        // username already exist. this will throw exception

        Account school1;
        Contact staff1, staff2;

        school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, 5, false);
        insert new List <Account> {school1};

        staff1 = TestUtils.createContact('Staff 1', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
        staff2 = TestUtils.createContact('Staff 2', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
        staff1.Email = 'teststaff1@xyz.com';
        staff2.Email = 'teststaff2@xyz.com';
        insert new List <Contact> {staff1, staff2};

        User schoolPortalUser1, schoolPortalUser2;
        schoolPortalUser1 = TestUtils.createPortalUser(staff1.LastName, 'teststaff1@xyz.com', 'alias1', staff1.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);
        schoolPortalUser2 = TestUtils.createPortalUser(staff2.LastName, 'teststaff2@xyz.com', 'alias2', staff2.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);

        String errMsg;

        System.runAs(thisUser){
            Test.startTest();
                insert new List <User> {schoolPortalUser1, schoolPortalUser2};

                try {
                    staff1.Email = 'teststaff2@xyz.com';
                    update staff1;
                } catch (exception e) {
                    errMsg = e.getMessage();
                }
            Test.stopTest();
        }

        system.assertNotEquals(null, ContactAction.errorMessage);
        system.assertNotEquals(null, errMsg);

        // verify staff1, schoolPortalUser1 not changed
        system.assertEquals('teststaff1@xyz.com', [select Email from Contact where Id = :staff1.Id].Email);
        system.assertEquals('teststaff1@xyz.com', [select UserName from User where Id = :schoolPortalUser1.Id].UserName);
        system.assertEquals('teststaff1@xyz.com', [select Email from User where Id = :schoolPortalUser1.Id].Email);
    }

    @isTest
    private static void testDifferentEmail() {
        SpringCMActionTest.setupCustomSettings();
        SpringCMAction.isTest = true; // set to true so web callout doesn't try to fire

        String newEmailAddress = generateRandomEmail();
        String userEmailAddress = generateRandomEmail();
        String userEmailAddress2 = generateRandomEmail();

        Account school1;
        Contact staff1, staff2;

        school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, 5, false);
        insert new List <Account> {school1};

        staff1 = TestUtils.createContact('Staff 1', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
        staff2 = TestUtils.createContact('Staff 2', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
        staff1.Email = userEmailAddress;
        staff2.Email = userEmailAddress2;
        insert new List <Contact> {staff1, staff2};

        User schoolPortalUser1, schoolPortalUser2;
        schoolPortalUser1 = TestUtils.createPortalUser(staff1.LastName, userEmailAddress, 'alias1', staff1.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);
        schoolPortalUser2 = TestUtils.createPortalUser(staff2.LastName, userEmailAddress2, 'alias2', staff2.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);
        List<User> schoolUsers = new List<User> { schoolPortalUser1, schoolPortalUser2 };

        String errMsg;
        System.runAs(thisUser) {
            insert schoolUsers;

            Test.startTest();
                try {
                    staff1.Email = newEmailAddress;
                    update staff1;
                } catch (Exception e) {
                    errMsg = e.getMessage();
                }
            Test.stopTest();
        }

        // your schoolPortalUser wasn't being attached to a Contact, because there was already a User with that Contact
        // in the system. I adjusted the assert statement to look at the Username for the TestData.PortalUser and this got
        // the test to work

        system.assertEquals(null, ContactAction.errorMessage);
        system.assertEquals(null, errMsg);

        system.assertEquals(newEmailAddress, [select Email from Contact where Id = :staff1.Id].Email);
        system.assertEquals(newEmailAddress, [select UserName from User where Id = :schoolPortalUser1.Id].UserName);
        system.assertEquals(newEmailAddress, [select Email from User where Id = :schoolPortalUser1.Id].Email);
    }

    @isTest
    private static void testUsernameSuffix() {
        SpringCMActionTest.setupCustomSettings();
        SpringCMAction.isTest = true; // set to true so web callout doesn't try to fire

        Portal_Username_Suffix_Setting__c cs;
        Account school1;
        Contact staff1, staff2;

        cs = new Portal_Username_Suffix_Setting__c(name='Username_Suffix', Value__c='.sandbox', Active__c=true);
        insert cs;

        school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, 5, false);
        insert new List <Account> {school1};

        staff1 = TestUtils.createContact('Staff 1', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
        staff1.Email = 'teststaff1@xyz.com';
        insert new List <Contact> {staff1};

        User schoolPortalUser1, schoolPortalUser2;
        schoolPortalUser1 = TestUtils.createPortalUser(staff1.LastName, 'teststaff1@xyz.com', 'alias1', staff1.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);

        String errMsg;
        System.runAs(thisUser){

            Test.startTest();

                insert new List <User> {schoolPortalUser1};

                try {
                    staff1.Email = 'teststaffabc1@xyz.com';

                    update staff1;
                } catch (exception e) {
                    errMsg = e.getMessage();
                }

            Test.stopTest();
        }

        system.assertEquals(null, ContactAction.errorMessage);
        system.assertEquals(null, errMsg);

        system.assertEquals('teststaffabc1@xyz.com', [select Email from Contact where Id = :staff1.Id].Email);
        system.assertEquals('teststaffabc1@xyz.com.sandbox', [select UserName from User where Id = :schoolPortalUser1.Id].UserName);
        system.assertEquals('teststaffabc1@xyz.com', [select Email from User where Id = :schoolPortalUser1.Id].Email);
    }

    @isTest
    private static void testDifferentEmailBulk() {
        SpringCMActionTest.setupCustomSettings();
        SpringCMAction.isTest = true; // set to true so web callout doesn't try to fire

        // username already exist. this will throw exception
        Integer batchSize = 10;
        Account school1;
        Contact staff1, staff2;
        List <Contact> staffList = new List <Contact>();
        List <User> schoolPortalUserList = new List <User>();

        school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, batchSize, false);
        insert new List <Account> {school1};

        for (Integer i=0; i<batchSize; i++) {
            Contact staff = TestUtils.createContact('Staff ' + i, school1.Id, RecordTypes.schoolStaffContactTypeId, false);
            staffList.add(staff);
        }
        insert staffList;

        for (Integer i=0; i<batchSize; i++) {
            User schoolPortalUser = TestUtils.createPortalUser(staffList[i].LastName, 'teststaff' + i + '@xyz.com', 'alias' + i, staffList[i].Id, GlobalVariables.schoolPortalAdminProfileId, true, false);
            schoolPortalUserList.add(schoolPortalUser);
        }

        Map <String, Integer> userId_Index_map = new Map <String, Integer>();

        String errMsg;
        System.runAs(thisUser){

            Test.startTest();

                insert schoolPortalUserList;

                for (Integer i=0; i<batchSize; i++) {
                    userId_Index_map.put(schoolPortalUserList[i].Id, i);
                }

                try {
                    for (Integer i=0; i<batchSize; i++) {
                        staffList[i].Email = 'teststaffabc' + i + '@xyz.com';
                    }

                    update staffList;
                } catch (Exception e) {
                    errMsg = e.getMessage();
                }

            Test.stopTest();
        }

        system.assertEquals(null, ContactAction.errorMessage);
        system.assertEquals(null, errMsg);

        for (User u : [select Username, Email from User where Id in :schoolPortalUserList]) {
            Integer i = userId_Index_map.get(u.Id);
            system.assertEquals('teststaffabc' + i + '@xyz.com', u.Username);
            system.assertEquals('teststaffabc' + i + '@xyz.com', u.Email);
        }
    }

    @isTest
    private static void testSameEmailBulk() {
        SpringCMActionTest.setupCustomSettings();
        SpringCMAction.isTest = true; // set to true so web callout doesn't try to fire

        Integer batchSize = 10;
        Account school1;
        Contact staff1, staff2;
        List <Contact> staffList = new List <Contact>();
        List <User> schoolPortalUserList = new List <User>();

        school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, batchSize, false);
        insert new List <Account> {school1};

        for (Integer i=0; i<batchSize; i++) {
            Contact staff = TestUtils.createContact('Staff ' + i, school1.Id, RecordTypes.schoolStaffContactTypeId, false);
            staffList.add(staff);
        }
        insert staffList;

        for (Integer i=0; i<batchSize; i++) {
            User schoolPortalUser = TestUtils.createPortalUser(staffList[i].LastName, 'teststaff' + i + '@xyz.com', 'alias' + i, staffList[i].Id, GlobalVariables.schoolPortalAdminProfileId, true, false);
            schoolPortalUserList.add(schoolPortalUser);
        }

        Map <String, Integer> userId_Index_map = new Map <String, Integer>();

        String errMsg;
        System.runAs(thisUser){

            Test.startTest();

                insert schoolPortalUserList;

                for (Integer i=0; i<batchSize; i++) {
                    userId_Index_map.put(schoolPortalUserList[i].Id, i);
                }
                try {
                    for (Integer i=0; i<batchSize; i++) {
                        // set email to same for all users
                        staffList[i].Email = 'teststaff1@xyz.com';
                    }

                    update staffList;
                } catch (Exception e) {
                    errMsg = e.getMessage();
                }

            Test.stopTest();
        }

        system.assertEquals(null, ContactAction.errorMessage);
        system.assertNotEquals(null, errMsg);

        // usernames not changed
        for (User u : [select Username, Email from User where Id in :schoolPortalUserList]) {
            Integer i = userId_Index_map.get(u.Id);
            system.assertEquals('teststaff' + i + '@xyz.com', u.Username);
            system.assertEquals('teststaff' + i + '@xyz.com', u.Email);
        }
    }

    @isTest
    private static void testDifferentEmailPartialBulk() {
        SpringCMActionTest.setupCustomSettings();
        SpringCMAction.isTest = true; // set to true so web callout doesn't try to fire

        Integer batchSize = 10;
        Account school1;
        Contact staff1, staff2;
        List <Contact> staffList = new List <Contact>();
        List <User> schoolPortalUserList = new List <User>();

        school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, batchSize, false);
        insert new List <Account> {school1};

        for (Integer i=0; i<batchSize; i++) {
            Contact staff = TestUtils.createContact('Staff ' + i, school1.Id, RecordTypes.schoolStaffContactTypeId, false);
            staffList.add(staff);
        }
        insert staffList;

        for (Integer i=0; i<batchSize; i++) {
            User schoolPortalUser = TestUtils.createPortalUser(staffList[i].LastName, 'teststaff' + i + '@xyz.com', 'alias' + i, staffList[i].Id, GlobalVariables.schoolPortalAdminProfileId, true, false);
            schoolPortalUserList.add(schoolPortalUser);
        }

        Map <String, Integer> userId_Index_map = new Map <String, Integer>();

        String errMsg;
        System.runAs(thisUser){
            Test.startTest();

                insert schoolPortalUserList;

                for (Integer i=0; i<batchSize; i++) {
                    userId_Index_map.put(schoolPortalUserList[i].Id, i);
                }

                try {
                    for (Integer i=0; i<batchSize/2; i++) {
                        // set email to same for some users
                        staffList[i].Email = 'teststaff1@xyz.com';
                    }
                    for (Integer i=batchSize/2; i<batchSize; i++) {
                        // set email to different for other users
                        staffList[i].Email = 'teststaffabc' + i + '@xyz.com';
                    }
                    update staffList;
                } catch (Exception e) {
                    errMsg = e.getMessage();
                }

            Test.stopTest();
        }

        system.assertEquals(null, ContactAction.errorMessage);

        // looks like salesforce does not allow duplicate emails for portal user.
        // FIELD_CUSTOM_VALIDATION_EXCEPTION, Duplicate email: teststaff1@xyz.com
        // and full contact batch update fails
        system.assertNotEquals(null, errMsg);

        for (User u : [select Username, Email from User where Id in :schoolPortalUserList]) {
            Integer i = userId_Index_map.get(u.Id);
            system.assertEquals('teststaff' + i + '@xyz.com', u.Username);
            system.assertEquals('teststaff' + i + '@xyz.com', u.Email);
        }
    }

    // Trigger is in contact
    // We will set parent A contact's email and verify if it is getting copied to other objects

    @isTest
    private static void testCopyParentAEmail() {
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);

        Account family = TestUtils.createAccount('Individual Account', RecordTypes.individualAccountTypeId, 5, false);
        Account school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, 5, false);
        school1.Alert_Frequency__c = 'Weekly digest of new applications';
        school1.SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest();
        school1.SSS_School_Code__c = '11111';
        school1.Portal_Version__c = SubscriptionTypes.BASIC_TYPE;
        insert new List<Account> { family, school1 };

        Contact staff1 = TestUtils.createContact('Staff 1', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
        staff1.Email = 'staff1@test.org';
        staff1.PFS_Alert_Preferences__c = 'Receive Alerts';

        parentA = TestUtils.createContact('Parent A', family.Id, RecordTypes.parentContactTypeId, false);

        Contact student1 = TestUtils.createContact('Student 1', family.Id, RecordTypes.studentContactTypeId, false);
        student1.Birthdate = Date.today().addDays(-200);

        insert new List<Contact> { staff1, parentA, student1 };

        User portalUser;

        System.runAs(thisUser){
            portalUser = TestUtils.createPortalUser(staff1.LastName, staff1.Email + String.valueOf(Math.random()) + String.valueOf(Math.random()), 'alias', staff1.Id, GlobalVariables.schoolPortalAdminProfileId, true, true);
        }

        TestUtils.insertAccountShare(staff1.AccountId, portalUser.Id);

        List<Academic_Year__c> academicYears = GlobalVariables.getAllAcademicYears();
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        String academicYearName = GlobalVariables.getCurrentAcademicYear().Name;

        createTables(academicYears);
        createSSSConstants(academicYearId);

        TestUtils.createUnusualConditions(academicYearId, true);

        PFS__c pfs1;
        School_PFS_Assignment__c spfsa1;

        System.runAs(portalUser){
            pfs1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
            pfs1.Original_Submission_Date__c = (Datetime)System.today().addDays(-3);
            pfs1.PFS_Status__c = 'Application Submitted';
            pfs1.Payment_Status__c = 'Unpaid';

            Dml.WithoutSharing.insertObjects(new List<PFS__c>{ pfs1 });

            // [CH] NAIS-1866 Updated to use the new version of createApplicant that respects the
            //        Contacts's name
            Applicant__c applicant1A = TestUtils.createApplicant(student1, pfs1.Id, false);
            Dml.WithoutSharing.insertObjects(new List<Applicant__c> { applicant1A });

            Student_Folder__c studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
            Dml.WithoutSharing.insertObjects(new List<Student_Folder__c> { studentFolder1});

            spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder1.Id, false);

            // put them in this order so that spfsa1 has earlier created date
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> { spfsa1 });
        }

        // Share records to school staff
        Set<Id> spaIds = new Set<Id> {spfsa1.Id};
        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);

        // required documents
        Required_Document__c rd1 = TestUtils.createRequiredDocument(academicYearId, school1.Id, false);
        insert new List <Required_Document__c> {rd1};

        // family documents
        Family_Document__c fd1 = TestUtils.createFamilyDocument('FamilyDoc1', academicYearId, false);
        insert new List <Family_Document__c> {fd1};

        // school document assignments
        School_Document_Assignment__c sda1 = TestUtils.createSchoolDocumentAssignment('sda1', fd1.Id, rd1.Id, spfsa1.Id, false);
        insert new List <School_Document_Assignment__c> {sda1};

        // application fee waivers
        Application_Fee_Waiver__c afw1 = TestUtils.createApplicationFeeWaiver(school1.Id, parentA.Id, academicYearId, false);
        insert new List <Application_Fee_Waiver__c> {afw1};

        Test.startTest();

        Opportunity oppty = TestUtils.createOpportunity('Oppty ', RecordTypes.pfsApplicationFeeOpportunityTypeId, pfs1.Id, academicYearName, false);
        oppty.StageName = 'Open';    // required field
        oppty.CloseDate = system.today();    // required field
        oppty.Parent_A__c = parentA.Id;
        insert oppty;

        parentA.Email = 'test@xyz.com';
        update parentA;

        system.assertEquals('test@xyz.com', [select Parent_A_Email__c from School_PFS_Assignment__c
            where Id = :spfsa1.Id].Parent_A_Email__c);
        system.assertEquals('test@xyz.com', [select Parent_A_Email__c from School_Document_Assignment__c
            where Id = :sda1.Id].Parent_A_Email__c);
        system.assertEquals('test@xyz.com', [select Parent_A_Email__c from Family_Document__c
            where Id = :fd1.Id].Parent_A_Email__c);
        system.assertEquals('test@xyz.com', [select Parent_A_Email__c from Application_Fee_Waiver__c
            where Id = :afw1.Id].Parent_A_Email__c);
        system.assertEquals('test@xyz.com', [select Parent_A_Email__c from Opportunity
            where Id = :oppty.Id].Parent_A_Email__c);

        Test.stopTest();
    }

    @isTest
    private static void testCopyParentAEmailBulk() {
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);

        Integer batchSize = 5;

        String academicYearId;
        String academicYearName;
        Account school1;
        Contact student1;
        Student_Folder__c studentFolder1;

        List <Contact> parentAList = new List <Contact>();
        List <PFS__c> pfsList = new List <PFS__c>();
        List <Applicant__c> applicantList = new List <Applicant__c>();
        List <School_PFS_Assignment__c> spfsaList = new List <School_PFS_Assignment__c>();
        List <Required_Document__c> rdList = new List <Required_Document__c>();
        List <Family_Document__c> fdList = new List <Family_Document__c>();
        List <School_Document_Assignment__c> sdaList = new List <School_Document_Assignment__c>();
        List <Application_Fee_Waiver__c> afwList = new List <Application_Fee_Waiver__c>();
        List <Opportunity> opptyList = new List <Opportunity>();
        List <Transaction_Line_Item__c> tliList = new List <Transaction_Line_Item__c>();

        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        academicYearName = GlobalVariables.getCurrentAcademicYear().Name;

        school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, null, false);
        insert school1;

        student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        insert student1;

        studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
        insert studentFolder1;

        for (Integer i=0; i<batchSize; i++) {
            parentA = TestUtils.createContact('Parent A' + i, null, RecordTypes.parentContactTypeId, false);
            parentAList.add(parentA);
        }
        insert parentAList;

        for (Integer i=0; i<batchSize; i++) {
            PFS__c pfs = TestUtils.createPFS('PFS A' + i, academicYearId, parentAList[i].Id, false);
            pfsList.add(pfs);
        }
        insert pfsList;

        for (Integer i=0; i<batchSize; i++) {
            Applicant__c applicant = TestUtils.createApplicant(student1.Id, pfsList[i].Id, false);
            applicantList.add(applicant);
        }
        insert applicantList;

        for (Integer i=0; i<batchSize; i++) {
            School_PFS_Assignment__c spfsa = TestUtils.createSchoolPFSAssignment(academicYearId, applicantList[i].Id, school1.Id, studentFolder1.Id, false);
            spfsaList.add(spfsa);
        }
        insert spfsaList;

        for (Integer i=0; i<batchSize; i++) {
            Required_Document__c rd = TestUtils.createRequiredDocument(academicYearId, school1.Id, false);
            rdList.add(rd);
        }
        insert rdList;

        for (Integer i=0; i<batchSize; i++) {
            Family_Document__c fd = TestUtils.createFamilyDocument('FamilyDoc' + i, academicYearId, false);
            fdList.add(fd);
        }
        insert fdList;

        for (Integer i=0; i<batchSize; i++) {
            School_Document_Assignment__c sda = TestUtils.createSchoolDocumentAssignment('sda' + i, fdList[i].Id, rdList[i].Id, spfsaList[i].Id, false);
            sdaList.add(sda);
        }
        insert sdaList;

        for (Integer i=0; i<batchSize; i++) {
            Application_Fee_Waiver__c afw = TestUtils.createApplicationFeeWaiver(school1.Id, parentAList[i].Id, academicYearId, false);
            afwList.add(afw);
        }
        insert afwList;

        Test.startTest();

        for (Integer i=0; i<batchSize; i++) {
            Opportunity oppty = TestUtils.createOpportunity('Oppty ' + i, RecordTypes.pfsApplicationFeeOpportunityTypeId, pfsList[i].Id, academicYearName, false);
            oppty.StageName = 'Open';    // required field
            oppty.CloseDate = system.today();    // required field
            opptyList.add(oppty);
        }
        insert opptyList;

        // Transaction Line Item
        Map<String, Schema.RecordTypeInfo> tliTypeMap = Transaction_Line_Item__c.SObjectType.getDescribe().getRecordTypeInfosByName();
        Id tliTypeId = tliTypeMap.get('PFS Fee Waiver').RecordTypeId;
        for (Integer i=0; i<batchSize; i++) {
            Transaction_Line_Item__c tli = TestUtils.createTransactionLineItem(opptyList[i].Id, tliTypeId, false);
            tliList.add(tli);
        }
        insert tliList;

        for (Integer i=0; i<batchSize; i++) {
            parentAList[i].Email = 'test' + i + '@xyz.com';    // avoid Duplicate email error by including i
        }
        update parentAList;

        // requery and verify Parent_A_Email__c
        system.assertEquals(batchSize, [select count() from School_PFS_Assignment__c
            where Id in :spfsaList and Parent_A_Email__c like 'test%@xyz.com']);
        system.assertEquals(batchSize, [select count() from School_Document_Assignment__c
            where Id in :sdaList and Parent_A_Email__c like 'test%@xyz.com']);
        system.assertEquals(batchSize, [select count() from Family_Document__c
            where Id in :fdList and Parent_A_Email__c like 'test%@xyz.com']);
        system.assertEquals(batchSize, [select count() from Application_Fee_Waiver__c
            where Id in :afwList and Parent_A_Email__c like 'test%@xyz.com']);
        system.assertEquals(batchSize, [select count() from Opportunity
            where Id in :opptyList and Parent_A_Email__c like 'test%@xyz.com']);
        system.assertEquals(batchSize, [select count() from Transaction_Line_Item__c
            where Id in :tliList and Parent_A_Email__c like 'test%@xyz.com']);

        Test.stopTest();
    }

    //SFP#19, [G.S]
    @isTest
    private static void testUpdateSSSMainContact() {
        Account account1 = TestUtils.createAccount('Account 1', RecordTypes.schoolAccountTypeId, 1, false);

        Database.insert(new List<Account>{account1});

        Contact contact1 = TestUtils.createContact('Test User1', account1.Id, RecordTypes.schoolStaffContactTypeId, false);
        contact1.SSS_Main_Contact__c = true;
        Contact contact2 = TestUtils.createContact('Test User2', account1.Id, RecordTypes.schoolStaffContactTypeId, false);
        Contact contact3 = TestUtils.createContact('Test User3', account1.Id, RecordTypes.schoolStaffContactTypeId, false);
        Database.insert(new List<Contact>{contact1, contact2, contact3});

        Integer mcCount = [select count() from contact where AccountId = :account1.Id and SSS_Main_Contact__c = true];
        System.assertEquals(1, mcCount);

        contact2.SSS_Main_Contact__c = true;
        update contact2;

        mcCount = [select count() from contact where AccountId = :account1.Id and SSS_Main_Contact__c = true];
        System.assertEquals(1, mcCount);

        Contact cntct = [select SSS_Main_Contact__c from contact where Id = :contact1.Id];
        System.assertEquals(false, cntct.SSS_Main_Contact__c);

        cntct = [select SSS_Main_Contact__c from contact where Id = :contact2.Id];
        System.assertEquals(true, cntct.SSS_Main_Contact__c);
    }

    @isTest
    private static void bucketAccountModelForNewFamilyPortalContacts()
    {        
        Test.startTest();
            student = new Contact(FirstName='Benson', LastName = 'Benny', RecordTypeId = recordTypes.studentContactTypeId);
            parentA = new Contact(FirstName='Benparent', LastName = 'Benny', RecordTypeId = recordTypes.parentContactTypeId);
            insert new List<Contact>{student, parentA};
        Test.stopTest();

        student = [Select AccountId from Contact where Id=:student.Id limit 1];
        parentA = [Select AccountId from Contact where Id=:parentA.Id limit 1];

        system.assertEquals('Benson Benny Account', [Select Name from Account where Id=:student.AccountId limit 1].Name);
        system.assertEquals('Benparent Benny Account', [Select Name from Account where Id=:parentA.AccountId limit 1].Name);

    }//End:bucketAccountModelForFamilyPortalContacts

    @isTest
    private static void deleteBucketAccountModelForFamilyPortalContacts()
    {
        Account studentAccount, parentAccount;
        student = new Contact(FirstName='Benson', LastName = 'Benny', RecordTypeId = recordTypes.studentContactTypeId);
        parentA = new Contact(FirstName='Benparent', LastName = 'Benny', RecordTypeId = recordTypes.parentContactTypeId);
        insert new List<Contact>{student, parentA};

        Test.startTest();
            student = [Select AccountId from Contact where Id=:student.Id limit 1];
            studentAccount = [Select Name from Account where Id=:student.AccountId limit 1];

            parentA = [Select AccountId from Contact where Id=:parentA.Id limit 1];
            parentAccount = [Select Name from Account where Id=:parentA.AccountId limit 1];

            system.assertEquals('Benson Benny Account', studentAccount.Name);
            system.assertEquals('Benparent Benny Account', parentAccount.Name);

            delete new List<Contact>{student, parentA};
        Test.stopTest();

        system.assertEquals(0, (new List<Account>([Select Id from Account where Id=:studentAccount.Id limit 1])).size());
        system.assertEquals(0, (new List<Account>([Select Id from Account where Id=:parentAccount.Id limit 1])).size());
        system.assertEquals(0, (new List<Contact>([Select Id from Contact where Id=:student.Id limit 1])).size());
        system.assertEquals(0, (new List<Contact>([Select Id from Contact where Id=:parentA.Id limit 1])).size());

    }//End:bucketAccountModelForFamilyPortalContacts

    private static void createTables(List<Academic_Year__c> academicYears)
    {
        List<Federal_Tax_Table__c> fedTaxTables = new List<Federal_Tax_Table__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            fedTaxTables.add(TestUtils.createFederalTaxTable(academicYears[i].Id, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_SINGLE, false));
        }
        Database.insert(fedTaxTables);

        List<State_Tax_Table__c> stateTaxTables = new List<State_Tax_Table__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            stateTaxTables.add(TestUtils.createStateTaxTable(academicYears[i].Id, false));
        }
        Database.insert(stateTaxTables);

        List<Employment_Allowance__c> emplAllowances = new List<Employment_Allowance__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            emplAllowances.add(TestUtils.createEmploymentAllowance(academicYears[i].Id, false));
        }
        Database.insert(emplAllowances);

        List<Net_Worth_of_Business_Farm__c> busFarm = new List<Net_Worth_of_Business_Farm__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            busFarm.add(TestUtils.createBusinessFarm(academicYears[i].Id, false));
        }
        Database.insert(busFarm);

        List<Retirement_Allowance__c> retAllowances = new List<Retirement_Allowance__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            retAllowances.add(TestUtils.createRetirementAllowance(academicYears[i].Id, false));
        }
        Database.insert(retAllowances);

        List<Asset_Progressivity__c> assetProgs = new List<Asset_Progressivity__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            assetProgs.add(TestUtils.createAssetProgressivity(academicYears[i].Id, false));
        }
        Database.insert(assetProgs);

        List<Income_Protection_Allowance__c> incProAllowances = new List<Income_Protection_Allowance__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            incProAllowances.add(TestUtils.createIncomeProtectionAllowance(academicYears[i].Id, false));
        }
        Database.insert(incProAllowances);

        List<Expected_Contribution_Rate__c> expContRates = new List<Expected_Contribution_Rate__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            expContRates.add(TestUtils.createExpectedContributionRate(academicYears[i].Id, false));
        }
        Database.insert(expContRates);

        List<Housing_Index_Multiplier__c> houseIndexes = new List<Housing_Index_Multiplier__c>{};
        for(Integer i=0; i<academicYears.size(); i++){
            houseIndexes.add(TestUtils.createHousingIndexMultiplier(academicYears[i].Id, false));
        }
        Database.insert(houseIndexes);
    }

    private static void createSSSConstants(Id academicYearId) {
        SSS_Constants__c sssConst1 = TestUtils.createSSSConstants(academicYearId, false);
        Database.insert(new List<SSS_Constants__c>{sssConst1});
    }

    private static String generateRandomEmail() {
        return 'test' + Math.random() + 'staff' + Math.random() + '@xyz.com';
    }
    
    @isTest
    private static void createBucketAccountForFamilyContact_emptyHouseholdForNewParent_contactHouseholdIsCreatedAlongWithTheBucketAccount()
    {
        // Reuse existing test.
        bucketAccountModelForNewFamilyPortalContacts();
        
        // This time we verify that the Contact.Household is set.
        student = [Select Id, Household__r.Name, FirstName, LastName from Contact where Id=:student.Id limit 1];
        parentA = [Select Id, Household__r.Name, FirstName, LastName from Contact where Id=:parentA.Id limit 1];
        system.assertEquals(null, student.Household__c);
        system.assertEquals(parentA.FirstName + ' ' + parentA.LastName + ' Household', parentA.Household__r.Name);
        
        //We also verify that there are not duplicated households for the same contact
        system.assertEquals(0, (new List<Household__c>([SELECT Id FROM Household__c WHERE Name =: student.FirstName + ' ' + student.LastName + ' Household'])).size());
        system.assertEquals(1, (new List<Household__c>([SELECT Id FROM Household__c WHERE Name =: parentA.FirstName + ' ' + parentA.LastName + ' Household'])).size());
    }//End:createBucketAccountForFamilyContact_emptyHouseholdForNewParent_contactHouseholdIsCreatedAlongWithTheBucketAccount

    @isTest
    private static void createBucketAccountForFamilyContact_newParentAndStudent_householdCreatedForParentOnly()
    {
        Contact parent = ContactTestData.Instance
            .forFirstName('Parent')
            .forLastName('Tester')
            .asParent()
            .create();
            
        Contact student = ContactTestData.Instance
            .forFirstName('Student')
            .forLastName('Tester')
            .asStudent()
            .create();
        
        parent.Household__c = null;
        student.Household__c = null;
        
        insert new List<Contact>{parent, student};
        
        Test.startTest();
        
            parent = [SELECT Id, Household__r.Name, Account.Name FROM Contact WHERE Id =: parent.Id LIMIT 1];
            student = [SELECT Id, Household__c, Account.Name FROM Contact WHERE Id =: student.Id LIMIT 1];
            
            system.assertNotEquals(null, parent.Household__c, 'Parent contacts should have its own household.');
            system.assertEquals('Parent Tester Household', parent.Household__r.Name, 'Parent contacts should have its own household.');
            system.assertEquals('Parent Tester Account', parent.Account.Name);
            
            system.assertEquals('Student Tester Account', student.Account.Name);
            system.assertEquals(null, student.Household__c, 'We do not want household records created for students.');
            
        Test.stopTest();
    }
}