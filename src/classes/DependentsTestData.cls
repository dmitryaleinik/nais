/**
 * @description This class is used to create Dependents records for unit tests.
 */
@isTest
public class DependentsTestData extends SObjectTestData {
    /**
     * @description Get the default values for the Dependents__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Dependents__c.PFS__c => PfsTestData.Instance.DefaultPfs.Id
        };
    }

    /**
     * @description Set the PFS__c field on the current Dependents record.
     * @param pfsId The Pfs Id to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forPfsId(Id pfsId) {
        return (DependentsTestData) with(Dependents__c.PFS__c, pfsId);
    }

    /**
     * @description Set the Dependent_1_School_Next_Year__c field on the current Dependents record.
     * @param dependent1SchoolNextYear The name of the School that dependent1 will attend next year to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent1SchoolNextYear(String dependent1SchoolNextYear) {
        return (DependentsTestData) with(Dependents__c.Dependent_1_School_Next_Year__c, dependent1SchoolNextYear);
    }

    /**
     * @description Set the Dependent_2_School_Next_Year__c field on the current Dependents record.
     * @param dependent2SchoolNextYear The name of the School that dependent2 will attend next year to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent2SchoolNextYear(String dependent2SchoolNextYear) {
        return (DependentsTestData) with(Dependents__c.Dependent_2_School_Next_Year__c, dependent2SchoolNextYear);
    }

    /**
     * @description Set the Dependent_1_Gender__c field on the current Dependents record.
     * @param dependent1Gender The sex of the dependent1 to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent1Gender(String dependent1Gender) {
        return (DependentsTestData) with(Dependents__c.Dependent_1_Gender__c, dependent1Gender);
    }

    /**
     * @description Set the Dependent_2_Gender__c field on the current Dependents record.
     * @param dependent2Gender The sex of the dependent2 to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent2Gender(String dependent2Gender) {
        return (DependentsTestData) with(Dependents__c.Dependent_2_Gender__c, dependent2Gender);
    }

    /**
     * @description Set the Dependent_3_Gender__c field on the current Dependents record.
     * @param dependent3Gender The sex of the dependent3 to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent3Gender(String dependent3Gender) {
        return (DependentsTestData) with(Dependents__c.Dependent_3_Gender__c, dependent3Gender);
    }

    /**
     * @description Sets default values for all the required fields for dependent 1.
     * @return The current instance.
     */
    public DependentsTestData defaultDependent1() {
        return forDependent1FullName('Default Dependent Name').forDependent1Birthdate(Date.today().addMonths(-1)).forDependent1Gender('Female').forDependent1CurrentGrade('1');
    }

    /**
     * @description Sets default values for all the required fields for dependent 2.
     * @return The current instance.
     */
    public DependentsTestData defaultDependent2() {
        return forDependent2FullName('Default Dependent Name').forDependent2Birthdate(Date.today().addMonths(-1)).forDependent2Gender('Female').forDependent2CurrentGrade('1');
    }

    /**
     * @description Sets default values for all the required fields for dependent 3.
     * @return The current instance.
     */
    public DependentsTestData defaultDependent3() {
        return forDependent3FullName('Default Dependent Name').forDependent3Birthdate(Date.today().addMonths(-1)).forDependent3Gender('Female').forDependent3CurrentGrade('1');
    }

    /**
     * @description Set the Dependent_1_Full_Name__c field on the current Dependents record.
     * @param dependent1FullName The full name of the dependent1 to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent1FullName(String dependent1FullName) {
        return (DependentsTestData) with(Dependents__c.Dependent_1_Full_Name__c, dependent1FullName);
    }

    /**
     * @description Set the Dependent_2_Full_Name__c field on the current Dependents record.
     * @param dependent2FullName The full name of the dependent2 to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent2FullName(String dependent2FullName) {
        return (DependentsTestData) with(Dependents__c.Dependent_2_Full_Name__c, dependent2FullName);
    }

    /**
     * @description Set the Dependent_3_Full_Name__c field on the current Dependents record.
     * @param fullName The full name of the dependent3 to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent3FullName(String fullName) {
        return (DependentsTestData) with(Dependents__c.Dependent_3_Full_Name__c, fullName);
    }

    /**
     * @description Set the Dependent_5_Full_Name__c field on the current Dependents record.
     * @param fullName The full name of the dependent5 to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent5FullName(String fullName) {
        return (DependentsTestData) with(Dependents__c.Dependent_5_Full_Name__c, fullName);
    }

    /**
     * @description Set the Dependent_1_Birthdate__c field on the current Dependents record.
     * @param dependent1Birthdate The Date of birth of the dependent1 to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent1Birthdate(Date dependent1Birthdate) {
        return (DependentsTestData) with(Dependents__c.Dependent_1_Birthdate__c, dependent1Birthdate);
    }

    /**
     * @description Set the Dependent_2_Birthdate__c field on the current Dependents record.
     * @param dependent2Birthdate The Date of birth of the dependent2 to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent2Birthdate(Date dependent2Birthdate) {
        return (DependentsTestData) with(Dependents__c.Dependent_2_Birthdate__c, dependent2Birthdate);
    }

    /**
     * @description Set the Dependent_3_Birthdate__c field on the current Dependents record.
     * @param dependent3Birthdate The Date of birth of the dependent3 to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent3Birthdate(Date dependent3Birthdate) {
        return (DependentsTestData) with(Dependents__c.Dependent_3_Birthdate__c, dependent3Birthdate);
    }

    /**
     * @description Set the Dependent_1_Current_School__c field on the current Dependents record.
     * @param dependent1CurrentSchool The current school of the dependent1 to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent1CurrentSchool(String dependent1CurrentSchool) {
        return (DependentsTestData) with(Dependents__c.Dependent_1_Current_School__c, dependent1CurrentSchool);
    }

    /**
     * @description Set the Dependent_2_Current_School__c field on the current Dependents record.
     * @param dependent2CurrentSchool The current school of the dependent2 to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent2CurrentSchool(String dependent2CurrentSchool) {
        return (DependentsTestData) with(Dependents__c.Dependent_2_Current_School__c, dependent2CurrentSchool);
    }

    /**
     * @description Set the Dependent_1_Current_Grade__c field on the current Dependents record.
     * @param dependent1CurrentGrade The current grade of the dependent1 to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent1CurrentGrade(String dependent1CurrentGrade) {
        return (DependentsTestData) with(Dependents__c.Dependent_1_Current_Grade__c, dependent1CurrentGrade);
    }

    /**
     * @description Set the Dependent_2_Current_Grade__c field on the current Dependents record.
     * @param dependent2CurrentGrade The current grade of the dependent2 to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent2CurrentGrade(String dependent2CurrentGrade) {
        return (DependentsTestData) with(Dependents__c.Dependent_2_Current_Grade__c, dependent2CurrentGrade);
    }

    /**
     * @description Set the Dependent_3_Current_Grade__c field on the current Dependents record.
     * @param dependent3CurrentGrade The current grade of the dependent3 to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent3CurrentGrade(String dependent3CurrentGrade) {
        return (DependentsTestData) with(Dependents__c.Dependent_3_Current_Grade__c, dependent3CurrentGrade);
    }

    /**
     * @description Set the Dependent_1_Parent_Sources_Est__c field on the current Dependents record.
     * @param d1psEst The Dependent 1 Parent Sources Est to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent1ParentSourcesEst(Integer d1psEst) {
        return (DependentsTestData) with(Dependents__c.Dependent_1_Parent_Sources_Est__c, d1psEst);
    }

    /**
     * @description Set the Dependent_2_Parent_Sources_Est__c field on the current Dependents record.
     * @param d2psEst The Dependent 2 Parent Sources Est to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent2ParentSourcesEst(Integer d2psEst) {
        return (DependentsTestData) with(Dependents__c.Dependent_2_Parent_Sources_Est__c, d2psEst);
    }

    /**
     * @description Set the Dependent_3_Parent_Sources_Est__c field on the current Dependents record.
     * @param d3psEst The Dependent 3 Parent Sources Est to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent3ParentSourcesEst(Integer d3psEst) {
        return (DependentsTestData) with(Dependents__c.Dependent_3_Parent_Sources_Est__c, d3psEst);
    }

    /**
     * @description Set the Dependent_4_Parent_Sources_Est__c field on the current Dependents record.
     * @param d4psEst The Dependent 4 Parent Sources Est to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent4ParentSourcesEst(Integer d4psEst) {
        return (DependentsTestData) with(Dependents__c.Dependent_4_Parent_Sources_Est__c, d4psEst);
    }

    /**
     * @description Set the Dependent_5_Parent_Sources_Est__c field on the current Dependents record.
     * @param d5psEst The Dependent 5 Parent Sources Est to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent5ParentSourcesEst(Integer d5psEst) {
        return (DependentsTestData) with(Dependents__c.Dependent_5_Parent_Sources_Est__c, d5psEst);
    }

    /**
     * @description Set the Dependent_6_Parent_Sources_Est__c field on the current Dependents record.
     * @param d6psEst The Dependent 6 Parent Sources Est to set on the Dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData forDependent6ParentSourcesEst(Integer d6psEst) {
        return (DependentsTestData) with(Dependents__c.Dependent_6_Parent_Sources_Est__c, d6psEst);
    }

    /**
     * @description Set the Number_of_Dependents__c field on the current Dependents record.
     * @param numberOfDependents The number of dependents.
     * @return The current working instance of DependentsTestData.
     */
    public DependentsTestData withNumberOfDependents(Integer numberOfDependents) {
        return (DependentsTestData) with(Dependents__c.Number_of_Dependents__c, numberOfDependents);
    }

    /**
     * @description Insert the current working Dependents__c record.
     * @return The currently operated upon Dependents__c record.
     */
    public Dependents__c insertDependents() {
        return (Dependents__c)insertRecord();
    }

    /**
     * @description Create the current working Dependents record without resetting
     *             the stored values in this instance of DependentsTestData.
     * @return A non-inserted Dependents__c record using the currently stored field
     *             values.
     */
    public Dependents__c createDependentsWithoutReset() {
        return (Dependents__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Dependents__c record.
     * @return The currently operated upon Dependents__c record.
     */
    public Dependents__c create() {
        return (Dependents__c)super.buildWithReset();
    }

    /**
     * @description The default Dependents__c record.
     */
    public Dependents__c DefaultDependents {
        get {
            if (DefaultDependents == null) {
                DefaultDependents = createDependentsWithoutReset();
                insert DefaultDependents;
            }
            return DefaultDependents;
        }
        private set;
    }

    /**
     * @description Get the Dependents__c SObjectType.
     * @return The Dependents__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Dependents__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static DependentsTestData Instance {
        get {
            if (Instance == null) {
                Instance = new DependentsTestData();
            }
            return Instance;
        }
        private set;
    }

    private DependentsTestData() { }
}