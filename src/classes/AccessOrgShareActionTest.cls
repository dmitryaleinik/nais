/*
 * Spec-111 School Portal - Access Orgs; Req# R-007
 *    Trigger on Affiliation, PFS, Applicant, Student Folder and User to share all PFS and Student Folder records of a parent with the users of each
 *    Access Org the parent has opted-in with (school pfs assignments). 
 *
 * WH, Exponent Partners, 2013
 */
@isTest
private class AccessOrgShareActionTest
{
    
    // [CH] NAIS-1766 Adding to help avoid Mixed DML Errors
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    
    // 3 users for 2 access orgs; 2 applicants, 1 with 2 PFS and the other with 1 PFS for 2 schools
    @isTest
    private static void recordsSharedToAccessOrgUsersOnSchoolPFSAssignmentInsert() {
        Account org1 = TestUtils.createAccount('Test Access Org 1', RecordTypes.accessOrgAccountTypeId, 3, false);
        Account org2 = TestUtils.createAccount('Test Access Org 2', RecordTypes.accessOrgAccountTypeId, 3, false);
        Account school1 = TestUtils.createAccount('Test School 1', RecordTypes.schoolAccountTypeId, 3, false);
        Account school2 = TestUtils.createAccount('Test School 2', RecordTypes.schoolAccountTypeId, 3, false);
        insert new List<Account> { org1, org2, school1, school2 };
        
        // [CH] NAIS-1766 Testing for Group based sharing
        List<String> groupNames = new List<String>{'X' + org1.Id, 'X' + org2.Id, 'X' + school1.Id, 'X' + school2.Id};
        Map<String, Group> groupsMap = new Map<String, Group>();
        for(Group groupRecord : [select Id, DeveloperName from Group where DeveloperName in :groupNames]){
            groupsMap.put(groupRecord.DeveloperName, groupRecord);
        }
        
        Contact c11 = TestUtils.createContact('Org 1 User 1', org1.Id, RecordTypes.schoolStaffContactTypeId, false);    // org1
        Contact c12 = TestUtils.createContact('Org 1 User 2', org1.Id, RecordTypes.schoolStaffContactTypeId, false);    // org1
        Contact c21 = TestUtils.createContact('Org 2 User 1', org2.Id, RecordTypes.schoolStaffContactTypeId, false);    // org2
        
        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        Contact parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
        
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        Contact student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
        
        insert new List<Contact> { c11, c12, c21, parentA, parentB, student1, student2 };
        
        // Workaround to avoid asynch shareRecordsToNewSchoolUsers() off UserAfter trigger obscuring results of batch sharing code
        Id portalProfileId = [select Id from Profile where Name = :ProfileSettings.PartnerProfileName].Id;
        
        User u11 = TestUtils.createPortalUser('Org 1 User 1', 'u11@test.org', 'u11', c11.Id, portalProfileId, true, false);
        User u12 = TestUtils.createPortalUser('Org 1 User 2', 'u12@test.org', 'u12', c12.Id, portalProfileId, true, false);
        User u21 = TestUtils.createPortalUser('Org 2 User 1', 'u21@test.org', 'u21', c21.Id, portalProfileId, true, false);
        
        System.runAs(thisUser){
            insert new List<User> { u11, u12, u21 };
        }
        
        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        
        TestUtils.createUnusualConditions(academicYearId, true);
        
        PFS__c pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
        PFS__c pfsB = TestUtils.createPFS('PFS B', academicYearId, parentB.Id, false);
        insert new List<PFS__c> { pfsA, pfsB };
        
        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, false);
        Applicant__c applicant1B = TestUtils.createApplicant(student1.Id, pfsB.Id, false);
        Applicant__c applicant2A = TestUtils.createApplicant(student2.Id, pfsA.Id, false);
        insert new List<Applicant__c> { applicant1A, applicant1B, applicant2A };
        
        Student_Folder__c studentFolder11 = TestUtils.createStudentFolder('Student Folder 1.1', academicYearId, student1.Id, false);
        Student_Folder__c studentFolder21 = TestUtils.createStudentFolder('Student Folder 2.1', academicYearId, student2.Id, false);
        Student_Folder__c studentFolder22 = TestUtils.createStudentFolder('Student Folder 2.2', academicYearId, student2.Id, false);
        insert new List<Student_Folder__c> { studentFolder11, studentFolder21, studentFolder22 };
        
        //School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder11.Id, false);
        //School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1B.Id, school1.Id, studentFolder11.Id, false);
        //School_PFS_Assignment__c spfsa3 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.Id, school1.Id, studentFolder21.Id, false);
        //School_PFS_Assignment__c spfsa4 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.Id, school2.Id, studentFolder22.Id, false);
        //insert new List<School_PFS_Assignment__c> { spfsa1, spfsa2, spfsa3, spfsa4 };
        
        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, org1.Id, studentFolder11.Id, false);
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1B.Id, org1.Id, studentFolder11.Id, false);
        School_PFS_Assignment__c spfsa3 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.Id, org1.Id, studentFolder21.Id, false);
        insert new List<School_PFS_Assignment__c> { spfsa1, spfsa2, spfsa3 };
        
        System.runAs(thisUser){
            Test.startTest();                
                // call scheduled batch Apex to process new access org sharing
                Database.executeBatch(new SchoolStaffShareBatch());
            Test.stopTest();
        }
        
        
        // parentA shares with org 1 through applicant1A & applicant2A - 
        //    {pfsA} shared to org1 user {u11, u12}                                                (1 x 2 = 2 PFS__Share)
        //    {studentFolder11, studentFolder21} shared to org1 user {u1, u2}                        (2 x 2 = 4 Student_Folder__Share)
        //    => {spfsa1, spfsa3, applicant1A, applicant2A}
        
        // parentB shares with org 1 through applicant1B - 
        //    {pfsB} shared to org1 user {u11, u12}                                                (1 x 2 = 2 PFS__Share)
        //    {studentFolder11} shared to org1 user {u1, u2}                                        (included above)
        //    => {spfsa2, applicant1B}
        
        assertPFSShares(new List<Id> {pfsA.Id, pfsB.Id}, new List<Id> {groupsMap.get('X' + org1.Id).Id}, 2);
        assertStudentFolderShares(new List<Id> {studentFolder11.Id, studentFolder21.Id}, new List<Id> {groupsMap.get('X' + org1.Id).Id}, 2);
        
        // u11 & u12 in org1 has no access to studentFolder22
        assertNoStudentFolderShares(new List<Id> {studentFolder22.Id}, new List<Id> {groupsMap.get('X' + org1.Id).Id});
        
        // u21 in org2 has no acccess to pfsA or pfsB
        assertNoPFSShares(new List<Id> {pfsA.Id, pfsB.Id}, new List<Id> {groupsMap.get('X' + org2.Id).Id});
        
        // u21 in org2 has no access to studentFolder11, studentFolder21, studentFolder22
        assertNoStudentFolderShares(new List<Id> {studentFolder11.Id, studentFolder21.Id, studentFolder22.Id}, new List<Id> {groupsMap.get('X' + org2.Id).Id});
    }
    
    /* [CH] NAIS-1766 This logic is already tested for group based sharing in the UserActionTest class
    // 1 new active and 1 inactive user for 1 access org; 2 applicants, 1 with 2 PFS and the other with 1 PFS for 1 school
    @isTest
    private static void recordsSharedToNewAccessOrgUserOnUserInsert() {
        Account org = TestUtils.createAccount('Test Access Org', RecordTypes.accessOrgAccountTypeId, 3, false);
        Account school = TestUtils.createAccount('Test School', RecordTypes.schoolAccountTypeId, 3, false);
        insert new List<Account> { org, school };
        
        Contact c1 = TestUtils.createContact('Org User 1', org.Id, RecordTypes.schoolStaffContactTypeId, false);
        Contact c2 = TestUtils.createContact('Org User 2', org.Id, RecordTypes.schoolStaffContactTypeId, false);
        
        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        Contact parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
        
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        Contact student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
        
        insert new List<Contact> { c1, c2, parentA, parentB, student1, student2 };
        
        User u1 = TestUtils.createPortalUser('Org User 1', 'ou1@test.org', 'ou1', c1.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);    // active user
        User u2 = TestUtils.createPortalUser('Org User 2', 'ou2@test.org', 'ou2', c2.Id, GlobalVariables.schoolPortalAdminProfileId, false, false);    // inactive user
                
        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        
        TestUtils.createUnusualConditions(academicYearId, true);
        
        PFS__c pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
        PFS__c pfsB = TestUtils.createPFS('PFS B', academicYearId, parentB.Id, false);
        insert new List<PFS__c> { pfsA, pfsB };
        
        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, false);
        Applicant__c applicant1B = TestUtils.createApplicant(student1.Id, pfsB.Id, false);
        Applicant__c applicant2A = TestUtils.createApplicant(student2.Id, pfsA.Id, false);
        insert new List<Applicant__c> { applicant1A, applicant1B, applicant2A };
        
        Student_Folder__c studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
        Student_Folder__c studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', academicYearId, student2.Id, false);
        insert new List<Student_Folder__c> { studentFolder1, studentFolder2 };
        
        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, org.Id, studentFolder1.Id, false);
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1B.Id, org.Id, studentFolder1.Id, false);
        School_PFS_Assignment__c spfsa3 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.Id, org.Id, studentFolder2.Id, false);
        insert new List<School_PFS_Assignment__c> { spfsa1, spfsa2, spfsa3 };
        
        Test.startTest();
        insert new List<User> { u1, u2 };
        Test.stopTest();
                
        // parentA with 2 students - 
        //    {pfsA} shared to new org staff {u1}                                            (1 x 1 = 1 PFS__Share)
        //    {studentFolder1, studentFolder2} shared to new org staff {u1}                (2 x 1 = 2 Student_Folder__Share)
        //    => {spfsa1, spfsa3, applicant1A, applicant2A}
        
        // parentB with 1 student -
        //    {pfsB} shared to new org staff {u1}                                            (1 x 1 = 1 PFS__Share)
        //    {studentFolder1} shared to new org staff {u1}                                (included above)
        //    => {spfsa2, applicant1B}
        
        assertPFSShares(new List<Id> {pfsA.Id, pfsB.Id}, new List<Id> {u1.Id}, 2);
        assertStudentFolderShares(new List<Id> {studentFolder1.Id, studentFolder2.Id}, new List<Id> {u1.Id}, 2);
        
        // inactive u2 in org has no acccess to pfs and studentFounders
        assertNoPFSShares(new List<Id> {pfsA.Id, pfsB.Id}, new List<Id> {u2.Id});
        assertNoStudentFolderShares(new List<Id> {studentFolder1.Id, studentFolder2.Id}, new List<Id> {u2.Id});
    }
    
    /* [CH] NAIS-1766 This logic is already tested for group based sharing in the UserActionTest class
    // 1 reactivated user for 1 access org; 2 applicants, 1 with 2 PFS and the other with 1 PFS for 1 school
    @isTest
    private static void recordsSharedToNewAccessOrgUserOnUserUpdate() {
        Account org = TestUtils.createAccount('Test Access Org', RecordTypes.accessOrgAccountTypeId, 3, false);
        Account school = TestUtils.createAccount('Test School', RecordTypes.schoolAccountTypeId, 3, false);
        insert new List<Account> { org, school };
        
        Contact c1 = TestUtils.createContact('Org User 1', org.Id, RecordTypes.schoolStaffContactTypeId, false);
        Contact c2 = TestUtils.createContact('Org User 2', org.Id, RecordTypes.schoolStaffContactTypeId, false);
        
        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        Contact parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
        
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        Contact student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
        
        insert new List<Contact> { c1, c2, parentA, parentB, student1, student2 };
        
        User u1 = TestUtils.createPortalUser('Org User 1', 'ou1@test.org', 'ou1', c1.Id, GlobalVariables.schoolPortalAdminProfileId, false, false);        // inactive user
        User u2 = TestUtils.createPortalUser('Org User 2', 'ou2@test.org', 'ou2', c2.Id, GlobalVariables.schoolPortalAdminProfileId, false, false);        // inactive user
        insert new List<User> { u1, u2 };
                
        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        
        TestUtils.createUnusualConditions(academicYearId, true);
        
        PFS__c pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
        PFS__c pfsB = TestUtils.createPFS('PFS B', academicYearId, parentB.Id, false);
        insert new List<PFS__c> { pfsA, pfsB };
        
        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, false);
        Applicant__c applicant1B = TestUtils.createApplicant(student1.Id, pfsB.Id, false);
        Applicant__c applicant2A = TestUtils.createApplicant(student2.Id, pfsA.Id, false);
        insert new List<Applicant__c> { applicant1A, applicant1B, applicant2A };
        
        Student_Folder__c studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
        Student_Folder__c studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', academicYearId, student2.Id, false);
        insert new List<Student_Folder__c> { studentFolder1, studentFolder2 };
        
        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, org.Id, studentFolder1.Id, false);
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1B.Id, org.Id, studentFolder1.Id, false);
        School_PFS_Assignment__c spfsa3 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.Id, org.Id, studentFolder2.Id, false);
        insert new List<School_PFS_Assignment__c> { spfsa1, spfsa2, spfsa3 };
        
        User sysAdminUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs(sysAdminUser) {    // work around mixed dml error when run in developer console
            Test.startTest();
            u2.IsActive = true;
            update u2;
            Test.stopTest();
        }
                
        // parentA with 2 students - 
        //    {pfsA} shared to activated org staff {u2}                                    (1 x 1 = 1 PFS__Share)
        //    {studentFolder1, studentFolder2} shared to activated org staff {u2}            (2 x 1 = 2 Student_Folder__Share)
        //    => {spfsa1, spfsa3, applicant1A, applicant2A}
        
        // parentB with 1 student -
        //    {pfsB} shared to activated org staff {u2}                                    (1 x 1 = 1 PFS__Share)
        //    {studentFolder1} shared to activated org staff {u2}                            (included above)
        //    => {spfsa2, applicant1B}
        
        assertPFSShares(new List<Id> {pfsA.Id, pfsB.Id}, new List<Id> {u2.Id}, 2);
        assertStudentFolderShares(new List<Id> {studentFolder1.Id, studentFolder2.Id}, new List<Id> {u2.Id}, 2);
        
        // inactive u1 in org has no acccess to pfs and studentFounders
        assertNoPFSShares(new List<Id> {pfsA.Id, pfsB.Id}, new List<Id> {u1.Id});
        assertNoStudentFolderShares(new List<Id> {studentFolder1.Id, studentFolder2.Id}, new List<Id> {u1.Id});
    }
    */
    
    // 1 user for 1 access org; 1 parent with 2 applicants, another with a new pfs inserted
    @isTest
    private static void newPFSNotSharedToAccessOrgUserWithoutSchoolPFSAssignment() {
        Account org = TestUtils.createAccount('Test Access Org', RecordTypes.accessOrgAccountTypeId, 3, false);
        Account school = TestUtils.createAccount('Test School', RecordTypes.schoolAccountTypeId, 3, false);
        insert new List<Account> { org, school };
        
        // [CH] NAIS-1766 For testing group based sharing
        String groupName = 'X' + org.Id;
        List<Group> groups = [select Id, DeveloperName from Group where DeveloperName = :groupName];
        Id groupId = groups[0].Id;
        
        Contact c = TestUtils.createContact('Org User', org.Id, RecordTypes.schoolStaffContactTypeId, false);
        
        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        Contact parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
        
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        Contact student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
        
        insert new List<Contact> { c, parentA, parentB, student1, student2 };
        
        // Workaround to avoid asynch shareRecordsToNewSchoolUsers() off UserAfter trigger obscuring results of batch sharing code
        Id portalProfileId = [select Id from Profile where Name = :ProfileSettings.PartnerProfileName].Id;
        
        System.runAs(thisUser){
            User u = TestUtils.createPortalUser('Org User', 'ou@test.org', 'ou', c.Id, portalProfileId, true, true);
        }
                
        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        
        TestUtils.createUnusualConditions(academicYearId, true);
        
        PFS__c pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, true);            // inserted
        PFS__c pfsB = TestUtils.createPFS('PFS B', academicYearId, parentB.Id, false);            // not inserted
        
        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, false);
        Applicant__c applicant2A = TestUtils.createApplicant(student2.Id, pfsA.Id, false);
        insert new List<Applicant__c> { applicant1A, applicant2A };
        
        Student_Folder__c studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
        Student_Folder__c studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', academicYearId, student2.Id, false);
        insert new List<Student_Folder__c> { studentFolder1, studentFolder2 };
        
        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, org.Id, studentFolder1.Id, false);
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.Id, org.Id, studentFolder2.Id, false);
        insert new List<School_PFS_Assignment__c> { spfsa1, spfsa2 };
        
        System.runAs(thisUser){
            Test.startTest();
            
            insert pfsB;
            // call scheduled batch Apex to process new PFS's
            Database.executeBatch(new SchoolStaffShareBatch());
            Test.stopTest();
        }
                
        // parentA with 2 students - 
        //    {pfsA} shared to org staff {u}                                                (1 x 1 = 1 PFS__Share)
        //    {studentFolder1, studentFolder2} shared to org staff {u}                    (2 x 1 = 2 Student_Folder__Share)
        //    => {applicant1A, applicant2A}
        
        // parentB with no student -
        //    {pfsB} not shared to org staff {u}    w/o school PFS assignment
        
        assertPFSShares(new List<Id> {pfsA.Id}, new List<Id> {groupId}, 1);
        assertStudentFolderShares(new List<Id> {studentFolder1.Id, studentFolder2.Id}, new List<Id> {groupId}, 2);
        
        assertNoPFSShares(new List<Id> {pfsB.Id}, new List<Id> {groupId});
    }
    
    // 1 user for 1 access org; 1 parent with 1 applicant, another with a new applicant inserted
    @isTest
    private static void newApplicantNotSharedToAccessOrgUserWithoutSchoolPFSAssignment() {
        Account org = TestUtils.createAccount('Test Access Org', RecordTypes.accessOrgAccountTypeId, 3, false);
        // Account school = TestUtils.createAccount('Test School', RecordTypes.schoolAccountTypeId, 3, false);
        // insert new List<Account> { org, school };
        insert new List<Account> { org };
        
        // [CH] NAIS-1766 For testing group based sharing
        String groupName = 'X' + org.Id;
        List<Group> groups = [select Id, DeveloperName from Group where DeveloperName = :groupName];
        Id groupId = groups[0].Id;
        
        Contact c = TestUtils.createContact('Org User', org.Id, RecordTypes.schoolStaffContactTypeId, false);
        
        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        Contact parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
        
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        Contact student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
        
        insert new List<Contact> { c, parentA, parentB, student1, student2 };
        
        // Workaround to avoid asynch shareRecordsToNewSchoolUsers() off UserAfter trigger obscuring results of batch sharing code
        Id portalProfileId = [select Id from Profile where Name = :ProfileSettings.PartnerProfileName].Id;
        
        System.runAs(thisUser){
            User u = TestUtils.createPortalUser('Org User', 'ou@test.org', 'ou', c.Id, portalProfileId, true, true);
        }
                
        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        
        TestUtils.createUnusualConditions(academicYearId, true);
        
        PFS__c pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
        PFS__c pfsB = TestUtils.createPFS('PFS B', academicYearId, parentB.Id, false);
        insert new List<PFS__c> { pfsA, pfsB };
        
        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, true);        // inserted
        Applicant__c applicant2B = TestUtils.createApplicant(student2.Id, pfsB.Id, false);        // not inserted
        
        Student_Folder__c studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
        Student_Folder__c studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', academicYearId, student2.Id, false);
        insert new List<Student_Folder__c> { studentFolder1, studentFolder2 };
        
        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, org.Id, studentFolder1.Id, true);
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2B.Id, org.Id, studentFolder2.Id, false);
        
        System.runAs(thisUser){
        Test.startTest();
        insert applicant2B;
        // call scheduled batch Apex to process new applicants
        Database.executeBatch(new SchoolStaffShareBatch());
        Test.stopTest();
        }
                
        // parentA with student1 - 
        //    {pfsA} shared to org staff {u}                                                (1 x 1 = 1 PFS__Share)
        //    {studentFolder1} shared to org staff {u}                                    (1 x 1 = 1 Student_Folder__Share)
        //    => {applicant1A}
        
        // parentB with student2 -
        //    {pfsB} not shared to org staff {u} w/o school PFS assignment
        //    {studentFolder2} not shared to org staff {u} w/o school PFS assignment
        //    => {}
        
        assertPFSShares(new List<Id> {pfsA.Id}, new List<Id> {groupId}, 1);
        assertStudentFolderShares(new List<Id> {studentFolder1.Id}, new List<Id> {groupId}, 1);
        
        assertNoPFSShares(new List<Id> {pfsB.Id}, new List<Id> {groupId});
        assertNoStudentFolderShares(new List<Id> {studentFolder2.Id}, new List<Id> {groupId});
    }
    
    // 1 user for 1 access org; 2 parents with 1 applicant each; 1 applicant has a new folder inserted
    @isTest
    private static void newStudentFolderNotSharedToAccessOrgUserWithoutSchoolPFSAssignment() {
        Account org = TestUtils.createAccount('Test Access Org', RecordTypes.accessOrgAccountTypeId, 3, false);
        Account school = TestUtils.createAccount('Test School', RecordTypes.schoolAccountTypeId, 3, false);
        insert new List<Account> { org, school };
        
        // [CH] NAIS-1766 For testing group based sharing
        String groupName = 'X' + org.Id;
        List<Group> groups = [select Id, DeveloperName from Group where DeveloperName = :groupName];
        Id groupId = groups[0].Id;
        
        Contact c = TestUtils.createContact('Org User', org.Id, RecordTypes.schoolStaffContactTypeId, false);
        
        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, false);
        Contact parentB = TestUtils.createContact('Parent B', null, RecordTypes.parentContactTypeId, false);
        
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, false);
        Contact student2 = TestUtils.createContact('Student 2', null, RecordTypes.studentContactTypeId, false);
        
        insert new List<Contact> { c, parentA, parentB, student1, student2 };
        
        // Workaround to avoid asynch shareRecordsToNewSchoolUsers() off UserAfter trigger obscuring results of batch sharing code
        Id portalProfileId = [select Id from Profile where Name = :ProfileSettings.PartnerProfileName].Id;
        
        System.runAs(thisUser){
            User u = TestUtils.createPortalUser('Org User', 'ou@test.org', 'ou', c.Id, portalProfileId, true, true);
        }
                
        TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        
        TestUtils.createUnusualConditions(academicYearId, true);
        
        PFS__c pfsA = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
        PFS__c pfsB = TestUtils.createPFS('PFS B', academicYearId, parentB.Id, false);
        insert new List<PFS__c> { pfsA, pfsB };
        
        Applicant__c applicant1A = TestUtils.createApplicant(student1.Id, pfsA.Id, false);
        Applicant__c applicant2B = TestUtils.createApplicant(student2.Id, pfsB.Id, false);
        insert new List<Applicant__c> { applicant1A, applicant2B };
        
        Student_Folder__c studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, true);        // inserted
        Student_Folder__c studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', academicYearId, student2.Id, false);        // not inserted
        
        School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, org.Id, studentFolder1.Id, true);
        School_PFS_Assignment__c spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2B.Id, org.Id, studentFolder2.Id, false);
        
        System.runAs(thisUser){
            Test.startTest();
            insert studentFolder2;
            // call scheduled batch Apex to process new applicants and student folders
            Database.executeBatch(new SchoolStaffShareBatch());
            Test.stopTest();
        }
                
        // parentA with student1 - 
        //    {pfsA} shared to org staff {u}                                                (1 x 1 = 1 PFS__Share)
        //    {studentFolder1} shared to org staff {u}                                    (1 x 1 = 1 Student_Folder__Share)
        //    => {applicant1A}
        
        // parentB with student2 -
        //    {pfsB} not shared to org staff {u} w/o school PFS assignment
        //    {studentFolder2} not shared to org staff {u} w/o school PFS assignment
        //    => {}
        
        assertPFSShares(new List<Id> {pfsA.Id}, new List<Id> {groupId}, 1);
        assertStudentFolderShares(new List<Id> {studentFolder1.Id}, new List<Id> {groupId}, 1);
        
        assertNoPFSShares(new List<Id> {pfsB.Id}, new List<Id> {groupId});
        assertNoStudentFolderShares(new List<Id> {studentFolder2.Id}, new List<Id> {groupId});
    }
    
    /*
     * Utility methods
     */
    
    // assert that there are manual PFS__Share for all these combinations of PFSs and users, and total manual share count
    private static void assertPFSShares(List<Id> pfsIds, List<Id> userIds, Integer totalCount) {
        List<PFS__Share> shares = [select ParentId, UserOrGroupId, AccessLevel 
                    from PFS__Share where ParentId in :pfsIds and UserOrGroupId in :userIds and RowCause = 'Manual'];
        Integer allShareCount = [select count() from PFS__Share where RowCause = 'Manual'];
        
        System.assertEquals(pfsIds.size() * userIds.size(), shares.size());
        System.assertEquals(totalCount, allShareCount);
        for (PFS__Share ps : shares) {
            System.assertEquals('Edit', ps.AccessLevel);
        }
    }
    
    // assert that there are manual Student_Folder__Share for all these combinations of student folders and users, and total manual share count
    private static void assertStudentFolderShares(List<Id> studentFolderIds, List<Id> userIds, Integer totalCount) {
        List<Student_Folder__Share> shares = [select ParentId, UserOrGroupId, AccessLevel 
                    from Student_Folder__Share where ParentId in :studentFolderIds and UserOrGroupId in :userIds and RowCause = 'Manual'];
        Integer allShareCount = [select count() from Student_Folder__Share where RowCause = 'Manual'];
        
        System.assertEquals(studentFolderIds.size() * userIds.size(), shares.size());
        System.assertEquals(totalCount, allShareCount);
        for (Student_Folder__Share sfs : shares) {
            System.assertEquals('Edit', sfs.AccessLevel);
        }
    }
    
    // assert that there is no manual PFS__Share for all these combinations of PFSs and users
    private static void assertNoPFSShares(List<Id> pfsIds, List<Id> userIds) {
        System.assertEquals(0, 
            [select count() from PFS__Share where ParentId in :pfsIds and UserOrGroupId in :userIds and RowCause = 'Manual']);
    }
    
    // assert that there is no manual Student_Folder__Share for all these combinations of student folders and users
    private static void assertNoStudentFolderShares(List<Id> studentFolderIds, List<Id> userIds) {
        System.assertEquals(0, 
            [select count() from Student_Folder__Share where ParentId in :studentFolderIds and UserOrGroupId in :userIds and RowCause = 'Manual']);
    }
}