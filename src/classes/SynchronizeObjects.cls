/*
* NAIS-243
*
* Info on the parent new/edit page should pull from the PFS record, not the contact record, 
* for both parent A and parent B. 
*
* Use the Parent A and Parent B fields on the PFS object for all info on the page 
* (name, address, birthdate, gender, etc). All fields should be created in config on the PFS object; 
*
* When updated on this page, the corresponding field on the contact record should also be updated.
*
* Nathan, Exponent Partners, 2013
*/
public class SynchronizeObjects {

    // input
    public Map <Id, PFS__c> pfsOldMap;
    public Map <Id, PFS__c> pfsNewMap;
    

    // to prevent trigger recursive call
    public static Boolean isSyncPFSParentsToContactExecuting = false;
         
    // constructor
    public SynchronizeObjects() {
    
    }    

    public void SyncPFSParentsToContact() {
    
        // start execution
        isSyncPFSParentsToContactExecuting = true;
        
        // init vars
        Set <String> parentIdSet = new Set <String>();
        Map <String, Contact> id_contact_map = new Map <String, Contact>();
        List <Contact> contactUpdateList = new List <Contact>();
        Set <String> contactUpdateIdSet = new Set <String>();    // to avoid dupes in update list
        
        if (pfsOldMap == null || pfsNewMap == null) {
            //isSyncPFSParentsToContactExecuting = false; COMMENT OUT TO PREVENT TESTDATA SOQL LIMIT [dp] 8.21.13
            return;
        }   

        // get parent ids
        if (pfsNewMap != null) {
            for (PFS__c pfs : pfsNewMap.values()) {
                parentIdSet.add(pfs.Parent_A__c);
                parentIdSet.add(pfs.Parent_B__c);
            }
        }
        parentIdSet.remove(null);
        
        if (parentIdSet.size() == 0) {
            //isSyncPFSParentsToContactExecuting = false; COMMENT OUT TO PREVENT TESTDATA SOQL LIMIT [dp] 8.21.13
            return;
        }
        
        // get parent records
        List <Contact> conList = [select Id, Name,
            Birthdate,
            Current_Age__c,
            Email,
            FirstName,
            Gender__c,
            HomePhone,
            LastName,
            MailingCity,
            MailingCountry,
            MailingCountryCode,
            MailingPostalCode,
            MailingState,
            MailingStateCode,
            MailingStreet,
            Middle_Name__c,
            MobilePhone,
            Phone,
            Preferred_Phone__c,
            RecordTypeId,
            Salutation,
            SSN__c,
            Suffix__c        
        from Contact
        where Id in :parentIdSet];
        
        for (Contact con : conList) {
            id_contact_map.put(con.Id, con);
        }
        
        // copy fields from PFS to contact
        for (PFS__c pfs : pfsNewMap.values()) {
            PFS__c oldPFS;
            if (pfsOldMap.containsKey(pfs.Id)) {
                oldPFS = pfsOldMap.get(pfs.Id);
            }
            if (oldPFS == null) {
                continue;
            }
            // parent A
            if (pfs.Parent_A__c != null && id_contact_map.containsKey(pfs.Parent_A__c)) {
                
                Contact con = id_contact_map.get(pfs.Parent_A__c);
                Boolean updateContact = false;
                
                // NAIS-397
                // Copy only changed fields
                if (pfs.Parent_A_Address__c != oldPFS.Parent_A_Address__c && con.MailingStreet != pfs.Parent_A_Address__c) {
                    con.MailingStreet = pfs.Parent_A_Address__c;
                    updateContact = true;
                }
                if (pfs.Parent_A_Birthdate__c != oldPFS.Parent_A_Birthdate__c && con.Birthdate != pfs.Parent_A_Birthdate__c) {
                    con.Birthdate = pfs.Parent_A_Birthdate__c;
                    updateContact = true;
                }
                if (pfs.Parent_A_City__c != oldPFS.Parent_A_City__c && con.MailingCity != pfs.Parent_A_City__c) {
                    con.MailingCity = pfs.Parent_A_City__c;
                    updateContact = true;
                }
                
                /* 
                // you have to set first country and then state. they are dependent picklist
                // if the dependency is not valid then it will throw error
                // for example country United States and state British Colombia will throw error
                if (countryLabelValueMap != null && countryLabelValueMap.containsKey(pfs.Parent_A_Country__c)) {
                    con.MailingCountryCode = countryLabelValueMap.get(pfs.Parent_A_Country__c);
                    if (stateLabelValueMap != null && stateLabelValueMap.containsKey(pfs.Parent_A_State__c)) {
                        con.MailingStateCode = stateLabelValueMap.get(pfs.Parent_A_State__c);
                    }
                }
                */
                
                // country and state picklists on PFS updated to synchronize with new country and state picklist values on Contact
                if (pfs.Parent_A_Country__c != oldPFS.Parent_A_Country__c && con.MailingCountry != pfs.Parent_A_Country__c) {
                    con.MailingCountry = pfs.Parent_A_Country__c;
                    updateContact = true;
                }
                if (pfs.Parent_A_State__c != oldPFS.Parent_A_State__c && con.MailingState != pfs.Parent_A_State__c) {
                    con.MailingState = pfs.Parent_A_State__c;
                    updateContact = true;
                }
                if (pfs.Parent_A_Email__c != oldPFS.Parent_A_Email__c && con.Email != pfs.Parent_A_Email__c) {
                    con.Email = pfs.Parent_A_Email__c;
                    updateContact = true;
                }
                if (pfs.Parent_A_First_Name__c != oldPFS.Parent_A_First_Name__c && con.FirstName != pfs.Parent_A_First_Name__c) {
                    con.FirstName = pfs.Parent_A_First_Name__c;
                    updateContact = true;
                }
                if (pfs.Parent_A_Gender__c != oldPFS.Parent_A_Gender__c && con.Gender__c != pfs.Parent_A_Gender__c) {
                    con.Gender__c = pfs.Parent_A_Gender__c;
                    updateContact = true;
                }
                if (pfs.Parent_A_Home_Phone__c != oldPFS.Parent_A_Home_Phone__c && con.HomePhone != pfs.Parent_A_Home_Phone__c) {
                    con.HomePhone = pfs.Parent_A_Home_Phone__c;
                    updateContact = true;
                }
                if (pfs.Parent_A_Last_Name__c != oldPFS.Parent_A_Last_Name__c && pfs.Parent_A_Last_Name__c != null &&
                    con.LastName != pfs.Parent_A_Last_Name__c) {
                    con.LastName = pfs.Parent_A_Last_Name__c;
                    updateContact = true;
                }
                if (pfs.Parent_A_Middle_Name__c != oldPFS.Parent_A_Middle_Name__c && con.Middle_Name__c != pfs.Parent_A_Middle_Name__c) {
                    con.Middle_Name__c = pfs.Parent_A_Middle_Name__c;
                    updateContact = true;
                }
                if (pfs.Parent_A_Mobile_Phone__c != oldPFS.Parent_A_Mobile_Phone__c && con.MobilePhone != pfs.Parent_A_Mobile_Phone__c) {
                    con.MobilePhone = pfs.Parent_A_Mobile_Phone__c;
                    updateContact = true;
                }
                if (pfs.Parent_A_Preferred_Phone__c != oldPFS.Parent_A_Preferred_Phone__c && con.Preferred_Phone__c != pfs.Parent_A_Preferred_Phone__c) {
                    con.Preferred_Phone__c = pfs.Parent_A_Preferred_Phone__c;
                    updateContact = true;
                }
                if (pfs.Parent_A_Suffix__c != oldPFS.Parent_A_Suffix__c && con.Suffix__c != pfs.Parent_A_Suffix__c) {
                    con.Suffix__c = pfs.Parent_A_Suffix__c;
                    updateContact = true;
                }
                
                if (pfs.Parent_A_Prefix__c != oldPFS.Parent_A_Prefix__c && con.Salutation != pfs.Parent_A_Prefix__c) {
                    con.Salutation = pfs.Parent_A_Prefix__c;
                    updateContact = true;
                }
                
                if (pfs.Parent_A_Work_Phone__c != oldPFS.Parent_A_Work_Phone__c && con.Phone != pfs.Parent_A_Work_Phone__c) {
                    con.Phone = pfs.Parent_A_Work_Phone__c;
                    updateContact = true;
                }
                if (pfs.Parent_A_ZIP_Postal_Code__c != oldPFS.Parent_A_ZIP_Postal_Code__c && con.MailingPostalCode != pfs.Parent_A_ZIP_Postal_Code__c) {
                    con.MailingPostalCode = pfs.Parent_A_ZIP_Postal_Code__c;
                    updateContact = true;
                }
        
                if (updateContact == true && contactUpdateIdSet.add(con.Id)) {
                    contactUpdateList.add(con);
                }
            }
            
            // parent B
            if (pfs.Parent_B__c != null && id_contact_map.containsKey(pfs.Parent_B__c)) {
                
                Contact con = id_contact_map.get(pfs.Parent_B__c);
                Boolean updateContact = false;
                
                // NAIS-397
                // Copy only changed fields
                if (pfs.Parent_B_Address__c != oldPFS.Parent_B_Address__c && con.MailingStreet != pfs.Parent_B_Address__c) {
                    con.MailingStreet = pfs.Parent_B_Address__c;
                    updateContact = true;
                }
                if (pfs.Parent_B_Birthdate__c != oldPFS.Parent_B_Birthdate__c && con.Birthdate != pfs.Parent_B_Birthdate__c) {
                    con.Birthdate = pfs.Parent_B_Birthdate__c;
                    updateContact = true;
                }
                if (pfs.Parent_B_City__c != oldPFS.Parent_B_City__c && con.MailingCity != pfs.Parent_B_City__c) {
                    con.MailingCity = pfs.Parent_B_City__c;
                    updateContact = true;
                }
                
                /* 
                // you have to set first country and then state. they are dependent picklist
                // if the dependency is not valid then it will throw error
                // for example country United States and state British Colombia will throw error
                if (countryLabelValueMap != null && countryLabelValueMap.containsKey(pfs.Parent_B_Country__c)) {
                    con.MailingCountryCode = countryLabelValueMap.get(pfs.Parent_B_Country__c);
                    if (stateLabelValueMap != null && stateLabelValueMap.containsKey(pfs.Parent_B_State__c)) {
                        con.MailingStateCode = stateLabelValueMap.get(pfs.Parent_B_State__c);
                    }
                }
                */
                
                // country and state picklists on PFS updated to synchronize with new country and state picklist values on Contact
                if (pfs.Parent_B_Country__c != oldPFS.Parent_B_Country__c && con.MailingCountry != pfs.Parent_B_Country__c) {
                    con.MailingCountry = pfs.Parent_B_Country__c;
                    updateContact = true;
                }
                if (pfs.Parent_B_State__c != oldPFS.Parent_B_State__c && con.MailingState != pfs.Parent_B_State__c) {
                    con.MailingState = pfs.Parent_B_State__c;
                    updateContact = true;
                }
                if (pfs.Parent_B_Email__c != oldPFS.Parent_B_Email__c && con.Email != pfs.Parent_B_Email__c) {
                    con.Email = pfs.Parent_B_Email__c;
                    updateContact = true;
                }
                if (pfs.Parent_B_First_Name__c != oldPFS.Parent_B_First_Name__c && con.FirstName != pfs.Parent_B_First_Name__c) {
                    con.FirstName = pfs.Parent_B_First_Name__c;
                    updateContact = true;
                }
                if (pfs.Parent_B_Gender__c != oldPFS.Parent_B_Gender__c && con.Gender__c != pfs.Parent_B_Gender__c) {
                    con.Gender__c = pfs.Parent_B_Gender__c;
                    updateContact = true;
                }
                if (pfs.Parent_B_Home_Phone__c != oldPFS.Parent_B_Home_Phone__c && con.HomePhone != pfs.Parent_B_Home_Phone__c) {
                    con.HomePhone = pfs.Parent_B_Home_Phone__c;
                    updateContact = true;
                }
                if (pfs.Parent_B_Last_Name__c != oldPFS.Parent_B_Last_Name__c && pfs.Parent_B_Last_Name__c != null &&
                    con.LastName != pfs.Parent_B_Last_Name__c) {
                    con.LastName = pfs.Parent_B_Last_Name__c;
                    updateContact = true;
                }
                if (pfs.Parent_B_Middle_Name__c != oldPFS.Parent_B_Middle_Name__c && con.Middle_Name__c != pfs.Parent_B_Middle_Name__c) {
                    con.Middle_Name__c = pfs.Parent_B_Middle_Name__c;
                    updateContact = true;
                }
                if (pfs.Parent_B_Mobile_Phone__c != oldPFS.Parent_B_Mobile_Phone__c && con.MobilePhone != pfs.Parent_B_Mobile_Phone__c) {
                    con.MobilePhone = pfs.Parent_B_Mobile_Phone__c;
                    updateContact = true;
                }
                if (pfs.Parent_B_Preferred_Phone__c != oldPFS.Parent_B_Preferred_Phone__c && con.Preferred_Phone__c != pfs.Parent_B_Preferred_Phone__c) {
                    con.Preferred_Phone__c = pfs.Parent_B_Preferred_Phone__c;
                    updateContact = true;
                }
                if (pfs.Parent_B_Suffix__c != oldPFS.Parent_B_Suffix__c && con.Suffix__c != pfs.Parent_B_Suffix__c) {
                    con.Suffix__c = pfs.Parent_B_Suffix__c;
                    updateContact = true;
                }
                if (pfs.Parent_B_Prefix__c != oldPFS.Parent_B_Prefix__c && con.Salutation != pfs.Parent_B_Prefix__c) {
                    con.Salutation = pfs.Parent_B_Prefix__c;
                    updateContact = true;
                }
                if (pfs.Parent_B_Work_Phone__c != oldPFS.Parent_B_Work_Phone__c && con.Phone != pfs.Parent_B_Work_Phone__c) {
                    con.Phone = pfs.Parent_B_Work_Phone__c;
                    updateContact = true;
                }
                if (pfs.Parent_B_ZIP_Postal_Code__c != oldPFS.Parent_B_ZIP_Postal_Code__c && con.MailingPostalCode != pfs.Parent_B_ZIP_Postal_Code__c) {
                    con.MailingPostalCode = pfs.Parent_B_ZIP_Postal_Code__c;
                    updateContact = true;
                }
        
                if (updateContact == true && contactUpdateIdSet.add(con.Id)) {
                    contactUpdateList.add(con);
                }
            }
        }
        
        // update contact
        if (contactUpdateList.size() > 0) {
            update contactUpdateList;
        } 
        
        // finish execution
        //isSyncPFSParentsToContactExecuting = false; COMMENT OUT TO PREVENT TESTDATA SOQL LIMIT [dp] 8.21.13           
    }
}