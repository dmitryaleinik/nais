/**
* @description This class is used to retrieve the configurations for the Family Portal.
*/
public class FamilyPortalSettings {
    
    /**
     * @description The Family Portal Settings record.
     */
    @testVisible 
    public static Family_Portal_Settings__c Record {
        get {
            if (Record == null) {
                // Try getting a record that was already inserted.
                Record = Family_Portal_Settings__c.getInstance('Family');

                // If there still isn't one and we are running a test, we will mock the record with default values.
                if (Record == null && Test.isRunningTest()) {
                    Record = initTestData();
                }
            }
            return Record;
        }
        private set;
    }
    
    @testVisible
    public static Decimal Adjusment_High_Percentage {
        get {
            if (Adjusment_High_Percentage == null) {
                Adjusment_High_Percentage = Record.Adjusment_High_Percentage__c;
            }
            return Adjusment_High_Percentage;
        }
        private set;
    }
    
    @testVisible 
    public static Boolean CC_Amex {
        get {
            if (CC_Amex == null) {
                CC_Amex = Record.CC_Amex__c;
            }
            return CC_Amex;
        }
        private set;
    }
    
    @testVisible 
    public static Boolean CC_Discover {
        get {
            if (CC_Discover == null) {
                CC_Discover = Record.CC_Discover__c;
            }
            return CC_Discover;
        }
        private set;
    }
    
    @testVisible 
    public static Boolean CC_MasterCard {
        get {
            if (CC_MasterCard == null) {
                CC_MasterCard = Record.CC_MasterCard__c;
            }
            return CC_MasterCard;
        }
        private set;
    }
    
    @testVisible 
    public static Boolean CC_Visa {
        get {
            if (CC_Visa == null) {
                CC_Visa = Record.CC_Visa__c;
            }
            return CC_Visa;
        }
        private set;
    }
    
    @testVisible 
    public static Decimal Default_EZ_Household_Assets {
        get {
            if (Default_EZ_Household_Assets == null) {
                Default_EZ_Household_Assets = Record.Default_EZ_Household_Assets__c;
            }
            return Default_EZ_Household_Assets;
        }
        private set;
    }
    
    @testVisible 
    public static Boolean Disable_Auto_Pop_Dependents {
        get {
            if (Disable_Auto_Pop_Dependents == null) {
                Disable_Auto_Pop_Dependents = Record.Disable_Auto_Pop_Dependents__c;
            }
            return Disable_Auto_Pop_Dependents;
        }
        private set;
    }
    
    @testVisible 
    public static Datetime Early_Access_Start_Date {
        get {
            if (Early_Access_Start_Date == null) {
                Early_Access_Start_Date = Record.Early_Access_Start_Date__c;
            }
            return Early_Access_Start_Date;
        }
        private set;
    }
    
    @testVisible 
    public static Id Individual_Account_Owner_Id {
        get {
            if (Individual_Account_Owner_Id == null) {
                Individual_Account_Owner_Id = Record.Individual_Account_Owner_Id__c;
            }
            return Individual_Account_Owner_Id;
        }
        private set;
    }
    
    @testVisible 
    public static Boolean In_Pilot {
        get {
            if (In_Pilot == null) {
                In_Pilot = Record.In_Pilot__c;
            }
            return In_Pilot;
        }
        private set;
    }
    
    @testVisible 
    public static Decimal Parent_A_Total_Salary {
        get {
            if (Parent_A_Total_Salary == null) {
                Parent_A_Total_Salary = Record.Parent_A_Total_Salary__c;
            }
            return Parent_A_Total_Salary;
        }
        private set;
    }
    
    @testVisible 
    public static Decimal Parent_B_Total_Salary {
        get {
            if (Parent_B_Total_Salary == null) {
                Parent_B_Total_Salary = Record.Parent_B_Total_Salary__c;
            }
            return Parent_B_Total_Salary;
        }
        private set;
    }
    
    @testVisible 
    public static Date Override_Today {
        get {
            if (Override_Today == null) {
                Override_Today = Record.Override_Today__c;
            }
            return Override_Today;
        }
        set;
    }
    
    @testVisible 
    private static Family_Portal_Settings__c initTestData() {
        Family_Portal_Settings__c settings = FamilyPortalSettingsTestData.Instance.createFamilyPortalSettingsWithoutReset();
        settings.Individual_Account_Owner_Id__c = UserInfo.getUserId();

        return settings;
    }
}