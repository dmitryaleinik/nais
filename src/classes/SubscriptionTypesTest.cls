@isTest
private class SubscriptionTypesTest {
    @isTest
    private static void getPortalVersionSubscriptionType_nullParam_expectNullResult() {
        Test.startTest();
        String result = SubscriptionTypes.Instance.getPortalVersionSubscriptionType(null);
        Test.stopTest();

        System.assertEquals(null, result, 'Expected the result to be null.');
    }

    @isTest
    private static void getPortalVersionSubscriptionType_invalidString_expectNullResult() {
        Test.startTest();
        String result = SubscriptionTypes.Instance.getPortalVersionSubscriptionType('invalidType');
        Test.stopTest();

        System.assertEquals(null, result, 'Expected the result to be null.');
    }

    @isTest
    private static void getPortalVersionSubscriptionType_passInExtended_expectNullResult() {
        Test.startTest();
        String result = SubscriptionTypes.Instance.getPortalVersionSubscriptionType('Extended');
        Test.stopTest();

        System.assertEquals(null, result, 'Expected the result to be null.');
    }

    @isTest
    private static void getPortalVersionSubscriptionType_passInWaiverDistributor_expectNullResult() {
        Test.startTest();
        String result = SubscriptionTypes.Instance.getPortalVersionSubscriptionType('Waiver Distributor');
        Test.stopTest();

        System.assertEquals(null, result, 'Expected the result to be null.');
    }

    @isTest
    private static void getPortalVersionSubscriptionType_passInFull_expectFullFeatured() {
        Test.startTest();
        String result = SubscriptionTypes.Instance
                .getPortalVersionSubscriptionType(SubscriptionTypes.SUBSCRIPTION_FULL_TYPE);
        Test.stopTest();

        System.assertEquals(SubscriptionTypes.PORTAL_VERSION_FULL_TYPE, result,
                'Expected the result to be ' + SubscriptionTypes.PORTAL_VERSION_FULL_TYPE + '.');
    }

    @isTest
    private static void getPortalVersionSubscriptionType_passInBasic_expectBasic() {
        Test.startTest();
        String result = SubscriptionTypes.Instance
                .getPortalVersionSubscriptionType(SubscriptionTypes.BASIC_TYPE);
        Test.stopTest();

        System.assertEquals(SubscriptionTypes.BASIC_TYPE, result,
                'Expected the result to be ' + SubscriptionTypes.BASIC_TYPE + '.');
    }
}