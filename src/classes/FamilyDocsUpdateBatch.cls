/**
 * @description This batch is used for bulk updates of family documents and allows us to turn off the W2 dupe checking for
 *              the context so that we can avoid async exceptions from calling future methods from batches.
 */
public class FamilyDocsUpdateBatch implements Database.Batchable<SObject> {

    public List<Family_Document__c> FamilyDocsToUpdate { get; private set; }
    public Boolean CheckForW2Dupes { get; private set; }

    public FamilyDocsUpdateBatch(List<Family_Document__c> docsToUpdate, Boolean checkW2Dupes) {
        FamilyDocsToUpdate = docsToUpdate;
        CheckForW2Dupes = checkW2Dupes;
    }

    public FamilyDocsUpdateBatch(List<Family_Document__c> docsToUpdate) {
        this(docsToUpdate, true);
    }

    public List<SObject> start(Database.BatchableContext bc) {
        return FamilyDocsToUpdate;
    }

    public void execute(Database.BatchableContext bc, List<SObject> scope) {
        // If CheckForW2Dupes is true, set W2DuplicateCheck.isExecuting to false so that the trigger runs the dupe check logic.
        W2DuplicateCheck.isExecuting = !CheckForW2Dupes;

        update scope;
    }

    public void finish(Database.BatchableContext bc) { }
}