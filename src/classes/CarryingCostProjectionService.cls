/**
 * @description Prototype class created for IDEA-4 to attempt projecting the carrying cost of a student given a financial aid award.
 */
public class CarryingCostProjectionService {

    // A min grade of 0 represents kindergarten
    private static final Integer MIN_GRADE = 0;
    private static final Integer MAX_GRADE = 12;

    private CarryingCostProjectionService() { }

    public Projections calculate(Request projRequest) {

        // Get student folders
        // For now assume that all folders will be for the same academic year.
        List<Student_Folder__c> folders = selectFolders(projRequest.FolderIds);

        // Gather school Ids from folders
        Set<Id> schoolIds = getSchoolIds(folders);

        // If tuition percent change is specified, only query annual settings for academic year specified on folders.
        // Otherwise include annual settings for 2-3 academic years prior
        // If percent change not specified, use historical data to calculate average percent change.
        // For now, assume that percent change is always specified.
        Set<String> academicYears = getAcademicYears(projRequest, folders);

        Map<Id, List<Annual_Setting__c>> settingsBySchoolId = getSettingsBySchoolId(schoolIds, academicYears);

        // For each folder, project carrying cost by:
        // -- increasing tuition by percent change
        // -- calculate annual cost by using Aid_of_Tuition__c
        // -- repeat until we reach max grade.
        // -- add total projection to response.
        Map<Id, Decimal> projectionsByFolderId = calculateProjections(folders, settingsBySchoolId, projRequest);

        return new Projections(projectionsByFolderId);
    }

    private Map<Id, Decimal> calculateProjections(List<Student_Folder__c> folders, Map<Id, List<Annual_Setting__c>> settingsBySchoolId, Request projRequest) {
        Map<Id, Decimal> projectionsByFolderId = new Map<Id, Decimal>();

        for (Student_Folder__c folder : folders) {
            List<Annual_Setting__c> settings = settingsBySchoolId.get(folder.School__c);

            Decimal projection = doProjection(folder, settings, projRequest);

            projectionsByFolderId.put(folder.Id, projection);
        }

        return projectionsByFolderId;
    }

    private Decimal doProjection(Student_Folder__c folder, List<Annual_Setting__c> settings, Request projRequest) {
        Annual_Setting__c setting = settings[0];

        // This should be coming from the annual settings based on the current grade being projected or just using the one schedule fields.
        Decimal tuition = folder.Day_Boarding__c == 'Day' ? setting.One_Schedule_Day_Tuition__c : setting.One_Schedule_Boarding_Tuition__c;

        // if null, use mock value.
        if (tuition == null) {
            tuition = 10000;
        }

        Decimal tuitionPercentIncrease = folder.Day_Boarding__c == 'Day' ? projRequest.DayTuitionPercentChange : projRequest.BoardingTuitionPercentChange;
        Decimal percentage = tuitionPercentIncrease / 100;

        // For now, using a mock value
        Decimal startingGrant = 3000;
        Integer startGrade = folder.Grade_Applying__c == 'K' ? 0 : Integer.valueOf(folder.Grade_Applying__c);
        // The grade they are applying for is accounted for with the starting grant. Start at the next grade for projections.
        startGrade++;

        // This should be coming from the student folder but there is a bug that prevents us from doing so.
        Decimal aidPercentOfTuition = startingGrant / tuition;
        System.debug('&&& aid percentage of tuition: ' + aidPercentOfTuition);

        Decimal totalCost = startingGrant;
        for (Integer i = startGrade; i <= MAX_GRADE; i++) {
            Decimal tuitionIncrease = tuition * percentage;
            System.debug('&&& tuition increase for grade ' + i + ': ' + tuitionIncrease);
            tuition = tuition + tuitionIncrease;
            System.debug('&&& projected tuition for grade ' + i + ': ' + tuition);
            Decimal projectedAid = tuition * aidPercentOfTuition;
            System.debug('&&& projected aid for grade ' + i + ': ' + projectedAid);
            totalCost = totalCost + projectedAid;
        }

        return totalCost;
    }

    private List<Student_Folder__c> selectFolders(Set<Id> recordIds) {
        return [SELECT Id, Day_Boarding__c, Grade_Applying__c, Grant_Awarded__c, Academic_Year_Picklist__c, School__c, Aid_of_Tuition__c FROM Student_Folder__c WHERE Id IN :recordIds];
    }

    private Set<Id> getSchoolIds(List<Student_Folder__c> folders) {
        Set<Id> schoolIds = new Set<Id>();

        for (Student_Folder__c folder : folders) {
            schoolIds.add(folder.School__c);
        }

        return schoolIds;
    }

    private Set<String> getAcademicYears(Request projRequest, List<Student_Folder__c> folders) {
        String latestAcademicYear = folders[0].Academic_Year_Picklist__c;

        Set<String> academicYears = new Set<String> { latestAcademicYear };

        if (projRequest.DayTuitionPercentChange != null && projRequest.BoardingTuitionPercentChange != null) {
            return academicYears;
        }

        Integer startYear = Integer.valueOf(latestAcademicYear.left(4));
        Integer endYear = Integer.valueOf(latestAcademicYear.right(4));

        academicYears.add(String.valueOf(startYear - 1) + '-' + String.valueOf(endYear - 1));
        academicYears.add(String.valueOf(startYear - 2) + '-' + String.valueOf(endYear - 2));

        return academicYears;
    }

    private List<Annual_Setting__c> selectAnnualSettings(Set<Id> schoolIds, Set<String> academicYears) {
        return [SELECT Id, One_Schedule_Boarding_Tuition__c, One_Schedule_Day_Tuition__c, Academic_Year_Name__c, School__c FROM Annual_Setting__c WHERE School__c IN :schoolIds AND Academic_Year_Name__c IN :academicYears];
    }

    private Map<Id, List<Annual_Setting__c>> getSettingsBySchoolId(Set<Id> schoolIds, Set<String> academicYears) {
        List<Annual_Setting__c> allSettings = selectAnnualSettings(schoolIds, academicYears);

        Map<Id, List<Annual_Setting__c>> settingsBySchoolId = new Map<Id, List<Annual_Setting__c>>();

        for (Annual_Setting__c setting : allSettings) {
            List<Annual_Setting__c> settingsForSchool = settingsBySchoolId.get(setting.School__c);

            if (settingsForSchool == null) {
                settingsForSchool = new List<Annual_Setting__c>();
            }

            settingsForSchool.add(setting);
            settingsBySchoolId.put(setting.School__c, settingsForSchool);
        }

        return settingsBySchoolId;
    }

    // We may want the request to contain more data on each individual folder so that we have the ability to specify more info per folder
    // For example, the maximum grade. Not every school does K-12.
    // While we could find a way to query for additional info that may be needed, it might also be nice to allow school users to change inputs and run different projections.
    public class Request {

        public Set<Id> FolderIds { get; set; }

        // If this is null, then we will use student folder to get past annual settings and calculate an average percent increase in tuition.
        public Decimal DayTuitionPercentChange { get; set; }

        public Decimal BoardingTuitionPercentChange { get; set; }
    }

    // We should include more than just a map of numbers. Other information could be helpful such as:
    // -- a more granular look at each projected year per student
    // -- the number of years projected
    // -- The projected tuition cost per year
    // After running projection and displaying results, we could even let school users adjust values to see how things would change.
    public class Projections {

        public Projections(Map<Id, Decimal> projectionsByFolder) {
            this.CostByFolderId = projectionsByFolder;
        }

        public Map<Id, Decimal> CostByFolderId { get; private set; }
    }

    public static CarryingCostProjectionService Instance {
        get {
            if (Instance == null) {
                Instance = new CarryingCostProjectionService();
            }
            return Instance;
        }
        private set;
    }
}