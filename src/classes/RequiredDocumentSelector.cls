/**
 * @description This class is responsible for querying Required Document records.
 */
public class RequiredDocumentSelector extends fflib_SObjectSelector {
    
    @testVisible private static final String SCHOOL_IDS_PARAM = 'schoolIds';
    @testVisible private static final String ACADEMIC_YEAR_NAMES_PARAM = 'academicYearNames';
    
    /**
     * @description Default constructor for an instance of the RequiredDocumentSelector that will not include field sets while
     *              enforcing FLS and CRUD.
     */
    public RequiredDocumentSelector() {
        // We have to avoid enforcing FLS or CRUD so that family users can access these records as well in very rare
        // situations where they must be able to query and create required documents along with annual settings as part of the family side integration.
        super(false, false, false, false);
    }
    
    /**
     * @description Query the existing required documents for the given Schools/AcademicYear.
     * @param schoolIds The set of school ids that will be used to query RD.
     * @param academicYearNames The set of academic years that will be used to query RD.
     * @return A list of RD that mets the given conditions.
     */
    public List<Required_Document__c> selectBySchoolAcademicYear(Set<Id> schoolIds,  Set<String> academicYearNames) {
        
        ArgumentNullException.throwIfNull(schoolIds, SCHOOL_IDS_PARAM);
        ArgumentNullException.throwIfNull(academicYearNames, ACADEMIC_YEAR_NAMES_PARAM);

        String condition = 'School__c IN :schoolIds AND Academic_Year_Name__c IN :academicYearNames AND Deleted__c = false';
        
        fflib_QueryFactory requiredDocumentQueryFactory = newQueryFactory();
        
        return Database.Query(requiredDocumentQueryFactory.setCondition(condition).toSOQL());
    }
    
    /**
     * @description Query and maps the existing required documents for the given Schools/AcademicYear.
     * @param schoolIds The set of school ids that will be used to query RD.
     * @param academicYearNames The set of academic years that will be used to query RD.
     * @return A map of RD that mets the given conditions. Where the key is: SchoolId + '-' + academicYearName + '-' + docYear
     */
    public Map<String, List<Required_Document__c>> mapBySchoolAcademicYear(Set<Id> schoolIds,  Set<String> academicYearNames) {
        
        ArgumentNullException.throwIfNull(schoolIds, SCHOOL_IDS_PARAM);
        ArgumentNullException.throwIfNull(academicYearNames, ACADEMIC_YEAR_NAMES_PARAM);
        
        List<Required_Document__c> records = selectBySchoolAcademicYear(schoolIds, academicYearNames);
        
        Map<String, List<Required_Document__c>> result = new Map<String, List<Required_Document__c>>();
        
        for(Required_Document__c rd : records) {
            
           String key = RequiredDocumentSelector.newInstance().createMapKey(rd);            
            List<Required_Document__c> tmp = result.containsKey(key) ? result.get(key) : new List<Required_Document__c>();
            tmp.add(rd);
            result.put(key, tmp);
        }
        
        return result;
    }
    
    public String createMapKey(Id SchoolId, String academicYearName, String docYear) {
        
        return SchoolId + '-' + academicYearName + '-' + docYear;
    }
    
    public String createMapKey(Required_Document__c rd) {

        return rd.School__c + '-' + rd.Academic_Year_Name__c + '-' + rd.Document_Year__c;
    }
    
    private Schema.SObjectType getSObjectType() {
        return Required_Document__c.SObjectType;
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>{
                Required_Document__c.Name,
                Required_Document__c.Academic_Year__c,
                Required_Document__c.Academic_Year_Name__c,
                Required_Document__c.Deleted__c,
                Required_Document__c.Document_Type__c,
                Required_Document__c.Document_Year__c,
                Required_Document__c.School__c
        };
    }
    
    private String mOrderBy;
    
    /**
     * @description Controls the default ordering of records returned by the base queries,
     *              defaults to the 'Name' field of the object or 'CreatedDate' if there is none.
     **/
    public override String getOrderBy() {
        
        if (mOrderBy == null) {
            return super.getOrderBy();
        }
        return mOrderBy;
    }
    
    /**
     * @description Use this method to override the default sorting provided by fflib_SObjectSelector.
     * @param orderBy You can use the following formatt: 'Academic_Year__r.Start_Date__c DESC, Name ASC'
     */
    public void setOrderBy(String orderBy) { 
        mOrderBy = orderBy;
    }
    
    /**
     * @description Creates a new instance of the RequiredDocumentSelector.
     * @return An instance of RequiredDocumentSelector.
     */
    public static RequiredDocumentSelector newInstance() {
        return new RequiredDocumentSelector();
    }
}