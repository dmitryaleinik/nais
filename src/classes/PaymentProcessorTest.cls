@isTest
private class PaymentProcessorTest {
    private static Payment_Processor__mdt getPaymentProcessorRecord() {
        List<Payment_Processor__mdt> paymentProcessors = PaymentProcessorSelector.Instance
                .selectByName(new Set<String> { PaymentProcessor.PAYMENT_PROCESSOR_NAME });

        System.assert(!paymentProcessors.isEmpty(), 'Expected a Payment Processor record to be returned.');
        System.assertEquals(1, paymentProcessors.size(), 'Expected there to be exactly 1 Payment Processor record.');

        return paymentProcessors[0];
    }

    private static Payment_Provider__mdt getPaymentProviderRecord() {
        String providerName = getPaymentProcessorRecord().Payment_Provider__c;
        if (providerName == null) {
            throw new ConfigurationException('No Payment Provider found.');
        }

        List<Payment_Provider__mdt> paymentProviders = PaymentProviderSelector.Instance
                .selectByName(new Set<String> { providerName });
        if (paymentProviders == null || paymentProviders.isEmpty()) {
            throw new ConfigurationException('No Payment Provider found.');
        }
        return paymentProviders[0];
    }

    @isTest
    private static void pfsApplicationFeeTracker_expectValuesMatch() {
        String pfsApplicationFeeTracker = getPaymentProcessorRecord().PFS_Application_Fee_Tracker__c;
        String tracker = PaymentProcessor.PfsApplicationFeeTracker;

        System.assertEquals(pfsApplicationFeeTracker, tracker,
                'Expected the tracker to be ' + pfsApplicationFeeTracker + '.');
    }

    @isTest
    private static void pfsApplicationFeeItemDescription_expectValuesMatch() {
        String pfsApplicationFeeItemDescription = getPaymentProcessorRecord().PFS_Application_Fee_Item_Description__c;
        String description = PaymentProcessor.PfsApplicationFeeItemDescription;

        System.assertEquals(pfsApplicationFeeItemDescription, description,
                'Expected the description to be ' + pfsApplicationFeeItemDescription + '.');
    }

    @isTest
    private static void creditCardStatementDescriptor_expectValuesMatch() {
        String creditCardStatementDescriptor = getPaymentProcessorRecord().Credit_Card_Statement_Descriptor__c;
        String description = PaymentProcessor.CreditCardStatementDescriptor;

        System.assertEquals(creditCardStatementDescriptor, description,
                'Expected the description to be ' + creditCardStatementDescriptor + '.');
    }

    @isTest
    private static void customDebugs_expectValuesMatch() {
        Boolean debug1 = getPaymentProviderRecord().Custom_Debug__c;
        Boolean debug2 = getPaymentProviderRecord().Custom_Debug_2__c;
        Boolean ppDebug1 = PaymentProcessor.CustomDebug;
        Boolean ppDebug2 = PaymentProcessor.CustomDebug2;

        System.assertEquals(debug1, ppDebug1, 'Expected the values of the custom debug 1 to match.');
        System.assertEquals(debug2, ppDebug2, 'Expected the values of the custom debug 2 to match.');
    }

    @isTest
    private static void receiptOrgWideEmailAddress_expectValuesMatch() {
        String receiptOrgWideEmailAddress = getPaymentProcessorRecord().Receipt_Org_Wide_Email_Address__c;
        String emailAddress = PaymentProcessor.ReceiptOrgWideEmailAddress;

        System.assertEquals(receiptOrgWideEmailAddress, emailAddress,
                'Expected the description to be ' + receiptOrgWideEmailAddress + '.');
    }

    @isTest
    private static void receiptEmailSubject_expectValuesMatch() {
        String receiptEmailSubject = getPaymentProcessorRecord().Receipt_Email_Subject__c;
        String subject = PaymentProcessor.ReceiptEmailSubject;

        System.assertEquals(receiptEmailSubject, subject,
                'Expected the email subject to be ' + receiptEmailSubject + '.');
    }

    @isTest
    private static void inTestMode_expectValuesMatch() {
        Payment_Provider__mdt providerRecord = getPaymentProviderRecord();
        Boolean testMode = providerRecord == null || providerRecord.Enable_Live_Transactions__c == false;
        Boolean isTestMode = PaymentProcessor.InTestMode;

        System.assertEquals(testMode, isTestMode, 'Expected the test mode values to match.');
    }

    @isTest
    private static void testCnPSelectOptions() {
        // make sure the Click and Pledge dropdown options are available
        System.assert(PaymentProcessor.getBillingCountrySelectOptions().size() > 0);
        System.assert(PaymentProcessor.getBillingStateUSSelectOptions().size() > 0);
        System.assert(PaymentProcessor.getExpireMonthSelectOptions().size() > 0);
        System.assert(PaymentProcessor.getExpireYearSelectOptions().size() > 0);
        System.assert(PaymentProcessor.getAccountTypeSelectOptions().size() > 0);

        SelectOption option = PaymentProcessor.getAccountTypeSelectOptions()[0];
        System.assertEquals(option.getLabel(), PaymentProcessor.getAccountTypeLabel(option.getValue()));
    }

    @isTest
    private static void process_creditCard_expectSuccessfulResult() {
        PaymentRequest request = new PaymentRequest();
        request.PaymentInformation = PaymentInformationTestData.Instance.asCreditCardInformation();
        request.OpportunityId = OpportunityTestData.Instance.DefaultOpportunity.Id;

        Test.startTest();
        PaymentResult result = PaymentProcessor.process(request);
        Test.stopTest();

        System.assert(result.isSuccess, 'Expected the payment to be a success.');
        System.assertNotEquals(null, result.TransactionLineItemId, 'Expected a Transaction Line Item to be generated.');
    }

    @isTest
    private static void process_invalidCreditCard_expectFailureResult() {
        PaymentRequest request = new PaymentRequest();
        CreditCardInformation ccInfo = PaymentInformationTestData.Instance.asCreditCardInformation();
        ccInfo.CardNumber = '123456';
        request.PaymentInformation = ccInfo;
        request.OpportunityId = OpportunityTestData.Instance.DefaultOpportunity.Id;

        Test.startTest();
        PaymentResult result = PaymentProcessor.process(request);
        Test.stopTest();

        System.assert(!result.isSuccess, 'Expected the payment to be a success.');
    }

    @isTest
    private static void process_check_expectSuccessfulResult() {
        PaymentRequest request = new PaymentRequest();
        request.PaymentInformation = PaymentInformationTestData.Instance.asECheckInformation();
        request.OpportunityId = OpportunityTestData.Instance.DefaultOpportunity.Id;

        Test.startTest();
        PaymentResult result = PaymentProcessor.process(request);
        Test.stopTest();

        System.assert(result.isSuccess, 'Expected the payment to be a success.');
        System.assertNotEquals(null, result.TransactionLineItemId, 'Expected a Transaction Line Item to be generated.');
    }

    @isTest
    private static void process_invalidCheck_expectFailureResult() {
        PaymentRequest request = new PaymentRequest();
        ECheckInformation ecInfo = PaymentInformationTestData.Instance.asECheckInformation();
        ecInfo.BankName = null;
        request.PaymentInformation = ecInfo;
        request.OpportunityId = OpportunityTestData.Instance.DefaultOpportunity.Id;

        Test.startTest();
        PaymentResult result = PaymentProcessor.process(request);
        Test.stopTest();

        System.assert(!result.isSuccess, 'Expected the payment to be a success.');
    }
}