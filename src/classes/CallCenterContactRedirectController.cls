public with sharing class CallCenterContactRedirectController {
    /*move this to recordtype class when it gets checked into GIT for now leave it here*/


     private static final Map<String, Schema.RecordTypeInfo> caseTypeMap = Case.SObjectType.getDescribe().getRecordTypeInfosByName();
    public static final Id parentSupportCaseTypeId = caseTypeMap.get('Parent Call').RecordTypeId;
     public static final Id schoolSupportCaseTypeId = caseTypeMap.get('School Call').RecordTypeId;

    private static final String CALL_CENTER_APP_NAME = 'Call_Center_Agent_Console';
    public static Id CallCenterConsoleAppId {
        get {
            if(CallCenterConsoleAppId == null) {
                List<AppMenuItem> menuItems = [SELECT ApplicationId FROM AppMenuItem WHERE Name = :CALL_CENTER_APP_NAME];
                if(menuItems.isEmpty()) { throw new AppNotFoundException('No Console App named' + CALL_CENTER_APP_NAME + ' was found');}
                CallCenterConsoleAppId = menuItems[0].ApplicationId;
            }
            return CallCenterConsoleAppId;
        }
    }

    public CallCenterContactRedirectController(ApexPages.StandardController controller) {
    }

    private String getCaseURL(String recordTypeID, String contactID){

        return getCaseURL(recordTypeID, contactID, null);
    }

    // overload method, default to "true" for phoneAutoFill
    public static String getCaseURL(String recordTypeID, String contactID, String accountID){
        return getCaseURL(recordTypeID, contactID, accountID, true);
    }

    //private String getCaseURL(String recordTypeID, String contactID, String accountID){ // refactoring to public static method [dp] 1.17.2014
    public static String getCaseURL(String recordTypeID, String contactID, String accountID, Boolean phoneAutofill){

        Cookie cDnis = ApexPages.currentPage().getCookies().get('dnis');
        Cookie cAni =  ApexPages.currentPage().getCookies().get('ani');
        Cookie cCallId =  ApexPages.currentPage().getCookies().get('callID');
         FieldIDbyName__c dnisSetting  =     FieldIDbyName__c.getInstance('dnis');
         FieldIDbyName__c aniSetting  =     FieldIDbyName__c.getInstance('ani');
         FieldIDbyName__c callIDSetting  =     FieldIDbyName__c.getInstance('callID');
         FieldIDbyName__c phoneIfNotInSystemSetting  =     FieldIDbyName__c.getInstance('phoneIfNotInSystem');

         String url = '/500/e?RecordType=' + recordTypeID ;
         url+=(contactID!=null) ?  '&def_contact_id=' + contactID : '';
         url+=(accountID!=null) ?  '&def_account_id=' + accountID : '';
         System.debug('dnisSetting ' +dnisSetting + ' cDnis ' + cDnis);
         System.debug('aniSetting ' +aniSetting + ' ani ' + cAni);
         url+= (dnisSetting!=null && cDnis!=null) ? '&' + dnisSetting.Field_ID__c + '=' + cDnis.getValue()  :'' ;
         url+=  (aniSetting!=null && cAni!=null) ? '&'+ aniSetting.Field_ID__c + '=' + cAni.getValue() : '';
         url+=  (callIDSetting!=null && cCallId!=null) ? '&'+ callIDSetting.Field_ID__c + '=' + cCallId.getValue() : '';

         if (phoneAutofill){
             url+=  (phoneIfNotInSystemSetting!=null && cAni!=null) ? '&'+ phoneIfNotInSystemSetting.Field_ID__c + '=' + cAni.getValue() : '';
         }
         // NAIS-1392 set retURL
         // refactored to add nothing in case both contact id and account id are null, since this may be used other places [dp] 1.17.2014
         url +=(contactID!=null) ? '&retURL=/' + contactID : '';
         url +=(contactID==null && accountID!=null) ? '&retURL=/' + accountID : '';

         url += '&writephonetocase=1';
         return url;
    }

    // overload method, pass in "false" for phone autofill
    public PageReference newSchoolCaseFromContactNoPhoneAutofill() {
        return newSchoolCaseFromContact(false);
    }

    // overload method, default to "true" for phone autofill
    public PageReference newSchoolCaseFromContact() {
        return newSchoolCaseFromContact(true);
    }

    // method takes boolean to determine whether or not to autofill the "Phone (if not in system)" field
    public PageReference newSchoolCaseFromContact(Boolean phoneAutofill) {
        Id contactId = System.currentPageReference().getParameters().get('id');

        Contact c =[select account.id from contact where id = :contactId  ];
        PageReference newPage = new PageReference(getCaseURL(schoolSupportCaseTypeId,contactId,c.account.id, phoneAutofill));
        newPage.setRedirect(true);
        return newPage;
    }

    // overload method, pass in "false" for phone autofill
    public PageReference newParentCaseFromContactNoPhoneAutofill() {
        return newParentCaseFromContact(false);
    }
    // overload method, default to "true" for phone autofill
    public PageReference newParentCaseFromContact() {
        return newParentCaseFromContact(true);
    }

    //getCaseUrl -- method takes boolean to determine whether or not to autofill the "Phone (if not in system)" field
    public PageReference newParentCaseFromContact(Boolean phoneAutofill) {

        Id contactId = System.currentPageReference().getParameters().get('id');
        PageReference newPage;
         if(Test.isRunningTest()) {
            newPage = new PageReference('/console?tsid=' + CallCenterConsoleAppId);
            newPage.setAnchor(EncodingUtil.urlEncode(getCaseURL(parentSupportCaseTypeId, contactId, null, phoneAutofill), 'UTF-8'));
        } else {
            newPage = new PageReference(getCaseURL(parentSupportCaseTypeId, contactId, null, phoneAutofill));
        }

        newPage.setRedirect(true);
        return newPage;
    }

    //getCaseUrl
    public PageReference newSchoolCaseFromAccount() {
        Id accountID = System.currentPageReference().getParameters().get('id');


         PageReference newPage = new PageReference(getCaseURL(schoolSupportCaseTypeId,null,accountID));
        newPage.setRedirect(true);
        return newPage;


    }

    public class AppNotFoundException extends Exception {}

}