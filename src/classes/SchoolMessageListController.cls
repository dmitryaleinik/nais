/**
 * Class: SchoolMessageListController
 *
 * Copyright (C) 2015 NAIS
 *
 * Purpose: Separate the current Cases tab into one for managing school/family messages, and one for managing support cases
 *
 * Separate the list of cases in two tab:
 * 1)Parent Message for RecordType = School / Parent Communication
 * 2)Support Tickets for RecordType = School Portal Case
 *
 * Where Referenced:
 *      - School Portal - SChoolMessageList component
 *
 * The component that is related is invoked from these pages:
 *      - SchoolPortal - SchoolListMessage page
 *      - SchoolPortal - SchoolSupportTicket page
 *
 * Change History:
 *
 * Developer           Date        JIRA Ref     Description
 * ---------------------------------------------------------------------------------------
 * Andrea Useche       05/19/15    NAIS-1973   Initial Development
 * Charles Howard      06/22/15    NAIS-2467   Refactoring to fix Apex CPU limit error
 *
 */
public with sharing class SchoolMessageListController implements SchoolAcademicYearSelectorInterface{
     /*Initialization*/
    public SchoolMessageListController(){
        firstLabel='<<First';
        lastLabel ='Last>>';
        prevLabel ='<Prev';
        nextLabel='Next>';
        pageNumber =1;
        //Get the value of the Custom Setting for size of page
        LimitSize = SchoolPortalSettings.Fee_Waivers_Table_Page_Size;

        loadAcademicYear();
    }
    /*End Initialization*/
    /*Properties*/

    // NAIS-2467
    private map<Id, Case> caseMap {get; set;}
    public List<Case> cases {
        get {
            loadCasesList();
            validateCasePFS();
            return caseMap.values();
        }
        set;
    }

    public String titleMessage {get;set;}
    public String txtInformation {get;set;}
    public Boolean IsFamilyPortal {get;set;}
    public Boolean isSupportTicket {get;set;}
    public Id recordTypeId {get; set;}
    map<Id, String> folderNameByCase {get;set;}
    public Map<String, List<School_PFS_Assignment__c>> mapSPAsByPFSNumber {get; set;}
    public String titleMessageNew {get;set;}
    public String retUrl {get;set;}
    /*Pagination*/
    public Integer pageNumber {get;set;}
    public Integer totalPages {get;set;}
    private integer OffsetSize =0;
    @testVisible private integer LimitSize {get;set;}
    public String firstLabel{get;set;}
    public String lastLabel{get;set;}
    public String prevLabel{get;set;}
    public String nextLabel{get;set;}
    public Integer totalRecs {get; set;}
    public List<Integer> numberPages{get;set;}
    public Boolean ismapSPAEmpty {get;set;}
    /*End Pagination*/

    /*End Properties*/

    /*Action Methods*/
    //Redirect to the Layout for create a new case with the correct recordType, and return to the previous list
    public PageReference NewAction() {
        PageReference p;
        string retPage='';
        if (!IsFamilyPortal) {
            Schema.DescribeSObjectResult obj = Case.SObjectType.getDescribe();
            p= new PageReference('/apex/SchoolMessage');
            retPage= (isSupportTicket) ? '/apex/SchoolSupportTicket' : '/apex/SchoolListMessage';
        }
        p.getParameters().put('RecordType', recordTypeId);
        p.getParameters().put('retURL', retPage);
        p.getParameters().put('isSupport', '1');
        p.setRedirect(true);
        return p;
    }

    public String ascOrDesc {get{if(ascOrDesc == null) ascOrDesc ='DESC';return ascOrDesc;}set;}
    public String lastSortFieldName {get; set;}
    public String sortFieldName {get; set;}

    // sort by columns
    public void sort() {
        if (lastSortFieldName == sortFieldName) {
            if (ascOrDesc == 'ASC') {
                ascOrDesc = 'DESC';
            } else {
                ascOrDesc = 'ASC';
            }
        } else {
            ascOrDesc = 'DESC';
        }

        lastSortFieldName = sortFieldName;
    }

    public string filterTerm {get;set;}

    //Go to the first page on pagination
    public void FirstPage() {
        pageNumber=1;
        OffsetSize = 0;
    }
    //Go to the previous page on pagination
    public void previous() {
        if(pageNumber > 1) {
            pageNumber = pageNumber-1;
            OffsetSize = (OffsetSize-LimitSize > 0)?(OffsetSize-LimitSize): 0;
        }
    }
    //Go to the next page on pagination
    public void next() {
        if(pageNumber < totalPages) {
            pageNumber = pageNumber+1;
            OffsetSize = (OffsetSize+LimitSize > TotalRecs)? TotalRecs: OffsetSize+LimitSize;
        }
    }
    //Go to the last page on pagination
    public void LastPage() {
        pageNumber = totalPages;
        OffsetSize = (pageNumber*LimitSize)-LimitSize;
    }
    //Evaluate the size to the records for show the pagination buttons.
    public boolean getPagination() {
        return (totalRecs > 0 && totalRecs > LimitSize)? true:false;
    }
    //Go to the specific page on pagination
    public void goTo() {
        OffsetSize = (pageNumber==1)?0:(pageNumber*LimitSize)-LimitSize;
    }
    /*End Action Methods*/
    /*Helper Methods*/
    //Get a list of cases according to the recortype.

    // [DP] 06.29.2015 NAIS-2480
    public static List<Id> schoolPortalCaseRecordTypes = new List<Id>{RecordTypes.schoolParentCommunicationCaseTypeId, RecordTypes.familyPortalCaseSchoolTypeId};
    public static List<Id> schoolSupportCaseRecordTypes = new List<Id>{RecordTypes.schoolPortalCaseTypeId};

    private void loadCasesList() {
        // [DP] 06.29.2015 NAIS-2480 using a list now, since we need to query multiple record types
        List<Id> recordTypeIdList;
        recordTypeIdList = (!isSupportTicket) ? schoolPortalCaseRecordTypes : schoolSupportCaseRecordTypes;

        numberPages = new list<integer>();
        recordTypeId = recordTypeIdList[0]; // [DP] 06.29.2015 NAIS-2480 this is the default record type id for new cases
        titleMessageNew = (!isSupportTicket)?'New Parent Message':'New Support Ticket';
        retUrl= (isSupportTicket)?'SchoolSupportTicket':'SchoolListMessage';

        if(sortFieldName==null){
            sortFieldName = 'Created_Updated_for_School_Portal__c';
            lastSortFieldName = sortFieldName;
            ascOrDesc = 'ASC';
        } else if(sortFieldName =='Applicant__r.First_Name__c'){
            sortFieldName = 'Applicant__r.First_Name__c '+ascOrDesc+', Applicant__r.Last_Name__c';
        }

        DateTime currentAcademicYearStartDate = currentAcademicYear.Start_Date__c;
        DateTime currentAcademicYearEndDate = currentAcademicYear.End_Date__c;

        // SFP-1700: We only pass in school Ids if we are viewing cases submitted by the school. Otherwise we pass in a
        // null value to tell the selector not to use the accountId filter.
        List<Id> schoolIds;
        if (isSupportTicket) {
            schoolIds = new List<Id>{ GlobalVariables.getCurrentSchoolId() };
        }

        totalRecs = CaseSelector.Instance.selectWithoutLimitAndOffset(
                recordTypeIdList,
                schoolIds,
                currentAcademicYearStartDate,
                currentAcademicYearEndDate,
                filterTerm,
                sortFieldName,
                ascOrDesc).size();

        List<Case> caseList = CaseSelector.Instance.selectWithCustomLimitOffsetOrder(
                recordTypeIdList,
                schoolIds,
                currentAcademicYearStartDate,
                currentAcademicYearEndDate,
                filterTerm,
                sortFieldName,
                ascOrDesc,
                limitSize,
                offsetSize);
        caseMap = new Map<Id,Case>(caseList);

        if (math.mod(totalRecs, LimitSize) == 0) {
            totalPages = (Integer)totalRecs / LimitSize;
        } else {
            totalPages = ((Integer)totalRecs / LimitSize) + 1;
        }

        for (Integer i = 1; i <= totalPages; i++) {
            numberPages.add(i);
        }

        //If the list is for schoolParentCommunicationCaseTypeId recordType, we need load the StudentFolder related to these cases.
        if(!IsFamilyPortal && !isSupportTicket)
            loadStudentFolder();
    }

    //Get the Student Folders links related to each case.
    private void loadStudentFolder() {
        list<Case> auxCases = caseMap.values();

        set<String> PFSNumbers = new set<String>();
        set<Id> AccountIds =  new set<Id>();
        ismapSPAEmpty = false;

        // Capture the PFS and Account related to the case
        for (Case c: auxCases) {
            if (String.isNotBlank(c.PFS__c)){
                PFSNumbers.add(c.PFS__r.PFS_Number__c);
            } else if (String.isNotBlank(c.Applicant__c)) {
                PFSNumbers.add(c.Applicant__r.Pfs__r.PFS_Number__c);
            }
            AccountIds.add(c.CreatedBy.Contact.AccountId);
        }

        //Query for all SPA with the PFSNumber and Account related to the cases.
        //Return a map with list to SPA per AccountId.

        // Initialize the container structure for SPA records, grouped by PFS Number
        mapSPAsByPFSNumber = new Map<String, List<School_PFS_Assignment__c>>{}; // v2
        for(School_PFS_Assignment__c schoolPFS: [Select Id, Student_Folder__c, Applicant__c, Applicant__r.Contact__r.Name,PFS_Number__c,School__c
                                                                from School_PFS_Assignment__c where PFS_Number__c IN :PFSNumbers])
        {
            // Get the group of all SPA records related to the currently referenced PFS v2
            List<School_PFS_Assignment__c> currentList = mapSPAsByPFSNumber.get(schoolPFS.PFS_Number__c);

            // If there is not an existing list of SPAs then instantiate a new list v2
            if(currentList == null)
                currentList = new List<School_PFS_Assignment__c>{};

            // Add the current SPA to the list, and insert the complete list into the map v2
            //  with the PFS Number as the key
            currentList.add(schoolPFS);
            mapSPAsByPFSNumber.put(schoolPFS.PFS_Number__c, currentList);
        }
        //SFP-50,[G.S]
        if (mapSPAsByPFSNumber == null || mapSPAsByPFSNumber.size() == 0) {
            ismapSPAEmpty = true;
        }

        //SFP-289, [G.S]
        for (Case c: auxCases) {
            List<School_PFS_Assignment__c> spfaList = new List<School_PFS_Assignment__c>();
            spfaList.add(new School_PFS_Assignment__c());
            if(String.isNotBlank(c.PfS__c) && !mapSPAsByPFSNumber.containsKey(c.PfS__r.PFS_Number__c)){
                mapSPAsByPFSNumber.put(c.PfS__r.PFS_Number__c, spfaList);
            }
        }
    }

    public void validateCasePFS(){
        // loop through cases and remove any that are missing a PFS association
        case c;
        for (id each : caseMap.keyset()){
            c = caseMap.get(each);
            if(String.isNotBlank(c.PFS__c) && c.PFS__r.PFS_Number__c == null)
                caseMap.remove(each);
            else if( String.isNotBlank(c.Applicant__c) && c.Applicant__r.Pfs__r.PFS_Number__c == null)
                caseMap.remove(each);
        }
    }
    /*End Helper Methods*/

    // used for the acad year selector
    public SchoolMessageListController Me {
        get { return this; }
    }

    public Id academicyearid;

    public String getAcademicYearId() {
        return academicyearid;
    }
    public Academic_Year__c currentAcademicYear { get; private set; }
    public void setAcademicYearId(String academicYearIdParam) {
        academicyearid = academicYearIdParam;
        currentAcademicYear = GlobalVariables.getAcademicYear(academicyearid);
    }

    // on change of academic year, reload page with new folder
    public PageReference SchoolAcademicYearSelector_OnChange(Boolean saveRecord) {
        PageReference newPage = new PageReference(ApexPages.currentPage().getUrl());
        newPage.getParameters().put('academicyearid', getAcademicYearId());
        newPage.setRedirect(true);
        return newPage;
    }

    public void loadAcademicYear() {
        // Due to the selector_onchange not refreshing properly, we redirect to the same page with academicyearid parameter.
        Map<String, String> parameters = ApexPages.currentPage().getParameters();
        String academicYearId = null;
        if (parameters.containsKey('academicyearid')) {
            setAcademicYearId(parameters.get('academicyearid'));
        } else {
            // [CH] NAIS-1666
            setAcademicYearId(GlobalVariables.getCurrentAcademicYear().Id);
        }
    }
}