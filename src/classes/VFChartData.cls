public without sharing class VFChartData {
    /*Initialization*/
   
    public VFChartData(String name, Integer data1) {
        this.name = name;
        this.data1 = data1;
        
        this.nameShort = setNameShort(this.name);
        this.data1TipValue = setTipLabel(this.data1);
    }
    
    public VFChartData(String name, Integer data1, Integer data2) {
        this.name = name;
        this.data1 = data1;
        this.data2 = data2;
        
        this.nameShort = setNameShort(this.name);
        this.data1TipValue = setTipLabel(this.data1);
    }
    
    public VFChartData(String name, Integer data1, Integer data2, Integer data3) {
        this.name = name;
        this.data1 = data1;
        this.data2 = data2;
        this.data3 = data3;
        
        this.nameShort = setNameShort(this.name);
        this.data1TipValue = setTipLabel(this.data1);
    }
   
  
   
    /*End Initialization*/
   
    /*Properties*/
    public String name { get; set; }
    public String nameShort {get; set;}
    public Integer data1 { get; set; }
    public Integer data2 { get; set; }
    public Integer data3 { get; set; }
    public String data1TipValue {get; set;}
    
    /*End Properties*/
       
    /*Action Methods*/
   
    /*End Action Methods*/
   
    /*Helper Methods*/
    
    public String setTipLabel(Integer i){
        List<String> args = new String[]{'0','number','###,###,##0.00'};
        String s = String.format(i.format(), args);
        s = '$' +s;
        System.debug(s);
        return s;
    }
    
    public String setNameShort(String s){
        Integer maxLength = 20;
        
        if (s.length() < maxLength){
            return s;
        } else {
            return s.left(maxLength) + '...';
        }
    }
   
    /*End Helper Methods*/

    


}