/**
 * ContactDataAccessService.cls
 *
 * @description Data access service class for Contact
 *
 * @author Chase Logan @ Presence PG
 */
public class ContactDataAccessService extends DataAccessService {
    
    /**
     * @description: Queries a Contact record by ID, queries all fields by default because of the direct query of
     * a singe record by ID limited to 1
     *
     * @param contactId - The ID of the Contact record
     *
     * @return A Contact record
     */
    public Contact getContactById( Id contactId) {
        Contact returnVal;

        if ( contactId != null) {

            try {

                returnVal = Database.query( 'select ' + String.join( super.getSObjectFieldNames( 'Contact'), ',') +
                                             ' from Contact' + 
                                            ' where Id = :contactId' + 
                                            ' limit 1');
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in ContactDataAccessService.getContactById' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }

        return returnVal;
    }

    /**
     * @description: Queries Contact Email by Set of Contact ID's, concatenates results into CSV string of
     * email addresses
     *
     * @param contactIdSet - The Set of Contact ID's
     *
     * @return A list of Email addresses
     */
    public List<String> getContactEmailCSVStringByIdSet( Set<String> contactIdSet) {
        List<String> returnList;

        if ( contactIdSet != null) {

            try {

                List<Contact> resultList = Database.query( 'select Id, Name, Email' +
                                                             ' from Contact' + 
                                                           ' where Id in :contactIdSet' +
                                                           ' limit 10000');
                returnList = new List<String>();
                for ( Contact c : resultList) {

                    returnList.add( c.Email.trim());
                }
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in ContactDataAccessService.getContactEmailCSVStringByIdSet' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }

        return returnList;
    }

    /**
     * @description: Queries Contact Email by Set of Contact ID's
     *
     * @param contactIdSet - The Set of Contact ID's
     *
     * @return A list of Contact records
     */
    public List<Contact> getContactEmailByIdSet( Set<String> contactIdSet) {
        List<Contact> returnList;

        if ( contactIdSet != null) {

            try {

                returnList = Database.query( 'select Id, Name, Email' +
                                               ' from Contact' + 
                                             ' where Id in :contactIdSet' +
                                             ' limit 10000');
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in ContactDataAccessService.getContactEmailByIdSet' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }

        return returnList;
    }

    public List<Contact> getContactsByNames( List<String> firstNames, List<String> lastNames, List<Date> birthDates) {
        List<Contact> returnval;

        if( firstNames == null && lastNames == null && birthDates == null) return returnval;

        try {

            returnVal = Database.query( 'select ' + String.join( super.getSObjectFieldNames( 'Contact'), ',') +
                                             ' from Contact' + 
                                            ' where firstname in :firstNames ' +
                                              ' and lastname in :lastNames ' +
                                              ' and birthdate in :birthdates ' +
                                              ' order by CreatedDate desc ');
            System.debug( 'DEBUG:::ContactDataAccessService.getContactsByNames:Error:' + returnVal);
        } catch( Exception e) {

            System.debug( 'DEBUG:::exception in ContactDataAccessService.getContactsByNames:Error:' + e.getMessage());
            throw new DataAccessServiceException( e);
        }

        return returnval;
    }

}