/**
 *    NAIS-69: Document Preview
 *
 *    SL, Exponent Partners, 2013
 */
public with sharing class PreviewPageController {
    public static final boolean UAT = Test.isRunningTest() ? false : Databank_Settings__c.getInstance('Databank').Use_UAT__c;
    public static final SpringCMUAT.SpringCMServiceSoap SPRINGCM_UAT = new SpringCMUAT.SpringCMServiceSoap();
    public static final SpringCMProd.SpringCMServiceSoap SPRINGCM_PROD = new SpringCMProd.SpringCMServiceSoap();
    
    public transient String previewImage {get; private set; }
    public Integer maxPages {get;private set;}
    public Integer currentPage {get; private set;}
    private String token {get;set;}
    private String docid {get;set;}
    
    public boolean isValidPage {get; set;}
    public Family_Document__c theFamilyDocument {get; set;}
    public String errorMessage {get; set;}
    
    public String theZoom {get; set;}
    
    public Boolean previewPending {get;set;}

    public PreviewPageController() {
        isValidPage = true;
        theFamilyDocument = null;
        docid = ApexPages.currentPage().getParameters().get('id');
        
        if (docId == null) {
            errorMessage = 'The document is not currently available for preview.';
            isValidPage = false;
            return;
        }

        if (String.isNotBlank(docid)) {
            for (Family_Document__c fd : [SELECT Id, Import_Id__c, Document_Status__c, Document_Type__c, Document_Year__c, Document_Pertains_to__c
                                            FROM Family_Document__c 
                                            WHERE Import_Id__c = :docid LIMIT 1]) {
                theFamilyDocument = fd;
            }
        }
        
        if (theFamilyDocument == null) {
            errorMessage = 'Unable to load document';
            isValidPage = false;
            return;
        }        
        
        //SpringCMUAT.SpringCMServiceSoap springCM = new SpringCMUAT.SpringCMServiceSoap();
        
        if (!Test.isRunningTest()) {
            try {
                if (UAT) {    
                    Boolean success = false;
                    String lastErrorMSG = '';
                    Databank_Settings__c databankSettings;
                    databankSettings = Databank_Settings__c.getInstance('Databank');

                    Integer tryAttempts = (databankSettings != null && databankSettings.TryAttemptsDocs__c != null) ? Integer.valueOf(databankSettings.TryAttemptsDocs__c) : 5;

                    // [DP] NAIS-2009 -- retrying up to 10 times total (apex limit) to get around the POODLE error
                    for (Integer i = 0; i < tryAttempts; i++){
                        try {    
                            token = SPRINGCM_UAT.AuthenticateSSO(UserInfo.getUsername(),
                                                            UserInfo.getSessionId(),
                                                            databankSettings.API_Partner_Server_URL__c,
                                                            databankSettings.APIKey__c,
                                                            databankSettings.AccountId__c);

                            i = tryAttempts + 1;     // once success occurs we break out of loop
                            success = true;         // and set the booleand to true
                        } catch (Exception e){
                            if (e.getMessage().toLowerCase().contains('handshake')){
                                lastErrorMSG = e.getMessage();
                                SpringCMAction.fakeSleep();
                            } else {
                                throw new SpringCMProd.springCMException(e.getMessage());
                            }
                        }
                    }

                    if (!success){
                        throw new SpringCMProd.springCMException('There was an error authenticating to SpringCM.  Please retry or contact ' 
                            + Label.Company_Name_Full + '.  Error: ' + lastErrorMSG);
                    }

                    success = false;
                    lastErrorMSG = '';
                    SpringCMUAT.SCMDocument document;
                    for (Integer i = 0; i < tryAttempts; i++){
                        try {    
                            document = SPRINGCM_UAT.DocumentGetById(token, docid, false);
                            maxPages = document.PDFPageCount;
                            i = tryAttempts + 1;     // once success occurs we break out of loop
                            success = true;         // and set the booleand to true
                        } catch (Exception e){
                        if (e.getMessage().toLowerCase().contains('handshake')){
                                lastErrorMSG = e.getMessage();
                                SpringCMAction.fakeSleep();
                            } else {
                                throw new SpringCMProd.springCMException(e.getMessage());
                            }
                        }
                    }

                    if (!success){
                        throw new SpringCMProd.springCMException('There was an error retrieving the document from SpringCM.  Please retry or contact ' 
                            + Label.Company_Name_Full + '.  Error: ' + lastErrorMSG);
                    }
                }
                else {
                    Boolean success = false;
                    String lastErrorMSG = '';
                    Databank_Settings__c databankSettings;
                    databankSettings = Databank_Settings__c.getInstance('Databank');

                    Integer tryAttempts = (databankSettings != null && databankSettings.TryAttemptsDocs__c != null) ? Integer.valueOf(databankSettings.TryAttemptsDocs__c) : 5;
                    
                    // [DP] NAIS-2009 -- retrying up to 10 times total (apex limit) to get around the POODLE error
                    for (Integer i = 0; i < tryAttempts; i++){
                        try {    
                            token = SPRINGCM_PROD.AuthenticateSSO(UserInfo.getUsername(),
                                                            UserInfo.getSessionId(),
                                                            databankSettings.API_Partner_Server_URL__c,
                                                            databankSettings.APIKey__c,
                                                            databankSettings.AccountId__c);
                                                                    
                            i = tryAttempts + 1;     // once success occurs we break out of loop
                            success = true;         // and set the booleand to true
                        } catch (Exception e){
                        if (e.getMessage().toLowerCase().contains('handshake')){
                                lastErrorMSG = e.getMessage();
                                SpringCMAction.fakeSleep();
                            } else {
                                throw new SpringCMProd.springCMException(e.getMessage());
                            }
                        }
                    }

                    if (!success){
                        throw new SpringCMProd.springCMException('There was an error authenticating to SpringCM.  Please retry or contact ' 
                            + Label.Company_Name_Full + '.  Error: ' + lastErrorMSG);
                    }

                    success = false;
                    lastErrorMSG = '';
                    SpringCMProd.SCMDocument document;
                    for (Integer i = 0; i < tryAttempts; i++){
                        try {    
                            document = SPRINGCM_PROD.DocumentGetById(token, docid, false);
                            maxPages = document.PDFPageCount;
                            i = tryAttempts + 1;     // once success occurs we break out of loop
                            success = true;         // and set the booleand to true
                        } catch (Exception e){
                        if (e.getMessage().toLowerCase().contains('handshake')){
                                lastErrorMSG = e.getMessage();
                                SpringCMAction.fakeSleep();
                            } else {
                                throw new SpringCMProd.springCMException(e.getMessage());
                            }
                        }
                    }

                    if (!success){
                        throw new SpringCMProd.springCMException('There was an error retrieving the document from SpringCM.  Please retry or contact ' 
                            + Label.Company_Name_Full + '.  Error: ' + lastErrorMSG);
                    }
                
                }
            }
            catch (Exception e) {
                System.debug(LoggingLevel.Error, 'Document Preview Exception: ' + e.getMessage() + ' ' + e.getStackTraceString());
                isValidPage = false;
                errorMessage = 'The document could not be loaded -- error getting token.';
            }
        }
        else {
            maxPages = 2; // unit test value
        }
        
        setPreview(1);
        
    }
    
    public void nextPage(){
        Integer nextPage = currentPage+1;
        if(nextPage<=maxPages){
            setPreview(nextPage);
        }
    }
    
    public void previousPage(){
        Integer previousPage = currentPage-1;
        if(previousPage!=0){
            setPreview(previousPage);
        }
    }
    
    //Here you may have to poll for the preview image and catch errors
    //if there is a page count, but no preview available, SpringCM will create 
    //one, but you have to poll for it to be created
    //Other zooms are available, but it would have to be built in and polled for as well.
    private void setPreview(Integer pageNumber){
        // SpringCMUAT.SpringCMServiceSoap springCM = new SpringCMUAT.SpringCMServiceSoap();
        currentPage = pageNumber;
        if (!Test.isRunningTest()) {
            // use to try smaller zooms if we get a size failure, only some options are available:
            // see http://docsqa.springcm.com/SOAP/v201305/html/T_ADEXS_WebServices_v201305_Common_SCMPreviewImageZoom.htm
            theZoom = 'Percent100';
            
            // used to track if the download failed, most likely because it's too big
            Boolean previousDownloadFailedDueToSize = false;


            
            // try to download at 100%
            try {
                previewPending=false;
                previewImage = doDownload();
            } 
            catch (Exception e) {
                // if image is unavailable we will poll for it with VF Action Poller
                if(e.getMessage().contains('Document preview image unavailable')){
                    previewPending=true;
                // if image failed due to size we will try again at different zoom
                } else if (e.getMessage().contains('exceeds maximum')){
                    previousDownloadFailedDueToSize = true;
                    System.debug('TESTING setting previousDownloadFailedDueToSize');
                }
                System.debug(LoggingLevel.Error, 'Document Preview Exception at 100%: ' + e.getMessage() + ' ' + e.getStackTraceString());
            }
            
            // if previous download failed, try to download at 75%
            if (previousDownloadFailedDueToSize){
                theZoom = 'Percent75';
                previousDownloadFailedDueToSize = false;
                try {
                    previewImage = doDownload();
                } 
                catch (Exception e) {
                    if (e.getMessage().contains('exceeds maximum')){
                        previousDownloadFailedDueToSize = true;
                        System.debug('TESTING setting previousDownloadFailedDueToSize');
                    }
                    System.debug(LoggingLevel.Error, 'Document Preview Exception at 75%: ' + e.getMessage() + ' ' + e.getStackTraceString());
                }
            }
            

            // if previous download failed, try to download at 50%
            if (previousDownloadFailedDueToSize){
                theZoom = 'Percent50';
                previousDownloadFailedDueToSize = false;
                try {
                    previewImage = doDownload();
                } 
                catch (Exception e) {
                    if (e.getMessage().contains('exceeds maximum')){
                        previousDownloadFailedDueToSize = true;
                        System.debug('TESTING setting previousDownloadFailedDueToSize');
                    }
                    System.debug(LoggingLevel.Error, 'Document Preview Exception at 50%: ' + e.getMessage() + ' ' + e.getStackTraceString());
                }
            }
            
            // if previous download failed, try to download at thumbnail level
            if (previousDownloadFailedDueToSize){
                theZoom = 'Thumbnail';
                previousDownloadFailedDueToSize = false;
                try {
                    previewImage = doDownload();
                } 
                // final catch -- if thumbnail failed, throw an error
                catch (Exception e) {
                    System.debug(LoggingLevel.Error, 'Document Preview Exception: ' + e.getMessage() + ' ' + e.getStackTraceString());
                    isValidPage = false;
                    errorMessage = 'The document could not be loaded -- error downloading document.';
                }
            }
                
        }
        else {
            previewImage = 'Test Image Data';
        }
    }
    
    private String doDownload(){
        if (UAT) {
            return SPRINGCM_UAT.DocumentDownloadPreviewImage(token,docid,0,-1,currentPage, theZoom);
        }
        else {
            return SPRINGCM_PROD.DocumentDownloadPreviewImage(token,docid,0,-1,currentPage, theZoom);
        }
    }

    public PageReference imageCheck(){
        setPreview(1);
        return null;
    }
    
}