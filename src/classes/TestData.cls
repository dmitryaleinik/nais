/*
 * Utility methods for use in tests
 *  create<Object>  - return an sObject; insert object if last argument 'insertData' is true
 */
@isTest
public without sharing class TestData {

    /*Initialization*/
       // [CH] NAIS-1766 Adding to help avoid Mixed DML Errors
    public static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

    public testData(){
        // [CH] NAIS-1766 Updated to include two params
        this(true, true);
    }

    // [CH] NAIS-1766 Adding to avoid mixed DML in test methods
    public TestData(Boolean createUser){
        this(true, createUser);
    }

    public testData(Boolean createTables, Boolean createUser){

        family = TestUtils.createAccount('Individual Account', RecordTypes.individualAccountTypeId, 5, false);
        school1 = TestUtils.createAccount('school1', RecordTypes.schoolAccountTypeId, 5, false);
        school1.Alert_Frequency__c = 'Weekly digest of new applications';
        school1.SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest();
        school1.SSS_School_Code__c = '11111';
        school1.Portal_Version__c = SubscriptionTypes.BASIC_TYPE;
        school2 = TestUtils.createAccount('school2', RecordTypes.schoolAccountTypeId, null, false);
        school2.SSS_Subscriber_Status__c = GlobalVariables.getActiveSSSStatusForTest();
        school2.Portal_Version__c = SubscriptionTypes.BASIC_TYPE;
        Account schoolKS = TestUtils.createAccount('Test School KS 1', RecordTypes.schoolAccountTypeId, 3, false);
        schoolKS.Portal_Version__c = SubscriptionTypes.BASIC_TYPE;
        insert new List<Account> { family, school1, school2, schoolKS };

        //NAIS-1738 Start
        sub1 = TestUtils.createSubscription(school1.Id, system.today().addDays(-14), system.today().addYears(1), 'no', 'Basic', false);
        sub2 = TestUtils.createSubscription(school2.Id, system.today().addDays(-14), system.today().addYears(1), 'no', 'Basic', false);
        subKS = TestUtils.createSubscription(schoolKS.Id, system.today().addDays(-14), system.today().addYears(1), 'no', 'Basic', false);
        insert new List<Subscription__c> {sub1, sub2, subKS};
        //NAIS-1738 End

        // Create school custom settings
        SchoolPortalSettings.KS_School_Account_Id = schoolKS.Id;

        staff1 = TestUtils.createContact('Staff 1', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
        staff1.Email = 'staff1@test.org';
        staff1.PFS_Alert_Preferences__c = 'Receive Alerts';

        staff2 = TestUtils.createContact('Staff 2', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
        staff2.Email = 'staff2@test.org';
        staff2.PFS_Alert_Preferences__c = 'Do Not Receive Alerts';

        staff3 = TestUtils.createContact('Staff 3', school1.Id, RecordTypes.schoolStaffContactTypeId, false);
        staff3.Email = 'staff3@test.org';
        staff3.PFS_Alert_Preferences__c = 'Receive Alerts';

        parentA = TestUtils.createContact('Parent A', family.Id, RecordTypes.parentContactTypeId, false);
        parentB = TestUtils.createContact('Parent B', family.Id, RecordTypes.parentContactTypeId, false);
        parentC = TestUtils.createContact('Parent C', family.Id, RecordTypes.parentContactTypeId, false);

        student1 = TestUtils.createContact('Student 1', family.Id, RecordTypes.studentContactTypeId, false);
        student1.Birthdate = Date.today().addDays(-200);
        student2 = TestUtils.createContact('Student 2', family.Id, RecordTypes.studentContactTypeId, false);
        student2.Birthdate = Date.today().addDays(-200);
        student3 = TestUtils.createContact('Student 3', family.Id, RecordTypes.studentContactTypeId, false);
        student3.Birthdate = Date.today().addDays(-200);

        insert new List<Contact> { staff1, staff2, staff3, parentA, parentB, parentC, student1, student2, student3 };

        // [CH] NAIS-1766
        System.runAs(thisUser){
            portalUser = TestUtils.createPortalUser(staff1.LastName, staff1.Email + String.valueOf(Math.random()) + String.valueOf(Math.random()), 'alias', staff1.Id, GlobalVariables.schoolPortalAdminProfileId, true, true);
        }

        // SFP-672 Create Share between portalUser and its school because criteria based sharing rules do not run in
        // unit tests.
        TestUtils.insertAccountShare(staff1.AccountId, portalUser.Id);

        List<Academic_Year__c> academicYears = GlobalVariables.getAllAcademicYears();
        academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        academicYearPastId = GlobalVariables.getPreviousAcademicYear().Id;
        // NAIS-2383 [DP] 04.02.2015 refactor to get name for picklist value
        academicYearName = GlobalVariables.getCurrentAcademicYear().Name;
        academicYearPastName = GlobalVariables.getPreviousAcademicYear().Name;

        if(createTables){

            fedTaxTables = new List<Federal_Tax_Table__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                fedTaxTables.add(TestUtils.createFederalTaxTable(academicYears[i].Id, EfcPicklistValues.FILING_TYPE_INDIVIDUAL_SINGLE, false));
            }
            Database.insert(fedTaxTables);

            stateTaxTables = new List<State_Tax_Table__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                stateTaxTables.add(TestUtils.createStateTaxTable(academicYears[i].Id, false));
            }
            Database.insert(stateTaxTables);

            emplAllowances = new List<Employment_Allowance__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                emplAllowances.add(TestUtils.createEmploymentAllowance(academicYears[i].Id, false));
            }
            Database.insert(emplAllowances);

            busFarm = new List<Net_Worth_of_Business_Farm__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                busFarm.add(TestUtils.createBusinessFarm(academicYears[i].Id, false));
            }
            Database.insert(busFarm);

            retAllowances = new List<Retirement_Allowance__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                retAllowances.add(TestUtils.createRetirementAllowance(academicYears[i].Id, false));
            }
            Database.insert(retAllowances);

            assetProgs = new List<Asset_Progressivity__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                assetProgs.add(TestUtils.createAssetProgressivity(academicYears[i].Id, false));
            }
            Database.insert(assetProgs);

            incProAllowances = new List<Income_Protection_Allowance__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                incProAllowances.add(TestUtils.createIncomeProtectionAllowance(academicYears[i].Id, false));
            }
            Database.insert(incProAllowances);

            expContRates = new List<Expected_Contribution_Rate__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                expContRates.add(TestUtils.createExpectedContributionRate(academicYears[i].Id, false));
            }
            Database.insert(expContRates);

            houseIndexes = new List<Housing_Index_Multiplier__c>{};
            for(Integer i=0; i<academicYears.size(); i++){
                houseIndexes.add(TestUtils.createHousingIndexMultiplier(academicYears[i].Id, false));
            }
            Database.insert(houseIndexes);
        }

        // Create SSSConstants records for the Academic Years
        sssConst1 = TestUtils.createSSSConstants(academicYearId, false);
        sssConst2 = TestUtils.createSSSConstants(academicYearPastId, false);
        Database.insert(new List<SSS_Constants__c>{sssConst1, sssConst2});

        TestUtils.createUnusualConditions(academicYearId, true);

        System.runAs(portalUser){
            pfs1 = TestUtils.createPFS('PFS A', academicYearId, parentA.Id, false);
            pfs1.Original_Submission_Date__c = (Datetime)System.today().addDays(-3);
            pfs1.PFS_Status__c = 'Application Submitted';
            pfs1.Payment_Status__c = 'Unpaid';

            pfs2 = TestUtils.createPFS('PFS B', academicYearId, parentB.Id, false);
            pfs2.Original_Submission_Date__c = (Datetime)System.today().addDays(-5);
            pfs2.PFS_Status__c = 'Application Submitted';
            pfs2.Payment_Status__c = 'Unpaid';

            pfs1Past = TestUtils.createPFS('PFS A Past', academicYearPastId, parentA.Id, false);
            pfs1Past.Original_Submission_Date__c = Date.newInstance(2012, 12, 12);
            pfs1Past.PFS_Status__c = 'Application Submitted';

            pfs2Past = TestUtils.createPFS('PFS A Past', academicYearPastId, parentC.Id, false);
            pfs2Past.Original_Submission_Date__c = Date.newInstance(2012, 12, 12);
            pfs2Past.PFS_Status__c = 'Application Submitted';
            pfs2Past.Fee_Waived__c = 'Yes';
            Dml.WithoutSharing.insertObjects(new List<PFS__c>{ pfs1, pfs2, pfs1Past, pfs2Past });

            // [CH] NAIS-1866 Updated to use the new version of createApplicant that respects the
            //        Contacts's name
            applicant1A = TestUtils.createApplicant(student1, pfs1.Id, false);
            applicant1B = TestUtils.createApplicant(student1, pfs2.Id, false);
            applicant2A = TestUtils.createApplicant(student2, pfs1.Id, false);
            applicant1School2 = TestUtils.createApplicant(student1, pfs1Past.Id, false);
            applicant1PAST = TestUtils.createApplicant(student1, pfs1Past.Id, false);
            applicant2Past = TestUtils.createApplicant(student3, pfs2Past.Id, false);
            Dml.WithoutSharing.insertObjects(new List<Applicant__c> { applicant1A, applicant1B, applicant2A, applicant1PAST, applicant2Past, applicant1School2});

            studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', academicYearId, student1.Id, false);
            studentFolder2 = TestUtils.createStudentFolder('Student Folder 2', academicYearId, student2.Id, false);
            studentFolder1Past = TestUtils.createStudentFolder('Student Folder 1', academicYearPastId, student1.Id, false);
            studentFolder2Past = TestUtils.createStudentFolder('Student Folder 2P', academicYearPastId, student3.Id, false);
            studentFolder1School2 = TestUtils.createStudentFolder('Student Folder 1 School 2', academicYearId, student1.Id, false);
            Dml.WithoutSharing.insertObjects(new List<Student_Folder__c> { studentFolder1, studentFolder2, studentFolder1Past, studentFolder2Past, studentFolder1School2});

            spfsa1 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1A.Id, school1.Id, studentFolder1.Id, false);
            spfsa2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1B.Id, school1.Id, studentFolder1.Id, false);
            spfsa3 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant2A.Id, school1.Id, studentFolder2.Id, false);
            spfsa1Past = TestUtils.createSchoolPFSAssignment(academicYearPastId, applicant1Past.Id, school1.Id, studentFolder1Past.Id, false);
            spfsa2Past = TestUtils.createSchoolPFSAssignment(academicYearPastId, applicant2Past.Id, school1.Id, studentFolder2Past.Id, false);
            spfsa1School2 = TestUtils.createSchoolPFSAssignment(academicYearId, applicant1School2.Id, school2.Id, studentFolder1School2.Id, false);
            // put them in this order so that spfsa1 has earlier created date
            Dml.WithoutSharing.insertObjects(new List<School_PFS_Assignment__c> { spfsa2, spfsa1, spfsa3, spfsa1Past, spfsa2Past, spfsa1School2});
        }

        // Share records to school staff
        Set<Id> spaIds = new Set<Id> {spfsa2.Id, spfsa1.Id, spfsa3.Id, spfsa1Past.Id, spfsa2Past.Id, spfsa1School2.Id};
        List<School_PFS_Assignment__c> spasForSharing = (List<School_PFS_Assignment__c>)Database.query(SchoolStaffShareBatch.query);
        SchoolStaffShareAction.shareRecordsToSchoolUsers(spasForSharing, null);

        // required documents
        rd1 = TestUtils.createRequiredDocument(academicYearId, school1.Id, false);
        rd2 = TestUtils.createRequiredDocument(academicYearId, school1.Id, false);
        insert new List <Required_Document__c> {rd1, rd2};

        // family documents
        fd1 = TestUtils.createFamilyDocument('FamilyDoc1', academicYearId, false);
        fd2 = TestUtils.createFamilyDocument('FamilyDoc2', academicYearId, false);
        insert new List <Family_Document__c> {fd1, fd2};

        // school document assignments
        sda1 = TestUtils.createSchoolDocumentAssignment('sda1', fd1.Id, rd1.Id, spfsa1.Id, false);
        sda2 = TestUtils.createSchoolDocumentAssignment('sda2', fd2.Id, rd2.Id, spfsa1.Id, false);
        insert new List <School_Document_Assignment__c> {sda1, sda2};

        // application fee waivers
        afw1 = TestUtils.createApplicationFeeWaiver(school1.Id, parentA.Id, academicYearId, false);
        afw2 = TestUtils.createApplicationFeeWaiver(school1.Id, parentA.Id, academicYearId, false);
        insert new List <Application_Fee_Waiver__c> {afw1, afw2};

        // Application Fee Settings
        applicationFeeSetting = new Application_Fee_Settings__c(
                Name = pfs1.Academic_Year_Picklist__c,
                Means_Based_Fee_Waiver_Acct_Id__c = school1.Id,
                Maximum_Assets_Waiver__c = 25000,
                Discounted_Amount__c = 48,
                Amount__c = 48,
                Discounted_Product_Code__c = 'Discounted Product Code',
                Product_Code__c = 'Product Code');
        insert applicationFeeSetting;

        // verification
        verification1 = TestUtils.createVerification(false);
        insert verification1;

        dependents1 = new Dependents__c(PFS__c = pfs1.Id, Dependent_1_School_Next_Year__c='schoolnext', Dependent_1_Gender__c='female', Dependent_1_Full_Name__c='full name1', Dependent_1_Current_School__c='sacajawea', Dependent_1_Current_Grade__c='5', Dependent_1_Birthdate__c=System.today(), Dependent_2_School_Next_Year__c='schoolnext', Dependent_2_Gender__c='female', Dependent_2_Full_Name__c='full name2', Dependent_2_Current_School__c='sacajawea', Dependent_2_Current_Grade__c='5', Dependent_2_Birthdate__c=System.today());
        dependents2 = new Dependents__c(PFS__c = pfs2.Id, Dependent_1_School_Next_Year__c='schoolnext', Dependent_1_Gender__c='female', Dependent_1_Full_Name__c='full name1', Dependent_1_Current_School__c='sacajawea', Dependent_1_Current_Grade__c='5', Dependent_1_Birthdate__c=System.today(), Dependent_2_School_Next_Year__c='schoolnext', Dependent_2_Gender__c='female', Dependent_2_Full_Name__c='full name2', Dependent_2_Current_School__c='sacajawea', Dependent_2_Current_Grade__c='5', Dependent_2_Birthdate__c=System.today());
        dependents1Past = new Dependents__c(PFS__c = pfs1Past.Id, Dependent_1_School_Next_Year__c='schoolnext', Dependent_1_Gender__c='female', Dependent_1_Full_Name__c='full name1', Dependent_1_Current_School__c='sacajawea', Dependent_1_Current_Grade__c='5', Dependent_1_Birthdate__c=System.today(), Dependent_2_School_Next_Year__c='schoolnext', Dependent_2_Gender__c='female', Dependent_2_Full_Name__c='full name2', Dependent_2_Current_School__c='sacajawea', Dependent_2_Current_Grade__c='5', Dependent_2_Birthdate__c=System.today());
        insert new List<Dependents__c> { dependents1, dependents2, dependents1Past};

        budget1 = new Budget_Group__c(Name = 'Budget1', School__c = school1.Id, Academic_Year__c = academicYearId, Total_Funds__c = 10000);
        budget2 = new Budget_Group__c(Name = 'Budget2', School__c = school1.Id, Academic_Year__c = academicYearId, Total_Funds__c = 10000);
        budget3 = new Budget_Group__c(Name = 'Budget3', School__c = school1.Id, Academic_Year__c = academicYearId, Total_Funds__c = 10000);
        insert new List<Budget_Group__c> { budget1, budget2, budget3};

    }
    /*End Initialization*/

    /*Properties*/
    public Account family {get; set;}
    public Account school1 {get; set;}
    public Account school2 {get; set;}
    public Contact staff1 {get; set;}
    public Contact staff2 {get; set;}
    public Contact staff3 {get; set;}
    public Contact parentA {get; set;}
    public Contact parentB {get; set;}
    public Contact parentC {get; set;}
    public Contact student1 {get; set;}
    public Contact student2 {get; set;}
    public Contact student3 {get; set;}
    public User portalUser {get; set;}

    public Budget_Group__c budget1 {get; set;}
    public Budget_Group__c budget2 {get; set;}
    public Budget_Group__c budget3 {get; set;}

    public Id academicYearId {get; set;}
    public Id academicYearPastId {get; set;}
    // NAIS-2383 [DP] 04.02.2015 refactor to get name for picklist value
    public String academicYearName {get; set;}
    public String academicYearPastName {get; set;}

    public PFS__c pfs1  {get; set;}
    public PFS__c pfs2 {get; set;}
    public PFS__c pfs1Past {get; set;}
    public PFS__c pfs2Past {get; set;}
    public Applicant__c applicant1A {get; set;}
    public Applicant__c applicant1B {get; set;}
    public Applicant__c applicant2A {get; set;}
    public Applicant__c applicant1School2 {get; set;}
    public Applicant__c applicant1PAST {get; set;}
    public Applicant__c applicant2Past {get; set;}
    public Student_Folder__c studentFolder1 {get; set;}
    public Student_Folder__c studentFolder2 {get; set;}
    public Student_Folder__c studentFolder1Past {get; set;}
    public Student_Folder__c studentFolder2Past {get; set;}
    public Student_Folder__c studentFolder1School2 {get; set;}
    public School_PFS_Assignment__c spfsa1 {get; set;}
    public School_PFS_Assignment__c spfsa2 {get; set;}
    public School_PFS_Assignment__c spfsa3 {get; set;}
    public School_PFS_Assignment__c spfsa1Past {get; set;}
    public School_PFS_Assignment__c spfsa2Past {get; set;}
    public School_PFS_Assignment__c spfsa1School2 {get; set;}
    public School_PFS_Assignment__c spfsa2School2 {get; set;}
    public Required_Document__c rd1, rd2;
    public Family_Document__c fd1, fd2;
    public School_Document_Assignment__c sda1, sda2;
    public Application_Fee_Waiver__c afw1, afw2;
    public Application_Fee_Settings__c applicationFeeSetting;
    public Verification__c verification1;
    public Dependents__c dependents1 {get; set;}
    public Dependents__c dependents2 {get; set;}
    public Dependents__c dependents1Past {get; set;}
    public Subscription__c sub1 {get; set;} //NAIS-1738
    public Subscription__c sub2 {get; set;} // NAIS-1738
    public Subscription__c subKS {get; set;} // NAIS-1738

    public List<Federal_Tax_Table__c> fedTaxTables {get; set;}
    public List<State_Tax_Table__c> stateTaxTables {get; set;}
    public List<Employment_Allowance__c> emplAllowances {get; set;}
    public List<Net_Worth_of_Business_Farm__c> busFarm {get; set;}
    public List<Retirement_Allowance__c> retAllowances {get; set;}
    public List<Asset_Progressivity__c> assetProgs {get; set;}
    public List<Income_Protection_Allowance__c> incProAllowances {get; set;}
    public List<Expected_Contribution_Rate__c> expContRates {get; set;}
    public List<Housing_Index_Multiplier__c> houseIndexes {get; set;}

    // Create SSSConstants records for the Academic Years
    public SSS_Constants__c sssConst1 {get; set;}
    public SSS_Constants__c sssConst2 {get; set;}


    /*End Properties*/

    /*Action Methods*/

    /*End Action Methods*/

    /*Helper Methods*/

    /*End Helper Methods*/
}