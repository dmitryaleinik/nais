/**
 * @description This batch job is used to update student folders where the student lookup field is empty. Student Folders
 *              should never be in this state nevertheless, some records have gotten to this point and need to be fixed.
 *              The records will be fixed by querying School PFS Assignments for a specific academic year where the related
 *              student folder is missing the school. We query for the PFS Assignments because it has the school record
 *              which needs to be populated on the related student folder. The folders will then be updated.
 */
public class UpdateFoldersMissingSchoolBatch implements Database.Batchable<SObject> {

    private static final String ACADEMIC_YEAR_TO_PROCESS_PARAM = 'academicYearToProcess';

    // This variable will store the academic year which folders need to be updated for. We are only allowing one academic
    // year per batch because there may be a lot of records to update per year. Rather than update records across multiple
    // academic years, we will keep the context of the batch focused on one at a time.
    public String AcademicYear { get; private set; }

    private UpdateFoldersMissingSchoolBatch(String academicYearToProcess) {
        ArgumentNullException.throwIfNull(academicYearToProcess, ACADEMIC_YEAR_TO_PROCESS_PARAM);
        AcademicYear = academicYearToProcess;
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        // Query School PFS Assignments for the specified academic year where the related folder is missing a school.
        return Database.getQueryLocator([SELECT Id, School__c, Student_Folder__c, Student_Folder__r.School__c FROM School_PFS_Assignment__c WHERE Academic_Year_Picklist__c = :AcademicYear AND Student_Folder__r.School__c = null ORDER BY School__c]);
    }

    public void execute(Database.BatchableContext bc, List<SObject> scope) {
        List<School_PFS_Assignment__c> pfsAssignments = (List<School_PFS_Assignment__c>)scope;

        Map<Id, Student_Folder__c> foldersToUpdateById = new Map<Id, Student_Folder__c>();

        for (School_PFS_Assignment__c pfsAssignment : pfsAssignments) {
            Student_Folder__c folderToUpdate = new Student_Folder__c(Id = pfsAssignment.Student_Folder__c);
            folderToUpdate.School__c = pfsAssignment.School__c;
            foldersToUpdateById.put(folderToUpdate.Id, folderToUpdate);
        }

        if (!foldersToUpdateById.isEmpty()) {
            update foldersToUpdateById.values();
        }
    }

    public void finish(Database.BatchableContext bc) { }

    public static Id startBatch(String academicYearToProcess) {
        return Database.executeBatch(new UpdateFoldersMissingSchoolBatch(academicYearToProcess));
    }
}