/*
 * Spec-109 School Portal - User Profile;
 * BR, Exponent Partners, 2013
 */

// Not done yet. -brieder030513

@isTest
private class SchoolUserProfileControllerTest
{
    
    private static final Id portalProfileId = [select Id from Profile where Name = :ProfileSettings.SchoolAdminProfileName].Id;
    
    /*Initialization*/
    
    private static User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    
    // normal use case - access page from within School Portal
    @isTest
    private static void initFromWithinSchoolPortal()
    {
        Account school = TestUtils.createAccount('School A', RecordTypes.schoolAccountTypeId, 2, true);
        Contact staff = TestUtils.createContact('Staff A', school.Id, RecordTypes.schoolStaffContactTypeId, true);
        User portalUser = TestUtils.createPortalUser('Staff A', 'sa@test.org', 'sa', staff.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);
        
        System.runAs(thisUser){
            insert portalUser;
        }
        
        System.runAs(portalUser) {
            Test.setCurrentPage(Page.SchoolUserProfile);
            
            ApexPages.StandardController sc = new ApexPages.StandardController(new Account());
            SchoolUserProfileController ext = new SchoolUserProfileController(sc);
            
            // get the school of the current portal user
            System.assertEquals(staff.Id, ext.myContact.Id);
        }
    }
    
    // valid use case? - access page with Id passed in
    @isTest
    private static void initByContactId()
    {
        Account school = TestUtils.createAccount('School A', RecordTypes.schoolAccountTypeId, 2, true);
        Contact staff = TestUtils.createContact('Staff A', school.Id, RecordTypes.schoolStaffContactTypeId, true);
        User portalUser = TestUtils.createPortalUser('Staff A', 'sa@test.org', 'sa', staff.Id, GlobalVariables.schoolPortalAdminProfileId, true, false);
        
        System.runAs(thisUser){
            insert portalUser;
        }

        Test.setCurrentPage(Page.SchoolUserProfile);
        ApexPages.currentPage().getParameters().put('id', staff.Id);
            
        ApexPages.StandardController sc = new ApexPages.StandardController(school);
        SchoolUserProfileController ext = new SchoolUserProfileController(sc);
            
        System.assertEquals(staff.Id, ext.myContact.Id);
    }

    /*Action Methods*/
    @isTest
    private static void testSave()
    {
         SpringCMActionTest.setupCustomSettings(); // these are needed for call to SpringCM with email change of portal user
         SpringCMAction.isTest = true; // set to true so web callout doesn't try to fire
         
        Account school = TestUtils.createAccount('School A', RecordTypes.schoolAccountTypeId, 2, true);
        Contact staff = TestUtils.createContact('Staff A', school.Id, RecordTypes.schoolStaffContactTypeId, true);
        User portalUser = TestUtils.createPortalUser('Staff A', 'sa@test.org', 'sa', staff.Id, portalProfileId, true, false);
        
        System.runAs(thisUser){
            insert portalUser;
        }
        
        System.runAs(portalUser) {
            
            Test.setCurrentPage(Page.SchoolUserProfile);
            
            ApexPages.StandardController ctrlStd = new ApexPages.StandardController(new Contact());
            SchoolUserProfileController ctrlExtn = new SchoolUserProfileController(ctrlStd);
            
            // Change email and save.
            ctrlExtn.myContact.Email = 'test@sa.org';
            PageReference pageRef = ctrlExtn.save();
            
            Contact updatedStaff = [select Email from Contact where Id = :staff.Id];
            
            // verify that edit is saved
            System.assertEquals('test@sa.org', updatedStaff.Email);
            // verify page redirection - should go to portal home?
            //System.assertEquals(Page.SchoolUserProfile, pageRef);
        }
    }
    
    @isTest
    private static void testCancel()
    {
        Account school = TestUtils.createAccount('School A', RecordTypes.schoolAccountTypeId, 2, true);
        Contact staff = TestUtils.createContact('Staff A', school.Id, RecordTypes.schoolStaffContactTypeId, true);
        User portalUser = TestUtils.createPortalUser('Staff A', 'sa@test.org', 'sa', staff.Id, portalProfileId, true, false);
        
        System.runAs(thisUser){
            insert portalUser;
        }
        
        System.runAs(portalUser) {
            
            Test.setCurrentPage(Page.SchoolUserProfile);
            
            ApexPages.StandardController ctrlStd = new ApexPages.StandardController(new Contact());
            SchoolUserProfileController ctrlExtn = new SchoolUserProfileController(ctrlStd);
            
            // Change email and cancel.
            ctrlExtn.myContact.Email = 'test@sa.org';        
            PageReference pageRef = ctrlExtn.cancel();
            
            Contact updatedStaff = [select Email from Contact where Id = :staff.Id];
            
            // verify that edit is not saved
            System.assertEquals(null, updatedStaff.Email);
            // verify page redirection - should go to portal home?
            //System.assertEquals(Page.SchoolUserProfile, pageRef);
        }
    }
}