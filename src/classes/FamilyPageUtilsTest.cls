@isTest
private class FamilyPageUtilsTest 
{
   @isTest
   static void testAllPages() {
        Test.startTest();
        PageReference testPage = Page.FamilyDashboard;        
        Test.setCurrentPage(testPage);
        system.assertEquals('Dashboard', FamilyPageUtils.getFamilyPageTitle(testPage));

        testPage = Page.familydocuments;        
        Test.setCurrentPage(testPage);
        system.assertEquals('My Documents', FamilyPageUtils.getFamilyPageTitle(testPage));    
        Test.stopTest();
    }//End:testPFSPages
}