public with sharing class ExpCore {

    private static Map<String, String[]> fieldNames = new Map<String, String[]>();
    public static String[] GetAllFieldNames(String sObjectTypeName) {
        
        ExpCore.fieldNames = new Map<String, String[]>{}; // NAIS-2497 [CH] Need to clear this between calls otherwise things get duplicated
        
        sObjectTypeName = sObjectTypeName.toLowerCase();
        
        if(!ExpCore.fieldNames.containsKey(sObjectTypeName)) {
            
            Schema.SObjectType t = Schema.getGLobalDescribe().get(sObjectTypeName);
            
            if(t != null) {
                Map<String, Schema.SObjectField> fieldMap = t.getDescribe().fields.getMap();
                
                String[] fields = new List<String>();
                fields.addAll(fieldMap.keySet());
                
                ExpCore.fieldNames.put(sObjectTypeName, fields);
            }
            
        }
        
        return ExpCore.fieldNames.get(sObjectTypeName);
    }
    
    private static Map<String, Map<String, String>> fieldLabels = new Map<String, Map<String, String>>();
    public static Map<String, String> GetAllFieldLabels(String sObjectTypeName) {
        
        ExpCore.fieldLabels = new Map<String, Map<String,String>>{}; // NAIS-2497 [CH] Need to clear this between calls otherwise things get duplicated
        
        sObjectTypeName = sObjectTypeName.toLowerCase();
        
        if(!ExpCore.fieldLabels.containsKey(sObjectTypeName)) {
            
            Schema.SObjectType t = Schema.getGLobalDescribe().get(sObjectTypeName);
            
            if(t != null) {
                
                Map<String, Schema.SObjectField> fieldInfo = t.getDescribe().fields.getMap();
                Map<String, String> labels = new Map<String, String>();
                
                for(String key : fieldInfo.keySet ()) {
                    labels.put(key, fieldInfo.get(key).getDescribe().label);
                }
                
                ExpCore.fieldLabels.put(sObjectTypeName, labels);
            }
            
        }
        
        return ExpCore.fieldLabels.get(sObjectTypeName);
    }
    
       // [CH] Simple conversion from a yes/no string value to Boolean (defaults to false)
    public static Boolean yesNoToBoolean(String yesNoValue){
        return (yesNoValue != null && yesNoValue == 'yes') ? true : false; 
    }
    
    // [CH] Simple conversion from Boolean to yes/no string (defaults to No)
    public static String booleanToYesNo(Boolean boolValue){
        return (boolValue != null && boolValue) ? 'Yes' : 'No'; 
    }
    
    // returns default Decimal if value is null
    public static Decimal defaultForNull(Decimal value, Decimal defaultValue) {
        if (value == null) {
            return defaultValue;
        }
        else {
            return value;
        }
    }
    
    // returns default String if value is null
    public static String defaultForNull(String value, String defaultValue) {
        if (value == null) {
            return defaultValue;
        }
        else {
            return value;
        }
    }
}