/*
 * SPEC-121
 *
 * The school should be able to see all answers provided by the families on the main application screens
 * This is the Household Information link under the PFS as well as the Additional Information link under the PFS.
 * The idea is to show all fields which have not fed into the EFC, but are asked on the PFS so that the school
 * can see everything without needing to go into the printable PFS to see the information.
 *
 * Nathan, Exponent Partners, 2013
*/
public with sharing class SchoolHouseholdMemberController {

    /*Initialization*/

    String studentId;
    static final Integer dependentsPerRecord = 6;    // max dependents in a dependent record
    Integer dependentsCount = 0;    // existing dependents in a dependent record

    public string origFolderDayBoardingValue; // NAIS-2252 [DP] 02.05.2015
    public string origFolderStudentIdValue;
    
    // constructor
    public SchoolHouseholdMemberController() {

        mode = ApexPages.currentPage().getParameters().get('mode');
        schoolPFSAssignmentId = ApexPages.currentPage().getParameters().get('schoolPFSAssignmentId');
        householdMemberId = ApexPages.currentPage().getParameters().get('householdMemberId');
        spfsaSiblingId = ApexPages.currentPage().getParameters().get('spfsaSiblingId');
        dependentsId = ApexPages.currentPage().getParameters().get('dependentsId');
        dependentNumber = ApexPages.currentPage().getParameters().get('dependentNumber');
        relationship = ApexPages.currentPage().getParameters().get('relationship');

        if (mode == 'New') {
            typeSelectRendered = true;
        }

        schoolPFSAssignment = new School_PFS_Assignment__c();
        applicant = new Applicant__c();
        applicantOfSibling = new Applicant__c();
        pfs = new PFS__c();
        memberContact = new Contact();
        student = new Contact();
        dependents = new Dependents__c();

        loadApplicant();
        loadApplicantOfSibling();
        loadLeftNav();
        loadStudent();
        loadPFS();
        loadHouseholdMember();
        loadDependents();
        loadFamilyMemberTypeOption();
    }

    /*End Initialization*/

    /*Properties*/

    // input
    public String mode {get; set;}    // page in new or edit mode
    public String schoolPFSAssignmentId {get; set;}
    public String householdMemberId {get; set;}
    public String spfsaSiblingId {get; set;}    // used for applicant record of sibling
    public String dependentsId {get; set;}
    public String dependentNumber {get; set;}
    public String relationship {get; set;}

    public List <SelectOption> familyMemberTypeOption {get; private set;}
    public String selectedFamilyMemberType {get; set;}
    public School_PFS_Assignment__c schoolPFSAssignment {get; set;}
    public School_PFS_Assignment__c schoolPFSASibling {get; set;}//SFP-165, [G.S]
    public Applicant__c applicant {get; set;}
    public Applicant__c applicantOfSibling {get; set;}
    public Student_Folder__c folderOfSibling {get; set;}
    public Contact memberContact {get; set;}
    public Contact student {get; private set;}
    public Dependents__c dependents {get; private set;}

    public PFS__c pfs {get; set;}
    public Boolean typeSelectRendered {get; private set;}
    public Boolean ksSchoolRendered {get {return GlobalVariables.isKSSchoolUser();} private set;}

    // to pass to left nav
    public Student_Folder__c folder {get; set;}
    public School_PFS_Assignment__c spfs1 {get; set;}
    public School_PFS_Assignment__c spfs2 {get; set;}

    /*End Properties*/

    /*Action Methods*/

    /*Called by Next button when adding a family member*/
    public void nextFamilyMemberType() {

        typeSelectRendered = false;
    }

    /*Called by Cancel button when adding a family member*/
    public PageReference cancelFamilyMemberType() {
        return returnUrl();
    }

    /*Called by Save button when adding or editing a family member of type student, parent A,B or sibling*/
    public PageReference saveMemberContact() {
        String errMsg;
        if(! (selectedFamilyMemberType == 'SIBLING' && mode == 'New') )
        {  
            // create savepoint. everything must be committed together
            Savepoint sp = Database.setSavepoint();
            
            // edit parent B
             try{
                 if (memberContact != null && memberContact.Id != null){
                     update memberContact;
                     
                     // work information fields are from pfs
                     if (pfs != null && pfs.Id != null) {
                         update pfs;
                     }
                 }
                 
                 if(folderOfSibling != null && folderOfSibling.Id != null 
                 && (folderOfSibling.Day_Boarding__c != origFolderDayBoardingValue 
                    || folderOfSibling.Student_ID__c != origFolderStudentIdValue)){
                    update folderOfSibling;
                }
                
                if(selectedFamilyMemberType=='STUDENT' && folder!=null && folder.Id!=null 
                && folder.Student_ID__c != origFolderStudentIdValue){
                    update folder;
                }
                
                // save schoolPFSAssignment: Student lives with comes from this object
                if (schoolPFSAssignment != null && schoolPFSAssignment.Id != null) {
                     update schoolPFSAssignment;
                }
                
                 if (schoolPFSASibling != null && schoolPFSASibling.Id != null) {
                      update schoolPFSASibling;
                 }
                 
                 // save applicant: Lives with field for student is coming from this object
                 if (applicant != null && applicant.Id != null) {
                     update applicant;
                 }
                 
                 // save applicant of sibling
                 if (applicantOfSibling != null && applicantOfSibling.Id != null) {
                     update applicantOfSibling;
                 }
                 
                 // save pfs for Parent A and B
                 if (pfs != null && pfs.Id != null) {
                     update pfs;
                 }
             }catch(Exception e){
                 errMsg = e.getMessage();
                 Database.rollback(sp);
                ErrorMessage(errMsg);
                return null;
             }
        }

        return returnUrl();
    }

    /*Called by Cancel button when adding or editing a family member of type student, parent A,B or sibling*/
    public PageReference cancelMemberContact() {

        return returnUrl();
    }

    /*Called by Save button when adding or editing a family member of type dependent*/
    public PageReference saveDependent() {
        if (dependents.Id != null) {
            // editing dependent
            try {
                Dependents__c dpnts = new Dependents__c();
                dpnts.put('Id', dependents.Id);
                copyDependent(dpnts, Integer.valueOf(dependentNumber), dependents, 1);

                update dpnts;
            } catch (Exception e) {
                ErrorMessage(e.getMessage());
                return null;
            }
        } else {
            // adding new dependent
            if (pfs != null && pfs.Id != null) {
                String selectStr = getDependentsQuery();
                String queryStr = selectStr + ' where PFS__c = \'' + pfs.Id + '\' order by CreatedDate asc';

                List <Dependents__c> depList = Database.Query(queryStr);

                // looks for empty slot
                Integer dependentNumber;
                Dependents__c dpnts;
                if (depList.size() > 0) {
                    dpnts = depList[0];
                    for (Integer i = 1; i <= dependentsPerRecord; i++) {
                        String nameField = 'Dependent_' + i + '_Full_Name__c';
                        if (dpnts.get(nameField) == null) {
                            dependentNumber = i;
                            break;
                        }
                    }
                }

                if (dpnts != null) {
                    // set new dependent at empty slot
                    if (dependentNumber != null) {
                        try {
                            copyDependent(dpnts, Integer.valueOf(dependentNumber), dependents, 1);
                            if (pfs.Number_of_Dependent_Children__c == null) {
                                pfs.Number_of_Dependent_Children__c = dependentNumber;
                                update pfs;
                            }
                            update dpnts;
                        } catch (Exception e) {
                            ErrorMessage(e.getMessage());
                            return null;
                        }
                    } else {
                        ErrorMessage('You cannot create more than ' + dependentsPerRecord + ' dependents');
                        return null;
                    }
                } else {
                    // no dependent record for pfs. create new record
                    try {
                        dependents.PFS__c = pfs.Id;
                        if (pfs.Number_of_Dependent_Children__c == null) {
                            pfs.Number_of_Dependent_Children__c = 1;
                            update pfs;
                        }
                        insert dependents;
                        Dependents__c dep = [select Id, Dependent_1_Full_Name__c from dependents__c where Id = :dependents.Id ];
                    } catch (Exception e) {
                        ErrorMessage(e.getMessage());
                        return null;
                    }
                }
            }
        }

        return returnUrl();
    }

    /*Called by Cancel button when adding or editing a family member of type dependent*/
    public PageReference cancelDependent() {
        return returnUrl();
    }

    /*Called by Save button when adding or editing a family member of type additional parent*/
    public PageReference saveAdditionalParent() {

        try {
            if (pfs != null && pfs.Id != null) {
                pfs.Other_Parent__c = (pfs.Addl_Parent_First_Name__c!=null && pfs.Addl_Parent_First_Name__c!=''?'Yes':'No');
                update pfs;
            }
        } catch (Exception e) {
            ErrorMessage(e.getMessage());
            return null;
        }

        return returnUrl();
    }

    /*Called by Cancel button when adding or editing a family member of type dependent*/
    public PageReference cancelAdditionalParent() {
        return returnUrl();
    }

    /*End Action Methods*/

    /*Helper Methods*/

    private void loadFamilyMemberTypeOption() {

        // This picklist will be dynamic depending on who is already specified
        // There can only be one Parent A, one Parent B and one additional parent.
        // There can be any number of dependents

        familyMemberTypeOption = new List <SelectOption>();

        // always include Parent A in edit mode
        if (mode == 'Edit') {
            familyMemberTypeOption.add(new SelectOption('STUDENT', 'Student'));
            familyMemberTypeOption.add(new SelectOption('PARENT_A', 'Parent A'));
        }
        if (mode == 'New' && pfs != null && pfs.Parent_B__c == null) {
            // NAIS-1804 [dp] remove parent B option
            //familyMemberTypeOption.add(new SelectOption('PARENT_B', 'Parent B'));
        } else if (mode == 'Edit' && pfs != null && pfs.Parent_B__c != null) {
            familyMemberTypeOption.add(new SelectOption('PARENT_B', 'Parent B'));
        }
        if (mode == 'Edit' || (mode == 'New' && pfs != null && pfs.Addl_Parent_Last_Name__c == null)) {
            familyMemberTypeOption.add(new SelectOption('ADDITIONAL_PARENT', 'Additional Parent'));
        }
        // max 6 dependents can be created
        if ((mode == 'New' && dependentsCount < dependentsPerRecord) || mode == 'Edit') {
            familyMemberTypeOption.add(new SelectOption('DEPENDENT', 'Dependent (non-applicant)'));
        }
        // sibling cannot be added by school. only can edit
        if (mode == 'Edit') {
            familyMemberTypeOption.add(new SelectOption('SIBLING', 'Sibling Applicant'));
        }
        // household member without any relationship. contact is having same household id
        if (mode == 'Edit') {
            familyMemberTypeOption.add(new SelectOption('HOUSEHOLD_MEMBER', 'Household Member'));
        }

        // init
        if (mode == 'Edit') {
            if (relationship == 'Student') {
                selectedFamilyMemberType = 'STUDENT';
            } else if (relationship == 'Sibling') {
                selectedFamilyMemberType = 'SIBLING';
            } else if (relationship == 'Parent A') {
                if (pfs != null && pfs.Parent_A__c != null) {
                    selectedFamilyMemberType = 'PARENT_A';
                }
            } else if (relationship == 'Parent B') {
                if (pfs != null && pfs.Parent_B__c != null) {
                    selectedFamilyMemberType = 'PARENT_B';
                }
            } else if (relationship == 'Dependent') {
                if (dependentsId != null && dependentsId != '') {
                    selectedFamilyMemberType = 'DEPENDENT';
                }
            } else if (relationship == 'Additional Parent') {
                if (pfs != null && pfs.Id != null) {
                    selectedFamilyMemberType = 'ADDITIONAL_PARENT';
                }
            } else if (relationship == 'Household Member') {
                if (memberContact != null && memberContact.Id != null) {
                    selectedFamilyMemberType = 'HOUSEHOLD_MEMBER';
                }
            }
        }
    }

    private void loadStudent() {

        if (applicant != null && applicant.Contact__c != null) {
            List <Contact> studList = [select Id, Name,
                                       Household__c
                                       from Contact where Id = :applicant.Contact__c];
            if (studList.size() > 0) {
                student = studList[0];
                studentId = student.Id;
            }
        }
    }

    private void loadApplicant() {

        if (schoolPFSAssignmentId != null && schoolPFSAssignmentId != '') {
            List <School_PFS_Assignment__c> spfsaList = [select Id, Name,
                                            Applicant__c,
                                            School__c, School__r.Name,
                                            Student_Folder__c,
                                            Student_Lives_With__c,
                                            Grade_Applying__c,
                                            Student_Folder__r.Day_Boarding__c,
                                            Day_Boarding__c,
                                            Applicant__r.Current_School__c
                                            from School_PFS_Assignment__c where Id = :schoolPFSAssignmentId];
            if (spfsaList.size() > 0) {
                schoolPFSAssignment = spfsaList[0];
                List <Applicant__c> appList = [select Id, Name,
                                               Address_Not_Known__c,
                                               Age__c,
                                               Applicant_Court_Guardianship__c,
                                               Applicant_Parents_Deceased__c,
                                               Applicant_Receiving_Welfare_Benefits__c,
                                               Birth_Parents_Included_in_Application__c,
                                               Birth_Parents_Marital_Status__c,
                                               Birthdate_new__c,
                                               Contact__c,
                                               Current_Grade__c,
                                               Current_School__c,
                                               Eligible_for_KS_K_12_Financial_Aid__c,
                                               Eligible_for_KS_Preschool_Financial_Aid__c,
                                               First_Name__c,
                                               Gender__c,
                                               Kamehameha_ID__c,
                                               KS_K_12_Financial_Aid_Day_Boarding__c,
                                               Kipona_Scholarship_Program_Schools__c,
                                               Last_Name__c,
                                               Lives_with__c,
                                               Middle_Initial__c,
                                               Other_Birth_Parent_City__c,
                                               Other_Birth_Parent_Country__c,
                                               Other_Birth_Parent_First_Name__c,
                                               Other_Birth_Parent_Last_Name__c,
                                               Other_Birth_Parent_MI__c,
                                               Other_Birth_Parent_Phone__c,
                                               Other_Birth_Parent_State_Province__c,
                                               Other_Birth_Parent_Street_Address__c,
                                               Other_Birth_Parent_ZIP_Postal_Code__c,
                                               Pauahi_Keiki_Scholars_Program_Schools__c,
                                               Parent_A_Relationship_to_Applicant__c,
                                               Parent_B_Relationship_to_Applicant__c,
                                               Parent_in_Civil_Union_Partnership__c,
                                               PFS__c,
                                               Received_K_12_Financial_Aid_Before__c,
                                               Received_Kipona_Scholarship_Before__c,
                                               Received_Pauahi_Keiki_Scholarship_Before__c,
                                               Received_Preschool_Financial_Aid_Before__c,
                                               Suffix__c

                                               from Applicant__c where Id = :schoolPFSAssignment.Applicant__c];
                if (appList.size() > 0) {
                    applicant = appList[0];
                }
            }
        }
    }

    private void loadApplicantOfSibling() {

        if (spfsaSiblingId != null && spfsaSiblingId != '') {
            List <School_PFS_Assignment__c> spfsaList = [select Id, Name,
                                            Applicant__c,
                                            Applicant__r.Age__c,
                                            Applicant__r.First_Name__c,
                                            Applicant__r.Gender__c,
                                            Applicant__r.Last_Name__c,
                                            Applicant__r.Middle_Initial__c,
                                            Applicant__r.Suffix__c,
                                            Applicant__r.Birthdate_new__c,
                                            Student_Folder__c,
                                            Student_Folder__r.Id,
                                            Student_Folder__r.Day_Boarding__c,
                                            Student_Folder__r.Student_ID__c, 
                                            Grade_Applying__c,
                                            Applicant__r.Current_School__c
                                            from School_PFS_Assignment__c where Id = :spfsaSiblingId];

            if (spfsaList.size() > 0) {
                schoolPFSASibling = spfsaList[0];//SFP-165, [G.S]
                applicantOfSibling = spfsaList[0].Applicant__r;
                folderOfSibling = spfsaList[0].Student_Folder__r; // NAIS-2252 [DP] 02.05.2015
                origFolderDayBoardingValue = folderOfSibling.Day_Boarding__c; // NAIS-2252 [DP] 02.05.2015
                origFolderStudentIdValue = folderOfSibling.Student_ID__c;
            }
        }
    }

    private void loadLeftNav() {

        if (schoolPFSAssignment != null) {
            folder = GlobalVariables.getFolderAndSchoolPFSAssignments(schoolPFSAssignment.Student_Folder__c);
            if (folder.School_PFS_Assignments__r.size() > 0) {
                spfs1 = folder.School_PFS_Assignments__r.size() > 0 ? folder.School_PFS_Assignments__r[0] : null;
                spfs2 = folder.School_PFS_Assignments__r.size() > 1 ? folder.School_PFS_Assignments__r[1] : null;
            }
        }
    }

    private void loadPFS() {
        if (applicant != null && applicant.PFS__c != null) {
            List <PFS__c> pfsList = [select Id, Name,
                                     Addl_Parent_City__c,
                                     Addl_Parent_Country__c,
                                     Addl_Parent_First_Name__c,
                                     Addl_Parent_Joint_custody__c,
                                     Addl_Parent_Last_Name__c,
                                     Addl_Parent_Middle_Initial__c,
                                     Add_l_Parent_Phone__c,
                                     Addl_Parent_Relationship__c,
                                     Addl_Parent_State_Province__c,
                                     Addl_Parent_Street_Address__c,
                                     Addl_Parent_Suffix__c,
                                     Addl_Parent_ZIP_Postal_Code__c,
                                     Agree_to_KS_Legal_Terms__c,
                                     Parent_A__c,
                                     Parent_A_Add_l_Job_Descrip__c,
                                     Parent_A_Address__c,
                                     Parent_A_Age__c,
                                     Parent_A_Birthdate__c,
                                     Parent_A_City__c,
                                     Parent_A_Country__c,
                                     Parent_A_Email__c,
                                     Parent_A_Employer__c,
                                     Parent_A_First_Name__c,
                                     Parent_A_Gender__c,
                                     Parent_A_Home_Phone__c,
                                     Parent_A_Last_Name__c,
                                     Parent_A_Middle_Name__c,
                                     Parent_A_Mobile_Phone__c,
                                     Parent_A_Occupation__c,
                                     Parent_A_Preferred_Phone__c,
                                     Parent_A_Prefix__c,
                                     Parent_A_State__c,
                                     Parent_A_Suffix__c,
                                     Parent_A_Title__c,
                                     Parent_A_Work_Phone__c,
                                     Parent_A_Years_Worked__c,
                                     Parent_A_ZIP_Postal_Code__c,
                                     Parent_B__c,
                                     Parent_B_Add_l_Job_Descrip__c,
                                     Parent_B_Address__c,
                                     Parent_B_Age__c,
                                     Parent_B_Birthdate__c,
                                     Parent_B_City__c,
                                     Parent_B_Country__c,
                                     Parent_B_Email__c,
                                     Parent_B_Employer__c,
                                     Parent_B_First_Name__c,
                                     Parent_B_Home_Phone__c,
                                     Parent_B_Gender__c,
                                     Parent_B_Last_Name__c,
                                     Parent_B_Middle_Name__c,
                                     Parent_B_Mobile_Phone__c,
                                     Parent_B_Occupation__c,
                                     Parent_B_Preferred_Phone__c,
                                     Parent_B_Prefix__c,
                                     Parent_B_State__c,
                                     Parent_B_Suffix__c,
                                     Parent_B_Title__c,
                                     Parent_B_Work_Phone__c,
                                     Parent_B_Years_Worked__c,
                                     Parent_B_ZIP_Postal_Code__c,
                                     Year_of_divorce__c,
                                     Number_of_Dependent_Children__c

                                     from PFS__c
                                     where Id = :applicant.PFS__c];
            if (pfsList.size() > 0) {
                pfs = pfsList[0];
            }
        }
    }

    private void loadHouseholdMember() {

        if (householdMemberId != null && householdMemberId != '') {
            List <Contact> conList = [select Id, Name,

                                      Birthdate,
                                      Current_Age__c,
                                      Email,
                                      FirstName,
                                      Gender__c,
                                      HomePhone,
                                      LastName,
                                      MailingCity,
                                      MailingCountry,
                                      MailingPostalCode,
                                      MailingState,
                                      MailingStreet,
                                      Middle_Name__c,
                                      MobilePhone,
                                      Phone,
                                      RecordTypeId,
                                      Salutation,
                                      SSN__c,
                                      Suffix__c

                                      from Contact where Id = :householdMemberId];

            if (conList.size() > 0) {
                memberContact = conList[0];
            }
        }
    }

    private void loadDependents() {

        // find how many empty slots are available
        // this is used to show/hide new dependent option
        // currently max 6 dependents can be created

        dependentsCount = 0;

        if (pfs != null && pfs.Id != null) {
            String selectStr = 'select Id, Name, ';
            for (Integer i = 1; i <= dependentsPerRecord; i++) {
                selectStr += 'Dependent_' + i + '_Full_Name__c, ';
            }
            selectStr += 'PFS__c from Dependents__c ';
            String queryStr = selectStr + ' where PFS__c = \'' + pfs.Id + '\'';
            List <Dependents__c> dpntList = Database.Query(queryStr);
            if (dpntList.size() > 0) {
                Dependents__c dpnt = dpntList[0];
                for (Integer i = 1; i <= dependentsPerRecord; i++) {
                    String nameField = 'Dependent_' + i + '_Full_Name__c';
                    if (dpnt.get(nameField) != null) {
                        dependentsCount++;
                    }
                }
            }
        }

        // load dependent for edit

        dependents = new Dependents__c();

        if (dependentsId != null && dependentsId != '' && dependentNumber != null && dependentNumber != '') {
            if (dependentNumber.isNumeric() && Integer.valueOf(dependentNumber) > 0 &&
                    Integer.valueOf(dependentNumber) <= dependentsPerRecord) {

                String selectStr = getDependentsQuery();
                String queryStr = selectStr + ' where Id = \'' + dependentsId + '\'';
                List <Dependents__c> dpntList = Database.Query(queryStr);
                if (dpntList.size() > 0) {
                    Dependents__c dpnt = dpntList[0];

                    dependents.put('Id', dpnt.get('Id'));
                    copyDependent(dependents, 1, dpnt, Integer.valueOf(dependentNumber));
                }
            }
        }
    }

    public PageReference returnUrl() {
        PageReference returnPageRef;
        String retURL = new PageReference(ApexPages.currentPage().getURL()).getParameters().get('retURL');
        if (retURL != null) {
            returnPageRef = new PageReference(retURL);
        }
        return returnPageRef;
    }

    private String getDependentsQuery() {

        String fieldStr = '';
        Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Dependents__c.fields.getMap();
        for (Schema.SObjectField field : fieldMap.values()) {
            Schema.DescribeFieldResult dfr = field.getDescribe();
            if (dfr.isAccessible() == true) {
                if (fieldStr == '') {
                    fieldStr = dfr.getName();
                } else {
                    fieldStr += ', ' + dfr.getName();
                }
            }
        }

        String query = 'select ' + fieldStr + ' from Dependents__c ';

        return query;
    }

    private void copyDependent(Dependents__c toDep, Integer toDepNum, Dependents__c fromDep, Integer fromDepNum) {

        toDep.put('Dependent_' + toDepNum + '_Birthdate__c', fromDep.get('Dependent_' + fromDepNum + '_Birthdate__c'));
        toDep.put('Dependent_' + toDepNum + '_Current_Grade__c', fromDep.get('Dependent_' + fromDepNum + '_Current_Grade__c'));
        toDep.put('Dependent_' + toDepNum + '_Current_School__c', fromDep.get('Dependent_' + fromDepNum + '_Current_School__c'));
        toDep.put('Dependent_' + toDepNum + '_Fin_Aid_Sources_Current__c', fromDep.get('Dependent_' + fromDepNum + '_Fin_Aid_Sources_Current__c'));
        //toDep.put('Dependent_' + toDepNum + '_Fin_Aid_Sources_Est__c', fromDep.get('Dependent_' + fromDepNum + '_Fin_Aid_Sources_Est__c')); FIELD IS OBSOLETE, DELETED [dp] 8.23.13
        toDep.put('Dependent_' + toDepNum + '_Full_Name__c', fromDep.get('Dependent_' + fromDepNum + '_Full_Name__c'));
        toDep.put('Dependent_' + toDepNum + '_Gender__c', fromDep.get('Dependent_' + fromDepNum + '_Gender__c'));
        toDep.put('Dependent_' + toDepNum + '_Grade_Next_Year__c', fromDep.get('Dependent_' + fromDepNum + '_Grade_Next_Year__c'));
        toDep.put('Dependent_' + toDepNum + '_Loan_Sources_Current__c', fromDep.get('Dependent_' + fromDepNum + '_Loan_Sources_Current__c'));
        toDep.put('Dependent_' + toDepNum + '_Loan_Sources_Est__c', fromDep.get('Dependent_' + fromDepNum + '_Loan_Sources_Est__c'));
        toDep.put('Dependent_' + toDepNum + '_Other_Source_Description__c', fromDep.get('Dependent_' + fromDepNum + '_Other_Source_Description__c'));
        toDep.put('Dependent_' + toDepNum + '_Other_Source_Sources_Current__c', fromDep.get('Dependent_' + fromDepNum + '_Other_Source_Sources_Current__c'));
        toDep.put('Dependent_' + toDepNum + '_Other_Source_Sources_Est__c', fromDep.get('Dependent_' + fromDepNum + '_Other_Source_Sources_Est__c'));
        toDep.put('Dependent_' + toDepNum + '_Parent_Sources_Current__c', fromDep.get('Dependent_' + fromDepNum + '_Parent_Sources_Current__c'));
        toDep.put('Dependent_' + toDepNum + '_Parent_Sources_Est__c', fromDep.get('Dependent_' + fromDepNum + '_Parent_Sources_Est__c'));
        toDep.put('Dependent_' + toDepNum + '_School_Next_Year__c', fromDep.get('Dependent_' + fromDepNum + '_School_Next_Year__c'));
        toDep.put('Dependent_' + toDepNum + '_School_Next_Year_Type__c', fromDep.get('Dependent_' + fromDepNum + '_School_Next_Year_Type__c'));
        toDep.put('Dependent_' + toDepNum + '_Student_Sources_Current__c', fromDep.get('Dependent_' + fromDepNum + '_Student_Sources_Current__c'));
        toDep.put('Dependent_' + toDepNum + '_Student_Sources_Est__c', fromDep.get('Dependent_' + fromDepNum + '_Student_Sources_Est__c'));
        toDep.put('Dependent_' + toDepNum + '_Tuition_Cost_Current__c', fromDep.get('Dependent_' + fromDepNum + '_Tuition_Cost_Current__c'));
        toDep.put('Dependent_' + toDepNum + '_Tuition_Cost_Est__c', fromDep.get('Dependent_' + fromDepNum + '_Tuition_Cost_Est__c'));
    }

    public void ErrorMessage(String msgStr) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msgStr));
    }

    /*End Helper Methods*/
}