@isTest
private class InternalEfcCalcStatusControllerTest {
    
    @isTest
    private static void testInternalEfcCalcStatusControllerForRecalculateBatchStatus() {
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
        createTestData(EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE_BATCH);
        Test.setCurrentPage(Page.InternalEfcCalculationStatus);
        
        // instantiate the controller
        InternalEfcCalculationStatusController controller = new InternalEfcCalculationStatusController();
        
        // check that there are records to recalculate
        System.assertNotEquals(0, controller.batchRecalcCountSSS);
        System.assertNotEquals(0, controller.batchRecalcCountRevision);
        
        // verify initiating batch jobs
        controller.recalcSSSForRecalculateBatchStatus();
        System.assertNotEquals(null, controller.batchInstanceId);
        
        controller.recalcRevisionForRecalculateBatchStatus();
        System.assertNotEquals(null, controller.batchInstanceId);
        
        controller.recalcAllForRecalculateBatchStatus();
        System.assertNotEquals(null, controller.batchInstanceId);
    }
    
    @isTest
    private static void testInternalEfcCalcStatusControllerForRecalculateStatus() {
        // disable the automatic efc calc
        EfcCalculatorAction.setIsDisabledRevisionAutoCalc(true);
        EfcCalculatorAction.setIsDisabledSssAutoCalc(true);
        createTestData(EfcPicklistValues.EFC_CALC_STATUS_RECALCULATE);
        
        // set up test data
        Test.setCurrentPage(Page.InternalEfcCalculationStatus);
        
        // instantiate the controller
        InternalEfcCalculationStatusController controller = new InternalEfcCalculationStatusController();
        
        // check that there are records to recalculate
        System.assertNotEquals(0, controller.recalcCountSSS);
        System.assertNotEquals(0, controller.recalcCountRevision);
        
        // verify initiating batch jobs
        controller.recalcSSSForRecalculateStatus();
        System.assertNotEquals(null, controller.batchInstanceId);
        
        controller.recalcRevisionForRecalculateStatus();
        System.assertNotEquals(null, controller.batchInstanceId);
    }

    private static void createTestData(String pfsSpfsaStatusToSet) {
        // set pfs and school pfs assignment calc status to Recalculate
        AcademicYearTestData.Instance.insertAcademicYears(6);
        List<Academic_Year__c> academicYears = GlobalVariables.getAllAcademicYears();
        Academic_Year__c currentAcademicYear = GlobalVariables.getCurrentAcademicYear();
        Id academicYearPastId = GlobalVariables.getPreviousAcademicYear().Id;
        PFS__c pfs1 = PfsTestData.Instance
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forSssEfcCalcStatus(pfsSpfsaStatusToSet)
            .asSubmitted().DefaultPfs;
        School_PFS_Assignment__c spfsa1 = SchoolPfsAssignmentTestData.Instance
            .forAcademicYearPicklist(currentAcademicYear.Name).DefaultSchoolPfsAssignment;

        Contact parent2 = ContactTestData.Instance
            .asParent().insertContact();
        PFS__c pfs2 = PfsTestData.Instance
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forParentA(parent2.Id)
            .asSubmitted().insertPfs();
        Applicant__c applicant2 = ApplicantTestData.Instance
            .forPfsId(pfs2.Id)
            .forContactId(ApplicantTestData.Instance.DefaultApplicant.Contact__c).insertApplicant();
        School_PFS_Assignment__c spfsa2 = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicant2.Id)
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .forRevisionEfcCalcStatus(pfsSpfsaStatusToSet).insertSchoolPfsAssignment();
    }
}