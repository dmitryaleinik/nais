public without sharing class SchoolDashboardBudgetTinyPie {

        public Budget_Group__c bg {get; set;}
        public List<VFChartData> budgetPieData {get; set;}
        public Integer unallocated {get; set;}
        public Integer totalAllocated {get; set;}
        public Integer totalFunds {get; set;}
        
        public SchoolDashboardBudgetTinyPie(Budget_Group__c bgParam){
            bg = bgParam;
            unallocated = bg.Total_Remaining__c == null ? 0 : Integer.valueOf(bg.Total_Remaining__c);
            unallocated = Math.max(unallocated, 0);
            totalAllocated = bg.Total_Allocated__c == null ? 0 : Integer.valueOf(bg.Total_Allocated__c);
            totalFunds = bg.Total_Funds__c == null ? 0 : Integer.valueOf(bg.Total_Funds__c);

            budgetPieData = new List<VFChartData>();
            budgetPieData.add(new VFChartData('Unallocated', unallocated));
            budgetPieData.add(new VFChartData('Total Allocated', totalAllocated));
            
        }
}