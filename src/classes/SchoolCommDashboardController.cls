/**
 * SchoolCommDashboardController.cls
 *
 * @description: Server side controller for Communications Dashboard functionality, handles interactions w/ data model and view state manipulation.
 *
 * @author: Chase Logan @ Presence PG
 */
public class SchoolCommDashboardController implements SchoolAcademicYearSelectorInterface {

    /* member vars and properties */
    public Boolean massEmailEnabled {

        get {
            // Handle app enabled/disabled
            return MassEmailUtil.isMassEmailGloballyEnabledByAccount( 
                        GlobalVariables.getCurrentSchoolId(), UserInfo.getProfileId());
        }
        private set;
    }
    
    // default ctor
    public SchoolCommDashboardController() {
        academicyearid = System.currentPagereference().getParameters().get('academicyearid');
    }

    // Launch Mass Email Dashboard
    public PageReference launchMassEmail() {

        PageReference pageRef = Page.SchoolMassEmailDashboard;
        pageRef.setRedirect( true);        
        pageRef = addAcademicYearParamToPageRef(pageRef);
        
        return pageRef;
    }
    
    private PageReference addAcademicYearParamToPageRef(PageReference pageRef) {
        
        if (academicyearid!=null) {
            
            pageRef.getParameters().put('academicyearid', academicyearid);
        } else {
            
            pageRef.getParameters().put('academicyearid',  GlobalVariables.getCurrentAcademicYear().Id);
        }
        
        return pageRef;
    }
    
    // Launch Parent Messages Dashboard
    public PageReference launchParentMessages() {

        PageReference pageRef = Page.SchoolListMessage;
        pageRef.setRedirect( true);   
        pageRef = addAcademicYearParamToPageRef(pageRef);

        return pageRef;
    }

    /* 
     * Solely for supporting header and academic year selector, copied from existing code 
     */
       public Id academicyearid;
    public Academic_Year__c currentAcademicYear { get; private set; }

    public SchoolCommDashboardController Me {
        get { return this; }
    }
    public String getAcademicYearId() {
        return academicyearid;
    }
    public void setAcademicYearId( String academicYearIdParam) {
        academicyearid = academicYearIdParam;
        currentAcademicYear = GlobalVariables.getAcademicYear( academicyearid);
    }
    // on change of academic year, reload page with new folder
    public PageReference SchoolAcademicYearSelector_OnChange( Boolean saveRecord) {
        PageReference newPage = new PageReference( ApexPages.currentPage().getUrl());
        newPage.getParameters().put( 'academicyearid', getAcademicYearId());
        newPage.setRedirect( true);
        return newPage;
    }
    // Due to the selector_onchange not refreshing properly, we redirect to the same page with academicyearid parameter.
    public void loadAcademicYear() {
        Map<String, String> parameters = ApexPages.currentPage().getParameters();
        String academicYearId = null;
        if ( parameters.containsKey( 'academicyearid'))
            setAcademicYearId(parameters.get( 'academicyearid'));
        else {
            setAcademicYearId( GlobalVariables.getCurrentAcademicYear().Id);
        }
    }
    /* end header support */
    
}