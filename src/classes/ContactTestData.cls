/**
 * @description This class is used to create Contact records for unit tests.
 */
@isTest
public class ContactTestData extends SObjectTestData {
    @testVisible private static final String LAST_NAME = 'Contact Name';

    /**
     * @description Get the default values for the Contact object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Contact.LastName => LAST_NAME,
                Contact.Household__c => HouseholdTestData.Instance.DefaultHousehold.Id
        };
    }

    /**
     * @description Set the Record Type Id to be for a School Staff member.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData asSchoolStaff() {
        return (ContactTestData) with(Contact.RecordTypeId, getRecordTypeId('School Staff'));
    }

    /**
     * @description Set the Record Type Id to be for a Parent/Guardian.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData asParent() {
        return (ContactTestData) with(Contact.RecordTypeId, getRecordTypeId('Parent/Guardian'));
    }

    /**
     * @description Set the Record Type Id to be for a Student.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData asStudent() {
        return (ContactTestData) with(Contact.RecordTypeId, getRecordTypeId('Student'));
    }

    /**
     * @description Set the FirstName of the contact record.
     * @param firstName The first name to set on the current contact record.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData forFirstName(String firstName) {
        return (ContactTestData) with(Contact.FirstName, firstName);
    }

    /**
     * @description Set the LastName of the contact record.
     * @param lastName The last name to set on the current contact record.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData forLastName(String lastName) {
        return (ContactTestData) with(Contact.LastName, lastName);
    }

    /**
     * @description Set the Email of the contact record.
     * @param email The email address to set on the current contact record.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData forEmail(String email) {
        return (ContactTestData) with(Contact.Email, email);
    }

    /**
     * @description Set the BirthDate of the contact record.
     * @param birthDate The BirthDate to set on the current contact record.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData forBirthDate(Date birthDate) {
        return (ContactTestData) with(Contact.Birthdate, birthDate);
    }

    /**
     * @description Set the MailingCountry of the contact record.
     * @param mailingCountry The Mailing Country to set the current contact
     *             record to.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData forMailingCountry(String mailingCountry) {
        return (ContactTestData) with(Contact.MailingCountry, mailingCountry);
    }

    /**
     * @description Set the RecordTypeId of the contact record.
     * @param recordTypeId The Record Type Id to set the current contact
     *             record to.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData forRecordTypeId(Id recordTypeId) {
        return (ContactTestData) with(Contact.RecordTypeId, recordTypeId);
    }

    /**
     * @description Set the AccountId of the contact record.
     * @param accountId The Account Id to define on the current contact
     *             record.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData forAccount(Id accountId) {
        return (ContactTestData) with(Contact.AccountId, accountId);
    }

    /**
     * @description Set the Ethnicity__c of the contact record.
     * @param ethnicity The ethnicity to set the current contact record to.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData forEthnicity(String ethnicity) {
        return (ContactTestData) with(Contact.Ethnicity__c, ethnicity);
    }

    /**
     * @description Set the Security_Question_1__c of the contact record.
     * @param secQuestion1 The security question 1 for this Contact.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData forSecQuestion1(String secQuestion1) {
        return (ContactTestData)with(Contact.Security_Question_1__c, secQuestion1);
    }

    /**
     * @description Set the Security_Question_2__c of the contact record.
     * @param secQuestion2 The security question 2 for this Contact.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData forSecQuestion2(String secQuestion2) {
        return (ContactTestData)with(Contact.Security_Question_2__c, secQuestion2);
    }

    /**
     * @description Set the Security_Question_3__c of the contact record.
     * @param secQuestion3 The security question 3 for this Contact.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData forSecQuestion3(String secQuestion3) {
        return (ContactTestData)with(Contact.Security_Question_3__c, secQuestion3);
    }

    /**
     * @description Set the Security_Answer_1__c of the contact record.
     * @param secQuestionAnswer1 The security question answer 1 for this Contact.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData forSecQuestionAnswer1(String secQuestionAnswer1) {
        return (ContactTestData)with(Contact.Security_Answer_1__c, secQuestionAnswer1);
    }

    /**
     * @description Set the Security_Answer_2__c of the contact record.
     * @param secQuestionAnswer2 The security question answer 2 for this Contact.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData forSecQuestionAnswer2(String secQuestionAnswer2) {
        return (ContactTestData)with(Contact.Security_Answer_2__c, secQuestionAnswer2);
    }

    /**
     * @description Set the Security_Answer_3__c of the contact record.
     * @param secQuestionAnswer3 The security question answer 3 for this Contact.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData forSecQuestionAnswer3(String secQuestionAnswer3) {
        return (ContactTestData)with(Contact.Security_Answer_3__c, secQuestionAnswer3);
    }

    /**
     * @description Set the Household__c of the contact record.
     * @param householdId The household this contact belongs to.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData forHousehold(Id householdId) {
        return (ContactTestData)with(Contact.Household__c, householdId);
    }

    /**
     * @description Set the Recalc_Sharing_For_Contact_Merge__c of the contact record.
     * @param recalcSharingForContactMerge The flag to determine recalculate sharing or not.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData forRecalcSharingForContactMerge(Boolean recalcSharingForContactMerge)
    {
        return (ContactTestData)with(Contact.Recalc_Sharing_For_Contact_Merge__c, recalcSharingForContactMerge);
    }

    /**
     * @description Set the RavennaId__c field of the contact record.
     * @param ravennaId The ravennaId.
     * @return The current instance of ContactTestData.
     */
    public ContactTestData withRavennaId(String ravennaId) {
        return (ContactTestData)with(Contact.RavennaId__c, ravennaId);
    }

    /**
     * @description Insert the current working Contact record.
     * @return The currently operated upon Contact record.
     */
    public Contact insertContact() {
        return (Contact)insertRecord();
    }

    /**
     * @description Create the current working Contact record without resetting
     *             the stored values in this instance of ContactTestData.
     * @return A non-inserted Contact record using the currently stored field
     *             values.
     */
    public Contact createContactWithoutReset() {
        return (Contact)buildWithoutReset();
    }

    /**
     * @description Create the current working Contact record.
     * @return The currently operated upon Contact record.
     */
    public Contact create() {
        return (Contact)super.buildWithReset();
    }

    /**
     * @description The default Contact record.
     */
    public Contact DefaultContact {
        get {
            if (DefaultContact == null) {
                DefaultContact = createContactWithoutReset();
                insert DefaultContact;
            }
            return DefaultContact;
        }
        private set;
    }

    /**
     * @description Get the Contact SObjectType.
     * @return The Contact SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Contact.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static ContactTestData Instance {
        get {
            if (Instance == null) {
                Instance = new ContactTestData();
            }
            return Instance;
        }
        private set;
    }

    private ContactTestData() { }
}