/**
 * BaseIntegrationApiService.cls
 *
 * @description: 
 *
 * @author: Mike Havrila @ Presence PG
 */

public abstract class BaseIntegrationApiService {
    
    private final String LOOPKUP_TYPE = 'LOOKUP';
    private final String ID_TYPE = 'ID';
    private final String NUMBER_TYPE = 'NUMBER';
    private final String BOOLEAN_TYPE = 'BOOLEAN';
    private final String PICKLIST_TYPE = 'PICKLIST';
    private final String DATE_TYPE = 'DATE';

    public class BaseIntegrationApiServiceException extends Exception {}
    public class IntegrationException extends Exception {}

    /**
     * @description: get Integration Mapping Vlaues and return a Map
     * 
     * @param source - The source name of the Auth Provider. 
     *
     * @param provierType - The Provder type to retreive the auth token (Open Id Connect);
     * 
     * @return An Auth Token from Auth Provider. 
     */
    public String getAuthToken( String source, String providerType) {

        String returnVal; 
        if( source == null && providerType == null) return null;

        try {

           Id authId = [select Id 
                          from AuthProvider 
                         where DeveloperName = :source 
                         limit 1].Id;

           returnVal = Auth.AuthToken.getAccessToken( authId, providerType);
        } catch ( Exception e) {

            System.debug('DEBUG:::IntegrationService.getAuthToken Error: ' + e.getMessage());
        }
        
        return returnVal;
    }

    public Object getObjectFromHttpRequest( String httpEndpoint, Map<String, String> httpHeaders, String httpBody, String httpMethod) {

        Object returnVal;
        try {

            String httpResponse = HttpService.sendHttpRequest( httpEndpoint, httpHeaders, httpBody, HttpService.HTTP_GET);

            if( httpResponse != null) {

                returnVal  = (Object)JSON.deserializeUntyped( httpResponse);
            }
            if( returnVal != null && returnVal instanceof List<Object>) {

                returnVal = (List<Object>)JSON.deserializeUntyped( httpResponse);
            } else if( returnVal != null && returnVal instanceof Map<String, Object> ) {

                returnVal = (Map<String, Object>)JSON.deserializeUntyped( httpResponse);
            }
            return returnVal;
        } catch (Exception e) {

            throw new IntegrationException( 'Exception Making webservice callout' + e);
        }
    }

    public String sendHttpRequest( String httpEndpoint, Map<String, String> httpHeaders, String httpBody, String httpMethod) {
        String returnVal = '';

        if ( !String.isEmpty( httpEndpoint)) {

            try {

                // set request endpoint and method
                HttpRequest httpReq = new HttpRequest();
                httpReq.setEndpoint( httpEndpoint);
                //httpReq.setBody( ( !String.isEmpty( httpBody) ? httpBody : ''));
                // handle any headers present
                if ( httpHeaders != null && httpHeaders.size() > 0) {
                    for ( String s : httpHeaders.keySet()) httpReq.setHeader( s, httpHeaders.get( s));
                }
                httpReq.setMethod( 'GET');

                System.debug('DEBUG:::sendHttpRequest11' + httpReq);

                // execute request and return response body
                Http http = new Http();
                HttpResponse httpResp = http.send( httpReq);

                returnVal = httpResp.getBody();
            } catch ( Exception e) {

                // log exception but re-throw to caller for handling
                System.debug( 'DEBUG:::Exception in HttpService.sendHttpRequest, message: '
                        + e.getMessage());
                //throw new HttpServiceException( e);
            }
        }

        return returnVal;
    }

    public List<sObject> mapStudentsToApplicant( List<Object> dataToMap, List<IntegrationMappingModel> applicantMapping) {
        System.debug('DEBUG::: BaseIntegrationService.mapStudentsToApplicant' + applicantMapping);
        String targetObject = FamilyIntegrationModel.APPLICANT_OBJECT;
        String sourceObject = FamilyIntegrationModel.STUDENTS_TYPE;
        List<sObject> returnVal =  this.mapRecords( dataToMap, applicantMapping, targetObject, sourceObject);
        System.debug('DEBUG::: BaseIntegrationService.mapStudentsToApplicant' + returnVal);
        return  returnVal;
    }

    protected List<sObject> mapRecords( List<Object> dataToMap, List<IntegrationMappingModel> mappings, String targetObject, String sourceObject) {

        List<sObject> recordsToCreate = new List<sObject>();
        System.debug('DEBUG::: mapping' + mappings);
        for( Object source : dataToMap) {
            
            // create a new instance of the targetObject   
            sObject newObject = Schema.getGlobalDescribe().get( targetObject).newSObject();

            Map<String, Object> sourceValues = ( Map<String, Object>)source;
            for( IntegrationMappingModel mapping : mappings) {
                System.debug('DEBUG::: mapping' + sourceValues);
                System.debug('DEBUG::: mapping' + sourceValues.get( mapping.sourceField));



                Object tempValue;
                if( !mapping.isSourceComponded) {


                    System.debug('DEBUG::: mapping' + sourceValues.get( mapping.sourceField));
                    // This handles source fields that have more than once value;
                    for(String splitSource : mapping.sourceField.split(',')) {

                        if( sourceValues.get( splitSource) != null){
                            tempValue = sourceValues.get( splitSource);
                            break;
                        } else {
                            tempValue = this.buildParams( sourceValues, mapping.sourceField);
                        }
                    }

                    // there may be source objects mapped to multiple sf objects, processed separately
                    if( mapping.targetObject == targetObject && tempValue != null) {
                        if( mapping.dataType == LOOPKUP_TYPE) {

                            newObject.put( mapping.targetField.split(':')[0], tempValue);
                        } else if( mapping.dataType == ID_TYPE) {

                            // if this is an id, treat it as an external id from a spefic external system : prepend this source
                            newObject.put( mapping.targetField, String.valueOf( tempValue));
                        } else if( mapping.dataType == NUMBER_TYPE) {
                            // implement me

                        } else if( mapping.dataType == BOOLEAN_TYPE) {
                            // implement me

                        } else if( mapping.dataType == PICKLIST_TYPE) {

                            //String mappedValue = mapPicklist(mapping.sourceField__c, string.valueOf(tempValue) );
                            newObject.put( mapping.targetField, String.valueOf( tempValue));
                        } else if( mapping.dataType == DATE_TYPE) {

                            String sourceDate = String.valueOf( tempValue);
                            Date d = Date.newInstance(Integer.valueOf( sourceDate.split('-')[0]), Integer.valueOf( sourceDate.split('-')[1]), Integer.valueOf( sourceDate.split('-')[2]));
                            newObject.put(mapping.targetField, d) ;
                        } else {

                            newObject.put(mapping.targetField, tempValue);
                        }
                    }
                }
            }
            recordsToCreate.add( newObject);
        }


        // do lookups if necessary
        for( IntegrationMappingModel mapping : mappings) {

          if( mapping.dataType == LOOPKUP_TYPE && mapping.targetObject == targetObject) {

                //recordsToCreate = this.lookupRecords( recordsToCreate, mapping);
            }  
        }

        return recordsToCreate;
    }

    private String buildParamsReturnVal;
    private String buildParams( Map<String, Object> response, String param) {

        List<String> splitParams = new List<String>();
        if( param.indexOf('.') > 0) {

            splitParams = param.split('\\.');
        }
        // dot notation. currentSchoolApplications.school.schoolID
        //String jsonInput = '{\n' + ' "folioId" :"111111", \n' + ' "currentSchoolApplications" :  [{ "id":"11111", "school": { "schoolId" : "12345", "ncesId" : "99999" } } ]  \n' '}';
        if( param.indexOf('.') > 0 && response.get( splitParams[0]) != null) {

            Object responseItem = response.get( splitParams.get(0));
            if( responseItem instanceof List<Object>) {

                List<Object> items = ( List<Object>)responseItem;
                for( Object item : items) {

                    Map<String, Object> i = (Map<String, Object>)item;
                    splitParams.remove(0);
                    this.buildParams( i, String.join(splitParams, '.'));
                }
            } else {

                Map<String, Object> i = (Map<String, Object>)responseItem;
                splitParams.remove(0);
                this.buildParams( i, String.join(splitParams, '.'));
            }
        } else if( param.indexOf('.') == -1 && response.get( param) != null) {
            buildParamsReturnVal =  (String)response.get( param);
        }

        return buildParamsReturnVal;
    }
}