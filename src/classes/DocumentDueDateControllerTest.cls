/**
 * DocumentDueDateControllerTest.cls
 *
 * @description: Test class for DocumentDueDateConntroller.cls
 *
 * @author: Mike havrilla @ Presence PG
 */

@isTest
private class DocumentDueDateControllerTest {
    private static Account testAccount;
    private static PFS__c testPFS;

    // setup test data
    static {
        // Create an Account with an active SSS subscriber status
        testAccount = TestUtils.createAccount('Test Account', RecordTypes.individualAccountTypeId, 5, true);
        Subscription__c testSubscribe = TestUtils.createSubscription(testAccount.Id, Date.today().addDays(-5), Date.today().addDays(40), 'No', 'Full', true);
        
        Family_Portal_Settings__c famPortalSettings = new Family_Portal_Settings__c(Name='Family', Individual_Account_Owner_Id__c=UserInfo.getUserId());
        insert famPortalSettings;

        SchoolPortalSettings.KS_School_Account_Id = testAccount.Id;

        Document_Type_Settings__c typeSettings1 = new Document_Type_Settings__c(Name='1040', Full_Type_Name__c='1040', Index_Order__c=0, Tax_Document__c = true);
        Document_Type_Settings__c typeSettings2 = new Document_Type_Settings__c(Name='W2', Full_Type_Name__c='W2', Index_Order__c=1, Tax_Document__c = true);
        Document_Type_Settings__c typeSettings3 = new Document_Type_Settings__c(Name='Some Other Document', Full_Type_Name__c='Some Other Document', Index_Order__c=2, Tax_Document__c = true);
        Document_Type_Settings__c typeSettings4 = new Document_Type_Settings__c(Name='School/Organization Other Document', Full_Type_Name__c='School/Organization Other Document', Category__c = 'School-Specific Document', Index_Order__c=2, Tax_Document__c = false);
        
        Database.insert(new List<Document_Type_Settings__c> {typeSettings1, typeSettings2, typeSettings3, typeSettings4});
        
        Academic_Year__c testAcademicYear = TestUtils.createAcademicYear(GlobalVariables.getCurrentYearString(), true);
        Academic_Year__c testPreviousYear = TestUtils.createAcademicYear(GlobalVariables.getPreviousYearString(), true);
        
        Household__c testHousehold = TestUtils.createHousehold('Test family', true);

        // Create annual Settings
        Annual_Setting__c testAnnualSetting = TestUtils.createAnnualSetting(testAccount.Id, testAcademicYear.Id, false);
        testAnnualSetting.Current_Year_Tax_Documents_Deadline_Type__c = 'No Fixed Deadlines';
        testAnnualSetting.Prior_Year_Tax_Documents_Deadline_Type__c = 'No Fixed Deadlines';
        testAnnualSetting.Current_Year_Tax_Documents_New__c = Date.today().addDays(-2);
        testAnnualSetting.Current_Year_Tax_Documents_Returning__c = Date.today().addDays(-2);
        testAnnualSetting.Prior_Year_Tax_Documents_New__c = Date.today().addDays(-3);
        testAnnualSetting.Prior_Year_Tax_Documents_Returning__c = Date.today().addDays(-3);
        insert testAnnualSetting;

        Contact testParentA = TestUtils.createContact('Parent A', testAccount.Id, RecordTypes.parentContactTypeId, false);
        testParentA.Household__c = testHousehold.Id;
        insert testParentA;

        Contact testAppContact = TestUtils.createContact('Applicant 1', testAccount.Id, RecordTypes.studentContactTypeId, true);

        testPFS = TestUtils.createPFS('Test PFS', testAcademicYear.Id, testParentA.Id, true);
        Applicant__c testApplicant = TestUtils.createApplicant(testAppContact.Id, testPFS.Id, true);
        Student_Folder__c testStudentFolder = TestUtils.createStudentFolder('Test Folder', testAcademicYear.Id, testAppContact.Id, true);

        School_PFS_Assignment__c testSPA = TestUtils.createSchoolPFSAssignment(testAcademicYear.Id, testApplicant.Id, testAccount.Id, testStudentFolder.Id, true);
        
    }

    @isTest
    private static void loadDocumentDueDates_DueDatesCreated_DueDatesPopulated() {
        // Arrange
        // Nothing to Arrange, done in the static block. 

        //Act 
        Test.startTest();
            DocumentDueDateController controller = new DocumentDueDateController();
            controller.pfs = testPfs;
            List<DocumentDueDateController.DocDueDateRow> dueDates = controller.documentDueDates;
        Test.stopTest();

        //Assert
        System.debug('DEBUG:::testDocumentDueDateController' + dueDates.get(0));
        String testCurrentYearDates = Date.today().addDays(-2).format();
        System.assertEquals(testCurrentYearDates, dueDates.get(0).newCurrent, 'The Current Year Tax Documents for New Students Match');
        System.assertEquals(testCurrentYearDates, dueDates.get(0).returningCurrent, 'The Current Year Tax Documents for Returning Students Match');

        String testPreviousYearDates = Date.today().addDays(-3).format();
        System.assertEquals(testPreviousYearDates, dueDates.get(0).newPrevious, 'The Previous Year Tax Documents for New Students Match');
        System.assertEquals(testPreviousYearDates, dueDates.get(0).returningPrevious, 'The Previous Year Tax Documents for Returning Students Match');

        System.assertEquals(testAccount.Name, dueDates.get(0).schoolName, 'The school names match for this record');
    
    }

    @isTest
    private static void loadDocumentDueDates_DueDatesCreated_HeaderPropertyDisabled() {
        // Arrange
        // Nothing to Arrange Done in the static block

        //Act 
        Test.startTest();
            DocumentDueDateController controller = new DocumentDueDateController();
            controller.pfs = testPfs;
            controller.showHeader = false;
            String headerClass = controller.headerClass;
            List<DocumentDueDateController.DocDueDateRow> dueDates = controller.documentDueDates;
        Test.stopTest();

        //Assert
        System.assert(!controller.showHeader, 'Show Header is disabled');

    }
}