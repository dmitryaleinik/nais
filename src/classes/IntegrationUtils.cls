/**
 * IntegrationUtils.cls
 *
 * @description: Utility Class for all Integration Processes.
 *
 * @author: Mike Havrilla @ Presence PG
 */

public class IntegrationUtils {

    // These fields are used to generate keys for identifying applicants.
    private static final List<String> APPLICANT_COMPOUND_KEYS = new List<String> { 'first_name__c', 'last_name__c', 'birthdate_new__c'};

    /**
     * @description:  Recursively Iterates over Map, to search for a dot notation param
     *
     * @param Map<String, Object> objectToSearch - object to iterate over. Can have nested objects
     *
     * @param String param: param to get. (I.e. currentSchoolApplications.school.schoolId)
     *
     * @return returns the objectValue. (i.e. schoolid value)
     */
    public static Object getValuesFromObject( Map<String, Object> objectToSearch, String param) {
        Object objectValuesTemp;
        List<String> splitParams = new List<String>();
        final String DELIMETER = '.';
        if( param.indexOf( DELIMETER) > 0) {

            splitParams = param.split('\\.');
        }

        // dot notation. currentSchoolApplications.school.schoolID
        // String jsonInput = '{\n' + ' "folioId" :"111111", \n' + ' "currentSchoolApplications" :  [{ "id":"11111", "school": { "schoolId" : "12345", "ncesId" : "99999" } }, { "id":"11111", "school": { "schoolId" : "12345", "ncesId" : "123456" } } ]  \n' '}';
        if( param.indexOf( DELIMETER) > 0 && objectToSearch.get( splitParams[0]) != null) {

            Object responseItem = objectToSearch.get( splitParams.get(0));
            if( responseItem instanceof List<Object>) {

                // If we have a nested array, we need to loop over that array and return a list of object
                // That allow us to cast later, but we need them to create mutliple records.
                List<Object> items = ( List<Object>)responseItem;
                splitParams.remove(0);
                objectValuesTemp = IntegrationUtils.getValuesFromObjectList( items, String.join(splitParams, '.'));
            } else {

                // If it's just a map, we're good and continue just grabbing the correct values
                // and recursively calling again.
                Map<String, Object> i = (Map<String, Object>)responseItem;
                splitParams.remove(0);
                return IntegrationUtils.getValuesFromObject( i, String.join(splitParams, '.'));
            }
        } else if( param.indexOf( DELIMETER) == -1 && objectToSearch.get( param) != null) {
            objectValuesTemp =  objectToSearch.get( param);
        }

        return objectValuesTemp;
    }


    /**
     * @description:  Recursively Iterates over a lIst of objects, based on dot notation prarams.
     * If they are multiple levels deep it calls getValuesFromObject with the correct map and
     * that will recursively call itself until the values are found.
     *
     * @param List<Object> objectToSearch - List of objects to iterate over. Can have nested objects
     *
     * @param String param: param to get. (I.e. currentSchoolApplications.school.schoolId)
     *
     * @return returns the objectValue. (i.e. schoolid value)
     */

    public static Object getValuesFromObjectList( List<Object> objectToSearch, String param) {
        List<Object> newItems = new List<Object>();
        List<String> splitParams = new List<String>();

        for( Object item : objectToSearch) {

            Map<String, Object> i = (Map<String, Object>)item;
            newItems.add( IntegrationUtils.getValuesFromObject( i, param));
        }

        return newItems;
    }

    /**
     * @description:  Queries account sObject to with a list of School ids.
     *
     * @param String fieldToCheck: field to validate against on the Account Object
     *
     * @param PFS__c pfs: used to create current annual settings incase they were not created.
     *
     * @return Map<String, sObject> collection of account, and IntegrationId__c field.  (i.e. schoolid value)
     */
    public static Map<String, sObject> checkForValidSchools( String fieldToCheck, List<String> schoolIds, PFS__c pfs) {
        Map<String, sObject> returnVal = new Map<String, sObject>();

        String query = 'select Id, Name, ' + fieldToCheck;
        query += ' from Account where ' + fieldToCheck;
        query += ' in :schoolIds and SSS_Subscriber_Status__C = \'Current\'';

        for( sObject result : Database.query(query)) {

            returnVal.put( (String)result.get('IntegrationId__c'), result);
            returnVal.put( (String)result.get('Id'), result);

            // Check to make sure there are Annual Settings on the school : Creating annual settings if they do not exist
            if( Test.isRunningTest()) {

                GlobalVariables.getCurrentAnnualSettings( true, null, GlobalVariables.getAcademicYearByName( '2017-2018').Id, (Id)result.get('Id'));
            } else {

                GlobalVariables.getCurrentAnnualSettings( true, null, GlobalVariables.getAcademicYearByName( pfs.Academic_Year_Picklist__c).Id, (Id)result.get('Id'));
            }
        }

        return returnVal;
    }

    /**
     * @description For each of the current subscriber schools, make sure they have annual settings. We do this by
     *              iterating over the schools and calling GlobalVariables.getCurrentAnnualSettings() This is not
     *              bulkified at all so we do SOQL/DML in a for loop here. This should be refactored to handle this in bulk.
     */
    public static void ensureSchoolsHaveAnnualSettings(IntegrationApiModel.ApiState apiState, List<Account> schoolsToCheck) {
        if (apiState == null || apiState.pfs == null || schoolsToCheck == null) {
            return;
        }

        for (Account schoolAccount : schoolsToCheck) {
            // Check to make sure there are Annual Settings on the school : Creating annual settings if they do not exist
            if( Test.isRunningTest()) {
                GlobalVariables.getCurrentAnnualSettings( true, null, GlobalVariables.getAcademicYearByName('2017-2018').Id, schoolAccount.Id);
            } else {

                GlobalVariables.getCurrentAnnualSettings( true, null, GlobalVariables.getAcademicYearByName(apiState.pfs.Academic_Year_Picklist__c).Id, schoolAccount.Id);
            }
        }
    }

    /**
     * @description: Splits a colon and space delimited field, returns the last instances
     *
     * @param String lookupFieldToSplit - field to split ( i.e. School__c:Account Integration__c)
     *
     * @return String split value. (i.e. Integration__c)
     */
    public static String splitLookupField( String lookupFieldToSplit) {
        return lookupFieldToSplit.split(':')[1].split(' ')[1];
    }

    /**
     * @description:  Creates a collection of Fields with external Id field.
     *
     * @param fieldsToCompound - list of fields to compound
     *
     * @param sObjects - list of sobjects to loop over
     *
     * @param idField - idField to update.
     *
     * @return returns a map of compound key to obj field.
     */
    public static Map<String, String> getCompoundKeysToIdField( List<String> fieldsToCompound, List<sObject> sObjects, String idField) {

        Map<String, String> returnVal = new Map<String, String>();
        for( sObject obj : sObjects) {

            String key = IntegrationUtils.getCompoundKey( obj, fieldsToCompound);
            returnVal.put( key, (String)obj.get( idField));
        }

        // the compoundKey will be used to get the external id from first/last/bday later
        return returnVal;
    }

    /**
     * @description: Returns a compounded key, a list of fields to compound.
     *
     * @param sObject obj: sobject to get values from.
     *
     * @param List<String> fieldsToCompound: list of fields to use to compound
     *
     * @return String: returns a compounded key, with a delimiter. i.e.( Test%Student)
     */
    public static String getCompoundKey( sObject obj, List<String> fieldsToCompound) {
        String returnVal;

        List<String> keyToCompound = new List<String>();
        for( String fieldToCompound : fieldsToCompound) {

            if( obj.get( fieldToCompound) instanceof Date) {

                keyToCompound.add( String.valueOf( obj.get( fieldToCompound)));
            } else {

                keyToCompound.add((String) obj.get(fieldToCompound));
            }
        }

        returnVal = IntegrationUtils.buildCompoundKey(keyToCompound, '%');

        return returnVal;
    }

    /**
     * @description: Iterates over a list of values and joins them together.
     *
     * @param List<String> stringsToCompound: list of strings to join together.
     *
     * @param String separator: delimeter to use with String.format
     *
     * @return String: returns a compounded key, with a delimiter. i.e.( Test%Student)
     */
    public static String buildCompoundKey( List<String> stringsToCompound, String separator) {

        if( ( stringsToCompound == null || stringsToCompound.size() == 0) && separator == null ) return null;

        List<String> placeholders = new List<String>();
        for( Integer i = 0; i < stringsToCompound.size(); i++) {

            if( stringsToCompound.get(i) != null) placeholders.add( '{' + i + '}');
        }

        String joinedPlaceholders = String.join( placeholders, separator);
        return String.format(joinedPlaceholders, stringsToCompound);
    }

    /**
     * @description: Subtracts a year from the current picklist to use in api call.
     * Use this if the api calls school year is one behind SSS.
     *
     * @param Pfs__c pfs
     *
     * @return returns an academic year - 1 (2016-2017 retuns 2015-2016)
     */
    public static String getAdmissionYear( Pfs__c pfs) {

        List<String> admissionYears = pfs.Academic_Year_Picklist__c.split('-');
        Integer yearStart = Integer.valueOf( admissionYears[0]) - 1;
        Integer yearEnd = Integer.valueOf( admissionYears[1]) - 1;

        return String.valueOf( yearStart + '-' + yearEnd);
    }

    /**
     * @description: Returns a page preference to start SSO and oauth process.
     * Use this if the api calls school year is one behind SSS.
     *
     * @param String integrationSource: gets the integration source from Auth Provider SSO Url
     *
     * @param String retUrl: oauth return url.
     *
     * @return PageReference
     */
    public static PageReference kickoffSSO( String integrationSource, String retUrl) {

        if (!Test.isRunningTest()) {

            string ssoURL = Auth.AuthConfiguration.getAuthProviderSsoUrl( Site.getbasesecureurl(), retUrl, integrationSource);
            string regUrl = ssoUrl.replace('sso', 'oauth');
            return new PageReference(regUrl);
        } else {

            return null;
        }
    }

    /**
     * @description: Logs a user out based on integrationSource, provider and redirectURL.
     *
     * @param String integrationSource: the integration source from Auth.Providers source
     *
     * @param String providerType: Auth.Provider Provider Type (sao, 'Open Id Connect').
     *
     * @param String redirectUrl: url to redirect to on logo.
     *
     * @return a new page Reference.
     */
    public static PageReference logout( String integrationSource, String providerType, String redirectUrl) {
        String currentToken;
        if (!Test.isRunningTest()) {

            Id authId = [
                    select Id
                    from AuthProvider
                    where DeveloperName = :providerType
                    limit 1
            ].Id;

            // Due to limiations of the SAO oauth, we are storing the access token as userId:authToken
            // so we must split it here
            String authTokenUserId = Auth.AuthToken.getAccessToken( authId, providerType);
            if( authTokenUserId.indexOf( ':') > 0) {

                List<String> splitAccessToken = authTokenUserId.split(':');
                currentToken = splitAccessToken[1];
            }

            //TODO::
            // redirectUrI currently can only handle ONE query string parameter. This is a limitation of SAO logout and APIGEE.
            String endpoint = 'https://ssatbdev-test.apigee.net/oauth/endsession?access_token=' + currentToken + '&post_logout_redirect_uri=' + redirectUrl;
            return new PageReference( endpoint);
        } else {

            return null;
        }
    }

    /**
     * @description: Logs a user out based on integrationSource, provider and redirectURL.
     *
     * @param String integrationSource: the integration source from Auth.Providers source
     *
     * @param String providerType: Auth.Provider Provider Type (sao, 'Open Id Connect').
     *
     * @param String redirectUrl: url to redirect to on logo.
     *
     * @return a new page Reference.
     */

    public static String getUserAuthToken( String integrationSource, String providerType) {

        String authToken;

        if( !Test.isRunningTest()) {

            Id authId = [
                    select Id
                    from AuthProvider
                    where DeveloperName = :integrationSource
                    limit 1 ].Id;

            authToken = Auth.AuthToken.getAccessToken( authId, providerType);
        } else {

            if( integrationSource == 'sao') {
                authToken = 'TestUser:TestToken';
            } else {
                authToken = 'TestToken';
            }
        }

        return authToken;
    }

    public static String splitSAOAuthToken( String authToken) {

        if( authToken != null && authToken.indexOf( ':') > 0) {

            List<String> splitAccessToken = authToken.split(':');
            authToken = splitAccessToken[1];
        }

        return authToken;
    }

    /**
     * @description: Wrapper method that kicks off creating Applicants. Students and SPFS Assignments
     *
     * @param: apiState - A Current Instance of IntegrationApiModel.ApiState
     */
    public static void handleIntegrationDML(IntegrationApiModel.ApiState apiState) {
         
         handleIntegrationDML(apiState, FamilyIntegrationModel.STUDENT_APPLICATION);
    }

    public static void handleIntegrationDML(IntegrationApiModel.ApiState apiState, String sourceIntegrationModel) {

        IntegrationUtils.createApplicants(apiState);
        IntegrationUtils.createStudentFolders(apiState, sourceIntegrationModel);
        IntegrationUtils.createStudentPFSAssignments(apiState, sourceIntegrationModel);
    }

    /**
     * @description: Maps collection of student id's to sobject based on external id and upserts to Applicant__c
     *
     * @param: apiState - A Current Instance of IntegrationApiModel.ApiState
     */
    private static void createApplicants( IntegrationApiModel.ApiState apiState) {

        List<String> firstNames = new List<String>();
        List<String> lastNames = new List<String>();
        List<Date> birthdates = new List<Date>();
        Map<String, String> studentKeyToIdMap = new Map<String, String>();

        // Loop over all applicants and build out query string params.
        for( String studentId : apiState.familyModel.studentIdToApplicantMap.keySet()) {
            String firstName = (String)apiState.familyModel.studentIdToApplicantMap.get( studentId).get( 'first_name__c');
            String lastName = (String)apiState.familyModel.studentIdToApplicantMap.get( studentId).get( 'last_name__c');
            Date birthdate = (Date)apiState.familyModel.studentIdToApplicantMap.get( studentId).get( 'birthdate_new__c');

            firstNames.add( firstName);
            lastNames.add( lastName);
            birthdates.add( birthdate);

            // update the object.break;
            List<String> nameList = new List<String> { firstName, lastName, String.valueOf( birthdate)};
            studentKeyToIdMap.put( IntegrationUtils.buildCompoundKey(nameList, '%'), studentId);
        }

        String applicantExternalIdField;
        for( IntegrationMappingModel mapping : apiState.familyModel.mappingModelBySourceMap.get( FamilyIntegrationModel.STUDENTS_TYPE)) {

            if(mapping.dataType == FamilyIntegrationModel.ID_FIELD && mapping.targetObject == FamilyIntegrationModel.APPLICANT_OBJECT) {

                applicantExternalIdField = mapping.targetField;
            }
        }

        // SOQL find existing applicants.
        Set<String> studentsToIgnore = new Set<String>();
        Map<String, String> applicantNameKeyMap = new Map<String, String>();

        List<sObject> applicants = new ApplicantDataAccessService().getApplicantsByNames( firstNames, lastNames, birthDates, apiState.familyModel.studentIdToApplicantMap.keySet(), applicantExternalIdField, apiState.pfs.Id);
        for ( sObject applicant : applicants) {
            // SFP-1551 Before all of this name/birthday key matching we first see if the applicant has an external Id matching one from studentIdToApplicantMap.
            // If it does, we know to override the applicant in the studentIdToApplicantMap so that the existing record in our system gets updated.
            // This should handle a scenario where a returning family who used the integration framework changed the name in the other system.
            String applicantExternalId = String.valueOf(applicant.get(applicantExternalIdField));
            if (String.isNotBlank(applicantExternalId) && apiState.familyModel.studentIdToApplicantMap.containsKey(applicantExternalIdField)) {
                apiState.familyModel.studentIdToApplicantMap.put(applicantExternalId, applicant);
                continue;
            }

            List<String> nameList = new List<String> { String.valueOf( applicant.get( 'First_Name__c')), String.valueOf( applicant.get( 'Last_Name__c')) };
            if ( String.isNotEmpty( String.valueOf( applicant.get( 'Birthdate_new__c')))) {
                Applicant__c applicantDate = (Applicant__c)applicant;
                nameList.add( String.valueOf( Date.newInstance( applicantDate.Birthdate_new__c.year(), applicantDate.Birthdate_new__c.month(), applicantDate.Birthdate_new__c.day())));
            }

            String applicantKey = IntegrationUtils.buildCompoundKey(nameList, '%');
            String studentId = studentKeyToIdMap.get( applicantKey);

            // If an applicant record matches one of the students from the External API by first/last name and birthdate, we need to update the record in our system.
            if (studentKeyToIdMap.containsKey(applicantKey)) {

                // If the external Id for this integration (e.g. Sao_Id__c, RavennaId__c) is null, populate it.
                if (applicant.get( applicantExternalIdField) == null) {
                    applicant = apiState.familyModel.populateFieldByType(applicant, applicantExternalIdField, studentId);
                }

                // overwrite applicant in our map so that existing applicants that have come from the integration have the latest info from the other system.
                apiState.familyModel.studentIdToApplicantMap.put( studentId, applicant);
            }
        }

        Type applicant = Type.forName( 'List<Applicant__c>');
        List<sObject> applicantsToInsert = (List<sObject>) applicant.newInstance();
        Set<Id> applicantIds = new Set<Id>();
        for ( sObject app : apiState.familyModel.studentIdToApplicantMap.values()) {

            Id applicantId = (Id)app.get( FamilyIntegrationModel.ID_FIELD);

            if (!applicantIds.contains( applicantId)) applicantsToInsert.add( app);

            if (applicantId != null) applicantIds.add( applicantId);
        }

        DML.WithoutSharing.upsertObjects( applicantsToInsert);

        // SFP-1178 On the api state, populate the applicantsToUpsert property so we can reference them later on.
        // This will let us easily retrieve the applicants and related student contact records in case we need to do special handling such as syncing ethnicity values.
        apiState.familyModel.applicantsToUpsert = applicantsToInsert;
    }
    
   /**
     * @description: Creates student folders, and maps raw data to sobject based on Integration_Mapping_Mdt
     *
     * @param: apiState - A Current Instance of IntegrationApiModel.ApiState
     */
    private static void createStudentFolders( IntegrationApiModel.ApiState apiState, String sourceIntegrationModel) {

        String folderIdField;
        List<String> keysToCompound = new List<String> { 'first_name__c', 'last_name__c', 'birthdate_new__c'};

        for( IntegrationMappingModel mapping : apiState.familyModel.mappingModelBySourceMap.get(sourceIntegrationModel)) {

            if(mapping.dataType == FamilyIntegrationModel.ID_FIELD && mapping.targetObject == FamilyIntegrationModel.STUDENT_FOLDER_OBJECT) {

                folderIdField = mapping.targetField;
            }
        }

        List<sObject> mappedFolders = new List<sObject>();
        Map<String,String> folderNameKeyMap = new Map<String,String>();
        List<String> firstNames = new List<String>();
        List<String> lastNames = new List<String>();
        List<Date> birthdates = new List<Date>();

        for( String studentId : apiState.familyModel.studentIdToApplicantMap.keySet()) {
            // SFP-1686: Create folders from application response, this is where we create the map of student folders that will be inserted.
            // We do this from the studentIdToResponseMap which only allows for one application per student when really a single student applies to multiple schools.
            // Here we can use the results of the ApplicationResponsesByStudentExternalId property to ensure each application is inserted.
            List<sObject> mappedStudentFolders = apiState.familyModel.mapStudentsToFolders( apiState.familyModel.ApplicationResponsesByStudentId.get( studentId), apiState.familyModel.mappingModelBySourceMap.get(sourceIntegrationModel));
            for( sObject studentFolder : mappedStudentFolders) {

                String firstName = (String)apiState.familyModel.studentIdToApplicantMap.get( studentId).get( 'first_name__c');
                String lastName = (String)apiState.familyModel.studentIdToApplicantMap.get( studentId).get( 'last_name__c');
                Date birthdate = (Date)apiState.familyModel.studentIdToApplicantMap.get( studentId).get( 'birthdate_new__c');

                // SFP-1551: Populate the first part of the folder name here when we know that we have the correct student first name/last name.
                // The school name will be appended when we have the school.
                // This avoids the risk of assuming that the number of first names/last names matches the total number of school folders.
                // Before SFP-1551, every student folder would have the name of the first student which is wrong...
                String folderName = firstName + ' ' + lastName + ' - ';
                studentFolder = apiState.familyModel.populateFieldByType( studentFolder, 'name', folderName);

                firstNames.add( firstName);
                lastNames.add( lastName);
                birthdates.add( birthdate);

                // update the object.break;
                List<String> nameList = new List<String> { firstName, lastName, String.valueOf( birthdate)};

                folderNameKeyMap.put((String)studentFolder.get(folderIdField), IntegrationUtils.buildCompoundKey(nameList, '%'));
                // Sett Student__c filed.
                studentFolder = apiState.familyModel.populateFieldByType(studentFolder, FamilyIntegrationModel.STUDENT_OBJECT, IntegrationUtils.buildCompoundKey(nameList, '%'));
            }

            mappedFolders.addAll( mappedStudentFolders);
        }

        // SOQL find students.
        Map<String, String> contactNameKeyMap = new Map<String, String>();
        List<Contact> contacts = new ContactDataAccessService().getContactsByNames( firstNames, lastNames, birthDates);
        for ( Contact c : contacts) {

            List<String> nameList = new List<String> { c.FirstName, c.LastName };
            if ( String.isNotEmpty( String.valueOf( c.Birthdate))) {

                nameList.add( String.valueOf( Date.newInstance(c.birthdate.year(),c.birthdate.month(),c.birthdate.day())));
            }

            contactNameKeyMap.put( IntegrationUtils.buildCompoundKey(nameList, '%'), c.Id);
        }

        Integer i = 0;
        List<sObject> studentFoldersToCreate = new List<sObject>();
        for ( sObject studentFolder : mappedFolders) {

            String nameKey = folderNameKeyMap.get( (String)studentFolder.get( folderIdField));
            studentFolder = apiState.familyModel.populateFieldByType(studentFolder, FamilyIntegrationModel.STUDENT_OBJECT, contactNameKeyMap.get(nameKey));

            studentFolder = apiState.familyModel.populateFieldByType(studentFolder, 'Academic_Year_Picklist__c', apiState.pfs.Academic_Year_Picklist__c);

            if ( studentFolder.get( FamilyIntegrationModel.SCHOOL_OBJECT) != null
                    && apiState.familyModel.schoolIdsToAccountMap.containsKey( (String)studentFolder.get( FamilyIntegrationModel.SCHOOL_OBJECT)) != null) {

                sObject schoolAcct = apiState.familyModel.schoolIdsToAccountMap.get( (String)studentFolder.get( FamilyIntegrationModel.SCHOOL_OBJECT));

                // it's possible schoolAcct can be null based on the data
                // only create accounts that are not null.
                if( schoolAcct != null) {
                    // SFP-1551: Now we take the first part of the name added when we were gathering student names and append the school name to it.
                    String folderName = (String)studentFolder.get('name') + (String)schoolAcct.get('name');

                    studentFolder = apiState.familyModel.populateFieldByType( studentFolder, 'name', folderName);
                    studentFolder = apiState.familyModel.populateFieldByType( studentFolder, 'OwnerId', GlobalVariables.getIndyOwner());
                    studentFoldersToCreate.add( studentFolder);
                }
                i++;
            }
        }

        Schema.SObjectField folderField = Schema.getGlobalDescribe().get( FamilyIntegrationModel.STUDENT_FOLDER_OBJECT).getDescribe().fields.getMap().get( folderIdField);

        Type studentFolderType = Type.forName( 'List<Student_Folder__c>');
        list<sObject> studentFoldersToUpsert = ( List<sObject>) studentFolderType.newInstance();
        studentFoldersToUpsert.addAll( studentFoldersToCreate);

        DML.WithoutSharing.upsertObjects( studentFoldersToUpsert, folderField);
    }
    
     /**
     * @description: Creates student pfsAssignmetns, and maps raw data to sobject based on Integration_Mapping_Mdt
     *
     * @param: apiState - A Current Instance of IntegrationApiModel.ApiState
     */
    private static void createStudentPFSAssignments( IntegrationApiModel.ApiState apiState, String sourceIntegrationModel) {

        String spaIdField;
        for ( IntegrationMappingModel mapping : apiState.familyModel.mappingModelBySourceMap.get( sourceIntegrationModel)) {

            if ( mapping.dataType == FamilyIntegrationModel.ID_FIELD && mapping.targetObject == FamilyIntegrationModel.SCHOOL_PFS_ASSIGN_OBJECT) {

                spaIdField = mapping.targetField;
            }
        }

        List<sObject> mappedSPA = new List<sObject>();
        for ( String studentId : apiState.familyModel.studentIdToApplicantMap.keySet()) {

            List<sObject> mappedStudentSPA = apiState.familyModel.mapStudentToSPA( apiState.familyModel.ApplicationResponsesByStudentId.get( studentId), apiState.familyModel.mappingModelBySourceMap.get( sourceIntegrationModel));
            for ( sObject spa : mappedStudentSPA) {

                // here loop over all spa's and populate files that will be consistent across ALL integrations
                sObject schoolAcct = apiState.familyModel.schoolIdsToAccountMap.get( (String)spa.get( FamilyIntegrationModel.SCHOOL_OBJECT));
                if( schoolAcct != null) {
                    String name = (String)apiState.familyModel.studentIdToApplicantMap.get( studentId).get( 'first_name__c') + ' ' +
                                  (String)apiState.familyModel.studentIdToApplicantMap.get( studentId).get( 'last_name__c') + ' - ' +
                                  (String)schoolAcct.get('name');

                    spa = apiState.familyModel.populateFieldByType( spa, 'name', name);
                    spa = apiState.familyModel.populateFieldByType( spa, FamilyIntegrationModel.APPLICANT_OBJECT, (String)apiState.familyModel.studentIdToApplicantMap.get( studentId).Id);
                    spa = apiState.familyModel.populateFieldByType( spa, 'Academic_Year_Picklist__c', apiState.pfs.Academic_Year_Picklist__c);
                }
            }

            mappedSPA.addAll( mappedStudentSPA);
        }

        List<sObject> spasToCreate = new List<sObject>();
        for ( sObject spa : mappedSPA) {

            if ( spa.get( FamilyIntegrationModel.SCHOOL_OBJECT) != null &&
                    apiState.familyModel.schoolIdsToAccountMap.containsKey( (String)spa.get( FamilyIntegrationModel.SCHOOL_OBJECT))) {

                spasToCreate.add(spa);
            }
        }

        // DML
        Schema.SObjectField spaField = Schema.getGlobalDescribe().get( FamilyIntegrationModel.SCHOOL_PFS_ASSIGN_OBJECT).getDescribe().fields.getMap().get( spaIdField);
        Type spaType = Type.forName('List<School_PFS_Assignment__c>');
        List<sObject> spaToCreate = (List<sObject>) spaType.newInstance();
        spaToCreate.addAll( ( List<sObject>)spasToCreate);

        DML.WithoutSharing.upsertObjects(spaToCreate, spaField);
    }

    /**
     * @description Use to handle post processing events after external endpoints have been called to retrieve students,
     *              applications, and school info from another system. This method will take the ApiState and group all
     *              the responses that represent student profiles by the student's external Id. The responses from the
     *              students endpoint are then used to create applicant SObjects where we populate the fields based on
     *              the Integration Mapping custom metadata records. The Applicant Sobjects are then grouped by the
     *              external Id configured for this integration. This grouping will allow us to upsert the applicant
     *              records later on in case the applicant already exists in the system and just needs to be updated
     *              from a previous year.
     */
    public static void populateApplicantObjects(IntegrationApiModel.ApiState apiState, String studentApiIdField, String studentApiName) {
        // first loop over the response for 'Students' and populuate the response map by student Id.
        for( Object o : apiState.httpResponseByApi.get( studentApiName)) {

            List<Object> responseObjects = new List<Object>();
            Map<String, Object> values = (Map<String, Object>)o;

            String idValue = String.valueOf( values.get( studentApiIdField));
            if( apiState.familyModel.studentIdToResponseMap.containsKey( idValue)) {

                responseObjects = apiState.familyModel.studentIdToResponseMap.get( idValue);
            }

            responseObjects.add( o);
            apiState.familyModel.studentIdToResponseMap.put( idValue, responseObjects);
        }

        // map applicants to Applicant Object.
        List<sObject> mappedApplicants = apiState.familyModel.mapStudentsToApplicant( apiState.httpResponseByApi.get( studentApiName),
                apiState.familyModel.mappingModelBySourceMap.get( FamilyIntegrationModel.STUDENTS_TYPE));

        apiState.familyModel.studentApplicants = mappedApplicants;
        apiState.familyModel.populatPfsIdOnObject( apiState.familyModel.studentApplicants, apiState.pfs.Id);

        // get Applicant external Id
        String applicantExternalId = apiState.familyModel.getExternalIdFieldByObject( apiState.familyModel.mappingModelBySourceMap.get(FamilyIntegrationModel.STUDENTS_TYPE), FamilyIntegrationModel.APPLICANT_OBJECT);

        // Find existing applicants by external Id;
        // This groups the applicant Sobjects that were created by the external Id field configured for the integration.
        apiState.familyModel.setStudentIdsToSobject( apiState.familyModel.studentApplicants, applicantExternalId);
    }

    /**
     * @description: Handles the initialize processing of Applicants__c and validates there are current SSS subscriber schools
     *
     * @param: apiState - A Current Instance of IntegrationApiModel.ApiState
     *
     * @param: studentApiIdField - Student Api Id Field
     *
     * @param: studentApiName - Name of the current Students Api.
     */
    public static void processApplicants( IntegrationApiModel.ApiState apiState, String studentApiIdField, String studentApiName) {
        populateApplicantObjects(apiState, studentApiIdField, studentApiName);

        // Map existing applicant Id's to the setStudentIdsToSobject

        String fieldToCheck;
        for ( IntegrationMappingModel mapping : apiState.familyModel.mappingModelBySourceMap.get( FamilyIntegrationModel.STUDENT_APPLICATION)) {

            // Loop over the mapping and split the lookup field for Student_Folder__c;
            // if it exists then split it and get the external_id_field.
            if( mapping.dataType == FamilyIntegrationModel.LOOPKUP_FIELD && mapping.targetObject == FamilyIntegrationModel.STUDENT_FOLDER_OBJECT) {

                fieldToCheck = IntegrationUtils.splitLookupField( mapping.targetField);
                break;
            }
        }

        List<String> schools = new List<String>();
        for ( String studentId : apiState.familyModel.studentIdToSchoolsMap.keySet()) {

            schools.addAll( apiState.familyModel.studentIdToSchoolsMap.get( studentId));
        }
        
        apiState.familyModel.schoolIdsToAccountMap = IntegrationUtils.checkForValidSchools( fieldToCheck, schools, apiState.pfs);

        // schoolsIdToAccountMap is null, don't continue: this means there are no schools for this applicant.
        if ( apiState.familyModel.schoolIdsToAccountMap == null || apiState.familyModel.schoolIdsToAccountMap.isEmpty()) {

            throw new IntegrationApiService.IntegrationException( System.Label.Integration_No_Subscriber_School_Error);
        }

        // Process Student Folders now, by keset.
        for ( String studentId : apiState.familyModel.studentIdToApplicantMap.keySet()) {
            Boolean hasSchool = false;
            for ( String schoolId : apiState.familyModel.studentIdToSchoolsMap.get( studentId)) {

                if ( apiState.familyModel.schoolIdsToAccountMap.containsKey( schoolId)) {
                    hasSchool = true;
                    break;
                }
            }

            if ( !hasSchool)  apiState.familyModel.studentIdToApplicantMap.remove( studentId);
        }
    }

    public static void writeToIntegrationLog( String stackTraceString, String exceptionMessage, String bodyContent) {

        IntegrationUtils.writeToIntegrationLog( stackTraceString, exceptionMessage, bodyContent, null);
    }
    /**
     * @description: Writes a record to IntegrationLog__c
     *
     * @param: stackTraceString - String representation of stack trace
     * @param: exceptionMessage - String of actual caught exception message
     * @param: bodyContent - main content intended to be stored in IntegrationLog__c
     */
    public static void writeToIntegrationLog( String stackTraceString, String exceptionMessage, String bodyContent, String source) {

        // if all params empty, early return
        if ( String.isBlank( stackTraceString) && String.isBlank( exceptionMessage) && String.isBlank( bodyContent)) return;

        IntegrationLog__c iLog = new IntegrationLog__c(
                Body__c = 'Stack Trace: ' + ( String.isNotBlank( stackTraceString) ? stackTraceString : 'N/A') +
                        ' :Exception Message: ' + ( String.isNotBlank( exceptionMessage) ? exceptionMessage : 'N/A') +
                        ' :Log Content: ' + ( String.isNotBlank( bodyContent) ? bodyContent : 'N/A'),
                Source__c = String.isNotBlank( source) ? source : 'N/A');
        insert iLog;
    }
}