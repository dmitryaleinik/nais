/**
 * Spec-134 School Portal - Multiple User Security; Req# R-493
 *    Batch job on School PFS Assignment, Trigger on User and Affiliation to share all PFS and Student Folder records relevant to the school with the School users
 *    for each school they are affiliated with.
 *
 * NAIS-339 Multiple User Security - Account Change
 *    Trigger on Contact to share all PFS and Student Folder records to a school staff user upon change to a different school Account.
 *
 * NAIS-1617 New Process for BulkSharing
 *    Delegate sharing in Trigger on User, Affiliation, and Contact to scheduled batch job on User when asynchronous sharing would have exceeded DML Limit.
 *
 * NAIS-1766 [CH] Refactoring to use Group-based sharing
 *
 * WH, Exponent Partners, 2013, 2014
 * CH, Exponent Partners, 2014
 **/
public class SchoolStaffShareAction {
    /*
     * for use in tests
     */
    public static Boolean suppressUserTrigger = false;
    public static Boolean forceException = false;
    public class ForcedException extends Exception {}

    private static final String NO_ACCESS = 'None';

    /**
     * Spec-134 School Portal - Multiple User Security; Req# R-493
     */

    /**
     *    1) Called from after insert of School PFS Assignment (by way of scheduled batch job to process unshared School PFS Assignments)
     *        spfsaIdList    - list of new school pfs assignments
     *        newUserIds    - null
     *        share PFS and Student Folder records of new School PFS Assignments to all existing active school staff users
     *
     *    2) Called through shareRecordsToNewSchoolUsers() from after insert or update of User
     *        spfsaIdList - list of existing school pfs assignments for the schools the new users are affiliated with
     *        newUserIds    - list of new school staff users inserted or activated
     *        share PFS and Student Folder records of existing School PFS Assignments only to the newly inserted/activated school staff users
     *
     *    3) Called through shareRecordsToNewAffiliatedSchoolUsers() from after insert or update of Affiliation
     *        spfsaIdList - list of existing school pfs assignments for the schools of the new affiliations
     *        newUserIds    - list of school staff users newly affiliated with the schools
     *        share PFS and Student Folder records of existing School PFS Assignments only to the newly affiliated school staff users
     *
     *    1 + 2 SOQL in getStaffUsersBySchoolId() + 0~2 SOQL in shareRecordsWithSchools(); 2~3 DML in shareRecordsWithSchools()
     */
    // [CH] NAIS-1766 Refactoring to use full SPA records
    public static void shareRecordsToSchoolUsers(List<School_PFS_Assignment__c> spfsaList, List<Id> newUserIds) {
        // for automated tests
        if (forceException) {
            throw new ForcedException('Forced exception for testing');
        }

        Set<Id> schoolIds = new Set<Id>();                                        // all schools involved
        Map<Id, Set<Id>> PFSsBySchoolId = new Map<Id, Set<Id>>();                // PFSs for each school
        Map<Id, Set<Id>> studentFoldersBySchoolId = new Map<Id, Set<Id>>();        // student folders for each school
        Map<Id, Set<Id>> parentAccountsBySchoolId = new Map<Id, Set<Id>>();

        // [CH] NAIS-1766 Refactoring to use full SPA records
        for (School_PFS_Assignment__c spfsa : spfsaList) {
            schoolIds.add(spfsa.School__c);

            Set<Id> PFSs = PFSsBySchoolId.get(spfsa.School__c);
            if (PFSs == null) {
                PFSs = new Set<Id>();
                PFSsBySchoolId.put(spfsa.School__c, PFSs);
            }
            PFSs.add(spfsa.Applicant__r.PFS__c);

            Set<Id> studentFolders = studentFoldersBySchoolId.get(spfsa.School__c);
            if (studentFolders == null) {
                studentFolders = new Set<Id>();
                studentFoldersBySchoolId.put(spfsa.School__c, studentFolders);
            }
            studentFolders.add(spfsa.Student_Folder__c);

            Set<Id> parentAccounts = parentAccountsBySchoolId.get(spfsa.School__c);
            if (parentAccounts == null) {
                parentAccounts = new Set<Id>();
                parentAccountsBySchoolId.put(spfsa.School__c, parentAccounts);
            }
            parentAccounts.add(spfsa.Applicant__r.PFS__r.Parent_A__r.AccountId);
        }
        // share the PFS, Individual Parent Account, and Student Folder records
        Boolean dmlLimitNotExceeded = true;
        Boolean shouldCheckExistingShares = newUserIds == null;
        shareRecordsWithSchools(PFSsBySchoolId, studentFoldersBySchoolId, parentAccountsBySchoolId, shouldCheckExistingShares);

        // reset hidden Sharing Processed flag when called from scheduled batch job that processes new School PFS Assignments
        if (newUserIds == null) {
            List<School_PFS_Assignment__c> spfsaListToUpdate = new List<School_PFS_Assignment__c>();

            // Cycle through SPA records and mark the ones complete.
            for (School_PFS_Assignment__c spaRecord : spfsaList) {
                    spaRecord.Sharing_Processed__c = true;
                    spfsaListToUpdate.add(spaRecord);
            }
            if (!spfsaListToUpdate.isEmpty()) {
                update spfsaListToUpdate;
            }
        }
    }

    @future
    public static void addAffiliatedUsersToGroups(List<Id> affiliationIds) {
        List<Affiliation__c> affiliationsList = [select Id, Contact__r.AccountId, Contact__c, Organization__c, Contact__r.RecordTypeId,
                Organization__r.RecordTypeId, Status__c, Type__c
        from Affiliation__c
        where Id in :affiliationIds ];
        addAffiliatedUsersToGroups(affiliationsList);
    }

    // [CH] NAIS-1766 Adding to support group-based sharing
    public static void addAffiliatedUsersToGroups(List<Affiliation__c> affiliations) {
        List<Id> contactIds = new List<Id>();
        List<String> groupNames = new List<String>{};

        // Cycle through incoming Affiliations
        for (Affiliation__c aff : affiliations) {
            // The non-profit starter pack automatically inserts an affiliation linked
            //    to the same Account as the Contact's account.  We don't need to add sharing
            //  in that case.
            if (aff.Contact__r.AccountId != aff.Organization__c) {
                // Add referenced Contact Id to a list
                contactIds.add(aff.Contact__c);

                // Add referenced Account Id to a list
                groupNames.add('X' + aff.Organization__c);
            }
        }

        // Query for Users associated to referenced Contacts and put
        //  them into a Map with ContactId as the key
        Map<Id, User> usersMap = new Map<Id, User>();
        for (User usr : [select Id, ContactId from User where ContactId in :contactIds]) {
            usersMap.put(usr.ContactId, usr);
        }

        // Query for Groups associated to reference Accounts and put them into a Map
        //  with the DeveloperName as the key
        Map<String, Group> groupsMap = new Map<String, Group>();
        for (Group grp : [select Id, DeveloperName from Group where DeveloperName in :groupNames]) {
            groupsMap.put(grp.DeveloperName, grp);
        }

        // Cycle through the affiliation records
        List<GroupMember> groupMembersToInsert = new List<GroupMember>();
        for (Affiliation__c aff : affiliations) {
            // Look up the User to add to a Group
            User userToLink = usersMap.get(aff.Contact__c);

            // Look up the Group to add the User to
            Group groupToLink = groupsMap.get('X' + aff.Organization__c);

            if (userToLink != null && groupToLink != null) {
                // Add a GroupMember record to the list to create
                groupMembersToInsert.add(new GroupMember(GroupId = groupToLink.Id, UserOrGroupId = userToLink.Id));
            }
        }

        // Insert new GroupMember records
        if (groupMembersToInsert.size() > 0) {
            Database.insert(groupMembersToInsert);
        }
    }

    // [CH] NAIS-1766 Adding to support group-based sharing
    @future
    public static void removeDeletedAffiliationsFromGroups(Map<Id, Id> affiliationTupleMap) {
        List<Affiliation__c> affiliationsList = new List<Affiliation__c>();

        for (Id contactId : affiliationTupleMap.keySet()) {
            Affiliation__c newAffiliation = new Affiliation__c(Contact__c = contactId, Organization__c = affiliationTupleMap.get(contactId));
            affiliationsList.add(newAffiliation);
        }
        removeAffiliatedUsersFromGroups(affiliationsList);
    }

    @future
    public static void removeAffiliatedUsersFromGroups(List<Id> affiliationIds) {
        List<Affiliation__c> affiliationsList = [select Id, Contact__c, Organization__c, Status__c, Type__c
        from Affiliation__c
        where Id in :affiliationIds ];
        removeAffiliatedUsersFromGroups(affiliationsList);
    }

    public static void removeAffiliatedUsersFromGroups(List<Affiliation__c> affiliations) {
        Set<Id> contactIds = new Set<Id>();
        List<String> groupNames = new List<String>{};

        // Cycle through incoming Affiliations
        for (Affiliation__c aff : affiliations) {
            // Add referenced Contact Id to a list
            contactIds.add(aff.Contact__c);

            // Add referenced Account Id to a list
            groupNames.add('X' + aff.Organization__c);
        }

        // [CH] NAIS-2051 Start
        // Query for referenced Contact records, and put them into a Map with ContactId & AccountId as the key
        Set<String> contactAccountSet = new Set<String>{};
        for (Contact contactRecord : [select Id, AccountId from Contact where Id in :contactIds]) {
            contactAccountSet.add(contactRecord.Id + '_' + contactRecord.AccountId);
        }

        for (Affiliation__c aff : affiliations) {
            // If the current affiliation matches the Contact's Account
            if (contactAccountSet.contains(aff.Contact__c + '_' + aff.Organization__c)) {
                // then remove the ContactId from the Contact Id list
                contactIds.remove(aff.Contact__c);
                // (this prevents the User from being removed from the Account's Group)
            }
        }
        // NAIS-2051 End

        // Since we can't do UserOrGroup.ContactId, query for Users that have a Contact in the list and add them to a Map
        Map<Id, User> usersMap = new Map<Id, User>();
        List<Id> userIdList = new List<Id>();
        for (User usr : [select Id, ContactId from User where ContactId in :contactIds]) {
            usersMap.put(usr.ContactId, usr);
            userIdList.add(usr.Id);
        }

        if (usersMap.values() != null && userIdList.size() > 0) {
            // Query for GroupMembers and add them to a Map
            Map<String, GroupMember> groupMemberMap = new Map<String, GroupMember>();
            for (GroupMember grpm : [select Id, UserOrGroupId, Group.DeveloperName
            from GroupMember
            where UserOrGroupId in :userIdList
            and Group.DeveloperName in :groupNames]) {
                groupMemberMap.put(grpm.UserOrGroupId + '-' + grpm.Group.DeveloperName, grpm);
            }

            List<Id> membersToRemove = new List<Id>();
            for (Affiliation__c aff : affiliations) {
                GroupMember toRemove = groupMemberMap.get(usersMap.get(aff.Contact__c).Id + '-' + 'X' + aff.Organization__c);

                if (toRemove != null) {
                    membersToRemove.add(toRemove.Id);
                }
            }

            // Insert new GroupMember records
            if (membersToRemove.size() > 0) {
                Database.delete(membersToRemove);
            }
        }
    }

    /*
     * Utility methods
     */

    /**
     * @description [CH] NAIS-1766 Refactored the method to process sharing on an account by account basis, and to connect
     *              Sharing records to Account-specific public groups.
     *              NOTE: 3 SOQL & 3 DML if checkExistingShares is true, otherwise 0 SOQL & 3 DML
     */
    public static void shareRecordsWithSchools(Map<Id, Set<Id>> PFSsBySchoolId, Map<Id, Set<Id>> studentFoldersBySchoolId,
            Map<Id, Set<Id>> parentAccountsBySchoolId, Boolean checkExistingShares) {

        // Compile the list of Public Group names for Accounts referenced by SPA records
        List<String> groupNames = new List<String>();
        Map<Id, Set<Id>> mapToTraverse = (PFSsBySchoolId.isEmpty()) ? studentFoldersBySchoolId : PFSsBySchoolId;
        for (Id aId : mapToTraverse.keySet()) {
            groupNames.add('X' + aId);
        }

        // Query for the group of Public Group records for the specified Accounts and
        //  put them into a Map with the DeveloperName as the key
        Map<String, Id> groupsMap = new Map<String, Id>();
        for (Group groupRecord : [select Id, DeveloperName from Group where DeveloperName in :groupNames]) {
            groupsMap.put(groupRecord.DeveloperName, groupRecord.Id);
        }

        Set<String> shareKeys = new Set<String>();
        if (checkExistingShares) {
            // gather existing record shares that cannot be overwritten ('All' access for owners)
            Set<Id> pfsIds = new Set<Id>();
            Set<Id> studentFolderIds = new Set<Id>();
            Set<Id> parentAccountIds = new Set<Id>();

            for (Id aId : studentFoldersBySchoolId.keySet()) {
                if (PFSsBySchoolId.get(aId) != null) { pfsIds.addAll(PFSsBySchoolId.get(aId)); }                                // should be non-null
                if (studentFoldersBySchoolId.get(aId) != null) { studentFolderIds.addAll(studentFoldersBySchoolId.get(aId)); }  // should be non-null
                if (parentAccountsBySchoolId.get(aId) != null) { parentAccountIds.addAll(parentAccountsBySchoolId.get(aId)); }  // should be non-null
            }

            for (PFS__Share ps : [select ParentId, UserOrGroupId
            // [CH] NAIS-1766 Refactor for Account-specific Public Groups
                    from PFS__Share where ParentId in :pfsIds and UserOrGroupId in :groupsMap.values() and AccessLevel = 'All']) {
                shareKeys.add(ps.ParentId + '-' + ps.UserOrGroupId);
            }

            for (Student_Folder__Share sfs : [select ParentId, UserOrGroupId
                    from Student_Folder__Share where ParentId in :studentFolderIds and UserOrGroupId in :groupsMap.values() and AccessLevel = 'All']) {
                shareKeys.add(sfs.ParentId + '-' + sfs.UserOrGroupId);
            }

            // Currently individual accounts only need to be shared because of access to mean based waivers.
            // School users will not need to write to that account so not filtering on AccessLevel = 'All' like the other
            // queries
            for (AccountShare aShare : [SELECT AccountId, UserOrGroupId FROM AccountShare
                    WHERE AccountId in :parentAccountIds AND UserOrGroupId in :groupsMap.values()]) {
                shareKeys.add(aShare.AccountId + '-' + aShare.UserOrGroupId);
            }

        }

        // create the shares
        List<PFS__Share> pfsSharesToAdd = new List<PFS__Share>();
        List<Student_Folder__Share> studentFolderSharesToAdd = new List<Student_Folder__Share>();
        List<AccountShare> parentAccountSharesToAdd = new List<AccountShare>();

        for (Id schoolId : mapToTraverse.keySet()) {                // for each school
            Set<Id> pfsRecordIds = PFSsBySchoolId.get(schoolId);
            Set<Id> folderRecordIds = studentFoldersBySchoolId.get(schoolId);
            Set<Id> parentAccountIds = parentAccountsBySchoolId.get(schoolId);

            // Create sharing records for PFSs
            for (Id pfsId : (pfsRecordIds != null ? pfsRecordIds : new Set<Id>())) {
                if (!checkExistingShares || !shareKeys.contains(pfsId + '-' + schoolId)) {
                    shareKeys.add(pfsId + '-' + schoolId);
                    Id groupId = groupsMap.get('X' + schoolId);
                    if (groupId != null) {
                        pfsSharesToAdd.add(
                                new PFS__Share(
                                        ParentId = pfsId,
                                        UserOrGroupId = groupId,
                                        AccessLevel = 'Edit',
                                        RowCause = 'Manual'
                                ));
                    }
                }
            }

            for (Id sfId : (folderRecordIds != null ? folderRecordIds : new Set<Id>())) {
                if (!checkExistingShares || !shareKeys.contains(sfId + '-' + schoolId)) {
                    shareKeys.add(sfId + '-' + schoolId);
                    Id groupId = groupsMap.get('X' + schoolId);
                    if (groupId != null) {
                        studentFolderSharesToAdd.add(
                                new Student_Folder__Share(
                                        ParentId = sfId,
                                        UserOrGroupId = groupId,
                                        AccessLevel = 'Edit',
                                        RowCause = 'Manual'
                                ));
                    }
                }
            }

            for (Id parentAccountId : (parentAccountIds != null ? parentAccountIds : new Set<Id>())) {
                if(!checkExistingShares || !shareKeys.contains(parentAccountId + '-' + schoolId)) {
                    shareKeys.add(parentAccountId + '-' + schoolId);
                    Id groupId = groupsMap.get('X' + schoolId);
                    if (groupId != null) {
                        parentAccountSharesToAdd.add(new AccountShare(
                                AccountId = parentAccountId,
                                AccountAccessLevel = 'Edit',
                                UserOrGroupId = groupId,
                                CaseAccessLevel = NO_ACCESS,
                                OpportunityAccessLevel = NO_ACCESS
                        ));
                    }
                }
            }
        }

        if (!pfsSharesToAdd.isEmpty()) {
            insert pfsSharesToAdd;
        }

        if (!studentFolderSharesToAdd.isEmpty()) {
            insert studentFolderSharesToAdd;
        }

        if (!parentAccountSharesToAdd.isEmpty()) {
            insert parentAccountSharesToAdd;
        }
    }

    public static Integer getDMLLimit() {
        if (Test.isRunningTest()) {
            return 200;        // to speed up unit tests
        } else {
            // apply a safety margin of 200
            return Limits.getLimitDMLRows() - 200;
        }
    }

}