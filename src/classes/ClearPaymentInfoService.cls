/**
* Clears payment fields of Chargent Order and Transaction records. 
*/
public class ClearPaymentInfoService
{

    /**
     * @description Gets a list of transaction records build sets of ids of Chargent Order and Transaction records.
     * @param transactions Transaction list to handle.
     */
    public void handleTransactionAndOrderRecords(List<ChargentOrders__Transaction__c> transactions) {
        Set<Id> transactionIds = new Map<Id, ChargentOrders__Transaction__c>(transactions).keySet();
        Set<Id> chargentOrderIds = new Set<Id>();

        for (ChargentOrders__Transaction__c singleTransaction : transactions) {
            if (singleTransaction.ChargentOrders__Order__c != null) {
                chargentOrderIds.add(singleTransaction.ChargentOrders__Order__c);
            }
        }

        clearTransactionPaymentFields(transactionIds);
        clearChargentOrderPaymentFields(chargentOrderIds);
    }

    /**
     * @description Gets a set of transaction record Ids and updates payment fields of these records with
     *              null values.
     * @param transactionIds Transaction Ids to update appropriate records.
     */
    private void clearTransactionPaymentFields(Set<Id> transactionIds)
    {
        List<ChargentOrders__Transaction__c> transactionsToUpdate = new  List<ChargentOrders__Transaction__c>();
        
        for (Id transactionId : transactionIds) {
            transactionsToUpdate.add(new ChargentOrders__Transaction__c(
                Id = transactionId, 
                ChargentOrders__Credit_Card_Name__c = null,
                ChargentOrders__Credit_Card_Type__c = null,
                ChargentOrders__Credit_Card_Number__c = null,
                ChargentOrders__Card_Last_4__c = null,
                ChargentOrders__Bank_Account_Number__c = null,
                ChargentOrders__Bank_Account_Name__c = null,
                ChargentOrders__Bank_Routing_Number__c = null,
                ChargentOrders__Bank_Account_Type__c = null,
                ChargentOrders__Bank_Name__c = null,
                PaymentInfoIsCleared__c = true));
        }

        update transactionsToUpdate;
    }

    /**
     * @description Gets a set of chargent order record Ids and updates payment fields of these records with
     *              null values.
     * @param chargentOrderIds Chargent Order Ids to update appropriate records.
     */
    private void clearChargentOrderPaymentFields(Set<Id> chargentOrderIds)
    {
        List<ChargentOrders__ChargentOrder__c> chargentOrdersToUpdate = new List<ChargentOrders__ChargentOrder__c>();

        for (Id chargentOrderId : chargentOrderIds) {
            chargentOrdersToUpdate.add(new ChargentOrders__ChargentOrder__c(
                Id = chargentOrderId, 
                ChargentOrders__Card_Number__c = null,
                ChargentOrders__Card_Last_4__c = null,
                ChargentOrders__Card_Expiration_Month__c = null,
                ChargentOrders__Card_Expiration_Year__c = null,
                ChargentOrders__Card_Security_Code__c = null,
                ChargentOrders__Card_Type__c = null,
                ChargentOrders__Bank_Name__c = null,
                ChargentOrders__Bank_Account_Name__c = null,
                ChargentOrders__Bank_Account_Number__c = null,
                ChargentOrders__Bank_Routing_Number__c = null,
                ChargentOrders__Bank_Account_Type__c = null,
                PaymentInfoIsCleared__c = true));
        }

        update chargentOrdersToUpdate;
    }

    /**
     * @description An instance property to store the singleton
     *              instance of the ClearPaymentInfoService.
     */
    public static ClearPaymentInfoService Instance {
        get {
            if (Instance == null) {
                Instance = new ClearPaymentInfoService();
            }
            return Instance;
        }
        private set;
    }

    /**
     * @description Private constructor to keep this from being
     *              instantiated externally.
     */
    private ClearPaymentInfoService() {}
}