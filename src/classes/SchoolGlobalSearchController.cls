public with sharing class SchoolGlobalSearchController {
    
    public String searchText {get; set;}
    
    public SchoolGlobalSearchController(){}
    
    public PageReference doSearch(){
        return new PageReference('/apex/SchoolMyApplicants?search=' + searchText);
    }
    public PageReference resetSearch(){
        
        PageReference resetReference = new PageReference('/apex/SchoolMyApplicants');
        resetReference.setRedirect(true);
        return resetReference;
    }
}