@isTest
private class SchoolFamilyContribWrksheetPrintContTest 
{

    private static final Decimal ORIGINAL_SOCIAL_SECURITY_AMOUNT = 50.00;
    private static final Decimal ORIGINAL_MEDICARE_AMOUNT = 100.00;
    private static final Decimal REVISED_SOCIAL_SECURITY_AMOUNT = 40.00;
    private static final Decimal REVISED_MEDICARE_AMOUNT = 80.00;
    private static final Decimal SOCIAL_SECURITY_AMOUNT = 45.00;
    private static final Decimal MEDICARE_AMOUNT = 90.00;
    private static final String ERROR_MISSING_ID= 'Missing schoolpfsassignmentid';
    private static final String PFS_BUSINESS_NOT_INDICATED_VALUE= 'Yes';

    @isTest
    private static void setCombinedMedicareAndSSFields_revisionFieldsNull_expectRevisionNull() {
        School_PFS_Assignment__c spa = SchoolPfsAssignmentTestData.Instance
                .withOrigMedicareAllowance(ORIGINAL_MEDICARE_AMOUNT)
                .withOrigSocialSecurityAllowance(ORIGINAL_SOCIAL_SECURITY_AMOUNT)
                .insertSchoolPfsAssignment();

        SchoolFamilyContribWrksheetPrintCont.FCWPrintWrapper fcwPrintWrapper =
                new SchoolFamilyContribWrksheetPrintCont.FCWPrintWrapper(spa, null, null, null, null);

        Test.startTest();
        fcwPrintWrapper.setCombinedMedicareAndSSFields();
        Test.stopTest();

        Decimal expectedAmount = ORIGINAL_SOCIAL_SECURITY_AMOUNT + ORIGINAL_MEDICARE_AMOUNT;

        System.assertEquals(null, fcwPrintWrapper.medicareAndSSCombinedRevised);
        System.assertEquals(expectedAmount, fcwPrintWrapper.medicareAndSSCombinedOrig);
    }

    @isTest
    private static void setCombinedMedicareAndSSFields_medicareRevisionFieldPopulated_expectMixedRevision()
    {
        School_PFS_Assignment__c spa = SchoolPfsAssignmentTestData.Instance
            .withMedicareAllowance(MEDICARE_AMOUNT)
            .withSocialSecurityAllowance(SOCIAL_SECURITY_AMOUNT)
            .withOrigMedicareAllowance(ORIGINAL_MEDICARE_AMOUNT)
            .withOrigSocialSecurityAllowance(ORIGINAL_SOCIAL_SECURITY_AMOUNT)
            .withRevisedMedicareAllowance(REVISED_MEDICARE_AMOUNT)
            .insertSchoolPfsAssignment();

        SchoolFamilyContribWrksheetPrintCont.FCWPrintWrapper fcwPrintWrapper =
                new SchoolFamilyContribWrksheetPrintCont.FCWPrintWrapper(spa, null, null, null, null);

        Test.startTest();
            fcwPrintWrapper.setCombinedMedicareAndSSFields();
        Test.stopTest();

        Decimal expectedAmount = SOCIAL_SECURITY_AMOUNT + REVISED_MEDICARE_AMOUNT;

        System.assertEquals(expectedAmount, fcwPrintWrapper.medicareAndSSCombinedRevised);
    }

    @isTest
    private static void setCombinedMedicareAndSSFields_socialSecurityRevisionFieldPopulated_expectMixedRevision()
    {
        School_PFS_Assignment__c spa = SchoolPfsAssignmentTestData.Instance
            .withMedicareAllowance(MEDICARE_AMOUNT)
            .withSocialSecurityAllowance(SOCIAL_SECURITY_AMOUNT)
            .withOrigMedicareAllowance(ORIGINAL_MEDICARE_AMOUNT)
            .withOrigSocialSecurityAllowance(ORIGINAL_SOCIAL_SECURITY_AMOUNT)
            .withRevisedSocialSecurityAllowance(REVISED_SOCIAL_SECURITY_AMOUNT)
            .insertSchoolPfsAssignment();

        SchoolFamilyContribWrksheetPrintCont.FCWPrintWrapper fcwPrintWrapper =
                new SchoolFamilyContribWrksheetPrintCont.FCWPrintWrapper(spa, null, null, null, null);

        Test.startTest();
            fcwPrintWrapper.setCombinedMedicareAndSSFields();
        Test.stopTest();

        Decimal expectedAmount = MEDICARE_AMOUNT + REVISED_SOCIAL_SECURITY_AMOUNT;

        System.assertEquals(expectedAmount, fcwPrintWrapper.medicareAndSSCombinedRevised);
    }

    @isTest
    private static void setCombinedMedicareAndSSFields_bothRevisionFieldsPopulated_expectRevisionSum() {
        School_PFS_Assignment__c spa = SchoolPfsAssignmentTestData.Instance
                .withOrigMedicareAllowance(ORIGINAL_MEDICARE_AMOUNT)
                .withOrigSocialSecurityAllowance(ORIGINAL_SOCIAL_SECURITY_AMOUNT)
                .withRevisedSocialSecurityAllowance(REVISED_SOCIAL_SECURITY_AMOUNT)
                .withRevisedMedicareAllowance(REVISED_MEDICARE_AMOUNT)
                .insertSchoolPfsAssignment();

        SchoolFamilyContribWrksheetPrintCont.FCWPrintWrapper fcwPrintWrapper =
                new SchoolFamilyContribWrksheetPrintCont.FCWPrintWrapper(spa, null, null, null, null);

        Test.startTest();
            fcwPrintWrapper.setCombinedMedicareAndSSFields();
        Test.stopTest();

        Decimal expectedAmount = REVISED_MEDICARE_AMOUNT + REVISED_SOCIAL_SECURITY_AMOUNT;

        System.assertEquals(expectedAmount, fcwPrintWrapper.medicareAndSSCombinedRevised);
    }

    @isTest
    private static void testCreateControllerWithoutPageParameters_expectException()
    {
        Test.setCurrentPage(Page.SchoolFamilyContributionWorksheetPrint);

        try{
            Test.startTest();
                SchoolFamilyContribWrksheetPrintCont controller = new SchoolFamilyContribWrksheetPrintCont();
            Test.stopTest();
        }
        catch (ArgumentException ex){
            System.assert(ex.getMessage().contains(ERROR_MISSING_ID));
        }
    }

    @isTest
    private static void testCreateControllerWithSetIdsAndAllNecessaryFields_expectFcwPrintWrappersNotEmpty()
    {
        Student_Folder__c studentFolder = StudentFolderTestData.Instance.DefaultStudentFolder;
        Academic_Year__c academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;
        PFS__c pfs = PfsTestData.Instance.forBusinessNotIndicated(PFS_BUSINESS_NOT_INDICATED_VALUE).DefaultPfs;
        Applicant__c applicant = ApplicantTestData.Instance.forPfsId(pfs.Id).DefaultApplicant;
        School_PFS_Assignment__c spa = SchoolPfsAssignmentTestData.Instance
                .forStudentFolderId(studentFolder.Id)
                .forAcademicYearPicklist(academicYear.Name)
                .forApplicantId(applicant.Id)
                .DefaultSchoolPfsAssignment;

        Unusual_Conditions__c unusualConditions = UnusualConditionsTestData.Instance
            .forAcademicYearId(academicYear.Id).DefaultUnusualConditions;
        SSS_Constants__c sssConstants = SssConstantsTestData.Instance
            .forAcademicYearId(academicYear.Id).DefaultSssConstants;

        Test.startTest();
            SchoolFamilyContribWrksheetPrintCont controller = 
                new SchoolFamilyContribWrksheetPrintCont(new Set<Id>{studentFolder.Id});
        Test.stopTest();

        System.assert(!controller.fcwPrintWrappers.isEmpty());
        System.assertNotEquals(null, controller.singleFCWPrintWrapper);
        System.assert(!controller.singleFCWPrintWrapper.missingRecords);
    }

    @isTest
    private static void testCreateSpaControllerWithSpaAndWithoutAllNecessaryFields_expectMissingRecords()
    {
        School_PFS_Assignment__c spa = SchoolPfsAssignmentTestData.Instance.DefaultSchoolPfsAssignment;

        PageReference pageRef = new ApexPages.Pagereference(
            '/apex/SchoolFamilyContributionWorksheetPrint?schoolpfsassignmentid=' + spa.Id);
        Test.setCurrentPage(pageRef);

        Test.startTest();
            SchoolFamilyContribWrksheetPrintCont controller = new SchoolFamilyContribWrksheetPrintCont();
        Test.stopTest();

        System.assertNotEquals(null, controller.singleFCWPrintWrapper);
        System.assert(controller.singleFCWPrintWrapper.missingRecords);
    }

    @isTest
    private static void testCreateSpaControllerWithFolderIdAndWithoutAllNecessaryFields_expectMissingRecords()
    {
        Student_Folder__c studentFolder = StudentFolderTestData.Instance.DefaultStudentFolder;
        School_PFS_Assignment__c spa = SchoolPfsAssignmentTestData.Instance
                .forStudentFolderId(studentFolder.Id)
                .DefaultSchoolPfsAssignment;

        PageReference pageRef = new ApexPages.Pagereference(
            '/apex/SchoolFamilyContributionWorksheetPrint?folderid=' + studentFolder.Id);
        Test.setCurrentPage(pageRef);

        Test.startTest();
            SchoolFamilyContribWrksheetPrintCont controller = new SchoolFamilyContribWrksheetPrintCont();
        Test.stopTest();

        System.assertNotEquals(null, controller.singleFCWPrintWrapper);
        System.assert(controller.singleFCWPrintWrapper.missingRecords);
    }

    @isTest
    private static void testAddSiblingName()
    {
        String siblingName = 'Test';
        School_PFS_Assignment__c spa = SchoolPfsAssignmentTestData.Instance.DefaultSchoolPfsAssignment;

        SchoolFamilyContribWrksheetPrintCont.FCWPrintWrapper fcwWrapper = 
            new SchoolFamilyContribWrksheetPrintCont.FCWPrintWrapper(spa.Id, spa.Name);
        fcwWrapper.siblingNames = new List<String>();
        fcwWrapper.siblingNameSet = new Set<String>();

        Test.startTest();
            fcwWrapper.addSiblingName(siblingName);
            System.assertEquals(1, fcwWrapper.siblingNames.size());
            System.assertEquals(siblingName, fcwWrapper.siblingNames[0]);
            System.assertEquals(1, fcwWrapper.siblingNameSet.size());
            System.assert(fcwWrapper.siblingNameSet.contains(siblingName));

            fcwWrapper.addSiblingName(siblingName);
            System.assertEquals(1, fcwWrapper.siblingNames.size());
            System.assertEquals(1, fcwWrapper.siblingNameSet.size());
        Test.stopTest();
    }
}