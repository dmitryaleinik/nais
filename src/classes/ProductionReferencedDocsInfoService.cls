public class ProductionReferencedDocsInfoService
{
       
    @testvisible private static Production_referenced_Documents_Info__mdt documentInfo;

    public static String buildInternalLinkUrl(String docInfoRecordDevName) {
        String url = '';

        documentInfo = getDocumentsInfoByDevName(docInfoRecordDevName);
        
        if (documentInfo == null) {
            return url;
        }

        url = buildUrlString(documentInfo);

        return url;
    }

    private static Production_referenced_Documents_Info__mdt getDocumentsInfoByDevName(String refDocumentInfoName) {
        List<Production_referenced_Documents_Info__mdt> documentsInfo = [
            SELECT Document_Name__c, Object_Type__c, Url_Link_String__c
            FROM Production_referenced_Documents_Info__mdt
                WHERE DeveloperName = :refDocumentInfoName];

        if (documentsInfo.isEmpty()) {
            return null;
        }
        
        return documentsInfo[0];
    }

    private static String buildUrlString(Production_referenced_Documents_Info__mdt docInfo) {
        String urlString = '';

        String queryString = 'SELECT Id FROM ' + docInfo.Object_Type__c + ' WHERE ';
        
        if (docInfo.Object_Type__c.contains('Content')) {
            queryString += 'Title';
        } else {
            queryString += 'Name';
        }

        queryString += ' = \'' + docInfo.Document_Name__c + '\'';

        List<SObject> sobjectRecords = Database.query(queryString);

        if (!sobjectRecords.isEmpty()) {
            urlString = docInfo.Url_Link_String__c + sobjectRecords[0].Id;
        }

        return urlString;
    }
}