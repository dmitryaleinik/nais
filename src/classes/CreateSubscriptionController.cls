/**
  * @description Handles creating a subscription for an opportunity.
  */
public class CreateSubscriptionController {
    @testVisible private static final String REGULAR = 'Regular';
    @testVisible private static final String DISCOUNTED = 'Discounted';
    @testVisible private static final String CLOSED_WON = 'Closed Won';
    @testVisible private static final String EARLY_BIRD = 'Early Bird';
    @testVisible private static final String EARLY_BIRD_TRANSACTION_TYPE = 'Early Bird Subscription Discount';
    @testVisible private static final String SCHOOL_DISCOUNT = 'School Discount';
    @testVisible private static final String SCHOOL_SUBSCRIPTION = 'School Subscription';
    @testVisible private static final String ID_PARAM = 'id';
    @testVisible private static final String RENEWAL = 'Renewal';
    @testVisible private static final String FORWARD_SLASH = '/';

    @testVisible private ApexPages.StandardController controller;
    private Subscription__c subscription;
    private Opportunity opportunity;

    /**
     * @description The user defined Subscription Type that has been chosen:
     *              either Regular or Discounted. Defaults to Regular.
     */
    public String SubscriptionType {
        get {
            if (SubscriptionType == null) {
                SubscriptionType = REGULAR;
            }
            return SubscriptionType;
        }
        set;
    }

    /**
     * @description The School Discount Transaction Line Item that is being
     *              generated based on a user defined discounted subscription.
     */
    public Transaction_Line_Item__c SchoolDiscount {
        get {
            if (SchoolDiscount == null) {
                List<Transaction_Line_Item__c> existingDiscounts = getExistingNonEarlyBirdDiscounts();

                if (existingDiscounts.isEmpty()) {
                    SchoolDiscount = new Transaction_Line_Item__c(
                            Transaction_Type__c = SCHOOL_DISCOUNT,
                            Opportunity__c = getOpportunity().Id,
                            RecordTypeId = RecordTypes.schoolDiscountTransactionTypeId,
                            Transaction_Date__c = System.today());
                } else {
                    SchoolDiscount = existingDiscounts[0];
                }
            }
            return SchoolDiscount;
        }
        set;
    }

    /**
     * @description Returns true if the Account has a valid current subscription
     *              for the defined Academic Year on the opportunity, else false.
     */
    public Boolean HasCurrentSubscription {
        get {
            return getCurrentSubscription() != null;
        }
    }

    /**
     * @description Returns true if the Opportunity has an Academic Year defined
     *              on it, else false.
     */
    public Boolean HasAcademicYear {
        get {
            return String.isNotBlank(getOpportunity().Academic_Year_Picklist__c);
        }
    }

    /**
     * @description Return strue if there is no Academic Year record associated
     *              with the Academic Year Picklist of the Opportunity does not
     *              exist, else false.
     */
    public Boolean AcademicYearRecordMissing {
        get {
            String academicYear = getOpportunity().Academic_Year_Picklist__c;
            if (String.isBlank(academicYear)) {
                return true;
            }

            return ([SELECT Id FROM Academic_Year__c WHERE Name = :academicYear]).size() == 0;
        }
    }

    /**
     * @description The current Academic Year that will be used by JavaScript to
     *              determine the valid Product Codes to allow the user to choose
     *              from.
     */
    public String ProductCodeAcademicYear {
        get {
            return getOpportunity().Academic_Year_Picklist__c.left(4) + '-' +
                   getOpportunity().Academic_Year_Picklist__c.right(2);
        }
    }

    /**
     * @description Deliver the pre-defined subscription type options available to
     *              the end user, Regular or Discounted.
     * @return A list of SelectOptions that allows the user to choose which type
     *         of Subscription they would like to generate: Regular or Discounted.
     */
    public List<SelectOption> getSubscriptionTypeOptions() {
        return new List<SelectOption> {
                new SelectOption(REGULAR, REGULAR),
                new SelectOption(DISCOUNTED, DISCOUNTED)
        };
    }

    /**
     * @description Returns the user back to the Opportunity page that is
     *              associated with the Opportunity record in the standard
     *              controller. This occurs when the user hits cancel or
     *              when there is no payment required after subscription
     *              generation.
     * @return A PageReference to the Opportunity standard page for the
     *         current record.
     */
    public PageReference returnToOpportunity() {
        return new PageReference(FORWARD_SLASH + getOpportunity().Id);
    }

    /**
     * @description Constructor that saves the standard controller for later
     *              processing and determines if the Opportunity is properly
     *              configured.
     * @param standardController The standard Opportunity controller for this
     *        Opportunity record's page.
     */
    public CreateSubscriptionController(ApexPages.StandardController standardController) {
        this.controller = standardController;

        if (!HasAcademicYear) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.NoAcademicYearDefined));
        } else if (AcademicYearRecordMissing) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,
                    String.format(Label.AcademicYearRecordMissing, new List<String> {
                            getOpportunity().Academic_Year_Picklist__c })));
        }

        if (HasCurrentSubscription) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, getSubscriptionExistsErrorMessage()));
        }
    }

    /**
     * @description Handles generating the Transaction Line Items for the Opportunity's
     *              Subscription.
     * @return A PageReference that will either be back to the Opportunity if there
     *         is no payment needed, the Internal Payment page if there is payment
     *         needed, or null if there is an error.
     */
    public PageReference generateTransactions() {
        List<Transaction_Line_Item__c> transactions = new List<Transaction_Line_Item__c>();

        // Create Sale transaction if it doesn't already exist.
        if (!hasExistingSaleTransactionLineItem()) {
            transactions.add(TransactionLineItemUtils.createSaleTLI(getOpportunity(), false));
        }

        // If there are existing discounts assigned to the opportunity but the
        // user has chosen a regular subscription. Get rid of those discounts.
        if (SubscriptionType == REGULAR && SchoolDiscount.Id != null) {
            delete SchoolDiscount;
            SchoolDiscount = null;
            existingNonEarlyBirdDiscounts = null;
        }

        if (SubscriptionType == REGULAR &&
            getOpportunity().New_Renewal__c == RENEWAL &&
            GlobalVariables.isEarlyBirdRenewal()) {

            String academicYear = getOpportunity().Academic_Year_Picklist__c.left(4);

            // Create the discount transaction for early bird renewals
            Decimal discountAmount = TransactionLineItemUtils.getSchoolSubscriptionDiscountTLIAmount(
                    SubscriptionType, EARLY_BIRD, academicYear);

            Transaction_Line_Item__c earlyBirdTransaction = TransactionLineItemUtils.createSchoolEarlyBirdDiscountTLI(
                    getOpportunity(), discountAmount, false);

            if (earlyBirdTransaction != null) {
                transactions.add(earlyBirdTransaction);
            }
        } else if (SubscriptionType == DISCOUNTED) {
            if (String.isBlank(SchoolDiscount.Product_Code__c)) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.MustChooseProductCode));
                return null;
            } else if (SchoolDiscount.Product_Amount__c == null) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.TLI_Has_Empty_Unit_Price));
                return null;
            }

            SchoolDiscount.Amount__c = Decimal.valueOf(SchoolDiscount.Product_Amount__c);

            transactions.add(SchoolDiscount);
        }

        if (!transactions.isEmpty()) {
            upsert transactions;
        }

        // SFP-1531: Make sure that any opportunity that has a net amount of $0 due is closed. This will cause the subscription to be generated.
        if (getOpportunity(true).Net_Amount_Due__c == 0)
        {
            update new Opportunity(Id = opportunity.Id, StageName = CLOSED_WON);
        }

        // Send the user to the Internal Payment page if there is a balance due.
        if (getOpportunity(true).Net_Amount_Due__c > 0) {
            PageReference paymentPage = Page.InternalPayment;
            paymentPage.getParameters().put(ID_PARAM, getOpportunity().Id);
            paymentPage.setRedirect(true);

            return paymentPage;
        }

        return returnToOpportunity();
    }

    /**
     * @description Determine whether the Create Submission page should keep
     *              the user from submitting the subscription creation form.
     *              This is true when the user has selected the discounted
     *              subscription type and has not chosen a product code.
     * @return True if the user has chosen the discounted subscription type
     *         but has not yet chosen a product code, otherwise false.
     */
    public Boolean getBlockSubmission() {
        return SubscriptionType == DISCOUNTED && String.isBlank(SchoolDiscount.Product_Code__c);
    }

    private Subscription__c getCurrentSubscription() {
        if (subscription != null) {
            return subscription;
        }

        // If there is no Academic Year defined on the opportunity then we cannot
        // possibly verify that there is a valid current subscription for it.
        String academicYear = getOpportunity().Academic_Year_Picklist__c;
        if (String.isBlank(academicYear)) {
            return null;
        }

        String startYear = academicYear.left(4);
        if (String.isBlank(startYear)) {
            return null;
        }

        // The CALENDAR_YEAR SOQL keyword requires an integer, so let's square that away
        Integer opportunitySubscriptionStartDate = Integer.valueOf(startYear);
        Id accountId = getOpportunity().AccountId;

        if (opportunitySubscriptionStartDate == null) {
            return null;
        }

        List<Subscription__c> subscriptions =
                [SELECT
                        Name,
                        Start_Date__c,
                        End_Date__c
                   FROM Subscription__c
                  WHERE Account__c = :accountId
                    AND CALENDAR_YEAR(End_Date__c) = :opportunitySubscriptionStartDate
                  LIMIT 1];

        if (!subscriptions.isEmpty()) {
            subscription = subscriptions[0];
        }

        return subscription;
    }

    private List<Transaction_Line_Item__c> existingNonEarlyBirdDiscounts;
    private List<Transaction_Line_Item__c> getExistingNonEarlyBirdDiscounts() {
        if (existingNonEarlyBirdDiscounts == null) {
            existingNonEarlyBirdDiscounts = TransactionLineItemSelector.Instance.selectByOpportunityAndRecordType(
                    new Set<Id>{ getOpportunity().Id },
                    new Set<Id>{ RecordTypes.schoolDiscountTransactionTypeId });

            for (Integer i = existingNonEarlyBirdDiscounts.size()-1, l = 0; i >= l; i--) {
                if (existingNonEarlyBirdDiscounts[i].Transaction_Type__c == EARLY_BIRD_TRANSACTION_TYPE) {
                    existingNonEarlyBirdDiscounts.remove(i);
                }
            }
        }

        return existingNonEarlyBirdDiscounts;
    }

    @testVisible
    private String getSubscriptionExistsErrorMessage() {
        if (HasCurrentSubscription) {
            return String.format(Label.SubscriptionAlreadyExists, new List<String>{
                    String.valueOf(getCurrentSubscription().Start_Date__c.year()),
                    '<a href=\'/' + getCurrentSubscription().Id + '\'>' + getCurrentSubscription().Name + '</a>'
            });
        }

        return null;
    }

    @testVisible
    private Opportunity getOpportunity() {
        return getOpportunity(false);
    }

    @testVisible
    private Opportunity getOpportunity(Boolean requery) {
        if (opportunity == null || requery) {
            Id opportunityId = controller.getRecord().Id;
            Id subscriptionFeeRecordTypeId = RecordTypes.opportunitySubscriptionFeeTypeId;

            List<Opportunity> opportunities = [SELECT
                    Academic_Year_Picklist__c,
                    AccountId,
                    Name,
                    Net_Amount_Due__c,
                    New_Renewal__c,
                    RecordTypeId,
                    Subscription_Period_Start_Year__c,
                    Subscription_Type__c,
                    StageName
            FROM Opportunity
            WHERE Id = :opportunityId
              AND RecordTypeId = :subscriptionFeeRecordTypeId
            LIMIT 1];

            if (!opportunities.isEmpty()) {
                opportunity = opportunities[0];
            }
        }

        return opportunity;
    }

    private Boolean hasExistingSaleTransactionLineItem() {
        Opportunity currentOpportunity = getOpportunity();
        Id recordTypeId = RecordTypes.saleTransactionTypeId;

        List<Transaction_Line_Item__c> existingTransactions = [SELECT
                                                                      Account_Code__c,
                                                                      Account_Label__c,
                                                                      Amount__c,
                                                                      Opportunity__c,
                                                                      Product_Amount__c,
                                                                      Product_Code__c,
                                                                      RecordTypeId,
                                                                      Transaction_Code__c,
                                                                      Transaction_Date__c,
                                                                      Transaction_Status__c,
                                                                      Transaction_Type__c
                                                                 FROM Transaction_Line_Item__c
                                                                WHERE Opportunity__c = :currentOpportunity.Id
                                                                  AND RecordTypeId = :recordTypeId
                                                                  AND Transaction_Type__c = :SCHOOL_SUBSCRIPTION];

        if (!existingTransactions.isEmpty()) {
            return true;
        }

        return false;
    }
}