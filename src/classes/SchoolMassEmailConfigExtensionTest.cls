/**
 * SchoolMassEmailConfigExtensionTest.cls
 *
 * @description: Test class for SchoolMassEmailConfigExtension using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
private class SchoolMassEmailConfigExtensionTest {

    /* positive test cases */

    // test controller init w/ header/academic year selector
    @isTest 
    static void constructor_academicYearSelector_validInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            Test.startTest();
                
                PageReference massEmailConfigPref = Page.SchoolMassEmailConfig;
                Test.setCurrentPage( massEmailConfigPref);
                SchoolMassEmailConfigExtension massEmailConfigExt = 
                    new SchoolMassEmailConfigExtension( new ApexPages.StandardController( new Account()));
                massEmailConfigExt.loadAcademicYear();
                massEmailConfigExt.SchoolAcademicYearSelector_OnChange( null);
            Test.stopTest();

            // Assert
            System.assertEquals( massEmailConfigExt.Me, massEmailConfigExt);
            System.assertEquals( massEmailConfigExt.getAcademicYearId(), GlobalVariables.getCurrentAcademicYear().Id);
        }
    }

    // test constructor w/ init
    @isTest 
    static void init_pageLoad_validInit() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            Test.startTest();
                
                PageReference massEmailConfigPref = Page.SchoolMassEmailConfig;
                Test.setCurrentPage( massEmailConfigPref);
                SchoolMassEmailConfigExtension massEmailConfigExt = 
                    new SchoolMassEmailConfigExtension( new ApexPages.StandardController( new Account()));
                Boolean enabled = massEmailConfigExt.massEmailEnabled;
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, massEmailConfigExt.acctModel);
        }
    }

    // test save changes
    @isTest 
    static void saveChanges_save_validSave() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            Test.startTest();
                
                PageReference massEmailConfigPref = Page.SchoolMassEmailConfig;
                Test.setCurrentPage( massEmailConfigPref);
                SchoolMassEmailConfigExtension massEmailConfigExt = 
                    new SchoolMassEmailConfigExtension( new ApexPages.StandardController( new Account()));
                massEmailConfigExt.saveChanges();
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, massEmailConfigExt.acctModel);
        }
    }

    // test save changes with logo
    @isTest 
    static void saveChanges_saveLogo_validSave() {
        
        // Arrange
        MassEmailSendTestDataFactory tData;
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
         
            tData = new MassEmailSendTestDataFactory( false);
        }

        // Act
        System.runAs( tData.portalUser) {
            
            Test.startTest();
                
                PageReference massEmailConfigPref = Page.SchoolMassEmailConfig;
                Test.setCurrentPage( massEmailConfigPref);
                SchoolMassEmailConfigExtension massEmailConfigExt = 
                    new SchoolMassEmailConfigExtension( new ApexPages.StandardController( new Account()));
                massEmailConfigExt.attachment = new Attachment();
                massEmailConfigExt.attachment.Name = 'My Attachment';
                massEmailConfigExt.attachment.Body = Blob.valueOf( 'My Body');
                massEmailConfigExt.saveChanges();
            Test.stopTest();

            // Assert
            System.assertNotEquals( null, massEmailConfigExt.acctModel);
        }
    }

}