/**
* @FamilyDocumentCleanupScheduler.cls
* @date 3/6/2014
* @author  CH for Exponent Partners
* @description Schedulable class for cleaning up orphaned Family Document records
*/

public with sharing class FamilyDocumentCleanupScheduler implements Schedulable{
    
    public static void doDeletes(){
        // Look up lifespan custom setting
        Document_Cleanup__c docCleanupSetting = Document_Cleanup__c.getValues('Family Document Life');
        Integer lifespan;
        Integer querySize;
        
        if(docCleanupSetting != null){
            lifespan = Integer.valueOf(docCleanupSetting.Number_Value__c);
            querySize = Integer.valueOf(docCleanupSetting.Query_Size__c);  
        }
        else{
            lifespan = 1;
            querySize = 2; 
        }
        
        // Query for orphaned Family Document records that have passed their lifespan
        Map<Id, Family_Document__c> familyDocsToDelete = new Map<Id, Family_Document__c>([select Id, (select Id from School_Document_Assignments__r where Required_Document__c = null and PFS_Specific__c != true) 
                                                                                            from Family_Document__c 
                                                                                            where Document_Status__c = 'Not Received' 
                                                                                            and CreatedDate <= :DateTime.now().addDays(-lifespan)
                                                                                            limit :querySize]);            
            
        // Compile the list of School Document Assignment records to delete
        List<School_Document_Assignment__c> sdRecordsToDelete = new List<School_Document_Assignment__c>();                                                                                
        for(Family_Document__c fdRecord : familyDocsToDelete.values()){
            sdRecordsToDelete.addAll(fdRecord.School_Document_Assignments__r);
        }
        
        // Delete SDA records related to doomed Family Documents
        delete sdRecordsToDelete;
        
        // Delete the list of Family Documents
        List<Id> idsToDelete = new List<Id>{};
        idsToDelete.addAll(familyDocsToDelete.keySet()); 
        Database.delete(idsToDelete);
    }
    

    public void execute(SchedulableContext SC) { 
        doDeletes();
    }

}