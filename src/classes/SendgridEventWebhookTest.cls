/**
 * SendgridEventWebhookTest.cls
 *
 * @description: Test class for SendgridEventWebhook using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
 *
 * @author: Chase Logan @ Presence PG
 */
@isTest (seeAllData = false)
private class SendgridEventWebhookTest {
    
    /* test data setup */
    @testSetup 
    static void setupTestData() {
        MassEmailSendTestDataFactory.createEmailTemplate();
        
        User testUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs( testUser) {
            
            MassEmailSendTestDataFactory massESTestDataFactory = new MassEmailSendTestDataFactory();
        }
    }
    
    /* negative test cases */

    // Test an empty call to handleSendgridEvent endpoint
    @isTest 
    static void handleSendgridEvent_emptyRequest_noUpdate() {
        
        // Arrange
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'services/apexrest/SendgridEvent';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        String reqString = '[ {} ]';
        Blob myBlob = Blob.valueOf( reqString);

        // Act
        Test.startTest();
            req.requestBody = myBlob;
            SendgridEventWebhook.handleSendgridEvent();
        Test.stopTest();

        // Assert
        // no work
    }

    // Test a call to handleSendgridEvent without school Id
    @isTest 
    static void handleSendgridEvent_invalidRequest_noUpdate() {

        // Arrange
        MassEmailSendTestDataFactory.createRequiredPFSData();
        Mass_Email_Send__c massES = [select Id, School__c, School_PFS_Assignments__c from Mass_Email_Send__c where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
        Student_Folder__c sF = [select Id from Student_Folder__c where Name = :MassEmailSendTestDataFactory.TEST_STUDENT_FOLDER1_NAME];
        School_PFS_Assignment__c sPFS = [select Id from School_PFS_Assignment__c where Student_Folder__c = :sF.Id];

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'services/apexrest/SendgridEvent';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        String reqString = '[ { "useragent" : null, "url_offset" : null, "url" : null, "tls" : "1", "timestamp" : 1470265697,' +
                              ' "spa_id" : "' + sPFS.Id + '",' +
                              ' "sg_message_id" : "YZd9kEjRTQCHm9oov7We-A.filter0858p1mdw1.24793.57A2795C41.0",' +
                              ' "sg_event_id" : "_k7Ja7M1R7CKIi2K84mOQA",' +
                              ' "sender_id" : null,' +
                              ' "send_at" : null,' +
                              ' "school_id" : null,' +
                              ' "response" : "250 2.0.0 OK 1470265697 w198si9978666iow.134 - gsmtp ",' +
                              ' "recipient_id" : "003J000001Nc0ZAIAZ",' +
                              ' "reason" : null,' +
                              ' "mass_email_send_id" : "' + massES.Id + '",' +
                              ' "ip" : "167.89.55.41",' +
                              ' "event" : "delivered",' +
                              ' "email" : "email@gmail.com",' +
                              ' "cert_err" : null,' +
                              ' "attempt" : null,' +
                              ' "asm_group_id" : null,' +
                              ' "academic_year" : "2016-2017"' +
                            '} ]';
        Blob myBlob = Blob.valueOf( reqString);

        // Act
        Test.startTest();
            req.requestBody = myBlob;
            SendgridEventWebhook.handleSendgridEvent();
        Test.stopTest();

        // Assert
        sPFS = [select Id, Mass_Email_Events__c from School_PFS_Assignment__c where Student_Folder__c = :sF.Id];
        System.assertEquals( null, sPFS.Mass_Email_Events__c);
    }

    /* positive test cases */
    
    // Test a valid call to handleSendgridEvent
    @isTest 
    static void handleSendgridEvent_validRequest_validResult() {

        // Arrange
        MassEmailSendTestDataFactory.createRequiredPFSData();
        Mass_Email_Send__c massES = [select Id, School__c from Mass_Email_Send__c where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
        Student_Folder__c sF = [select Id from Student_Folder__c where Name = :MassEmailSendTestDataFactory.TEST_STUDENT_FOLDER1_NAME];
        School_PFS_Assignment__c sPFS = [select Id from School_PFS_Assignment__c where Student_Folder__c = :sF.Id];
        Contact c = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_PARENT1_CONTACT_EMAIL];

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'services/apexrest/SendgridEvent';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        String reqString = '[ { "useragent" : null, "url_offset" : null, "url" : null, "tls" : "1", "timestamp" : 1470265697,' +
                              ' "spa_id" : "' + sPFS.Id + '",' +
                              ' "sg_message_id" : "YZd9kEjRTQCHm9oov7We-A.filter0858p1mdw1.24793.57A2795C41.0",' +
                              ' "sg_event_id" : "_k7Ja7M1R7CKIi2K84mOQA",' +
                              ' "sender_id" : null,' +
                              ' "send_at" : null,' +
                              ' "school_id" : "' + massES.School__c + '",' +
                              ' "response" : "250 2.0.0 OK 1470265697 w198si9978666iow.134 - gsmtp ",' +
                              ' "recipient_id" : "' + c.Id + '",' +
                              ' "reason" : null,' +
                              ' "mass_email_send_id" : "' + massES.Id + '",' +
                              ' "ip" : "167.89.55.41",' +
                              ' "event" : "delivered",' +
                              ' "email" : "email@gmail.com",' +
                              ' "cert_err" : null,' +
                              ' "attempt" : null,' +
                              ' "asm_group_id" : null,' +
                              ' "academic_year" : "2016-2017"' +
                            '} ]';
        Blob myBlob = Blob.valueOf( reqString);

        // Act
        Test.startTest();
            req.requestBody = myBlob;
            SendgridEventWebhook.handleSendgridEvent();
        Test.stopTest();

        // Assert
        sPFS = [select Id, Mass_Email_Events__c from School_PFS_Assignment__c where Student_Folder__c = :sF.Id];
        System.assertNotEquals( null, sPFS.Mass_Email_Events__c);
    } 

    // Test a valid call to handleSendgridEvent w/ multiple events for a single parent
    @isTest 
    static void handleSendgridEvent_validRequestMultipleEventsPerParent_validResult() {

        // Arrange
        MassEmailSendTestDataFactory.createRequiredPFSData();
        Mass_Email_Send__c massES = [select Id, School__c from Mass_Email_Send__c where Name = :MassEmailSendTestDataFactory.TEST_MASS_EMAIL1_NAME];
        Student_Folder__c sF = [select Id from Student_Folder__c where Name = :MassEmailSendTestDataFactory.TEST_STUDENT_FOLDER1_NAME];
        School_PFS_Assignment__c sPFS = [select Id from School_PFS_Assignment__c where Student_Folder__c = :sF.Id];
        Contact c = [select Id from Contact where Email = :MassEmailSendTestDataFactory.TEST_PARENT1_CONTACT_EMAIL];

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'services/apexrest/SendgridEvent';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        String reqString = '[ { "useragent" : null, "url_offset" : null, "url" : null, "tls" : "1", "timestamp" : 1470265697,' +
                              ' "spa_id" : "' + sPFS.Id + '",' +
                              ' "sg_message_id" : "YZd9kEjRTQCHm9oov7We-A.filter0858p1mdw1.24793.57A2795C41.0",' +
                              ' "sg_event_id" : "_k7Ja7M1R7CKIi2K84mOQA",' +
                              ' "sender_id" : null,' +
                              ' "send_at" : null,' +
                              ' "school_id" : "' + massES.School__c + '",' +
                              ' "response" : "250 2.0.0 OK 1470265697 w198si9978666iow.134 - gsmtp ",' +
                              ' "recipient_id" : "' + c.Id + '",' +
                              ' "reason" : null,' +
                              ' "mass_email_send_id" : "' + massES.Id + '",' +
                              ' "ip" : "167.89.55.41",' +
                              ' "event" : "delivered",' +
                              ' "email" : "email@gmail.com",' +
                              ' "cert_err" : null,' +
                              ' "attempt" : null,' +
                              ' "asm_group_id" : null,' +
                              ' "academic_year" : "2016-2017"' +
                            '},' +
                            '{ "useragent" : null, "url_offset" : null, "url" : null, "tls" : "1", "timestamp" : 1470265697,' +
                              ' "spa_id" : "' + sPFS.Id + '",' +
                              ' "sg_message_id" : "YZd9kEjRTQCHm9oov7We-A.filter0858p1mdw1.24793.57A2795C41.0",' +
                              ' "sg_event_id" : "_k7Ja7M1R7CKIi2K84mOQA",' +
                              ' "sender_id" : null,' +
                              ' "send_at" : null,' +
                              ' "school_id" : "' + massES.School__c + '",' +
                              ' "response" : "250 2.0.0 OK 1470265697 w198si9978666iow.134 - gsmtp ",' +
                              ' "recipient_id" : "' + c.Id + '",' +
                              ' "reason" : null,' +
                              ' "mass_email_send_id" : "' + massES.Id + '",' +
                              ' "ip" : "167.89.55.41",' +
                              ' "event" : "opened",' +
                              ' "email" : "email@gmail.com",' +
                              ' "cert_err" : null,' +
                              ' "attempt" : null,' +
                              ' "asm_group_id" : null,' +
                              ' "academic_year" : "2016-2017"' +
                            '} ]';
        Blob myBlob = Blob.valueOf( reqString);

        // Act
        Test.startTest();
            req.requestBody = myBlob;
            SendgridEventWebhook.handleSendgridEvent();
        Test.stopTest();

        // Assert
        sPFS = [select Id, Mass_Email_Events__c from School_PFS_Assignment__c where Student_Folder__c = :sF.Id];
        System.assertNotEquals( null, sPFS.Mass_Email_Events__c);
    } 
    
}