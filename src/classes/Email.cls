/**
 * Email.cls
 *
 * @description: Represents a single Email message.
 *
 * @author: Chase Logan @ Presence PG
 */
public class Email {
    
    /* properties, lazy loaded where applicable */
    public List<String> toAddresses {
        get { return ( this.toAddresses == null ? this.toAddresses = new List<String>() : this.toAddresses); }
        set {
            this.toAddresses = value;
        }
    }
    public List<String> fromAddresses {
        get { return ( this.fromAddresses == null ? this.fromAddresses = new List<String>() : this.fromAddresses); }
        set {
            this.fromAddresses = value;
        }
    }
    public String messageBody {
        get {
            return ( !String.isEmpty( this.messageBody) ? EncodingUtil.urlEncode( this.messageBody, 'UTF-8') : '');
        }
        set {
            this.messageBody = value;
        }
    }
    public String messageSubject {
        get {
            return ( !String.isEmpty( this.messageSubject) ? EncodingUtil.urlEncode( this.messageSubject, 'UTF-8') : '');
        }
        set {
            this.messageSubject = value;
        }
    }
    public Id schoolId {
        get;
        set {
            this.schoolId = value;
        }
    }
    public Id senderId {
        get;
        set {
            this.senderId = value;
        }
    }
    public Id recipientId {
        get;
        set {
            this.recipientId = value;
        }
    }
    public Id spaId {
        get; 
        set {
            this.spaId = value;
        }
    }
    public Id massEmailSendId {
        get; 
        set {
            this.massEmailSendId = value;
        }
    }
    public String academicYear {
        get;
        set {
            this.academicYear = value;
        }
    }

    // default ctor
    public Email() {}

    // overloaded ctor to populate required fields on instantiation
    public Email( List<String> toList, List<String> fromList, String subject, String body) {

        this.toAddresses = ( toList != null && toList.size() > 0 ? toList : this.toAddresses);
        this.fromAddresses = ( fromList != null && fromList.size() > 0 ? fromList : this.fromAddresses);
        this.messageSubject = ( subject != null ? subject : '');
        this.messageBody = ( body != null ? body : '');
    }

    // overloaded ctor to populate required fields and extra parameters on instantiation
    public Email( List<String> toList, List<String> fromList, String subject, String body,
                  Id schoolId, Id senderId, Id recipId, Id spaId, Id massEmailId, String academicYear) {

        this.toAddresses = ( toList != null && toList.size() > 0 ? toList : this.toAddresses);
        this.fromAddresses = ( fromList != null && fromList.size() > 0 ? fromList : this.fromAddresses);
        this.messageSubject = ( subject != null ? subject : '');
        this.messageBody = ( body != null ? body : '');

        this.schoolId = ( schoolId != null ? schoolId : this.schoolId);
        this.senderId = ( senderId != null ? senderId : this.senderId);
        this.recipientId = ( recipId != null ? recipId : this.recipientId);
        this.spaId = ( spaId != null ? spaId : this.spaId);
        this.massEmailSendId = ( massEmailId != null ? massEmailId : this.massEmailSendId);
        this.academicYear = ( academicYear != null ? academicYear : this.academicYear);
    }

}