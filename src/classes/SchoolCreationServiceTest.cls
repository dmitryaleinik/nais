@isTest
private with sharing class SchoolCreationServiceTest
{

    @isTest
    private static void  testSetSSSCodesWithFeatureTogglesAutoGenerateSSSCodesFalse()
    {
        List<Account> schoolsAndAccessOrgs = new List<Account>();
        Integer numberOfSchools = 5;
        Integer numberOfAccessOrgs = 5;
        Integer lastSSSCode = 200000;
        Account_Settings__c accountSettings = AccountSettingsTestData.Instance.forLastSSSCode(lastSSSCode).DefaultAccountSettings;
        FeatureToggles.setToggle('Auto_Generate_SSS_Codes__c', false);

        for(Integer i = 0; i < numberOfSchools; i++)
        {
            schoolsAndAccessOrgs.add(AccountTestData.Instance.asSchool().create());
        }

        for(Integer i = 0; i < numberOfAccessOrgs; i++)
        {
            schoolsAndAccessOrgs.add(AccountTestData.Instance.asAccessOrganisation().create());
        }

        Test.startTest();
            insert schoolsAndAccessOrgs;
        Test.stopTest();

        System.assertEquals(200000, lastSSSCode, 'expect that last sss code was not increased and the custom setting was not updated');

        schoolsAndAccessOrgs = AccountSelector.Instance.selectSchoolsAndAccessOrgsWithHigherSSSCodes(lastSSSCode);
        System.assert(schoolsAndAccessOrgs.isEmpty(), 'expect to not have any new generated sss codes on accounts');
    }

    @isTest
    private static void testSetSSSCodesWithoutExistingHigherSSSCodes()
    {
        List<Account> schoolsAndAccessOrgs = new List<Account>();
        Integer numberOfSchools = 5;
        Integer numberOfAccessOrgs = 5;
        Integer lastSSSCode = 200000;
        Account_Settings__c accountSettings = AccountSettingsTestData.Instance
                .forLastSSSCode(lastSSSCode).DefaultAccountSettings;
        FeatureToggles.setToggle('Auto_Generate_SSS_Codes__c', true);

        for(Integer i = 0; i < numberOfSchools; i++)
        {
            schoolsAndAccessOrgs.add(AccountTestData.Instance.asSchool().create());
        }

        for(Integer i = 0; i < numberOfAccessOrgs; i++)
        {
            schoolsAndAccessOrgs.add(AccountTestData.Instance.asAccessOrganisation().create());
        }

        Test.startTest();
            insert schoolsAndAccessOrgs;
        Test.stopTest();


        schoolsAndAccessOrgs = AccountSelector.Instance.selectById(new Map<Id, Account>(schoolsAndAccessOrgs).keySet(),
                new Set<String>{'SSS_School_Code__c'});

        Set<String> expectedCodes = new Set<String>{'200001', '200002', '200003', '200004', '200005'
                , '200006', '200007', '200008', '200009', '200010'};
        System.assertEquals(200010, Account_Settings__c.getValues(accountSettings.Name).Last_SSS_Code__c);

        Set<String> generatedCodes = new Set<String>{};
        for (Account acc : schoolsAndAccessOrgs)
        {
            generatedCodes.add(acc.SSS_School_Code__c);
        }
        System.assert(expectedCodes.containsAll(generatedCodes));
    }

    @isTest
    private static void  testSetSSSCodesWithExistingHigherSSSCodes()
    {
        List<String> existingSSSCodes = new List<String>{'200004', '200005', '200006',
                '200008', '200010', '200011', '200013'};
        List<Account> existingSchools = new List<Account>();
        for (Integer i = 0; i < existingSSSCodes.size(); i++)
        {
            existingSchools.add(AccountTestData.Instance.asSchool().forSSSSchoolCode(existingSSSCodes[i]).create());
        }
        insert existingSchools;

        Integer lastSSSCode = 200005;
        Account_Settings__c accountSettings = AccountSettingsTestData.Instance
                .forLastSSSCode(lastSSSCode).DefaultAccountSettings;
        FeatureToggles.setToggle('Auto_Generate_SSS_Codes__c', true);

        Integer numberOfSchools = 5;
        Integer numberOfAccessOrgs = 5;
        List<Account> newSchoolsAndAccessOrgs = new List<Account>();
        for(Integer i = 0; i < numberOfSchools; i++)
        {
            newSchoolsAndAccessOrgs.add(AccountTestData.Instance.asSchool().create());
        }

        for(Integer i = 0; i < numberOfAccessOrgs; i++)
        {
            newSchoolsAndAccessOrgs.add(AccountTestData.Instance.asAccessOrganisation().create());
        }

        Test.startTest();
            insert newSchoolsAndAccessOrgs;
        Test.stopTest();

        newSchoolsAndAccessOrgs = AccountSelector.Instance.selectById(new Map<Id, Account>(newSchoolsAndAccessOrgs)
                .keySet(), new Set<String>{'SSS_School_Code__c'});

        System.assertEquals(200020, Account_Settings__c.getValues(accountSettings.Name).Last_SSS_Code__c);

        Set<String> generatedCodes = new Set<String>{};
        for (Account newSchool : newSchoolsAndAccessOrgs)
        {
            generatedCodes.add(newSchool.SSS_School_Code__c);
        }
        Set<String> expectedSSSCodes = new Set<String>{'200007', '200009', '200012', '200014', '200015',
                    '200016', '200017', '200018', '200019', '200020'};
        System.assert(expectedSSSCodes.containsAll(generatedCodes));
    }
}