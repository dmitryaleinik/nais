public with sharing class SchoolPicker {
    
    /*Initialization*/
    public SchoolPicker(){
        u = GlobalVariables.getCurrentUser();
        
        // put in wrappers in case user doesn't have access to Account
        schoolWrappers = new List<AcctWrapper>();
        // NAIS-1713 Alphabetized list of Schools Start
        List<Account> accounts = GlobalVariables.getAllSchoolsForUser();
        Map<String, Account> accountMap = New Map<String, Account>();
        //List<String> sortThis = new List<String>();
        
        for(Account a: accounts){
            accountMap.put(a.Name, a);
            //sortThis.add(a.Name);
        }
        /*
        sortThis.sort();
        List<Account> nameSortedAccounts = new List<Account>();
        for(String s: sortThis){
            nameSortedAccounts.add(accountMap.get(s));
        }
        
        for (Account a : nameSortedAccounts){
            schoolWrappers.add(new AcctWrapper(a));
        }
        */
        for(String key: getSortedKeyset(accountMap)){
            schoolWrappers.add(new AcctWrapper(accountMap.get(key)));
        }
        // NAIS-1713 End
    }
   
    /*End Initialization*/
   
    /*Properties*/
    public User u {get; set;}
    public Id selectedSchoolId {get; set;}
    public List<AcctWrapper> schoolWrappers {get; set;}
    /*End Properties*/
       
    /*Action Methods*/
    // get id from page, write it to User record, redirect to dashboard
    public pageReference setInFocusSchoolAndRedirect(){
        selectedSchoolId = System.currentPagereference().getParameters().get('selectedSchoolId');
        
        if (selectedSchoolId == null){
            System.assert(false);
        }
        
        if (u.In_Focus_School__c != selectedSchoolId){
            u.In_Focus_School__c = selectedSchoolId;
            update u;
        }
        
        return Page.SchoolDashboard;
    }
    //NAIS-1713 Start
    public  List<String> getSortedKeyset(Map<String, Account> dataMap) {
        List<String> keySetList = new List<String>();
        keySetList.addAll(dataMap.keySet());
        keySetList.sort();
        return keySetList;
    }
    //NAIS-1713 End
    /*End Action Methods*/
   
    /*Helper Methods*/
   
    /*End Helper Methods*/
    
    /*Internal Class*/
    public class AcctWrapper{
        public String acctName {get; set;}
        public String acctId {get; set;}
        
        public AcctWrapper(Account a){
            acctName = a.Name;
            acctId = a.Id;
        }
    }
    /*End Internal Class*/
    
    
    /***************************************************************************
    START UNIT TESTS
    ***************************************************************************/
    
}