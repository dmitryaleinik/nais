/**
 * SchoolPFSAssignmentDataAccessService.cls
 *
 * @description Data access service class for School_PFS_Assignment__c
 *
 * @author Chase Logan @ Presence PG
 */
public class SchoolPFSAssignmentDataAccessService extends DataAccessService {
    
    /**
     * @description: Queries a School_PFS_Assignment__c record by ID, queries all fields by default because of the direct query of
     * a singe record by ID limited to 1
     *
     * @param spaId - The ID of the School_PFS_Assignment__c record
     *
     * @return A School_PFS_Assignment__c record
     */
    public School_PFS_Assignment__c getSchoolPFSAssignmentById( Id spaId) {
        School_PFS_Assignment__c returnVal;

        if ( spaId != null) {

            try {

                returnVal = Database.query( 'select ' + String.join( super.getSObjectFieldNames( 'School_PFS_Assignment__c'), ',') + ',' +
                                                  ' Applicant__r.PFS__r.Parent_A_First_Name__c, Applicant__r.PFS__r.Parent_A_Last_Name__c' +
                                             ' from School_PFS_Assignment__c' + 
                                            ' where Id = :spaId' + 
                                            ' limit 1');
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in SchoolPFSAssignmentDataAccessService.getSchoolPFSAssignmentById' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }

        return returnVal;
    }

    /**
     * @description: Queries School_PFS_Assignment__c.Mass_Email_Events__c by Id set
     *
     * @param spaIdSet - The Set of School_PFS_Assignment__c ID's
     *
     * @param lockRecords - a boolean indicating whether or not to apply record lock on returned records
     *
     * @return A list of School_PFS_Assignment__c records
     */
    public List<School_PFS_Assignment__c> getSPAEmailEventByIdSet( Set<String> spaIdSet, Boolean lockRecords) {
        List<School_PFS_Assignment__c> returnList;

        if ( spaIdSet != null) {

            try {

                returnList = Database.query( 'select Mass_Email_Events__c, Applicant__r.PFS__r.Parent_A__c, ' +
                                               'Applicant__r.PFS__r.Parent_A_First_Name__c, Applicant__r.PFS__r.Parent_A_Last_Name__c' +
                                               ' from School_PFS_Assignment__c' + 
                                                ' where Id in :spaIdSet' +
                                                ' limit 10000' + ( lockRecords != null && lockRecords == true ? 
                                                  ' for update' : ''));
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in SchoolPFSAssignmentDataAccessService.getSPAEmailEventByIdSet' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }

        return returnList;
    }

    /**
     * @description: Queries School_PFS_Assignment__c by ID set
     *
     * @param spaIdSet - The Set of School_PFS_Assignment__c ID's
     *
     * @return A list of School_PFS_Assignment__c records
     */
    public List<School_PFS_Assignment__c> getSchoolPFSByIdSet( Set<String> spaIdSet) {
        List<School_PFS_Assignment__c> returnList;

        if ( spaIdSet != null) {

            try {

                returnList = Database.query( 'select ' + String.join( super.getSObjectFieldNames( 'School_PFS_Assignment__c'), ',') + ',' +
                                                     ' Student_Folder__r.Academic_Year_Picklist__c, Applicant__r.PFS__r.Parent_A__c, ' +
                                                     ' Applicant__r.PFS__r.Parent_A_First_Name__c, Applicant__r.PFS__r.Parent_A_Last_Name__c' +
                                              ' from School_PFS_Assignment__c' + 
                                             ' where Id in :spaIdSet');
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in SchoolPFSAssignmentDataAccessService.getSchoolPFSByIdSet' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }

        return returnList;
    }

    /**
     * @description: Queries School_PFS_Assignment__c by Parent_A ID set
     *
     * @param parentIdSet - The Set of Parent_A (Contact) ID's
     *
     * @param lockRecords - a boolean indicating whether or not to apply record lock on returned records
     *
     * @return A list of School_PFS_Assignment__c records
     */
    public List<School_PFS_Assignment__c> getSchoolPFSByParentAIdSet( Set<String> parentIdSet, Set<String> schoolIdSet, Set<String> academicYearSet, Boolean lockRecords) {
        List<School_PFS_Assignment__c> returnList;

        if ( parentIdSet != null && schoolIdSet != null && academicYearSet != null) {

            try {

                returnList = Database.query( 'select Id, Mass_Email_Events__c, School__c, Student_Folder__r.Academic_Year_Picklist__c, Applicant__r.PFS__r.Parent_A__c' +
                                               ' from School_PFS_Assignment__c' + 
                                                ' where Applicant__r.PFS__r.Parent_A__c in :parentIdSet' +
                                                  ' and School__c in :schoolIdSet' + 
                                                  ' and Student_Folder__r.Academic_Year_Picklist__c in :academicYearSet' +
                                                ' limit 10000' + ( lockRecords != null && lockRecords == true ? 
                                                  ' for update' : ''));
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in SchoolPFSAssignmentDataAccessService.getSchoolPFSByParentAIdSet' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }

        return returnList;
    }

    /**
     * @description: Queries School_PFS_Assignment__c by Student Folder ID
     *
     * @param studFolderId - The Student_Folder__c.Id value for the SPA(s)
     *
     * @return A list of School_PFS_Assignment__c records
     */
    public List<School_PFS_Assignment__c> getSchoolPFSByStudentFolderId( String studFolderId) {
        List<School_PFS_Assignment__c> returnList;

        if ( studFolderId != null) {

            try {

                returnList = Database.query( 'select Id, Mass_Email_Events__c, Student_Folder__r.Academic_Year_Picklist__c, Applicant__r.PFS__r.Parent_A__c' +
                                              ' from School_PFS_Assignment__c' + 
                                             ' where Student_Folder__c = :studFolderId');
            } catch ( Exception e) {

                // log it and bubble to caller for handling
                System.debug( 'DEBUG:::exception in SchoolPFSAssignmentDataAccessService.getSchoolPFSByStudentFolderId' +
                    e.getMessage());
                throw new DataAccessServiceException( e);
            }
        }

        return returnList;
    }

}