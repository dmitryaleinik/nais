/**
 * @description Provides information about the logged in User.
 */
public with sharing class CurrentUser {
    // the local prefix refers to the fact that our cache partition is not part of another package.
    // the naissession namespace refers to the partition we have specifically for the session cache.
    @testVisible private static final String CURRENT_USER_CACHE = 'local.naissession.currentuser';
    @testVisible private static final String GUEST_USER_TYPE = 'Guest';

    private static Subscription__c currentSubscription;

    /**
     * @description The list of Subscriptions for the school of the current user.
     */
    public static List<Subscription__c> Subscriptions {
        get {
            if (Subscriptions == null) {
                if (getCurrentSchool() == null) {
                    return null;
                }

                Id accountId = getCurrentSchool().Id;
                Subscriptions = SubscriptionSelector.Instance.selectByAccountId(new Set<Id> { accountId });
            }

            return Subscriptions;
        }
        private set;
    }

    /**
     * @description Determines whether or not the current user is a guest user (unauthenticated).
     * @return True if the user is of type guest, otherwise false.
     */
    public static Boolean isGuest() {
        return UserInfo.getUserType().equals(GUEST_USER_TYPE);
    }

    /**
     * @description Determine if the currently logged in user has a subscription for the current
     *              academic year.
     */
    public static Boolean hasCurrentSubscription() {
        return getCurrentSubscription() != null;
    }

    /**
     * @description Determine if the currently logged in user's school has a subscription fee opportunity
     *              that is in closed/won status for the current academic year.
     */
    public static Boolean hasSubscriptionOpportunityForCurrentAcademicYear() {
        if (getCurrentSchool() == null) {
            return false;
        }

        Id schoolId = getCurrentSchool().Id;
        List<Opportunity> opportunities = OpportunitySelector.Instance.selectByAccountId(new Set<Id> { schoolId });
        Academic_Year__c academicYear = AcademicYearService.Instance.getCurrentAcademicYear();

        Id subscriptionFeeRecordTypeId = Opportunity.getSObjectType()
                .getDescribe().getRecordTypeInfosByName().get('Subscription Fee').getRecordTypeId();

        for (Opportunity opportunity : opportunities) {
            if (opportunity.RecordTypeId == subscriptionFeeRecordTypeId &&
                opportunity.Academic_Year_Picklist__c == academicYear.Name &&
                opportunity.StageName == 'Closed Won') {

                return true;
            }
        }

        return false;
    }

    /**
     * @description Determines whether or not the current user should have early access to the family portal for the
     *              current academic year. A school must open early access before any family portal user can access
     *              early. If a school is allowing early access, then the current user will be checked. If the early
     *              access cookie is set with a value of true, users will have early access. If the cookie is not set,
     *              then the current user must be logged in and have early access set to true.
     * @return True if the current user should have early access.
     */
    public static Boolean hasEarlyAccess() {
        // If early access isn't open, then no users can access the family portal for the current academic year.
        if (!AccessService.Instance.isEarlyAccessOpen()) {
            return false;
        }

        // If the early access cookie has been set, we don't need to check the user record.
        // Let the user into the family portal early.
        if (!Trigger.isExecuting && AccessService.Instance.isEarlyAccessCookieSet()) {
            return true;
        }

        User currentUser = getCurrentUser();

        return currentUser != null && currentUser.Early_Access__c == true;
    }

    /**
     * @description Get the current subscription for the school associated
     *              with the currently logged in user.
     * @return The subscription for the current academic year if one exists.
     */
    public static Subscription__c getCurrentSubscription() {
        if (!isSchoolUser()) {
            return null;
        }

        for (Subscription__c subscription : Subscriptions) {
            if (subscription.Cancelled__c == 'No' && subscription.End_Date__c >= Date.today()) {
                currentSubscription = subscription;
                break;
            }
        }

        return currentSubscription;
    }

    /**
     * @description Get the school for the currently logged in user.
     * @return The account for the school associated with the currently
     *         logged in user.
     */
    public static Account getCurrentSchool() {
        if (!isSchoolUser()) {
            return null;
        }

        return GlobalVariables.getCurrentSchool();
    }

    @testVisible private static User currentUserRecord;
    /**
     * @description Gets the current user record. The record will be queried if the class has not already
     *              cached the current user record. If the user is a guest, null is returned.
     * @return A User record.
     */
    public static User getCurrentUser() {
        // If the user isn't logged in, return null.
        if (isGuest()) {
            return null;
        }

        if (currentUserRecord != null) {
            return currentUserRecord;
        }

        // If the cache was null, we will have to query.
        Set<Id> userIds = new Set<Id> { UserInfo.getUserId() };

        List<User> users = UserSelector.newInstance().selectById(userIds);

        currentUserRecord = users.isEmpty() ? null : users[0];

        // If no users were found return null, otherwise return the first record.
        return currentUserRecord;
    }

    /**
     * @description Returns true if the current user's profile is any of the school portal profiles.
     * @return True if the user is a school user.
     */
    public static Boolean isSchoolUser() {
        Id currentUserProfileId = UserInfo.getProfileId();

        if (currentUserProfileId == null) {
            return false;
        }

        return GlobalVariables.schoolPortalAdminProfileIds.contains(currentUserProfileId) ||
                GlobalVariables.schoolPortalProfileIds.contains(currentUserProfileId) ||
                GlobalVariables.schoolPortalBasicProfileIds.contains(currentUserProfileId);
    }

    /**
     * @description Clears the cached current user record.
     */
    public static void clearCache() {
        currentUserRecord = null;
    }
}