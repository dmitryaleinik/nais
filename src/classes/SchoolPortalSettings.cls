/**
* @description This class is used to retrieve the configurations for the school portal.
*/
public class SchoolPortalSettings {
    /**
     * @description The School Portal Settings record.
     */
    public static School_Portal_Settings_2__mdt Record {
        get {
            if (Record == null) {
                List<String> fieldNames = new List<String> (
                        Schema.SObjectType.School_Portal_Settings_2__mdt.fields.getMap().keySet());

                List<School_Portal_Settings_2__mdt> sps = Database.query(
                   'SELECT ' + String.Join(fieldNames, ',')
                   + ' FROM School_Portal_Settings_2__mdt '
                   + ' WHERE MasterLabel=\'School\' '
                   + ' LIMIT 1');

                Record = (sps != null && !sps.isEmpty()) ? sps[0] : null;
            }
            return Record;
        }
        private set;
    }

    public static Decimal Additional_Questions_Limit {
        get {
            if (Additional_Questions_Limit == null) {
                Additional_Questions_Limit = Record.Additional_Questions_Limit__c;
            }
            return Additional_Questions_Limit;
        }
        private set;
    }

    public static Decimal Annual_Setting_Async_Update_Threshold
    {
        get
        {
            if (Annual_Setting_Async_Update_Threshold == null)
            {
                Annual_Setting_Async_Update_Threshold = Record.Annual_Setting_Async_Update_Threshold__c;
            }
            return Annual_Setting_Async_Update_Threshold;
        }
        private set;
    }

    public static String Budget_Allocation_Report_Name {
        get {
            if (Budget_Allocation_Report_Name==null) {
                Budget_Allocation_Report_Name = Record.Budget_Allocation_Report_Name__c;
            }
            return Budget_Allocation_Report_Name;
        }
        private set;
    }

    public static ID Budget_Allocation_Report_ID {
        get {
            if (Budget_Allocation_Report_ID==null) {
                Budget_Allocation_Report_ID = getReportIdByName(Record.Budget_Allocation_Report_Name__c);
            }
            return Budget_Allocation_Report_ID;
        }
        private set;
    }

    public static Decimal Default_PY_Verification_Threshold {
        get {
            if (Default_PY_Verification_Threshold==null) {
                Default_PY_Verification_Threshold = Record.Default_PY_Verification_Threshold__c!=null
                    ? Record.Default_PY_Verification_Threshold__c : 15.0;
            }
            return Default_PY_Verification_Threshold;
        }
        private set;
    }

    public static Integer Email_Alert_Submission_Days {
        get {
            if (Email_Alert_Submission_Days==null) {
                Email_Alert_Submission_Days = Record.Email_Alert_Submission_Days__c != null
                    ? Integer.valueOf(Record.Email_Alert_Submission_Days__c) : 14;
            }
            return Email_Alert_Submission_Days;
        }
        private set;
    }

    public static Integer Fee_Waivers_Table_Page_Size {
        get {
            if (Fee_Waivers_Table_Page_Size==null) {
                Fee_Waivers_Table_Page_Size = Record.Fee_Waivers_Table_Page_Size__c != null
                    ? Integer.ValueOf(Record.Fee_Waivers_Table_Page_Size__c) : 50;
            }
            return Fee_Waivers_Table_Page_Size;
        }
        private set;
    }

    public static String Folder_Status_Report_Name {
        get {
            if (Folder_Status_Report_Name==null) {
                Folder_Status_Report_Name = Record.Folder_Status_Report_Name__c;
            }
            return Folder_Status_Report_Name;
        }
        private set;
    }

    public static ID Folder_Status_Report_Id {
        get {
            if (Folder_Status_Report_Id==null) {
                Folder_Status_Report_Id = getReportIdByName(Record.Folder_Status_Report_Name__c);
            }
            return Folder_Status_Report_Id;
        }
        private set;
    }

    public static ID Import_User_Id_18 {
        get {
            if (Import_User_Id_18==null) {
                Import_User_Id_18 = Record.Import_User_Id_18__c != null
                                  ? Record.Import_User_Id_18__c : '005d0000002HEooAAG';
            }
            return Import_User_Id_18;
        }
        private set;
    }

    public static ID KS_School_Account_Id {
        get {
            if (KS_School_Account_Id==null) {
                KS_School_Account_Id = Record.KS_School_Account_Id__c;
            }
            return KS_School_Account_Id;
        }
        private set;
    }

    public static Integer Max_Number_of_Users {
        get {
            if (Max_Number_of_Users==null) {
                Max_Number_of_Users = Record.Max_Number_of_Users__c != null
                    ? Integer.ValueOf(Record.Max_Number_of_Users__c)
                    : 0;
            }
            return Max_Number_of_Users;
        }
        private set;
    }

    public static String MIE_Link_on_Annual_Settings_page {
        get {
            if (MIE_Link_on_Annual_Settings_page==null) {
                MIE_Link_on_Annual_Settings_page = Record.MIE_Link_on_Annual_Settings_page__c != null
                    ? Record.MIE_Link_on_Annual_Settings_page__c
                    : '#';
            }
            return MIE_Link_on_Annual_Settings_page;
        }
        private set;
    }

    public static Boolean MIE_Pilot {
        get {
            if (MIE_Pilot==null) {
                MIE_Pilot = Record.MIE_Pilot__c;
            }
            return MIE_Pilot;
        }
        private set;
    }

    public static Date Overlap_End_Date {
        get {
            if (Overlap_End_Date==null) {
                Overlap_End_Date = Record.Overlap_End_Date__c;
            }
            return Overlap_End_Date;
        }
        private set;
    }

    public static Date Overlap_Start_Date {
        get {
            if (Overlap_Start_Date==null) {
                Overlap_Start_Date = Record.Overlap_Start_Date__c;
            }
            return Overlap_Start_Date;
        }
        private set;
    }

    public static Integer Req_Doc_Synchronous_Threshold {
        get {
            if (Req_Doc_Synchronous_Threshold==null) {
                Req_Doc_Synchronous_Threshold = Record.Req_Doc_Synchronous_Threshold__c != null
                    ? Integer.ValueOf(Record.Req_Doc_Synchronous_Threshold__c) : 1000;
            }
            return Req_Doc_Synchronous_Threshold;
        }
        private set;
    }

    public static String SSS_Org_Wide_Email_Address {
        get {
            if (SSS_Org_Wide_Email_Address==null) {
                SSS_Org_Wide_Email_Address = Record.SSS_Org_Wide_Email_Address__c;
            }
            return SSS_Org_Wide_Email_Address;
        }
        private set;
    }

    public static Date Override_Today {
        get {
            if (Override_Today==null) {
                Override_Today = Record.Override_Today__c;
            }
            return Override_Today;
        }
        private set;
    }

    private static Id getReportIdByName(String reportName) {
        List<Report> reports = [SELECT Id FROM Report WHERE Name = :reportName LIMIT 1];

        if (reports.isEmpty()) {
            return null;
        }

        return reports[0].Id;
    }

    public static Decimal Verification_Match_Tolerance {
        get {
            if (Verification_Match_Tolerance==null) {
                Verification_Match_Tolerance = Record.Verification_Match_Tolerance__c!=null
                    ? Record.Verification_Match_Tolerance__c : 0;
            }
            return Verification_Match_Tolerance;
        }
        private set;
    }
    
    /**
    * @description Used in SchoolAdditionalDocsBulkController to set the maximum limit of student folders to process.
    */
    public static Decimal Require_Additional_Documents_Bulk_Limit {
        get {
            if (Require_Additional_Documents_Bulk_Limit==null) {
                Require_Additional_Documents_Bulk_Limit = Record.Require_Additional_Documents_Bulk_Limit__c!=null
                    ? Record.Require_Additional_Documents_Bulk_Limit__c : 100;
            }
            return Require_Additional_Documents_Bulk_Limit;
        }
        private set;
    }
    
    /**
    * @description Used in SchoolRequestMIEBulkController to set the maximum limit of student folders to process.
    */
    public static Decimal Require_MIE_Bulk_Limit {
        get {
            if (Require_MIE_Bulk_Limit==null) {
                Require_MIE_Bulk_Limit = Record.Require_MIE_Bulk_Limit__c !=null
                    ? Record.Require_MIE_Bulk_Limit__c : 100;
            }
            return Require_MIE_Bulk_Limit;
        }
        private set;
    }

    private static Id getAccountIdByRecordTypeId(Id recordTypeId) {
        List<Account> accounts = [SELECT Id FROM Account WHERE RecordTypeId = :recordTypeId LIMIT 1];

        if (accounts.isEmpty()) {
            return null;
        }

        return accounts[0].Id;
    }
}