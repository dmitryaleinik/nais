/**
 * @description Handle processing involving Business Farms.
 **/
public class BusinessFarmService {

    @testVisible private static final String BUSINESS_FARM_PARAM = 'businessFarm';
    @testVisible private static final String BUSINESS_FARMS_PARAM = 'businessFarms';
    @testVisible private static final String PFS_ASSIGNMENT_PARAM = 'pfsAssignment';
    @testVisible private static final String BUSINESS_ENTITY_TYPE_PARTNERSHIP = 'Partnership';
    @testVisible private static final String BUSINESS_ENTITY_TYPE_CORPORATION = 'Corporation';
    @testVisible private static final String BUSINESS = 'Business';
    @testVisible private static final String FARM = 'Farm';

    /**
     * @description Fix any potential null sequence numbers existing on
     *              business farm records. This will determine the total
     *              number of business farm records for the parent Pfs
     *              record and place any null sequence numbered Business
     *              Farms at the end.
     * @param businessFarm The Business Farm to resolve a potential null
     *        sequence for.
     * @throws An ArgumentNullException if businessFarms is null.
     */
    public void fixNullSequenceNumbers(Business_Farm__c businessFarm) {
        ArgumentNullException.throwIfNull(businessFarm, BUSINESS_FARM_PARAM);
        fixNullSequenceNumbers(new List<Business_Farm__c> { businessFarm });
    }

    /**
     * @description Fix any potential null sequence numbers existing on
     *              business farm records. This will determine the total
     *              number of business farm records for the parent Pfs
     *              record and place any null sequence numbered Business
     *              Farms at the end.
     * @param businessFarms The Business Farms to resolve any null sequence
     *        numbers for.
     * @throws An ArgumentNullException if businessFarms is null.
     */
    public void fixNullSequenceNumbers(List<Business_Farm__c> businessFarms) {
        ArgumentNullException.throwIfNull(businessFarms, BUSINESS_FARMS_PARAM);

        Map<Id, List<Business_Farm__c>> businessFarmsByPfsId = getAllBusinessFarmsByPfsId(businessFarms);
        List<Business_Farm__c> businessFarmsToUpdate = new List<Business_Farm__c>();

        Set<Id> pfsIds = businessFarmsByPfsId.keySet();
        for (Id pfsId : pfsIds) {
            List<Business_Farm__c> allPfsBusinessFarms = businessFarmsByPfsId.get(pfsId);
            List<Business_Farm__c> nullSequenceBusinessFarms = new List<Business_Farm__c>();

            for (Business_Farm__c businessFarm : allPfsBusinessFarms) {
                if (businessFarm.Sequence_Number__c == null) {
                    nullSequenceBusinessFarms.add(businessFarm);
                }
            }

            Integer offset = allPfsBusinessFarms.size() - nullSequenceBusinessFarms.size();
            for (Integer i = 0, l = nullSequenceBusinessFarms.size(); i < l; i++) {
                // Add it in this manner so we mess up the delta of the record.
                businessFarmsToUpdate.add(new Business_Farm__c(
                        Id = nullSequenceBusinessFarms[i].Id,
                        Sequence_Number__c = (i+1)));
            }
        }

        if (!businessFarmsToUpdate.isEmpty()) {
            update businessFarmsToUpdate;
        }
    }

    /**
     * @description Bulkify the query to select all Business Farm records
     *              and pass them back sorted by Pfs Id.
     * @param businessFarms The Business Farms to use to generate a query
     *        for all Business Farms by Pfs Id with.
     * @return A map of collections of Business Farms by their respective
     *         Pfs Ids.
     */
    private Map<Id, List<Business_Farm__c>> getAllBusinessFarmsByPfsId(List<Business_Farm__c> businessFarms) {
        Map<Id, List<Business_Farm__c>> businessFarmsByPfsId = new Map<Id, List<Business_Farm__c>>();
        Set<Id> pfsIds = new Set<Id>();

        // Extract the Pfs Ids so we can query for the Business Farms.
        for (Business_Farm__c businessFarm : businessFarms) {
            pfsIds.add(businessFarm.PFS__c);
        }

        List<Business_Farm__c> allPfsBusinessFarms = BusinessFarmSelector.Instance.selectByPfsId(pfsIds);
        for (Business_Farm__c businessFarm : allPfsBusinessFarms) {
            if (!businessFarmsByPfsId.containsKey(businessFarm.PFS__c)) {
                businessFarmsByPfsId.put(businessFarm.PFS__c, new List<Business_Farm__c>());
            }

            businessFarmsByPfsId.get(businessFarm.PFS__c).add(businessFarm);
        }

        return businessFarmsByPfsId;
    }

    /**
     * @description Separates the business farm assignments so farm and business information is grouped correctly.
     *              Businesses, Farms, and SCorporations/Partnerships are grouped separately. Before 2018-2019,
     *              SCorporations/Partnerships are included with Businesses and Farms.
     * @param pfsAssignment The SPA that we need business information separated for.
     * @return A summary of business information.
     * @throws ArgumentNullException if pfsAssignment is null.
     */
    public BusinessSummary getBusinessSummary(School_PFS_Assignment__c pfsAssignment) {
        ArgumentNullException.throwIfNull(pfsAssignment, PFS_ASSIGNMENT_PARAM);

        return new BusinessSummary(pfsAssignment);
    }

    /**
     * @description This class contains the SBFA records and some summary data for all businesses in a single School PFS Assignment. This class will store businesses and farms separately and include total net profits for each.
     */
    public class BusinessSummary {

        private BusinessFarmService.BusinessSummary(School_PFS_Assignment__c pfsAssignment) {
            if (FeatureToggles.isEnabled('Exclude_S_Corps_Partnerships_From_Sums__c') &&
                    AcademicYearUtil.is20182019OrLater(pfsAssignment.Academic_Year_Picklist__c)) {
                separateBusinessesFarmsAndSCorpPartnerships(pfsAssignment.School_Biz_Farm_Assignments__r);
            } else {
                separateBusinessesAndFarms(pfsAssignment.School_Biz_Farm_Assignments__r);
            }
        }

        /**
         * @description The sum of net profits for the businesses based on the information submitted by the family.
         *
         *              When the Exclude_S_Corps_Partnerships_From_Sums__c feature toggle is enabled and the academic
         *              year is 2018-2019 or later, this should not include any business that is an S-Corporation or
         *              Partnership.
         */
        public Decimal BusinessNetProfit_Original {
            get { return sumOriginalNetProfit(Businesses); }
        }

        /**
         * @description The sum of net profits for the businesses based on the revisions made by the school.
         *
         *              When the Exclude_S_Corps_Partnerships_From_Sums__c feature toggle is enabled and the academic
         *              year is 2018-2019 or later, this should not include any business that is an S-Corporation or
         *              Partnership.
         */
        public Decimal BusinessNetProfit_Revised {
            get { return sumRevisedNetProfit(Businesses); }
        }

        /**
         * @description The sum of net profits for the farms based on the information submitted by the family.
         *
         *              When the Exclude_S_Corps_Partnerships_From_Sums__c feature toggle is enabled and the academic
         *              year is 2018-2019 or later, this should not include any farm that is an S-Corporation or
         *              Partnership.
         */
        public Decimal FarmNetProfit_Original {
            get { return sumOriginalNetProfit(Farms); }
        }

        /**
         * @description The sum of net profits for the farms based on the revisions made by the school.
         *
         *              When the Exclude_S_Corps_Partnerships_From_Sums__c feature toggle is enabled and the academic
         *              year is 2018-2019 or later, this should not include any farm that is an S-Corporation or
         *              Partnership.
         */
        public Decimal FarmNetProfit_Revised {
            get { return sumRevisedNetProfit(Farms); }
        }

        /**
         * @description The sum of net profits for the S-Corporations/Partnerships based on the information submitted by the family.
         *
         *              When the Exclude_S_Corps_Partnerships_From_Sums__c feature toggle is disabled or the academic
         *              year is before 2018-2019, this should be 0.
         */
        public Decimal SCorpPartnershipNetProfit_Original {
            get { return sumOriginalNetProfit(PartnershipsAndSCorporations); }
        }

        /**
         * @description The sum of net profits for the S-Corporations/Partnerships based on the revisions made by the school.
         *
         *              When the Exclude_S_Corps_Partnerships_From_Sums__c feature toggle is disabled or the academic
         *              year is before 2018-2019, this should be 0.
         */
        public Decimal SCorpPartnershipNetProfit_Revised {
            get { return sumRevisedNetProfit(PartnershipsAndSCorporations); }
        }

        /**
         * @description The Biz Farm Assignments that represent businesses. When the Exclude_S_Corps_Partnerships_From_Sums__c
         *              feature toggle is enabled and the academic year is 2018-2019 or later, this should not include
         *              any business that is an S-Corporation or Partnership. In that case, those businesses will be
         *              found in the PartnershipsAndSCorporations property.
         *
         *              SBFA records should not be added to this property directly. Call addBusiness()
         */
        public List<School_Biz_Farm_Assignment__c> Businesses {
            get {
                if (Businesses == null) {
                    Businesses = new List<School_Biz_Farm_Assignment__c>();
                }
                return Businesses;
            }
            private set;
        }

        /**
         * @description The Biz Farm Assignments that represent farms. When the Exclude_S_Corps_Partnerships_From_Sums__c
         *              feature toggle is enabled and the academic year is 2018-2019 or later, this should not include
         *              any farm that is an S-Corporation or Partnership. In that case, those farms will be
         *              found in the PartnershipsAndSCorporations property.
         *
         *              SBFA records should not be added to this property directly. Call addFarm()
         */
        public List<School_Biz_Farm_Assignment__c> Farms {
            get {
                if (Farms == null) {
                    Farms = new List<School_Biz_Farm_Assignment__c>();
                }
                return Farms;
            }
            private set;
        }

        /**
         * @description The Biz Farm Assignments that represent S-Corporations and Partnerships. When the
         *              Exclude_S_Corps_Partnerships_From_Sums__c feature toggle is disabled and the academic year is
         *              before 2018-2019, this should be empty.
         */
        public List<School_Biz_Farm_Assignment__c> PartnershipsAndSCorporations {
            get {
                if (PartnershipsAndSCorporations == null) {
                    PartnershipsAndSCorporations = new List<School_Biz_Farm_Assignment__c>();
                }
                return PartnershipsAndSCorporations;
            }
            private set;
        }

        /**
         * @description Separates businesses and farms and places them in the appropriate properties.
         */
        private void separateBusinessesAndFarms(List<School_Biz_Farm_Assignment__c> sbfasToSeparate) {
            if (sbfasToSeparate == null || sbfasToSeparate.isEmpty()) {
                return;
            }

            for (School_Biz_Farm_Assignment__c bizFarmAssignment : sbfasToSeparate) {
                if (FARM.equalsIgnoreCase(bizFarmAssignment.Business_Farm__r.Business_or_Farm__c)) {
                    Farms.add(bizFarmAssignment);
                } else {
                    // SFP-1617: We don't explicity check to see if Business_Farm__r.Business_or_Farm__c equals 'Business'
                    // because some default/legacy behavior depended on that.
                    Businesses.add(bizFarmAssignment);
                }
            }
        }

        /**
         * @description Separates businesses and farms and places them in the appropriate properties while also adding
         *              any S-Corporations/Partnerships to a separate property.
         */
        private void separateBusinessesFarmsAndSCorpPartnerships(List<School_Biz_Farm_Assignment__c> sbfasToSeparate) {
            if (sbfasToSeparate == null || sbfasToSeparate.isEmpty()) {
                return;
            }

            for (School_Biz_Farm_Assignment__c bizFarmAssignment : sbfasToSeparate) {
                if (isPartnershipOrSCorporation(bizFarmAssignment)) {
                    PartnershipsAndSCorporations.add(bizFarmAssignment);
                } else if (FARM.equalsIgnoreCase(bizFarmAssignment.Business_Farm__r.Business_or_Farm__c)) {
                    Farms.add(bizFarmAssignment);
                } else {
                    // SFP-1617: We don't explicity check to see if Business_Farm__r.Business_or_Farm__c equals 'Business'
                    // because some default/legacy behavior depended on that.
                    Businesses.add(bizFarmAssignment);
                }
            }
        }

        private Decimal sumOriginalNetProfit(List<School_Biz_Farm_Assignment__c> sbfasToSum) {
            Decimal sum = 0;

            for (School_Biz_Farm_Assignment__c sbfa : sbfasToSum) {
                sum += EfcUtil.nullToZero(sbfa.Orig_Net_Profit_Loss_Business_Farm__c);
            }

            return sum;
        }

        private Decimal sumRevisedNetProfit(List<School_Biz_Farm_Assignment__c> sbfasToSum) {
            Decimal sum = 0;

            for (School_Biz_Farm_Assignment__c sbfa : sbfasToSum) {
                sum += EfcUtil.nullToZero(sbfa.Net_Profit_Loss_Business_Farm__c);
            }

            return sum;
        }

        private Boolean isPartnershipOrSCorporation(School_Biz_Farm_Assignment__c bizFarmAssignment) {
            if (String.isBlank(bizFarmAssignment.Business_Entity_Type__c)) {
                return false;
            }

            return BUSINESS_ENTITY_TYPE_CORPORATION.equalsIgnoreCase(bizFarmAssignment.Business_Entity_Type__c) ||
                    BUSINESS_ENTITY_TYPE_PARTNERSHIP.equalsIgnoreCase(bizFarmAssignment.Business_Entity_Type__c);
        }
    }

    /**
     * @description Singleton Instance of the BusinessFarmService.
     */
    public static BusinessFarmService Instance {
        get {
            if (Instance == null) {
                Instance = new BusinessFarmService();
            }
            return Instance;
        }
        private set;
    }

    private BusinessFarmService() {}
}