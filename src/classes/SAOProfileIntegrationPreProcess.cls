/**
* SAOProfileIntegrationPreProcess.cls
*
* @description: Handles preprocessing of SAO integration api calls.
* Currently called before the first API call and retrieves the AuthToken and currentStudentId
*
* @author: Mike Havrila @ Presence PG
*/

public class SAOProfileIntegrationPreProcess implements IntegrationApiModel.IAction {
    
    private final String PROFILES_API = 'Profiles';
    private String paramId;
    private String authToken;
    public SAOProfileIntegrationPreProcess() {}
    
    /**
    * @description: PreProcess invoke method. Populates the auth token and the parent ProfileId from the token response.
    * Since, the oauth process doesn't provide a current user route, we colon delimited it in IntegrationOAuthProvider
    * when we get the auth token we can split and get the current user guid as well. Since that is required for the first
    * api call.
    *
    * @param: apiState - A Current Instance of IntegrationApiModel.ApiState
    */
    public void invoke( IntegrationApiModel.ApiState apiState) {
        
        final String firstApiName = PROFILES_API;//We can not use apiState.currentApi. Because, at this point it is null.
        
        this.getUserIdAndTokenFromAuth(apiState.apiSource);//apiState.apiSource = sao
        
        apiState.authToken = this.authToken;

        apiState.httpParamsByApi.put(firstApiName, new List<String> { this.paramId });
    }
    
    /**
    * @description: gets the Access Token via Auth Provider, and splits the auth token
    * by colon delimited to return both the currentStudent and authToekn.
    *
    * @param: String providerType - Auth Provider providerName = (sao, ravenna).
    */
    private void getUserIdAndTokenFromAuth( String providerType) {
        
        if ( providerType != null) {
            
            String authTokenUserId = IntegrationUtils.getUserAuthToken( providerType, providerType);
            
            // Due to limiations of the SAO oauth, we are storing the access token as userId:authToken
            // so we must split it here
            if( authTokenUserId != null && authTokenUserId.indexOf( ':') > 0) {
                
                List<String> splitAccessToken = authTokenUserId.split(':');
                this.paramId = splitAccessToken[0];
                this.authToken = splitAccessToken[1];
            }
        }
    }
}