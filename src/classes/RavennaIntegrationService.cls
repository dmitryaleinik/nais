/**
 * RavennaIntegrationService.cls
 *
 * @description:
 *
 * @author: Mike Havrila @ Presence PG
 */
public class RavennaIntegrationService extends BaseIntegrationService {
//    private PFS__c pfs;
//    private Map<String, List<IntegrationMappingModel>> mappingModelBySource;
//    private list<Object> responseObject;
//    private FamilyIntegrationModel integrationModel;
//    private IntegrationApiService apiService;
//    private String source;
//
//    // constants
//    private List<String> keysToCompound = new List<String> { 'first_name__c', 'last_name__c', 'birthdate_new__c'};
//
//    public RavennaIntegrationService( PFS__c pfs) {
//        this.pfs = pfs;
//
//    }
//
//    public IIntegrationProvider init( String source) {
//
//        if( !this.pfs.StatParentsGuardians__c) {
//
//            return new NullIntegrationService();
//        }
//        this.source = source;
//        this.integrationModel = new FamilyIntegrationModel( source);
//        this.mappingModelBySource = this.integrationModel.mappingModelBySource;
//        //this.apiService = new IntegrationApiService(this.pfs);
//        return this;
//    }
//
//    public IIntegrationProvider postProcess() {
//        return this;
//    }
//
//    /* Use pre-process to loop get all students from endpoints */
//    public IIntegrationProvider preProcess() {
//
//        // Make API Calls
//        //this.apiService.setIntegrationApi( this.integrationModel.source, this.integrationModel.apiBaseUrl);
//       // String token = super.getAuthToken( 'Ravenna', 'Open ID Connect');
//        //httpHeaders.put( 'Authorization', 'Bearer ' + token);
//        //this.apiService.httpHeaders = httpHeaders;
//        // Step 1: Get Students from Source.
//        List<Object> studentsFromSource = this.getStudents( this.integrationModel.integrationSource);
//        if( studentsFromSource == null) {
//
//            throw new IntegrationException('No Students to process');
//        }
//
//        // Step 2: map response to Sobject
//        List<sObject> studentApplicants = super.mapRecords( studentsFromSource, mappingModelBySource.get( this.integrationModel.APPLICANT_OBJECT),
//                                                                            this.integrationModel.APPLICANT_OBJECT, this.integrationModel.STUDENTS_TYPE);
//
//        // Step 3: Map each student, put SobjectType of PFS__c on the object
//        studentApplicants = super.populatPfsIdOnObject( studentApplicants, this.pfs.Id);
//
//        // Step 4: Create Compound Key for Lookups.
//        String applicantExternalIdField = this.integrationModel.getMappingFieldByType( this.mappingModelBySource.get( this.integrationModel.STUDENTS_TYPE),
//                                                                                              this.integrationModel.ID_FIELD,
//                                                                                              this.integrationModel.APPLICANT_OBJECT);
//        // Step 5: Find existing applicants
//        Map<String, String> compoundSearchKeyToId = this.integrationModel.getCompoundKeysToIdField( keysToCompound, studentApplicants, applicantExternalIdField);
//
//        // Step 6: Build collection of student Ids based on Applicant object;
//        this.integrationModel.setStudentIdsToSobject( studentApplicants, applicantExternalIdField);
//        this.integrationModel.setApplicationsById( this.integrationModel.studentIdToSobject.keySet(), pfs.Id);
//
//        this.integrationModel.mapApplicantIdToStudents( compoundSearchKeyToId, keysToCompound, applicantExternalIdField);
//
//        // Step 7: Starting Folder/SPA processing - Now we can create new applicants;
//        // TODO: DELTE THESE LINES
//        Map<String, List<String>> studentIdToSchools = new Map<String, List<String>>();
//
//        this.setStudentsToProcess( this.integrationModel.studentIdToSobject.keySet(), this.getAcademicYear());
//        //Map<String, List<Object>> studentsToProcess = this.integrationModel.getApplicantIdToSchoolId( studentIdToSobject.keySet(), this.getAcademicYear());
//
//        // We have not found any applicants in Ravenna who have applied to any school in SSS for the current
//        // Academic Year.
//        if( this.integrationModel.applicantIdToSchoolId.isEmpty()) {
//            throw new IntegrationException('Unable to find applicant data for the selected year.');
//        }
//
//        // callouts complete so now move to processing.
//        return this;
//
//    }
//
//    public IIntegrationProvider process() {
//        // Custom MDT with Endpoints 1. applicants 2. Students/Applicants 3. Schools.
//        // Get the order of the api calls and information. (model class);
//        // Base Integration - > Factory that passes in the name of the process. (ravenna -> api calls and instaniates List<IIntegrationApiCall-> );
//
//        // Process handles looping over students to process and
//        // TODO: Get School Set;
//        Set<String> school = new Set<String>();
//
//        // Process Applicants and upsert them.
//        this.integrationModel.checkForMatchingSchools();
//        this.integrationModel.setApplicantsToUpsert();
//
//        this.integrationModel.upsertItemsWithExternalId( 'Applicant__c', 'List<Applicant__c>', this.integrationModel.applicantsToUpsert);
//
//        // get the unique id fields for this object, must be present
//        String studentFolderIdField = this.integrationModel.getMappingFieldByType( this.integrationModel.mappingModelBySource.get( 'Student Applications'),
//                                                                  'ID', 'Student_Folder__c' );
//        // Loop over studentId to applicants by Key
//        List<String> keysToCompound = new List<String> { 'first_name__c', 'last_name__c', 'birthdate_new__c'};
//        // folders
//
//        Map<String,String> folderNameToCompoundKey = new Map<String,String>();
//        Map<String, List<String>> studentNamesToSearch = new Map<String, List<String>>();
//        List<sObject> folders = new List<sObject>();
//        for(String studentId : this.integrationModel.studentIdToSobject.keyset()) {
//
//            List<sObject> studentFolders = super.mapRecords( this.integrationModel.studentsToProcess.get( studentId) , mappingModelBySource.get( 'Student Applications'), 'Student_Folder__c', 'Student Applications');
//            String compoundKey = this.integrationModel.getCompoundKey( this.integrationModel.studentIdToSobject.get( studentId), keysToCompound);
//
//            List<String> firstNames = new List<String>();
//            List<String> lastNames = new List<String>();
//            List<String> birthdates = new List<String>();
//            for( sObject studentFolder : studentFolders) {
//
//                this.integrationModel.populateFieldByType( studentFolder, 'Student__c', compoundKey);
//                String firstName = (String)this.integrationModel.studentIdToSobject.get( studentId).get( 'first_name__c');
//                String lastName = (String)this.integrationModel.studentIdToSobject.get( studentId).get( 'last_name__c');
//                String birthdate = (String)this.integrationModel.studentIdToSobject.get( studentId).get( 'birthdate_new__c');
//
//                firstNames.add( firstName);
//                lastNames.add( lastName);
//                birthdates.add( birthdate);
//
//                // update the record
//                //ac.schoolIdToName.get(applicantIdToSchoolId.get((string)each.get(folderIdMapping)))
//                String schoolId = (String)this.integrationModel.applicantIdToSchoolId.get( (String)studentFolder.get( studentFolderIdField));
//                String schoolName = (String)this.integrationModel.schoolIdToName.get( schoolId);
//                String nameKey = firstName + ' ' + lastName + '-' + schoolName;
//
//                studentFolder.put( 'name', nameKey);
//                studentFolder.put( 'Student__c', compoundKey);
//
//                // temporarily assign an invalid id (compound key) to the lookup, which will be used to get the correct id for that compound key
//                folderNameToCompoundKey.put( (String)studentFolder.get( studentFolderIdField), compoundKey);
//                folders.add( studentFolder);
//            }
//            studentNamesToSearch.put( 'FirstNames', firstNames);
//            studentNamesToSearch.put( 'LastNames', lastNames);
//            studentNamesToSearch.put( 'Birthdates', birthdates);
//        }
//
//        // PROCESS STUDENT_FOLDERS;
//        String spaIdField = this.integrationModel.getMappingFieldByType( this.integrationModel.mappingModelBySource.get( 'Student Applications'),
//                                                                  'ID', 'School_PFS_Assignment__c' );
//        // Now go look up all students based off foldr
//        List<sObject> students = new IntegrationMappingDataAccessService().getStudentsByNames( studentNamesToSearch);
//        list<sObject> foldersToUpsert = new list<sObject>();
//        Map<String, String> contactKeyToId = new Map<String, String>();
//        for( sObject obj : students) {
//
//            Contact studentContact = (Contact)obj;
//            Date birthdate = Date.newInstance( studentContact.birthdate.year(), studentContact.birthdate.month(), studentContact.birthdate.day());
//            String convertedBirthdate = String.valueOf( birthdate);
//            List<String> fieldsToCompound = new List<String>();
//            fieldsToCompound.add( (String)obj.get( 'firstName'));
//            fieldsToCompound.add( (String)obj.get( 'lastName'));
//            fieldsToCompound.add( convertedBirthdate);
//
//            String compoundKey = this.integrationModel.buildCompoundKey(fieldsToCompound, '%');
//            contactKeyToId.put( compoundKey, (String)obj.get('id'));
//
//            for( sObject folder : folders) {
//
//                String nameKey = folderNameToCompoundKey.get( (String)folder.get( studentFolderIdField));
//                folder.put( 'Student__c', contactKeyToId.get( nameKey));
//
//                if( folder.get( 'School__c') != null && this.integrationModel.schoolIds.contains( (String)folder.get('School__c'))) {
//                    folder.put( 'OwnerId', GlobalVariables.getIndyOwner());
//                    foldersToUpsert.add( folder);
//                }
//            }
//        }
//
//        // upsert
//        this.integrationModel.upsertItemsWithExternalId( 'Student_Folder__c', 'List<Student_Folder__c>', foldersToUpsert, studentFolderIdField);
//
//        List<sObject> schoolPFSAssignments = new List<sObject>();
//        list<sObject> schoolPFSAssignmentsToUpsert = new list<School_PFS_Assignment__c>();
//        // Finally Handle SPA's
//        for( String studentId : this.integrationModel.studentIdToSobject.keyset()) {
//            List<sObject> studentFolders = super.mapRecords( this.integrationModel.studentsToProcess.get( studentId) , mappingModelBySource.get( 'Student Applications'), 'School_PFS_Assignment__c', 'Student Applications');
//
//            for( sObject studentFolder : studentFolders) {
//                String firstName = (String)this.integrationModel.studentIdToSobject.get( studentId).get( 'first_name__c');
//                String lastName = (String)this.integrationModel.studentIdToSobject.get( studentId).get( 'last_name__c');
//                String birthdate = (String)this.integrationModel.studentIdToSobject.get( studentId).get( 'birthdate_new__c');
//
//                String schoolId = (String)this.integrationModel.applicantIdToSchoolId.get( (String)studentFolder.get( spaIdField));
//                String schoolName = (String)this.integrationModel.schoolIdToName.get( schoolId);
//                String nameKey = firstName + ' ' + lastName + '-' + schoolName;
//
//                studentFolder.put( 'name', nameKey);
//                studentFolder.put( 'Applicant__c', this.integrationModel.studentIdToSobject.get( studentId).get( 'id'));
//                studentFolder.put( 'Academic_Year_Picklist__c', this.pfs.Academic_Year_Picklist__c);
//
//                schoolPFSAssignments.add( studentFolder);
//                if( studentFolder.get('School__c') != null && this.integrationModel.schoolIds.contains( (String)studentFolder.get('School__c'))) {
//                    schoolPFSAssignmentsToUpsert.add( studentFolder);
//                }
//            }
//        }
//
//        // dynamically handle Upsert
//        this.integrationModel.upsertItemsWithExternalId( 'School_PFS_Assignment__c', 'List<School_PFS_Assignment__c>', schoolPFSAssignmentsToUpsert, spaIdField);
//
//        return this;
//    }
//
//    /* Ravenna Specific callouts */
//    private List<Object> getStudents( String source) {
//
//        List<Object> returnVal;
//        Map<String, String> httpHeaders = new Map<String, String>();
//
//        String httpEndpoint = this.integrationModel.apiBaseUrl + '/api/v1/students/';
//        String token = super.getAuthToken( 'Ravenna', 'Open ID Connect');
//        httpHeaders.put( 'Authorization', 'Bearer ' + token);
//
//        returnVal = super.getObjectFromHttpRequest( httpEndpoint, httpHeaders, null, HttpService.HTTP_GET);
//
//        return returnVal;
//    }
//
//    /* Ravenna Specific callouts */
//    private List<Object> getStudentApplicants( String studentId, String academicYear) {
//
//        List<Object> returnVal;
//        Map<String, String> httpHeaders = new Map<String, String>();
//
//        String httpEndpoint = this.integrationModel.apiBaseUrl + '/api/v1/students/' + studentId + '/applications?admission_year=' + academicYear;
//        String token = super.getAuthToken( 'Ravenna', 'Open ID Connect');
//        httpHeaders.put( 'Authorization', 'Bearer ' + token);
//
//        returnVal = super.getObjectFromHttpRequest( httpEndpoint, httpHeaders, null, HttpService.HTTP_GET);
//
//        return returnVal;
//    }
//
//    // Pre/Post
//    /* Ravenna Specific callouts */
//    private List<Object> getSchoolsById( String schoolId) {
//
//        List<Object> returnVal;
//        Map<String, String> httpHeaders = new Map<String, String>();
//
//        String httpEndpoint = this.integrationModel.apiBaseUrl + '/api/v1/schools/' + schoolId;
//        String token = super.getAuthToken( 'Ravenna', 'Open ID Connect');
//        httpHeaders.put( 'Authorization', 'Bearer ' + token);
//
//        returnVal = super.getObjectFromHttpRequest( httpEndpoint, httpHeaders, null, HttpService.HTTP_GET);
//        return returnVal;
//    }
//
//    // Ravenna Specific
//    public void setStudentsToProcess( Set<String> studentIds, String academicYear) {
//
//        // loop over all studentid
//        for( String studentId : studentIds) {
//
//            this.integrationModel.addStudentIdToSchool( studentId, null);
//
//            // Make callout to get Student Applicants;
//            List<Object> studentApplicationsResponse = this.getStudentApplicants( studentId, academicYear);
//            List<Object> applications = new List<Object>();
//            for( Object application : studentApplicationsResponse) {
//
//                Map<String,object> studentApplications = (Map<String, Object>)application;
//                if( studentApplications.isEmpty()) {
//
//                    // Student might have come back before but has not applied this year.
//                    continue;
//                }
//
//                // Need to map over studentId -> NceId now.
//                String studentSchoolId = (String)studentApplications.get( this.integrationModel.SCHOOL_ID_VALUE);
//                if( !this.integrationModel.studentToNcesId.containsKey( studentSchoolId)) {
//
//                    // this should only return one user.
//                    Map<String, Object> schoolResponse = (Map<String, Object>)this.getSchoolsById( studentSchoolId)[0];
//                    String ncesId = (String)schoolResponse.get( this.integrationModel.NCES_ID_VALUE);
//                    this.integrationModel.addStudentToNcesId( studentSchoolId, ncesId);
//                }
//
//                String schoolId = this.integrationModel.studentToNcesId.get( studentSchoolId) != null
//                                  ? this.integrationModel.studentToNcesId.get( studentSchoolId)
//                                  : studentSchoolId;
//
//                // update student applications
//                this.integrationModel.addApplicantIdToSchoolId( (String)studentApplications.get( this.integrationModel.ID_FIELD), schoolId);
//                //this.integrationModel.applicantIdToSchoolId.put( (String)studentApplications.get( this.integrationModel.ID_FIELD), schoolId);
//                this.integrationModel.addStudentIdToSchool( studentId, schoolId);
//                //this.integrationModel.studentIdToSchools.get( studentId).add( schoolId);
//                this.integrationModel.addSchoolId( schoolId);
//                //this.integrationModel.schoolIds.add( schoolId);
//
//                studentApplications.put( this.integrationModel.STUDENT_VALUE, studentId);
//                studentApplications.put( this.integrationModel.SCHOOL_ID_VALUE, schoolId);
//
//                // TODO: populate grade;
//                applications.add( (Object)studentApplications);
//            }
//
//            this.integrationModel.addStudentsToProcess( studentid, applications);
//            //this.integrationModel.studentsToProcess.put( studentid, applications);
//        }
//
//    }
//
//
//    // TODO CREATE MAPPING HERE.
//    private String mapGrade( String fieldName, String value){
//        if( fieldName == 'apply_grade' || fieldName == 'current_grade') {
//            if( value == 'K') return 'Kindergarten';
//            if( value == 'PS') return 'Preschool';
//            if( value == 'PK') return 'Pre-K';
//            if( value == 'PG') return 'Post Graduation';
//        }
//
//        // no match, so just return the value
//        return value;
//    }
//
//    private String getAcademicYear() {
//
//        List<String> admissionYears = pfs.Academic_Year_Picklist__c.split( '-');
//        Integer yearStart = Integer.valueof( admissionYears[0]) - 1;
//        Integer yearEnd = Integer.valueof( admissionYears[1]) - 1;
//        String admissionYear = String.valueOf(yearStart + '-' + yearEnd);
//
//        return admissionYear;
//    }
}