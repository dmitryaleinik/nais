@isTest
private class FamilyDashboardControllerTest {

    private static TestDataFamily tdf;
    private static User currentUser = [Select Id from User where Id = :UserInfo.getUserId()];

    private static User setupTestData() {
        User sysAdminUser = new User();
        sysAdminUser.Alias = 'TestSyAd';
        sysAdminUser.LanguageLocaleKey = 'en_US';
        sysAdminUser.LocaleSidKey = 'en_US';
        sysAdminUser.EmailEncodingKey = 'ISO-8859-1';
        sysAdminUser.FirstName = 'Sys';
        sysAdminUser.LastName = 'admin';
        sysAdminUser.Username = 'sysadmin@sys.adm';
        sysAdminUser.ProfileId = GlobalVariables.sysAdminProfileId;
        sysAdminUser.Email = sysAdminUser.Username;

        sysAdminUser.CommunityNickName = sysAdminUser.Email.substring(0, sysAdminUser.Email.indexOf('@')) +'_'+Math.round((Math.random() * 100000));

        sysAdminUser.TimeZoneSidKey = UserInfo.getTimeZone().getId();
        insert sysAdminUser;

        FamilyAppSettings__c fps = new FamilyAppSettings__c();
        fps.Name = 'FamilyIncome';
        fps.Main_Label_Field__c = 'FamilyIncome__c';
        fps.Sub_Label_Field__c = ' BasicTax__c';
        fps.Next_Page__c = 'Basic Tax Next Page';
        fps.Status_Field__c = 'statFamilyIncomeBoolean__c';
        fps.Completed_Field__c = 'statFamilyIncome__c';


        FamilyAppSettings__c appSettings1 = new FamilyAppSettings__c();
        appSettings1.Main_Label_Field__c = 'HouseholdInformation__c';
        appSettings1.Sub_Label_Field__c = 'ParentsGuardians__c';
        appSettings1.Name = 'ParentsGuardians';
        appSettings1.Next_Page__c = 'ApplicantInformation';

        FamilyAppSettings__c appSettings2 = new FamilyAppSettings__c();
        appSettings2.Main_Label_Field__c = 'HouseholdInformation__c';
        appSettings2.Sub_Label_Field__c = 'ApplicantInformation__c';
        appSettings2.Name = 'ApplicantInformation';
        appSettings2.Next_Page__c = 'DependentInformation';

        FamilyAppSettings__c appSettings3 = new FamilyAppSettings__c();
        appSettings3.Main_Label_Field__c = 'HouseholdInformation__c';
        appSettings3.Sub_Label_Field__c = '';
        appSettings3.Name = 'HouseholdInformation';
        appSettings3.Next_Page__c = 'ParentsGuardians';
        appSettings3.Is_Speedbump_Page__c = true;
        appSettings3.Status_Field__c = 'statHouseholdInformationBoolean__c';

        Database.insert(new List<FamilyAppSettings__c> {fps, appSettings1, appSettings2, appSettings3});


        // current year fees - these should be used
        Application_Fee_Settings__c feeSettings = new Application_Fee_Settings__c();
        feeSettings.Name = GlobalVariables.getCurrentYearString();
        feeSettings.Amount__c = 50;
        feeSettings.Discounted_Amount__c = 40;
        feeSettings.Product_Code__c = 'Test Product Code'; // [SL] NAIS-1031: new required field
        feeSettings.Discounted_Product_Code__c = 'KS Test Product Code'; // [SL] NAIS-1031: new required field
        insert feeSettings;

        /*
        TermsAndConditions__c tcTest = new TermsAndConditions__c();
        tcTest.Name = 'Terms and Conditions';
        insert tcTest;
        */
        tdf = new TestDataFamily(true);

        Question_Labels__c questionLabels = new Question_Labels__c();
        questionLabels.Academic_Year__c = tdf.academicYearId;
        questionLabels.Language__c = 'en_US';
        questionLabels.Add_l_Parent_Phone__c = 'Test Additional Phone Label';

        questionLabels.ParentsGuardians__c = 'Parents and Guardians';
        questionLabels.HouseholdInformation__c = 'Household Information';
        questionLabels.ApplicantInformation__c = 'Applicant Information';
        questionLabels.FamilyIncome__c = 'Family Income';
        questionLabels.BasicTax__c = 'Basic Tax Test';

        insert questionLabels;

        tdf.familyPortalUser.SYSTEM_Terms_and_Conditions_Accepted__c = System.now();
        update tdf.familyPortalUser;

        return tdf.familyPortalUser;
        //TODO put in custom settings for status fields
    }

    @isTest
    private static void testInit() {
        User u;
        System.runAs(currentUser) {
            u = setupTestData();
        }

        System.runAs(u) {
            PageReference pageRef = Page.FamilyDashboard;
            Test.setCurrentPage(pageRef);
            FamilyTemplateController template = new FamilyTemplateController();
            FamilyDashboardController roller = new FamilyDashboardController(template);
            roller.pageLoad();

            System.assertEquals(tdf.pfsA.Id, roller.pfsId);
        }
    }

    @isTest
    private static void testInitAndStages1() {
        User u;
        System.runAs(currentUser) {
            u = setupTestData();
        }
        Test.startTest();
        FamilyDashboardController roller;

        // instantiate to get pfsId
        System.runAs(u) {
            Test.setCurrentPage(Page.FamilyDashboard);
            FamilyTemplateController template = new FamilyTemplateController();
            roller = new FamilyDashboardController(template);
            roller.pageLoad();
        }

        PFS__c testPFS = [Select Id, PFS_Status__c from PFS__c where Id = :roller.pfsId];
        testPFS.PFS_Status__c = null;
        update testPFS;


        System.runAs(u) {
            Test.setCurrentPage(Page.FamilyDashboard);
            FamilyTemplateController template = new FamilyTemplateController();
            roller = new FamilyDashboardController(template);
            roller.pageLoad();
            System.assertEquals(roller.pfsId, testPFS.Id);
            System.assertEquals('PREPARE', roller.getPFSStage());

        }

        testPFS = [Select Id, PFS_Status__c from PFS__c where Id = :roller.pfsId];
        testPFS.PFS_Status__c = 'Application In Progress';
        update testPFS;

        System.runAs(u) {
            FamilyTemplateController template = new FamilyTemplateController();
            roller = new FamilyDashboardController(template);
            roller.pageLoad();
            System.assertEquals('COMPLETING', roller.getPFSStage());
        }

        testPFS.statFamilyIncomeBoolean__c = true;
        update testPFS;

        // "In Progress" and All Sections Complete is now "COMPLETING"
        System.runAs(u) {
            FamilyTemplateController template = new FamilyTemplateController();
            roller = new FamilyDashboardController(template);
            roller.pageLoad();
            System.assertEquals('COMPLETING', roller.getPFSStage());
        }

        Test.stopTest();
    }

    @isTest
    private static void testInitAndStages2() {
        User u;
        System.runAs(currentUser) {
            u = setupTestData();
        }

        Test.startTest();

        FamilyDashboardController roller;

        // instantiate to get pfsId
        System.runAs(u) {
            Test.setCurrentPage(Page.FamilyDashboard);
            FamilyTemplateController template = new FamilyTemplateController();
            roller = new FamilyDashboardController(template);
            roller.pageLoad();
        }

        PFS__c testPFS = [Select Id, PFS_Status__c from PFS__c where Id = :roller.pfsId];
        testPFS.PFS_Status__c = 'Application Submitted';
        update testPFS;

        System.runAs(u) {
            Test.setCurrentPage(Page.FamilyDashboard);
            FamilyTemplateController template = new FamilyTemplateController();
            roller = new FamilyDashboardController(template);
            roller.pageLoad();
            System.assertEquals(testPFS.Id, roller.pfsId);
            System.assertEquals('PAY', roller.getPFSStage());
        }

        Test.stopTest();
    }

    @isTest (seeAllData = false)
    private static void testInitAndMisc() {
        User u;
        Academic_Year__c ay2;
        PFS__c testPFS;
        System.runAs(currentUser) {
            u = setupTestData();

            testPFS = [Select Id, PFS_Status__c from PFS__c where Id = :tdf.pfsA.Id];
            testPFS.systemLastScreenParam__c = 'FamilyIncome';
            update testPFS;

            ay2 = TestUtils.createAcademicYear('2002-2003', true);
        }

        Test.startTest();
        System.runAs(u) {
            Test.setCurrentPage(Page.FamilyDashboard);
            FamilyTemplateController template = new FamilyTemplateController();
            FamilyDashboardController roller = new FamilyDashboardController(template);
            roller.pageLoad();

            System.assertEquals(tdf.pfsA.Id, roller.pfsId);

            System.assertEquals(false, roller.getFeeIsWaived());
            System.assertEquals(false, roller.getWaiverRequestedButNotYetWaived());
            // System.assertEquals(50, roller.getFee()); // [SL] NAIS-1031: commenting out, and putting this in a separate test

            System.assertEquals('Basic Tax Test', roller.getLastSectionName());
            //System.assertEquals('Basic Tax Info', roller.getLastSectionName());
            roller.getLastSectionURL();
            System.assertEquals(false, roller.getAllSectionsDone());
            System.assertEquals(false, roller.getIsPaid());

            roller.payNow();
            roller.submitPFS();

            template.YearSelector_OnChange(ay2.ID);
        }
        Test.stopTest();
    }

    @isTest (seeAllData = false)
    private static void testFeeWaiverMethods() {
        User u;
        Academic_Year__c ay2;
        PFS__c testPFS;
        System.runAs(currentUser) {
            u = setupTestData();

            testPFS = [Select Id, PFS_Status__c from PFS__c where Id = :tdf.pfsA.Id];
            testPFS.systemLastScreenParam__c = 'FamilyIncome';
            update testPFS;

            ay2 = TestUtils.createAcademicYear('2002-2003', true);
        }

        Application_Fee_Waiver__c afw = new Application_Fee_Waiver__c();
        afw.Contact__c = tdf.pfsA.Parent_A__c;
        afw.Academic_Year__c = GlobalVariables.getAcademicYearByName(tdf.pfsA.Academic_Year_Picklist__c).Id;
        afw.Account__c = tdf.School1.Id;
        insert afw;

        Test.startTest();
        System.runAs(u) {
            PageReference pageRef = Page.FamilyPaymentFeewaiverApplied;
            Test.setCurrentPage(pageRef);
            FamilyTemplateController template = new FamilyTemplateController();
            FamilyDashboardController roller = new FamilyDashboardController(template);
            roller.pageLoad();

            System.assertEquals(tdf.pfsA.Id, roller.pfsId);
            // should now be true since we just inserted a waiver
            System.assertEquals(true, roller.getWaiverRequestedButNotYetWaived());
        }

        tdf.pfsA.Fee_Waived__c = 'Yes';
        update tdf.pfsA;

        afw.Status__c = 'Assigned';
        update afw;

        System.runAs(u) {
            FamilyTemplateController template = new FamilyTemplateController();
            FamilyDashboardController roller = new FamilyDashboardController(template);
            roller.pageLoad();

            System.assertEquals(tdf.pfsA.Id, roller.pfsId);
            // should now be false since fee is waived
            System.assertEquals(false, roller.getWaiverRequestedButNotYetWaived());
        }

        Test.stopTest();
    }

    @isTest // [SL] NAIS-1031 Test getFee()
    private static void testGetFeeBeforeSubmit() {
        User u;
        Academic_Year__c ay2;
        PFS__c testPFS;
        System.runAs(currentUser) {
            u = setupTestData();

            testPFS = [Select Id, PFS_Status__c from PFS__c where Id = :tdf.pfsA.Id];
            testPFS.PFS_Status__c = 'Application In Progress';
            update testPFS;

            ay2 = TestUtils.createAcademicYear('2002-2003', true);
        }

        System.runAs(u) {
            PageReference pageRef = Page.FamilyPaymentFeewaiverApplied;
            Test.setCurrentPage(pageRef);
            FamilyTemplateController template = new FamilyTemplateController();
            FamilyDashboardController roller = new FamilyDashboardController(template);
            roller.pageLoad();
            System.assertEquals(tdf.pfsA.Id, roller.pfsId);
            System.assertEquals(50, roller.fee);
        }
    }

    @isTest // [SL] NAIS-1031 Test getFee()
    private static void testGetFeeBeforeSubmitKS() {
        User u;
        Academic_Year__c ay2;
        PFS__c testPFS;
        System.runAs(currentUser) {
            Account ksSchool = TestUtils.createAccount('KS School', RecordTypes.schoolAccountTypeId, 3, true);
            Account nonKSSchool = TestUtils.createAccount('Non-KS School', RecordTypes.schoolAccountTypeId, 3, true);


            u = setupTestData();

        Test.startTest();

            testPFS = [Select Id, PFS_Status__c, Academic_Year_Picklist__c from PFS__c where Id = :tdf.pfsA.Id];
            testPFS.PFS_Status__c = 'Application In Progress';
            update testPFS;

            ay2 = TestUtils.createAcademicYear('2002-2003', true);

            // set to only KS school
            tdf.schoolpfsa1.Withdrawn__c = 'Yes';
            tdf.schoolpfsa2.Withdrawn__c = 'Yes';
            update new List<School_PFS_Assignment__c> {tdf.schoolpfsa1, tdf.schoolpfsa2};
            Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, true);
            Applicant__c applicant1 = TestUtils.createApplicant(student1.Id, testPFS.Id, true);
            Student_Folder__c studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', testPFS.Academic_Year_Picklist__c, student1.Id, true);
            School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(testPFS.Academic_Year_Picklist__c, applicant1.Id, tdf.schoolKS.Id, studentFolder1.Id, false);
            spfsa1.PFS_Status__c = 'Application In Progress';
            insert spfsa1;
        }


        System.runAs(u) {
            PageReference pageRef = Page.FamilyPaymentFeewaiverApplied;
            Test.setCurrentPage(pageRef);
            FamilyTemplateController template = new FamilyTemplateController();
            FamilyDashboardController roller = new FamilyDashboardController(template);
            roller.pageLoad();
            System.assertEquals(tdf.pfsA.Id, roller.pfsId);
            System.assertEquals(40, roller.fee);
        }

        Test.stopTest();
    }

    @isTest // [SL] NAIS-1031 Test getFee()
    private static void testGetFeeAfterSubmit() {
        User u;
        Academic_Year__c ay2;
        PFS__c testPFS;
        System.runAs(currentUser) {
            u = setupTestData();

            Test.startTest();
            testPFS = [Select Id, PFS_Status__c from PFS__c where Id = :tdf.pfsA.Id];
            testPFS.PFS_Status__c = 'Application Submitted';
            update testPFS;

            ay2 = TestUtils.createAcademicYear('2002-2003', true);
        }

        System.runAs(u) {
            PageReference pageRef = Page.FamilyPaymentFeewaiverApplied;
            Test.setCurrentPage(pageRef);
            FamilyTemplateController template = new FamilyTemplateController();
            FamilyDashboardController roller = new FamilyDashboardController(template);
            roller.pageLoad();
            System.assertEquals(tdf.pfsA.Id, roller.pfsId);
            System.assertEquals(50, roller.fee);
        }
        Test.stopTest();
    }

    @isTest // [SL] NAIS-1031 Test getFee()
    private static void testGetFeeAfterSubmitKS() {
        User u;
        Academic_Year__c ay2;
        PFS__c testPFS;
        System.runAs(currentUser) {
            Account ksSchool = TestUtils.createAccount('KS School', RecordTypes.schoolAccountTypeId, 3, true);
            Account nonKSSchool = TestUtils.createAccount('Non-KS School', RecordTypes.schoolAccountTypeId, 3, true);

            u = setupTestData();

        }

        Test.startTest();

        System.runAs(currentUser) {
            testPFS = [Select Id, PFS_Status__c, Academic_Year_Picklist__c from PFS__c where Id = :tdf.pfsA.Id];
            testPFS.PFS_Status__c = 'Application In Progress';
            // testPFS.PFS_Status__c = 'Application Submitted';
            update testPFS;

            ay2 = TestUtils.createAcademicYear('2002-2003', true);

            // set to only KS school
            tdf.schoolpfsa1.Withdrawn__c = 'Yes';
            tdf.schoolpfsa2.Withdrawn__c = 'Yes';
            update new List<School_PFS_Assignment__c> {tdf.schoolpfsa1, tdf.schoolpfsa2};
            Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, true);
            Applicant__c applicant1 = TestUtils.createApplicant(student1.Id, testPFS.Id, true);
            Student_Folder__c studentFolder1 = TestUtils.createStudentFolder('Student Folder 1', testPFS.Academic_Year_Picklist__c, student1.Id, true);
            School_PFS_Assignment__c spfsa1 = TestUtils.createSchoolPFSAssignment(testPFS.Academic_Year_Picklist__c, applicant1.Id, tdf.schoolKS.Id, studentFolder1.Id, false);
            spfsa1.PFS_Status__c = 'Application Submitted';
            insert spfsa1;
        }

        System.runAs(u) {
            PageReference pageRef = Page.FamilyPaymentFeewaiverApplied;
            Test.setCurrentPage(pageRef);
            FamilyTemplateController template = new FamilyTemplateController();
            FamilyDashboardController roller = new FamilyDashboardController(template);
            roller.pageLoad();
            System.assertEquals(tdf.pfsA.Id, roller.pfsId);

            System.assertEquals(40, roller.fee);
        }
        Test.stopTest();
    }

    @isTest
    private static void testInitAndSchoolPFSAssignments() {
        User u;
        System.runAs(currentUser) {
            u = setupTestData();
        }

        FamilyDashboardController roller;

        System.runAs(u) {
            Test.setCurrentPage(Page.FamilyDashboard);
            FamilyTemplateController template = new FamilyTemplateController();
            roller = new FamilyDashboardController(template);
            roller.pageLoad();
            System.assertEquals(tdf.pfsA.Id, template.pfsId);
            System.assertEquals(tdf.pfsA.Id, roller.pfsId);
        }


        Test.startTest();
        List<School_PFS_Assignment__c> testSPFSAs;

        testSPFSAs = [Select Id from School_PFS_Assignment__c where Applicant__r.PFS__c = :roller.pfsId];
        System.assertEquals(2, testSPFSAs.size());
        System.assertEquals(2, roller.schPFSWrappers.size());
        System.assertEquals(false, roller.schPFSWrappers[0].isLocked);
        System.assertEquals(false, roller.schPFSWrappers[1].isLocked);

        testSPFSAs[0].Family_May_Submit_Updates__c = 'No';
        testSPFSAs[1].Family_May_Submit_Updates__c = 'No';
        update testSPFSAs;

        System.runAs(u) {

            FamilyTemplateController template = new FamilyTemplateController();
            roller = new FamilyDashboardController(template);
            roller.pageLoad();

            System.assertEquals(tdf.pfsA.Id, roller.pfsId);

            // ann set deadline is set to yesterday
            System.assertEquals(false, roller.schPFSWrappers[0].deadLinePassed);
        }

        tdf.annSetSchool1.PFS_Deadline_Type__c = AnnualSettingHelper.DEADLINE_TYPE_HARD;
        update tdf.annSetSchool1;

        System.runAs(u) {

            FamilyTemplateController template = new FamilyTemplateController();
            roller = new FamilyDashboardController(template);
            roller.pageLoad();

            System.assertEquals(tdf.pfsA.Id, roller.pfsId);

            // ann set deadline is set to yesterday
            System.assertEquals(true, roller.schPFSWrappers[0].deadLinePassed);
        }

        System.assertEquals(2, roller.schPFSWrappers.size());
        System.assertEquals(true, roller.schPFSWrappers[0].isLocked);
        System.assertEquals(true, roller.schPFSWrappers[1].isLocked);

        tdf.school1.SSS_Subscriber_Status__c = 'Expired';
        update tdf.school1;

        System.runAs(u) {

            FamilyTemplateController template = new FamilyTemplateController();
            roller = new FamilyDashboardController(template);
            roller.pageLoad();

            System.assertEquals(tdf.pfsA.Id, roller.pfsId);
        }

        Test.stopTest();

        System.assertEquals(2, roller.schPFSWrappers.size());
        System.assertEquals(false, roller.schPFSWrappers[0].isSubscribed);
        System.assertEquals(false, roller.schPFSWrappers[1].isSubscribed);
    }

    @isTest
    private static void processEarlyAccess_noCookie_userNotUpdated() {
        User schoolPortalUser = UserTestData.insertSchoolPortalUser();
        Test.setCurrentPage(Page.FamilyDashboard);

        FamilyDashboardController controller = new FamilyDashboardController(new FamilyTemplateController());
        controller.userRecord = schoolPortalUser;

        Integer dmlCount = System.Limits.getDmlRows();

        Test.startTest();
            controller.processEarlyAccess();
        Test.stopTest();

        System.assertEquals(dmlCount, System.Limits.getDmlRows(), 'Expected there to be no additional dml transactions.');
    }

    @isTest
    private static void processEarlyAccess_cookieExists_userUpdated() {
        User schoolPortalUser = UserTestData.insertSchoolPortalUser();
        Cookie earlyAccessCookie = new Cookie('earlyaccess', 'true', '/', 300, false);
        ApexPages.currentPage().setCookies(new List<Cookie> { earlyAccessCookie });

        Test.setCurrentPage(Page.FamilyDashboard);

        FamilyDashboardController controller = new FamilyDashboardController(new FamilyTemplateController());
        controller.userRecord = schoolPortalUser;

        Integer dmlCount = System.Limits.getDmlRows();

        System.runAs(schoolPortalUser) {
            Test.startTest();
                controller.processEarlyAccess();
            Test.stopTest();
        }

        System.assertEquals(dmlCount+1, System.Limits.getDmlRows(), 'Expected there to be one more dml transaction.');
    }

    @isTest
    private static void getFAQs_prePaymentFAQRender_returnsItems() {
        User u;
        currentUser.UserPermissionsKnowledgeUser = true;
        update currentUser;

        System.runAs( currentUser) {
            u = setupTestData();

            Integer numberOfArticles = 15;
            List<Knowledge__kav> articles = KnowledgeAVTestData.Instance.insertKnowledgeAVs(numberOfArticles);

            // Create articles with the default category.
            String dataCategoryName = 'Pre_Payment_FAQ';
            List<Knowledge__DataCategorySelection> categories = new List<Knowledge__DataCategorySelection>();
            for (Integer i=0; i<numberOfArticles; i++) {
                categories.add(KnowledgeDCSTestData.Instance
                    .forDataCategoryName(dataCategoryName)
                    .forParentId(articles[i].Id).create());
            }
            insert categories;

            articles = KnowledgeTestHelper.getKnowledgeArticleByIds(articles);
            KnowledgeTestHelper.publishArticles(articles);
        }

        System.runAs(u) {
            PageReference pageRef = Page.FamilyDashboard;
            Test.setCurrentPage( pageRef);

            FamilyTemplateController template = new FamilyTemplateController();
            FamilyDashboardController roller = new FamilyDashboardController( template);
            roller.pageLoad();
            List<KnowledgeDataAccessService.KnowledgeWrapper> faqs = roller.getFaqs();

            // Right now this protal user doens't have acess to see the FAQ's, because the channels are not yet.
            // We cannot set the channels via APEX in SFDC as of yet, so I'm just asserting that nothing was returned.
            System.assertEquals(0, faqs.size(), 'Number of FAQ\'s did not match expected size');
        }
    }

    @isTest
    private static void areBusinessFarmsCompleted_checkBusinessFarms_false() {
        User u;
        List<Business_Farm__c> businessFarms = new List<Business_Farm__c>();

        System.runAs( currentUser) {
            u = setupTestData();
            Business_Farm__c businessFarm = new Business_Farm__c();
            businessFarm.statBusinessInformation__c = false;
            businessFarm.Pfs__c = tdf.pfsA.Id;
            insert businessFarm;

            businessFarms.add( businessFarm);
        }

        System.runAs( u) {
            PageReference pageRef = Page.FamilyDashboard;
            Test.setCurrentPage( pageRef);

            FamilyTemplateController template = new FamilyTemplateController();
            FamilyDashboardController roller = new FamilyDashboardController( template);
            roller.pageLoad();
            Boolean isComplete = roller.areBusinessFarmsComplete( businessFarms);

            System.assert(!isComplete, 'Business Farms are not complete');
        }
    }

    @isTest
    private static void getNextOutstandingItems_hasOutstanding_outstandingAvailable() {
        User u;

        System.runAs( currentUser) {
            u = setupTestData();

            PFS__c testPFS = [Select Id, StatFamilyIncomeBoolean__c from PFS__c where Id = :tdf.pfsA.Id];
            testPFS.StatFamilyIncomeBoolean__c = false;
            update testPFS;
        }

        System.runAs( u) {
            PageReference pageRef = Page.FamilyDashboard;
            Test.setCurrentPage( pageRef);

            FamilyTemplateController template = new FamilyTemplateController();
            FamilyDashboardController roller = new FamilyDashboardController( template);
            roller.pageLoad();

            List<String> outstandingItems = roller.getNextOutStandingItems();

            Boolean contains = false;
            for( String s : outstandingItems) {
                contains = s.equals( 'Basic Tax Test') ? true : false;
            }
            System.assert(contains);

        }
    }
}