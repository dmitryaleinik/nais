/**
* FamilyHelpResultsDetailControllerTest.cls
*
* @description: Test class for FamilyHelpResultsDetailController.cls using the Arrange/Act/Assert pattern. Method naming: <methodName>_<condition>_<expectedResult>
*
* @author: Mike Havrila @ Presence PG
*/

@isTest (seeAllData = false)
private class FamilyHelpResultsDetailControllerTest {

    static List<Knowledge__kav> articles;
    static User testUser;

    /* test data setup */
    static {
        testUser = CurrentUser.getCurrentUser();
        testUser.UserPermissionsKnowledgeUser = true;
        update testUser;

        System.runAs( testUser) {
            articles = KnowledgeAVTestData.Instance.insertKnowledgeAVs(2);

            // Create articles with the default category.
            List<Knowledge__DataCategorySelection> categories = new List<Knowledge__DataCategorySelection>();

            for (Knowledge__kav article : articles) {
                categories.add(KnowledgeDCSTestData.Instance.forParentId(article.Id).create());
            }

            insert categories;

            articles = KnowledgeTestHelper.getKnowledgeArticleByIds(articles);

            KnowledgeTestHelper.publishArticles( articles);
        }
    }

    // test page action method w/ valid user, valid redirect
    @isTest private static void initModelAndView_redirectToCategoryPage_successRedirect() {
        // Act
        System.runAs( testUser) {
            PFS__c pfs = PfsTestData.Instance.DefaultPfs;
            AcademicYearTestData.Instance.insertAcademicYears(5);

            Test.startTest();
                PageReference initPage = Page.FamilyHelpResultsDetail;
                initPage.getParameters().put('id', pfs.Id);
                initPage.getParameters().put('articleId', articles.get(0).KnowledgeArticleId );
                initPage.getParameters().put('returnUrl', 'MyReturnUrl');

                Test.setCurrentPage( initPage);

                FamilyTemplateController templateContoller = new FamilyTemplateController();
                FamilyHelpResultsDetailController controller = new FamilyHelpResultsDetailController( templateContoller);

            Test.stopTest();

            // Assert
            System.assertEquals( articles.get(0).Id, controller.knowledgeArticle.Id, 'Knowledge Article Id\'s do not match');
            // implement me
        }
    }
}