public with sharing class TableIncomeProtectionAllowance {

    private static Map<Id, List<Income_Protection_Allowance__c>> incomeProtectionAllowanceDataByYear = new Map<Id, List<Income_Protection_Allowance__c>>();

    public static List<Income_Protection_Allowance__c> getIncomeProtectionAllowanceData(Id academicYearId) {
        List<Income_Protection_Allowance__c> incomeProtectionAllowanceData = incomeProtectionAllowanceDataByYear.get(academicYearId);
        if (incomeProtectionAllowanceData == null) {
            incomeProtectionAllowanceData = [SELECT Family_Size__c, Allowance__c, Family_Size_Multiplier__c, Housing_Allowance__c, Other_Allowance__c
                                                FROM Income_Protection_Allowance__c
                                                WHERE Academic_Year__c = :academicYearId
                                                ORDER BY Family_Size__c ASC];
            incomeProtectionAllowanceDataByYear.put(academicYearId, incomeProtectionAllowanceData);
        }
        return incomeProtectionAllowanceData;
    }

    // [CH] NAIS-1885 Adding to handle splitting out housing and other
    public static List<Decimal> getIncomeProtectionAllowance(EfcWorksheetData efcData) {
        Decimal allowance = 0;
        if (efcData.familySize == null) {
            throw new EfcException('Missing family size for Income Protection Allowance lookup');
        }

        List<Income_Protection_Allowance__c> incomeProtectionAllowanceData = getIncomeProtectionAllowanceData(efcData.academicYearId);
        if (incomeProtectionAllowanceData.isEmpty()) {
            throw new EfcException.EfcMissingDataException('No Income Protection Allowance data found for academic year \'' + EfcUtil.getAcademicYearName(efcData.academicYearId) + '\'');
        }

        // Scan the set of IPA records
        Income_Protection_Allowance__c lastIpa = null;
        for (Income_Protection_Allowance__c ipa : incomeProtectionAllowanceData) {
            lastIpa = ipa;
            if (ipa.Family_Size__c == efcData.familySize) {
                break;
            }
        }

        // Get the SSS Constants record for the current year
        SSS_Constants__c constants = EfcConstants.getSssConstantsForYear(efcData.AcademicYearId);
        if (constants == null) {
            throw new EfcException.EfcMissingDataException('No SSS Constants data found for academic year \'' + EfcUtil.getAcademicYearName(efcData.academicYearId) + '\'');
        }

        Decimal ipaHousing = 0;
        // If the school wants to adjust the housing portion of IPA
        // Note:  We need to calculate the Housing portion even if there's a manual
        //             override value because we need to populate the Orig_Income_Protection_Allowance__c field
        if(efcData.adjustHousingPortion == 'Yes' && efcData.ipaHousingFamilyOf4 != null && efcData.ipaHousingFamilyOf4 > 0){
            // Look up the multipliers and additional constant values
            Decimal multiplier = ExpCore.defaultForNull(lastIpa.Family_Size_Multiplier__c, 0);
            // If the family size is greater than the highest IPA size
            if(efcData.familySize > lastIpa.Family_Size__c){
                // Add the correct amount to the multiplier for each additional member beyond the largest defined IPA record
                Decimal multiplierPerAdditional = ExpCore.defaultForNull(constants.IPA_Housing_Multiplier_for_Additional__c, 0);
                Decimal extraMembers = efcData.familySize - lastIpa.Family_Size__c;
                multiplier = multiplier + (multiplierPerAdditional * extraMembers);
            }
            // Multiply the family of 4 value by the multiplier
            ipaHousing = efcData.ipaHousingFamilyOf4 * multiplier;
        }
        else{ // Else
            // Add the standard value for Housing based on family size
            ipaHousing = lastIpa.Housing_Allowance__c;
            // If the family size is greater than the highest IPA size
            if(efcData.familySize > lastIpa.Family_Size__c){
                // Add the correct Housing amount for additional members
                Decimal constantPerAdditional = ExpCore.defaultForNull(constants.IPA_Housing_For_Each_Additional__c, 0);
                Decimal extraMembers = efcData.familySize - lastIpa.Family_Size__c;
                ipaHousing = ipaHousing + (constantPerAdditional * extraMembers);
            }
        }

        // [CH] NAIS-1885 Need to check if the calculated value has changed before the totals are calculated
        if(efcData.incomeProtectionAllowanceHousingOrig != ipaHousing && efcData.incomeProtectionAllowanceHousingOrig != null){
            efcData.incomeProtectionAllowanceHousing = ipaHousing;
        }

        // Add the standard value for Other based on family size
        Decimal ipaOther = lastIpa.Other_Allowance__c;
        // If the family size is greater than the highest IPA size
        if(efcData.familySize > lastIpa.Family_Size__c){
            // Add the correct Other amount for additional members
            Decimal constantPerAdditional = ExpCore.defaultForNull(constants.IPA_Other_For_Each_Additional__c , 0);
            Decimal extraMembers = efcData.familySize - lastIpa.Family_Size__c;
            ipaOther = ipaOther + (constantPerAdditional * extraMembers);
        }

        // Add together the Housing and Other amounts
        Decimal ipaTotal;
        // If there has been an amount manually entered and the school
        //   wants to adjust the housing portion
        if(efcData.incomeProtectionAllowanceHousing != null && efcData.incomeProtectionAllowanceHousing > 0 && efcData.adjustHousingPortion == 'Yes'){
            // Use the manually entered value
            ipaTotal = efcData.incomeProtectionAllowanceHousing + ipaOther;
        }
        else{
            ipaTotal = ipaHousing + ipaOther;
        }

        // Return the results for the IPA values lines 62a, 62b, and 62 respectively.
        return new List<Decimal> {ipaHousing, ipaOther, ipaTotal};
    }
}