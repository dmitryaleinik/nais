/**
 * @description Pass the user through to the family view of the PFS.
 */
public without sharing class CallCenterPFSFamilyView {
    @testVisible private static final String FAMILY_VIEW_MESSAGE =
            'This page should be accessed from the "Family View" button on a PFS record.';

    private Id pfsId;

    /**
     * @description Collect out the PFS Id and display an error message
     *              if it doesn't exist.
     */
    public CallCenterPFSFamilyView() {
        pfsId = System.currentPagereference().getParameters().get('id');
        if (pfsId == null) {
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, FAMILY_VIEW_MESSAGE));
        }
    }

    /**
     * @description Set up the page reference to relocate the user to
     *              the family dashboard.
     */
    public PageReference init() {
        if (pfsId == null) {
            return null;
        }

        PageReference pr = Page.FamilyDashboard;
        pr.getParameters().put('id', pfsId);
        return pr;
    }
}