public with sharing class FamilySamplePFSController {

    /*Initialization*/
        public FamilySamplePFSController(){
    
        }
    /*End Initialization*/
    
    /*Properties*/
        public PFS__c pfs {get; set;}
        
        public ApplicationUtils appUtils {
            get{
                if(appUtils == null){
                    appUtils = new ApplicationUtils('en_US', pfs);    
                }
                return appUtils;
            }
            set;
        }
    
    /*End Properties*/
    
    /*Action Methods*/
    /*End Action Methods*/
    
    /*Helper Methods*/
    /*End Helper Methods*/


    

}