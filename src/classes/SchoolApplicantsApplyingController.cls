/*
SFP-97 - Intial Development - [G.S]
Allow for internal users to generate a list of schools that their applicants are applying to. 
There would be a page where a single SSS code and academic year (picklist) would be entered
*/

public without sharing class SchoolApplicantsApplyingController implements SchoolAcademicYearSelectorInterface{
    
    public String sssCode {get;set;}
    public School_PFS_Assignment__c selectedSPfsa {get;set;}
    public transient List<School_PFS_Assignment__c> spfsaList {get;set;}
    public String forExcel {get;set;}

    public boolean isSchoolUser{get;set;}//SFP-328
    
    public SchoolApplicantsApplyingController(){
        this.isSchoolUser = (GlobalVariables.isSchoolPortalProfile(UserInfo.getProfileId())?true:false);
        selectedSPfsa = new School_PFS_Assignment__c();
        forExcel = ApexPages.currentPage().getParameters().get('forExcel');
        sssCode = GlobalVariables.getCurrentSchoolSSSNumber();//SFP-328
        selectedSPfsa.Academic_Year_Picklist__c = !this.isSchoolUser?'':GlobalVariables.getAcademicYear(getAcademicYearId()).Name;//SFP-328
        
        if(String.isNotBlank(forExcel) && forExcel == 'yes'){
            sssCode =  ApexPages.currentPage().getParameters().get('sssCodeExcel');
            newAcademicYearId = ApexPages.currentPage().getParameters().get('selectedYearExcel');//SFP-328
            selectedSPfsa.Academic_Year_Picklist__c = ApexPages.currentPage().getParameters().get('selectedYearExcel');
            searchSchools();
        }
    }
    
    public Pagereference searchSchools(){
        
        Boolean hasError = false;
        Set<ID> applicantsSet = new Set<ID>();
        String selectedYear = !this.isSchoolUser || forExcel == 'yes'?
                                    selectedSPfsa.Academic_Year_Picklist__c:
                                    GlobalVariables.getAcademicYear(newAcademicYearId).Name;//SFP-328

        if(String.isBlank(sssCode)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter SSS Code'));
            hasError = true;
        }
 
        if(String.isBlank(selectedYear)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select Academic Year'));
            hasError = true;
        }
        
        if(hasError)
            return null;


        for(School_PFS_Assignment__c spfsa : [Select Applicant__c FROM School_PFS_Assignment__c
                                                WHERE Academic_Year_Picklist__c = :selectedYear
                                                AND School__r.SSS_School_Code__c = :sssCode
                                                AND Withdrawn__c = 'No']){
            applicantsSet.add(spfsa.Applicant__c);
        }
       
        if(applicantsSet.size() > 0){
            spfsaList = new WithoutSharingQuerier().query(selectedYear, applicantsSet, sssCode);
            if(spfsaList.size() == 0){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'No results for the entered criteria.'));
            }
        }
        else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'No results for the entered criteria.'));
        }
        return null;
    }
    
    public SchoolApplicantsApplyingController getThisController() {
        return this;
    }
    private void loadAcademicYearId()
    {
        if(this.newAcademicYearId==null){
            string urlAcademicyearid = ApexPages.currentPage().getParameters().get('academicyearid');
            setAcademicYearId(urlAcademicyearid!=null?urlAcademicyearid:GlobalVariables.getCurrentAcademicYear().Id);
        }
    }
    
    /*SchoolAcademicYearSelectorInterface Implementation*/    
    public String newAcademicYearId;
    
    public String getAcademicYearId() {
        if(this.newAcademicYearId==null)
            loadAcademicYearId();
        return this.newAcademicYearId;
    }
    
    public void setAcademicYearId(String academicYearId) {
        this.newAcademicYearId = academicYearId;
    }
    
    public PageReference SchoolAcademicYearSelector_OnChange(Boolean saveRecord) {        
        PageReference newPage = Page.SchoolApplicantsApplying;
        if (newAcademicYearId != null && newAcademicYearId != '') {
            newPage.getParameters().put('academicyearid', newAcademicYearId);
        }
        newPage.setRedirect(true);
        return newPage;
    }    
    /*End SchoolAcademicYearSelectorInterface Implementation*/

    private without sharing class WithoutSharingQuerier {
        public List<School_PFS_Assignment__c> query(String selectedYear, Set<Id> applicantIds, String currentSssCode) {
            return [SELECT School__r.SSS_School_Code__c, School__r.Name,
                           Applicant__r.First_Name__c, Applicant__r.Last_Name__c, Day_Boarding__c, New_Returning__c
                    FROM School_PFS_Assignment__c
                    WHERE Academic_Year_Picklist__c = :selectedYear
                        AND Applicant__c IN :applicantIds
                        AND Withdrawn__c = 'No'
                        AND School__r.SSS_School_Code__c<>:currentSssCode
                    ORDER BY School__r.Name asc, Applicant__r.Last_Name__c ASC LIMIT 10000];
        }
    }
}