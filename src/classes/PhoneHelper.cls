public with sharing class PhoneHelper
{

    // doesn't work private static String usPhonePattern = '(\\d-)?(\\d{3}-)?\\d{3}-\\d{4}';
    
    private static String complexStringPattern = '^(?=.{7,32}$)(\\(?\\+?[0-9]*\\)?)?[0-9_\\- \\(\\)]*((\\s?x\\s?|ext\\s?|extension\\s?)\\d{1,5}){0,1}$';

    public static boolean isValidPhone(String phoneNumber)
    {
        Pattern complexPattern = Pattern.compile(complexStringPattern);
        Matcher m = complexPattern.matcher(phoneNumber);
                
        return m.matches(); 
    }

    public static String getSFPhone(String phoneNumber)
    {
        
        if (!isValidPhone(phoneNumber)) 
            return null;
        
        Pattern nonDIGITS = Pattern.compile('\\D+');
        Matcher stripSymbols = nonDIGITS.matcher(phoneNumber);
        phoneNumber = stripSymbols.replaceALL('');
        phoneNumber = stripLeading1(phoneNumber);
        phoneNumber = '(' +phoneNumber.substring(0,3)+') '+ phoneNumber.substring(3,6) +'-'+phoneNumber.substring(6,10);
        
        return phoneNumber; 
    }

    public static String getSFSearchablePhone(String phoneNumber)
    {
        if (!isValidPhone(phoneNumber))
            return null;
        
        Pattern nonDIGITS = Pattern.compile('\\D+');
        Matcher stripSymbols = nonDIGITS.matcher(phoneNumber);
        phoneNumber = stripSymbols.replaceALL('');
        phoneNumber = stripLeading1(phoneNumber);
        phoneNumber = '%' +phoneNumber.substring(0,3)+'%'+ phoneNumber.substring(3,6) +'%'+phoneNumber.substring(6,10);
        
        return phoneNumber; 
    }
    
    public static String stripLeading1(String phone)
    {
        Pattern stripLeading1 = Pattern.compile('^\\D*1');
        Matcher m = stripLeading1.matcher(phone);
        
        return m.replaceALL('');
    }
}