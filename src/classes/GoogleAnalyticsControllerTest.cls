@isTest
private class GoogleAnalyticsControllerTest {
    @isTest
    private static void googleAnalyticsCode_noCustomSettingSet_expectNull() {
        GoogleAnalyticsController controller = new GoogleAnalyticsController();

        Test.startTest();
        System.runAs(UserTestData.insertSchoolPortalUser()) {
            System.assertEquals(null, controller.GoogleAnalyticsCode, 'Expected the value to be null.');
        }
        Test.stopTest();
    }

    @isTest
    private static void googleAnalyticsCode_customSettingSet_expectCustomSettingReturned() {
        GoogleAnalyticsController controller = new GoogleAnalyticsController();
        Google_Analytics__c analyticsCode = new Google_Analytics__c(
                Name = 'School_Portal', Google_Analytics_Code__c = 'UA-12345');
        insert analyticsCode;

        Test.startTest();
        System.runAs(UserTestData.insertSchoolPortalUser()) {
            System.assertEquals('UA-12345', controller.GoogleAnalyticsCode, 'Expected the value to be the same.');
        }
        Test.stopTest();
    }
}