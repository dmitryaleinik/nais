/**
 * Class: SpringCMAjaxUploadFallbackController
 *
 * Copyright (C) 2015
 *
 * Purpose: Controller for fallback (non-ajax) upload page, which can't be in a modal
 * 
 * Where Referenced: SpringCMAjaxUploadFallback.page
 *   
 *
 * Change History:
 *
 * Developer         Date                          Description
 * ---------------------------------------------------------------------------------------
 * Drew Piston      2015.10.20            Initial Development
 *
 * 
 */


public class SpringCMAjaxUploadFallbackController {

    // these properties are needed for the component
    public Family_Document__c fDoc {get; set;}
    public String pfsNumber {get; set;}
    public String hhId {get; set;}
    public String hhName {get; set;}
    public PFS__c thePFS {get; set;}

    // constructor -- get url params and do query so we can render the component
    public SpringCMAjaxUploadFallbackController() {

        String fDocId = System.currentPagereference().getParameters().get('fdocid');

        fDoc = [Select Id, Name, Document_Year__c, Document_Type__c, Household__c, Household__r.Name, Document_Source__c, School_Code__c, Document_Pertains_To__c 
                    FROM Family_Document__c 
                    WHERE Id = :fDocId];
        pfsNumber = System.currentPagereference().getParameters().get('pfsnum');
        thePFS = [Select Id, Academic_Year_Picklist__c FROM PFS__c where PFS_Number__c = :pfsNumber];
        Academic_Year__c acadYear = [Select Id, Name FROM Academic_Year__c where Name = :thePFS.Academic_Year_Picklist__c];
        hhId = fDoc.Household__c;
        hhName = fDoc.Household__r.Name;
    }


    // SFP-29 START
    public String DBVerifyFlag {
        get {
            Boolean doVerify = false;
            for (School_Document_Assignment__c sda : [select Verify_Data__c from School_Document_Assignment__c where Document__c = :fDoc.Id and Verify_Data__c = true]) {
                doVerify = sda.Verify_Data__c;
            }
            return doVerify ? 'T' : 'F';
        }
    }
}

/*

WRAPPED IN PORTAL

public class SpringCMAjaxUploadFallbackController implements FamilyPortalTopBarInterface{

    public Family_Document__c fDoc {get; set;}
    public String pfsNumber {get; set;}
    public String hhId {get; set;}
    public String hhName {get; set;}
    public PFS__c thePFS {get; set;}


    public SpringCMAjaxUploadFallbackController() {

        String fDocId = System.currentPagereference().getParameters().get('fdocid');

        fDoc = [Select Id, Name, Document_Year__c, Document_Type__c, Household__c, Household__r.Name, Document_Source__c, School_Code__c, Document_Pertains_To__c 
                    FROM Family_Document__c 
                    WHERE Id = :fDocId];
        pfsNumber = System.currentPagereference().getParameters().get('pfsnum');
        String pfsId = System.currentPagereference().getParameters().get('id');
        String academicYearId = System.currentPagereference().getParameters().get('academicyearid');

        if (String.isBlank(pfsID)){
            thePFS = [Select Id, Academic_Year_Picklist__c, PFS_Status__c FROM PFS__c where PFS_Number__c = :pfsNumber];
        } else {
            thePFS = new PFS__c(Id = pfsId);
        }

        User currentUser = GlobalVariables.getCurrentUser();
        if (GlobalVariables.isSysAdminUser(currentUser) || GlobalVariables.isCallCenterUser(currentUser)){
            thePFS = ApplicationUtils.queryPFSRecord(thePFS.Id, null, null);
            // if this not a sys admin or call center user, get record based on contact id and academic year
        } else {
            thePFS = ApplicationUtils.queryPFSRecord(null, currentUser.ContactId, academicYearId);
        }


        //Academic_Year__c acadYear = [Select Id, Name FROM Academic_Year__c where Name = :thePFS.Academic_Year_Picklist__c];
        hhId = fDoc.Household__c;
        hhName = fDoc.Household__r.Name;
    }

    public SpringCMAjaxUploadFallbackController Me {
        get { return this; }
    }


    public String getAcademicYearId(){
        return GlobalVariables.getAcademicYearByName(thePFS.Academic_Year_Picklist__c).Id;
    }
    
    public PageReference YearSelector_OnChange(Id newAcademicYearId){
        
        String acadYearId = newAcademicYearId;
        
        PageReference pr = Page.FamilyDashboard;
        pr.getParameters().put('academicyearid', acadYearId);
        return pr;        
    }

}


*/