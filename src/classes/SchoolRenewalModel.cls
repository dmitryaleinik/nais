public with sharing class SchoolRenewalModel
{

    public Date subscriptionExpiration;
    public String getSubscriptionExpiration() {
        return subscriptionExpiration.month() + '/' + subscriptionExpiration.day() + '/' + subscriptionExpiration.year();
    }
    public String subscriptionType { get; set; }
    
    public Date newSubscriptionStartDate;
    public String getNewSubscriptionStartDate() {
        return newSubscriptionStartDate.month() + '/' + newSubscriptionStartDate.day() + '/' + newSubscriptionStartDate.year();
    }
    public Date newSubscriptionEndDate;
    public String getNewSubscriptionEndDate() {
        return newSubscriptionEndDate.month() + '/' + newSubscriptionEndDate.day() + '/' + newSubscriptionEndDate.year();
    }
    public String newSubscriptionType { get; set; }
    
    public List<SelectOption> subscriptionTypeOptions { get; set; }
    
    public Decimal subscriptionFee { get; set; }
    public Decimal discount { get; set; }
    public Decimal discountedFee { get; set; }
    public Account currentSchool { get; set; }
    
    //Constructor
    public SchoolRenewalModel (Account currentSchool, Opportunity currentOpp, Boolean isNewSubscription) {
        this.currentSchool = currentSchool;
        subscriptionExpiration = currentSchool.Paid_Through__c;
        subscriptionType = currentSchool.SSS_Subscription_Type_Current__c;

        if (String.isBlank(currentOpp.Subscription_Type__c))
        {
            newSubscriptionType = '';
        }
        else
        {
            newSubscriptionType = currentOpp.Subscription_Type__c;
        }

        if (subscriptionType == 'Basic')
        {
            subscriptionTypeOptions = new List<SelectOption> {
                new SelectOption('', '- Select -'),
                new SelectOption('Basic', 'Basic'),
                new SelectOption('Full', 'Full'),
                new SelectOption('Premium', 'Premium')
            };
        } else {
            subscriptionTypeOptions = new List<SelectOption> {
                new SelectOption('', '- Select -'),
                new SelectOption('Full', 'Full'),
                new SelectOption('Premium', 'Premium')
            };
        }
        
        Subscription_Settings__c ss = Subscription_Settings__c.getInstance('Subscription Settings');

        try {
                if (isNewSubscription){
                    newSubscriptionStartDate = Date.newInstance(currentSchool.Paid_Through__c.year(), ss.Start_Month__c.intValue(), ss.Start_Day__c.intValue());
                    newSubscriptionEndDate = Date.newInstance(currentSchool.Paid_Through__c.addYears(1).year(), ss.End_Month__c.intValue(), ss.End_Day__c.intValue());
                } else {
                    newSubscriptionStartDate = Date.newInstance(currentSchool.Paid_Through__c.addYears(-1).year(), ss.Start_Month__c.intValue(), ss.Start_Day__c.intValue());
                    newSubscriptionEndDate = Date.newInstance(currentSchool.Paid_Through__c.year(), ss.End_Month__c.intValue(), ss.End_Day__c.intValue());              
                }
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an error retrieving the subscription start and end dates.'));
            return;
        }
        
        calculateSubscriptionFee();
    }
    //End Constructor
    
    public void calculateSubscriptionFee() {
        subscriptionFee = 0;
        discount = 0;
        discountedFee = 0;
        String expYear = String.valueOf(newSubscriptionEndDate.year());
        if (newSubscriptionType != 'Basic') {
            discount = GlobalVariables.isEarlyBirdRenewal() ?
                        TransactionLineItemUtils.getSchoolSubscriptionDiscountTLIAmount(newSubscriptionType, 'Early Bird', expYear) : 0;
        }
        subscriptionFee = TransactionLineItemUtils.getSchoolSubscriptionSaleTLIAmount(newSubscriptionType, expYear);
        
        if(discount != null) {
            discountedFee = subscriptionFee - discount;
        }else {
            discountedFee = subscriptionFee;
        }
    }
}