/**
 * @description Query for Payment_Processor__mdt records.
 **/
public class PaymentProcessorSelector extends fflib_SObjectSelector {
    @testVisible private static final String PAYMENT_PROCESSOR_NAMES = 'paymentProcessorNames';

    /**
     * @description Select Payment_Processor__mdt configuration records by
     *              their developer names.
     * @param paymentProcessorNames The Developer names to select the Payment
     *        Processor records by.
     * @return A list of Payment_Processor__mdt records.
     * @throws An ArgumentNullException if the param is null.
     */
    public List<Payment_Processor__mdt> selectByName(Set<String> paymentProcessorNames) {
        ArgumentNullException.throwIfNull(paymentProcessorNames, PAYMENT_PROCESSOR_NAMES);

        assertIsAccessible();

        String query = 'SELECT {0} FROM {1} WHERE DeveloperName IN :paymentProcessorNames ORDER BY {2}';
        return Database.query(String.format(query, new List<String> {
                getFieldListString(),
                getSObjectName(),
                getOrderBy()
        }));
    }

    private SObjectType getSObjectType() {
        return Payment_Processor__mdt.getSObjectType();
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                Payment_Processor__mdt.Credit_Card_Statement_Descriptor__c,
                Payment_Processor__mdt.DeveloperName,
                Payment_Processor__mdt.MasterLabel,
                Payment_Processor__mdt.Payment_Provider__c,
                Payment_Processor__mdt.PFS_Application_Fee_Item_Description__c,
                Payment_Processor__mdt.PFS_Application_Fee_Tracker__c,
                Payment_Processor__mdt.Receipt_Email_Subject__c,
                Payment_Processor__mdt.Receipt_Org_Wide_Email_Address__c
        };
    }

    /**
     * @description The singleton instance of the Payment
     *              Processor Selector.
     */
    public static PaymentProcessorSelector Instance {
        get {
            if (Instance == null) {
                Instance = new PaymentProcessorSelector();
            }
            return Instance;
        }
        private set;
    }

    private PaymentProcessorSelector() {}
}