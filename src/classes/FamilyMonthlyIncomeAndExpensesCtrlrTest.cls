@isTest
private class FamilyMonthlyIncomeAndExpensesCtrlrTest
{

    private static PFS__c pfsRecord;
    private static ApplicationUtils appUtils;
    private static String currentScreen;
    
    static void generateBasicPFSData()
    {
        Id academicYearId = GlobalVariables.getCurrentAcademicYear().Id;
        Contact parentA = TestUtils.createContact('Parent A', null, RecordTypes.parentContactTypeId, true);
        Contact student1 = TestUtils.createContact('Student 1', null, RecordTypes.studentContactTypeId, true);
        
        User userRecord = [select Id, Name, ContactId, Contact.LastName, Contact.Name, Contact.MailingStreet, Contact.MailingCity, Contact.MailingState,  
                        Contact.MailingPostalCode, Contact.MailingCountry, Contact.Gender__c, Contact.Birthdate, 
                        Contact.Email, Contact.HomePhone, Contact.MobilePhone, Contact.Preferred_Phone__c,
                        Contact.FirstName, Contact.Middle_Name__c, Contact.Suffix__c, Contact.Salutation, Contact.Phone,
                        LanguageLocaleKey
                        from User 
                        where Id = :UserInfo.getUserId()];
                        
        pfsRecord = ApplicationUtils.generateBlankPFS(academicYearId, userRecord);
        
        Id pfsId = [select Id from PFS__c limit 1].Id;
        pfsRecord = ApplicationUtils.queryPFSRecord(pfsId);
        
        if (currentScreen != null){
            appUtils = new ApplicationUtils(UserInfo.getLanguage(), pfsRecord, currentScreen);
        } else {
            appUtils = new ApplicationUtils(UserInfo.getLanguage(), pfsRecord);
        }
    }

    @isTest
    private static void testTotals()
    {
        currentScreen = 'MonthlyIncomeExpenses';
        generateBasicPFSData();
        
        //For: getTotalMonthlyIncome
        pfsRecord.Alimony_Current__c = 12000;//Divided by 12 months=1000
        pfsRecord.Child_Support_Received_Current__c = 24000;//Divided by 12 months=2000
        pfsRecord.Monthly_Income_Parents_After_Taxes__c = 2500;
        pfsRecord.Monthly_Income_Applicants_After_Taxes__c = 2400;
        pfsRecord.Other_Monthly_Income__c = 3;
        
        //For: getTotalMonthlyIncome
        pfsRecord.Monthly_Expenses_Alimony__c = 100;
        pfsRecord.Monthly_Expenses_Child_Support__c = 300;
        pfsRecord.Other_Monthly_Support_Expenses__c = 100;
        
        //For: getTotalMonthlyHousingExpenses
        pfsRecord.Monthly_Expenses_Mortage_Rent_primary__c = 100;
        pfsRecord.Monthly_Expenses_Utilities__c = 200;
        pfsRecord.Monthly_Expenses_Property_Taxes_Expenses__c = 300;
        pfsRecord.Monthly_Expenses_Home_s_Owner_Insurance__c = 400;
        pfsRecord.Monthly_Expenses_Association_Fees__c = 500;
        pfsRecord.Monthly_Expenses_Maintenance__c = 600;
        pfsRecord.Monthly_Expenses_Mortage_Rent_all__c = 700;
        pfsRecord.Other_Monthly_Housing_Expenses__c = 30;
        
        //For: getTotalMonthlyTuitionExpenses
        pfsRecord.Monthly_Expenses_Tuition__c = 50;
        pfsRecord.Monthly_Expenses_Childcare__c = 40;
        pfsRecord.Other_Monthly_Tuition_Childcare_Expenses__c = 300;
        
        //For: getTotalMonthlyVehicleExpenses
        pfsRecord.Monthly_Expenses_Vehicle_Payments__c = 50;
        pfsRecord.Monthly_Expenses_Vehicle_Insurance__c = 40;
        pfsRecord.Monthly_Expenses_Vehicle_Maintenance__c = 300;
        pfsRecord.Other_Monthly_Expenses_Vehicle__c = 300;
        
        //For: getTotalMonthlyHealthcareExpenses
        pfsRecord.Monthly_Expenses_Healthcare_Insurance__c = 250;
        pfsRecord.Monthly_Expenses_Dental_Insurance__c = 30;
        pfsRecord.Monthly_Expenses_Life_Insurance__c = 20;
        pfsRecord.Monthly_Expenses_Insurance_not_covered__c = 15;
        pfsRecord.Other_Monthly_Healthcare_Expenses__c = 12;
        
        //For: getTotalMonthlyFoodExpenses
        pfsRecord.Monthly_Expenses_Groceries__c = 120;
        pfsRecord.Monthly_Expenses_Dining_out__c = 30;
        pfsRecord.Other_Monthly_Food_Expenses__c = 20;
        
        //For: getTotalMonthlyMiscellaneousExpenses
        pfsRecord.Monthly_Expenses_Clothing__c = 15;
        pfsRecord.Monthly_Expenses_Pet_Care__c = 15;
        pfsRecord.Monthly_Expenses_Bus_Metro_Rail__c = 10;
        pfsRecord.Monthly_Expenses_Savings__c = 10;
        pfsRecord.Monthly_Expenses_Entertainment__c = 55;
        pfsRecord.Other_Monthly_Miscellaneous_Expenses__c = 35;
        
        update pfsRecord;
        
        FamilyAppCompController compController = new FamilyAppCompController();
        compController.pfs = pfsRecord;
        compController.appUtils = appUtils;
        FamilyMonthlyIncomeAndExpensesController controller = new FamilyMonthlyIncomeAndExpensesController(compController);
        
        System.assertEquals('7903.00', controller.getTotalMonthlyIncome());
        System.assertEquals('500.00', controller.getTotalMonthlySupportExpenses());
        System.assertEquals('2830.00', controller.getTotalMonthlyHousingExpenses());
        System.assertEquals('390.00', controller.getTotalMonthlyTuitionExpenses());
        System.assertEquals('690.00', controller.getTotalMonthlyVehicleExpenses());
        System.assertEquals('327.00', controller.getTotalMonthlyHealthcareExpenses());
        System.assertEquals('170.00', controller.getTotalMonthlyFoodExpenses());
        System.assertEquals('140.00', controller.getTotalMonthlyMiscellaneousExpenses());
    }
}