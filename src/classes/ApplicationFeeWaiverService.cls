/**
 * Created by Nate on 5/25/17.
 */

public class ApplicationFeeWaiverService {
    @testVisible private static final String PENDING_QUALIFICATION = 'Pending Qualification';
    @testVisible private static final String ASSIGNED = 'Assigned';
    @testVisible private static final String WAIVER_REQUESTED_STATUS = 'Waiver Requested';
    @testVisible private static final String WAIVER_DENIED_STATUS = 'Declined';


    private ApplicationFeeWaiverService() {}

    /**
     * @description Apply waiver to pfs application.  If auto waiver is enabled follow
     *              auto waiver flow, else follow mbfw flow.
     * @param pfs The PFS that the Application Fee Waivers should be associated
     *        with.
     * @returns A boolean if waivers were applied.
     */
    public boolean applyAvailableWaivers(PFS__c pfs) {

        if (FeatureToggles.isEnabled('Auto_Fee_Waiver__c')) {
            return autoWaiverAssignmentFlow(pfs);
        } else {
            return mbfwPriorityFlow(pfs);
        }
    }

    /**
     * @description Flow for auto waivers enabled.
     * @param pfs The PFS that the Application Fee Waivers should be associated
     *        with.
     * @returns A boolean if waivers were applied.
     */
    private boolean autoWaiverAssignmentFlow(PFS__c pfs) {

        // Check if school has auto waiver enabled and has any waiver's available
        if (autoWaiverApplied(pfs)) {
            return true;
        } else {
            return mbfwPriorityFlow(pfs);
        }

        return false;
    }

    /**
     * @description Flow for MBFW if Auto Waiver isn't enabled
     * @param pfs The PFS that the Application Fee Waivers should be associated
     *        with.
     * @returns A boolean if waivers were applied.
     */
    private boolean mbfwPriorityFlow(PFS__c pfs) {

        System.debug('Waiver Applied MBFW FLOW');

        if (mbfwEligible(pfs)) {
            System.debug('Waiver Applied MBFW Eligible');
            // If Applicant is MBFW eligable, check if user has waiver's already applied else create MBFW
            List<Application_Fee_Waiver__c> existingWaivers = getExistingApplicationFeeWaivers(pfs, new Set<String> { PENDING_QUALIFICATION, ASSIGNED });
            if (!hasAlreadyAssignedWaiver(existingWaivers)) {
                insertMbfwWaiver(pfs);
            }
            return true;

        } else {
            // If not MBFW eligable check for Existing waivers
            if (usedExistingWaiver(pfs)) {
                return true;
            }
            return false;
        }

    }

    /**
     * @description Called During MBFW Flow, Checks for already assigned waivers and
     *              sets any already Pending waiver's to denied.
     * @param pfs The PFS that the Application Fee Waivers should be associated
     *        with.
     * @returns A boolean if waiver is assigned.
     */
    private boolean hasAlreadyAssignedWaiver(List<Application_Fee_Waiver__c> existingWaivers) {
        boolean hasExistingAssignedWaiver = false;

        for (Application_Fee_Waiver__c waiver : existingWaivers) {
            hasExistingAssignedWaiver = (hasExistingAssignedWaiver || waiver.Status__c == ASSIGNED);

            if (waiver.Status__c == PENDING_QUALIFICATION) {
                waiver.Status__c = WAIVER_DENIED_STATUS;
            }
        }

        if (!existingWaivers.isEmpty()) {
            update existingWaivers;
        }

        return hasExistingAssignedWaiver;
    }

    /**
     * @description Check for existing waiver's set to Pending, if found use the waiver.
     * @param pfs The PFS that the Application Fee Waivers should be associated
     *        with.
     * @returns A boolean if waiver was found pending and assigned.
     */
    private boolean usedExistingWaiver(PFS__c pfs) {
        boolean waiverApplied = false;

        // Use an existing application fee waiver if it exists
        List<Application_Fee_Waiver__c> existingWaivers = getExistingApplicationFeeWaivers(pfs, new Set<String> { PENDING_QUALIFICATION });
        if (!existingWaivers.isEmpty()) {
            waiverApplied = applyExistingWaiver(existingWaivers);
        }

        return waiverApplied;
    }

    /**
     * @description Creates a waiver for the pfs and school.
     * @param schoolId The school to assign the waiver for
     * @param pfs The PFS that the Application Fee Waivers should be associated
     *        with.
    */
    private void createFeeWaiver(Id schoolId, PFS__c pfs) {
        Id academicYear = GlobalVariables.getAcademicYearByName(pfs.Academic_Year_Picklist__c).Id;
        Application_Fee_Waiver__c waiver = new Application_Fee_Waiver__c(
                Academic_Year__c = academicYear,
                Contact__c = pfs.Parent_A__c,
                Status__c = ASSIGNED,
                Account__c = schoolId);

        insert waiver;
    }

    /**
     * @description If School has auto waiver's available, create waiver for school
     * @param pfs The PFS that the Application Fee Waivers should be associated
     *        with.
     * @returns A boolean if waiver was created
     */
    private boolean autoWaiverApplied(PFS__c pfs) {

        // Get School with Auto Waiver's available, sorted by highest balance
        List<School_PFS_Assignment__c> autoWaiverSchools = getSchoolWithAutoFeeWaiver(pfs);
        
        if (!autoWaiverSchools.isEmpty()) {
            declinePendingWaivers(pfs);
            createFeeWaiver(autoWaiverSchools[0].School__c, pfs);
            return true;
        }
        return false;
    }

    /**
     * @description declines all existing pending and assigned waivers for all waivers of the PFS
     * @param pfs The PFS that the Application Fee Waivers should be associated
     *        with.
     */
    private void declinePendingWaivers(PFS__c pfs) {
        List<Application_Fee_Waiver__c> existingWaivers = getExistingApplicationFeeWaivers(pfs, new Set<String> { PENDING_QUALIFICATION });

        for (Application_Fee_Waiver__c waiver : existingWaivers) {
                waiver.Status__c = WAIVER_DENIED_STATUS;
        }

        if (!existingWaivers.isEmpty()) {
            update existingWaivers;
        }
    }

    /**
     * @description Creates and inserts an MBFW waiver
     * @param pfs The PFS that the Application Fee Waivers should be associated
     *        with.
     */
    private void insertMbfwWaiver(PFS__c pfs) {
        Id academicYear = GlobalVariables.getAcademicYearByName(pfs.Academic_Year_Picklist__c).Id;
        Id accountId = ApplicationFeeSettingsService.Instance.getMeansBasedFeeWaiverAccountId();

        if (accountId != null) {
            Application_Fee_Waiver__c waiver = new Application_Fee_Waiver__c(
                    Academic_Year__c = academicYear,
                    Contact__c = pfs.Parent_A__c,
                    Status__c = ASSIGNED,
                    Account__c = accountId);
            insert waiver;
        }
    }

    /**
     * @description Creates an MBFW waiver is eligible.
     * @param pfs The PFS that the Application Fee Waivers should be associated
     *        with.
     * @returns A boolean if waiver was created
     */
    private boolean mbfwWaiverApplied(PFS__c pfs) {
        if (mbfwEligible(pfs) ){
            insertMbfwWaiver(pfs);
            return true;
        }
        return false;
    }

    /**
     * @description Checks if the PFS is MBFW eligble
     * @param pfs The PFS that the Application Fee Waivers should be associated
     *        with.
     * @returns A boolean if pfs is eligible
     */
    @testVisible 
    private boolean mbfwEligible(PFS__c pfs) {
        
        List<EfcWorksheetData> worksheets = EfcCalculatorAction.calculateSssEfc(new Set<Id> {pfs.Id}, false);
        // Assume only 1 worksheet given a single PFS
        EfcWorksheetData worksheet;
        if (!worksheets.isEmpty()) {
            worksheet = worksheets.get(0);
        }
        // worksheet values aren't changed when copying back to PFS and we don't want to assign this
        // data until AFTER the trigger has run following regular flow
        if (worksheet != null && worksheet.familySize != null && worksheet.totalIncome != null
                && worksheet.totalAssets != null && pfs.Parent_A_Country__c != null) {
            
            
            Decimal totalIncome = getTotalIncomeForMbfwEligible(pfs, worksheet);
            
            // check for MBFW based on anticipated values in final PFS worksheet calculation
            if (PFSAction.pfsQualifiesFeeWaiver(pfs.Academic_Year_Picklist__c, worksheet.familySize,
                    totalIncome, worksheet.totalAssets, pfs.Own_Business_or_Farm__c,
                    pfs.Parent_A_Country__c)) {
                return true;
            }
        }

        return false;
    }
    
    /**
     * @description When calculating total income for the MBW formula, we need to subtract 8j 
     *              current value from the total income if Question #3 on the PFS EZ questionnaire = yes.
     * @param pfs The PFS that the Application Fee Waivers should be associated with.
     * @param worksheet Data structure used to pass around EFC inputs and calculated values.
     * @returns A boolean if pfs is eligible
     */
    private Decimal getTotalIncomeForMbfwEligible(PFS__c pfs, EfcWorksheetData worksheet) {
        
        Decimal result = 0;
        if (pfs.Household_Has_Received_Benefits__c == 'Yes') {
        
            result = worksheet.totalIncome - EfcUtil.nullToZero(worksheet.welfareVeteransAndWorkersComp);
            
        } else {
            
            result = worksheet.totalIncome;
        }
        
        return result;
    }

    /**
     * @description If School has waiver set to pending, update to Assigned
     * @param pfs The PFS that the Application Fee Waivers should be associated
     *        with.
     * @returns A boolean if waiver was assigned
     */
    private boolean applyExistingWaiver(List<Application_Fee_Waiver__c> existingWaivers) {
        boolean waiverAssigned = false;

        if (!existingWaivers.isEmpty()) {
            for (Application_Fee_Waiver__c waiver : existingWaivers) {
                if (!waiverAssigned && waiver.Status__c == PENDING_QUALIFICATION) {
                    waiver.Status__c = ASSIGNED;
                    waiverAssigned = true;
                }
            }
            update existingWaivers;
        }
        return waiverAssigned;

    }

    /**
     * @description Query for Application Fee Waivers of a given status
     * @param pfs The PFS that the Application Fee Waivers should be associated
     *        with.
     * @param A set of statuses to query for
     * @returns List of Application Waivers of a set status for the pfs Parent
     */
    private List<Application_Fee_Waiver__c> getExistingApplicationFeeWaivers(PFS__c pfs, Set<String> statuses) {
        Id academicYear = GlobalVariables.getAcademicYearByName(pfs.Academic_Year_Picklist__c).id;

        return [SELECT
                Id,
                Account__c,
                Status__c,
                Academic_Year__c,
                Contact__c
        FROM Application_Fee_Waiver__c
        WHERE Contact__c = :pfs.Parent_A__c
        AND Academic_Year__c = :academicYear
        AND Status__c in :statuses
        ORDER BY Status__c ASC];
    }

    /**
     * @description Query for School and their waivers
     * @param pfs The PFS that the Application Fee Waivers should be associated
     *        with.
     * @returns List of PFS Assignments for the school
     */
    private static List<School_PFS_Assignment__c> getSchoolWithAutoFeeWaiver(PFS__c pfs) {

        return [SELECT School__r.Current_Year_Waiver_Balance__c,
                       School__r.Auto_Fee_Waiver__c,
                       PFS_ID__c
                  FROM School_PFS_Assignment__c
                 WHERE PFS_ID__c = :pfs.Id
                   AND School__r.Auto_Fee_Waiver__c = true
                   AND School__r.Current_Year_Waiver_Balance__c > 0
                   ORDER BY School__r.Current_Year_Waiver_Balance__c DESC];
    }

    public static ApplicationFeeWaiverService Instance {
        get {
            if (Instance == null) {
                Instance = new ApplicationFeeWaiverService();
            }
            return Instance;
        }
        private set;
    }
}