/**
 * @description This class is used to create Academic Year records for unit tests.
 */
@isTest
public class AcademicYearTestData extends SObjectTestData {
    @testVisible private static final String AY_NAME = Date.today().year() + '-' + Date.today().addYears(1).year();

    /**
     * @description Get the default values for the Academic_Year__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Academic_Year__c.Name => AY_NAME,
                Academic_Year__c.Start_Date__c => Date.today(),
                Academic_Year__c.End_Date__c => Date.today().addYears(1),
                Academic_Year__c.Family_Portal_Start_Date__c => Date.today(),
                Academic_Year__c.Family_Portal_End_Date__c => Date.today().addYears(1),
                Academic_Year__c.Include_Adjustable_IPA_Setting__c => 'Yes',
                Academic_Year__c.Non_PFS_Folder_Open__c => Date.today(),
                Academic_Year__c.Non_PFS_Folder_Close__c => Date.today().addYears(1)
        };
    }

    /**
     * @description Set the Name field on the current Academic Year record.
     * @param name The Name to set on the Academic Year.
     * @return The current working instance of AcademicYearTestData.
     */
    public AcademicYearTestData forName(String name) {
        return (AcademicYearTestData) with(Academic_Year__c.Name, name);
    }

    /**
     * @description Set the date thresholds to make this Academic Year valid
     *              for the previous year.
     * @return The current working instance of AcademicYearTestData.
     */
    public AcademicYearTestData asPreviousYear() {
        return (AcademicYearTestData) with(Academic_Year__c.Family_Portal_Start_Date__c, Date.today().addYears(-1))
                                     .with(Academic_Year__c.Family_Portal_End_Date__c, Date.today())
                                     .with(Academic_Year__c.Start_Date__c, Date.today().addYears(-1))
                                     .with(Academic_Year__c.End_Date__c, Date.today())
                                     .with(Academic_Year__c.Name, Date.today().addYears(-1).year() + '-' + Date.today().year());
    }

    /**
     * @description Set the date thresholds to make this Academic Year valid
     *              for the previous previous year.
     * @return The current working instance of AcademicYearTestData.
     */	
    public AcademicYearTestData asPreviousPreviousYear() {
        return (AcademicYearTestData) with(Academic_Year__c.Family_Portal_Start_Date__c, Date.today().addYears(-2))
                                     .with(Academic_Year__c.Family_Portal_End_Date__c, Date.today())
                                     .with(Academic_Year__c.Start_Date__c, Date.today().addYears(-2))
                                     .with(Academic_Year__c.End_Date__c, Date.today().addYears(-1))
                                     .with(Academic_Year__c.Name, Date.today().addYears(-2).year() + '-' + Date.today().year());
    }
	
    /**
     * @description Set the date thresholds to make this Academic Year valid
     *              for the Next year.
     * @return The current working instance of AcademicYearTestData.
     */
    public AcademicYearTestData asNextYear()
    {
        return (AcademicYearTestData) with(Academic_Year__c.Family_Portal_Start_Date__c, Date.today().addYears(1))
                                     .with(Academic_Year__c.Family_Portal_End_Date__c, Date.today().addYears(2))
                                     .with(Academic_Year__c.Start_Date__c, Date.today().addYears(1))
                                     .with(Academic_Year__c.End_Date__c, Date.today().addYears(2))
                                     .with(Academic_Year__c.Name, Date.today().addYears(1).year() + '-' + Date.today().addYears(2).year());
    }

    /**
     * @description Set the Start_Date__c field on the current Academic Year record.
     * @param startDate The Start Date to set on the Academic Year.
     * @return The current working instance of AcademicYearTestData.
     */
    public AcademicYearTestData forStartDate(Date startDate) {
        return (AcademicYearTestData) with(Academic_Year__c.Start_Date__c, startDate);
    }

    /**
     * @description Set the Family_Portal_Start_Date__c field on the current Academic Year record.
     * @param familyPortalStartDate The Family Portal Start Date to set on the Academic Year.
     * @return The current working instance of AcademicYearTestData.
     */
    public AcademicYearTestData forFamilyPortalStartDate(Date familyPortalStartDate) {
        return (AcademicYearTestData) with(Academic_Year__c.Family_Portal_Start_Date__c, familyPortalStartDate);
    }

    /**
     * @description Set the End_Date__c field on the current Academic Year record.
     * @param endDate The End Date to set on the Academic Year.
     * @return The current working instance of AcademicYearTestData.
     */
    public AcademicYearTestData forEndDate(Date endDate) {
        return (AcademicYearTestData) with(Academic_Year__c.End_Date__c, endDate);
    }

    /**
     * @description Set the Family_Portal_End_Date__c field on the current Academic Year record.
     * @param endDate The Family Portal End Date to set on the Academic Year.
     * @return The current working instance of AcademicYearTestData.
     */
    public AcademicYearTestData forFamilyPortalEndDate(Date familyPortalEndDate) {
        return (AcademicYearTestData) with(Academic_Year__c.Family_Portal_End_Date__c, familyPortalEndDate);
    }

    /**
     * @description Insert the current working Academic_Year__c record.
     * @return The currently operated upon Academic_Year__c record.
     */
    public Academic_Year__c insertAcademicYear() {
        return (Academic_Year__c)insertRecord();
    }

    /**
     * @description Create the current working Academic Year record without resetting
     *              the stored values in this instance of AcademicYearTestData.
     * @return A non-inserted Academic_Year__c record using the currently stored field
     *         values.
     */
    public Academic_Year__c createAcademicYearWithoutReset() {
        return (Academic_Year__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Academic_Year__c record.
     * @return The currently operated upon Academic_Year__c record.
     */
    public Academic_Year__c create() {
        return (Academic_Year__c)super.buildWithReset();
    }

    /**
     * @description The default Academic_Year__c record.
     */
    public Academic_Year__c DefaultAcademicYear {
        get {
            if (DefaultAcademicYear == null) {
                DefaultAcademicYear = createAcademicYearWithoutReset();
                insert DefaultAcademicYear;
            }
            return DefaultAcademicYear;
        }
        private set;
    }

    /**
     * @description Creates the specified number of academic years. None of the academic years will be overlapping. The
     *              first year will be in the future, the second year will be current, and all remaining years will be
     *              in the past.
     * @param numberToCreate The number of records to create.
     * @return A list of academic year records.
     */
    public List<Academic_Year__c> createAcademicYears(Integer numberToCreate) {
        Map<String, Academic_Year__c> existingAcademicYears = getExistingAcademicYearsByName();
        List<Academic_Year__c> years = new List<Academic_Year__c>();

        Integer currentYear = Date.Today().year();
        Integer startMonth = Date.today().addMonths(1).month();
        // Get the last day of last month and last month.
        Integer endDay = Date.today().addMonths(1).toStartOfMonth().addDays(-1).day();
        Integer endMonth = Date.today().addMonths(1).toStartOfMonth().addDays(-1).month();

        // For January, the term will end in the same year (e.g. 1/1/2016 - 12/31/2016) so make the year offsets the same.
        Integer startYearOffset = startMonth == 1 ? -1 : 0;
        Integer endYearOffset = -1;

        // The name offsets can always be the same.
        Integer startYearNameOffset = -1;
        Integer endYearNameOffset = -2;

        for (Integer i = 0; i < numberToCreate; i++) {
            Integer startYear = currentYear - startYearOffset;
            Integer endYear = currentYear - endYearOffset;

            String startYearName = String.valueOf(currentYear - startYearNameOffset);
            String endYearName = String.valueOf(currentYear - endYearNameOffset);

            String name = startYearName + '-' + endYearName;

            Date startDate = Date.newInstance(startYear, startMonth, 1);
            Date endDate = Date.newInstance(endYear, endMonth, endDay);

            if (existingAcademicYears.get(name) == null) {
                years.add(
                    AcademicYearTestData.Instance
                            .forName(name)
                            .forStartDate(startDate)
                            .forFamilyPortalStartDate(startDate)
                            .forEndDate(endDate)
                            .forFamilyPortalEndDate(endDate)
                            .createAcademicYearWithoutReset()
                );
            }

            startYearOffset++;
            endYearOffset++;
            startYearNameOffset++;
            endYearNameOffset++;
        }

        return years;
    }

    /**
     * @description Inserts the specified number of academic years.
     * @param numberToInsert The number of records to insert.
     * @return A list of academic years.
     */
    public List<Academic_Year__c> insertAcademicYears(Integer numberToInsert) {
        List<Academic_Year__c> recordsToInsert = createAcademicYears(numberToInsert);

        insert recordsToInsert;

        return recordsToInsert;
    }

    /**
     * @description Inserts 5 academic year records where 2 are current for family users and one is in the future. The rest of the academic years will be in the past.
     * @return Academic year records.
     */
    public List<Academic_Year__c> insertOverlappingYears() {
        Integer numberOfRecords = 5;

        List<Academic_Year__c> testAcademicYears = createAcademicYears(numberOfRecords);

        // make first academic year in the future for family users.
        testAcademicYears[0].Family_Portal_Start_Date__c = Date.today().addDays(3);
        testAcademicYears[0].Family_Portal_End_Date__c = Date.today().addDays(20);

        // make the second academic year current based on todays date for family users.
        testAcademicYears[1].Family_Portal_Start_Date__c = Date.today().addDays(-5);
        testAcademicYears[1].Family_Portal_End_Date__c = Date.today().addDays(5);

        // make the third academic year overlap with the current academic year for family users.
        testAcademicYears[2].Family_Portal_Start_Date__c = Date.today().addDays(-15);
        testAcademicYears[2].Family_Portal_End_Date__c = Date.today().addDays(1);

        insert testAcademicYears;

        return testAcademicYears;
    }

    private Map<String, Academic_Year__c> getExistingAcademicYearsByName() {
        Map<String, Academic_Year__c> academicYearsByName = new Map<String, Academic_Year__c>();

        List<Academic_Year__c> academicYears = AcademicYearsSelector.Instance.selectAll();
        for (Academic_Year__c academicYear : academicYears) {
            academicYearsByName.put(academicYear.Name, academicYear);
        }

        return academicYearsByName;
    }


    /**
     * @description Get the Academic_Year__c SObjectType.
     * @return The Academic_Year__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Academic_Year__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static AcademicYearTestData Instance {
        get {
            if (Instance == null) {
                Instance = new AcademicYearTestData();
            }
            return Instance;
        }
        private set;
    }

    private AcademicYearTestData() { }
}