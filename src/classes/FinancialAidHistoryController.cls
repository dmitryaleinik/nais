/**
 * @description Controller for a component that will show historical data for families that apply for financial aid year after year.
 */
public with sharing class FinancialAidHistoryController {

    private FinancialAidHistoryService.History finAidHistory;
    public FinancialAidHistoryService.History getFinAidHistory() {
        if (finAidHistory != null) {
            return finAidHistory;
        }

        finAidHistory = FinancialAidHistoryService.Instance.getSampleHistory();

        return finAidHistory;
    }

    public List<FinancialAidHistoryService.FamilyContributionSummary> getEfcSummaries() {
        return getFinAidHistory().EfcData;
    }

    public List<FinancialAidHistoryService.VerificationSummary> getVerificationSummaries() {
        return getFinAidHistory().VerificationData;
    }

    private List<SelectOption> terms;
    public List<SelectOption> getTerms() {
        if (terms != null) {
            return terms;
        }

        terms = new List<SelectOption>();

        for (FinancialAidHistoryService.FinancialAidSummary summary : getFinAidHistory().FinancialAidData) {
            terms.add(new SelectOption(summary.Term, summary.Term));
        }

        return terms;
    }

    public String Term {
        get {
            if (Term == null) {
                Integer lastSummary = getFinAidHistory().FinancialAidData.size() - 1;

                Term = getFinAidHistory().FinancialAidData[lastSummary].Term;
            }
            return Term;
        }
        set;
    }

    public FinancialAidHistoryService.FinancialAidSummary getSelectedSummary() {
        for (FinancialAidHistoryService.FinancialAidSummary summary : getFinAidHistory().FinancialAidData) {
            if (summary.Term == Term) {
                return summary;
            }
        }

        Integer lastSummary = getFinAidHistory().FinancialAidData.size() - 1;

        return getFinAidHistory().FinancialAidData[lastSummary];
    }
}