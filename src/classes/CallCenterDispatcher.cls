/**
* Testclass =  TestCallCenterCookieWorkflow
*/
public with sharing class CallCenterDispatcher
{

    private static final String PARENT_CALLER_URL = '/apex/CallCenterParentCaller';

    public PageReference getRedir()
    {
        PageReference newPage;
        String dnis = ApexPages.currentPage().getParameters().get('dnis');
        String ani = ApexPages.currentPage().getParameters().get('ani');
        String username = ApexPages.currentPage().getParameters().get('username');
        String callID = ApexPages.currentPage().getParameters().get('callID');

        DNIS__c dnisSetting = (dnis != null && dnis.length() > 0) ? DNIS__c.getInstance(dnis) : null;

        if (dnisSetting == null || dnisSetting.isParentDNIS__c){
            newPage = createParentCallerPage();
        } else if (isSchoolDnis(dnisSetting)) {
            system.debug('dnis ' + dnisSetting.name + 'dnis ' + dnisSetting.isSchoolDNIS__c );
            newPage = new PageReference('/apex/CallCenterSchoolCaller');
        }
        else { //for now parent caller is default
            newPage = createParentCallerPage();
        }

        // Set cookies with CTI parameters since they are always necessary.
        Cookie cDnis = new Cookie('dnis',dnis,null,-1,false);
        Cookie cAni =new Cookie('ani',ani,null,-1,false);
        Cookie cUsername = new Cookie('userName',userName,null,-1,false);
        Cookie cCallID = new Cookie('callID',callID,null,-1,false);

        newPage.setCookies(new Cookie[]{cDnis,cAni,cUsername,cCallID});

        // Always set url parameters for school callers.
        // If we are handling a parent caller, only set url parameters for users that are not using console.
        // This just keeps the url looking nicer.
        if (isSchoolDnis(dnisSetting)) {
            newPage.getParameters().put('ani',ani );
            newPage.getParameters().put('dnis', dnis);
            newPage.getParameters().put('username', username);
            newPage.getParameters().put('callID',callID);
        }

        newPage.setRedirect(true);

        return newPage;
    }

    private Boolean isSchoolDnis(DNIS__c dnisRecord)
    {
        return dnisRecord != null && dnisRecord.isSchoolDNIS__c == true;
    }

    private PageReference createParentCallerPage()
    {
        String url = PARENT_CALLER_URL;

        PageReference parentCallerPage =
                new PageReference('/console?tsid=' + CallCenterContactRedirectController.CallCenterConsoleAppId);

        parentCallerPage.setAnchor(EncodingUtil.urlEncode(url, 'UTF-8'));

        return parentCallerPage;
    }
}