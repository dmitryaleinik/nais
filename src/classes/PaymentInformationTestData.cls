/**
 * @description Test data class for PaymentInformation sub classes.
 */
@isTest
public class PaymentInformationTestData {
    /**
     * @description Generate a generic CreditCardInformation instance.
     */
    public CreditCardInformation asCreditCardInformation() {
        CreditCardInformation ccInfo = new CreditCardInformation();

        ccInfo.CardType = 'Visa';
        ccInfo.NameOnCard = 'Tom Tucker';
        ccInfo.CardNumber = '4242424242424242';
        ccInfo.Cvv2 = '100';
        ccInfo.ExpirationMM = String.valueof(System.today().addMonths(1).month());
        ccInfo.ExpirationYY = String.valueof(System.today().addYears(1).year()).right(2);

        return (CreditCardInformation)populateStandardInformation((PaymentInformation)ccInfo);
    }

    /**
     * @description Generate a generic ECheckInformation instance.
     */
    public ECheckInformation asECheckInformation() {
        ECheckInformation ecInfo = new ECheckInformation();

        ecInfo.BankName = 'BankOfAmerica';
        ecInfo.AccountType = 'Savings';
        ecInfo.AccountNumber = '123456789';
        ecInfo.RoutingNumber = '111222333';
        ecInfo.CheckType = 'Business';
        ecInfo.CheckNumber = '1234';

        return (ECheckInformation)populateStandardInformation((PaymentInformation)ecInfo);
    }

    private PaymentInformation populateStandardInformation(PaymentInformation paymentInformation) {
        paymentInformation.ItemName = 'Mustache Comb';
        paymentInformation.PaymentAmount = 47.00;
        paymentInformation.BillingFirstName = 'Tom';
        paymentInformation.BillingLastName = 'Tucker';
        paymentInformation.BillingStreet1 = 'Quahog 5 News';
        paymentInformation.BillingStreet2 = 'Suite 6';
        paymentInformation.BillingCity = 'Quahog';
        paymentInformation.BillingState = 'RI';
        paymentInformation.BillingPostalCode = '01234';
        paymentInformation.BillingCountryCode = 'US';
        paymentInformation.BillingEmail = 'tucker@quahog5news.com';

        return paymentInformation;
    }

    /**
     * @description Singleton instance of the test data class.
     */
    public static PaymentInformationTestData Instance {
        get {
            if (Instance == null) {
                Instance = new PaymentInformationTestData();
            }
            return Instance;
        }
        private set;
    }

    private PaymentInformationTestData() {}
}