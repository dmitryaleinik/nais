public with sharing class SchoolRequestMIEBulkController {
    
    public MonthlyIncomeAndExpensesService.MIEResponseBulk response {get; set;}
    public Integer foldersCounter {get; set;}
    public Boolean processHasStarted {get; set;}
    private Map<Id, Student_Folder__c> folders {get; set;}
    
    public SchoolRequestMIEBulkController(ApexPages.StandardSetController controller)
    {
        folders = new Map<Id, Student_Folder__c>((Student_Folder__c[])controller.getSelected());
        foldersCounter = folders.size();
        processHasStarted = false;
        
    }//End-Constructor
    
    public PageReference startProcess() {
        
        if (isValidRequest()) {
            
            processHasStarted = true;
            
            MonthlyIncomeAndExpensesService.MIERequestBulk request = 
                new MonthlyIncomeAndExpensesService.MIERequestBulk(folders.keySet());

            // SFP-1720: This constructor needs to specify true for the skipClosedAY param since we shouldn't be sending
            // requesting MIE from families if they can't login to the portal to complete it.
            response = new MonthlyIncomeAndExpensesService.MIEResponseBulk(request, true, true);
            
            if (!String.isBlank(response.errorMessage)) {
                
                processHasStarted = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, response.errorMessage));
            }
        }
        
        return null;
    }//End:startProcess
    
    private Boolean isValidRequest() {
        
        Decimal MAX_LIMIT_FOLDERS = SchoolPortalSettings.Require_MIE_Bulk_Limit;
        
        if (!SchoolPortalSettings.MIE_Pilot) {
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.MIE_Pilot_Flag_Disabled_Message));
            return false;
        } else if (folders != null && folders.size() > MAX_LIMIT_FOLDERS) {
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select no more than ' + MAX_LIMIT_FOLDERS +' applicants.'));
            return false;
        }
        
        return true;
    }//End:isValidRequest
    
    public PageReference redirectAdvancedList() {
        
        PageReference nextPage = Page.StudentAdvancedList;
        nextPage.setRedirect(true);
        return nextPage;
    }//End:redirectAdvancedList
    
    public Boolean isPopup {
        get {
            
            if (isPopup == null) {
                
                isPopup = ApexPages.currentPage().getParameters().get('popup') == 'yes';
            }
            
            return isPopup;
        }
        private set;
    }
}