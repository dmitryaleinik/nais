global class AccountCustomButton {
    
    Webservice static Integer recalculateVerificationStatuses(Id accountId){
        
        Set<Id> spaIds = new Set<Id>();
        Set<Id> householdIds = new Set<Id>();
        List<School_PFS_Assignment__c> spasToUpdate = new List<School_PFS_Assignment__c>();
        String currentAcademicYearStr = AcademicYearService.Instance.getCurrentAcademicYear().Name;
        
        // 1. query the SPAs for that school (the account) and the current academic year, which have been submitted and paid in full.
        for(School_PFS_Assignment__c spa : [
            SELECT Id 
            FROM School_PFS_Assignment__c 
            WHERE School__c =: accountId 
            AND Academic_Year_Picklist__c =: currentAcademicYearStr 
            AND PFS_Status__c = 'Application Submitted' 
            AND Payment_Status__c = 'Paid in Full' 
            AND Verification_Processing_Status__c = 'Processing Complete']) {

            spaIds.add(spa.Id);
            
            spa.Verification_Processing_Status__c = 'Processing Required';
            spasToUpdate.add(spa);
        }
        
        //2. Once we have those SPAs, we can get the households Ids and pass them into the VerificationAction.cls.
        for(School_Document_Assignment__c sda : [
            SELECT Id, Household_ID__c 
            FROM School_Document_Assignment__c   
            WHERE School_PFS_Assignment__c IN: spaIds 
            AND Household_ID__c != null]) {
            
            householdIds.add(sda.Household_ID__c);
        }
        
        if( !householdIds.isEmpty() ) {
            
            VerificationAction.setVerficationStatusValues(null, householdIds);
        }
        
        // 3. Then we can take each SPA, set the Verification Processing Status field to Processing Required, and update the records.
        if( !spasToUpdate.isEmpty() ) {
            
            BatchGenericUpsert batchUpsert = new BatchGenericUpsert();
            batchUpsert.recordsToUpsert = spasToUpdate;
            Database.executeBatch(batchUpsert);
            
            return spasToUpdate.size();
        }
        
        return 0;
    }//End:recalculateVerificationStatuses
}