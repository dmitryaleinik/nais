/*
 * NAIS-1092: Payment receipt
 *
 * SL, Exponent Partners, 2013
 */
public class FamilyPaymentReceiptController {

    @testvisible private static final String FAMILY_PAYMENT_RECEIPT_LOGO_LINK_DOCINFO_NAME = 'FamilyPaymentReceipt_Logo_Link';

    /* Inner class to hold Transaction Line Item values used for the Payment Receipt*/
    public class ReceiptValues {

        // properties
        private Transaction_Line_Item__c theTLI = null;
        public String academicYear {get; set;}
        public String pfsNumber {get; set;}
        public Date paymentDate {get; set;}
        public Decimal amount {get; set;}
        public boolean isAutoPayment {get; set;}
        public String transactionType {get; set;}
        public boolean isCreditDebit {get; set;}
        public String cardType {get; set;}
        public String cardNumber {get; set;}
        public String referenceId {get; set;}
        public String contactFirstName {get; set;}
        public String contactFullName {get; set;}
        public String description {get; set;}

        // instantiate the ReceiptValues class with a transaction line item
        public ReceiptValues(Id transactionLineItemId) {

            for (Transaction_Line_Item__c tli : [SELECT Id, Opportunity__r.PFS__r.PFS_Number__c, Opportunity__r.PFS__r.Academic_Year_Picklist__c,
                                                        Opportunity__r.PFS__r.Parent_A_First_Name__c, Opportunity__r.PFS__r.Parent_A_Last_Name__c,
                                                        Transaction_Date__c, Amount__c, Transaction_Type__c, Transaction_Id__c, Tender_Type__c,
                                                        CC_Last_4_Digits__c, RecordTypeId
                                                    FROM Transaction_Line_Item__c
                                                    WHERE Id=:transactionLineItemId
                                                    AND RecordTypeId IN (:RecordTypes.paymentAutoTransactionTypeId, :RecordTypes.paymentTransactionTypeId)
                                                    LIMIT 1]) {
                theTLI = tli;
            }

            if (theTLI != null) {
                academicYear = theTLI.Opportunity__r.PFS__r.Academic_Year_Picklist__c;
                pfsNumber = theTLI.Opportunity__r.PFS__r.PFS_Number__c;
                paymentDate = theTLI.Transaction_Date__c;
                amount = theTLI.Amount__c;
                isAutoPayment = (theTLI.RecordTypeId == RecordTypes.paymentAutoTransactionTypeId);
                transactionType = theTLI.Transaction_Type__c;
                isCreditDebit = (theTLI.Transaction_Type__c == 'Credit/Debit Card');
                cardType = theTLI.Tender_Type__c;
                cardNumber = theTLI.CC_Last_4_Digits__c;
                referenceId = theTLI.Transaction_Id__c;
                contactFirstName = theTLI.Opportunity__r.PFS__r.Parent_A_First_Name__c;
                contactFullName = theTLI.Opportunity__r.PFS__r.Parent_A_First_Name__c + ' ' + theTLI.Opportunity__r.PFS__r.Parent_A_Last_Name__c;
                description = (String.isBlank(PaymentProcessor.CreditCardStatementDescriptor)) ? 'SSS NAIS' :
                        PaymentProcessor.CreditCardStatementDescriptor;
            }
        }

        // utility method to set page parameters from the receipt values
        public void setPageParameters(PageReference receiptPage, boolean renderAsHtml) {
            receiptPage.getParameters().put('academicYear', academicYear);
            receiptPage.getParameters().put('pfsNumber', pfsNumber);
            receiptPage.getParameters().put('paymentDate', paymentDate.format());
            receiptPage.getParameters().put('amount', amount.toPlainString());
            receiptPage.getParameters().put('isAutoPayment', String.valueOf(isAutoPayment));
            receiptPage.getParameters().put('transactionType', transactionType);
            receiptPage.getParameters().put('isCreditDebit', String.valueOf(isCreditDebit));
            receiptPage.getParameters().put('cardType', cardType);
            receiptPage.getParameters().put('cardNumber', cardNumber);
            receiptPage.getParameters().put('referenceId', referenceId);
            receiptPage.getParameters().put('renderAsHtml', String.valueOf(renderAsHtml));
            receiptPage.getParameters().put('contactFirstName', String.valueOf(contactFirstName));
            receiptPage.getParameters().put('contactFullName', String.valueOf(contactFullName));
            receiptPage.getParameters().put('description', String.valueOf(description));
        }
    }


    /*** THE MAIN CONTROLLER CLASS ***/

    /* Properties */
    public String transactionLineItemIdParam {get; set;}
    public ReceiptValues receiptValues {get; set;}
    public String familyPaymentReceiptLogoLinkUrl {
        get {

            if (familyPaymentReceiptLogoLinkUrl == null) {
                familyPaymentReceiptLogoLinkUrl =
                    DocumentLinkMappingService.getDocumentRecordId(FAMILY_PAYMENT_RECEIPT_LOGO_LINK_DOCINFO_NAME);
            }
            return familyPaymentReceiptLogoLinkUrl;

            /*if (familyPaymentReceiptLogoLinkUrl == null) {
                familyPaymentReceiptLogoLinkUrl =
                    DocumentLinkMappingService.buildInternalLinkUrl(FAMILY_PAYMENT_RECEIPT_LOGO_LINK_DOCINFO_NAME);

                if (String.isNotBlank(familyPaymentReceiptLogoLinkUrl)) {
                    String orgId = UserInfo.getOrganizationId();
                    familyPaymentReceiptLogoLinkUrl += '&oid=' + orgId;
                }
            }
            return familyPaymentReceiptLogoLinkUrl;*/
        }
        private set;
    }

    /* Initialization */
    public FamilyPaymentReceiptController() {
        transactionLineItemIdParam = ApexPages.currentPage().getParameters().get('id');
        if (String.isNotBlank(transactionLineItemIdParam)) {
            receiptValues = new ReceiptValues(transactionLineItemIdParam);
        }
        else {
            receiptValues = null;
        }
    }

    /* STATIC HELPER METHODS */

    // retrieves the contents of the HTML receipt page as a string
    public static String getHtmlContent(Id transactionLineItemId) {
        PageReference receiptPage = Page.FamilyPaymentReceipt;
        ReceiptValues receiptValues = new ReceiptValues(transactionLineItemId);

        // Note: we're passing the receipt values as individual page params, since the TLI record is not
        // exposed to getContent() when running as a family portal user
        receiptValues.setPageParameters(receiptPage, true);

        if (!Test.isRunningTest()) {
            return receiptPage.getContent().toString();
        }
        else {
            // getContent() isn't allowed in a unit test
            return 'Unit test placeholder content';
        }
    }

    // retrieves the contents of the Plain Text receipt page as a string
    public static String getPlainTextContent(Id transactionLineItemId) {
        PageReference receiptPage = Page.FamilyPaymentReceiptPlainText;
        ReceiptValues receiptValues = new ReceiptValues(transactionLineItemId);

        // Note: we're passing the receipt values as individual page params, since the TLI record is not
        // exposed to getContent() when running as a family portal user
        receiptValues.setPageParameters(receiptPage, true);

        if (!Test.isRunningTest()) {
            return receiptPage.getContent().toString();
        }
        else {
            // getContent() isn't allowed in a unit test
            return 'Unit test placeholder content';
        }
    }
}