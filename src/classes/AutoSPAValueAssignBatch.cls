/**
 *    SFP-649
 *    [10.6.16] If Copy_Verification_Values_to_Revision__c is toggled false to true
 *                 this batch job processes existing revision fields for Verification fields that are 
 *                'Complete - Matched' per SFP-640 [jB]
 *    [10.18.16] Merge changes from SFP-787 to run "Sum on SPA" as part of batch job 
 */
global virtual class AutoSPAValueAssignBatch implements Database.Batchable<sObject> {

    protected Set<String> mostRecentAcademicYearNames;
    
    // only copy fields that are 'Completed - Matched' on verification to their revision field
    private static final String COMPLETED_MATCHED = 'Complete - Matched';
    private static final String PROCESSING_COMPLETE = 'Processing Complete';
    private static final String ANNUAL_SETTING_IDS_PARAM = 'annualSettingIds';
    private static final String BIZ_FARM_SUBQUERY = '(SELECT Business_Farm__r.Business_or_Farm__c, Orig_Net_Profit_Loss_Business_Farm__c, Net_Profit_Loss_Business_Farm__c from School_Biz_Farm_Assignments__r)';
                                    
    // [10.26.16] defines all ORIG_fields for mapping to test if they have been modified
    //          by the school [jB]
    @TestVisible
    private static final Map<String, String> origParentEntryFieldMap = new Map<String, String>{
            'Salary_Wages_Parent_A__c' => 'Orig_Salary_Wages_Parent_A__c', // 1
            'Salary_Wages_Parent_B__c' => 'Orig_Salary_Wages_Parent_B__c', // 2
            'Interest_Income__c' => 'Orig_Interest_Income__c', // 3A
            'Dividend_Income__c' => 'Orig_Dividend_Income__c', // 3a
            'Alimony_Received__c' => 'Orig_Alimony_Received__c', // 4
            'Taxable_Refunds__c' => 'Orig_Taxable_Refunds__c',  //7a
            'Capital_Gain_Loss__c' => 'Orig_Capital_Gain_Loss__c', // 7b
            'Other_Gain_Loss__c' => 'Orig_Other_Gain_Loss__c', // 7c
            'IRA_Distribution__c' => 'Orig_IRA_Distribution__c', // 7d
            'Pensions_and_Annuities__c' => 'Orig_Pensions_and_Annuities__c', // 7e
            'Rental_Real_Estate_Trusts_etc__c' => 'Orig_Rental_Real_Estate_Trusts_etc__c', // 7f
            'Unemployment_Compensation__c' => 'Orig_Unemployment_Compensation__c', // 7g
            'Social_Security_Benefits_Taxable__c' => 'Orig_Social_Security_Benefits_Taxable__c', // 7h
            'Other_Income__c' => 'Orig_Other_Income__c', // 7i
            'Deductible_Part_of_Self_Employment_Tax__c' => 'Orig_Deductible_Part_Self_Employment_Tax__c', // 8a
            'Keogh_Plan_Payment__c' => 'Orig_Keogh_Plan_Payment__c', // 8b, 13b
            'Untaxed_IRA_Plan_Payment__c' => 'Orig_Untaxed_IRA_Plan_Payment__c', // 8c, 13a
            'Other_Adjustments_to_Income__c' => 'Orig_Other_Adjustments_to_Income__c', // 8d
            'Social_Security_Benefits__c' => 'Orig_Social_Security_Benefits__c', // 11
            'Pre_Tax_Payments_to_Retirement_Plans__c' => 'Orig_Pre_Tax_Payment_to_Retirement_Plans__c', // 12a
            'Earned_Income_Credits__c' => 'Orig_Earned_Income_Credits__c', // 12f
            'Tax_Exempt_Investment_Income__c' => 'Orig_Tax_Exempt_Investment_Income__c', // 12h
            'Federal_Income_Tax_Calculation_Revision__c' => 'Orig_Federal_Income_Tax_Reported__c', // 22b
            'Social_Security_Tax_Allowance_Calc_Rev__c' => 'Orig_Social_Security_Tax_Allow_Calc__c', // 23b
            'Medicare_Tax_Allowance_Calc_Revision__c' => 'Orig_Medicare_Tax_Allowance_Calculated__c', // 24b
            'Total_SE_Tax_Paid__c' => 'Orig_Total_SE_Tax_Paid__c', // 25b
            'Medical_Dental_Expense__c' => 'Orig_Medical_Dental_Expense__c' // 30
    };

    // [10.26.16] Force override fields are always set reguardless of revision value,
    // as long as they meet status criteria
    private static final Set<String> forceOverrideTaxFields = new Set<String>{
            'Depreciation_Sec_179_Exp_Sch_C__c', // 15a
            'Depreciation_Sec_179_Exp_Sch_F__c', // 15b
            'Home_Bus_Exp_Sch_C__c', // 15g
            'Depreciation_Schedule_E__c' // 15e
    };

    // [10.14.16] defines all fields that should be auto copied to revision based on verification status
    // or if 1040s are completed. - FCW line numbers at the time of writing are listed in comments [jB]
    private static Map<String, String> revisionFieldMapCY = new Map<String, String>{
            'Salary_Wages_Parent_A__c' => 'Sum_W2_Wages_Parent_A__c', // 1
            'Salary_Wages_Parent_B__c' => 'Sum_W2_Wages_Parent_B__c', // 2
            'Interest_Income__c' => 'X1040_Taxable_Interest__c', // 3A
            'Dividend_Income__c' => 'X1040_Dividends__c', // 3a
            'Alimony_Received__c' => 'X1040_Alimony__c', // 4
            'Taxable_Refunds__c' => 'Taxable_Refunds__c',  //7a
            'Capital_Gain_Loss__c' => 'Capital_Gain_Loss__c', // 7b
            'Other_Gain_Loss__c' => 'Other_Gain_Loss__c', // 7c
            'IRA_Distribution__c' => 'IRA_Distribution__c', // 7d
            'Pensions_and_Annuities__c' => 'Pensions_and_Annuities__c', // 7e
            'Rental_Real_Estate_Trusts_etc__c' => 'Rental_Real_Estate_Trusts_etc__c', // 7f
            'Unemployment_Compensation__c' => 'Unemployment_Compensation__c', // 7g
            'Social_Security_Benefits_Taxable__c' => 'Social_Security_Benefits_Taxable__c', // 7h
            'Other_Income__c' => 'Other_income__c', // 7i
            'Deductible_Part_of_Self_Employment_Tax__c' => 'Self_Employment_Tax_Deductible_Part__c', // 8a
            'Keogh_Plan_Payment__c' => 'SEP_SIMPLE_and_Qualified_Plans__c', // 8b, 13b
            'Untaxed_IRA_Plan_Payment__c' => 'IRA_Deduction__c', // 8c, 13a
            'Other_Adjustments_to_Income__c' => 'Adjustments_to_Income__c', // 8d
            'Social_Security_Benefits__c' => 'X1040_Social_Security_Benefits__c', // 11
            'Pre_Tax_Payments_to_Retirement_Plans__c' => 'Sum_Pre_Tax_Payments_To_Retirement_Plans__c', // 12a
            'Earned_Income_Credits__c' => 'Earned_Income_Credits__c', // 12f
            'Tax_Exempt_Investment_Income__c' => 'X1040_Tax_exempt_Interest__c', // 12h
            'Depreciation_Sec_179_Exp_Sch_C__c' => 'Sum_Sch_C_Depreciation_Sect_179__c', // 15a
            'Depreciation_Sec_179_Exp_Sch_F__c' => 'Sum_Sch_F_Depreciation_Sec_179_Exp__c', // 15b
            'Home_Bus_Exp_Sch_C__c' => 'Sum_Sch_C_Exp_Bus_Uses_Home__c', // 15g
            'Depreciation_Schedule_E__c' => 'Sum_Sch_E_Depreciation_Total__c', // 15e
            'Federal_Income_Tax_Calculation_Revision__c' => 'X1040_Total_Tax__c', // 22b
            'Social_Security_Tax_Allowance_Calc_Rev__c' => 'Sum_W2_SS_Tax__c', // 23b
            'Medicare_Tax_Allowance_Calc_Revision__c' => 'Sum_W2_Medicare__c', // 24b
            'Total_SE_Tax_Paid__c' => 'Self_Employment_Tax__c', // 25b
            'Medical_Dental_Expense__c' => 'Sched_A_Med_Dental_Exp__c' // 30
    };

    private static Map<String, String> revisionFieldMapPY = new Map<String, String>{
            'Salary_Wages_Parent_A__c' => 'PY_Sum_W2_Wages_Parent_A__c', // 1
            'Salary_Wages_Parent_B__c' => 'PY_Sum_W2_Wages_Parent_B__c', // 2
            'Interest_Income__c' => 'PY_X1040_Taxable_Interest__c', // 3A
            'Dividend_Income__c' => 'PY_X1040_Dividends__c', // 3a
            'Alimony_Received__c' => 'PY_X1040_Alimony__c', // 4
            'Taxable_Refunds__c' => 'PY_Taxable_Refunds__c',  //7a
            'Capital_Gain_Loss__c' => 'PY_Capital_Gain_Loss__c', // 7b
            'Other_Gain_Loss__c' => 'PY_Other_Gain_Loss__c', // 7c
            'IRA_Distribution__c' => 'PY_IRA_Distribution__c', // 7d
            'Pensions_and_Annuities__c' => 'PY_Pensions_and_Annuities__c', // 7e
            'Rental_Real_Estate_Trusts_etc__c' => 'PY_Rental_Real_Estate_Trusts_etc__c', // 7f
            'Unemployment_Compensation__c' => 'PY_Unemployment_Compensation__c', // 7g
            'Social_Security_Benefits_Taxable__c' => 'PY_Social_Security_Benefits_Taxable__c', // 7h
            'Other_Income__c' => 'PY_Other_income__c', // 7i
            'Deductible_Part_of_Self_Employment_Tax__c' => 'PY_Self_Employment_Tax_Deductible_Part__c', // 8a
            'Keogh_Plan_Payment__c' => 'PY_SEP_SIMPLE_and_Qualified_Plans__c', // 8b, 13b
            'Untaxed_IRA_Plan_Payment__c' => 'PY_IRA_Deduction__c', // 8c, 13a
            'Other_Adjustments_to_Income__c' => 'PY_Adjustments_to_Income__c', // 8d
            'Social_Security_Benefits__c' => 'PY_X1040_Social_Security_Benefits__c', // 11
            'Pre_Tax_Payments_to_Retirement_Plans__c' => 'PY_Sum_Pre_Tax_Payments_To_Retirement_Plans__c', // 12a
            'Earned_Income_Credits__c' => 'PY_Earned_Income_Credits__c', // 12f
            'Tax_Exempt_Investment_Income__c' => 'PY_X1040_Tax_exempt_Interest__c', // 12h
            'Depreciation_Sec_179_Exp_Sch_C__c' => 'PY_Sum_Sch_C_Depreciation_Sect_179__c', // 15a
            'Depreciation_Sec_179_Exp_Sch_F__c' => 'PY_Sum_Sch_F_Depreciation_Sec_179_Exp__c', // 15b
            'Home_Bus_Exp_Sch_C__c' => 'PY_Sum_Sch_C_Exp_Bus_Uses_Home__c', // 15g
            'Depreciation_Schedule_E__c' => 'PY_Sum_Sch_E_Depreciation_Total__c', // 15e
            'Federal_Income_Tax_Calculation_Revision__c' => 'PY_X1040_Total_Tax__c', // 22b
            'Social_Security_Tax_Allowance_Calc_Rev__c' => 'PY_Sum_W2_SS_Tax__c', // 23b
            'Medicare_Tax_Allowance_Calc_Revision__c' => 'PY_Sum_W2_Medicare__c', // 24b
            'Total_SE_Tax_Paid__c' => 'PY_Self_Employment_Tax__c', // 25b
            'Medical_Dental_Expense__c' => 'PY_Sched_A_Med_Dental_Exp__c' // 30
    };

    // Defines the fields that are copied to SPA based on their Verification Status for CY
    private static final Map<String, String> verificationFieldStatusFieldsCY = new Map<String, String>{
            'Salary_Wages_Parent_A__c' => 'X1040_Wages_Status__c', // 1
            'Salary_Wages_Parent_B__c' => 'X1040_Wages_Status__c', // 2
            'Rental_Real_Estate_Trusts_etc__c' => 'Rental_Real_Estate_Trusts_Etc_Status__c', // 7a
            'Pre_Tax_Payments_to_Retirement_Plans__c' => 'X1040_Wages_Status__c', // 12a
            'Depreciation_Sec_179_Exp_Sch_C__c' => 'X1040_Business_Income_Loss_Status__c', // 15a
            'Depreciation_Sec_179_Exp_Sch_F__c' => 'Net_Profit_Loss_Farm_Status__c', // 15b
            'Home_Bus_Exp_Sch_C__c' => 'X1040_Business_Income_Loss_Status__c', // 15g
            'Depreciation_Schedule_E__c' => 'Rental_Real_Estate_Trusts_Etc_Status__c', // 15e
            'Social_Security_Tax_Allowance_Calc_Rev__c' => 'X1040_Wages_Status__c', // 23b
            'Medicare_Tax_Allowance_Calc_Revision__c' => 'X1040_Wages_Status__c' // 24b
    };

    // Defines the fields that are copied to SPA based on their Verification Status for PY
    private static final Map<String, String> verificationFieldStatusFieldsPY = new Map<String, String>{
            'Salary_Wages_Parent_A__c' => 'PY_X1040_Wages_Status__c', // 1
            'Salary_Wages_Parent_B__c' => 'PY_X1040_Wages_Status__c', // 2
            'Rental_Real_Estate_Trusts_etc__c' => 'PY_Rental_Real_Estate_Trusts_Etc_Status__c', // 7a
            'Pre_Tax_Payments_to_Retirement_Plans__c' => 'PY_X1040_Wages_Status__c', // 12a
            'Depreciation_Sec_179_Exp_Sch_C__c' => 'PY_X1040_Business_Income_Loss_Status__c', // 15a
            'Depreciation_Sec_179_Exp_Sch_F__c' => 'PY_Net_Profit_Loss_Farm_Status__c', // 15b
            'Home_Bus_Exp_Sch_C__c' => 'PY_X1040_Business_Income_Loss_Status__c', // 15g
            'Depreciation_Schedule_E__c' => 'PY_Rental_Real_Estate_Trusts_Etc_Status__c', // 15e
            'Social_Security_Tax_Allowance_Calc_Rev__c' => 'PY_X1040_Wages_Status__c', // 23b
            'Medicare_Tax_Allowance_Calc_Revision__c' => 'PY_X1040_Wages_Status__c' // 24b
    };	
	
    // fields to be pulled from verification for CY
    private static final Map<String, String> verificationFieldsCY = new Map<String, String>{
            'X1040_Taxable_Interest__c' => 'X1040_Taxable_Interest_B__c', //3a
            'X1040_Dividends__c' => 'X1040_Dividends_B__c', // 3b
            'X1040_Alimony__c' => 'X1040_Alimony_B__c', // 4
            'X1040_Business_Income_Loss__c' => 'X1040_Business_Income_Loss_B__c', // 5 - SFP-909 (if one business)
            'Net_Profit_Loss_Farm__c' => 'Net_Profit_Loss_Farm_B__c', // 6 - SFP-909 (if one farm)
            'Taxable_Refunds__c' => 'Taxable_Refunds_B__c', // 7a
            'Capital_Gain_Loss__c' => 'Capital_Gain_Loss_B__c', //7b
            'Other_Gain_Loss__c' => 'Other_Gain_Loss_B__c', // 7c
            'IRA_Distribution__c' => 'IRA_Distribution_B__c', // 7d
            'Pensions_and_Annuities__c' => 'Pensions_and_Annuities_B__c', // 7e
            'Unemployment_Compensation__c' => 'Unemployment_Compensation_B__c', // 7g
            'Social_Security_Benefits_Taxable__c' => 'Social_Security_Benefits_Taxable_B__c', // 7h
            'Other_income__c' => 'Other_income_B__c', // 7i
            'Self_Employment_Tax_Deductible_Part__c' => 'Self_Employment_Tax_Deductible_Part_B__c',// 8a
            'SEP_SIMPLE_and_Qualified_Plans__c' => 'SEP_SIMPLE_and_Qualified_Plans_B__c', // 8b
            'IRA_Deduction__c' => 'IRA_Deduction_B__c', // 8c
            'Adjustments_to_Income__c' => 'Adjustments_to_Income_B__c', // 8d
            'X1040_Social_Security_Benefits__c' => 'X1040_Social_Security_Benefits_B__c', // 11
            'Earned_Income_Credits__c' => 'Earned_Income_Credits_B__c', // 12
            'X1040_Tax_exempt_Interest__c' => 'X1040_Tax_exempt_Interest_B__c', // 12h
            'X1040_Total_Tax__c' => 'X1040_Total_Tax_B__c', // 22b
            'Self_Employment_Tax__c' => 'Self_Employment_Tax_B__c', // 25b
            'Sched_A_Med_Dental_Exp__c' => 'Sched_A_Med_Dental_Exp_B__c' // 30
    };

    // fields to be pulled from verification for PY
    private static final Map<String, String> verificationFieldsPY = new Map<String, String>{
            'X1040_Taxable_Interest__c' => 'PY_X1040_Taxable_Interest_B__c', //3a
            'X1040_Dividends__c' => 'PY_X1040_Dividends_B__c', // 3b
            'X1040_Alimony__c' => 'PY_X1040_Alimony_B__c', // 4
            'X1040_Business_Income_Loss__c' => 'PY_X1040_Business_Income_Loss_B__c', // 5 - SFP-909 (if one business)
            'Net_Profit_Loss_Farm__c' => 'PY_Net_Profit_Loss_Farm_B__c', // 6 - SFP-909 (if one farm)
            'Taxable_Refunds__c' => 'PY_Taxable_Refunds_B__c', // 7a
            'Capital_Gain_Loss__c' => 'PY_Capital_Gain_Loss_B__c', //7b
            'Other_Gain_Loss__c' => 'PY_Other_Gain_Loss_B__c', // 7c
            'IRA_Distribution__c' => 'PY_IRA_Distribution_B__c', // 7d
            'Pensions_and_Annuities__c' => 'PY_Pensions_and_Annuities_B__c', // 7e
            'Unemployment_Compensation__c' => 'PY_Unemployment_Compensation_B__c', // 7g
            'Social_Security_Benefits_Taxable__c' => 'PY_Social_Security_Benefits_Taxable_B__c', // 7h
            'Other_income__c' => 'PY_Other_income_B__c', // 7i
            'Self_Employment_Tax_Deductible_Part__c' => 'PY_Self_Employment_Tax_Deductible_Part_B__c',// 8a
            'SEP_SIMPLE_and_Qualified_Plans__c' => 'PY_SEP_SIMPLE_and_Qualified_Plans_B__c', // 8b
            'IRA_Deduction__c' => 'PY_IRA_Deduction_B__c', // 8c
            'Adjustments_to_Income__c' => 'PY_Adjustments_to_Income_B__c', // 8d
            'X1040_Social_Security_Benefits__c' => 'PY_X1040_Social_Security_Benefits_B__c', // 11
            'Earned_Income_Credits__c' => 'PY_Earned_Income_Credits_B__c', // 12
            'X1040_Tax_exempt_Interest__c' => 'PY_X1040_Tax_exempt_Interest_B__c', // 12h
            'X1040_Total_Tax__c' => 'PY_X1040_Total_Tax_B__c', // 22b
            'Self_Employment_Tax__c' => 'PY_Self_Employment_Tax_B__c', // 25b
            'Sched_A_Med_Dental_Exp__c' => 'PY_Sched_A_Med_Dental_Exp_B__c' // 30
    };	
	
    // most fields with document verification status' are pulled directly from the sum of the document
    // values, however, some have status' and are pulled directly from verification field
    private static final Map<String, String> statusFieldsPulledFromVerificationCY = new Map<String, String>{
            'Rental_Real_Estate_Trusts_etc__c' => 'Rental_Real_Estate_Trusts_etc_B__c'
    };

    // most fields with document verification status' are pulled directly from the sum of the document
    // values, however, some have status' and are pulled directly from verification field
    private static final Map<String, String> statusFieldsPulledFromVerificationPY = new Map<String, String>{
            'Rental_Real_Estate_Trusts_etc__c' => 'PY_Rental_Real_Estate_Trusts_etc_B__c'
    };

    // SFP-909 status fields that drive copy for BizFarm
    private static final Map<String, String> businessFarmVerifications = new Map<String, String>{
            'X1040_Business_Income_Loss_Status__c' => 'Business',
            'Net_Profit_Loss_Farm_Status__c' => 'Farm'
    };

    // SFP-909 map the business farm values to their verification fields
    private static final Map<String, String> businessFarmVerificationValues = new Map<String, String>{
            'Business' => 'X1040_Business_Income_Loss__c',
            'Farm' => 'Net_Profit_Loss_Farm__c'
    };

    String query;

    protected Set<Id> annualSettingIds;
    protected boolean isPremium;
    /**
     *    Use similar queries as the SchoolFCW Controller to pull in fields
     *    SFP-811 Modified to take annualSettingsIds OR spaIds (from family document processor)
     *            So the batch can either be triggered when auto-copy revisions is set to 'Yes'
     *            or when new family documents are set to processed.
     */
    global AutoSPAValueAssignBatch(Set<Id> annualSettingIds, boolean isPremium) {
        ArgumentNullException.throwIfNull(annualSettingIds, ANNUAL_SETTING_IDS_PARAM);

        String[] VerificationFieldNames = ExpCore.GetAllFieldNames('Verification__c');
        String[] schoolPFSAssignmentFieldNames = ExpCore.GetAllFieldNames('School_PFS_Assignment__c');
        this.annualSettingIds = annualSettingIds;
        this.isPremium = isPremium;
        assignMapsForCurrentPriorYears(isPremium);	
		
        for (Integer i = 0; i < VerificationFieldNames.size(); i++) {
            VerificationFieldNames[i] = 'Applicant__r.PFS__r.Verification__r.' + VerificationFieldNames[i];
        }
        List<String> fieldNames = new List<String>();
        fieldNames.addAll(schoolPFSAssignmentFieldNames);
        fieldNames.addAll(VerificationFieldNames);
        String whereClause = '';

        // annualSettingsIds
        whereClause += ' WHERE School__c IN (SELECT School__c FROM Annual_Setting__c WHERE Id IN :annualSettingIds ';
        whereClause += ' AND Copy_Verification_Values_to_Revisions__c = \'Yes\')';

        // filter out deleted SPAs
        whereClause += ' And IsDeleted = false';

        // Select only those school's SPAs that are flagged to be copied
        query = 'SELECT ' + String.Join(fieldNames, ',') + ',' + BIZ_FARM_SUBQUERY + ' FROM School_PFS_Assignment__c ' + whereClause;
        System.debug('query = '+query);
    }
    
    /**
     * @description Constructor used to run this batch for PFS Assignments that have been flagged for processing.
     */
    global AutoSPAValueAssignBatch() {
        
        mostRecentAcademicYearNames = new Set<String>();
        assignMapsForCurrentPriorYears(false);
        List<Academic_Year__c> availableYears = AcademicYearService.Instance.getAvailableAcademicYears();
        for (Academic_Year__c academicYear : availableYears) {
            
            mostRecentAcademicYearNames.add(academicYear.Name);
            
            if(mostRecentAcademicYearNames.size()==2) break;
        }
        
        // Select only those school's SPAs that are flagged to be copied.
        query = buildAllPfsAssignmentsToProcessQuery();
    }

	
    /**
     *
     */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    /**
     *    Modify batch job to first run setSpaFieldsFromFamilyDocument so that sum values will be saved to SPA
     */
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<School_PFS_Assignment__c> spas = (List<School_PFS_Assignment__c>)scope;
        autoAssignVerificationValues(spas);
    }
    
    private static List<School_PFS_Assignment__c> queryPfsAssignmentsWithRelatedObjects(Set<Id> recordsToQuery) {
        
        String[] VerificationFieldNames = ExpCore.GetAllFieldNames('Verification__c');
        String[] schoolPFSAssignmentFieldNames = ExpCore.GetAllFieldNames('School_PFS_Assignment__c');

        for (Integer i = 0; i < VerificationFieldNames.size(); i++) {
            VerificationFieldNames[i] = 'Applicant__r.PFS__r.Verification__r.' + VerificationFieldNames[i];
        }
        List<String> fieldNames = new List<String>();
        fieldNames.addAll(schoolPFSAssignmentFieldNames);
        fieldNames.addAll(VerificationFieldNames);

        String whereClause = 'WHERE Id IN: recordsToQuery';
        
        
        List<School_PFS_Assignment__c> result = Database.query('SELECT ' + String.Join(fieldNames, ',') + ',' + BIZ_FARM_SUBQUERY + ' FROM School_PFS_Assignment__c ' + whereClause);
        return result;
    }

    private static Map<String, String> revisionFieldMap = new Map<String, String>();
	private static Map<String, String> verificationFields = new Map<String, String>();
	private static Map<String, String> verificationFieldStatusFields = new Map<String, String>();
	private static Map<String, String> statusFieldsPulledFromVerification = new Map<String, String>();
    
    /**
     * @description : Method to assign the Map values based on the School Subscription type. 
	 *                If Subscription is Premium assign PY maps, else assign CY maps.          
     */
    private static void assignMapsForCurrentPriorYears(boolean isPremium) {
		boolean checkPremium = isPremium;
		if (checkPremium) {
			revisionFieldMap = new Map<String, String>(revisionFieldMapPY);
			verificationFields = new Map<String, String>(verificationFieldsPY);
			verificationFieldStatusFields = new Map<String, String>(verificationFieldStatusFieldsPY);
			statusFieldsPulledFromVerification = new Map<String, String>(statusFieldsPulledFromVerificationPY);
			
		} else {
			revisionFieldMap = new Map<String, String>(revisionFieldMapCY);
			verificationFields = new Map<String, String>(verificationFieldsCY);
			verificationFieldStatusFields = new Map<String, String>(verificationFieldStatusFieldsCY);
            statusFieldsPulledFromVerification = new Map<String, String>(statusFieldsPulledFromVerificationCY);
			
		}
	}
    
    /**
     * @description This method builds a query for School PFS Assignments dynamically that will include related
     *              verification and business farm info. The query will end with WHERE Verification_Processing_Status__c
     *              = 'Processing Required' AND IsDeleted = false. This allows additional WHERE criteria to be added.
     * @return A query for School PFS Assignment records.
     */
    private static String buildAllPfsAssignmentsToProcessQuery() {
        
        return 'SELECT Id FROM School_PFS_Assignment__c WHERE Academic_Year_Picklist__c IN :mostRecentAcademicYearNames AND IsDeleted = false AND Verification_Processing_Status__c = \'Processing Required\' ';
    }

    /**
     * @description Takes in a list of school pfs assignments and transfers document verification values if necessary.
     * @param spas The list of records to process.
     */
    private static void autoAssignVerificationValues(List<School_PFS_Assignment__c> spas) {
        Map<Id, School_PFS_Assignment__c> spasMap = new Map<Id, School_PFS_Assignment__c>(spas);
        spas = queryPfsAssignmentsWithRelatedObjects(spasMap.KeySet());
        
        Map<Id, School_PFS_Assignment__c> spaAssignmentsToUpdateById = new Map<Id, School_PFS_Assignment__c>();
        List<School_Biz_Farm_Assignment__c> businessFarmsToUpdate = new List<School_Biz_Farm_Assignment__c>();
        Map<String, Set<School_PFS_Assignment__c>> spasBySchoolId = new Map<String, Set<School_PFS_Assignment__c>>();
        Set<String> spaAcademicYearNames = new Set<String>();

        // filter out spas that should have their values auto-copied
        for (School_PFS_Assignment__c spa : spas) {
            if (spasBySchoolId.get(spa.School__c) == null) {
                spasBySchoolId.put(spa.School__c, new Set<School_PFS_Assignment__c>());
            }
            if (!spasBySchoolId.get(spa.School__c).contains(spa)) {
                spasBySchoolId.get(spa.School__c).add(spa);
            }
            spaAcademicYearNames.add(spa.Academic_Year_Name__c);
        }

        // set all the field sums before processing auto-copy
        FamilyDocumentAction.setSpaFieldsFromFamilyDocument(spas);

        Map<Id, School_PFS_Assignment__c> spasToAutoCopyById = FamilyDocumentAction.findSPAIdsThatShouldAutoCopy(spasBySchoolId, spaAcademicYearNames);
        Set<Id> autoCopySPAIds = spasToAutoCopyById.keySet();

        // Use a map of all the SPAs to get the Ids of the records that should NOT have verification auto copied.
        // For these records, we need to at least update them to indicate processing was complete
        Map<Id, School_PFS_Assignment__c> allSpasById = new Map<Id, School_PFS_Assignment__c>(spas);
        Set<Id> remainingSpaIds = allSpasById.keySet();
        remainingSpaIds.removeAll(autoCopySPAIds);
        for (Id spaId : remainingSpaIds) {
            School_PFS_Assignment__c newRecord =
                    new School_PFS_Assignment__c(Id = spaId, Verification_Processing_Status__c = PROCESSING_COMPLETE);
            spaAssignmentsToUpdateById.put(spaId, newRecord);
        }

        // Consolidate with constructor query
        String[] VerificationFieldNames = ExpCore.GetAllFieldNames('Verification__c');
        String[] schoolPFSAssignmentFieldNames = ExpCore.GetAllFieldNames('School_PFS_Assignment__c');
        for (Integer i = 0; i < VerificationFieldNames.size(); i++) {
            VerificationFieldNames[i] = 'Applicant__r.PFS__r.Verification__r.' + VerificationFieldNames[i];
        }
        
        List<String> fieldNames = new List<String>();
        fieldNames.addAll(schoolPFSAssignmentFieldNames);
        fieldNames.addAll(VerificationFieldNames);
        // SFP-650, if we come across a Verification without an Expected Number of 1040s update it here which requires 2 PFS Fields
        fieldNames.add('Applicant__r.PFS__r.Filing_Status__c');
        fieldNames.add('Applicant__r.PFS__r.Parent_B_Filing_Status__c');

        // Select only those school's SPAs that are flagged to be copied
        String spaQuery = 'SELECT ' + String.Join(fieldNames, ',') + ',' + BIZ_FARM_SUBQUERY + ' FROM School_PFS_Assignment__c WHERE Id IN :autoCopySPAIds';

        // consolidate ^
        List<Verification__c> verificationsToUpdate = new List<Verification__c>();
        for (School_PFS_Assignment__c spa : Database.query(spaQuery)) {
            Applicant__c applicant = (Applicant__c) spa.getSobject('Applicant__r');
            PFS__c pfs = (PFS__c) applicant.getSobject('PFS__r');
            Verification__c verification = (Verification__c) pfs.getSobject('Verification__r');
            List<School_Biz_Farm_Assignment__c> sbfas = (List<School_Biz_Farm_Assignment__c>) spa.getSObjects('School_Biz_Farm_Assignments__r');

            // issues with verifications in some test cases
            if (verification != null) {
                // SFP-650 (set it locally and update it)
                if(verification.Expected_Number_of_1040s__c == null){
                    verification.Expected_Number_of_1040s__c = EfcConstants.isFilingMarriedSeparated(pfs.Filing_Status__c, pfs.Parent_B_Filing_Status__c) ? 2 : 1;
                    verificationsToUpdate.add(verification);
                }

                for (String revisionField : revisionFieldMap.keyset()) {
                    String verificationSourceField = revisionFieldMap.get(revisionField);
                    // SFP-811 check copy revision fields from verification if expecting 2 1040s and both have been recieved, and
                    //            annual setting copy verification values to revision is set to 'Yes'
                    if (verificationFields.keyset().contains(verificationSourceField)
                            && !verificationFieldStatusFields.keyset().contains(revisionField)) {
                        Boolean copySingleParentVerificationValues = (verification.Expected_Number_of_1040s__c == 1
                                && verification.Parent_A_1040_Verified__c);
                        Boolean copyBothVerificationValues = (verification.Expected_Number_of_1040s__c == 2 &&
                                verification.Parent_A_1040_Verified__c &&
                                verification.Parent_B_1040_Verified__c);
                        
                        // only copy if revision field is same as orig field, none of the force override fields have
                        // values that come directly from verification
                        if (revisionUnchanged(spa, revisionField)) {
                            // copy just verification values from Parent A
                            if (copySingleParentVerificationValues) {
                                spa.put(revisionField, (Decimal) verification.get(verificationSourceField));
                            }
                            if (copyBothVerificationValues) {
                                spa.put(revisionField, sumVerificationFieldsForRevision(revisionField, verification, true));
                            }
                        }
                    } else {
                        if (isCompletedMatched(revisionField, verification) &&
                                (revisionUnchanged(spa, revisionField) || forceOverrideTaxFields.contains(revisionField))) {
                            // copy the value from it's fields on verification
                            if (statusFieldsPulledFromVerification.keyset().contains(revisionField)) {
                                Decimal verificationSum = sumVerificationFieldsForRevision(revisionField, verification, false);
                                spa.put(revisionField, (verificationSum != 0.0 ? verificationSum : null));
                            } else {
                                // copy the value from it's sum field on SPA
                                spa.put(revisionField, (Decimal) spa.get(revisionFieldMap.get(revisionField)));
                            }
                        }
                    }
                }
                
                // Mark this SPA as processing completed so it doesn't get picked up by other jobs.
                // Even if verification values were not copied, all SPAs will still be updated.
                spa.Verification_Processing_Status__c = PROCESSING_COMPLETE;
                spaAssignmentsToUpdateById.put(spa.Id, spa);

                // SFP-909 - [10.27.16] If there is only one BizFarm assignment than the Verification Value from X1040 Verification should be
                //          copied to Bizfarm record [jB]
                if (sbfas != null) {
                    // SFP-952 - Count businesses and farms only copy values if there is 1 either
                    Integer countFarm = 0;
                    Integer countBusiness = 0;
                    List<School_Biz_Farm_Assignment__c> farms = new List<School_Biz_Farm_Assignment__c>();
                    List<School_Biz_Farm_Assignment__c> businesses = new List<School_Biz_Farm_Assignment__c>();

                    for (School_Biz_Farm_Assignment__c sbfa : sbfas) {
                        if (sbfa.Business_Farm__r.Business_or_Farm__c == 'Business') {
                            businesses.add(sbfa);
                        }
                        if (sbfa.Business_Farm__r.Business_or_Farm__c == 'Farm') {
                            farms.add(sbfa);
                        }
                    }

                    // farms
                    if (farms.size() == 1 && verification.get('Net_Profit_Loss_Farm_Status__c') == COMPLETED_MATCHED &&
                            farms[0].Net_Profit_Loss_Business_Farm__c == farms[0].Orig_Net_Profit_Loss_Business_Farm__c) {
                        farms[0].Net_Profit_Loss_Business_Farm__c = sumVerificationFieldsForBusinessFields('Net_Profit_Loss_Farm__c', verification);
                        businessFarmsToUpdate.add(farms[0]);
                    }

                    // business
                    if (businesses.size() == 1 && verification.get('X1040_Business_Income_Loss_Status__c') == COMPLETED_MATCHED &&
                            businesses[0].Net_Profit_Loss_Business_Farm__c == businesses[0].Orig_Net_Profit_Loss_Business_Farm__c) {
                        businesses[0].Net_Profit_Loss_Business_Farm__c = sumVerificationFieldsForBusinessFields('X1040_Business_Income_Loss__c', verification);
                        businessFarmsToUpdate.add(businesses[0]);
                    }
                }
            } // end verification != null
        }

        if (!spaAssignmentsToUpdateById.isEmpty()) {
            update spaAssignmentsToUpdateById.values();
        }

        if (!businessFarmsToUpdate.isEmpty()) {
            update businessFarmsToUpdate;
        }

        // SFP-650
        if(!verificationsToUpdate.isEmpty()){
            update verificationsToUpdate;
        }
    }

    /**
     * @description Takes in a set of school pfs assignments Ids and transfers document verification values if necessary.
     * @param pfsAssignmentIds The Ids of the records to process.
     */
    public static void autoAssignVerificationValues(Set<Id> pfsAssignmentIds) {
        
        List<School_PFS_Assignment__c> spas = new List<School_PFS_Assignment__c>([SELECT Id FROM School_PFS_Assignment__c WHERE Id IN :pfsAssignmentIds AND Verification_Processing_Status__c = 'Processing Required' AND IsDeleted = false LIMIT 50000]);

        autoAssignVerificationValues(spas);
    }

    /**
     *    SFP-909 sumVerificationFields for biz farm assignments
     */
    private static Decimal sumVerificationFieldsForBusinessFields(String verificationField, Verification__c verification) {
        String verificationFieldB = verificationFields.get(verificationField);
        Decimal verificationA = verification.get(verificationField) != null ? (Decimal) verification.get(verificationField) : 0;
        Decimal verificationB = verification.get(verificationFieldB) != null ? (Decimal) verification.get(verificationFieldB) : 0;
        return verificationA + verificationB;
    }

    /**
     *    SFP-811 Modified to handle cases with when 2 1040s
     */
    private static Decimal sumVerificationFieldsForRevision(String revisionField, Verification__c verification, Boolean statusFieldOnVerification) {
        String verificationField = revisionFieldMap.get(revisionField);
        String verificationBField = statusFieldOnVerification ? verificationFields.get(verificationField) : statusFieldsPulledFromVerification.get(revisionField);
        Decimal verificationA = verification.get(verificationField) != null ? (Decimal) verification.get(verificationField) : 0;
        Decimal verificationB = verification.get(verificationBField) != null ? (Decimal) verification.get(verificationBField) : 0;
        return verificationA + verificationB;
    }

    /**
     *    [10.26.16] Revision fields are automatically copied from ORIG field, if they do not match than
     *               modification has been made by the school, therefore DO NOT COPY over field value
     */
    private static Boolean revisionUnchanged(School_PFS_Assignment__c spa, String revisionField) {
        if (spa.get(revisionField) == null) {
            return true;
        } else {
            Decimal revisionFieldVal = (Decimal) spa.get(revisionField);
            Decimal origFieldVal = getOrgFieldValueFromSpa(spa, revisionField);
            // if revisionField == origField we can overwrite the field value with the verification value
            if (revisionFieldVal == origFieldVal) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     *    [10.16.16] Orig fields are calculated from ORIG_ field or from another calculation
     *                field on SPA [jB]
     */
    private static Decimal getOrgFieldValueFromSpa(School_PFS_Assignment__c spa, String revisionField) {
        Decimal retVal = 0.0;
        // field to pull is orig value
        if (origParentEntryFieldMap.keyset().contains(revisionField)) {
            String origField = origParentEntryFieldMap.get(revisionField);
            retVal = spa.get(origField) != null ? (Decimal) spa.get(origField) : 0.0;
        }

        return retVal;
    }

    /**
     *
     */
    private static Boolean isCompletedMatched(String revisionField, Verification__c verification) {
        if (verification == null) {
            return false;
        }
        String verificationFieldStatusField = verificationFieldStatusFields.get(revisionField);
        String verificationValue = verification.get(verificationFieldStatusField) != null ? (String) verification.get(verificationFieldStatusField) : '';
        
        return verificationValue == COMPLETED_MATCHED;
    }

    /**
     *
     */
    global void finish(Database.BatchableContext BC) {

    }
}