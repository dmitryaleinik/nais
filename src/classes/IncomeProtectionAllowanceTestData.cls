/**
 * @description This class is used to create Income Protection Allowance records for unit tests.
 */
@isTest
public class IncomeProtectionAllowanceTestData extends SObjectTestData {
    @testVisible private static final Decimal HOUSING_ALLOWANCE = 20;
    @testVisible private static final Decimal OTHER_ALLOWANCE = 12;
    @testVisible private static final Decimal ALLOWANCE = 21;
    @testVisible private static final Decimal FAMILY_SIZE = 8;

    /**
     * @description Get the default values for the Income_Protection_Allowance__c object.
     * @return A map of Schema.SObjectFields to their default values.
     */
    protected override Map<Schema.SObjectField,Object> getDefaultValueMap() {
        return new Map<Schema.SObjectField,Object> {
                Income_Protection_Allowance__c.Academic_Year__c => AcademicYearTestData.Instance.DefaultAcademicYear.Id,
                Income_Protection_Allowance__c.Housing_Allowance__c => HOUSING_ALLOWANCE,
                Income_Protection_Allowance__c.Other_Allowance__c => OTHER_ALLOWANCE,
                Income_Protection_Allowance__c.Allowance__c => ALLOWANCE,
                Income_Protection_Allowance__c.Family_Size__c => FAMILY_SIZE
        };
    }

    /**
     * @description Set the Academic Year on the current Income Protection Allowance
     *             record.
     * @param academicYearId The Id of the Academic Year to set on the current Income
     *             Protection Allowance record.
     * @return The current working instance of IncomeProtectionAllowanceTestData.
     */
    public IncomeProtectionAllowanceTestData forAcademicYearId(Id academicYearId) {
        return (IncomeProtectionAllowanceTestData) with(Income_Protection_Allowance__c.Academic_Year__c, academicYearId);
    }

    /**
     * @description Insert the current working Income_Protection_Allowance__c record.
     * @return The currently operated upon Income_Protection_Allowance__c record.
     */
    public Income_Protection_Allowance__c insertIncomeProtectionAllowance() {
        return (Income_Protection_Allowance__c)insertRecord();
    }

    /**
     * @description Create the current working Income Protection Allowance record without resetting
     *             the stored values in this instance of IncomeProtectionAllowanceTestData.
     * @return A non-inserted Income_Protection_Allowance__c record using the currently stored field
     *             values.
     */
    public Income_Protection_Allowance__c createIncomeProtectionAllowanceWithoutReset() {
        return (Income_Protection_Allowance__c)buildWithoutReset();
    }

    /**
     * @description Create the current working Income_Protection_Allowance__c record.
     * @return The currently operated upon Income_Protection_Allowance__c record.
     */
    public Income_Protection_Allowance__c create() {
        return (Income_Protection_Allowance__c)super.buildWithReset();
    }

    /**
     * @description The default Income_Protection_Allowance__c record.
     */
    public Income_Protection_Allowance__c DefaultIncomeProtectionAllowance {
        get {
            if (DefaultIncomeProtectionAllowance == null) {
                DefaultIncomeProtectionAllowance = createIncomeProtectionAllowanceWithoutReset();
                insert DefaultIncomeProtectionAllowance;
            }
            return DefaultIncomeProtectionAllowance;
        }
        private set;
    }

    /**
     * @description Get the Income_Protection_Allowance__c SObjectType.
     * @return The Income_Protection_Allowance__c SObjectType.
     */
    protected override Schema.SObjectType getSObjectType() {
        return Income_Protection_Allowance__c.SObjectType;
    }

    /**
     * @description Static singleton instance property.
     */
    public static IncomeProtectionAllowanceTestData Instance {
        get {
            if (Instance == null) {
                Instance = new IncomeProtectionAllowanceTestData();
            }
            return Instance;
        }
        private set;
    }

    private IncomeProtectionAllowanceTestData() { }
}