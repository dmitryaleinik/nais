@isTest
private class GlobalVariablesTest
{

    private static Integer uniquefier = 1;
    private static User u;
    private static User thisUser = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];

    @isTest
    private static void testOverlapPeriodAndEditableYear_asSchoolPortalUser(){
        GlobalVariablesTest.CreateData();

        System.assert(!GlobalVariables.isInOverlapPeriod());

        SchoolPortalSettings.Overlap_Start_Date = Date.today().addDays(-5);
        SchoolPortalSettings.Overlap_End_Date = Date.today().addDays(5);

        // Use test user created with test data.
        User testUser = u;

        // Mock school portal user by adding testUser's profile to the school portal profile Ids set.
        GlobalVariables.schoolPortalProfileIds.add(testUser.ProfileId);

        System.runAs(u) {
            System.assert(GlobalVariables.isInOverlapPeriod());

            String currentYearString = GlobalVariables.getCurrentYearString();
            String priorYearString = GlobalVariables.getPreviousYearString();
            String nonEditableYearString = '1999-2000';

            System.assert(GlobalVariables.isEditableYear(currentYearString));
            System.assert(GlobalVariables.isEditableYear(priorYearString));
            System.assert(!GlobalVariables.isEditableYear(nonEditableYearString));
        }
    }

    @isTest
    private static void overrideTodayTest() {
        Profile_Type_Grouping__c schoolAdminPTG = new Profile_Type_Grouping__c();
        SchoolAdminPTG.Name = 'PTG School Name';
        schoolAdminPTG.Profile_Name__c = ProfileSettings.SchoolAdminProfileName;
        schoolAdminPTG.Is_School_Admin_Profile__c = true;
        insert schoolAdminPTG;

                User schoolUser;
        User parentUser;
        Contact c, parentA;
        
        System.assertEquals(true, FamilyPortalSettings.Individual_Account_Owner_Id != null);
        
        System.runAs(thisUser){
            Test.startTest();
                Account a = TestUtils.createAccount('test account', RecordTypes.schoolAccountTypeId, 500, true);
                c = TestUtils.createContact('test contact', a.Id, RecordTypes.schoolStaffContactTypeId, false);
                parentA = TestUtils.createContact('Test', 'Parent A', null, RecordTypes.parentContactTypeId, false);
                insert new List<Contact>{c,parentA};
            Test.stopTest();

            parentA = [Select AccountId from Contact where Id=:parentA.Id limit 1];
            system.assertEquals('Test Parent A Account', [Select Name from Account where Id=:parentA.AccountId limit 1].Name);

            Id schoolPortalProfileId = [select Id from Profile where Name = :ProfileSettings.SchoolAdminProfileName].Id;
            Id familyPortalProfileId = [select Id from Profile where Name = :ProfileSettings.FamilyProfileName].Id;
            schoolUser = TestUtils.createPortalUser('test portal user', 'nais@exponentpartners.comschool', 'testP', c.Id, schoolPortalProfileId, true, false);
            parentUser = TestUtils.createPortalUser('test family user', 'nais@exponentpartners.comfamily', 'testF', parentA.Id, familyPortalProfileId, true, false);
            Database.insert(new List<User>{schoolUser, parentUser});
        }

        Date today = Date.today();
        SchoolPortalSettings.Overlap_Start_Date = today.addDays(-5);
        SchoolPortalSettings.Overlap_End_Date = today.addDays(5);

        System.AssertEquals(today, GlobalVariables.getCurrentDate());

        System.runAs(schoolUser){
            System.AssertEquals(today, GlobalVariables.getCurrentDate());
        }

        System.runAs(parentUser){
            System.AssertEquals(today, GlobalVariables.getCurrentDate());
        }

        FamilyPortalSettings.Override_Today = today.addDays(4);

        SchoolPortalSettings.Override_Today = today.addDays(6);

        System.AssertEquals(today.addDays(4), GlobalVariables.getCurrentDate());

        System.runAs(schoolUser){
            System.AssertEquals(today.addDays(6), GlobalVariables.getCurrentDate());
        }
        System.runAs(parentUser){
            System.AssertEquals(today.addDays(4), GlobalVariables.getCurrentDate());
        }
    }

    @isTest
    private static void getCurrentAcademicYearsTest() {
        GlobalVariablesTest.CreateData();

        List<Academic_Year__c> years = GlobalVariables.getCurrentAcademicYears(date.newInstance(System.today().year(), 4, 2));
        System.assertEquals(1, years.size(), 'Current Academic Years: ' + years);

        years = GlobalVariables.getCurrentAcademicYears(date.newInstance(System.today().year(), 5, 2));
        System.assertEquals(1, years.size(), 'Current Academic Years: ' + years);

        System.assertEquals('2011', GlobalVariables.getDocYearStringFromAcadYearName('2012-2013'));
    }

    @isTest
    private static void getCurrentAnnualSettingsTest() {
        GlobalVariablesTest.CreateData();

        System.runAs(u) {
            List<Annual_Setting__c> settings = GlobalVariables.getCurrentAnnualSettings();

            settings = GlobalVariables.getCurrentAnnualSettings(false, date.newInstance(System.today().year(), 4, 2));
            System.assertEquals(1, settings.size());

            settings = GlobalVariables.getCurrentAnnualSettings(false, date.newInstance(System.today().year(), 5, 2));
            System.assertEquals(1, settings.size());
            System.assertEquals(0, settings[0].Total_Waivers_Override_Default__c);
            System.assertEquals(null, settings[0].Waivers_Assigned__c);

            // new annual settings created has waiver counts initialized
            //in former unit test this was hard-coded to 1/1/2012
            //When there's no AnnualSetting record for previous academic year. Then Waivers_Assigned__c is set to 0. Otherwise, Waivers_Assigned__c is null.
            String firstAcademicYearName = [SELECT Id, Name FROM Academic_Year__c ORDER BY Start_Date__c ASC LIMIT 1].Name;
            Integer firstAcademicYearInt = Integer.ValueOf(firstAcademicYearName.left(4));
            settings = GlobalVariables.getCurrentAnnualSettings(false, date.newInstance(firstAcademicYearInt, 1, 31));
            System.assertEquals(1, settings.size());
            //SFP-858:School_Portal_Settings__c.Total_Fee_Waivers__c (Removed)
            //System.assertEquals(20, settings[0].Total_Waivers_Override_Default__c);
            System.assertEquals(0, settings[0].Waivers_Assigned__c);
        }
    }

    @isTest
    private static void testProfileIdMethods(){
        Id profileId = [Select Id from Profile where Name = 'Standard Platform User'][0].Id;

        Profile_Type_Grouping__c ptg = new Profile_Type_Grouping__c();
        ptg.Name = 'Standard Platform User';
        ptg.Profile_Name__c = 'Standard Platform User';
        insert ptg;

        u = new User(ProfileId = profileId);

        System.assertEquals(false, GlobalVariables.isSchoolPortalAdminProfile(u));
        System.assertEquals(false, GlobalVariables.isCallCenterUser(u));
        System.assertEquals(false, GlobalVariables.isSysAdminUser(u));
        System.assertEquals(false, GlobalVariables.isSchoolPortalProfile(u.ProfileId));

        ptg.Is_School_Admin_Profile__c = false;
        ptg.Is_Call_Center_Profile__c = false;
        ptg.Is_Sys_Admin__c = false;
        ptg.Is_School_Profile__c = true;
        update ptg;

        // need to rerun this to fake new context
        GlobalVariables.populateProfileIds();

        System.assertEquals(false, GlobalVariables.isSchoolPortalAdminProfile(u));
        System.assertEquals(false, GlobalVariables.isCallCenterUser(u));
        System.assertEquals(false, GlobalVariables.isSysAdminUser(u));
        System.assertEquals(true, GlobalVariables.isSchoolPortalProfile(u.ProfileId));

        ptg.Is_School_Admin_Profile__c = false;
        ptg.Is_Call_Center_Profile__c = false;
        ptg.Is_Sys_Admin__c = true;
        ptg.Is_School_Profile__c = true;
        update ptg;

        // need to rerun this to fake new context
        GlobalVariables.populateProfileIds();

        System.assertEquals(false, GlobalVariables.isSchoolPortalAdminProfile(u));
        System.assertEquals(false, GlobalVariables.isCallCenterUser(u));
        System.assertEquals(true, GlobalVariables.isSysAdminUser(u));
        System.assertEquals(true, GlobalVariables.isSchoolPortalProfile(u.ProfileId));

        ptg.Is_School_Admin_Profile__c = false;
        ptg.Is_School_Profile__c = true;
        ptg.Is_Sys_Admin__c = true;
        ptg.Is_Call_Center_Profile__c = true;
        ptg.Is_Databank__c = true;
        update ptg;

        // need to rerun this to fake new context
        GlobalVariables.populateProfileIds();

        System.assertEquals(false, GlobalVariables.isSchoolPortalAdminProfile(u));
        System.assertEquals(true, GlobalVariables.isCallCenterUser(u));
        System.assertEquals(true, GlobalVariables.isCallCenterUser(u.ProfileId));
        System.assertEquals(true, GlobalVariables.isSysAdminUser(u));
        System.assertEquals(true, GlobalVariables.isSchoolPortalProfile(u.ProfileId));
        System.assertEquals(true, GlobalVariables.isDatabankUser(u));

        ptg.Is_School_Admin_Profile__c = true;
        ptg.Is_School_Profile__c = true;
        ptg.Is_Sys_Admin__c = true;
        ptg.Is_Call_Center_Profile__c = true;
        ptg.Is_Databank__c = false;
        update ptg;

        // need to rerun this to fake new context
        GlobalVariables.populateProfileIds();
        User userNullValue = null;

        System.assertEquals(true, GlobalVariables.isSchoolPortalAdminProfile(u));
        System.assertEquals(false, GlobalVariables.isSchoolPortalAdminProfile(userNullValue));
        System.assertEquals(true, GlobalVariables.isCallCenterUser(u));
        System.assertEquals(false, GlobalVariables.isCallCenterUser(userNullValue));
        System.assertEquals(true, GlobalVariables.isSysAdminUser(u));
        System.assertEquals(true, GlobalVariables.isSysAdminUser(userNullValue));
        System.assertEquals(true, GlobalVariables.isSysAdminUser(u.ProfileId));
        System.assertEquals(true, GlobalVariables.isSchoolPortalProfile(u.ProfileId));
        System.assertEquals(false, GlobalVariables.isDatabankUser(u));
        System.assertEquals(false, GlobalVariables.isDatabankUser(userNullValue));
        System.assertEquals(false, GlobalVariables.isLimitedSchoolView());
    }

    @isTest
    private static void testGettersForProfileIdSets(){
        Id profileId = [Select Id from Profile where Name = 'Standard Platform User'][0].Id;

        Profile_Type_Grouping__c ptg = new Profile_Type_Grouping__c();
        ptg.Name = 'Standard Platform User';
        ptg.Profile_Name__c = 'Standard Platform User';
        ptg.Is_School_Admin_Profile__c = true;
        ptg.Is_School_Profile__c = true;
        ptg.Is_Sys_Admin__c = true;
        ptg.Is_Call_Center_Profile__c = true;
        insert ptg;

        System.assert(GlobalVariables.callCenterProfileIds.size() > 0);
    }

    @isTest
    private static void testDefaultValues() {
        Account a = TestUtils.createAccount('test account', RecordTypes.schoolAccountTypeId, 500, true);
        Contact c = TestUtils.createContact('test contact', a.Id, RecordTypes.schoolStaffContactTypeId, true);

        Id portalProfileId = [select Id from Profile where Name = :ProfileSettings.SchoolAdminProfileName].Id;

        u = TestUtils.createPortalUser('test portaluser', 'testGV@exponentpartners.com' + String.valueOf(uniquefier++), 'test', c.Id, portalProfileId, true, false);
        System.runAs(thisUser){
            Database.insert(u);
        }

        TestUtils.insertAccountShare(c.AccountId, u.Id);

        List<Academic_Year__c> years = TestUtils.createAcademicYears();


        System.runAs(u) {

            List<Annual_Setting__c> settings = GlobalVariables.getCurrentAnnualSettings(true,years.get(0).Id);
            System.assert(settings.size() > 0);
            Annual_Setting__c aSetting = settings.get(0);
            System.assert(String.isNotBlank(aSetting.Apply_Minimum_Income__c));
            System.assert(String.isNotBlank(aSetting.Use_Home_Equity__c));

            delete aSetting;
            settings = GlobalVariables.getCurrentAnnualSettings(true,system.today(),years.get(0).Id, null);
            aSetting = settings.get(0);
            system.assertNotEquals(null, aSetting.Id);
            system.assertEquals(false, aSetting.Don_t_collect_prior_year_documents_New__c);
            system.assertEquals(false, aSetting.Don_t_collect_prior_year_documents_Ret__c);
            system.assertEquals(false, aSetting.Don_t_collect_current_year_document_New__c);
            system.assertEquals(false, aSetting.Don_t_collect_current_year_document_Ret__c);
            system.assertEquals(null, aSetting.PFS_New__c);
            system.assertEquals(null, aSetting.PFS_Returning__c);
            system.assertEquals(null, aSetting.PFS_Deadline_Type__c);
            system.assertEquals(null, aSetting.PFS_Reminder_Date__c);
            system.assertEquals(null, aSetting.PFS_Reminder_Date_Returning__c);
            system.assertEquals(null, aSetting.Prior_Year_Tax_Documents_New__c);
            system.assertEquals(null, aSetting.Prior_Year_Tax_Documents_Returning__c);
            system.assertEquals(null, aSetting.Prior_Year_Tax_Documents_Deadline_Type__c);
            system.assertEquals(null, aSetting.Document_Reminder_Date__c);
            system.assertEquals(null, aSetting.Document_Reminder_Date_Returning__c);
            system.assertEquals(null, aSetting.Current_Year_Tax_Documents_New__c);
            system.assertEquals(null, aSetting.Current_Year_Tax_Documents_Returning__c);
            system.assertEquals(null, aSetting.Current_Year_Tax_Documents_Deadline_Type__c);
            system.assertEquals(null, aSetting.Curr_Year_Tax_Docs_Reminder_Date_New__c);
            system.assertEquals(null, aSetting.Curr_Yr_Tax_Docs_Remind_Date_Returning__c);
        }
    }

    @isTest
    private static void isFamilyPortalUser_asFamilyPortalUser_expectTrue() {
        User familyUser = UserTestData.createFamilyPortalUser();

        System.runAs(familyUser) {
            System.assert(GlobalVariables.isFamilyPortalUser(), 'Expected true for a family portal user.');
        }
    }

    @isTest
    private static void isFamilyPortalUser_asSchoolPortalUser_expectFalse() {
        User schoolPortalUser = UserTestData.insertSchoolPortalUser();

        System.runAs(schoolPortalUser) {
            System.assert(!GlobalVariables.isFamilyPortalUser(), 'Expected true for a school portal user.');
        }
    }

    @isTest
    private static void testGetActiveSSSStatusForTest()
    {
        String status = 'Current';
        GlobalVariables.activeSSSStatuses = new Set<String>{status};

        Test.startTest();
            String activeStatus = GlobalVariables.getActiveSSSStatusForTest();
        Test.stopTest();

        System.assertEquals(status, activeStatus);
    }

    @isTest
    private static void testGetCurrentSchoolSSSNumber()
    {
        Profile_Type_Grouping__c ptgSchoolPortalUser = new Profile_Type_Grouping__c(
            Name = 'School Portal User', Profile_Name__c = ProfileSettings.SchoolUserProfileName, Is_School_Profile__c = true);
        Profile_Type_Grouping__c ptgAdminUser = new Profile_Type_Grouping__c(
            Name = 'System Administrator', Profile_Name__c = 'System Administrator', Is_Sys_Admin__c = true);
        insert new List<Profile_Type_Grouping__c>{ptgSchoolPortalUser, ptgAdminUser};
        Account school = AccountTestData.Instance.asSchool().DefaultAccount;
        Contact staff = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(school.Id).DefaultContact;
        User schoolPortalUser;
        System.runAs(thisUser)
        {
            schoolPortalUser = UserTestData.Instance
                .forContactId(staff.Id)
                .forProfileId(GlobalVariables.schoolPortalUserProfileId).insertUser();
        }
        TestUtils.insertAccountShare(staff.AccountId, schoolPortalUser.Id);

        Test.startTest();
            System.assertEquals(null, GlobalVariables.getCurrentSchoolSSSNumber());

            System.runAs(schoolPortalUser)
            {
                GlobalVariables.allSchoolsForUser = null;
                System.assertEquals(null, GlobalVariables.getCurrentSchoolSSSNumber());
                
                GlobalVariables.allSchoolsForUser = null;
                String sssSchoolCode = '1000';
                school.SSS_School_Code__c = sssSchoolCode;
                update school;

                System.assertEquals(sssSchoolCode, GlobalVariables.getCurrentSchoolSSSNumber());
            }
        Test.stopTest();
    }

    @isTest
    private static void testGlobalMessages()
    {
        Global_Message__c globalMessageAllPages = GlobalMessageTestData.Instance.create();
        Global_Message__c globalMessageLandingPage = GlobalMessageTestData.Instance
            .forPortal(GlobalVariables.GM_PORTAL_FAMILY_ALL_PAGES).create();
        insert new List<Global_Message__c>{globalMessageAllPages, globalMessageLandingPage};

        Test.startTest();
            List<Global_Message__c> globalMessages = GlobalVariables.GlobalMessages('School Login', false);
            System.assertEquals(0, globalMessages.size());

            globalMessages = GlobalVariables.GlobalMessages('Family Login', false);
            system.assertEquals(2, globalMessages.size());
        Test.stopTest();
    }

    @isTest
    private static void testIsFamilyLoginLandingPage()
    {
        Test.startTest();
            PageReference pageRef = Page.FamilyLoginConfirmation;
            system.Test.setCurrentPage(pageRef);
            system.assert(!GlobalVariables.isFamilyLoginLandingPage());

            pageRef = Page.FamilyLogin;
            system.Test.setCurrentPage(pageRef);
            system.assert(GlobalVariables.isFamilyLoginLandingPage());
        Test.stopTest();
    }

    @isTest
    private static void testCallCenterProfileIds()
    {
        Profile_Type_Grouping__c ptgCallCenterUser = new Profile_Type_Grouping__c(
            Name = 'NAIS CST User', Profile_Name__c = 'NAIS CST User', Is_Call_Center_Profile__c = true);
        insert new List<Profile_Type_Grouping__c>{ptgCallCenterUser};

        Test.startTest();
            System.assertEquals(1, GlobalVariables.callCenterProfileIds.size());
        Test.stopTest();
    }

    @isTest
    private static void testIsRenewalSeason_ExpectTrue()
    {
        Renewal_Annual_Settings__c rs = createRenewalAnnualSettings(true);

        Test.startTest();
            System.assert(!GlobalVariables.isRenewalSeason());

            insert rs;
            System.assert(GlobalVariables.isRenewalSeason());
        Test.stopTest();
    }

    @isTest
    private static void testIsRenewalSeason_ExpectFalse()
    {
        Date currentDate = Date.today();
        Renewal_Annual_Settings__c rs = new Renewal_Annual_Settings__c(
            Name = String.valueOf(currentDate.year()),
            Renewal_Season_Start_Date__c = currentDate.addMonths(+1),
            Early_Bird_End_Date__c = currentDate.addMonths(+2),
            Renewal_Season_End_Date__c = currentDate.addMonths(+3));

        Test.startTest();
            System.assert(!GlobalVariables.isRenewalSeason());

            insert rs;
            System.assert(!GlobalVariables.isRenewalSeason());
        Test.stopTest();
    }

    @isTest
    private static void testIsEarlyBirdRenewal_expectTrue()
    {
        Renewal_Annual_Settings__c rs = createRenewalAnnualSettings(true);

        Test.startTest();
            System.assert(!GlobalVariables.isEarlyBirdRenewal());

            insert rs;
            System.assert(GlobalVariables.isEarlyBirdRenewal());
        Test.stopTest();
    }

    @isTest
    private static void testIsEarlyBirdRenewal_expectFalse()
    {
        Renewal_Annual_Settings__c rs = createRenewalAnnualSettings(false);

        Test.startTest();
            System.assert(!GlobalVariables.isEarlyBirdRenewal());

            insert rs;
            System.assert(!GlobalVariables.isEarlyBirdRenewal());
        Test.stopTest();
    }

    @isTest
    private static void testGetCallCenterProfileIds()
    {
        Profile_Type_Grouping__c ptg = new Profile_Type_Grouping__c(
            Name = 'NAIS CST User', Profile_Name__c = 'NAIS CST User', Is_Call_Center_Profile__c = true);
        insert ptg;

        Test.startTest();
            System.assertEquals(1, GlobalVariables.getCallCenterProfileIds().size());
        Test.stopTest();
    }

    @isTest
    private static void testUsersSchoolHasBudgetGroups()
    {
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
        Academic_Year__c priorAcademicYear = AcademicYearTestData.Instance.create();
        insert new List<Academic_Year__c>{currentAcademicYear, priorAcademicYear};

        Account school = AccountTestData.Instance.asSchool().DefaultAccount;
        Contact staff = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(school.Id).DefaultContact;
        User schoolPortalUser;
        System.runAs(thisUser)
        {
            schoolPortalUser = UserTestData.Instance
                .forContactId(staff.Id)
                .forProfileId(GlobalVariables.schoolPortalUserProfileId).insertUser();
        }
        TestUtils.insertAccountShare(staff.AccountId, schoolPortalUser.Id);

        Test.startTest();
            System.runAs(schoolPortalUser)
            {
                Budget_Group__c budgetGroup = BudgetGroupTestData.Instance
                    .forAcademicYearId(currentAcademicYear.Id).insertBudgetGroup();
            
                System.assert(GlobalVariables.usersSchoolHasBudgetGroups(currentAcademicYear.Id));

                System.assert(!GlobalVariables.usersSchoolHasBudgetGroups(priorAcademicYear.Id));
            }
        Test.stopTest();
    }

    @isTest
    private static void testGetAcademicYearMethods()
    {
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
        Academic_Year__c priorAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
        insert new List<Academic_Year__c>{currentAcademicYear, priorAcademicYear};

        Test.startTest();
            System.assertEquals(currentAcademicYear.Id, GlobalVariables.getAcademicYearByName(currentAcademicYear.Name).Id);
            System.assertEquals(priorAcademicYear.Id, GlobalVariables.getPreviousAcademicYear().Id);

            //Line below is necessary to test ELSE statement in the getPreviousAcademicYear method
            System.assertEquals(priorAcademicYear.Id, GlobalVariables.getPreviousAcademicYear().Id);

            System.assertEquals(priorAcademicYear.Id, GlobalVariables.getAcademicYear(priorAcademicYear.Id).Id);

            System.assertEquals(priorAcademicYear.Name, GlobalVariables.getPreviousAcademicYear(currentAcademicYear.Id).Name);

            System.assertEquals(currentAcademicYear.Id, GlobalVariables.getCurrentOrSelectedAcadYearForFamily().Id);
            System.assertEquals(2, GlobalVariables.getCurrentAcademicYears().size());
        Test.stopTest();
    }

    @isTest
    private static void testGetMostRecentSubscription()
    {
        Account school = AccountTestData.Instance.asSchool().DefaultAccount;
        Contact staff = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(school.Id).DefaultContact;
        User schoolPortalUser;
        System.runAs(thisUser)
        {
            schoolPortalUser = UserTestData.Instance
                .forContactId(staff.Id)
                .forProfileId(GlobalVariables.schoolPortalUserProfileId).insertUser();
        }
        TestUtils.insertAccountShare(staff.AccountId, schoolPortalUser.Id);
        Subscription__c subscription1 = SubscriptionTestData.Instance
            .forEndDate(Date.Today().addDays(5))
            .create();
        Subscription__c subscription2 = SubscriptionTestData.Instance
            .forEndDate(Date.Today().addDays(10))
            .create();
        Subscription__c mostRecentSubscription = SubscriptionTestData.Instance
            .forEndDate(Date.Today().addDays(15))
            .create();
        
        Test.startTest();
            System.runAs(schoolPortalUser)
            {
                System.assertEquals(new Subscription__c(),  GlobalVariables.getMostRecentSubscription());

                insert new List<Subscription__c>{subscription1, subscription2, mostRecentSubscription};
                System.assertEquals(mostRecentSubscription.Id,  GlobalVariables.getMostRecentSubscription().Id);
            }
        Test.stopTest();
    }

    @isTest
    private static void testGetFolderAndSchoolPFSAssignments()
    {
        Student_Folder__c defaultStudentFolder =  StudentFolderTestData.Instance.DefaultStudentFolder;
        School_PFS_Assignment__c defaultSpfsa = SchoolPfsAssignmentTestData.Instance.DefaultSchoolPfsAssignment;

        Test.startTest();
            Student_Folder__c studentFolder = GlobalVariables.getFolderAndSchoolPFSAssignments(
                StudentFolderTestData.Instance.DefaultStudentFolder.Id);
        Test.stopTest();

        System.assertEquals(defaultStudentFolder.Id, studentFolder.Id);
        System.assertEquals(1, studentFolder.School_PFS_Assignments__r.size());
        System.assertEquals(defaultSpfsa.Id, studentFolder.School_PFS_Assignments__r[0].Id);
    }

    @isTest
    private static void testGetSiblingApplicantSchoolPFSAssignments()
    {
        Student_Folder__c studentFolder1 = StudentFolderTestData.Instance.DefaultStudentFolder;
        School_PFS_Assignment__c spfsa1 = SchoolPfsAssignmentTestData.Instance.DefaultSchoolPfsAssignment;

        Contact siblingStudent = ContactTestData.Instance.asStudent().insertContact();
        Student_Folder__c siblingStudentFolder = StudentFolderTestData.Instance
            .forStudentId(siblingStudent.Id).insertStudentFolder();
        Applicant__c siblingApplicant = ApplicantTestData.Instance
            .forContactId(siblingStudent.Id).insertApplicant();
        School_PFS_Assignment__c siblingSpfsa = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(siblingApplicant.Id)
            .forStudentFolderId(siblingStudentFolder.Id).insertSchoolPfsAssignment();

        Test.startTest();
            System.assertEquals(
                new List<School_PFS_Assignment__c>(), GlobalVariables.getSiblingApplicantSchoolPFSAssignments(null, null));

            List<School_PFS_Assignment__c> spfsas = GlobalVariables.getSiblingApplicantSchoolPFSAssignments(
                new Set<Id>{PfsTestData.Instance.DefaultPfs.Id}, studentFolder1);

            System.assertEquals(1, spfsas.size());
        Test.stopTest();

        System.assertEquals(siblingSpfsa.Id, spfsas[0].Id);
    }

    @isTest
    private static void testGetDependents()
    {
        Dependents__c dependent = DependentsTestData.Instance.DefaultDependents;

        Test.startTest();
            System.assert(GlobalVariables.getDependents(null).isEmpty());

            List<Dependents__c> dependents = GlobalVariables.getDependents(new Set<Id>{PfsTestData.Instance.DefaultPfs.Id});
            System.assertEquals(1, dependents.size());
            System.assertEquals(dependent.Id, dependents[0].Id);
        Test.stopTest();
    }

    @isTest
    private static void testPfsIsSubmitted()
    {
        Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.DefaultAcademicYear;

        Contact student1 = ContactTestData.Instance.create();
        Contact student2 = ContactTestData.Instance.create();
        insert new List<Contact> {student1, student2};
        
        Student_Folder__c studentFolder1 = StudentFolderTestData.Instance
            .forStudentId(student1.Id).create();
        Student_Folder__c studentFolder2 = StudentFolderTestData.Instance
            .forStudentId(student2.Id).create();
        insert new List<Student_Folder__c> {studentFolder1, studentFolder2};

        Pfs__c inProgressPfs = PfsTestData.Instance
            .forAcademicYearPicklist(currentAcademicYear.Name).create();
        Pfs__c submittedAndPaidPfs = PfsTestData.Instance
            .forAcademicYearPicklist(currentAcademicYear.Name)
            .asSubmitted()
            .asPaid().create();
        insert new List<Pfs__c>{inProgressPfs, submittedAndPaidPfs};

        Applicant__c applicant1 = ApplicantTestData.Instance
            .forContactId(student1.Id)
            .forPfsId(inProgressPfs.Id).create();
        Applicant__c applicant2 = ApplicantTestData.Instance
            .forContactId(student2.Id)
            .forPfsId(submittedAndPaidPfs.Id).create();
        insert new List<Applicant__c>{applicant1, applicant2};

        School_PFS_Assignment__c spfsa1 = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicant1.Id)
            .forStudentFolderId(studentFolder1.Id).create();
        School_PFS_Assignment__c spfsa2 = SchoolPfsAssignmentTestData.Instance
            .forApplicantId(applicant2.Id)
            .forStudentFolderId(studentFolder2.Id).create();
        insert new List<School_PFS_Assignment__c>{spfsa1, spfsa2};

        School_PFS_Assignment__c nullValueSpfsa = null;

        Test.startTest();
            System.assert(!GlobalVariables.pfsIsSubmitted(nullValueSpfsa));
            System.assert(!GlobalVariables.pfsIsSubmitted(spfsa1));
            System.assert(GlobalVariables.pfsIsSubmitted(spfsa2));
        Test.stopTest();
    }

    @isTest
    private static void testIsVerifiableDocumentType()
    {
        String document1NameValue = 'Document1';
        String document2Value = 'Document2';
        String document2Name = GlobalVariables.VERIFIABLE_DOCUMENT_STRING + '1';
        Document_Custom_Settings__c docCS1 = new Document_Custom_Settings__c(
            Name = document1NameValue, Value__c = document1NameValue, Active__c = true);
        Document_Custom_Settings__c docCS2 = new Document_Custom_Settings__c(
            Name = document2Name, Value__c = document2Value, Active__c = true);
        insert new List<Document_Custom_Settings__c>{docCS1, docCS2};

        Test.startTest();
            System.assert(!GlobalVariables.isVerifiableDocumentType(null));
            System.assert(!GlobalVariables.isVerifiableDocumentType(document1NameValue));
            System.assert(GlobalVariables.isVerifiableDocumentType(document2Value));
        Test.stopTest();
    }

    @isTest
    private static void testUserHasAcceptedTermsAndConditions()
    {
        Account school = AccountTestData.Instance.asSchool().create();
        Account family = AccountTestData.Instance.asFamily().create();
        insert new List<Account>{school, family};

        Academic_Year__c academicYear = AcademicYearTestData.Instance.DefaultAcademicYear;

        Contact staff = ContactTestData.Instance
            .asSchoolStaff()
            .forAccount(school.Id).create();
        Contact parent = ContactTestData.Instance
            .asParent()
            .forAccount(family.Id).create();
        insert new List<Contact>{staff, parent};

        User schoolPortalUser = UserTestData.Instance
            .forUsername(UserTestData.generateTestEmail())
            .forContactId(staff.Id)
            .forSystemTermsAndConditionsAccepted(System.now().addDays(-1))
            .forProfileId(GlobalVariables.schoolPortalUserProfileId).create();
        User systemAdmin = UserTestData.Instance
            .forUsername(UserTestData.generateTestEmail())
            .forProfileId(GlobalVariables.sysAdminProfileId).create();
        User familyPortalUser = UserTestData.Instance
            .forUsername(UserTestData.generateTestEmail())
            .forContactId(parent.Id)
            .forSystemTermsAndConditionsAccepted(System.now().addDays(-1))
            .forProfileId(GlobalVariables.familyPortalProfileId).create();

        Help_Configuration__c helpConfig = new Help_Configuration__c(
            Academic_Year__c = academicYear.Id, Language__c = thisUser.LanguageLocaleKey,
            School_Terms_and_Conditions_Date_Time__c = System.now().addDays(-1),
            NAIS_Terms_and_Conditions_Date_Time__c = System.now().addDays(-1));
        
        System.runAs(thisUser)
        {
            insert new List<User>{schoolPortalUser, systemAdmin, familyPortalUser};
        }
        TestUtils.insertAccountShare(staff.AccountId, schoolPortalUser.Id);


        Test.startTest();
            System.runAs(systemAdmin)
            {
                System.assert(GlobalVariables.userHasAcceptedTermsAndConditions(false));
            }

            System.runAs(schoolPortalUser)
            {
                System.assert(GlobalVariables.userHasAcceptedTermsAndConditions(true));
            }

            insert helpConfig;
            
            System.runAs(schoolPortalUser)
            {
                System.assert(GlobalVariables.userHasAcceptedTermsAndConditions(true));
            }
            
            System.runAs(familyPortalUser)
            {
                System.assert(GlobalVariables.userHasAcceptedTermsAndConditions(false));
            }
        Test.stopTest();
    }
    
    private static void CreateData()
    {
        Account a = TestUtils.createAccount('test account', RecordTypes.schoolAccountTypeId, 500, true);
        Contact c = TestUtils.createContact('test contact', a.Id, RecordTypes.schoolStaffContactTypeId, true);

        Id portalProfileId = [select Id from Profile where Name = :ProfileSettings.SchoolAdminProfileName].Id;

        u = TestUtils.createPortalUser(
            'test portaluser', 'testGV@exponentpartners.com' + String.valueOf(uniquefier++), 'test', c.Id, portalProfileId, true, false);
        System.runAs(thisUser){
            Database.insert(u);
        }

        List<Academic_Year__c> years = AcademicYearTestData.Instance.insertAcademicYears(7);

        List<Annual_Setting__c> settings = new List<Annual_Setting__c>();
        Integer i = 0;
        for(Academic_Year__c year : years)
        {
            // skip last year
            if (i < years.size() - 1)
            {
                Annual_Setting__c annualSetting = new Annual_Setting__c(
                    School__c = a.Id,
                    Academic_Year__c = year.Id,
                    PFS_Reminder_Date__c = date.newInstance(2013, 3, 31),
                    Document_Reminder_Date__c = date.newInstance(2013, 3, 31));

                settings.add(annualSetting);
            }
            i++;
        }

        insert settings;
    }

    private static Renewal_Annual_Settings__c createRenewalAnnualSettings(Boolean isEarlyBird)
    {
        Renewal_Annual_Settings__c renewalAnnualSetting;
        Date currentDate = Date.Today();

        if (isEarlyBird)
        {
            renewalAnnualSetting = new Renewal_Annual_Settings__c(
                Name = String.valueOf(currentDate.year()),
                Renewal_Season_Start_Date__c = currentDate.addMonths(-1),
                Early_Bird_End_Date__c = currentDate.addDays(+1),
                Renewal_Season_End_Date__c = currentDate.addMonths(+1));
        } 
        else
        {
            renewalAnnualSetting = new Renewal_Annual_Settings__c(
                Name = String.valueOf(currentDate.year()),
                Renewal_Season_Start_Date__c = currentDate.addMonths(-1),
                Early_Bird_End_Date__c = currentDate.addDays(-1),
                Renewal_Season_End_Date__c = currentDate.addMonths(+1));
        }

        return renewalAnnualSetting;
    }
    
    @isTest private static void getCurrentAnnualSettings_previousAnnualSettingWithValuesForFeeWaiversFields_newAnnualSettingWithCleanFeeWaiverFields() {
        
        //Create current and previous academic years.
		Academic_Year__c currentAcademicYear = AcademicYearTestData.Instance.create();
		Academic_Year__c priorAcademicYear = AcademicYearTestData.Instance.asPreviousYear().create();
		insert new List<Academic_Year__c>{currentAcademicYear, priorAcademicYear};

        //Create a School.
		Account school = AccountTestData.Instance.asSchool().create();
		insert school;
		
		//Create Annual Setting.
		Annual_Setting__c previousAnnualSetting = AnnualSettingsTestData.Instance
		    .forSchoolId(school.Id)
		    .forAcademicYearId(priorAcademicYear.Id)
		    .create();
	    previousAnnualSetting.Waivers_Assigned__c = 10;
	    previousAnnualSetting.Total_Waivers_Override_Default__c = 10;
	    insert previousAnnualSetting;
	    
	    Test.StartTest();
	       
	       List<Annual_Setting__c> currentAnnualSettings = GlobalVariables.getCurrentAnnualSettings(true, system.today(), currentAcademicYear.Id, school.Id);
	       Annual_Setting__c currentAnnualSetting = currentAnnualSettings[0];
	       
	       currentAnnualSetting = [
	           SELECT Id, Waivers_Assigned__c, Total_Waivers_Override_Default__c 
	           FROM Annual_Setting__c 
	           WHERE Id =: currentAnnualSetting.Id LIMIT 1];
	       
	       System.AssertEquals(0, currentAnnualSetting.Waivers_Assigned__c);
	       System.AssertEquals(0, currentAnnualSetting.Total_Waivers_Override_Default__c);
	    Test.StopTest();
    }
}