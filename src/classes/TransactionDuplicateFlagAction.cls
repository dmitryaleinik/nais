public class TransactionDuplicateFlagAction {
    public static boolean canRun = true;

    public static void flagDuplicateTransactions(List<Transaction_Line_Item__c> triggerNew) {
        Set<ID> oppids = new Set<ID>();
        for(Transaction_Line_Item__c lineitem: triggerNew) {
            if (lineitem.Transaction_Category__c == 'Payment') {
                oppids.add(lineitem.Opportunity__c);
            }
        }
        if (oppids.size()==0)
            return;
         Map<ID,Opportunity> oppMap = new Map<ID,Opportunity> ([select id, Net_Amount_Due__c,Count_of_Payments__c from Opportunity where id in :oppids]);

        for(Transaction_Line_Item__c lineitem: triggerNew) {
            System.debug ('payment  '  +lineitem.Transaction_Category__c+ ' ' + lineitem.Opportunity__r.Net_Amount_Due__c + ' ' + lineitem.Opportunity__r.Count_of_Payments__c);
            if (lineitem.Transaction_Category__c == 'Payment' &&
                oppMap.get(lineitem.Opportunity__c).Net_Amount_Due__c <= 0 &&
                oppMap.get(lineitem.Opportunity__c).Count_of_Payments__c > 0) {
                lineitem.Duplicate_Payment__c = true;
            }
        }
    }

}